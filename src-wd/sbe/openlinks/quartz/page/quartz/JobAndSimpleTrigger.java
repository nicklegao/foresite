package sbe.openlinks.quartz.page.quartz;

import java.util.Iterator;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;

/**
 * Provides a Quartz Job and Simple Trigger display class.
 */
public class JobAndSimpleTrigger
{

	private static final long ONE_SECOND_IN_MILISECONDS = 1000L;

	private final JobDetail jobDetail;
	private final Trigger trigger;
	private final Scheduler scheduler;

	public JobAndSimpleTrigger(JobDetail jobDetail, Trigger trigger, Scheduler scheduler)
	{
		this.jobDetail = jobDetail;
		this.trigger = trigger;
		this.scheduler = scheduler;
	}

	/**
 * 
 */
	public JobAndSimpleTrigger()
	{
		this.jobDetail = null;
		this.trigger = null;
		this.scheduler = null;
	}



	public JobDetail getJob()
	{
		return jobDetail;
	}

	public Trigger getTrigger()
	{
		return trigger;
	}

	public int getTriggerState()
	{
		try
		{
			return scheduler.getTriggerState(trigger.getName(), trigger.getGroup());
		}
		catch (Throwable th)
		{
			return Trigger.STATE_NONE;
		}
	}


	public String getTriggerStateAsString()
	{
		switch (getTriggerState())
		{
			case Trigger.STATE_NONE:
				return "None";
			case Trigger.STATE_NORMAL:
				return "Normal";
			case Trigger.STATE_PAUSED:
				return "Paused";
			case Trigger.STATE_BLOCKED:
				return "Running";
			case Trigger.STATE_COMPLETE:
				return "Complete";
			case Trigger.STATE_ERROR:
				return "Error";
		}
		return "Unknown";
	}

	public String getRepeat()
	{
		if (trigger instanceof CronTrigger)
			return "Cron Expression";

		long count = ((SimpleTrigger) trigger).getRepeatCount();

		if (count == -1)
			return "Continuously";
		else if (count == 0)
			return "Run once";
		else if (count == 1)
			return "Repeat once";
		else
			return "Repeat " + count;
	}

	public String getInterval()
	{
		if (trigger instanceof CronTrigger)
			return ((CronTrigger) trigger).getCronExpression();

		long interval = ((SimpleTrigger) trigger).getRepeatInterval() / ONE_SECOND_IN_MILISECONDS;

		if (interval == 1)
		{
			return "1 second";
		}
		else if (interval < 60)
		{
			return "" + interval + " seconds";
		}
		else
		{
			interval = interval / 60;
			if (interval == 1)
			{
				return "1 minute";
			}
			else if (interval < 60)
			{
				return "" + interval + " minutes";
			}
			else
			{
				interval = interval / 60;
				if (interval == 1)
				{
					return "1 hour";
				}
				else
				{
					return "" + interval + "hours";
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public boolean isExecuting() throws SchedulerException
	{
		for (Iterator i = scheduler.getCurrentlyExecutingJobs().iterator(); i.hasNext();)
		{
			JobExecutionContext context = (JobExecutionContext) i.next();
			if (context.getJobDetail().getFullName().equals(jobDetail.getFullName()))
			{
				return true;
			}
		}
		return false;
	}
}
