package sbe.openlinks.quartz.page.quartz;

public class JobHistory
{

	private String elementId;
	private String jobName;
	private String feedSource;
	private String rootPath;
	private String startDate;
	private String endDate;
	private String numberOfNodes;
	private String number_Succeed;
	private String number_Failed;
	private String number_Inserted;
	private String number_Updated;
	private String logFile;


	public String getJobName()
	{
		return jobName;
	}

	public void setJobName(String jobName)
	{
		this.jobName = jobName;
	}

	public String getFeedSource()
	{
		return feedSource;
	}

	public void setFeedSource(String feedSource)
	{
		this.feedSource = feedSource;
	}

	public String getRootPath()
	{
		return rootPath;
	}

	public void setRootPath(String rootPath)
	{
		this.rootPath = rootPath;
	}

	public String getStartDate()
	{
		return startDate;
	}

	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}

	public String getEndDate()
	{
		return endDate;
	}

	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}

	public String getNumberOfNodes()
	{
		return numberOfNodes;
	}

	public void setNumberOfNodes(String numberOfNodes)
	{
		this.numberOfNodes = numberOfNodes;
	}

	public String getNumber_Succeed()
	{
		return number_Succeed;
	}

	public void setNumber_Succeed(String number_Succeed)
	{
		this.number_Succeed = number_Succeed;
	}

	public String getNumber_Failed()
	{
		return number_Failed;
	}

	public void setNumber_Failed(String number_Failed)
	{
		this.number_Failed = number_Failed;
	}

	public String getNumber_Inserted()
	{
		return number_Inserted;
	}

	public void setNumber_Inserted(String number_Inserted)
	{
		this.number_Inserted = number_Inserted;
	}

	public String getNumber_Updated()
	{
		return number_Updated;
	}

	public void setNumber_Updated(String number_Updated)
	{
		this.number_Updated = number_Updated;
	}

	public String getElementId()
	{
		return elementId;
	}

	public void setElementId(String elementId)
	{
		this.elementId = elementId;
	}

	public String getLogFile()
	{
		return logFile;
	}

	public void setLogFile(String logFile)
	{
		this.logFile = logFile;
	}




}
