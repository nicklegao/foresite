package sbe.openlinks.quartz.page.quartz;

import org.quartz.Job;

public interface IFeedJob extends Job
{

	String getDownloadFileUrl();

	String getDownloadFilePath();

	String getConfigFileUrl();

	String getConfigFilePath();

	String getRemoteUrl();

	String getJobDescription();

}
