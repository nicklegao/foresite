package sbe.openlinks.quartz.page.quartz;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.ee.servlet.QuartzInitializerListener;
import org.quartz.impl.StdSchedulerFactory;

import au.net.webdirector.admin.quartz.JobScheduleObj;
import au.net.webdirector.admin.quartz.SchedulerService;

public class SchedulerServlet extends HttpServlet
{

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(SchedulerServlet.class);

	private Scheduler scheduler;

	private SchedulerService service = null;

	@Override
	public void init()
	{
		try
		{
			setService(getServletContext());
			scheduler = StdSchedulerFactory.getDefaultScheduler();
			System.out.println("STARTING SCHEDULER:" + scheduler);
			scheduler.start();

		}
		catch (Exception ex)
		{
			logger.error("SchedulerService init exception: ", ex);
			ex.printStackTrace();
		}
	}

	@Override
	public void destroy()
	{
		try
		{
			System.out.println("SHUTING DOWN SCHEDULER");
			scheduler.shutdown();
		}
		catch (Exception ex)
		{
			System.out.println("SCHEDULER DESTROY ERROR " + ex.getMessage());
			ex.printStackTrace();
		}
	}

	private void setService(ServletContext context)
	{
		if (service != null)
		{
			return;
		}
		StdSchedulerFactory schedulerFactory = (StdSchedulerFactory) context.getAttribute(QuartzInitializerListener.QUARTZ_FACTORY_KEY);
		if (schedulerFactory != null)
		{
			service = new SchedulerService(schedulerFactory);
		}
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		String result = reloadJobsFromOldTable(request, response);
		response.getOutputStream().println(result);
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		String result = reloadJobsFromOldTable(request, response);
		response.getOutputStream().println(result);
	}

	private String reloadJobsFromOldTable(HttpServletRequest request, HttpServletResponse response)
	{
//		String lineSeparater = "<br>";
		String lineSeparater = "\n";
		StringBuffer sb = new StringBuffer();
		List<JobScheduleObj> jobs = service.getStoredJobs();
		sb.append("Read " + jobs.size() + " job(s) from openlinks_jobs table").append(lineSeparater);
		for (JobScheduleObj job : jobs)
		{
			try
			{
				sb.append("Adding job : " + job.getJobDetail().getName()).append(lineSeparater);
				service.scheduleJob(job);
				sb.append("Job added").append(lineSeparater);
			}
			catch (Exception e)
			{
				JobDetail jobDetail = job.getJobDetail();
				logger.error("scheduleJob init error: " + (jobDetail == null ? "" : jobDetail.getName()), e);
				sb.append(e.getMessage()).append(lineSeparater);
			}
		}
		sb.append("Please consider removing data from openlinks_jobs table").append(lineSeparater);
		return sb.toString();
	}


}
