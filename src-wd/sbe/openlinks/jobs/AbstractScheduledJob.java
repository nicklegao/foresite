package sbe.openlinks.jobs;

import java.io.File;
import java.util.Date;
import java.util.Hashtable;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import sbe.openlinks.quartz.page.quartz.IFeedJob;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.utils.SimpleDateFormat;

public abstract class AbstractScheduledJob implements IFeedJob
{

	protected Logger logger = null;

	public void execute(JobExecutionContext context) throws JobExecutionException
	{

		// attach individual logger
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd-HH_mm_ss");
		SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String logFilePath = Defaults.getInstance().getStoreDir() + "/temp/" + this.getClass().getSimpleName() + "_" + dateFormat1.format(new Date()) + ".log";
		logger = getLogger(this.getClass(), logFilePath);
		// start to do job
		Date startDate = new Date();
		logger.info("==================[" + getJobDescription() + "] task start @ " + dateFormat2.format(startDate) + "===================");
		beforeJob(context);
		doJob(context);
		Date endDate = new Date();
		logger.info("==================[" + getJobDescription() + "] task end   @ " + dateFormat2.format(endDate) + "===================");
		// write result into history table
		int numberTotal = getTotal();
		int numberSucceed = getSuccessNo();
		int numberFailed = getFailedNo();

		Hashtable<String, String> hashtable = new Hashtable<String, String>();
		hashtable.put("JobName", getJobDescription());
		hashtable.put("StartDate", dateFormat2.format(startDate));
		hashtable.put("EndDate", dateFormat2.format(endDate));
		hashtable.put("NumberOfNodes", String.valueOf(numberTotal));
		hashtable.put("Number_Succeed", String.valueOf(numberSucceed));
		hashtable.put("Number_Failed", String.valueOf(numberFailed));
		hashtable.put("logFile", logFilePath.substring(logFilePath.lastIndexOf("/") + 1));
		int inserted = new DBaccess().insertData(hashtable, null, "openlinks_history");

		if (inserted < 0)
		{
			logger.error("exception COULD NOT SAVE JOB HISTORY FOR JOB " + getJobDescription() + " Number of Total : " + String.valueOf(numberTotal) + " Success: " + String.valueOf(numberSucceed) + " Failed:" + String.valueOf(numberFailed));
		}
	}

	public String getDownloadFileUrl()
	{
		// Don't need in this scenario, this was used in vito's openlinks job.
		return null;
	}

	public String getDownloadFilePath()
	{
		// Don't need in this scenario, this was used in vito's openlinks job.
		return null;
	}

	public String getConfigFileUrl()
	{
		// Don't need in this scenario, this was used in vito's openlinks job.
		return null;
	}

	public String getConfigFilePath()
	{
		// Don't need in this scenario, this was used in vito's openlinks job.
		return null;
	}

	public String getRemoteUrl()
	{
		// Don't need in this scenario, this was used in vito's openlinks job.
		return null;
	}


	private static Logger getLogger(Class processor, String filePath)
	{
		Logger log = Logger.getLogger(processor);
		try
		{

			File file = new File(filePath);
			FileAppender appender = new FileAppender(new PatternLayout("%-5p [%d{ISO8601}]: %m%n"), file.getAbsolutePath(), false);
			appender.setName("FileAppender");
			log.removeAllAppenders();
			log.addAppender(appender);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return log;
	}

	protected File getLogFile()
	{
		FileAppender appender = (FileAppender) logger.getAppender("FileAppender");
		return new File(appender.getFile());
	}

	public abstract void beforeJob(JobExecutionContext context);

	public abstract void doJob(JobExecutionContext context);

	public abstract int getTotal();

	public abstract int getSuccessNo();

	public abstract int getFailedNo();


}
