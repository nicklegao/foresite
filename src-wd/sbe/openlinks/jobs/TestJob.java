package sbe.openlinks.jobs;

import java.util.UUID;

import org.quartz.JobExecutionContext;

public class TestJob extends AbstractScheduledJob
{

	boolean success = false;
	String key = UUID.randomUUID().toString();

	@Override
	public String getJobDescription()
	{
		// TODO Auto-generated method stub
		return "Test";
	}

	@Override
	public void beforeJob(JobExecutionContext context)
	{
		// TODO Auto-generated method stub
		try
		{
			logger.info(key);
			Thread.sleep(1 * 1000);
		}
		catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		success = true;
	}

	@Override
	public void doJob(JobExecutionContext context)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public int getTotal()
	{
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public int getSuccessNo()
	{
		// TODO Auto-generated method stub
		return success ? 1 : 0;
	}

	@Override
	public int getFailedNo()
	{
		// TODO Auto-generated method stub
		return success ? 0 : 1;
	}


}
