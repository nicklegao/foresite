package sbe.openlinks.jobs;

import java.io.File;
import java.util.List;

import org.quartz.JobExecutionContext;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;

public class SBERemoveLogsJobRunner extends AbstractScheduledJob
{

	int total;
	int successNo;
	int failedNo;

	public String getJobDescription()
	{
		return "Remove Old Logs";
	}

	@Override
	public void doJob(JobExecutionContext context)
	{
		try
		{
			total = 0;
			successNo = 0;
			failedNo = 0;
			int days = 7;
			try
			{
				days = Integer.parseInt((String) context.getMergedJobDataMap().get("days"));
			}
			catch (Exception e)
			{
				// don't care
			}
			removeLogs(days);
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
		}
	}

	@Override
	public int getTotal()
	{
		return total;
	}

	@Override
	public int getSuccessNo()
	{
		return successNo;
	}

	@Override
	public int getFailedNo()
	{
		return failedNo;
	}

	private void removeLogs(int days)
	{
		DBaccess db = new DBaccess();
		String sql = "select Element_id, logFile from openlinks_history where datediff(now(), EndDate) > ?";
		//List<String[]> list = db.select(sql, new String[] {});
		List<String[]> list = db.selectQuery(sql, new Object[] { days });
		total = list.size();
		for (String[] info : list)
		{
			try
			{
				db.updateData("delete from openlinks_history where Element_id = ?", new Object[] { info[0] });
				String file = Defaults.getInstance().getOStypeTempDir().replace("\\", "/") + "/" + info[1];
				new File(file).delete();
				logger.info("Remove Log File [" + info[0] + ":" + info[1] + "]");
				successNo++;
			}
			catch (Exception e)
			{
				failedNo++;
			}
		}
	}

	@Override
	public void beforeJob(JobExecutionContext context)
	{
		total = 0;
		successNo = 0;
		failedNo = 0;
	}

}
