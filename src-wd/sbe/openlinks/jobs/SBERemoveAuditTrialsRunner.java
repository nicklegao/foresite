package sbe.openlinks.jobs;

import org.quartz.JobExecutionContext;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;

public class SBERemoveAuditTrialsRunner extends AbstractScheduledJob
{

	int total;
	int successNo;
	int failedNo;

	public String getJobDescription()
	{
		return "Remove Audit Trials";
	}

	@Override
	public void doJob(JobExecutionContext context)
	{
		try
		{
			int days = 365;
			try
			{
				days = Integer.parseInt((String) context.getMergedJobDataMap().get("days"));
			}
			catch (Exception e)
			{
				// don't care
			}
			removeTrials(days);
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
		}
	}

	@Override
	public int getTotal()
	{
		return total;
	}

	@Override
	public int getSuccessNo()
	{
		return successNo;
	}

	@Override
	public int getFailedNo()
	{
		return failedNo;
	}

	private void removeTrials(int days)
	{
		DBaccess db = new DBaccess();
		String sql = "delete from sys_audit_trial where datediff(now(), EVENT_WHEN) > ?";
		total = db.updateData(sql, new Object[] { days });
		successNo = total;
	}

	@Override
	public void beforeJob(JobExecutionContext context)
	{
		total = 0;
		successNo = 0;
		failedNo = 0;
	}
}
