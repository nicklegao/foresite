/**
 * @author Sushant Verma
 * @date 9 Mar 2016
 */
package au.net.webdirector.admin.user;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.net.webdirector.admin.utils.JSPPath;

@Controller
@RequestMapping("/user")
public class UserController
{
	@RequestMapping(value = "edit", method = RequestMethod.GET)
	public String editUser(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return JSPPath.path("userEditNonAdmin");
	}

	@RequestMapping(value = "changePassword", method = RequestMethod.GET)
	public String changePassword(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return JSPPath.path("changePassword");
	}
}
