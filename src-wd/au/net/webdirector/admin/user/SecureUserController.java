/**
 * @author Sushant Verma
 * @date 9 Mar 2016
 */
package au.net.webdirector.admin.user;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.net.webdirector.admin.utils.SecureJSPPath;

@Controller
@RequestMapping("/secure/user")
public class SecureUserController
{
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.path("userList");
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addUser(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.path("userEdit");
	}

	@RequestMapping(value = "edit", method = RequestMethod.GET)
	public String editUser(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.path("userEdit");
	}

	@RequestMapping(value = "categories", method = RequestMethod.POST)
	public String assignCategories(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.path("enhancedAssignCategories");
	}
}
