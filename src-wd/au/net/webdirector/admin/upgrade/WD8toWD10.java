/**
 * @author Nick Yiming Gao
 * @date 2015-5-18
 */
package au.net.webdirector.admin.upgrade;

/**
 * 
 */

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import webdirector.db.client.ClientFileUtils;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;

public class WD8toWD10
{

	private static Logger logger = Logger.getLogger(WD8toWD10.class);

	private static String lineSep = "\n";

	private DBaccess db = new DBaccess();
	private DataAccess da = DataAccess.getInstance();
	private List<String> message = new ArrayList<String>();

	public static final String[] OLD_TABLES_LIKE_CLAUSE = new String[] {
			"z_stats%",
			"webstats",
			"userlevel",
			"user_module_category_control",
			"sys_modulesummarycols",
			"status",
			"smsmarketing_%",
			"service",
			"login_config",
			"lock",
			"language",
			"emailmarketing_%",
			"dtproperties",
			"bak_drop_down_multi_selections"
	};

	public String check()
	{
		return null;
	}

	public String upgrade()
	{
		message = new ArrayList<String>();

		updateEncoding();
		upgradeSysTables();
		upgradeFunction();
		upgradeTriggers();
		upgradeHistoryAndWorkflows();
		upgradeCategoryTables();
		upgradeMisc();
		addReadonlyLabel();
		addNoChangeLabel();
		addTelstraFieldsToNotes();

		removeOldTables();
		return StringUtils.join(message, lineSep);
	}

	/**
	 * 
	 */
	private void addTelstraFieldsToNotes()
	{
		try
		{
			String sql = "ALTER TABLE `notes` "
					+ " ADD COLUMN `live` TINYINT(1) NOT NULL DEFAULT 1 AFTER `content`, "
					+ " ADD COLUMN `live_date` DATETIME NULL AFTER `live`, "
					+ " ADD COLUMN `expire_date` DATETIME NULL AFTER `live_date`; ";
			db.updateData(sql);

			sql = "ALTER TABLE `notes_ver` "
					+ " ADD COLUMN `live` TINYINT(1) NOT NULL DEFAULT 1 AFTER `content`, "
					+ " ADD COLUMN `live_date` DATETIME NULL AFTER `live`, "
					+ " ADD COLUMN `expire_date` DATETIME NULL AFTER `live_date`; ";
			db.updateData(sql);

			sql = "ALTER TABLE `notes_draft` "
					+ " ADD COLUMN `live` TINYINT(1) NOT NULL DEFAULT 1 AFTER `content`, "
					+ " ADD COLUMN `live_date` DATETIME NULL AFTER `live`, "
					+ " ADD COLUMN `expire_date` DATETIME NULL AFTER `live_date`; ";
			db.updateData(sql);
		}
		catch (Exception e)
		{
			message.add("FAILED TO UPDATE notes");
			logger.fatal("Error", e);
		}
	}

	private void upgradeMisc()
	{
		//=============================================================
		try
		{
			String sql = "ALTER TABLE `openlinks_jobs` " +
					" ADD COLUMN `CronExpression` VARCHAR(200) NULL DEFAULT NULL AFTER `Context` ";
			db.updateData(sql);
		}
		catch (Exception e)
		{
			message.add("FAILED TO UPDATE openlinks_jobs");
			logger.fatal("Error", e);
		}
	}

	/**
	 * 
	 */
	private void upgradeCategoryTables()
	{
		try
		{
			List<String> modules = getModules();

			for (int i = 0; i < modules.size(); i++)
			{
				String module = modules.get(i);
				logger.info(String.format("Upgrading category tables (%3s of %s): %s", (i + 1), modules.size(), module));

				String[] tableTypes = { "", "_ver", "_draft" };
				for (String tableType : tableTypes)
				{

					String table = "categories_" + module + tableType;

					// 1. add column Live int(10) to categories_[module],categories_[module]_ver,categories_[module]_draft after Expire_date
					Vector<String[]> col = db.selectQuery("SHOW COLUMNS FROM `" + table + "` LIKE 'live'");
					if (col.isEmpty())
					{
						String sql = "ALTER TABLE `" + table + "` ADD COLUMN `Live` INT(10) NULL DEFAULT '0' AFTER `Expire_date`";
						db.updateData(sql);
					}

					// 2. update categories_[module],categories_[module]_ver,categories_[module]_draft set live = attrcheck_live, if applicable
					col = db.selectQuery("SHOW COLUMNS FROM `" + table + "` LIKE 'attrcheck_live'");
					if (!col.isEmpty())
					{
						String sql = "UPDATE `" + table + "` SET `Live` = `ATTRCHECK_Live`";
						db.updateData(sql);
					}

				}

			}
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
		}

	}

	public void updateEncoding()
	{
		updateEncoding("utf8", "utf8_unicode_ci");
	}

	public void updateEncoding(String characterSet, String collate)
	{
		//=============================================================
		try
		{
			String sql = "ALTER SCHEMA " +
					" DEFAULT CHARACTER SET " + characterSet + "  DEFAULT COLLATE " + collate;
			db.updateData(sql);
		}
		catch (Exception e)
		{
			message.add("FAILED TO UPDATE SCHEMA encoding to DEFAULT CHARACTER SET " + characterSet + "  DEFAULT COLLATE " + collate);
			logger.fatal("Error", e);
		}
		//=============================================================
		List<String> tables = db.selectQuerySingleCol("SHOW TABLES");
		for (String table : tables)
		{
			try
			{
				String sql = "ALTER TABLE `" + table + "`" +
						" CHARACTER SET = " + characterSet + " , COLLATE = " + collate;
				db.updateData(sql);

			}
			catch (Exception e)
			{
				message.add("FAILED TO ALTER TABLE encoding of '" + table + "' to CHARACTER SET = " + characterSet + " , COLLATE = " + collate);
				logger.warn("FAILED TO ALTER TABLE encoding of '" + table + "' to CHARACTER SET = " + characterSet + " , COLLATE = " + collate);
			}
		}


		List<String[]> rows = db.selectQuery("SELECT TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME, COLUMN_TYPE "
				+ " FROM INFORMATION_SCHEMA.COLUMNS "
				+ " WHERE table_schema != 'information_schema' "
				+ " and ( (character_set_name is not null and character_set_name != '" + characterSet + "' )"
				+ "    or (collation_name is not null and collation_name != '" + collate + "' ) )");
		for (String[] meta : rows)
		{
			String schema = meta[0];
			String table = meta[1];
			String colName = meta[2];
			String colType = meta[3];

			String alterColumn = "ALTER TABLE `" + schema + "`.`" + table + "`" +
					" CHANGE COLUMN `" + colName + "` `" + colName + "` " + colType + " CHARACTER SET " + characterSet + " COLLATE " + collate;

			try
			{
				db.updateData(alterColumn);
			}
			catch (Exception e)
			{
				message.add("FAILED TO ALTER TABLE encoding of column. SQL: " + alterColumn);
				logger.warn("FAILED TO ALTER TABLE encoding of column. SQL: " + alterColumn);
			}
		}


	}

	private void upgradeSysTables()
	{
		//=============================================================
		try
		{
			String sql = "ALTER TABLE `sys_moduleconfig`" +
					" ADD COLUMN `color` VARCHAR(255) NULL DEFAULT 'bg-color-darken txt-color-white' AFTER `cate_valid_script`," +
					" ADD COLUMN `icon` VARCHAR(255) NULL DEFAULT 'fa fa-certificate' AFTER `color`," +
					" ADD COLUMN `version_control_enabled` int(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `icon`," +
					" ADD COLUMN `workflow_enabled` int(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `version_control_enabled`," +
					" ADD COLUMN `show_live_flag` int(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `workflow_enabled`," +
					" ADD COLUMN `has_sort` INT(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `show_live_flag`," +
					" ADD COLUMN `has_copy` INT(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `has_sort`," +
					" ADD COLUMN `has_cut` INT(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `has_copy`," +
					" ADD COLUMN `has_import` INT(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `has_cut`," +
					" ADD COLUMN `has_export` INT(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `has_import`," +
					" ADD COLUMN `has_full_ckeditor` INT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `has_export`," +
					" ADD COLUMN `has_form_components` INT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `has_full_ckeditor`," +
					" ADD COLUMN `has_browse_server` INT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `has_form_components`," +
					" ADD COLUMN `customised_buttons` INT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `has_browse_server`";
			db.updateData(sql);
		}
		catch (Exception e)
		{
			message.add("ALTER TABLE `sys_moduleconfig` failed");
			logger.error("Error", e);
		}
		//=============================================================
		try
		{
			//This ALSO updates the character set and collate
			String sql = "ALTER TABLE `openlinks_history` "
					+ " CHANGE COLUMN `JobName` `JobName` VARCHAR(500) NULL COMMENT '' ,"
					+ " CHANGE COLUMN `FeedSource` `FeedSource` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '' ,"
					+ " CHANGE COLUMN `RootPath` `RootPath` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '' ,"
					+ " CHANGE COLUMN `StartDate` `StartDate` DATETIME NULL DEFAULT NULL COMMENT '' ,"
					+ " CHANGE COLUMN `EndDate` `EndDate` DATETIME NULL DEFAULT NULL COMMENT '' ,"
					+ " CHANGE COLUMN `NumberOfNodes` `NumberOfNodes` INT(10) NULL DEFAULT NULL COMMENT '' ,"
					+ " CHANGE COLUMN `Number_Succeed` `Number_Succeed` INT(10) NULL DEFAULT NULL COMMENT '' ,"
					+ " CHANGE COLUMN `Number_Failed` `Number_Failed` INT(10) NULL DEFAULT NULL COMMENT '' ,"
					+ " CHANGE COLUMN `Number_Inserted` `Number_Inserted` INT(10) NULL DEFAULT NULL COMMENT '' ,"
					+ " CHANGE COLUMN `Number_Updated` `Number_Updated` INT(10) NULL DEFAULT NULL COMMENT '' ,"
					+ " CHANGE COLUMN `logFile` `logFile` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '' ";
			db.updateData(sql);
		}
		catch (Exception e)
		{
			message.add("ALTER TABLE `openlinks_history` failed");
			logger.error("Error", e);
		}
		//=============================================================
		try
		{
			String sql = "ALTER TABLE `users`" +
					" ADD COLUMN `incorrectLoginAttempts` INT(10) NULL DEFAULT 0";
			db.updateData(sql);
		}
		catch (Exception e)
		{
			message.add("ALTER TABLE `users` failed");
			logger.error("Error", e);
		}
		//=============================================================
		try
		{
			String sql = "ALTER TABLE `users`" +
					" ADD COLUMN `lastIncorrectLogin` TIMESTAMP NULL DEFAULT NULL";
			db.updateData(sql);
		}
		catch (Exception e)
		{
			message.add("ALTER TABLE `users` failed");
			logger.error("Error", e);
		}
		//=============================================================
		try
		{
			String sql = "ALTER TABLE `users`" +
					" ADD COLUMN `default_layout` CHAR(50) NULL DEFAULT NULL";
			db.updateData(sql);
		}
		catch (Exception e)
		{
			message.add("ALTER TABLE `users` failed");
			logger.error("Error", e);
		}
		//=============================================================
		try
		{
			String sql = "ALTER TABLE `users`" +
					" ADD COLUMN `resetPasswordToken` VARCHAR(255) NULL DEFAULT NULL";
			db.updateData(sql);
		}
		catch (Exception e)
		{
			message.add("ALTER TABLE `users` failed");
			logger.error("Error", e);
		}
		//=============================================================
		try
		{
			String sql = "ALTER TABLE `users`" +
					" ADD COLUMN `access_file_manager` VARCHAR(20) DEFAULT 'no'";
			db.updateData(sql);
		}
		catch (Exception e)
		{
			message.add("ALTER TABLE `users` failed");
			logger.error("Error", e);
		}
		//=============================================================
		try
		{
			String sql = "ALTER TABLE `users`" +
					" ADD COLUMN `can_change_password` VARCHAR(20) DEFAULT 'no'";
			db.updateData(sql);
		}
		catch (Exception e)
		{
			message.add("ALTER TABLE `users` failed");
			logger.error("Error", e);
		}
		//=============================================================
		try
		{
			String sql = "ALTER TABLE `users`" +
					" ADD COLUMN `can_edit_account` VARCHAR(20) DEFAULT 'no'";
			db.updateData(sql);
		}
		catch (Exception e)
		{
			message.add("ALTER TABLE `users` failed");
			logger.error("Error", e);
		}
		//=============================================================
		try
		{
			db.selectQuery("select * from sys_module_approvers where 1 != 1");
		}
		catch (Exception e)
		{
			String sql = "CREATE TABLE  `sys_module_approvers` (" +
					"   `id` int(11) NOT NULL AUTO_INCREMENT," +
					"   `internal_module_name` varchar(45) NOT NULL," +
					"   `user_id` int(10) NOT NULL," +
					"   PRIMARY KEY (`id`)" +
					" )";
			db.updateData(sql);
		}
		//=============================================================
		try
		{
			db.selectQuery("select content from notes where 1 != 1");
		}
		catch (Exception e)
		{
			String sql = "ALTER TABLE `notes`" +
					" CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci";
			db.updateData(sql);
		}
		//=============================================================
		try
		{
			db.selectQuery("select content from notes where 1 != 1");
		}
		catch (Exception e)
		{
			String sql = "ALTER TABLE `notes`" +
					" ADD COLUMN `content` text";
			db.updateData(sql);
			ClientFileUtils cfu = new ClientFileUtils();
			sql = "select * from notes";
			List<Hashtable<String, String>> notes = da.select(sql, new HashtableMapper());
			for (int i = 0; i < notes.size(); i++)
			{
				Hashtable<String, String> note = notes.get(i);

				String id = note.get("id");
				String filePath = note.get("note");

				logger.info(String.format("Importing note %s of %s id=%s", (i + 1), notes.size(), id));
				String content = cfu.readTextContents(cfu.getFilePathFromStoreName(filePath), true).toString();
				da.updateData("update notes set content = ? where id = ?", new String[] { content, id });
			}
		}
		//=============================================================
		try
		{
			db.selectQuery("select * from sys_version_rec where 1 != 1");
		}
		catch (Exception e)
		{
			String sql = "CREATE TABLE  `sys_version_rec` (" +
					"   `Version_ID` int(11) NOT NULL AUTO_INCREMENT," +
					"   `V_WHEN` datetime," +
					"   `V_WHO` varchar(45) NOT NULL," +
					"   `V_ACTION` varchar(45) NOT NULL," +
					"   `V_MODULE` varchar(45) NOT NULL," +
					"   `V_TABLE` varchar(45) NOT NULL," +
					"   `V_DATA_ID` int(11) NOT NULL," +
					"   `V_VERSION_NO` int(11)," +
					"   PRIMARY KEY (`Version_ID`)" +
					" )";
			db.updateData(sql);
			sql = "CREATE TRIGGER ins_sys_version_rec BEFORE INSERT ON sys_version_rec" +
					" FOR EACH ROW BEGIN" +
					"     SET NEW.V_WHEN = NOW();" +
					"     SET NEW.V_VERSION_NO = (SELECT ifnull(MAX(V_VERSION_NO),0) + 1 FROM sys_version_rec WHERE V_MODULE = NEW.V_MODULE and V_TABLE = NEW.V_TABLE and V_DATA_ID = NEW.V_DATA_ID);" +
					" END";
			db.updateData(sql);
		}
		//=============================================================
		try
		{
			db.selectQuery("select * from notes_ver where 1 != 1");
		}
		catch (Exception e)
		{
			db.updateData("create table `notes_ver` like `notes`");
			String sql = "ALTER TABLE `notes_ver` MODIFY COLUMN `id` INT(10) NOT NULL," +
					"  ADD COLUMN `Version_ID` INTEGER(11)," +
					"  DROP PRIMARY KEY";
			db.updateData(sql);
		}
		//=============================================================
		try
		{
			db.selectQuery("select * from drop_down_multi_selections_ver where 1 != 1");
		}
		catch (Exception e)
		{
			db.updateData("create table `drop_down_multi_selections_ver` like `drop_down_multi_selections`");
			String sql = "ALTER TABLE `drop_down_multi_selections_ver`" +
					"  ADD COLUMN `Version_ID` INTEGER(11)";
			db.updateData(sql);
		}
		//=============================================================
		try
		{
			db.selectQuery("select * from sys_draft_rec where 1 != 1");
		}
		catch (Exception e)
		{
			String sql = "CREATE TABLE  `sys_draft_rec` (" +
					"  `D_ID` int(11) NOT NULL AUTO_INCREMENT," +
					"  `D_WHEN` datetime NOT NULL," +
					"  `D_WHO` varchar(45) NOT NULL," +
					"  `D_MODULE` varchar(45) NOT NULL," +
					"  `D_TABLE` varchar(45) NOT NULL," +
					"  `D_DRAFT_ID` int(11) NOT NULL," +
					"  `D_DATA_ID` int(11) NOT NULL," +
					"  `D_STATUS` varchar(45) NOT NULL," +
					"  PRIMARY KEY (`D_ID`)" +
					") ";
			db.updateData(sql);
		}
		//=============================================================
		try
		{
			db.selectQuery("select * from notes_draft where 1 != 1");
		}
		catch (Exception e)
		{
			db.updateData("create table `notes_draft` like `notes`");
		}
		//=============================================================
		try
		{
			db.selectQuery("select * from drop_down_multi_selections_draft where 1 != 1");
		}
		catch (Exception e)
		{
			db.updateData("create table `drop_down_multi_selections_draft` like `drop_down_multi_selections`");
		}
	}

	private void upgradeHistoryAndWorkflows()
	{
		List<String> modules = getModules();

		for (int i = 0; i < modules.size(); i++)
		{
			String module = modules.get(i);
			logger.info(String.format("Upgrading history and workflow (%3s of %s): %s", (i + 1), modules.size(), module));

			createHistoryTable(module);
			createWorkflowTable(module);
		}

	}

	private void createWorkflowTable(String module)
	{
		try
		{
			db.selectQuery("select * from elements_" + module + "_draft where 1!=1");
		}
		catch (Exception e)
		{
			db.updateData("create table elements_" + module + "_draft like elements_" + module);
		}
		try
		{
			db.selectQuery("select * from categories_" + module + "_draft where 1!=1");
		}
		catch (Exception e)
		{
			db.updateData("create table categories_" + module + "_draft like categories_" + module);
		}
	}

	private void createHistoryTable(String module)
	{
		try
		{
			db.selectQuery("select * from elements_" + module + "_ver where 1!=1");
		}
		catch (Exception e)
		{
			try
			{
				db.updateData("create table elements_" + module + "_ver like elements_" + module);
				db.updateData("ALTER TABLE elements_" + module + "_ver MODIFY COLUMN Element_id INT(10) NOT NULL, ADD COLUMN Version_ID INTEGER(11) First");
				db.updateData("ALTER TABLE elements_" + module + "_ver DROP PRIMARY KEY");
			}
			catch (Exception ee)
			{
				message.add("ALTER TABLE `elements_" + module + "_ver` failed");
				logger.error("ALTER TABLE `elements_" + module + "_ver` failed", ee);
			}
		}
		try
		{
			db.selectQuery("select * from categories_" + module + "_ver where 1!=1");
		}
		catch (Exception e)
		{
			try
			{
				db.updateData("create table categories_" + module + "_ver like categories_" + module);
				db.updateData("ALTER TABLE categories_" + module + "_ver MODIFY COLUMN Category_id INT(10) NOT NULL, ADD COLUMN Version_ID INTEGER(11) First");
				db.updateData("ALTER TABLE categories_" + module + "_ver DROP PRIMARY KEY");
			}
			catch (Exception ee)
			{
				message.add("ALTER TABLE `categories_" + module + "_ver` failed");
				logger.error("ALTER TABLE `categories_" + module + "_ver` failed", ee);
			}
		}
	}

	private void upgradeTriggers()
	{
		List<String> modules = getModules();

		for (int i = 0; i < modules.size(); i++)
		{
			String module = modules.get(i);
			logger.info(String.format("Upgrading module triggers (%3s of %s): %s", (i + 1), modules.size(), module));

			deleteTrigger(module);
			addFields(module);
			createTrigger(module);
		}
	}

	/**
	 * @param string
	 */
	private void createTrigger(String module)
	{
		String sqlie = "CREATE TRIGGER insert_e_" + module + " BEFORE INSERT ON elements_" + module + " FOR EACH ROW begin set new.Create_date=now(); set new.FRIENDLY_NAME=friendly(new.ATTR_Headline); end";
		db.updateData(sqlie);
		String sqlue = "CREATE TRIGGER update_e_" + module + " BEFORE UPDATE ON elements_" + module + " FOR EACH ROW begin set new.last_update_date=now(); set new.FRIENDLY_NAME=friendly(new.ATTR_Headline); end";
		db.updateData(sqlue);
		String sqlic = "CREATE TRIGGER insert_c_" + module + " BEFORE INSERT ON categories_" + module + " FOR EACH ROW begin set new.Create_date=now(); set new.FRIENDLY_NAME=friendly(new.ATTR_categoryName); end";
		db.updateData(sqlic);
		String sqluc = "CREATE TRIGGER update_c_" + module + " BEFORE UPDATE ON categories_" + module + " FOR EACH ROW begin set new.last_update_date=now(); set new.FRIENDLY_NAME=friendly(new.ATTR_categoryName); end";
		db.updateData(sqluc);
	}

	/**
	 * @param module
	 */
	private void addFields(String module)
	{
		try
		{
			db.selectQuerySingleCol("select LAST_UPDATE_DATE from elements_" + module + " where 1!=1");
		}
		catch (Exception e)
		{
			db.updateData("ALTER TABLE elements_" + module + " ADD COLUMN LAST_UPDATE_DATE DATETIME AFTER CREATE_DATE");
			db.updateData("update elements_" + module + " set create_date = now() where create_date is null");
			db.updateData("update elements_" + module + " set last_update_date = now() where last_update_date is null");
			message.add("Created LAST_UPDATE_DATE in elements_" + module);
		}
		try
		{
			db.selectQuerySingleCol("select LAST_UPDATE_DATE from categories_" + module + " where 1!=1");
		}
		catch (Exception e)
		{
			db.updateData("ALTER TABLE categories_" + module + " ADD COLUMN LAST_UPDATE_DATE DATETIME AFTER CREATE_DATE");
			db.updateData("update categories_" + module + " set create_date = now() where create_date is null");
			db.updateData("update categories_" + module + " set last_update_date = now() where last_update_date is null");
			message.add("Created LAST_UPDATE_DATE in categories_" + module);
		}
		try
		{
			db.selectQuerySingleCol("select FRIENDLY_NAME from elements_" + module + " where 1!=1");
		}
		catch (Exception e)
		{
			db.updateData("ALTER TABLE elements_" + module + " ADD COLUMN FRIENDLY_NAME VARCHAR(500) AFTER LAST_UPDATE_DATE");
			db.updateData("update elements_" + module + " set friendly_name = friendly(attr_headline)");
			message.add("Created FRIENDLY_NAME in elements_" + module);
		}
		try
		{
			db.selectQuerySingleCol("select FRIENDLY_NAME from categories_" + module + " where 1!=1");
		}
		catch (Exception e)
		{
			db.updateData("ALTER TABLE categories_" + module + " ADD COLUMN FRIENDLY_NAME VARCHAR(500) AFTER LAST_UPDATE_DATE");
			db.updateData("update categories_" + module + " set friendly_name = friendly(attr_categoryName)");
			message.add("Created FRIENDLY_NAME in categories_" + module);
		}

	}

	/**
	 * @param module
	 * @return
	 */
	private void deleteTrigger(String module)
	{
		String sqlie = "drop trigger if exists insert_e_" + module;
		db.updateData(sqlie);
		String sqlic = "drop trigger if exists insert_c_" + module;
		db.updateData(sqlic);
		String sqlue = "drop trigger if exists update_e_" + module;
		db.updateData(sqlue);
		String sqluc = "drop trigger if exists update_c_" + module;
		db.updateData(sqluc);
	}

	/**
	 * @return
	 */
	private List<String> getModules()
	{
		List<String> tables = db.selectQuerySingleCol("SHOW TABLES");
		List<String> modules = new ArrayList<String>();
		for (String table : tables)
		{
			if (table.toLowerCase().startsWith("categories_") && !table.toLowerCase().endsWith("_ver") && !table.toLowerCase().endsWith("_draft"))
			{
				int startIndex = "categories_".length();
				String moduleToAdd = table.substring(startIndex);
				modules.add(moduleToAdd.toLowerCase());
			}
		}
		return modules;
	}

	private void upgradeFunction()
	{
		db.updateData("DROP FUNCTION IF EXISTS friendly;");

		String script = "";
		script += " CREATE FUNCTION friendly(name TEXT)";
		script += "   RETURNS TEXT";
		script += " BEGIN";
		script += "   DECLARE v_temp TEXT;";
		script += "   DECLARE v_idx INT;";
		script += "   DECLARE v_char CHAR;";
		script += "   DECLARE v_last CHAR;";
		script += "   DECLARE v_ret TEXT;";
		script += "   IF name is null THEN";
		script += "     RETURN name;";
		script += "   END IF;";
		script += "   SET v_temp = LCASE(name);";
		script += " ";
		script += "   SET v_temp = REPLACE(v_temp, '&amp;', '-');";
		script += "   SET v_temp = REPLACE(v_temp, '&apos;', '-');";
		script += "   SET v_temp = REPLACE(v_temp, '&gt;', '-');";
		script += "   SET v_temp = REPLACE(v_temp, '&lt;', '-');";
		script += "   SET v_temp = REPLACE(v_temp, '&quot;', '-');";
		script += " ";
		script += "   SET v_last = '';";
		script += "   SET v_idx = 0;";
		script += "   SET v_ret = '';";
		script += "   cloop: LOOP";
		script += "     SET v_idx = v_idx + 1;";
		script += " ";
		script += "     IF v_idx = length(v_temp)+1 THEN";
		script += "       LEAVE cloop;";
		script += "     END IF;";
		script += " ";
		script += "     SET v_char = MID(v_temp, v_idx, 1);";
		script += "     IF FIELD(binary v_char, 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','-','1','2','3','4','5','6','7','8','9','0') = 0 THEN";
		script += "       SET v_char = '-';";
		script += "     END IF;";
		script += " ";
		script += "     IF (v_last != '-' OR v_char != '-') AND (v_char != '-' OR v_idx != length(v_temp)) THEN";
		script += "       SET v_ret = CONCAT(v_ret, v_char);";
		script += "       SET v_last = v_char;";
		script += "     END IF;";
		script += " ";
		script += " ";
		script += " ";
		script += "   END LOOP;";
		script += "   IF substr(v_ret, length(v_ret)) = '-' THEN";
		script += " 	SET v_ret = substr(v_ret,1, length(v_ret) - 1);";
		script += "   END IF;";
		script += "   RETURN v_ret;";
		script += " END";
		db.updateData(script);
	}

	private void addReadonlyLabel()
	{
		Vector<String> tables = db.selectQuerySingleCol("SELECT concat('labels_', internal_module_name) FROM sys_moduleconfig");
		Vector<String> catTables = db.selectQuerySingleCol("SELECT concat('categorylabels_', internal_module_name) FROM sys_moduleconfig");
		tables.addAll(catTables);
		tables.add("labels_template");
		tables.add("categorylabels_template");


		for (String table : tables)
		{
			try
			{
				String sql = "ALTER TABLE " + DatabaseValidation.encodeParam(table) +
						" ADD COLUMN `Label_ReadOnly` INT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `Label_tokenized`";
				db.updateData(sql);
			}
			catch (Exception e)
			{
				message.add("addReadonlyLabel() failed on table " + table);
				logger.error("Error", e);
			}
		}
	}

	private void addNoChangeLabel()
	{
		Vector<String> tables = db.selectQuerySingleCol("SELECT concat('labels_', internal_module_name) FROM sys_moduleconfig");
		Vector<String> catTables = db.selectQuerySingleCol("SELECT concat('categorylabels_', internal_module_name) FROM sys_moduleconfig");
		tables.addAll(catTables);
		tables.add("labels_template");
		tables.add("categorylabels_template");


		for (String table : tables)
		{
			try
			{
				String sql = "ALTER TABLE " + DatabaseValidation.encodeParam(table) +
						" ADD COLUMN `Label_NoChange` INT(1) UNSIGNED NOT NULL DEFAULT '0'";
				db.updateData(sql);
			}
			catch (Exception e)
			{
				message.add("addNoChangeLabel() failed on table " + table);
				logger.error("Error", e);
			}
		}
	}

	private void removeOldTables()
	{
		for (String tableMatch : OLD_TABLES_LIKE_CLAUSE)
		{
			//Find the old tables (if they still exist)
			List<String> tables = db.selectQuerySingleCol("SHOW TABLES LIKE '" + tableMatch + "'");
			for (String table : tables)
			{
				try
				{
					String sql = "DROP TABLE `" + table + "`";
					db.updateData(sql);
					message.add("Successfully DROPped TABLE '" + table + "'");
				}
				catch (Exception e)
				{
					message.add("remove table:(" + table + ") failed");
					logger.warn("Failed to DROP TABLE '" + table + "'");
				}
			}
		}

		try
		{
			String sql = "ALTER TABLE " + DatabaseValidation.encodeParam("users") +
					" DROP COLUMN `flexModules`, " +
					" DROP COLUMN `Modules` ";
			db.updateData(sql);
		}
		catch (Exception e)
		{
			message.add("failed to remove old columns from users table");
			logger.error("Error", e);
		}
	}
}
