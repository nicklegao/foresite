package au.net.webdirector.admin.bulkUpload;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang.StringUtils;

import au.net.webdirector.common.datalayer.admin.db.DataAccess;

/**
 * TODO: SUSHANT/NICK: Need to implement this into WD10
 * 
 * This code is not tested in WD10
 */
@Deprecated
public class BulkJQueryUploadServlet extends HttpServlet
{
	private String storeDir = null;
	private au.net.webdirector.common.Defaults d = au.net.webdirector.common.Defaults.getInstance();
	private static final long serialVersionUID = 1L;
	private Map<String, String> moduleFieldNames = new HashMap<String, String>();

	public void init()
	{
		// called when servlet initialised
		d.setProperties(getServletContext());
		storeDir = d.getStoreDir();
		// ensure tmp exists
		File tmp = new File(storeDir + "/_tmp");
		if (!tmp.exists())
			tmp.mkdirs();

		String sModules = getServletConfig().getInitParameter("available-modules");
		String sFieldNames = getServletConfig().getInitParameter("file-field-names");

		String[] modules = StringUtils.split(sModules, ",");
		String[] fields = StringUtils.split(sFieldNames, ",");

		for (int i = 0; i < modules.length; i++)
		{
			moduleFieldNames.put(modules[i], fields[i]);
		}

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 * 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		PrintWriter writer = response.getWriter();
		writer.write("call POST with multipart form data");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 * 
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String contextPath = request.getContextPath();
		if (!ServletFileUpload.isMultipartContent(request))
		{
			throw new IllegalArgumentException("Request is not multipart, please 'multipart/form-data' enctype for your form.");
		}

		ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
		PrintWriter writer = response.getWriter();
		response.setContentType("text/plain");
		StringBuffer sb = new StringBuffer();
		try
		{
			List<FileItem> items = uploadHandler.parseRequest(request);
			String id = "0";
			String module = "";
			sb.append("[");
			for (FileItem item : items)
			{
				if (item.isFormField())
				{
					if (item.getFieldName().equals("id"))
					{
						id = item.getString();
					}
					if (item.getFieldName().equals("module"))
					{
						module = item.getString();
					}
				}
				else
				{
					try
					{
						UploadResult result = createDocument(item, id, module, contextPath);
						sb.append("{\"url\":\"" + result.getUrl() + "\",\"thumbnail_url\":\"" + result.getThumbnailUrl() + "\",\"name\":\"" + result.getFileName() + "\",\"type\":\""
								+ result.getType() + "\",\"size\":" + result.getSize() + ",\"delete_url\":\"" + result.getDeleteUrl() + "\",\"delete_type\":\"" + result.getDeleteType() + "\"}");
					}
					catch (Exception e)
					{
						String e_message = e.getMessage();
						String showMsg = e_message;
						if (e instanceof org.springframework.dao.DuplicateKeyException && e_message.indexOf("Duplicate entry '") != -1)
						{
							int start = showMsg.indexOf("Duplicate entry '");
							if (start != -1 && showMsg.indexOf("'", start + "Duplicate entry ".length() + 1) != -1)
							{
								int end = showMsg.indexOf("'", start + "Duplicate entry ".length() + 1);
								showMsg = showMsg.substring(start + "Duplicate entry ".length(), end);
								showMsg = "Duplicate value: " + showMsg + "' found!";
							}

						}
						sb.append("{\"name\":\"" + item.getName() + "\",\"type\":\""
								+ item.getContentType() + "\",\"size\":" + item.getSize() + ",\"error\":\"" + showMsg + "\"}");
					}
				}
			}
			sb.append("]");
			writer.write(sb.toString());
			System.out.println("===================================");
		}
		catch (FileUploadException e)
		{

			throw new RuntimeException(e);
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
		finally
		{
			writer.close();
		}

	}

	private UploadResult createDocument(FileItem item, String categoryID, String module, String contextPath) throws Exception
	{

		String fileName = item.getName();
		String headline = fileName.indexOf(".") == -1 ? fileName : fileName.substring(0, fileName.lastIndexOf("."));
		String sql = "insert into elements_" + module + " (Language_id, Status_id, Lock_id, Version, display_order, Live, Category_id, ATTR_Headline) values (1, 1, 1, 1, 0, 0, ?, ?)";
		int elementID = DataAccess.getInstance().insertDataQuery(sql, new Object[] { categoryID, headline });
		if (elementID <= 0)
		{
			throw new RuntimeException("Data insert error!");
		}
		String relatedFilePath = "/" + module + "/" + elementID + "/ATTRFILE_File1/";
		String relatedThumbPath = relatedFilePath + "_thumb/";
		String relatedFileName = relatedFilePath + fileName;
		File dir = new File(storeDir + relatedFilePath);
		dir.mkdirs();
		File file = new File(dir, fileName);
		item.write(file);
		boolean thumbGenerated = false;
		if (item.getContentType().toLowerCase().startsWith("image/"))
		{
			thumbGenerated = d.generateThumb(file);
		}
		String sqlUpd = "update elements_" + module + " set " + moduleFieldNames.get(module) + " = ? where element_id = ?";
		DataAccess.getInstance().updateData(sqlUpd, new Object[] { relatedFileName, elementID });
		UploadResult ret = new UploadResult();
		ret.setFileName(fileName);
		ret.setUrl("/" + d.getStoreContext() + relatedFileName);
		if (thumbGenerated)
		{
			ret.setThumbnailUrl("/" + d.getStoreContext() + relatedThumbPath + fileName);
		}
		else
		{
			ret.setThumbnailUrl(getFileIconPath(fileName, contextPath));
		}
		ret.setType(item.getContentType());
		ret.setSize(item.getSize());

		return ret;
	}

	private String getFileIconPath(String fileName, String contextPath)
	{
		String url = contextPath + "/clientImages/filetypes/";
		String[] types = new String[] { "accdb", "avi", "bmp", "css", "doc", "docx", "eml", "eps", "fla", "gif", "html", "ind", "ini", "jpeg", "jpg", "jsf", "midi", "mov", "mp3", "mpeg", "pdf",
				"png", "pptx", "proj", "psd", "pst", "pub", "rar", "readme", "settings", "text", "tiff", "url", "vsd", "wav", "wma", "wmv", "xlsx", "zip" };
		if (fileName.indexOf(".") == -1)
		{
			return url + "none.png";
		}
		String ext = fileName.substring(fileName.lastIndexOf(".") + 1);
		for (String type : types)
		{
			if (type.equalsIgnoreCase(ext))
			{
				return url + type + ".png";
			}
		}
		return url + "none.png";
	}
}

class UploadResult
{
	private String fileName;
	private String url;
	private String thumbnailUrl;
	private String type;
	private long size;
	private String deleteUrl = "not used any more";
	private String deleteType = "DELETE";

	public String getFileName()
	{
		return fileName;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	public String getDeleteUrl()
	{
		return deleteUrl;
	}

	public String getDeleteType()
	{
		return deleteType;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getThumbnailUrl()
	{
		return thumbnailUrl;
	}

	public void setThumbnailUrl(String thumbnailUrl)
	{
		this.thumbnailUrl = thumbnailUrl;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public long getSize()
	{
		return size;
	}

	public void setSize(long size)
	{
		this.size = size;
	}

}
