/**
 * 2:48:52 PM 14/02/2012
 * @author Vito Lefemine
 */
package au.net.webdirector.admin.quartz;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.quartz.JobDetail;

import com.google.gson.GsonBuilder;

/**
 * @author Vito Lefemine
 *
 */
public class JobScheduleObj
{

	JobDetail jobDetail;
	Date startDate;
	Date endDate;
	int repeatCount;
	long repeatInterval;
	String className;
	String context;
	String cronExpression;

	/**
	 * 
	 */
	public JobScheduleObj()
	{
		super();
	}

	/**
	 * @param jobDetail
	 * @param startDate
	 * @param endDate
	 * @param repeatCount
	 * @param repeatInterval
	 */
//	public JobScheduleObj(JobDetail jobDetail, Date startDate, Date endDate,
//			int repeatCount, long repeatInterval,String className, String ID) {
//		super();
//		this.jobDetail = jobDetail;
//		this.startDate = startDate;
//		this.endDate = endDate;
//		this.repeatCount = repeatCount;
//		this.repeatInterval = repeatInterval;
//		this.className = className;
//		this.ID = ID;
//		
//	}
	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	public JobDetail getJobDetail()
	{
		return jobDetail;
	}

	public void setJobDetail(JobDetail jobDetail)
	{
		this.jobDetail = jobDetail;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public int getRepeatCount()
	{
		return repeatCount;
	}

	public void setRepeatCount(int repeatCount)
	{
		this.repeatCount = repeatCount;
	}

	public long getRepeatInterval()
	{
		return repeatInterval;
	}

	public void setRepeatInterval(long repeatInterval)
	{
		this.repeatInterval = repeatInterval;
	}

	public String getClassName()
	{
		return className;
	}

	public void setClassName(String className)
	{
		this.className = className;
	}

	public String getContext()
	{
		return context;
	}

	public void setContext(String context)
	{
		this.context = context;
	}

	public Map<String, String> getContextMap()
	{
		return (Map<String, String>) new GsonBuilder().create().fromJson(context, HashMap.class);
	}

	/**
	 * @return the cronExpression
	 */
	public String getCronExpression()
	{
		return cronExpression;
	}

	/**
	 * @param cronExpression
	 *            the cronExpression to set
	 */
	public void setCronExpression(String cronExpression)
	{
		this.cronExpression = cronExpression;
	}

	public boolean isCronJob()
	{
		return StringUtils.isNotBlank(this.cronExpression) && repeatCount == -2;
	}




}
