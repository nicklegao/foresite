/**
 * 2:44:52 PM 17/02/2012
 * @author Vito Lefemine
 */
package au.net.webdirector.admin.quartz;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * @author Vito Lefemine
 *
 */
public class QuartzUtils
{

	private static Logger logger = Logger.getLogger(QuartzUtils.class);

	public static Class[] getClasses(String packageName) throws ClassNotFoundException, IOException
	{
		logger.debug("Searching for classes in " + packageName);
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		assert classLoader != null;
		String path = packageName.replace('.', '/');
		Enumeration<URL> resources = classLoader.getResources(path);
		logger.debug("path " + path);

		List<File> dirs = new ArrayList<File>();
		logger.debug("dirs size " + dirs.size());
		while (resources.hasMoreElements())
		{
			URL resource = resources.nextElement();
			logger.debug("resource.getFile() " + resource.getFile());
			dirs.add(new File(resource.getFile().replaceAll("%20", " ")));
		}
		ArrayList<Class> classes = new ArrayList<Class>();
		for (File directory : dirs)
		{
			classes.addAll(findClasses(directory, packageName));
		}
		return classes.toArray(new Class[classes.size()]);
	}


	public static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException
	{
		List<Class> classes = new ArrayList<Class>();
		if (!directory.exists())
		{
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files)
		{
			if (file.isDirectory())
			{
				assert !file.getName().contains(".");
				logger.debug("Adding all " + file.getAbsolutePath() + packageName + "." + file.getName());
				classes.addAll(findClasses(file, packageName + "." + file.getName()));

			}
			else if (file.getName().endsWith(".class"))
			{

				logger.debug("ADDING CLASS  " + (packageName + '.' + file.getName().substring(0, file.getName().length() - ".class".length())));
				Class c = null;
				try
				{
					c = Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - ".class".length()));
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				if (null != c)
				{
					logger.debug("Adding class c" + c.getCanonicalName());
					classes.add(c);
				}
				else
					logger.debug("NULL class c" + c.getCanonicalName());
			}
		}
		return classes;
	}


}
