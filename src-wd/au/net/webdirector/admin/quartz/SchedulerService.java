package au.net.webdirector.admin.quartz;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Vector;

import org.apache.commons.lang.Validate;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.utils.SimpleDateFormat;
import sbe.openlinks.quartz.page.quartz.JobAndSimpleTrigger;
import sbe.openlinks.quartz.page.quartz.JobHistory;

/**
 * Provides a service wrapper class around the Quartz scheduler class.
 */
public class SchedulerService
{

	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

	/** The scheduler group name. */
	public static final String GROUP_NAME = "jobs";

	/** The scheduler trigger group name. */
	public static final String TRIGGER_GROUP_NAME = "Trigger Group";

	/** The quartz scheduler instance. */
	private final Scheduler scheduler;

	private static Logger logger = Logger.getLogger(SchedulerService.class);
	DBaccess dba = new DBaccess();

	// Constructor ------------------------------------------------------------

	/**
	 * Create a new scheduler service with the given Quartz Scheduler Factory.
	 * 
	 * @param schedulerFactory
	 *            the Quartz Scheduler Factory
	 */
	public SchedulerService(StdSchedulerFactory schedulerFactory)
	{
		Validate.notNull(schedulerFactory, "Null schedulerFactory param");

		try
		{
			scheduler = schedulerFactory.getScheduler();

		}
		catch (SchedulerException se)
		{
			String msg = "Could not obtain Quartz Scheduler instance";
			throw new RuntimeException(msg);
		}
	}


	private Date getNextRunDate(JobScheduleObj job)
	{
		Date startDate = job.getStartDate();
		int loop = 0;
		while (startDate.before(new Date()) && (job.getRepeatCount() == -1 || ++loop <= job.getRepeatCount()))
		{
			startDate = DateUtils.addSeconds(startDate, (int) (job.getRepeatInterval() / 1000));
		}
		return startDate;
	}

	// Public Methods ---------------------------------------------------------

	/**
	 * Schedule a new quartz job with the given job details, start and end date,
	 * repeat count and repeat interval.
	 * 
	 * @param jobDetail
	 *            the quartz job detail
	 * @param startDate
	 *            the start date of the job
	 * @param endDate
	 *            the end date of the job
	 * @param repeatCount
	 *            the repeat count
	 * @param repeatInterval
	 *            the job repeat interval in milliseconds
	 */
	public boolean scheduleJob(JobScheduleObj job) throws Exception
	{
		JobDetail jobDetail = job.getJobDetail();
		Date startDate = getNextRunDate(job);
		Validate.notNull(jobDetail, "Null jobDetail parameter");
		try
		{
			Trigger trigger;
			if (job.isCronJob())
				trigger = new CronTrigger(jobDetail.getName(), TRIGGER_GROUP_NAME, job.getCronExpression());
			else
				trigger = new SimpleTrigger(jobDetail.getName(), TRIGGER_GROUP_NAME, jobDetail.getName(), jobDetail.getGroup(), startDate, job.getEndDate(), job.getRepeatCount(), job.getRepeatInterval());


			if (job.getContextMap() != null)
			{
				for (Entry entry : job.getContextMap().entrySet())
				{
					trigger.getJobDataMap().put(entry.getKey(), entry.getValue());
				}
			}
			Date nextRun = getScheduler().scheduleJob(jobDetail, trigger);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			logger.info(" Job[" + jobDetail.getName() + "] Scheduled next run will be @ " + sdf.format(nextRun));

			return true;
		}
		catch (SchedulerException se)
		{
			throw se;

		}
	}

	public boolean deleteJobHistoryRecord(String elid)
	{
		String sql = "select logFile FROM openlinks_history WHERE Element_id =?";
		logger.info("sql = " + sql);
		String[] cols = { "logFile" };
		//Vector<String[]> res = dba.select(sql, cols);
		Vector<String[]> res = dba.selectQuery(sql, new String[] { elid });
		if (null != res && res.size() > 0)
		{
			String fileName = res.get(0)[0];
			File f = new File(Defaults.getInstance().getOStypeTempDir().replace("\\", "/") + "/" + fileName);
			f.delete();
		}

		String sql_deleteJobH = "DELETE FROM openlinks_history WHERE Element_id =?";
		int rowsAffected = dba.updateData(sql_deleteJobH, new Object[] { elid });
		if (rowsAffected > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Return true if the scheduler is paused.
	 * 
	 * @return true if the scheduler is paused
	 */
	public boolean isPaused()
	{
		try
		{
			return getScheduler().isInStandbyMode();

		}
		catch (SchedulerException se)
		{
			throw new RuntimeException("Could not determine if scheduler is in standby mode");
		}
	}

	/**
	 * Pause all the scheduled jobs, and interrupt any currently executing jobs.
	 */
	@SuppressWarnings("unchecked")
	public void pauseAll()
	{
		try
		{
			for (Iterator i = scheduler.getCurrentlyExecutingJobs().iterator(); i.hasNext();)
			{
				JobExecutionContext context = (JobExecutionContext) i.next();
				interruptJob(context.getJobDetail().getName());
			}

			getScheduler().pauseAll();

		}
		catch (SchedulerException se)
		{
			String msg = "Could not pause all jobs";
			throw new RuntimeException(msg, se);
		}
	}

	/**
	 * Resume all paused jobs.
	 */
	public void resumeAll()
	{
		try
		{
			getScheduler().resumeAll();
		}
		catch (SchedulerException se)
		{
			String msg = "Could not resume all jobs";
			throw new RuntimeException(msg, se);
		}
	}

	/**
	 * Pause the job for the given name.
	 * 
	 * @param jobName
	 *            the name of the job to pause
	 */
	public boolean pauseJob(String jobName)
	{
		try
		{
			getScheduler().pauseJob(jobName, GROUP_NAME);
			return true;
		}
		catch (SchedulerException se)
		{
			se.printStackTrace();
			return false;
		}
	}

	/**
	 * Resume the job for the given name.
	 * 
	 * @param jobName
	 *            the name of the job to resume
	 */
	public void resumeJob(String jobName) throws Exception
	{
		try
		{
			getScheduler().resumeJob(jobName, GROUP_NAME);

		}
		catch (SchedulerException se)
		{
			String msg = "Could not resume job: " + jobName;
			throw new RuntimeException(msg, se);

		}
	}

	/**
	 * Trigger the job for the given name.
	 * 
	 * @param jobName
	 *            the name of the job to trigger
	 */
	public boolean triggerJob(String jobName)
	{
		try
		{
			Trigger trigger = scheduler.getTrigger(jobName, SchedulerService.TRIGGER_GROUP_NAME);
			scheduler.triggerJob(jobName, GROUP_NAME, trigger.getJobDataMap());
			return true;
		}
		catch (SchedulerException se)
		{
			se.printStackTrace();
			return false;
		}
	}

	/**
	 * Interrupt the job for the given name, return true if the job was found.
	 * 
	 * @param jobName
	 *            the name of the job to interrupt
	 */
	public boolean interruptJob(String jobName)
	{
		try
		{
			return getScheduler().interrupt(jobName, GROUP_NAME);

		}
		catch (SchedulerException se)
		{
			String msg = "Could not obtain Quartz Scheduler JobDetails";
			throw new RuntimeException(msg, se);
		}
	}

	/**
	 * Delete the job for the given name, return true if the job was found.
	 * 
	 * @param jobName
	 *            the name of the job to delete
	 * @return true if the Job was found and deleted.
	 */
	public boolean deleteJob(String jobName)
	{
		try
		{
			return getScheduler().deleteJob(jobName, GROUP_NAME);
		}
		catch (SchedulerException se)
		{
			String msg = "Could not obtain Quartz Scheduler JobDetails";
			throw new RuntimeException(msg, se);
		}
	}

	/**
	 * Return the list of scheduled JobDetails for the group name:
	 * "click-group".
	 * 
	 * @return the list of scheduled JobDetails for the group name:
	 *         "click-group"
	 */
	public List<JobDetail> getJobDetailList()
	{
		try
		{
			List<JobDetail> list = new ArrayList<JobDetail>();

			String[] jobNames = getScheduler().getJobNames(GROUP_NAME);

			for (String jobName : jobNames)
			{
				try
				{
					list.add(getScheduler().getJobDetail(jobName, GROUP_NAME));
				}
				catch (Exception e)
				{

				}
			}

			return list;

		}
		catch (SchedulerException se)
		{
			String msg = "Could not obtain Quartz Scheduler JobDetails";
			throw new RuntimeException(msg, se);
		}
	}

	/**
	 * Return the JobDetail for the given name and group name: "click-group".
	 * 
	 * @return the JobDetail for the given name and group name: "click-group".
	 */
	public JobDetail getJobDetail(String jobName)
	{
		try
		{
			return getScheduler().getJobDetail(jobName, GROUP_NAME);

		}
		catch (SchedulerException se)
		{
			String msg = "Could not obtain Quartz Scheduler JobDetail";
			throw new RuntimeException(msg, se);
		}
	}

	/**
	 * Return true if the scheduler has the named job.
	 * 
	 * @param jobName
	 *            the name of the job
	 * @return true if the scheduler has the named job
	 */
	public boolean hasJob(String jobName)
	{
		try
		{
			return (getScheduler().getJobDetail(jobName, GROUP_NAME) != null);

		}
		catch (SchedulerException se)
		{
			String msg = "Could not obtain Quartz Scheduler JobDetail";
			throw new RuntimeException(msg, se);
		}
	}

	/**
	 * Return the list of JobDetail and Trigger associations.
	 * 
	 * @return the list of JobDetail and Trigger associations
	 */
	public List<JobAndSimpleTrigger> getJobAndTriggerList()
	{
		try
		{
			List<JobAndSimpleTrigger> list = new ArrayList<JobAndSimpleTrigger>();

			// get trigger by name and group, then get JobDetail
			String[] triggerNames = scheduler.getTriggerNames(TRIGGER_GROUP_NAME);
			for (int j = 0; j < triggerNames.length; j++)
			{
				String triggerName = triggerNames[j];
				try
				{
					Trigger trigger = scheduler.getTrigger(triggerName, TRIGGER_GROUP_NAME);
					JobDetail jobDetail = scheduler.getJobDetail(trigger.getJobName(), trigger.getJobGroup());
					list.add(new JobAndSimpleTrigger(jobDetail, trigger, getScheduler()));
				}
				catch (Exception e)
				{

				}
			}
			/**
			 * remove them String[] jobNames = getScheduler().getJobNames(
			 * GROUP_NAME );
			 * 
			 * for ( int i = 0; i < jobNames.length; i++ ) { String jobName =
			 * jobNames[i];
			 * 
			 * JobDetail jobDetail = getJobDetail( jobName );
			 * 
			 * SimpleTrigger trigger = ( SimpleTrigger
			 * )getScheduler().getTrigger( jobName, TRIGGER_GROUP_NAME );
			 * if(null!= trigger && null!= jobDetail) list.add( new
			 * JobAndSimpleTrigger( jobDetail, trigger, getScheduler() ) ); }
			 */
			return list;

		}
		catch (SchedulerException se)
		{
			String msg = "Could not obtain Quartz Scheduler JobAndTriggerList";
			throw new RuntimeException(msg, se);
		}
	}

	/**
	 * Return the Job and Trigger for the given job name
	 * 
	 * @param jobName
	 *            the name of the job
	 * @return the Job and Trigger for the given job name
	 */
	public JobAndSimpleTrigger getJobAndTrigger(String jobName)
	{
		Validate.notNull(jobName, "Null jobName parameter");

		try
		{
			JobDetail jobDetail = getJobDetail(jobName);

			if (jobDetail != null)
			{
				SimpleTrigger trigger = (SimpleTrigger) getScheduler().getTrigger(jobName, TRIGGER_GROUP_NAME);

				return new JobAndSimpleTrigger(jobDetail, trigger, getScheduler());

			}
			else
			{
				return null;
			}

		}
		catch (SchedulerException se)
		{
			String msg = "Could not obtain Quartz Scheduler JobAndTrigger";
			throw new RuntimeException(msg, se);
		}
	}

	/**
	 * Return the scheduler instance.
	 * 
	 * @return the scheduler instance
	 */
	public Scheduler getScheduler()
	{
		return scheduler;
	}

	public static String getGROUP_NAME()
	{
		return GROUP_NAME;
	}

	public static String getTRIGGER_GROUP_NAME()
	{
		return TRIGGER_GROUP_NAME;
	}

	/**
	 * get a list of job history if parameter is not null return a list of job
	 * history for specific job name
	 * 
	 * @param jobName
	 * @return
	 */
	public List<JobHistory> getJobHistory(String jobName)
	{
		String[] columns = { "JobName", "FeedSource", "RootPath", "StartDate", "EndDate", "NumberOfNodes", "Number_Succeed", "Number_Failed", "Number_Inserted", "Number_Updated", "Element_id", "logFile" };
		String sql_getJobHistory = "SELECT `JobName`, `FeedSource`, `RootPath`, `StartDate`, `EndDate`, `NumberOfNodes`, `Number_Succeed`, `Number_Failed`, `Number_Inserted`, `Number_Updated`,`Element_id`,`logFile`" + " FROM openlinks_history ";

		List<String> params = new ArrayList<String>();

		if (jobName != null && !jobName.equals(""))
		{
			sql_getJobHistory += " WHERE JobName=?";
			params.add(jobName);
		}
		sql_getJobHistory += " order by Element_id desc";
		List<JobHistory> jobHistories = new ArrayList<JobHistory>();
		//Vector<String[]> v_results = dba.select(sql_getJobHistory, columns);
		Vector<String[]> v_results = dba.selectQuery(sql_getJobHistory, params.toArray());

		for (String[] results : v_results)
		{
			JobHistory jobHistory = new JobHistory();
			jobHistory.setJobName(results[0]);
			jobHistory.setFeedSource(results[1]);
			jobHistory.setRootPath(results[2]);
			jobHistory.setStartDate(results[3]);
			jobHistory.setEndDate(results[4]);
			jobHistory.setNumberOfNodes(results[5]);
			jobHistory.setNumber_Succeed(results[6]);
			jobHistory.setNumber_Failed(results[7]);
			jobHistory.setNumber_Inserted(results[8]);
			jobHistory.setNumber_Updated(results[9]);
			jobHistory.setElementId(results[10]);
			jobHistory.setLogFile(results[11]);
			jobHistories.add(jobHistory);

		}
		return jobHistories;
	}

	public List<JobScheduleObj> getStoredJobs()
	{
		List<JobScheduleObj> jobList = new ArrayList<JobScheduleObj>();
		try
		{
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

			//String[] columns = { "JobName", "JobGroup", "JobClass", "TriggerName", "TriggerGroup", "StartDate", "EndDate", "RepeatCount", "RepeatInterval", "Context", "CronExpression" };
			String sql_getJobs = "SELECT JobName, JobGroup, JobClass, TriggerName, TriggerGroup, StartDate, EndDate, RepeatCount, RepeatInterval, Context, CronExpression FROM openlinks_jobs";
			sql_getJobs += " where";
			sql_getJobs += " (timestampdiff(SECOND, now(),StartDate) * 1000 + repeatcount * repeatInterval > 0 ";
			sql_getJobs += " and repeatcount != -1) ";
			sql_getJobs += " or ";
			sql_getJobs += " (repeatcount = -1 and now() < EndDate) ";
			sql_getJobs += " or (RepeatCount = -2)";
			//Vector<String[]> v_results = dba.select(sql_getJobs, columns);
			Vector<String[]> v_results = dba.selectQuery(sql_getJobs);
			for (Iterator<?> iter = v_results.iterator(); iter.hasNext();)
			{
				String[] results = (String[]) iter.next();
				JobScheduleObj job = new JobScheduleObj();
				JobDetail detail = new JobDetail();
				detail.setName(results[0]);
				detail.setGroup(results[1]);
				detail.setJobClass(Class.forName(results[2]));
				job.setJobDetail(detail);
				job.setStartDate(sdf.parse(results[5]));
				job.setEndDate(sdf.parse(results[6]));
				job.setRepeatCount(Integer.parseInt(results[7]));
				job.setRepeatInterval(Integer.parseInt(results[8]));
				job.setContext(results[9]);
				job.setCronExpression(results[10]);
				jobList.add(job);
			}
		}
		catch (Exception e)
		{
			logger.error("Error: ", e);
		}
		return jobList;
	}
}
