/**
 * 12:02:28 PM 14/02/2012
 * @author Vito Lefemine
 */
package au.net.webdirector.admin.quartz;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.ee.servlet.QuartzInitializerListener;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.net.webdirector.admin.utils.SecureJSPPath;
import sbe.openlinks.jobs.AbstractScheduledJob;
import sbe.openlinks.quartz.page.quartz.IFeedJob;
import sbe.openlinks.quartz.page.quartz.JobAndSimpleTrigger;

/**
 * @author Vito Lefemine
 * 
 */
@Controller
@RequestMapping("/secure/scheduler")
public class SecureQuartzJobController
{
	Logger logger = Logger.getLogger(SecureQuartzJobController.class);

	private SchedulerService service = null;

	@InitBinder
	public void initBinder(WebDataBinder binder)
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		dateFormat.setLenient(false);
		// true passed to CustomDateEditor constructor means convert empty
		// String to null
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

	@RequestMapping(value = { "/*" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET })
	public String index()
	{
		return SecureJSPPath.path("openLinks");
	}

	@RequestMapping("/jobHistory")
	public String getJobHistory(HttpServletRequest request, Model model, @RequestParam String jobName)
	{
		model.addAttribute("jobList", getService(request.getSession().getServletContext()).getJobHistory(jobName));
		return "jsp/secure/openlinks/jobHistory";
	}

	@RequestMapping("/jobList")
	public String getJobList(HttpServletRequest request, Model model)
	{
		try
		{
			List<JobAndSimpleTrigger> jobList = getService(request.getSession().getServletContext()).getJobAndTriggerList();
			model.addAttribute("jobList", jobList);
			return "jsp/secure/openlinks/jobList";
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping("jobSchedule")
	public String jobSchedule(HttpServletRequest request, Model model)
	{

		int idx = getService(request.getSession().getServletContext()).getJobDetailList().size() + 1;
		JobDetail jd = new JobDetail("Job Name [" + idx + "]", SchedulerService.GROUP_NAME, AbstractScheduledJob.class);
		Date end = new Date();
		GregorianCalendar gc = new GregorianCalendar();
		gc.add(Calendar.DAY_OF_MONTH, 1);
		end.setTime(gc.getTimeInMillis());
		long repeatTime = 1000 * 60 * 60 * 1; // 1h
		JobScheduleObj SchedulObj = new JobScheduleObj();
		SchedulObj.setJobDetail(jd);
		SchedulObj.setStartDate(new Date());
		SchedulObj.setEndDate(end);
		SchedulObj.setRepeatInterval(repeatTime);
		SchedulObj.setClassName(AbstractScheduledJob.class.getCanonicalName());
		// jd, new Date(), end, 0,
		// repeatTime,"sbe.openlinks.jobs.TestImportElementsFlex20Job"

		Map<String, String> availableJob = getAvailableJobs();

		model.addAttribute("availableJob", availableJob);
		model.addAttribute("SchedulObj", SchedulObj);
		return "jsp/secure/openlinks/jobSchedule";
	}

	/**
	 * @return
	 */
	private Map<String, String> getAvailableJobs()
	{
		/*
		 * 
		 * Get list of available classes
		 */
		Map<String, String> availableJob = new LinkedHashMap<String, String>();
		try
		{
			for (Class c : QuartzUtils.getClasses("sbe.openlinks.jobs"))
			{
				if (!c.getSimpleName().toLowerCase().contains("abstract"))
				{
					IFeedJob job = (IFeedJob) c.newInstance();
					String desc = job.getJobDescription();
					if (null != desc && !desc.equalsIgnoreCase(""))
						availableJob.put("" + c.getCanonicalName(), desc);
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return availableJob;
	}

	@RequestMapping("/deleteJoHistoryRecords")
	@ResponseBody
	public String deleteJobHistoryRecord(@RequestParam String elid, HttpServletRequest request)
	{
		StringTokenizer strk = new StringTokenizer(elid, ",");
		SchedulerService service = getService(request.getSession().getServletContext());
		boolean res = true;
		while (strk.hasMoreElements())
		{
			res &= service.deleteJobHistoryRecord(strk.nextToken());
		}
		return "" + res;
	}

	@RequestMapping("/deleteJob")
	@ResponseBody
	public String deleteJob(@RequestParam String jobName, HttpServletRequest request)
	{

		return "" + getService(request.getSession().getServletContext()).deleteJob(jobName);
	}

	@RequestMapping("/triggerJob")
	@ResponseBody
	public String triggerJob(@RequestParam String jobName, HttpServletRequest request)
	{
		return "" + getService(request.getSession().getServletContext()).triggerJob(jobName);
	}

	@RequestMapping("/resumeJob")
	@ResponseBody
	public String stopJob(@RequestParam String jobName, HttpServletRequest request)
	{
		try
		{
			getService(request.getSession().getServletContext()).resumeJob(jobName);
		}
		catch (Exception e)
		{
			logger.fatal(e);
			return "Internal Server Error";
		}
		return "" + true;
	}

	@RequestMapping("/pauseJob")
	@ResponseBody
	public String pauseJob(@RequestParam String jobName, HttpServletRequest request)
	{
		return "" + getService(request.getSession().getServletContext()).pauseJob(jobName);
	}

	@RequestMapping("/interruptJob")
	@ResponseBody
	public String interruptJob(@RequestParam String jobName, HttpServletRequest request)
	{
		return "" + getService(request.getSession().getServletContext()).interruptJob(jobName);
	}

	@RequestMapping("/jobScheduleSubmit")
	public String jobSchedule(HttpServletRequest request, Model model, JobScheduleObj schedulObj)
	{
		boolean resSchedule = false;
		if (null == schedulObj.getJobDetail() || schedulObj.getJobDetail().getName().length() == 0 || schedulObj.getJobDetail().getName().length() > 100)
		{
			model.addAttribute("resultInsert", false);
			model.addAttribute("AdditionalInfo", "Incorrect Job Name");
			return "jsp/secure/openlinks/jobSchedule";
		}

		try
		{
			Class theClass = Class.forName(schedulObj.getClassName());
			schedulObj.getJobDetail().setJobClass(theClass);
			schedulObj.getJobDetail().setGroup(SchedulerService.GROUP_NAME);
			/*
			 * run Continously
			 */
			if (schedulObj.getRepeatCount() != -1)
			{
				GregorianCalendar gc = new GregorianCalendar();
				gc.add(Calendar.YEAR, 100);
				schedulObj.setEndDate(gc.getTime());

			}
			if (schedulObj.getRepeatCount() == 0 && schedulObj.getStartDate().before(DateUtils.addSeconds(new Date(), 10)))
			{
				schedulObj.setStartDate(DateUtils.addSeconds(new Date(), 10));
			}
			resSchedule = getService(request.getSession().getServletContext()).scheduleJob(schedulObj);
			model.addAttribute("resultInsert", resSchedule);
		}
		catch (Exception e)
		{
			logger.fatal("Error:", e);
			model.addAttribute("resultInsert", false);
			model.addAttribute("AdditionalInfo", "" + e.getMessage());
		}
		int idx = getService(request.getSession().getServletContext()).getJobDetailList().size() + 1;

		schedulObj.getJobDetail().setName("Job Name [" + idx + "]");

		Map<String, String> availableJob = getAvailableJobs();

		model.addAttribute("availableJob", availableJob);

		model.addAttribute("SchedulObj", schedulObj);

		return "jsp/secure/openlinks/jobSchedule";
	}

	/**
	 * @return the service
	 */
	public SchedulerService getService(ServletContext context)
	{
		if (service == null)
		{
			StdSchedulerFactory schedulerFactory = (StdSchedulerFactory) context.getAttribute(QuartzInitializerListener.QUARTZ_FACTORY_KEY);
			if (schedulerFactory != null)
			{
				service = new SchedulerService(schedulerFactory);
			}
		}
		return service;
	}

}
