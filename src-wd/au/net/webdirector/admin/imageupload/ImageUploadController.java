package au.net.webdirector.admin.imageupload;

import java.io.File;
import java.io.IOException;
import au.net.webdirector.common.utils.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import au.net.webdirector.common.Defaults;

@Controller
public class ImageUploadController
{
	private static Logger logger = Logger.getLogger(ImageUploadController.class);


	@RequestMapping("/ckeditor/upload-image")
	@ResponseBody
	public UploadResponse uploadImage(
			HttpServletRequest request,
			HttpServletResponse response) throws IllegalStateException, IOException
	{
		MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
		MultipartFile mFile = multi.getFile("upload");
		String filename = mFile.getOriginalFilename();
		String relativePath = "_images/uploader/" + new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
		File dir = new File(Defaults.getInstance().getStoreDir(), relativePath);
		if (!dir.exists())
		{
			dir.mkdirs();
		}
		File dest = newFile(dir, filename);
		mFile.transferTo(dest);
		UploadError error = new UploadError(201, "A file with the same name already exists. The uploaded file was renamed to \"" + dest.getName() + "\".");
		String storeContext = Defaults.getInstance().getStoreContext();
		storeContext = StringUtils.startsWith(storeContext, "/") ? storeContext : "/" + storeContext;
		String url = storeContext + "/" + relativePath + "/" + dest.getName();
		UploadResponse res = new UploadResponse(dest.getName(), 1, error, url);
		return res;
	}

	private File newFile(File dir, String filename)
	{
		SimpleDateFormat sf = new SimpleDateFormat("HHmmssSSS", Locale.ENGLISH);
		File dest = new File(dir, sf.format(new Date()) + "_" + RandomStringUtils.randomAlphanumeric(5) + "_" + filename);
		int count = 0;
		while (dest.exists())
		{
			if (++count > 5)
			{
				throw new RuntimeException("Cannnot create file:" + dest.getAbsolutePath());
			}
			dest = new File(dir, sf.format(new Date()) + "_" + RandomStringUtils.randomAlphanumeric(5) + "_" + filename);
		}
		return dest;
	}


	private class UploadResponse
	{
		String fileName;
		int uploaded;
		UploadError error;
		String url;

		public UploadResponse(String fileName, int uploaded, UploadError error, String url)
		{
			super();
			this.fileName = fileName;
			this.uploaded = uploaded;
			this.error = error;
			this.url = url;
		}

		public String getFileName()
		{
			return fileName;
		}

		public int getUploaded()
		{
			return uploaded;
		}

		public UploadError getError()
		{
			return error;
		}

		public String getUrl()
		{
			return url;
		}

	}

	private class UploadError
	{
		int number;
		String message;

		public UploadError(int number, String message)
		{
			super();
			this.number = number;
			this.message = message;
		}

		public int getNumber()
		{
			return number;
		}

		public String getMessage()
		{
			return message;
		}

	}
}
