package au.net.webdirector.admin.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/free/csrf-token")
public class CheckCSRFTokenController
{

	@RequestMapping("/init")
	public String init(HttpServletRequest request, HttpServletResponse response)
	{
		response.setContentType("application/javascript");
		return "jsp/csrfToken";
	}
}
