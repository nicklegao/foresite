package au.net.webdirector.admin.security;

import java.util.Hashtable;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import au.net.webdirector.admin.security.domain.User;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;

public class UserDAO
{

	static Logger logger = Logger.getLogger(UserDAO.class);

	public static UserDAO getInstance(ServletContext servletContext)
	{
		return getInstance();
	}

	public static UserDAO getInstance()
	{
		return new UserDAO();
	}

	private UserDAO()
	{

	}

	public User getUserByUserName(String userName, String password)
	{
		String query = "select * from users where binary user_name = ? and binary password = ?";
		List<Hashtable<String, String>> hUsers = DataAccess.getInstance().select(query, new String[] { userName, password }, new HashtableMapper());
		return getUser(hUsers);
	}

	public User getUserByUserName(String userName)
	{
		String query = "select * from users where binary user_name = ?";
		List<Hashtable<String, String>> hUsers = DataAccess.getInstance().select(query, new String[] { userName }, new HashtableMapper());
		return getUser(hUsers);
	}

	public User getUserById(int id)
	{
		List<Hashtable<String, String>> hUsers = DataAccess.getInstance().select("select * from users where user_id = ?", new Object[] { id }, new HashtableMapper());
		return getUser(hUsers);
	}

	private User getUser(List<Hashtable<String, String>> hUsers)
	{
		if (hUsers.size() == 0)
		{
			return null;
		}
		User user = new User();
		user.construct(hUsers.get(0));
		return user;
	}

	@Deprecated
	public boolean recordIncorrectLogin(String username)
	{
		int updates = DataAccess.getInstance().updateData(
				"UPDATE users "
						+ " SET incorrectLoginAttempts=incorrectLoginAttempts+1,"
						+ " lastIncorrectLogin = now() "
						+ " WHERE binary user_name=?", new String[] { username });
		return updates == 1;
	}

	@Deprecated
	public boolean recordCorrectLogin(String username)
	{
		int updates = DataAccess.getInstance().updateData(
				"UPDATE users "
						+ " SET incorrectLoginAttempts=0,"
						+ " lastIncorrectLogin = NULL "
						+ " WHERE binary user_name=?", new String[] { username });
		return updates == 1;
	}

}
