package au.net.webdirector.admin.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import webdirector.db.UserUtils;
import au.net.webdirector.common.utils.password.PasswordUtils;

@Controller
@RequestMapping("/configureUsers")
public class UserConfigController
{

	@RequestMapping("/updateUser")
	@ResponseBody
	public SimpleJsonResponse updateUser(HttpServletRequest request)
	{
		String userId = request.getParameter("userId");
		String userName = request.getParameter("userName");
		String name = request.getParameter("name");
		String company = request.getParameter("company");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String password = PasswordUtils.encrypt(request.getParameter("password"));

		String user_type = (String) request.getParameter("User_Type");

		String administrator = ("1".equals(user_type)) ? "on" : null;
		String secureUser = ("2".equals(user_type)) ? "on" : null;
		String moduleUser = ("4".equals(user_type)) ? "on" : null;
		String readOnlyUser = ("5".equals(user_type)) ? "on" : null;

		String moduleSelectedSingle = request.getParameter("moduleSelected");
		String default_layout = request.getParameter("Default_Layout");
		String access_file_manager = request.getParameter("Access_File_Manager");
		String can_change_password = request.getParameter("Can_Change_Password") == null ? "no" : request.getParameter("Can_Change_Password");
		String can_edit_account = request.getParameter("Can_Edit_Account") == null ? "no" : request.getParameter("Can_Edit_Account");
		boolean isAdd = StringUtils.isBlank(userId);
		String[] moduleSelected = null;
		if (moduleSelectedSingle != null)
		{
			moduleSelected = moduleSelectedSingle.split(",");
		}

		if (moduleSelected != null)
		{
			System.out.println("mod " + moduleSelected.length);
			System.out.println("userName " + userName);
		}
		else
		{
			System.out.println("mod is null");
		}

		boolean success = false;
		String message = "";

		try
		{

			UserUtils db = new UserUtils();
			if (isAdd && db.getUserByUsername(userName) != null)
			{
				return new SimpleJsonResponse(false, "Login User Name already exists!");
			}
			String[] user = db.getUserByEmail(email);
			if (isAdd && user != null
					|| !isAdd && user != null && !user[0].equals(userId))
			// add && user !=null
			// update && user !=null && !user[0].equals(userId)
			{
				return new SimpleJsonResponse(false, "Email already exists!");
			}
			if (isAdd)
			{
				success = db.addUser(userName, name, company, email, phone, password, moduleSelected, administrator, secureUser, moduleUser, readOnlyUser, default_layout, access_file_manager, can_change_password, can_edit_account);
			}
			else
			{
				success = db.editUser(userId, userName, name, company, email, phone, password, moduleSelected, administrator, secureUser, moduleUser, readOnlyUser, default_layout, access_file_manager, can_change_password, can_edit_account);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			message = "Internal Server Error";
		}
		return new SimpleJsonResponse(success, message);
	}

	@RequestMapping("/updateUserNonAdmin")
	@ResponseBody
	public SimpleJsonResponse updateUserNonAdmin(HttpServletRequest request)
	{
		String userId = (String) request.getSession().getAttribute(LoginController.WD_USER_ID);
		String userName = request.getParameter("userName");
		String name = request.getParameter("name");
		String company = request.getParameter("company");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");

		String default_layout = request.getParameter("Default_Layout");

		boolean success = false;
		String message = "";

		try
		{
			UserUtils db = new UserUtils();
			success = db.updateUser(userId, userName, name, company, email, phone, default_layout);

		}
		catch (Exception e)
		{
			e.printStackTrace();
			message = "Internal Server Error";
		}
		return new SimpleJsonResponse(success, message);
	}

	@RequestMapping("/deleteUser")
	@ResponseBody
	public SimpleJsonResponse deleteUser(@RequestParam String userId)
	{
		boolean success = false;
		String message = "";

		try
		{
			UserUtils db = new UserUtils();
			success = db.deleteUser(userId);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			message = "Internal Server Error";
		}
		return new SimpleJsonResponse(success, message);
	}

	@RequestMapping("/changePassword")
	@ResponseBody
	public SimpleJsonResponse changePassword(HttpSession session,
			@RequestParam String currentPassword,
			@RequestParam String newPassword,
			@RequestParam String repeatNewPassword)
	{
		boolean success = false;
		String message = "";

		try
		{
			UserUtils db = new UserUtils();
			message = db.updateAdminPasswordDetails((String) session.getAttribute(LoginController.WD_USER_ID), currentPassword, newPassword, repeatNewPassword);
			if (message.equals("Password updated OK"))
			{
				success = true;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			message = "Internal Server Error";
		}
		return new SimpleJsonResponse(success, message);
	}

	public static class SimpleJsonResponse
	{
		private final boolean success;
		private final String message;

		public SimpleJsonResponse(boolean success, String message)
		{
			this.success = success;
			this.message = message;
		}

		public boolean isSuccess()
		{
			return success;
		}

		public String getMessage()
		{
			return message;
		}
	}

}
