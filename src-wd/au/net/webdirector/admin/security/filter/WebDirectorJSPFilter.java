/**
 * @author Nick Yiming Gao
 * @date 2015-9-24
 */
package au.net.webdirector.admin.security.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

public class WebDirectorJSPFilter implements Filter
{

	protected static Logger logger = Logger.getLogger(WebDirectorJSPFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException
	{
		// Nothing to do...
		logger.info("init");
	}

	@Override
	public void destroy()
	{
		// Nothing to do...
		logger.info("destroy");
	}

	@Override
	public void doFilter(
			ServletRequest req,
			ServletResponse resp,
			FilterChain chain) throws IOException, ServletException
	{

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		HttpSession session = request.getSession();

		String uri = request.getRequestURI();
		if (uri.endsWith(".jsp"))
		{
			logger.fatal("Blocking direct access for JSP: " + uri);
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
		}
		else
		{
			chain.doFilter(req, resp);
		}

	}
}
