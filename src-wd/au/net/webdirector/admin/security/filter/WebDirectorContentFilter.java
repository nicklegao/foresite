package au.net.webdirector.admin.security.filter;

import javax.servlet.http.HttpServletRequest;

public class WebDirectorContentFilter extends WebDirectorAbstractFilter
{

	@Override
	protected boolean needLogin(HttpServletRequest request)
	{
		String uri = request.getRequestURI();
		return uri.endsWith(".html") || uri.endsWith(".servlet");
	}

	@Override
	protected boolean needAdmin(HttpServletRequest request)
	{
		String uri = request.getRequestURI();
		return uri.contains("/secure/");
	}


}
