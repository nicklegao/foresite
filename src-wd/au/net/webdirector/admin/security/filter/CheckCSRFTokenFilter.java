/**
 * @author Nick Yiming Gao
 * @date 2016-3-7
 */
package au.net.webdirector.admin.security.filter;

/**
 * 
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpMethod;

import au.com.ci.system.ResettableStreamHttpServletRequest;

import com.oreilly.servlet.MultipartRequest;

public class CheckCSRFTokenFilter implements Filter
{
	public static String TokenFieldName = "SYS_CSRF_TOKEN";
	private static Logger logger = Logger.getLogger(CheckCSRFTokenFilter.class);

	List<String> exemptions = new ArrayList<String>();

	@Override
	public void destroy()
	{

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException
	{
		ResettableStreamHttpServletRequest request = new ResettableStreamHttpServletRequest((HttpServletRequest) req);
//		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		getCSRFTokenToSession(request.getSession());

		if (exempt(request) || tokenValid(request))
		{
			request.resetInputStream();
			chain.doFilter(request, res);
			return;
		}
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		logger.warn("Required token does not exist!");
		return;

	}

	/**
	 * @param request
	 */
	public static String renewCSRFTokenToSession(HttpSession session)
	{
		String uuid = UUID.randomUUID().toString();
		session.setAttribute(TokenFieldName, uuid);
		return uuid;
	}

	public static String getCSRFTokenToSession(HttpSession session)
	{
		if (session.getAttribute(TokenFieldName) == null)
		{
			return renewCSRFTokenToSession(session);
		}
		return (String) session.getAttribute(TokenFieldName);
	}

	private boolean tokenValid(HttpServletRequest request)
	{
		if (request.getContentType() != null
				&& request.getContentType().indexOf("multipart/form-data") > -1)
		{
			return tokenValidForMultipartRequest(request);
		}
		if (request.getParameter(TokenFieldName) == null && request.getHeader(TokenFieldName) == null)
		{
			return false;
		}
		String token = request.getParameter(TokenFieldName) == null ? request.getHeader(TokenFieldName) : request.getParameter(TokenFieldName);
		return StringUtils.equalsIgnoreCase(token, getCSRFTokenToSession(request.getSession()));
	}

	private boolean tokenValidForMultipartRequest(HttpServletRequest request)
	{
		try
		{
			MultipartRequest multi = new MultipartRequest(request, System.getProperty("java.io.tmpdir"));
			if (multi.getParameter(TokenFieldName) == null && request.getHeader(TokenFieldName) == null)
			{
				return false;
			}
			String token = multi.getParameter(TokenFieldName) == null ? request.getHeader(TokenFieldName) : multi.getParameter(TokenFieldName);
			return StringUtils.equalsIgnoreCase(token, getCSRFTokenToSession(request.getSession()));
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return false;
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException
	{
		exemptions.clear();
		String param = filterConfig.getInitParameter("exemptions");
		parseExemptions(param);
	}

	private void parseExemptions(String param)
	{
		if (StringUtils.isNotBlank(param))
		{
			exemptions.addAll(Arrays.asList(StringUtils.split(param, ",")));
		}
	}

	private boolean exempt(HttpServletRequest request)
	{
		if (StringUtils.equalsIgnoreCase(request.getMethod(), HttpMethod.GET.name()) || StringUtils.equalsIgnoreCase(request.getMethod(), HttpMethod.HEAD.name()))
		{
			return true;
		}
		String url = request.getRequestURI();
		for (String pattern : exemptions)
		{
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(url);
			if (m.find())
			{
				return true;
			}
		}
		return false;
	}
}
