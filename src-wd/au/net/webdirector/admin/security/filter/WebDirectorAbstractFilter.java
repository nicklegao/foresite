/**
 * @author Nick Yiming Gao
 * @date 2015-9-24
 */
package au.net.webdirector.admin.security.filter;

/**
 * 
 */

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.admin.security.LoginController;

public abstract class WebDirectorAbstractFilter implements Filter
{

	protected static Logger logger = Logger.getLogger(WebDirectorAbstractFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException
	{
		// Nothing to do...
		logger.info("init");
	}

	@Override
	public void destroy()
	{
		// Nothing to do...
		logger.info("destroy");
	}

	@Override
	public void doFilter(
			ServletRequest req,
			ServletResponse resp,
			FilterChain chain) throws IOException, ServletException
	{

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		HttpSession session = request.getSession();

		// Set standard HTTP/1.1 no-cache headers.
		response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");

		// Set standard HTTP/1.0 no-cache header.
		response.setHeader("Pragma", "no-cache");

		String userId = (String) session.getAttribute(LoginController.WD_USER_ID);
		Boolean isAdmin = (Boolean) session.getAttribute(LoginController.WD_USER_ADMIN);

		if (needLogin(request) && userId == null)
		{
			loginRedirect(request, response);
			return;
		}

		if (needAdmin(request) && (isAdmin == null || !isAdmin))
		{
			unauthorisedRedirect(request, response);
			return;
		}

		response.setHeader("session-lifetime", Integer.toString(session.getMaxInactiveInterval()));
		chain.doFilter(req, resp);
	}

	protected void loginRedirect(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		logger.warn("Blocking forbidden request for: " + request.getRequestURI());
		if (ajaxCall(request))
		{
			response.setStatus(403); // forbidden
		}
		else
		{
			response.sendRedirect("/webdirector");
		}
	}

	protected void unauthorisedRedirect(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		logger.warn("Blocking unauthorized request for: " + request.getRequestURI());
		if (ajaxCall(request))
		{
			response.setStatus(401); // unauthorized
		}
		else
		{
			response.sendRedirect("/admin/unauthorised.jsp");
		}
	}

	protected boolean ajaxCall(HttpServletRequest request)
	{
		String ajax = request.getHeader("X-Requested-With");
		return StringUtils.equalsIgnoreCase(ajax, "XMLHttpRequest");
	}

	protected abstract boolean needLogin(HttpServletRequest request);

	protected abstract boolean needAdmin(HttpServletRequest request);
}
