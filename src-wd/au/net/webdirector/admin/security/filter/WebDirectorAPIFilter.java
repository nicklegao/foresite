package au.net.webdirector.admin.security.filter;

import javax.servlet.http.HttpServletRequest;

public class WebDirectorAPIFilter extends WebDirectorAbstractFilter
{
	@Override
	protected boolean needLogin(HttpServletRequest request)
	{
		String uri = request.getRequestURI();
		if (uri.equals("/webdirector"))
		{
			return false;
		}
		int endOfAPIPath = uri.indexOf("/", 1);
		String apiPath = uri.substring(endOfAPIPath);

		return !apiPath.startsWith("/free/");
	}

	@Override
	protected boolean needAdmin(HttpServletRequest request)
	{
		String uri = request.getRequestURI();
		return uri.contains("/secure/");
	}
}
