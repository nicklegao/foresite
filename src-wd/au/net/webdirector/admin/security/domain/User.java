package au.net.webdirector.admin.security.domain;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map;



public class User implements Serializable
{

	private static final long serialVersionUID = -2332995372300967718L;
	private String username;
	private String password;
	private int id;
	private String name;
	private String company;
	private int userLevel;
	private String defaultLayout;
	private String email;

	private String incorrectLoginAttempts;
	private String lastIncorrectLogin;

	public int getUserLevel()
	{
		return userLevel;
	}

	public void setUserLevel(int userLevel)
	{
		this.userLevel = userLevel;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	/**
	 * @deprecated Use PasswordUtils
	 */
	public String getPassword()
	{
		System.err.println("WARN: Fetching password. See PasswordUtils to decrypt.");
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getCompany()
	{
		return company;
	}

	public void setCompany(String company)
	{
		this.company = company;
	}

	public String getIncorrectLoginAttempts()
	{
		return incorrectLoginAttempts;
	}

	public void setIncorrectLoginAttempts(String incorrectLoginAttempts)
	{
		this.incorrectLoginAttempts = incorrectLoginAttempts;
	}

	public String getLastIncorrectLogin()
	{
		return lastIncorrectLogin;
	}

	public void setLastIncorrectLogin(String lastIncorrectLogin)
	{
		this.lastIncorrectLogin = lastIncorrectLogin;
	}



	public String getDefaultLayout()
	{
		return defaultLayout;
	}

	public void setDefaultLayout(String defaultLayout)
	{
		this.defaultLayout = defaultLayout;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public void construct(Map<String, String> m)
	{
		this.setUserLevel(Integer.parseInt(m.get("UserLevel_id")));
		this.setUsername(m.get("user_name"));
		this.setPassword(m.get("password"));
		this.setId(Integer.parseInt(m.get("user_id")));
		this.setName(m.get("Name"));
		this.setCompany(m.get("Company"));
		this.setIncorrectLoginAttempts(m.get("incorrectLoginAttempts"));
		this.setLastIncorrectLogin(m.get("lastIncorrectLogin"));
		this.setDefaultLayout(m.get("default_layout"));
		this.setEmail(m.get("Email"));
	}

	public Hashtable<String, String> toHashtable()
	{
		Hashtable<String, String> m = new Hashtable<String, String>();
		m.put("UserLevel_id", String.valueOf(this.getUserLevel()));
		m.put("user_name", this.getUsername());
		m.put("password", this.getPassword());
		m.put("user_id", String.valueOf(this.getId()));
		m.put("Name", this.getName());
		m.put("Company", this.getCompany());
		m.put("incorrectLoginAttempts", this.getIncorrectLoginAttempts());
		m.put("lastIncorrectLogin", this.getLastIncorrectLogin());
		m.put("default_layout", this.getDefaultLayout());
		m.put("Email", this.getEmail());
		return m;
	}

}
