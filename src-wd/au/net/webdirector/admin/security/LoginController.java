package au.net.webdirector.admin.security;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UrlPathHelper;

import com.ci.webdirector.utils.SimpleStringCipher;

import au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder;
import au.corporateinteractive.qcloud.proposalbuilder.service.SecurityKeyService;
import au.net.webdirector.admin.logging.LoggingController;
import au.net.webdirector.admin.modules.privileges.PrivilegeUtils;
import au.net.webdirector.admin.security.domain.User;
import au.net.webdirector.admin.security.filter.CheckCSRFTokenFilter;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.util.text.DefaultDateFormat;
import au.net.webdirector.common.utils.SimpleDateFormat;
import au.net.webdirector.common.utils.email.FileTemplateEmailBuilder;
import au.net.webdirector.common.utils.password.PasswordUtils;

@Controller
public class LoginController
{
	private static Logger logger = Logger.getLogger(LoginController.class);

	public static final String WD_USER = "WebDirector_User";
	public static final String WD_USER_ID = "WebDirector_UserId";
	public static final String WD_USER_LEVEL_ID = "userLevelId";
	public static final String WD_USER_ADMIN = "userAdmin";

	private static final int INCORRECT_LOGIN_THRESHOLD = 3;
	private static final int INCORRECT_LOGIN_LOCKOUT_MS = (60 * 1000) * 5/*Minutes*/;

	private static final int RESET_PASSWORD_TOKEN_EXPIRES_IN_MINUTES = 30;

	private static final String WD_LOGIN_VERIFICATION = "webdirectorLoginVerificationKey";


	public static final String[] SESSION_ATTRIBUTES = new String[] {
			WD_USER,
			WD_USER_ID,
			WD_USER_LEVEL_ID,
			WD_USER_ADMIN,
	};

	public static final String[] SESSION_CACHE_ATTRIBUTES = new String[] {
			//Externally set (known) dependencies...
			PrivilegeUtils.SESSION_PRIV_OBJS,
			PrivilegeUtils.SESSION_PRIV_LOCKS,
			LoggingController.SESSION_LOG_HISTORY
	};


	//TODO: Spring validation
	@RequestMapping("/free/logout")
	@ResponseBody
	public boolean logoutUser(
			HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session)
	{
		for (String attr : SESSION_ATTRIBUTES)
		{
			session.removeAttribute(attr);
		}
		clearCache(session);

		request.setAttribute("AuditTrial_LogoutSuccessful", true);
		return true;
	}

	//TODO: Spring validation
	@RequestMapping("/free/login")
	//UPDATE setFirstScreen(...)
	@ResponseBody
	public AuthenticationResponse loginUser(
			HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session,
			@RequestParam String username,
			@RequestParam String password,
			@RequestParam(required = false) String remember) throws IOException
	{
		//TODO: License validation
		//TODO: Use a configuration file instead of web.xml
		String encryptedPasword = PasswordUtils.encrypt(password);

		AuthenticationResponse authenticationResponse = loginUser(request, username, encryptedPasword, response);

		request.setAttribute("AuditTrial_LoginSuccessful", authenticationResponse.isSuccess());
		return authenticationResponse;
	}

	@RequestMapping("/free/verifyLogin-link")
	public void verifyLink(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String username,
			@RequestParam String verificationCode) throws IOException, SQLException
	{

		String verificationKey = (String) session.getAttribute(WD_LOGIN_VERIFICATION);

		verificationCode = username + ":" + verificationCode;

		if (StringUtils.equals(verificationCode, verificationKey))
		{
			UserDAO dao = UserDAO.getInstance();
			User user = dao.getUserByUserName(username);

			session.setAttribute(WD_USER, username);
			session.setAttribute(WD_USER_ID, "" + user.getId());
			session.setAttribute(WD_USER_LEVEL_ID, "" + user.getUserLevel());
			session.setAttribute(WD_USER_ADMIN, user.getUserLevel() == 1);
			//session.setAttribute("pass", password);
			CheckCSRFTokenFilter.renewCSRFTokenToSession(session);
			response.sendRedirect(request.getContextPath() + "/webdirector/workbench");
		}
		else
		{
			request.setAttribute("title", "Oops!");//default message
			request.setAttribute("message", "Your Security link is not valid.");
//			request.setAttribute("email", email);
			response.sendRedirect(request.getContextPath() + "/security/genericMessage");
		}
	}

	@RequestMapping("/free/verifyLogin")
	@ResponseBody
	public AuthenticationResponse verifyLogin(
			HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session,
			@RequestParam String username,
			@RequestParam String verificationCode) throws IOException
	{

		Boolean isLink = (Objects.equals(request.getParameter("islink"), "true") ? true : false);

		String verificationKey = (String) session.getAttribute(WD_LOGIN_VERIFICATION);

		verificationCode = username + ":" + verificationCode;

		if (StringUtils.equals(verificationCode, verificationKey))
		{
			return putUserIntoSession(username, session, request, isLink, response);
		}
		else
		{
			return new AuthenticationResponse(false, "Verification code incorrect.");
		}
	}

	private static AuthenticationResponse putUserIntoSession(String username, HttpSession session, HttpServletRequest request, boolean isLink, HttpServletResponse response) throws IOException
	{
		UserDAO dao = UserDAO.getInstance();
		User user = dao.getUserByUserName(username);

		session.setAttribute(WD_USER, username);
		session.setAttribute(WD_USER_ID, "" + user.getId());
		session.setAttribute(WD_USER_LEVEL_ID, "" + user.getUserLevel());
		session.setAttribute(WD_USER_ADMIN, user.getUserLevel() == 1);
		//session.setAttribute("pass", password);
		CheckCSRFTokenFilter.renewCSRFTokenToSession(session);

		//TODO: What does this do?
		// loginSuccessful is used by AuditTrialFilter 
		//request.setAttribute("AuditTrial_LoginSuccessful", true);

		String firstScreen = "";

		String defaultLayout = user.getDefaultLayout();

		if (null != defaultLayout && defaultLayout.equalsIgnoreCase("Dashboard"))
		{
			firstScreen = "/webdirector/dashboard";
		}
		else
		{
			firstScreen = "/webdirector/workbench";
		}

		String firstLoginScreenCookieName = firstLoginScreenCookieName(user.getId());
		for (Cookie cookie : request.getCookies())
		{
			if (cookie.getName().equals(firstLoginScreenCookieName))
			{
				firstScreen = cookie.getValue();
			}
		}

		dao.recordCorrectLogin(username);
		if (isLink)
		{

			response.sendRedirect("/webdirector/workbench");
			return new AuthenticationResponse(true, firstScreen);
		}
		else
		{
			return new AuthenticationResponse(true, firstScreen);
		}
	}

	public static void setFirstScreen(HttpServletRequest request, HttpServletResponse response)
	{
		HttpSession session = request.getSession();
		int userID = Integer.parseInt((String) session.getAttribute(WD_USER_ID));

		Calendar cal = GregorianCalendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		long ageMilliseconds = cal.getTimeInMillis() - System.currentTimeMillis();

		Cookie p = new Cookie(firstLoginScreenCookieName(userID), new UrlPathHelper().getOriginatingRequestUri(request));
		p.setPath("/webdirector");
		p.setMaxAge((int) (ageMilliseconds / 1000));
		response.addCookie(p);
	}

	public static AuthenticationResponse loginUser(HttpServletRequest request, String username, String encryptedPasword, HttpServletResponse response) throws IOException
	{
		HttpSession session = request.getSession();
		//TODO: License validation
		//TODO: Use a configuration file instead of web.xml

		Defaults d = Defaults.getInstance();
		d.setProperties(session.getServletContext());

		UserDAO dao = UserDAO.getInstance();
		User user = dao.getUserByUserName(username);
		if (user != null)
		{
			//check incorrect logins FIRST
			int incorrectLogins = Integer.parseInt(user.getIncorrectLoginAttempts());
			Date lastAttempt = DefaultDateFormat.parseDate(user.getLastIncorrectLogin());
			if (lastAttempt != null
					&& incorrectLogins > 0
					&& incorrectLogins % INCORRECT_LOGIN_THRESHOLD == 0)
			{
				long timeSince = System.currentTimeMillis() - lastAttempt.getTime();
				if (timeSince < INCORRECT_LOGIN_LOCKOUT_MS)
				{
					return new AuthenticationResponse(false, "Your account is currently locked.");
				}
			}

			//now check password
			if (user.getPassword().equals(encryptedPasword))
			{

				if ("dev".equals(System.getProperty("au.net.webdirector.serverMode")))
				{
					return putUserIntoSession(username, session, request, false, response);
				}

				String verificationKey = new SecurityKeyService().generateRandomKey(12, 6, "-");
				session.setAttribute(WD_LOGIN_VERIFICATION, username + ":" + verificationKey);

				boolean emailSent = new EmailBuilder().prepareWebdirectorSignupVerification(verificationKey, username).addTo(user.getEmail()).doSend();
				if (emailSent)
				{
					return new AuthenticationResponse(true, "Credentials valid.");
				}
				else
				{
					return new AuthenticationResponse(false, "Something went wrong. Please try again later.");
				}

			}
			else
			{
				dao.recordIncorrectLogin(username);
				return new AuthenticationResponse(false, "Incorrect password.");
			}
		}
		else
		{
			dao.recordIncorrectLogin(username);

			return new AuthenticationResponse(false, "Sorry, user not found.");
		}
	}

	public static void clearCache(HttpSession session)
	{
		for (String attr : SESSION_CACHE_ATTRIBUTES)
		{
			session.removeAttribute(attr);
		}
	}

	/**
	 * @param id
	 * @return
	 */
	private static String firstLoginScreenCookieName(int userID)
	{
		return "autologinScreen-" + userID;
	}

	public static class AuthenticationResponse
	{

		private final boolean success;
		private final String message;

		public AuthenticationResponse(boolean success, String message)
		{
			this.success = success;
			this.message = message;
		}

		public boolean isSuccess()
		{
			return success;
		}

		public String getMessage()
		{
			return message;
		}
	}

	@RequestMapping("/free/forgot-password")
	@ResponseBody
	public AuthenticationResponse forgotPassword(
			HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session,
			@RequestParam String username)
	{

		UserDAO dao = UserDAO.getInstance();
		User user = dao.getUserByUserName(username);
		if (user == null)
		{
			return new AuthenticationResponse(false, "Sorry, user not found.");
		}
		String email = user.getEmail();
		if (!isValidEmailAddress(email))
		{
			return new AuthenticationResponse(false, "Sorry, your registered email address is invalid.<br>Please contract site admin to reset your password.");
		}
		String emailForDisplay = asteriskEmail(email);
		String now = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String tokenForURL = SimpleStringCipher.encrypt(now, true);
		String tokenForDB = SimpleStringCipher.encrypt(now, false);
		String usernameForURL = username;
		try
		{
			usernameForURL = URLEncoder.encode(username, "utf-8");
		}
		catch (Exception e)
		{

		}
		new DBaccess().updateData("update users set resetPasswordToken = ? where user_name = ?", new String[] { tokenForDB, username });

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("resetPasswordToken", tokenForURL);
		params.put("username", usernameForURL);
		params.put("expire_min", RESET_PASSWORD_TOKEN_EXPIRES_IN_MINUTES);
		boolean emailSent = false;
		try
		{
			emailSent = new FileTemplateEmailBuilder("/au/net/webdirector/admin/template/", request)
					.addModule("wd_users", user.toHashtable())
					.prepareEmail("resetPasswordLink.html", "Reset Your Webdirector Password", params)
					.addTo(email)
					.doSend();
		}
		catch (Exception e)
		{
			logger.error("Error", e);
		}
		if (!emailSent)
		{
			return new AuthenticationResponse(false, "Failed to send email to " + emailForDisplay);
		}
		return new AuthenticationResponse(true, "An email has been sent to " + emailForDisplay + ". Please check it out and follow the instruction.");
	}

	/**
	 * @param email
	 * @return
	 */
	private String asteriskEmail(String email)
	{
		if (StringUtils.isBlank(email))
		{
			return "";
		}
		String[] array = StringUtils.split(email, "@");
		if (array.length != 2)
		{
			return email;
		}
		String begin = array[0].length() > 2 ? array[0].substring(0, 2) + StringUtils.repeat("*", array[0].length() - 2) : array[0];
		String end = array[1].length() > 2 ? StringUtils.repeat("*", array[1].length() - 2) + array[1].substring(array[1].length() - 2) : array[1];
		return begin + "@" + end;
	}

	private boolean isValidEmailAddress(String email)
	{
		boolean result = true;
		try
		{
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		}
		catch (AddressException ex)
		{
			result = false;
		}
		return result;
	}

	@RequestMapping("/free/reset-password")
	public String resetPassword(
			HttpServletRequest request,
			@RequestParam String user,
			@RequestParam String token)
	{

		request.setAttribute("user", user);
		String errorMsg = validateResetPasswordRequest(user, token);
		if (errorMsg != null)
		{
			request.setAttribute("ErrorMsg", errorMsg);
			return "user/forgot-password";
		}
		request.setAttribute("token", token);
		return "user/reset-password";
	}


	@RequestMapping("/free/reset-password/do")
	@ResponseBody
	public AuthenticationResponse resetPasswordDo(
			HttpServletRequest request,
			@RequestParam String username,
			@RequestParam String token,
			@RequestParam String password)
	{
		String errorMsg = validateResetPasswordRequest(username, token);
		if (null != errorMsg)
		{
			return new AuthenticationResponse(false, errorMsg);
		}
		try
		{
			TransactionDataAccess.getInstance().update("update users set resetPasswordToken = '',incorrectLoginAttempts=0, password = ? where user_name = ?", new String[] { PasswordUtils.encrypt(password), username });
			return new AuthenticationResponse(true, "");
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return new AuthenticationResponse(false, "Failed to reset your password. Please contact Site Admin.");
		}
	}

	private String validateResetPasswordRequest(String user, String token)
	{
		try
		{
			String count = TransactionDataAccess.getInstance().selectString("select count(*) from users where user_name = ? and resetPasswordToken = ?", new String[] { user, token });
			if (StringUtils.equals(count, "0"))
			{
				return "Sorry, invalid parameters. Please try again.";
			}
		}
		catch (SQLException e)
		{
			return "Sorry, invalid parameters. Please try again.";
		}
		String sTime = SimpleStringCipher.decrypt(token);
		try
		{
			Date dTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(sTime);
			if (DateUtils.addMinutes(dTime, RESET_PASSWORD_TOKEN_EXPIRES_IN_MINUTES).before(new Date()))
			{
				return "Sorry, this link has been expired. Please try again.";
			}
		}
		catch (Exception e)
		{
			return "Sorry, invalid parameters. Please try again.";
		}
		return null;
	}

	@RequestMapping(value = "/free/unlock-account", method = RequestMethod.POST)
	@ResponseBody
	public AuthenticationResponse unlockAccountPost(
			HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session,
			@RequestParam String username)
	{

		UserDAO dao = UserDAO.getInstance();
		User user = dao.getUserByUserName(username);
		if (user == null)
		{
			return new AuthenticationResponse(false, "Sorry, user not found.");
		}
		int incorrectLogins = Integer.parseInt(user.getIncorrectLoginAttempts());
		Date lastAttempt = DefaultDateFormat.parseDate(user.getLastIncorrectLogin());
		boolean locked = false;
		if (lastAttempt != null
				&& incorrectLogins > 0
				&& incorrectLogins % INCORRECT_LOGIN_THRESHOLD == 0)
		{
			long timeSince = System.currentTimeMillis() - lastAttempt.getTime();
			if (timeSince < INCORRECT_LOGIN_LOCKOUT_MS)
			{
				locked = true;
			}
		}
		if (!locked)
		{
			return new AuthenticationResponse(false, "Your account had been unlocked already.");
		}
		String email = user.getEmail();
		if (!isValidEmailAddress(email))
		{
			return new AuthenticationResponse(false, "Sorry, your registered email address is invalid.<br>Please contract site admin to unlock your account.");
		}
		String emailForDisplay = asteriskEmail(email);
		String now = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String tokenForURL = SimpleStringCipher.encrypt(now, true);
		String tokenForDB = SimpleStringCipher.encrypt(now, false);
		String usernameForURL = username;
		try
		{
			usernameForURL = URLEncoder.encode(username, "utf-8");
		}
		catch (Exception e)
		{

		}
		new DBaccess().updateData("update users set resetPasswordToken = ? where user_name = ?", new String[] { tokenForDB, username });

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("unlockAccountToken", tokenForURL);
		params.put("username", usernameForURL);
		params.put("expire_min", RESET_PASSWORD_TOKEN_EXPIRES_IN_MINUTES);
		boolean emailSent = false;
		try
		{
			emailSent = new FileTemplateEmailBuilder("/au/net/webdirector/admin/template/", request)
					.addModule("wd_users", user.toHashtable())
					.prepareEmail("unlockAccountLink.html", "Unlock Your WebDirector Account", params)
					.addTo(email)
					.doSend();
		}
		catch (Exception e)
		{
			logger.error("Error", e);
		}
		if (!emailSent)
		{
			return new AuthenticationResponse(false, "Failed to send email to " + emailForDisplay);
		}
		return new AuthenticationResponse(true, "An email has been sent to " + emailForDisplay + ". Please check it out and follow the instruction.");
	}

	@RequestMapping(value = "/free/unlock-account", method = RequestMethod.GET)
	public String unlockAccountGet(
			HttpServletRequest request,
			@RequestParam String user,
			@RequestParam String token)
	{
		request.setAttribute("user", user);
		String errorMsg = validateResetPasswordRequest(user, token);
		if (errorMsg != null)
		{
			request.setAttribute("ErrorMsg", errorMsg);
			return "user/unlock-account";
		}
		new DBaccess().updateData("update users set resetPasswordToken = '', incorrectLoginAttempts=0 where user_name = ?", new String[] { user });
		request.setAttribute("Success", true);
		return "user/unlock-account";
	}
}
