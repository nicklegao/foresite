/**
 * @author Sushant Verma
 * @date 8 Mar 2016
 */
package au.net.webdirector.admin.utils;

public class SecureJSPPath
{
	public static String iframePath(String s)
	{
		return path("iframe/" + s);
	}

	public static String path(String s)
	{
		return JSPPath.path("secure/" + s);
	}
}
