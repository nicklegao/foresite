/**
 * @author Sushant Verma
 * @date 8 Mar 2016
 */
package au.net.webdirector.admin.utils;

public class JSPPath
{
	public static String path(String s)
	{
		return "jsp/" + s;
	}
}
