package au.net.webdirector.admin.utils;

import java.io.File;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import au.net.webdirector.common.Defaults;

@Controller
public class HousekeepingController
{

	@RequestMapping("/deleteTempFiles")
	@ResponseBody
	public boolean deleteTemporaryFiles()
	{
		boolean success = false;
		try
		{
			Defaults d = Defaults.getInstance();
			String storeDir = d.getStoreDir();
			success = d.nuke(new File(storeDir + "/_tmp"), true);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return success;
	}

}
