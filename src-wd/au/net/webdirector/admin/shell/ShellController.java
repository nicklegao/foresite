/**
 * @author Sushant Verma
 * @date 7 Mar 2016
 */
package au.net.webdirector.admin.shell;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import au.net.webdirector.admin.security.LoginController;

@Controller
public class ShellController
{
	private static Logger logger = Logger.getLogger(ShellController.class);

	@RequestMapping(value = { "*" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET })
	public String getLogin(
			HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session) throws IOException
	{
		String userId = (String) session.getAttribute(LoginController.WD_USER_ID);
		if (StringUtils.isNotEmpty(userId))
		{
			logger.info("Already logged in... redirecting");
			response.sendRedirect("/webdirector/workbench");
			return null;
		}
		else
		{
			logger.info("Loading login");
			return "login";
		}
	}

	@RequestMapping(value = { "/workbench" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET })
	public String getWorkbench()
	{
		logger.info("Loading workbench");
		return "workbench";
	}

	@RequestMapping(value = { "/dashboard" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET })
	public String getDashboard()
	{
		logger.info("Loading dashboard");
		return "dashboard";
	}
}
