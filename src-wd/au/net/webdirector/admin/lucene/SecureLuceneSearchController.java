/**
 * @author Sushant Verma
 * @date 9 Mar 2016
 */
package au.net.webdirector.admin.lucene;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.net.webdirector.admin.utils.SecureJSPPath;

@Controller
@RequestMapping("/secure/lucene")
public class SecureLuceneSearchController
{
	@RequestMapping(value = "/iframe/rebuild", method = RequestMethod.GET)
	public String rebuild(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.iframePath("deleteSearchIndex");
	}

	@RequestMapping(value = "/iframe/rebuild", method = RequestMethod.POST)
	public String rebuildDo(
			HttpServletRequest request,
			HttpServletResponse response)
	{
		return SecureJSPPath.iframePath("deleteSearchIndex_h");
	}
}
