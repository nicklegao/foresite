package au.net.webdirector.admin.version;

import java.util.Date;

import au.net.webdirector.common.datalayer.base.db.DataMap;

public class VersionInfo
{
//	Version_ID, V_WHEN, V_WHO, V_ACTION, V_MODULE, V_TABLE, V_DATA_ID, V_VERSION_NO
	String sysId;
	Date when;
	String who;
	String action;
	String module;
	String tableType;
	String dataId;
	String versionNo;

	VersionInfo(DataMap data)
	{
		this.sysId = data.getString("Version_ID");
		this.when = data.getDate("V_WHEN");
		this.who = data.getString("V_WHO");
		this.action = data.getString("V_ACTION");
		this.module = data.getString("V_MODULE");
		this.tableType = data.getString("V_TABLE");
		this.dataId = data.getString("V_DATA_ID");
		this.versionNo = data.getString("V_VERSION_NO");
	}

	public String getSysId()
	{
		return sysId;
	}

	public Date getWhen()
	{
		return when;
	}

	public String getWho()
	{
		return who;
	}

	public String getAction()
	{
		return action;
	}

	public String getModule()
	{
		return module;
	}

	public String getTableType()
	{
		return tableType;
	}

	public String getDataId()
	{
		return dataId;
	}

	public String getVersionNo()
	{
		return versionNo;
	}


}
