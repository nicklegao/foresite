package au.net.webdirector.admin.version.customized.dms;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.net.webdirector.admin.version.VersionControlService;
import au.net.webdirector.admin.version.customized.VersionController;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;

import com.oreilly.servlet.MultipartRequest;

public class DynamicFormSave implements VersionController
{

	private static Logger logger = Logger.getLogger(DynamicFormSave.class);

	@Override
	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
		// do nothing
		return;
	}

	@Override
	public void archive(ResettableStreamHttpServletRequest request) throws Exception
	{
		if (request.getAttribute("_cURRENt_pROCESSEd_rESULt_") == null || !(boolean) request.getAttribute("_cURRENt_pROCESSEd_rESULt_"))
		{
			logger.info("Saving form failed, quit... ");
			return;
		}
		if (!StringUtils.equalsIgnoreCase("true", (String) request.getAttribute("VERSION_CONTROL")))
		{
			logger.info("No need to save version info, quit... ");
			return;
		}
		MultipartRequest multi = new MultipartRequest(request, Defaults.getInstance().getStoreDir() + "/_tmp", "UTF-8");
		String action = multi.getParameter("action");
		String table = multi.getParameter("table");
		String module = multi.getParameter("module");
		TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true));
		try
		{
			VersionControlService vcs = new VersionControlService(request, txManager);
			if (!vcs.isVersionControlEnabled(module))
			{
				logger.info(module + " is not configured for version control, quit... ");
				return;
			}
			logger.info("Checking version tables... ");
			vcs.checkVersionTable(module);
			String dataId = (String) request.getAttribute("_cURRENt_pROCESSEd_dATAId_");
			logger.info("Registering version info into system table ... ");
			String versionId = vcs.register(action, module, table, dataId);
			logger.info("Generating version copy ... ");
			vcs.copy(module, table, dataId, versionId);
		}
		catch (Exception e)
		{
			logger.error("Error", e);
			txManager.rollback();
		}
		finally
		{
			logger.info("Commiting ... ");
			txManager.commit();
			logger.info("Done");
		}
	}

	@Override
	public String[] getMappedUrls()
	{
		return new String[] { "/webdirector/form/save", "/webdirector/form/review" };
	}

}
