package au.net.webdirector.admin.version.customized.dms;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.net.webdirector.admin.bulkEdit.domain.BulkSaveSummary;
import au.net.webdirector.admin.version.VersionControlService;
import au.net.webdirector.admin.version.customized.VersionController;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.ModuleData;

public class BulkEditSave implements VersionController
{

	private static Logger logger = Logger.getLogger(BulkEditSave.class);

	@Override
	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
		// do nothing
		return;
	}

	@Override
	public void archive(ResettableStreamHttpServletRequest request) throws Exception
	{
		if (!StringUtils.equalsIgnoreCase("success", (String) request.getAttribute("BULK_EDIT_RESULT")))
		{
			logger.info("Saving bulk edit failed, quit... ");
			return;
		}
		if (!StringUtils.equalsIgnoreCase("true", (String) request.getAttribute("VERSION_CONTROL")))
		{
			logger.info("No need to save version info, quit... ");
			return;
		}

		TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true));
		try
		{
			BulkSaveSummary changeSummary = (BulkSaveSummary) request.getAttribute("BULK_EDIT_REQUEST");
			String module = changeSummary.getModuleName();
			String table = changeSummary.getDataType();

			VersionControlService vcs = new VersionControlService(request, txManager);
			if (!vcs.isVersionControlEnabled(module))
			{
				logger.info(module + " is not configured for version control, quit... ");
				return;
			}
			logger.info("Checking version tables... ");
			vcs.checkVersionTable(module);

			for (ModuleData updates : changeSummary.generateUpdateList())
			{
				String dataId = updates.getId();
				logger.info("Registering version info into system table ... ");
				String versionId = vcs.register("update", module, table, dataId);
				logger.info("Generating version copy ... ");
				vcs.copy(module, table, dataId, versionId);
			}

			for (String dataId : changeSummary.getDeletedRows())
			{
				logger.info("Registering version info into system table ... ");
				String versionId = vcs.register("delete", module, table, dataId);
			}

		}
		catch (Exception e)
		{
			logger.error("Error", e);
			txManager.rollback();
		}
		finally
		{
			logger.info("Commiting ... ");
			txManager.commit();
			logger.info("Done");
		}
	}

	@Override
	public String[] getMappedUrls()
	{
		return new String[] { "/webdirector/bulkEdit/save" };
	}

}
