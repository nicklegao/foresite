package au.net.webdirector.admin.version;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
import au.net.webdirector.common.datalayer.base.db.entity.TextFile;
import au.net.webdirector.common.datalayer.base.db.entity.version.VersionStoreFile;
import au.net.webdirector.common.datalayer.base.db.entity.version.VersionTextFile;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.EntityType;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;

public class TransactionVersionAccess extends TransactionModuleAccess
{

	protected static Logger logger = Logger.getLogger(TransactionModuleAccess.class);

	public static TransactionVersionAccess getInstance(TransactionManager txManager)
	{
		return new TransactionVersionAccess(txManager);
	}

	protected TransactionVersionAccess(TransactionManager txManager)
	{
		super(txManager);
	}

	protected String getNotesTable()
	{
		return "notes_ver";
	}

	protected String getMultiOptionsTable()
	{
		return "drop_down_multi_selections_ver";
	}

	@Override
	protected String getElementsTable(String module)
	{
		return "Elements_" + module + "_ver";
	}

	@Override
	protected String getCategoriesTable(String module)
	{
		return "Categories_" + module + "_ver";
	}

	@Override
	protected String getDataTable(String module, EntityType type)
	{
		return (EntityType.ELEMENT.equals(type) ? "elements_" : "categories_") + module + "_ver";
	}

	@Override
	protected StoreFile createStoreFileFromPath(String path)
	{
		return new VersionStoreFile(path);
	}

	@Override
	protected TextFile createTextFileFromPath(String path)
	{
		return new VersionTextFile(path);
	}

	@Override
	protected String getForiegnKeyOfNote()
	{
		return "version_id";
	}

	@Override
	protected String getPrimaryKeyOfModuleData()
	{
		return "version_id";
	}

	@Override
	protected String getForiegnKeyOfMultiDrop()
	{
		return "version_id";
	}
}
