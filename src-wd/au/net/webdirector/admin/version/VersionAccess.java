package au.net.webdirector.admin.version;


import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.client.ModuleAccess;

public class VersionAccess extends ModuleAccess
{

	protected static Logger logger = Logger.getLogger(ModuleAccess.class);

	public static VersionAccess getInstance()
	{
		return new VersionAccess();
	}

	@Override
	protected String getNotesTable()
	{
		return "notes_ver";
	}

	@Override
	protected String getMultiOptionsTable()
	{
		return "drop_down_multi_selections_ver";
	}

	@Override
	protected String getElementsTable(String module)
	{
		return "Elements_" + module + "_ver";
	}

	@Override
	protected String getCategoriesTable(String module)
	{
		return "Categories_" + module + "_ver";
	}

	@Override
	protected String getForiegnKeyOfNote()
	{
		return "version_id";
	}

	@Override
	protected String getPrimaryKeyOfModuleData()
	{
		return "version_id";
	}

	@Override
	protected String getForiegnKeyOfMultiDrop()
	{
		return "version_id";
	}
}
