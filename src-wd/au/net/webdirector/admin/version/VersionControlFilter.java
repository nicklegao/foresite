package au.net.webdirector.admin.version;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.system.classfinder.JavaClassFinder;
import au.net.webdirector.admin.version.customized.VersionController;

public class VersionControlFilter implements Filter
{

	private static Logger logger = Logger.getLogger(VersionControlFilter.class);

	private Map<String, Class<? extends VersionController>> controller = new HashMap<String, Class<? extends VersionController>>();

	public void init(FilterConfig arg0) throws ServletException
	{
		JavaClassFinder classFinder = new JavaClassFinder(VersionController.class);
		List<Class<? extends VersionController>> classes = classFinder.findAllMatchingTypes("au.net.webdirector.admin.version.customized");

		for (Class<? extends VersionController> clazz : classes)
		{
			try
			{
				VersionController t = clazz.newInstance();
				for (String url : t.getMappedUrls())
				{
					controller.put(url, clazz);
				}
			}
			catch (Exception e)
			{
				logger.info("Skip " + clazz.getCanonicalName());
			}
		}
	}

	public void destroy()
	{

	}

	private VersionController initVersionController(HttpServletRequest request)
	{
		String url = request.getRequestURI();
		for (Entry<String, Class<? extends VersionController>> e : controller.entrySet())
		{
			if (url.endsWith(e.getKey()))
			{
				try
				{
					return e.getValue().newInstance();
				}
				catch (Exception e1)
				{
					logger.error("Skip url = [" + url + "] trialer = [" + e.getValue().getCanonicalName() + "]");
				}
			}
		}

		return null;
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException
	{
		ResettableStreamHttpServletRequest request = new ResettableStreamHttpServletRequest((HttpServletRequest) req);
		VersionController controller = initVersionController((HttpServletRequest) req);
		try
		{
			if (controller != null)
			{
				controller.beforeProcess(request);
				request.resetInputStream();
			}
		}
		catch (Exception e)
		{
			logger.error("Error", e);
		}
		chain.doFilter(request, res);
		try
		{
			if (controller != null)
			{
				request.resetInputStream();
				controller.archive(request);
				request.resetInputStream();
			}
		}
		catch (Exception e)
		{
			logger.error("Error", e);
		}
	}

}
