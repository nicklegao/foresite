package au.net.webdirector.admin.version;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.admin.datalayer.client.AdminModuleHelper;
import au.net.webdirector.admin.datalayer.client.Difference;
import au.net.webdirector.admin.security.LoginController;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccess;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleData;
import au.net.webdirector.common.datalayer.client.ModuleHelper;

public class VersionControlService
{

	private static Logger logger = Logger.getLogger(VersionControlService.class);

	private TransactionManager txManager;
	private TransactionDataAccess da;
	private StoreFileAccess fa;

	private String userName;

	public VersionControlService(HttpServletRequest request)
	{
		this.txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(), StoreFileAccessFactory.getInstance());
		this.da = txManager.getDataAccess();
		this.fa = txManager.getFileAccess();
		HttpSession session = request.getSession();
		this.userName = session.getAttribute(LoginController.WD_USER) != null ? (String) session.getAttribute(LoginController.WD_USER) : "Unknown";
	}

	public VersionControlService(HttpServletRequest request, TransactionManager txManager)
	{
		this.txManager = txManager;
		this.da = txManager.getDataAccess();
		this.fa = txManager.getFileAccess();
		HttpSession session = request.getSession();
		this.userName = session.getAttribute(LoginController.WD_USER) != null ? (String) session.getAttribute(LoginController.WD_USER) : "Unknown";
	}

	private String getModule()
	{
		return null;
	}

	public boolean isVersionControlEnabled(String module)
	{
		try
		{
			return StringUtils.equals("1", da.selectString("select version_control_enabled from sys_moduleconfig where internal_module_name = ?", new String[] { module }));
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return false;
		}
	}

	public void checkVersionTable(String module)
	{
		prepareVersionTable("elements_" + module);
		prepareVersionTable("categories_" + module);
	}

	private void prepareVersionTable(String templateName)
	{
		try
		{
			da.selectString("select count(*) from " + templateName + "_ver where 1 != 1");
		}
		catch (SQLException e)
		{
			if (e.getErrorCode() == 1146)
			{
				logger.warn("Could not find version tables for " + templateName);
				// ALTER TABLE `wd10`.`elements_services_ver` MODIFY COLUMN
				// `Element_id` INT(10) NOT NULL,
				// ADD COLUMN `Version_ID` INTEGER(11),
				// ADD COLUMN `Record_ID` INTEGER(11) UNSIGNED NOT NULL
				// AUTO_INCREMENT ,
				// DROP PRIMARY KEY,
				// ADD PRIMARY KEY (`Record_ID`);
			}
		}
	}

	public String register(String action, String module, String table, String dataId) throws SQLException
	{
		DataMap data = new DataMap();
		// let trigger update when and version no
		data.put("v_who", userName);
		data.put("v_action", action);
		data.put("v_module", module);
		data.put("v_table", table);
		data.put("v_data_id", dataId);
		return String.valueOf(da.insert(data, "sys_version_rec"));
	}

	public void copy(String module, String tableType, String dataId, String versionId) throws SQLException, IOException
	{
		logger.info("=== copy main record");
		String idFieldName = StringUtils.equalsIgnoreCase("Elements", tableType) ? "Element_id" : "Category_id";
		da.update("insert into " + tableType + "_" + module + "_ver select " + versionId + ", e.* from " + tableType + "_" + module + " e where e." + idFieldName + " = ?", new String[] { dataId });
		logger.info("=== copy multiple selection");
		da.update("insert into drop_down_multi_selections_ver select *," + versionId + " from drop_down_multi_selections where ELEMENT_ID = ? and MODULE_NAME = ? and MODULE_TYPE = ?", new String[] {
				dataId, module, StringUtils.equalsIgnoreCase(tableType, "Elements") ? "element" : "category" });
		logger.info("=== copy notes");
		da.update("insert into notes_ver select *," + versionId + " from notes where moduleName=? and foriegnKey=? and elementOrCategory=?", new String[] { module, dataId, tableType });
		logger.info("=== copy stores");
		Defaults d = Defaults.getInstance();
		String srcPath = module + (StringUtils.equalsIgnoreCase("Elements", tableType) ? "/" : "/Categories/") + dataId;
		File srcDir = new File(d.getStoreDir(), srcPath);
		String verPath = "_VERSION_CONTROL/" + module + (StringUtils.equalsIgnoreCase("Elements", tableType) ? "/" : "/Categories/") + dataId + "/" + versionId;
		if (fa.exists(srcDir))
		{
			fa.copy(srcDir, new File(d.getStoreDir(), verPath));
		}
		logger.info("=== done");
	}

	public boolean hasVersion(String module, String tableType, String dataId) throws SQLException
	{
		return !"0".equals(da.selectString("select count(1) from sys_version_rec where v_module = ? and v_table = ? and v_data_id = ?", new String[] { module, tableType, dataId }));
	}

	public void revert(String module, String tableType, String versionId) throws SQLException, IOException
	{
		DataMap version = da.selectMap("select V_VERSION_NO,V_DATA_ID from sys_version_rec where V_MODULE = ? and V_TABLE = ? and Version_ID = ?", new String[] { module, tableType, versionId });
		String action = "Revert From Ver:" + version.getString("V_VERSION_NO");
		TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true));
		try
		{
			if (!isVersionControlEnabled(module))
			{
				logger.info(module + " is not configured for version control, quit... ");
				return;
			}
			String dataId = version.getString("V_DATA_ID");
			logger.info("Registering version info into system table ... ");
			String newId = register(action, module, tableType, dataId);

			logger.info("Reverting data ... ");
			updateFromHistory(module, tableType, dataId, versionId);
			logger.info("Reverting finished.");

			logger.info("Generating version copy ... ");
			copy(module, tableType, dataId, newId);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			txManager.rollback();
		}
		finally
		{
			logger.info("Commiting ... ");
			txManager.commit();
			logger.info("Done");
		}
	}

	private void updateFromHistory(String module, String tableType, String dataId, String versionId) throws SQLException, IOException
	{
		String tableName = tableType + "_" + module;
		List<String> mainColumns = ModuleHelper.getInstance(txManager).getDBColumns(tableType, module);
		logger.info("=== copy main record");
		String idFieldName = StringUtils.equalsIgnoreCase("Elements", tableType) ? "Element_id" : "Category_id";
		da.update("delete from " + tableName + " where " + idFieldName + " = ?", new String[] { dataId });
		da.update("insert into " + tableName + " (" + StringUtils.join(mainColumns, ",") + ") select " + StringUtils.join(mainColumns, ",") + " from " + tableName + "_ver where version_id = ?",
				new String[] { versionId });
		logger.info("=== copy multiple selection");
		da.update("delete from drop_down_multi_selections where MODULE_NAME = ? and MODULE_TYPE = ? and ELEMENT_ID = ?", new String[] { module,
				StringUtils.equalsIgnoreCase(tableType, "Elements") ? "element" : "category", dataId });
		da.update(
				"insert into drop_down_multi_selections (SELECTED_VALUE, ELEMENT_ID, ATTRMULTI_NAME, MODULE_NAME, MODULE_TYPE) select SELECTED_VALUE, ELEMENT_ID, ATTRMULTI_NAME, MODULE_NAME, MODULE_TYPE from drop_down_multi_selections_ver where VERSION_ID = ?",
				new String[] { versionId });
		logger.info("=== copy notes");
		da.update("delete from notes where moduleName = ? and elementOrCategory = ? and foriegnKey = ?", new String[] { module, tableType, dataId });
		da.update(
				"insert into notes (id, moduleName, foriegnKey, elementOrCategory, createDate, note, who, followUpDate, fieldName, content) select id, moduleName, foriegnKey, elementOrCategory, createDate, note, who, followUpDate, fieldName, content from notes_ver where version_id=?",
				new String[] { versionId });
		logger.info("=== copy stores");
		Defaults d = Defaults.getInstance();
		String dataPath = module + (StringUtils.equalsIgnoreCase("Elements", tableType) ? "/" : "/Categories/") + dataId;
		File dataDir = new File(d.getStoreDir(), dataPath);
		String verPath = "_VERSION_CONTROL/" + module + (StringUtils.equalsIgnoreCase("Elements", tableType) ? "/" : "/Categories/") + dataId + "/" + versionId;
		File verDir = new File(d.getStoreDir(), verPath);
		if (fa.exists(verDir))
		{
			fa.copy(verDir, dataDir);
		}
		logger.info("=== done");
	}

	public List<VersionInfo> getVersionList(String module, String tableType, String dataId) throws SQLException
	{
		List<VersionInfo> versions = new ArrayList<VersionInfo>();
		List<DataMap> result = da.selectMaps("select * from sys_version_rec where v_module = ? and v_table = ? and v_data_id = ? order by v_version_no desc",
				new String[] { module, tableType, dataId });
		for (DataMap version : result)
		{
			versions.add(new VersionInfo(version));
		}
		return versions;
	}

	public List<Difference> compare(String module, String tableType, String versionId1, String versionId2) throws SQLException
	{
		boolean isElement = StringUtils.equalsIgnoreCase("Elements", tableType);
		VersionAccess va = VersionAccess.getInstance();
		ModuleData conditions = isElement ? new ElementData() : new CategoryData();
		conditions.put("version_id", versionId1);
		ModuleData data1 = isElement ? va.getElement(module, (ElementData) conditions) : va.getCategory(module, (CategoryData) conditions);
		conditions.put("version_id", versionId2);
		ModuleData data2 = isElement ? va.getElement(module, (ElementData) conditions) : va.getCategory(module, (CategoryData) conditions);
		return AdminModuleHelper.getInstance().compare(module, tableType, data1, data2);

	}
}
