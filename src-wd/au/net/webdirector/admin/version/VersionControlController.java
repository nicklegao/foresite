package au.net.webdirector.admin.version;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.net.webdirector.admin.datalayer.client.Difference;

@Controller
@RequestMapping("/versionControl")
public class VersionControlController
{

	@RequestMapping("/compare")
	@ResponseBody
	public List<Difference> compareVersions(HttpServletRequest request,
			@RequestParam String module,
			@RequestParam String tableType,
			@RequestParam String versionId1,
			@RequestParam String versionId2)
	{

		List<Difference> diff = null;
		try
		{
			VersionControlService svc = new VersionControlService(request);
			diff = svc.compare(module, tableType, versionId1, versionId2);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return diff;
	}

	@RequestMapping("/revert")
	@ResponseBody
	public void revert(HttpServletRequest request,
			@RequestParam String module,
			@RequestParam String tableType,
			@RequestParam String versionId)
	{
		try
		{
			VersionControlService svc = new VersionControlService(request);
			svc.revert(module, tableType, versionId);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/history")
	public String history()
	{
		return "jsp/versionHistory";
	}
}
