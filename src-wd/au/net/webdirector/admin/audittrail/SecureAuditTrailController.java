/**
 * @author Sushant Verma
 * @date 8 Mar 2016
 */
package au.net.webdirector.admin.audittrail;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import au.net.webdirector.admin.utils.SecureJSPPath;

@Controller
@RequestMapping("/secure/audittrail/")
public class SecureAuditTrailController
{
	private static Logger logger = Logger.getLogger(SecureAuditTrailController.class);


	@RequestMapping(value = "/iframe/report")
	public String viewReport(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.iframePath("auditTrailReport");
	}
}
