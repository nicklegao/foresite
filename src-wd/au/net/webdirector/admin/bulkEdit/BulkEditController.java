package au.net.webdirector.admin.bulkEdit;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.net.webdirector.admin.bulkEdit.domain.BulkEditSummary;
import au.net.webdirector.admin.bulkEdit.domain.BulkSaveResponse;
import au.net.webdirector.admin.bulkEdit.domain.BulkSaveSummary;
import au.net.webdirector.admin.bulkEdit.domain.datatable.Column;
import au.net.webdirector.admin.bulkEdit.domain.datatable.DataConverter;
import au.net.webdirector.admin.bulkEdit.domain.datatable.DatatableProcessingRequest;
import au.net.webdirector.admin.bulkEdit.domain.datatable.DatatableProcessingResponse;
import au.net.webdirector.admin.datalayer.client.AdminModuleHelper;
import au.net.webdirector.admin.modules.privileges.CategoryPrivileges;
import au.net.webdirector.admin.modules.privileges.PrivilegeUtils;
import au.net.webdirector.admin.security.LoginController;
import au.net.webdirector.admin.workflow.WorkflowHelper;
import au.net.webdirector.admin.workflow.WorkflowInfo;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.ModuleData;
import au.net.webdirector.common.datalayer.client.ModuleHelper;
import au.net.webdirector.common.datalayer.client.module.ModuleAttributes;

@Controller
public class BulkEditController
{
	private static Logger logger = Logger.getLogger(BulkEditController.class);


	@RequestMapping("/bulkEdit")
	public String getBulkEditor(
			HttpServletRequest request,
			@RequestParam(value = "tablePrefix") String module,
			@RequestParam String widgetType,
			@RequestParam(value = "ID") String id,
			@RequestParam(required = false) String show)
	{
		boolean recursive = false;
		int level = 0;
		int moduleLevel = ModuleHelper.getInstance().getModuleLevels(module);
		int showLevel = StringUtils.isBlank(show) || StringUtils.equalsIgnoreCase(show, "e") ? -1 : Integer.parseInt(StringUtils.substring(show, 1));
		if (!id.equals("0"))
		{
			ModuleAccess ma = ModuleAccess.getInstance();
			CategoryData cd = null;
			try
			{
				cd = ma.getCategoryById(module, id);
				level = cd.getFolderLevel();
			}
			catch (Exception e)
			{
				logger.fatal("Oh NO!", e);
			}
			if (cd == null)
			{
				return "jsp/widgetError";
			}
		}

		request.setAttribute("module", module);
		recursive = (StringUtils.equalsIgnoreCase("categoryList", widgetType) && level < showLevel - 1) || (StringUtils.equalsIgnoreCase("elementList", widgetType) && level < moduleLevel);
		request.setAttribute("recursive", recursive);
		return "jsp/bulkEdit";
	}

	/**
	 * Note there are missing paramters. Be sure to read docs.
	 * 
	 * @see https://datatables.net/manual/server-side
	 * @param session
	 * @param response
	 * @param draw
	 * @param length
	 * @return
	 */
	@RequestMapping("/bulkEdit/data")
	@ResponseBody
	public DatatableProcessingResponse<DataMap> getBulkEditData(HttpSession session,
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam String type,
			@RequestParam String dataId,
			@RequestParam String module,
			@RequestParam(required = false, value = "all") boolean recursive,
			@RequestParam(required = false) String show)
	{
		int showLevel = StringUtils.isBlank(show) || StringUtils.equalsIgnoreCase(show, "e") ? -1 : Integer.parseInt(StringUtils.substring(show, 1));
		DatatableProcessingRequest searchRequest = new DatatableProcessingRequest(request);

		DatatableProcessingResponse<DataMap> tableResponse = new DatatableProcessingResponse<DataMap>();
		tableResponse.setDraw(searchRequest.draw);

		String tablePrefix = "";
		AdminModuleHelper moduleHelper = AdminModuleHelper.getInstance();
		Set<String> filteredIDs = null;
		PrivilegeUtils privilegeUtils = PrivilegeUtils.getCachedInstance(module, session, false);
		boolean isElements;
		boolean columnFilteresApplied = areFiltersApplied(searchRequest.columns);

		if (type.equalsIgnoreCase("elementList"))
		{
			tablePrefix = "elements";
			isElements = true;
			filteredIDs = moduleHelper.filterElements(privilegeUtils, module, dataId, searchRequest.search.value);

			//UPDATE TOTALS
			int total = moduleHelper.countElements(module, dataId, null, null);
			tableResponse.setRecordsTotal(total);
			tableResponse.setRecordsFiltered(total);

			if (columnFilteresApplied || filteredIDs != null)
			{
				if (columnFilteresApplied || filteredIDs.size() > 0)
				{
					tableResponse.setRecordsFiltered(moduleHelper.countElements(module, dataId, filteredIDs, searchRequest.columns));
				}
				else if (filteredIDs.size() == 0)
				{
					//(filteredIDs.size() == 0)
					//Dont bother searching
					tableResponse.setRecordsFiltered(0);
				}
			}
		}
		else if (type.equalsIgnoreCase("categoryList"))
		{
			tablePrefix = "categories";
			isElements = false;

			filteredIDs = moduleHelper.filterCategories(privilegeUtils, module, dataId, searchRequest.search.value);

			int total = moduleHelper.countCategories(module, dataId, "", privilegeUtils, CategoryPrivileges.READ, showLevel, null);
			tableResponse.setRecordsTotal(total);
			tableResponse.setRecordsFiltered(total);
			if (columnFilteresApplied || !StringUtils.isEmpty(searchRequest.search.value))
			{
				tableResponse.setRecordsFiltered(moduleHelper.countCategories(module, dataId, searchRequest.search.value, privilegeUtils, CategoryPrivileges.READ, showLevel, searchRequest.columns));
			}
		}
		else
		{
			response.setStatus(400);// Invalid data type requested
			return new DatatableProcessingResponse<DataMap>();
		}

		List<String> internalNames = new LinkedList<String>();
		for (Column col : searchRequest.columns)
		{
			internalNames.add(col.data);
		}

		List<? extends ModuleData> data = moduleHelper.getDataPage(
				module,
				tablePrefix,
				dataId,
				internalNames,
				searchRequest.columns[searchRequest.order[0].column].data,
				searchRequest.order[0].dir,
				searchRequest.length,
				searchRequest.start,
				filteredIDs,
				showLevel,
				searchRequest.columns);

		DataConverter dataConverter = DataConverter.getInstance(module);

		List<DataMap> transformedData = new LinkedList<DataMap>();

		CategoryPrivileges priv = null;
		WorkflowHelper wh = WorkflowHelper.getInstance();
		Map<String, WorkflowInfo> workflowData;
		if (isElements)
		{
			priv = privilegeUtils.getPrivilegeForCategory(dataId);//Parent category permissions
			workflowData = wh.getAllStatus(module, "Elements");
		}
		else
		{
			workflowData = wh.getAllStatus(module, "Categories");
		}
		if (null != data)
		{
			for (ModuleData dbData : data)
			{
				DataMap dm = new DataMap();
				for (Column col : searchRequest.columns)
				{
					Object val = dataConverter.transformTypeForDatatable(col.data, dbData);
					dm.put(col.data, val);
				}
				dm.put("parent_id", dbData.getParentId());
				if (!isElements)
				{
					priv = privilegeUtils.getPrivilegeForCategory(dbData.getId());
				}
				else
				{
					priv = privilegeUtils.getPrivilegeForCategory(dbData.getParentId());
				}
				dm.put("privlage", priv.getSum());
				if (workflowData.containsKey(dbData.getId()))
				{
					dm.put("workflow", workflowData.get(dbData.getId()).getStatus());
				}
				else
				{
					dm.put("workflow", "live");
				}
				transformedData.add(dm);
			}
		}

		tableResponse.setData(transformedData);

		return tableResponse;
	}

	@RequestMapping("/bulkEdit/columns")
	@ResponseBody
	public BulkEditSummary getBulkEditColumns(HttpSession session,
			HttpServletResponse response,
			@RequestParam String module,
			@RequestParam String type,
			@RequestParam String id)
	{
		try
		{
			PrivilegeUtils privilegeUtils = PrivilegeUtils.getCachedInstance(module, session, false);
			ModuleHelper moduleHelper = ModuleHelper.getInstance();
			CategoryData categoryData = ModuleAccess.getInstance().getCategoryById(module, id);
			String parentID = categoryData != null ? categoryData.getParentId() : null;
			CategoryPrivileges privilages = privilegeUtils.getPrivilegeForCategory(parentID);

			if (type.equalsIgnoreCase("elementList"))
			{
				List<ModuleAttributes> allColumns = moduleHelper.getElementColumnsInfo(module);
//				int totalElements = moduleHelper.countElements(module, id, filterQuery);

				return new BulkEditSummary(allColumns, parentID, privilages);
			}
			else if (type.equalsIgnoreCase("categoryList"))
			{
				String userID = (String) session.getAttribute(LoginController.WD_USER_ID);
				int categoryLevel = Integer.parseInt(moduleHelper.getFolderLevelForCategory(module, id));

				int depth = moduleHelper.getModuleLevels(module);
				List<ModuleAttributes> allColumns = moduleHelper.getCategoryColumnsInfo(module, String.valueOf(categoryLevel + 1));
//				int totalCategories = moduleHelper.countCategories(module, id, filterQuery, userID, CategoryPrivileges.R);

				return new BulkEditSummary(allColumns,
						depth,
						categoryLevel,
						parentID,
						privilages);
			}
			else
			{
				response.setStatus(400);// Invalid data type requested
				return BulkEditSummary.None();
			}
		}
		catch (Exception e)
		{
			logger.fatal(e);
		}

		response.setStatus(418);//im a teapot... make me a sandwich!
		return BulkEditSummary.None();
	}

	@RequestMapping("/bulkEdit/save")
	@ResponseBody
	public BulkSaveResponse saveBulkEditData(HttpSession session,
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestBody BulkSaveSummary changeSummary)
	{
		TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true));
		String userId = (String) session.getAttribute(LoginController.WD_USER_ID);
		boolean privilegeIssues = changeSummary.filterWithPermissions(session);
		boolean isWorkflowEnabled = false;//assume

		try
		{
			String module = changeSummary.getModuleName();
			List<ModuleData> deletedRows = changeSummary.generateDeleteList();
			List<ModuleData> changedRows = changeSummary.generateUpdateList();

			WorkflowHelper workflowHelper = WorkflowHelper.getInstance(txManager);

			boolean moduleWorkflowEnabled = workflowHelper.isWorkflowEnabled(changeSummary.getModuleName());
			boolean isUserNeedApproval = workflowHelper.isUserNeedApproval(changeSummary.getModuleName(), userId);

			isWorkflowEnabled = moduleWorkflowEnabled && isUserNeedApproval;
			for (ModuleData change : changedRows)
			{
				if (isWorkflowEnabled)
				{
					workflowHelper.saveDataToWorkflow(changeSummary.getModuleName(), change, WorkflowHelper.ACTION_UPDATE, WorkflowInfo.DRAFT, userId);
				}
				else
				{
					workflowHelper.saveDataToLive(changeSummary.getModuleName(), change, WorkflowHelper.ACTION_UPDATE);
				}

			}
			for (ModuleData deleted : deletedRows)
			{
				String tableType = deleted.isElement() ? "Elements" : "Categories";
				boolean hasWorkflow = workflowHelper.getStatus(module, tableType, deleted.getId()) != null;
				if (isWorkflowEnabled)
				{
					if (hasWorkflow)
					{
						workflowHelper.deleteDraftByDataId(module, tableType, deleted.getId());
						workflowHelper.markAsDeleted(changeSummary.getModuleName(), tableType, deleted.getId(), userId);
					}
					else
					{
						workflowHelper.markAsDeleted(changeSummary.getModuleName(), tableType, deleted.getId(), userId);
					}
				}
				else
				{
					if (!hasWorkflow)
					{
						workflowHelper.deleteLiveByDataId(changeSummary.getModuleName(), tableType, deleted.getId());
					}
					else
					{
						workflowHelper.deleteDraftByDataId(changeSummary.getModuleName(), tableType, deleted.getId());
						workflowHelper.deleteLiveByDataId(changeSummary.getModuleName(), tableType, deleted.getId());
					}

				}
			}
			txManager.commit();
		}
		catch (Exception e)
		{
			logger.fatal("Unable to perform bulk update.", e);

			txManager.rollback();
			return BulkSaveResponse.failed(e.getClass().getCanonicalName() + " : " + e.getMessage());
		}

		request.setAttribute("BULK_EDIT_REQUEST", changeSummary);
		request.setAttribute("BULK_EDIT_RESULT", "success");
		request.setAttribute("VERSION_CONTROL", isWorkflowEnabled ? "false" : "true");

		if (privilegeIssues)
		{
			return BulkSaveResponse.successWithWarning("Save possibly completed successfully however due to unsufficient privilages some data could not be changed. Please contact support for additional assistance.");
		}

		return BulkSaveResponse.success();
	}

	private boolean areFiltersApplied(Column[] colums)
	{
		if (null != colums)
		{
			for (Column c : colums)
			{
				if (StringUtils.isNotBlank(c.search.value))
				{
					return true;
				}
			}
		}
		return false;
	}

	private List<String> orderedSummaryForModuleData(List<DataMap> allColumns,
			ModuleData individualElement)
	{
		ModuleHelper moduleHelper = ModuleHelper.getInstance();
		List<String> minifiedSummary = new LinkedList<String>();

		for (DataMap columnMetadata : allColumns)
		{
			String internalColumnName = columnMetadata.getString("internal");
			String renderValue = individualElement.getString(internalColumnName);
			if (moduleHelper.columnIsDate(internalColumnName))
			{
				Date d = individualElement.getDate(internalColumnName);
				if (d != null)
				{
					renderValue = Long.toString(d.getTime());
				}
			}
			else if (moduleHelper.columnIsFile(internalColumnName))
			{
				String s = individualElement.getString(internalColumnName);
				if (s != null && s.length() > 0)
				{
					renderValue = "/" + Defaults.getInstance().getStoreContext() + s;
				}
			}
			minifiedSummary.add(renderValue);
		}

		return minifiedSummary;
	}

}
