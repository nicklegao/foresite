package au.net.webdirector.admin.bulkEdit.domain.datatable;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import au.net.webdirector.common.utils.DeepObjectParamParser;

public class Order
{

	public final int column;
	public final String dir;

	public Order(HttpServletRequest request, int index)
	{
		this.column = DeepObjectParamParser.readInt(request, "order", index, "column");
		this.dir = DeepObjectParamParser.readString(request, "order", index, "dir");
	}

	public static Order[] readArray(HttpServletRequest request)
	{
		List<Order> ret = new LinkedList<Order>();
		int count = DeepObjectParamParser.countArray(request, "order", "column");
		for (int i = 0; i < count; ++i)
		{
			ret.add(new Order(request, i));
		}
		return ret.toArray(new Order[0]);
	}
}
