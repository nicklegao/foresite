package au.net.webdirector.admin.bulkEdit.domain.datatable;

import java.util.List;

public class DatatableProcessingResponse<T>
{
	private int draw;
	private int recordsTotal;
	private int recordsFiltered;
	private List<T> data;

	public DatatableProcessingResponse()
	{
		// TODO Auto-generated constructor stub
	}

	public DatatableProcessingResponse(int draw, int total, int filtered, List<T> data)
	{
		this.draw = draw;
		this.recordsTotal = total;
		this.recordsFiltered = filtered;
		this.data = data;
	}

	public int getDraw()
	{
		return draw;
	}

	public void setDraw(int draw)
	{
		this.draw = draw;
	}

	public int getRecordsTotal()
	{
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal)
	{
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered()
	{
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered)
	{
		this.recordsFiltered = recordsFiltered;
	}

	public List<T> getData()
	{
		return data;
	}

	public void setData(List<T> data)
	{
		this.data = data;
	}
}
