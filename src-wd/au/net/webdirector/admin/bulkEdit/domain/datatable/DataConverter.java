/**
 * @author Sushant Verma
 * @date 19 Mar 2015
 */
package au.net.webdirector.admin.bulkEdit.domain.datatable;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.entity.MultiOptions;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.ModuleData;

/**
 * 
 */
public class DataConverter
{
	private final String STORE_FILE_PREFIX;

	private static Logger logger = Logger.getLogger(DataConverter.class);

	private ModuleAccess moduleAccess;

	private String module;

	public static DataConverter getInstance(String module)
	{
		return new DataConverter(module);
	}

	private DataConverter(String module)
	{
		STORE_FILE_PREFIX = "/" + Defaults.getInstance().getStoreContext() + "/";
		moduleAccess = ModuleAccess.getInstance();

		this.module = module;
	}

	/**
	 * @param data
	 * @param values
	 * @return
	 */
	public Object transformTypeForDatatable(String dataType, ModuleData values)
	{
		dataType = dataType.toLowerCase();
		if (dataType.equals("live_date")
				|| dataType.equals("expire_date")
				|| dataType.startsWith("attrdate_"))
		{
			Date date = values.getDate(dataType);
			if (date != null)
			{
				return Long.toString(date.getTime());
			}
		}
		else if (dataType.startsWith("attrtext_")
				|| dataType.startsWith("attrlong_")
				|| dataType.startsWith("attrfile_"))
		{
			String val = values.getString(dataType);
			if (val != null && val.length() > 0)
			{
				return STORE_FILE_PREFIX + val;
			}
		}
		else if (dataType.startsWith("attrmulti_"))
		{
			try
			{
				MultiOptions multiOptions = moduleAccess.getMultiOptions(values, dataType, module);
				return multiOptions.getValues();
			}
			catch (SQLException e)
			{
				logger.error("Error:", e);
			}
			return new String[] {};
		}
		return values.getString(dataType);
	}

	/**
	 * 
	 * @param id
	 * @param isElement
	 * @param dataType
	 * @param value
	 * @return
	 */
	public Object transformTypeFromDatatable(String id, boolean isElement, String dataType, Object value)
	{
		dataType = dataType.toLowerCase();
		if (dataType.equals("live_date")
				|| dataType.equals("expire_date")
				|| dataType.startsWith("attrdate_"))
		{
			Date date = new Date(Long.parseLong((String) value));
			return date;
		}
		else if (dataType.startsWith("attrmulti_"))
		{
			MultiOptions options = new MultiOptions(
					id,
					dataType,
					module,
					isElement ? ElementData.TYPE : CategoryData.TYPE,
					(List<String>) value);

			return options;
		}

		return value;
	}
}
