package au.net.webdirector.admin.bulkEdit.domain.datatable;

import javax.servlet.http.HttpServletRequest;

import au.net.webdirector.common.utils.DeepObjectParamParser;

public class Search
{

	public final String value;
	public final String regex;

	public Search(HttpServletRequest request, int columnNumber)
	{
		this.value = DeepObjectParamParser.readString(request, "columns", columnNumber, "search", "value");
		this.regex = DeepObjectParamParser.readString(request, "columns", columnNumber, "search", "regex");
	}

	public Search(HttpServletRequest request)
	{
		this.value = DeepObjectParamParser.readString(request, "search", "value");
		this.regex = DeepObjectParamParser.readString(request, "search", "regex");
	}
}
