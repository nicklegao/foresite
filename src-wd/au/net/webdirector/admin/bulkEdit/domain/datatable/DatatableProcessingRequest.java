package au.net.webdirector.admin.bulkEdit.domain.datatable;

import javax.servlet.http.HttpServletRequest;

import au.net.webdirector.common.utils.DeepObjectParamParser;

/**
 * @see https://datatables.net/manual/server-side
 * @author Sushant Verma
 *
 */
public class DatatableProcessingRequest
{
	public int draw;//ignore but dont discard

	public int length;
	public int start;

	public Search search;
	public Column[] columns;
	public Order[] order;

	public DatatableProcessingRequest()
	{
	}

	public DatatableProcessingRequest(HttpServletRequest request)
	{
		this.draw = DeepObjectParamParser.readInt(request, "draw");
		this.start = DeepObjectParamParser.readInt(request, "start");
		this.length = DeepObjectParamParser.readInt(request, "length");

		this.search = new Search(request);
		this.columns = Column.readArray(request);
		this.order = Order.readArray(request);
	}
}
