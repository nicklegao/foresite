package au.net.webdirector.admin.bulkEdit.domain.datatable;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import au.net.webdirector.common.utils.DeepObjectParamParser;

public class Column
{

	public final String data;
	public final String name;
	public final Search search;

	public Column(HttpServletRequest request, int index)
	{
		this.data = DeepObjectParamParser.readString(request, "columns", index, "data");
		this.name = DeepObjectParamParser.readString(request, "columns", index, "name");
		this.search = new Search(request, index);
	}

	public static Column[] readArray(HttpServletRequest request)
	{
		List<Column> ret = new LinkedList<Column>();
		int count = DeepObjectParamParser.countArray(request, "columns", "data");
		for (int i = 0; i < count; ++i)
		{
			ret.add(new Column(request, i));
		}
		return ret.toArray(new Column[0]);
	}
}
