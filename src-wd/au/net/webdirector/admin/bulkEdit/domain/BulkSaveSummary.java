package au.net.webdirector.admin.bulkEdit.domain;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import au.net.webdirector.admin.bulkEdit.domain.datatable.DataConverter;
import au.net.webdirector.admin.modules.privileges.CategoryPrivileges;
import au.net.webdirector.admin.modules.privileges.PrivilegeUtils;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleData;

public class BulkSaveSummary
{
	private static Logger logger = Logger.getLogger(BulkSaveSummary.class);

	List<String> deletedRows;
	List<String> modifiedColumns;
	List<List<Object>> modifiedRows;
	int modifiedCells;

	String moduleName;
	String dataType;

	public List<ModuleData> generateDeleteList()
	{
		List<ModuleData> deletes = new LinkedList<ModuleData>();

		boolean isElements = dataType.equalsIgnoreCase("elements");
		for (String deleteID : deletedRows)
		{
			if (isElements)
			{
				ElementData data = new ElementData();
				data.setId(deleteID);
				deletes.add(data);
			}
			else
			{
				CategoryData data = new CategoryData();
				data.setId(deleteID);
				deletes.add(data);
			}

		}

		return deletes;
	}

	public List<ModuleData> generateUpdateList()
	{
		List<String> modifiedColumns = getModifiedColumns();
		List<ModuleData> changes = new LinkedList<ModuleData>();
		DataConverter dc = DataConverter.getInstance(moduleName);
		for (List<Object> row : getModifiedRows())
		{
			if (row.size() != modifiedColumns.size())
				logger.fatal("Incorrect number of columns for row: " + row);

			ModuleData delta;
			if (dataType.equalsIgnoreCase("elements"))
			{
				delta = new ElementData();
			}
			else
			{
				delta = new CategoryData();
			}
			for (int i = 0; i < modifiedColumns.size(); i++)
			{
				String t = modifiedColumns.get(i);
				Object valObj = row.get(i);
				Object transformedValue = dc.transformTypeFromDatatable(
						delta.getId(),
						dataType.equalsIgnoreCase("elements"),
						t,
						valObj);
				delta.put(t, transformedValue);
			}
			changes.add(delta);
		}

		return changes;
	}

	/**
	 * @return
	 */
	public Map<String, List<List<String>>> generateUpdateMapForMultiselect()
	{
		List<String> modifiedColumns = getModifiedColumns();
		Map<String, List<List<String>>> changes = new HashMap<String, List<List<String>>>();
		for (List<Object> row : getModifiedRows())
		{
			if (row.size() != modifiedColumns.size())
				logger.fatal("Incorrect number of columns for row: " + row);

			String id = "";
			for (int i = 0; i < modifiedColumns.size(); i++)
			{
				String type = modifiedColumns.get(i);
				Object valObj = row.get(i);

				if (valObj instanceof String)
				{
					if (type.equalsIgnoreCase("element_id")
							|| type.equalsIgnoreCase("category_id"))
					{
						id = (String) valObj;
						changes.put(id, new LinkedList<List<String>>());
					}
				}
				else if (valObj instanceof List)
				{
					List<List<String>> multiselects = changes.get(id);
					multiselects.add((List) valObj);
				}
			}
		}

		return changes;
	}

	/**
	 * @param userId
	 * @return
	 */
	public boolean filterWithPermissions(HttpSession session)
	{
		boolean isElement = "elements".equalsIgnoreCase(dataType);
		PrivilegeUtils privilegeUtils = PrivilegeUtils.getCachedInstance(moduleName, session, false);
		boolean privilegeIssues = false;

		//Find column index of the ID column so we can read the changed data.
		int idColumnIndex = 0;
		for (int i = 0; i < modifiedColumns.size(); i++)
		{
			String columnName = modifiedColumns.get(i);
			if (columnName.equalsIgnoreCase("category_id")
					|| columnName.equalsIgnoreCase("element_id"))
			{
				idColumnIndex = i;
				break;
			}
		}

		//Check permissions
		if (isElement)
		{
			String anyElementID;
			if (deletedRows.size() > 0)
			{
				anyElementID = deletedRows.get(0);
			}
			else
			{
				anyElementID = (String) modifiedRows.get(0).get(idColumnIndex);
			}
			CategoryPrivileges elementFamilyPrivilege = privilegeUtils.getPrivilegeForElement(anyElementID);

			if (!elementFamilyPrivilege.isDelete() && deletedRows.size() > 0)
			{
				//Not allowed to delete elements... remove all items
				privilegeIssues = true;
				deletedRows.clear();
			}

			if (!elementFamilyPrivilege.isEdit() && modifiedRows.size() > 0)
			{
				//Not allowed to edit elements... remove all items
				privilegeIssues = true;
				modifiedRows.clear();
			}
		}
		else
		{
			List<String> cantDelete = new LinkedList<String>();
			for (String deleteID : deletedRows)
			{
				if (privilegeUtils.getPrivilegeForCategory(deleteID).isDelete() == false)
				{
					//Not allowed to delete this category
					privilegeIssues = true;
					cantDelete.add(deleteID);
				}
			}

			List<List<Object>> cantEdit = new LinkedList<List<Object>>();
			for (List<Object> change : modifiedRows)
			{
				String updateID = (String) change.get(idColumnIndex);
				if (privilegeUtils.getPrivilegeForCategory(updateID).isEdit() == false)
				{
					//Not allowed to edit this category
					privilegeIssues = true;
					cantEdit.add(change);
				}
			}
			modifiedRows.removeAll(cantEdit);
		}

		return privilegeIssues;
	}

	public List<String> getDeletedRows()
	{
		return deletedRows;
	}

	public void setDeletedRows(List<String> deletedRows)
	{
		this.deletedRows = deletedRows;
	}

	public List<String> getModifiedColumns()
	{
		return modifiedColumns;
	}

	public void setModifiedColumns(List<String> modifiedColumns)
	{
		this.modifiedColumns = modifiedColumns;
	}

	public List<List<Object>> getModifiedRows()
	{
		return modifiedRows;
	}

	public void setModifiedRows(List<List<Object>> modifiedRows)
	{
		this.modifiedRows = modifiedRows;
	}

	public int getModifiedCells()
	{
		return modifiedCells;
	}

	public void setModifiedCells(int modifiedCells)
	{
		this.modifiedCells = modifiedCells;
	}

	public String getModuleName()
	{
		return moduleName;
	}

	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}

	public String getDataType()
	{
		return dataType;
	}

	public void setDataType(String dataType)
	{
		this.dataType = dataType;
	}
}
