package au.net.webdirector.admin.bulkEdit.domain;

import java.util.List;

import au.net.webdirector.admin.modules.privileges.CategoryPrivileges;
import au.net.webdirector.admin.modules.privileges.LazyCategoryPrivileges;
import au.net.webdirector.common.datalayer.client.module.ModuleAttributes;

public class BulkEditSummary
{
	private List<ModuleAttributes> columns;
	private int depth;
	private int level;
	private String parentID;
	private boolean isInsert;

	public BulkEditSummary(List<ModuleAttributes> allColumns)
	{
		this(allColumns, -1, -1, null, new LazyCategoryPrivileges(0));
	}

	public BulkEditSummary(List<ModuleAttributes> allColumns, String parentID, CategoryPrivileges privilages)
	{
		this(allColumns, -1, -1, parentID, privilages);
	}

	public BulkEditSummary(List<ModuleAttributes> allColumns, int depth, int level, String parentID, CategoryPrivileges privilages)
	{
		this.columns = allColumns;
		this.depth = depth;
		this.level = level;
		this.parentID = parentID;

		this.setInsert(privilages.isInsert());
	}

	public static BulkEditSummary None()
	{
		return new BulkEditSummary(null);
	}

	public List<ModuleAttributes> getColumns()
	{
		return columns;
	}

	public void setColumns(List<ModuleAttributes> columns)
	{
		this.columns = columns;
	}

	public int getLevel()
	{
		return level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	public int getDepth()
	{
		return depth;
	}

	public void setDepth(int depth)
	{
		this.depth = depth;
	}

	public String getParentID()
	{
		return parentID;
	}

	public void setParentID(String parentID)
	{
		this.parentID = parentID;
	}

	public boolean isInsert()
	{
		return isInsert;
	}

	public void setInsert(boolean isInsert)
	{
		this.isInsert = isInsert;
	}
}
