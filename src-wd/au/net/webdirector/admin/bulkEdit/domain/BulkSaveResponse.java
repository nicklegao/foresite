/**
 * @author Sushant Verma
 * @date 25 Mar 2015
 */
package au.net.webdirector.admin.bulkEdit.domain;

import org.apache.log4j.Logger;

public class BulkSaveResponse
{
	private static Logger logger = Logger.getLogger(BulkSaveResponse.class);

	private boolean success;

	private boolean warnings;

	private String message;

	private BulkSaveResponse()
	{
		this.success = false;
		this.warnings = false;
	}

	/**
	 * @param string
	 * @return
	 */
	public static BulkSaveResponse failed(String message)
	{
		BulkSaveResponse r = new BulkSaveResponse();
		r.success = false;
		r.message = message;
		return r;
	}

	/**
	 * @param string
	 * @return
	 */
	public static BulkSaveResponse successWithWarning(String message)
	{
		BulkSaveResponse r = success();
		r.warnings = true;
		r.message = message;
		return null;
	}

	/**
	 * @return
	 */
	public static BulkSaveResponse success()
	{
		BulkSaveResponse r = new BulkSaveResponse();
		r.success = true;
		return r;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	public boolean isWarnings()
	{
		return warnings;
	}

	public void setWarnings(boolean warnings)
	{
		this.warnings = warnings;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}
}
