/**
 * @author Sushant Verma
 * @date 2 Sep 2015
 */
package au.net.webdirector.admin.triggers;

import java.util.HashSet;

import javax.servlet.ServletContext;

import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;

public class ModuleActionSet extends HashSet<ModuleAction> implements ModuleAction
{
	private static final long serialVersionUID = -4699025413556820993L;

	private String moduleName;

	public ModuleActionSet(String moduleName)
	{
		this.moduleName = moduleName;
	}

	@Override
	public String[] supportedModules()
	{
		return new String[] { moduleName };
	}

	@Override
	public long executionCount()
	{
		int count = 0;
		for (ModuleAction moduleAction : this)
		{
			count += moduleAction.executionCount();
		}
		return count;
	}

	@Override
	public long totalExecutionTime()
	{
		long count = 0;
		for (ModuleAction moduleAction : this)
		{
			count += moduleAction.totalExecutionTime();
		}
		return count;
	}

	@Override
	public long calculateAverageExecutionTime()
	{
		long count = executionCount();
		if (count > 0)
			return totalExecutionTime() / count;
		return 0;
	}

	/* Post-save operations */

	@Override
	public void didInsertElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.didInsertElementOnModule(servletContext, moduleName, elementData);
		}
	}

	@Override
	public void didInsertCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.didInsertCategoryOnModule(servletContext, moduleName, categoryData);
		}
	}

	@Override
	public void didUpdateElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.didUpdateElementOnModule(servletContext, moduleName, elementData);
		}
	}

	@Override
	public void didUpdateCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.didUpdateCategoryOnModule(servletContext, moduleName, categoryData);
		}
	}

	@Override
	public void didDeleteElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.didDeleteElementOnModule(servletContext, moduleName, elementData);
		}
	}

	@Override
	public void didDeleteCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.didDeleteCategoryOnModule(servletContext, moduleName, categoryData);
		}
	}

	/* Pre-save operations */

	@Override
	public void willInsertElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData) throws Exception
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.willInsertElementOnModule(servletContext, moduleName, elementData);
		}
	}

	@Override
	public void willInsertCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData) throws Exception
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.willInsertCategoryOnModule(servletContext, moduleName, categoryData);
		}
	}

	@Override
	public void willUpdateElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData) throws Exception
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.willUpdateElementOnModule(servletContext, moduleName, elementData);
		}
	}

	@Override
	public void willUpdateCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData) throws Exception
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.willUpdateCategoryOnModule(servletContext, moduleName, categoryData);
		}
	}

	@Override
	public void willDeleteElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData) throws Exception
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.willDeleteElementOnModule(servletContext, moduleName, elementData);
		}
	}

	@Override
	public void willDeleteCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData) throws Exception
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.willDeleteCategoryOnModule(servletContext, moduleName, categoryData);
		}
	}

	@Override
	public void didMoveElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.didMoveElementOnModule(servletContext, moduleName, elementData);
		}
	}

	@Override
	public void didMoveCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.didMoveCategoryOnModule(servletContext, moduleName, categoryData);
		}
	}

	@Override
	public void didCopyElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.didCopyElementOnModule(servletContext, moduleName, elementData);
		}
	}

	@Override
	public void didCopyCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.didCopyCategoryOnModule(servletContext, moduleName, categoryData);
		}
	}

	@Override
	public void willMoveElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData) throws Exception
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.willMoveElementOnModule(servletContext, moduleName, elementData);
		}
	}

	@Override
	public void willMoveCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData) throws Exception
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.willMoveCategoryOnModule(servletContext, moduleName, categoryData);
		}
	}

	@Override
	public void willCopyElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData) throws Exception
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.willCopyElementOnModule(servletContext, moduleName, elementData);
		}
	}

	@Override
	public void willCopyCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData) throws Exception
	{
		for (ModuleAction moduleAction : this)
		{
			moduleAction.willCopyCategoryOnModule(servletContext, moduleName, categoryData);
		}
	}
}
