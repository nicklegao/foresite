/**
 * @author Sushant Verma
 * @date 1 Sep 2015
 */
package au.net.webdirector.admin.triggers;

import javax.servlet.ServletContext;

import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;

/**
 * Note: ElementData/CategoryData may be incomplete due to the
 * lazy nature of BulkEdit and FolderTree.
 * 
 * To guarantee integrity, use only getId(), isElement() and isCategory()
 */
public interface ModuleAction
{
	public String[] supportedModules();

	public long executionCount();

	public long totalExecutionTime();

	public long calculateAverageExecutionTime();

	/* Post-save operations */

	public void didInsertElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData);

	public void didInsertCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData);

	public void didUpdateElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData);

	public void didUpdateCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData);

	public void didDeleteElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData);

	public void didDeleteCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData);

	public void didMoveElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData);

	public void didMoveCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData);

	public void didCopyElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData);

	public void didCopyCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData);

	/* Pre-save operations */

	/**
	 * 
	 * @param servletContext
	 * @param moduleName
	 * @param elementData
	 *            Should not have id but DataMap should be populated as insert
	 *            can only occur via DynamicFormBuilder
	 * @throws Exception
	 *             Throw exception to prevent db write and send user defined
	 *             exception message to the front end
	 */
	public void willInsertElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData) throws Exception;

	/**
	 * 
	 * @param servletContext
	 * @param moduleName
	 * @param categoryData
	 *            Should not have id but DataMap should be populated as insert
	 *            can only occur via DynamicFormBuilder
	 * @throws Exception
	 *             Throw exception to prevent db write and send user defined
	 *             exception message to the front end
	 */
	public void willInsertCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData) throws Exception;

	/**
	 * 
	 * @param servletContext
	 * @param moduleName
	 * @param elementData
	 * @throws Exception
	 *             Throw exception to prevent db write and send user defined
	 *             exception message to the front end
	 */
	public void willUpdateElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData) throws Exception;

	/**
	 * 
	 * @param servletContext
	 * @param moduleName
	 * @param categoryData
	 * @throws Exception
	 *             Throw exception to prevent db write and send user defined
	 *             exception message to the front end
	 */
	public void willUpdateCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData) throws Exception;

	/**
	 * 
	 * @param servletContext
	 * @param moduleName
	 * @param elementData
	 * @throws Exception
	 *             Throw exception to prevent db write and send user defined
	 *             exception message to the front end
	 */
	public void willDeleteElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData) throws Exception;

	/**
	 * 
	 * @param servletContext
	 * @param moduleName
	 * @param categoryData
	 * @throws Exception
	 *             Throw exception to prevent db write and send user defined
	 *             exception message to the front end
	 */
	public void willDeleteCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData) throws Exception;

	/**
	 * 
	 * @param servletContext
	 * @param moduleName
	 * @param elementData
	 *            Will contain id of move source, and parent id of target parent
	 * @throws Exception
	 *             Throw exception to prevent db write and send user defined
	 *             exception message to the front end
	 */
	public void willMoveElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData) throws Exception;

	/**
	 * 
	 * @param servletContext
	 * @param moduleName
	 * @param categoryData
	 *            Will contain id of move source, and parent id of target parent
	 * @throws Exception
	 *             Throw exception to prevent db write and send user defined
	 *             exception message to the front end
	 */
	public void willMoveCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData) throws Exception;

	/**
	 * 
	 * @param servletContext
	 * @param moduleName
	 * @param elementData
	 *            Will contain id of copy source, and parent id of target parent
	 * @throws Exception
	 *             Throw exception to prevent db write and send user defined
	 *             exception message to the front end
	 */
	public void willCopyElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData) throws Exception;

	/**
	 * 
	 * @param servletContext
	 * @param moduleName
	 * @param categoryData
	 *            Will contain id of copy source, and parent id of target parent
	 * @throws Exception
	 *             Throw exception to prevent db write and send user defined
	 *             exception message to the front end
	 */
	public void willCopyCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData) throws Exception;
}
