/**
 * @author Nick Yiming Gao
 * @date 2016-9-7
 */
package au.net.webdirector.admin.triggers.customised;

/**
 * 
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.ServletContext;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.log4j.Logger;

import au.net.webdirector.admin.triggers.AbstractModuleActions;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleData;

import com.google.gson.Gson;

public class GenericCallbackNotifier extends AbstractModuleActions
{

	private static Logger logger = Logger.getLogger(GenericCallbackNotifier.class);

	protected String getCallbackUrl(String action, String type, String module)
	{
		File propertiesFile = new File(Defaults.getInstance().getStoreDir() + "/_config/" + module + ".settings");
		if (!propertiesFile.exists())
		{
			return null;
		}
		Properties prop = new Properties();
		InputStream input = null;

		try
		{
			input = new FileInputStream(propertiesFile);
			prop.load(input);
			return prop.getProperty(action + "." + type + ".callback");
		}
		catch (IOException ex)
		{
			logger.error("", ex);
			return null;
		}
		finally
		{
			IOUtils.closeQuietly(input);
		}

	}

	protected void postToCallbackUrl(String action, String type, String module, ModuleData data)
	{
		String url = getCallbackUrl(action, type, module);
		if (url == null)
		{
			return;
		}
		HttpPost post = new HttpPost(url);
		HttpParams params = new BasicHttpParams();
		params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
		DefaultHttpClient httpClient = new DefaultHttpClient(params);
		if (url.toLowerCase().startsWith("https://"))
		{
			try
			{
				supportSelfSignedSSL(httpClient);
			}
			catch (Exception e)
			{
				logger.error("Error:", e);
			}
		}
		try
		{
			Map<String, Object> request = new HashMap<String, Object>();
			request.put("action", action);
			request.put("type", type);
			request.put("module", module);
			request.put("id", data.getId());

			String content = new Gson().toJson(request);
			StringEntity entity = new StringEntity(content, HTTP.UTF_8);
			entity.setContentType("application/json");
			post.setEntity(entity);
			logger.info("start posting to " + url);
			HttpResponse postResponse = httpClient.execute(post);
			logger.info("end posting");
			if (!postResponse.getStatusLine().toString().contains("200 OK"))
			{
				logger.error(postResponse.getStatusLine().toString());
				return;
				// throw new RuntimeException("Receive error when posting to " + url + " -> [" + postResponse.getStatusLine().toString() + "]");
			}
			String response = IOUtils.toString(postResponse.getEntity().getContent());
			logger.info("200 OK, return = " + response);
		}
		catch (Exception ex)
		{
			logger.error("", ex);
		}
	}

	private static void supportSelfSignedSSL(DefaultHttpClient httpClient) throws KeyManagementException, NoSuchAlgorithmException
	{
		SSLContext ctx = SSLContext.getInstance("TLS");
		X509TrustManager tm = new X509TrustManager()
		{

			public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException
			{
			}

			public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException
			{
			}

			public X509Certificate[] getAcceptedIssuers()
			{
				return null;
			}
		};
		ctx.init(null, new TrustManager[] { tm }, null);
		SSLSocketFactory ssf = new SSLSocketFactory(ctx, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		ClientConnectionManager ccm = httpClient.getConnectionManager();
		SchemeRegistry sr = ccm.getSchemeRegistry();
		sr.register(new Scheme("https", 443, ssf));
	}

	@Override
	public String[] supportedModules()
	{
		return new String[] { "*" };
	}

	@Override
	public void didInsertElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
		postToCallbackUrl("insert", "element", moduleName, elementData);
	}

	@Override
	public void didInsertCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{

		postToCallbackUrl("insert", "category", moduleName, categoryData);
	}

	@Override
	public void didUpdateElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
		postToCallbackUrl("update", "element", moduleName, elementData);
	}

	@Override
	public void didUpdateCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{

		postToCallbackUrl("update", "category", moduleName, categoryData);
	}

	@Override
	public void didDeleteElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
		postToCallbackUrl("delete", "element", moduleName, elementData);
	}

	@Override
	public void didDeleteCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
		postToCallbackUrl("delete", "category", moduleName, categoryData);
	}


}
