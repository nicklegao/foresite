/**
 * @author Jaysun Lee
 * @date 4 Jan 2016
 */
package au.net.webdirector.admin.triggers.customised.dms;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.net.webdirector.admin.tree.LazyTreeService;
import au.net.webdirector.admin.triggers.ModuleActionSet;
import au.net.webdirector.admin.triggers.ModuleActionTrigger;
import au.net.webdirector.admin.triggers.customised.ModuleTriggerController;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleData;

public class FolderTreeSave implements ModuleTriggerController
{
	private static Logger logger = Logger.getLogger(FolderTreeSave.class);

	@Override
	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
		parse(request, true);
	}

	@Override
	public void afterProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
		parse(request, false);
	}

	private void parse(ResettableStreamHttpServletRequest request, boolean isPreOperation) throws Exception
	{
		String module = request.getParameter("moduleName");

		ArrayList<Integer> assetIds = parseAssetIdsFromRequest(request, isPreOperation);
		ArrayList<ModuleData> moduleDatas = parseIdsToModuleData(request, assetIds);

		ServletContext servletContext = request.getServletContext();
		ModuleActionSet moduleActions = ModuleActionTrigger.actionsForModule(module);

		if (moduleActions.size() > 0)
		{
			if (isPreOperation)
				runPreOps(request, module, moduleDatas, servletContext, moduleActions);
			else
				runPostOps(request, module, moduleDatas, servletContext, moduleActions);
		}
	}

	private ArrayList<ModuleData> parseIdsToModuleData(ResettableStreamHttpServletRequest request, ArrayList<Integer> assetIds)
	{
		ArrayList<ModuleData> moduleDatas = new ArrayList<ModuleData>();

		for (int assetId : assetIds)
		{
			ModuleData moduleData;

			if (isElement(request))
				moduleData = new ElementData();
			else
				moduleData = new CategoryData();

			moduleData.setId(Integer.toString(assetId));
			if (!isSingleOperation(request))
			{
				String parentId = request.getParameter("parentId");
				moduleData.setParentId(parentId);
			}
			moduleDatas.add(moduleData);
		}

		return moduleDatas;
	}

	private void runPreOps(ResettableStreamHttpServletRequest request, String module, ArrayList<ModuleData> moduleDatas, ServletContext servletContext, ModuleActionSet moduleActions) throws Exception
	{
		for (ModuleData moduleData : moduleDatas)
		{
			if (isDeleteAction(request))
			{
				if (isElement(request))
					moduleActions.willDeleteElementOnModule(servletContext, module, (ElementData) moduleData);
				else
					moduleActions.willDeleteCategoryOnModule(servletContext, module, (CategoryData) moduleData);
			}
			else if (isMoveAction(request))
			{
				if (isElement(request))
					moduleActions.willMoveElementOnModule(servletContext, module, (ElementData) moduleData);
				else
					moduleActions.willMoveCategoryOnModule(servletContext, module, (CategoryData) moduleData);
			}
			else if (isCopyAction(request))
			{
				if (isElement(request))
					moduleActions.willCopyElementOnModule(servletContext, module, (ElementData) moduleData);
				else
					moduleActions.willCopyCategoryOnModule(servletContext, module, (CategoryData) moduleData);
			}
		}
	}

	private void runPostOps(ResettableStreamHttpServletRequest request, String module, ArrayList<ModuleData> moduleDatas, ServletContext servletContext, ModuleActionSet moduleActions)
	{
		for (ModuleData moduleData : moduleDatas)
		{
			if (isDeleteAction(request))
			{
				if (isElement(request))
					moduleActions.didDeleteElementOnModule(servletContext, module, (ElementData) moduleData);
				else
					moduleActions.didDeleteCategoryOnModule(servletContext, module, (CategoryData) moduleData);
			}
			else if (isMoveAction(request))
			{
				if (isElement(request))
					moduleActions.didMoveElementOnModule(servletContext, module, (ElementData) moduleData);
				else
					moduleActions.didMoveCategoryOnModule(servletContext, module, (CategoryData) moduleData);
			}
			else if (isCopyAction(request))
			{
				if (isElement(request))
					moduleActions.didCopyElementOnModule(servletContext, module, (ElementData) moduleData);
				else
					moduleActions.didCopyCategoryOnModule(servletContext, module, (CategoryData) moduleData);
			}
		}
	}


	private ArrayList<Integer> parseAssetIdsFromRequest(HttpServletRequestWrapper request, boolean isPreOperation)
	{
		ArrayList<Integer> assetIds = new ArrayList<Integer>();

		// Parse Ids
		if (isSingleOperation(request))
		{
			String id = request.getParameter("id");
			assetIds.add(Integer.parseInt(id));
		}
		else if (!isPreOperation && isCopyAction(request)) // special case if post db write, we need the ids of the new asset copies
		{
			ArrayList<Integer> copyTargetIds = (ArrayList<Integer>) request.getAttribute(LazyTreeService.COPY_TARGET_IDS);
			if (copyTargetIds != null && !copyTargetIds.isEmpty())
				assetIds = copyTargetIds;
		}
		else
		{
			String[] ids = request.getParameterValues("items[]");
			for (String id : ids)
				assetIds.add(Integer.parseInt(id));
		}

		return assetIds;
	}

	private boolean isSingleOperation(HttpServletRequestWrapper request)
	{
		return StringUtils.equals(request.getRequestURI(), URL_DELETE_ELEMENT)
				|| StringUtils.equals(request.getRequestURI(), URL_DELETE_CATEGORY);
	}

	private boolean isElement(HttpServletRequestWrapper request)
	{
		return StringUtils.equals(request.getRequestURI(), URL_DELETE_ELEMENT)
				|| StringUtils.equals(request.getRequestURI(), URL_COPY_ELEMENTS)
				|| StringUtils.equals(request.getRequestURI(), URL_MOVE_ELEMENTS);
	}

	private boolean isDeleteAction(HttpServletRequestWrapper request)
	{
		return StringUtils.equals(request.getRequestURI(), URL_DELETE_ELEMENT)
				|| StringUtils.equals(request.getRequestURI(), URL_DELETE_CATEGORY);
	}

	private boolean isMoveAction(HttpServletRequestWrapper request)
	{
		return StringUtils.equals(request.getRequestURI(), URL_MOVE_ELEMENTS)
				|| StringUtils.equals(request.getRequestURI(), URL_MOVE_CATEGORIES);
	}

	private boolean isCopyAction(HttpServletRequestWrapper request)
	{
		return StringUtils.equals(request.getRequestURI(), URL_COPY_ELEMENTS)
				|| StringUtils.equals(request.getRequestURI(), URL_COPY_CATEGORIES);
	}

	@Override
	public String[] getMappedUrls()
	{
		return new String[] {
				URL_DELETE_ELEMENT,
				URL_DELETE_CATEGORY,
				URL_COPY_ELEMENTS,
				URL_COPY_CATEGORIES,
				URL_MOVE_ELEMENTS,
				URL_MOVE_CATEGORIES
		};
	}

	private static final String URL_DELETE_ELEMENT = "/webdirector/folderTree/deleteElement";
	private static final String URL_DELETE_CATEGORY = "/webdirector/folderTree/deleteCategory";
	private static final String URL_COPY_ELEMENTS = "/webdirector/folderTree/copyElements";
	private static final String URL_COPY_CATEGORIES = "/webdirector/folderTree/copyCategories";
	private static final String URL_MOVE_ELEMENTS = "/webdirector/folderTree/moveElements";
	private static final String URL_MOVE_CATEGORIES = "/webdirector/folderTree/moveCategories";
}
