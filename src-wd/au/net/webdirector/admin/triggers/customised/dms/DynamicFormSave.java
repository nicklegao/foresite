package au.net.webdirector.admin.triggers.customised.dms;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.net.webdirector.admin.edit.DynamicFormBuilderController;
import au.net.webdirector.admin.triggers.ModuleActionSet;
import au.net.webdirector.admin.triggers.ModuleActionTrigger;
import au.net.webdirector.admin.triggers.customised.ModuleTriggerController;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleData;

import com.oreilly.servlet.MultipartRequest;

public class DynamicFormSave implements ModuleTriggerController
{
	private static Logger logger = Logger.getLogger(DynamicFormSave.class);

	@Override
	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
		parse(request, true);
	}

	@Override
	public void afterProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
		if (request.getAttribute("_cURRENt_pROCESSEd_rESULt_") == null || !(boolean) request.getAttribute("_cURRENt_pROCESSEd_rESULt_")
				|| isDeleteAction(request))
		{
			logger.info("Saving form failed, quit... ");
			return;
		}

		parse(request, false);
	}

	/**
	 * 
	 * @param request
	 * @param isPreOperation
	 *            Is the pre-database operation function to be used or
	 *            post-database operation function
	 */
	private void parse(ResettableStreamHttpServletRequest request, boolean isPreOperation) throws Exception
	{
		MultipartRequest multi = new MultipartRequest(request, Defaults.getInstance().getStoreDir() + "/_tmp", "UTF-8");
		String action = multi.getParameter("action");
		String module = multi.getParameter("module");

		// parse request to ModuleData
		request.resetInputStream();
		CommonsMultipartResolver resolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest mhsr = resolver.resolveMultipart(request);
		ModuleData moduleData = DynamicFormBuilderController.constructModuleDataFromMultipartRequest(mhsr);

		if (!isPreOperation)
		{
			String dataId = (String) request.getAttribute("_cURRENt_pROCESSEd_dATAId_");
			moduleData.setId(dataId);
		}

		ServletContext servletContext = request.getServletContext();
		ModuleActionSet moduleActions = ModuleActionTrigger.actionsForModule(module);

		if (moduleActions.size() > 0)
		{
			if (isPreOperation)
				runPreOps(action, module, isDeleteAction(request), moduleActions, servletContext, moduleData);
			else
				runPostOps(action, module, isDeleteAction(request), moduleActions, servletContext, moduleData);
		}
	}

	private void runPreOps(String action, String module, boolean isDeleteAction,
			ModuleActionSet moduleActions, ServletContext servletContext, ModuleData moduleData) throws Exception
	{
		logger.info("START running trigger pre-actions");

		boolean isElement = moduleData.isElement();

		if (action.equalsIgnoreCase("update"))
		{
			if (isElement)
				moduleActions.willUpdateElementOnModule(servletContext, module, (ElementData) moduleData);
			else
				moduleActions.willUpdateCategoryOnModule(servletContext, module, (CategoryData) moduleData);
		}
		else if (action.equalsIgnoreCase("insert"))
		{
			if (isElement)
				moduleActions.willInsertElementOnModule(servletContext, module, (ElementData) moduleData);
			else
				moduleActions.willInsertCategoryOnModule(servletContext, module, (CategoryData) moduleData);
		}
		else if (isDeleteAction)
		{
			if (isElement)
				moduleActions.willDeleteElementOnModule(servletContext, module, (ElementData) moduleData);
			else
				moduleActions.willDeleteCategoryOnModule(servletContext, module, (CategoryData) moduleData);
		}

		logger.info("DONE running trigger pre-actions");
	}

	private void runPostOps(String action, String module, boolean isDeleteAction,
			ModuleActionSet moduleActions, ServletContext servletContext, ModuleData moduleData) throws Exception
	{
		logger.info("START running trigger actions");

		boolean isElement = moduleData.isElement();

		if (action.equalsIgnoreCase("update"))
		{
			if (isElement)
				moduleActions.didUpdateElementOnModule(servletContext, module, (ElementData) moduleData);
			else
				moduleActions.didUpdateCategoryOnModule(servletContext, module, (CategoryData) moduleData);
		}
		else if (action.equalsIgnoreCase("insert"))
		{
			if (isElement)
				moduleActions.didInsertElementOnModule(servletContext, module, (ElementData) moduleData);
			else
				moduleActions.didInsertCategoryOnModule(servletContext, module, (CategoryData) moduleData);
		}
		else if (isDeleteAction)
		{
			if (isElement)
				moduleActions.didDeleteElementOnModule(servletContext, module, (ElementData) moduleData);
			else
				moduleActions.didDeleteCategoryOnModule(servletContext, module, (CategoryData) moduleData);
		}

		logger.info("DONE running trigger actions");
	}

	private boolean isDeleteAction(HttpServletRequestWrapper request)
	{
		return StringUtils.equals(request.getRequestURI(), "/webdirector/form/delete");
	}

	@Override
	public String[] getMappedUrls()
	{
		return new String[] { "/webdirector/form/save", "/webdirector/form/review", "/webdirector/form/delete" };
	}

}
