package au.net.webdirector.admin.triggers.customised.dms;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.net.webdirector.admin.bulkEdit.domain.BulkSaveSummary;
import au.net.webdirector.admin.triggers.ModuleActionSet;
import au.net.webdirector.admin.triggers.ModuleActionTrigger;
import au.net.webdirector.admin.triggers.customised.ModuleTriggerController;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleData;

public class BulkEditSave implements ModuleTriggerController
{

	private static Logger logger = Logger.getLogger(BulkEditSave.class);

	@Override
	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
		BulkSaveSummary bulkEditSummary = new Gson().fromJson(request.getReader(), BulkSaveSummary.class);
		parse(request, bulkEditSummary, true);
	}

	@Override
	public void afterProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
		if (!StringUtils.equalsIgnoreCase("success", (String) request.getAttribute("BULK_EDIT_RESULT")))
		{
			logger.info("Saving bulk edit failed, quit... ");
			return;
		}
		if (!StringUtils.equalsIgnoreCase("true", (String) request.getAttribute("VERSION_CONTROL")))
		{
			logger.info("No need to save version info, quit... ");
			return;
		}

		BulkSaveSummary changeSummary = (BulkSaveSummary) request.getAttribute("BULK_EDIT_REQUEST");
		parse(request, changeSummary, false);
	}

	/**
	 * @param request
	 * @param changeSummary
	 * @throws Exception
	 */
	private void parse(ResettableStreamHttpServletRequest request, BulkSaveSummary changeSummary, boolean isPreOperation) throws Exception
	{
		String module = changeSummary.getModuleName();
		String table = changeSummary.getDataType();
		boolean isElement = table.equalsIgnoreCase("elements");

		ServletContext servletContext = request.getServletContext();
		ModuleActionSet moduleActions = ModuleActionTrigger.actionsForModule(module);

		if (moduleActions.size() > 0)
		{
			if (isPreOperation)
				runPreOps(changeSummary, module, isElement, servletContext, moduleActions);
			else
				runPostOps(changeSummary, module, isElement, servletContext, moduleActions);
		}
		logger.info("Done");
	}

	/**
	 * @param changeSummary
	 * @param module
	 * @param isElement
	 * @param servletContext
	 * @param moduleActions
	 * @throws Exception
	 */
	private void runPreOps(BulkSaveSummary changeSummary, String module, boolean isElement, ServletContext servletContext, ModuleActionSet moduleActions) throws Exception
	{
		logger.info("START running trigger actions");

		for (ModuleData updates : changeSummary.generateUpdateList())
		{
			if (isElement)
				moduleActions.willUpdateElementOnModule(servletContext, module, (ElementData) updates);
			else
				moduleActions.willUpdateCategoryOnModule(servletContext, module, (CategoryData) updates);
		}
		for (ModuleData updates : changeSummary.generateDeleteList())
		{
			if (isElement)
				moduleActions.willDeleteElementOnModule(servletContext, module, (ElementData) updates);
			else
				moduleActions.willDeleteCategoryOnModule(servletContext, module, (CategoryData) updates);
		}

		logger.info("DONE running trigger actions");
	}

	/**
	 * @param changeSummary
	 * @param module
	 * @param isElement
	 * @param servletContext
	 * @param moduleActions
	 */
	private void runPostOps(BulkSaveSummary changeSummary, String module, boolean isElement, ServletContext servletContext, ModuleActionSet moduleActions)
	{
		logger.info("START running trigger actions");

		for (ModuleData updates : changeSummary.generateUpdateList())
		{
			if (isElement)
				moduleActions.didUpdateElementOnModule(servletContext, module, (ElementData) updates);
			else
				moduleActions.didUpdateCategoryOnModule(servletContext, module, (CategoryData) updates);
		}
		for (ModuleData updates : changeSummary.generateDeleteList())
		{
			if (isElement)
				moduleActions.didDeleteElementOnModule(servletContext, module, (ElementData) updates);
			else
				moduleActions.didDeleteCategoryOnModule(servletContext, module, (CategoryData) updates);
		}

		logger.info("DONE running trigger actions");
	}

	@Override
	public String[] getMappedUrls()
	{
		return new String[] { "/webdirector/bulkEdit/save" };
	}
}
