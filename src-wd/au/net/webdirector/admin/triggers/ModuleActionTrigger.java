/**
 * @author Sushant Verma
 * @date 1 Sep 2015
 */
package au.net.webdirector.admin.triggers;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class ModuleActionTrigger extends HttpServlet
{
	private static Logger logger = Logger.getLogger(ModuleActionTrigger.class);

	private static final Map<String, ModuleActionSet> assignedModuleActions = new HashMap<String, ModuleActionSet>();

	@Override
	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);
		String paramTriggerClasses = config.getInitParameter("triggerClasses");
		logger.info("ModuleActionTriggers being configured with classes: " + paramTriggerClasses);
		if (StringUtils.isBlank(paramTriggerClasses))
		{
			return;
		}
		for (String className : paramTriggerClasses.split(","))
		{
			className = className.trim();
			boolean success = false;
			try
			{
				Class<?> c = Class.forName(className);
				ModuleAction moduleAction = (ModuleAction) c.newInstance();
				String[] supportedModules = moduleAction.supportedModules();

				for (String moduleName : supportedModules)
				{
					assignModule(moduleName, moduleAction);
				}
				success = true;
			}
			catch (ClassNotFoundException e)
			{
				logger.fatal("Unable to find class... " + className, e);
				success = false;
			}
			catch (ClassCastException e)
			{
				logger.fatal("Unable to load class... " + className + " please ensure it implements " + ModuleAction.class.getCanonicalName(), e);
				success = false;
			}
			catch (InstantiationException e)
			{
				logger.fatal("Error:", e);
				success = false;
			}
			catch (IllegalAccessException e)
			{
				logger.fatal("Error:", e);
				success = false;
			}

			if (!success)
			{
				System.exit(1);
			}
		}
		logger.warn("Module action triggers registered.");

		for (String moduleName : assignedModuleActions.keySet())
		{
			logger.info("Module actions for module: " + moduleName);
			for (ModuleAction action : assignedModuleActions.get(moduleName))
			{
				logger.info(action.getClass().getCanonicalName());
			}
		}
	}

	public static Set<ModuleAction> assignModule(String moduleName, ModuleAction moduleAction)
	{
		moduleName = moduleName.toLowerCase();
		ModuleActionSet actions = assignedModuleActions.get(moduleName);
		if (actions == null)
		{
			actions = new ModuleActionSet(moduleName);
			assignedModuleActions.put(moduleName, actions);
		}
		actions.add(moduleAction);

		return actions;
	}

	public static ModuleActionSet actionsForModule(String moduleName)
	{
		moduleName = moduleName.toLowerCase();
		ModuleActionSet copy = new ModuleActionSet(moduleName);
		ModuleActionSet actionsAll = assignedModuleActions.get("*");
		if (actionsAll != null)
		{
			copy.addAll(actionsAll);
		}
		ModuleActionSet actions = assignedModuleActions.get(moduleName);
		if (actions != null)
		{
			copy.addAll(actions);
		}
		return copy;
	}
}
