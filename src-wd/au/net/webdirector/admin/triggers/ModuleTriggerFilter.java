package au.net.webdirector.admin.triggers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.system.classfinder.JavaClassFinder;
import au.net.webdirector.admin.edit.DynamicFormBuilderController.SimpleJsonResponse;
import au.net.webdirector.admin.triggers.customised.ModuleTriggerController;

import com.google.gson.Gson;

public class ModuleTriggerFilter implements Filter
{

	private static Logger logger = Logger.getLogger(ModuleTriggerFilter.class);

	private Map<String, Class<? extends ModuleTriggerController>> controller = new HashMap<String, Class<? extends ModuleTriggerController>>();

	public void init(FilterConfig arg0) throws ServletException
	{
		JavaClassFinder classFinder = new JavaClassFinder(ModuleTriggerController.class);

		List<Class<? extends ModuleTriggerController>> classes = classFinder.findAllMatchingTypes(ModuleTriggerController.class.getPackage().getName() + ".dms");

		for (Class<? extends ModuleTriggerController> clazz : classes)
		{
			try
			{
				ModuleTriggerController t = clazz.newInstance();
				for (String url : t.getMappedUrls())
				{
					controller.put(url, clazz);
				}
			}
			catch (Exception e)
			{
				logger.info("Skip " + clazz.getCanonicalName());
			}
		}
	}

	public void destroy()
	{

	}

	private ModuleTriggerController initModuleTriggerController(HttpServletRequest request)
	{
		String url = request.getRequestURI();
		for (Entry<String, Class<? extends ModuleTriggerController>> e : controller.entrySet())
		{
			if (url.endsWith(e.getKey()))
			{
				try
				{
					return e.getValue().newInstance();
				}
				catch (Exception e1)
				{
					logger.error("Skip url = [" + url + "] trialer = [" + e.getValue().getCanonicalName() + "]");
				}
			}
		}

		return null;
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException
	{
		ResettableStreamHttpServletRequest request = new ResettableStreamHttpServletRequest((HttpServletRequest) req);
		ModuleTriggerController controller = initModuleTriggerController((HttpServletRequest) req);
		try
		{
			if (controller != null)
			{
				try
				{
					controller.beforeProcess(request);
				}
				catch (Exception e)
				{
					logger.error("Error: ", e);
					// Send SimpleJsonResponse back to front end
					SimpleJsonResponse simpleJsonResponse = new SimpleJsonResponse(false, e.getMessage());
					new Gson().toJson(simpleJsonResponse, res.getWriter());
					res.setContentType("application/json");
					return;
				}
				request.resetInputStream();
			}
		}
		catch (Exception e)
		{
			logger.error("Error: ", e);
		}
		chain.doFilter(request, res);
		try
		{
			if (controller != null)
			{
				request.resetInputStream();
				controller.afterProcess(request);
				request.resetInputStream();
			}
		}
		catch (Exception e)
		{
			logger.error("Error: ", e);
		}
	}
}
