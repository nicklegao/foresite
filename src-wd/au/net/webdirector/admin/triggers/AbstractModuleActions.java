/**
 * @author Sushant Verma
 * @date 1 Sep 2015
 */
package au.net.webdirector.admin.triggers;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;

public abstract class AbstractModuleActions implements ModuleAction
{
	private static Logger logger = Logger.getLogger(AbstractModuleActions.class);

	protected long counter = 0;
	protected long totalExecutionTime = 0;

	@Override
	public abstract String[] supportedModules();

	@Override
	public long executionCount()
	{
		return counter;
	}

	@Override
	public long totalExecutionTime()
	{
		return totalExecutionTime;
	}

	@Override
	public long calculateAverageExecutionTime()
	{
		long count = executionCount();
		if (count > 0)
			return totalExecutionTime() / count;
		return 0;
	}

	@Override
	public void didInsertElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
	}

	@Override
	public void didInsertCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
		didUpdateCategoryOnModule(servletContext, moduleName, categoryData);
	}

	@Override
	public void didUpdateElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
	}

	@Override
	public void didUpdateCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
	}

	@Override
	public void didDeleteElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
	}

	@Override
	public void didDeleteCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
	}

	@Override
	public void willInsertElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData) throws Exception
	{
	}

	@Override
	public void willInsertCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData) throws Exception
	{
	}

	@Override
	public void willUpdateElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData) throws Exception
	{
	}

	@Override
	public void willUpdateCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData) throws Exception
	{
	}

	@Override
	public void willDeleteElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData) throws Exception
	{
	}

	@Override
	public void willDeleteCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData) throws Exception
	{
	}

	@Override
	public void didMoveElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
	}

	@Override
	public void didMoveCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
	}

	@Override
	public void didCopyElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
	}

	@Override
	public void didCopyCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
	}

	@Override
	public void willMoveElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
	}

	@Override
	public void willMoveCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
	}

	@Override
	public void willCopyElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
	}

	@Override
	public void willCopyCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
	}
}
