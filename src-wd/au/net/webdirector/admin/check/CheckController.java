package au.net.webdirector.admin.check;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import au.net.webdirector.common.Defaults;

@Controller
@RequestMapping("/check")
public class CheckController
{

	Defaults d = Defaults.getInstance();
	static Logger logger = Logger.getLogger(CheckController.class);


	@RequestMapping(value = { "**" })
	public String getChecks(
			HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session) throws IOException, ServletException
	{
		String uri = request.getRequestURI();

		uri = uri.replaceAll("/webdirector/", "jsp/");

		return uri;
	}
}
