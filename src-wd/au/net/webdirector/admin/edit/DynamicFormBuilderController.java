package au.net.webdirector.admin.edit;

import java.io.File;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import webdirector.db.DButils;
import webdirector.db.client.ClientFileUtils;
import au.com.ci.webdirector.user.UserManager;
import au.net.webdirector.admin.modules.privileges.CategoryPrivileges;
import au.net.webdirector.admin.modules.privileges.PrivilegeUtils;
import au.net.webdirector.admin.workflow.WorkflowAccess;
import au.net.webdirector.admin.workflow.WorkflowHelper;
import au.net.webdirector.admin.workflow.WorkflowInfo;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.AbstractFile;
import au.net.webdirector.common.datalayer.base.db.entity.MultiOptions;
import au.net.webdirector.common.datalayer.base.db.entity.Note;
import au.net.webdirector.common.datalayer.base.db.entity.Notes;
import au.net.webdirector.common.datalayer.base.db.entity.SharedFile;
import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
import au.net.webdirector.common.datalayer.base.db.entity.TextFile;
import au.net.webdirector.common.datalayer.base.db.entity.workflow.DraftStoreFile;
import au.net.webdirector.common.datalayer.base.db.entity.workflow.DraftTextFile;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleData;
import au.net.webdirector.common.datalayer.client.ModuleHelper;
import au.net.webdirector.common.datalayer.client.module.ModuleAttributes;
import au.net.webdirector.common.datalayer.util.text.DefaultDateFormat;
import au.net.webdirector.common.search.SearchContent;
import au.net.webdirector.common.utils.FileUtils;
import au.net.webdirector.common.utils.SimpleDateFormat;
import au.net.webdirector.common.utils.email.FileTemplateEmailBuilder;
import au.net.webdirector.common.utils.password.PasswordUtils;

@Controller
public class DynamicFormBuilderController
{

	Defaults d = Defaults.getInstance();
	static Logger logger = Logger.getLogger(DynamicFormBuilderController.class);


	@RequestMapping(value = "/widget-error", method = RequestMethod.GET)
	public String widgetError()
	{
		return "jsp/widgetError";
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String getEditor()
	{
		return "jsp/dynamicFormBuilder";
	}

	@RequestMapping(value = "/template/save/dialoge", method = RequestMethod.GET)
	public String templateSaveDialoge()
	{
		return "jsp/templateSave";
	}

	@RequestMapping(value = "/attrlong/delete/dialoge", method = RequestMethod.GET)
	public String attrlongDeleteDialoge()
	{
		return "jsp/a_clearATTRLONG";
	}

	@RequestMapping(value = "/lookup-options", method = RequestMethod.GET)
	public String lookupOptions()
	{
		return "jsp/lookupOptions";
	}

	@RequestMapping(value = "/lookup-options/data", method = RequestMethod.GET)
	public String lookupOptionsData()
	{
		return "jsp/lookupOptionsDataProvider";
	}

	@RequestMapping("/form/review")
	@ResponseBody
	public SimpleJsonResponse formReview(HttpServletRequest request)
	{
		String userId = UserManager.getUserId(request);
		if (userId == null)
		{
			return new SimpleJsonResponse(false, "Unknown user");
		}
		TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true));
		try
		{
			MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
			String module = multi.getParameter("module");
			String tableType = multi.getParameter("table");
			String dataId = multi.getParameter("ID");
			String draftId = multi.getParameter("DID");
			String status = multi.getParameter("status");
			String declineReason = multi.getParameter("declineReason");

			if (!WorkflowInfo.APPROVED.equals(status) && !WorkflowInfo.DECLINED.equals(status))
			{
				throw new InvalidParameterException("status = " + status);
			}
			// build ModuleData based on submitted form
			WorkflowHelper helper = WorkflowHelper.getInstance(txManager);
			WorkflowInfo workflow = helper.getStatusByDraftId(module, tableType, draftId);
			// check if work flow enabled
			if (workflow == null)
			{
				throw new IllegalStateException("No work flow info found for [" + module + ":" + tableType + ":" + draftId + "]");
			}
			if (WorkflowInfo.APPROVED.equals(status))
			{
				String insertedID = helper.copyWorkflowToLive(module, StringUtils.equals(tableType, "Elements"), draftId);

				boolean isElement = StringUtils.equalsIgnoreCase("Elements", tableType);
				boolean isInsert = !helper.isIdValid(workflow.getDataId());
				if (!isElement && isInsert)
				{
					PrivilegeUtils priv = new PrivilegeUtils(module, workflow.getWho());
					if (!priv.isUnrestricted())
					{
						//Not admin... add CRUD to the current user on this element.
						priv.setPrivlageOnCategory(txManager, CategoryPrivileges.ALL_CRUD, insertedID);
					}
				}

				request.setAttribute("FORM_SAVE_DATAID", workflow.getDataId());
				request.setAttribute("FORM_SAVE_RESULT", "success");
				request.setAttribute("_cURRENt_pROCESSEd_dATAId_", workflow.getDataId());
				request.setAttribute("_cURRENt_pROCESSEd_rESULt_", true);
				request.setAttribute("VERSION_CONTROL", "true");
			}
			else
			{
				sendEmailOnDecline(userId, workflow, declineReason);
				helper.saveStatus(userId, workflow, status);
			}
			helper.deleteDraftByDraftId(module, tableType, draftId);
			txManager.commit();
			return new SimpleJsonResponse(true, "Success");
		}
		catch (Exception e)
		{
			txManager.rollback();
			logger.error("Error", e);
			return new SimpleJsonResponse(false, e.getClass().getCanonicalName() + " " + e.getMessage());
		}
	}

	@RequestMapping("/form/save")
	@ResponseBody
	public SimpleJsonResponse formSave(HttpServletRequest request)
	{
		String userId = UserManager.getUserId(request);
		if (userId == null)
		{
			return new SimpleJsonResponse(false, "Unknown user");
		}
		// put all the files in tmp
		TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true));
		try
		{
			MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
			String module = multi.getParameter("module");
			String tableType = multi.getParameter("table");
			String action = multi.getParameter("action");
			String dataId = multi.getParameter("ID");
			String parentId = multi.getParameter("PID");
			String draftId = multi.getParameter("DID");
			String status = multi.getParameter("status");
			String stage = multi.getParameter("stage");

			if (multi.getParameter("sendOldDraftInEmail").equals("true"))
				formSendEmailDraft(userId, draftId, module, tableType);

			// check if work flow enabled
			WorkflowHelper workflowHelper = WorkflowHelper.getInstance(txManager);
			boolean needSaveToWorkflow = workflowHelper.isWorkflowEnabled(module) && workflowHelper.isUserNeedApproval(module, userId);
			boolean isWorkflowStage = StringUtils.equals("workflow", stage);
			WorkflowInfo workflow = isWorkflowStage ? workflowHelper.getStatusByDraftId(module, tableType, draftId) : workflowHelper.getStatus(module, tableType, dataId);
			boolean hasWorkflow = workflow != null;
			boolean isInsert = StringUtils.equalsIgnoreCase(action, "insert");
			multi.setAttribute("needSaveToWorkflow", needSaveToWorkflow);


			// build ModuleData based on submitted form
			boolean isElement = StringUtils.equalsIgnoreCase("Elements", tableType);
			ModuleData d = constructModuleDataFromMultipartRequest(multi);
			if (!isElement)
			{
				((CategoryData) d).setFolderLevel(Integer.parseInt(multi.getParameter("level")));
			}


			if (isInsert)
			{
				d.setParentId(parentId);
				d.removeId();
			}
			else if (isWorkflowStage)
			{
				d.setId(draftId);
			}
			else
			{
				d.setId(dataId);
			}
			if (needSaveToWorkflow)
			{
				if (isWorkflowStage)
				{
					dataId = workflowHelper.updateDataToWorkflow(module, d, action, status, userId);
				}
				else if (hasWorkflow)
				{
					workflowHelper.deleteDraftByDataId(module, tableType, dataId);
					draftId = workflowHelper.copyLiveToWorkflow(module, isElement, dataId);
					d.setId(draftId);
					dataId = workflowHelper.updateDataToWorkflow(module, d, action, status, userId, dataId);
				}
				else
				{
					draftId = workflowHelper.copyLiveToWorkflow(module, isElement, dataId);
					d.setId(draftId);
					dataId = workflowHelper.updateDataToWorkflow(module, d, action, status, userId, dataId);
				}
			}
			else
			//admin
			{
				if (isWorkflowStage)
				{
					dataId = workflowHelper.updateDataToWorkflow(module, d, action, status, userId);
					workflowHelper.copyWorkflowToLive(module, isElement, draftId);
					workflowHelper.deleteDraftByDraftId(module, tableType, draftId);
				}
				else if (hasWorkflow || !hasWorkflow)
				{
					workflowHelper.deleteDraftByDataId(module, tableType, dataId);
					dataId = workflowHelper.updateDataToLive(module, d, action);

					if (!isElement && isInsert)
					{
						PrivilegeUtils priv = PrivilegeUtils.getCachedInstance(module, request.getSession(), false);
						if (!priv.isUnrestricted())
						{
							//Not admin... add CRUD to the current user on this element.
							priv.setPrivlageOnCategory(txManager, CategoryPrivileges.ALL_CRUD, dataId);
						}
					}

				}
			}

			request.setAttribute("FORM_SAVE_DATAID", dataId);
			request.setAttribute("FORM_SAVE_RESULT", "success");
			request.setAttribute("_cURRENt_pROCESSEd_dATAId_", dataId);
			request.setAttribute("_cURRENt_pROCESSEd_rESULt_", true);
			request.setAttribute("VERSION_CONTROL", needSaveToWorkflow ? "false" : "true");

			txManager.commit();

			if (isInsert)
			{
				PrivilegeUtils.getCachedInstance(module, request.getSession(), true);
			}
			return new SimpleJsonResponse(true, "Data saved successfully");
		}
		catch (Exception e)
		{
			txManager.rollback();
			logger.error("Error", e);
			return new SimpleJsonResponse(false, e.getClass().getCanonicalName() + " " + e.getMessage());
		}
	}

	/**
	 * @param e
	 * @param multi
	 * @throws ParseException
	 * @throws IOException
	 * @throws IllegalStateException
	 */
	public static ModuleData constructModuleDataFromMultipartRequest(MultipartHttpServletRequest multi) throws ParseException, IllegalStateException, IOException
	{
		String tableType = multi.getParameter("table");
		String module = multi.getParameter("module");
		String id = multi.getParameter("ID");
		String action = multi.getParameter("action");
		String draftId = multi.getParameter("DID");
		String parentId = multi.getParameter("PID");
		boolean needToSaveWorkflow = false;
		if (multi.getAttribute("needSaveToWorkflow") != null)
		{
			needToSaveWorkflow = (boolean) multi.getAttribute("needSaveToWorkflow");
		}
		boolean isElement = StringUtils.equalsIgnoreCase("Elements", tableType);
		boolean isInsert = StringUtils.equalsIgnoreCase(action, "insert");
		ModuleData e = isElement ? new ElementData() : new CategoryData();
		if (needToSaveWorkflow)
		{
			id = draftId;
		}
		if (!isElement)
		{
			((CategoryData) e).setFolderLevel(Integer.parseInt(multi.getParameter("level")));
		}
		if (isInsert)
		{
			e.setParentId(parentId);
			e.removeId();
		}

		Map<String, Notes> notesMap = new LinkedHashMap<String, Notes>();
		for (Object paramName : Collections.list(multi.getParameterNames()))
		{
			String name = (String) paramName;
			if (StringUtils.equalsIgnoreCase(name, "Live"))
			{
				e.setLive(StringUtils.equals(multi.getParameter(name), "1"));
				continue;
			}
			if (StringUtils.equalsIgnoreCase(name, "Live_date"))
			{
				e.setLiveDate(multi.getParameter(name));
				continue;
			}
			if (StringUtils.equalsIgnoreCase(name, "Expire_date"))
			{
				e.setExpireDate(multi.getParameter(name));
				continue;
			}
			if (StringUtils.startsWith(name, "DUMMYATTRMULTI_") && !e.containsKey(name.substring(5)))
			{
				// if no option has been selected, form won't submit this field
				MultiOptions options = new MultiOptions();
				options.setField(name.substring(5));
				options.setId(id);
				options.setModule(module);
				options.setType(tableType);
				options.setValues(new ArrayList<String>());
				e.put(name.substring(5), options);
				continue;
			}
			if (StringUtils.startsWith(name, "DUMMY"))
			{
				continue;
			}
			if (StringUtils.startsWith(name, "note-ATTRNOTES_"))
			{
				String[] info = StringUtils.split(name, "-");
				String fieldName = info[1];
				String noteId = info[2];
				if (StringUtils.equals(noteId, "0") && StringUtils.isBlank(multi.getParameter(name)))
				{
					continue;
				}
				if (!notesMap.containsKey(fieldName))
				{
					Notes notes = new Notes();
					notesMap.put(fieldName, notes);
					notes.setElementOrCategory(tableType);
					notes.setField(fieldName);
					notes.setId(id);
					notes.setModule(module);
					List<Note> list = new ArrayList<Note>();
					notes.setNotes(list);
				}
				Note note = new Note();
				if (!StringUtils.equals(noteId, "0"))
				{
					note.setCreateDate((Date) new DefaultDateFormat().parse(multi.getParameter("noteCreate-" + noteId)));
					note.setId(noteId);
					// new metadata
					note.setLive(multi.getParameter("noteLive-" + noteId) != null);
					note.setLive_date(DefaultDateFormat.parseDate(multi.getParameter("noteLiveDate-" + noteId)));
					note.setExpire_date(DefaultDateFormat.parseDate(multi.getParameter("noteExpireDate-" + noteId)));
				}
				else
				{
					note.setLive(multi.getParameter("noteLive-" + fieldName) != null);
					note.setLive_date(DefaultDateFormat.parseDate(multi.getParameter("noteLiveDate-" + fieldName)));
					note.setExpire_date(DefaultDateFormat.parseDate(multi.getParameter("noteExpireDate-" + fieldName)));
				}
				note.setWho(multi.getParameter("noteWho-" + noteId));

				note.setContent(multi.getParameter(name));
				notesMap.get(fieldName).getNotes().add(note);
				continue;
			}
			if (!StringUtils.startsWith(name, "ATTR"))
			{
				continue;
			}
			if (StringUtils.startsWith(name, "ATTRFILE_"))
			{
				// will process uploaded files later, skip here 
				continue;
			}
			if (StringUtils.startsWith(name, "ATTRDATE_"))
			{
				e.put(name, DefaultDateFormat.parseDate(multi.getParameter(name)));
				continue;
			}
			if (StringUtils.startsWith(name, "ATTRLONG_"))
			{
				if (needToSaveWorkflow)
				{
					e.put(name, new DraftTextFile(multi.getParameter(name), module, name, !isElement));
				}
				else
				{
					e.put(name, new TextFile(multi.getParameter(name), module, name, !isElement));
				}
				continue;
			}
			if (StringUtils.startsWith(name, "ATTRTEXT_"))
			{
				if (needToSaveWorkflow)
				{
					DraftTextFile text = new DraftTextFile(multi.getParameter(name), module, name, !isElement);
					text.setId(id);
					e.put(name, text);
				}
				else
				{
					TextFile text = new TextFile(multi.getParameter(name), module, name, !isElement);
					text.setId(id);
					e.put(name, text);
				}
				continue;
			}
			if (StringUtils.startsWith(name, "ATTRMULTI_"))
			{
				MultiOptions options = new MultiOptions();
				options.setField(name);
				options.setId(id);
				options.setModule(module);
				options.setType(tableType);
				options.setValues(Arrays.asList(multi.getParameterValues(name)));
				e.put(name, options);
				continue;
			}
			if (StringUtils.startsWith(name, "ATTRPASS_"))
			{
				if (!StringUtils.equals(PasswordUtils.FAKE_PASSWD, multi.getParameter(name)))
				{
					e.put(name, PasswordUtils.encrypt(multi.getParameter(name)));
				}
				continue;
			}
			if (StringUtils.startsWith(name, "ATTRSHAREDFILE_"))
			{
				SharedFile shared = new SharedFile(multi.getParameter(name));
				e.put(name, shared);
				continue;
			}
			e.put(name, multi.getParameter(name));
//			e.put(name, new String(multi.getParameter(name).getBytes("UTF-8")));
		}
		for (Entry<String, Notes> entry : notesMap.entrySet())
		{
			e.put(entry.getKey(), entry.getValue());
		}
		// process uploaded files here
		for (Iterator<String> it = multi.getFileNames(); it.hasNext();)
		{
			String name = it.next();
			MultipartFile multiFile = multi.getFile(name);
			if (!Defaults.isFileExtAllowed(multiFile.getOriginalFilename()))
			{
				throw new RuntimeException("File type is not allowed: " + multiFile.getOriginalFilename());
			}
			if (StringUtils.startsWith(name, "ATTRFILE_") && !multiFile.isEmpty())
			{
				File tmp = new File(Defaults.getInstance().getStoreDir(), "_tmp/" + name);
				if (!tmp.exists())
				{
					tmp.mkdirs();
				}
				File uploadFile = new File(tmp, multiFile.getOriginalFilename());
				multiFile.transferTo(uploadFile);
				if (needToSaveWorkflow)
				{
					DraftStoreFile draftStoreFile = new DraftStoreFile(uploadFile, module, name, !isElement);
					draftStoreFile.setId(id);
					e.put(name, draftStoreFile);
				}
				else
				{
					StoreFile storeFile = new StoreFile(uploadFile, module, name, !isElement);
					storeFile.setId(id);
					e.put(name, storeFile);
				}
				continue;
			}
		}
		return e;
	}

	@RequestMapping("/form/delete")
	@ResponseBody
	public SimpleJsonResponse formDelete(HttpServletRequest request)
	{
		String userId = UserManager.getUserId(request);
		if (userId == null)
		{
			return new SimpleJsonResponse(false, "Unknown user");
		}
		MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
		String dataId = multi.getParameter("ID");
		String draftId = multi.getParameter("DID");
		String parentId = multi.getParameter("PID");
		String tableType = multi.getParameter("table");
		String module = multi.getParameter("module");
		String stage = multi.getParameter("stage");
		TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true));
		try
		{
			WorkflowHelper workflowHelper = WorkflowHelper.getInstance(txManager);
			boolean needSaveToWorkflow = workflowHelper.isWorkflowEnabled(module) && workflowHelper.isUserNeedApproval(module, userId);
			boolean isWorkflowStage = StringUtils.equals("workflow", stage);
			WorkflowInfo workflow = isWorkflowStage ? workflowHelper.getStatusByDraftId(module, tableType, draftId) : workflowHelper.getStatus(module, tableType, dataId);
			boolean hasWorkflow = workflow != null;
			if (needSaveToWorkflow)
			{
				if (isWorkflowStage)
				{
					workflowHelper.deleteDraftByDraftId(module, tableType, draftId);
				}
				else if (hasWorkflow)
				{
					workflowHelper.deleteDraftByDataId(module, tableType, dataId);
					workflowHelper.markAsDeleted(module, tableType, dataId, userId);
				}
				else
				{
					workflowHelper.markAsDeleted(module, tableType, dataId, userId);
				}
			}
			else
			{
				if (isWorkflowStage)
				{
					workflowHelper.deleteDraftByDraftId(module, tableType, draftId);
				}
				else if (!hasWorkflow)
				{
					workflowHelper.deleteLiveByDataId(module, tableType, dataId);
				}
				else
				{
					workflowHelper.deleteDraftByDataId(module, tableType, dataId);
					workflowHelper.deleteLiveByDataId(module, tableType, dataId);
				}

			}
			txManager.commit();
			return new SimpleJsonResponse(true, "Data deleted successfully");
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			txManager.rollback();
			return new SimpleJsonResponse(false, e.getClass().getCanonicalName() + " " + e.getMessage());
		}
	}

	@RequestMapping("/template/save")
	@ResponseBody
	public boolean templateSave(HttpServletRequest request)
	{
		String contents = (String) request.getParameter("contents");
		String srcFile = (String) request.getParameter("srcFile");
		String dynForm = (String) request.getParameter("dynForm");

		// read this once
		final String storeDir = Defaults.getInstance().getStoreDir();
		String filename = (String) request.getParameter("filename");

		// special case for saving email template
		if (srcFile == null || srcFile.equals("null"))
		{
			srcFile = "/_tmp/template.txt";
		}

		String dir = storeDir + "\\_templates\\";
		String dest = dir + filename;
		// does dir already exist ? If not create it
		File f = new File(dest);
		if (!f.exists())
		{
			try
			{
				File tempFile = new File(dir);
				tempFile.mkdirs();
				f.createNewFile();
			}
			catch (Exception ex)
			{
				System.out.println(ex.getMessage());
			}
		}

		// update contents of file with text
		ClientFileUtils cfu = new ClientFileUtils();
		try
		{
			if (dynForm != null && dynForm.equals("true"))
			{
				cfu.overwriteTextContents(dest, contents);
			}
			else
			{
				StringBuffer sb = cfu.readTextContents(storeDir + srcFile);
				cfu.overwriteTextContents(dest, sb.toString());
			}
			return true;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			return false;
		}
	}

	@RequestMapping("/template/delete")
	@ResponseBody
	public boolean templateDelete(HttpServletRequest request)
	{
		String srcFile = request.getParameter("srcFile");
		new FileUtils().clearFileContents(srcFile);
		return true;
	}

	@RequestMapping("/note/delete")
	@ResponseBody
	public boolean noteDelete(HttpServletRequest request)
	{
		if (null == request.getParameter("id"))
		{
			return false;
		}
		DButils dbu = new DButils();
		return dbu.deleteNote(request.getParameter("id"));
	}

	@RequestMapping("/file/delete")
	@ResponseBody
	public boolean fileDelete(HttpServletRequest request)
	{
		Defaults d = Defaults.getInstance();
		String f = (String) request.getParameter("file");
		String col = (String) request.getParameter("col");
		String id = (String) request.getParameter("id");
		String tableType = (String) request.getParameter("type");
		String module = request.getParameter("module");
		d.removeFile(f, id, module, col, tableType);

		// reindex lucene index
		SearchContent searchContent = new SearchContent("Elements".equals(tableType), module, String.valueOf(id));
		searchContent.updateAsset();
		return true;
	}

	@RequestMapping("/form/getDraftSaveDate")
	@ResponseBody
	public boolean formGetDraftSaveDate(@RequestParam String draftEditDate, @RequestParam String DataID, @RequestParam String DraftID, @RequestParam String module, @RequestParam String table)
	{
		Date before = new Date(Long.parseLong(draftEditDate));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String beforeEditDate = sdf.format(before) + ".0";

		DButils dbu = new DButils();
		Vector<String> result = null;

		if (DataID.equals("0"))
		{// Draft with no live
			result = dbu.selectQuerySingleCol("select D_WHEN from sys_draft_rec where D_DRAFT_ID = ? and D_MODULE = ? and D_TABLE = ?", new String[] { DraftID, module, table });
		}
		else
		{
			result = dbu.selectQuerySingleCol("select D_WHEN from sys_draft_rec where D_DATA_ID = ? and D_MODULE = ? and D_TABLE = ?", new String[] { DataID, module, table });
		}

		if (result.size() > 0 && result != null)
		{
			String afterEditDate = result.get(0);
			if (!beforeEditDate.equals(afterEditDate))
				return true;
		}

		return false;
	}

	private void formSendEmailDraft(String userId, String draftID, String module, String tableType)
	{
		try
		{
			WorkflowInfo workflow = WorkflowHelper.getInstance().getStatusByDraftId(module, tableType, draftID);
			sendEmailWithDataAndTemplate("Your draft has been overwritten", "draftOverwritten.html", workflow, new HashMap<String, Object>(), userId);
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
		}
	}

	private void sendEmailOnDecline(String userId, WorkflowInfo workflow, String reason) throws Exception
	{
		try
		{
			Map<String, Object> custom = new HashMap<String, Object>();
			custom.put("declineReason", reason);
			sendEmailWithDataAndTemplate("Your draft has been declined", "draftDeclined.html", workflow, custom, userId);
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
		}
	}

	private void sendEmailWithDataAndTemplate(String subject, String templateName, WorkflowInfo workflow, Map<String, Object> customMap, String userId) throws Exception
	{
		String draftIDToSend = workflow.getDraftId();
		String authorID = workflow.getWho();
		String table = workflow.getType();
		String module = workflow.getModule();
		// Get draft row
		ModuleData draftData = null;
		List<ModuleAttributes> allColumns = null;
		WorkflowAccess wa = WorkflowAccess.getInstance();
		ModuleHelper mh = ModuleHelper.getInstance();

		if (table.equals("Elements"))
		{
			draftData = wa.getElementById(module, draftIDToSend);
			allColumns = mh.getElementColumnsInfo(module);
		}
		else if (table.equals("Categories"))
		{
			draftData = wa.getCategoryById(module, draftIDToSend);
			allColumns = mh.getCategoryColumnsInfo(module, draftData.getString("folderLevel"));
		}
		else
		{
			throw new Exception("No table type supplied");
		}

		ArrayList<String> files = new ArrayList<String>();

		Map<String, Object> data = new HashMap<String, Object>();
		for (ModuleAttributes col : allColumns)
		{
			if (col.getInternal().startsWith("attrtext") || col.getInternal().startsWith("attrlong") || col.getInternal().startsWith("attrfile"))
			{
				AbstractFile af = draftData.getStoreFile(col.getInternal());
				if (af != null)
				{
					files.add(af.getStoredFile().getAbsolutePath());
					continue;
				}
			}
			data.put(col.getExternal(), draftData.getString(col.getInternal()));
		}
		TransactionDataAccess da = TransactionDataAccess.getInstance();
		String[] authorInfo = da.selectStringArray("select Email, Name from Users where user_id = ?", authorID);
		String operator = da.selectString("select Name from Users where user_id = ?", userId);
		FileTemplateEmailBuilder emailBuilder = new FileTemplateEmailBuilder("/au/net/webdirector/admin/template/");
		if (null != customMap && !customMap.isEmpty())
		{
			emailBuilder.addModule("custom", customMap);
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("data", data);
		params.put("operator", "" + operator);
		params.put("author", "" + authorInfo[1]);
		emailBuilder.prepareEmail(templateName, subject, params)
				.addTo(authorInfo[0])
				.addAttachment(files.toArray(new String[] {}))
				.doSend();
	}

	public static class SimpleJsonResponse
	{
		private final boolean success;
		private final String message;

		public SimpleJsonResponse(boolean success, String message)
		{
			this.success = success;
			this.message = message;
		}

		public boolean isSuccess()
		{
			return success;
		}

		public String getMessage()
		{
			return message;
		}
	}
}
