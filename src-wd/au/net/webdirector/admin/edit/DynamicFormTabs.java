package au.net.webdirector.admin.edit;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;

public class DynamicFormTabs
{
	static Logger logger = Logger.getLogger(DynamicFormTabs.class);

	Hashtable<String, String> columns = new Hashtable();
	String[] orderedColumnTabNames;
	int tabCounter = 0;
	int totalTabs = 0;

	public DynamicFormTabs(String module)
	{
		Vector results = getTabList(module);
		this.totalTabs = getTotalTabs(module);
//		if ((results != null) && (results.size() > 0))
		fillHash(results);
	}

	public DynamicFormTabs(String module, String Prefix, String level)
	{
		Vector results = getCatTabList(module, level);
		this.totalTabs = getTotalCatTabs(module, level);
		fillHash(results);
	}

	public List<String> getOrderedColumnTabNames()
	{
		return new ArrayList<String>(getOrderedColumnTabs().keySet());
	}

	public Map<String, List<Integer>> getOrderedColumnTabs()
	{
		Map<String, List<Integer>> map = new LinkedHashMap<String, List<Integer>>();
		for (int i = 0; i < this.orderedColumnTabNames.length; i++)
		{
			String name = this.orderedColumnTabNames[i];
			if (!map.containsKey(name))
			{
				map.put(name, new ArrayList<Integer>());
			}
			map.get(name).add(i);
		}
		return map;
	}

	private int getTotalTabs(String module)
	{
		String query = "select distinct(Label_TabName) from Labels_"
				+ DatabaseValidation.encodeParam(module)
				+ " where Label_OnOff = 1";
		DBaccess db = new DBaccess();
		//return db.select(query).size();
		return db.selectQuerySingleCol(query).size();
	}

	private int getTotalCatTabs(String module, String level)
	{
		String query = "select distinct(Label_TabName) from Categorylabels_"
				+ DatabaseValidation.encodeParam(module)
				+ " where Label_OnOff = 1 and (Attribute_Level = 0 or Attribute_Level = ?)";
		DBaccess db = new DBaccess();
		//return db.select(query).size();
		return db.selectQuerySingleCol(query, new String[] { level }).size();
	}

	private void fillHash(Vector results)
	{
		this.orderedColumnTabNames = new String[results.size()];
		for (int i = 0; i < results.size(); i++)
		{
			String[] s = (String[]) results.elementAt(i);

			if ((s[1] == null) || (s[1].equals("")) || (s[1].equals("null")))
				s[1] = "All";
			this.columns.put(s[0], s[1]);
			this.orderedColumnTabNames[i] = s[1];
		}
		/**
		 * String[] s = (String[]) results.elementAt(0);
		 * this.orderedColumnTabNames[0] = s[1];
		 * this.columns.put("ATTR_Headline", s[1]);
		 */
	}

	private void fillCatHash(Vector results)
	{
		this.orderedColumnTabNames = new String[results.size()];
		for (int i = 0; i < results.size(); i++)
		{
			String[] s = (String[]) results.elementAt(i);

			if ((s[1] == null) || (s[1].equals("")) || (s[1].equals("null")))
				s[1] = "All";
			this.columns.put(s[0], s[1]);
			this.orderedColumnTabNames[i] = s[1];
		}
		/**
		 * String[] s = (String[]) null;
		 * if ((results == null) || (results.size() == 0)) {
		 * this.columns.put("ATTR_categoryName", "All");
		 * this.orderedColumnTabNames[0] = "All";
		 * s = new String[2];
		 * 
		 * s[1] = "ALL";
		 * } else {
		 * s = (String[]) results.elementAt(0);
		 * this.orderedColumnTabNames[0] = s[1];
		 * }
		 * this.columns.put("ATTR_categoryName", s[1]);
		 */
	}

	private Vector getTabList(String module)
	{
		String[] cols = { "Label_InternalName", "Label_TabName" };
		String query = "select Label_InternalName, Label_TabName from Labels_"
				+ DatabaseValidation.encodeParam(module)
				+ " where Label_OnOff = 1 order by display_order";
		logger.debug(query);
		DBaccess db = new DBaccess();
		//return db.select(query, cols);
		// TODO validated
		return db.selectQuery(query);
	}

	private Vector getCatTabList(String module, String level)
	{
		String[] cols = { "Label_InternalName", "Label_TabName" };
		String query = "select Label_InternalName, label_TabName from Categorylabels_"
				+ DatabaseValidation.encodeParam(module)
				+ " where Label_OnOff = 1  and (Attribute_Level = 0 or Attribute_Level = ?) order by display_order";
		DBaccess db = new DBaccess();
		//return db.select(query, cols);
		return db.selectQuery(query, new String[] { level });
	}

	public String initialTabListOutline()
	{
		StringBuffer sb = new StringBuffer();
		String selected = " class=\"selected\"";
		String lastCorner = "";

		int tabCount = 1;
		for (int i = 0; i < this.orderedColumnTabNames.length; i++)
		{
			if (i == 0)
			{
				sb.append("<li><span><a href=\"#\" rel=\"dog" + tabCount + "\""
						+ selected + " id=\"leftcorner\" style=\"padding-top: 5px;\">"
						+ this.orderedColumnTabNames[i] + "</a></span></li>\n");
			}
			else if (!this.orderedColumnTabNames[i]
					.equals(this.orderedColumnTabNames[(i - 1)]))
			{
				tabCount++;
				if (tabCount == this.totalTabs)
					lastCorner = "id=\"rightcorner\"";
				sb.append("<li><a href=\"#\" rel=\"dog" + tabCount + "\" "
						+ lastCorner + ">" + this.orderedColumnTabNames[i]
						+ "</a></li>\n");
			}
		}

		return sb.toString();
	}

	public String checkForStartNewTabDiv(int position)
	{
		if (position == 0)
		{
			this.tabCounter = 1;
			return "<div id=\"dog" + this.tabCounter
					+ "\" class=\"tabcontent\">";
		}
		if (!this.orderedColumnTabNames[position]
				.equals(this.orderedColumnTabNames[(position - 1)]))
		{
			this.tabCounter += 1;
			return "<div id=\"dog" + this.tabCounter
					+ "\" class=\"tabcontent\">";
		}

		return "";
	}

	public String checkForEndNewTabDiv(int position)
	{
		try
		{
			if (position + 1 == this.orderedColumnTabNames.length)
			{
				return "</div>";
			}
			if (this.orderedColumnTabNames[position]
					.equals(this.orderedColumnTabNames[(position + 1)]))
			{
				return "";
			}
			return "</div>";
		}
		catch (Exception e)
		{
			System.out.println(" Exception "
					+ this.orderedColumnTabNames.length + " vs " + position);
			e.printStackTrace();
		}
		return "</div>";
	}

	public String getTabNameForColumn(String internalColumnName)
	{
		return (String) this.columns.get(internalColumnName);
	}
}