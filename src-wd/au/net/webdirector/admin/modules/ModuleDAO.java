package au.net.webdirector.admin.modules;

import java.util.List;
import java.util.Map;

import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashMapMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;

public class ModuleDAO
{

	DataAccess db = DataAccess.getInstance();

	public List<Map<String, Object>> getModuleDetailsForUser(String userId)
	{
		String query = "SELECT internal_module_name as module_name, client_module_name as module_description, levels, color, icon, " +
				"(CASE WHEN u.UserLevel_id = 1 THEN FALSE ELSE TRUE END) as filter_by_privileges, " +
				"m.version_control_enabled, m.workflow_enabled, " +
				"m.has_sort, m.has_cut, m.has_copy, m.has_import, m.has_export,m.has_full_ckeditor,m.has_form_components,m.has_browse_server,m.customised_buttons " +
				"FROM sys_moduleconfig m " +
				"INNER JOIN sys_usermodules um ON um.module_id = m.id " +
				"INNER JOIN users u ON u.user_id = um.user_id " +
				"LEFT JOIN user_module_privileges p ON (p.module_name = m.internal_module_name AND p.user_id = u.user_id) " +
				"WHERE u.user_id = ? " +
				"GROUP BY m.internal_module_name " +
				"ORDER BY module_description ";
		return db.select(query, new Object[] { userId }, new HashMapMapper());
	}

	public Map<String, Object> getModuleDetailsForModule(String aModule)
	{
		String query = "SELECT internal_module_name as module_name, client_module_name as module_description, levels, color, icon, show_live_flag " +
				"FROM sys_moduleconfig " +
				"WHERE internal_module_name = ?";
		List<Map<String, Object>> res = db.select(query, new Object[] { aModule }, new HashMapMapper());
		return res.size() == 0 ? null : res.get(0);
	}

	public String getModuleName(String tablePrefix)
	{
		String query = "SELECT client_module_name " +
				"FROM sys_moduleconfig " +
				"WHERE internal_module_name = ?";
		List<String> res = db.select(query, new Object[] { tablePrefix }, new StringMapper());
		if (res.size() == 0)
		{
			return null;
		}
		return res.get(0);
	}

	public void updateModuleSettings(
			String internalName,
			String externalName,
			String moduleLevel,
			String color,
			String icon,
			boolean hasSort,
			boolean hasCut,
			boolean hasCopy,
			boolean hasImport,
			boolean hasExport,
			boolean hasVersionControl,
			boolean hasWorkflow,
			boolean showLiveFlag,
			boolean hasFullCkeditor,
			boolean hasFormComponents,
			boolean hasBrowseServer,
			String customisedButtons
			)
	{
		db.updateData("UPDATE sys_moduleconfig SET " +
				"client_module_name = ?, " +
				"levels = ?, " +
				"color = ?, " +
				"icon = ?, " +
				"has_sort = ?, " +
				"has_cut = ?, " +
				"has_copy = ?, " +
				"has_import = ?, " +
				"has_export = ?, " +
				"version_control_enabled = ?, " +
				"workflow_enabled = ?, " +
				"show_live_flag = ?, " +
				"has_full_ckeditor = ?, " +
				"has_form_components = ?, " +
				"has_browse_server = ?, " +
				"customised_buttons = ? " +
				"WHERE internal_module_name = ? ",
				new Object[] {
						externalName,
						moduleLevel,
						color,
						icon,
						hasSort,
						hasCut,
						hasCopy,
						hasImport,
						hasExport,
						hasVersionControl,
						hasWorkflow,
						showLiveFlag,
						hasFullCkeditor,
						hasFormComponents,
						hasBrowseServer,
						customisedButtons,
						internalName
				});
	}

}
