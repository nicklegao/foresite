package au.net.webdirector.admin.modules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import webdirector.db.DBmanage;
import webdirector.db.DButils;
import au.net.webdirector.admin.workflow.WorkflowHelper;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashMapMapper;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import au.net.webdirector.common.utils.module.DropDownBuilder;

@Controller
@RequestMapping("/configureModule")
public class ModuleConfigController
{
	private static Logger logger = Logger.getLogger(ModuleConfigController.class);

	@RequestMapping("/updateMetadata")
	@ResponseBody
	public SimpleJsonResponse updateMetadata(HttpServletRequest request)
	{
		String showMsg = null;
		String mode = request.getParameter("mode");
		String moduleName = request.getParameter("moduleName");
		String tableName = "Elements_" + moduleName;
		boolean success = false;
		DButils db = new DButils(tableName);

		// form submitted, update db
		try
		{
			success = db.updateLabelsTableFromForm(request, moduleName, mode);
			if (("Category").equalsIgnoreCase(mode))
			{
				DynamicFormScriptUtil.setCategoryFormValidScript(moduleName, request.getParameter("formValidationScripts"));
			}
			else
			{
				DynamicFormScriptUtil.setAssetFormValidScript(moduleName, request.getParameter("formValidationScripts"));
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			showMsg = "";
			if (e instanceof org.springframework.dao.DuplicateKeyException && e.getMessage().indexOf("Duplicate entry '") != -1)
			{
				int start = showMsg.indexOf("Duplicate entry '");
				if (start != -1 && showMsg.indexOf("'", start + "Duplicate entry ".length() + 1) != -1)
				{
					int end = showMsg.indexOf("'", start + "Duplicate entry ".length() + 1);
					showMsg = showMsg.substring(start + "Duplicate entry ".length(), end);
					showMsg = "Duplicate value: " + showMsg + "' found!";
				}
				showMsg = "DuplicateKeyException occured!\n" + showMsg;
			}
			else
			{
				showMsg = "Error occured!\n";
			}
		}
		return new SimpleJsonResponse(success, showMsg);
	}

	@RequestMapping("/addColumn")
	@ResponseBody
	public SimpleJsonResponse addColumn(HttpServletRequest request)
	{
		String mode = request.getParameter("mode");
		String moduleName = request.getParameter("moduleName");

		String tableName;
		String labelTableName;
		if ("Category".equalsIgnoreCase(mode))
		{
			tableName = "Categories_" + moduleName;
			labelTableName = "CategoryLabels_" + moduleName;
		}
		else
		{
			tableName = "Elements_" + moduleName;
			labelTableName = "Labels_" + moduleName;
		}
		String colName = request.getParameter("addCol");
		String colType = request.getParameter("colType");
		String colSize = request.getParameter("colSize");

		boolean success = false;
		String message = null;

		try
		{
			DBmanage dbManage = new DBmanage();
			message = dbManage.addNewColumn(colName, colType, tableName, labelTableName, colSize);
			String fullColName = dbManage.getFullColName(colName, colType);
			//TODO: Pull form validation from DBmanage into controller
			if (colType.equals("SharedFile"))
			{
				if (dbManage.addIndex(fullColName, tableName, ""))
				{
					new DBaccess().updateData("update " + labelTableName + " set Label_Index = '1' where Label_internalName = ?", new String[] { fullColName });
				}
			}
			if (message != null && message.contains(" success"))
			{
				success = true;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			message = "Invalid column name";
		}
		return new SimpleJsonResponse(success, message);
	}

	@RequestMapping("/deleteColumn")
	@ResponseBody
	public SimpleJsonResponse deleteColumn(HttpServletRequest request)
	{
		String mode = request.getParameter("mode");
		String moduleName = request.getParameter("moduleName");

		String tableName;
		String labelTableName;
		if ("Category".equalsIgnoreCase(mode))
		{
			tableName = "Categories_" + moduleName;
			labelTableName = "CategoryLabels_" + moduleName;
		}
		else
		{
			tableName = "Elements_" + moduleName;
			labelTableName = "Labels_" + moduleName;
		}
		String removeCol = (String) request.getParameter("deleteCol");

		boolean success = false;
		String message = null;

		try
		{
			DBmanage dbManage = new DBmanage();
			if (removeCol != null && !removeCol.equals(""))
			{
				success = dbManage.removeColumn(removeCol, tableName, labelTableName);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			message = "Internal Server Error";
		}
		return new SimpleJsonResponse(success, message);
	}

	@RequestMapping("/getMacroColumns")
	@ResponseBody
	public Object getMacroColumns(HttpServletRequest request)
	{
		String modType = request.getParameter("modType");
		String labelTablePrefix = "labels_";
		if ("CATEGORY".equals(modType))
		{
			labelTablePrefix = "category" + labelTablePrefix;
		}
		String modName = request.getParameter("modName");
		String sql = "SELECT Label_InternalName, Label_ExternalName FROM " + DatabaseValidation.encodeParam(labelTablePrefix) + DatabaseValidation.encodeParam(modName)
				+ " WHERE Label_OnOff=1";
		List<HashMap<String, String>> results = new ArrayList<HashMap<String, String>>();
		if ("CATEGORY".equals(modType))
		{
			HashMap<String, String> categoryIdNameValue = new HashMap<String, String>();
			categoryIdNameValue.put("Label_InternalName", "Category_id");
			categoryIdNameValue.put("Label_ExternalName", "Category ID");
			results.add(categoryIdNameValue);
		}
		else
		{
			HashMap<String, String> elementIdNameValue = new HashMap<String, String>();
			elementIdNameValue.put("Label_InternalName", "Element_id");
			elementIdNameValue.put("Label_ExternalName", "Element ID");
			results.add(elementIdNameValue);
		}
		results.addAll(DataAccess.getInstance().select(sql, new HashMapMapper()));
		return results;
	}

	@RequestMapping("/deleteDropDownValue")
	@ResponseBody
	public String deleteDropDownValue(@RequestParam String dropDownId)
	{
		DropDownBuilder ddb = new DropDownBuilder();
		ddb.deleteDropDownValue(dropDownId);
		return null;
	}

	@RequestMapping("/saveDropDownValue")
	@ResponseBody
	public String saveDropDownValue(HttpServletRequest request)
	{
		String moduleName = (String) request.getParameter("moduleName");
		String type = request.getParameter("type");
		String colName = request.getParameter("colName");

		DropDownBuilder ddb = new DropDownBuilder();
		if (StringUtils.isNotBlank(request.getParameter("dropDownName")) && StringUtils.isNotBlank(request.getParameter("dropDownValue")))
		{
			ddb.setDropDownValue(moduleName, colName, request.getParameter("dropDownName"), request.getParameter("dropDownValue"), type);
		}
		String[] ids = request.getParameterValues("id");
		String[] dd_names = request.getParameterValues("DD_NAME");
		String[] dd_values = request.getParameterValues("DD_VALUE");
		if (ids != null)
		{
			for (int i = 0; i < ids.length; i++)
			{
				ddb.updateDropDownValue(ids[i], dd_names[i], dd_values[i]);
			}
		}
		return null;
	}

	@RequestMapping("/updateModuleSettings")
	@ResponseBody
	public SimpleJsonResponse updateModuleSettings(
			@RequestParam String moduleName,
			@RequestParam String editAttrModuleName,
			@RequestParam String editAttrModuleLevel,
			@RequestParam String editModuleColor,
			@RequestParam String editModuleIcon,
			@RequestParam(required = false) String hasSort,
			@RequestParam(required = false) String hasCut,
			@RequestParam(required = false) String hasCopy,
			@RequestParam(required = false) String hasImport,
			@RequestParam(required = false) String hasExport,
			@RequestParam(required = false) String hasVersionControl,
			@RequestParam(required = false) String hasWorkflow,
			@RequestParam(required = false) String showLiveFlag,
			@RequestParam(required = false) String hasFullCkeditor,
			@RequestParam(required = false) String hasFormComponents,
			@RequestParam(required = false) String hasBrowseServer,
			@RequestParam(required = false) String customisedButtons
			)
	{
		boolean hasSortBool = (hasSort != null);
		boolean hasCutBool = (hasCut != null);
		boolean hasCopyBool = (hasCopy != null);
		boolean hasImportBool = (hasImport != null);
		boolean hasExportBool = (hasExport != null);
		boolean hasVersionControlBool = (hasVersionControl != null);
		boolean hasWorkflowBool = (hasWorkflow != null);
		boolean showLiveFlagBool = (showLiveFlag != null);
		boolean hasFullCkeditorBool = (hasFullCkeditor != null);
		boolean hasFormComponentsBool = (hasFormComponents != null);
		boolean hasBrowseServerBool = (hasBrowseServer != null);
		try
		{
			ModuleDAO db = new ModuleDAO();
			db.updateModuleSettings(
					moduleName,
					editAttrModuleName,
					editAttrModuleLevel,
					editModuleColor,
					editModuleIcon,
					hasSortBool,
					hasCutBool,
					hasCopyBool,
					hasImportBool,
					hasExportBool,
					hasVersionControlBool,
					hasWorkflowBool,
					showLiveFlagBool,
					hasFullCkeditorBool,
					hasFormComponentsBool,
					hasBrowseServerBool,
					customisedButtons
					);

			return new SimpleJsonResponse(true, null);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return new SimpleJsonResponse(false, "Internal Server Error");
		}
	}

	@RequestMapping("/assignApprovers")
	@ResponseBody
	public SimpleJsonResponse assignApprovers(HttpServletRequest request,
			@RequestParam String moduleName)
	{
		String prefix = "approver_";
		int prefixLength = prefix.length();

		Set<String> setUsers = new HashSet<String>();
		for (String param : (Set<String>) request.getParameterMap().keySet())
		{
			if (param.startsWith(prefix))
			{
				String userID = param.substring(prefixLength);
				setUsers.add(userID);
			}
		}

		TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false));
		try
		{
			WorkflowHelper helper = WorkflowHelper.getInstance(txManager);
			helper.setModuleApprovers(moduleName, setUsers);

			txManager.commit();
			return new SimpleJsonResponse(true, null);
		}
		catch (Exception e)
		{
			txManager.rollback();

			e.printStackTrace();
			return new SimpleJsonResponse(false, "Internal Server Error");
		}
	}

	public static class SimpleJsonResponse
	{
		private final boolean success;
		private final String message;

		public SimpleJsonResponse(boolean success, String message)
		{
			this.success = success;
			this.message = message;
		}

		public boolean isSuccess()
		{
			return success;
		}

		public String getMessage()
		{
			return message;
		}
	}
}
