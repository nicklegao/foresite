package au.net.webdirector.admin.modules;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;

public class DynamicFormScriptUtil
{

	public static String getAssetFormValidScript(String moduleName)
	{
		String script = "";
		// read from file;
		try
		{
			script = FileUtils.readFileToString(getScriptFileForAsset(moduleName));
		}
		catch (Exception e)
		{
		}
		if (StringUtils.isNotBlank(script))
		{
			return script;
		}
		// read from db;
		try
		{
			script = TransactionDataAccess.getInstance().selectString("select asset_valid_script from sys_moduleconfig where internal_module_name = ?", new String[] { moduleName });
		}
		catch (Exception e)
		{
		}

		if (StringUtils.isNotBlank(script))
		{
			return script;
		}

		script += "DynamicFormValidator.Elements['" + moduleName + "'] = function($jContext){\n";
		script += "\n";
		script += "  onInit = function(){\n";
		script += "  }\n";
		script += "\n";
		script += "  onDelete = function(){\n";
		script += "  }\n";
		script += "\n";
		script += "  onCreate = function(){\n";
		script += "  }\n";
		script += "\n";
		script += "  onUpdate = function(){\n";
		script += "  }\n";
		script += "\n";
		script += "  return {\n";
		script += "    onInit: onInit,\n";
		script += "    onDelete: onDelete,\n";
		script += "    onCreate: onCreate,\n";
		script += "    onUpdate: onUpdate\n";
		script += "  }\n";
		script += "}\n";
		return script;

	}

	public static String getCategoryFormValidScript(String moduleName)
	{
		String script = "";
		// read from file;
		try
		{
			script = FileUtils.readFileToString(getScriptFileForCategory(moduleName));
		}
		catch (Exception e)
		{
		}
		if (StringUtils.isNotBlank(script))
		{
			return script;
		}
		// read from db
		try
		{
			script = TransactionDataAccess.getInstance().selectString("select cate_valid_script from sys_moduleconfig where internal_module_name = ?", new String[] { moduleName });
		}
		catch (Exception e)
		{
		}
		if (StringUtils.isNotBlank(script))
		{
			return script;
		}

		// create new template
		script += "DynamicFormValidator.Categories['" + moduleName + "'] = function($jContext){\n";
		script += "\n";
		script += "  onInit = function(){\n";
		script += "  }\n";
		script += "\n";
		script += "  onDelete = function(){\n";
		script += "  }\n";
		script += "\n";
		script += "  onCreate = function(){\n";
		script += "  }\n";
		script += "\n";
		script += "  onUpdate = function(){\n";
		script += "  }\n";
		script += "\n";
		script += "  return {\n";
		script += "    onInit: onInit,\n";
		script += "    onDelete: onDelete,\n";
		script += "    onCreate: onCreate,\n";
		script += "    onUpdate: onUpdate\n";
		script += "  }\n";
		script += "}\n";

		return script;
	}

	public static void setAssetFormValidScript(String moduleName, String script)
	{
		// backup file
		File scriptFile = getScriptFileForAsset(moduleName);
		backup(scriptFile);
		// save to file
		try
		{
			FileUtils.writeStringToFile(scriptFile, script);
		}
		catch (IOException e)
		{
		}
		// save to db
		try
		{
			TransactionDataAccess.getInstance().update("update sys_moduleconfig set asset_valid_script = ? where internal_module_name = ?", new String[] { script, moduleName });
		}
		catch (SQLException e)
		{
		}
	}

	public static void setCategoryFormValidScript(String moduleName, String script)
	{
		// backup file
		File scriptFile = getScriptFileForCategory(moduleName);
		backup(scriptFile);
		// save to file
		try
		{
			FileUtils.writeStringToFile(scriptFile, script);
		}
		catch (IOException e)
		{
		}
		// save to db
		try
		{
			TransactionDataAccess.getInstance().update("update sys_moduleconfig set cate_valid_script = ? where internal_module_name = ?", new String[] { script, moduleName });
		}
		catch (SQLException e)
		{
		}
	}

	private static File getScriptFileForAsset(String module)
	{
		return getScriptFile(module, "validator_asset");
	}

	private static File getScriptFileForCategory(String module)
	{
		return getScriptFile(module, "validator_category");
	}

	private static File getScriptFile(String module, String type)
	{
		File script = new File(Defaults.getInstance().getStoreDir(), "/_scripts/" + module + "/" + type + ".js");
		if (!script.getParentFile().exists())
		{
			script.getParentFile().mkdirs();
		}
		return script;
	}

	private static void backup(File file)
	{
		if (!file.exists())
		{
			return;
		}
		File bakFile = new File(file.getAbsolutePath() + "." + new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date()));
		try
		{
			FileUtils.copyFile(file, bakFile);
		}
		catch (IOException e)
		{
		}
	}


}
