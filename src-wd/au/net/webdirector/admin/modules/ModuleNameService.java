package au.net.webdirector.admin.modules;

import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 18/02/2006
 * Time: 12:29:49
 * 
 * @noinspection StringConcatenationInsideStringBufferAppend
 * @modified by Sushant (9/01/2014)
 */
public class ModuleNameService
{

	private Hashtable NameLookup = new Hashtable();

	public ModuleNameService()
	{
		createNameLookup();
	}

	/**
	 *
	 * @return a list of all modules in Webdirector
	 */
	private String[] getAllModules()
	{
		return DynamicModuleController.sharedInstance().getModuleNameLookup().keySet().toArray(new String[0]);
	}

	/**
	 *
	 * This function has been dumbed down in terms of functionality. The old
	 * version compared the license key against
	 * the valid modules.
	 *
	 * @contact Sushant
	 * @return all the modules in webdirector that are valid modules based on
	 *         the binary module number
	 */
	public String[] getAllValidModules()
	{
		String[] allModules = getAllModules();
		return allModules;
	}

	/**
	 * Given the internal module name this method queries a lookup table to find
	 * the equivalent external name
	 * 
	 * @param internalName
	 * @return String extenalName
	 */

	public String getModuleName(String internalName)
	{
		String externalName = (String) NameLookup.get(internalName);
		if (externalName == null)
			System.out.println("ERROR: Looking for module name " + internalName + " but didn't find a match, <NULL> returned");

		return (externalName);
	}

	private void createNameLookup()
	{
		NameLookup.clear();
		NameLookup.putAll(DynamicModuleController.sharedInstance().getModuleNameLookup());
	}
}
