package au.net.webdirector.admin.modules;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.net.webdirector.admin.utils.SecureJSPPath;

@Controller
@RequestMapping("/secure/module")
public class SecureModuleController
{
	@RequestMapping(value = "/iframe/create", method = RequestMethod.GET)
	public String createModule(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.iframePath("createModule");
	}

	@RequestMapping(value = "/iframe/create", method = RequestMethod.POST)
	public String createModuleDo(
			HttpServletRequest request,
			HttpServletResponse response)
	{
		return SecureJSPPath.iframePath("createModule_do");
	}

	@RequestMapping(value = "/iframe/delete", method = RequestMethod.GET)
	public String deleteModule(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.iframePath("deleteModule");
	}

	@RequestMapping(value = "/iframe/delete", method = RequestMethod.POST)
	public String deleteModuleDo(
			HttpServletRequest request,
			HttpServletResponse response)
	{
		return SecureJSPPath.iframePath("deleteModule_do");
	}

	@RequestMapping(value = "/iframe/purge", method = RequestMethod.GET)
	public String purgeModule(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.iframePath("purgeModule");
	}

	@RequestMapping(value = "/iframe/purge", method = RequestMethod.POST)
	public String purgeModuleDo(
			HttpServletRequest request,
			HttpServletResponse response)
	{
		return SecureJSPPath.iframePath("purgeModule_h");
	}

	@RequestMapping(value = "/iframe/purgeDo", method = RequestMethod.POST)
	public String purgeModuleDoFinal(
			HttpServletRequest request,
			HttpServletResponse response)
	{
		return SecureJSPPath.iframePath("purgeModuleFinal");
	}

	@RequestMapping(value = "/configure/settings", method = RequestMethod.GET)
	public String configureModuleSettings(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.path("configureModuleSettings");
	}

	@RequestMapping(value = "/configure/permissions", method = RequestMethod.GET)
	public String configureModulePermissions(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.path("configureModulePermissions");
	}

	@RequestMapping(value = "/configure", method = RequestMethod.GET)
	public String configureModule(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.path("configureModule");
	}

	@RequestMapping(value = "/configure/dropdown/values")
	public String dropdownValues(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.path("dropDownValue");
	}

	@RequestMapping(value = "/configure/sort-attribute")
	public String sortAttributes(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.path("sortAttributes");
	}

	@RequestMapping(value = "/configure/developer/generate-code", method = RequestMethod.GET)
	public String developerGenerateModuleCode(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.path("developer/generateModuleCode");
	}

//	@RequestMapping(value = "/iframe/export-categories")
	public String exportCategories(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.iframePath("exportCategories");
	}

//	@RequestMapping(value = "/iframe/export-elements")
	public String exportElements(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.iframePath("exportElements");
	}

	@RequestMapping(value = "/iframe/export-email", method = RequestMethod.POST)
	public String emailResults(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.iframePath("exportEmail");
	}

	//@RequestMapping(value = "/iframe/import-data")
	public String importData(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.iframePath("data_import_page1");
	}
}
