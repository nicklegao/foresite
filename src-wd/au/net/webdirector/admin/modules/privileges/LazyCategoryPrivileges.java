package au.net.webdirector.admin.modules.privileges;

import java.io.Serializable;

public class LazyCategoryPrivileges implements CategoryPrivileges, Serializable
{
	private static final long serialVersionUID = -3096323870540711188L;

	protected int total;

	public LazyCategoryPrivileges(int privileges)
	{
		this.total = privileges;
	}

	public boolean isInsert()
	{
		return isAll(CREATE);
	}

	public boolean isView()
	{
		return isAll(READ);
	}

	public boolean isEdit()
	{
		return isAll(UPDATE);
	}

	public boolean isDelete()
	{
		return isAll(DELETE);
	}

	public boolean isReset()
	{
		return isAll(RESET);
	}

	public boolean isNotPassDown()
	{
		return isAll(NOT_PASS_DOWN);
	}

	public boolean isAll(int level)
	{
		return (getSum() & level) == level;
	}

	public boolean isAny(int level)
	{
		return (getSum() & level) > 0;
	}

	public int getSum()
	{
		return total;
	}
}
