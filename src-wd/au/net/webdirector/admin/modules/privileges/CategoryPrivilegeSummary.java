package au.net.webdirector.admin.modules.privileges;

import java.util.ArrayList;
import java.util.List;

public class CategoryPrivilegeSummary extends LazyCategoryPrivileges
{
	private static final long serialVersionUID = 5562599128675510937L;

	public CategoryPrivilegeSummary()
	{
		this(0);
	}

	/**
	 * @param privileges
	 */
	public CategoryPrivilegeSummary(int privileges)
	{
		super(privileges);
	}

	String categoryID;
	String parentID;
	int folderLevel;
	String name;
	List<CategoryPrivilegeSummary> children = new ArrayList<CategoryPrivilegeSummary>();

	public String getCategoryID()
	{
		return categoryID;
	}

	public String getParentID()
	{
		return parentID;
	}

	public int getFolderLevel()
	{
		return folderLevel;
	}

	public String getName()
	{
		return name;
	}

	public List<CategoryPrivilegeSummary> getChildren()
	{
		return children;
	}

	public void setCategoryID(String categoryID)
	{
		this.categoryID = categoryID;
	}

	public void setParentID(String parentID)
	{
		this.parentID = parentID;
	}

	public void setFolderLevel(int folderLevel)
	{
		this.folderLevel = folderLevel;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void addChild(CategoryPrivilegeSummary child)
	{
		this.children.add(child);
	}

	public boolean hasChildren()
	{
		return this.children.size() > 0;
	}

	public void setPrivileges(int parseInt)
	{
		this.total = parseInt;
	}

	public int getPrivileges()
	{
		return this.total;
	}
}
