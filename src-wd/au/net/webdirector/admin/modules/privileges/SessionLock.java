/**
 * @author Sushant Verma
 * @date 7 Mar 2016
 */
package au.net.webdirector.admin.modules.privileges;

import java.io.Serializable;

public class SessionLock implements Serializable
{
	private static final long serialVersionUID = 2621411198627077894L;
	private String rand;

	public SessionLock(String rand)
	{
		this.rand = rand;
	}

	public String getRand()
	{
		return rand;
	}

	public void setRand(String rand)
	{
		this.rand = rand;
	}
}
