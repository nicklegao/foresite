package au.net.webdirector.admin.modules.privileges;

public class CachedCategoryPrivileges extends LazyCategoryPrivileges
{
	private static final long serialVersionUID = 7622286018626880089L;

	boolean insert;
	boolean view;
	boolean edit;
	boolean delete;
	boolean notPassDown;
	boolean reset;

	public CachedCategoryPrivileges(int privileges)
	{
		super(privileges);

		this.insert = super.isInsert();
		this.view = super.isView();
		this.edit = super.isEdit();
		this.delete = super.isDelete();
		this.notPassDown = super.isNotPassDown();
		this.reset = super.isReset();
	}

	@Override
	public boolean isInsert()
	{
		return insert;
	}

	public void setInsert(boolean insert)
	{
		this.insert = insert;
	}

	@Override
	public boolean isView()
	{
		return view;
	}

	public void setView(boolean view)
	{
		this.view = view;
	}

	@Override
	public boolean isEdit()
	{
		return edit;
	}

	public void setEdit(boolean edit)
	{
		this.edit = edit;
	}

	@Override
	public boolean isDelete()
	{
		return delete;
	}

	public void setDelete(boolean del)
	{
		this.delete = del;
	}

	@Override
	public boolean isNotPassDown()
	{
		return notPassDown;
	}

	public void setNotPassDown(boolean notPassDown)
	{
		this.notPassDown = notPassDown;
	}

	@Override
	public boolean isReset()
	{
		return reset;
	}

	public void setReset(boolean reset)
	{
		this.reset = reset;
	}
}
