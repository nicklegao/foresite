/**
 * @author Sushant Verma
 * @date 20 Mar 2015
 */
package au.net.webdirector.admin.modules.privileges.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.admin.modules.privileges.CategoryPrivileges;
import au.net.webdirector.admin.modules.privileges.LazyCategoryPrivileges;

public class PermissionMatrix
{
	private static Logger logger = Logger.getLogger(PermissionMatrix.class);

	public final List<String[]> data;

	private Map<String, CategoryPrivileges> cachedCategoryPermissions;
	private Set<String> cachedCategoryVisibility;

	public final int moduleLevels;

	public final String module;

	public final String userID;

	public PermissionMatrix(List<String[]> data, int moduleLevels, String module, String userID)
	{
		this.data = data;
		this.moduleLevels = moduleLevels;
		this.module = module;
		this.userID = userID;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		//Use this for debugging purposes
		StringBuffer sb = new StringBuffer();
		sb.append("Module: " + module + "\n");
		sb.append("ModuleLevels: " + moduleLevels + "\n");
		sb.append("User: " + userID + "\n");
		String colFormat = StringUtils.repeat(" %6s |", (moduleLevels + 1) * 2) + "\n";
		List<String> headerRow = new LinkedList<String>();
		for (int i = 0; i <= moduleLevels; i++)
		{
			headerRow.add("C" + i);
		}
		for (int i = 0; i <= moduleLevels; i++)
		{
			headerRow.add("Priv" + i);
		}
		sb.append("Permission matrix:...\n");

		sb.append(String.format(colFormat, headerRow.toArray(new String[0])));
		for (String[] row : data)
		{
			sb.append(String.format(colFormat, row));
		}

		if (cachedCategoryPermissions != null)
		{
			sb.append("\nCached Category IDs:...\n");

			sb.append(String.format("%5s %6s\n", "ID", "RPCRUD"));
			for (String cid : new TreeSet<String>(cachedCategoryPermissions.keySet()))
			{
				sb.append(String.format("%5s %6s - %s\n",
						cid,
						Integer.toBinaryString(cachedCategoryPermissions.get(cid).getSum()).replace(' ', '0'),
						cachedCategoryPermissions.get(cid).getSum()));
			}
		}
		else
		{
			sb.append("\nNo cached category permissions!");
		}

		if (cachedCategoryVisibility != null)
		{
			sb.append("\nCached Category Visibilities:...\n");

			sb.append(StringUtils.join(cachedCategoryVisibility, ","));
		}
		else
		{
			sb.append("\nNo cached category visibility!");
		}

		return sb.toString();
	}

	public Map<String, CategoryPrivileges> readPermissions()
	{
		return readPermissions(false);
	}

	public Map<String, CategoryPrivileges> readPermissions(boolean forceRead)
	{
		if (forceRead || cachedCategoryPermissions == null)
		{
			cachedCategoryPermissions = new HashMap<String, CategoryPrivileges>();
			CategoryPermissions[] permissionAncestory = new CategoryPermissions[moduleLevels + 1];
			for (String[] permission : data)
			{
				for (int i = 0; i < moduleLevels + 1; i++)
				{
					String currentCategoryID = permission[i];
					int matrixPermission;
					try
					{
						matrixPermission = Integer.parseInt(permission[moduleLevels + 1 + i]);
					}
					catch (NumberFormatException e)
					{
						matrixPermission = 0;
					}

					if (permissionAncestory[i] == null//first data[] row
							|| !permissionAncestory[i].categoryID.equals(currentCategoryID)) //first time category is being read
					{
						int inheritPrivilages;
						boolean hasReset = (matrixPermission & CategoryPrivileges.RESET) == CategoryPrivileges.RESET;

						if (i == 0//reading first column... cant access [i-1]
								|| hasReset)
						{
							inheritPrivilages = 0;
							if (hasReset)
							{
								//Remove reset
								matrixPermission = matrixPermission ^ CategoryPrivileges.RESET;
							}
						}
						else
						{
							inheritPrivilages = permissionAncestory[i - 1].permission;
							boolean parentNotPassDown = (inheritPrivilages & CategoryPrivileges.NOT_PASS_DOWN) == CategoryPrivileges.NOT_PASS_DOWN;
							if (parentNotPassDown)
							{
								inheritPrivilages = 0;
							}
						}

						int derivedPermissions = inheritPrivilages | matrixPermission;
						permissionAncestory[i] = new CategoryPermissions(currentCategoryID, derivedPermissions);
						if (StringUtils.isNotBlank(currentCategoryID))
						{
							cachedCategoryPermissions.put(currentCategoryID, new LazyCategoryPrivileges(derivedPermissions));
						}
					}
				}
			}
		}
		return cachedCategoryPermissions;
	}

	public Set<String> readVisibility()
	{
		return readVisibility(false);
	}

	public Set<String> readVisibility(boolean forceRead)
	{
		if (forceRead || cachedCategoryVisibility == null)
		{
			Map<String, CategoryPrivileges> permissions = readPermissions();

			cachedCategoryVisibility = new HashSet<String>();
			for (String[] permission : data)
			{
				boolean addAncestory = false;
				for (int i = moduleLevels; i >= 0; i--)//RIGHT TO LEFT
				{
					String categoryID = permission[i];
					if (permissions.containsKey(categoryID) && permissions.get(categoryID).isAny(CategoryPrivileges.ALL_CRUD))
					{
						addAncestory = true;
					}
					//else continue looping left though categories
					if (addAncestory && StringUtils.isNotBlank(categoryID))
					{
						cachedCategoryVisibility.add(categoryID);
					}
				}
			}
		}
		return cachedCategoryVisibility;
	}

	private class CategoryPermissions
	{
		public String categoryID;
		public int permission;

		public CategoryPermissions(String categoryID, int setPermission)
		{
			this.categoryID = categoryID;
			this.permission = setPermission;
		}
	}
}
