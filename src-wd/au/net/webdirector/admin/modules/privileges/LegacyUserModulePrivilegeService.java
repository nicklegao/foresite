package au.net.webdirector.admin.modules.privileges;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import webdirector.db.DButils;
import webdirector.db.client.ClientDataAccess;

public class LegacyUserModulePrivilegeService
{
	private static Logger logger = Logger.getLogger(LegacyUserModulePrivilegeService.class);
	ClientDataAccess cda = new ClientDataAccess();
	DButils db = new DButils();

	String moduleName;
	String userID;

	/**
	 * @param moduleName
	 * @param userID
	 */
	public LegacyUserModulePrivilegeService(String moduleName, String userID)
	{
		this.moduleName = moduleName;
		this.userID = userID;
	}

	public CategoryPrivilegeSummary getAllCategoryPrivileges()
	{

		Map<String, CategoryPrivilegeSummary> flatCategoryPrivileges = new HashMap<String, CategoryPrivilegeSummary>();
		CategoryPrivilegeSummary root = new CategoryPrivilegeSummary();
		moduleName = DatabaseValidation.encodeParam(moduleName);
		root.setCategoryID("0");
		root.setFolderLevel(0);
		root.setParentID("0");
		String sqlRoot = "select p.ATTRINTEGER_privileges from user_module_privileges p where p.module_name = ? and p.user_id = ? and p.category_id = '0'";
		//List<String> rootPriv = db.select(sqlRoot);
		List<String> rootPriv = db.selectQuerySingleCol(sqlRoot, new String[] { moduleName, userID });
		if (rootPriv.size() > 0)
		{
			root.setPrivileges(Integer.parseInt(rootPriv.get(0)));
		}
		sqlRoot = "select client_module_name from sys_moduleconfig where internal_module_name = ?";
		//root.setName((String) (db.select(sqlRoot).get(0)));
		root.setName((String) (db.selectQuerySingleCol(sqlRoot, new String[] { moduleName }).get(0)));
		flatCategoryPrivileges.put(root.getCategoryID(), root);
		String sql = "select c.Category_id, c.Category_ParentID, c.folderLevel, c.ATTR_categoryName, ifnull(p.ATTRINTEGER_privileges, 0) as ATTRINTEGER_privileges from categories_" + moduleName + " c left join user_module_privileges p on p.category_id = c.category_id and p.module_name = '" + moduleName + "' and p.user_id = ? order by folderLevel, Category_ParentID, ATTR_categoryName";

		//List<String[]> list = db.select(sql, new String[] { "a", "b", "c", "d", "e" });
		List<String[]> list = db.selectQuery(sql, new String[] { userID });
		for (String[] rs : list)
		{
			if (!flatCategoryPrivileges.containsKey(rs[1]))
			{
				continue;
			}
			CategoryPrivilegeSummary p = new CategoryPrivilegeSummary();
			p.setCategoryID(rs[0]);
			p.setParentID(rs[1]);
			p.setFolderLevel(Integer.parseInt(rs[2]));
			p.setName(rs[3]);
			p.setPrivileges(Integer.parseInt(rs[4]));
			CategoryPrivilegeSummary parent = flatCategoryPrivileges.get(p.getParentID());
			parent.addChild(p);
			flatCategoryPrivileges.put(p.getCategoryID(), p);
		}
		return root;
	}

	public void saveAssignCategories(List<CategoryPrivilegeSummary> privileges)
	{
		logger.info("In SaveAssignCategory Method =======>>>>>>>>>>>>");
		db.updateData("DELETE FROM user_module_privileges where MODULE_NAME=? and USER_ID=?", new Object[] { moduleName, userID });
		logger.info("Ready to insert " + privileges.size() + " privileges =======>>>>>>>>>>>>");
		for (CategoryPrivilegeSummary privilege : privileges)
		{
			Hashtable<String, String> ht = new Hashtable<String, String>();
			ht.put("MODULE_NAME", moduleName);
			ht.put("USER_ID", userID);
			ht.put("CATEGORY_ID", privilege.getCategoryID());
			ht.put("FOLDERLEVEL", String.valueOf(privilege.getFolderLevel()));
			ht.put("ATTRINTEGER_privileges", String.valueOf(privilege.getPrivileges()));
			db.insertData(ht, "ID", "user_module_privileges");
		}
		logger.info("Out SaveAssignCategory Method =======>>>>>>>>>>>>");
		logger.info("Finished =======>>>>>>>>>>>>");
	}

	public CategoryPrivilegeSummary getCategoryPrivilege(String category_id)
	{
		String folderLevel = "0";
		if (!"0".equals(category_id))
		{
			String sql = "select folderLevel from categories_" + DatabaseValidation.encodeParam(moduleName) + " where category_id = ?";
			//folderLevel = (String) db.select(sql).get(0);
			folderLevel = (String) db.selectQuerySingleCol(sql, new String[] { category_id }).get(0);
		}
		String sqlRoot = "select p.category_id, p.ATTRINTEGER_privileges from user_module_privileges p where p.module_name = ? and p.user_id = ?";
		//List<String[]> list = db.select(sqlRoot, new String[] { "a", "b" });
		List<String[]> list = db.selectQuery(sqlRoot, new String[] { moduleName, userID });
		Map<String, Integer> privileges = new HashMap<String, Integer>();
		for (String[] rs : list)
		{
			privileges.put(rs[0], Integer.parseInt(rs[1]));
		}
		return getCategoryPrivilege(category_id, privileges, Integer.parseInt(folderLevel));
	}

	private CategoryPrivilegeSummary getCategoryPrivilege(String category_id, Map<String, Integer> privileges, int folderLevel)
	{
		if (privileges.containsKey(category_id))
		{
			CategoryPrivilegeSummary p = new CategoryPrivilegeSummary();
			p.setCategoryID(category_id);
			p.setPrivileges(privileges.get(category_id));
			return p;
		}
		if ("0".equals(category_id))
		{
			CategoryPrivilegeSummary p = new CategoryPrivilegeSummary();
			p.setCategoryID(category_id);
			p.setPrivileges(CategoryPrivilegeSummary.READ + CategoryPrivilegeSummary.UPDATE + CategoryPrivilegeSummary.DELETE);
			if (folderLevel > 0)
			{
				p.setPrivileges(CategoryPrivilegeSummary.ALL_CRUD);
			}
			return p;
		}
		String sql = "select category_parentid from categories_" + DatabaseValidation.encodeParam(moduleName) + " where category_id = ?";
		//List<String> parent = db.select(sql);
		List<String> parent = db.selectQuerySingleCol(sql, new String[] { category_id });
		return getCategoryPrivilege(parent.get(0), privileges, folderLevel);
	}

	public String drawTree(CategoryPrivilegeSummary treeObj, boolean viewOnly, int folderDepth)
	{
		String ret = "";
		ret += "  <div class=\"ci-tree-item ci-tree-label\">";
		ret += "    <div class=\"ci-tree-item ci-tree-column-name\">";
		for (int i = 0; i < treeObj.getFolderLevel(); i++)
		{
			ret += "<div class=\"ci-tree-item ci-tree-space\"></div>";
		}
		if (treeObj.hasChildren())
		{
			ret += "<div class=\"ci-tree-item ci-tree-icon ci-tree-icon-plus\"></div>";
		}
		ret += "      " + treeObj.getName();
		ret += "    </div>";

		// Checkboxes 
		ret += "    <div class=\"ci-tree-item ci-tree-column-cb\">";
		ret += "      <input type=\"checkbox\" name=\"R_" + treeObj.getFolderLevel() + "_" + treeObj.getCategoryID() + "\" value=\"4\" " + (treeObj.isView() ? "checked" : "") + ">";
		ret += "    </div>";
		if (!viewOnly)
		{
			ret += "    <div class=\"ci-tree-item ci-tree-column-cb\">";
			ret += "      <input type=\"checkbox\" name=\"C_" + treeObj.getFolderLevel() + "_" + treeObj.getCategoryID() + "\" value=\"8\" " + (treeObj.isInsert() ? "checked" : "") + ">";
			ret += "    </div>";
			ret += "    <div class=\"ci-tree-item ci-tree-column-cb\">";
			ret += "      <input type=\"checkbox\" name=\"U_" + treeObj.getFolderLevel() + "_" + treeObj.getCategoryID() + "\" value=\"2\" " + (treeObj.isEdit() ? "checked" : "") + ">";
			ret += "    </div>";
			ret += "    <div class=\"ci-tree-item ci-tree-column-cb\">";
			ret += "      <input type=\"checkbox\" name=\"D_" + treeObj.getFolderLevel() + "_" + treeObj.getCategoryID() + "\" value=\"1\" " + (treeObj.isDelete() ? "checked" : "") + ">";
			ret += "    </div>";
		}

		if (treeObj.getFolderLevel() != folderDepth)
		{
			ret += "    <div class=\"ci-tree-item ci-tree-column-cb\">";
			ret += "      <input type=\"checkbox\" name=\"P_" + treeObj.getFolderLevel() + "_" + treeObj.getCategoryID() + "\" value=\"16\" " + (treeObj.isNotPassDown() ? "checked" : "") + ">";
			ret += "    </div>";
		}
		else
		{
			ret += "    <div class=\"ci-tree-item ci-tree-column-cb\">";
			ret += "      <i class=\"fa fa-times-circle text-danger\"></i>";
			ret += "      <input type=\"checkbox\" style=\"display:none\" name=\"P_" + treeObj.getFolderLevel() + "_" + treeObj.getCategoryID() + "\" value=\"16\" " + (treeObj.isNotPassDown() ? "checked" : "") + ">";
			ret += "    </div>";
		}

		if (!treeObj.getCategoryID().equals("0"))
		{
			ret += "    <div class=\"ci-tree-item ci-tree-column-cb\">";
			ret += "      <input type=\"checkbox\" name=\"X_" + treeObj.getFolderLevel() + "_" + treeObj.getCategoryID() + "\" value=\"32\" " + (treeObj.isReset() ? "checked" : "") + ">";
			ret += "    </div>";
		}
		else
		{
			ret += "    <div class=\"ci-tree-item ci-tree-column-cb\">";
			ret += "      <i class=\"fa fa-times-circle text-danger\"></i>";
			ret += "      <input type=\"checkbox\" style=\"display:none\" name=\"X_" + treeObj.getFolderLevel() + "_" + treeObj.getCategoryID() + "\" value=\"32\" " + (treeObj.isReset() ? "checked" : "") + ">";
			ret += "    </div>";
		}

		ret += "  </div>";
		ret += "  <div class=\"ci-tree-item ci-tree-group\" style=\"display:none;\">";
		for (CategoryPrivilegeSummary child : treeObj.getChildren())
		{
			ret += drawTree(child, viewOnly, folderDepth);
		}
		ret += "  </div>";
		return ret;
	}

	public List<CategoryPrivilegeSummary> parsePrivileges(HttpServletRequest request)
	{
		Map<String, CategoryPrivilegeSummary> privileges = new LinkedHashMap<String, CategoryPrivilegeSummary>();
		Enumeration en = request.getParameterNames();
		while (en.hasMoreElements())
		{
			String name = (String) en.nextElement();
			if (!name.startsWith("C_") && !name.startsWith("R_") && !name.startsWith("U_") && !name.startsWith("D_") && !name.startsWith("P_") && !name.startsWith("X_"))
			{
				continue;
			}
			String categoryID = name.substring(4);
			int folderLevel = Integer.parseInt(name.substring(2, 3));
			CategoryPrivilegeSummary privilege = null;
			if (privileges.containsKey(categoryID))
			{
				privilege = privileges.get(categoryID);
			}
			else
			{
				privilege = new CategoryPrivilegeSummary();
				privilege.setCategoryID(categoryID);
				privilege.setFolderLevel(folderLevel);
				privileges.put(categoryID, privilege);
			}
			privilege.setPrivileges(privilege.getPrivileges() + Integer.parseInt(request.getParameter(name)));
		}
		return new ArrayList<CategoryPrivilegeSummary>(privileges.values());
	}

	public String getJsonPrivilege(String id, String currentLevel, String selectedLevel, String userLevel)
	{
		String categoryID = id;
		boolean canConfigModule = false;
		boolean canRefreshModule = true;

		boolean canInsert = true;
		boolean canDelete = true;
		boolean canEdit = true;
		boolean canSort = true;
		boolean canCut = true;
		boolean canCopy = true;
		boolean canPaste = false;

		boolean canExport = false;
		boolean canImport = false;

		boolean canBulkUpload = false;

		if (currentLevel == null || selectedLevel == null)
		{
			return output(false, false, false, false, false, false, false, false, false, false, false, false);
		}
		int moduleLevel = Defaults.getModuleLevelsInt(moduleName);
		if (Integer.parseInt(currentLevel) > moduleLevel)
		{
			categoryID = new ClientDataAccess().getCategoryFromElement(moduleName, id);
		}

		if (moduleName.equals("REFERENCELIB") || moduleName.equals("DOWNLOADS"))
		{
			canBulkUpload = true;
		}
		if (!userLevel.equals("1") && !userLevel.equals("4"))
		{
			return output(false, true, false, false, false, false, false, false, false, false, false, false);
		}
		if (!selectedLevel.equals(""))
		{
			int sl = Integer.parseInt(selectedLevel);
			int cl = Integer.parseInt(currentLevel);
			canPaste = (sl == cl);
		}
		if (userLevel.equals("1"))
		{
			canConfigModule = true;
			canExport = true;
			canImport = true;
		}
		else
		{
			CategoryPrivilegeSummary privilege = getCategoryPrivilege(categoryID);
			canInsert = privilege.isInsert();
			canEdit = privilege.isEdit();
			canDelete = privilege.isDelete();
			canCut = privilege.isDelete();
			canPaste = privilege.isInsert();
		}
		return output(canConfigModule, canRefreshModule, canInsert, canDelete, canEdit, canSort, canCut, canCopy, canPaste, canExport, canImport, canBulkUpload);

	}

	private String output(boolean canConfigModule, boolean canRefreshModule, boolean canInsert, boolean canDelete, boolean canEdit, boolean canSort, boolean canCut, boolean canCopy, boolean canPaste, boolean canExport, boolean canImport, boolean canBulkUpload)
	{
		String ret = "{";
		ret += "\"canConfigModule\":" + canConfigModule + ",";
		ret += "\"canRefreshModule\":" + canRefreshModule + ",";
		ret += "\"canInsert\":" + canInsert + ",";
		ret += "\"canDelete\":" + canDelete + ",";
		ret += "\"canEdit\":" + canEdit + ",";
		ret += "\"canSort\":" + canSort + ",";
		ret += "\"canCut\":" + canCut + ",";
		ret += "\"canCopy\":" + canCopy + ",";
		ret += "\"canPaste\":" + canPaste + ",";
		ret += "\"canExport\":" + canExport + ",";
		ret += "\"canImport\":" + canImport + ",";
		ret += "\"canBulkUpload\":" + canBulkUpload;
		ret += "}";
		return ret;
	}
}
