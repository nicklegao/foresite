package au.net.webdirector.admin.modules.privileges;

public interface CategoryPrivileges
{
	public static final int RESET = 32;
	public static final int NOT_PASS_DOWN = 16;
	/*
	 * CRUD
	 */
	public static final int CREATE = 8;
	public static final int READ = 4;
	public static final int UPDATE = 2;
	public static final int DELETE = 1;
	public static final int ALL_CRUD = CREATE + READ + UPDATE + DELETE;

	public static final int NONE = 0;

	public boolean isInsert();

	public boolean isView();

	public boolean isEdit();

	public boolean isDelete();

	public boolean isReset();

	public boolean isNotPassDown();

	public boolean isAll(int level);

	public boolean isAny(int level);

	public int getSum();
}
