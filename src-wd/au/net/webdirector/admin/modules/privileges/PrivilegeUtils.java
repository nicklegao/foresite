package au.net.webdirector.admin.modules.privileges;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.ci.webdirector.user.UserManager;
import au.net.webdirector.admin.modules.privileges.model.PermissionMatrix;
import au.net.webdirector.admin.security.LoginController;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;

public class PrivilegeUtils implements Serializable
{
	private static final long serialVersionUID = -1331128202994037956L;

	private static Logger logger = Logger.getLogger(PrivilegeUtils.class);

	private int moduleLevels;
	private String module;
	private String userID;

	Map<String, CategoryPrivileges> categoryPrivilage;
	Set<String> cachedCategoryVisibility;

	private static PrivilegeUtils getInstance(String module, String userID)
	{
		return new PrivilegeUtils(module, userID);
	}

	public PrivilegeUtils(String module, String userID)
	{
		int moduleLevels = Defaults.getInstance().getModuleLevelsInt(module);

		this.moduleLevels = moduleLevels;
		this.module = module;
		this.userID = userID;
	}

	public String getModule()
	{
		return module;
	}

	public void setModule(String module)
	{
		this.module = module;
	}

	public String getUserID()
	{
		return userID;
	}

	public void setUserID(String userID)
	{
		this.userID = userID;
	}

	public boolean isUnrestricted()
	{
		return UserManager.getUserType(userID) == UserManager.ADMIN;
	}

	public Set<String> categoriesWithPrivilages(int permissions)
	{
		cacheAllPrivileges();

		Set<String> matches = new HashSet<String>();
		for (String categoryID : categoryPrivilage.keySet())
		{
			CategoryPrivileges priv = categoryPrivilage.get(categoryID);
			if (priv.isAll(permissions))
			{
				matches.add(categoryID);
			}
		}
		return matches;
	}

	private PermissionMatrix generatePermissionMatrix()
	{
		StringBuffer innerSQL = new StringBuffer();
		StringBuffer outerSQL = new StringBuffer();
		List<String> queryParams = new LinkedList<String>();

		/*
		 * STEP 1...
		 * Combine the category ID columns
		 * 
		 * IE... Transform this category tree
		 * X
		 * |\
		 * | 1
		 * | |\
		 * | | 2
		 * | | |\
		 * | | | 3
		 * | |  \
		 * | |   4
		 * | \
		 * |  9
		 *  \
		 *   5
		 *    \
		 *     6
		 *     |\
		 *     | 7
		 *      \
		 *       8
		 * 
		 * .. Into this...
		 * c0, c1, c2, c3
		 * 0   1   2   3
		 * 0   1   2   4
		 * 0   9   *   *
		 * 0   5   6   7
		 * 0   5   6   8
		 * 
		 * NOTES:
		 * - To cater for incomplete trees (eg id=9), we use left join to do this.
		 */

		List<String> cols = new LinkedList<String>();
		for (int i = 0; i <= moduleLevels; ++i)
		{
			cols.add("cat" + i + ".c" + i);
		}
		for (int i = 0; i <= moduleLevels; ++i)
		{
			cols.add("modPriv" + i + ".ATTRINTEGER_privileges as priv" + i);
		}
		innerSQL.append(" SELECT ").append(StringUtils.join(cols, ","));
		innerSQL.append(" FROM ");

		//Dummy table for top level
		innerSQL.append(" (SELECT 0 as c0) cat0 ");

		for (int i = 1; i <= moduleLevels; ++i)
		{
			/*
			 * left join 
			 * (
			 *   select Category_id as c1,
			 *   Category_ParentID as cp1
			 *   from categories_PRODUCTS
			 *   where folderLevel=1
			 * ) as cat1
			 * ON cat0.c0 = cat1.cp1
			 */
			innerSQL.append(" LEFT JOIN ( ")
					.append(" SELECT Category_id as c" + i + ", ")
					.append(" Category_ParentID as cp" + i)
					.append(" from categories_" + module)
					.append(" where folderLevel=" + i)
					.append(" ) as cat" + i)
					.append(" ON cat" + (i - 1) + ".c" + (i - 1) + " = cat" + i + ".cp" + i);
		}

		/*
		 * STEP 2...
		 * Now merge the set permissions for each category. Bitwise permissions...
		 * - C = 4 = 1000
		 * - R = 3 = 0100
		 * - U = 2 = 0010
		 * - D = 1 = 0001
		 * 
		 * Table now becomes something like this...
		 * c0 | c1 | c2 | c3 | priv0 | priv1 | priv2 | priv3
		 * 0    1    2    3    *       12      *       *
		 * 0    1    2    4    *       12      2       *
		 * 0    9    *    *    *       12      2       *
		 * 0    5    6    7    *       12      *       *
		 * 0    5    6    8    *       12      10      *
		 */

		for (int i = 0; i <= moduleLevels; ++i)
		{
			/*
			 * left join 
			 * (
			 *   select ATTRINTEGER_privileges, CATEGORY_ID
			 *   from user_module_privileges
			 *   where MODULE_NAME = 'PRODUCTS'
			 *   and folderLevel=0
			 *   and USER_ID=12
			 * ) as modPriv0
			 * ON modPriv0.CATEGORY_ID = cat0.c0
			 */
			innerSQL.append(" LEFT JOIN ( ")
					.append(" SELECT ATTRINTEGER_privileges, ")
					.append(" CATEGORY_ID")
					.append(" from user_module_privileges")
					.append(" where MODULE_NAME='" + module + "'")
					.append(" and folderLevel=" + i)
					.append(" and USER_ID=" + userID)
					.append(" ) as modPriv" + i)
					.append(" ON modPriv" + i + ".CATEGORY_ID = cat" + i + ".c" + i);
		}

		/*
		 * STEP 3...
		 * Nest the inner query into a outer query that:
		 *  1) filters for the desired permission level
		 *  2) orders the results
		 *  
		 */
		List<String> orderByClause = new LinkedList<String>();
		for (int i = 0; i <= moduleLevels; ++i)
		{
			orderByClause.add("results.priv" + i + " desc");
			orderByClause.add((i + 1) + " asc");
		}

		outerSQL.append("SELECT * FROM (")
				.append(innerSQL)
				.append(") as results")
				.append(" ORDER BY " + StringUtils.join(orderByClause, ","));

		/*
		 * Step 4... Go grab a tea!
		 */
		try
		{
			List<String[]> result = TransactionDataAccess.getInstance()
					.selectStringArrays(outerSQL.toString(), queryParams.toArray(new String[0]));

			return new PermissionMatrix(result, moduleLevels, module, userID);
		}
		catch (SQLException e)
		{
			logger.fatal("UNABLE TO FETCH PERMISSIONS", e);
		}

		return new PermissionMatrix(new LinkedList<String[]>(), moduleLevels, module, userID);
	}

	private void cacheAllPrivileges()
	{
		cacheAllPrivileges(false);
	}

	private void cacheAllPrivileges(boolean forceReload)
	{
		if (categoryPrivilage == null || forceReload)
		{
			synchronized (this)
			{
				categoryPrivilage = new HashMap<String, CategoryPrivileges>();

				PermissionMatrix permissionMatrix = generatePermissionMatrix();
				cachedCategoryVisibility = permissionMatrix.readVisibility();

				recordMatrixPrivilage(permissionMatrix);
			}
		}
	}

	private void recordMatrixPrivilage(PermissionMatrix matrix)
	{
		categoryPrivilage.putAll(matrix.readPermissions());
		//logger.info(matrix);//Enable to see debugging
	}

	public CategoryPrivileges getPrivilegeForCategory(String categoryID)
	{
		if (isUnrestricted() == false)
		{
			cacheAllPrivileges();

			if (categoryPrivilage.containsKey(categoryID))
			{
				int current = categoryPrivilage.get(categoryID).getSum();
				return new CachedCategoryPrivileges(current);
			}
			return new CachedCategoryPrivileges(CategoryPrivileges.NONE);
		}

		return new CachedCategoryPrivileges(CategoryPrivileges.ALL_CRUD);
	}

	public CategoryPrivileges getPrivilegeForElement(String elementID)
	{
		try
		{
			ElementData ed = ModuleAccess.getInstance().getElementById(module, elementID);

			if (ed != null)
			{
				return getPrivilegeForCategory(ed.getParentId());
			}
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
		}

		return new CachedCategoryPrivileges(CategoryPrivileges.NONE);
	}

	public Set<String> getCategoryVisibility()
	{
		return cachedCategoryVisibility;
	}

	public static final String SESSION_PRIV_OBJS = "SESSION_PRIV_OBJ";
	public static final String SESSION_PRIV_LOCKS = "SESSION_PRIV_LOCK";

	public static PrivilegeUtils getCachedInstance(String moduleName, HttpSession session, boolean forceReload)
	{
		Map<String, SessionLock> sessionLocks = (Map<String, SessionLock>) session.getAttribute(SESSION_PRIV_LOCKS);
		Map<String, PrivilegeUtils> privlagesCache = (Map<String, PrivilegeUtils>) session.getAttribute(SESSION_PRIV_OBJS);

		//Init the required maps for managing locks
		if (sessionLocks == null || privlagesCache == null)
		{
			synchronized (PrivilegeUtils.class)
			{
				sessionLocks = (Map<String, SessionLock>) session.getAttribute(SESSION_PRIV_LOCKS);
				privlagesCache = (Map<String, PrivilegeUtils>) session.getAttribute(SESSION_PRIV_OBJS);

				if (sessionLocks == null)
				{
					sessionLocks = Collections.synchronizedMap(new HashMap<String, SessionLock>());
					session.setAttribute(SESSION_PRIV_LOCKS, sessionLocks);
				}

				if (privlagesCache == null)
				{
					privlagesCache = Collections.synchronizedMap(new HashMap<String, PrivilegeUtils>());
					session.setAttribute(SESSION_PRIV_OBJS, privlagesCache);
				}
			}
		}

		//Create the lock (if needed)
		SessionLock lock = sessionLocks.get(moduleName);
		if (lock == null)
		{
			synchronized (PrivilegeUtils.class)
			{
				lock = sessionLocks.get(moduleName);
				if (lock == null)
				{
					lock = new SessionLock(UUID.randomUUID().toString());
					sessionLocks.put(moduleName, lock);
					session.setAttribute(SESSION_PRIV_LOCKS, sessionLocks);
				}
			}
		}

		PrivilegeUtils privilegeUtils = privlagesCache.get(moduleName);
		if (privilegeUtils == null || forceReload == true)
		{
			synchronized (lock)
			{
				privilegeUtils = privlagesCache.get(moduleName);
				if (privilegeUtils == null || forceReload == true)
				{
					String userId = (String) session.getAttribute(LoginController.WD_USER_ID);
					privilegeUtils = PrivilegeUtils.getInstance(moduleName, userId);
					privilegeUtils.cacheAllPrivileges();

					privlagesCache.put(moduleName, privilegeUtils);
					session.setAttribute(SESSION_PRIV_OBJS, privlagesCache);
				}
			}
		}
		return privilegeUtils;
	}

	public void setPrivlageOnCategory(int setPrivilage, String... categoryIDs)
	{
		TransactionDataAccess da = TransactionDataAccess.getInstance(false);
		TransactionManager tm = TransactionManager.getInstance(da);

		try
		{

			setPrivlageOnCategory(tm, setPrivilage, categoryIDs);

			tm.commit();
			logger.info("Privilages updated successfully... Reloading privilages");
			cacheAllPrivileges(true);
		}
		catch (Exception e)
		{
			logger.fatal("Unable to set privilages for category. ROLLBACK", e);
			tm.rollback();
		}
	}

	public void setPrivlageOnCategory(TransactionManager tm, int setPrivilage, String... categoryIDs) throws SQLException
	{

		TransactionDataAccess da = tm.getDataAccess();
		for (String categoryID : categoryIDs)
		{
			//Delete the existing category privilage (if any)
			int deleted = da.update("DELETE FROM user_module_privileges "
					+ " where MODULE_NAME=? "
					+ " and USER_ID=? "
					+ " and CATEGORY_ID=?", new Object[] {
							module,
							userID,
							categoryID });
			if (deleted > 0)
				logger.warn("Overwritting privilages for category_ID" + categoryID + " to " + setPrivilage);

			//Now insert the new row
			String folderLevel = da.selectString("select folderLevel from categories_" + module + " where CATEGORY_ID = ?", categoryID);

			da.insert("INSERT INTO user_module_privileges "
					+ "(MODULE_NAME, CATEGORY_ID, USER_ID, ATTRINTEGER_privileges, folderLevel)"
					+ " VALUES (?, ?, ?, ?, ?)",
					module,
					categoryID,
					userID,
					Integer.toString(setPrivilage),
					folderLevel);
		}
	}
}
