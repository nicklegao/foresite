/**
 * 6:59:29 PM 12/01/2012
 * @author Vito Lefemine
 */
package au.net.webdirector.admin.modules;

import java.sql.Connection;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;

/**
 * @author Vito Lefemine
 *
 */
@Controller
@RequestMapping("labels")
public class LabelsController
{

	static Logger logger = Logger.getLogger(LabelsController.class);

	@RequestMapping("/editLabelExternalName")
	@ResponseBody
	public String editAttributeExternalName(@RequestParam("labelInternalName") String labelInternalName,
			@RequestParam("labelExternalName") String labelExternalName, @RequestParam("type") String type
			, HttpServletRequest request)
	{
		Connection conn = null;
		String returnVal = "";
		String tableName = "";

		if (type.equalsIgnoreCase("elements"))
		{
			tableName = "Labels_";
		}
		else if (type.equalsIgnoreCase("category"))
		{
			tableName = "categoryLabels_";
		}
		else
		{
			return "no type mapping!";
		}
		if (null == labelInternalName || labelInternalName.trim().equalsIgnoreCase(""))
			return "no key specified!";

		try
		{
			DBaccess dba = new DBaccess();
			String moduleName = (String) request.getSession().getAttribute("moduleSelected");
			if (null != moduleName && !moduleName.trim().equalsIgnoreCase(""))
			{
				Vector<Object> param = new Vector<Object>(2);
				param.add(labelExternalName);
				param.add(labelInternalName);

				String sql = "update " + tableName + moduleName + " set Label_ExternalName = ? where Label_internalName= ? ";

				int changes = dba.updateData(sql, param.toArray());

				if (changes > 0)
				{
					returnVal = labelExternalName;
				}
			}
			else
			{
				returnVal = "error";
			}
		}
		catch (Exception e)
		{
			logger.error(e.getMessage(), e);
			returnVal = "error";
		}
		return returnVal;

	}

	@RequestMapping("/editLabelTab")
	@ResponseBody
	public String editTabName(@RequestParam("labelInternalName") String labelInternalName,
			@RequestParam("tabName") String tabName, @RequestParam("type") String type
			, HttpServletRequest request)
	{

		String tableName = "";

		if (type.equalsIgnoreCase("elements"))
		{
			tableName = "Labels_";
		}
		else if (type.equalsIgnoreCase("category"))
		{
			tableName = "categoryLabels_";
		}
		else
		{
			return "no type mapping!";
		}
		if (null == labelInternalName || labelInternalName.trim().equalsIgnoreCase(""))
			return "no key specified!";

		Connection conn = null;
		String retValue = "";
		try
		{
			logger.debug("Exception tabName: " + tabName + " labelInternalName:" + labelInternalName);
			DBaccess dba = new DBaccess();
			String moduleName = (String) request.getSession().getAttribute("moduleSelected");
			if (null != moduleName && !moduleName.trim().equalsIgnoreCase(""))
			{
				Vector<Object> param = new Vector<Object>(2);
				param.add(tabName);
				param.add(labelInternalName);

				String sql = "update " + tableName + moduleName + " set Label_TabName = ? where Label_internalName= ? ";

				int changes = dba.updateData(sql, param.toArray());
				if (changes > 0)
				{
					retValue = tabName;
				}
			}
			else
			{
				retValue = "error";
			}
		}
		catch (Exception e)
		{
			logger.error(e.getMessage(), e);
			retValue = "error";
		}
		return retValue;
	}

	@RequestMapping("/updateSortingOrder")
	@ResponseBody
	public void updateSortingOrder(@RequestParam String orderString, @RequestParam String moduleName, @RequestParam String moduleType)
	{
		SortItemDAO dao = new SortItemDAO();
		dao.updateSortingOrder(orderString, moduleName, moduleType);
	}

}
