package au.net.webdirector.admin.modules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import webdirector.db.DButils;

public class SortItemDAO
{

	protected DButils dbu = new DButils();

	public boolean updateSortingOrder(String orderString, String moduleName, String moduleType)
	{
		System.out.println("Order String " + orderString);
		setOrderInModule(parseTheOrderString(orderString), moduleName, moduleType);
		return false;
	}

	public Map<String, String> parseTheOrderString(String orderString)
	{
		Map<String, String> mapOrder = new HashMap<String, String>();
		StringTokenizer str = new StringTokenizer(orderString, "|");
		int k = 1;
		while (str.hasMoreTokens())
		{
			String layerString = str.nextToken();
			String orderAndEntity = layerString.substring(layerString.indexOf("lyr") + 3, layerString.length());
//			String order = orderAndEntity.substring(0, orderAndEntity.indexOf("T"));
			String entity = orderAndEntity.substring(orderAndEntity.indexOf("T") + 1, orderAndEntity.length());
			mapOrder.put(entity, new Integer(k).toString());
			k++;
		}
		return mapOrder;
	}

	public void setOrderInModule(Map<String, String> mapOfOrder, String moduleName, String moduleType)
	{
		Iterator<?> it = mapOfOrder.keySet().iterator();
		while (it.hasNext())
		{
			String entityId = (String) it.next();
			String value = (String) mapOfOrder.get(entityId);
			String whereClause = null;
			String tableName = null;
			System.out.println("moduleType " + moduleType);
			//Object [] params = new Object[]{};
			List<String> params = new ArrayList<String>();
			//params[0] = value;
			params.add(value);

			if ("Category".equals(moduleType))
			{
				whereClause = "Category_id= ?";
				//params[1] = entityId;
				params.add(entityId);
				tableName = "CATEGORIES_" + moduleName;
			}
			else if ("labels".equals(moduleType))
			{
				whereClause = "Label_id= ?";
				//params[1] =entityId;
				params.add(entityId);
				tableName = "labels_" + moduleName;
			}
			else if ("categorylabels".equals(moduleType))
			{
				whereClause = "Label_id= ?";
				//params[1] = entityId;
				params.add(entityId);
				tableName = "categorylabels_" + moduleName;
			}
			else
			{
				whereClause = "Element_id=?";
				//params[1] =entityId;
				params.add(entityId);
				tableName = "ELEMENTS_" + moduleName;
			}
			String query = "UPDATE " + tableName + " SET display_order=? WHERE " + whereClause;
			System.out.println(query);
			dbu.updateData(query, params.toArray());
		}
	}
}