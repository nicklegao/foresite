/**
 * 
 */
package au.net.webdirector.admin.modules;

import java.io.File;
import java.sql.SQLException;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import org.apache.log4j.Logger;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;

/**
 * Date: 9/01/2014
 * 
 * @author sushant
 * 
 */
public class DynamicModuleController
{
	private static final long serialVersionUID = -1826664697483546076L;

	private Logger logger = Logger.getLogger(DynamicModuleController.class);

	private static final String INTERNAL_TEMPLATE_NAME = "template";

	public static DynamicModuleController sharedInstance()
	{
		return new DynamicModuleController();
	}

	private DBaccess db = new DBaccess();

	private DynamicModuleController()
	{
	}

	public Map<String, String> getModulesForUser(String userId, TransactionDataAccess da)
	{
		try
		{
			List<String[]> modules = da.selectStringArrays("SELECT internal_module_name, client_module_name " +
					"FROM users u, sys_usermodules um, sys_moduleconfig m " +
					"WHERE u.user_id = um.user_id " +
					"AND um.module_id = m.id " +
					"AND u.user_id = ?", new String[] { userId });
			return genLookupMap(modules);
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return new TreeMap<String, String>();
		}

	}

	public Map<String, String> getModulesForUser(String userId)
	{
		@SuppressWarnings("unchecked")
		Vector<String[]> modules = db.selectQuery("SELECT internal_module_name, client_module_name " +
				"FROM users u, sys_usermodules um, sys_moduleconfig m " +
				"WHERE u.user_id = um.user_id " +
				"AND um.module_id = m.id " +
				"AND u.user_id = ?", new String[] { userId });

		return genLookupMap(modules);
	}

	public Map<String, String> getDeletableModuleNames()
	{
		@SuppressWarnings("unchecked")
		Vector<String[]> modules = db.selectQuery("SELECT internal_module_name, client_module_name FROM sys_moduleconfig where delete_allowed=1 and hidden_module = 0");

		return genLookupMap(modules);
	}

	public Map<String, String> getModuleNameLookup()
	{
		@SuppressWarnings("unchecked")
//		Vector<String[]> modules = select("SELECT internal_module_name, client_module_name " +
//				"FROM sys_moduleconfig", new String[]{});
				Vector<String[]> modules = db.selectQuery("SELECT internal_module_name, client_module_name FROM sys_moduleconfig where hidden_module = 0 order by client_module_name");

		return genLookupMap(modules);
	}

//	public Map<String, String> getDeletableModuleNames()
//	{
//		@SuppressWarnings("unchecked")
//		Vector<String[]> modules = db.selectQuery("SELECT internal_module_name, client_module_name FROM sys_moduleconfig where delete_allowed=1");
//
//		return genLookupMap(modules);
//	}
//
//	public Map<String, String> getModuleNameLookup()
//	{
//		@SuppressWarnings("unchecked")
////		Vector<String[]> modules = select("SELECT internal_module_name, client_module_name " +
////				"FROM sys_moduleconfig", new String[]{});
//		Vector<String[]> modules = db.selectQuery("SELECT internal_module_name, client_module_name FROM sys_moduleconfig order by client_module_name");
//
//		return genLookupMap(modules);
//	}

	private Map<String, String> genLookupMap(List<String[]> modules)
	{
		LinkedHashMap<String, String> matched = new LinkedHashMap<String, String>();
		for (String[] cols : modules)
		{
			matched.put(cols[0], cols[1]);
		}
		return matched;
	}

	public void assignModulesToUser(String[] modules, String userId, boolean clearDataFirst)
	{
		TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false));
		try
		{
			assignModulesToUser(modules, userId, clearDataFirst, txManager);
		}
		catch (Exception e)
		{
			txManager.rollback();
		}
		finally
		{
			txManager.commit();
		}
	}

	public void assignModulesToUser(String[] modules, String userId, boolean clearDataFirst, TransactionManager txManager) throws SQLException
	{
		TransactionDataAccess da = txManager.getDataAccess();
		if (clearDataFirst)
		{
			da.update("delete from sys_usermodules where user_id = ?", new Object[] { userId });
		}

		Map<String, String> currentModules = getModulesForUser(userId, da);

		for (String internalName : modules)
		{
			if (currentModules.containsKey(internalName))
			{
				//This module is already assigned to this user.
				continue;
			}
			String moduleId = da.selectString("select id from sys_moduleconfig where internal_module_name= ? ", new String[] { internalName });
			if (moduleId != null)
			{
				DataMap record = new DataMap();
				record.put("user_id", userId);
				record.put("module_id", moduleId);
				da.insert(record, "sys_usermodules");
				//make sure we dont add it again
				currentModules.put(internalName, "NEW");
			}
		}
	}

	/**
	 * @param internalName
	 * @param displayName
	 * @return error description if found
	 */
	public String checkPreconditionsForModuleCreation(String internalName, String displayName, String cloneTarget)
	{
		//Vector exists = select("SELECT client_module_name FROM sys_moduleconfig where internal_module_name = '"+internalName+"'");
		Vector exists = db.selectQuerySingleCol("SELECT client_module_name FROM sys_moduleconfig where internal_module_name = ?", new String[] { internalName });
		if (exists.size() > 0)
		{
			return "Existing module with friendly name '" + exists.get(0) + "' already exists. Please choose a new internal name.";
		}
		else
		{
			try
			{
				//exists = select("SELECT * FROM labels_"+internalName);
				exists = db.selectQuerySingleCol("SELECT * FROM labels_" + DatabaseValidation.encodeParam(internalName));
				if (exists.size() > 0)
				{
					//Shouldnt be able to select data.
					return "Internal data inconsistency. Please contact support with error code 140106-1";
				}
			}
			catch (Exception e)
			{
				//all good!
			}
		}

		if (cloneTarget != null)
		{
			//Vector cloneTargetExists = select("SELECT client_module_name FROM sys_moduleconfig where internal_module_name = '"+cloneTarget+"'");
			Vector cloneTargetExists = db.selectQuerySingleCol("SELECT client_module_name FROM sys_moduleconfig where internal_module_name = ?", new String[] { cloneTarget });
			if (cloneTargetExists.size() == 0)
			{
				return "Cant find clone target with name " + cloneTarget;
			}
			else if (cloneTargetExists.size() > 1)
			{
				return "Internal data inconsistency. Please contact support with error code 140108-2";
			}
		}

		return null;
	}

	/**
	 * @param deleteTarget
	 * @return error description if found
	 */
	public String checkPreconditionsForModuleDeletion(String deleteTarget)
	{
		//Vector exists = select("SELECT client_module_name FROM sys_moduleconfig where internal_module_name = '"+deleteTarget+"'");
		Vector exists = db.selectQuerySingleCol("SELECT client_module_name FROM sys_moduleconfig where internal_module_name = ? ", new String[] { deleteTarget });
		if (exists.size() == 0)
		{
			return "Internal data inconsistency. Please contact support with error code 140108-3. Unable to find module with internal name: " + deleteTarget;
		}
		return null;
	}

	public String purgeModule(String target)
	{
		String[] tables = new String[] {
				"elements_" + target,
				"categories_" + target,
				"elements_" + target + "_draft",
				"categories_" + target + "_draft",
				"elements_" + target + "_ver",
				"categories_" + target + "_ver" };

		logger.info("Truncating tables ...");
		for (String table : tables)
		{
			try
			{
				db.updateData("truncate table " + table);
			}
			catch (Exception e)
			{
				logger.fatal("Unable to truncate table " + table, e);
				return "Unable to truncate table " + table;
			}
		}

		logger.info("Deleting drop_down_multi_selections");
		try
		{
			db.updateData("delete from drop_down_multi_selections where MODULE_NAME=? ", new Object[] { target });
			db.updateData("delete from drop_down_multi_selections_ver where MODULE_NAME=? ", new Object[] { target });
			db.updateData("delete from drop_down_multi_selections_draft where MODULE_NAME=? ", new Object[] { target });
		}
		catch (Exception e)
		{
			logger.fatal("Unable to delete from drop_down_multi_selections", e);
			return "Unable to delete from drop_down_multi_selections";
		}

		logger.info("Deleting notes");
		try
		{
			db.updateData("delete from notes where MODULENAME=? ", new Object[] { target });
			db.updateData("delete from notes_ver where MODULENAME=? ", new Object[] { target });
			db.updateData("delete from notes_draft where MODULENAME=? ", new Object[] { target });
		}
		catch (Exception e)
		{
			logger.fatal("Unable to delete from notes", e);
			return "Unable to delete from notes";
		}

		logger.info("Deleting from sys_draft_rec table");
		try
		{
			db.updateData("delete from sys_draft_rec where D_MODULE=? ", new Object[] { target });
		}
		catch (Exception e)
		{
			logger.fatal("Unable to delete from sys_draft_rec", e);
			return "Unable to delete from sys_draft_rec";
		}

		logger.info("Deleting from sys_version_rec table");
		try
		{
			db.updateData("delete from sys_version_rec where V_MODULE=? ", new Object[] { target });
		}
		catch (Exception e)
		{
			logger.fatal("Unable to delete from sys_version_rec", e);
			return "Unable to delete from sys_version_rec";
		}
		logger.info("Deleting from stores");
		try
		{
			Defaults.getInstance().nuke(new File(Defaults.getInstance().getStoreDir(), target));
		}
		catch (Exception e)
		{
			logger.fatal("Unable to delete from stores", e);
			return "Unable to delete from stores";
		}
		return null;
	}

	public String deleteModule(String target)
	{
		purgeModule(target);
		String[] tables = new String[] {
				"elements_" + target,
				"categories_" + target,
				"labels_" + target,
				"categorylabels_" + target,
				"elements_" + target + "_draft",
				"categories_" + target + "_draft",
				"elements_" + target + "_ver",
				"categories_" + target + "_ver" };


		logger.info("Deleting from internal module configuration");
		try
		{
			db.updateData("delete from sys_moduleconfig where internal_module_name=?", new Object[] { target });
		}
		catch (Exception e)
		{
			logger.fatal("Unable to remove module from internal configuration", e);
			return "Unable to remove module from internal configuration";
		}

		logger.info("Deleting drop_down");
		try
		{
			db.updateData("delete from drop_down where MODULE_NAME= ?", new Object[] { target });
		}
		catch (Exception e)
		{
			logger.fatal("Unable to delete from drop_down", e);
			return "Unable to delete from drop_down";
		}

		logger.info("Dropping " + tables.length + " data tables");
		int unableToDelete = 0;
		for (String table : tables)
		{
			try
			{
				db.updateData("drop table " + table);
			}
			catch (Exception e)
			{
				++unableToDelete;
				logger.fatal("Unable to drop table", e);
			}
		}

		if (unableToDelete > 0)
		{
			return String.format("Unable to delete %s out of %s internal database tables.", unableToDelete, tables.length);
		}

		return null;
	}

	public String createTemplatedModule(String internalName, String displayName, boolean versionControlEnabled, boolean workflowEnabled, String userId)
	{
		return cloneModuleStructure(internalName, displayName, versionControlEnabled, workflowEnabled, INTERNAL_TEMPLATE_NAME, userId);
	}

	public String cloneModuleStructure(String internalName, String displayName, boolean versionControlEnabled, boolean workflowEnabled, String cloneTarget, String userId)
	{
		logger.info("Start creating new module with internal name: " + internalName);

		try
		{
			//create the tables...
			logger.info("Creating tables for new module");
			db.updateData("create table elements_" + internalName + " like elements_" + cloneTarget);
			db.updateData("create table elements_" + internalName + "_ver like elements_" + cloneTarget + "_ver");
			db.updateData("create table elements_" + internalName + "_draft like elements_" + cloneTarget + "_draft");
			db.updateData("create table categories_" + internalName + " like categories_" + cloneTarget);
			db.updateData("create table categories_" + internalName + "_ver like categories_" + cloneTarget + "_ver");
			db.updateData("create table categories_" + internalName + "_draft like categories_" + cloneTarget + "_draft");
			db.updateData("create table labels_" + internalName + " like labels_" + cloneTarget);
			db.updateData("create table categorylabels_" + internalName + " like categorylabels_" + cloneTarget);

			//reset the auto-increment
			logger.info("Resetting auto-increment on new module");
			db.updateData("ALTER TABLE elements_" + internalName + " AUTO_INCREMENT = 1");
			db.updateData("ALTER TABLE categories_" + internalName + " AUTO_INCREMENT = 1");
			db.updateData("ALTER TABLE elements_" + internalName + "_draft AUTO_INCREMENT = 1");
			db.updateData("ALTER TABLE categories_" + internalName + "_draft AUTO_INCREMENT = 1");

			//add triggers (friendly / last) on live tables only
			logger.info("Adding triggers on new module");
			db.updateData("CREATE TRIGGER insert_e_" + internalName + " BEFORE INSERT ON elements_" + internalName + " FOR EACH ROW begin set new.Create_date=now();set new.FRIENDLY_NAME=friendly(new.ATTR_Headline); end");
			db.updateData("CREATE TRIGGER update_e_" + internalName + " BEFORE UPDATE ON elements_" + internalName + " FOR EACH ROW begin set new.last_update_date=now();set new.FRIENDLY_NAME=friendly(new.ATTR_Headline); end");
			db.updateData("CREATE TRIGGER insert_c_" + internalName + " BEFORE INSERT ON categories_" + internalName + " FOR EACH ROW begin set new.Create_date=now();set new.FRIENDLY_NAME=friendly(new.ATTR_categoryName); end");
			db.updateData("CREATE TRIGGER update_c_" + internalName + " BEFORE UPDATE ON categories_" + internalName + " FOR EACH ROW begin set new.last_update_date=now();set new.FRIENDLY_NAME=friendly(new.ATTR_categoryName); end");

			//recreate the labels metadata
			logger.info("Copying labels into new module");
			db.updateData("INSERT INTO labels_" + internalName + " SELECT * FROM labels_" + cloneTarget);
			db.updateData("INSERT INTO categorylabels_" + internalName + " SELECT * FROM categorylabels_" + cloneTarget);

			logger.info("Read dropdowns from old module");
			DataAccess da = DataAccess.getInstance();
			List<Hashtable<String, String>> currentmulti = DataAccess.getInstance().select("SELECT * FROM drop_down where module_name = ?", new Object[] { cloneTarget }, new HashtableMapper());
			logger.info("Write " + currentmulti.size() + " dropdowns to new module");
			for (Hashtable<String, String> ht : currentmulti)
			{
				ht.remove("ID");
				ht.put("MODULE_NAME", internalName);
				int id = db.insertData(ht, "ID", "drop_down");
				if (id < 0)
				{
					logger.error("Didnt insert dropdown data successfully");
				}
			}

			//let the rest of webdirector see the module.
			int targetLevels = 1;
			if (false == cloneTarget.equals(INTERNAL_TEMPLATE_NAME))
			{
				logger.info("Reading number of levels from existing module");
				try
				{
					//Vector v = select("select levels from sys_moduleconfig where internal_module_name='"+cloneTarget+"'");
					Vector v = db.selectQuerySingleCol("select levels from sys_moduleconfig where internal_module_name=? ", new String[] { cloneTarget });
					targetLevels = Integer.parseInt((String) v.get(0));
				}
				catch (Exception e)
				{
					//Something went wrong :(
					logger.error("Unable to read number of levels", e);
					targetLevels = 1;
				}
			}

			logger.info("Adding module to sys_moduleconfig");
			int moduleId = db.insertQuery("INSERT INTO sys_moduleconfig (internal_module_name, client_module_name, levels, delete_allowed, version_control_enabled, workflow_enabled) " +
					" VALUES (?, ?, ?, 1, ?, ?)", new String[] { internalName, displayName, String.valueOf(targetLevels), versionControlEnabled ? "1" : "0", workflowEnabled ? "1" : "0" });

			logger.info("Assign new module to current user");
			db.updateData("INSERT INTO sys_usermodules (user_id, module_id) " +
					" VALUES (?, ?)", new String[] { userId, String.valueOf(moduleId) });
//			logger.info("Creating workflow tables");
//			updateData("CREATE TABLE elements_" + internalName + "_draft LIKE elements_" + internalName);
//			updateData("CREATE TABLE categories_" + internalName + "_draft LIKE categories_" + internalName);
//			
//			logger.info("Creating version history tables");
//			updateData("CREATE TABLE elements_" + internalName + "_ver LIKE elements_" + internalName);
//			updateData("ALTER TABLE elements_" + internalName + "_ver MODIFY COLUMN Element_id INT(10) NOT NULL, " +
//					" ADD COLUMN Version_ID INTEGER(11), " +
//					" DROP PRIMARY KEY ");
//			
//			updateData("CREATE TABLE categories_" + internalName + "_ver LIKE categories_" + internalName);
//			updateData("ALTER TABLE categories_" + internalName + "_ver MODIFY COLUMN Category_id INT(10) NOT NULL, " +
//					" ADD COLUMN Version_ID INTEGER(11), " +
//					" DROP PRIMARY KEY ");

			//update internal caches...

			logger.info("Done creating new module with internal name: " + internalName);
		}
		catch (Exception e)
		{
			//Something horrible went wrong :(
			logger.fatal("Unable to create new module", e);
			return "Fatal error while attempting to create new module. Please contact support with error code 140108-1 and timestamp: " + new Date();
		}

		return null;
	}

}
