package au.net.webdirector.admin.modules;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.net.webdirector.admin.modules.domain.WidgetDisplayMetadata;
import au.net.webdirector.admin.security.LoginController;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;

@Controller
public class ModuleController
{

	ModuleDAO dao = new ModuleDAO();

	@RequestMapping("/module/assignedModules")
	@ResponseBody
	public List<Map<String, Object>> getModuleDetailsForUser(HttpSession session)
	{
		String userId = (String) session.getAttribute(LoginController.WD_USER_ID);
		return dao.getModuleDetailsForUser(userId);
	}

	@RequestMapping("/module/elementDisplayMetadata")
	@ResponseBody
	public WidgetDisplayMetadata elementDisplayMetadata(
			@RequestParam String tablePrefix,
			@RequestParam String elementID)
	{

		String breadcrumb = getElementBreadcrumb(tablePrefix, elementID);
		Map<String, Object> moduleDetails = dao.getModuleDetailsForModule(tablePrefix);

		return new WidgetDisplayMetadata()
				.withBreadcrumb(breadcrumb)
				.withColorClass((String) moduleDetails.get("color")).withShowLiveFlag(String.valueOf(moduleDetails.get("show_live_flag")));
	}

	public String getElementBreadcrumb(
			@RequestParam String tablePrefix,
			@RequestParam String elementID)
	{
		ModuleAccess moduleAccess = ModuleAccess.getInstance();

		try
		{
			ElementData elementData = moduleAccess.getElementById(tablePrefix, elementID);
			if (elementData != null)
			{
				return String.format("%s | %s",
						getCategoryBreadcrumb(tablePrefix, elementData.getParentId()),
						elementData.getName());
			}
			else
			{
				return String.format("%s | ElementID: %s",
						getCategoryBreadcrumb(tablePrefix, elementData.getParentId()),
						elementID);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return tablePrefix + " E" + elementID;
		}
	}

	@RequestMapping("/module/categoryDisplayMetadata")
	@ResponseBody
	public WidgetDisplayMetadata categoryDisplayMetadata(
			@RequestParam String tablePrefix,
			@RequestParam String categoryID)
	{

		String breadcrumb = getCategoryBreadcrumb(tablePrefix, categoryID);
		Map<String, Object> moduleDetails = dao.getModuleDetailsForModule(tablePrefix);

		return new WidgetDisplayMetadata()
				.withBreadcrumb(breadcrumb)
				.withColorClass((String) moduleDetails.get("color")).withShowLiveFlag(String.valueOf(moduleDetails.get("show_live_flag")));
	}

	public String getCategoryBreadcrumb(
			@RequestParam String tablePrefix,
			@RequestParam String categoryID)
	{
		ModuleAccess moduleAccess = ModuleAccess.getInstance();

		try
		{
			CategoryData categoryData = moduleAccess.getCategoryById(tablePrefix, categoryID);
			if (categoryData != null)
			{
				if (categoryData.getParentId().equalsIgnoreCase("0"))
				{
					return String.format("%s | %s",
							dao.getModuleName(tablePrefix),
							categoryData.getName());
				}
				else
				{
					return String.format("%s | %s",
							getCategoryBreadcrumb(tablePrefix, categoryData.getParentId()),
							categoryData.getName());
				}
			}
			else
			{
				return String.format("%s",
						dao.getModuleName(tablePrefix));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return tablePrefix + " C" + categoryID;
		}
	}

	@RequestMapping("/module/load-validator")
	public void loadValidator(
			@RequestParam String module,
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		response.setContentType("text/javascript");
		OutputStream out = response.getOutputStream();
		IOUtils.write(DynamicFormScriptUtil.getCategoryFormValidScript(module), out);
		IOUtils.write(DynamicFormScriptUtil.getAssetFormValidScript(module), out);
	}
}
