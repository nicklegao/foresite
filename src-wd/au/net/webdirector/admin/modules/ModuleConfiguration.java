package au.net.webdirector.admin.modules;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.ServletContext;

import au.net.webdirector.admin.modules.domain.Module;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;

public class ModuleConfiguration
{

	private static ModuleConfiguration singleton = new ModuleConfiguration();

	public static ModuleConfiguration getInstance(ServletContext servletContext)
	{
		return getInstance();
	}

	public static ModuleConfiguration getInstance()
	{
		return singleton;
	}

	public List<Module> getAllModules()
	{
		List<Module> modules = new ArrayList<Module>();
		List<Hashtable<String, String>> rs = DataAccess.getInstance().select("select * from sys_moduleconfig", new HashtableMapper());
		for (Hashtable<String, String> r : rs)
		{
			Module m = new Module();
			m.construct(r);
			modules.add(m);
		}
		return modules;
	}

	public boolean updateModule(Module module)
	{
		try
		{
			Hashtable<String, String> h = module.toHashtable();
			if (module.getId() == 0)
			{
				h.remove("id");
				new DBaccess().insertData(h, "id", "sys_moduleconfig");
			}
			else
			{
				new DBaccess().updateData(h, "id", "sys_moduleconfig");
			}
			// do we need to save sys_modulesummarycols?
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public Module getModule(String internalModuleName)
	{
		List<Hashtable<String, String>> rs = DataAccess.getInstance().select("select * from sys_moduleconfig where internal_module_name = ? ", new Object[] { internalModuleName }, new HashtableMapper());
		if (rs.size() == 0)
		{
			return null;
		}
		Module m = new Module();
		m.construct(rs.get(0));
		return m;
	}
}
