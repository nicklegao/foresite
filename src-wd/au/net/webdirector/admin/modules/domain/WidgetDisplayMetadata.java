package au.net.webdirector.admin.modules.domain;

public class WidgetDisplayMetadata
{
	private String breadcrumb;
	private String colorClass;
	private String showLiveFlag;

	public String getBreadcrumb()
	{
		return breadcrumb;
	}

	public void setBreadcrumb(String breadcrumb)
	{
		this.breadcrumb = breadcrumb;
	}

	public WidgetDisplayMetadata withBreadcrumb(String breadcrumb)
	{
		this.breadcrumb = breadcrumb;
		return this;
	}

	public String getColorClass()
	{
		return colorClass;
	}

	public void setColorClass(String colorClass)
	{
		this.colorClass = colorClass;
	}

	public WidgetDisplayMetadata withColorClass(String colorClass)
	{
		this.colorClass = colorClass;
		return this;
	}
	
	public String getShowLiveFlag()
	{
		return showLiveFlag;
	}

	public void setShowLiveFlag(String showLiveFlag)
	{
		this.showLiveFlag = showLiveFlag;
	}
	
	public WidgetDisplayMetadata withShowLiveFlag(String showLiveFlag)
	{
		this.showLiveFlag = showLiveFlag;
		return this;
	}
}
