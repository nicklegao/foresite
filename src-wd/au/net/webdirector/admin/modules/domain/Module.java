package au.net.webdirector.admin.modules.domain;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class Module
{

	private int id;
	private String moduleName;
	private String externalModuleName;
	private int levels;
	private String icon;
	private String color;

	private boolean hasSort;
	private boolean hasCopy;
	private boolean hasCut;
	private boolean hasImport;
	private boolean hasExport;

	private boolean hasFullCkeditor;
	private boolean hasFormComponents;
	private boolean hasBrowseServer;
	private String customisedButtons;

	private boolean hasVersionControl;
	private boolean hasWorkflow;
	private boolean showLiveFlag;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getModuleName()
	{
		return moduleName;
	}

	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}

	public String getExternalModuleName()
	{
		return externalModuleName;
	}

	public void setExternalModuleName(String externalModuleName)
	{
		this.externalModuleName = externalModuleName;
	}

	public int getLevels()
	{
		return levels;
	}

	public void setLevels(int levels)
	{
		this.levels = levels;
	}

	public String getIcon()
	{
		return icon;
	}

	public void setIcon(String icon)
	{
		this.icon = icon;
	}

	public String getColor()
	{
		return color;
	}

	public void setColor(String color)
	{
		this.color = color;
	}

	public boolean getHasVersionControl()
	{
		return hasVersionControl;
	}

	public void setHasVersionControl(boolean hasVersionControl)
	{
		this.hasVersionControl = hasVersionControl;
	}

	public boolean getHasWorkflow()
	{
		return hasWorkflow;
	}

	public void setHasWorkflow(boolean hasWorkflow)
	{
		this.hasWorkflow = hasWorkflow;
	}

	public boolean getHasSort()
	{
		return hasSort;
	}

	public void setHasSort(boolean hasSort)
	{
		this.hasSort = hasSort;
	}

	public boolean getHasCopy()
	{
		return hasCopy;
	}

	public void setHasCopy(boolean hasCopy)
	{
		this.hasCopy = hasCopy;
	}

	public boolean getHasCut()
	{
		return hasCut;
	}

	public void setHasCut(boolean hasCut)
	{
		this.hasCut = hasCut;
	}

	public boolean getHasImport()
	{
		return hasImport;
	}

	public void setHasImport(boolean hasImport)
	{
		this.hasImport = hasImport;
	}

	public boolean getHasExport()
	{
		return hasExport;
	}

	public void setHasExport(boolean hasExport)
	{
		this.hasExport = hasExport;
	}

	public boolean getHasFullCkeditor()
	{
		return hasFullCkeditor;
	}

	public void setHasFullCkeditor(boolean hasFullCkeditor)
	{
		this.hasFullCkeditor = hasFullCkeditor;
	}

	public boolean getHasFormComponents()
	{
		return hasFormComponents;
	}

	public void setHasFormComponents(boolean hasFormComponents)
	{
		this.hasFormComponents = hasFormComponents;
	}

	public boolean getHasBrowseServer()
	{
		return hasBrowseServer;
	}

	public void setHasBrowseServer(boolean hasBrowseServer)
	{
		this.hasBrowseServer = hasBrowseServer;
	}

	public String getCustomisedButtons()
	{
		return customisedButtons;
	}

	public void setCustomisedButtons(String customisedButtons)
	{
		this.customisedButtons = customisedButtons;
	}

	public boolean hasShowLiveFlag()
	{
		return showLiveFlag;
	}

	public void setShowLiveFlag(boolean showLiveFlag)
	{
		this.showLiveFlag = showLiveFlag;
	}

	public void construct(Map<String, String> m)
	{
		this.setId(Integer.parseInt(m.get("id")));
		this.setLevels(StringUtils.isBlank(m.get("levels")) ? 0 : Integer.parseInt(m.get("levels")));
		this.setModuleName(m.get("internal_module_name"));
		this.setExternalModuleName(m.get("client_module_name"));
		this.setIcon(m.get("icon"));
		this.setColor(m.get("color"));

		this.setHasVersionControl(Integer.parseInt(m.get("version_control_enabled")) == 1);
		this.setHasWorkflow(Integer.parseInt(m.get("workflow_enabled")) == 1);
		this.setShowLiveFlag(Integer.parseInt(m.get("show_live_flag")) == 1);

		this.setHasSort(Integer.parseInt(m.get("has_sort")) == 1);
		this.setHasCopy(Integer.parseInt(m.get("has_copy")) == 1);
		this.setHasCut(Integer.parseInt(m.get("has_cut")) == 1);
		this.setHasImport(Integer.parseInt(m.get("has_import")) == 1);
		this.setHasExport(Integer.parseInt(m.get("has_export")) == 1);
		this.setHasFullCkeditor(Integer.parseInt(m.get("has_full_ckeditor")) == 1);
		this.setHasFormComponents(Integer.parseInt(m.get("has_form_components")) == 1);
		this.setHasBrowseServer(Integer.parseInt(m.get("has_browse_server")) == 1);
		this.setCustomisedButtons(m.get("customised_buttons"));
	}

	public Hashtable<String, String> toHashtable()
	{
		Hashtable<String, String> m = new Hashtable<String, String>();
		m.put("id", String.valueOf(this.getId()));
		m.put("levels", String.valueOf(this.getLevels()));
		m.put("internal_module_name", this.getModuleName());
		m.put("client_module_name", this.getExternalModuleName());
		m.put("icon", this.getIcon());
		m.put("color", this.getColor());

		m.put("version_control_enabled", this.getHasVersionControl() ? "1" : "0");
		m.put("workflow_enabled", this.getHasWorkflow() ? "1" : "0");
		m.put("show_live_flag", this.hasShowLiveFlag() ? "1" : "0");

		m.put("has_sort", this.getHasSort() ? "1" : "0");
		m.put("has_copy", this.getHasCopy() ? "1" : "0");
		m.put("has_cut", this.getHasCut() ? "1" : "0");
		m.put("has_import", this.getHasImport() ? "1" : "0");
		m.put("has_export", this.getHasExport() ? "1" : "0");
		m.put("has_full_ckeditor", this.getHasFullCkeditor() ? "1" : "0");
		m.put("has_form_components", this.getHasFormComponents() ? "1" : "0");
		m.put("has_browse_server", this.getHasBrowseServer() ? "1" : "0");
		m.put("customised_buttons", this.getCustomisedButtons());
		return m;
	}

	public String printCustomisedButtons()
	{
		if (StringUtils.isBlank(customisedButtons))
		{
			return "['']";
		}
		String[] array = StringUtils.split(customisedButtons, ",");
		for (int i = 0; i < array.length; i++)
		{
			array[i] = array[i].trim();
		}
		return "['" + StringUtils.join(array, "','") + "']";
	}
}
