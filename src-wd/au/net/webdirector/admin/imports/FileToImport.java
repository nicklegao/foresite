package au.net.webdirector.admin.imports;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 24/07/2008
 * Time: 18:40:33
 * To change this template use File | Settings | File Templates.
 */
public class FileToImport
{

	public String getModule()
	{
		return module;
	}

	public void setModule(String module)
	{
		this.module = module;
	}

	public String getColumnName()
	{
		return columnName;
	}

	public void setColumnName(String columnName)
	{
		this.columnName = columnName;
	}

	public String getLocalPath()
	{
		return localPath;
	}

	public void setLocalPath(String localPath)
	{
		this.localPath = localPath;
	}

	public String getStorePath()
	{
		return storePath;
	}

	public void setStorePath(String storePath)
	{
		this.storePath = storePath;
	}

	public String getElementId()
	{
		return elementId;
	}

	public void setElementId(String elementId)
	{
		this.elementId = elementId;
	}

	public FileToImport(String columnName, String localPath, String storePath, String module)
	{
		this.columnName = columnName;
		this.localPath = localPath;
		this.storePath = storePath;
		this.module = module;
	}

	private String columnName = "";
	private String localPath = "";
	private String storePath = "";
	private String elementId = "";
	private String module = "";



}
