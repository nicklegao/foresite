package au.net.webdirector.admin.imports;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class DownloadFile
{
	public boolean downloadFile(String fileHttpPath, String fileDestName)
			throws MalformedURLException, IOException
	{
		boolean status = false;
		try
		{
			URL url = new URL(fileHttpPath);
			System.out.println("Opening connection to " + fileHttpPath + "...");
			URLConnection urlC = url.openConnection();

			InputStream is = url.openStream();

			System.out.print("Copying resource (type: " + urlC.getContentType());
			System.out.flush();
			FileOutputStream fos = null;
			fos = new FileOutputStream(fileDestName);
			int count = 0;
			int oneChar;
			while ((oneChar = is.read()) != -1)
			{
				fos.write(oneChar);
				count++;
			}
			is.close();
			fos.close();
			System.out.println(count + " byte(s) copied");
			status = true;
		}
		catch (MalformedURLException e)
		{
			status = false;
			throw e;
		}
		catch (IOException e)
		{
			status = false;
			throw e;
		}
		return status;
	}
}