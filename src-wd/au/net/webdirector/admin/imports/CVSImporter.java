package au.net.webdirector.admin.imports;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

public class CVSImporter implements Procedure
{

	private static Logger logger = Logger.getLogger(CVSImporter.class);
	private String id;
	private ImportCSV importer;
	private int status = NOT_START;
	private String result;
	private Map properties = new HashMap();
	private boolean isSuccessful = false;

	private static Map registered = new HashMap();

	public String getID()
	{
		return id;
	}

	public int getCompletePercentage()
	{
		if (status == NOT_START)
		{
			return 0;
		}
		if (status == COMPLISHED)
		{
			return 100;
		}
		if (importer.getTotalRecord() == 0)
		{
			return 100;
		}
		return importer.getCompletedNumber() * 100 / importer.getTotalRecord();
	}

	public int getStatus()
	{
		return status;
	}

	private CVSImporter()
	{
		id = UUID.randomUUID().toString();
		importer = new ImportCSV();
	}

	public static CVSImporter getInstance()
	{
		CVSImporter inst = new CVSImporter();
		registered.put(inst.getID(), inst);
		return inst;
	}

	public static CVSImporter getInstance(String id)
	{
		CVSImporter inst = (CVSImporter) registered.get(id);
		return inst;
	}

	public static void destroyInstance(String id)
	{
		registered.remove(id);
	}

	public void destroy()
	{
		destroyInstance(this.getID());
	}

	public void init(Map properties)
	{
		this.properties = properties;
		importer.countTotalRecordNumber((String) properties.get("importFile"));
	}

	public void run()
	{
		status = PROCESSING;
		String moduleName = (String) properties.get("moduleName");
		String importFile = (String) properties.get("importFile");
		String importType = (String) properties.get("importType");
		String assetLive = (String) properties.get("assetLive");
		String deleteAll = (String) properties.get("deleteAll");
		String ignoreFirstLine = (String) properties.get("ignoreFirstLine");
		Map map = (Map) properties.get("map");
		ServletContext context = (ServletContext) properties.get("context");
		String pKey = (String) properties.get("pKey");
		importer.setIgnoreFirstLine(null != ignoreFirstLine && "on".equalsIgnoreCase(ignoreFirstLine));
		try
		{
			result = importer.importData(moduleName, importFile, importType, assetLive, deleteAll, map, context, pKey);
			isSuccessful = true;
		}
		catch (Exception e)
		{
			String e_message = e.getMessage();
			if (e_message.indexOf("Duplicate entry '") != -1)
			{
				String showMsg = e_message;
				int start = showMsg.indexOf("Duplicate entry '");
				if (start != -1 && showMsg.indexOf("'", start + "Duplicate entry '".length() + 1) != -1)
				{
					int end = showMsg.indexOf("'", start + "Duplicate entry '".length() + 1);
					showMsg = showMsg.substring(start + "Duplicate entry '".length(), end);
					showMsg = "Duplicate value: '" + showMsg + "' found!";
				}
				result = "<font color='red'>Error occured! DuplicateKeyException - " + showMsg + "</font>";
			}
			else
			{
				result = "<font color='red'>Error occured! Exception:" + e.getMessage() + "</font>";
			}
			result += "<font color='red'> Whole transaction rolled back!</font>";
			logger.error("Error:", e);
			isSuccessful = false;
		}
		status = COMPLISHED;
	}

	public Object getOperator()
	{
		return importer;
	}

	public String getResult()
	{
		return result;
	}

	public boolean isSuccessful()
	{
		return this.isSuccessful;
	}

	public List<String> getErrors()
	{
		return importer.getErrors();
	}

	public List<String> getResultDetails()
	{
		return importer.getResultDetails();
	}
}
