package au.net.webdirector.admin.imports;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import webdirector.db.client.ClientDataAccess;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.utils.module.StoreUtils;

import com.csvreader.CsvReader;

public class ImportCSV
{

	private static Logger logger = Logger.getLogger(ImportCSV.class);
	private static int MAX_BATCH = 1000;

	private DBaccess db = new DBaccess();
	private DataAccess da = DataAccess.getInstance();

	protected static Defaults d = Defaults.getInstance();
	int lineNo;
	public LinkedHashMap hMap = new LinkedHashMap();
	public int updateCases = 0;
	public int insertCases = 0;
	int vtIndex = 0;
	boolean ignoreFirstLine = false;
	private Connection conn = null;
	private Statement stmt = null;
	private Statement stmtRead = null;

	public String[] readFirstLine(String importFile) throws Exception
	{
		File f = new File(d.getStoreDir() + "/_tmp/importFile/" + importFile);
		String[] arrLineData = null;
		if (f.exists())
		{
			FileReader fr = new FileReader(f);
			BufferedReader is = new BufferedReader(fr);

			CsvReader csvReader = new CsvReader(is);

			int count = 0;
			String line;
			while (csvReader.readRecord())
			{
				line = csvReader.getRawRecord();
				for (int i = 0; i < line.length(); i++)
				{
					char c = line.charAt(i);
					if (c > '')
					{
						throw new Exception("Invalid characters found in file: " + line + "<br> Character:" + c);
					}
				}

				if (count == 0)
				{
					if (line.indexOf(",") == -1)
					{
						throw new Exception("No Comma seperated values found in first Line of file");
					}
					arrLineData = csvReader.getValues();
				}
				count++;
			}
			csvReader.close();
			return arrLineData;
		}
		return null;
	}

	public String importData(String moduleToImport, String importFile, String importType, String assetLive, String deleteAll, Map map, ServletContext servletContext, String pKey) throws IOException,
			SQLException, Exception
	{
		int rowsInserted = 0;
		File f = new File(d.getStoreDir() + "/_tmp/importFile/" + importFile);
		CSV csv = new CSV();

		if (f.exists())
		{
			FileReader fr = new FileReader(f);
			BufferedReader is = new BufferedReader(fr);
			logger.info("====process(is:" + f + ", " + moduleToImport + ", " + importType + ", " + assetLive + ", " + deleteAll + ", map, servletContext, " + pKey + ")");
			rowsInserted = process(is, moduleToImport, importType, assetLive, deleteAll, map, servletContext, pKey);

			boolean b = f.delete();

		}

		return String.valueOf(rowsInserted) + " rows imported OK.";
	}

	protected int process(BufferedReader is, String moduleToImport, String importType, String assetLive, String deleteAll, Map map, ServletContext servletContext, String pKey) throws IOException,
			Exception
	{
		ClientDataAccess cda = new ClientDataAccess();

		int rowsInserted = 0;

		// this.lineNo = 0;
		boolean flag = false;

		String[] cols = null;

		if ((importType != null) && (importType.equals("Asset")))
		{
			cols = cda.getElementCols(moduleToImport);
		}
		else
		{
			cols = cda.getCategoryCols(moduleToImport);
		}
		CsvReader csvReader = null;
		try
		{

			initDataBaseStuff();
			if ((deleteAll != null) && (deleteAll.equals("1")))
			{
				String deleteQry = "";
				deleteQry = "delete from Elements_" + moduleToImport;
				int rowsDeleted = stmt.executeUpdate(deleteQry);

				if ((importType != null) && (!importType.equalsIgnoreCase("Asset")))
				{
					deleteQry = "delete from Categories_" + moduleToImport;
					rowsDeleted = stmt.executeUpdate(deleteQry);
				}
			}

			csvReader = new CsvReader(is);
			boolean firstLineSkipped = false;
			int batch = 0;
			while (csvReader.readRecord())
			{
				if (ignoreFirstLine && !firstLineSkipped)
				{
					firstLineSkipped = true;
					continue;
				}

				String[] arrLineData = csvReader.getValues();
				for (int i = 0; i < arrLineData.length; i++)
				{

					arrLineData[i] = arrLineData[i].replaceAll("\"", "");

				}
				if ((importType != null) && (importType.equals("Asset")))
				{
					flag = insertElements(arrLineData, moduleToImport, assetLive, deleteAll, cols, map, servletContext, pKey);
				}
				else
				{
					flag = insertCategories(arrLineData, moduleToImport, assetLive, deleteAll, cols, map, servletContext);
				}
				if (!flag)
				{
					// conn.rollback();
					break;
				}
				// delay for test only, make precess slow.
				// Thread.sleep(1000);
				rowsInserted++;

				if (batch++ > MAX_BATCH)
				{
					conn.commit();
					clearDataBaseStuff();
					initDataBaseStuff();
					batch = 0;
				}
			}
			batch = 0;
			conn.commit();
		}
		catch (Exception ex)
		{
			csvReader.close();
			// whenever exception occurs, always commit to save the already imported data.
			logger.error("Errors:", ex);
			conn.commit();
			throw ex;
		}
		finally
		{
			clearDataBaseStuff();
			csvReader.close();
		}
		return rowsInserted;
	}

	private void initDataBaseStuff() throws SQLException
	{
		conn = getConnection();
		conn.setAutoCommit(false);
		stmt = conn.createStatement();
		stmtRead = conn.createStatement();
	}

	private void clearDataBaseStuff()
	{
		if (stmt != null)
		{
			try
			{
				stmt.close();
			}
			catch (Exception e)
			{

			}
			stmt = null;
		}
		if (stmtRead != null)
		{
			try
			{
				stmtRead.close();
			}
			catch (Exception e)
			{

			}
			stmtRead = null;
		}
		if (conn != null)
		{
			try
			{
				conn.close();
			}
			catch (Exception e)
			{

			}
			conn = null;
		}
	}

	protected boolean insertCategories(String[] arrLineData, String moduleToImport, String assetLive, String deleteAll, String[] cols, Map map, ServletContext servletContext) throws Exception
	{

		String catBaseQuery = "insert into Categories_" + moduleToImport + " (";
		StringBuffer updateQuery = new StringBuffer("update Categories_" + moduleToImport + " set ");
		StringBuffer keys = new StringBuffer();
		StringBuffer vals = new StringBuffer();
		Vector fileImports = new Vector();
		int id = 0;

		int levels = Integer.parseInt(Defaults.getModuleLevels(moduleToImport));

		String[] categories = new String[levels];

		categories = processCategories(moduleToImport, levels, arrLineData, "Category");

		String Category_id = "0";
		for (int i = levels - 1; i >= 0; i--)
		{

			if (arrLineData[i].trim().equals(""))
				continue;
			Category_id = categories[i];
			break;
		}

		keys.append("ATTR_categoryName,");
		vals.append("'" + arrLineData[levels] + "',");

		for (int i = 0; i < cols.length; i++)
		{
			String colName = cols[i];
			int dataIndex = -1;
			String index = (String) map.get(colName);

			if (index == null)
				continue;
			dataIndex = Integer.parseInt(index);
			String data = arrLineData[dataIndex];

			data = StringEscapeUtils.escapeXml(data);
			data = sq(data);

			keys.append(colName + ",");
			if ((colName.startsWith("ATTRCURRENCY")) || (colName.startsWith("ATTRFLOAT")))
			{
				Float fl;
				if (data.trim().equals(""))
				{
					data = "0";
				}
				try
				{
					fl = Float.valueOf(Float.parseFloat(data));
				}
				catch (NumberFormatException ex)
				{

					throw new Exception("Non Float found for Float type data.<br> Column:" + colName + " Value:" + data);
				}
				vals.append("'" + data + "',");
				updateQuery.append(colName + "='");
				updateQuery.append(data + "', ");
			}
			else if (colName.startsWith("ATTRINTEGER"))
			{
				Integer fl;
				if (data.trim().equals(""))
				{
					data = "0";
				}
				try
				{
					fl = Integer.valueOf(Integer.parseInt(data));
				}
				catch (NumberFormatException ex)
				{

					throw new Exception("Non integer found for integer type data.<br> Column:" + colName + " Value:" + data);
				}
				vals.append("'" + data + "',");
				updateQuery.append(colName + "='");
				updateQuery.append(data + "', ");
			}
			else if (((colName.startsWith("ATTRFILE")) || (colName.startsWith("ATTRLONG"))) && (!data.trim().equals("")) && (!data.equals(" ")))
			{
				String storePath = "/" + moduleToImport + "/8888/" + colName + "/" + data.substring(data.lastIndexOf("/") + 1);
				vals.append("'" + storePath + "',");
				updateQuery.append(colName + "='");
				updateQuery.append(storePath + "', ");

				fileImports.add(new FileToImport(colName, data, storePath, moduleToImport));
			}
			else if (colName.startsWith("ATTRDATE"))
			{
				boolean validDate = isValidDate(data);
				if (!validDate)
				{
					data = "1900-01-01";
				}

				vals.append("'" + data + "',");
				updateQuery.append(data + "',");
			}
			else
			{
				vals.append("'" + arrLineData[dataIndex] + "',");
				updateQuery.append(colName + "='");
				updateQuery.append(data + "', ");
			}

		}

		String k = keys.substring(0, keys.length() - 1);
		String s = vals.substring(0, vals.length() - 1);

		String query = updateQuery.substring(0, updateQuery.length() - 2);

		query = query + " where Category_id=" + Category_id;

		id = updateData(query);

		if (id == 0)
		{
			return false;
		}

		return loadAllFileImports(fileImports, Integer.parseInt(Category_id), "Categories", servletContext);
	}

	protected boolean insertElements(String[] arrLineData, String moduleToImport, String assetLive, String deleteAll, String[] cols, Map map, ServletContext servletContext, String pKey)
			throws Exception
	{
		int levels = Integer.parseInt(Defaults.getModuleLevels(moduleToImport));
		String[] categories = new String[levels];
		int id = 0;
		categories = processCategories(moduleToImport, levels, arrLineData, "Asset");

		String element_id = "0";

		if ((pKey != null) && (!pKey.equals("")))
		{
			String pKeyData = arrLineData[Integer.parseInt((String) map.get(pKey))];

			String existQry = "select element_id from Elements_" + moduleToImport + " where " + pKey + "='" + pKeyData + "' and Category_id=" + categories[(levels - 1)];
			ResultSet rs = null;
			try
			{
				rs = stmtRead.executeQuery(existQry);
				if (rs.next())
				{
					element_id = rs.getString(1);
				}
			}
			finally
			{
				if (rs != null)
					rs.close();
			}
		}
		String assetBaseQuery = "";
		StringBuffer keys = new StringBuffer();
		StringBuffer vals = new StringBuffer();
		StringBuffer updateString = new StringBuffer();
		Vector fileImports = new Vector();

		if ((element_id != null) && (!element_id.equals("0")))
		{
			assetBaseQuery = "update Elements_" + moduleToImport + " set ";
			this.updateCases += 1;
		}
		else
		{
			assetBaseQuery = "insert into Elements_" + moduleToImport + " (";
			this.insertCases += 1;
		}

		for (int i = 0; i < cols.length; i++)
		{
			String colName = cols[i];

			int dataIndex = -1;
			String index = (String) map.get(colName);
			if (index == null)
				continue;
			dataIndex = Integer.parseInt(index);
			String data = arrLineData[dataIndex];
			data = StringEscapeUtils.escapeXml(data);
			data = sq(data);

			keys.append(colName + ", ");
			updateString.append(colName + "='");
			if ((colName.startsWith("ATTRCURRENCY")) || (colName.startsWith("ATTRFLOAT")))
			{
				Float fl;
				if (data.equals(""))
				{
					data = "0";
				}
				try
				{
					fl = Float.valueOf(Float.parseFloat(data));
				}
				catch (NumberFormatException ex)
				{

					throw new Exception("Non Float found for Float type data.<br> Column:" + colName + " Value:" + data);
				}
				vals.append("'" + data + "',");
				updateString.append(data + "',");
			}
			else if (colName.startsWith("ATTRINTEGER"))
			{
				Integer fl;
				if (data.equals(""))
				{
					data = "0";
				}
				try
				{
					fl = Integer.valueOf(Integer.parseInt(data));
				}
				catch (NumberFormatException ex)
				{

					throw new Exception("Non Integer found for Integer type data.<br> Column:" + colName + " Value:" + data);
				}
				vals.append("'" + data + "',");
				updateString.append(data + "',");
			}
			else if (((colName.startsWith("ATTRFILE")) || (colName.startsWith("ATTRLONG"))) && (!data.trim().equals("")) && (!data.equals(" ")))
			{
				String storePath = "/" + moduleToImport + "/8888/" + colName + "/" + data.substring(data.lastIndexOf("/") + 1);
				vals.append("'" + storePath + "',");
				updateString.append(storePath + "',");
				fileImports.add(new FileToImport(colName, data, storePath, moduleToImport));
			}
			else if (colName.startsWith("ATTRDATE"))
			{
				boolean validDate = isValidDate(data);
				if (!validDate)
				{
					data = "1900-01-01";
				}

				vals.append("'" + data + "',");
				updateString.append(data + "',");
			}
			else
			{
				vals.append("'" + data + "',");
				updateString.append(data + "',");
			}

		}

		String k = keys.substring(0, keys.length() - 2);
		String s = vals.substring(0, vals.length() - 1);

		String update = updateString.substring(0, updateString.length() - 1);

		if ((element_id != null) && (!element_id.equals("0")))
		{
			if ((assetLive != null) && (assetLive.equals("1")))
				assetBaseQuery = assetBaseQuery + update + ",Live=1 where element_id=" + element_id + " and Category_id=" + categories[(levels - 1)];
			else
			{
				assetBaseQuery = assetBaseQuery + update + ",Live=0 where element_id=" + element_id + " and Category_id=" + categories[(levels - 1)];
			}

			id = updateData(assetBaseQuery);
		}
		else
		{
			if ((assetLive != null) && (assetLive.equals("1")))
				assetBaseQuery = assetBaseQuery + k + ", Language_id,Status_id,Lock_id,Version,Live,Category_id) values (" + s + ",1,1,1,1,1," + categories[(levels - 1)] + ")";
			else
			{
				assetBaseQuery = assetBaseQuery + k + ", Language_id,Status_id,Lock_id,Version,Live,Category_id) values (" + s + ",1,1,1,1,0," + categories[(levels - 1)] + ")";
			}

			id = insertData(assetBaseQuery, "", "Elements_" + moduleToImport);
		}

		if (id == 0)
		{
			return false;
		}
		boolean ret = loadAllFileImports(fileImports, id, "Asset", servletContext);
		return ret;
	}

	private boolean loadAllFileImports(Vector fileImports, int elementId, String importType, ServletContext servletContext) throws Exception
	{
		StoreUtils storeUtils = new StoreUtils();
		DownloadFile df = new DownloadFile();
		String colName = "";
		String destination = "";
		String updateDestination = "";

		for (int i = 0; i < fileImports.size(); i++)
		{
			FileToImport fileToImport = (FileToImport) fileImports.elementAt(i);

			String filePath = fileToImport.getLocalPath();
			String srcFile = "";
			boolean status;
			if (filePath.indexOf("http://") != -1)
			{
				String fileName = filePath.substring(filePath.lastIndexOf("/"));
				srcFile = d.getOStypeTempDir() + d.getOSTypeDirSep() + fileName;

				status = df.downloadFile(filePath, srcFile);
			}
			else if (filePath.indexOf(":\\") != -1)
			{

				srcFile = filePath;

			}
			else
			{

				String importBasePath = servletContext.getInitParameter("TempWindows");
				srcFile = importBasePath + filePath;
			}

			fileToImport.setStorePath(fileToImport.getStorePath().replace("/8888/", "/" + elementId + "/"));
			colName = fileToImport.getColumnName();
			boolean generateThumb = true;
			destination = d.getStoreDir() + fileToImport.getStorePath().substring(0, fileToImport.getStorePath().lastIndexOf("/"));

			if (colName.startsWith("ATTRLONG"))
			{
				generateThumb = false;
				updateDestination = fileToImport.getStorePath().substring(0, fileToImport.getStorePath().lastIndexOf("/")) + "/" + "ATTRLONG_PageText.txt";
			}
			else
			{
				generateThumb = true;
				updateDestination = fileToImport.getStorePath();
			}

			if (!storeUtils.copyToStores(srcFile, destination, generateThumb))
			{
				logger.error("********* FILE IMPORT FAILED FOR FILE .************");
				logger.error("******** SRC " + srcFile);
				logger.error("********                              *************");
				logger.error("***************************************************");
			}

			updateDestination = updateDestination.replace('\\', '/');
			String query = "";
			if ((importType != null) && (importType.equalsIgnoreCase("Asset")))
			{
				query = "update elements_" + fileToImport.getModule() + " set " + fileToImport.getColumnName() + " = " + "'" + updateDestination + "' where element_id = " + elementId;
			}
			else
			{
				query = "update categories_" + fileToImport.getModule() + " set " + fileToImport.getColumnName() + " = " + "'" + updateDestination + "' where Category_id = " + elementId;
			}
			if (updateData(query) != 1)
			{
				return false;
			}
		}
		return true;
	}

	protected String[] processCategories(String moduleToImport, int levels, String[] arrLineData, String importType) throws Exception
	{
		String[] categories = new String[levels];
		String parentID = null;

		for (int i = 0; i < levels; i++)
		{
			String data = arrLineData[i];
			if ((data != null) && (data.trim().equals("")))
			{
				categories[i] = "0";
			}
			else
			{
				data = StringEscapeUtils.escapeXml(data);
				data = sq(data);

				if (!doesCategoryExist(moduleToImport, String.valueOf(i + 1), data, parentID))
				{
					parentID = insertParentCategories(moduleToImport, String.valueOf(i + 1), data, parentID);
				}
				else
				{
					parentID = getCategoryIdUsingParent(moduleToImport, String.valueOf(i + 1), data, parentID);
				}

				categories[i] = parentID;
			}
		}
		return categories;
	}

	protected String insertParentCategories(String moduleToImport, String categorylevel, String categoryName, String parentID) throws Exception
	{
		String id = "0";
		if (parentID == null)
			parentID = "0";
		String query = "insert into Categories_" + moduleToImport + " (ATTR_categoryName,folderLevel,Category_ParentID) " + "values ('" + categoryName + "'," + categorylevel + "," + parentID + ")";

		id = String.valueOf(insertData(query, "Category_id", "Categories_" + moduleToImport));
		return id;
	}

	private synchronized int insertData(String query, String keyColumn, String tableName) throws Exception
	{
		if ((keyColumn == null) || (keyColumn.equals("")))
		{
			keyColumn = "Element_id";
		}

		int maxID = getMaxID(keyColumn, tableName);

		maxID++;
		int id = maxID;

		String newQuery = insertExtraArg(query, keyColumn, id);

		int rowCount = updateData(newQuery);
		if (rowCount != 1)
		{
			id = 0;
		}
		return id;
	}

	private int getMaxID(String keyColumn, String tableName) throws Exception
	{
		String query = "select max(" + keyColumn + ") from " + tableName;
		String maxID = "0";

		ResultSet rs = null;
		try
		{
			rs = stmtRead.executeQuery(query);
			if (rs.next())
			{
				maxID = rs.getString(1);
			}

			if ((maxID == null) || (maxID.equals("")) || (maxID.equals("0")))
			{
				maxID = "1";
			}
		}
		finally
		{
			if (null != rs)
				rs.close();
		}
		return Integer.parseInt(maxID);
	}

	private String insertExtraArg(String query, String keyColumn, int id)
	{
		StringBuffer sb = new StringBuffer(100);
		sb.append(query);
		int last = query.lastIndexOf(")");
		sb.insert(last, "," + String.valueOf(id));
		int first = query.indexOf(")");
		sb.insert(first, "," + keyColumn);

		return sb.toString();
	}

	private Connection getConnection()
	{
		return new DBaccess().getDBConnection();
	}

	public int updateData(String query) throws Exception
	{
		int rowsAffected = 0;
		try
		{
			rowsAffected = stmt.executeUpdate(query);
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
			this.hMap.put(new Integer(this.vtIndex), "<font color='Red'>Row:&nbsp;" + (this.vtIndex + 1) + "&nbsp;&nbsp;&nbsp; Status:&nbsp;<b>Fail</b><br>" + query + "</font>");
		}
		this.vtIndex += 1;
		return rowsAffected;
	}

	public final String sq(String str)
	{
		String SQ = "'";
		if (d.getDEBUG())
		{
			logger.debug(str);
		}

		String token = null;
		StringBuffer returnString = new StringBuffer();

		if (str != null)
		{
			StringTokenizer st = new StringTokenizer(str, SQ, true);
			while (st.hasMoreTokens())
			{
				token = st.nextToken();
				if (token.equals(SQ))
				{
					returnString.append(token);
					returnString.append(SQ);
				}

				returnString.append(token);
			}

		}

		return returnString.toString();
	}

	public String getCategoryIdUsingParent(String module, String theLevel, String category, String parent) throws SQLException
	{
		String query = "select Category_id from Categories_" + module + " where " + "ATTR_categoryName = '" + category + "' and folderLevel = " + theLevel;

		if (parent != null)
		{
			query = "select Category_id from Categories_" + module + " where " + "ATTR_categoryName = '" + category + "' and folderLevel = " + theLevel + " and Category_ParentID = " + parent;
		}

		ResultSet rs = null;
		try
		{
			rs = stmtRead.executeQuery(query);
			if (!rs.next())
			{
				return "0";
			}
			return rs.getString(1);
		}
		finally
		{
			if (rs != null)
			{
				rs.close();
			}
		}
	}

	public boolean doesCategoryExist(String module, String theLevel, String category, String parent) throws SQLException
	{
		String id = getCategoryIdUsingParent(module, theLevel, category, parent);
		return !id.equals("0");
	}

	public boolean isValidDate(String date) throws Exception
	{
		try
		{
			String format = "yyyy-MM-dd";
			if (date.indexOf('/') != -1)
			{
				format = "yyyy/MM/dd";
			}
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			sdf.setLenient(false);
			sdf.parse(date);
		}
		catch (ParseException e)
		{
			return false;
		}

		return true;
	}

	public static void main(String[] as)
	{
		ImportCSV ic = new ImportCSV();
		try
		{
			boolean d1 = ic.isValidDate("Amit");
			logger.info("Final Date:" + d1);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public int getTotalRecord()
	{
		return this.lineNo;
	}

	public int getCompletedNumber()
	{
		return updateCases + insertCases;
	}

	void countTotalRecordNumber(String importFile)
	{
		this.lineNo = 0;
		File f = new File(d.getStoreDir() + "/_tmp/importFile/" + importFile);
		if (f.exists())
		{
			CsvReader csvReader = null;
			try
			{
				csvReader = new CsvReader(new BufferedReader(new FileReader(f)));
				boolean firstLineSkipped = false;
				while (csvReader.readRecord())
				{
					if (ignoreFirstLine && !firstLineSkipped)
					{
						firstLineSkipped = true;
						continue;
					}
					this.lineNo += 1;
				}
			}
			catch (Exception e)
			{
			}
			finally
			{
				csvReader.close();
			}
		}

	}

	public List<String> getErrors()
	{
		List<String> ret = new ArrayList();
		Iterator iterator = hMap.keySet().iterator();
		while (iterator.hasNext())
		{
			Integer key = (Integer) iterator.next();
			String value = (String) hMap.get(key);
			ret.add(value);
		}
		return ret;
	}

	public List<String> getResultDetails()
	{
		List<String> ret = new ArrayList();
		ret.add("Total No. of Updates: " + updateCases);
		ret.add("Total No. of Inserts: " + insertCases);
		return ret;
	}

	public void setIgnoreFirstLine(boolean ignoreFirstLine)
	{
		this.ignoreFirstLine = ignoreFirstLine;
	}

}