package au.net.webdirector.admin.imports;

import java.util.List;
import java.util.Map;

public interface Procedure extends Runnable
{
	public final static int NOT_START = 0;

	public final static int PROCESSING = 1;

	public final static int COMPLISHED = 2;

	public final static int NOT_EXIST = 3;

	public String getID();

	public int getCompletePercentage();

	public int getStatus();

	public void destroy();

	public void init(Map properties);

	public Object getOperator();

	public String getResult();

	public boolean isSuccessful();

	public List<String> getErrors();

	public List<String> getResultDetails();
}
