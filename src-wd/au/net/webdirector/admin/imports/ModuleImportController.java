/**
 * @author Nick Yiming Gao
 * @date 2015-10-8
 */
package au.net.webdirector.admin.imports;

/**
 * 
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import au.net.webdirector.common.Defaults;

@Controller
public class ModuleImportController
{

	private static Logger logger = Logger.getLogger(ModuleImportController.class);

	@RequestMapping("/secure/data/import/upload")
	public String dataImportUpload(HttpServletRequest request) throws IllegalStateException, IOException
	{
		MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
		MultipartFile file = new ArrayList<MultipartFile>(multi.getFileMap().values()).get(0);
		File dir = new File(Defaults.getInstance().getStoreDir(), "_tmp/" + file.getName());
		if (!dir.exists())
		{
			dir.mkdirs();
		}
		File dataFile = new File(dir, file.getOriginalFilename());
		file.transferTo(dataFile);
		//String type = multi.getContentType(n);
		request.setAttribute("importFile", dataFile.getName());
		request.setAttribute("moduleName", multi.getParameter("moduleName"));
		request.setAttribute("arg1", multi.getParameter("deleteFirst"));
		request.setAttribute("arg2", multi.getParameter("importFileFormat"));
		request.setAttribute("importType", multi.getParameter("importType"));
		request.setAttribute("assetLive", multi.getParameter("assetLive"));
		request.setAttribute("switchOff", multi.getParameter("switchOff"));
		logger.debug(" uploaded ...");

		return "jsp/secure/iframe/data_import_page2";
	}

	@RequestMapping("/secure/data/import/do")
	public String dataImportDo(HttpServletRequest request) throws IllegalStateException, IOException
	{
		Defaults d = Defaults.getInstance();

		String moduleName = request.getParameter("moduleName");
		String importFile = request.getParameter("importFile");
		String importType = request.getParameter("importType");
		String assetLive = request.getParameter("assetLive");
		String deleteAll = request.getParameter("deleteAll");
		String ignoreFirstLine = request.getParameter("ignoreFirstLine");
		if (ignoreFirstLine == null)
		{
			ignoreFirstLine = "off";
		}
		String pKey = request.getParameter("pKey");
		String[] mapping = request.getParameterValues("mapping");

		String mapDir = d.getStoreDir() + "/_importFileMapping";
		String fileName = request.getParameter("mapFile");

		System.out.println("\n\n--------->fileName:" + fileName);
		System.out.println("\n\n--------->Full Path:" + mapDir + "/" + fileName);
		if (fileName != null && !fileName.equals(""))
		{
			File dir = new File(mapDir);
			if (!dir.exists())
				dir.mkdir();

			File file = new File(mapDir + "/" + fileName);
			Writer output = new BufferedWriter(new FileWriter(file));

			output.write(moduleName + "\n");
			for (int i = 0; i < mapping.length; i++)
			{
				String[] arrMap = mapping[i].split("----");
				output.write(mapping[i] + "\n");
			}
			output.close();
		}

		LinkedHashMap map = new LinkedHashMap();
		for (int i = 0; i < mapping.length; i++)
		{
			String[] arrMap = mapping[i].split("----");
			map.put(arrMap[1], arrMap[0]);
			//out.println("<br>"+mapping[i]);
		}


		String result = "Processing...";
		Procedure imp = CVSImporter.getInstance();
		Map prop = new HashMap();
		prop.put("moduleName", moduleName);
		prop.put("importFile", importFile);
		prop.put("importType", importType);
		prop.put("assetLive", assetLive);
		prop.put("deleteAll", deleteAll);
		prop.put("ignoreFirstLine", ignoreFirstLine);
		prop.put("map", map);
		prop.put("pKey", pKey);
		prop.put("context", request.getServletContext());
		imp.init(prop);
		request.setAttribute("AuditTrial_Importer", imp);
		new Thread(imp).start();
		request.setAttribute("moduleName", moduleName);
		request.setAttribute("importerId", imp.getID());
		return "jsp/secure/iframe/data_import_page2_h";
	}

	@RequestMapping("/secure/data/import/percentage")
	@ResponseBody
	public String dataImportPercentage(HttpServletRequest request) throws IllegalStateException, IOException
	{
		String id = request.getParameter("id");

		CVSImporter imp = CVSImporter.getInstance(id);

		request.setAttribute("AuditTrial_Importer", imp);
		if (imp == null)
		{
			return "{\"status\":\"" + Procedure.NOT_EXIST + "\", \"percentage\": \"100\", \"info\": \"\"}";
		}
		StringBuffer sb = new StringBuffer();
		sb.append("{\"status\":\"" + imp.getStatus() + "\", \"percentage\": \"" + imp.getCompletePercentage() + "\", \"info\": \"");
		if (imp.getStatus() == Procedure.COMPLISHED)
		{
			sb.append("<tr><td><font color='#000000'>").append(imp.getResult()).append("</font></td></tr>");
			if (imp.isSuccessful())
			{
				sb.append(formatDetails(imp.getResultDetails()));
			}
			else
			{
				sb.append(formatDetails(imp.getErrors()));
			}
		}
		sb.append("\"}");
		if (imp.getStatus() == Procedure.COMPLISHED)
		{
			imp.destroy();
		}
		return sb.toString();
	}


	@RequestMapping(value = "/secure/data/import/delete-map-file", method = RequestMethod.POST)
	@ResponseBody
	public boolean deleteMapFile(HttpServletRequest request) throws IllegalStateException, IOException
	{
		String storeDir = Defaults.getInstance().getStoreDir();
		String fileName = request.getParameter("importFileFormat");

		String mapDir = storeDir + "/_importFileMapping";

		System.out.println("\n\n--------->fileName:" + fileName);
		System.out.println("\n\n--------->Full Path:" + mapDir + "/" + fileName);
		boolean result = false;
		if (fileName != null && !fileName.equals(""))
		{
			File file = new File(mapDir + "/" + fileName);
			result = file.delete();
		}
		return result;
	}

	private String formatDetails(List<String> details)
	{
		StringBuffer sb = new StringBuffer();
		for (String value : details)
		{
			sb.append("<tr><td><font color='#000000'>").append(value).append("</font></td></tr>");
		}
		return sb.toString();
	}
}
