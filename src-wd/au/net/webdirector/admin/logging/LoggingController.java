package au.net.webdirector.admin.logging;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Sushant Verma
 */
@Controller
public class LoggingController
{
	Logger logger = Logger.getLogger(LoggingController.class);

	public static final String SESSION_LOG_HISTORY = "SESSION_LOG_HISTORY";

	public LoggingController()
	{
		logger.setLevel(Level.ALL);
	}

	@RequestMapping("/free/logging")
	public void recordLog(HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{
		String addr = request.getRemoteHost();
		String level = request.getParameter("level");
		String url = request.getParameter("url");
		String message = request.getParameter("message");

		int endOfUrl = url.lastIndexOf('?');
		if (endOfUrl > 0)
		{
			url = url.substring(0, endOfUrl);
		}
		String currentTimestamp = new Date().toString();
		String archiveMessage = String.format("[%-5s] %s [%s] %s : %s", level, addr, currentTimestamp, url, message);
		String consoleMessage = String.format("%s %s : %s", addr, url, message);

		List<String> history = (List<String>) session.getAttribute(SESSION_LOG_HISTORY);
		if (history == null)
		{
			history = Collections.synchronizedList(new LinkedList<String>());
			session.setAttribute(SESSION_LOG_HISTORY, history);
		}

		history.add(archiveMessage);
		while (history.size() > 1000)
		{
			history.remove(0);
		}
		if (level.equalsIgnoreCase("trace"))
		{
			logger.trace(consoleMessage);
		}
		else if (level.equalsIgnoreCase("debug"))
		{
			logger.debug(consoleMessage);
		}
		else if (level.equalsIgnoreCase("info"))
		{
			logger.info(consoleMessage);
		}
		else if (level.equalsIgnoreCase("warn"))
		{
			logger.warn(consoleMessage);
		}
		else if (level.equalsIgnoreCase("error"))
		{
			logger.error(consoleMessage);
		}
		else if (level.equalsIgnoreCase("fatal"))
		{
			logger.fatal(consoleMessage);
		}
	}

	@RequestMapping("/free/logging/report")
	public void sendLog(HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session,
			@RequestParam String description)
	{
		try
		{
			String agentString = request.getHeader("user-agent");

			String addr = request.getRemoteHost();
			logger.info("Emailing log4javascript logs for addr: " + addr);

			List<String> history = (List<String>) session.getAttribute(SESSION_LOG_HISTORY);

//			PendingEmail pe = new EmailBuilder(request).prepareErrorEmail(description, agentString, history);
//			pe.doSend();
		}
		catch (Exception e)
		{
			logger.fatal("Unable to send log4j logging email.", e);
		}
	}
}
