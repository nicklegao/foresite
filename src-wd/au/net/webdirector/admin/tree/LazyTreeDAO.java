package au.net.webdirector.admin.tree;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.jdbc.core.RowMapper;

import au.net.webdirector.admin.tree.mapper.ElementRowMapper;
import au.net.webdirector.admin.tree.mapper.LazyCategoryRowMapper;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.mapper.BooleanMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.HashMapMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.IntegerMapper;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;

public class LazyTreeDAO
{

	final int categoryLimit = Defaults.getCategoryNumberPerLoadInFolderTree();
	final int elementLimit = Defaults.getAssetNumberPerLoadInFolderTree();

	private boolean isWorkflowEnabled(String moduleName) throws SQLException
	{
		String query = "SELECT workflow_enabled FROM sys_moduleconfig WHERE internal_module_name = ? ";
		String[] params = new String[] { moduleName };
		return TreeDataAccess.getInstance().selectIntoList(query, new BooleanMapper(), params).get(0);
	}

	public Set<TreeNode> getCategories(String moduleName, String parentId, int pageNum, Set<String> permittedIDs, boolean isTopLevel) throws SQLException
	{
		moduleName = DatabaseValidation.encodeParam(moduleName);

		String inClause = null;
		if (permittedIDs != null)
		{
			inClause = buildInClause(permittedIDs);
		}

		if (isWorkflowEnabled(moduleName))
		{
			return getCategoriesWithWorkflow(moduleName, parentId, pageNum, inClause);
		}
		else
		{
			return getCategoriesWithoutWorkflow(moduleName, parentId, pageNum, inClause);
		}
	}

	private Set<TreeNode> getCategoriesWithoutWorkflow(String moduleName, String parentId, int pageNum, String inClause) throws SQLException
	{
		int offset = categoryLimit * pageNum;
		String query = "select distinct c.Category_id, c.Category_ParentID, c.ATTR_categoryName, c.folderLevel, if(i.id is null, 0, 1) as numOfChildren, c.Live, c.Live_date, c.Expire_date" +
				" from categories_" + moduleName + " c left join (" +
				"  select category_id as id, Category_ParentID as parent from categories_" + moduleName +
				"  union all select element_id as id, category_id as parent from elements_" + moduleName +
				" ) i on c.category_id = i.parent  " +
				" where" + (moduleName.toLowerCase().equalsIgnoreCase("users") ? " c.ATTRDATE_supportAuthTime > DATE(NOW()) and" : "") + " c.Category_ParentID = ? " + (inClause != null ? " and c.Category_id IN " + inClause : "") + " order by ATTR_categoryName " +
				" LIMIT " + categoryLimit + " OFFSET " + offset;
		String[] params = new String[] { parentId };

		Set<TreeNode> set1 = TreeDataAccess.getInstance().selectIntoSet(query, new LazyCategoryRowMapper(false), params);

//		if (moduleName.toLowerCase().equalsIgnoreCase("users")) {
////			String queryTrial = "select distinct c.Category_id, c.Category_ParentID, c.ATTR_categoryName, c.folderLevel, if(i.id is null, 0, 1) as numOfChildren, c.Live, c.Live_date, c.Expire_date" +
////					" from categories_" + moduleName + " c left join (" +
////					"  select category_id as id, Category_ParentID as parent from categories_" + moduleName +
////					"  union all select element_id as id, category_id as parent from elements_" + moduleName +
////					" ) i on c.category_id = i.parent " +
////					(parentId.equalsIgnoreCase("0") ?
////							" where c.ATTRDROP_pricingPlan = 'TRIAL' and c.Category_ParentID = ? " + (inClause != null ? " and c.Category_id IN " + inClause : "") + " order by ATTR_categoryName "
////							: "left join (select ATTRDROP_pricingPlan as plan from categories_" + moduleName +
////							" ) p on p.plan = 'TRIAL' where c.Category_ParentID = ? " + (inClause != null ? " and c.Category_id IN " + inClause : "") + " order by ATTR_categoryName "
////					) +
////					" LIMIT " + categoryLimit + " OFFSET " + offset;
////			String[] paramsTrial = new String[] { parentId };
////
////			Set<TreeNode> setTrial = TreeDataAccess.getInstance().selectIntoSet(queryTrial, new LazyCategoryRowMapper(false), paramsTrial);
////			for (TreeNode set : setTrial) {
////				if (!set1.contains(set)) {
////					set1.add(set);
////				}
////			}
//		}
		return set1;
	}

	public Set<TreeNode> getCategoriesWithWorkflow(String moduleName, String parentId, int pageNum, String inClause) throws SQLException
	{
		String subSql = "select distinct c.Category_id, c.Category_ParentID, c.ATTR_categoryName, c.folderLevel, if(i.id is null, 0, 1) as numOfChildren, c.Live, c.Live_date, c.Expire_date" +
				" from categories_" + moduleName + " c left join (" +
				"  select category_id as id, Category_ParentID as parent from categories_" + moduleName +
				"  union all select element_id as id, category_id as parent from elements_" + moduleName +
				" ) i on c.category_id = i.parent  ";
		int offset = categoryLimit * pageNum;
		String query = " SELECT w.mode, w.D_DRAFT_ID, w.D_STATUS, w.ATTR_categoryName, w.Category_id, w.Category_ParentID, w.folderLevel, w.numOfChildren, w.Live, w.Live_date, w.Expire_date " +
				"	from  " +
				"		(select 'live' as mode, s.D_DRAFT_ID, s.D_STATUS, c.ATTR_categoryName, c.Category_id, c.Category_ParentID, c.folderLevel, c.numOfChildren, c.Live, c.Live_date, c.Expire_date" +
				"			from (" + subSql + ") c " +
				"				LEFT JOIN sys_draft_rec s " +
				"				on c.Category_id = s.D_DATA_ID " +
				"					and s.D_MODULE = ? " +
				"					and s.D_TABLE = 'Categories' " +
				"			where"  + (moduleName.toLowerCase().equalsIgnoreCase("users") ? " c.ATTRDATE_supportAuthTime > DATE(NOW()) and" : "") + " c.Category_ParentID = ? " + (inClause != null ? " and c.Category_id IN " + inClause : "") +
				"		union all " +
				"		select 'workflow' as mode, s.D_DRAFT_ID as 'D_DRAFT_ID', s.D_STATUS, d.ATTR_categoryName, 0 as Category_id, d.Category_ParentID, d.folderLevel, " +
				"			0 as numOfChildren, d.Live, d.Live_date, d.Expire_date " +
				"			from categories_" + moduleName + "_draft d, sys_draft_rec s " +
				"			where d.Category_id = s.D_DRAFT_ID " +
				"			and s.D_MODULE = ? " +
				"			and s.D_TABLE = 'Categories' " +
				"			and d.Category_ParentID = ? " +
				"			and s.D_DATA_ID = 0) w " +
				" order by ATTR_categoryName " +
				" LIMIT " + categoryLimit + " OFFSET " + offset;
		String[] params = new String[] { moduleName, parentId, moduleName, parentId };
		return TreeDataAccess.getInstance().selectIntoSet(query, new LazyCategoryRowMapper(true), params);
	}

	public Set<TreeNode> getElements(String moduleName, String categoryId, int pageNum) throws SQLException
	{
		moduleName = DatabaseValidation.encodeParam(moduleName);

		if (isWorkflowEnabled(moduleName))
		{
			return getElementsWithWorkflow(moduleName, categoryId, pageNum);
		}
		else
		{
			return getElementsWithoutWorkflow(moduleName, categoryId, pageNum);
		}
	}

	public Set<TreeNode> getElementsWithoutWorkflow(String moduleName, String categoryId, int pageNum) throws SQLException
	{
		int offset = elementLimit * pageNum;
		String query1 = "Select Element_id, Category_id, ATTR_Headline, Live, Live_date, Expire_date " +
				" from elements_" + moduleName + " where Category_ID = ?" + (moduleName.toLowerCase().equalsIgnoreCase("users") ? " and ATTRDATE_supportAuthTime > DATE(NOW())" : "") + " order by ATTR_Headline " +
				" LIMIT " + elementLimit + " OFFSET " + offset;
		String[] params = new String[] { categoryId };
		Set<TreeNode> set1 = TreeDataAccess.getInstance().selectIntoSet(query1, new ElementRowMapper(false), params);

//		if (moduleName.toLowerCase().equalsIgnoreCase("users"))
//		{
////			String queryTrial = "Select e.Element_id, e.Category_id, e.ATTR_Headline, e.Live, e.Live_date, e.Expire_date " +
////					" from elements_" + moduleName + " e, categories_users c1 where c1.ATTRDROP_pricingPlan = 'TRIAL' and e.Category_ID = ? order by e.ATTR_Headline " +
////					" LIMIT " + elementLimit + " OFFSET " + offset;
////			String[] paramsTrial = new String[] { categoryId };
////			Set<TreeNode> setTrial = TreeDataAccess.getInstance().selectIntoSet(queryTrial, new ElementRowMapper(false), paramsTrial);
////			for (TreeNode set : setTrial) {
////				if (!set1.contains(set)) {
////					set1.add(set);
////				}
////			}
//		}
		return set1;
	}

	public Set<TreeNode> getElementsWithWorkflow(String moduleName, String categoryId, int pageNum) throws SQLException
	{
		int offset = elementLimit * pageNum;
		String query = " SELECT w.mode, w.D_DRAFT_ID, w.D_STATUS, w.ATTR_Headline, w.Element_id, w.Category_id, w.Live, w.Live_date, w.Expire_date " +
				"	from  " +
				"		(select 'live' as mode, s.D_DRAFT_ID as 'D_DRAFT_ID', s.D_STATUS, e.ATTR_Headline, e.Element_id, e.Category_id, e.Live, e.Live_date, e.Expire_date " +
				"			FROM elements_" + moduleName + " e " +
				"				LEFT JOIN sys_draft_rec s " +
				"				on e.ELEMENT_ID = s.D_DATA_ID " +
				"					and s.D_MODULE = ? " +
				"					and s.D_TABLE = 'elements' " +
				"			WHERE e.Category_ID = ? " +
				"		union all " +
				"		select 'workflow' as mode, s.D_DRAFT_ID as 'D_DRAFT_ID', s.D_STATUS, d.ATTR_Headline, 0 as Element_id, d.Category_id, d.Live, d.Live_date, d.Expire_date " +
				"			from elements_" + moduleName + "_draft d, sys_draft_rec s " +
				"			where d.ELEMENT_ID = s.D_DRAFT_ID " +
				"			and s.D_MODULE = ? " +
				"			and s.D_TABLE = 'elements' " +
				"			and d.Category_ID = ? " +
				"			and s.D_DATA_ID = 0) w " +
				" order by attr_headline " +
				" LIMIT " + elementLimit + " OFFSET " + offset;
		String[] params = new String[] { moduleName, categoryId, moduleName, categoryId };
		return TreeDataAccess.getInstance().selectIntoSet(query, new ElementRowMapper(true), params);
	}

	public int countCategories(String moduleName, String parentId, Set<String> permittedIDs) throws SQLException
	{
		moduleName = DatabaseValidation.encodeParam(moduleName);

		String query;
		if (permittedIDs != null && permittedIDs.size() > 0)
		{
			String inClause = buildInClause(permittedIDs);
			query = "select count(*) from categories_" + moduleName + " where Category_ParentID = ? and Category_id IN " + inClause;
		}
		else
		{
			query = "select count(*) from categories_" + moduleName + " where Category_ParentID = ? ";
		}
		String[] params = new String[] { parentId };
		return TreeDataAccess.getInstance().selectIntoList(query, new IntegerMapper(), params).get(0);
	}

	public int countElements(String moduleName, String categoryId) throws SQLException
	{
		moduleName = DatabaseValidation.encodeParam(moduleName);

		String query = "select count(*) from elements_" + moduleName + " where Category_ID = ? ";
		String[] params = new String[] { categoryId };
		return TreeDataAccess.getInstance().selectIntoList(query, new IntegerMapper(), params).get(0);
	}

	/*public CategoryPrivileges getCategoryPrivileges(String moduleName, String categoryId, String userId) {
		String query = "select COALESCE(MAX(ATTRINTEGER_privileges),0) AS privileges " +
				"from user_module_privileges where module_name = ? and category_id = ? and user_id = ? ";
		String[] params = new String[] { moduleName, categoryId, userId };
		int privileges = (Integer) db.select(query, params, new IntegerMapper()).get(0);
		return new CachedCategoryPrivileges(privileges);
	}*/

	public List<Map<String, Object>> getCategoriesByDisplayOrder(String moduleName, String parentId) throws SQLException
	{
		moduleName = DatabaseValidation.encodeParam(moduleName);
		String query = "select Category_id AS id, ATTR_categoryName AS title from Categories_" + moduleName + " where category_parentid = ? order by display_order";
		String[] params = new String[] { parentId };
		return TreeDataAccess.getInstance().selectIntoList(query, new HashMapMapper(), params);
	}

	public List<Map<String, Object>> getElementsByDisplayOrder(String moduleName, String parentId) throws SQLException
	{
		moduleName = DatabaseValidation.encodeParam(moduleName);
		String query = "select Element_id AS id, ATTR_Headline AS title from elements_" + moduleName +
				" where live='1' and (live_date is null or live_date < now()) and (expire_date is null or expire_date > now()) and category_id = ? order by display_order";
		String[] params = new String[] { parentId };
		return TreeDataAccess.getInstance().selectIntoList(query, new HashMapMapper(), params);
	}

	public Map<String, TreeNode> getAllElements(String moduleName) throws SQLException
	{
		moduleName = DatabaseValidation.encodeParam(moduleName);
		String query = "select element_id, category_id, ATTR_Headline, Live, Live_date, Expire_date from elements_" + moduleName;
		return TreeDataAccess.getInstance().selectIntoMap(query, new ElementRowMapper(false), "element_id");
	}

	public Map<String, TreeNode> getAllCategories(String moduleName, Set<String> permittedIDs) throws SQLException
	{
		moduleName = DatabaseValidation.encodeParam(moduleName);
		String query = "select distinct c.Category_id, c.Category_ParentID, c.ATTR_categoryName, c.folderLevel, if(i.id is null, 0, 1) as numOfChildren" +
				" from categories_" + moduleName + " c left join (" +
				"  select category_id as id, Category_ParentID as parent from categories_" + moduleName +
				"  union all select element_id as id, category_id as parent from elements_" + moduleName +
				" ) i on c.category_id = i.parent  ";
		if (permittedIDs != null)
		{
			query += " where c.Category_id IN " + buildInClause(permittedIDs);
		}
		return TreeDataAccess.getInstance().selectIntoMap(query, new CategoryRowMapper(), "category_id");
	}

	private static class CategoryRowMapper implements RowMapper<TreeNode>
	{
		@Override
		public TreeNode mapRow(ResultSet rs, int rowNum) throws SQLException
		{
			TreeNode node = new TreeNode();
			node.setKey(rs.getString("Category_id"));
			node.setParentId(rs.getString("Category_ParentID"));
			node.setTitle(rs.getString("ATTR_categoryName"));
			node.setFolderLevel(rs.getInt("folderLevel"));
			node.setFolder(true);
			node.setExpanded(false);
			if (rs.getInt("numOfChildren") > 0)
			{
				node.setLazy(true);
			}
			else
			{
				node.setLazy(false);
			}
			return node;
		}
	}

	private String buildInClause(Set<String> items)
	{
		if (items.size() == 0)
			return null;
		StringBuilder sb = new StringBuilder("(");
		for (String s : items)
		{
			if (s.length() > 0)
				sb.append(s).append(",");
		}
		if (items.size() > 0)
		{
			sb.deleteCharAt(sb.length() - 1);
		}
		sb.append(")");
		return sb.toString();
	}

}
