/**
 * @author Patrick King
 * @date 2 Apr 2015
 */
package au.net.webdirector.admin.tree.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.springframework.jdbc.core.RowMapper;

import au.net.webdirector.admin.tree.TreeNode;
import au.net.webdirector.admin.workflow.WorkflowInfo;

public class LazyCategoryRowMapper implements RowMapper<TreeNode>
{

	private boolean isWorkflowEnabled;

	public LazyCategoryRowMapper(boolean isWorkflowEnabled)
	{
		this.isWorkflowEnabled = isWorkflowEnabled;
	}

	@Override
	public TreeNode mapRow(ResultSet rs, int row) throws SQLException
	{
		TreeNode node = new TreeNode();
		node.setKey(rs.getString("Category_id"));
		node.setParentId(rs.getString("Category_ParentID"));
		node.setTitle(rs.getString("ATTR_categoryName"));
		node.setFolderLevel(rs.getInt("folderLevel"));
		node.setFolder(true);
		int numOfChildren = rs.getInt("numOfChildren");
		//node.setNumOfChildren(numOfChildren);
		node.setLazy(numOfChildren > 0 ? true : false);

		if (rs.getInt("Live") == 1)
		{
			node.setExtraClasses(TreeNode.STATUS_LIVE);

			Timestamp liveDate = rs.getTimestamp("Live_date");
			Timestamp expireDate = rs.getTimestamp("Expire_date");
			Timestamp now = new Timestamp(System.currentTimeMillis());

			if (liveDate != null)
			{
				if (now.before(liveDate))
				{
					node.setExtraClasses(TreeNode.STATUS_SCHEDULED_FUTURE);
				}
				else
				{
					node.setExtraClasses(TreeNode.STATUS_SCHEDULED_LIVE);
				}
			}

			if (expireDate != null)
			{
				if (now.after(expireDate))
				{
					node.setExtraClasses(TreeNode.STATUS_SCHEDULED_PAST);
				}
			}
		}
		else
		{
			node.setExtraClasses(TreeNode.STATUS_NOT_LIVE);
		}

		if (isWorkflowEnabled)
		{
			if (rs.getString("mode").equals("workflow"))
			{
				node.setWorkflow(true);
				node.addExtraClasses(TreeNode.WORKFLOW_NEW);
				node.setDraftId(rs.getString("D_DRAFT_ID"));
				node.setKey(TreeNode.PREFIX_WORKFLOW_CATEGORY + node.getDraftId());
			}
			String status = rs.getString("D_STATUS");
			if (WorkflowInfo.DRAFT.equals(status) || WorkflowInfo.DECLINED.equals(status))
			{
				node.addExtraClasses(TreeNode.WORKFLOW_STATUS_DRAFT);
			}
			else if (WorkflowInfo.TO_BE_APPROVED.equals(status) || WorkflowInfo.TO_BE_DELETED.equals(status))
			{
				node.addExtraClasses(TreeNode.WORKFLOW_STATUS_PENDING_APPROVAL);
			}
		}
		return node;
	}

}