package au.net.webdirector.admin.tree;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import au.net.webdirector.admin.modules.privileges.CategoryPrivileges;

public class TreeNode
{

	public static final String STATUS_NOT_LIVE = "status-not-live";
	public static final String STATUS_LIVE = "status-live";
	public static final String STATUS_SCHEDULED_PAST = "status-scheduled-past";
	public static final String STATUS_SCHEDULED_LIVE = "status-scheduled-live";
	public static final String STATUS_SCHEDULED_FUTURE = "status-scheduled-future";

	public static final String WORKFLOW_NEW = "workflow-new";
	public static final String WORKFLOW_STATUS_DRAFT = "workflow-status-draft";
	public static final String WORKFLOW_STATUS_PENDING_APPROVAL = "workflow-status-pending-approval";

	public static final String PREFIX_WORKFLOW_ELEMENT = "W_E_";
	public static final String PREFIX_WORKFLOW_CATEGORY = "W_C_";

	private Set<TreeNode> children;
	private CategoryPrivileges privileges;

	private String key;
	private String parentId;
	private String title;
	private int folderLevel;
	private boolean folder;
	private boolean lazy;
	private boolean expanded;
	private boolean workflow;
	private String draftId;
	private String extraClasses;

	public TreeNode()
	{
	}

	public boolean hasChildren()
	{
		return children != null && children.size() > 0;
	}

	public Set<TreeNode> getChildren()
	{
		return children;
	}

	public void setChildren(Set<TreeNode> children)
	{
		this.children = children;
	}

	public void addChild(TreeNode node)
	{
		if (this.children == null)
		{
			this.children = new TreeSet<TreeNode>(new Comparator<TreeNode>()
			{
				@Override
				public int compare(TreeNode o1, TreeNode o2)
				{
					String s1 = o1.key != null ? o1.key : "";
					String s2 = o2.key != null ? o2.key : "";
					return s1.compareToIgnoreCase(s2);
				}
			});
		}
		this.children.add(node);
	}

	public CategoryPrivileges getPrivileges()
	{
		return privileges;
	}

	public void setPrivileges(CategoryPrivileges privileges)
	{
		this.privileges = privileges;
	}

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	public String getParentId()
	{
		return parentId;
	}

	public void setParentId(String parentId)
	{
		this.parentId = parentId;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public int getFolderLevel()
	{
		return folderLevel;
	}

	public void setFolderLevel(int folderLevel)
	{
		this.folderLevel = folderLevel;
	}

	public boolean isFolder()
	{
		return folder;
	}

	public void setFolder(boolean folder)
	{
		this.folder = folder;
	}

	public boolean isLazy()
	{
		return lazy;
	}

	public void setLazy(boolean lazy)
	{
		this.lazy = lazy;
	}

	public String getExtraClasses()
	{
		return extraClasses;
	}

	public void setExtraClasses(String extraClasses)
	{
		this.extraClasses = extraClasses;
	}

	public void addExtraClasses(String extraClasses)
	{
		if (this.extraClasses == null)
		{
			this.extraClasses = extraClasses;
		}
		else
		{
			this.extraClasses += " " + extraClasses;
		}
	}

	public boolean isExpanded()
	{
		return expanded;
	}

	public void setExpanded(boolean expanded)
	{
		this.expanded = expanded;
	}

	public boolean isWorkflow()
	{
		return workflow;
	}

	public void setWorkflow(boolean workflow)
	{
		this.workflow = workflow;
	}

	public String getDraftId()
	{
		return draftId;
	}

	public void setDraftId(String draftId)
	{
		this.draftId = draftId;
	}

	@Override
	public int hashCode()
	{
		return (key == null) ? 0 : key.hashCode();
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TreeNode other = (TreeNode) obj;
		if (key == null)
		{
			if (other.key != null)
				return false;
		}
		else if (!key.equals(other.key))
			return false;
		return true;
	}

/*	@Override
	public int compareTo(TreeNode o) {
		return this.key.compareTo(o.key);
	}*/
}
