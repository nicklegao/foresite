package au.net.webdirector.admin.tree;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.JdbcUtils;

import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;

public class TreeDataAccess extends TransactionDataAccess
{

	public static TreeDataAccess getInstance()
	{
		return new TreeDataAccess(true);
	}

	public TreeDataAccess(boolean autoCommit)
	{
		super(autoCommit);
	}

	public <T> List<T> selectIntoList(String query, RowMapper<T> mapper, String... args) throws SQLException
	{
		return doSelect(query, mapper, args);
	}

	public <T> Map<String, T> selectIntoMap(String query, RowMapper<T> mapper, String keyColumn, String... args) throws SQLException
	{
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try
		{
			prepareConnection();
			stmt = conn.prepareStatement(query);
			for (int i = 0; i < args.length; i++)
			{
				stmt.setObject(i + 1, args[i]);
			}
			rs = stmt.executeQuery();
			LinkedHashMap<String, T> map = new LinkedHashMap<String, T>();
			while (rs.next())
			{
				String key = rs.getString(keyColumn);
				T value = mapper.mapRow(rs, rs.getRow());
				map.put(key, value);
			}
			return map;
		}
		finally
		{
			JdbcUtils.closeResultSet(rs);
			JdbcUtils.closeStatement(stmt);
			if (autoCommit)
			{
				releaseConnection();
			}
		}
	}

	public <T> Set<T> selectIntoSet(String query, RowMapper<T> mapper, String... args) throws SQLException
	{
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try
		{
			prepareConnection();
			stmt = conn.prepareStatement(query);
			for (int i = 0; i < args.length; i++)
			{
				stmt.setObject(i + 1, args[i]);
			}
			rs = stmt.executeQuery();
			LinkedHashSet<T> set = new LinkedHashSet<T>();
			while (rs.next())
			{
				T value = mapper.mapRow(rs, rs.getRow());
				set.add(value);
			}
			return set;
		}
		finally
		{
			JdbcUtils.closeResultSet(rs);
			JdbcUtils.closeStatement(stmt);
			if (autoCommit)
			{
				releaseConnection();
			}
		}
	}
}
