package au.net.webdirector.admin.tree;

import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import webdirector.utils.Blog.BlogCategoryUtil;
import au.net.webdirector.admin.modules.SortItemDAO;
import au.net.webdirector.admin.modules.privileges.CategoryPrivileges;
import au.net.webdirector.admin.modules.privileges.PrivilegeUtils;
import au.net.webdirector.admin.utils.SecureJSPPath;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.search.Search;
import au.net.webdirector.common.utils.module.AssetService;
import au.net.webdirector.common.utils.module.CategoryService;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Controller
@RequestMapping("/folderTree")
public class LazyTreeService
{

	public static final String COPY_TARGET_IDS = "COPY_TARGET_IDS";

	final int categoryLimit = Defaults.getCategoryNumberPerLoadInFolderTree();
	final int elementLimit = Defaults.getAssetNumberPerLoadInFolderTree();

	LazyTreeDAO dao = new LazyTreeDAO();

	@RequestMapping("/lazyload")
	@ResponseBody
	public Set<TreeNode> lazyloadNode(
			@RequestParam String moduleName,
			@RequestParam int levels,
			@RequestParam int curLevel,
			@RequestParam String parentId,
			@RequestParam int pageNum,
			@RequestParam(required = false) Integer pageTo,
			@RequestParam boolean filterByPrivileges,
			@RequestParam(required = false) String expandedNodes,
			HttpSession session)
	{
		Map<String, Map<String, Object>> expandedNodeMap = parseNodeMap(expandedNodes);
		PrivilegeUtils privUtils = null;
		if (filterByPrivileges)
		{
			privUtils = PrivilegeUtils.getCachedInstance(moduleName, session, false);
		}
		//Load initial node set
		Set<TreeNode> nodeSets = fetchChildren(moduleName, levels, curLevel, parentId, pageNum, privUtils, expandedNodeMap);

		//Load up to page
		if (pageTo != null)
		{
			for (pageNum = 1; pageNum <= pageTo; pageNum++)
			{
				Set<TreeNode> nextNodeSet = fetchChildren(moduleName, levels, curLevel, parentId, pageNum, privUtils, expandedNodeMap);

				//If next set of nodes exists
				if (nextNodeSet != null)
				{
					//Removal of load more node
					Iterator<TreeNode> treeIterator = nodeSets.iterator();
					while (treeIterator.hasNext())
					{
						TreeNode node = treeIterator.next();
						String key = node.getKey();
						if (key.equals(parentId + "-load"))
						{
							treeIterator.remove();
						}
					}
					//Add the next node set to existing node set
					nodeSets.addAll(nextNodeSet);
				}
			}
		}

		return nodeSets;
	}

	public Set<TreeNode> fetchChildren(
			String moduleName,
			int levels,
			int curLevel,
			String parentId,
			int pageNum,
			PrivilegeUtils privUtils,
			Map<String, Map<String, Object>> expandedNodeMap)
	{
		Set<TreeNode> nodes = null;
		try
		{
			Set<String> permittedIDs = privUtils != null ? privUtils.getCategoryVisibility() : null;
			int itemTotal, remainingItems;
			if (curLevel + 1 < levels)
			{
				itemTotal = dao.countCategories(moduleName, parentId, permittedIDs);
				nodes = dao.getCategories(moduleName, parentId, pageNum, permittedIDs, false);
				remainingItems = itemTotal - (categoryLimit * pageNum) - nodes.size();
			}
			else if (curLevel + 1 == levels)
			{
				itemTotal = dao.countCategories(moduleName, parentId, permittedIDs);
				nodes = dao.getCategories(moduleName, parentId, pageNum, permittedIDs, true);
				remainingItems = itemTotal - (categoryLimit * pageNum) - nodes.size();
			}
			else
			{
				itemTotal = dao.countElements(moduleName, parentId);
				nodes = dao.getElements(moduleName, parentId, pageNum);
				remainingItems = itemTotal - (elementLimit * pageNum) - nodes.size();
			}

			if (curLevel < levels && privUtils != null)
			{
				for (TreeNode node : nodes)
				{
					boolean isWorkflow = node.getDraftId() != null;
					String permissionID = isWorkflow ? node.getParentId() : node.getKey();

					CategoryPrivileges setTo = privUtils.getPrivilegeForCategory(permissionID);
					// move check to javascript as admin cannot insert when it's a workflow too, and admin has no privUtils
					/*
					if (isWorkflow)
					{
						CachedCategoryPrivileges modifiedPriv = new CachedCategoryPrivileges(setTo.getSum());
						modifiedPriv.setInsert(false);
						setTo = modifiedPriv;
					}
					*/
					node.setPrivileges(setTo);
				}
			}

			if (remainingItems > 0)
			{
				TreeNode loadMore = new TreeNode();
				loadMore.setKey(parentId + "-load");
				loadMore.setTitle(remainingItems + " more...");
				loadMore.setExtraClasses("load-more");
				nodes.add(loadMore);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		if (expandedNodeMap != null)
		{
			loadExpanded(moduleName, levels, nodes, parentId, privUtils, expandedNodeMap);
		}
		return nodes;
	}

	private void loadExpanded(String moduleName, int levels, Set<TreeNode> nodes, String currentNodeId, PrivilegeUtils privUtils, Map<String, Map<String, Object>> expandedNodeMap)
	{
		Map<String, Object> expandedChildren = expandedNodeMap.get(currentNodeId);
		if (expandedChildren == null)
		{
			return;
		}
		for (TreeNode node : nodes)
		{
			if (expandedChildren.containsKey(node.getKey()))
			{
				Set<TreeNode> children = fetchChildren(moduleName, levels, node.getFolderLevel(), node.getKey(), 0, privUtils, expandedNodeMap);
				node.setChildren(children);
				node.setExpanded(true);
				node.setLazy(false);
			}
		}
	}

	@RequestMapping("/initTree")
	@ResponseBody
	public List<TreeNode> initTree(
			@RequestParam String moduleName,
			@RequestParam int levels,
			@RequestParam boolean filterByPrivileges,
			@RequestParam(required = false) String expandedNodes,
			HttpSession session)
	{
		Map<String, Map<String, Object>> expandedNodeMap = parseNodeMap(expandedNodes);

		CategoryPrivileges rootPrivileges = null;
		PrivilegeUtils privUtils = null;
		if (filterByPrivileges)
		{
			privUtils = PrivilegeUtils.getCachedInstance(moduleName, session, true);
			rootPrivileges = privUtils.getPrivilegeForCategory("0");
		}

		TreeNode root = new TreeNode();
		root.setKey("0");
		root.setFolder(true);
		root.setExpanded(true);
		Set<TreeNode> firstLevel = fetchChildren(moduleName, levels, 0, "0", 0, privUtils, expandedNodeMap);
		root.setChildren(firstLevel);
		root.setExtraClasses("root-category");
		root.setPrivileges(rootPrivileges);

		List<TreeNode> nodes = new ArrayList<TreeNode>();
		nodes.add(root);
		return nodes;
	}

	private Map<String, Map<String, Object>> parseNodeMap(String json)
	{
		Type type = new TypeToken<Map<String, Map<String, Object>>>()
		{
		}.getType();
		return new Gson().fromJson(json, type);
	}

	@RequestMapping("/filterTree")
	@ResponseBody
	public List<TreeNode> filterTree(@RequestParam String moduleName,
			@RequestParam int levels,
			@RequestParam boolean filterByPrivileges,
			@RequestParam String filterText,
			@RequestParam String searchMode,
			HttpSession session)
	{
		try
		{
			Search search = new Search(moduleName);
			Set<String> matches = search.searchTree(filterText, searchMode);
			PrivilegeUtils privUtils = null;
			if (filterByPrivileges)
			{
				privUtils = PrivilegeUtils.getCachedInstance(moduleName, session, true);
			}
			if (searchMode.equals("category"))
			{
				return buildCategorySearchTree(moduleName, matches, privUtils);
			}
			else
			{
				return buildElementSearchTree(moduleName, matches, privUtils);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	private List<TreeNode> buildCategorySearchTree(String moduleName, Set<String> matches, PrivilegeUtils privUtils) throws SQLException
	{
		Set<String> permittedIDs = privUtils != null ? privUtils.getCategoryVisibility() : null;
		Map<String, TreeNode> categoryNodes = dao.getAllCategories(moduleName, permittedIDs);

		TreeNode root = new TreeNode();
		root.setKey("0");
		root.setFolder(true);
		root.setExpanded(true);
		root.setExtraClasses("root-category");
		categoryNodes.put("0", root);

		if (privUtils != null)
		{
			for (TreeNode node : categoryNodes.values())
			{
				node.setPrivileges(privUtils.getPrivilegeForCategory(node.getKey()));
			}
		}

		List<TreeNode> filteredNodes = new ArrayList<TreeNode>();
		filteredNodes.add(root);

		for (String match : matches)
		{
			TreeNode node = categoryNodes.get(match);
			attachToParent(node, categoryNodes);
		}
		return filteredNodes;
	}

	private List<TreeNode> buildElementSearchTree(String moduleName, Set<String> matches, PrivilegeUtils privUtils) throws SQLException
	{
		Set<String> permittedIDs = privUtils != null ? privUtils.getCategoryVisibility() : null;
		Map<String, TreeNode> elementNodes = dao.getAllElements(moduleName);
		Map<String, TreeNode> categoryNodes = dao.getAllCategories(moduleName, permittedIDs);

		TreeNode root = new TreeNode();
		root.setKey("0");
		root.setFolder(true);
		root.setExpanded(true);
		root.setExtraClasses("root-category");
		categoryNodes.put("0", root);

		if (privUtils != null)
		{
			for (TreeNode node : categoryNodes.values())
			{
				node.setPrivileges(privUtils.getPrivilegeForCategory(node.getKey()));
			}
		}

		List<TreeNode> filteredNodes = new ArrayList<TreeNode>();
		filteredNodes.add(root);

		for (String match : matches)
		{
			TreeNode node = elementNodes.get(match);
			attachToParent(node, categoryNodes);
		}
		return filteredNodes;
	}

	@RequestMapping("/expandTree")
	@ResponseBody
	public List<TreeNode> filterTree(@RequestParam String moduleName,
			@RequestParam String id,
			@RequestParam int levels,
			@RequestParam boolean filterByPrivileges,
			@RequestParam(required = false) String expandedNodes,
			@RequestParam String expandMode,
			HttpSession session)
	{
		Map<String, Map<String, Object>> expandedNodeMap = parseNodeMap(expandedNodes);
		try
		{
			PrivilegeUtils privUtils = null;
			if (filterByPrivileges)
			{
				privUtils = PrivilegeUtils.getCachedInstance(moduleName, session, true);
			}
			return buildTreeFromParentId(moduleName, id, levels, privUtils, expandedNodeMap);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	private List<TreeNode> buildTreeFromParentId(String moduleName, String id, int levels, PrivilegeUtils privUtils, Map<String, Map<String, Object>> expandedNodeMap) throws SQLException
	{
		Set<String> permittedIDs = privUtils != null ? privUtils.getCategoryVisibility() : null;
		Map<String, TreeNode> elementNodes = dao.getAllElements(moduleName);
		Map<String, TreeNode> categoryNodes = dao.getAllCategories(moduleName, permittedIDs);
		String parentId = null;
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		Map<String, Object> rootMap = new LinkedHashMap<String, Object>();

		//Initilisation of ExpandedNodeMap and addition of what we want to expand
		for (Map.Entry<String, TreeNode> entry : categoryNodes.entrySet())
		{
			TreeNode node = entry.getValue();
			if (node.getKey().equals(id))
			{
				parentId = node.getParentId();
				//Checking if the root has expanded
				if (expandedNodeMap != null && expandedNodeMap.get("0") != null)
				{
					rootMap = expandedNodeMap.get("0");
					if (rootMap.get(parentId) == null)
					{
						rootMap.put(parentId, true);
					}
				}
				else
				{
					rootMap.put(parentId, true);
				}
				//Adding the category to the tree unless the parent is root
				if (expandedNodeMap != null && expandedNodeMap.get(parentId) != null)
				{
					map = expandedNodeMap.get(parentId);
				}
				map.put(id, true);
			}
		}

		if (expandedNodeMap == null)
		{
			expandedNodeMap = new LinkedHashMap<String, Map<String, Object>>();
		}

		expandedNodeMap.put("0", rootMap);
		expandedNodeMap.put(parentId, map);


		Set<TreeNode> firstLevel = fetchChildren(moduleName, levels, 0, "0", 0, privUtils, expandedNodeMap);
		TreeNode root = new TreeNode();
		root.setKey("0");
		root.setFolder(true);
		root.setExpanded(true);
		root.setExtraClasses("root-category");
		categoryNodes.put("0", root);

		if (privUtils != null)
		{
			for (TreeNode node : categoryNodes.values())
			{
				node.setPrivileges(privUtils.getPrivilegeForCategory(node.getKey()));
			}
		}

		List<TreeNode> tree = new ArrayList<TreeNode>();
		tree.add(root);

		for (TreeNode first : firstLevel)
		{
			attachToParent(first, categoryNodes);
		}

		return tree;
	}

	private void attachToParent(TreeNode node, Map<String, TreeNode> categoryNodes)
	{
		if (node == null || node.getKey().equals("0"))
		{
			return;
		}
		TreeNode parent = categoryNodes.get(node.getParentId());
		if (parent == null)
		{
			return;
		}
		parent.addChild(node);
		parent.setExpanded(true);
		parent.setLazy(false);
		attachToParent(parent, categoryNodes);
	}

	@RequestMapping("/deleteElement")
	@ResponseBody
	public boolean deleteElement(@RequestParam String moduleName, @RequestParam int id)
	{
		boolean success = false;
		try
		{
			AssetService assetService = new AssetService();
			success = assetService.deleteAsset(moduleName, id);
			if (moduleName.equalsIgnoreCase("FLEX11"))
			{
				BlogCategoryUtil bcu = new BlogCategoryUtil();
				bcu.deleteBlogCategoriesMapping("" + id);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return success;
	}

	@RequestMapping("/deleteCategory")
	@ResponseBody
	public boolean deleteCategory(@RequestParam String moduleName, @RequestParam int id)
	{
		boolean success = false;
		try
		{
			CategoryService categoryService = new CategoryService();
			success = categoryService.deleteCategory(moduleName, id);
			if (moduleName.equalsIgnoreCase("FLEX11"))
			{
				BlogCategoryUtil bcu = new BlogCategoryUtil();
				bcu.deleteBlogCategories("" + id);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return success;
	}

	@RequestMapping("/updateSortingOrder")
	@ResponseBody
	public void updateSortingOrder(@RequestParam String orderString, @RequestParam String moduleName, @RequestParam String moduleType)
	{
		SortItemDAO dao = new SortItemDAO();
		dao.updateSortingOrder(orderString, moduleName, moduleType);
	}

	@RequestMapping("/copyElements")
	@ResponseBody
	public SimpleJsonResponse copyElements(HttpServletRequest request,
			@RequestParam("items[]") int[] items, @RequestParam int parentId, @RequestParam String moduleName)
	{
		boolean success = false;
		try
		{
			AssetService assetService = new AssetService();
			ArrayList<Integer> copyTargetIds = new ArrayList<Integer>();

			for (int id : items)
			{
				int copyTargetId = assetService.copyAsset(id, parentId, moduleName);
				copyTargetIds.add(copyTargetId);
				success = copyTargetId != 0;
			}
			request.setAttribute(COPY_TARGET_IDS, copyTargetIds); // Put copy ids in request for ModuleTrigger use
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		if (success)
			return new SimpleJsonResponse(success, "Element copy successful.");
		else
			return new SimpleJsonResponse(success, "Element copy operation not successful.");
	}

	@RequestMapping("/copyCategories")
	@ResponseBody
	public SimpleJsonResponse copyCategories(HttpServletRequest request,
			@RequestParam("items[]") int[] items, @RequestParam int parentId, @RequestParam String moduleName)
	{
		boolean success = false;
		try
		{
			CategoryService categoryService = new CategoryService();
			ArrayList<Integer> copyTargetIds = new ArrayList<Integer>();
			for (int id : items)
			{
				int copyTargetId = categoryService.copyCategory(id, parentId, moduleName);
				copyTargetIds.add(copyTargetId);
				success = copyTargetId != 0;
			}
			request.setAttribute(COPY_TARGET_IDS, copyTargetIds); // Put copy ids in request for ModuleTrigger use
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		if (success)
			return new SimpleJsonResponse(success, "Category copy successful.");
		else
			return new SimpleJsonResponse(success, "Category copy operation not successful.");
	}

	@RequestMapping("/moveElements")
	@ResponseBody
	public SimpleJsonResponse moveElements(@RequestParam("items[]") int[] items, @RequestParam int parentId, @RequestParam String moduleName)
	{
		boolean success = false;
		try
		{
			AssetService assetService = new AssetService();
			for (int id : items)
			{
				success = assetService.moveAsset(id, parentId, moduleName);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		if (success)
			return new SimpleJsonResponse(success, "Element move successful.");
		else
			return new SimpleJsonResponse(success, "Element move operation not successful.");
	}

	/**
	 * 
	 * @param items
	 *            Ids of categories to be reassigned a new parent category
	 * @param parentId
	 *            Id of target category to be the new parent category
	 * @param moduleName
	 * @return
	 */
	@RequestMapping("/moveCategories")
	@ResponseBody
	public SimpleJsonResponse moveCategories(@RequestParam("items[]") int[] items, @RequestParam int parentId, @RequestParam String moduleName)
	{
		boolean success = false;
		try
		{
			CategoryService categoryService = new CategoryService();
			for (int id : items)
			{
				success = categoryService.moveCategory(id, parentId, moduleName);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		if (success)
			return new SimpleJsonResponse(success, "Category move successful.");
		else
			return new SimpleJsonResponse(success, "Category move operation not successful.");
	}



	@RequestMapping(value = "/sort-data")
	public String sortData(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{
		return SecureJSPPath.path("categoryOrder");
	}

	public static class SimpleJsonResponse
	{
		private final boolean success;
		private final String message;

		public SimpleJsonResponse(boolean success, String message)
		{
			this.success = success;
			this.message = message;
		}

		public boolean isSuccess()
		{
			return success;
		}

		public String getMessage()
		{
			return message;
		}
	}
}
