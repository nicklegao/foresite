/**
 * @author Nick Yiming Gao
 * @date 2015/03/27
 */
package au.net.webdirector.admin.workflow;

/**
 * 
 */

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.admin.datalayer.client.AdminModuleHelper;
import au.net.webdirector.admin.datalayer.client.Difference;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.ModuleData;

public class WorkflowService
{

	private static Logger logger = Logger.getLogger(WorkflowService.class);


	public List<Difference> compare(String module, String tableType, String draftid) throws SQLException
	{
		WorkflowInfo workflow = WorkflowHelper.getInstance().getStatusByDraftId(module, tableType, draftid);
		if (workflow == null)
		{
			throw new RuntimeException("No workflow data found for [" + module + ":" + tableType + ":" + draftid + "]");
		}
		boolean isElement = StringUtils.equalsIgnoreCase("Elements", tableType);
		WorkflowAccess da = WorkflowAccess.getInstance();
		ModuleData conditions = isElement ? new ElementData() : new CategoryData();
		conditions.setId(workflow.getDraftId());
		ModuleData data1 = isElement ? da.getElement(module, (ElementData) conditions) : da.getCategory(module, (CategoryData) conditions);

		ModuleData data2 = null;
		if (!StringUtils.isBlank(workflow.getDataId()))
		{
			ModuleAccess ma = ModuleAccess.getInstance();
			conditions.setId(workflow.getDataId());
			data2 = isElement ? ma.getElement(module, (ElementData) conditions) : ma.getCategory(module, (CategoryData) conditions);
		}
		return AdminModuleHelper.getInstance().compare(module, tableType, data1, data2);
	}

}
