/**
 * @author Sushant Verma
 * @date 10 Apr 2015
 */
package au.net.webdirector.admin.workflow.domain;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import au.net.webdirector.admin.workflow.WorkflowInfo;

public class TodoResponse
{
	private static Logger logger = Logger.getLogger(TodoResponse.class);

	List<WorkflowInfo> workflowItems;
	Map<String, String> userToName;
	Map<String, String> moduleToName;
	Map<String, String> itemToHeadline;

	/**
	 * @param changes
	 * @param userToName
	 * @param moduleToName
	 * @param itemToHeadline
	 */
	public TodoResponse(List<WorkflowInfo> workflowItems, Map<String, String> userToName, Map<String, String> moduleToName, Map<String, String> itemToHeadline)
	{
		this.workflowItems = workflowItems;
		this.userToName = userToName;
		this.moduleToName = moduleToName;
		this.itemToHeadline = itemToHeadline;
	}

	public List<WorkflowInfo> getWorkflowItems()
	{
		return workflowItems;
	}

	public void setWorkflowItems(List<WorkflowInfo> workflowItems)
	{
		this.workflowItems = workflowItems;
	}

	public Map<String, String> getUserToName()
	{
		return userToName;
	}

	public void setUserToName(Map<String, String> userToName)
	{
		this.userToName = userToName;
	}

	public Map<String, String> getModuleToName()
	{
		return moduleToName;
	}

	public void setModuleToName(Map<String, String> moduleToName)
	{
		this.moduleToName = moduleToName;
	}

	public Map<String, String> getItemToHeadline()
	{
		return itemToHeadline;
	}

	public void setItemToHeadline(Map<String, String> itemToHeadline)
	{
		this.itemToHeadline = itemToHeadline;
	}
}
