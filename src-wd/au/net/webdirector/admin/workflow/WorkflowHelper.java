package au.net.webdirector.admin.workflow;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.admin.modules.ModuleConfiguration;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccess;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleData;
import au.net.webdirector.common.datalayer.client.ModuleHelper;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;

public class WorkflowHelper
{
	private Logger logger = Logger.getLogger(WorkflowHelper.class);

	public static final String ACTION_CREATE = "create";
	public static final String ACTION_UPDATE = "update";
	public static final String ACTION_DELETE = "delete";

	public static WorkflowHelper getInstance()
	{
		return getInstance(TransactionManager.getInstance(TransactionDataAccess.getInstance()));
	}

	public static WorkflowHelper getInstance(TransactionManager txManager)
	{
		return new WorkflowHelper(txManager);
	}

	private TransactionManager txManager;

	private WorkflowHelper(TransactionManager txManager)
	{
		this.txManager = txManager;
	}

	private TransactionDataAccess getDataAccess()
	{
		return txManager.getDataAccess();
	}

	public boolean isIdValid(String id)
	{
		return StringUtils.isNotBlank(id) && !StringUtils.equals(id, "0");
	}

	public boolean isWorkflowEnabled(String module)
	{
		return ModuleConfiguration.getInstance().getModule(module).getHasWorkflow();
	}

	public boolean isDataApprover(String module, String userId) throws SQLException
	{
//		boolean isAdmin = UserManager.isAdmin(userId);
//		return isAdmin || isAssignedModuleApprover(module, userId);
//
		return isAssignedModuleApprover(module, userId);
	}

	public boolean isUserNeedApproval(String module, String userId) throws SQLException
	{
		return !isDataApprover(module, userId);
	}

	public WorkflowInfo getStatus(String module, String tableType, String id) throws SQLException
	{
		if (StringUtils.isBlank(id) || StringUtils.equals(id, "0"))
		{
			return null;
		}
		String sql = "select * from sys_draft_rec where D_MODULE = ? and D_TABLE = ? and D_DATA_ID = ?";
		DataMap result = getDataAccess().selectMap(sql, new String[] { module, tableType, id });
		if (result == null)
		{
			return null;
		}
		WorkflowInfo workflow = new WorkflowInfo(result);
		return workflow;
	}

	public Map<String, WorkflowInfo> getAllStatus(String module, String tableType)
	{
		String sql = "select * from sys_draft_rec where D_MODULE = ? and D_TABLE = ?";
		List<DataMap> results = null;
		try
		{
			results = getDataAccess().selectMaps(sql, new String[] { module, tableType });
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
		}
		if (results == null)
		{
			return null;
		}
		Map<String, WorkflowInfo> matches = new HashMap<String, WorkflowInfo>();
		for (DataMap result : results)
		{
			WorkflowInfo workflow = new WorkflowInfo(result);

			matches.put(workflow.getDataId(), workflow);
		}
		return matches;
	}


	public WorkflowInfo getStatusByDraftId(String module, String tableType, String draftId) throws SQLException
	{
		if (StringUtils.isBlank(draftId) || StringUtils.equals(draftId, "0"))
		{
			return null;
		}
		String sql = "select * from sys_draft_rec where D_MODULE = ? and D_TABLE = ? and D_DRAFT_ID = ?";
		DataMap result = getDataAccess().selectMap(sql, new String[] { module, tableType, draftId });
		if (result == null)
		{
			return null;
		}
		WorkflowInfo workflow = new WorkflowInfo(result);
		return workflow;
	}

	public void saveStatus(String userId, WorkflowInfo workflow, String status) throws SQLException
	{
		getDataAccess().update("update sys_draft_rec set d_when = now(), d_who=?, d_status = ?, D_DRAFT_ID = ?, D_DATA_ID = ? where d_id = ?", new String[] { userId, status, StringUtils.isBlank(workflow.getDraftId()) ? "0" : workflow.getDraftId(), StringUtils.isBlank(workflow.getDataId()) ? "0" : workflow.getDataId(), workflow.getSysId() });
	}

	public void saveStatus(String userId, String module, String tableType, String draftId, String dataId, String status) throws SQLException
	{
		getDataAccess().update("insert into sys_draft_rec (D_WHEN, D_WHO, D_MODULE, D_TABLE, D_DRAFT_ID, D_DATA_ID, D_STATUS) values (now(), ?, ?, ?, ?, ?, ?)", new String[] { userId, module, tableType, StringUtils.isBlank(draftId) ? "0" : draftId, StringUtils.isBlank(dataId) ? "0" : dataId, status });
	}

	public void removeStatus(WorkflowInfo workflow) throws SQLException
	{
		getDataAccess().update("delete from sys_draft_rec where d_id = ?", new String[] { workflow.getSysId() });
	}

	public String saveDataToLive(
			String module,
			ModuleData moduleData,
			String action) throws SQLException, IOException
	{
		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
		return writeData(module, moduleData, action, ma);
	}

	private String writeData(
			String module,
			ModuleData moduleData,
			String action,
			TransactionModuleAccess ma) throws SQLException, IOException
	{
		String dataId = moduleData.getId();
		boolean isElement = "element".equals(moduleData.getType());
		if (isElement)
		{
			if (StringUtils.equalsIgnoreCase(action, "update"))
			{
				ma.updateElement((ElementData) moduleData, module);
			}
			else
			{
				moduleData.removeId();
				ElementData newElement = ma.insertElement((ElementData) moduleData, module);
				dataId = newElement.getId();
			}
		}
		else
		{
			if (StringUtils.equalsIgnoreCase(action, "update"))
			{
				ma.updateCategory((CategoryData) moduleData, module);
			}
			else
			{
				moduleData.removeId();
				CategoryData newCategory = ma.insertCategory((CategoryData) moduleData, module);
				dataId = newCategory.getId();
			}
		}
		return dataId;
	}

	public String saveDataToWorkflow(
			String module,
			ModuleData moduleData,
			String action,
			String status,
			String userId) throws SQLException, IOException
	{
		TransactionModuleAccess ma = TransactionWorkflowAccess.getInstance(txManager);

		boolean isElement = "element".equals(moduleData.getType());
		String tableType = isElement ? "Elements" : "Categories";
		final String dataId = moduleData.getId();
		WorkflowInfo workflow = getStatus(module, tableType, dataId);
		// if there is work flow, you need to update it, but replace data id with draft id first
		// if there is no work flow, and id is blank, means a brand new draft needs to insert
		// if there is no work flow, and id is NOT blank, means it's a draft based on live version, because the form might not have uploaded files, 
		//    you need to insert a draft which is same as live version, then update it.
		if (workflow != null && !StringUtils.equals(workflow.getDraftId(), "0"))
		{
			moduleData.setId(workflow.getDraftId());
		}
		else
		{
			if (StringUtils.isBlank(dataId))
			{
				action = "insert";
			}
			else
			{
				String newId = copyLiveToWorkflow(module, isElement, dataId);
				moduleData.setId(newId);
			}
		}
		String draftId = writeData(module, moduleData, action, ma);
		if (workflow == null)
		{
			saveStatus(userId, module, tableType, draftId, dataId, status);
		}
		else
		{
			workflow.setDraftId(draftId);
			saveStatus(userId, workflow, status);
		}
		return dataId;
	}


	public String copyLiveToWorkflow(String module, boolean isElement, String dataId) throws SQLException, IOException
	{
		logger.info("Load current live data into draft table ... ");
		TransactionDataAccess da = txManager.getDataAccess();
		StoreFileAccess fa = txManager.getFileAccess();
		String tableType = isElement ? "Elements" : "Categories";
		logger.info("copying data ... ");
		String tableName = tableType + "_" + module;
		List<String> mainColumns = ModuleHelper.getInstance(txManager).getDBColumns(tableType, module);
		String idFieldName = isElement ? "Element_id" : "Category_id";
		mainColumns.remove(idFieldName);
		logger.info("=== copy main record");
		int newId = da.insert("insert into " + tableName + "_draft (" + StringUtils.join(mainColumns, ",") + ") select " + StringUtils.join(mainColumns, ",") + " from " + tableName + " where " + idFieldName + " = ?", new String[] { dataId });
		List<String> updateFiles = new ArrayList<String>();
		for (String column : mainColumns)
		{
			if (column.startsWith("attrfile_") || column.startsWith("attrtext_") || column.startsWith("attrlong_"))
			{
				updateFiles.add(column + "=if(" + column + " = '', " + column + ", concat('/_DRAFT',replace(" + column + ",'/" + dataId + "/','/" + newId + "/')))");
			}
		}
		if (updateFiles.size() > 0)
		{
			da.update("update " + tableName + "_draft set " + StringUtils.join(updateFiles, ",") + " where " + idFieldName + " = ?", new String[] { String.valueOf(newId) });
		}
		logger.info("=== copy notes");
		da.update("insert into notes_draft (moduleName, foriegnKey, elementOrCategory, createDate, note, who, followUpDate, fieldName, content) select moduleName, " + newId + ", elementOrCategory, createDate, note, who, followUpDate, fieldName, content from notes where moduleName = ? and elementOrCategory = ? and foriegnKey = ?", new String[] { module, tableType, dataId });
		da.update("update notes_draft set fieldName = concat(SUBSTRING_INDEX( fieldName , '-', 1), '-', id) where moduleName = ? and elementOrCategory = ? and foriegnKey = ?", new String[] { module, tableType, String.valueOf(newId) });
		logger.info("=== copy stores");
		Defaults d = Defaults.getInstance();
		String srcPath = module + (StringUtils.equalsIgnoreCase("Elements", tableType) ? "/" : "/Categories/") + dataId;
		File srcDir = new File(d.getStoreDir(), srcPath);
		String draftPath = "_DRAFT/" + module + (StringUtils.equalsIgnoreCase("Elements", tableType) ? "/" : "/Categories/") + newId;
		if (fa.exists(srcDir))
		{
			fa.copy(srcDir, new File(d.getStoreDir(), draftPath));
		}
		logger.info("=== done");
		return String.valueOf(newId);
	}

	/**
	 * 
	 */
	public List<WorkflowInfo> getWorkflowSummary()
	{
		String sql = "select * from sys_draft_rec order by D_WHEN desc";
		List<DataMap> result = new LinkedList<DataMap>();
		try
		{
			result = getDataAccess().selectMaps(sql, new String[] {});
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
		}

		List<WorkflowInfo> workflowInfos = new LinkedList<WorkflowInfo>();
		for (DataMap dm : result)
		{
			WorkflowInfo workflow = new WorkflowInfo(dm);
			workflowInfos.add(workflow);
		}
		return workflowInfos;
	}

	public String copyWorkflowToLive(String module, boolean isElement, String draftId) throws SQLException, IOException
	{
		WorkflowInfo workflow = getStatusByDraftId(module, isElement ? "Elements" : "Categories", draftId);
		String tableType = workflow.getType();
		String dataId = workflow.getDataId();
		logger.info("Load current live data into draft table ... ");
		TransactionDataAccess da = txManager.getDataAccess();
		StoreFileAccess fa = txManager.getFileAccess();

		logger.info("copying data ... ");
		String tableName = tableType + "_" + module;
		List<String> mainColumns = ModuleHelper.getInstance(txManager).getDBColumns(tableType, module);
		String idFieldName = StringUtils.equalsIgnoreCase(tableType, "Elements") ? "Element_id" : "Category_id";
		mainColumns.remove(idFieldName);
		if (isIdValid(dataId))
		{
			logger.info("=== update main record");
			List<String> updateColumns = new ArrayList<String>();
			for (String column : mainColumns)
			{
				updateColumns.add("m." + column + "=d." + column);
			}
			String updateSql = "update " + tableName + " as m, " + tableName + "_draft as d set " + StringUtils.join(updateColumns, ",") + " where m." + idFieldName + " = ? and d." + idFieldName + " = ?";
			da.update(updateSql, new String[] { dataId, draftId });
		}
		else
		{
			logger.info("=== insert main record");
			String insertSql = "insert into " + tableName + " (" + StringUtils.join(mainColumns, ",") + ") (select " + StringUtils.join(mainColumns, ",") + " from " + tableName + "_draft where " + idFieldName + " = ?)";
			dataId = String.valueOf(da.insert(insertSql, new String[] { draftId }));
			workflow.setDataId(dataId);
		}
		List<String> updateFiles = new ArrayList<String>();
		for (String column : mainColumns)
		{
			if (column.startsWith("attrfile_") || column.startsWith("attrtext_") || column.startsWith("attrlong_"))
			{
				updateFiles.add(column + "=if(" + column + " = '', " + column + ", substr(replace(" + column + ",'/" + draftId + "/','/" + dataId + "/'), 8))");
			}
		}
		if (updateFiles.size() > 0)
		{
			da.update("update " + tableName + " set " + StringUtils.join(updateFiles, ",") + " where " + idFieldName + " = ?", new String[] { String.valueOf(dataId) });
		}
		logger.info("=== copy notes");
		da.update("delete from notes where moduleName = ? and elementOrCategory = ? and foriegnKey = ?", new String[] { module, tableType, dataId });
		da.update("insert into notes (moduleName, foriegnKey, elementOrCategory, createDate, note, who, followUpDate, fieldName, content) select moduleName, " + dataId + ", elementOrCategory, createDate, note, who, followUpDate, fieldName, content from notes_draft where moduleName = ? and elementOrCategory = ? and foriegnKey = ?", new String[] { module, tableType, draftId });
		da.update("update notes set fieldName = concat(SUBSTRING_INDEX( fieldName , '-', 1), '-', id) where moduleName = ? and elementOrCategory = ? and foriegnKey = ?", new String[] { module, tableType, String.valueOf(dataId) });
		logger.info("=== copy stores");
		Defaults d = Defaults.getInstance();
		String srcPath = "_DRAFT/" + module + (StringUtils.equalsIgnoreCase("Elements", tableType) ? "/" : "/Categories/") + draftId;
		File srcDir = new File(d.getStoreDir(), srcPath);
		String dataPath = module + (StringUtils.equalsIgnoreCase("Elements", tableType) ? "/" : "/Categories/") + dataId;
		if (fa.exists(srcDir))
		{
			fa.copy(srcDir, new File(d.getStoreDir(), dataPath));
		}
		logger.info("=== done");
		return dataId;
	}

	public String saveDraftToWorkflow(
			String module,
			ModuleData moduleData,
			String action,
			String status,
			String userId) throws SQLException, IOException
	{
		TransactionModuleAccess ma = TransactionWorkflowAccess.getInstance(txManager);

		boolean isElement = "element".equals(moduleData.getType());
		String tableType = isElement ? "Elements" : "Categories";
		WorkflowInfo workflow = getStatusByDraftId(module, tableType, moduleData.getId());

		writeData(module, moduleData, action, ma);
		saveStatus(userId, workflow, status);
		return moduleData.getId();
	}

	public String updateDataToWorkflow(String module, ModuleData moduleData, String action, String status, String userId) throws SQLException, IOException
	{
		return updateDataToWorkflow(module, moduleData, action, status, userId, "0");
	}

	public String updateDataToWorkflow(String module, ModuleData moduleData, String action, String status, String userId, String currentDataId) throws SQLException, IOException
	{
		TransactionModuleAccess ma = TransactionWorkflowAccess.getInstance(txManager);
		String tableType = ElementData.TYPE.equals(moduleData.getType()) ? "Elements" : "Categories";
		String draftId = writeData(module, moduleData, action, ma);
		WorkflowInfo workflow = getStatusByDraftId(module, tableType, moduleData.getId());
		String dataId = null;
		if (workflow == null)
		{
			dataId = currentDataId;
			saveStatus(userId, module, tableType, draftId, dataId, status);
		}
		else
		{
			dataId = workflow.getDataId();
			saveStatus(userId, workflow, status);
		}
		return dataId;
	}

	/**
	 * @param module
	 * @param tableType
	 * @param dataId
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */
	public void deleteDraftByDataId(String module, String tableType, String dataId) throws SQLException, IOException
	{
		WorkflowInfo workflow = getStatus(module, tableType, dataId);
		if (workflow == null)
		{
			return;
		}
		TransactionModuleAccess twa = TransactionWorkflowAccess.getInstance(txManager);
		if (StringUtils.equals("Elements", tableType))
		{
			twa.deleteElementById(module, workflow.getDraftId());
		}
		else
		{
			twa.deleteCategoryById(module, workflow.getDraftId());
		}
		removeStatus(workflow);
	}

	/**
	 * @param module
	 * @param tableType
	 * @param dataId
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public void deleteDraftByDraftId(String module, String tableType, String draftId) throws SQLException, IOException
	{
		WorkflowInfo workflow = getStatusByDraftId(module, tableType, draftId);
		if (workflow == null)
		{
			return;
		}
		TransactionModuleAccess twa = TransactionWorkflowAccess.getInstance(txManager);
		if (StringUtils.equals("Elements", tableType))
		{
			twa.deleteElementById(module, workflow.getDraftId());
		}
		else
		{
			twa.deleteCategoryById(module, workflow.getDraftId());
		}
		removeStatus(workflow);
	}

	/**
	 * @param module
	 * @param tableType
	 * @param dataId
	 */
	public void markAsDeleted(String module, String tableType, String dataId, String userId) throws SQLException, IOException
	{
		String draftId = copyLiveToWorkflow(module, StringUtils.equals("Elements", tableType), dataId);
		saveStatus(userId, module, tableType, draftId, dataId, WorkflowInfo.TO_BE_DELETED);
	}

	/**
	 * @param module
	 * @param tableType
	 * @param dataId
	 * @throws IOException
	 * @throws SQLException
	 */
	public void deleteLiveByDataId(String module, String tableType, String dataId) throws SQLException, IOException
	{
		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
		if (StringUtils.equals("Elements", tableType))
		{
			ma.deleteElementById(module, dataId);
		}
		else
		{
			ma.deleteCategoryById(module, dataId);
		}
	}

	/**
	 * @param module
	 * @param d
	 * @param action
	 * @return
	 */
	public String updateDataToLive(String module, ModuleData moduleData, String action) throws SQLException, IOException
	{
		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
		return writeData(module, moduleData, action, ma);
	}

	/**
	 * @param moduleName
	 * @param setUsers
	 * @throws SQLException
	 */
	public void setModuleApprovers(String moduleName, Set<String> setUsers) throws SQLException
	{
		TransactionDataAccess tda = txManager.getDataAccess();
		tda.update("DELETE FROM sys_module_approvers WHERE internal_module_name = ?", moduleName);

		for (String userID : setUsers)
		{
			tda.update("INSERT INTO sys_module_approvers (`internal_module_name`, `user_id`) VALUES (?, ?)", moduleName, userID);
		}
	}

	/**
	 * @param moduleName
	 * @param setUsers
	 * @return
	 */
	public Set<String> getModuleApprovers(String moduleName)
	{
		try
		{
			List<String> matches = getDataAccess().selectStrings("SELECT user_id FROM sys_module_approvers WHERE internal_module_name = ?", moduleName);
			return new HashSet<String>(matches);
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return new HashSet<String>();
		}
	}

	/**
	 * @param moduleName
	 * @param setUsers
	 * @return
	 */
	public boolean isAssignedModuleApprover(String moduleName, String userID)
	{
		try
		{
			String count = getDataAccess().selectString("SELECT count(*) FROM sys_module_approvers WHERE internal_module_name = ? AND user_id = ?", moduleName, userID);
			return Integer.parseInt(count) > 0;
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return false;
		}
	}
}
