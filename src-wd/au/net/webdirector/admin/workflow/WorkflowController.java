/**
 * @author Nick Yiming Gao
 * @date 2015/03/27
 */
package au.net.webdirector.admin.workflow;

/**
 * 
 */

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.net.webdirector.admin.datalayer.client.Difference;

@Controller
@RequestMapping("/workflowControl")
public class WorkflowController
{

	private static Logger logger = Logger.getLogger(WorkflowController.class);

	@RequestMapping("/compare")
	@ResponseBody
	public List<Difference> compare(HttpServletRequest request,
			@RequestParam String module,
			@RequestParam String tableType,
			@RequestParam String draftId)
	{

		List<Difference> diff = null;
		try
		{
			WorkflowService svc = new WorkflowService();
			diff = svc.compare(module, tableType, draftId);
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			// TODO Auto-generated catch block
		}
		return diff;
	}
}
