package au.net.webdirector.admin.workflow;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.client.ModuleAccess;

public class WorkflowAccess extends ModuleAccess
{

	protected static Logger logger = Logger.getLogger(ModuleAccess.class);

	public static WorkflowAccess getInstance()
	{
		return new WorkflowAccess();
	}

	@Override
	protected String getNotesTable()
	{
		return "notes_draft";
	}

	@Override
	protected String getMultiOptionsTable()
	{
		return "drop_down_multi_selections_draft";
	}

	@Override
	protected String getElementsTable(String module)
	{
		return "Elements_" + module + "_draft";
	}

	@Override
	protected String getCategoriesTable(String module)
	{
		return "Categories_" + module + "_draft";
	}

	@Override
	protected String getForiegnKeyOfNote()
	{
		return "draft_id";
	}

	@Override
	protected String getPrimaryKeyOfModuleData()
	{
		return "draft_id";
	}

	@Override
	protected String getForiegnKeyOfMultiDrop()
	{
		return "draft_id";
	}
}
