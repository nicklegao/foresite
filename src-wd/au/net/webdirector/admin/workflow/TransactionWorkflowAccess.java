package au.net.webdirector.admin.workflow;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
import au.net.webdirector.common.datalayer.base.db.entity.TextFile;
import au.net.webdirector.common.datalayer.base.db.entity.workflow.DraftStoreFile;
import au.net.webdirector.common.datalayer.base.db.entity.workflow.DraftTextFile;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.EntityType;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;

public class TransactionWorkflowAccess extends TransactionModuleAccess
{

	protected static Logger logger = Logger.getLogger(TransactionModuleAccess.class);

	public static TransactionWorkflowAccess getInstance(TransactionManager txManager)
	{
		return new TransactionWorkflowAccess(txManager);
	}

	protected TransactionWorkflowAccess(TransactionManager txManager)
	{
		super(txManager);
	}

	protected String getNotesTable()
	{
		return "notes_draft";
	}

	protected String getMultiOptionsTable()
	{
		return "drop_down_multi_selections_draft";
	}

	@Override
	protected String getElementsTable(String module)
	{
		return "Elements_" + module + "_draft";
	}

	@Override
	protected String getCategoriesTable(String module)
	{
		return "Categories_" + module + "_draft";
	}

	@Override
	protected String getDataTable(String module, EntityType type)
	{
		return (EntityType.ELEMENT.equals(type) ? "elements_" : "categories_") + module + "_draft";
	}

	@Override
	protected StoreFile createStoreFileFromPath(String path)
	{
		return new DraftStoreFile(path);
	}

	@Override
	protected TextFile createTextFileFromPath(String path)
	{
		return new DraftTextFile(path);
	}


	@Override
	protected String getForiegnKeyOfNote()
	{
		return "draft_id";
	}

	@Override
	protected String getPrimaryKeyOfModuleData()
	{
		return "draft_id";
	}

	@Override
	protected String getForiegnKeyOfMultiDrop()
	{
		return "draft_id";
	}

	@Override
	protected int deleteCategory(CategoryData category, String module) throws SQLException, IOException
	{

		deleteStoreFiles(category, module);
		deleteNotes(category, module);
		deleteMultiOptions(category, module);

		// for a draft category, no sub categories and elements need to be deleted
		// because, 1. no draft is allowed inside a new draft, 2. if the draft to be deleted has live data, all children have live id as parent id.

		// delete myself
		CategoryData conditions = new CategoryData();
		conditions.setId(category.getId());
		int result = getDataAccess().delete(getCategoriesTable(module), conditions);
		deleteLuceneIndex(EntityType.CATEGORY, module, category.getId());
		return result;
	}
}
