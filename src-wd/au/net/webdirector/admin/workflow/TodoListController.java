/**
 * @author Sushant Verma
 * @date 10 Apr 2015
 */
package au.net.webdirector.admin.workflow;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import webdirector.db.UserUtils;
import au.net.webdirector.admin.modules.ModuleConfiguration;
import au.net.webdirector.admin.modules.domain.Module;
import au.net.webdirector.admin.workflow.domain.TodoResponse;
import au.net.webdirector.common.datalayer.client.ModuleData;

@Controller
public class TodoListController
{
	private static Logger logger = Logger.getLogger(TodoListController.class);

	@RequestMapping("/todoList")
	@ResponseBody
	public TodoResponse compare(HttpServletRequest request)
	{
		UserUtils userUtils = new UserUtils();
		ModuleConfiguration moduleConfiguration = ModuleConfiguration.getInstance();

		List<WorkflowInfo> workflowItems = WorkflowHelper.getInstance().getWorkflowSummary();
		Map<String, String> userToName = new HashMap<String, String>();
		Map<String, String> moduleToName = new HashMap<String, String>();
		Map<String, String> itemToHeadline = new HashMap<String, String>();

		WorkflowAccess workflowAccess = WorkflowAccess.getInstance();

		for (WorkflowInfo change : workflowItems)
		{
			//load user
			if (userToName.containsKey(change.getWho()) == false)
			{
				String name = userUtils.getUsernameByID(change.getWho());
				userToName.put(change.getWho(), name);
			}

			//load module name
			if (moduleToName.containsKey(change.getModule()) == false)
			{
				Module module = moduleConfiguration.getModule(change.getModule());
				String name = module == null ? "" : module.getExternalModuleName();
				moduleToName.put(change.getModule(), name);
			}

			String headline = null;
			try
			{
				ModuleData ed;
				if (change.getType().equalsIgnoreCase("Elements"))
				{
					ed = workflowAccess.getElementById(change.getModule(), change.getDraftId());
				}
				else
				{
					ed = workflowAccess.getCategoryById(change.getModule(), change.getDraftId());
				}
				if (ed != null)
					headline = ed.getName();
			}
			catch (SQLException e)
			{
				logger.error("Error:", e);
			}
			if (headline != null)
			{
				itemToHeadline.put(change.getSysId(), headline);
			}
		}
		return new TodoResponse(workflowItems, userToName, moduleToName, itemToHeadline);
	}
}
