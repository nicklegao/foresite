package au.net.webdirector.admin.workflow;

import java.util.Date;

import au.net.webdirector.common.datalayer.base.db.DataMap;

public class WorkflowInfo
{

	public static String DRAFT = "DRAFT";
	public static String TO_BE_APPROVED = "TO_BE_APPROVED";
	public static String DECLINED = "DECLINED";
	public static String APPROVED = "APPROVED";
	public static String TO_BE_DELETED = "TO_BE_DELETED";

	private String sysId;
	private String status;
	private Date when;
	private String who;
	private String module;
	private String type;
	private String dataId;
	private String draftId;

	WorkflowInfo()
	{

	}

	/**
	 * @param dm
	 */
	public WorkflowInfo(DataMap dm)
	{
		setSysId(dm.getString("D_ID"));
		setDraftId(dm.getString("D_DRAFT_ID"));
		setDataId(dm.getString("D_DATA_ID"));
		setModule(dm.getString("D_MODULE"));
		setStatus(dm.getString("D_STATUS"));
		setType(dm.getString("D_TABLE"));
		setWhen(dm.getDate("D_WHEN"));
		setWho(dm.getString("D_WHO"));
	}

	void setStatus(String status)
	{
		this.status = status;
	}

	void setWhen(Date when)
	{
		this.when = when;
	}

	void setWho(String who)
	{
		this.who = who;
	}

	void setModule(String module)
	{
		this.module = module;
	}

	void setType(String type)
	{
		this.type = type;
	}

	public void setDataId(String dataId)
	{
		this.dataId = dataId;
	}

	public void setDraftId(String draftId)
	{
		this.draftId = draftId;
	}

	void setSysId(String sysId)
	{
		this.sysId = sysId;
	}

	public String getStatus()
	{
		return status;
	}

	public Date getWhen()
	{
		return when;
	}

	public String getWho()
	{
		return who;
	}

	public String getModule()
	{
		return module;
	}

	public String getType()
	{
		return type;
	}

	public String getDataId()
	{
		return dataId;
	}

	public String getDraftId()
	{
		return draftId;
	}

	public String getSysId()
	{
		return sysId;
	}

	public boolean isDraft()
	{
		return DRAFT.equals(status);
	}

	public boolean isToBeApproved()
	{
		return TO_BE_APPROVED.equals(status);
	}

	public boolean isDeclined()
	{
		return DECLINED.equals(status);
	}

	public boolean isToBeDeleted()
	{
		return TO_BE_DELETED.equals(status);
	}
}
