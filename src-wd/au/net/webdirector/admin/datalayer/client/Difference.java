package au.net.webdirector.admin.datalayer.client;

public class Difference
{

	String fieldName;
	String name;
	String first;
	String second;

	public Difference(String fieldName, String first, String second)
	{
		super();
		this.fieldName = fieldName;
		this.first = first;
		this.second = second;
	}

	public String getFieldName()
	{
		return fieldName;
	}

	public String getFirst()
	{
		return first;
	}

	public String getSecond()
	{
		return second;
	}

	public String getName()
	{
		return name;
	}

}
