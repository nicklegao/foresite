/**
 * @author Nick Yiming Gao
 * @date 2016-5-31
 */
package au.net.webdirector.admin.datalayer.client;

/**
 * 
 */

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.ci.system.FileDiffer;
import au.net.webdirector.admin.modules.privileges.CachedCategoryPrivileges;
import au.net.webdirector.admin.modules.privileges.CategoryPrivileges;
import au.net.webdirector.admin.modules.privileges.PrivilegeUtils;
import au.net.webdirector.admin.version.VersionAccess;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.ModuleData;
import au.net.webdirector.common.datalayer.client.ModuleHelper;
import au.net.webdirector.common.datalayer.client.module.ModuleAttributes;
import au.net.webdirector.common.datalayer.lucene.LuceneHelper;

public class AdminModuleHelper extends ModuleHelper
{

	public static AdminModuleHelper getInstance()
	{
		return getInstance(TransactionManager.getInstance(TransactionDataAccess.getInstance()));
	}

	public static AdminModuleHelper getInstance(TransactionManager txManager)
	{
		return new AdminModuleHelper(txManager);
	}

	protected AdminModuleHelper(TransactionManager txManager)
	{
		super(txManager);
	}

	private static Logger logger = Logger.getLogger(AdminModuleHelper.class);

	public int countCategories(String module, String categoryParentID, String filterQuery, PrivilegeUtils privilageUtils, int permissions)
	{
		try
		{
			Set<String> filterIDs = new HashSet<String>();

			boolean hasUnrestrictedPermissions = privilageUtils.isUnrestricted();
			boolean mustUseInClause = false;

			//Search lucene for matching category indexes
			if (StringUtils.isNotBlank(filterQuery))
			{
				logger.info("Adding Lucene filter for category query: " + filterQuery);

				List<String> ids = new LuceneHelper(module).searchCategories(filterQuery);
				filterIDs.addAll(ids);

				mustUseInClause = true;
				logger.info("Matched " + ids.size() + " categories.");
			}

			//filter by permissions
			if (!hasUnrestrictedPermissions)
			{
				logger.info("Adding users based on permissions for categoryID: " + categoryParentID);

				Set<String> categoryIDs = privilageUtils.categoriesWithPrivilages(permissions);
				filterIDs.addAll(categoryIDs);

				mustUseInClause = true;
				logger.info("Matched " + categoryIDs.size() + " categories.");
			}

			if (mustUseInClause && filterIDs.size() == 0)
			{
				logger.info("No categories to filter");
				return 0;
			}

			String sql = "select count(*) from categories_" + module
					+ " where Category_ParentID=" + categoryParentID;
			if (mustUseInClause)
			{
				sql = sql + " and Category_id in (" + StringUtils.join(filterIDs, ",") + ")";
			}

			String count = TransactionDataAccess.getInstance().selectString(sql);
			return Integer.parseInt(count);
		}
		catch (Exception e)
		{
			logger.fatal(e);
		}

		return 0;
	}

	public Set<String> filterElements(PrivilegeUtils privilegeUtils, String module, String categoryParentId, String searchQuery)
	{
		CategoryPrivileges privilage;
		if (privilegeUtils.isUnrestricted())
		{
			privilage = new CachedCategoryPrivileges(CategoryPrivileges.ALL_CRUD);
		}
		else
		{
			privilage = privilegeUtils.getPrivilegeForCategory(categoryParentId);
		}

		if (privilage.isView())
		{
			if (StringUtils.isEmpty(searchQuery))
			{
				return null;
			}
			else
			{
				//Perform Lucene Search
				List<String> searchFilter = searchElements(module, searchQuery);
				return new HashSet<String>(searchFilter);
			}
		}
		else
		{
			return new HashSet<String>();
		}
	}

	/**
	 * @param module
	 * @param dataId
	 * @param value
	 * @return not null -> set of matched categoryIDs (can be empty)
	 * @return null -> No filters applied
	 */
	public Set<String> filterCategories(PrivilegeUtils privilegeUtils, String module, String categoryParentId, String searchQuery)
	{
		//searchPrivlages
		Set<String> privlegeFilter = null;
		if (!privilegeUtils.isUnrestricted())
		{
			privlegeFilter = privilegeUtils.categoriesWithPrivilages(CategoryPrivileges.READ);
		}

		//Search lucene
		List<String> searchFilter = null;
		if (!StringUtils.isEmpty(searchQuery))
		{
			searchFilter = searchCategories(module, searchQuery);
		}

		//combine the two search results from above...
		Set<String> matched = null;

		if (privlegeFilter != null)
		{
			if (matched == null)
				matched = new HashSet<String>(privlegeFilter);
			else
				matched.retainAll(privlegeFilter);//Will never get here
		}
		if (searchFilter != null)
		{
			if (matched == null)
				matched = new HashSet<String>(searchFilter);
			else
				matched.retainAll(searchFilter);
		}

		return matched;
	}

	public List<Difference> compare(String module, String tableType, ModuleData data1, ModuleData data2) throws SQLException
	{
		boolean isElement = StringUtils.equalsIgnoreCase("Elements", tableType);
		VersionAccess va = VersionAccess.getInstance();
		List<Difference> ret = new ArrayList<Difference>();
		List<String> mutualKeys = new ArrayList<String>();
		if (data1 != null)
		{
			for (String key : data1.keySet())
			{
				if (data2 == null || !data2.containsKey(key))
				{
					if (key.startsWith("attrmulti_"))
					{
						ret.add(new Difference(key, va.getMultiOptions(data1, key, module).asStr(), ""));
						continue;
					}
					if (key.startsWith("attrnotes_"))
					{
						ret.add(new Difference(key, va.getNotes(data1, key, module).asStr(), ""));
						continue;
					}
					ret.add(new Difference(key, data1.getString(key), ""));
					continue;
				}
				try
				{
					if (key.startsWith("attrmulti_"))
					{
						String value1 = va.getMultiOptions(data1, key, module).asStr();
						String value2 = va.getMultiOptions(data2, key, module).asStr();
						if (!StringUtils.equals(value1, value2))
						{
							ret.add(new Difference(key, value1, value2));
							continue;
						}
						continue;
					}
					if (key.startsWith("attrnotes_"))
					{
						String value1 = va.getNotes(data1, key, module).asStr();
						String value2 = va.getNotes(data2, key, module).asStr();
						if (!StringUtils.equals(value1, value2))
						{
							ret.add(new Difference(key, value1, value2));
							continue;
						}
						continue;
					}
					if (key.startsWith("attrfile_") || key.startsWith("attrtext_") || key.startsWith("attrlong_"))
					{
						StoreFile file1 = data1.getStoreFile(key);
						StoreFile file2 = data2.getStoreFile(key);
						if (file1 == null && file2 == null)
						{
							continue;
						}
						if (file1 == null || file2 == null)
						{
							ret.add(new Difference(key, data1.getString(key), data2.getString(key)));
							continue;
						}
						if (!StringUtils.equals(file1.getPath(), file2.getPath()))
						{
							ret.add(new Difference(key, file1.getPath(), file2.getPath()));
							continue;
						}
						try
						{
							if (!FileDiffer.binaryEquals(file1.getStoredFile(), file2.getStoredFile()))
							{
								ret.add(new Difference(key, file1.getPath(), file2.getPath()));
								continue;
							}
						}
						catch (Exception e)
						{
							logger.error(e);
						}
					}
					String value1 = data1.getString(key);
					String value2 = data2.getString(key);
					if (!StringUtils.equals(value1, value2))
					{
						ret.add(new Difference(key, value1, value2));
						continue;
					}
					continue;
				}
				finally
				{
					mutualKeys.add(key);
				}
			}
		}
		if (data2 != null)
		{
			for (String key : data2.keySet())
			{
				if (mutualKeys.contains(key))
				{
					continue;
				}
				if (key.startsWith("attrmulti_"))
				{
					ret.add(new Difference(key, "", va.getMultiOptions(data2, key, module).asStr()));
					continue;
				}
				if (key.startsWith("attrnotes_"))
				{
					ret.add(new Difference(key, "", va.getNotes(data2, key, module).asStr()));
					continue;
				}
				ret.add(new Difference(key, "", data2.getString(key)));
			}
		}
		List<ModuleAttributes> columnsInfo = isElement ? ModuleHelper.getInstance().getElementColumnsInfo(module, false) : ModuleHelper.getInstance().getAllCategoryColumnsInfo(module);
		for (int i = 0; i < ret.size(); i++)
		{
			Difference diff = ret.get(i);
			for (ModuleAttributes attr : columnsInfo)
			{
				if (StringUtils.equalsIgnoreCase(diff.getFieldName(), attr.getInternal()))
				{
					diff.name = attr.getExternal();
					break;
				}
			}
			if (StringUtils.isBlank(diff.getName()))
			{
				ret.remove(i);
				i--;
			}
		}
		return ret;
	}
}
