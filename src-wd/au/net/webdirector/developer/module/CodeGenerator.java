/**
 * @author Sushant Verma
 * @date 16 Feb 2016
 */
package au.net.webdirector.developer.module;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.mapper.DataMapMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;

public class CodeGenerator
{
	private static Logger logger = Logger.getLogger(CodeGenerator.class);

	public StringBuffer generate(int minID)
	{
		List<String> internalNames = DataAccess.getInstance().select(
				"SELECT internal_module_name FROM sys_moduleconfig "
						+ " where delete_allowed = 0 "
						+ " and id > ? "
						+ " order by internal_module_name",
				new Object[] {
						Integer.toString(minID)
				}, StringMapper.getInstance());

		StringBuffer sb = new StringBuffer();


		for (String moduleName : internalNames)
		{
			sb.append("/////// START " + moduleName);
			sb.append(generateModule(moduleName));
		}


		return sb;
	}

	public StringBuffer generateModule(String moduleName)
	{
		StringBuffer sb = new StringBuffer();

		sb.append("\n\t/**\n");
		sb.append("\t * <b>The internal module name</b>\n");
		sb.append("\t */\n");
		sb.append("\tpublic static final String MODULE = \"" + moduleName + "\";\n\n");

		List<DataMap> elements = DataAccess.getInstance().select(
				"SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'ELEMENTS_" + moduleName + "' order by column_name",
				new Object[] {
				}, DataMapMapper.getInstance());

		List<DataMap> categories = DataAccess.getInstance().select(
				"SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'CATEGORIES_" + moduleName + "' order by column_name",
				new Object[] {
				}, DataMapMapper.getInstance());

		summariseModuleType(sb, false, categories, moduleName);
		sb.append("\n");
		summariseModuleType(sb, true, elements, moduleName);

		sb.append("\n");
		sb.append("\n");

		return sb;
	}

	/**
	 * @param sb
	 * @param moduleName
	 * @param string
	 * @param categories
	 */
	private void summariseModuleType(StringBuffer sb, boolean isElement, List<DataMap> types, String moduleName)
	{
		for (DataMap dm : types)
		{
			String columnName = dm.getString("column_name");
			if (excludeColumn(columnName))
			{
				continue;
			}

			String symbolicName = columnName.substring(columnName.indexOf('_'), columnName.length());

			String constName = symbolicName
					.replaceAll("[^a-zA-Z0-9]", "")
					.replaceAll("([a-z ])([A-Z]+)", "$1 $2")
					.replaceAll("([a-z ])([0-9]+)", "$1 $2")
					.replaceAll("([A-Z ])([0-9]+)", "$1 $2")
					.replaceAll("([0-9 ])([a-zA-Z]+)", "$1 $2")
					.replaceAll("\\s+", "_").toUpperCase();

			List<DataMap> l = DataAccess.getInstance().select(
					"SELECT * FROM " + (isElement ? "labels_" : "categorylabels_") + moduleName + " where Label_InternalName=?",
					new String[] { columnName }, DataMapMapper.getInstance());
			String catLevel = "";
			DataMap labels = l.get(0);
			if (labels.containsKey("Attribute_Level"))
			{
				int level = labels.getInt("Attribute_Level");
				if (level > 0)
				{
					catLevel = level + "";
				}
			}

			sb.append("\n\t/**\n");
			sb.append("\t * <b>").append(columnName).append("</b><br/>\n");
			sb.append("\t * Display: ").append(labels.getString("Label_ExternalName")).append("\n");
			if (StringUtils.isNotBlank(labels.getString("Label_TabName")))
			{
				sb.append("\t * Tab: ").append(labels.getString("Label_TabName")).append("\n");
			}
			if (StringUtils.isNotBlank(labels.getString("Label_Info")))
			{
				sb.append("\t * Help: ").append(labels.getString("Label_Info")).append("\n");
			}
			if (labels.containsKey("Attribute_Level") && labels.getInt("Attribute_Level") > 0)
			{
				sb.append("\t * <b>").append("CATEGORY LEVEL " + labels.getInt("Attribute_Level") + " ONLY").append("</b><br/>\n");
			}
			sb.append("\t */\n");
			sb.append("\tpublic static final String " + (isElement ? "E" : "C") + catLevel + "_" + constName + " = \"" + columnName + "\";\n");
		}

	}

	private boolean excludeColumn(String columnName)
	{
		return columnName.startsWith("ATTR") == false;
	}

	public static void main(String[] args)
	{
		String columnName = "ATTR_yearEstablished";
		String symbolicName = columnName.substring(columnName.indexOf('_'), columnName.length());

		System.out.println(symbolicName);
		System.out.println(symbolicName
				.replaceAll("[^a-zA-Z0-9]", "")
				.replaceAll("([a-z ])([A-Z]+)", "$1 $2")
				.replaceAll("([a-z ])([0-9]+)", "$1 $2")
				.replaceAll("([A-Z ])([0-9]+)", "$1 $2")
				.replaceAll("([0-9 ])([a-zA-Z]+)", "$1 $2")
				.replaceAll("\\s+", "_").toUpperCase());

	}
}
