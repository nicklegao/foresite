DELIMITER $$

DROP FUNCTION IF EXISTS `friendly`$$
CREATE FUNCTION `friendly`(name TEXT) RETURNS text CHARSET latin1
BEGIN
  DECLARE v_temp TEXT;   
  DECLARE v_idx INT;   
  DECLARE v_char CHAR;   
  DECLARE v_last CHAR;   
  DECLARE v_ret TEXT;   
  IF name is null THEN     
    RETURN name;   
  END IF;   
  SET v_temp = LCASE(name);    
  SET v_temp = REPLACE(v_temp, '&amp;', '-');   
  SET v_temp = REPLACE(v_temp, '&apos;', '-');   
  SET v_temp = REPLACE(v_temp, '&gt;', '-');   
  SET v_temp = REPLACE(v_temp, '&lt;', '-');   
  SET v_temp = REPLACE(v_temp, '&quot;', '-');    
  SET v_last = '-';   
  SET v_idx = 0;   
  SET v_ret = '';   
  cloop: 
  LOOP     
    SET v_idx = v_idx + 1;      
    IF v_idx = length(v_temp)+1 THEN       
      LEAVE cloop;     
    END IF;      
    SET v_char = MID(v_temp, v_idx, 1);     
    IF FIELD(binary v_char, 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','-','1','2','3','4','5','6','7','8','9','0') = 0 THEN
      SET v_char = '-';     
    END IF;      
    IF (v_last != '-' OR v_char != '-') AND (v_char != '-' OR v_idx != length(v_temp)) THEN       
      SET v_ret = CONCAT(v_ret, v_char);       
      SET v_last = v_char;     
    END IF;      
  END LOOP;   
  IF substr(v_ret, length(v_ret)) = '-' THEN 	
    SET v_ret = substr(v_ret,1, length(v_ret) - 1);   
  END IF;   
  RETURN v_ret; 
END;

 $$

DELIMITER ;