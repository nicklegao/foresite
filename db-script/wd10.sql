ALTER TABLE `users` 
CHANGE COLUMN `user_name` `user_name` VARCHAR(255) NOT NULL ,
CHANGE COLUMN `Email` `Email` VARCHAR(255) NULL ;


ALTER TABLE `sys_moduleconfig` 
 ADD COLUMN `color` `color` VARCHAR(255) NULL DEFAULT 'bg-color-darken txt-color-white' AFTER `cate_valid_script`,
 ADD COLUMN `icon` `icon` VARCHAR(255) NULL DEFAULT 'fa fa-certificate' AFTER `color`,
 ADD COLUMN `version_control_enabled` int(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `icon`,
 ADD COLUMN `workflow_enabled` int(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `version_control_enabled`,
 ADD COLUMN `has_sort` INT(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `workflow_enabled`,
 ADD COLUMN `has_copy` INT(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `has_sort`,
 ADD COLUMN `has_cut` INT(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `has_copy`,
 ADD COLUMN `has_import` INT(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `has_cut`,
 ADD COLUMN `has_export` INT(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `has_import`,
 ADD COLUMN `has_full_ckeditor` INT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `has_export`,
 ADD COLUMN `has_form_components` INT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `has_full_ckeditor`,
 ADD COLUMN `has_browse_server` INT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `has_form_components`,
 ADD COLUMN `customised_buttons` varchar(500) NULL DEFAULT '' AFTER `has_browse_server`
 ;
 

ALTER TABLE `users` 
 ADD COLUMN `incorrectLoginAttempts` INT(10) NULL DEFAULT 0 AFTER `pwdLastChange`;
ALTER TABLE `users` 
 ADD COLUMN `lastIncorrectLogin` TIMESTAMP NULL DEFAULT NULL AFTER `incorrectLoginAttempts`;
ALTER TABLE `users` 
 ADD COLUMN `default_layout` CHAR(50) NULL DEFAULT NULL AFTER `lastIncorrectLogin`;
ALTER TABLE `users` 
 ADD COLUMN `resetPasswordToken` VARCHAR(255) NULL DEFAULT NULL AFTER `default_layout`;
 
ALTER TABLE `users` ADD COLUMN `access_file_manager` VARCHAR(20) DEFAULT 'no';
ALTER TABLE `users` ADD COLUMN `can_change_password` VARCHAR(20) DEFAULT 'no';
ALTER TABLE `users` ADD COLUMN `can_edit_account` VARCHAR(20) DEFAULT 'no';


DROP TABLE IF EXISTS `sys_version_rec`;
CREATE TABLE  `sys_version_rec` (
  `Version_ID` int(11) NOT NULL AUTO_INCREMENT,
  `V_WHEN` datetime,
  `V_WHO` varchar(45) NOT NULL,
  `V_ACTION` varchar(45) NOT NULL,
  `V_MODULE` varchar(45) NOT NULL,
  `V_TABLE` varchar(45) NOT NULL,
  `V_DATA_ID` int(11) NOT NULL,
  `V_VERSION_NO` int(11),
  PRIMARY KEY (`Version_ID`)
) ;

CREATE TABLE `sys_module_approvers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `internal_module_name` VARCHAR(45) NOT NULL,
  `user_id` INT(10) NOT NULL,
  PRIMARY KEY (`id`)
);

 
DELIMITER $$

CREATE TRIGGER ins_sys_version_rec BEFORE INSERT ON sys_version_rec
FOR EACH ROW BEGIN
    SET NEW.V_WHEN = NOW();
    SET NEW.V_VERSION_NO = (SELECT ifnull(MAX(V_VERSION_NO),0) + 1 FROM sys_version_rec WHERE V_MODULE = NEW.V_MODULE and V_TABLE = NEW.V_TABLE and V_DATA_ID = NEW.V_DATA_ID);
END $$

DELIMITER ;

DROP TABLE IF EXISTS notes_ver;
create table `notes_ver` like `notes`; 
ALTER TABLE `notes_ver` MODIFY COLUMN `id` INT(10) NOT NULL,
 ADD COLUMN `Version_ID` INTEGER(11),
 DROP PRIMARY KEY; 
 
 
DROP TABLE IF EXISTS drop_down_multi_selections_ver;
create table `drop_down_multi_selections_ver` like `drop_down_multi_selections`; 
ALTER TABLE `drop_down_multi_selections_ver` ADD COLUMN `Version_ID` INTEGER(11); 
 

DROP TABLE IF EXISTS elements_services_ver;
create table `elements_services_ver` like `elements_services`; 
ALTER TABLE `elements_services_ver` MODIFY COLUMN `Element_id` INT(10) NOT NULL,
 ADD COLUMN `Version_ID` INTEGER(11),
 DROP PRIMARY KEY;


DROP TABLE IF EXISTS elements_products_ver;
create table `elements_products_ver` like `elements_products`; 
ALTER TABLE `elements_products_ver` MODIFY COLUMN `Element_id` INT(10) NOT NULL,
 ADD COLUMN `Version_ID` INTEGER(11),
 DROP PRIMARY KEY;
 

DROP TABLE IF EXISTS `sys_draft_rec`;
CREATE TABLE  `sys_draft_rec` (
  `D_ID` int(11) NOT NULL AUTO_INCREMENT,
  `D_WHEN` datetime NOT NULL,
  `D_WHO` varchar(45) NOT NULL,
  `D_MODULE` varchar(45) NOT NULL,
  `D_TABLE` varchar(45) NOT NULL,
  `D_DRAFT_ID` int(11) NOT NULL,
  `D_DATA_ID` int(11) NOT NULL,
  `D_STATUS` varchar(45) NOT NULL,
  PRIMARY KEY (`D_ID`)
) ;

DROP TABLE IF EXISTS `sys_module_contextmenu`;
CREATE TABLE `sys_module_contextmenu`
(
  `module_name`   varchar(45),
  `has_sort`          int(1) unsigned     default 1,
  `has_copy`          int(1) unsigned     default 1,
  `has_cut`           int(1) unsigned     default 1,
  `has_import`        int(1) unsigned     default 1,
  `has_export`        int(1) unsigned     default 1
);

INSERT INTO sys_module_contextmenu (module_name)
(SELECT internal_module_name FROM sys_moduleconfig);

create table `notes_draft` like `notes`; 
 
create table `drop_down_multi_selections_ver_draft` like `drop_down_multi_selections`; 
 
create table `elements_services_draft` like `elements_services`; 


create table `elements_products_draft` like `elements_products`; 

create table elements_ants_draft like elements_ants; 

create table elements_sushant_draft like elements_sushant; 

create table categories_sushant_draft like categories_sushant; 

ALTER TABLE `openlinks_jobs` 
ADD COLUMN `CronExpression` VARCHAR(200) NULL DEFAULT NULL AFTER `Context`;

ALTER TABLE `notes` 
ADD COLUMN `live` TINYINT(1) NOT NULL DEFAULT 1 AFTER `content`,
ADD COLUMN `live_date` DATETIME NULL AFTER `live`,
ADD COLUMN `expire_date` DATETIME NULL AFTER `live_date`;

ALTER TABLE `notes_ver` 
ADD COLUMN `live` TINYINT(1) NOT NULL DEFAULT 1 AFTER `content`,
ADD COLUMN `live_date` DATETIME NULL AFTER `live`,
ADD COLUMN `expire_date` DATETIME NULL AFTER `live_date`;

ALTER TABLE `notes_draft` 
ADD COLUMN `live` TINYINT(1) NOT NULL DEFAULT 1 AFTER `content`,
ADD COLUMN `live_date` DATETIME NULL AFTER `live`,
ADD COLUMN `expire_date` DATETIME NULL AFTER `live_date`;