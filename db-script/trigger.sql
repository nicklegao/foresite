DELIMITER $$

-- elements_products
ALTER TABLE `elements_products` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_products $$

CREATE TRIGGER `insert_e_products` BEFORE INSERT ON `elements_products` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_products $$

CREATE TRIGGER `update_e_products` BEFORE UPDATE ON `elements_products` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_products
ALTER TABLE `categories_products` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_products $$

CREATE TRIGGER `insert_c_products` BEFORE INSERT ON `categories_products` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_products $$

CREATE TRIGGER `update_c_products` BEFORE UPDATE ON `categories_products` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_events
ALTER TABLE `elements_events` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_events $$

CREATE TRIGGER `insert_e_events` BEFORE INSERT ON `elements_events` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_events $$

CREATE TRIGGER `update_e_events` BEFORE UPDATE ON `elements_events` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_events
ALTER TABLE `categories_events` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_events $$

CREATE TRIGGER `insert_c_events` BEFORE INSERT ON `categories_events` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_events $$

CREATE TRIGGER `update_c_events` BEFORE UPDATE ON `categories_events` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_faq
ALTER TABLE `elements_faq` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_faq $$

CREATE TRIGGER `insert_e_faq` BEFORE INSERT ON `elements_faq` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_faq $$

CREATE TRIGGER `update_e_faq` BEFORE UPDATE ON `elements_faq` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_faq
ALTER TABLE `categories_faq` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_faq $$

CREATE TRIGGER `insert_c_faq` BEFORE INSERT ON `categories_faq` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_faq $$

CREATE TRIGGER `update_c_faq` BEFORE UPDATE ON `categories_faq` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_flex1
ALTER TABLE `elements_flex1` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex1 $$

CREATE TRIGGER `insert_e_flex1` BEFORE INSERT ON `elements_flex1` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex1 $$

CREATE TRIGGER `update_e_flex1` BEFORE UPDATE ON `elements_flex1` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex1
ALTER TABLE `categories_flex1` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex1 $$

CREATE TRIGGER `insert_c_flex1` BEFORE INSERT ON `categories_flex1` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex1 $$

CREATE TRIGGER `update_c_flex1` BEFORE UPDATE ON `categories_flex1` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- elements_ flex2
ALTER TABLE `elements_flex2` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex2 $$

CREATE TRIGGER `insert_e_flex2` BEFORE INSERT ON `elements_flex2` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex2 $$

CREATE TRIGGER `update_e_flex2` BEFORE UPDATE ON `elements_flex2` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex2
ALTER TABLE `categories_flex2` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex2 $$

CREATE TRIGGER `insert_c_flex2` BEFORE INSERT ON `categories_flex2` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex2 $$

CREATE TRIGGER `update_c_flex2` BEFORE UPDATE ON `categories_flex2` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ flex3
ALTER TABLE `elements_flex3` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex3 $$

CREATE TRIGGER `insert_e_flex3` BEFORE INSERT ON `elements_flex3` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex3 $$

CREATE TRIGGER `update_e_flex3` BEFORE UPDATE ON `elements_flex3` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex3
ALTER TABLE `categories_flex3` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex3 $$

CREATE TRIGGER `insert_c_flex3` BEFORE INSERT ON `categories_flex3` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex3 $$

CREATE TRIGGER `update_c_flex3` BEFORE UPDATE ON `categories_flex3` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ flex4
ALTER TABLE `elements_flex4` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex4 $$

CREATE TRIGGER `insert_e_flex4` BEFORE INSERT ON `elements_flex4` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex4 $$

CREATE TRIGGER `update_e_flex4` BEFORE UPDATE ON `elements_flex4` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex4
ALTER TABLE `categories_flex4` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex4 $$

CREATE TRIGGER `insert_c_flex4` BEFORE INSERT ON `categories_flex4` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex4 $$

CREATE TRIGGER `update_c_flex4` BEFORE UPDATE ON `categories_flex4` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ flex5
ALTER TABLE `elements_flex5` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex5 $$

CREATE TRIGGER `insert_e_flex5` BEFORE INSERT ON `elements_flex5` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex5 $$

CREATE TRIGGER `update_e_flex5` BEFORE UPDATE ON `elements_flex5` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex5
ALTER TABLE `categories_flex5` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex5 $$

CREATE TRIGGER `insert_c_flex5` BEFORE INSERT ON `categories_flex5` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex5 $$

CREATE TRIGGER `update_c_flex5` BEFORE UPDATE ON `categories_flex5` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ flex6
ALTER TABLE `elements_flex6` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex6 $$

CREATE TRIGGER `insert_e_flex6` BEFORE INSERT ON `elements_flex6` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex6 $$

CREATE TRIGGER `update_e_flex6` BEFORE UPDATE ON `elements_flex6` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex6
ALTER TABLE `categories_flex6` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex6 $$

CREATE TRIGGER `insert_c_flex6` BEFORE INSERT ON `categories_flex6` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex6 $$

CREATE TRIGGER `update_c_flex6` BEFORE UPDATE ON `categories_flex6` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ flex7
ALTER TABLE `elements_flex7` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex7 $$

CREATE TRIGGER `insert_e_flex7` BEFORE INSERT ON `elements_flex7` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex7 $$

CREATE TRIGGER `update_e_flex7` BEFORE UPDATE ON `elements_flex7` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex7
ALTER TABLE `categories_flex7` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex7 $$

CREATE TRIGGER `insert_c_flex7` BEFORE INSERT ON `categories_flex7` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex7 $$

CREATE TRIGGER `update_c_flex7` BEFORE UPDATE ON `categories_flex7` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ flex8
ALTER TABLE `elements_flex8` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex8 $$

CREATE TRIGGER `insert_e_flex8` BEFORE INSERT ON `elements_flex8` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex8 $$

CREATE TRIGGER `update_e_flex8` BEFORE UPDATE ON `elements_flex8` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex8
ALTER TABLE `categories_flex8` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex8 $$

CREATE TRIGGER `insert_c_flex8` BEFORE INSERT ON `categories_flex8` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex8 $$

CREATE TRIGGER `update_c_flex8` BEFORE UPDATE ON `categories_flex8` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ flex9
ALTER TABLE `elements_flex9` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex9 $$

CREATE TRIGGER `insert_e_flex9` BEFORE INSERT ON `elements_flex9` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex9 $$

CREATE TRIGGER `update_e_flex9` BEFORE UPDATE ON `elements_flex9` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex9
ALTER TABLE `categories_flex9` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex9 $$

CREATE TRIGGER `insert_c_flex9` BEFORE INSERT ON `categories_flex9` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex9 $$

CREATE TRIGGER `update_c_flex9` BEFORE UPDATE ON `categories_flex9` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_flex10
ALTER TABLE `elements_flex10` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex10 $$

CREATE TRIGGER `insert_e_flex10` BEFORE INSERT ON `elements_flex10` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex10 $$

CREATE TRIGGER `update_e_flex10` BEFORE UPDATE ON `elements_flex10` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex10
ALTER TABLE `categories_flex10` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex10 $$

CREATE TRIGGER `insert_c_flex10` BEFORE INSERT ON `categories_flex10` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex10 $$

CREATE TRIGGER `update_c_flex10` BEFORE UPDATE ON `categories_flex10` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_flex11
ALTER TABLE `elements_flex11` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex11 $$

CREATE TRIGGER `insert_e_flex11` BEFORE INSERT ON `elements_flex11` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex11 $$

CREATE TRIGGER `update_e_flex11` BEFORE UPDATE ON `elements_flex11` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex11
ALTER TABLE `categories_flex11` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex11 $$

CREATE TRIGGER `insert_c_flex11` BEFORE INSERT ON `categories_flex11` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex11 $$

CREATE TRIGGER `update_c_flex11` BEFORE UPDATE ON `categories_flex11` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_flex12
ALTER TABLE `elements_flex12` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex12 $$

CREATE TRIGGER `insert_e_flex12` BEFORE INSERT ON `elements_flex12` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex12 $$

CREATE TRIGGER `update_e_flex12` BEFORE UPDATE ON `elements_flex12` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex12
ALTER TABLE `categories_flex12` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex12 $$

CREATE TRIGGER `insert_c_flex12` BEFORE INSERT ON `categories_flex12` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex12 $$

CREATE TRIGGER `update_c_flex12` BEFORE UPDATE ON `categories_flex12` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_flex13
ALTER TABLE `elements_flex13` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex13 $$

CREATE TRIGGER `insert_e_flex13` BEFORE INSERT ON `elements_flex13` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex13 $$

CREATE TRIGGER `update_e_flex13` BEFORE UPDATE ON `elements_flex13` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex13
ALTER TABLE `categories_flex13` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex13 $$

CREATE TRIGGER `insert_c_flex13` BEFORE INSERT ON `categories_flex13` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex13 $$

CREATE TRIGGER `update_c_flex13` BEFORE UPDATE ON `categories_flex13` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_flex14
ALTER TABLE `elements_flex14` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex14 $$

CREATE TRIGGER `insert_e_flex14` BEFORE INSERT ON `elements_flex14` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex14 $$

CREATE TRIGGER `update_e_flex14` BEFORE UPDATE ON `elements_flex14` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex14
ALTER TABLE `categories_flex14` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex14 $$

CREATE TRIGGER `insert_c_flex14` BEFORE INSERT ON `categories_flex14` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex14 $$

CREATE TRIGGER `update_c_flex14` BEFORE UPDATE ON `categories_flex14` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ flex15
ALTER TABLE `elements_flex15` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex15 $$

CREATE TRIGGER `insert_e_flex15` BEFORE INSERT ON `elements_flex15` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex15 $$

CREATE TRIGGER `update_e_flex15` BEFORE UPDATE ON `elements_flex15` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex15
ALTER TABLE `categories_flex15` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex15 $$

CREATE TRIGGER `insert_c_flex15` BEFORE INSERT ON `categories_flex15` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex15 $$

CREATE TRIGGER `update_c_flex15` BEFORE UPDATE ON `categories_flex15` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ flex16
ALTER TABLE `elements_flex16` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex16 $$

CREATE TRIGGER `insert_e_flex16` BEFORE INSERT ON `elements_flex16` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex16 $$

CREATE TRIGGER `update_e_flex16` BEFORE UPDATE ON `elements_flex16` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex16
ALTER TABLE `categories_flex16` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex16 $$

CREATE TRIGGER `insert_c_flex16` BEFORE INSERT ON `categories_flex16` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex16 $$

CREATE TRIGGER `update_c_flex16` BEFORE UPDATE ON `categories_flex16` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ flex17
ALTER TABLE `elements_flex17` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex17 $$

CREATE TRIGGER `insert_e_flex17` BEFORE INSERT ON `elements_flex17` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex17 $$

CREATE TRIGGER `update_e_flex17` BEFORE UPDATE ON `elements_flex17` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex17
ALTER TABLE `categories_flex17` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex17 $$

CREATE TRIGGER `insert_c_flex17` BEFORE INSERT ON `categories_flex17` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex17 $$

CREATE TRIGGER `update_c_flex17` BEFORE UPDATE ON `categories_flex17` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ flex18
ALTER TABLE `elements_flex18` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex18 $$

CREATE TRIGGER `insert_e_flex18` BEFORE INSERT ON `elements_flex18` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex18 $$

CREATE TRIGGER `update_e_flex18` BEFORE UPDATE ON `elements_flex18` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex18
ALTER TABLE `categories_flex18` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex18 $$

CREATE TRIGGER `insert_c_flex18` BEFORE INSERT ON `categories_flex18` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex18 $$

CREATE TRIGGER `update_c_flex18` BEFORE UPDATE ON `categories_flex18` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ flex19
ALTER TABLE `elements_flex19` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex19 $$

CREATE TRIGGER `insert_e_flex19` BEFORE INSERT ON `elements_flex19` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex19 $$

CREATE TRIGGER `update_e_flex19` BEFORE UPDATE ON `elements_flex19` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex19
ALTER TABLE `categories_flex19` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex19 $$

CREATE TRIGGER `insert_c_flex19` BEFORE INSERT ON `categories_flex19` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex19 $$

CREATE TRIGGER `update_c_flex19` BEFORE UPDATE ON `categories_flex19` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ flex20
ALTER TABLE `elements_flex20` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_flex20 $$

CREATE TRIGGER `insert_e_flex20` BEFORE INSERT ON `elements_flex20` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_flex20 $$

CREATE TRIGGER `update_e_flex20` BEFORE UPDATE ON `elements_flex20` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_flex20
ALTER TABLE `categories_flex20` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_flex20 $$

CREATE TRIGGER `insert_c_flex20` BEFORE INSERT ON `categories_flex20` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_flex20 $$

CREATE TRIGGER `update_c_flex20` BEFORE UPDATE ON `categories_flex20` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- elements_ help
ALTER TABLE `elements_help` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_help $$

CREATE TRIGGER `insert_e_help` BEFORE INSERT ON `elements_help` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_help $$

CREATE TRIGGER `update_e_help` BEFORE UPDATE ON `elements_help` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_help
ALTER TABLE `categories_help` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_help $$

CREATE TRIGGER `insert_c_help` BEFORE INSERT ON `categories_help` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_help $$

CREATE TRIGGER `update_c_help` BEFORE UPDATE ON `categories_help` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ information
ALTER TABLE `elements_information` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_information $$

CREATE TRIGGER `insert_e_information` BEFORE INSERT ON `elements_information` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_information $$

CREATE TRIGGER `update_e_information` BEFORE UPDATE ON `elements_information` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_information
ALTER TABLE `categories_information` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_information $$

CREATE TRIGGER `insert_c_information` BEFORE INSERT ON `categories_information` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_information $$

CREATE TRIGGER `update_c_information` BEFORE UPDATE ON `categories_information` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ jobadvertising
ALTER TABLE `elements_jobadvertising` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_jobadvertising $$

CREATE TRIGGER `insert_e_jobadvertising` BEFORE INSERT ON `elements_jobadvertising` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_jobadvertising $$

CREATE TRIGGER `update_e_jobadvertising` BEFORE UPDATE ON `elements_jobadvertising` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_jobadvertising
ALTER TABLE `categories_jobadvertising` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_jobadvertising $$

CREATE TRIGGER `insert_c_jobadvertising` BEFORE INSERT ON `categories_jobadvertising` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_jobadvertising $$

CREATE TRIGGER `update_c_jobadvertising` BEFORE UPDATE ON `categories_jobadvertising` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- elements_ locations
ALTER TABLE `elements_locations` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_locations $$

CREATE TRIGGER `insert_e_locations` BEFORE INSERT ON `elements_locations` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_locations $$

CREATE TRIGGER `update_e_locations` BEFORE UPDATE ON `elements_locations` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_locations
ALTER TABLE `categories_locations` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_locations $$

CREATE TRIGGER `insert_c_locations` BEFORE INSERT ON `categories_locations` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_locations $$

CREATE TRIGGER `update_c_locations` BEFORE UPDATE ON `categories_locations` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ members
ALTER TABLE `elements_members` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_members $$

CREATE TRIGGER `insert_e_members` BEFORE INSERT ON `elements_members` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_members $$

CREATE TRIGGER `update_e_members` BEFORE UPDATE ON `elements_members` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_members
ALTER TABLE `categories_members` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_members $$

CREATE TRIGGER `insert_c_members` BEFORE INSERT ON `categories_members` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_members $$

CREATE TRIGGER `update_c_members` BEFORE UPDATE ON `categories_members` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ news
ALTER TABLE `elements_news` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_news $$

CREATE TRIGGER `insert_e_news` BEFORE INSERT ON `elements_news` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_news $$

CREATE TRIGGER `update_e_news` BEFORE UPDATE ON `elements_news` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_news
ALTER TABLE `categories_news` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_news $$

CREATE TRIGGER `insert_c_news` BEFORE INSERT ON `categories_news` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_news $$

CREATE TRIGGER `update_c_news` BEFORE UPDATE ON `categories_news` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ referencelib
ALTER TABLE `elements_referencelib` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_referencelib $$

CREATE TRIGGER `insert_e_referencelib` BEFORE INSERT ON `elements_referencelib` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_referencelib $$

CREATE TRIGGER `update_e_referencelib` BEFORE UPDATE ON `elements_referencelib` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_referencelib
ALTER TABLE `categories_referencelib` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_referencelib $$

CREATE TRIGGER `insert_c_referencelib` BEFORE INSERT ON `categories_referencelib` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_referencelib $$

CREATE TRIGGER `update_c_referencelib` BEFORE UPDATE ON `categories_referencelib` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ salesorders
ALTER TABLE `elements_salesorders` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_salesorders $$

CREATE TRIGGER `insert_e_salesorders` BEFORE INSERT ON `elements_salesorders` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_salesorders $$

CREATE TRIGGER `update_e_salesorders` BEFORE UPDATE ON `elements_salesorders` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_salesorders
ALTER TABLE `categories_salesorders` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_salesorders $$

CREATE TRIGGER `insert_c_salesorders` BEFORE INSERT ON `categories_salesorders` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_salesorders $$

CREATE TRIGGER `update_c_salesorders` BEFORE UPDATE ON `categories_salesorders` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ services
ALTER TABLE `elements_services` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_services $$

CREATE TRIGGER `insert_e_services` BEFORE INSERT ON `elements_services` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_services $$

CREATE TRIGGER `update_e_services` BEFORE UPDATE ON `elements_services` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_services
ALTER TABLE `categories_services` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_services $$

CREATE TRIGGER `insert_c_services` BEFORE INSERT ON `categories_services` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_services $$

CREATE TRIGGER `update_c_services` BEFORE UPDATE ON `categories_services` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ smallbusiness
ALTER TABLE `elements_smallbusiness` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_smallbusiness $$

CREATE TRIGGER `insert_e_smallbusiness` BEFORE INSERT ON `elements_smallbusiness` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_smallbusiness $$

CREATE TRIGGER `update_e_smallbusiness` BEFORE UPDATE ON `elements_smallbusiness` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_smallbusiness
ALTER TABLE `categories_smallbusiness` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_smallbusiness $$

CREATE TRIGGER `insert_c_smallbusiness` BEFORE INSERT ON `categories_smallbusiness` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_smallbusiness $$

CREATE TRIGGER `update_c_smallbusiness` BEFORE UPDATE ON `categories_smallbusiness` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- elements_ specialoffers
ALTER TABLE `elements_specialoffers` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_specialoffers $$

CREATE TRIGGER `insert_e_specialoffers` BEFORE INSERT ON `elements_specialoffers` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_specialoffers $$

CREATE TRIGGER `update_e_specialoffers` BEFORE UPDATE ON `elements_specialoffers` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_specialoffers
ALTER TABLE `categories_specialoffers` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_specialoffers $$

CREATE TRIGGER `insert_c_specialoffers` BEFORE INSERT ON `categories_specialoffers` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_specialoffers $$

CREATE TRIGGER `update_c_specialoffers` BEFORE UPDATE ON `categories_specialoffers` FOR EACH ROW begin
set
new.last_update_date=now();
end $$



-- elements_ subscribe
ALTER TABLE `elements_subscribe` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_subscribe $$

CREATE TRIGGER `insert_e_subscribe` BEFORE INSERT ON `elements_subscribe` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_subscribe $$

CREATE TRIGGER `update_e_subscribe` BEFORE UPDATE ON `elements_subscribe` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_subscribe
ALTER TABLE `categories_subscribe` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_subscribe $$

CREATE TRIGGER `insert_c_subscribe` BEFORE INSERT ON `categories_subscribe` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_subscribe $$

CREATE TRIGGER `update_c_subscribe` BEFORE UPDATE ON `categories_subscribe` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


-- elements_ technicaltesting
ALTER TABLE `elements_technicaltesting` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_e_technicaltesting $$

CREATE TRIGGER `insert_e_technicaltesting` BEFORE INSERT ON `elements_technicaltesting` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_e_technicaltesting $$

CREATE TRIGGER `update_e_technicaltesting` BEFORE UPDATE ON `elements_technicaltesting` FOR EACH ROW begin
set
new.last_update_date=now();
end $$

-- categories_technicaltesting
ALTER TABLE `categories_technicaltesting` ADD COLUMN `LAST_UPDATE_DATE` DATETIME AFTER `CREATE_DATE`$$

drop trigger if exists insert_c_technicaltesting $$

CREATE TRIGGER `insert_c_technicaltesting` BEFORE INSERT ON `categories_technicaltesting` FOR EACH ROW begin
set
new.Create_date=now();
end $$


drop trigger if exists update_c_technicaltesting $$

CREATE TRIGGER `update_c_technicaltesting` BEFORE UPDATE ON `categories_technicaltesting` FOR EACH ROW begin
set
new.last_update_date=now();
end $$


DELIMITER ;

update categories_events           set create_date = now() where create_date is null;
update categories_faq              set create_date = now() where create_date is null;
update categories_flex1            set create_date = now() where create_date is null;
update categories_flex10           set create_date = now() where create_date is null;
update categories_flex11           set create_date = now() where create_date is null;
update categories_flex12           set create_date = now() where create_date is null;
update categories_flex13           set create_date = now() where create_date is null;
update categories_flex14           set create_date = now() where create_date is null;
update categories_flex15           set create_date = now() where create_date is null;
update categories_flex16           set create_date = now() where create_date is null;
update categories_flex17           set create_date = now() where create_date is null;
update categories_flex18           set create_date = now() where create_date is null;
update categories_flex19           set create_date = now() where create_date is null;
update categories_flex2            set create_date = now() where create_date is null;
update categories_flex20           set create_date = now() where create_date is null;
update categories_flex3            set create_date = now() where create_date is null;
update categories_flex4            set create_date = now() where create_date is null;
update categories_flex5            set create_date = now() where create_date is null;
update categories_flex6            set create_date = now() where create_date is null;
update categories_flex7            set create_date = now() where create_date is null;
update categories_flex8            set create_date = now() where create_date is null;
update categories_flex9            set create_date = now() where create_date is null;
update categories_help             set create_date = now() where create_date is null;
update categories_information      set create_date = now() where create_date is null;
update categories_jobadvertising   set create_date = now() where create_date is null;
update categories_locations        set create_date = now() where create_date is null;
update categories_members          set create_date = now() where create_date is null;
update categories_news             set create_date = now() where create_date is null;
update categories_products         set create_date = now() where create_date is null;
update categories_referencelib     set create_date = now() where create_date is null;
update categories_salesorders      set create_date = now() where create_date is null;
update categories_services         set create_date = now() where create_date is null;
update categories_smallbusiness    set create_date = now() where create_date is null;
update categories_specialoffers    set create_date = now() where create_date is null;
update categories_subscribe        set create_date = now() where create_date is null;
update categories_technicaltesting set create_date = now() where create_date is null;

update categories_events           set last_update_date = now() where last_update_date is null;
update categories_faq              set last_update_date = now() where last_update_date is null;
update categories_flex1            set last_update_date = now() where last_update_date is null;
update categories_flex10           set last_update_date = now() where last_update_date is null;
update categories_flex11           set last_update_date = now() where last_update_date is null;
update categories_flex12           set last_update_date = now() where last_update_date is null;
update categories_flex13           set last_update_date = now() where last_update_date is null;
update categories_flex14           set last_update_date = now() where last_update_date is null;
update categories_flex15           set last_update_date = now() where last_update_date is null;
update categories_flex16           set last_update_date = now() where last_update_date is null;
update categories_flex17           set last_update_date = now() where last_update_date is null;
update categories_flex18           set last_update_date = now() where last_update_date is null;
update categories_flex19           set last_update_date = now() where last_update_date is null;
update categories_flex2            set last_update_date = now() where last_update_date is null;
update categories_flex20           set last_update_date = now() where last_update_date is null;
update categories_flex3            set last_update_date = now() where last_update_date is null;
update categories_flex4            set last_update_date = now() where last_update_date is null;
update categories_flex5            set last_update_date = now() where last_update_date is null;
update categories_flex6            set last_update_date = now() where last_update_date is null;
update categories_flex7            set last_update_date = now() where last_update_date is null;
update categories_flex8            set last_update_date = now() where last_update_date is null;
update categories_flex9            set last_update_date = now() where last_update_date is null;
update categories_help             set last_update_date = now() where last_update_date is null;
update categories_information      set last_update_date = now() where last_update_date is null;
update categories_jobadvertising   set last_update_date = now() where last_update_date is null;
update categories_locations        set last_update_date = now() where last_update_date is null;
update categories_members          set last_update_date = now() where last_update_date is null;
update categories_news             set last_update_date = now() where last_update_date is null;
update categories_products         set last_update_date = now() where last_update_date is null;
update categories_referencelib     set last_update_date = now() where last_update_date is null;
update categories_salesorders      set last_update_date = now() where last_update_date is null;
update categories_services         set last_update_date = now() where last_update_date is null;
update categories_smallbusiness    set last_update_date = now() where last_update_date is null;
update categories_specialoffers    set last_update_date = now() where last_update_date is null;
update categories_subscribe        set last_update_date = now() where last_update_date is null;
update categories_technicaltesting set last_update_date = now() where last_update_date is null;

update elements_events           set create_date = now() where create_date is null;
update elements_faq              set create_date = now() where create_date is null;
update elements_flex1            set create_date = now() where create_date is null;
update elements_flex10           set create_date = now() where create_date is null;
update elements_flex11           set create_date = now() where create_date is null;
update elements_flex12           set create_date = now() where create_date is null;
update elements_flex13           set create_date = now() where create_date is null;
update elements_flex14           set create_date = now() where create_date is null;
update elements_flex15           set create_date = now() where create_date is null;
update elements_flex16           set create_date = now() where create_date is null;
update elements_flex17           set create_date = now() where create_date is null;
update elements_flex18           set create_date = now() where create_date is null;
update elements_flex19           set create_date = now() where create_date is null;
update elements_flex2            set create_date = now() where create_date is null;
update elements_flex20           set create_date = now() where create_date is null;
update elements_flex3            set create_date = now() where create_date is null;
update elements_flex4            set create_date = now() where create_date is null;
update elements_flex5            set create_date = now() where create_date is null;
update elements_flex6            set create_date = now() where create_date is null;
update elements_flex7            set create_date = now() where create_date is null;
update elements_flex8            set create_date = now() where create_date is null;
update elements_flex9            set create_date = now() where create_date is null;
update elements_help             set create_date = now() where create_date is null;
update elements_information      set create_date = now() where create_date is null;
update elements_jobadvertising   set create_date = now() where create_date is null;
update elements_locations        set create_date = now() where create_date is null;
update elements_members          set create_date = now() where create_date is null;
update elements_news             set create_date = now() where create_date is null;
update elements_products         set create_date = now() where create_date is null;
update elements_referencelib     set create_date = now() where create_date is null;
update elements_salesorders      set create_date = now() where create_date is null;
update elements_services         set create_date = now() where create_date is null;
update elements_smallbusiness    set create_date = now() where create_date is null;
update elements_specialoffers    set create_date = now() where create_date is null;
update elements_subscribe        set create_date = now() where create_date is null;
update elements_technicaltesting set create_date = now() where create_date is null;

update elements_events           set last_update_date = now() where last_update_date is null;
update elements_faq              set last_update_date = now() where last_update_date is null;
update elements_flex1            set last_update_date = now() where last_update_date is null;
update elements_flex10           set last_update_date = now() where last_update_date is null;
update elements_flex11           set last_update_date = now() where last_update_date is null;
update elements_flex12           set last_update_date = now() where last_update_date is null;
update elements_flex13           set last_update_date = now() where last_update_date is null;
update elements_flex14           set last_update_date = now() where last_update_date is null;
update elements_flex15           set last_update_date = now() where last_update_date is null;
update elements_flex16           set last_update_date = now() where last_update_date is null;
update elements_flex17           set last_update_date = now() where last_update_date is null;
update elements_flex18           set last_update_date = now() where last_update_date is null;
update elements_flex19           set last_update_date = now() where last_update_date is null;
update elements_flex2            set last_update_date = now() where last_update_date is null;
update elements_flex20           set last_update_date = now() where last_update_date is null;
update elements_flex3            set last_update_date = now() where last_update_date is null;
update elements_flex4            set last_update_date = now() where last_update_date is null;
update elements_flex5            set last_update_date = now() where last_update_date is null;
update elements_flex6            set last_update_date = now() where last_update_date is null;
update elements_flex7            set last_update_date = now() where last_update_date is null;
update elements_flex8            set last_update_date = now() where last_update_date is null;
update elements_flex9            set last_update_date = now() where last_update_date is null;
update elements_help             set last_update_date = now() where last_update_date is null;
update elements_information      set last_update_date = now() where last_update_date is null;
update elements_jobadvertising   set last_update_date = now() where last_update_date is null;
update elements_locations        set last_update_date = now() where last_update_date is null;
update elements_members          set last_update_date = now() where last_update_date is null;
update elements_news             set last_update_date = now() where last_update_date is null;
update elements_products         set last_update_date = now() where last_update_date is null;
update elements_referencelib     set last_update_date = now() where last_update_date is null;
update elements_salesorders      set last_update_date = now() where last_update_date is null;
update elements_services         set last_update_date = now() where last_update_date is null;
update elements_smallbusiness    set last_update_date = now() where last_update_date is null;
update elements_specialoffers    set last_update_date = now() where last_update_date is null;
update elements_subscribe        set last_update_date = now() where last_update_date is null;
update elements_technicaltesting set last_update_date = now() where last_update_date is null;
