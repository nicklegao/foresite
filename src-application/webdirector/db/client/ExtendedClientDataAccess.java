package webdirector.db.client;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Vector;

public class ExtendedClientDataAccess extends ClientDataAccess{
	
	/**
	 */
	public Set<String> getCategoryIdByElements(String module, String... elements) {
	    Set<String> matches = new LinkedHashSet<String>();
		for (String element_id : elements)
		{
			Vector match = getElement(module, element_id, false);
			if (match.size() > 0)
			{
				Hashtable<String, String> ht = (Hashtable<String, String>) match.get(0);
				matches.add(ht.get("Category_id"));
			}
		}
		return matches;
	}
	
	public Hashtable<String, String> getElementByNameWithCategory(String module, String elementName, int cID) {
		String categoryId = Integer.toString(cID);
		Vector<Hashtable<String, String>> v = getElementByName(module, elementName);
		if (v != null && v.size() > 0)
		{
			for (Hashtable<String, String> ht : v)
			{
				String parentId = ht.get("Category_id");
				if (parentId.equals(categoryId))
				{
					return ht;
				}
			}
		}
		return null;
	}

	public String getCategoryIdByCategoryNameWithParent(String module, String categoryName, int parentId) {
		String pargetParent = Integer.toString(parentId);
		Vector<Hashtable<String, String>> categories = getCategoryChildren(module, pargetParent);
		for (Hashtable<String, String> ht : categories)
		{
			if (pargetParent.equals(ht.get("Category_ParentID")) && categoryName.equalsIgnoreCase(ht.get("ATTR_categoryName")))
			{
				return ht.get("Category_id");
			}
		}
		return null;
	}
}
