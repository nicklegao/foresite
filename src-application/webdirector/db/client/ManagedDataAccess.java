package webdirector.db.client;

import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TManagedDataAccess;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.utils.module.StoreUtils;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

public abstract class ManagedDataAccess
{
	Logger logger = Logger.getLogger(ManagedDataAccess.class);

	public static final SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final SimpleDateFormat dbDateFormatNew = new SimpleDateFormat("dd MMMM yyyy HH:mm a");
	public static final String E_ID = "Element_id";
	public static final String E_CATEGORY_ID = "Category_id";
	public static final String E_HEADLINE = "ATTR_Headline";
	public static final String E_LIVE = "Live";

	public static final String C_ID = "Category_id";
	public static final String C_NAME = "ATTR_categoryName";
	public static final String C_PARENT = "Category_ParentID";
	public static final String C_FOLDER_LEVEL = "folderLevel";
	public static final String C_LIVE = E_LIVE;

	protected DBaccess db = new DBaccess();
	protected ExtendedClientDataAccess cda = new ExtendedClientDataAccess();

	protected Hashtable<String, String> standardCreateCategoryHashtable(String label)
	{
		return standardCreateCategoryHashtable(0, label);
	}

	protected Hashtable<String, String> standardCreateCategoryHashtable(int parentId, String label)
	{
		return standardCreateCategoryHashtable(parentId, 1, label);
	}

	protected Hashtable<String, String> standardCreateCategoryHashtable(int parentId, int folderLevel, String label)
	{
		Hashtable<String, String> ht = new Hashtable<String, String>();
		ht.put("folderLevel", Integer.toString(folderLevel));

		ht.put("Category_ParentID", Integer.toString(parentId));
		ht.put("ATTR_categoryName", label);

		return ht;
	}

	protected Hashtable<String, String> standardCreateElementHashtable(int categoryId, String label)
	{
		return standardCreateElementHashtable(Integer.toString(categoryId), label);
	}

	protected Hashtable<String, String> standardCreateElementHashtable(String categoryId, String label)
	{
		Hashtable<String, String> ht = new Hashtable<String, String>();
		ht.put("Live", Integer.toString(1));
		ht.put("Language_id", Integer.toString(1));
		ht.put("Status_id", Integer.toString(1));
		ht.put("Version", Integer.toString(1));
		ht.put("Lock_id", Integer.toString(1));

		ht.put("ATTR_Headline", label);
		ht.put("Category_ID", categoryId);

		return ht;
	}

	protected String standardLiveClauseForCustomQuery()
	{
		return standardLiveClauseForCustomQuery(null);
	}

	protected String standardLiveClauseForCustomQuery(String tableAlias)
	{
		String tablePrefix = "";
		if (tableAlias != null && tableAlias.length() > 0)
		{
			tablePrefix = tableAlias + ".";
		}
		return " " + tablePrefix + "Live = 1 "
				+ " and (" + tablePrefix + "Live_date <= now() or " + tablePrefix + "Live_date IS NULL ) "
				+ " and (" + tablePrefix + "Expire_date >= now() or " + tablePrefix + "Expire_date IS NULL ) ";
	}

	protected int createElement(Hashtable<String, String> ht,
			String module)
	{
		return db.insertData(ht, "Element_id", "elements_" + module);
	}

	protected int createCategory(Hashtable<String, String> ht,
			String module)
	{
		return db.insertData(ht, "Category_id", "categories_" + module);
	}

	protected int updateElement(Hashtable<String, String> ht,
			String module)
	{
		return db.updateData(ht, "Element_id", "elements_" + module);
	}

	protected int updateCategory(Hashtable<String, String> ht,
			String module)
	{
		return db.updateData(ht, "Category_id", "categories_" + module);
	}

	protected boolean deleteElement(String element,
			String module)
	{
		return deleteElement(Integer.parseInt(element), module);
	}

	protected boolean deleteElement(int element,
			String module)
	{
		StoreUtils su = new StoreUtils();
		boolean success = su.deleteElementFromStores(module, element);

		int updated = DataAccess.getInstance().updateData(
				"delete from elements_" + module +
						" where Element_id=?",
				new String[] { Integer.toString(element) });
		//Uncomment and test the following when needed...
//      int dropdowns = DataAccess.getInstance().updateData(
//                      "delete from drop_down_multi_selections "+
//                      "where MODULE_NAME="+module+" "+
//                      "and ELEMENT_ID="+element, new String[]{});

		return success && updated > 0;
	}

	public int getOrCreateCategory(String categoryName, String module)
	{
		return getOrCreateCategory(categoryName, 0, 1, module);
	}

	public int getOrCreateCategory(String companyId, String companyName, String module)
	{
		List<String> list = db.selectQuerySingleCol("select category_id from categories_" + module + " where attrdrop_companyid = ?", new String[] { companyId });
		if (list.size() > 0)
		{
			return Integer.parseInt(list.get(0));
		}
		Hashtable<String, String> ht = standardCreateCategoryHashtable(companyName);
		insertNotNull(ht, "Category_ParentID", "0");
		insertNotNull(ht, "folderLevel", "1");
		insertNotNull(ht, "ATTRDROP_companyID", companyId);
		return createCategory(ht, module);
	}

	public int getOrCreateCategory(String categoryName, int parentId, int folderLevel, String module)
	{
		String categoryId = cda.getCategoryIdByCategoryNameWithParent(module, categoryName, parentId);
		if (categoryId == null)
		{
			Hashtable<String, String> ht = standardCreateCategoryHashtable(categoryName);
			insertNotNull(ht, "Category_ParentID", parentId);
			insertNotNull(ht, "folderLevel", folderLevel);
			categoryId = Integer.toString(createCategory(ht, module));
		}
		return Integer.parseInt(categoryId);
	}

	//Avoid using this.
	protected void insertNotNull(Hashtable<String, String> ht, String key,
			String val)
	{
		if (val != null)
			ht.put(key, val);
	}

	protected void insertNotNull(Hashtable<String, String> ht, String key,
			String val, int trimLength)
	{
		if (val != null)
			insertNotNull(ht, key, val.length() <= trimLength ? val : val.substring(0, trimLength));
	}

	protected void insertNotNull(Hashtable<String, String> ht, String key,
			int val)
	{
		ht.put(key, Integer.toString(val));
	}

	protected void insertNotNull(Hashtable<String, String> ht, String key,
			double val)
	{
		ht.put(key, Double.toString(val));
	}

	protected void insertNotNull(Hashtable<String, String> ht, String key,
			boolean val)
	{
		ht.put(key, val ? "1" : "0");
	}

	protected void insertNotNull(Hashtable<String, String> ht, String key,
			Date val)
	{
		if (val != null)
			ht.put(key, dbDateFormat.format(val));
	}

	/*
	 * Reusable utility functions
	 */

	protected int getYMDCategory(Date date, String module)
	{
		int yearID = getOrCreateCategory(new SimpleDateFormat("yyyy").format(date), module);
		int monthID = getOrCreateCategory(new SimpleDateFormat("MM MMM").format(date), yearID, 2, module);
		int dayID = getOrCreateCategory(new SimpleDateFormat("dd EEE").format(date), monthID, 3, module);
		return dayID;
	}

	@Deprecated
	protected boolean checkContentAccess(String contenId, String module, String proposalId)
	{
//		String ownerId = new ProposalDataAccess().getTravelDocById(proposalId).get(ProposalDataAccess.E_OWNER);
//		UserAccountDataAccess uada = new UserAccountDataAccess();
//		String userCompanyId = uada.getUserCompanyIdForUserId(ownerId);
//		DataAccess da = DataAccess.getInstance();
//
//		List<Hashtable<String, String>> result = da.select("SELECT c1.ATTRDROP_CompanyID as companyId "
//				+ "FROM categories_" + module + " c1 join categories_" + module + " c2, elements_" + module + " e "
//				+ "where c1.Category_id=c2.Category_ParentID "
//				+ "and e.Category_id=c2.Category_id and e.Element_id=?",
//				new String[] { contenId },
//				HashtableMapper.getInstance());
//
//		return userCompanyId.equals(result.get(0).get("companyId"));
		boolean result = false;
		try (TransactionManager transactionManager = TransactionManager.getInstance())
		{
			TManagedDataAccess tmda = new TManagedDataAccess(transactionManager);
			transactionManager.commit();
			result = tmda.checkContentAccess(contenId, module, proposalId);
		}
		catch (Exception e)
		{
			logger.fatal("Oh No!", e);
		}
		return result;
	}
}
