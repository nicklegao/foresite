package sbe.openlinks.jobs;

import org.quartz.JobExecutionContext;

import com.mandrill.MandrillWebhookInitializer;

public class MandrillWebhookInitJob extends AbstractScheduledJob
{
	int total;
	int successNo;
	int failedNo;

	@Override
	public String getJobDescription()
	{
		return "Init mandrill web hook for all hosts";
	}

	@Override
	public void beforeJob(JobExecutionContext context)
	{
		total = 0;
		successNo = 0;
		failedNo = 0;
	}

	@Override
	public void doJob(JobExecutionContext context)
	{
		total = new MandrillWebhookInitializer(logger).init();
		successNo = total;
	}

	@Override
	public int getTotal()
	{
		return total;
	}

	@Override
	public int getSuccessNo()
	{
		return successNo;
	}

	@Override
	public int getFailedNo()
	{
		// TODO Auto-generated method stub
		return 0;
	}


}
