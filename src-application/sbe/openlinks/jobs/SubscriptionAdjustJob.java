package sbe.openlinks.jobs;

import static au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess.C_BILLING_USERS;
import static au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess.USER_MODULE;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;

import com.stripe.net.APIResource;

import au.com.ci.crm.xero.ContactsSynchronizer;
import au.com.ci.crm.xero.TDInvoicesSynchronizer;
import au.com.ci.sbe.util.ModuleVariables;
import au.corporateinteractive.qcloud.proposalbuilder.db.InvoiceDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.SubscriptionChangesDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.model.bill.BookingSummary;
import au.corporateinteractive.qcloud.proposalbuilder.model.bill.TDInvoice;
import au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder;
import au.corporateinteractive.qcloud.proposalbuilder.service.StripeService;
import au.corporateinteractive.qcloud.subscription.SubscriptionPlan;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.entity.MultiOptions;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.utils.email.PendingEmail;

public class SubscriptionAdjustJob extends AbstractScheduledJob
{
	private static Logger logger = Logger.getLogger(SubscriptionAdjustJob.class);

	TDInvoicesSynchronizer invoiceSync;
	ContactsSynchronizer contactSync;

	int total;
	int successNo;
	int failedNo;

	@Override
	public String getJobDescription()
	{
		return "Generate Invoice For Each Customer";
	}

	@Override
	public void beforeJob(JobExecutionContext context)
	{
		total = 0;
		successNo = 0;
		failedNo = 0;

		invoiceSync = new TDInvoicesSynchronizer(logger, "AU");
		contactSync = new ContactsSynchronizer(logger, "AU");
	}

	@Override
	public void doJob(JobExecutionContext context)
	{
		Date date = null;
		try
		{
			date = new SimpleDateFormat("yyyy-MM-dd").parse((String) context.getMergedJobDataMap().get("date"));
		}
		catch (Exception e)
		{

		}
		String email = null;
		try
		{
			email = (String) context.getMergedJobDataMap().get("email");
			if (StringUtils.isBlank(email))
			{
				email = getSendToEmail();
			}
		}
		catch (Exception e)
		{

		}
		List<String> customers = null;
		try
		{

			if (context.getMergedJobDataMap().get("customers") != null)
			{
				customers = (List<String>) context.getMergedJobDataMap().get("customers");
			}
			if (customers == null)
			{
				customers = selectCustomersToBill();
			}
			total = customers.size();
			for (String companyId : customers)
			{

				// update main subscription
				try (TransactionManager txManager = TransactionManager.getInstance())
				{
					TUserAccountDataAccess ada = new TUserAccountDataAccess(txManager);
					CategoryData company = ada.getCompanyWithId(companyId);
					logger.info("Adjusting subscription for company (" + companyId + ").");
					adjustCustomerSubscription(company, ada, txManager);
					txManager.commit();
					logger.info("Generating invoice for company (" + companyId + ").");
					generateInvoice(company, txManager);
					txManager.commit();
					successNo++;
					logger.info("Processing company (" + companyId + ") finished.");
				}
				catch (Exception e)
				{
					logger.error("Cannot process company (" + companyId + ")", e);
					failedNo++;
				}
			}
		}
		catch (Exception e)
		{
			// don't care
		}


		if (true)
		{
			logger.info("Synchronising contacts ...");
			contactSync.sendSyncResult(date, email);
			logger.info("Synchronising invoices ...");
			invoiceSync.sendSyncResult(date, email);
		}
	}

	private String getSendToEmail()
	{
		try
		{
			return ModuleVariables.getString("XeroSyncSendToEmailAddress");
		}
		catch (Exception e)
		{
			return "";
		}
	}

	private String generateInvoice(CategoryData company, TransactionManager txManager) throws Exception
	{
		if (!UserAccountDataAccess.isPayNeeded(company))
		{
			return null;
		}
		SubscriptionChangesDataAccess subscriptionService = new SubscriptionChangesDataAccess();
		CategoryData category = subscriptionService.getCategory(company.getId());
		Date today = DateUtils.truncate(new Date(), Calendar.DATE);

		Calendar oneMonthAgo = Calendar.getInstance();
		oneMonthAgo.setTime(today);
		oneMonthAgo.add(Calendar.MONTH, -1);
		List<ElementData> unbilleds = subscriptionService.getUnbilledSubscriptions(company.getId(), oneMonthAgo.getTime());
		if (!subscriptionService.needBill(category, unbilleds, company, oneMonthAgo.getTime()))
		{
			return null;
		}
		List<BookingSummary> summaries = subscriptionService.getBookingSummaries(company, oneMonthAgo.getTime(), today, txManager);

		List<DataMap> usages = subscriptionService.getUsages(company, summaries);
		TDInvoice invoice = subscriptionService.constructInvoice(category, unbilleds, usages, today, summaries);
		String invoiceJson = APIResource.PRETTY_PRINT_GSON.toJson(invoice);
		double amount = StripeService.toDollar(invoice.getTotal());
		ElementData invoiceCI = new InvoiceDataAccess().saveInvoice(company.getId(), company.getName(), invoice.getId(), invoiceJson, today, amount, invoice.getCurrency());
		StripeService.getInstance().generateInvoicePdf(invoiceCI, null);

		//TODO: Charge via Stripe? Not for now to reduce expense.

		subscriptionService.markAsBilled(category, unbilleds, today, txManager);
		String invoiceId = invoiceCI.getId();

		sendInvoice(company, invoiceCI);
		return invoiceId;
	}

	private void sendInvoice(CategoryData company, ElementData invoiceCI)
	{
		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");
		String[] receiverEmails = StripeService.getUserEmailsInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_USERS);

		try
		{
			MultiOptions billingUsersList = ModuleAccess.getInstance().getMultiOptions(company, C_BILLING_USERS,
					USER_MODULE);

			if (billingUsersList.getValues().size() > 0)
			{
				receiverEmails = new String[billingUsersList.getValues().size()];
				receiverEmails = billingUsersList.getValues().toArray(receiverEmails);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}


		EmailBuilder builder = new EmailBuilder()
				.addModule(UserAccountDataAccess.USER_MODULE, company)
				.addModule(InvoiceDataAccess.MODULE, invoiceCI);

		PendingEmail email = builder.prepareInvoiceReadyEmail();

		email.addTo(receiverEmails);
		email.addBCC(adminEmail.split(","));
		email.addAttachment(invoiceCI.getStoreFile(InvoiceDataAccess.E_INVOICE_PDF).getStoredFile());
		email.doSend();

	}

	public void adjustCustomerSubscription(CategoryData company, TUserAccountDataAccess ada, TransactionManager txManager) throws SQLException
	{
		SubscriptionPlan currentPlan = SubscriptionPlan.getPlanByName(company.getString(ada.C_ADDRESS_COUNTRY), company.getString(ada.C_PRICING_PLAN));
		currentPlan.setQuantity(1);
		if (SubscriptionPlan.isTrialPlan(currentPlan.getName())
				|| !UserAccountDataAccess.isPayNeeded(company)
				|| billingNotStartedYet(company))
		{
			return;
		}

		SubscriptionPlan additionalUserPlan = SubscriptionPlan.getAdditionalUserPlan(company.getString(ada.C_ADDRESS_COUNTRY));
		int currentUserAmount = ada.countUsers(company.getId());
		additionalUserPlan.setQuantity(currentUserAmount - currentPlan.getMaxUsers());
		new SubscriptionChangesDataAccess().upgradePlan(company, currentPlan, txManager, null, additionalUserPlan);
	}

	private boolean billingNotStartedYet(CategoryData company)
	{
		Date billingStartDate = company.getDate(TUserAccountDataAccess.C1_BILLING_START_DATE);
		if (billingStartDate == null)
		{
			throw new RuntimeException("No billing start date set for the customer : (" + company.getId() + ") " + company.getName());
		}
		billingStartDate = DateUtils.truncate(billingStartDate, Calendar.DATE);
		Date today = DateUtils.truncate(new Date(), Calendar.DATE);
		return billingStartDate.after(today);
	}

	private List<String> selectCustomersToBill()
	{
		List<String> customers = new ArrayList<>();
		try
		{
			List<CategoryData> companys = ModuleAccess.getInstance().getCategories(
					"select * from categories_useraccounts where "
							+ " live = 1 and attrdrop_pricingplan != 'trial' "
							// + " and (attrdate_lastbillingdate is null or date(attrdate_lastbillingdate) != date(now())) "
							+ " and folderlevel = 1 "
							+ " and (attrdrop_status is null or attrdrop_status != 'suspended')");
			for (CategoryData company : companys)
			{
				customers.add(company.getId());
			}
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
		}
		return customers;
	}


	@Override
	public int getTotal()
	{
		return total;
	}

	@Override
	public int getSuccessNo()
	{
		return successNo;
	}

	@Override
	public int getFailedNo()
	{
		return failedNo;
	}


}
