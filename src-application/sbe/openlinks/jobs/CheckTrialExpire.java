package sbe.openlinks.jobs;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;

import au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;

public class CheckTrialExpire extends AbstractScheduledJob
{
	private static Logger logger = Logger.getLogger(CheckTrialExpire.class);

	int total;
	int successNo;
	int failedNo;

	@Override
	public String getJobDescription()
	{
		return "Check for companies with expired trial period";
	}

	@Override
	public void beforeJob(JobExecutionContext context)
	{
		total = 0;
		successNo = 0;
		failedNo = 0;
	}

	@Override
	public void doJob(JobExecutionContext context)
	{
		SimpleDateFormat dateformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try
		{
			Calendar upload = GregorianCalendar.getInstance();
			Date uploadDate = upload.getTime();

			Calendar begining = GregorianCalendar.getInstance();
			Calendar end = GregorianCalendar.getInstance();

			begining.set(Calendar.MINUTE, 0);
			begining.set(Calendar.HOUR_OF_DAY, 0);
			begining.set(Calendar.SECOND, 0);
			Date beginDate = begining.getTime();

			end.add(Calendar.DATE, 1);
			end.set(Calendar.MINUTE, 0);
			end.set(Calendar.HOUR_OF_DAY, 0);
			end.set(Calendar.SECOND, 0);
			Date endDate = end.getTime();

			logger.info(String.format("Fetching companies on trial expiring between %s and %s", beginDate, endDate));

			String beginingOfDay = dateformatter.format(beginDate);
			String endOfDay = dateformatter.format(endDate);
			DataAccess da = DataAccess.getInstance();
			String sql = "SELECT "
					+ "user.ATTR_name as firstname, "
					+ "user.ATTR_surname as surname, "
					+ "user.ATTR_Headline as email, "
					+ "user.ATTR_mobileNumber as mobileNumber, "
					+ "company.ATTR_categoryName as companyName, "
					+ "company.ATTRDATE_trialPlanExpiry as trialExpireDate "
					+ "FROM elements_useraccounts user "
					+ "JOIN categories_users team, categories_users company "
					+ "WHERE "
					+ "company.ATTRDATE_trialPlanExpiry BETWEEN ? AND ? "
					+ "AND company.ATTRDROP_pricingPlan = 'TRIAL' "
					+ "AND user.ATTRDROP_userType = 'admin' "
					+ "AND team.Category_id=user.Category_id "
					+ "AND team.Category_ParentID=company.Category_id "
					+ "ORDER BY trialExpireDate";
			logger.info("SQL: " + sql);
			List<Hashtable<String, String>> users = da.select(sql,
					new String[] { beginingOfDay, endOfDay },
					HashtableMapper.getInstance());

			logger.info(String.format("Found %s companies with trial expiring today", users.size()));

			if (users.size() > 0)
			{
				//send report
				new EmailBuilder().prepareExpiredTrialReportEmail(users).addTo("darren.strong2@gmail.com").doSend();
			}

			for (Hashtable<String, String> record : users)
			{
				new EmailBuilder().prepareExpiredTrialEmail(record).addTo(record.get("email")).doSend();
			}

		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			new EmailBuilder().prepareErrorEmailForContext("Exception looking for companies with expired trial periods", e).doSend();
		}
	}

	@Override
	public int getTotal()
	{
		return total;
	}

	@Override
	public int getSuccessNo()
	{
		return successNo;
	}

	@Override
	public int getFailedNo()
	{
		// TODO Auto-generated method stub
		return 0;
	}


}
