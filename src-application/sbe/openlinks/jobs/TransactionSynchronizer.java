//package sbe.openlinks.jobs;
//
//import static au.corporateinteractive.qcloud.proposalbuilder.service.StripeService.calcGstPercent;
//import static au.corporateinteractive.qcloud.proposalbuilder.service.StripeService.parseDate;
//import static au.corporateinteractive.qcloud.proposalbuilder.service.StripeService.toDollar;
//
//import java.util.Date;
//import java.util.List;
//
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//import org.quartz.JobExecutionContext;
//
//import au.corporateinteractive.qcloud.proposalbuilder.db.BalanceTransactionDataAccess;
//import au.corporateinteractive.qcloud.proposalbuilder.service.StripeService;
//import au.net.webdirector.common.datalayer.base.db.DataMap;
//import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
//import au.net.webdirector.common.datalayer.client.CategoryData;
//import au.net.webdirector.common.datalayer.client.ElementData;
//import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
//
//import com.stripe.model.BalanceTransaction;
//import com.stripe.model.Charge;
//
//public class TransactionSynchronizer extends AbstractScheduledStatefulJob
//{
//	private static Logger logger = Logger.getLogger(TransactionSynchronizer.class);
//
//	@Override
//	public String getJobDescription()
//	{
//		return "Stripe: Sychronize Balance Transactions";
//	}
//
//	@Override
//	public void beforeJob(JobExecutionContext context)
//	{
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public void doJob(JobExecutionContext context)
//	{
//		try (TransactionManager txManager = TransactionManager.getInstance())
//		{
//			TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
//			StripeService service = StripeService.getInstance();
//			CategoryData unprocessed = ma.getCategoryByName(BalanceTransactionDataAccess.MODULE, "Unprocessed");
//			DataMap last = ma.getDataAccess().selectMap("select ifnull(max(ATTRDATE_Created), date('2017-01-01')) as created from elements_transactions");
//			Date since = last.getDate("created");
//			logger.info("retrieving transactions create after or at :" + since);
//			List<BalanceTransaction> transactions = service.retrieveStripeBalanceTransactions(since);
//			logger.info("found " + transactions.size() + " transactions");
//			for (BalanceTransaction tx : transactions)
//			{
//				ElementData trans = ma.getElementByName(BalanceTransactionDataAccess.MODULE, tx.getId());
//				if (trans != null)
//				{
//					logger.info("Processed");
//					continue;
//				}
//				Charge charge = service.retrieveStripeCharge(tx.getSource());
//				CategoryData company = service.findCompanyFromStripeId(charge.getCustomer());
//				trans = new ElementData();
//				trans.setParentId(unprocessed.getId());
//				trans.setLive(true);
//				trans.setName(tx.getId());
//				trans.put(BalanceTransactionDataAccess.E_AMOUNT, toDollar(tx.getAmount()));
//				trans.put(BalanceTransactionDataAccess.E_AVAILABLE_ON, parseDate(tx.getAvailableOn()));
//				trans.put(BalanceTransactionDataAccess.E_CREATED, parseDate(tx.getCreated()));
//				trans.put(BalanceTransactionDataAccess.E_CURRENCY, tx.getCurrency().toUpperCase());
//				trans.put(BalanceTransactionDataAccess.E_CUSTOMER_AMOUNT, toDollar(charge.getAmount()));
//				trans.put(BalanceTransactionDataAccess.E_CUSTOMER_CURRENCY, charge.getCurrency().toUpperCase());
//				trans.put(BalanceTransactionDataAccess.E_FEE, toDollar(tx.getFee()));
//				double net = toDollar(tx.getNet());
//				double gst = 0;
//
//				if (company != null)
//				{
//					trans.put(BalanceTransactionDataAccess.E_COMPANY_ID, company.getId());
//					trans.put(BalanceTransactionDataAccess.E_COMPANY_NAME, company.getName());
//					gst = net - net / (100 + calcGstPercent(company)) * 100;
//				}
//				else
//				{
//					trans.put(BalanceTransactionDataAccess.E_COMPANY_NAME, charge.getCustomer());
//					gst = StringUtils.equalsIgnoreCase(charge.getCurrency(), "AUD") ? net - net / 1.1 : 0;
//				}
//
//				trans.put(BalanceTransactionDataAccess.E_GST, gst);
//				trans.put(BalanceTransactionDataAccess.E_NET, net);
//				trans.put(BalanceTransactionDataAccess.E_SOURCE, tx.getSource());
//				ma.insertElement(trans, BalanceTransactionDataAccess.MODULE);
//			}
//			txManager.commit();
//			logger.info("Finished");
//		}
//		catch (Exception e)
//		{
//			logger.error("Error:", e);
//		}
//
//	}
//
//	@Override
//	public int getTotal()
//	{
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	@Override
//	public int getSuccessNo()
//	{
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	@Override
//	public int getFailedNo()
//	{
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//
//}
