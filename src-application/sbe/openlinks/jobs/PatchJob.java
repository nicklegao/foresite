//package sbe.openlinks.jobs;
//
//import java.util.List;
//
//import org.apache.log4j.Logger;
//import org.quartz.JobExecutionContext;
//
//import au.corporateinteractive.qcloud.proposalbuilder.db.SignaturesDataAccess;
//import au.net.webdirector.common.datalayer.admin.db.DataAccess;
//import au.net.webdirector.common.datalayer.base.db.mapper.ElementDataMapper;
//import au.net.webdirector.common.datalayer.client.ElementData;
//
///**
// * @author Sishir
// *
// */
//public class PatchJob extends AbstractScheduledJob
//{
//
//	private static Logger logger = Logger.getLogger(PatchJob.class);
//
//	int total;
//	int successNo;
//	int failedNo;
//
//	@Override
//	public String getJobDescription()
//	{
//		return "Fixing signature module structure";
//	}
//
//	@Override
//	public void beforeJob(JobExecutionContext context)
//	{
//		total = 0;
//		successNo = 0;
//		failedNo = 0;
//	}
//
//	@Override
//	public void doJob(JobExecutionContext context)
//	{
//		DataAccess.getInstance().updateData("delete from categories_signatures", null);
//		List<ElementData> signatureElements = DataAccess.getInstance().select("select * from elements_signatures", new ElementDataMapper());
//		total = signatureElements.size();
//		for (ElementData se : signatureElements)
//		{
//			try
//			{
//				String proposalId = se.getString(SignaturesDataAccess.E_PROPOSAL_ID);
//
//				int signatureCategoryId = new SignaturesDataAccess().getCategory(proposalId, se.getCreateDate(), SignaturesDataAccess.SIGNATURES_MODULE);
//
//				DataAccess.getInstance().updateData("update elements_signatures set category_id=? where ATTRDROP_proposalId=?", new String[] { String.valueOf(signatureCategoryId), proposalId });
//				successNo++;
//			}
//			catch (Exception e)
//			{
//				logger.error("Error", e);
//				failedNo++;
//			}
//
//		}
//	}
//
//	@Override
//	public int getTotal()
//	{
//		return total;
//	}
//
//	@Override
//	public int getSuccessNo()
//	{
//		return successNo;
//	}
//
//	@Override
//	public int getFailedNo()
//	{
//		return failedNo;
//	}
//
//}
