
package sbe.openlinks.jobs;

import static au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess.USER_MODULE;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;

import au.com.ci.sbe.util.ModuleVariables;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.WebdirectorDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.common.utils.email.PendingEmail;

public class IdleCustomerCleanupJob extends AbstractScheduledJob
{
	private static Logger logger = Logger.getLogger(IdleCustomerCleanupJob.class);

	int total;
	int successNo;
	int failedNo;

	@Override
	public String getJobDescription()
	{
		return "Cleanup Idle Customers";
	}

	@Override
	public void beforeJob(JobExecutionContext context)
	{
		total = 0;
		successNo = 0;
		failedNo = 0;
	}

	@Override
	public void doJob(JobExecutionContext context)
	{
		List<CategoryData> idle = selectIdleCustomers();
		for (CategoryData customer : idle)
		{
			try
			{
				sendDeletionAlertEmail(customer);
				TransactionDataAccess.getInstance().update("update categories_users set ATTRDATE_deletionAlertEmailSent = now() where category_id = ?", customer.getId());
			}
			catch (Exception e)
			{
				logger.error("Error:", e);
			}
		}
		List<CategoryData> dueDelete = selectDueDeleteCustomers();
		for (CategoryData customer : dueDelete)
		{
			try (TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true)))
			{
				TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
				new WebdirectorDataAccess().deleteCompanyCategoryFromModules(customer.getId(), ma);
				txManager.commit();
			}
			catch (Exception e)
			{
				logger.error("Error:", e);
			}
		}
	}


	private void sendDeletionAlertEmail(CategoryData customer) throws SQLException
	{
		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");
		new UserAccountDataAccess().getUsersByCompanyId(customer.getId());

		String[] receiverEmails = TransactionDataAccess.getInstance().selectStrings("select attr_headline from elements_useraccounts e, categories_users c where e.category_id = c.category_id and c.category_parentId = ?", customer.getId()).toArray(new String[] {});

		PendingEmail email = new EmailBuilder()
				.addModule(USER_MODULE, customer)
				.prepareAccountDeletionAlertEmail();
		if (receiverEmails.length == 0)
		{
			email.addTo(adminEmail.split(","));
		}
		else
		{
			email.addTo(receiverEmails);
			email.addBCC(adminEmail.split(","));
		}
		email.doSend();

	}

	private List<CategoryData> selectIdleCustomers()
	{
		try
		{
			List<CategoryData> companys = ModuleAccess.getInstance().getCategories(
					"select * from categories_users where "
							+ " attrdrop_pricingplan = 'trial' "
							+ " and datediff(now(), ATTRDATE_trialPlanExpiry) > 90 "
							+ " and (ATTRDATE_deletionAlertEmailSent is null or ATTRDATE_trialPlanExpiry > ATTRDATE_deletionAlertEmailSent) ");
			return companys;
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return new ArrayList<CategoryData>();
		}
	}

	private List<CategoryData> selectDueDeleteCustomers()
	{
		try
		{
			List<CategoryData> companys = ModuleAccess.getInstance().getCategories(
					"select * from categories_users where "
							+ " attrdrop_pricingplan = 'trial' "
							+ " and datediff(now(), ATTRDATE_trialPlanExpiry) > 90 "
							+ " and ATTRDATE_deletionAlertEmailSent > ATTRDATE_trialPlanExpiry "
							+ " and datediff(now(), ATTRDATE_deletionAlertEmailSent) > 14 ");
			return companys;
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return new ArrayList<CategoryData>();
		}
	}

	@Override
	public int getTotal()
	{
		return total;
	}

	@Override
	public int getSuccessNo()
	{
		return successNo;
	}

	@Override
	public int getFailedNo()
	{
		return failedNo;
	}


}
