package model;

import java.util.Date;

import tool.developer.model.CIEntity;
import tool.developer.anno.CIField;

public class SysModuleConfig implements CIEntity
{
	//USER_DEFINED_START 
	
	@CIField(db = "id")
	private Integer id;
	
	public void setId(Integer id)
	{
		this.id = id;
	}
	
	public Integer getId()
	{
		return id;
	}
	
	public SysModuleConfig withId(Integer id)
	{
		this.id = id;
		return this;
	}
	 
	
	@CIField(db = "internal_module_name")
	private String internalModuleName;
	
	public void setInternalModuleName(String internalModuleName)
	{
		this.internalModuleName = internalModuleName;
	}
	
	public String getInternalModuleName()
	{
		return internalModuleName;
	}
	
	public SysModuleConfig withInternalModuleName(String internalModuleName)
	{
		this.internalModuleName = internalModuleName;
		return this;
	}
	 
	
	@CIField(db = "client_module_name")
	private String clientModuleName;
	
	public void setClientModuleName(String clientModuleName)
	{
		this.clientModuleName = clientModuleName;
	}
	
	public String getClientModuleName()
	{
		return clientModuleName;
	}
	
	public SysModuleConfig withClientModuleName(String clientModuleName)
	{
		this.clientModuleName = clientModuleName;
		return this;
	}
	 
	
	@CIField(db = "levels")
	private Integer levels;
	
	public void setLevels(Integer levels)
	{
		this.levels = levels;
	}
	
	public Integer getLevels()
	{
		return levels;
	}
	
	public SysModuleConfig withLevels(Integer levels)
	{
		this.levels = levels;
		return this;
	}
	 
	
	@CIField(db = "asset_valid_script")
	private String assetValidScript;
	
	public void setAssetValidScript(String assetValidScript)
	{
		this.assetValidScript = assetValidScript;
	}
	
	public String getAssetValidScript()
	{
		return assetValidScript;
	}
	
	public SysModuleConfig withAssetValidScript(String assetValidScript)
	{
		this.assetValidScript = assetValidScript;
		return this;
	}
	 
	
	@CIField(db = "cate_valid_script")
	private String cateValidScript;
	
	public void setCateValidScript(String cateValidScript)
	{
		this.cateValidScript = cateValidScript;
	}
	
	public String getCateValidScript()
	{
		return cateValidScript;
	}
	
	public SysModuleConfig withCateValidScript(String cateValidScript)
	{
		this.cateValidScript = cateValidScript;
		return this;
	}
	 
	
	@CIField(db = "color")
	private String color;
	
	public void setColor(String color)
	{
		this.color = color;
	}
	
	public String getColor()
	{
		return color;
	}
	
	public SysModuleConfig withColor(String color)
	{
		this.color = color;
		return this;
	}
	 
	
	@CIField(db = "icon")
	private String icon;
	
	public void setIcon(String icon)
	{
		this.icon = icon;
	}
	
	public String getIcon()
	{
		return icon;
	}
	
	public SysModuleConfig withIcon(String icon)
	{
		this.icon = icon;
		return this;
	}
	 
	
	@CIField(db = "version_control_enabled")
	private Integer versionControlEnabled;
	
	public void setVersionControlEnabled(Integer versionControlEnabled)
	{
		this.versionControlEnabled = versionControlEnabled;
	}
	
	public Integer getVersionControlEnabled()
	{
		return versionControlEnabled;
	}
	
	public SysModuleConfig withVersionControlEnabled(Integer versionControlEnabled)
	{
		this.versionControlEnabled = versionControlEnabled;
		return this;
	}
	 
	
	@CIField(db = "workflow_enabled")
	private Integer workflowEnabled;
	
	public void setWorkflowEnabled(Integer workflowEnabled)
	{
		this.workflowEnabled = workflowEnabled;
	}
	
	public Integer getWorkflowEnabled()
	{
		return workflowEnabled;
	}
	
	public SysModuleConfig withWorkflowEnabled(Integer workflowEnabled)
	{
		this.workflowEnabled = workflowEnabled;
		return this;
	}
	 
	
	@CIField(db = "has_sort")
	private Integer hasSort;
	
	public void setHasSort(Integer hasSort)
	{
		this.hasSort = hasSort;
	}
	
	public Integer getHasSort()
	{
		return hasSort;
	}
	
	public SysModuleConfig withHasSort(Integer hasSort)
	{
		this.hasSort = hasSort;
		return this;
	}
	 
	
	@CIField(db = "has_copy")
	private Integer hasCopy;
	
	public void setHasCopy(Integer hasCopy)
	{
		this.hasCopy = hasCopy;
	}
	
	public Integer getHasCopy()
	{
		return hasCopy;
	}
	
	public SysModuleConfig withHasCopy(Integer hasCopy)
	{
		this.hasCopy = hasCopy;
		return this;
	}
	 
	
	@CIField(db = "has_cut")
	private Integer hasCut;
	
	public void setHasCut(Integer hasCut)
	{
		this.hasCut = hasCut;
	}
	
	public Integer getHasCut()
	{
		return hasCut;
	}
	
	public SysModuleConfig withHasCut(Integer hasCut)
	{
		this.hasCut = hasCut;
		return this;
	}
	 
	
	@CIField(db = "has_import")
	private Integer hasImport;
	
	public void setHasImport(Integer hasImport)
	{
		this.hasImport = hasImport;
	}
	
	public Integer getHasImport()
	{
		return hasImport;
	}
	
	public SysModuleConfig withHasImport(Integer hasImport)
	{
		this.hasImport = hasImport;
		return this;
	}
	 
	
	@CIField(db = "has_export")
	private Integer hasExport;
	
	public void setHasExport(Integer hasExport)
	{
		this.hasExport = hasExport;
	}
	
	public Integer getHasExport()
	{
		return hasExport;
	}
	
	public SysModuleConfig withHasExport(Integer hasExport)
	{
		this.hasExport = hasExport;
		return this;
	}
	 
	
	@CIField(db = "has_full_ckeditor")
	private Integer hasFullCkeditor;
	
	public void setHasFullCkeditor(Integer hasFullCkeditor)
	{
		this.hasFullCkeditor = hasFullCkeditor;
	}
	
	public Integer getHasFullCkeditor()
	{
		return hasFullCkeditor;
	}
	
	public SysModuleConfig withHasFullCkeditor(Integer hasFullCkeditor)
	{
		this.hasFullCkeditor = hasFullCkeditor;
		return this;
	}
	 
	
	@CIField(db = "has_form_components")
	private Integer hasFormComponents;
	
	public void setHasFormComponents(Integer hasFormComponents)
	{
		this.hasFormComponents = hasFormComponents;
	}
	
	public Integer getHasFormComponents()
	{
		return hasFormComponents;
	}
	
	public SysModuleConfig withHasFormComponents(Integer hasFormComponents)
	{
		this.hasFormComponents = hasFormComponents;
		return this;
	}
	 
	
	@CIField(db = "has_browse_server")
	private Integer hasBrowseServer;
	
	public void setHasBrowseServer(Integer hasBrowseServer)
	{
		this.hasBrowseServer = hasBrowseServer;
	}
	
	public Integer getHasBrowseServer()
	{
		return hasBrowseServer;
	}
	
	public SysModuleConfig withHasBrowseServer(Integer hasBrowseServer)
	{
		this.hasBrowseServer = hasBrowseServer;
		return this;
	}
	 
	
	@CIField(db = "customised_buttons")
	private String customisedButtons;
	
	public void setCustomisedButtons(String customisedButtons)
	{
		this.customisedButtons = customisedButtons;
	}
	
	public String getCustomisedButtons()
	{
		return customisedButtons;
	}
	
	public SysModuleConfig withCustomisedButtons(String customisedButtons)
	{
		this.customisedButtons = customisedButtons;
		return this;
	}
	//USER_DEFINED_END 
}