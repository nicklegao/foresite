package model;

import java.util.Date;

import tool.developer.model.CIElement;
import tool.developer.anno.CIField;

public class Users implements CIElement
{
	@CIField(db = "Live_date")
	private Date liveDate;
	
	@CIField(db = "Expire_date")
	private Date expireDate;
	
	@CIField(db = "Create_date")
	private Date createDate;

	@CIField(db = "LAST_UPDATE_DATE")
	private Date lastUpdateDate;

	@CIField(db = "Live")
	private Boolean live;

	@CIField(db = "Category_id")
	private String categoryId;
	
	@CIField(db = "display_order")
	private Integer dispayOrder;

	@CIField(db = "Element_id")
	private String elementId;

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

	public Users withCreateDate(Date createDate)
	{
		this.createDate = createDate;
		return this;
	}

	public Date getLastUpdateDate()
	{
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate)
	{
		this.lastUpdateDate = lastUpdateDate;
	}

	public Users withLastUpdateDate(Date lastUpdateDate)
	{
		this.lastUpdateDate = lastUpdateDate;
		return this;
	}

	public Boolean getLive()
	{
		return live;
	}

	public void setLive(Boolean live)
	{
		this.live = live;
	}

	public Users withLive(Boolean live)
	{
		this.live = live;
		return this;
	}

	public String getCategoryId()
	{
		return categoryId;
	}

	public void setCategoryId(String categoryId)
	{
		this.categoryId = categoryId;
	}

	public Users withCategoryId(String categoryId)
	{
		this.categoryId = categoryId;
		return this;
	}

	public String getElementId()
	{
		return elementId;
	}
	
	public void setElementId(String elementId)
	{
		this.elementId = elementId;
	}
	
	public Users withElementId(String elementId)
	{
		this.elementId = elementId;
		return this;
	}

	public Date getLiveDate()
	{
		return liveDate;
	}

	public void setLiveDate(Date liveDate)
	{
		this.liveDate = liveDate;
	}
	
	public Users withLiveDate(Date liveDate)
	{
		this.liveDate = liveDate;
		return this;
	}

	public Date getExpireDate()
	{
		return expireDate;
	}

	public void setExpireDate(Date expireDate)
	{
		this.expireDate = expireDate;
	}
	
	public Users withExpireDate(Date expireDate)
	{
		this.expireDate = expireDate;
		return this;
	}

	public Integer getDispayOrder()
	{
		return dispayOrder;
	}

	public void setDispayOrder(Integer dispayOrder)
	{
		this.dispayOrder = dispayOrder;
	}
	
	public Users withDispayOrder(Integer dispayOrder)
	{
		this.dispayOrder = dispayOrder;
		return this;
	}
	
	//USER_DEFINED_START 
	
	@CIField(db = "ATTR_Headline")
	private String headline;
	
	public void setHeadline(String headline)
	{
		this.headline = headline;
	}
	
	public String getHeadline()
	{
		return headline;
	}
	
	public Users withHeadline(String headline)
	{
		this.headline = headline;
		return this;
	}
	 
	
	@CIField(db = "ATTR_password")
	private String password;
	
	public void setPassword(String password)
	{
		this.password = password;
	}
	
	public String getPassword()
	{
		return password;
	}
	
	public Users withPassword(String password)
	{
		this.password = password;
		return this;
	}
	 
	
	@CIField(db = "ATTRDATE_passwordExpire")
	private Date passwordExpire;
	
	public void setPasswordExpire(Date passwordExpire)
	{
		this.passwordExpire = passwordExpire;
	}
	
	public Date getPasswordExpire()
	{
		return passwordExpire;
	}
	
	public Users withPasswordExpire(Date passwordExpire)
	{
		this.passwordExpire = passwordExpire;
		return this;
	}
	 
	
	@CIField(db = "ATTR_name")
	private String name;
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public Users withName(String name)
	{
		this.name = name;
		return this;
	}
	 
	
	@CIField(db = "ATTR_mobileNumber")
	private String mobileNumber;
	
	public void setMobileNumber(String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}
	
	public String getMobileNumber()
	{
		return mobileNumber;
	}
	
	public Users withMobileNumber(String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
		return this;
	}
	 
	
	@CIField(db = "ATTR_resetPassword")
	private String resetPassword;
	
	public void setResetPassword(String resetPassword)
	{
		this.resetPassword = resetPassword;
	}
	
	public String getResetPassword()
	{
		return resetPassword;
	}
	
	public Users withResetPassword(String resetPassword)
	{
		this.resetPassword = resetPassword;
		return this;
	}
	 
	
	@CIField(db = "ATTR_surname")
	private String surname;
	
	public void setSurname(String surname)
	{
		this.surname = surname;
	}
	
	public String getSurname()
	{
		return surname;
	}
	
	public Users withSurname(String surname)
	{
		this.surname = surname;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_userType")
	private String userType;
	
	public void setUserType(String userType)
	{
		this.userType = userType;
	}
	
	public String getUserType()
	{
		return userType;
	}
	
	public Users withUserType(String userType)
	{
		this.userType = userType;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_notifyDailyEmailSummary")
	private Boolean notifyDailyEmailSummary;
	
	public void setNotifyDailyEmailSummary(Boolean notifyDailyEmailSummary)
	{
		this.notifyDailyEmailSummary = notifyDailyEmailSummary;
	}
	
	public Boolean getNotifyDailyEmailSummary()
	{
		return notifyDailyEmailSummary;
	}
	
	public Users withNotifyDailyEmailSummary(Boolean notifyDailyEmailSummary)
	{
		this.notifyDailyEmailSummary = notifyDailyEmailSummary;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_notifyNewComment")
	private Boolean notifyNewComment;
	
	public void setNotifyNewComment(Boolean notifyNewComment)
	{
		this.notifyNewComment = notifyNewComment;
	}
	
	public Boolean getNotifyNewComment()
	{
		return notifyNewComment;
	}
	
	public Users withNotifyNewComment(Boolean notifyNewComment)
	{
		this.notifyNewComment = notifyNewComment;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_notifyHandover")
	private Boolean notifyHandover;
	
	public void setNotifyHandover(Boolean notifyHandover)
	{
		this.notifyHandover = notifyHandover;
	}
	
	public Boolean getNotifyHandover()
	{
		return notifyHandover;
	}
	
	public Users withNotifyHandover(Boolean notifyHandover)
	{
		this.notifyHandover = notifyHandover;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_notifyProposalStatusChange")
	private Boolean notifyProposalStatusChange;
	
	public void setNotifyProposalStatusChange(Boolean notifyProposalStatusChange)
	{
		this.notifyProposalStatusChange = notifyProposalStatusChange;
	}
	
	public Boolean getNotifyProposalStatusChange()
	{
		return notifyProposalStatusChange;
	}
	
	public Users withNotifyProposalStatusChange(Boolean notifyProposalStatusChange)
	{
		this.notifyProposalStatusChange = notifyProposalStatusChange;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_activated")
	private Boolean activated;
	
	public void setActivated(Boolean activated)
	{
		this.activated = activated;
	}
	
	public Boolean getActivated()
	{
		return activated;
	}
	
	public Users withActivated(Boolean activated)
	{
		this.activated = activated;
		return this;
	}
	 
	
	@CIField(db = "ATTR_maestranoUserId")
	private String maestranoUserId;
	
	public void setMaestranoUserId(String maestranoUserId)
	{
		this.maestranoUserId = maestranoUserId;
	}
	
	public String getMaestranoUserId()
	{
		return maestranoUserId;
	}
	
	public Users withMaestranoUserId(String maestranoUserId)
	{
		this.maestranoUserId = maestranoUserId;
		return this;
	}
	 
	
	@CIField(db = "ATTR_maestranoRealEmail")
	private String maestranoRealEmail;
	
	public void setMaestranoRealEmail(String maestranoRealEmail)
	{
		this.maestranoRealEmail = maestranoRealEmail;
	}
	
	public String getMaestranoRealEmail()
	{
		return maestranoRealEmail;
	}
	
	public Users withMaestranoRealEmail(String maestranoRealEmail)
	{
		this.maestranoRealEmail = maestranoRealEmail;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_notifyProposalOpened")
	private Boolean notifyProposalOpened;
	
	public void setNotifyProposalOpened(Boolean notifyProposalOpened)
	{
		this.notifyProposalOpened = notifyProposalOpened;
	}
	
	public Boolean getNotifyProposalOpened()
	{
		return notifyProposalOpened;
	}
	
	public Users withNotifyProposalOpened(Boolean notifyProposalOpened)
	{
		this.notifyProposalOpened = notifyProposalOpened;
		return this;
	}
	 
	
	@CIField(db = "ATTR_DashboardRecord")
	private String dashboardRecord;
	
	public void setDashboardRecord(String dashboardRecord)
	{
		this.dashboardRecord = dashboardRecord;
	}
	
	public String getDashboardRecord()
	{
		return dashboardRecord;
	}
	
	public Users withDashboardRecord(String dashboardRecord)
	{
		this.dashboardRecord = dashboardRecord;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_SocketConnected")
	private Boolean socketConnected;
	
	public void setSocketConnected(Boolean socketConnected)
	{
		this.socketConnected = socketConnected;
	}
	
	public Boolean getSocketConnected()
	{
		return socketConnected;
	}
	
	public Users withSocketConnected(Boolean socketConnected)
	{
		this.socketConnected = socketConnected;
		return this;
	}
	 
	
	@CIField(db = "ATTR_SocketSessionID")
	private String socketSessionID;
	
	public void setSocketSessionID(String socketSessionID)
	{
		this.socketSessionID = socketSessionID;
	}
	
	public String getSocketSessionID()
	{
		return socketSessionID;
	}
	
	public Users withSocketSessionID(String socketSessionID)
	{
		this.socketSessionID = socketSessionID;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_SupportAdmin")
	private Boolean supportAdmin;
	
	public void setSupportAdmin(Boolean supportAdmin)
	{
		this.supportAdmin = supportAdmin;
	}
	
	public Boolean getSupportAdmin()
	{
		return supportAdmin;
	}
	
	public Users withSupportAdmin(Boolean supportAdmin)
	{
		this.supportAdmin = supportAdmin;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_FirstAccessTutorial")
	private Boolean firstAccessTutorial;
	
	public void setFirstAccessTutorial(Boolean firstAccessTutorial)
	{
		this.firstAccessTutorial = firstAccessTutorial;
	}
	
	public Boolean getFirstAccessTutorial()
	{
		return firstAccessTutorial;
	}
	
	public Users withFirstAccessTutorial(Boolean firstAccessTutorial)
	{
		this.firstAccessTutorial = firstAccessTutorial;
		return this;
	}
	 
	
	@CIField(db = "ATTR_Web_Token")
	private String webToken;
	
	public void setWebToken(String webToken)
	{
		this.webToken = webToken;
	}
	
	public String getWebToken()
	{
		return webToken;
	}
	
	public Users withWebToken(String webToken)
	{
		this.webToken = webToken;
		return this;
	}
	 
	
	@CIField(db = "ATTRDATE_LastAccess")
	private Date lastAccess;
	
	public void setLastAccess(Date lastAccess)
	{
		this.lastAccess = lastAccess;
	}
	
	public Date getLastAccess()
	{
		return lastAccess;
	}
	
	public Users withLastAccess(Date lastAccess)
	{
		this.lastAccess = lastAccess;
		return this;
	}
	 
	
	@CIField(db = "ATTR_travelport_consultantId")
	private String travelportConsultantId;
	
	public void setTravelportConsultantId(String travelportConsultantId)
	{
		this.travelportConsultantId = travelportConsultantId;
	}
	
	public String getTravelportConsultantId()
	{
		return travelportConsultantId;
	}
	
	public Users withTravelportConsultantId(String travelportConsultantId)
	{
		this.travelportConsultantId = travelportConsultantId;
		return this;
	}
	 
	
	@CIField(db = "ATTR_travelport_consultantPassword")
	private String travelportConsultantPassword;
	
	public void setTravelportConsultantPassword(String travelportConsultantPassword)
	{
		this.travelportConsultantPassword = travelportConsultantPassword;
	}
	
	public String getTravelportConsultantPassword()
	{
		return travelportConsultantPassword;
	}
	
	public Users withTravelportConsultantPassword(String travelportConsultantPassword)
	{
		this.travelportConsultantPassword = travelportConsultantPassword;
		return this;
	}
	 
	
	@CIField(db = "ATTR_sabre_consultantId")
	private String sabreConsultantId;
	
	public void setSabreConsultantId(String sabreConsultantId)
	{
		this.sabreConsultantId = sabreConsultantId;
	}
	
	public String getSabreConsultantId()
	{
		return sabreConsultantId;
	}
	
	public Users withSabreConsultantId(String sabreConsultantId)
	{
		this.sabreConsultantId = sabreConsultantId;
		return this;
	}
	 
	
	@CIField(db = "ATTR_sabre_consultantPassword")
	private String sabreConsultantPassword;
	
	public void setSabreConsultantPassword(String sabreConsultantPassword)
	{
		this.sabreConsultantPassword = sabreConsultantPassword;
	}
	
	public String getSabreConsultantPassword()
	{
		return sabreConsultantPassword;
	}
	
	public Users withSabreConsultantPassword(String sabreConsultantPassword)
	{
		this.sabreConsultantPassword = sabreConsultantPassword;
		return this;
	}
	 
	
	@CIField(db = "ATTR_IOS_Token")
	private String iOSToken;
	
	public void setIOSToken(String iOSToken)
	{
		this.iOSToken = iOSToken;
	}
	
	public String getIOSToken()
	{
		return iOSToken;
	}
	
	public Users withIOSToken(String iOSToken)
	{
		this.iOSToken = iOSToken;
		return this;
	}
	 
	
	@CIField(db = "ATTRDATE_supportAuthTime")
	private Date supportAuthTime;
	
	public void setSupportAuthTime(Date supportAuthTime)
	{
		this.supportAuthTime = supportAuthTime;
	}
	
	public Date getSupportAuthTime()
	{
		return supportAuthTime;
	}
	
	public Users withSupportAuthTime(Date supportAuthTime)
	{
		this.supportAuthTime = supportAuthTime;
		return this;
	}
	//USER_DEFINED_END 

}