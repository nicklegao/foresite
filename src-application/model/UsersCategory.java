package model;

import java.util.Date;

import tool.developer.model.CICategory;
import tool.developer.anno.CIField;

public class UsersCategory implements CICategory
{
	@CIField(db = "Live_date")
	private Date liveDate;
	
	@CIField(db = "Expire_date")
	private Date expireDate;
	
	@CIField(db = "Create_date")
	private Date createDate;

	@CIField(db = "LAST_UPDATE_DATE")
	private Date lastUpdateDate;

	@CIField(db = "Live")
	private Boolean live;

	@CIField(db = "Category_id")
	private String categoryId;
	
	@CIField(db = "display_order")
	private Integer dispayOrder;

	@CIField(db = "Category_ParentId")
	private String categoryParentId;

	@CIField(db = "folderLevel")
	private Integer folderLevel;

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

	public UsersCategory withCreateDate(Date createDate)
	{
		this.createDate = createDate;
		return this;
	}

	public Date getLastUpdateDate()
	{
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate)
	{
		this.lastUpdateDate = lastUpdateDate;
	}

	public UsersCategory withLastUpdateDate(Date lastUpdateDate)
	{
		this.lastUpdateDate = lastUpdateDate;
		return this;
	}

	public Boolean getLive()
	{
		return live;
	}

	public void setLive(Boolean live)
	{
		this.live = live;
	}

	public UsersCategory withLive(Boolean live)
	{
		this.live = live;
		return this;
	}

	public String getCategoryId()
	{
		return categoryId;
	}

	public void setCategoryId(String categoryId)
	{
		this.categoryId = categoryId;
	}

	public UsersCategory withCategoryId(String categoryId)
	{
		this.categoryId = categoryId;
		return this;
	}

	public Date getLiveDate()
	{
		return liveDate;
	}

	public void setLiveDate(Date liveDate)
	{
		this.liveDate = liveDate;
	}
	
	public UsersCategory withLiveDate(Date liveDate)
	{
		this.liveDate = liveDate;
		return this;
	}

	public Date getExpireDate()
	{
		return expireDate;
	}

	public void setExpireDate(Date expireDate)
	{
		this.expireDate = expireDate;
	}
	
	public UsersCategory withExpireDate(Date expireDate)
	{
		this.expireDate = expireDate;
		return this;
	}

	public Integer getDispayOrder()
	{
		return dispayOrder;
	}

	public void setDispayOrder(Integer dispayOrder)
	{
		this.dispayOrder = dispayOrder;
	}
	
	public UsersCategory withDispayOrder(Integer dispayOrder)
	{
		this.dispayOrder = dispayOrder;
		return this;
	}

	public String getCategoryParentId()
	{
		return categoryParentId;
	}

	public void setCategoryParentId(String categoryParentId)
	{
		this.categoryParentId = categoryParentId;
	}
	
	public UsersCategory withCategoryParentId(String categoryParentId)
	{
		this.categoryParentId = categoryParentId;
		return this;
	}

	public Integer getFolderLevel()
	{
		return folderLevel;
	}

	public void setFolderLevel(Integer folderLevel)
	{
		this.folderLevel = folderLevel;
	}
	
	public UsersCategory withFolderLevel(Integer folderLevel)
	{
		this.folderLevel = folderLevel;
		return this;
	}
	//USER_DEFINED_START 
	
	@CIField(db = "ATTR_categoryName")
	private String categoryName;
	
	public void setCategoryName(String categoryName)
	{
		this.categoryName = categoryName;
	}
	
	public String getCategoryName()
	{
		return categoryName;
	}
	
	public UsersCategory withCategoryName(String categoryName)
	{
		this.categoryName = categoryName;
		return this;
	}
	 
	
	@CIField(db = "ATTR_state")
	private String state;
	
	public void setState(String state)
	{
		this.state = state;
	}
	
	public String getState()
	{
		return state;
	}
	
	public UsersCategory withState(String state)
	{
		this.state = state;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_bodyFont")
	private String bodyFont;
	
	public void setBodyFont(String bodyFont)
	{
		this.bodyFont = bodyFont;
	}
	
	public String getBodyFont()
	{
		return bodyFont;
	}
	
	public UsersCategory withBodyFont(String bodyFont)
	{
		this.bodyFont = bodyFont;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_h1Font")
	private String h1Font;
	
	public void setH1Font(String h1Font)
	{
		this.h1Font = h1Font;
	}
	
	public String getH1Font()
	{
		return h1Font;
	}
	
	public UsersCategory withH1Font(String h1Font)
	{
		this.h1Font = h1Font;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_h2Font")
	private String h2Font;
	
	public void setH2Font(String h2Font)
	{
		this.h2Font = h2Font;
	}
	
	public String getH2Font()
	{
		return h2Font;
	}
	
	public UsersCategory withH2Font(String h2Font)
	{
		this.h2Font = h2Font;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_H3Font")
	private String h3Font;
	
	public void setH3Font(String h3Font)
	{
		this.h3Font = h3Font;
	}
	
	public String getH3Font()
	{
		return h3Font;
	}
	
	public UsersCategory withH3Font(String h3Font)
	{
		this.h3Font = h3Font;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_h1Size")
	private Integer h1Size;
	
	public void setH1Size(Integer h1Size)
	{
		this.h1Size = h1Size;
	}
	
	public Integer getH1Size()
	{
		return h1Size;
	}
	
	public UsersCategory withH1Size(Integer h1Size)
	{
		this.h1Size = h1Size;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_h2Size")
	private Integer h2Size;
	
	public void setH2Size(Integer h2Size)
	{
		this.h2Size = h2Size;
	}
	
	public Integer getH2Size()
	{
		return h2Size;
	}
	
	public UsersCategory withH2Size(Integer h2Size)
	{
		this.h2Size = h2Size;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_bodySize")
	private Integer bodySize;
	
	public void setBodySize(Integer bodySize)
	{
		this.bodySize = bodySize;
	}
	
	public Integer getBodySize()
	{
		return bodySize;
	}
	
	public UsersCategory withBodySize(Integer bodySize)
	{
		this.bodySize = bodySize;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_h3Size")
	private Integer h3Size;
	
	public void setH3Size(Integer h3Size)
	{
		this.h3Size = h3Size;
	}
	
	public Integer getH3Size()
	{
		return h3Size;
	}
	
	public UsersCategory withH3Size(Integer h3Size)
	{
		this.h3Size = h3Size;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_pricingPlan")
	private String pricingPlan;
	
	public void setPricingPlan(String pricingPlan)
	{
		this.pricingPlan = pricingPlan;
	}
	
	public String getPricingPlan()
	{
		return pricingPlan;
	}
	
	public UsersCategory withPricingPlan(String pricingPlan)
	{
		this.pricingPlan = pricingPlan;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_country")
	private String country;
	
	public void setCountry(String country)
	{
		this.country = country;
	}
	
	public String getCountry()
	{
		return country;
	}
	
	public UsersCategory withCountry(String country)
	{
		this.country = country;
		return this;
	}
	 
	
	@CIField(db = "ATTR_palette1")
	private String palette1;
	
	public void setPalette1(String palette1)
	{
		this.palette1 = palette1;
	}
	
	public String getPalette1()
	{
		return palette1;
	}
	
	public UsersCategory withPalette1(String palette1)
	{
		this.palette1 = palette1;
		return this;
	}
	 
	
	@CIField(db = "ATTR_palette2")
	private String palette2;
	
	public void setPalette2(String palette2)
	{
		this.palette2 = palette2;
	}
	
	public String getPalette2()
	{
		return palette2;
	}
	
	public UsersCategory withPalette2(String palette2)
	{
		this.palette2 = palette2;
		return this;
	}
	 
	
	@CIField(db = "ATTR_palette3")
	private String palette3;
	
	public void setPalette3(String palette3)
	{
		this.palette3 = palette3;
	}
	
	public String getPalette3()
	{
		return palette3;
	}
	
	public UsersCategory withPalette3(String palette3)
	{
		this.palette3 = palette3;
		return this;
	}
	 
	
	@CIField(db = "ATTR_palette4")
	private String palette4;
	
	public void setPalette4(String palette4)
	{
		this.palette4 = palette4;
	}
	
	public String getPalette4()
	{
		return palette4;
	}
	
	public UsersCategory withPalette4(String palette4)
	{
		this.palette4 = palette4;
		return this;
	}
	 
	
	@CIField(db = "ATTRFILE_Logo")
	private String logo;
	
	public void setLogo(String logo)
	{
		this.logo = logo;
	}
	
	public String getLogo()
	{
		return logo;
	}
	
	public UsersCategory withLogo(String logo)
	{
		this.logo = logo;
		return this;
	}
	 
	
	@CIField(db = "ATTR_expiryDate")
	private String expiryDate;
	
	public void setExpiryDate(String expiryDate)
	{
		this.expiryDate = expiryDate;
	}
	
	public String getExpiryDate()
	{
		return expiryDate;
	}
	
	public UsersCategory withExpiryDate(String expiryDate)
	{
		this.expiryDate = expiryDate;
		return this;
	}
	 
	
	@CIField(db = "ATTR_backgroundColour")
	private String backgroundColour;
	
	public void setBackgroundColour(String backgroundColour)
	{
		this.backgroundColour = backgroundColour;
	}
	
	public String getBackgroundColour()
	{
		return backgroundColour;
	}
	
	public UsersCategory withBackgroundColour(String backgroundColour)
	{
		this.backgroundColour = backgroundColour;
		return this;
	}
	 
	
	@CIField(db = "ATTR_locale")
	private String locale;
	
	public void setLocale(String locale)
	{
		this.locale = locale;
	}
	
	public String getLocale()
	{
		return locale;
	}
	
	public UsersCategory withLocale(String locale)
	{
		this.locale = locale;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_crmContractId")
	private Integer crmContractId;
	
	public void setCrmContractId(Integer crmContractId)
	{
		this.crmContractId = crmContractId;
	}
	
	public Integer getCrmContractId()
	{
		return crmContractId;
	}
	
	public UsersCategory withCrmContractId(Integer crmContractId)
	{
		this.crmContractId = crmContractId;
		return this;
	}
	 
	
	@CIField(db = "ATTR_maestranoGroupId")
	private String maestranoGroupId;
	
	public void setMaestranoGroupId(String maestranoGroupId)
	{
		this.maestranoGroupId = maestranoGroupId;
	}
	
	public String getMaestranoGroupId()
	{
		return maestranoGroupId;
	}
	
	public UsersCategory withMaestranoGroupId(String maestranoGroupId)
	{
		this.maestranoGroupId = maestranoGroupId;
		return this;
	}
	 
	
	@CIField(db = "ATTRDATE_trialPlanExpiry")
	private Date trialPlanExpiry;
	
	public void setTrialPlanExpiry(Date trialPlanExpiry)
	{
		this.trialPlanExpiry = trialPlanExpiry;
	}
	
	public Date getTrialPlanExpiry()
	{
		return trialPlanExpiry;
	}
	
	public UsersCategory withTrialPlanExpiry(Date trialPlanExpiry)
	{
		this.trialPlanExpiry = trialPlanExpiry;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_sectionSize")
	private Integer sectionSize;
	
	public void setSectionSize(Integer sectionSize)
	{
		this.sectionSize = sectionSize;
	}
	
	public Integer getSectionSize()
	{
		return sectionSize;
	}
	
	public UsersCategory withSectionSize(Integer sectionSize)
	{
		this.sectionSize = sectionSize;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_sectionFont")
	private String sectionFont;
	
	public void setSectionFont(String sectionFont)
	{
		this.sectionFont = sectionFont;
	}
	
	public String getSectionFont()
	{
		return sectionFont;
	}
	
	public UsersCategory withSectionFont(String sectionFont)
	{
		this.sectionFont = sectionFont;
		return this;
	}
	 
	
	@CIField(db = "ATTR_orderNowRedirectLink")
	private String orderNowRedirectLink;
	
	public void setOrderNowRedirectLink(String orderNowRedirectLink)
	{
		this.orderNowRedirectLink = orderNowRedirectLink;
	}
	
	public String getOrderNowRedirectLink()
	{
		return orderNowRedirectLink;
	}
	
	public UsersCategory withOrderNowRedirectLink(String orderNowRedirectLink)
	{
		this.orderNowRedirectLink = orderNowRedirectLink;
		return this;
	}
	 
	
	@CIField(db = "ATTR_maestranoTenantKey")
	private String maestranoTenantKey;
	
	public void setMaestranoTenantKey(String maestranoTenantKey)
	{
		this.maestranoTenantKey = maestranoTenantKey;
	}
	
	public String getMaestranoTenantKey()
	{
		return maestranoTenantKey;
	}
	
	public UsersCategory withMaestranoTenantKey(String maestranoTenantKey)
	{
		this.maestranoTenantKey = maestranoTenantKey;
		return this;
	}
	 
	
	@CIField(db = "ATTR_apiId")
	private String apiId;
	
	public void setApiId(String apiId)
	{
		this.apiId = apiId;
	}
	
	public String getApiId()
	{
		return apiId;
	}
	
	public UsersCategory withApiId(String apiId)
	{
		this.apiId = apiId;
		return this;
	}
	 
	
	@CIField(db = "ATTR_apiKey")
	private String apiKey;
	
	public void setApiKey(String apiKey)
	{
		this.apiKey = apiKey;
	}
	
	public String getApiKey()
	{
		return apiKey;
	}
	
	public UsersCategory withApiKey(String apiKey)
	{
		this.apiKey = apiKey;
		return this;
	}
	 
	
	@CIField(db = "ATTR_sectionBackgroundColour")
	private String sectionBackgroundColour;
	
	public void setSectionBackgroundColour(String sectionBackgroundColour)
	{
		this.sectionBackgroundColour = sectionBackgroundColour;
	}
	
	public String getSectionBackgroundColour()
	{
		return sectionBackgroundColour;
	}
	
	public UsersCategory withSectionBackgroundColour(String sectionBackgroundColour)
	{
		this.sectionBackgroundColour = sectionBackgroundColour;
		return this;
	}
	 
	
	@CIField(db = "ATTR_bodyColor")
	private String bodyColor;
	
	public void setBodyColor(String bodyColor)
	{
		this.bodyColor = bodyColor;
	}
	
	public String getBodyColor()
	{
		return bodyColor;
	}
	
	public UsersCategory withBodyColor(String bodyColor)
	{
		this.bodyColor = bodyColor;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_cover_title_size")
	private Integer coverTitleSize;
	
	public void setCoverTitleSize(Integer coverTitleSize)
	{
		this.coverTitleSize = coverTitleSize;
	}
	
	public Integer getCoverTitleSize()
	{
		return coverTitleSize;
	}
	
	public UsersCategory withCoverTitleSize(Integer coverTitleSize)
	{
		this.coverTitleSize = coverTitleSize;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_cover_title_font")
	private String coverTitleFont;
	
	public void setCoverTitleFont(String coverTitleFont)
	{
		this.coverTitleFont = coverTitleFont;
	}
	
	public String getCoverTitleFont()
	{
		return coverTitleFont;
	}
	
	public UsersCategory withCoverTitleFont(String coverTitleFont)
	{
		this.coverTitleFont = coverTitleFont;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_cover_company_size")
	private Integer coverCompanySize;
	
	public void setCoverCompanySize(Integer coverCompanySize)
	{
		this.coverCompanySize = coverCompanySize;
	}
	
	public Integer getCoverCompanySize()
	{
		return coverCompanySize;
	}
	
	public UsersCategory withCoverCompanySize(Integer coverCompanySize)
	{
		this.coverCompanySize = coverCompanySize;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_cover_company_font")
	private String coverCompanyFont;
	
	public void setCoverCompanyFont(String coverCompanyFont)
	{
		this.coverCompanyFont = coverCompanyFont;
	}
	
	public String getCoverCompanyFont()
	{
		return coverCompanyFont;
	}
	
	public UsersCategory withCoverCompanyFont(String coverCompanyFont)
	{
		this.coverCompanyFont = coverCompanyFont;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_cover_extra_size")
	private Integer coverExtraSize;
	
	public void setCoverExtraSize(Integer coverExtraSize)
	{
		this.coverExtraSize = coverExtraSize;
	}
	
	public Integer getCoverExtraSize()
	{
		return coverExtraSize;
	}
	
	public UsersCategory withCoverExtraSize(Integer coverExtraSize)
	{
		this.coverExtraSize = coverExtraSize;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_cover_extra_font")
	private String coverExtraFont;
	
	public void setCoverExtraFont(String coverExtraFont)
	{
		this.coverExtraFont = coverExtraFont;
	}
	
	public String getCoverExtraFont()
	{
		return coverExtraFont;
	}
	
	public UsersCategory withCoverExtraFont(String coverExtraFont)
	{
		this.coverExtraFont = coverExtraFont;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_estimatedValue")
	private Boolean estimatedValue;
	
	public void setEstimatedValue(Boolean estimatedValue)
	{
		this.estimatedValue = estimatedValue;
	}
	
	public Boolean getEstimatedValue()
	{
		return estimatedValue;
	}
	
	public UsersCategory withEstimatedValue(Boolean estimatedValue)
	{
		this.estimatedValue = estimatedValue;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_BillingPeriod")
	private String billingPeriod;
	
	public void setBillingPeriod(String billingPeriod)
	{
		this.billingPeriod = billingPeriod;
	}
	
	public String getBillingPeriod()
	{
		return billingPeriod;
	}
	
	public UsersCategory withBillingPeriod(String billingPeriod)
	{
		this.billingPeriod = billingPeriod;
		return this;
	}
	 
	
	@CIField(db = "ATTRDATE_BillingStartDate")
	private Date billingStartDate;
	
	public void setBillingStartDate(Date billingStartDate)
	{
		this.billingStartDate = billingStartDate;
	}
	
	public Date getBillingStartDate()
	{
		return billingStartDate;
	}
	
	public UsersCategory withBillingStartDate(Date billingStartDate)
	{
		this.billingStartDate = billingStartDate;
		return this;
	}
	 
	
	@CIField(db = "ATTR_PreapprovalKey")
	private String preapprovalKey;
	
	public void setPreapprovalKey(String preapprovalKey)
	{
		this.preapprovalKey = preapprovalKey;
	}
	
	public String getPreapprovalKey()
	{
		return preapprovalKey;
	}
	
	public UsersCategory withPreapprovalKey(String preapprovalKey)
	{
		this.preapprovalKey = preapprovalKey;
		return this;
	}
	 
	
	@CIField(db = "ATTRDATE_LastBillingDate")
	private Date lastBillingDate;
	
	public void setLastBillingDate(Date lastBillingDate)
	{
		this.lastBillingDate = lastBillingDate;
	}
	
	public Date getLastBillingDate()
	{
		return lastBillingDate;
	}
	
	public UsersCategory withLastBillingDate(Date lastBillingDate)
	{
		this.lastBillingDate = lastBillingDate;
		return this;
	}
	 
	
	@CIField(db = "ATTRCURRENCY_CreditBalance")
	private Double creditBalance;
	
	public void setCreditBalance(Double creditBalance)
	{
		this.creditBalance = creditBalance;
	}
	
	public Double getCreditBalance()
	{
		return creditBalance;
	}
	
	public UsersCategory withCreditBalance(Double creditBalance)
	{
		this.creditBalance = creditBalance;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_roundingValue")
	private Boolean roundingValue;
	
	public void setRoundingValue(Boolean roundingValue)
	{
		this.roundingValue = roundingValue;
	}
	
	public Boolean getRoundingValue()
	{
		return roundingValue;
	}
	
	public UsersCategory withRoundingValue(Boolean roundingValue)
	{
		this.roundingValue = roundingValue;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_enableTax")
	private Boolean enableTax;
	
	public void setEnableTax(Boolean enableTax)
	{
		this.enableTax = enableTax;
	}
	
	public Boolean getEnableTax()
	{
		return enableTax;
	}
	
	public UsersCategory withEnableTax(Boolean enableTax)
	{
		this.enableTax = enableTax;
		return this;
	}
	 
	
	@CIField(db = "ATTR_taxLabel")
	private String taxLabel;
	
	public void setTaxLabel(String taxLabel)
	{
		this.taxLabel = taxLabel;
	}
	
	public String getTaxLabel()
	{
		return taxLabel;
	}
	
	public UsersCategory withTaxLabel(String taxLabel)
	{
		this.taxLabel = taxLabel;
		return this;
	}
	 
	
	@CIField(db = "ATTR_taxPercentage")
	private String taxPercentage;
	
	public void setTaxPercentage(String taxPercentage)
	{
		this.taxPercentage = taxPercentage;
	}
	
	public String getTaxPercentage()
	{
		return taxPercentage;
	}
	
	public UsersCategory withTaxPercentage(String taxPercentage)
	{
		this.taxPercentage = taxPercentage;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_AutoPayDisabled")
	private Boolean autoPayDisabled;
	
	public void setAutoPayDisabled(Boolean autoPayDisabled)
	{
		this.autoPayDisabled = autoPayDisabled;
	}
	
	public Boolean getAutoPayDisabled()
	{
		return autoPayDisabled;
	}
	
	public UsersCategory withAutoPayDisabled(Boolean autoPayDisabled)
	{
		this.autoPayDisabled = autoPayDisabled;
		return this;
	}
	 
	
	@CIField(db = "ATTR_pricingTemplateName")
	private String pricingTemplateName;
	
	public void setPricingTemplateName(String pricingTemplateName)
	{
		this.pricingTemplateName = pricingTemplateName;
	}
	
	public String getPricingTemplateName()
	{
		return pricingTemplateName;
	}
	
	public UsersCategory withPricingTemplateName(String pricingTemplateName)
	{
		this.pricingTemplateName = pricingTemplateName;
		return this;
	}
	 
	
	@CIField(db = "ATTR_orderNowLabel")
	private String orderNowLabel;
	
	public void setOrderNowLabel(String orderNowLabel)
	{
		this.orderNowLabel = orderNowLabel;
	}
	
	public String getOrderNowLabel()
	{
		return orderNowLabel;
	}
	
	public UsersCategory withOrderNowLabel(String orderNowLabel)
	{
		this.orderNowLabel = orderNowLabel;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_firstLogin")
	private Boolean firstLogin;
	
	public void setFirstLogin(Boolean firstLogin)
	{
		this.firstLogin = firstLogin;
	}
	
	public Boolean getFirstLogin()
	{
		return firstLogin;
	}
	
	public UsersCategory withFirstLogin(Boolean firstLogin)
	{
		this.firstLogin = firstLogin;
		return this;
	}
	 
	
	@CIField(db = "ATTR_StripeID")
	private String stripeID;
	
	public void setStripeID(String stripeID)
	{
		this.stripeID = stripeID;
	}
	
	public String getStripeID()
	{
		return stripeID;
	}
	
	public UsersCategory withStripeID(String stripeID)
	{
		this.stripeID = stripeID;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_tableStyle")
	private String tableStyle;
	
	public void setTableStyle(String tableStyle)
	{
		this.tableStyle = tableStyle;
	}
	
	public String getTableStyle()
	{
		return tableStyle;
	}
	
	public UsersCategory withTableStyle(String tableStyle)
	{
		this.tableStyle = tableStyle;
		return this;
	}
	 
	
	@CIField(db = "ATTR_DateFormat")
	private String dateFormat;
	
	public void setDateFormat(String dateFormat)
	{
		this.dateFormat = dateFormat;
	}
	
	public String getDateFormat()
	{
		return dateFormat;
	}
	
	public UsersCategory withDateFormat(String dateFormat)
	{
		this.dateFormat = dateFormat;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_tabStyle")
	private String tabStyle;
	
	public void setTabStyle(String tabStyle)
	{
		this.tabStyle = tabStyle;
	}
	
	public String getTabStyle()
	{
		return tabStyle;
	}
	
	public UsersCategory withTabStyle(String tabStyle)
	{
		this.tabStyle = tabStyle;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_showTabs")
	private Boolean showTabs;
	
	public void setShowTabs(Boolean showTabs)
	{
		this.showTabs = showTabs;
	}
	
	public Boolean getShowTabs()
	{
		return showTabs;
	}
	
	public UsersCategory withShowTabs(Boolean showTabs)
	{
		this.showTabs = showTabs;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_multiPricing")
	private Boolean multiPricing;
	
	public void setMultiPricing(Boolean multiPricing)
	{
		this.multiPricing = multiPricing;
	}
	
	public Boolean getMultiPricing()
	{
		return multiPricing;
	}
	
	public UsersCategory withMultiPricing(Boolean multiPricing)
	{
		this.multiPricing = multiPricing;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_hideCoverPage")
	private Boolean hideCoverPage;
	
	public void setHideCoverPage(Boolean hideCoverPage)
	{
		this.hideCoverPage = hideCoverPage;
	}
	
	public Boolean getHideCoverPage()
	{
		return hideCoverPage;
	}
	
	public UsersCategory withHideCoverPage(Boolean hideCoverPage)
	{
		this.hideCoverPage = hideCoverPage;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_hideCoverProposalInfo")
	private Boolean hideCoverProposalInfo;
	
	public void setHideCoverProposalInfo(Boolean hideCoverProposalInfo)
	{
		this.hideCoverProposalInfo = hideCoverProposalInfo;
	}
	
	public Boolean getHideCoverProposalInfo()
	{
		return hideCoverProposalInfo;
	}
	
	public UsersCategory withHideCoverProposalInfo(Boolean hideCoverProposalInfo)
	{
		this.hideCoverProposalInfo = hideCoverProposalInfo;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_hideCoverBoxBackground")
	private Boolean hideCoverBoxBackground;
	
	public void setHideCoverBoxBackground(Boolean hideCoverBoxBackground)
	{
		this.hideCoverBoxBackground = hideCoverBoxBackground;
	}
	
	public Boolean getHideCoverBoxBackground()
	{
		return hideCoverBoxBackground;
	}
	
	public UsersCategory withHideCoverBoxBackground(Boolean hideCoverBoxBackground)
	{
		this.hideCoverBoxBackground = hideCoverBoxBackground;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_hideCoverSenderInfo")
	private Boolean hideCoverSenderInfo;
	
	public void setHideCoverSenderInfo(Boolean hideCoverSenderInfo)
	{
		this.hideCoverSenderInfo = hideCoverSenderInfo;
	}
	
	public Boolean getHideCoverSenderInfo()
	{
		return hideCoverSenderInfo;
	}
	
	public UsersCategory withHideCoverSenderInfo(Boolean hideCoverSenderInfo)
	{
		this.hideCoverSenderInfo = hideCoverSenderInfo;
		return this;
	}
	 
	
	@CIField(db = "ATTR_coverFontColor")
	private String coverFontColor;
	
	public void setCoverFontColor(String coverFontColor)
	{
		this.coverFontColor = coverFontColor;
	}
	
	public String getCoverFontColor()
	{
		return coverFontColor;
	}
	
	public UsersCategory withCoverFontColor(String coverFontColor)
	{
		this.coverFontColor = coverFontColor;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_Image_library_activated")
	private Boolean imageLibraryActivated;
	
	public void setImageLibraryActivated(Boolean imageLibraryActivated)
	{
		this.imageLibraryActivated = imageLibraryActivated;
	}
	
	public Boolean getImageLibraryActivated()
	{
		return imageLibraryActivated;
	}
	
	public UsersCategory withImageLibraryActivated(Boolean imageLibraryActivated)
	{
		this.imageLibraryActivated = imageLibraryActivated;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_text_library_activated")
	private Boolean textLibraryActivated;
	
	public void setTextLibraryActivated(Boolean textLibraryActivated)
	{
		this.textLibraryActivated = textLibraryActivated;
	}
	
	public Boolean getTextLibraryActivated()
	{
		return textLibraryActivated;
	}
	
	public UsersCategory withTextLibraryActivated(Boolean textLibraryActivated)
	{
		this.textLibraryActivated = textLibraryActivated;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_video_library_activated")
	private Boolean videoLibraryActivated;
	
	public void setVideoLibraryActivated(Boolean videoLibraryActivated)
	{
		this.videoLibraryActivated = videoLibraryActivated;
	}
	
	public Boolean getVideoLibraryActivated()
	{
		return videoLibraryActivated;
	}
	
	public UsersCategory withVideoLibraryActivated(Boolean videoLibraryActivated)
	{
		this.videoLibraryActivated = videoLibraryActivated;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_products_library_activated")
	private Boolean productsLibraryActivated;
	
	public void setProductsLibraryActivated(Boolean productsLibraryActivated)
	{
		this.productsLibraryActivated = productsLibraryActivated;
	}
	
	public Boolean getProductsLibraryActivated()
	{
		return productsLibraryActivated;
	}
	
	public UsersCategory withProductsLibraryActivated(Boolean productsLibraryActivated)
	{
		this.productsLibraryActivated = productsLibraryActivated;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_pdf_library_activated")
	private Boolean pdfLibraryActivated;
	
	public void setPdfLibraryActivated(Boolean pdfLibraryActivated)
	{
		this.pdfLibraryActivated = pdfLibraryActivated;
	}
	
	public Boolean getPdfLibraryActivated()
	{
		return pdfLibraryActivated;
	}
	
	public UsersCategory withPdfLibraryActivated(Boolean pdfLibraryActivated)
	{
		this.pdfLibraryActivated = pdfLibraryActivated;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_coverpage_library_activated")
	private Boolean coverpageLibraryActivated;
	
	public void setCoverpageLibraryActivated(Boolean coverpageLibraryActivated)
	{
		this.coverpageLibraryActivated = coverpageLibraryActivated;
	}
	
	public Boolean getCoverpageLibraryActivated()
	{
		return coverpageLibraryActivated;
	}
	
	public UsersCategory withCoverpageLibraryActivated(Boolean coverpageLibraryActivated)
	{
		this.coverpageLibraryActivated = coverpageLibraryActivated;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_template_library_activated")
	private Boolean templateLibraryActivated;
	
	public void setTemplateLibraryActivated(Boolean templateLibraryActivated)
	{
		this.templateLibraryActivated = templateLibraryActivated;
	}
	
	public Boolean getTemplateLibraryActivated()
	{
		return templateLibraryActivated;
	}
	
	public UsersCategory withTemplateLibraryActivated(Boolean templateLibraryActivated)
	{
		this.templateLibraryActivated = templateLibraryActivated;
		return this;
	}
	 
	
	@CIField(db = "ATTR_timeZone")
	private String timeZone;
	
	public void setTimeZone(String timeZone)
	{
		this.timeZone = timeZone;
	}
	
	public String getTimeZone()
	{
		return timeZone;
	}
	
	public UsersCategory withTimeZone(String timeZone)
	{
		this.timeZone = timeZone;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_appendPDFDefault")
	private Boolean appendPDFDefault;
	
	public void setAppendPDFDefault(Boolean appendPDFDefault)
	{
		this.appendPDFDefault = appendPDFDefault;
	}
	
	public Boolean getAppendPDFDefault()
	{
		return appendPDFDefault;
	}
	
	public UsersCategory withAppendPDFDefault(Boolean appendPDFDefault)
	{
		this.appendPDFDefault = appendPDFDefault;
		return this;
	}
	 
	
	@CIField(db = "ATTR_textMappingFileName")
	private String textMappingFileName;
	
	public void setTextMappingFileName(String textMappingFileName)
	{
		this.textMappingFileName = textMappingFileName;
	}
	
	public String getTextMappingFileName()
	{
		return textMappingFileName;
	}
	
	public UsersCategory withTextMappingFileName(String textMappingFileName)
	{
		this.textMappingFileName = textMappingFileName;
		return this;
	}
	 
	
	@CIField(db = "ATTR_defaultEmailStatus")
	private String defaultEmailStatus;
	
	public void setDefaultEmailStatus(String defaultEmailStatus)
	{
		this.defaultEmailStatus = defaultEmailStatus;
	}
	
	public String getDefaultEmailStatus()
	{
		return defaultEmailStatus;
	}
	
	public UsersCategory withDefaultEmailStatus(String defaultEmailStatus)
	{
		this.defaultEmailStatus = defaultEmailStatus;
		return this;
	}
	 
	
	@CIField(db = "ATTR_emailStatusString")
	private String emailStatusString;
	
	public void setEmailStatusString(String emailStatusString)
	{
		this.emailStatusString = emailStatusString;
	}
	
	public String getEmailStatusString()
	{
		return emailStatusString;
	}
	
	public UsersCategory withEmailStatusString(String emailStatusString)
	{
		this.emailStatusString = emailStatusString;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_MaestranoSync")
	private Boolean maestranoSync;
	
	public void setMaestranoSync(Boolean maestranoSync)
	{
		this.maestranoSync = maestranoSync;
	}
	
	public Boolean getMaestranoSync()
	{
		return maestranoSync;
	}
	
	public UsersCategory withMaestranoSync(Boolean maestranoSync)
	{
		this.maestranoSync = maestranoSync;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_MaestranoQuoteSync")
	private Boolean maestranoQuoteSync;
	
	public void setMaestranoQuoteSync(Boolean maestranoQuoteSync)
	{
		this.maestranoQuoteSync = maestranoQuoteSync;
	}
	
	public Boolean getMaestranoQuoteSync()
	{
		return maestranoQuoteSync;
	}
	
	public UsersCategory withMaestranoQuoteSync(Boolean maestranoQuoteSync)
	{
		this.maestranoQuoteSync = maestranoQuoteSync;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_MaestranoPDFSync")
	private Boolean maestranoPDFSync;
	
	public void setMaestranoPDFSync(Boolean maestranoPDFSync)
	{
		this.maestranoPDFSync = maestranoPDFSync;
	}
	
	public Boolean getMaestranoPDFSync()
	{
		return maestranoPDFSync;
	}
	
	public UsersCategory withMaestranoPDFSync(Boolean maestranoPDFSync)
	{
		this.maestranoPDFSync = maestranoPDFSync;
		return this;
	}
	 
	
	@CIField(db = "ATTR_sabreAgencyCode")
	private String sabreAgencyCode;
	
	public void setSabreAgencyCode(String sabreAgencyCode)
	{
		this.sabreAgencyCode = sabreAgencyCode;
	}
	
	public String getSabreAgencyCode()
	{
		return sabreAgencyCode;
	}
	
	public UsersCategory withSabreAgencyCode(String sabreAgencyCode)
	{
		this.sabreAgencyCode = sabreAgencyCode;
		return this;
	}
	 
	
	@CIField(db = "ATTR_sabreUsername")
	private String sabreUsername;
	
	public void setSabreUsername(String sabreUsername)
	{
		this.sabreUsername = sabreUsername;
	}
	
	public String getSabreUsername()
	{
		return sabreUsername;
	}
	
	public UsersCategory withSabreUsername(String sabreUsername)
	{
		this.sabreUsername = sabreUsername;
		return this;
	}
	 
	
	@CIField(db = "ATTR_sabrePassword")
	private String sabrePassword;
	
	public void setSabrePassword(String sabrePassword)
	{
		this.sabrePassword = sabrePassword;
	}
	
	public String getSabrePassword()
	{
		return sabrePassword;
	}
	
	public UsersCategory withSabrePassword(String sabrePassword)
	{
		this.sabrePassword = sabrePassword;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_teamCanEdit")
	private Boolean teamCanEdit;
	
	public void setTeamCanEdit(Boolean teamCanEdit)
	{
		this.teamCanEdit = teamCanEdit;
	}
	
	public Boolean getTeamCanEdit()
	{
		return teamCanEdit;
	}
	
	public UsersCategory withTeamCanEdit(Boolean teamCanEdit)
	{
		this.teamCanEdit = teamCanEdit;
		return this;
	}
	 
	
	@CIField(db = "ATTR_travelport_branchCode")
	private String travelportBranchCode;
	
	public void setTravelportBranchCode(String travelportBranchCode)
	{
		this.travelportBranchCode = travelportBranchCode;
	}
	
	public String getTravelportBranchCode()
	{
		return travelportBranchCode;
	}
	
	public UsersCategory withTravelportBranchCode(String travelportBranchCode)
	{
		this.travelportBranchCode = travelportBranchCode;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_hideTermsAlways")
	private Boolean hideTermsAlways;
	
	public void setHideTermsAlways(Boolean hideTermsAlways)
	{
		this.hideTermsAlways = hideTermsAlways;
	}
	
	public Boolean getHideTermsAlways()
	{
		return hideTermsAlways;
	}
	
	public UsersCategory withHideTermsAlways(Boolean hideTermsAlways)
	{
		this.hideTermsAlways = hideTermsAlways;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_proposalTemplateId")
	private String proposalTemplateId;
	
	public void setProposalTemplateId(String proposalTemplateId)
	{
		this.proposalTemplateId = proposalTemplateId;
	}
	
	public String getProposalTemplateId()
	{
		return proposalTemplateId;
	}
	
	public UsersCategory withProposalTemplateId(String proposalTemplateId)
	{
		this.proposalTemplateId = proposalTemplateId;
		return this;
	}
	 
	
	@CIField(db = "ATTR_pricingTitle")
	private String pricingTitle;
	
	public void setPricingTitle(String pricingTitle)
	{
		this.pricingTitle = pricingTitle;
	}
	
	public String getPricingTitle()
	{
		return pricingTitle;
	}
	
	public UsersCategory withPricingTitle(String pricingTitle)
	{
		this.pricingTitle = pricingTitle;
		return this;
	}
	 
	
	@CIField(db = "ATTR_arrivalguide_api_key")
	private String arrivalguideApiKey;
	
	public void setArrivalguideApiKey(String arrivalguideApiKey)
	{
		this.arrivalguideApiKey = arrivalguideApiKey;
	}
	
	public String getArrivalguideApiKey()
	{
		return arrivalguideApiKey;
	}
	
	public UsersCategory withArrivalguideApiKey(String arrivalguideApiKey)
	{
		this.arrivalguideApiKey = arrivalguideApiKey;
		return this;
	}
	 
	
	@CIField(db = "ATTR_sectionHeaderColor")
	private String sectionHeaderColor;
	
	public void setSectionHeaderColor(String sectionHeaderColor)
	{
		this.sectionHeaderColor = sectionHeaderColor;
	}
	
	public String getSectionHeaderColor()
	{
		return sectionHeaderColor;
	}
	
	public UsersCategory withSectionHeaderColor(String sectionHeaderColor)
	{
		this.sectionHeaderColor = sectionHeaderColor;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_sectionUppercase")
	private Boolean sectionUppercase;
	
	public void setSectionUppercase(Boolean sectionUppercase)
	{
		this.sectionUppercase = sectionUppercase;
	}
	
	public Boolean getSectionUppercase()
	{
		return sectionUppercase;
	}
	
	public UsersCategory withSectionUppercase(Boolean sectionUppercase)
	{
		this.sectionUppercase = sectionUppercase;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_sectionUnderline")
	private Boolean sectionUnderline;
	
	public void setSectionUnderline(Boolean sectionUnderline)
	{
		this.sectionUnderline = sectionUnderline;
	}
	
	public Boolean getSectionUnderline()
	{
		return sectionUnderline;
	}
	
	public UsersCategory withSectionUnderline(Boolean sectionUnderline)
	{
		this.sectionUnderline = sectionUnderline;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_sectionBold")
	private Boolean sectionBold;
	
	public void setSectionBold(Boolean sectionBold)
	{
		this.sectionBold = sectionBold;
	}
	
	public Boolean getSectionBold()
	{
		return sectionBold;
	}
	
	public UsersCategory withSectionBold(Boolean sectionBold)
	{
		this.sectionBold = sectionBold;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_sectionCustomFontColor")
	private Boolean sectionCustomFontColor;
	
	public void setSectionCustomFontColor(Boolean sectionCustomFontColor)
	{
		this.sectionCustomFontColor = sectionCustomFontColor;
	}
	
	public Boolean getSectionCustomFontColor()
	{
		return sectionCustomFontColor;
	}
	
	public UsersCategory withSectionCustomFontColor(Boolean sectionCustomFontColor)
	{
		this.sectionCustomFontColor = sectionCustomFontColor;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_h1Uppercase")
	private Boolean h1Uppercase;
	
	public void setH1Uppercase(Boolean h1Uppercase)
	{
		this.h1Uppercase = h1Uppercase;
	}
	
	public Boolean getH1Uppercase()
	{
		return h1Uppercase;
	}
	
	public UsersCategory withH1Uppercase(Boolean h1Uppercase)
	{
		this.h1Uppercase = h1Uppercase;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_h1Underline")
	private Boolean h1Underline;
	
	public void setH1Underline(Boolean h1Underline)
	{
		this.h1Underline = h1Underline;
	}
	
	public Boolean getH1Underline()
	{
		return h1Underline;
	}
	
	public UsersCategory withH1Underline(Boolean h1Underline)
	{
		this.h1Underline = h1Underline;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_h1Bold")
	private Boolean h1Bold;
	
	public void setH1Bold(Boolean h1Bold)
	{
		this.h1Bold = h1Bold;
	}
	
	public Boolean getH1Bold()
	{
		return h1Bold;
	}
	
	public UsersCategory withH1Bold(Boolean h1Bold)
	{
		this.h1Bold = h1Bold;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_h1CustomFontColor")
	private Boolean h1CustomFontColor;
	
	public void setH1CustomFontColor(Boolean h1CustomFontColor)
	{
		this.h1CustomFontColor = h1CustomFontColor;
	}
	
	public Boolean getH1CustomFontColor()
	{
		return h1CustomFontColor;
	}
	
	public UsersCategory withH1CustomFontColor(Boolean h1CustomFontColor)
	{
		this.h1CustomFontColor = h1CustomFontColor;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_h2Uppercase")
	private Boolean h2Uppercase;
	
	public void setH2Uppercase(Boolean h2Uppercase)
	{
		this.h2Uppercase = h2Uppercase;
	}
	
	public Boolean getH2Uppercase()
	{
		return h2Uppercase;
	}
	
	public UsersCategory withH2Uppercase(Boolean h2Uppercase)
	{
		this.h2Uppercase = h2Uppercase;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_h2Underline")
	private Boolean h2Underline;
	
	public void setH2Underline(Boolean h2Underline)
	{
		this.h2Underline = h2Underline;
	}
	
	public Boolean getH2Underline()
	{
		return h2Underline;
	}
	
	public UsersCategory withH2Underline(Boolean h2Underline)
	{
		this.h2Underline = h2Underline;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_h2Bold")
	private Boolean h2Bold;
	
	public void setH2Bold(Boolean h2Bold)
	{
		this.h2Bold = h2Bold;
	}
	
	public Boolean getH2Bold()
	{
		return h2Bold;
	}
	
	public UsersCategory withH2Bold(Boolean h2Bold)
	{
		this.h2Bold = h2Bold;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_h2CustomFontColor")
	private Boolean h2CustomFontColor;
	
	public void setH2CustomFontColor(Boolean h2CustomFontColor)
	{
		this.h2CustomFontColor = h2CustomFontColor;
	}
	
	public Boolean getH2CustomFontColor()
	{
		return h2CustomFontColor;
	}
	
	public UsersCategory withH2CustomFontColor(Boolean h2CustomFontColor)
	{
		this.h2CustomFontColor = h2CustomFontColor;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_h3Uppercase")
	private Boolean h3Uppercase;
	
	public void setH3Uppercase(Boolean h3Uppercase)
	{
		this.h3Uppercase = h3Uppercase;
	}
	
	public Boolean getH3Uppercase()
	{
		return h3Uppercase;
	}
	
	public UsersCategory withH3Uppercase(Boolean h3Uppercase)
	{
		this.h3Uppercase = h3Uppercase;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_h3Underline")
	private Boolean h3Underline;
	
	public void setH3Underline(Boolean h3Underline)
	{
		this.h3Underline = h3Underline;
	}
	
	public Boolean getH3Underline()
	{
		return h3Underline;
	}
	
	public UsersCategory withH3Underline(Boolean h3Underline)
	{
		this.h3Underline = h3Underline;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_h3Bold")
	private Boolean h3Bold;
	
	public void setH3Bold(Boolean h3Bold)
	{
		this.h3Bold = h3Bold;
	}
	
	public Boolean getH3Bold()
	{
		return h3Bold;
	}
	
	public UsersCategory withH3Bold(Boolean h3Bold)
	{
		this.h3Bold = h3Bold;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_h3CustomFontColor")
	private Boolean h3CustomFontColor;
	
	public void setH3CustomFontColor(Boolean h3CustomFontColor)
	{
		this.h3CustomFontColor = h3CustomFontColor;
	}
	
	public Boolean getH3CustomFontColor()
	{
		return h3CustomFontColor;
	}
	
	public UsersCategory withH3CustomFontColor(Boolean h3CustomFontColor)
	{
		this.h3CustomFontColor = h3CustomFontColor;
		return this;
	}
	 
	
	@CIField(db = "ATTR_h1HeaderColor")
	private String h1HeaderColor;
	
	public void setH1HeaderColor(String h1HeaderColor)
	{
		this.h1HeaderColor = h1HeaderColor;
	}
	
	public String getH1HeaderColor()
	{
		return h1HeaderColor;
	}
	
	public UsersCategory withH1HeaderColor(String h1HeaderColor)
	{
		this.h1HeaderColor = h1HeaderColor;
		return this;
	}
	 
	
	@CIField(db = "ATTR_h2HeaderColor")
	private String h2HeaderColor;
	
	public void setH2HeaderColor(String h2HeaderColor)
	{
		this.h2HeaderColor = h2HeaderColor;
	}
	
	public String getH2HeaderColor()
	{
		return h2HeaderColor;
	}
	
	public UsersCategory withH2HeaderColor(String h2HeaderColor)
	{
		this.h2HeaderColor = h2HeaderColor;
		return this;
	}
	 
	
	@CIField(db = "ATTR_h3HeaderColor")
	private String h3HeaderColor;
	
	public void setH3HeaderColor(String h3HeaderColor)
	{
		this.h3HeaderColor = h3HeaderColor;
	}
	
	public String getH3HeaderColor()
	{
		return h3HeaderColor;
	}
	
	public UsersCategory withH3HeaderColor(String h3HeaderColor)
	{
		this.h3HeaderColor = h3HeaderColor;
		return this;
	}
	 
	
	@CIField(db = "ATTR_tableStyleLine")
	private String tableStyleLine;
	
	public void setTableStyleLine(String tableStyleLine)
	{
		this.tableStyleLine = tableStyleLine;
	}
	
	public String getTableStyleLine()
	{
		return tableStyleLine;
	}
	
	public UsersCategory withTableStyleLine(String tableStyleLine)
	{
		this.tableStyleLine = tableStyleLine;
		return this;
	}
	 
	
	@CIField(db = "ATTR_tableStyleColor")
	private String tableStyleColor;
	
	public void setTableStyleColor(String tableStyleColor)
	{
		this.tableStyleColor = tableStyleColor;
	}
	
	public String getTableStyleColor()
	{
		return tableStyleColor;
	}
	
	public UsersCategory withTableStyleColor(String tableStyleColor)
	{
		this.tableStyleColor = tableStyleColor;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_tableStyleWeight")
	private Integer tableStyleWeight;
	
	public void setTableStyleWeight(Integer tableStyleWeight)
	{
		this.tableStyleWeight = tableStyleWeight;
	}
	
	public Integer getTableStyleWeight()
	{
		return tableStyleWeight;
	}
	
	public UsersCategory withTableStyleWeight(Integer tableStyleWeight)
	{
		this.tableStyleWeight = tableStyleWeight;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_autoSendItinerary")
	private Boolean autoSendItinerary;
	
	public void setAutoSendItinerary(Boolean autoSendItinerary)
	{
		this.autoSendItinerary = autoSendItinerary;
	}
	
	public Boolean getAutoSendItinerary()
	{
		return autoSendItinerary;
	}
	
	public UsersCategory withAutoSendItinerary(Boolean autoSendItinerary)
	{
		this.autoSendItinerary = autoSendItinerary;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_enableAddition")
	private Boolean enableAddition;
	
	public void setEnableAddition(Boolean enableAddition)
	{
		this.enableAddition = enableAddition;
	}
	
	public Boolean getEnableAddition()
	{
		return enableAddition;
	}
	
	public UsersCategory withEnableAddition(Boolean enableAddition)
	{
		this.enableAddition = enableAddition;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_AddUserPlanQty")
	private Integer addUserPlanQty;
	
	public void setAddUserPlanQty(Integer addUserPlanQty)
	{
		this.addUserPlanQty = addUserPlanQty;
	}
	
	public Integer getAddUserPlanQty()
	{
		return addUserPlanQty;
	}
	
	public UsersCategory withAddUserPlanQty(Integer addUserPlanQty)
	{
		this.addUserPlanQty = addUserPlanQty;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_AddDataPlanQty")
	private Integer addDataPlanQty;
	
	public void setAddDataPlanQty(Integer addDataPlanQty)
	{
		this.addDataPlanQty = addDataPlanQty;
	}
	
	public Integer getAddDataPlanQty()
	{
		return addDataPlanQty;
	}
	
	public UsersCategory withAddDataPlanQty(Integer addDataPlanQty)
	{
		this.addDataPlanQty = addDataPlanQty;
		return this;
	}
	 
	
	@CIField(db = "ATTR_StripePK")
	private String stripePK;
	
	public void setStripePK(String stripePK)
	{
		this.stripePK = stripePK;
	}
	
	public String getStripePK()
	{
		return stripePK;
	}
	
	public UsersCategory withStripePK(String stripePK)
	{
		this.stripePK = stripePK;
		return this;
	}
	 
	
	@CIField(db = "ATTR_StripeSK")
	private String stripeSK;
	
	public void setStripeSK(String stripeSK)
	{
		this.stripeSK = stripeSK;
	}
	
	public String getStripeSK()
	{
		return stripeSK;
	}
	
	public UsersCategory withStripeSK(String stripeSK)
	{
		this.stripeSK = stripeSK;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_spreadsheet_library_activated")
	private Boolean spreadsheetLibraryActivated;
	
	public void setSpreadsheetLibraryActivated(Boolean spreadsheetLibraryActivated)
	{
		this.spreadsheetLibraryActivated = spreadsheetLibraryActivated;
	}
	
	public Boolean getSpreadsheetLibraryActivated()
	{
		return spreadsheetLibraryActivated;
	}
	
	public UsersCategory withSpreadsheetLibraryActivated(Boolean spreadsheetLibraryActivated)
	{
		this.spreadsheetLibraryActivated = spreadsheetLibraryActivated;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_contentPaddingTop")
	private Integer contentPaddingTop;
	
	public void setContentPaddingTop(Integer contentPaddingTop)
	{
		this.contentPaddingTop = contentPaddingTop;
	}
	
	public Integer getContentPaddingTop()
	{
		return contentPaddingTop;
	}
	
	public UsersCategory withContentPaddingTop(Integer contentPaddingTop)
	{
		this.contentPaddingTop = contentPaddingTop;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_contentPaddingBottom")
	private Integer contentPaddingBottom;
	
	public void setContentPaddingBottom(Integer contentPaddingBottom)
	{
		this.contentPaddingBottom = contentPaddingBottom;
	}
	
	public Integer getContentPaddingBottom()
	{
		return contentPaddingBottom;
	}
	
	public UsersCategory withContentPaddingBottom(Integer contentPaddingBottom)
	{
		this.contentPaddingBottom = contentPaddingBottom;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_contentPaddingRight")
	private Integer contentPaddingRight;
	
	public void setContentPaddingRight(Integer contentPaddingRight)
	{
		this.contentPaddingRight = contentPaddingRight;
	}
	
	public Integer getContentPaddingRight()
	{
		return contentPaddingRight;
	}
	
	public UsersCategory withContentPaddingRight(Integer contentPaddingRight)
	{
		this.contentPaddingRight = contentPaddingRight;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_contentPaddingLeft")
	private Integer contentPaddingLeft;
	
	public void setContentPaddingLeft(Integer contentPaddingLeft)
	{
		this.contentPaddingLeft = contentPaddingLeft;
	}
	
	public Integer getContentPaddingLeft()
	{
		return contentPaddingLeft;
	}
	
	public UsersCategory withContentPaddingLeft(Integer contentPaddingLeft)
	{
		this.contentPaddingLeft = contentPaddingLeft;
		return this;
	}
	 
	
	@CIField(db = "ATTR_addressLine1")
	private String addressLine1;
	
	public void setAddressLine1(String addressLine1)
	{
		this.addressLine1 = addressLine1;
	}
	
	public String getAddressLine1()
	{
		return addressLine1;
	}
	
	public UsersCategory withAddressLine1(String addressLine1)
	{
		this.addressLine1 = addressLine1;
		return this;
	}
	 
	
	@CIField(db = "ATTR_addressLine2")
	private String addressLine2;
	
	public void setAddressLine2(String addressLine2)
	{
		this.addressLine2 = addressLine2;
	}
	
	public String getAddressLine2()
	{
		return addressLine2;
	}
	
	public UsersCategory withAddressLine2(String addressLine2)
	{
		this.addressLine2 = addressLine2;
		return this;
	}
	 
	
	@CIField(db = "ATTR_citySuburb")
	private String citySuburb;
	
	public void setCitySuburb(String citySuburb)
	{
		this.citySuburb = citySuburb;
	}
	
	public String getCitySuburb()
	{
		return citySuburb;
	}
	
	public UsersCategory withCitySuburb(String citySuburb)
	{
		this.citySuburb = citySuburb;
		return this;
	}
	 
	
	@CIField(db = "ATTR_postcode")
	private String postcode;
	
	public void setPostcode(String postcode)
	{
		this.postcode = postcode;
	}
	
	public String getPostcode()
	{
		return postcode;
	}
	
	public UsersCategory withPostcode(String postcode)
	{
		this.postcode = postcode;
		return this;
	}
	 
	
	@CIField(db = "ATTR_abnacn")
	private String abnacn;
	
	public void setAbnacn(String abnacn)
	{
		this.abnacn = abnacn;
	}
	
	public String getAbnacn()
	{
		return abnacn;
	}
	
	public UsersCategory withAbnacn(String abnacn)
	{
		this.abnacn = abnacn;
		return this;
	}
	 
	
	@CIField(db = "ATTRFILE_sectionBackgroundImage")
	private String sectionBackgroundImage;
	
	public void setSectionBackgroundImage(String sectionBackgroundImage)
	{
		this.sectionBackgroundImage = sectionBackgroundImage;
	}
	
	public String getSectionBackgroundImage()
	{
		return sectionBackgroundImage;
	}
	
	public UsersCategory withSectionBackgroundImage(String sectionBackgroundImage)
	{
		this.sectionBackgroundImage = sectionBackgroundImage;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_Status")
	private String status;
	
	public void setStatus(String status)
	{
		this.status = status;
	}
	
	public String getStatus()
	{
		return status;
	}
	
	public UsersCategory withStatus(String status)
	{
		this.status = status;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_Reseller")
	private String reseller;
	
	public void setReseller(String reseller)
	{
		this.reseller = reseller;
	}
	
	public String getReseller()
	{
		return reseller;
	}
	
	public UsersCategory withReseller(String reseller)
	{
		this.reseller = reseller;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_Activated")
	private Boolean activated;
	
	public void setActivated(Boolean activated)
	{
		this.activated = activated;
	}
	
	public Boolean getActivated()
	{
		return activated;
	}
	
	public UsersCategory withActivated(Boolean activated)
	{
		this.activated = activated;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_passwordExpiryDate")
	private Integer passwordExpiryDate;
	
	public void setPasswordExpiryDate(Integer passwordExpiryDate)
	{
		this.passwordExpiryDate = passwordExpiryDate;
	}
	
	public Integer getPasswordExpiryDate()
	{
		return passwordExpiryDate;
	}
	
	public UsersCategory withPasswordExpiryDate(Integer passwordExpiryDate)
	{
		this.passwordExpiryDate = passwordExpiryDate;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_itineraryTemplateId")
	private String itineraryTemplateId;
	
	public void setItineraryTemplateId(String itineraryTemplateId)
	{
		this.itineraryTemplateId = itineraryTemplateId;
	}
	
	public String getItineraryTemplateId()
	{
		return itineraryTemplateId;
	}
	
	public UsersCategory withItineraryTemplateId(String itineraryTemplateId)
	{
		this.itineraryTemplateId = itineraryTemplateId;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_HeaderTopActive")
	private Boolean headerTopActive;
	
	public void setHeaderTopActive(Boolean headerTopActive)
	{
		this.headerTopActive = headerTopActive;
	}
	
	public Boolean getHeaderTopActive()
	{
		return headerTopActive;
	}
	
	public UsersCategory withHeaderTopActive(Boolean headerTopActive)
	{
		this.headerTopActive = headerTopActive;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_HeaderBottomActive")
	private Boolean headerBottomActive;
	
	public void setHeaderBottomActive(Boolean headerBottomActive)
	{
		this.headerBottomActive = headerBottomActive;
	}
	
	public Boolean getHeaderBottomActive()
	{
		return headerBottomActive;
	}
	
	public UsersCategory withHeaderBottomActive(Boolean headerBottomActive)
	{
		this.headerBottomActive = headerBottomActive;
		return this;
	}
	 
	
	@CIField(db = "ATTR_HeaderTopBackground")
	private String headerTopBackground;
	
	public void setHeaderTopBackground(String headerTopBackground)
	{
		this.headerTopBackground = headerTopBackground;
	}
	
	public String getHeaderTopBackground()
	{
		return headerTopBackground;
	}
	
	public UsersCategory withHeaderTopBackground(String headerTopBackground)
	{
		this.headerTopBackground = headerTopBackground;
		return this;
	}
	 
	
	@CIField(db = "ATTR_HeaderBottomBackground")
	private String headerBottomBackground;
	
	public void setHeaderBottomBackground(String headerBottomBackground)
	{
		this.headerBottomBackground = headerBottomBackground;
	}
	
	public String getHeaderBottomBackground()
	{
		return headerBottomBackground;
	}
	
	public UsersCategory withHeaderBottomBackground(String headerBottomBackground)
	{
		this.headerBottomBackground = headerBottomBackground;
		return this;
	}
	 
	
	@CIField(db = "ATTR_HeaderTopData")
	private String headerTopData;
	
	public void setHeaderTopData(String headerTopData)
	{
		this.headerTopData = headerTopData;
	}
	
	public String getHeaderTopData()
	{
		return headerTopData;
	}
	
	public UsersCategory withHeaderTopData(String headerTopData)
	{
		this.headerTopData = headerTopData;
		return this;
	}
	 
	
	@CIField(db = "ATTR_HeaderBottomData")
	private String headerBottomData;
	
	public void setHeaderBottomData(String headerBottomData)
	{
		this.headerBottomData = headerBottomData;
	}
	
	public String getHeaderBottomData()
	{
		return headerBottomData;
	}
	
	public UsersCategory withHeaderBottomData(String headerBottomData)
	{
		this.headerBottomData = headerBottomData;
		return this;
	}
	 
	
	@CIField(db = "ATTR_HeaderTopFontColor")
	private String headerTopFontColor;
	
	public void setHeaderTopFontColor(String headerTopFontColor)
	{
		this.headerTopFontColor = headerTopFontColor;
	}
	
	public String getHeaderTopFontColor()
	{
		return headerTopFontColor;
	}
	
	public UsersCategory withHeaderTopFontColor(String headerTopFontColor)
	{
		this.headerTopFontColor = headerTopFontColor;
		return this;
	}
	 
	
	@CIField(db = "ATTR_HeaderBottomFontColor")
	private String headerBottomFontColor;
	
	public void setHeaderBottomFontColor(String headerBottomFontColor)
	{
		this.headerBottomFontColor = headerBottomFontColor;
	}
	
	public String getHeaderBottomFontColor()
	{
		return headerBottomFontColor;
	}
	
	public UsersCategory withHeaderBottomFontColor(String headerBottomFontColor)
	{
		this.headerBottomFontColor = headerBottomFontColor;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_autoDeleteItinerary")
	private Boolean autoDeleteItinerary;
	
	public void setAutoDeleteItinerary(Boolean autoDeleteItinerary)
	{
		this.autoDeleteItinerary = autoDeleteItinerary;
	}
	
	public Boolean getAutoDeleteItinerary()
	{
		return autoDeleteItinerary;
	}
	
	public UsersCategory withAutoDeleteItinerary(Boolean autoDeleteItinerary)
	{
		this.autoDeleteItinerary = autoDeleteItinerary;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_deleteInMonths")
	private Integer deleteInMonths;
	
	public void setDeleteInMonths(Integer deleteInMonths)
	{
		this.deleteInMonths = deleteInMonths;
	}
	
	public Integer getDeleteInMonths()
	{
		return deleteInMonths;
	}
	
	public UsersCategory withDeleteInMonths(Integer deleteInMonths)
	{
		this.deleteInMonths = deleteInMonths;
		return this;
	}
	 
	
	@CIField(db = "ATTRDROP_PayOption")
	private String payOption;
	
	public void setPayOption(String payOption)
	{
		this.payOption = payOption;
	}
	
	public String getPayOption()
	{
		return payOption;
	}
	
	public UsersCategory withPayOption(String payOption)
	{
		this.payOption = payOption;
		return this;
	}
	 
	
	@CIField(db = "ATTRDATE_supportAuthTime")
	private Date supportAuthTime;
	
	public void setSupportAuthTime(Date supportAuthTime)
	{
		this.supportAuthTime = supportAuthTime;
	}
	
	public Date getSupportAuthTime()
	{
		return supportAuthTime;
	}
	
	public UsersCategory withSupportAuthTime(Date supportAuthTime)
	{
		this.supportAuthTime = supportAuthTime;
		return this;
	}
	 
	
	@CIField(db = "ATTRINTEGER_AlertedDataPlanQty")
	private Integer alertedDataPlanQty;
	
	public void setAlertedDataPlanQty(Integer alertedDataPlanQty)
	{
		this.alertedDataPlanQty = alertedDataPlanQty;
	}
	
	public Integer getAlertedDataPlanQty()
	{
		return alertedDataPlanQty;
	}
	
	public UsersCategory withAlertedDataPlanQty(Integer alertedDataPlanQty)
	{
		this.alertedDataPlanQty = alertedDataPlanQty;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_encryptClientData")
	private Boolean encryptClientData;
	
	public void setEncryptClientData(Boolean encryptClientData)
	{
		this.encryptClientData = encryptClientData;
	}
	
	public Boolean getEncryptClientData()
	{
		return encryptClientData;
	}
	
	public UsersCategory withEncryptClientData(Boolean encryptClientData)
	{
		this.encryptClientData = encryptClientData;
		return this;
	}
	 
	
	@CIField(db = "ATTRDATE_deletionAlertEmailSent")
	private Date deletionAlertEmailSent;
	
	public void setDeletionAlertEmailSent(Date deletionAlertEmailSent)
	{
		this.deletionAlertEmailSent = deletionAlertEmailSent;
	}
	
	public Date getDeletionAlertEmailSent()
	{
		return deletionAlertEmailSent;
	}
	
	public UsersCategory withDeletionAlertEmailSent(Date deletionAlertEmailSent)
	{
		this.deletionAlertEmailSent = deletionAlertEmailSent;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_qcLinkedToMaestrano")
	private Boolean qcLinkedToMaestrano;
	
	public void setQcLinkedToMaestrano(Boolean qcLinkedToMaestrano)
	{
		this.qcLinkedToMaestrano = qcLinkedToMaestrano;
	}
	
	public Boolean getQcLinkedToMaestrano()
	{
		return qcLinkedToMaestrano;
	}
	
	public UsersCategory withQcLinkedToMaestrano(Boolean qcLinkedToMaestrano)
	{
		this.qcLinkedToMaestrano = qcLinkedToMaestrano;
		return this;
	}
	 
	
	@CIField(db = "ATTRCHECK_encryptedDataSync")
	private Boolean encryptedDataSync;
	
	public void setEncryptedDataSync(Boolean encryptedDataSync)
	{
		this.encryptedDataSync = encryptedDataSync;
	}
	
	public Boolean getEncryptedDataSync()
	{
		return encryptedDataSync;
	}
	
	public UsersCategory withEncryptedDataSync(Boolean encryptedDataSync)
	{
		this.encryptedDataSync = encryptedDataSync;
		return this;
	}
	//USER_DEFINED_END 

}