package com.mandrill;

public class QcloudMandrillHeaders extends MandrillHeaders
{
	public static String TAG_SYSTEM = "qcloud";
	public static String TAG_PROPOSAL = "proposal";

	public static String METADATA_PROPOSAL_ID = "proposal_id";

}
