package com.mandrill.request;

import org.apache.log4j.Logger;

import com.mandrill.response.WebhookResponse;

public class MandrillWebhookList extends MandrillAPIRequest
{
	/**
	 * @param key
	 */
	public MandrillWebhookList(String key)
	{
		super(key);
	}

	private static Logger logger = Logger.getLogger(MandrillPing.class);

	@Override
	public String requestPath()
	{
		return "/webhooks/list.json";
	}

	@Override
	public Class responseType()
	{
		return WebhookResponse[].class;
	}
}