/**
 * @author Sushant Verma
 * @date 7 Aug 2015
 */
package com.mandrill.request;

import org.apache.log4j.Logger;

public class MandrilInfo extends MandrillAPIRequest
{
	/**
	 * @param key
	 */
	public MandrilInfo(String key)
	{
		super(key);
	}

	private static Logger logger = Logger.getLogger(MandrilInfo.class);

	@Override
	public String requestPath()
	{
		return "/users/info.json";
	}

	@Override
	public Class responseType()
	{
		// TODO Need to implement
		return null;
	}
}
