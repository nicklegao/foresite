/**
 * @author Sushant Verma
 * @date 7 Aug 2015
 */
package com.mandrill.request;

import org.apache.log4j.Logger;

import com.mandrill.response.PingResponse;

public class MandrillPing extends MandrillAPIRequest
{
	/**
	 * @param key
	 */
	public MandrillPing(String key)
	{
		super(key);
	}

	private static Logger logger = Logger.getLogger(MandrillPing.class);

	@Override
	public String requestPath()
	{
		return "/users/ping2.json";
	}

	@Override
	public Class responseType()
	{
		return PingResponse.class;
	}
}
