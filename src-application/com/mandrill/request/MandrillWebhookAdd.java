package com.mandrill.request;

import org.apache.log4j.Logger;

import com.mandrill.response.WebhookResponse;

public class MandrillWebhookAdd extends MandrillAPIRequest
{
	/**
	 * @param key
	 */
	public MandrillWebhookAdd(String key)
	{
		super(key);
	}

	private static Logger logger = Logger.getLogger(MandrillPing.class);

	private String url;
	private String description;
	private String[] events;

	public static String SEND = "send";
	public static String HARD_BOUNCE = "hard_bounce";
	public static String SOFT_BOUNCE = "soft_bounce";
	public static String OPEN = "open";
	public static String CLICK = "click";
	public static String SPAM = "spam";
	public static String UNSUB = "unsub";
	public static String REJECT = "reject";
	public static String BLACKLIST = "blacklist";
	public static String WHITELIST = "whitelist";

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String[] getEvents()
	{
		return events;
	}

	public void setEvents(String[] events)
	{
		this.events = events;
	}

	@Override
	public String requestPath()
	{
		return "/webhooks/add.json";
	}

	@Override
	public Class responseType()
	{
		return WebhookResponse.class;
	}
}