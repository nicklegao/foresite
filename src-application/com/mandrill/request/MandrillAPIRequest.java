/**
 * @author Sushant Verma
 * @date 7 Aug 2015
 */
package com.mandrill.request;



public abstract class MandrillAPIRequest
{
	private String key;

	public MandrillAPIRequest(String key)
	{
		this.key = key;
	}

	public String getKey()
	{
		return key;
	}

	public abstract String requestPath();

	public abstract Class responseType();

	/**
	 * @return
	 */
	public String mandrillUrl()
	{
		return "https://mandrillapp.com/api/1.0" + requestPath();
	}
}
