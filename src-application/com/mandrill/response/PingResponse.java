/**
 * @author Sushant Verma
 * @date 10 Aug 2015
 */
package com.mandrill.response;

public class PingResponse extends MandrilAPIResponse
{
	private String PING;

	public String getPING()
	{
		return PING;
	}

	public void setPING(String pING)
	{
		PING = pING;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "Mandril Ping:" + PING;
	}
}
