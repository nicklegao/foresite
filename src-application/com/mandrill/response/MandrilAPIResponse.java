/**
 * @author Sushant Verma
 * @date 10 Aug 2015
 */
package com.mandrill.response;

public class MandrilAPIResponse
{
	private String status;
	private int code;
	private String name;
	private String message;

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public int getCode()
	{
		return code;
	}

	public void setCode(int code)
	{
		this.code = code;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}
}
