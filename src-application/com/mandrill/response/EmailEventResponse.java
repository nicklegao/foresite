/**
 * @author Rafael Menezes
 * @date 10 Aug 2015
 */
package com.mandrill.response;

import com.google.gson.internal.StringMap;

public class EmailEventResponse extends MandrilAPIResponse
{
	private String ts;
	private String _id;
	private String event;
	private StringMap<Object> msg;


	public String getTs()
	{
		return ts;
	}


	public void setTs(String ts)
	{
		this.ts = ts;
	}


	public String get_id()
	{
		return _id;
	}


	public void set_id(String _id)
	{
		this._id = _id;
	}


	public String getEvent()
	{
		return event;
	}


	public void setEvent(String event)
	{
		this.event = event;
	}


	public StringMap<Object> getMsg()
	{
		return msg;
	}


	public void setMsg(StringMap<Object> msg)
	{
		this.msg = msg;
	}

	public boolean hasMsg()
	{
		return this.msg != null;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "Mandrill event: " + msg.get("subject") + " (" + event + ")";
	}
}
