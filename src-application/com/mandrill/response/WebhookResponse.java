/**
 * @author Rafael Menezes
 * @date 13 Aug 2015
 */

package com.mandrill.response;


public class WebhookResponse extends MandrilAPIResponse
{

	private int id;
	private String url;
	private String description;
	private String auth_key;
	private String[] events;
	private String created_at;
	private String last_sent_at;
	private int batches_sent;
	private int events_sent;
	private String last_error;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getAuth_key()
	{
		return auth_key;
	}

	public void setAuth_key(String auth_key)
	{
		this.auth_key = auth_key;
	}

	public String[] getEvents()
	{
		return events;
	}

	public void setEvents(String[] events)
	{
		this.events = events;
	}

	public String getCreated_at()
	{
		return created_at;
	}

	public void setCreated_at(String created_at)
	{
		this.created_at = created_at;
	}

	public String getLast_sent_at()
	{
		return last_sent_at;
	}

	public void setLast_sent_at(String last_sent_at)
	{
		this.last_sent_at = last_sent_at;
	}

	public int getBatches_sent()
	{
		return batches_sent;
	}

	public void setBatches_sent(int batches_sent)
	{
		this.batches_sent = batches_sent;
	}

	public int getEvents_sent()
	{
		return events_sent;
	}

	public void setEvents_sent(int events_sent)
	{
		this.events_sent = events_sent;
	}

	public String getLast_error()
	{
		return last_error;
	}

	public void setLast_error(String last_error)
	{
		this.last_error = last_error;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "Mandril Webhook - (" + id + ") " + description + " - " + url;
	}
}
