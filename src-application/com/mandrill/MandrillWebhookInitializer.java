package com.mandrill;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import au.corporateinteractive.qcloud.proposalbuilder.db.EmailLogDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.utils.context.ThreadContext;

import com.google.gson.Gson;
import com.mandrill.request.MandrillAPIRequest;
import com.mandrill.request.MandrillWebhookAdd;
import com.mandrill.request.MandrillWebhookList;
import com.mandrill.response.WebhookResponse;

public class MandrillWebhookInitializer
{

	protected Logger logger = null;

	private static String eventUrl = "/api/free/emailBounceEvent";

	/**
	 * @param logger2
	 */
	public MandrillWebhookInitializer(Logger logger)
	{
		this.logger = logger;
	}

	public int init()
	{
		String key = Defaults.getInstance().getPasswordSMTPserver();
		String host = Defaults.getInstance().getWebSiteURL();
		initWebhook(key, host, null);
		List<CategoryData> customEmailSettings = new EmailLogDataAccess().getUsingMandrillCompanies();
		for (CategoryData setting : customEmailSettings)
		{
			key = setting.getString(EmailLogDataAccess.C1_PASSWORD_SMTPSERVER);
			host = setting.getString(EmailLogDataAccess.C1_HOST);
			initWebhook(key, host, setting.getString(EmailLogDataAccess.C1_COMPANY_ID));
		}

		return customEmailSettings.size() + 1;
	}

	/**
	 * @param key
	 * @param host
	 */
	private void initWebhook(String key, String host, String companyId)
	{
		try
		{
			ThreadContext.reset();

			MandrillWebhookList request = new MandrillWebhookList(key);
			String jsonObj = makeRequest(request);

			WebhookResponse[] resObj = new Gson().fromJson(jsonObj, WebhookResponse[].class);
			if (host.endsWith("/"))
			{
				host = host.substring(0, host.length() - 1);
			}

			WebhookResponse eventHook = null;
			for (WebhookResponse hook : resObj)
			{
				if (hook.getUrl().endsWith(host + eventUrl))
				{
					eventHook = hook;
				}
			}

			if (eventHook == null)
			{
				//request creation
				logger.info("Mandrill: Webwook for all events not found");
				eventHook = createWebhook(host + eventUrl, "all events", key);
				if (eventHook.getStatus() == null)
				{
					logger.info("Mandrill: Webhook for all events created: " + host + eventUrl);
				}
				else
				{
					logger.info("Mandrill: Webhook for all events was not created due to the error above");
				}
			}
			else
			{
				logger.info("Mandrill: Webwook for all events already exists");
			}
			saveWebhookResponse(eventHook, companyId);
		}
		catch (Exception e)
		{
			new EmailBuilder().prepareErrorEmailForContext("Unable to initialize Mandrill Webhooks", e).doSend();
		}

	}

	/**
	 * @param bounced
	 * @param companyId
	 */
	private void saveWebhookResponse(WebhookResponse bounced, String companyId)
	{
		if (StringUtils.isBlank(companyId))
		{
			return;
		}
		try
		{
			TransactionDataAccess.getInstance().update("update categories_email_log set attr_webhook = ? where attrdrop_companyid = ?", new Gson().toJson(bounced), companyId);
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
		}

	}

	private String makeRequest(MandrillAPIRequest request) throws UnsupportedEncodingException, IOException, ClientProtocolException
	{
		String url = request.mandrillUrl();

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);

		Gson gson = new Gson();
		String reqStr = gson.toJson(request);
		logger.info("MAKING REQUEST (" + request.getClass().getSimpleName() + "):" + reqStr);
		post.setEntity(new StringEntity(reqStr));

		HttpResponse response = client.execute(post);
		logger.trace("Sending 'POST' request to URL : " + url);
		logger.trace("Post parameters : " + post.getEntity());
		logger.trace("Response Code : " +
				response.getStatusLine().getStatusCode());

		BufferedReader rd = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null)
		{
			result.append(line);
		}

		logger.info("MANDRIL RESPONSE: " + result.toString());
		return result.toString();
	}

	private WebhookResponse createWebhook(String url, String description, String key, String... states) throws UnsupportedEncodingException, IOException, ClientProtocolException
	{
		MandrillWebhookAdd bouncedRequest = new MandrillWebhookAdd(key);

		bouncedRequest.setUrl(url);
		bouncedRequest.setDescription(description);
		bouncedRequest.setEvents(states);

		String jsonNewHook = makeRequest(bouncedRequest);
		WebhookResponse newHook = new Gson().fromJson(jsonNewHook, WebhookResponse.class);

		return newHook;
	}
}
