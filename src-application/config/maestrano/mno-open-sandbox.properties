# ===> App Configuration
#
# => environment
# The environment to connect to. If set to 'production' then all Single Sign-On (SSO) and API requests will be made to maestrano.com. If set to 'test' then requests will be made to api-sandbox.maestrano.io. 
# The api-sandbox allows you to easily test integration scenarios.
environment=test

# => host
# This is your application host (e.g: my-app.com) which is ultimately used to redirect users to the right SAML url during SSO handshake.
app.host=http\://localhost:8080

# ===> Api Configuration
#
# => id and key
# Your application App ID and API key which you can retrieve on http://maestrano.com via your cloud partner dashboard. 
# For testing you can retrieve/generate an api.id and api.key from the API Sandbox directly on http://api-sandbox.maestrano.io

# Open testing sandbox credentials
api.id=app-65
api.key=gfoa7nuyl3bphcx8z01rd2iet46j5mwk9qvs65

# ===> SSO Configuration
#
# => enabled
# Enable/Disable single sign-on. When troubleshooting authentication issues you might want to disable SSO temporarily
sso.enabled=true

# => sloEnabled
# Enable/Disable single logout. When troubleshooting authentication issues you might want to disable SLO temporarily. 
# If set to false then MnoSession#isValid - which should be used in a controller action filter to check user session - always return true
sso.sloEnabled=true

# => idm
# By default we consider that the domain managing user identification is the same as your application host (see above config.app.host parameter). 
# If you have a dedicated domain managing user identification and therefore responsible for the single sign-on handshake (e.g: https://idp.my-app.com) then you can specify it below
##sso.idm=https\://idp.myapp.com

# => idp (optional)
# This is the URL of the identity provider to use when triggering a SSO handshake. With a multi-tenant integration, each tenant would have its own URL. Defaults to https://maestrano.com
#sso.idm=https\://maestrano.com

# => initPath
# This is your application path to the SAML endpoint that allows users to initialize SSO authentication. 
# Upon reaching this endpoint users your application will automatically create a SAML request and redirect the user to Maestrano. Maestrano will then authenticate and authorize the user. 
# Upon authorization the user gets redirected to your application consumer endpoint (see below) for initial setup and/or login.
sso.initPath=/mno-enterprise/mno-open-sandbox/auth/saml/init

# => consumePath
#This is your application path to the SAML endpoint that allows users to finalize SSO authentication. 
# During the 'consume' action your application sets users (and associated group) up and/or log them in.
sso.consumePath=/mno-enterprise/mno-open-sandbox/auth/saml/consume

# => x509 SSL Certificate
# During the SSO handshake, the SSL certificate is validated and must match the IDP provider.
# For multi-tenant integration, the certificates may change per environment.
#sso.x509Fingerprint=2f:57:71:e4:40:19:57:37:a6:2c:f0:c5:82:52:2f:2e:41:b7:9d:7e
#sso.x509Certificate=-----BEGIN CERTIFICATE-----\nCERTIFICATE CONTENT==\n-----END CERTIFICATE-----

# => creationMode
# !IMPORTANT
# On Maestrano users can take several "instances" of your service. You can consider
# each "instance" as 1) a billing entity and 2) a collaboration group (this is
# equivalent to a 'customer account' in a commercial world). When users login to
# your application via single sign-on they actually login via a specific group which
# is then supposed to determine which data they have access to inside your application.
# 
# E.g: John and Jack are part of group 1. They should see the same data when they login to
# your application (employee info, analytics, sales etc..). John is also part of group 2 
# but not Jack. Therefore only John should be able to see the data belonging to group 2.
# 
# In most application this is done via collaboration/sharing/permission groups which is
# why a group is required to be created when a new user logs in via a new group (and 
# also for billing purpose - you charge a group, not a user directly). 
# 
# - mode: 'real'
# In an ideal world a user should be able to belong to several groups in your application.
# In this case you would set the 'sso.creation_mode' to 'real' which means that the uid
# and email we pass to you are the actual user email and maestrano universal id.
# 
# - mode: 'virtual'
# Now let's say that due to technical constraints your application cannot authorize a user
# to belong to several groups. Well next time John logs in via a different group there will
# be a problem: the user already exists (based on uid or email) and cannot be assigned 
# to a second group. To fix this you can set the 'sso.creation_mode' to 'virtual'. In this
# mode users get assigned a truly unique uid and email across groups. So next time John logs
# in a whole new user account can be created for him without any validation problem. In this
# mode the email we assign to him looks like "usr-sdf54.cld-45aa2@mail.maestrano.com". But don't
# worry we take care of forwarding any email you would send to this address
sso.creationMode="virtual"

# ===> Account Webhooks
# Single sign on has been setup into your app and Maestrano users are now able
# to use your service. Great! Wait what happens when a business (group) decides to 
# stop using your service? Also what happens when a user gets removed from a business?
# Well the endpoints below are for Maestrano to be able to notify you of such
# events.
#
# Even if the routes look restful we issue only issue DELETE requests for the moment
# to notify you of any service cancellation (group deletion) or any user being
# removed from a group.
webhook.account.groupsPath = /mno-enterprise/mno-open-sandbox/account/groups/\:id
webhook.account.groupUsersPath = /mno-enterprise/mno-open-sandbox/account/groups/\:group_id/users/\:id

# ===> Connec!\u2122 Webhooks
# == Notification Path
# This is the path of your application where notifications (created/updated entities) will be POSTed to.
# You should have a controller matching this path handling the update of your internal entities
# based on the Connec!\u2122 entities you receive
webhook.connec.notificationsPath = /mno-enterprise/mno-open-sandbox/connec/notifications

# == Subscriptions
# This is the list of entities (organizations,people,invoices etc.) for which you want to be
# notified upon creation/update in Connec!\u2122
webhook.connec.subscriptions.accounts = false
webhook.connec.subscriptions.company = false
webhook.connec.subscriptions.events = false
webhook.connec.subscriptions.event_orders = false
webhook.connec.subscriptions.invoices = false
webhook.connec.subscriptions.items = false
webhook.connec.subscriptions.journals = false
webhook.connec.subscriptions.organizations = false
webhook.connec.subscriptions.payments = false
webhook.connec.subscriptions.pay_items = false
webhook.connec.subscriptions.pay_schedules = false
webhook.connec.subscriptions.pay_stubs = false
webhook.connec.subscriptions.pay_runs = false
webhook.connec.subscriptions.people = false
webhook.connec.subscriptions.projects = false
webhook.connec.subscriptions.tax_codes = false
webhook.connec.subscriptions.tax_rates = false
webhook.connec.subscriptions.time_activities = false
webhook.connec.subscriptions.time_sheets = false
webhook.connec.subscriptions.venues = false
webhook.connec.subscriptions.work_locations = false