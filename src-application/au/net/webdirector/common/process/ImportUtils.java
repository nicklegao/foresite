package au.net.webdirector.common.process;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class ImportUtils
{
	private static Logger logger = Logger.getLogger(ImportUtils.class);

	public static String niceErrors(List<String> errors)
	{
		if (errors.size() == 0)
		{
			return null;
		}
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < errors.size(); i++)
		{
			sb.append("<p>" + (i + 1) + ". " + errors.get(i) + "</p>");
		}
		return sb.toString();
	}

	public static String niceErrors4Row(long recordNumber, String msg)
	{
		return "<p>Row " + recordNumber + " of the import file has the following problem(s):</p>" + msg;
	}

	public static String joinMessage(List<String> msgs, int max)
	{
		if (msgs.size() < max)
			return StringUtils.join(msgs, "");
		List<String> sub = msgs.subList(0, max);
		if (msgs.size() > sub.size())
		{
			sub.add("<p>There are " + (msgs.size() - sub.size()) + " more row(s) of the import file have similiar problems.</p>");
		}
		sub.add("<p>Please fix these problem(s) and try again.</p>");
		return StringUtils.join(sub, "");
	}
}
