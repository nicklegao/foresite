/**
 * @author Nick Yiming Gao
 * @date 05/04/2017 10:42:15 am
 */
package au.net.webdirector.common.process;

/**
 * 
 */

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

public class ProcessStatus
{

	private static Logger logger = Logger.getLogger(ProcessStatus.class);

	public static final String NOT_STARTED = "not started";
	public static final String RUNNING = "running";
	public static final String STOPPED = "stopped";
	public static final String FINISHED = "finished";
	public static final String DEAD = "dead";

	String status;
	int total;
	int processed;
	boolean success;
	int succeed;
	int failed;
	String message;
	Date start;
	Date lastUpdated;

	String stage;

	public String getStatus()
	{
		return status;
	}

	public int getTotal()
	{
		return total;
	}

	public int getProcessed()
	{
		return processed;
	}

	public String getMessage()
	{
		return message;
	}

	public Date getStart()
	{
		return start;
	}

	public Date getLastUpdated()
	{
		return lastUpdated;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public int getSucceed()
	{
		return succeed;
	}

	public int getFailed()
	{
		return failed;
	}

	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}

	public void setStart(Date start)
	{
		this.start = start;
	}

	public void setStatus(String status)
	{
		logger.info("status = " + status);
		this.status = status;
	}

	public void setTotal(int total)
	{
		this.total = total;
	}

	public void setProcessed(int processed)
	{
		this.processed = processed;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	public void setSucceed(int succeed)
	{
		this.succeed = succeed;
	}

	public void setFailed(int failed)
	{
		this.failed = failed;
	}

	public String getStage()
	{
		return stage;
	}

	public void setStage(String stage)
	{
		this.stage = stage;
	}

	public ProcessStatus(String status, String stage, int total, int processed, boolean success, int succeed, int failed, String message)
	{
		super();
		this.status = status;
		this.stage = stage;
		this.total = total;
		this.processed = processed;
		this.success = success;
		this.succeed = succeed;
		this.failed = failed;
		this.message = message;
	}

	public boolean isEnded()
	{
		return !NOT_STARTED.equals(status) && !RUNNING.equals(status);
	}

	/**
	 * @return
	 */
	public boolean tooOld()
	{
		if (this.lastUpdated == null)
		{
			return true;
		}
		return DateUtils.addSeconds(this.lastUpdated, 60).before(new Date());
	}

	public void processedPlusPlus()
	{
		processed++;
	}

	public void succeedPlusPlus()
	{
		succeed++;
	}
}
