/**
 * @author Nick Yiming Gao
 * @date 05/04/2017 10:19:45 am
 */
package au.net.webdirector.common.process;

/**
 * 
 */

import java.util.Date;
import java.util.UUID;

import org.apache.log4j.Logger;

public abstract class ProcessManager
{

	private static Logger logger = Logger.getLogger(ProcessManager.class);

	private static final int DEAD_TIMEOUT = 30 * 1000;
	private static final int CHECK_INTERVAL = 1000;

	public static ProcessManager getInstance()
	{
//		return RAMProcessManager.instance();
		return DBProcessManager.instance();
	}

	public String runProcessor(final Processor processor, String customId)
	{
		final Date startDate = new Date();
		final String processorId = registerProcessor(processor, customId);
		ProcessStatus status = processor.getStatus();
		status.setLastUpdated(new Date());
		status.setStart(startDate);
		saveStatus(processorId, status);
		new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				logger.info("Processor['" + processorId + "'] started processing...");
				try
				{
					processor.process();
				}
				catch (Exception e)
				{
					ProcessStatus status = processor.getStatus();
					status.setLastUpdated(new Date());
					status.setStart(startDate);
					saveStatus(processorId, status);
				}

				logger.info("Processor['" + processorId + "'] finished processing...");
			}

		}).start();
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					ProcessStatus status = null;
					int lastProcessed = -1;
					int counter = 0;
					do
					{
						logger.warn("checking status for processor [" + processorId + "]");
						try
						{
							Thread.sleep(CHECK_INTERVAL);
						}
						catch (Exception e)
						{

						}
						status = processor.getStatus();
						status.setLastUpdated(new Date());
						status.setStart(startDate);
						if (lastProcessed == status.getProcessed())
						{
							counter++;
						}
						else
						{
							counter = 0;
						}
						if (counter > DEAD_TIMEOUT / CHECK_INTERVAL)
						{
							status.setStatus(ProcessStatus.DEAD);
						}
						saveStatus(processorId, status);
						logger.warn("saved status for processor [" + processorId + "]");
					} while (!status.isEnded());

				}
				catch (Throwable e)
				{
					logger.error("Error", e);
				}
			}

		}).start();
		return processorId;
	}


	protected String registerProcessor(Processor processor, String customId)
	{
		String id = UUID.randomUUID().toString();
		Singleton s = processor.getClass().getAnnotation(Singleton.class);
		if (s != null)
		{
			id = processor.getClass().getName();
			if (getStatus(id) != null)
			{
				throw new RuntimeException("There is a running instance !");
			}
		}
		processor.setId(customId + "-" + id);
		return customId + "-" + id;
	}

	protected abstract void saveStatus(String processorId, ProcessStatus status);

	protected abstract void removeStatus(String processorId);

	public abstract void dumpDeadStatus();

	protected abstract ProcessStatus getStatus(String processorId);

	public void removeProcessStatus(String processorId, String customId)
	{
		removeStatus(customId + "-" + processorId);
	}

	public ProcessStatus getProcessStatus(String processorId, String customId)
	{
		ProcessStatus status = getStatus(customId + "-" + processorId);
		if (status == null)
		{
			return null;
		}
		if (status.tooOld())
		{
			removeStatus(processorId);
			return null;
		}
		return status;
	}

}
