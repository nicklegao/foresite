/**
 * @author Nick Yiming Gao
 * @date 05/04/2017 11:08:37 am
 */
package au.net.webdirector.common.process;

/**
 * 
 */

import org.apache.log4j.Logger;

@Singleton
public class TestProcessor implements Processor
{

	private static Logger logger = Logger.getLogger(TestProcessor.class);

	String status = ProcessStatus.NOT_STARTED;
	String stage = "Importing";
	int total;
	int processed;
	boolean success;
	int succeed;
	int failed;
	String id;
	String message;

	@Override
	public void process() throws Exception
	{
		status = ProcessStatus.RUNNING;
		try
		{
			total = 500;
			for (; processed < total; processed++)
			{
				Thread.sleep(1000);
			}
			status = ProcessStatus.FINISHED;
		}
		catch (Exception e)
		{
			status = ProcessStatus.STOPPED;
		}


	}

	@Override
	public ProcessStatus getStatus()
	{
		return new ProcessStatus(status, stage, total, processed, success, succeed, failed, message);
	}

	@Override
	public void setId(String id)
	{
		this.id = id;
	}

}
