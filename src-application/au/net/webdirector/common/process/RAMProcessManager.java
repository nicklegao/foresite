/**
 * @author Nick Yiming Gao
 * @date 05/04/2017 10:21:02 am
 */
package au.net.webdirector.common.process;

/**
 * 
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

public class RAMProcessManager extends ProcessManager
{

	private static Logger logger = Logger.getLogger(RAMProcessManager.class);

	private static RAMProcessManager instance = new RAMProcessManager();

	private Map<String, ProcessStatus> statuses = new ConcurrentHashMap<>();

	static RAMProcessManager instance()
	{
		return instance;
	}

	RAMProcessManager()
	{

	}

	@Override
	protected void saveStatus(String processorId, ProcessStatus status)
	{
		statuses.put(processorId, status);
	}

	@Override
	protected ProcessStatus getStatus(String processorId)
	{
		return statuses.get(processorId);
	}

	@Override
	protected void removeStatus(String processorId)
	{
		statuses.remove(processorId);
	}

	@Override
	public void dumpDeadStatus()
	{
		List<String> ids = new ArrayList<>();
		ids.addAll(statuses.keySet());
		for (String id : ids)
		{
			if (getStatus(id).tooOld())
			{
				removeStatus(id);
			}
		}
	}
}
