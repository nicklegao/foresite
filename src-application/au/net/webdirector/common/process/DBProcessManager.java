/**
 * @author Nick Yiming Gao
 * @date 05/04/2017 10:21:02 am
 */
package au.net.webdirector.common.process;

/**
 * 
 */

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;

public class DBProcessManager extends ProcessManager
{

	private static Logger logger = Logger.getLogger(DBProcessManager.class);

	private static DBProcessManager instance = new DBProcessManager();

	static DBProcessManager instance()
	{
		return instance;
	}

	DBProcessManager()
	{
		checkDBTable();
	}

	/**
	 * 
	 */
	private void checkDBTable()
	{
		TransactionDataAccess da = TransactionDataAccess.getInstance();
		try
		{
			da.selectString("select 1 from sys_process_status");
		}
		catch (SQLException e)
		{
			try
			{
				da.update("CREATE TABLE `sys_process_status` ( `PROC_ID` VARCHAR(100) NOT NULL"
						+ ",  `S_STATUS` VARCHAR(45) NOT NULL"
						+ ",  `S_STAGE` VARCHAR(45) NOT NULL"
						+ ",  `I_TOTAL` INTEGER NOT NULL"
						+ ",  `I_PROCESSED` INTEGER NOT NULL"
						+ ",  `I_IS_SUCCESS` INTEGER NOT NULL"
						+ ",  `I_SUCCEED` INTEGER NOT NULL"
						+ ",  `I_FAILED` INTEGER NOT NULL"
						+ ",  `D_START` TIMESTAMP NOT NULL"
						+ ",  `D_LASTUPDATED` TIMESTAMP NOT NULL"
						+ ", `S_MESSAGE` TEXT NOT NULL"
						+ ",  PRIMARY KEY (`PROC_ID`))");
			}
			catch (SQLException e1)
			{
				throw new RuntimeException(e1);
			}
		}

	}

	@Override
	protected void saveStatus(String processorId, ProcessStatus status)
	{
		TransactionDataAccess da = TransactionDataAccess.getInstance();
		QueryBuffer sql = new QueryBuffer();
		sql.append("insert into sys_process_status (PROC_ID, S_STATUS, S_STAGE, I_TOTAL, I_PROCESSED, I_IS_SUCCESS, I_SUCCEED, I_FAILED, D_START, D_LASTUPDATED, S_MESSAGE ) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) on DUPLICATE KEY UPDATE S_STATUS = ?, S_STAGE = ?, I_TOTAL = ?, I_PROCESSED = ?, I_IS_SUCCESS=?, I_SUCCEED=?, I_FAILED=?, D_START= ?, D_LASTUPDATED= ?, S_MESSAGE = ?"
				, processorId, status.getStatus(), status.getStage(), status.getTotal(), status.getProcessed(), status.isSuccess() ? 1 : 0, status.getSucceed(), status.getFailed(), status.getStart(), status.getLastUpdated(), status.getMessage()
				, status.getStatus(), status.getStage(), status.getTotal(), status.getProcessed(), status.isSuccess() ? 1 : 0, status.getSucceed(), status.getFailed(), status.getStart(), status.getLastUpdated(), status.getMessage());
		try
		{
			da.update(sql);
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
		}

	}

	@Override
	protected ProcessStatus getStatus(String processorId)
	{
		TransactionDataAccess da = TransactionDataAccess.getInstance();
		try
		{
			DataMap map = da.selectMap("select * from sys_process_status where proc_id = ?", processorId);
			if (map == null)
			{
				return null;
			}
			ProcessStatus status = new ProcessStatus(map.getString("S_STATUS"), map.getString("S_STAGE"), map.getInt("I_TOTAL"), map.getInt("I_PROCESSED"), map.getBoolean("I_IS_SUCCESS"), map.getInt("I_SUCCEED"), map.getInt("I_FAILED"), map.getString("S_MESSAGE"));
			status.setStart(map.getDate("D_START"));
			status.setLastUpdated(map.getDate("D_LASTUPDATED"));
			return status;
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see au.net.webdirector.common.process.ProcessManager#removeStatus(java.lang.String)
	 */
	@Override
	protected void removeStatus(String processorId)
	{
		try
		{
			TransactionDataAccess.getInstance().update("delete from sys_process_status where proc_id = ?", processorId);
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
		}

	}

	@Override
	public void dumpDeadStatus()
	{
		try
		{
			List<String> ids = TransactionDataAccess.getInstance().selectStrings("select proc_id from sys_process_status");
			for (String id : ids)
			{
				if (getStatus(id).tooOld())
				{
					removeStatus(id);
				}
			}
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
		}
	}


}
