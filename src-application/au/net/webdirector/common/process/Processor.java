/**
 * @author Nick Yiming Gao
 * @date 05/04/2017 11:08:00 am
 */
package au.net.webdirector.common.process;

/**
 * 
 */

public interface Processor
{

	public void process() throws Exception;

	public ProcessStatus getStatus();

	public void setId(String id);


}
