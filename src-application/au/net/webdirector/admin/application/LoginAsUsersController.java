package au.net.webdirector.admin.application;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.ci.webdirector.user.UserManager;
import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder;
import au.corporateinteractive.qcloud.proposalbuilder.service.SecurityKeyService;
import au.net.webdirector.admin.bulkEdit.domain.datatable.DatatableProcessingRequest;
import au.net.webdirector.admin.bulkEdit.domain.datatable.DatatableProcessingResponse;
import au.net.webdirector.admin.utils.SecureJSPPath;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.utils.SimpleDateFormat;

@Controller
@RequestMapping("/app")
public class LoginAsUsersController
{

	Logger logger = Logger.getLogger(LoginAsUsersController.class);

	@RequestMapping(value = "/iframe/users", method = RequestMethod.GET)
	public String usersList(
			HttpServletRequest request,
			HttpServletResponse response)
	{
		return SecureJSPPath.iframePath("users/list");
	}

	@RequestMapping(value = "/data/users", method = RequestMethod.POST)
	@ResponseBody
	public DatatableProcessingResponse usersData(
			HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session) throws Exception
	{
		DatatableProcessingRequest dataRequest = new DatatableProcessingRequest(request);

		DatatableProcessingResponse<DataMap> res = new DatatableProcessingResponse<DataMap>();

		res.setDraw(dataRequest.draw);
		int total = countRecordsTotal(dataRequest);
		res.setRecordsTotal(total);
		res.setRecordsFiltered(total);
		res.setData(loadData(dataRequest));
		return res;
	}

	/**
	 * @param dataRequest
	 * @return
	 * @throws SQLException
	 */
	private List<DataMap> loadData(DatatableProcessingRequest dataRequest) throws SQLException
	{
		QueryBuffer qb = getBasicQuery(dataRequest);
		QueryBuffer qb2 = getBasicQuery2(dataRequest);
		if (StringUtils.isNotBlank(dataRequest.search.value))
		{
			String search = "%" + dataRequest.search.value + "%";
			qb.append(" where (attr_headline like ? or company like ? or attr_name like ? or attr_surname like ?) ", search, search, search, search);
			qb2.append(" where (attr_headline like ? or company like ? or attr_name like ? or attr_surname like ?) ", search, search, search, search);
		}
		if (dataRequest.order.length > 0)
		{
			qb.append(" order by " + dataRequest.columns[dataRequest.order[0].column].data + " " + dataRequest.order[0].dir);
			qb2.append(" order by " + dataRequest.columns[dataRequest.order[0].column].data + " " + dataRequest.order[0].dir);
		}
		qb.append(" limit " + dataRequest.start + ", " + dataRequest.length);
		qb2.append(" limit " + dataRequest.start + ", " + dataRequest.length);
		List<String> qNames = new ArrayList<>();
		List<DataMap> q1 = TransactionDataAccess.getInstance().selectMaps(qb);
		List<DataMap> q2 = TransactionDataAccess.getInstance().selectMaps(qb2);
		List<DataMap> qFull = new ArrayList<>();
		if (q1 != null)
		{
			qFull.addAll(q1);
			for (DataMap q : q1)
			{
				for (String key : q.keySet())
				{
					Object value = q.get(key);
					if (value instanceof String)
					{
						if (key.toLowerCase().equalsIgnoreCase("attr_headline") && !qNames.contains(value))
						{
							qNames.add((String) value);
						}
					}
				}
			}
		}
		if (q2 != null)
		{
			for (DataMap q : q2)
			{
				for (String key : q.keySet())
				{
					Object value = q.get(key);
					if (value instanceof String)
					{
						if (key.toLowerCase().equalsIgnoreCase("attr_headline") && !qNames.contains(value))
						{
							qNames.add((String) value);
							qFull.add(q);
						}
					}
				}
			}
		}
		return qFull;
	}

	/**
	 * @param dataRequest
	 * @return
	 * @throws SQLException
	 * @throws NumberFormatException
	 */
	private int countRecordsTotal(DatatableProcessingRequest dataRequest) throws NumberFormatException, SQLException
	{
		// TODO Auto-generated method stub
		QueryBuffer qb = new QueryBuffer();
		qb.append("select count(1) from (");
		qb.append(getBasicQuery(dataRequest));
		qb.append(") as t");
		return Integer.parseInt(TransactionDataAccess.getInstance().selectString(qb));
	}

	private QueryBuffer getBasicQuery(DatatableProcessingRequest dataRequest)
	{
		QueryBuffer qb = new QueryBuffer();
		qb.append("select * from (");
		qb.append(" select ");
		qb.append(" element_id,attr_headline, ");
		qb.append(" c1.attr_categoryname as company, ");
		qb.append(" attr_name,attr_surname ");
		qb.append(" from elements_useraccounts e, categories_users c1, categories_users c2  ");
		qb.append(" where c1.category_id = c2.category_parentid and c2.category_id = e.category_id ");
		qb.append(" and e.live = 1 ");
		qb.append(" and (DATE(e.ATTRDATE_supportAuthTime) >= DATE(NOW())) ");
		qb.append(") as data ");
		return qb;
	}

	private QueryBuffer getBasicQuery2(DatatableProcessingRequest dataRequest)
	{
		QueryBuffer qb = new QueryBuffer();
		qb.append("select * from (");
		qb.append(" select ");
		qb.append(" element_id,attr_headline, ");
		qb.append(" c1.attr_categoryname as company, ");
		qb.append(" attr_name,attr_surname ");
		qb.append(" from elements_useraccounts e, categories_users c1, categories_users c2  ");
		qb.append(" where c1.category_id = c2.category_parentid and c2.category_id = e.category_id ");
		qb.append(" and e.live = 1 ");
		qb.append(" and c1.ATTRDROP_pricingPlan = 'TRIAL' ");
		qb.append(") as data ");
		return qb;
	}


//	@RequestMapping(value = "/loginas/users", method = RequestMethod.GET)
//	public void loginAsUser(
//			HttpServletRequest request,
//			HttpServletResponse response,
//			HttpSession session,
//			@RequestParam String email) throws Exception
//	{
//		try
//		{
//			UserController.autoLogin(request, response, session, email);
//		}
//		catch (Exception e)
//		{
//			logger.error("Error:", e);
//		}
//	}

	@RequestMapping("/loginas/verification")
	@ResponseBody
	public void loginAsVerification(HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session,
			@RequestParam String email)
	{

		String verificationKey = new SecurityKeyService().generateRandomKey(12, 6, "-");

		session.setAttribute("loginVerificationKey", verificationKey);
		session.setAttribute(Constants.LOGIN_AS, email);
		try
		{
			String loggedUserEmail = UserManager.getUserEmail(request);

			try
			{
				String authDate = new UserAccountDataAccess().getUserByUserName(email).get(UserAccountDataAccess.C1_SUPPORT_TIME);
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
				Date parsedDate = dateFormat.parse(authDate);
				Long time = parsedDate.getTime();
				Long nowTime = new Date().getTime();
				boolean emailSent = false;

				emailSent = new EmailBuilder().prepareSingleSignupVerification(verificationKey, email).addTo(loggedUserEmail).doSend();

//				if (nowTime > time) {
//					 emailSent = new EmailBuilder().prepareSingleSignupUserAuthRequest(verificationKey, loggedUserEmail, email).addTo(email).doSend();
//				} else {
//					emailSent = new EmailBuilder().prepareSingleSignupVerification(verificationKey, email).addTo(loggedUserEmail).doSend();
//				}
				logger.info("verificationKey=" + verificationKey);
				if (emailSent)
				{
					new EmailBuilder().prepareSingleSignupRequest(loggedUserEmail, email, false).addTo("support@corporateinteractive.com.au").doSend();
					response.sendRedirect(request.getContextPath() + "/signUpEmailSent");
				}
			}
			catch (Exception e)
			{ //this generic but you can control another types of exception
				// look the origin of excption
			}
		}
		catch (Exception e)

		{
			e.printStackTrace();
		}

	}

}
