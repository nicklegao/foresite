package au.net.webdirector.admin.application;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.corporateinteractive.qcloud.proposalbuilder.db.WebdirectorDataAccess;
import au.net.webdirector.admin.bulkEdit.domain.datatable.DatatableProcessingRequest;
import au.net.webdirector.admin.bulkEdit.domain.datatable.DatatableProcessingResponse;
import au.net.webdirector.admin.utils.SecureJSPPath;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;

@Controller
@RequestMapping("/secure/app")
public class CustomersController
{

	Logger logger = Logger.getLogger(CustomersController.class);

	@RequestMapping(value = "/iframe/customers", method = RequestMethod.GET)
	public String custoemrsList(
			HttpServletRequest request,
			HttpServletResponse response)
	{
		return SecureJSPPath.iframePath("customers/list");
	}

	@RequestMapping(value = "/delete/customer", method = RequestMethod.POST)
	@ResponseBody
	public AjaxResponse deleteCustomer(
			HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session,
			@RequestParam String id) throws Exception
	{
		try (TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true)))
		{
			TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
			new WebdirectorDataAccess().deleteCompanyCategoryFromModules(id, ma);
			txManager.commit();
			return new AjaxResponse(true, null);
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return new AjaxResponse(false, e.getMessage());
		}
	}

	@RequestMapping(value = "/data/customers", method = RequestMethod.POST)
	@ResponseBody
	public DatatableProcessingResponse usersData(
			HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session) throws Exception
	{
		DatatableProcessingRequest dataRequest = new DatatableProcessingRequest(request);

		DatatableProcessingResponse<DataMap> res = new DatatableProcessingResponse<DataMap>();

		res.setDraw(dataRequest.draw);
		int total = countRecordsTotal(dataRequest);
		res.setRecordsTotal(total);
		res.setRecordsFiltered(total);
		res.setData(loadData(dataRequest));
		return res;
	}

	/**
	 * @param dataRequest
	 * @return
	 * @throws SQLException
	 */
	private List<DataMap> loadData(DatatableProcessingRequest dataRequest) throws SQLException
	{
		QueryBuffer qb = getBasicQuery(dataRequest);
		if (StringUtils.isNotBlank(dataRequest.search.value))
		{
			String search = "%" + dataRequest.search.value + "%";
			qb.append(" where (company like ?) ", search);
		}
		if (dataRequest.order.length > 0)
		{
			qb.append(" order by " + dataRequest.columns[dataRequest.order[0].column].data + " " + dataRequest.order[0].dir);
		}
		qb.append(" limit " + dataRequest.start + ", " + dataRequest.length);
		return TransactionDataAccess.getInstance().selectMaps(qb);
	}

	/**
	 * @param dataRequest
	 * @return
	 * @throws SQLException
	 * @throws NumberFormatException
	 */
	private int countRecordsTotal(DatatableProcessingRequest dataRequest) throws NumberFormatException, SQLException
	{
		// TODO Auto-generated method stub
		QueryBuffer qb = new QueryBuffer();
		qb.append("select count(1) from (");
		qb.append(getBasicQuery(dataRequest));
		qb.append(") as t");
		return Integer.parseInt(TransactionDataAccess.getInstance().selectString(qb));
	}

	private QueryBuffer getBasicQuery(DatatableProcessingRequest dataRequest)
	{
		QueryBuffer qb = new QueryBuffer();
		qb.append("select * from (");
		qb.append(" select ");
		qb.append(" c1.attr_categoryname as company, c1.category_id as id, ");
		qb.append(" ATTRDROP_pricingPlan as plan,ATTRDATE_trialPlanExpiry as expiry,ATTRDROP_Status as status ");
		qb.append(" from categories_users c1  ");
		qb.append(" where c1.folderLevel = 1  ");
		qb.append(" and ( c1.ATTRDROP_Status = 'Cancelled' or (ATTRDROP_pricingPlan = 'trial' and ATTRDATE_trialPlanExpiry < now())) ");
		qb.append(") as data ");
		return qb;
	}

	public class AjaxResponse
	{
		boolean success;
		String message;

		public boolean isSuccess()
		{
			return success;
		}

		public void setSuccess(boolean success)
		{
			this.success = success;
		}

		public String getMessage()
		{
			return message;
		}

		public void setMessage(String message)
		{
			this.message = message;
		}

		public AjaxResponse(boolean success, String message)
		{
			super();
			this.success = success;
			this.message = message;
		}

	}
}
