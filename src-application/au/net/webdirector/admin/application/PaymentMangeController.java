package au.net.webdirector.admin.application;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.ci.webdirector.user.UserManager;
import au.net.webdirector.admin.bulkEdit.domain.datatable.DatatableProcessingRequest;
import au.net.webdirector.admin.bulkEdit.domain.datatable.DatatableProcessingResponse;
import au.net.webdirector.admin.security.UserDAO;
import au.net.webdirector.admin.security.domain.User;
import au.net.webdirector.admin.utils.SecureJSPPath;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.utils.DeepObjectParamParser;
import au.net.webdirector.common.utils.email.FileTemplateEmailBuilder;

import com.csvreader.CsvWriter;

@Controller
@RequestMapping("/secure/app")
public class PaymentMangeController
{

	@RequestMapping(value = "/iframe/payments", method = RequestMethod.GET)
	public String paymentsList(
			HttpServletRequest request,
			HttpServletResponse response)
	{
		return SecureJSPPath.iframePath("payments/list");
	}

	@RequestMapping(value = "/data/payments", method = RequestMethod.POST)
	@ResponseBody
	public DatatableProcessingResponse paymentsData(
			HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session) throws Exception
	{
		DatatableProcessingRequest dataRequest = new DatatableProcessingRequest(request);

		DatatableProcessingResponse<DataMap> res = new DatatableProcessingResponse<DataMap>();

		String processed = DeepObjectParamParser.readString(request, "processed");
		String startDate = DeepObjectParamParser.readString(request, "startDate");
		String endDate = DeepObjectParamParser.readString(request, "endDate");

		res.setDraw(dataRequest.draw);
		int total = countRecordsTotal(dataRequest, processed, startDate, endDate);
		res.setRecordsTotal(total);
		res.setRecordsFiltered(total);
		res.setData(loadData(dataRequest, processed, startDate, endDate));
		return res;
	}

	/**
	 * @param dataRequest
	 * @return
	 * @throws SQLException
	 */
	private List<DataMap> loadData(DatatableProcessingRequest dataRequest, String processed, String startDate, String endDate) throws SQLException
	{
		QueryBuffer qb = getBasicQuery(dataRequest, processed, startDate, endDate);
		if (dataRequest.order.length > 0)
		{
			qb.append(" order by " + dataRequest.columns[dataRequest.order[0].column].data + " " + dataRequest.order[0].dir);
		}
		qb.append(" limit " + dataRequest.start + ", " + dataRequest.length);
		return TransactionDataAccess.getInstance().selectMaps(qb);
	}

	/**
	 * @param dataRequest
	 * @return
	 * @throws SQLException
	 * @throws NumberFormatException
	 */
	private int countRecordsTotal(DatatableProcessingRequest dataRequest, String processed, String startDate, String endDate) throws NumberFormatException, SQLException
	{
		// TODO Auto-generated method stub
		QueryBuffer qb = new QueryBuffer();
		qb.append("select count(1) from (");
		qb.append(getBasicQuery(dataRequest, processed, startDate, endDate));
		qb.append(") as t");
		return Integer.parseInt(TransactionDataAccess.getInstance().selectString(qb));
	}

	private QueryBuffer getBasicQuery(DatatableProcessingRequest dataRequest, String processed, String startDate, String endDate)
	{
		QueryBuffer qb = new QueryBuffer();
		qb.append("select * from (");
		qb.append(" select ");
		qb.append(" element_id,attrdate_processedtime, ");
		qb.append(" if(attrdate_processedtime is null or attrdate_processedtime = '1900-01-01 00:00:00', 'N', 'Y') as processed, ");
		qb.append(" attr_companyname, ");
		qb.append(" attr_headline,attrdate_created, attrdate_availableon,attrcurrency_fee,attrcurrency_gst, ");
		qb.append(" attrcurrency_net, attr_currency ");
		qb.append(" from elements_transactions e ");
		qb.append(" where 1=1 ");
		if (StringUtils.isNotBlank(startDate))
		{
			qb.append(" and date(e.attrdate_created) >= ? ", startDate);
		}
		if (StringUtils.isNotBlank(endDate))
		{
			qb.append(" and date(e.attrdate_created) <= ? ", endDate);
		}
		qb.append(") as data where 1 = 1 ");
		if (StringUtils.isNotBlank(processed))
		{
			qb.append(" and processed = ? ", processed);
		}
		return qb;
	}



	@RequestMapping(value = "/mark/payments", method = RequestMethod.POST)
	@ResponseBody
	public boolean markPayments(
			HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session) throws Exception
	{
		TransactionDataAccess da = TransactionDataAccess.getInstance();
		String categoryId = da.selectString("select category_id from categories_transactions where attr_categoryName = 'processed'");
		String ids = request.getParameter("ids");
		QueryBuffer sql = new QueryBuffer();
		sql.append("update elements_transactions set attrdate_processedtime = now(), category_id = ?", categoryId);
		sql.appendIn(" where element_id in ", Arrays.asList(StringUtils.split(ids, ",")));
		da.update(sql);
		sendEmail(request);
		return true;
	}

	/**
	 * @param ids
	 * @param total
	 * @param gst
	 */
	private void sendEmail(HttpServletRequest request)
	{

		String ids = request.getParameter("ids");
		String total = request.getParameter("total");
		String gst = request.getParameter("gst");

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("total", total);
		params.put("gst", gst);
		UserDAO dao = UserDAO.getInstance();
		User user = dao.getUserByUserName(UserManager.getUserName(request));
		File csv = null;

		try
		{
			csv = createCsv(ids);
			boolean emailSent = new FileTemplateEmailBuilder("/au/net/webdirector/admin/template/", request)
					.addModule("wd_users", user.toHashtable())
					.prepareEmail("paymentProcessed.html", "Processing payment result", params)
					.addTo(user.getEmail())
					.addAttachment(csv)
					.doSend();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			FileUtils.deleteQuietly(csv);
		}

	}

	/**
	 * @param ids
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */
	private File createCsv(String ids) throws SQLException, IOException
	{
		File csv = new File(Defaults.getInstance().getOStypeTempDir(), "PaymentProcessResult.csv");
		QueryBuffer sql = new QueryBuffer();
		sql.append("select e.* from elements_transactions e ");
		sql.appendIn(" where e.element_id in ", Arrays.asList(StringUtils.split(ids, ",")));
		List<ElementData> records = ModuleAccess.getInstance().getElements(sql.getSQL(), sql.getParams());


		CsvWriter writer = new CsvWriter(csv.getAbsolutePath());
		writer.writeRecord(new String[] { "ID", "Customer", "Transaction Time", "Fee", "Received Amount", "GST" });

		for (DataMap record : records)
		{
			List<String> data = new ArrayList<String>();
			data.add(record.getString("element_id"));
			data.add(record.getString("attr_companyname"));
			data.add(record.getString("attrdate_created"));
			data.add(record.getString("attrcurrency_fee"));
			data.add(record.getString("attrcurrency_net"));
			data.add(record.getString("attrcurrency_gst"));
			writer.writeRecord(data.toArray(new String[] {}));
		}

		writer.close();
		return csv;
	}
}
