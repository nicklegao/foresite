/**
 * @author Nick Yiming Gao
 * @date 01/06/2017 4:53:00 pm
 */
package au.corporateinteractive.qcloud.i18n.service;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.TextFile;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.utils.context.ThreadContext;

/**
 *
 */

public class TextMappingService
{

	private static Logger logger = Logger.getLogger(TextMappingService.class);

	private static Map<String, Properties> langs = new HashMap<>();

	private static String[] supportedLangs = new String[] { "en", "fr", "zh" };

	public static HashMap<String, String> sectionPrefix = new HashMap<>();

	static
	{
		loadLanguages();
		loadSectionPrefix();
	}

	private static void loadLanguages()
	{
		for (String lang : supportedLangs)
		{
			try
			{
				langs.put(lang, PropertiesLoaderUtils.loadAllProperties("config/i18n/text-mapping." + lang + ".properties"));
			}
			catch (Exception e)
			{
				logger.error("Error:", e);
			}
		}
	}

	private static void loadSectionPrefix()
	{
		sectionPrefix.put("SYSTEM_COMMON", "General");
		sectionPrefix.put("SYSTEM_MESSAGE", "System Messages");
		sectionPrefix.put("TEXT_PRICING", "Pricing Section");
		sectionPrefix.put("TEXT_CON_PLANS", "Pricing Section");
		sectionPrefix.put("TEXT_PRICING_TERM_TYPE", "Pricing Section");
		sectionPrefix.put("TEXT_COVER", "Cover Page");
		sectionPrefix.put("SYSTEM_SIGN", "E-Sign");
		sectionPrefix.put("SYSTEM_PAYMENT", "qcPay");
		sectionPrefix.put("NO_MAPPING", "All");
	}

	public static boolean isSupported(String lang)
	{
		return ArrayUtils.contains(supportedLangs, lang);
	}

	public static String firstSupported(List<String> langs)
	{
		for (String lang : langs)
		{
			if (isSupported(lang))
			{
				return lang;
			}
		}
		return defaultLang();
	}

	public static String firstSupported(String[] langs)
	{
		return firstSupported(Arrays.asList(langs));
	}

	public static String defaultLang()
	{
		return "en";
	}

	public static void loadLanguange(String lang)
	{
		if (!isSupported(lang))
		{
			lang = defaultLang();
		}
		Properties prop = langs.get(lang);
		Map<String, String> textMapping = new HashMap<String, String>();
		for (Entry entry : prop.entrySet())
		{
			textMapping.put((String) entry.getKey(), (String) entry.getValue());
		}
		ThreadContext.setAttribute("text-mapping", textMapping);
	}

	public static void loadLanguange(HttpServletRequest request, String lang)
	{
		loadLanguange(request, lang, null);
	}

	public static void loadLanguange(HttpServletRequest request, String lang, HashMap<String, String> customMappings)
	{
		if (!isSupported(lang))
		{
			lang = defaultLang();
		}
		Properties prop = langs.get(lang);
		Map<String, String> textMapping = new HashMap<String, String>();
		for (Entry entry : prop.entrySet())
		{
			textMapping.put((String) entry.getKey(), (String) entry.getValue());
			request.setAttribute((String) entry.getKey(), (String) entry.getValue());
		}
		if (customMappings != null)
		{
			for (Entry entry : customMappings.entrySet())
			{
				if (StringUtils.isNotBlank((String) entry.getValue()))
				{
					textMapping.put((String) entry.getKey(), (String) entry.getValue());
					request.setAttribute((String) entry.getKey(), (String) entry.getValue());
				}
			}
		}
		ThreadContext.setAttribute("text-mapping", textMapping);
	}

	public static HashMap<String, String> getCompanyMappings(CategoryData company)
	{
		HashMap<String, String> companyLabels = new HashMap<String, String>();
		TextFile tf = company.getTextFile(UserAccountDataAccess.C1_CUSTOM_LABELS);
		Type mapType = new TypeToken<Map<String, String>>()
		{
		}.getType();
		if (tf != null)
		{
			if (tf.getContent() != null && !"".equals(tf.getContent().trim()))
			{
				companyLabels = new Gson().fromJson(tf.getContent(), mapType);
			}
		}
		return companyLabels;
	}
	
	public static Map<String, String> getCurrentTextMapping()
	{
		return ThreadContext.getAttribute("text-mapping") == null ? new HashMap<String, String>() : (Map<String, String>) ThreadContext.getAttribute("text-mapping");
	}

	public static String translate(String code)
	{
		String word = getCurrentTextMapping().get(code);
		return word == null ? "" : word;
	}
}
