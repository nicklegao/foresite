/**
 * @author Nick Yiming Gao
 * @date 16/06/2017 3:59:58 pm
 */
package au.corporateinteractive.qcloud.proposalbuilder.model;

/**
 * 
 */

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;

public class TableOfContent
{

	private static Logger logger = Logger.getLogger(TableOfContent.class);

	private int counter = 0;

	private int nextCounter()
	{
		return counter++;
	}

	public OL createOL()
	{
		return new OL();
	}

	public LI createLI(String text, LI parent)
	{
		LI li = new LI(text, "heading-anchor-" + nextCounter(), true);
		parent.addLI(li);
		return li;
	}

	public LI createLI(String text, String anchor, OL ol)
	{
		LI li = new LI(text, anchor, false);
		ol.addLI(li);
		return li;
	}

	public class OL
	{
		List<LI> lis = new ArrayList<>();

		private OL()
		{

		}

		private void addLI(LI li)
		{
			this.lis.add(li);
		}

		public String print()
		{
			StringBuffer sb = new StringBuffer();
			sb.append("<ol class=\"toc\">\n");
			for (LI li : lis)
			{
				sb.append(li.print());
			}
			sb.append("</ol>\n");
			return sb.toString();
		}

		private int size()
		{
			return this.lis.size();
		}
	}

	public class LI
	{
		OL ol = new OL();
		String anchor;
		String text;
		boolean encoded;

		public boolean isEncoded()
		{
			return encoded;
		}

		private LI(String text, String anchor, boolean encoded)
		{
			this.text = text;
			this.anchor = anchor;
			this.encoded = encoded;
		}

		/**
		 * @param li
		 */
		public void addLI(LI li)
		{
			this.ol.addLI(li);
		}

		/**
		 * @return
		 */
		public String print()
		{
			StringBuffer sb = new StringBuffer();
			sb.append("<li>\n");
			sb.append("<a href=\"" + this.getHref() + "\" " + (encoded ? "data-encoded=\"true\"" : "") + ">");
			sb.append(this.isEncoded() ? this.getText() : ESAPI.encoder().encodeForHTML(this.getText()));
			sb.append("</a>\n");
			if (ol.size() > 0)
			{

				sb.append(ol.print());
			}
			sb.append("</li>\n");
			return sb.toString();
		}

		public String getText()
		{
			return this.text;
		}

		public String getAnchor()
		{
			return this.anchor;
		}

		public String getHref()
		{
			return "#" + this.anchor;
		}

	}
}
