package au.corporateinteractive.qcloud.proposalbuilder.model;

import java.util.List;

public class AgentModel {

    public List<AgentInformationModel> agents;
    public String queueZRAPIKey;
    public String queueZRAPISecret;

    public List<AgentInformationModel> getAgents() {
        return agents;
    }

    public void setAgents(List<AgentInformationModel> agents) {
        this.agents = agents;
    }

    public String getQueueZRAPIKey() {
        return queueZRAPIKey;
    }

    public void setQueueZRAPIKey(String queueZRAPIKey) {
        this.queueZRAPIKey = queueZRAPIKey;
    }

    public String getQueueZRAPISecret() {
        return queueZRAPISecret;
    }

    public void setQueueZRAPISecret(String queueZRAPISecret) {
        this.queueZRAPISecret = queueZRAPISecret;
    }
}
