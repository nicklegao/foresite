package au.corporateinteractive.qcloud.proposalbuilder.model;

import java.util.List;

public class ContentLibraryResponse {

	private List<ContentLibraryNode> fields;

	public ContentLibraryResponse(List<ContentLibraryNode> fields) {
		this.setFields(fields);
	}

	public List<ContentLibraryNode> getFields() {
		return fields;
	}

	public void setFields(List<ContentLibraryNode> fields) {
		this.fields = fields;
	}
}
