package au.corporateinteractive.qcloud.proposalbuilder.model.fnb;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PlanCategory {
	private int productCategoryId;
	private String categoryTitle;
	private String productDescription;
	private String disclaimers;

	private List<FeatureAndBenefit>			  benefits = new LinkedList<FeatureAndBenefit>();

	private Map<String, List<FeatureAndBenefit>> features = new LinkedHashMap<String, List<FeatureAndBenefit>>();

	public PlanCategory(int productCategoryId, String categoryTitle, String productDescription, String disclaimers) {
		this.productCategoryId = productCategoryId;
		this.categoryTitle = categoryTitle;
		this.productDescription = productDescription;
		this.disclaimers = disclaimers;
	}

	public void addBenefit(FeatureAndBenefit benefit) {
		benefits.add(benefit);
	}

	public void addFeature(String planName, FeatureAndBenefit feature) {
		List<FeatureAndBenefit> planFeatures = features.get(planName);
		if (planFeatures == null)
		{
			planFeatures = new LinkedList<FeatureAndBenefit>();
			features.put(planName, planFeatures);
		}
		planFeatures.add(feature);
	}

	public Map<String, List<FeatureAndBenefit>> getFeatures() {
		return features;
	}

	public void setFeatures(Map<String, List<FeatureAndBenefit>> features) {
		this.features = features;
	}

	public List<FeatureAndBenefit> getBenefits() {
		return benefits;
	}

	public void setBenefits(List<FeatureAndBenefit> benefits) {
		this.benefits = benefits;
	}
	
	public int getProductCategoryId() {
		return productCategoryId;
	}
	
	public void setProductCategoryId(int productCategoryId) {
		this.productCategoryId = productCategoryId;
	}
	
	public String getCategoryTitle() {
		return categoryTitle;
	}
	
	public void setCategoryTitle(String categoryTitle) {
		this.categoryTitle = categoryTitle;
	}

  public String getProductDescription() {
    return productDescription;
  }

  public void setProductDescription(String productDescription) {
    this.productDescription = productDescription;
  }

  public String getDisclaimers() {
    return disclaimers;
  }
  
  public void setDisclaimers(String disclaimers) {
    this.disclaimers = disclaimers;
  }
  
  public String getProductionDescriptionHtml() {
    if (productDescription == null) {
      return "";
    }
    return productDescription.replaceAll("\r|\n|\r\n", "<br/>");
  }
  
  public String getDisclaimersHtml() {
    if (disclaimers == null) {
      return "";
    }
    return disclaimers.replaceAll("\r|\n|\r\n", "<br/>");
  }
	
}
