package au.corporateinteractive.qcloud.proposalbuilder.model.fnb;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

public class FeatureAndBenefit
{
	private static Logger logger = Logger.getLogger(FeatureAndBenefit.class);

	public static enum Type
	{
		FEATURE,
		BENEFIT
	}

	private String id;
	private String associatedId;
	private String title;
	private String contents;
	private Type type;

	public FeatureAndBenefit(String id, String associatedId, String title, String contents, Type type)
	{
		this.id = id;
		this.associatedId = associatedId;
		this.title = title;
		this.contents = contents;
		this.type = type;
	}

	public static FeatureAndBenefit createFeatureFromNote(Hashtable<String, String> note, String planId)
	{
		String id = note.get("id");
		NoteContents parsed = new NoteContents(note.get("content"));

		return createFeature(id, planId, parsed.note);
	}

	public static FeatureAndBenefit createBenefitFromNote(Hashtable<String, String> note, String categoryId)
	{
		String id = note.get("id");
		NoteContents parsed = new NoteContents(note.get("content"));
		return createBenefit(id, categoryId, parsed.title, parsed.contents);
	}

	public static FeatureAndBenefit createFeature(String id, String planId, String contents)
	{
		return new FeatureAndBenefit(id, planId, "", contents, Type.FEATURE);
	}

	public static FeatureAndBenefit createBenefit(String id, String categoryId, String title, String contents)
	{
		return new FeatureAndBenefit(id, categoryId, title, contents, Type.BENEFIT);
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getAssociatedId()
	{
		return associatedId;
	}

	public void setAssociatedId(String associatedId)
	{
		this.associatedId = associatedId;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getContents()
	{
		return contents;
	}

	public String getContentsForPdf()
	{
		if (contents != null)
		{
			return contents.replaceAll("\r\n|\r|\n", "<br/>");
		}
		return contents;
	}

	public List<String[]> getContentGroupsByPipe()
	{
		List<String[]> results = new ArrayList<String[]>();
		if (contents == null || "".equals(contents))
		{
			return results;
		}
		String[] groups = contents.split("\\|");
		for (String group : groups)
		{
			group = group.trim();
			if ("".equals(group))
			{
				continue;
			}
			int index = group.indexOf(" ");
			String[] result = new String[2];
			if (index > 0)
			{
				result[0] = group.substring(0, index);
				result[1] = group.substring(index);
			}
			else
			{
				result[0] = group;
				result[1] = "";
			}
			results.add(result);
		}
		return results;
	}

	public void setContents(String contents)
	{
		this.contents = contents;
	}

	public Type getType()
	{
		return type;
	}

	public void setType(Type type)
	{
		this.type = type;
	}
}
