package au.corporateinteractive.qcloud.proposalbuilder.model.fnb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

import org.apache.log4j.Logger;

public class NoteContents
{
	private static Logger logger = Logger.getLogger(NoteContents.class);

	public final String title;
	public final String contents;
	public final String note;

	public NoteContents(String content)
	{
		//
		String t = "";
		String c = "";
		try
		{
			StringBuffer sb = new StringBuffer();
			BufferedReader br = new BufferedReader(new StringReader(content));
			t = br.readLine();
			String line = null;

			while ((line = br.readLine()) != null)
			{
				if (sb.length() != 0)
				{
					sb.append("\n");
				}
				sb.append(line);
			}
			c = sb.toString();
			br.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		this.title = t.trim();
		this.contents = c.trim();
		if (this.contents.length() > 0)
		{
			this.note = this.title + "\n" + this.contents;
		}
		else
		{
			this.note = this.title;
		}
	}
}
