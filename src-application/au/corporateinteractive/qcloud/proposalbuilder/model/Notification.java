///**
// *
// */
//package au.corporateinteractive.qcloud.proposalbuilder.model;
//
//import java.util.Date;
//
//import au.corporateinteractive.qcloud.proposalbuilder.db.NotificationDataAccess;
//import au.net.webdirector.common.datalayer.client.ElementData;
//import webdirector.db.client.ClientFileUtils;
//
///**
// * @author Alberto
// *
// */
//public class Notification
//{
//	private String id;
//	private String title;
//	private String content;
//	private String type;
//	private String occurrence;
//	private Date liveDate;
//	private Date expiryDate;
//
//	public Notification(ElementData elementData)
//	{
//		ClientFileUtils clientFileUtils = new ClientFileUtils();
//
//		this.id = elementData.getId();
//		this.title = elementData.getName();
//		this.content = clientFileUtils.readTextContents(clientFileUtils.getFilePathFromStoreName(elementData.getString(NotificationDataAccess.E_CONTENT))).toString();
//		this.type = elementData.getString(NotificationDataAccess.E_TYPE);
//		this.occurrence = elementData.getString(NotificationDataAccess.E_OCCURRENCE);
//		this.liveDate = elementData.getLiveDate();
//		this.expiryDate = elementData.getExpireDate();
//	}
//
//	/**
//	 * @return the id
//	 */
//	public String getId()
//	{
//		return id;
//	}
//
//	/**
//	 * @param id
//	 *            the id to set
//	 */
//	public void setId(String id)
//	{
//		this.id = id;
//	}
//
//	/**
//	 * @return the title
//	 */
//	public String getTitle()
//	{
//		return title;
//	}
//
//	/**
//	 * @param title
//	 *            the title to set
//	 */
//	public void setTitle(String title)
//	{
//		this.title = title;
//	}
//
//	/**
//	 * @return the content
//	 */
//	public String getContent()
//	{
//		return content;
//	}
//
//	/**
//	 * @param content
//	 *            the content to set
//	 */
//	public void setContent(String content)
//	{
//		this.content = content;
//	}
//
//	/**
//	 * @return the type
//	 */
//	public String getType()
//	{
//		return type;
//	}
//
//	/**
//	 * @param type
//	 *            the type to set
//	 */
//	public void setType(String type)
//	{
//		this.type = type;
//	}
//
//
//	/**
//	 * @return the occurrence
//	 */
//	public String getOccurrence()
//	{
//		return occurrence;
//	}
//
//	/**
//	 * @param occurrence
//	 *            the occurrence to set
//	 */
//	public void setOccurrence(String occurrence)
//	{
//		this.occurrence = occurrence;
//	}
//
//	/**
//	 * @return the liveDate
//	 */
//	public Date getLiveDate()
//	{
//		return liveDate;
//	}
//
//	/**
//	 * @param liveDate
//	 *            the liveDate to set
//	 */
//	public void setLiveDate(Date liveDate)
//	{
//		this.liveDate = liveDate;
//	}
//
//	/**
//	 * @return the expiryDate
//	 */
//	public Date getExpiryDate()
//	{
//		return expiryDate;
//	}
//
//	/**
//	 * @param expiryDate
//	 *            the expiryDate to set
//	 */
//	public void setExpiryDate(Date expiryDate)
//	{
//		this.expiryDate = expiryDate;
//	}
//
//
//}
