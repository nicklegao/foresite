package au.corporateinteractive.qcloud.proposalbuilder.model;

public class MobileUserAuth {
    private boolean valid;
    private String message;
    private String username;
    private String userID;

    public MobileUserAuth(boolean valid, String message, String username, String userID)
    {
        this.valid = valid;
        this.message = message;
        this.username = username;
        this.userID = userID;
    }

    public boolean isValid() {
        return valid;
    }

    public String getMessage() {
        return message;
    }

    public String getUsername() {
        return username;
    }

    public String getUserID() {
        return userID;
    }
}
