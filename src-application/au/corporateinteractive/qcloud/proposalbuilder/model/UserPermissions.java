package au.corporateinteractive.qcloud.proposalbuilder.model;

import java.util.Hashtable;

import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess;

public class UserPermissions
{
	private String userId;
	private Hashtable<String, Boolean> permissions;

	public UserPermissions(String _userId, Hashtable<String, Boolean> _ht)
	{
		userId = _userId;
		permissions = _ht;
	}

	public String getUser()
	{
		return userId;
	}


	public boolean hasManageProjectsPermission()
	{
		return permissions.get(TUserRolesDataAccess.MANAGE_PROJECTS);
	}
	
//	public boolean hasManageTemplatesPermission()
//	{
//		return permissions.get(TUserRolesDataAccess.MANAGE_TEMPLATES);
//	}

	public boolean hasManageUsersPermission()
	{
		return permissions.get(TUserRolesDataAccess.MANAGE_USERS);
	}

	public boolean hasManageSettingsPermission()
	{
		return permissions.get(TUserRolesDataAccess.MANAGE_SETTINGS);
	}

	
}
