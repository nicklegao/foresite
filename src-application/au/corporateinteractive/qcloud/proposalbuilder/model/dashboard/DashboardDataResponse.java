package au.corporateinteractive.qcloud.proposalbuilder.model.dashboard;

import java.util.List;

import au.net.webdirector.common.datalayer.base.db.DataMap;

public class DashboardDataResponse
{
	private int draw;
	private int recordsTotal;
	private int recordsFiltered;
	private List<DataMap> data;

	public DashboardDataResponse(int draw, int total, int filtered, List<DataMap> data)
	{
		this.draw = draw;
		this.recordsTotal = total;
		this.recordsFiltered = filtered;
		this.data = data;
	}

	public int getDraw()
	{
		return draw;
	}

	public void setDraw(int draw)
	{
		this.draw = draw;
	}

	public int getRecordsTotal()
	{
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal)
	{
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered()
	{
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered)
	{
		this.recordsFiltered = recordsFiltered;
	}

	public List<DataMap> getData()
	{
		return data;
	}

	public void setData(List<DataMap> data)
	{
		this.data = data;
	}
}
