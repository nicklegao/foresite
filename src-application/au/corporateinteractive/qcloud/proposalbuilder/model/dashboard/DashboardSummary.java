package au.corporateinteractive.qcloud.proposalbuilder.model.dashboard;

public class DashboardSummary {
	public int total       = 0;
	
    public int draft    = 0;
    public int notstarted = 0;
    public int wip = 0;
    public int complete    = 0;
    public int incomplete	   = 0;
    public int unknown     = 0;

    public void calculateTotal() {
    	// Doesn't include DashboardSummary.unknown
        total = draft + notstarted + wip + complete + incomplete;
    }
}
