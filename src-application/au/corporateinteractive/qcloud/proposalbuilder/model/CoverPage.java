package au.corporateinteractive.qcloud.proposalbuilder.model;

import org.apache.commons.lang.StringUtils;

public class CoverPage
{

	private String id = "";

	private String thumbnail;
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getThumbnail()
	{
		return thumbnail;
	}

	public void setThumbnail(String thumbnail)
	{
		this.thumbnail = thumbnail;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	@Override
	public boolean equals(Object another)
	{
		if (!(another instanceof CoverPage))
		{
			return false;
		}
		return StringUtils.equals(this.id, ((CoverPage) another).getId());
	}
}
