package au.corporateinteractive.qcloud.proposalbuilder.model;

import java.util.List;

public class AgentInformationModel {
    public String id;
    public String agentName;
    public String agentCode;
    public String customerCode;
    public Boolean autoSend;
    public String templateID;
    public String phoneNumber;
    public List<String> homeCountries;
    public List<String> invalidCountries;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public Boolean getAutoSend() {
        if (autoSend == null) {
            return false;
        }
        return autoSend;
    }

    public void setAutoSend(Boolean autoSend) {
        this.autoSend = autoSend;
    }

    public String getTemplateID() {
        return templateID;
    }

    public void setTemplateID(String templateID) {
        this.templateID = templateID;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<String> getHomeCountries() {
        return homeCountries;
    }

    public void setHomeCountry(List<String> homeCountries) {
        this.homeCountries = homeCountries;
    }

    public List<String> getInvalidCountries() {
        return invalidCountries;
    }

    public void setInvalidCountries(List<String> invalidCountries) {
        this.invalidCountries = invalidCountries;
    }
}
