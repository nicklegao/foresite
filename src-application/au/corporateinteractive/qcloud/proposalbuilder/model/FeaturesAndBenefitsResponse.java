//package au.corporateinteractive.qcloud.proposalbuilder.model;
//
//import java.util.Hashtable;
//import java.util.LinkedHashMap;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Map;
//import java.util.Vector;
//
////import au.corporateinteractive.qcloud.proposalbuilder.db.PlanDataAccess;
//import au.corporateinteractive.qcloud.proposalbuilder.model.fnb.FeatureAndBenefit;
//import au.corporateinteractive.qcloud.proposalbuilder.model.fnb.PlanCategory;
//import webdirector.db.client.ClientDataAccess;
//import webdirector.db.client.ClientFileUtils;
//
//public class FeaturesAndBenefitsResponse {
//	private List<PlanCategory> categories = new LinkedList<PlanCategory>();
//	private Map<String, PlanCategory> categoryMap = new LinkedHashMap<String, PlanCategory>();
//
//	public void addFeature(String planCategoryName, String planCategoryId, String planName, FeatureAndBenefit feature) {
//		PlanCategory planCategory = categoryMap.get(planCategoryId);
//		if (planCategory == null)
//		{
//		  planCategory = createPlanCategory(planCategoryName, planCategoryId);
//			categoryMap.put(planCategoryId, planCategory);
//			categories.add(planCategory);
//		}
//		planCategory.addFeature(planName, feature);
//	}
//
//	public void addBenefits(String planCategoryName, String planCategoryId, FeatureAndBenefit benefit) {
//		PlanCategory planCategory = categoryMap.get(planCategoryId);
//		if (planCategory == null)
//		{
//			planCategory = createPlanCategory(planCategoryName, planCategoryId);
//			categoryMap.put(planCategoryId, planCategory);
//			categories.add(planCategory);
//		}
//		planCategory.addBenefit(benefit);
//	}
//
//	PlanCategory createPlanCategory(String planCategoryName, String planCategoryId) {
//    PlanCategory planCategory;
//    ClientDataAccess cda = new ClientDataAccess();
//    Vector<Hashtable<String, String>> categoryVt = cda.getCategory(PlanDataAccess.PLANS_MODULE, planCategoryId, true);
//    String productDescription = "";
//    String disclaimers = "";
//    if (categoryVt != null && categoryVt.size() > 0) {
//      Hashtable<String, String> categoryHt = categoryVt.get(0);
//      ClientFileUtils cfu = new ClientFileUtils();
//      productDescription = cfu.readTextContents(cfu.getFilePathFromStoreName(categoryHt.get("ATTRTEXT_productDescription")), true).toString();
//      disclaimers = cfu.readTextContents(cfu.getFilePathFromStoreName(categoryHt.get("ATTRTEXT_disclaimers")), true).toString();
//    }
//    planCategory = new PlanCategory(Integer.parseInt(planCategoryId), planCategoryName, productDescription, disclaimers);
//    return planCategory;
//  }
//
//	public List<PlanCategory> getCategories() {
//		return categories;
//	}
//
//	public void setCategories(List<PlanCategory> categories) {
//		this.categories = categories;
//	}
//
//	public boolean hasProductCategoryBeenProcessed(String searchProductCategory) {
//		return hasProductCategoryBeenProcessed(Integer.parseInt(searchProductCategory));
//	}
//
//	public boolean hasProductCategoryBeenProcessed(int searchProductCategory) {
//		for (PlanCategory c : categories)
//		{
//			if (c.getProductCategoryId() == searchProductCategory)
//			{
//				return true;
//			}
//		}
//		return false;
//	}
//}
