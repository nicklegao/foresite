package au.corporateinteractive.qcloud.proposalbuilder.model;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import au.corporateinteractive.qcloud.proposalbuilder.model.uc.UpSellCrossSell;

public class UCResponse {
	private Map<String, List<UpSellCrossSell>> matches = new LinkedHashMap<String, List<UpSellCrossSell>>();

	public void add(String planName, UpSellCrossSell createFromCategory) {
		List<UpSellCrossSell> planFeatures = matches.get(planName);
		if (planFeatures == null)
		{
			planFeatures = new LinkedList<UpSellCrossSell>();
			matches.put(planName, planFeatures);
		}
		planFeatures.add(createFromCategory);
	}

	public Map<String, List<UpSellCrossSell>> getMatches() {
		return matches;
	}

	public void setMatches(Map<String, List<UpSellCrossSell>> matches) {
		this.matches = matches;
	}
}
