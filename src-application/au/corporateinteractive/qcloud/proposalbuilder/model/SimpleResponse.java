package au.corporateinteractive.qcloud.proposalbuilder.model;

public class SimpleResponse {
	private boolean success;
	private int dataId;
	private String message;

	public SimpleResponse(int dataId) {
		this.success = true;
		this.dataId = dataId;
	}
	
	public SimpleResponse(String message) {
		this.success = false;
		this.message = message;
	}
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public int getDataId() {
		return dataId;
	}
	public void setDataId(int dataId) {
		this.dataId = dataId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
