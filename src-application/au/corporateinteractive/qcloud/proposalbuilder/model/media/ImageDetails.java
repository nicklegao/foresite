package au.corporateinteractive.qcloud.proposalbuilder.model.media;

public class ImageDetails
{

	private String id;
	private String title;
	private String path;
	private String caption;
	private String description;
	private boolean displayTitle;

	public ImageDetails(String id, String title, String path, String caption, String description, boolean displayTitle)
	{
		this.id = id;
		this.title = title;
		this.path = path;
		this.caption = caption;
		this.description = description;
		this.displayTitle = displayTitle;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getPath()
	{
		return path;
	}

	public void setPath(String path)
	{
		this.path = path;
	}

	public String getCaption()
	{
		return caption;
	}

	public void setCaption(String caption)
	{
		this.caption = caption;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public boolean isDisplayTitle()
	{
		return displayTitle;
	}

	public void setDisplayTitle(boolean displayTitle)
	{
		this.displayTitle = displayTitle;
	}

	public String getEncodedPath()
	{
		if (path == null)
		{
			return "";
		}
		return path.replace(" ", "%20");
	}

}
