package au.corporateinteractive.qcloud.proposalbuilder.model.media;

public class ProductDetails {
	
	private String id;
	private String title;
	private String model;
	private String description;
	private float price;
	private String path;
	
	public ProductDetails(String id, String title, String model,
			String description, float price, String path) {
		this.id = id;
		this.title = title;
		this.model = model;
		this.description = description;
		this.price = price;
		this.path = path;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	
}
