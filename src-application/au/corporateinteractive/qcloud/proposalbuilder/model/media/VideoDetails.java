package au.corporateinteractive.qcloud.proposalbuilder.model.media;

public class VideoDetails {

	private String id;
	private String title;
	private String code;
	private String caption;
	private String source;
	private String thumbnail;
	private String link;

	public VideoDetails(String id, String title, String code, String caption, String source, String thumbnail, String link) {
		this.id = id;
		this.title = title;
		this.code = code;
		this.caption = caption;
		this.source = source;
		this.thumbnail = thumbnail;
		this.link = link;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}
