package au.corporateinteractive.qcloud.proposalbuilder.model.media;

import au.corporateinteractive.utils.PDFUtils;

public class TextDetails {

	private String id;
	private String data;
	private String template;
	private boolean lockEnabled;

	public TextDetails(String id, String data, String template, boolean lockEnabled) {
		this.id = id;
		this.data = data;
		this.template = template;
		this.lockEnabled = lockEnabled;
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	public String getTemplate() {
	  return template;
	}
	
	public void setTemplate(String template) {
	  this.template = template;
	}
	
	public boolean isLockEnabled() {
	  return lockEnabled;
	}
	
	public void setLockEnabled(boolean lockEnabled) {
	  this.lockEnabled = lockEnabled;
	}
	
	public String getDataForPdf() {
		return PDFUtils.formatTextblock(data);
	}
}
