package au.corporateinteractive.qcloud.proposalbuilder.model.media;

import au.corporateinteractive.utils.PDFUtils;

public class PDFDetails {

	private String id;
	private String title;
	private String data;
	private String description;

	public PDFDetails(String id, String title, String data, String description) {
		this.id = id;
		this.title = title;
		this.data = data;
		this.description = description;
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	public String getDescription() {
	  return description;
	}
	
	public void setDescription(String description) {
	  this.description = description;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDataForPdf() {
		return PDFUtils.formatTextblock(data);
	}
}
