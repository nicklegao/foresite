package au.corporateinteractive.qcloud.proposalbuilder.model.bill;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.stripe.model.Invoice;

public class TDInvoice
{
	private static Logger logger = Logger.getLogger(TDInvoice.class);

	Invoice invoice;

	Date start;

	Date end;

	List<BookingSummary> summary;

	public Invoice getInvoice()
	{
		return invoice;
	}

	public Date getStart()
	{
		return start;
	}

	public Date getEnd()
	{
		return end;
	}

	public List<BookingSummary> getSummary()
	{
		return summary;
	}

	public TDInvoice()
	{

	}

	public TDInvoice(Invoice invoice, Date start, Date end, List<BookingSummary> summary)
	{
		super();
		this.invoice = invoice;
		this.start = start;
		this.end = end;
		this.summary = summary;
	}

	public Long getTotal()
	{
		return this.invoice.getTotal();
	}

	public String getCurrency()
	{
		return this.invoice.getCurrency();
	}

	public String getId()
	{
		return this.invoice.getId();
	}
}
