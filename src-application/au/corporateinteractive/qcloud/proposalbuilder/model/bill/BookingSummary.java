package au.corporateinteractive.qcloud.proposalbuilder.model.bill;

import org.apache.log4j.Logger;

public class BookingSummary
{
	private static Logger logger = Logger.getLogger(BookingSummary.class);

	String gds;
	String agent;
	String consultant;
	int amount;

	public String getGds()
	{
		return gds;
	}

	public String getAgent()
	{
		return agent;
	}

	public String getConsultant()
	{
		return consultant;
	}

	public int getAmount()
	{
		return amount;
	}

	public BookingSummary(String gds, String agent, String consultant, int amount)
	{
		super();
		this.gds = gds;
		this.agent = agent;
		this.consultant = consultant;
		this.amount = amount;
	}

	public BookingSummary()
	{
		super();
	}

}
