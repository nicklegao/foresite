package au.corporateinteractive.qcloud.proposalbuilder.model;

import org.apache.commons.lang.StringUtils;

public class ContentLibraryNode
{
	private String type;

	private String icon;

	private String id = "";

	private String label;

	private boolean leaf;

	private boolean hasChild;

	private String thumbnail;

	private double price;

	public String getThumbnail()
	{
		return thumbnail;
	}

	public void setThumbnail(String thumbnail)
	{
		this.thumbnail = thumbnail;
	}

	public boolean isHasChild()
	{
		return hasChild;
	}

	public void setHasChild(boolean hasChild)
	{
		this.hasChild = hasChild;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getLabel()
	{
		return label;
	}

	public void setLabel(String label)
	{
		this.label = label;
	}

	public boolean isLeaf()
	{
		return leaf;
	}

	public void setLeaf(boolean leaf)
	{
		this.leaf = leaf;
	}


	public double getPrice()
	{
		return price;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	public String getIcon()
	{
		return icon;
	}

	public void setIcon(String icon)
	{
		this.icon = icon;
	}

	@Override
	public int hashCode()
	{
		return label.hashCode();
	}

	@Override
	public boolean equals(Object another)
	{
		if (!(another instanceof ContentLibraryNode))
		{
			return false;
		}
		return StringUtils.equals(this.label, ((ContentLibraryNode) another).getLabel());
	}

	public void addId(String newId)
	{
		if (StringUtils.isBlank(id))
		{
			this.id = newId;
			return;
		}
		this.id += "," + newId;
	}
}
