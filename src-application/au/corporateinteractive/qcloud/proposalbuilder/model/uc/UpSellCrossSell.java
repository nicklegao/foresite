package au.corporateinteractive.qcloud.proposalbuilder.model.uc;

import java.util.Hashtable;

//import au.corporateinteractive.qcloud.proposalbuilder.db.PlanDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.utils.MoneyUtils;
import au.corporateinteractive.qcloud.proposalbuilder.utils.MoneyUtils.DisplayableMoney;
import au.net.webdirector.common.utils.module.StoreUtils;

public class UpSellCrossSell
{
	private String id;
	private String title;

	private double unitPrice;
	private String contents;
	private boolean isOneOff;

//	public static UpSellCrossSell createFromCategory(Hashtable<String, String> ht, Hashtable<String, String> pricingHT)
//	{
//		UpSellCrossSell uc = new UpSellCrossSell();
//		uc.id = ht.get(PlanDataAccess.C_ID);
//		uc.title = ht.get(PlanDataAccess.C_NAME);
//
//		String contentsLocation = pricingHT.get(PlanDataAccess.E_DETAILED_DESCRIPTION);
//		if (contentsLocation != null && contentsLocation.length() > 0)
//		{
//			uc.contents = new StoreUtils().readFieldFromStores(pricingHT.get(PlanDataAccess.E_DETAILED_DESCRIPTION));
//		}
//		else
//		{
//			uc.contents = "";
//		}
//		uc.unitPrice = Double.parseDouble(pricingHT.get(PlanDataAccess.E_COST));
//		uc.isOneOff = Integer.parseInt(pricingHT.get(PlanDataAccess.E_ONE_OFF)) > 0;
//		return uc;
//	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public double getUnitPrice()
	{
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice)
	{
		this.unitPrice = unitPrice;
	}

	public String getContents()
	{
		return contents;
	}

	public String getContentsForPdf()
	{
		if (contents != null)
		{
			return contents.replaceAll("\r\n|\r|\n", "<br/>");
		}
		return contents;
	}

	public void setContents(String contents)
	{
		this.contents = contents;
	}

	public boolean getIsOneOff()
	{
		return isOneOff;
	}

	public void setIsOneOff(boolean isOneOff)
	{
		this.isOneOff = isOneOff;
	}


	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public DisplayableMoney unitPriceForPdf()
	{
		return MoneyUtils.toDisplayableMoney(this.unitPrice);
	}
}
