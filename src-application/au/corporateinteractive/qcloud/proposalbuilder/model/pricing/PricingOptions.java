//package au.corporateinteractive.qcloud.proposalbuilder.model.pricing;
//
//import java.util.LinkedHashMap;
//import java.util.Map;
//
////import au.corporateinteractive.qcloud.proposalbuilder.db.PlanDataAccess;
//import au.net.webdirector.common.datalayer.client.ElementData;
//
//public class PricingOptions
//{
//	private String label;
//	private Map<Integer, PricingDetails> terms = new LinkedHashMap<Integer, PricingDetails>();
//
//	public PricingOptions(ElementData price)
//	{
//		label = price.getString(PlanDataAccess.E_HEADLINE);
//
//		addTermPricing(price);
//	}
//
//	public String getLabel()
//	{
//		return label;
//	}
//
//	public void setLabel(String label)
//	{
//		this.label = label;
//	}
//
//	public Map<Integer, PricingDetails> getTerms()
//	{
//		return terms;
//	}
//
//	public void setTerms(Map<Integer, PricingDetails> terms)
//	{
//		this.terms = terms;
//	}
//
//	public void addTermPricing(int id, PricingDetails detail)
//	{
//		terms.put(id, detail);
//	}
//
//	public void addTermPricing(ElementData price)
//	{
//		int elementId = Integer.parseInt(price.getString(PlanDataAccess.E_ID));
//		float cost = Float.parseFloat(price.getString(PlanDataAccess.E_COST));
//		int term = Integer.parseInt(price.getString(PlanDataAccess.E_TERM));
//		boolean isOneOff = Integer.parseInt(price.getString(PlanDataAccess.E_ONE_OFF)) > 0;
//
//		addTermPricing(elementId, new PricingDetails(elementId, cost, term, isOneOff));
//	}
//
//	public static class PricingDetails
//	{
//
//		private int id;
//		private float cost;
//		private int term;
//		private boolean oneOff;
//
//		public PricingDetails(int id, float cost, int term, boolean oneOff)
//		{
//			this.id = id;
//			this.cost = cost;
//			this.term = term;
//			this.oneOff = oneOff;
//		}
//
//		public int getId()
//		{
//			return id;
//		}
//
//		public void setId(int id)
//		{
//			this.id = id;
//		}
//
//		public float getCost()
//		{
//			return cost;
//		}
//
//		public void setCost(float cost)
//		{
//			this.cost = cost;
//		}
//
//		public int getTerm()
//		{
//			return term;
//		}
//
//		public void setTerm(int term)
//		{
//			this.term = term;
//		}
//
//		public boolean getOneOff()
//		{
//			return oneOff;
//		}
//
//		public void setOneOff(boolean oneOff)
//		{
//			this.oneOff = oneOff;
//		}
//	}
//}
