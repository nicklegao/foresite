/**
 * 
 */
package au.corporateinteractive.qcloud.proposalbuilder.model;

/**
 * @author Alberto
 *
 */
public class AttachmentResponse
{
	private int fileId;
	private boolean success;
	private String name;
	private String link;

	public AttachmentResponse(int fileId, String name, boolean success)
	{
		this.fileId = fileId;
		this.name = name;
		this.success = success;
	}

    public AttachmentResponse(int fileId, String name, String link, boolean success)
    {
        this.fileId = fileId;
        this.name = name;
        this.link = link;
        this.success = success;
    }

	public AttachmentResponse(String name, boolean success)
	{
		this.name = name;
		this.success = success;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	public int getFileId()
	{
		return fileId;
	}

	public void setFileId(int fileId)
	{
		this.fileId = fileId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getLink()
	{
		return link;
	}

	public void setLink(String link)
	{
		this.link = link;
	}
}