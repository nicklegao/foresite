package au.corporateinteractive.qcloud.proposalbuilder.model.email;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

public class HousekeepingSummary {
	private List<Hashtable<String, String>> expired = new LinkedList<Hashtable<String,String>>();
	private List<Hashtable<String, String>> expiringSoon = new LinkedList<Hashtable<String,String>>();

	public void addExpired(Hashtable<String, String> ht) {
		this.expired.add(ht);
	}

	public void addExpiringSoon(Hashtable<String, String> ht) {
		this.expiringSoon.add(ht);
	}
	
	public List<Hashtable<String, String>> getExpired() {
		return expired;
	}
	
	public List<Hashtable<String, String>> getExpiringSoon() {
		return expiringSoon;
	}
	
	public int getExpiredCount() {
		return expired.size();
	}
	
	public int getExpiringSoonCount() {
		return expiringSoon.size();
	}
	
	public int getTotalCount() {
		return getExpiredCount() + getExpiringSoonCount();
	}
}
