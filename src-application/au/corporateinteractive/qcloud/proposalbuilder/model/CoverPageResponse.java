package au.corporateinteractive.qcloud.proposalbuilder.model;

import java.util.List;

public class CoverPageResponse
{
	private List<CoverPage> pages;

	public CoverPageResponse(List<CoverPage> pages)
	{
		this.setPages(pages);
	}

	public List<CoverPage> getPages()
	{
		return pages;
	}

	public void setPages(List<CoverPage> pages)
	{
		this.pages = pages;
	}
}
