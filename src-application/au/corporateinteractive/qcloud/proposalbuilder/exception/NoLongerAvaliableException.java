package au.corporateinteractive.qcloud.proposalbuilder.exception;

public class NoLongerAvaliableException extends Exception {
	private static final long serialVersionUID = -4517766026350995233L;
	
	private String contentType;

	public NoLongerAvaliableException(String contentType) {
		this.setContentType(contentType);
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}
