package au.corporateinteractive.qcloud.proposalbuilder.control;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.InvoiceDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.net.webdirector.admin.bulkEdit.domain.datatable.DatatableProcessingRequest;
import au.net.webdirector.admin.bulkEdit.domain.datatable.DatatableProcessingResponse;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.ModuleHelper;
import au.net.webdirector.common.utils.DeepObjectParamParser;

@Controller
public class InvoiceController
{

	private static Logger logger = Logger.getLogger(InvoiceController.class);

	@RequestMapping(value = "/invoices/download")
	public void download(
			HttpServletResponse response,
			HttpSession session, @RequestParam String inv) throws Exception
	{

		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		CategoryData company = new UserAccountDataAccess().getUserCompanyForUserId(userId);
		String categoryId = TransactionDataAccess.getInstance().selectString("select category_id from categories_invoices where attrdrop_companyId = ?", company.getId());

		ElementData invoiceData;
		try
		{
			invoiceData = ModuleAccess.getInstance().getElements("select * from elements_invoices where category_id = ? and element_id = ?", categoryId, inv).get(0);
		}
		catch (Exception e)
		{
			logger.error("Error", e);
			response.getOutputStream().print("Cannot lookup invoice id = " + inv);
			response.setStatus(404);
			response.flushBuffer();
			return;
		}
		if (StringUtils.isBlank(invoiceData.getString(InvoiceDataAccess.E_INVOICE_PDF)))
		{
			response.getOutputStream().print("Invoice id = " + inv + " isn't ready for download.");
			response.setStatus(404);
			response.flushBuffer();
			return;
		}

		InputStream in = null;
		try
		{
			File pdf = ModuleHelper.getStoredFile(invoiceData.getString(InvoiceDataAccess.E_INVOICE_PDF));
			if (!pdf.exists() || !pdf.isFile())
			{
				response.getOutputStream().print("Invoice id = " + inv + " pdf is missing.");
				response.setStatus(404);
				response.flushBuffer();
				return;
			}
			String contentDisposition = String.format(
					"inline;filename=\"%s\"",
					pdf.getName());
			response.setHeader("Content-Disposition", contentDisposition);
			response.setContentType("application/pdf");
			in = new FileInputStream(pdf);
			IOUtils.copy(in, response.getOutputStream());
			response.flushBuffer();
		}
		catch (Exception e)
		{
			logger.error("Error", e);
			IOUtils.closeQuietly(in);
		}
		logger.info("End generating pdf for invoice: " + inv);
	}

	@RequestMapping(value = "/invoices/data", method = RequestMethod.POST)
	@ResponseBody
	public DatatableProcessingResponse<DataMap> paymentsData(
			HttpServletRequest request,
			HttpSession session) throws Exception
	{

		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		CategoryData company = new UserAccountDataAccess().getUserCompanyForUserId(userId);
		String categoryId = TransactionDataAccess.getInstance().selectString("select category_id from categories_invoices where attrdrop_companyId = ?", company.getId());
		categoryId = categoryId == null ? "0" : categoryId;

		DatatableProcessingRequest dataRequest = new DatatableProcessingRequest(request);

		DatatableProcessingResponse<DataMap> res = new DatatableProcessingResponse<DataMap>();

		String startDate = DeepObjectParamParser.readString(request, "startDate");
		String endDate = DeepObjectParamParser.readString(request, "endDate");

		res.setDraw(dataRequest.draw);
		int total = countRecordsTotal(dataRequest, categoryId, startDate, endDate);
		res.setRecordsTotal(total);
		res.setRecordsFiltered(total);
		res.setData(loadData(dataRequest, categoryId, startDate, endDate));
		return res;
	}

	/**
	 * @param dataRequest
	 * @return
	 * @throws SQLException
	 */
	private List<DataMap> loadData(DatatableProcessingRequest dataRequest, String categoryId, String startDate, String endDate) throws SQLException
	{
		QueryBuffer qb = getBasicQuery(dataRequest, categoryId, startDate, endDate);
		if (dataRequest.order.length > 0)
		{
			qb.append(" order by " + dataRequest.columns[dataRequest.order[0].column].data + " " + dataRequest.order[0].dir);
		}
		qb.append(" limit " + dataRequest.start + ", " + dataRequest.length);
		return TransactionDataAccess.getInstance().selectMaps(qb);
	}

	/**
	 * @param dataRequest
	 * @return
	 * @throws SQLException
	 * @throws NumberFormatException
	 */
	private int countRecordsTotal(DatatableProcessingRequest dataRequest, String categoryId, String startDate, String endDate) throws NumberFormatException, SQLException
	{
		// TODO Auto-generated method stub
		QueryBuffer qb = new QueryBuffer();
		qb.append("select count(1) from (");
		qb.append(getBasicQuery(dataRequest, categoryId, startDate, endDate));
		qb.append(") as t");
		return Integer.parseInt(TransactionDataAccess.getInstance().selectString(qb));
	}

	private QueryBuffer getBasicQuery(DatatableProcessingRequest dataRequest, String categoryId, String startDate, String endDate)
	{
		QueryBuffer qb = new QueryBuffer();
		qb.append(" select ");
		qb.append(" Element_id, ATTR_Headline, ATTRCHECK_Paid ");
		qb.append(" , ATTRDATE_Date, ATTRCURRENCY_Amount, ATTR_Currency ");
		qb.append(" from elements_invoices ");
		qb.append(" where category_id = ? ", categoryId);
		if (StringUtils.isNotBlank(startDate))
		{
			qb.append(" and date(ATTRDATE_Date) >= ? ", startDate);
		}
		if (StringUtils.isNotBlank(endDate))
		{
			qb.append(" and date(ATTRDATE_Date) <= ? ", endDate);
		}
		return qb;
	}


}
