/**
 * @author Jaysun Lee
 * @date 21 Sep 2015
 */
package au.corporateinteractive.qcloud.proposalbuilder.control;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.WebdirectorDataAccess;
import au.net.webdirector.admin.security.LoginController;

@Controller
public class WebdirectorIntegrationController extends AbstractController
{
	private static Logger logger = Logger.getLogger(WebdirectorIntegrationController.class);

	@RequestMapping("/autologinWebdirector")
	public String autologinWebdirector(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException
	{
		String username = (String) session.getAttribute(Constants.PORTAL_USERNAME_ATTR);

		String autoLoginPassword = WebdirectorDataAccess.automaticLoginWebdirector(username);

		if (autoLoginPassword != null)
		{
			if (StringUtils.equals(autoLoginPassword, WebdirectorDataAccess.FIRST_LOGIN_PASSWORD_PARAMETER_CHECK))
			{
				request.setAttribute("title", "Your first Administration System access");//default message
				request.setAttribute("message", "It appears this is your first time accessing the QuoteCloud Administration System. First you must set a password on your account "
						+ "via the \"Change Password\" menu option under Settings: Account. This will set your password on both your portal account "
						+ "(to login without Maestrano) and the Administration System account (also to be able to login without Maestrano.) ");
				return "register/genericMessage";
			}

			LoginController.loginUser(request, username, autoLoginPassword, response);
		}
		else
		{
			// force logout in case user already logged in prior to avoid confusion
			LoginController loginController = new LoginController();
			loginController.logoutUser(request, response, session);
		}

		return "redirect:/webdirector";
	}
}
