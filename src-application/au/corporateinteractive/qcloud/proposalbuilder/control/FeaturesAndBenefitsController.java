//package au.corporateinteractive.qcloud.proposalbuilder.control;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import org.apache.log4j.Logger;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import au.corporateinteractive.qcloud.proposalbuilder.db.PlanDataAccess;
//import au.corporateinteractive.qcloud.proposalbuilder.model.FeaturesAndBenefitsResponse;
//
//@Controller
//public class FeaturesAndBenefitsController extends AbstractController{
//
//	Logger logger = Logger.getLogger(FeaturesAndBenefitsController.class);
//
//	@RequestMapping("/fnbForPlans")
//	@ResponseBody
//	public FeaturesAndBenefitsResponse fnbForPlans(HttpServletRequest request, HttpServletResponse response, HttpSession session,
//			@RequestParam(required=false) String[] selectedPlans)
//	{
//		PlanDataAccess pda = new PlanDataAccess();
//
//		return pda.getFeaturesAndBenefitsForPlans(selectedPlans);
//	}
//
//}
