package au.corporateinteractive.qcloud.proposalbuilder.control;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.dashboard.datatable.DatatableProcessingRequest;
import au.corporateinteractive.qcloud.proposalbuilder.db.TasksDashboardDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.model.dashboard.DashboardDataResponse;
import au.corporateinteractive.qcloud.proposalbuilder.model.dashboard.DashboardSummary;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.client.CategoryData;

@Controller
public class DashboardController extends AbstractController
{

	Logger logger = Logger.getLogger(DashboardController.class);

	@RequestMapping("/dashboard")
	@ResponseBody
	public DashboardDataResponse dashboard(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception
	{
		DatatableProcessingRequest data = new DatatableProcessingRequest(request);

		TasksDashboardDataAccess pdda = new TasksDashboardDataAccess();

		String project = request.getParameter("project");
		String datetime = request.getParameter("datetime");
		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		Date date = new Date(Long.valueOf(datetime));
//
//		String[] accessRights = uada.getUserAccessRights((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR));
		CategoryData company = new UserAccountDataAccess().getUserCompanyForUserId(userId);
		List<DataMap> tasks = pdda.loadProposalsFiltered(date, data,project, company.getId(), userId);
		int countFiltered = pdda.countProposalsFiltered(date, data,project, company.getId(), userId);
		int countTotal = pdda.countProposalsTotal(date, project, company.getId(), userId);
		List<DataMap> proposalsResponse = new LinkedList<DataMap>();
		for (DataMap task : tasks)
		{
			pdda.injectGeneratedFields(task, company);
			proposalsResponse.add(task);
		}
		DashboardDataResponse result = new DashboardDataResponse(data.draw, countTotal, countFiltered, proposalsResponse);
		return result;
	}

	@RequestMapping("/dashboard_state_load")
	@ResponseBody
	public String dashboardLoad(HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{

		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

		UserAccountDataAccess uada = new UserAccountDataAccess();
		Hashtable<String, String> userData = uada.getUserWithId(userId);
		return userData.get(uada.DASHBOARD_RECORD);

	}

	@RequestMapping("/load_dashboard_filter_columns")
	@ResponseBody
	public String[] dashboardFilterColumnsLoad(HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{

		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

		TasksDashboardDataAccess pdda = new TasksDashboardDataAccess();

		String[] columns = pdda.getDashboardFilterColumns();

		return columns;

	}

	@RequestMapping("/dashboard_state_save")
	public void dashboardSave(HttpServletRequest request, HttpServletResponse response, HttpSession session, @RequestParam String data)
	{

		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		UserAccountDataAccess ada = new UserAccountDataAccess();
		ada.updateDashboard(userId, data);

	}

	@RequestMapping("/dashboard/summary")
	@ResponseBody
	public DashboardSummary summary(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, ParseException
	{
		String datetime = request.getParameter("datetime");
		UserAccountDataAccess uada = new UserAccountDataAccess();

		String userEmail = (String) session.getAttribute(Constants.PORTAL_USERNAME_ATTR);
		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		String companyID = (String) session.getAttribute(Constants.PORTAL_COMPANY_ID_ATTR);
		Date time = new Date(Long.valueOf(datetime));

		DashboardSummary summaryResponse = uada.getUserDashboardStats(time, companyID);

		System.out.println(summaryResponse);
		return summaryResponse;
	}

//	@RequestMapping("/dashboard/unreadComments")
//	@ResponseBody
//	public int unreadComments(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException
//	{
//		TravelDocsHistoryDataAccess phda = new TravelDocsHistoryDataAccess();
//		UserAccountDataAccess uada = new UserAccountDataAccess();
//
//		String[] accessRights = uada.getUserAccessRights((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR));
//
//		return phda.getTotalNotifications(accessRights);
//	}
//
//	@RequestMapping("/dashboard/notifications")
//	@ResponseBody
//	public List<Notification> notifications(
//			HttpServletRequest request,
//			HttpServletResponse response,
//			HttpSession session) throws IOException
//	{
//		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
//		NotificationDataAccess nda = new NotificationDataAccess();
//		return nda.getDailyNotificationsByUserId(userId);
//	}
//
//	@RequestMapping("/dashboard/notifications/{id}")
//	@ResponseBody
//	public Notification getNotificationById(
//			HttpServletRequest request,
//			HttpServletResponse response,
//			HttpSession session,
//			@PathVariable String id) throws IOException
//	{
//		NotificationDataAccess nda = new NotificationDataAccess();
//		return nda.getNotificationById(id);
//	}

	@RequestMapping("/dashboard/download")
	public void download(
			HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session) throws IOException, ParseException, JSONException
	{
		
	}

	public String formatDateTime(SimpleDateFormat csvDateTimeFormat, Hashtable<String, String> ht, String key) throws ParseException
	{
		try
		{
			Date d = dbDateFormat.parse(ht.get(key));
			String s = csvDateTimeFormat.format(d);
			return s;
		}
		catch (Exception e)
		{
			return "";
		}
	}

	@RequestMapping("/dashboard/notifications/viewed")
	@ResponseBody
	public boolean notificationsViewed(
			HttpServletRequest request,
			HttpServletResponse response,
			HttpSession session) throws IOException
	{
//		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
//		return new UserAccountReportingDataAccess().recordLastViewedNotifications(userId);
		return false;
	}

}