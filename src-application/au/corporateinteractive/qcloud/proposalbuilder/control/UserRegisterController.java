package au.corporateinteractive.qcloud.proposalbuilder.control;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.ci.sabre.InvalidClientException;
import au.com.ci.sabre.SabreClient;
import au.com.ci.sbe.util.ModuleVariables;
import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.CrmDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.ErrorException;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.WebdirectorDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.util.DataMapLegacyConverter;
import au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder;
import au.corporateinteractive.qcloud.proposalbuilder.service.SecurityKeyService;
import au.corporateinteractive.qcloud.proposalbuilder.utils.AjaxResponse;
import au.corporateinteractive.qcloud.subscription.SubscriptionPlan;
import au.corporateinteractive.service.RecaptchaService;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.common.utils.password.PasswordUtils;

@Controller
public class UserRegisterController extends AbstractController
{
	Logger logger = Logger.getLogger(UserRegisterController.class);

	@RequestMapping(value = "/free/sabre/verify", method = RequestMethod.POST)
	@ResponseBody
	public AjaxResponse verifySabre(HttpServletRequest request)
	{
		String clientId = request.getParameter("sabreClientId");
		String clientSecret = request.getParameter("sabreClientSecret");
		try
		{
			new SabreClient(clientId, clientSecret);
			return new AjaxResponse(true, "");
		}
		catch (InvalidClientException e)
		{
			logger.error("Error:", e);
			return new AjaxResponse(false, "");
		}

	}

	@RequestMapping(value = "/free/sabreuser/verify", method = RequestMethod.POST)
	@ResponseBody
	public AjaxResponse verifySabreUser(HttpServletRequest request, HttpSession session)
	{
		String firstname = request.getParameter("userfirstname");
		String lastname = request.getParameter("userlastname");
		String email = request.getParameter("useremail");
		String consultantId = request.getParameter("userconsultantid");
		try
		{

		}
		catch (Exception e)
		{
			logger.error("Error:", e);
		}
		return new AjaxResponse(false, "Couldn't send you a verification email to " + email + ".");
	}

	@RequestMapping(value = "/free/email/verify1", method = RequestMethod.POST)
	@ResponseBody
	public AjaxResponse verifyEmail(HttpServletRequest request, HttpSession session)
	{
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String email = request.getParameter("email");
		try
		{
			String exists = TransactionDataAccess.getInstance().selectString("select attr_headline from elements_users where attr_headline = ?", email);
			if (exists != null)
			{
				return new AjaxResponse(false, email + " has been registered already.");
			}
			String verificationKey = new SecurityKeyService().generateRandomKey(12, 6, "-");
			session.setAttribute("registerVerificationKey", verificationKey);
			boolean sent = new EmailBuilder(request)
					.prepareRegisterVerification(verificationKey, firstname, lastname)
					.addTo(email)
					.doSend();
			if (sent)
			{
				return new AjaxResponse(true, "", verificationKey.substring(0, verificationKey.indexOf("-") + 1));
			}
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
		}
		return new AjaxResponse(false, "Couldn't send you a verification email to " + email + ".");
	}

	@RequestMapping(value = "/free/email/verify2", method = RequestMethod.POST)
	@ResponseBody
	public boolean verifyCodeFromEmail(HttpServletRequest request, HttpSession session)
	{
		String verificationCode = request.getParameter("verificationCode").trim();
		return StringUtils.equals(verificationCode, (String) session.getAttribute("registerVerificationKey"));
	}

	@RequestMapping(value = "/free/register", method = RequestMethod.POST)
	public String registerAccount(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException
	{

		String username = request.getParameter("username");
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String mobileNumber = request.getParameter("mobileNumber") != null ? request.getParameter("mobileNumber") : "";
		String company = request.getParameter("company");
		String country = request.getParameter("country");
		String pricingPlan = request.getParameter("pricingPlan");
		String abnacn = request.getParameter("abnacn") != null ? request.getParameter("abnacn") : "";
		String addressLine1 = request.getParameter("addressLine1") != null ? request.getParameter("addressLine1") : "";
		String addressLine2 = request.getParameter("addressLine2") != null ? request.getParameter("addressLine2") : "";
		String citySuburb = request.getParameter("citySuburb") != null ? request.getParameter("citySuburb") : "";
		String state = request.getParameter("state") != null ? request.getParameter("state") : "";
		String postcode = request.getParameter("postcode") != null ? request.getParameter("postcode") : "";
		String recaptcha = request.getParameter("g-recaptcha-response") != null ? request.getParameter("g-recaptcha-response") : "";

		final RecaptchaService recaptchaService = RecaptchaService.sharedInstance();
		if (!recaptchaService.isResponseValid(request, recaptcha))
		{
			request.setAttribute("errorMessage", "Your reCaptcha verification was not valid.");
			return "register/registrationError";
		}

		name = WordUtils.capitalize(name.trim());
		surname = WordUtils.capitalize(surname.trim());
		username = username.trim();
		email = email.trim().toLowerCase();
		mobileNumber = mobileNumber.trim().toLowerCase();
		company = company.trim();
		abnacn = abnacn.trim().toLowerCase();

		addressLine1 = addressLine1.trim();
		addressLine2 = addressLine2.trim();
		citySuburb = citySuburb.trim();
		state = state.trim();
		postcode = postcode.trim();
		Hashtable<String, String> address = UserAccountDataAccess.constructAddressHt(addressLine1, addressLine2, citySuburb, state, postcode, country);

		pricingPlan = pricingPlan.toUpperCase();

		if (company.replaceAll(" ", "").equalsIgnoreCase("default"))
		{
			request.setAttribute("errorMessage", "You cannot choose DEFAULT as a company.");
			return "register/registrationError";
		}

		try
		{
			processRegistration(request, name, surname, email, username, password, mobileNumber, company, abnacn, pricingPlan, address);

			request.setAttribute("title", "Almost done...");//default message
			request.setAttribute("message", "Your QuoteCloud registration will now be processed. You will soon receive an email to activate your account.");
			return "register/genericMessage";

		}
		catch (ErrorException e)
		{
			logger.error("Sadness", e);
//			request.setAttribute("errorMessage", e.getMessage());
			e.printStackTrace();
		}
		catch (Exception e)
		{
			logger.error("Uber Sadness", e);
//			request.setAttribute("errorMessage", "Internal error... " + e.getMessage());
			e.printStackTrace();
		}
		return "register/registrationError";
	}

	@RequestMapping(value = "/free/register/plans", method = RequestMethod.POST)
	@ResponseBody
	public List<SubscriptionPlan> getCurrentSubscriptionPlans(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String country) throws IOException
	{
		return SubscriptionPlan.getCurrentSubscriptionPlans(country);
	}

	public static void processRegistration(HttpServletRequest request, String name, String surname, String email, String username, String password, String mobileNumber, String company, String abnacn, String pricingPlan, Hashtable<String, String> address) throws ErrorException, SQLException, IOException
	{
		UserAccountDataAccess ada = new UserAccountDataAccess();

		String temporaryPassword = UserController.randomPassword();
		String encryptedPassword = PasswordUtils.encrypt(password);
		int id = ada.registerNewUser(name, surname, email, username, "", encryptedPassword, mobileNumber, company, abnacn, address, pricingPlan);

		//TODO: at final go live we will need to call the following code on all unactivated users.
		String activationCode = new SecurityKeyService().getActivationKey(username, encryptedPassword);

		sendUserAccountActivationEmail(request, ada.getUserWithId(id), activationCode);

		//Email the admin to prescreen the account
		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");

		Hashtable<String, String> additionalData = new Hashtable<String, String>();
		additionalData.put("pricingPlan", pricingPlan);
		additionalData.put("company", company);
		additionalData.put("abnacn", abnacn);
		//Email the darren to acknowledge the registration
		new EmailBuilder(request)
				.addModule(UserAccountDataAccess.USER_MODULE, ada.getUserWithId(id))
				.addModule(UserAccountDataAccess.USER_MODULE, address)
				.addModule(UserAccountDataAccess.USER_MODULE, additionalData)
				.prepareRegistrationAcknowledgementEmail()
				.addTo(adminEmail.split(","))
				.doSend();

	}

	@RequestMapping(value = "/free/resend-activate", method = RequestMethod.POST)
	@ResponseBody
	public String resendActivateEmail(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException
	{
		String username = (String) session.getAttribute(Constants.LOGIN_ERROR_USERNAME);
		if (username == null)
		{
			return "";
		}
		try
		{
			UserAccountDataAccess ada = new UserAccountDataAccess();
			Hashtable<String, String> user = ada.getUserByUserName(username, false);
			String encryptedPassword = user.get(ada.E_PASSWORD);
			//TODO: at final go live we will need to call the following code on all unactivated users.
			String activationCode = new SecurityKeyService().getActivationKey(username, encryptedPassword);

			sendUserAccountActivationEmail(request, user, activationCode);
			return username;
		}
		catch (Exception e)
		{
			logger.error("Error: ", e);
			return "";
		}
	}

	@RequestMapping("/free/activateNewEmail")
	public String activateNewEmail(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String userId,
			@RequestParam String key,
			@RequestParam String newEmail) throws IOException, SQLException
	{
		UserAccountDataAccess ada = new UserAccountDataAccess();
		boolean success = ada.activateUserNewEmail(userId, key, newEmail);
		Hashtable<String, String> userTable = ada.getUserWithId(userId);
		if (success)
		{
			request.setAttribute(Constants.PORTAL_LOGIN_REDIRECT_PARAM, "/register/activated");
			UserController.autoLogin(request, response, session, userTable.get(UserAccountDataAccess.E_HEADLINE));
		}
		else
		{
			request.setAttribute("title", "Oops!");//default message
			request.setAttribute("message", "User not found or key is invalid");
		}
		return "register/genericMessage";

	}

	@RequestMapping("/free/activate")
	public String activate(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String userId,
			@RequestParam String key) throws IOException, SQLException
	{
		UserAccountDataAccess ada = new UserAccountDataAccess();

		boolean success = ada.activateUser(userId, key);
		if (success)
		{
			TransactionDataAccess da = TransactionDataAccess.getInstance(false);
			TransactionManager tm = TransactionManager.getInstance(da);
			TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);

			Hashtable<String, String> userTable = ada.getUserWithId(userId);
			CategoryData company = ada.getUserCompanyForUserId(userId);

			try
			{
				WebdirectorDataAccess wda = new WebdirectorDataAccess();

				String companyName = company.getName();

				wda.addCompanyCategoryToModules(company.getId(), companyName, ma, tm);

				SubscriptionPlan subscriptionPlan = SubscriptionPlan.getPlanByName(company.getString(UserAccountDataAccess.C_ADDRESS_COUNTRY), company.getString(UserAccountDataAccess.C_PRICING_PLAN));

				if (!SubscriptionPlan.isTrialPlan(subscriptionPlan.getName()))
				{
					CrmDataAccess cda = new CrmDataAccess();
					DataMapLegacyConverter dmlc = new DataMapLegacyConverter();
					boolean sentToCrmSuccess = cda.createNewContract(userTable, dmlc.convertFromDataMap(company), subscriptionPlan);
					if (!sentToCrmSuccess)
						throw new Exception("CRM contract creation failed.");

					// PaypalPay.getInstance().pay(companyId);
				}

				sendPostActivationEmail(request, ada.getUserWithId(userId));

				tm.commit();

				wda.postCommitUpdates(company.getId());

				logger.info("Privilages updated successfully... Reloading privilages");
			}
			catch (Exception e)
			{
				logger.fatal("Unable to set privilages for category. ROLLBACK", e);
				tm.rollback();

				request.setAttribute("title", "Error");
				request.setAttribute("message", "Internal server communication error");
				return "register/genericMessage";
			}

//			request.setAttribute("title", "Success!");
//			request.setAttribute("message", "You have been sent an email regarding your login details.");
//			request.setAttribute("showLogin", true);
			request.setAttribute(Constants.PORTAL_LOGIN_REDIRECT_PARAM, "/register/activated");
			UserController.autoLogin(request, response, session, userTable.get(UserAccountDataAccess.E_HEADLINE));
			// will redirect browser
		}
		else
		{
			request.setAttribute("title", "Oops!");//default message
			request.setAttribute("message", "User not found or already activated");
		}
		return "register/genericMessage";
	}


	@RequestMapping("/free/activateNewUser")
	public String activateNewUser(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String userId,
			@RequestParam String key) throws IOException, SQLException
	{
		UserAccountDataAccess ada = new UserAccountDataAccess();

		boolean success = ada.activateNewUser(userId, key);
		if (success)
		{
			TransactionDataAccess da = TransactionDataAccess.getInstance(false);
			TransactionManager tm = TransactionManager.getInstance(da);
			TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);

			Hashtable<String, String> userTable = ada.getUserWithId(userId);

			session.setAttribute(Constants.PORTAL_CHANGE_PASSWORD_ATTR, userId);
			session.setAttribute(Constants.PORTAL_USERNAME_ATTR, userTable.get(UserAccountDataAccess.E_HEADLINE));
			request.setAttribute(Constants.PORTAL_LOGIN_REDIRECT_PARAM, "/changePassword");
			UserController.autoLogin(request, response, session, userTable.get(UserAccountDataAccess.E_HEADLINE));
		}
		else
		{
			request.setAttribute("title", "Oops!");//default message
			request.setAttribute("message", "User not found or already activated");
		}
		return "register/genericMessage";
	}

	public static void sendUserAccountActivationEmail(HttpServletRequest request, Hashtable<String, String> userHash, String activationCode)
	{
		new EmailBuilder(request)
				.addModule(UserAccountDataAccess.USER_MODULE, userHash)
				.prepareAccountVerificationEmail(activationCode)
				.addTo(userHash.get(UserAccountDataAccess.E_EMAIL_ADDRESS))
				.doSend();
	}


	public void sendUserAccountEmailVerification(HttpServletRequest request, Hashtable<String, String> userHash, String activationCode, String newEmail)
	{
		new EmailBuilder(request)
				.addModule(UserAccountDataAccess.USER_MODULE, userHash)
				.prepareAccountChangeEmailVerification(activationCode, userHash.get(UserAccountDataAccess.E_EMAIL_ADDRESS), newEmail)
				.addTo(newEmail)
				.doSend();
	}


	public void sendUserAccountEmailVerification(HttpServletRequest request, ElementData userHash, String activationCode, String newEmail)
	{
		new EmailBuilder(request)
				.prepareNewUserVerificationEmail(activationCode, userHash.getId(), userHash.getString(UserAccountDataAccess.E_NAME))
				.addTo(newEmail)
				.doSend();
	}

	private void sendPostActivationEmail(HttpServletRequest request, Hashtable<String, String> userHash)
	{
		new EmailBuilder(request)
				.addModule(UserAccountDataAccess.USER_MODULE, userHash)
				.preparePostAccountActivationEmail()
				.addTo(userHash.get(UserAccountDataAccess.E_EMAIL_ADDRESS))
				.doSend();
	}
}
