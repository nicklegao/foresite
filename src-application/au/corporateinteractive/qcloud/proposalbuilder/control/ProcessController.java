/**
 * @author Jaysun Lee
 * @date 21 Sep 2015
 */
package au.corporateinteractive.qcloud.proposalbuilder.control;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.net.webdirector.common.process.ProcessManager;
import au.net.webdirector.common.process.ProcessStatus;

@RestController
@RequestMapping("/process")
public class ProcessController extends AbstractController
{
	private static Logger logger = Logger.getLogger(ProcessController.class);

	@PostConstruct
	public void init()
	{
		ProcessManager.getInstance().dumpDeadStatus();
	}


	@RequestMapping("/status/get")
	@ResponseBody
	public ProcessStatus getStatus(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String processor) throws Exception
	{
		String companyId = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR)).getId();
		ProcessManager manager = ProcessManager.getInstance();
		ProcessStatus status = manager.getProcessStatus(processor, companyId);
//		status = new ProcessStatus("running", 200, 79, false, 30, 49, "errors");
		return status;
	}

	@RequestMapping("/status/remove")
	@ResponseBody
	public boolean removeStatus(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String processor) throws Exception
	{
		String companyId = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR)).getId();
		ProcessManager manager = ProcessManager.getInstance();
		manager.removeProcessStatus(processor, companyId);
		return true;
	}


}
