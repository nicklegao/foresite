/**
 * @author Jaysun Lee
 * @date 13 Jan 2016
 */
package au.corporateinteractive.qcloud.proposalbuilder.control;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maestrano.exception.ApiException;
import com.maestrano.exception.AuthenticationException;
import com.maestrano.exception.InvalidRequestException;
import com.maestrano.exception.MnoConfigurationException;
import com.stripe.model.Customer;
import com.stripe.model.Subscription;

import au.com.ci.sbe.util.ModuleVariables;
import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.CrmDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.SubscriptionChangesDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder;
import au.corporateinteractive.qcloud.proposalbuilder.service.StripeService;
import au.corporateinteractive.qcloud.subscription.SubscriptionPlan;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;

@Controller
public class SubscriptionController
{
	private static Logger logger = Logger.getLogger(SubscriptionController.class);

	@RequestMapping("/changeSubscription")
	@ResponseBody
	public String changeSubscription(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam(value = "pricingPlan") String newPricingPlan) throws AuthenticationException, ApiException, InvalidRequestException, MnoConfigurationException, SQLException
	{
		UserAccountDataAccess uada = new UserAccountDataAccess();

		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		CategoryData company = uada.getUserCompanyForUserId(userId);

		String oldPlanName = company.getString(UserAccountDataAccess.C_PRICING_PLAN);
		SubscriptionPlan oldPlan = SubscriptionPlan.getPlanByName(company.getString(UserAccountDataAccess.C_ADDRESS_COUNTRY), oldPlanName);
		SubscriptionPlan newPlan = SubscriptionPlan.getPlanByName(company.getString(UserAccountDataAccess.C_ADDRESS_COUNTRY), newPricingPlan);
		newPlan.setQuantity(1);
		boolean success = false;
		try (TransactionManager txManager = TransactionManager.getInstance())
		{
			SubscriptionPlan additionalUserPlan = SubscriptionPlan.getAdditionalUserPlan(company.getString(uada.C_ADDRESS_COUNTRY));
			additionalUserPlan.setQuantity(uada.getUserCountInCompanyWithCompanyId(company.getId()) - newPlan.getMaxUsers());
			SubscriptionPlan additionalDataPlan = SubscriptionPlan.getAdditionalDataPlan(company.getString(uada.C_ADDRESS_COUNTRY));
//			double diskUsageGB = new DiskUsageDataAccess().getCurrentDiskUsageGB(company.getId());
			double maxDiskUsageGB = newPlan.getMaxData() + additionalUserPlan.getMaxData() * additionalUserPlan.getQuantity();
			int newDataPlanQty = new BigDecimal(0.0 - maxDiskUsageGB).divide(new BigDecimal(additionalDataPlan.getMaxData())).setScale(0, BigDecimal.ROUND_CEILING).intValue();
			additionalDataPlan.setQuantity(newDataPlanQty);

			if (!UserAccountDataAccess.isPayByCreditCard(company))
			{
				success = new SubscriptionChangesDataAccess().upgradePlan(company, newPlan, txManager, null, additionalUserPlan, additionalDataPlan);
			}
			else
			{
				success = StripeService.getInstance().upgradePlan(company, newPlan, txManager, null, additionalUserPlan, additionalDataPlan);
			}
			if (success)
			{
				updateSubscriptions(company, newPlan, userId, request, false, txManager);
				txManager.commit();
				return "success";
			}
		}
		catch (Exception e)
		{

		}
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		return "failed";
	}

	/**
	 * @param company
	 * @param name
	 * @param userId
	 * @param request
	 */
	private void updateSubscriptions(CategoryData company, SubscriptionPlan newPlan, String userId, HttpServletRequest request, boolean updateCrm, TransactionManager txManager)
	{
		updateSubscriptionOnCompany(newPlan.getName(), company.getId(), txManager);
		if (updateCrm)
		{
			try
			{
				CrmDataAccess cda = new CrmDataAccess();
				if (UserAccountDataAccess.doesCompanyHaveCrmContract(company))
					cda.updateContract(company.getInt(UserAccountDataAccess.C_CRM_CONTRACT_ID), newPlan);
				else
					cda.createNewContract(userId, newPlan);
			}
			catch (Exception e)
			{
				logger.error("Error:", e);
			}
		}

	}

	private void updateSubscriptionOnCompany(String pricingPlan, String companyId, TransactionManager txManager)
	{
		CategoryData updatedData = new CategoryData();
		updatedData.put(UserAccountDataAccess.C_PRICING_PLAN, pricingPlan);
		updatedData.put(UserAccountDataAccess.C1_BILLING_START_DATE, DateUtils.truncate(new Date(), Calendar.DATE));
		CategoryData conditions = new CategoryData();
		conditions.setId(companyId);
		try
		{
			txManager.getDataAccess().update(updatedData, "Categories_" + UserAccountDataAccess.USER_MODULE, conditions);
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
		}
	}

	@RequestMapping("/cancelSubscription")
	@ResponseBody
	public boolean emailSupport(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws SQLException
	{
		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

		UserAccountDataAccess ada = new UserAccountDataAccess();
		Hashtable<String, String> user = ada.getUserWithId(userId);
		CategoryData company = ada.getUserCompanyForUserId(userId);
		return cancelSubscription(company, user, request);
	}

	private static boolean cancelSubscription(CategoryData company, Hashtable<String, String> user, HttpServletRequest request)
	{
		try
		{
			if (!UserAccountDataAccess.isPayByCreditCard(company))
			{
				TransactionDataAccess.getInstance().update("update categories_users set ATTRDROP_pricingPlan = ?, ATTRDATE_trialPlanExpiry = date(now()), ATTRDROP_Status = 'Cancelled' where category_id = ?", SubscriptionPlan.TRIAL_PLAN, company.getId());
			}
			else
			{
				StripeService service = StripeService.getInstance();
				Customer customer = service.retrieveStripeCustomer(company);
				// delete card info
				StripeService.deleteCardFromCustomer(customer);
				// cancel current subscription in stripe
				List<Subscription> subs = service.retrieveCurrentSubscriptionsByCustomer(customer);

				if (subs != null)
				{
					for (Subscription sub : subs)
					{
						sub.cancel(null);

						if (sub.getMetadata().size() == 0)
						{
							// update company plan status = trial
							TransactionDataAccess.getInstance().update("update categories_users set ATTRDROP_pricingPlan = ?, ATTRDATE_trialPlanExpiry = date(now()), ATTRDROP_Status = 'Cancelled' where category_id = ?", SubscriptionPlan.TRIAL_PLAN, company.getId());
						}
						else
						{
							TransactionDataAccess.getInstance().update("update elements_market_subscribers set ATTRDROP_status = 'CANCELLED' where ATTR_subscriptionId = ?", sub.getId());
						}

					}
				}
			}
			sendCancellationEmail(request, company, user);

			request.getSession().invalidate();
		}
		catch (Exception e)
		{
			logger.error("Error", e);
		}
		return true;
	}

	/**
	 * @param request
	 * @param company
	 * @param user
	 */
	private static void sendCancellationEmail(HttpServletRequest request, CategoryData company, Hashtable<String, String> user)
	{

		List<Hashtable<String, String>> users = new UserAccountDataAccess().getUsersByCompanyId(company.getId());
		List<String> emails = new ArrayList<>();
		for (Hashtable<String, String> u : users)
		{
			emails.add(u.get(UserAccountDataAccess.E_EMAIL_ADDRESS.toLowerCase()));
		}
		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");
		new EmailBuilder(request)
				.addModule(UserAccountDataAccess.USER_MODULE, user)
				.addModule(UserAccountDataAccess.USER_MODULE, company)
				.prepareCancellationEmail()
				.addTo(emails.toArray(new String[] {}))
				.addBCC(adminEmail.split(","))
				.doSend();


	}
}
