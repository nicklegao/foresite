package au.corporateinteractive.qcloud.proposalbuilder.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TLabelsDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;

@Controller
public class LabelsController extends AbstractController
{

	Logger logger = Logger.getLogger(LabelsController.class);

//	@RequestMapping("/loadCustomFields")
//	@ResponseBody
//	public CustomFields loadCustomFields(HttpServletRequest request, HttpServletResponse response, HttpSession session)
//	{
//		CustomFieldsDataAccess cfda = new CustomFieldsDataAccess();
//		UserAccountDataAccess uada = new UserAccountDataAccess();
//		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
//		String companyId = uada.getUserCompanyForUserId(userId).getId();
//
//		String type = request.getParameter("type");
//		return cfda.getCompanyCustomFields(type, companyId);
//	}

	@RequestMapping("/updateLabels")
	@ResponseBody
	public boolean updateLabels(HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{
		boolean result = false;
		try (TransactionManager transactionManager = TransactionManager.getInstance())
		{
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

			TUserRolesDataAccess urda = new TUserRolesDataAccess(transactionManager);
			UserPermissions permissions = urda.getPermissions(userId);
//			if (!permissions.hasManageCustomFieldsPermission())
//			{
//				//TODO: should return message to show in the browser (BaseResponse)
//				logger.error("User " + userId + " tried to update custom labels but doesn't have permission for it.");
//				transactionManager.rollback();
//				return false;
//			}
			TLabelsDataAccess tlda = new TLabelsDataAccess(transactionManager);

			tlda.updateCompanyCustomLabels(request);
			transactionManager.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.fatal("Error updating custom labels", e);
			result = false;
		}

		return result;
	}
}
