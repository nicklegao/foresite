//package au.corporateinteractive.qcloud.proposalbuilder.control;
//
//import java.util.Map;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import org.apache.log4j.Logger;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import au.corporateinteractive.qcloud.proposalbuilder.db.PlanDataAccess;
//
//@Controller
//public class TermsAndConditionsController extends AbstractController{
//
//	Logger logger = Logger.getLogger(TermsAndConditionsController.class);
//
//	@RequestMapping("/tncForPlans")
//	@ResponseBody
//	public Map<String, String> fnbForPlans(HttpServletRequest request, HttpServletResponse response, HttpSession session,
//			@RequestParam(required=false) String[] selectedPlans ) {
//		PlanDataAccess pda = new PlanDataAccess();
//
//		return pda.getTermsForPlans(selectedPlans);
//	}
//
//}
