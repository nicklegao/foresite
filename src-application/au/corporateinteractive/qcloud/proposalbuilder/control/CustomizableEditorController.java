package au.corporateinteractive.qcloud.proposalbuilder.control;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;


@Controller
public class CustomizableEditorController extends AbstractController
{

	Logger logger = Logger.getLogger(CustomizableEditorController.class);


	@RequestMapping("/styles/save")
	@ResponseBody
	public boolean getOrCreateProposalStyles(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String proposalStyle)
	{
		boolean result = false;
		try (TransactionManager txManager = TransactionManager.getInstance())
		{

			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

			TUserAccountDataAccess uada = new TUserAccountDataAccess(txManager);
			TUserRolesDataAccess urda = new TUserRolesDataAccess(txManager);
			UserPermissions permissions = urda.getPermissions(userId);
//			if (!permissions.hasManagePricingStylePermission())
//			{
//				//TODO: should return message to show in the browser (BaseResponse)
//				logger.error("User " + userId + " tried to edit styles but doesn't have permission for it.");
//				txManager.rollback();
//				return false;
//			}
			uada.saveProposalStyles(userId, proposalStyle);

			txManager.commit();
			return true;

		}
		catch (Exception e)
		{
			logger.fatal("Error saving custom styles", e);
			result = false;
		}
		return result;
	}

	@RequestMapping("/styles/get")
	@ResponseBody
	public String getProposalStyles(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws JSONException, SQLException
	{
		try (TransactionManager txManager = TransactionManager.getInstance())
		{
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

			TUserAccountDataAccess uada = new TUserAccountDataAccess(null);
			String jsonFile = uada.getProposalStyles(userId);

			return jsonFile;
		}
		catch (Exception e)
		{
			logger.fatal("Error getting styles", e);
		}
		return "";
	}
}
