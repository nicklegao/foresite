package au.corporateinteractive.qcloud.proposalbuilder.control;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.csvreader.CsvWriter;
import com.oreilly.servlet.MultipartRequest;
import com.stripe.exception.StripeException;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;

import au.com.ci.sbe.util.ModuleVariables;
import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.EmailTemplateDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.StaticTextDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess.AuthenticationResponse;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess.AuthenticationStatus;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountReportingDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.WebdirectorDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions;
import au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder;
import au.corporateinteractive.qcloud.proposalbuilder.service.SecurityKeyService;
import au.corporateinteractive.qcloud.proposalbuilder.utils.AjaxResponse;
import au.corporateinteractive.qcloud.proposalbuilder.validations.RolesValidation;
import au.corporateinteractive.qcloud.proposalbuilder.validations.UsersValidation;
import au.corporateinteractive.qcloud.subscription.SubscriptionPlan;
import au.corporateinteractive.utils.GoogleAuthUtils;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.MultiOptions;
import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
import au.net.webdirector.common.datalayer.base.db.entity.TextFile;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.common.utils.module.StoreUtils;
import au.net.webdirector.common.utils.password.PasswordUtils;

@Controller
public class UserController extends AbstractController {
	Logger logger = Logger.getLogger(UserController.class);

	@RequestMapping("/free/forgotPassword")
	public String forgotPassword(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String email) throws IOException {
		email = email.trim().toLowerCase();

		UserAccountDataAccess ada = new UserAccountDataAccess();
		Hashtable<String, String> userHT = ada.getLiveUserByEmail(email);
		if (userHT != null) {
			logger.info("Starting forget password for user: " + email);
			String passwordResetKey = new SecurityKeyService().getForgotPasswordKey(
					userHT.get(UserAccountDataAccess.E_HEADLINE), userHT.get(UserAccountDataAccess.E_PASSWORD));

			new EmailBuilder(request).addModule(UserAccountDataAccess.USER_MODULE, userHT)
					.preparePasswordResetVerificationEmail(passwordResetKey)
					.addTo(userHT.get(UserAccountDataAccess.E_EMAIL_ADDRESS)).doSend();
		} else {
			logger.warn("User not found with email: " + email + " ... Unable to start forget password.");
		}

		request.setAttribute("title", "Thanks");
		request.setAttribute("message",
				"We need to verify your email address. Please check your email for additional instructions.");

		return "register/genericMessage";
	}

	@RequestMapping("/free/forgotPasswordConfirmation")
	public String forgotPasswordConfirmation(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, @RequestParam String userId, @RequestParam String key) throws IOException {
		userId = userId.trim().toLowerCase();
		key = key.trim();

		UserAccountDataAccess ada = new UserAccountDataAccess();
		Hashtable<String, String> userHT = ada.getUserWithId(userId);
		request.setAttribute("title", "Error");// default message
		request.setAttribute("message", "User not found or invaid key");
		if (userHT != null) {
			String email = userHT.get(UserAccountDataAccess.E_EMAIL_ADDRESS);
			logger.info("Forget password email verified for userId: " + userId + "... Determined email:" + email);
			String expectedPasswordResetKey = new SecurityKeyService().getForgotPasswordKey(
					userHT.get(UserAccountDataAccess.E_HEADLINE), userHT.get(UserAccountDataAccess.E_PASSWORD));

			if (expectedPasswordResetKey.equals(key)) {

				String temporaryPassword = userHT.get(UserAccountDataAccess.E_RESET_PASSWORD);
				if (temporaryPassword.length() == 0) {
					// Dont change the password twice!... could be confusing for users.
					temporaryPassword = randomPassword();

					Hashtable<String, String> updateUser = new Hashtable<String, String>();
					insertNotNull(updateUser, UserAccountDataAccess.E_ID, userHT.get(UserAccountDataAccess.E_ID));
					insertNotNull(updateUser, UserAccountDataAccess.E_RESET_PASSWORD, temporaryPassword);
					updateElement(updateUser, UserAccountDataAccess.USER_MODULE);
					userHT.putAll(updateUser);
				}

				new EmailBuilder(request).addModule(UserAccountDataAccess.USER_MODULE, userHT)
						.preparePasswordResetConfirmation().addTo(userHT.get(UserAccountDataAccess.E_EMAIL_ADDRESS))
						.doSend();

				request.setAttribute("title", "Success");// default message
				request.setAttribute("message",
						"Your password has been successfully reset. Please check your email for a temporary password.");
				request.setAttribute("showLogin", true);
			}

		} else {
			logger.warn("User not found with userID: " + userId + " ... Unable to verify email for forget password.");
		}

		return "register/genericMessage";
	}

	/*
	 * @RequestMapping("/free/preverify") public String preverify(HttpServletRequest
	 * request, HttpServletResponse response, HttpSession session,
	 * 
	 * @RequestParam int userId,
	 * 
	 * @RequestParam String key) throws IOException { UserAccountDataAccess ada =
	 * new UserAccountDataAccess(); Hashtable<String, String> ht =
	 * ada.getUserWithId(userId);
	 * 
	 * request.setAttribute("message", "User not found or invaid key"); if (ht !=
	 * null) { String activationCode = new SecurityKeyService()
	 * .getActivationKey(ht.get(UserAccountDataAccess.E_EMAIL),
	 * PasswordUtils.encrypt(ht.get(UserAccountDataAccess.E_RESET_PASSWORD)));
	 * 
	 * if (key.equals(activationCode)) { //Send email to user.
	 * sendUserAccountActivationEmail(request, ht, activationCode);
	 * 
	 * request.setAttribute("title", "Success!");//default message
	 * request.setAttribute("message", "Email sent to user with activation URL."); }
	 * } return "register/genericMessage"; }
	 */

	@RequestMapping("/generateEmailTwoFactor")
	@ResponseBody
	public Hashtable<String, Object> generateEmailTwoFactor(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		Hashtable<String, Object> responseObject = new Hashtable();
		try {
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			UserAccountDataAccess uada = new UserAccountDataAccess();
			ElementData user = uada.getUserElementWithUserID(userId);

			String randomKey = new SecurityKeyService().generateRandomKey(6, 6, "");
			logger.debug("VerificationCode:" + randomKey);
			session.setAttribute("enableEmailVerificationToken", randomKey);
			boolean emailSent = new EmailBuilder().prepareLoginTwoFactor(randomKey)
					.addTo(user.getString(UserAccountDataAccess.E_EMAIL_ADDRESS)).doSend();

			if (emailSent) {
				responseObject.put("success", true);
				responseObject.put("message", "Code generated");
			} else {
				responseObject.put("message", "Could not send verification email");
				responseObject.put("success", false);
			}
			return responseObject;
		} catch (Exception e) {
			responseObject.put("success", false);
			e.printStackTrace();
			responseObject.put("message", e.getMessage());
			logger.error("Error:", e);
			return responseObject;
		}
	}

	@RequestMapping("/generateMobileTwoFactor")
	@ResponseBody
	public Hashtable<String, Object> generateMobileTwoFactor(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		Hashtable<String, Object> responseObject = new Hashtable();
		try {
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			UserAccountDataAccess uada = new UserAccountDataAccess();
			ElementData user = uada.getUserElementWithUserID(userId);

			GoogleAuthenticator gAuth = new GoogleAuthenticator();
			GoogleAuthUtils gUtils = new GoogleAuthUtils();

			gAuth.setCredentialRepository(gUtils);

			if (!UserAccountDataAccess.isUserUsingMobileTFA(user)) {
				GoogleAuthenticatorKey key = gAuth.createCredentials(userId);

				if (StringUtils.isNotBlank(key.getKey())) {
					responseObject.put("success", true);
					responseObject.put("message", "Code generated");

				} else {
					responseObject.put("message", "Something");
					responseObject.put("success", false);
				}
			} else {
				responseObject.put("message", "This account already has mobile two factor already enabled");
				responseObject.put("success", false);
			}
			return responseObject;
		} catch (Exception e) {
			responseObject.put("success", false);
			e.printStackTrace();
			responseObject.put("message", e.getMessage());
			logger.error("Error:", e);
			return responseObject;
		}
	}

	@RequestMapping("/updateEmailTwoFactor")
	@ResponseBody
	public Hashtable<String, Object> updateEmailTwoFactor(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		Hashtable<String, Object> responseObject = new Hashtable();
		try (TransactionManager txManager = TransactionManager.getInstance()) {
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			TUserAccountDataAccess uada = new TUserAccountDataAccess(txManager);

			String verificationToken = request.getParameter("verificationToken");
			Boolean updateEmailVerification = Boolean.valueOf(request.getParameter("updateEmailVerification"));

			String token = (String) session.getAttribute("enableEmailVerificationToken");
			token = token.replaceAll("\\s+", "");
			if (token.equals(verificationToken)) {
				uada.updateEmailTwoFactor(userId, updateEmailVerification);
				responseObject.put("success", true);
				responseObject.put("message", "You have " + (updateEmailVerification ? "enabled" : "disabled")
						+ " two factor authentication via email");
			}

			txManager.commit();
			return responseObject;
		} catch (Exception e) {
			responseObject.put("success", false);
			responseObject.put("message", e.getMessage());
			logger.error("Error:", e);
			return responseObject;
		}
	}

	@RequestMapping("/updateMobileTwoFactor")
	@ResponseBody
	public Hashtable<String, Object> updateMobileTwoFactor(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		Hashtable<String, Object> responseObject = new Hashtable();
		try (TransactionManager txManager = TransactionManager.getInstance()) {
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			TUserAccountDataAccess uada = new TUserAccountDataAccess(txManager);

			String verificationCode = request.getParameter("verifytotp");
			verificationCode = verificationCode.replaceAll("\\s+", "");
			boolean enableTwoFactor = Boolean.valueOf(request.getParameter("updateMobileVerification"));

			GoogleAuthenticator gAuth = new GoogleAuthenticator();
			GoogleAuthUtils gUtils = new GoogleAuthUtils();

			gAuth.setCredentialRepository(gUtils);

			boolean validtotp = gAuth.authorizeUser(userId, Integer.valueOf(verificationCode));

			if (validtotp) {
				uada.updateMobileTwoFactor(userId, enableTwoFactor);
				responseObject.put("success", true);
				responseObject.put("message", "You have enabled two factor authentication via mobile");

			} else {
				responseObject.put("message", "Code is wrong");
				responseObject.put("success", false);
			}
			txManager.commit();
			return responseObject;
		} catch (Exception e) {
			responseObject.put("success", false);
			responseObject.put("message", e.getMessage());
			logger.error("Error:", e);
			return responseObject;
		}
	}

	@RequestMapping("/downloadUser")
	public void downloadUsers(HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws IOException, ParseException {

		String userIDSelected = request.getParameter("userSelected");
		if (userIDSelected.length() < 1) {
			userIDSelected = null;
		}

		SimpleDateFormat csvDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat csvDateTimeFormat = new SimpleDateFormat("d/MM/yyyy H:mm");
		SimpleDateFormat csvMonthFormat = new SimpleDateFormat("MMM yyyy");

		String userID = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

		UserAccountDataAccess uada = new UserAccountDataAccess();

		String companyId = uada.getUserCompanyForUserId(userID).getId();

		Hashtable<String, ElementData> selectedUser = new Hashtable<>();
		Hashtable<String, ElementData> companyUsers = new Hashtable<>();

		if (userIDSelected != null) {
			Hashtable<String, ElementData> company = uada.getCompanyUsers(companyId);
			ElementData user = company.get(userIDSelected);
			companyUsers.put(userIDSelected, user);
		} else {
			companyUsers = uada.getCompanyUsers(companyId);
		}

		try {
			response.setContentType("text/csv");

			// set a cokie because of "jquery.fileDownload.js"
			Cookie c = new Cookie("fileDownload", "true");
			c.setMaxAge(-1);
			c.setPath("/");
			response.addCookie(c);

			if (userIDSelected != null) {
				response.addHeader("Content-disposition",
						String.format("attachment;filename=QuoteCloudUserData(%s)-%s.csv", selectedUser.size(),
								new Date().getTime()));
			} else {
				response.addHeader("Content-disposition",
						String.format("attachment;filename=QuoteCloudUserData(%s)-%s.csv", companyUsers.size(),
								new Date().getTime()));
			}

			CsvWriter writer = new CsvWriter(new BufferedOutputStream(response.getOutputStream()), ',',
					Charset.defaultCharset());
			List<String> headerRow = new LinkedList<>();
			List<String> activeColumns = new ArrayList<>();

			List<String> columns = new ArrayList<>();
			Collection<ElementData> companyColumns = companyUsers.values();
			columns = Collections.list(uada.getUserWithId(companyColumns.iterator().next().getId()).keys());

			for (String col : columns) {
				if (!col.toLowerCase().contains("password") && !col.toLowerCase().contains("tutorial")
						&& !col.toLowerCase().contains("access") && !col.toLowerCase().contains("socket")
						&& !col.toLowerCase().contains("token") && !col.toLowerCase().contains("element_id")
						&& !col.toLowerCase().contains("category_id") && !col.toLowerCase().contains("dashboardrecord")
						&& !col.toLowerCase().contains("supportadmin")
						&& !col.toLowerCase().contains("supportauthtime")) {
					String[] newCol = col.split("_");
					String finalCol = col.replace(newCol[0] + "_", "");
					headerRow.add(finalCol);
					activeColumns.add(col);
				}
			}

			writer.writeRecord(headerRow.toArray(new String[] {}));

			if (companyUsers.size() > 0) {
				for (ElementData user : companyUsers.values()) {
					List<String> dataRow = new LinkedList<>();
					for (String col : activeColumns) {
						if (col.toLowerCase().contains("attrmulti")) {
							MultiOptions manages = ModuleAccess.getInstance().getMultiOptions(user, col,
									uada.USER_MODULE);
							if (manages != null) {
								if (col.toLowerCase().contains("role")) {
									TransactionManager tm = TransactionManager.getInstance();
									TUserRolesDataAccess urda = new TUserRolesDataAccess(tm);
									List<ElementData> roles = urda.getRolesForUserId(user.getId());

									List<String> managerList = new ArrayList<>();
									for (ElementData role : roles) {
										managerList.add(role.getName());
									}
									dataRow.add(StringUtils.join(managerList, ','));
								} else {
									List<String> managerList = new ArrayList<>();
									for (String managerId : manages.getValues()) {
										Hashtable<String, String> manager = uada.getUserWithId(managerId);
										managerList.add(manager.get(uada.E_EMAIL_ADDRESS));
									}
									dataRow.add(StringUtils.join(managerList, ','));
								}
							} else {
								dataRow.add("");
							}
						} else if (col.toLowerCase().contains("attrdate")) {
							Date userDate = user.getDate(col);
							DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
							dataRow.add(df.format(userDate));
						} else if (col.toLowerCase().contains("attrcheck")) {
							if (user.getBoolean(col)) {
								dataRow.add("True");
							} else {
								dataRow.add("False");
							}
						} else {
							dataRow.add(user.getString(col));
						}
					}
					writer.writeRecord(dataRow.toArray(new String[] {}));
				}
				writer.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@RequestMapping("/deleteUser")
	@ResponseBody
	public Hashtable<String, Object> deleteUser(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		Hashtable<String, Object> responseObject = new Hashtable();
		try (TransactionManager txManager = TransactionManager.getInstance()) {
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			TUserAccountDataAccess uada = new TUserAccountDataAccess(txManager);
			TUserRolesDataAccess urda = new TUserRolesDataAccess(txManager);
			CategoryData company = uada.getUserCompanyForUserId(userId);
			String companyId = company.getId();
			String id = request.getParameter("id");
			String newId = request.getParameter("newId");
			Hashtable<String, ElementData> users = uada.getCompanyUsers(companyId);
			UserPermissions up = urda.getPermissions(userId);
			if (up.hasManageUsersPermission()) {
				for (String key : users.keySet()) {
					ElementData user = users.get(key);
					if (user.getId().equals(id)) {
						int deleted = 0;
						int references = 0; // uada.checkUserForReferences(id);
						if (references > 0 && StringUtils.isNotBlank(newId)) {
//							uada.migrationsForUser(request, id, newId);
							deleted = uada.deleteUser(companyId, id);
							responseObject.put("success", true);
							responseObject.put("message", "Successfully deleted user.");
						} else if (references == 0) {
							deleted = uada.deleteUser(companyId, id);
							responseObject.put("success", true);
							responseObject.put("message", "Successfully deleted user.");
						} else {
							responseObject.put("success", false);
							responseObject.put("action", "migrate");
						}
					}
				}
			} else {
				responseObject.put("message", "You do not have permissions to delete this user");
				responseObject.put("success", false);
			}
			txManager.commit();
			return responseObject;
		} catch (Exception e) {
			responseObject.put("success", false);
			responseObject.put("message", e.getMessage());
			logger.error("Error:", e);
			return responseObject;
		}
	}

	@RequestMapping("/deleteTeam")
	@ResponseBody
	public Hashtable<String, Object> deleteTeam(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		Hashtable<String, Object> responseObject = new Hashtable();
		try (TransactionManager txManager = TransactionManager.getInstance()) {
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			TUserAccountDataAccess uada = new TUserAccountDataAccess(txManager);
			TUserRolesDataAccess urda = new TUserRolesDataAccess(txManager);
			CategoryData companyData = uada.getUserCompanyForUserId(userId);
			String companyId = companyData.getId();
			String id = request.getParameter("id");
			String newId = request.getParameter("newId");
			List<CategoryData> teams = uada.getCompanyTeams(companyId);
			UserPermissions up = urda.getPermissions(userId);
			if (up.hasManageUsersPermission()) {
				for (CategoryData team : teams) {
					if (team.getId().equals(id)) {
						int references = uada.checkTeamForReferences(id);
						if (references > 0 && StringUtils.isNotBlank(newId)) {
							uada.migrationsForTeam(id, newId);
							int deleted = uada.deleteTeam(id);
							responseObject.put("success", true);
						} else if (references == 0) {
							int deleted = uada.deleteTeam(id);
							responseObject.put("success", true);
						} else {
							responseObject.put("success", false);
							responseObject.put("action", "migrate");
						}
					}
				}
			} else {
				responseObject.put("message", "You do not have permissions to delete this team");
				responseObject.put("success", false);
			}
			txManager.commit();
			return responseObject;
		} catch (RuntimeException e) {
			logger.error("Error:", e);
			responseObject.put("success", false);
			responseObject.put("message", e.getMessage());
			return responseObject;
			// TODO Auto-generated catch block
		} catch (Exception e) {
			logger.error("Error:", e);
			return null;
		}
	}

	@RequestMapping("/deleteRole")
	@ResponseBody
	public Hashtable<String, Object> deleteRole(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		Hashtable<String, Object> responseObject = new Hashtable();
		try (TransactionManager txManager = TransactionManager.getInstance()) {
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			TUserAccountDataAccess uada = new TUserAccountDataAccess(txManager);
			TUserRolesDataAccess urda = new TUserRolesDataAccess(txManager);
			CategoryData companyData = uada.getUserCompanyForUserId(userId);
			String companyId = companyData.getId();
			String id = request.getParameter("id");
			List<ElementData> roles = urda.getCompanyRoles(companyId);
			UserPermissions up = urda.getPermissions(userId);
			if (up.hasManageUsersPermission()) {
				for (ElementData role : roles) {
					if (role.getId().equals(id)) {
						int deleted = urda.deleteRole(companyId, id);
						RolesValidation validation = new RolesValidation(txManager, null);
						List<String> errors = validation.validateRoles(companyId);
						if (errors.size() > 0) {
							responseObject.put("message", StringUtils.join(errors, ","));
							responseObject.put("success", false);
							txManager.rollback();
						} else {
							responseObject.put("message", "Sucessfully deleted role.");
							responseObject.put("success", true);
						}
						txManager.commit();
						return responseObject;
					}
				}
				responseObject.put("message", "Could not find role to delete.");
				responseObject.put("success", false);
			} else {
				responseObject.put("message", "You do not have permissions to delete this role");
				responseObject.put("success", false);
			}
			txManager.commit();
			return responseObject;
		} catch (RuntimeException e) {
			logger.error("Error:", e);
			responseObject.put("message", e.getMessage());
			responseObject.put("success", false);
			return responseObject;
		} catch (Exception e) {
			logger.error("Error:", e);
			responseObject.put("success", false);
			return responseObject;
		}
	}

	@RequestMapping("/updateAccount")
	public void updateAccount(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String myName, @RequestParam String mySurname, @RequestParam String myMobile)
			throws IOException {

		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		UserAccountDataAccess ada = new UserAccountDataAccess();
		ada.updateUserAccount(userId, myName, mySurname, myMobile);
	}

	@RequestMapping("/updateAccountEmail")
	public @ResponseBody String updateAccountEmail(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, @RequestParam String accountPassword, @RequestParam String newEmail)
			throws IOException {

		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		accountPassword = accountPassword.trim();
		// TODO: authenticate the user.
		UserAccountDataAccess ada = new UserAccountDataAccess();
		Hashtable<String, String> user = ada.getUserWithId(userId);
		Hashtable<String, String> newUser = ada.getLiveUserByEmail(newEmail);
		if (newUser != null) {
			logger.fatal("Email address is in use");
			return "emailUsed";
		}
		try {
			AuthenticationResponse authResponse = ada.authenticate(user.get(ada.E_HEADLINE), accountPassword);
			if (authResponse.status.isOK()) {
				String encryptedPassword = PasswordUtils.encrypt(accountPassword);
				UserRegisterController urc = new UserRegisterController();
				String activationCode = new SecurityKeyService().getActivationKey(newEmail, encryptedPassword);
				urc.sendUserAccountEmailVerification(request, user, activationCode, newEmail);
				return "true";
			} else {
				logger.fatal("Wrong password to change email address");
				return "false";
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return "false";
		}
//		ada.updateUserAccount(userId, myName, mySurname, myMobile);
	}

	@RequestMapping("/updateStyles")
	@ResponseBody
	public void updateStyles(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		try (TransactionManager transactionManager = TransactionManager.getInstance()) {
			TUserAccountDataAccess tuada = new TUserAccountDataAccess(transactionManager);
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

			TUserRolesDataAccess urda = new TUserRolesDataAccess(transactionManager);
			UserPermissions permissions = urda.getPermissions(userId);
//			if (!permissions.hasManageStylePermission())
//			{
//				//TODO: should return message to show in the browser (BaseResponse)
//				logger.error("User " + userId + " tried to edit styles but doesn't have permission for it.");
//				transactionManager.rollback();
//				return;
//			}

			CategoryData company = tuada.getUserCompanyForUserId(userId);
			MultipartRequest r = new MultipartRequest(request, System.getProperty("java.io.tmpdir"),
					Constants.MAX_ATTACHMENT_SIZE);
			File logo = r.getFile("logo");
			String fontFamilyCoverTitle = r.getParameter("fontFamilyCoverTitle");
			String fontSizeCoverTitle = r.getParameter("fontSizeCoverTitle");
			String fontFamilyCoverCompany = r.getParameter("fontFamilyCoverCompany");
			String fontSizeCoverCompany = r.getParameter("fontSizeCoverCompany");
			String fontFamilyCoverExtra = r.getParameter("fontFamilyCoverExtra");
			String fontSizeCoverExtra = r.getParameter("fontSizeCoverExtra");
			String fontFamilyBody = r.getParameter("fontFamilyBody");
			String fontSizeBody = r.getParameter("fontSizeBody");
			String fontColorBody = r.getParameter("fontColorBody");
			String fontFamilySECTION = r.getParameter("fontFamilySECTION");
			String fontSizeSECTION = r.getParameter("fontSizeSECTION");
			String fontColorSectionTitle = r.getParameter("fontColorSectionTitle");
			boolean uppercaseSectionTitle = "show".equals(r.getParameter("uppercaseSectionTitle"));
			boolean underlineSectionTitle = "show".equals(r.getParameter("underlineSectionTitle"));
			boolean boldSectionTitle = "show".equals(r.getParameter("boldSectionTitle"));
			boolean customSectionColor = "show".equals(r.getParameter("customSectionColor"));

			String fontColorHeading1 = r.getParameter("fontColorHeading1");
			boolean uppercaseHeading1 = "show".equals(r.getParameter("uppercaseHeading1"));
			boolean underlineHeading1 = "show".equals(r.getParameter("underlineHeading1"));
			boolean boldHeading1 = "show".equals(r.getParameter("boldHeading1"));
			boolean customHeading1Color = "show".equals(r.getParameter("customHeading1Color"));

			String fontColorHeading2 = r.getParameter("fontColorHeading2");
			boolean uppercaseHeading2 = "show".equals(r.getParameter("uppercaseHeading2"));
			boolean underlineHeading2 = "show".equals(r.getParameter("underlineHeading2"));
			boolean boldHeading2 = "show".equals(r.getParameter("boldHeading2"));
			boolean customHeading2Color = "show".equals(r.getParameter("customHeading2Color"));

			String fontColorHeading3 = r.getParameter("fontColorHeading3");
			boolean uppercaseHeading3 = "show".equals(r.getParameter("uppercaseHeading3"));
			boolean underlineHeading3 = "show".equals(r.getParameter("underlineHeading3"));
			boolean boldHeading3 = "show".equals(r.getParameter("boldHeading3"));
			boolean customHeading3Color = "show".equals(r.getParameter("customHeading3Color"));
			String fontFamilyH1 = r.getParameter("fontFamilyH1");
			String fontSizeH1 = r.getParameter("fontSizeH1");
			String fontFamilyH2 = r.getParameter("fontFamilyH2");
			String fontSizeH2 = r.getParameter("fontSizeH2");
			String fontFamilyH3 = r.getParameter("fontFamilyH3");
			String fontSizeH3 = r.getParameter("fontSizeH3");
			String palette1 = r.getParameter("palette1");
			String palette2 = r.getParameter("palette2");
			String palette3 = r.getParameter("palette3");
			String palette4 = r.getParameter("palette4");
			String backgroundColour = r.getParameter("backgroundColour");
			String sectionBackgroundColour = r.getParameter("sectionBackgroundColour");
			boolean removeLogo = "on".equals(r.getParameter("removeLogo"));
			String tableStyle = r.getParameter("tableStyle");
			String tabStyle = r.getParameter("tabStyle");
			boolean showTabs = !"show".equals(r.getParameter("showTabs"));
			boolean showCoverPage = !"show".equals(r.getParameter("showCoverpage"));
			boolean coverPageProposalInfo = "show".equals(r.getParameter("coverPageProposalInfo"));
			boolean coverPageSenderInfo = "show".equals(r.getParameter("coverPageSenderInfo"));
			boolean coverPageBoxBackground = "show".equals(r.getParameter("coverPageBoxBackground"));
			String coverPageColor = r.getParameter("coverPageColor");

			String contentLeftPadding = r.getParameter("contentLeftPadding");
			String contentRightPadding = r.getParameter("contentRightPadding");
			String contentTopPadding = r.getParameter("contentTopPadding");
			String contentBottomPadding = r.getParameter("contentBottomPadding");

			// PAGE STYLES
			boolean headerTopActive = "show".equals(r.getParameter("topHeaderActive"));
			boolean headerBottomActive = "show".equals(r.getParameter("bottomHeaderActive"));
			String headerTopBackground = r.getParameter("headerColorBackground");
			String headerBottomBackground = r.getParameter("footerColorBackground");
			String headerTopFontColor = r.getParameter("headerColorFont");
			String headerBottomFontColor = r.getParameter("footerColorFont");
			// Header Top
			String topStyleLeft = r.getParameter("topStyleLeft");
			String topStyleCenter = r.getParameter("topStyleCenter");
			String topStyleRight = r.getParameter("topStyleRight");
			String topLeftDataText = r.getParameter("topLeftData-text") != null
					? new String(r.getParameter("topLeftData-text").getBytes("iso-8859-1"), "UTF-8")
					: null;
			File topLeftDataImage = r.getFile("topLeftData-image");
			String topCenterDataText = r.getParameter("topCenterData-text") != null
					? new String(r.getParameter("topCenterData-text").getBytes("iso-8859-1"), "UTF-8")
					: null;
			File topCenterDataImage = r.getFile("topCenterData-image");
			String topRightDataText = r.getParameter("topRightData-text") != null
					? new String(r.getParameter("topRightData-text").getBytes("iso-8859-1"), "UTF-8")
					: null;
			File topRightDataImage = r.getFile("topRightData-image");
			// Header Bottom
			String bottomStyleLeft = r.getParameter("bottomStyleLeft");
			String bottomStyleCenter = r.getParameter("bottomStyleCenter");
			String bottomStyleRight = r.getParameter("bottomStyleRight");
			String bottomLeftDataText = r.getParameter("bottomLeftData-text") != null
					? new String(r.getParameter("bottomLeftData-text").getBytes("iso-8859-1"), "UTF-8")
					: null;
			File bottomLeftDataImage = r.getFile("bottomLeftData-image");
			String bottomCenterDataText = r.getParameter("bottomCenterData-text") != null
					? new String(r.getParameter("bottomCenterData-text").getBytes("iso-8859-1"), "UTF-8")
					: null;
			File bottomCenterDataImage = r.getFile("bottomCenterData-image");
			String bottomRightDataText = r.getParameter("bottomRightData-text") != null
					? new String(r.getParameter("bottomRightData-text").getBytes("iso-8859-1"), "UTF-8")
					: null;
			File bottomRightDataImage = r.getFile("bottomRightData-image");

			String topLeftDataImageString = saveHeaderFile(topLeftDataImage, company,
					TUserAccountDataAccess.C1_HEADER_TOP_DATA, r.getParameter("topLeftData-image-string"));
			String topCenterDataImageString = saveHeaderFile(topCenterDataImage, company,
					TUserAccountDataAccess.C1_HEADER_TOP_DATA, r.getParameter("topCenterData-image-string"));
			String topRightDataImageString = saveHeaderFile(topRightDataImage, company,
					TUserAccountDataAccess.C1_HEADER_TOP_DATA, r.getParameter("topRightData-image-string"));
			String bottomLeftDataImageString = saveHeaderFile(bottomLeftDataImage, company,
					TUserAccountDataAccess.C1_HEADER_BOTTOM_DATA, r.getParameter("bottomLeftData-image-string"));
			String bottomCenterDataImageString = saveHeaderFile(bottomCenterDataImage, company,
					TUserAccountDataAccess.C1_HEADER_BOTTOM_DATA, r.getParameter("bottomCenterData-image-string"));
			String bottomRightDataImageString = saveHeaderFile(bottomRightDataImage, company,
					TUserAccountDataAccess.C1_HEADER_BOTTOM_DATA, r.getParameter("bottomRightData-image-string"));

			String topData = "{\"left\":{\"option\":\"" + topStyleLeft + "\"";
			if (null != topLeftDataText) {
				topData += ",\"data\":\"" + JSONObject.escape(topLeftDataText) + "\",\"type\":\"text\"}";
			} else if (null != topLeftDataImageString) {
				topData += ",\"data\":\"" + topLeftDataImageString + "\",\"type\":\"image\"}";
			} else if (topStyleLeft.contains("page")) {
				topData += ",\"type\":\"page\"}";
			} else {
				topData += "}";
			}

			topData += ",\"center\":{\"option\":\"" + topStyleCenter + "\"";
			if (null != topCenterDataText) {
				topData += ",\"data\":\"" + JSONObject.escape(topCenterDataText) + "\",\"type\":\"text\"}";
			} else if (null != topCenterDataImageString) {
				topData += ",\"data\":\"" + topCenterDataImageString + "\",\"type\":\"image\"}";
			} else if (topStyleCenter.contains("page")) {
				topData += ",\"type\":\"page\"}";
			} else {
				topData += "}";
			}

			topData += ",\"right\":{\"option\":\"" + topStyleRight + "\"";
			if (null != topRightDataText) {
				topData += ",\"data\":\"" + JSONObject.escape(topRightDataText) + "\",\"type\":\"text\"}";
			} else if (null != topRightDataImageString) {
				topData += ",\"data\":\"" + topRightDataImageString + "\",\"type\":\"image\"}";
			} else if (topStyleRight.contains("page")) {
				topData += ",\"type\":\"page\"}";
			} else {
				topData += "}";
			}
			topData += "}";

			String bottomData = "{\"left\":{\"option\":\"" + bottomStyleLeft + "\"";
			if (null != bottomLeftDataText) {
				bottomData += ",\"data\":\"" + JSONObject.escape(bottomLeftDataText) + "\",\"type\":\"text\"}";
			} else if (null != bottomLeftDataImageString) {
				bottomData += ",\"data\":\"" + bottomLeftDataImageString + "\",\"type\":\"image\"}";
			} else if (bottomStyleLeft.contains("page")) {
				bottomData += ",\"type\":\"page\"}";
			} else {
				bottomData += "}";
			}

			bottomData += ",\"center\":{\"option\":\"" + bottomStyleCenter + "\"";
			if (null != bottomCenterDataText) {
				bottomData += ",\"data\":\"" + JSONObject.escape(bottomCenterDataText) + "\",\"type\":\"text\"}";
			} else if (null != bottomCenterDataImageString) {
				bottomData += ",\"data\":\"" + bottomCenterDataImageString + "\",\"type\":\"image\"}";
			} else if (bottomStyleCenter.contains("page")) {
				bottomData += ",\"type\":\"page\"}";
			} else {
				bottomData += "}";
			}

			bottomData += ",\"right\":{\"option\":\"" + bottomStyleRight + "\"";
			if (null != bottomRightDataText) {
				bottomData += ",\"data\":\"" + JSONObject.escape(bottomRightDataText) + "\",\"type\":\"text\"}";
			} else if (null != bottomRightDataImageString) {
				bottomData += ",\"data\":\"" + bottomRightDataImageString + "\",\"type\":\"image\"}";
			} else if (bottomStyleRight.contains("page")) {
				bottomData += ",\"type\":\"page\"}";
			} else {
				bottomData += "}";
			}
			bottomData += "}";

			if (removeLogo) {
				// delete logo
				StoreFile file = company.getStoreFile(TUserAccountDataAccess.C1_LOGO);
				company.put(TUserAccountDataAccess.C1_LOGO, "");
				Defaults.getInstance().removeFile(file.getPath(), company.getId(), TUserAccountDataAccess.USER_MODULE,
						TUserAccountDataAccess.C1_LOGO, "Category");
			} else {
				if (logo != null) {
					StoreUtils su = new StoreUtils();
					String storedFile = su.moveFileToCategoriesStores(TUserAccountDataAccess.USER_MODULE,
							Integer.valueOf(company.getId()), TUserAccountDataAccess.C1_LOGO, logo);
					File file = new File(Defaults.getInstance().getStoreDir() + storedFile);
					Defaults.getInstance().generateThumb(file);
					company.put(TUserAccountDataAccess.C1_LOGO, storedFile);
				}
			}

			company.put(TUserAccountDataAccess.C1_COVER_TITLE_FONT, fontFamilyCoverTitle);
			company.put(TUserAccountDataAccess.C1_COVER_TITLE_SIZE, fontSizeCoverTitle);
			company.put(TUserAccountDataAccess.C1_COVER_COMPANY_FONT, fontFamilyCoverCompany);
			company.put(TUserAccountDataAccess.C1_COVER_COMPANY_SIZE, fontSizeCoverCompany);
			company.put(TUserAccountDataAccess.C1_BODY_FONT, fontFamilyBody);
			company.put(TUserAccountDataAccess.C1_BODY_SIZE, fontSizeBody);
			company.put(TUserAccountDataAccess.C1_BODY_COLOR, fontColorBody);
			company.put(TUserAccountDataAccess.C1_SECTION_FONT, fontFamilySECTION);
			company.put(TUserAccountDataAccess.C1_SECTION_SIZE, fontSizeSECTION);
			company.put(TUserAccountDataAccess.C1_SECTION_CUSTOM_FONT_COLOR, customSectionColor);
			company.put(TUserAccountDataAccess.C1_SECTION_HEADER_COLOR, fontColorSectionTitle);
			company.put(TUserAccountDataAccess.C1_SECTION_UPPERCASE, uppercaseSectionTitle);
			company.put(TUserAccountDataAccess.C1_SECTION_UNDERLINE, underlineSectionTitle);
			company.put(TUserAccountDataAccess.C1_SECTION_BOLD, boldSectionTitle);
			company.put(TUserAccountDataAccess.C1_H1_FONT, fontFamilyH1);
			company.put(TUserAccountDataAccess.C1_H1_SIZE, fontSizeH1);
			company.put(TUserAccountDataAccess.C1_H1_CUSTOM_FONT_COLOR, customHeading1Color);
			company.put(TUserAccountDataAccess.C1_H1_HEADER_COLOR, fontColorHeading1);
			company.put(TUserAccountDataAccess.C1_H1_UPPERCASE, uppercaseHeading1);
			company.put(TUserAccountDataAccess.C1_H1_UNDERLINE, underlineHeading1);
			company.put(TUserAccountDataAccess.C1_H1_BOLD, boldHeading1);
			company.put(TUserAccountDataAccess.C1_H2_FONT, fontFamilyH2);
			company.put(TUserAccountDataAccess.C1_H2_SIZE, fontSizeH2);
			company.put(TUserAccountDataAccess.C1_H2_CUSTOM_FONT_COLOR, customHeading2Color);
			company.put(TUserAccountDataAccess.C1_H2_HEADER_COLOR, fontColorHeading2);
			company.put(TUserAccountDataAccess.C1_H2_UPPERCASE, uppercaseHeading2);
			company.put(TUserAccountDataAccess.C1_H2_UNDERLINE, underlineHeading2);
			company.put(TUserAccountDataAccess.C1_H2_BOLD, boldHeading2);
			company.put(TUserAccountDataAccess.C1_H3_FONT, fontFamilyH3);
			company.put(TUserAccountDataAccess.C1_H3_SIZE, fontSizeH3);
			company.put(TUserAccountDataAccess.C1_H3_CUSTOM_FONT_COLOR, customHeading3Color);
			company.put(TUserAccountDataAccess.C1_H3_HEADER_COLOR, fontColorHeading3);
			company.put(TUserAccountDataAccess.C1_H3_UPPERCASE, uppercaseHeading3);
			company.put(TUserAccountDataAccess.C1_H3_UNDERLINE, underlineHeading3);
			company.put(TUserAccountDataAccess.C1_H3_BOLD, boldHeading3);
			company.put(TUserAccountDataAccess.C1_PALETTE1, palette1);
			company.put(TUserAccountDataAccess.C1_PALETTE2, palette2);
			company.put(TUserAccountDataAccess.C1_PALETTE3, palette3);
			company.put(TUserAccountDataAccess.C1_PALETTE4, palette4);
			company.put(TUserAccountDataAccess.C1_BACKGROUND_COLOUR, backgroundColour);
			company.put(TUserAccountDataAccess.C1_SECTION_BACKGROUND_COLOUR, sectionBackgroundColour);
			company.put(TUserAccountDataAccess.C1_PROPOSAL_TABLE_STYLE, tableStyle);
			company.put(TUserAccountDataAccess.C1_PROPOSAL_TAB_STYLE, tabStyle);
			company.put(TUserAccountDataAccess.C1_PROPOSAL_SHOW_TABS, showTabs);
			company.put(TUserAccountDataAccess.C1_PROPOSAL_HIDE_COVERPAGE, showCoverPage);
			company.put(TUserAccountDataAccess.C1_PROPOSAL_HIDE_COVERPAGE_PROPOSAL_INFO, coverPageProposalInfo);
			company.put(TUserAccountDataAccess.C1_PROPOSAL_HIDE_COVERPAGE_SENDER_INFO, coverPageSenderInfo);
			company.put(TUserAccountDataAccess.C1_PROPOSAL_HIDE_COVERPAGE_BOX_BACKGROUND, coverPageBoxBackground);
			company.put(TUserAccountDataAccess.C1_PROPOSAL_COVERPAGE_FONT_COLOR, coverPageColor);
			company.put(TUserAccountDataAccess.C1_CONTENT_PADDING_LEFT,
					contentLeftPadding.trim().equalsIgnoreCase("") ? 0 : Integer.valueOf(contentLeftPadding));
			company.put(TUserAccountDataAccess.C1_CONTENT_PADDING_TOP,
					contentTopPadding.trim().equalsIgnoreCase("") ? 0 : Integer.valueOf(contentTopPadding));
			company.put(TUserAccountDataAccess.C1_CONTENT_PADDING_RIGHT,
					contentRightPadding.trim().equalsIgnoreCase("") ? 0 : Integer.valueOf(contentRightPadding));
			company.put(TUserAccountDataAccess.C1_CONTENT_PADDING_BOTTOM,
					contentBottomPadding.trim().equalsIgnoreCase("") ? 0 : Integer.valueOf(contentBottomPadding));

			// Page Styles
			company.put(TUserAccountDataAccess.C1_HEADER_TOP_ACTIVE, headerTopActive);
			company.put(TUserAccountDataAccess.C1_HEADER_BOTTOM_ACTIVE, headerBottomActive);
			company.put(TUserAccountDataAccess.C1_HEADER_TOP_BACKGROUND, headerTopBackground);
			company.put(TUserAccountDataAccess.C1_HEADER_BOTTOM_BACKGROUND, headerBottomBackground);
			company.put(TUserAccountDataAccess.C1_HEADER_TOP_FONT_COLOR, headerTopFontColor);
			company.put(TUserAccountDataAccess.C1_HEADER_BOTTOM_FONT_COLOR, headerBottomFontColor);
			company.put(TUserAccountDataAccess.C1_HEADER_TOP_DATA, topData);
			company.put(TUserAccountDataAccess.C1_HEADER_BOTTOM_DATA, bottomData);

			tuada.updateCompanySettings(company, request.getServletContext());
			transactionManager.commit();
		} catch (Exception e) {
			logger.fatal("Error updating company styles", e);
		}

	}

	private String saveHeaderFile(File image, CategoryData company, String location, String altString) {
		if (image != null) {
			StoreUtils su = new StoreUtils();
			String storedFile = su.moveFileToCategoriesStores(TUserAccountDataAccess.USER_MODULE,
					Integer.valueOf(company.getId()), location, image);
			File file = new File(Defaults.getInstance().getStoreDir() + storedFile);
			Defaults.getInstance().generateThumb(file);
			return storedFile;
		}
		return altString;
	}

	@RequestMapping("/loadStatusReasons")
	@ResponseBody
	public String loadStatusReasons(HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws SQLException {

		String reasons = null;
		return reasons;
	}

	@RequestMapping("/updateSettings")
	@ResponseBody
	public boolean updateSettings(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String expiryDate, @RequestParam String locale,
			@RequestParam(required = false) String orderNowRedirectLink) {
		boolean result = false;
		try (TransactionManager transactionManager = TransactionManager.getInstance()) {

			TUserAccountDataAccess tada = new TUserAccountDataAccess(transactionManager);
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

			TUserRolesDataAccess urda = new TUserRolesDataAccess(transactionManager);
			UserPermissions permissions = urda.getPermissions(userId);
			if (!permissions.hasManageSettingsPermission()) {
				// TODO: should return message to show in the browser (BaseResponse)
				logger.error("User " + userId + " tried to edit styles but doesn't have permission for it.");
				transactionManager.rollback();
				return false;
			}
			CategoryData companyData = tada.getUserCompanyForUserId(userId);
			String companyId = companyData.getId();

			boolean estimatedValue = (request.getParameter("estimatedValue") != null);
			boolean hideTermsAlways = (request.getParameter("hideTermsAlways") != null);
			boolean roundingValue = (request.getParameter("roundingValue") != null);
			boolean enableTax = (request.getParameter("enableTax") != null);
			boolean enableAddition = (request.getParameter("enableAddition") != null);
			boolean enableTeamEditing = (request.getParameter("enableTeamEditing") != null);
			boolean enableEncryptedDataSync = (request.getParameter("enableEncryptedDataSync") != null);

			String taxLabel = null == request.getParameter("taxLabel") ? "" : request.getParameter("taxLabel");
			String taxPercentage = null == request.getParameter("taxPercentage") ? ""
					: request.getParameter("taxPercentage");
			String orderNowLabel = request.getParameter("orderNowLabel");
			String dateformat = request.getParameter("dateformat");
			String timeZone = request.getParameter("timeZone");
			String subjectLineDefault = request.getParameter("subjectLine");
			String statusSubjectString = request.getParameter("statusSubjectString");
			String statusReasonJSON = request.getParameter("statusReasonJSON");
			int passwordExpiryDate = Integer.parseInt(request.getParameter("passwordExpiryDate"));

//			boolean encryptClientData = request.getParameter("encryptClientData") != null;

			TextFile jsonFile = new TextFile(statusReasonJSON, TUserAccountDataAccess.USER_MODULE,
					TUserAccountDataAccess.C1_STATUS_REASON_TEXT, companyData.getId(), true);

			CategoryData company = new CategoryData();
			company.put(TUserAccountDataAccess.C_ID, companyId);
			company.put(TUserAccountDataAccess.C1_ESTIMATED_VALUE, estimatedValue);
			company.put(TUserAccountDataAccess.C1_HIDE_TERMS_ALWAYS, hideTermsAlways);
			company.put(TUserAccountDataAccess.C1_ROUNDING, roundingValue);
			company.put(TUserAccountDataAccess.C1_ENABLE_TAX, enableTax);
			company.put(TUserAccountDataAccess.C1_ENABLE_ADDITION, enableAddition);
			company.put(TUserAccountDataAccess.PATCH_ALLOW_TEAM_EDITING, enableTeamEditing);
			company.put(TUserAccountDataAccess.ALLOW_ENCRYPTED_DATA_SYNC, enableEncryptedDataSync);
			company.put(TUserAccountDataAccess.C1_TAX_LABEL, taxLabel);
			company.put(TUserAccountDataAccess.C1_TAX_PERCENTAGE, taxPercentage);
			company.put(TUserAccountDataAccess.C1_EXPIRY_DATE, expiryDate);
			company.put(TUserAccountDataAccess.C1_PASSWORD_EXPIRY_DATE, passwordExpiryDate);
			company.put(TUserAccountDataAccess.C1_LOCALE, locale);
			company.put(TUserAccountDataAccess.C1_TIME_ZONE, timeZone);
			company.put(TUserAccountDataAccess.C1_EMAIL_STATUS_DEFAULT, subjectLineDefault);
			company.put(TUserAccountDataAccess.C1_EMAIL_STATUS_STRING, statusSubjectString);
			company.put(TUserAccountDataAccess.C1_DATE_FORMAT, dateformat);
			company.put(TUserAccountDataAccess.C1_ORDER_NOW_REDIRECT_LINK, orderNowRedirectLink);
			company.put(TUserAccountDataAccess.C1_ORDER_NOW_LABEL, orderNowLabel);
			company.put(TUserAccountDataAccess.C1_STATUS_REASON_TEXT, jsonFile);
//			company.put(TUserAccountDataAccess.C1_ENCRYPT_CLIENT_DATA, encryptClientData);

			tada.updateCompanySettings(company, request.getServletContext());

			String readyEmailTemplate = request.getParameter("readyEmailTemplate");
			String revisionEmailTemplate = request.getParameter("revisionEmailTemplate");

//			String pricingInfoText = request.getParameter("pricingInfoText");
//			String wantMoreDetail = request.getParameter("wantMoreDetail");

			EmailTemplateDataAccess etda = new EmailTemplateDataAccess();
			StaticTextDataAccess stda = new StaticTextDataAccess();

//			if (StringUtils.isNotBlank(readyEmailTemplate))
//			{
//				etda.updateTemplate(userId, "Proposal Ready", readyEmailTemplate);
//			}
//			if (StringUtils.isNotBlank(revisionEmailTemplate))
//			{
//				etda.updateTemplate(userId, "Proposal New Revision Ready", revisionEmailTemplate);
//			}

			////////
//			if (StringUtils.isBlank(pricingInfoText))
//			{
//				pricingInfoText = "";
//			}
//			if (StringUtils.isBlank(wantMoreDetail))
//			{
//				wantMoreDetail = "";
//			}
//			stda.setContentWithId(companyId, StaticTextDataAccess.EHEADLINE_PRICING, pricingInfoText);
//			stda.setContentWithId(companyId, StaticTextDataAccess.EHEADLINE_PRICING_WANT_MORE_DETAIL, wantMoreDetail);
			transactionManager.commit();
			result = true;
		} catch (Exception e) {
			logger.fatal("Error updating company settings", e);
		}

		return result;
	}

	@RequestMapping("/updateSystemSettings")
	@ResponseBody
	public boolean updateSystemSettings(HttpServletRequest request, HttpSession session) {
		try (TransactionManager transactionManager = TransactionManager.getInstance()) {
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			String nonReturnExpiry = request.getParameter("nonReturnExpiry");
			String returnExpiry = request.getParameter("returnExpiry");

			TUserAccountDataAccess uada = new TUserAccountDataAccess(transactionManager);
			CategoryData company = uada.getUserCompanyForUserId(userId);
			boolean success = uada.updateCompanySystemSettings(company, nonReturnExpiry, returnExpiry);
			transactionManager.commit();
			return success;
		} catch (Exception e) {
			logger.fatal("Oh No!", e);
		}
		return false;
	}

	@RequestMapping("/updateEmailTemplates")
	@ResponseBody
	public boolean updateEmailTemplates(HttpServletRequest request, HttpSession session) {
		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		try {
			String subject1 = request.getParameter("subject1");
			String subject2 = request.getParameter("subject2");
			String readyEmailTemplate = request.getParameter("readyEmailTemplate");
			String revisionEmailTemplate = request.getParameter("revisionEmailTemplate");

			EmailTemplateDataAccess etda = new EmailTemplateDataAccess();

			if (StringUtils.isNotBlank(readyEmailTemplate)) {
				etda.updateTemplate(userId, "Itinerary Ready", subject1, readyEmailTemplate);
			}
			if (StringUtils.isNotBlank(revisionEmailTemplate)) {
				etda.updateTemplate(userId, "Itinerary New Revision Ready", subject2, revisionEmailTemplate);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@RequestMapping("/readDefaultEmailTemplate")
	@ResponseBody
	public Map<String, String> readDefaultContent(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, @RequestParam String name) {

		return new EmailTemplateDataAccess().readDefaultTemplate(name);
	}

	@RequestMapping("/updateProposalBottomText")
	@ResponseBody
	public boolean updateProposalBottomText(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		boolean result = false;
		try (TransactionManager transactionManager = TransactionManager.getInstance()) {

			TUserAccountDataAccess tada = new TUserAccountDataAccess(transactionManager);
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

			TUserRolesDataAccess urda = new TUserRolesDataAccess(transactionManager);
			UserPermissions permissions = urda.getPermissions(userId);
			if (!permissions.hasManageSettingsPermission()) {
				// TODO: should return message to show in the browser (BaseResponse)
				logger.error("User " + userId + " tried to edit styles but doesn't have permission for it.");
				transactionManager.rollback();
				return false;
			}
			CategoryData companyData = tada.getUserCompanyForUserId(userId);
			String companyId = companyData.getId();

			String wantMoreDetail = request.getParameter("wantMoreDetail");

			StaticTextDataAccess stda = new StaticTextDataAccess();

			if (StringUtils.isBlank(wantMoreDetail)) {
				wantMoreDetail = "";
			}
			stda.setContentWithId(companyId, StaticTextDataAccess.EHEADLINE_PRICING_WANT_MORE_DETAIL, wantMoreDetail);
			transactionManager.commit();
			result = true;
		} catch (Exception e) {
			logger.fatal("Error updating company settings", e);
		}

		return result;
	}

	@RequestMapping("/updatePricingTableStyles")
	@ResponseBody
	public Boolean updatePricingTableTitle(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		boolean result = false;
		try (TransactionManager transactionManager = TransactionManager.getInstance()) {

			TUserAccountDataAccess tada = new TUserAccountDataAccess(transactionManager);
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

			TUserRolesDataAccess urda = new TUserRolesDataAccess(transactionManager);
			UserPermissions permissions = urda.getPermissions(userId);
			if (!permissions.hasManageSettingsPermission()) {
				// TODO: should return message to show in the browser (BaseResponse)
				logger.error("User " + userId + " tried to edit styles but doesn't have permission for it.");
				transactionManager.rollback();
				return false;
			}
			CategoryData companyData = tada.getUserCompanyForUserId(userId);
			String companyId = companyData.getId();

			String pricingTitle = request.getParameter("pricingTitle");
			String borderStyleType = request.getParameter("borderStyleType");
			String borderStyleSpacing = request.getParameter("borderStyleSpacing");
			String borderStyleColor = request.getParameter("borderStyleColor");

			CategoryData company = new CategoryData();
			company.setId(companyId);
			company.put(UserAccountDataAccess.C1_PRICING_TITLE, pricingTitle);
			company.put(UserAccountDataAccess.C1_TABLE_STYLE_LINE, borderStyleType);
			company.put(UserAccountDataAccess.C1_TABLE_STYLE_COLOR, borderStyleColor);
			company.put(UserAccountDataAccess.C1_TABLE_STYLE_WEIGHT, borderStyleSpacing);

			tada.updateCompanySettings(company, request.getServletContext());

			transactionManager.commit();

			result = true;
		} catch (Exception e) {
			logger.fatal("Error updating company settings", e);
		}

		return result;
	}

	@RequestMapping("/updateAccountSettings")
	@ResponseBody
	public AjaxResponse updateAccountSettings(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, @RequestParam String companyName) {
		try (TransactionManager transactionManager = TransactionManager.getInstance()) {

			TUserAccountDataAccess tada = new TUserAccountDataAccess(transactionManager);
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

			TUserRolesDataAccess urda = new TUserRolesDataAccess(transactionManager);
			UserPermissions permissions = urda.getPermissions(userId);
			if (!permissions.hasManageSettingsPermission()) {
				// TODO: should return message to show in the browser (BaseResponse)
				logger.error("User " + userId + " tried to edit styles but doesn't have permission for it.");
				transactionManager.rollback();
				return new AjaxResponse(false, "You don't have permission to do it.");
			}
			CategoryData companyData = tada.getUserCompanyForUserId(userId);
			String companyId = companyData.getId();

			String abnacn = request.getParameter("abnacn");
			String addressLine1 = request.getParameter("addressLine1");
			String addressLine2 = request.getParameter("addressLine2");
			String citySuburb = request.getParameter("citySuburb");
			String postcode = request.getParameter("postcode");
			String state = request.getParameter("state");
			String userPaymentEmailString = request.getParameter("payment-user-email-list");
			List<String> emailList = new ArrayList<>();
			List<String> rawList = Arrays.asList(userPaymentEmailString.split("\\s*,\\s*"));
			for (String email : rawList) {
				Pattern pattern = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
				Matcher mat = pattern.matcher(email);
				if (mat.matches()) {
					emailList.add(email);
				}
			}
			MultiOptions billingEmailList = new MultiOptions(companyData.getId(), UserAccountDataAccess.C_BILLING_USERS,
					UserAccountDataAccess.USER_MODULE, companyData.getType(), emailList);

			CategoryData company = new CategoryData();
			company.put(TUserAccountDataAccess.C_ID, companyId);
			company.put(TUserAccountDataAccess.C_ABN, abnacn);
			company.put(TUserAccountDataAccess.C_ADDRESS_LINE_1, addressLine1);
			company.put(TUserAccountDataAccess.C_ADDRESS_LINE_2, addressLine2);
			company.put(TUserAccountDataAccess.C_ADDRESS_CITY_SUBURB, citySuburb);
			company.put(TUserAccountDataAccess.C_ADDRESS_POSTCODE, postcode);
			company.put(TUserAccountDataAccess.C_ADDRESS_STATE, state);
			company.put(TUserAccountDataAccess.C_BILLING_USERS, billingEmailList);

			tada.updateCompanySettings(company);

			// Update COMPANY NAME
			TransactionModuleAccess ma = TransactionModuleAccess.getInstance(transactionManager);
			CategoryData companyTable = tada.getCompanyWithId(companyId);
			if (!StringUtils.equals(companyTable.getName(), companyName)) {
				WebdirectorDataAccess wda = new WebdirectorDataAccess();
				wda.changeCompanyCategoryToModules(companyId, companyName, ma);
			}
			transactionManager.commit();
			return new AjaxResponse(true, null);

		} catch (StripeException e) {
			logger.fatal("Error updateAccountSettings", e);
			return new AjaxResponse(false, e.getMessage());
		} catch (Exception e) {
			logger.fatal("Error updateAccountSettings", e);
			return new AjaxResponse(false,
					"It wasn't possible to change the settings. Contact your Qcloud administrator.");
		}
	}

	@RequestMapping("/updateFirstLoginSettings")
	@ResponseBody
	public boolean updateFirstLogin(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		boolean result = false;
		try (TransactionManager transactionManager = TransactionManager.getInstance()) {
			TUserAccountDataAccess tada = new TUserAccountDataAccess(transactionManager);
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

			TUserRolesDataAccess urda = new TUserRolesDataAccess(transactionManager);
			UserPermissions permissions = urda.getPermissions(userId);
			if (!permissions.hasManageSettingsPermission()) {
				// TODO: should return message to show in the browser (BaseResponse)
				logger.error("User " + userId + " tried to edit styles but doesn't have permission for it.");
				transactionManager.rollback();
				return false;
			}
			CategoryData company = tada.getUserCompanyForUserId(userId);

			String locale = request.getParameter("locale");
			String timeZone = request.getParameter("timeZone");
			company.put(TUserAccountDataAccess.C1_LOCALE, locale);
			company.put(TUserAccountDataAccess.C1_TIME_ZONE, timeZone);
			company.put(TUserAccountDataAccess.C1_FIRST_LOGIN, "1");

			tada.updateCompanySettings(company, request.getServletContext());
			transactionManager.commit();
			result = true;
		} catch (Exception e) {
			logger.fatal("Error updating company settings", e);
		}

		return result;
	}

	@RequestMapping("/updateFirstLoginTemplates")
	@ResponseBody
	public boolean updateFirstLoginTemplates(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		return false;
	}

	@RequestMapping("/updateTeam")
	public void updateTeam(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		HashMap<String, Object> responseObject = new HashMap<String, Object>();
		try (TransactionManager txManager = TransactionManager.getInstance()) {
			TUserAccountDataAccess ada = new TUserAccountDataAccess(txManager);
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			CategoryData companyData = ada.getUserCompanyForUserId(userId);
			String companyId = companyData.getId();
			TUserRolesDataAccess urda = new TUserRolesDataAccess(txManager);
			UserPermissions up = urda.getPermissions(userId);

			String id = request.getParameter("id");
			CategoryData team = ada.getTeamForTeamId(id);
			String teamName = request.getParameter("teamName");
			String action = request.getParameter("action");
			String[] productFolders = request.getParameterValues("productFolders[]");
			String[] textFolders = request.getParameterValues("textFolders[]");
			String[] imageFolders = request.getParameterValues("imageFolders[]");
			String[] videoFolders = request.getParameterValues("videoFolders[]");
			String[] pdfFolders = request.getParameterValues("pdfFolders[]");
			String[] coverPageFiles = request.getParameterValues("coverPageFiles[]");
			String[] templateFiles = request.getParameterValues("templateFiles[]");
			boolean productSwitch = "true".equals(request.getParameter("productSwitch"));
			boolean textSwitch = "true".equals(request.getParameter("textSwitch"));
			boolean imageSwitch = "true".equals(request.getParameter("imageSwitch"));
			boolean videoSwitch = "true".equals(request.getParameter("videoSwitch"));
			boolean pdfSwitch = "true".equals(request.getParameter("pdfSwitch"));
			boolean coverpageSwitch = "true".equals(request.getParameter("coverpageSwitch"));
			boolean templateSwitch = "true".equals(request.getParameter("templateSwitch"));

			String proposalTemplate = request.getParameter("proposalTemplate");
			String itiStyle = request.getParameter("itiStyle");
			boolean autoSendItinerary = "true".equals(request.getParameter("autoSendItinerary"));

			String arrivalGuideApiKey = request.getParameter("arrivalGuideApiKey");

//			String travelportBranchCode = request.getParameter("travelportBranchCode");
//			String travelportPassword = request.getParameter("travelportPassword");

			List<String> productFolderArray = new ArrayList<String>();
			if (productFolders != null) {
				productFolderArray = Arrays.asList(productFolders);
			}

			List<String> textFolderArray = new ArrayList<String>();
			if (textFolders != null) {
				textFolderArray = Arrays.asList(textFolders);
			}

			List<String> imageFolderArray = new ArrayList<String>();
			if (imageFolders != null) {
				imageFolderArray = Arrays.asList(imageFolders);
			}

			List<String> videoFolderArray = new ArrayList<String>();
			if (videoFolders != null) {
				videoFolderArray = Arrays.asList(videoFolders);
			}

			List<String> pdfFolderArray = new ArrayList<String>();
			if (pdfFolders != null) {
				pdfFolderArray = Arrays.asList(pdfFolders);
			}

			List<String> coverPageFileArray = new ArrayList<String>();
			if (coverPageFiles != null) {
				coverPageFileArray = Arrays.asList(coverPageFiles);
			}

			List<String> templateFileArray = new ArrayList<String>();
			if (templateFiles != null) {
				templateFileArray = Arrays.asList(templateFiles);
			}

			Boolean editCheck = up.hasManageUsersPermission() && team != null && team.getParentId().equals(companyId)
					&& action.equals("edit");
			Boolean addCheck = up.hasManageUsersPermission() && action.equals("add");
			if (editCheck) {
				ada.updateTeam(companyId, id, teamName, productFolderArray, textFolderArray, imageFolderArray,
						videoFolderArray, pdfFolderArray, coverPageFileArray, templateFileArray, productSwitch,
						textSwitch, imageSwitch, videoSwitch, pdfSwitch, coverpageSwitch, templateSwitch, itiStyle,
						proposalTemplate, autoSendItinerary, arrivalGuideApiKey);
			} else if (addCheck) {
				ada.createTeam(companyId, teamName, productFolderArray, textFolderArray, imageFolderArray,
						videoFolderArray, pdfFolderArray, coverPageFileArray, templateFileArray, productSwitch,
						textSwitch, imageSwitch, videoSwitch, pdfSwitch, coverpageSwitch, templateSwitch, itiStyle,
						proposalTemplate, arrivalGuideApiKey, autoSendItinerary);
			}

			txManager.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
			responseObject.put("message", e.getMessage());
			responseObject.put("success", false);
			// return responseObject;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping("/updateRole")
	@ResponseBody
	public HashMap<String, Object> updateRole(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		HashMap<String, Object> responseObject = new HashMap<String, Object>();
		try (TransactionManager txManager = TransactionManager.getInstance()) {
			TUserAccountDataAccess ada = new TUserAccountDataAccess(txManager);
			TUserRolesDataAccess urda = new TUserRolesDataAccess(txManager);
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			CategoryData companyData = ada.getUserCompanyForUserId(userId);
			String companyId = companyData.getId();
			UserPermissions up = urda.getPermissions(userId);

			String id = request.getParameter("id");
			String action = request.getParameter("action");
			String roleName = request.getParameter("roleName");
//			Boolean manageTemplates = Boolean.parseBoolean(request.getParameter("manageTemplates"));
			Boolean manageProjects = Boolean.parseBoolean(request.getParameter("manageProjects"));
			Boolean manageUsers = Boolean.parseBoolean(request.getParameter("manageUsers"));
			Boolean manageCompanySettings = Boolean.parseBoolean(request.getParameter("manageCompanySettings"));

			ElementData roleData = urda.getRoleForId(id);
			CategoryData roleCategoryData = urda.getRoleFolderCompanyId(companyId);

			Boolean editCheck = up.hasManageUsersPermission() && action.equals("edit") && roleData != null
					&& roleCategoryData.getString(urda.C_COMPANY_ID).equals(companyId);
			Boolean addCheck = up.hasManageUsersPermission() && action.equals("add");
			if (editCheck) {
				ElementData role = urda.updateRole(roleCategoryData.getId(), id, roleName, manageProjects, manageUsers,
						manageCompanySettings);
				RolesValidation validation = new RolesValidation(txManager, role);
				List<String> errors = validation.validateRoles(companyId);
				if (errors.size() > 0) {
					txManager.rollback();
					responseObject.put("message", StringUtils.join(errors, ","));
					responseObject.put("success", false);
				} else {
					txManager.commit();
					responseObject.put("message", "Succesfully updated role.");
					responseObject.put("success", true);
				}
			} else if (addCheck) {
				ElementData role = urda.createRole(roleCategoryData.getId(), roleName, manageProjects, manageUsers,
						manageCompanySettings);
				RolesValidation validation = new RolesValidation(txManager, role);
				List<String> errors = validation.validateRoles(companyId);
				if (errors.size() > 0) {
					txManager.rollback();
					responseObject.put("message", StringUtils.join(errors, ","));
					responseObject.put("success", false);
				} else {
					txManager.commit();
					responseObject.put("message", "Succesfully added role.");
					responseObject.put("success", true);
				}
			} else {
				txManager.rollback();
				responseObject.put("message", "Could not create/update role");
				responseObject.put("success", false);
			}
			return responseObject;
		} catch (RuntimeException e) {
			e.printStackTrace();
			responseObject.put("message", e.getMessage());
			responseObject.put("success", false);
			return responseObject;
		} catch (Exception e) {
			logger.error("Error: ", e);
			return null;
		}
	}

	@RequestMapping("/userNotExists")
	@ResponseBody
	public boolean userNotExists(HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws SQLException {
		String id = request.getParameter("id");
		String emailAddress = request.getParameter("emailAddress");
		return "0".equals(TransactionDataAccess.getInstance().selectString(
				"select count(1) from elements_useraccounts where attr_headline = ? and element_id != ?", emailAddress,
				id));
	}

	@RequestMapping("/sabreIdValid")
	@ResponseBody
	public boolean sabreIdValid(HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws SQLException {
		String id = request.getParameter("id");
		String sabreConsultantId = request.getParameter("sabreConsultantId");
		return "0".equals(TransactionDataAccess.getInstance().selectString(
				"select count(1) from elements_sabre where ATTR_consultantId = ? and ATTRDROP_userId != ?",
				sabreConsultantId, id));
	}

	@RequestMapping("/travelportBranchCodeValid")
	@ResponseBody
	public boolean travelportBranchCodeValid(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws SQLException {
		String id = request.getParameter("id");
		String travelportBranchCode = request.getParameter("travelportBranchCode");
		return "0".equals(TransactionDataAccess.getInstance().selectString(
				"select count(1) from categories_travelport where ATTR_branchCode = ? and ATTRDROP_team != ?",
				travelportBranchCode, id));
	}

	@RequestMapping("/travelportIdValid")
	@ResponseBody
	public boolean travelportIdValid(HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws SQLException {
		String id = request.getParameter("id");
		String travelportConsultantId = request.getParameter("travelportConsultantId");
		return "0".equals(TransactionDataAccess.getInstance().selectString(
				"select count(1) from elements_travelport where ATTR_consultantId = ? and ATTRDROP_user != ?",
				travelportConsultantId, id));
	}

	@RequestMapping("/reachMaxUserLimit")
	@ResponseBody
	public AjaxResponse reachMaxUserLimit(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		try (TransactionManager txManager = TransactionManager.getInstance()) {
			TUserAccountDataAccess ada = new TUserAccountDataAccess(txManager);
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			CategoryData company = ada.getUserCompanyForUserId(userId);
			SubscriptionPlan currentPlan = SubscriptionPlan.getPlanByName(company.getString(ada.C_ADDRESS_COUNTRY),
					company.getString(ada.C_PRICING_PLAN));
			if (SubscriptionPlan.isTrialPlan(currentPlan.getName())) {
				return new AjaxResponse(false, "");
			}
			if (!UserAccountDataAccess.isPayNeeded(company)) {
				return new AjaxResponse(false, "");
			}
			SubscriptionPlan additionalUserPlan = SubscriptionPlan
					.getAdditionalUserPlan(company.getString(ada.C_ADDRESS_COUNTRY));
			int userAmount = ada.countUsers(company.getId()) + 1;
			additionalUserPlan.setQuantity(userAmount - currentPlan.getMaxUsers());
			if (additionalUserPlan.getQuantity() <= 0) {
				return new AjaxResponse(false, "");
			}
			String msg = "";
			NumberFormat nf = new DecimalFormat("0.00");
			if (additionalUserPlan.getQuantity() == 1) {
				msg = "You will exceed your " + currentPlan.getMaxUsers() + " user plan limit.";
			} else {
				msg = "You have exceeded your " + currentPlan.getMaxUsers() + " user plan limit.";
			}
			msg += " Our pricing for QuoteCloud is " + additionalUserPlan.getCurrencySymbol()
					+ nf.format(additionalUserPlan.getPrice()) + " per User with "
					+ ((int) (additionalUserPlan.getMaxData() * 1000)) + "Mb data storage.";

//			SubscriptionPlan additionalDataPlan = SubscriptionPlan.getAdditionalDataPlan(company.getString(ada.C_ADDRESS_COUNTRY));
//			double diskUsageGB = new DiskUsageDataAccess().getCurrentDiskUsageGB(company.getId());
//			double maxDiskUsageGB = currentPlan.getMaxData() + additionalUserPlan.getMaxData() * additionalUserPlan.getQuantity();
//			int newDataPlanQty = new BigDecimal(diskUsageGB - maxDiskUsageGB).divide(new BigDecimal(additionalDataPlan.getMaxData())).setScale(0, BigDecimal.ROUND_CEILING).intValue();
//			int oldDataPlanQty = company.getInt(ada.C1_ADD_DATA_PLAN_QTY);
			return new AjaxResponse(true, msg);

		} catch (Exception e) {
			logger.error("Error", e);
			return new AjaxResponse(false, "");
		}
	}

	@RequestMapping("/updateUser")
	@ResponseBody
	public Map<String, Object> updateUser(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		Hashtable<String, Object> responseObject = new Hashtable<>();
		try (TransactionManager txManager = TransactionManager.getInstance()) {
			TUserAccountDataAccess ada = new TUserAccountDataAccess(txManager);
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			CategoryData companyData = ada.getUserCompanyForUserId(userId);
			String companyId = companyData.getId();
			TUserRolesDataAccess urda = new TUserRolesDataAccess(txManager);
			UserPermissions up = urda.getPermissions(userId);

			String id = request.getParameter("id");
			String action = request.getParameter("action");
			String username = request.getParameter("username");
			String email = request.getParameter("emailAddress");
			String name = request.getParameter("name");
			String surname = request.getParameter("surname");
			String password = request.getParameter("resetPassword");
			String phone = request.getParameter("phone");
//			String team = request.getParameter("teams");
			String[] roles = request.getParameterValues("roles[]");
			String[] manages = request.getParameterValues("manages[]");
			String[] proposalSendApproval = request.getParameterValues("proposalSendApproval[]");
//			Boolean dailyEmailSummary = Boolean.parseBoolean(request.getParameter("dailyEmailSummary"));
//			Boolean newCommentEmail = Boolean.parseBoolean(request.getParameter("newCommentEmail"));
//			Boolean handoverEmail = Boolean.parseBoolean(request.getParameter("handoverEmail"));
//			Boolean proposalStatusChangeEmail = Boolean.parseBoolean(request.getParameter("proposalStatusChangeEmail"));
//			Boolean proposalOpenedEmail = Boolean.parseBoolean(request.getParameter("proposalOpenedEmail"));

			List<String> rolesArray = new ArrayList<String>();
			if (roles != null) {
				rolesArray = Arrays.asList(roles);
			}

			List<String> managesArray = new ArrayList<String>();
			if (manages != null) {
				managesArray = Arrays.asList(manages);
			}

			List<String> proposalSendApprovalArray = new ArrayList<String>();
			if (proposalSendApproval != null) {
				proposalSendApprovalArray = Arrays.asList(proposalSendApproval);
			}

			ElementData userData = null;
			String editUserCompany = "";
			if (StringUtils.isNotBlank(id)) {
				userData = ada.getUserWithId(id);
				editUserCompany = ada.getUserCompanyForUserId(id).getId();
			}

			CategoryData company = ada.getCompanyWithId(companyId);
			int userCount = ada.getUserCountInCompanyWithCompanyId(companyId);

			List<CategoryData> teams = ada.getCompanyTeams(companyId);
			CategoryData teamData = teams.get(0);
			Boolean updateUser = true;
			Boolean editCheck = up.hasManageUsersPermission() && action.equals("edit") && userData != null
					&& editUserCompany.equals(companyId) && teamData.getParentId().equals(companyId);
			Boolean addCheck = up.hasManageUsersPermission() && action.equals("add") && StringUtils.isNotBlank(username)
					&& teamData != null && teamData.getParentId().equals(companyId);
			ElementData user = null;
			if (!editCheck && !addCheck) {
				txManager.rollback();
				responseObject.put("message", "Could not create/update user");
				responseObject.put("success", false);
				return responseObject;
			}
			if (editCheck) {
				user = ada.updateUser(id, teamData.getId(), name, surname, password, phone, rolesArray, managesArray,
						proposalSendApprovalArray);
			} else {
				surname = surname + " (Pending Verification)";
				// password = PasswordUtils.encrypt(password);
				user = ada.createUser(teamData.getId(), username, email, name, surname, password, phone, rolesArray,
						managesArray, proposalSendApprovalArray);
				UserRegisterController urc = new UserRegisterController();
				String activationCode = new SecurityKeyService().getActivationKey(username, password);
				urc.sendUserAccountEmailVerification(request, user, activationCode, email);
				updateUser = false;
			}
			UsersValidation validation = new UsersValidation(txManager, user);
			List<String> errors = validation.validateUsers(companyId);

			if (errors.size() > 0) {
				txManager.rollback();
				responseObject.put("message", StringUtils.join(errors, ","));
				responseObject.put("success", false);
				return responseObject;
			}

			txManager.commit();
			responseObject.put("message", updateUser ? "Sucessfully updated user."
					: "User " + email + " created successfully and has been sent an email to activate their account.");
			responseObject.put("success", true);
			return responseObject;
		} catch (Exception e) {
			logger.error("Error", e);
			responseObject.put("message", e.getMessage());
			responseObject.put("success", false);
			return responseObject;
		}
	}

	@RequestMapping("/resetPassword")
	public void resetPassword(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		String resetPassword = request.getParameter("resetPassword");
		String confirmPassword = request.getParameter("confirmPassword");
	}

	@RequestMapping("/resendActivation")
	@ResponseBody
	public Hashtable<String, Object> resendActivation(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		Hashtable<String, Object> responseObject = new Hashtable<>();
		try {
			String userIDSelected = request.getParameter("userSelected");
			if (userIDSelected.length() < 1) {
				userIDSelected = null;
			}

			String userID = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

			UserAccountDataAccess uada = new UserAccountDataAccess();

			String companyId = uada.getUserCompanyForUserId(userID).getId();

			if (userIDSelected != null) {
				Hashtable<String, ElementData> company = uada.getCompanyUsers(companyId);
				ElementData user = company.get(userIDSelected);
				if (user != null) {
					String email = user.getString(E_HEADLINE);
					UserRegisterController urc = new UserRegisterController();
					String activationCode = new SecurityKeyService().getActivationKey(email,
							user.getString(UserAccountDataAccess.E_RESET_PASSWORD));
					urc.sendUserAccountEmailVerification(request, user, activationCode, email);

					responseObject.put("message", "An activation email has been sent to " + email);
					responseObject.put("success", true);
					return responseObject;
				}
			}
		} catch (Exception e) {
			logger.error("Error:", e);
		}
		responseObject.put("message", "Could not resend an activation email. Please try again later.");
		responseObject.put("success", false);
		return responseObject;
	}

	public class AttachmentResponse {
		private int fileId;
		private boolean success;

		public AttachmentResponse(int fileId, boolean success) {
			this.fileId = fileId;
			this.success = success;
		}

		public boolean isSuccess() {
			return success;
		}

		public void setSuccess(boolean success) {
			this.success = success;
		}

		public int getFileId() {
			return fileId;
		}

		public void setFileId(int fileId) {
			this.fileId = fileId;
		}
	}

	@RequestMapping("/updateNotifications")
	public void updateNotifications(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam(required = false) String emailDailySummary,
			@RequestParam(required = false) String emailNewComment,
			@RequestParam(required = false) String emailHandover,
			@RequestParam(required = false) String emailStatusChange,
			@RequestParam(required = false) String emailProposalOpened) throws IOException {
		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		UserAccountDataAccess ada = new UserAccountDataAccess();
		ada.updateNotificationPreferences(userId, emailDailySummary != null, emailNewComment != null,
				emailHandover != null, emailStatusChange != null, emailProposalOpened != null);
	}

	@RequestMapping("/free/login")
	@ResponseBody
	public AuthenticationResponse login(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam(required = false) String username, @RequestParam(required = false) String password)
			throws IOException, SQLException {
		username = username.trim();
		password = password.trim();
		// TODO: authenticate the user.
		UserAccountDataAccess ada = new UserAccountDataAccess();
		AuthenticationResponse authResponse = ada.authenticate(username, password);
		Hashtable<String, String> user = ada.getUserByUserName(username);

		if (authResponse.status.isOK()) {
			logger.info("start logging user in....");
			processSuccessfulLogin(request, response, session, username, authResponse.status, authResponse.userId,
					authResponse.companyID, false);
			String redirectTo = request.getParameter(Constants.PORTAL_LOGIN_REDIRECT_PARAM);
			if (UserController.isTrialPeriodOver(session)[0]) {
				logger.info("user trial account expired, sending email....");
				sendUserLoginAfterExpiryEmail(request);
			} else {
				if (StringUtils.isBlank(redirectTo)) {
					request.removeAttribute(Constants.PORTAL_LOGIN_REDIRECT_PARAM);
				}

				if (redirectTo != null && redirectTo.length() > 0) {
					authResponse.message = redirectTo;
				} else {
					authResponse.message = request.getContextPath() + "/dashboard";
				}
				logger.info("user trial account hasn't expired, do nothing....");
			}
		} else if (authResponse.status == AuthenticationStatus.TWO_FACTOR) {
			session.setAttribute("authenticationUser", username);
			session.setAttribute("authenticationPassword", password);
			if (!UserAccountDataAccess.isUserUsingMobileTFA(user)) {
				String randomKey = new SecurityKeyService().generateRandomKey(6, 6, "");
				logger.debug("VerificationCode:" + randomKey);
				session.setAttribute("authenticationKey", randomKey);
				boolean emailSent = new EmailBuilder().prepareLoginTwoFactor(randomKey)
						.addTo(user.get(UserAccountDataAccess.E_EMAIL_ADDRESS)).doSend();
				authResponse.message = "Enter code sent to you via email.";
			} else {
				authResponse.message = "Enter code generated on your authentication app.";
			}
		} else if (authResponse.status == AuthenticationStatus.CHANGE_PASSWORD) {
			session.setAttribute(Constants.PORTAL_CHANGE_PASSWORD_ATTR, user.get(UserAccountDataAccess.E_ID));
		} else {
			loginFailed(response, session, authResponse, username);
			authResponse.message = request.getContextPath() + "/";
		}
		return authResponse;
	}

	@RequestMapping("/free/login/authenticate")
	@ResponseBody
	public AuthenticationResponse authenticateTwoFactor(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, @RequestParam String token) throws IOException, SQLException {
		token = token.replaceAll("\\s+", "");
		String verificationKey = (String) session.getAttribute("authenticationKey");
		String username = (String) session.getAttribute("authenticationUser");
		String password = (String) session.getAttribute("authenticationPassword");

		UserAccountDataAccess ada = new UserAccountDataAccess();

		Hashtable<String, String> user = ada.getUserByUserName(username);

		boolean twoFactorAuthenticated = false;

		if (!UserAccountDataAccess.isUserUsingMobileTFA(user)) {
			twoFactorAuthenticated = verificationKey.equalsIgnoreCase(token);
		} else {
			GoogleAuthenticator gAuth = new GoogleAuthenticator();
			GoogleAuthUtils gUtils = new GoogleAuthUtils();
			gAuth.setCredentialRepository(gUtils);
			twoFactorAuthenticated = gAuth.authorizeUser(user.get("Element_id"), Integer.valueOf(token));
		}

		AuthenticationResponse authResponse = ada.authenticate(username, password, twoFactorAuthenticated);

		if (authResponse.status.isOK()) {
			logger.info("start logging user in....");
			processSuccessfulLogin(request, response, session, username, authResponse.status, authResponse.userId,
					authResponse.companyID, false);
			String redirectTo = request.getParameter(Constants.PORTAL_LOGIN_REDIRECT_PARAM);
			session.removeAttribute("authenticationKey");
			session.removeAttribute("authenticationUser");
			session.removeAttribute("authenticationPasswords");
			if (UserController.isTrialPeriodOver(session)[0]) {
				logger.info("user trial account expired, sending email....");
				sendUserLoginAfterExpiryEmail(request);
			} else {
				if (StringUtils.isBlank(redirectTo)) {
					request.removeAttribute(Constants.PORTAL_LOGIN_REDIRECT_PARAM);
				}

				if (redirectTo != null && redirectTo.length() > 0) {
					authResponse.message = redirectTo;
				} else {
					authResponse.message = request.getContextPath() + "/dashboard";
				}
				logger.info("user trial account hasn't expired, do nothing....");
			}
		} else if (authResponse.status == AuthenticationStatus.CHANGE_PASSWORD) {
			session.setAttribute(Constants.PORTAL_CHANGE_PASSWORD_ATTR, authResponse.userId);
			session.setAttribute(Constants.PORTAL_USERNAME_ATTR, username);
			response.sendRedirect(request.getContextPath() + "/changePassword");
		} else if (authResponse.status == AuthenticationStatus.TWO_FACTOR) {
			authResponse.status = AuthenticationStatus.TWO_FACTOR_INCORRECT;
			authResponse.message = "Two factor incorrect";
		} else {
			loginFailed(response, session, authResponse, username);
			authResponse.message = request.getContextPath() + "/";
		}
		return authResponse;
	}

	@RequestMapping("/free/loginas/verification-submit")
	public void verify(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String verificationCode, @RequestParam String email) throws IOException, SQLException {
		verificationCode = verificationCode.trim();
		String verificationKey = (String) session.getAttribute("loginVerificationKey");

		if (StringUtils.equals(verificationCode, verificationKey)) {
			try {
				UserController.autoLogin(request, response, session, email);
			} catch (Exception e) {
				logger.error("Error:", e);
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		} else {
			session.setAttribute(Constants.VERIFICATION_ERROR, "Verification code incorrect.");
			response.sendRedirect("/verification");
		}
	}

	@RequestMapping("/free/loginas/verificationUser")
	public void verificationUser(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String verificationCode, @RequestParam String email, @RequestParam String customerEmail)
			throws IOException, SQLException {
		verificationCode = verificationCode.trim();
		String verificationKey = (String) session.getAttribute("loginVerificationKey");
		String timeApproved = request.getParameter("time");

		if (StringUtils.equals(verificationCode, verificationKey)) {
			if ((timeApproved != null) && (!Objects.equals(timeApproved, ""))) {

				try (TransactionManager transactionManager = TransactionManager.getInstance()) {
					TransactionModuleAccess tma = TransactionModuleAccess.getInstance(transactionManager);
					ElementData ele = tma.getElementByName(UserAccountDataAccess.USER_MODULE, customerEmail);
					Calendar c = Calendar.getInstance();
					c.setTime(new Date());

					if (Integer.parseInt(timeApproved) > 1) {
						c.add(Calendar.HOUR, 24);
					} else {
						c.add(Calendar.HOUR, 1);
					}

					ele.put(UserAccountDataAccess.C1_SUPPORT_TIME, c.getTime());
					tma.updateElement(ele, UserAccountDataAccess.USER_MODULE);
					transactionManager.commit();
				} catch (Exception e) {
					logger.error("Approved support time could not be added ERROROROROROROROOR", e);
				}
			}
			new EmailBuilder().prepareSingleSignupVerification(verificationKey, customerEmail).addTo(email).doSend();
			new EmailBuilder().prepareSingleSignupRequest(customerEmail, email, true)
					.addTo("support@corporateinteractive.com.au").doSend();
			response.sendRedirect(request.getContextPath() + "/security/verificationUser");
		} else {
			request.setAttribute("title", "Oops!");// default message
			request.setAttribute("message", "Your Security link is not valid.");
//			request.setAttribute("email", email);
			response.sendRedirect(request.getContextPath() + "/security/genericMessage");
		}
	}

	@RequestMapping("/free/loginas/verificationsubmitter")
	public void verificationsubmitter(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String verificationCode, @RequestParam String email) throws IOException, SQLException {
		verificationCode = verificationCode.trim();
		String verificationKey = (String) session.getAttribute("loginVerificationKey");

		if (StringUtils.equals(verificationCode, verificationKey)) {
			try {
				UserController.autoLogin(request, response, session, email);
			} catch (Exception e) {
				logger.error("Error:", e);
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		} else {
			session.setAttribute(Constants.VERIFICATION_ERROR, "Verification code incorrect.");
			response.sendRedirect("/verification");
		}
	}

	public static void autoLogin(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			String username) throws SQLException, IOException {
		ElementData user = ModuleAccess.getInstance().getElementByName(UserAccountDataAccess.USER_MODULE, username);
		CategoryData company = new UserAccountDataAccess().getUserCompanyForUserId(user.getId());
		new UserAccountReportingDataAccess().recordLastLogin(user.getId());
		processSuccessfulLogin(request, response, session, username, AuthenticationStatus.OK_USER, user.getId(),
				company.getId(), true);
	}

	public static String processLoginVerification(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, String username) throws SQLException, IOException {
		ElementData user = ModuleAccess.getInstance().getElementByName(UserAccountDataAccess.USER_MODULE, username);
		CategoryData company = new UserAccountDataAccess().getUserCompanyForUserId(user.getId());
		new UserAccountReportingDataAccess().recordLastLogin(user.getId());
		return processSuccessfulLoginVerification(request, response, session, username, AuthenticationStatus.OK_USER,
				user.getId(), company.getId());
	}

	private static String processSuccessfulLoginVerification(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, String username, AuthenticationStatus status, String userId, String companyId)
			throws IOException {
		configureLoginSuccess(session, username, userId, companyId, status);
		String redirectTo = request.getParameter(Constants.PORTAL_LOGIN_REDIRECT_PARAM);
		if (StringUtils.isBlank(redirectTo)) {
			redirectTo = (String) request.getAttribute(Constants.PORTAL_LOGIN_REDIRECT_PARAM);
			request.removeAttribute(Constants.PORTAL_LOGIN_REDIRECT_PARAM);
		}

		if (redirectTo != null && redirectTo.length() > 0) {
			return redirectTo;
		} else {
			return request.getContextPath() + "/dashboard";
		}
	}

	private static void processSuccessfulLogin(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, String username, AuthenticationStatus status, String userId, String companyID,
			boolean redirect) throws IOException {
		configureLoginSuccess(session, username, userId, companyID, status);
		if (redirect) {
			redirectForLogin(request, response);
		}
	}

	public static boolean[] isTrialPeriodOver(HttpSession session) {
		UserAccountDataAccess uada = new UserAccountDataAccess();
		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		CategoryData company = uada.getUserCompanyForUserId(userId);
		return new boolean[] { uada.isTrialPeriodOver(company), uada.trialPeriodOverInWeek(company) > -1 };
	}

	private static void redirectForLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String redirectTo = request.getParameter(Constants.PORTAL_LOGIN_REDIRECT_PARAM);
		if (StringUtils.isBlank(redirectTo)) {
			redirectTo = (String) request.getAttribute(Constants.PORTAL_LOGIN_REDIRECT_PARAM);
			request.removeAttribute(Constants.PORTAL_LOGIN_REDIRECT_PARAM);
		}

		if (redirectTo != null && redirectTo.length() > 0) {
			response.sendRedirect(redirectTo);
		} else {
			response.sendRedirect(request.getContextPath() + "/dashboard");
		}
	}

	@RequestMapping("/free/changePassword")
	public void doChangePassword(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam(required = false) String currentPassword, @RequestParam String newPassword,
			@RequestParam String repeatPassword) throws IOException, SQLException {
		if (currentPassword != null)
			currentPassword = currentPassword.trim();
		newPassword = newPassword.trim();
		repeatPassword = repeatPassword.trim();

		String userId = (String) session.getAttribute(Constants.PORTAL_CHANGE_PASSWORD_ATTR);
		String userName = (String) session.getAttribute(Constants.PORTAL_USERNAME_ATTR);
		if (userId == null) {
			userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		}

		if (newPassword.equals(repeatPassword)) {
			AuthenticationResponse authResponse = null;

			UserAccountDataAccess ada = new UserAccountDataAccess();
			CategoryData company = ada.getUserCompanyForUserId(userId);
			Hashtable<String, String> userInfo = ada.getUserWithId(userId);
			if (currentPassword == null && userInfo.get(ada.E_RESET_PASSWORD) != null)
				currentPassword = userInfo.get(ada.E_RESET_PASSWORD);
			authResponse = ada.changePassword(userId, currentPassword, newPassword);

			if (authResponse.status.isOK()) {
				WebdirectorDataAccess.resetWebdirectorPasswordIfExistsAndFirstLogin(userName, newPassword);
				configureLoginSuccess(session, userName, userId, company.getId(), authResponse.status);
				redirectForLogin(request, response);
				return;
			}
			session.setAttribute(Constants.SESSION_CHANGE_PASSWORD_ERROR_MSG, authResponse.message);
		} else {
			session.setAttribute(Constants.SESSION_CHANGE_PASSWORD_ERROR_MSG, "Two new passwords do not match.");
		}

		response.sendRedirect(request.getContextPath() + "/changePassword");
	}

	@RequestMapping("/free/logout")
	public void logout(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam(required = false, defaultValue = "true") boolean rebroadcast) {

		try {
			session.invalidate();// Maybe exception?
		} catch (IllegalStateException e) {
			logger.warn("Unable to invalidate session. This is Normal");
		}

		try {
			response.sendRedirect("/");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void loginFailed(HttpServletResponse response, HttpSession session, AuthenticationResponse auth,
			String username) throws IOException {
		session.setAttribute(Constants.LOGIN_ERROR, auth.message);
		session.setAttribute(Constants.LOGIN_STATUS, auth.status.name());
		session.setAttribute(Constants.LOGIN_ERROR_USERNAME, username);
		// response.sendRedirect("/");
	}

	private void loginTwoFactor(HttpServletResponse response, HttpSession session, AuthenticationResponse auth,
			String username) throws IOException {
		session.setAttribute(Constants.LOGIN_STATUS, auth.status.name());
	}

	protected static void configureLoginSuccess(HttpSession session, String username, String userId, String companyId,
			AuthenticationStatus success) {
		configureLoginSuccess(session, username, userId, companyId, success, session.getId());
	}

	protected static void configureLoginSuccess(HttpSession session, String username, String userId, String companyId,
			AuthenticationStatus success, String singleSignOnId) {
		// UserAccountDataAccess uada = new UserAccountDataAccess();

		// Hashtable<String, String> user = uada.getUserWithId(userId);

		session.setAttribute(Constants.PORTAL_SSO_SESSION_ID_ATTR, singleSignOnId);
		session.setAttribute(Constants.PORTAL_ALLOW_SEND_ATTR, Constants.PORTAL_ALLOW_SEND_ATTR);
		session.setAttribute(Constants.PORTAL_USERNAME_ATTR, username);
		session.setAttribute(Constants.PORTAL_USER_ID_ATTR, userId);
		session.setAttribute(Constants.PORTAL_COMPANY_ID_ATTR, companyId);
		session.setAttribute(Constants.LOGIN_ERROR, null);
	}

	private void sendUserLoginAfterExpiryEmail(HttpServletRequest request) {
		String userId = (String) request.getSession().getAttribute(Constants.PORTAL_USER_ID_ATTR);
		UserAccountDataAccess uada = new UserAccountDataAccess();
		CategoryData company = uada.getUserCompanyForUserId(userId);
		Hashtable<String, String> user = uada.getUserWithId(userId);
		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");

		new EmailBuilder(request).addModule(UserAccountDataAccess.USER_MODULE, user)
				.addModule(UserAccountDataAccess.USER_MODULE, company).prepareUserLoginAfterExpiryEmail()
				.addTo(adminEmail.split(",")).doSend();
	}

	private void sendUserAccountActivationEmail(HttpServletRequest request, Hashtable<String, String> userHash,
			String activationCode) {
		new EmailBuilder(request).addModule(UserAccountDataAccess.USER_MODULE, userHash)
				.prepareAccountVerificationEmail(activationCode)
				.addTo(userHash.get(UserAccountDataAccess.E_EMAIL_ADDRESS)).doSend();
	}

	public static String randomPassword() {
		return randomPassword("aaa#0000");
	}

	public static String randomPassword(String types) {
		String[] words = new String[] { "dancing", "running", "crawling",

				"spongy",

				"red", "blue", "yellow", "green", "white", "purple", "pink", "orange",

				"cat", "fish", "unicorn", "kangaroo", "tiger", "spider", "turtle", "Llama", "meerkat", "donkey", "dog",
				"whale" };
		int[] digits = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		char[] symbols = new char[] { '#', '!', '$', '@' };

		StringBuffer sb = new StringBuffer();
		for (char c : types.toCharArray()) {
			String s = "";
			if (Character.toString(c).matches("[0-9]")) {
				sb.append(digits[(int) Math.floor(Math.random() * digits.length)]);
			} else if (Character.toString(c).matches("[a-zA-Z]")) {
				String word = words[(int) Math.floor(Math.random() * words.length)];
				sb.append(WordUtils.capitalize(word));
			} else {
				sb.append(symbols[(int) Math.floor(Math.random() * symbols.length)]);
			}
		}
		return sb.toString();
	}

	// MARKET FUCNTIONS
	@RequestMapping("/market/sabre/updateUser")
	@ResponseBody
	public Map<String, Object> updateSabreUser(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		Hashtable<String, Object> responseObject = new Hashtable<>();
		try (TransactionManager txManager = TransactionManager.getInstance()) {
			TUserAccountDataAccess ada = new TUserAccountDataAccess(txManager);

			String id = request.getParameter("id");
			String sabreConsultantId = request.getParameter("sabreConsultantId");

			if (StringUtils.isNotBlank(id)) {
				ElementData userData = ada.getUserWithId(id);

//				if (MarketSubscriberDataAccess.getInstance().checkAppAccessForTeam(userData.getParentId(), Market.QCSABRE))
//				{
//					new SabreDataAccess().updateSabreConultant(userData.getParentId(), userData, sabreConsultantId);
//				}
			} else {
				txManager.rollback();
				responseObject.put("message", "Could not update user");
				responseObject.put("success", false);
				return responseObject;
			}

			txManager.commit();
			responseObject.put("message", "Sucessfully updated user.");
			responseObject.put("success", true);
			return responseObject;
		} catch (Exception e) {
			logger.error("Error", e);
			responseObject.put("message", e.getMessage());
			responseObject.put("success", false);
			return responseObject;
		}
	}

	@RequestMapping("/market/travelport/updateUser")
	@ResponseBody
	public Map<String, Object> updateTravelportUser(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		Hashtable<String, Object> responseObject = new Hashtable<>();
		try (TransactionManager txManager = TransactionManager.getInstance()) {
			TUserAccountDataAccess ada = new TUserAccountDataAccess(txManager);

			String id = request.getParameter("id");
			String travelportConsultantId = request.getParameter("travelportConsultantId");

			if (StringUtils.isNotBlank(id)) {
				ElementData userData = ada.getUserWithId(id);

//				if (MarketSubscriberDataAccess.getInstance().checkAppAccessForTeam(userData.getParentId(), Market.QCTRAVELPORT))
//				{
//					new TravelportDataAccess().updateTravelportConsultant(userData.getParentId(), userData, travelportConsultantId);
//				}
			} else {
				txManager.rollback();
				responseObject.put("message", "Could not update user");
				responseObject.put("success", false);
				return responseObject;
			}

			txManager.commit();
			responseObject.put("message", "Sucessfully updated user.");
			responseObject.put("success", true);
			return responseObject;
		} catch (Exception e) {
			logger.error("Error", e);
			responseObject.put("message", e.getMessage());
			responseObject.put("success", false);
			return responseObject;
		}
	}

	@RequestMapping("/market/sabre/updateTeam")
	public void updateSabreTeam(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		try (TransactionManager txManager = TransactionManager.getInstance()) {
			TUserAccountDataAccess ada = new TUserAccountDataAccess(txManager);
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			CategoryData companyData = ada.getUserCompanyForUserId(userId);
			String companyId = companyData.getId();

			String id = request.getParameter("id");
			CategoryData team = ada.getTeamForTeamId(id);

			String proposalTemplate = request.getParameter("proposalTemplate");
			String itiStyle = request.getParameter("itiStyle");
			boolean autoSendItinerary = "true".equals(request.getParameter("autoSendItinerary"));
			boolean autoDeleteItinerary = "true".equals(request.getParameter("autoDeleteItinerary"));
			String noOfMonths = request.getParameter("noOfMonths");

			String sabreAgencyCode = request.getParameter("sabreAgencyCode");
			String sabrePassword = request.getParameter("sabrePassword");

			ada.updateSabreTeam(companyId, id, team.getName(), sabreAgencyCode, sabrePassword, itiStyle,
					proposalTemplate, autoSendItinerary, autoDeleteItinerary, noOfMonths);

			txManager.commit();
		} catch (Exception e) {
			logger.error("Error", e);
		}
	}

	@RequestMapping("/market/travelport/updateTeam")
	public void updateTravelportTeam(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		try (TransactionManager txManager = TransactionManager.getInstance()) {
			TUserAccountDataAccess ada = new TUserAccountDataAccess(txManager);
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			CategoryData companyData = ada.getUserCompanyForUserId(userId);
			String companyId = companyData.getId();

			String id = request.getParameter("id");
			CategoryData team = ada.getTeamForTeamId(id);

			String travelportBranchCode = request.getParameter("travelportBranchCode");
			String travelportPassword = request.getParameter("travelportPassword");

			String proposalTemplate = request.getParameter("proposalTemplate");
			String itiStyle = request.getParameter("itiStyle");
			boolean autoSendItinerary = "true".equals(request.getParameter("autoSendItinerary"));
			boolean autoDeleteItinerary = "true".equals(request.getParameter("autoDeleteItinerary"));
			String noOfMonths = request.getParameter("noOfMonths");

			ada.updateTravelportTeam(companyId, id, team.getName(), travelportBranchCode, travelportPassword, itiStyle,
					proposalTemplate, autoSendItinerary, autoDeleteItinerary, noOfMonths);

			txManager.commit();
		} catch (Exception e) {
			logger.error("Error", e);
		}

	}

	@RequestMapping("/market/qcpayment/updateTeam")
	public void updateQcpaymentTeam(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		HashMap<String, Object> responseObject = new HashMap<String, Object>();
		try (TransactionManager txManager = TransactionManager.getInstance()) {
			TUserAccountDataAccess ada = new TUserAccountDataAccess(txManager);
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			CategoryData companyData = ada.getUserCompanyForUserId(userId);
			String companyId = companyData.getId();
			TUserRolesDataAccess urda = new TUserRolesDataAccess(txManager);
			UserPermissions up = urda.getPermissions(userId);

			String id = request.getParameter("id");
			String stripePK = request.getParameter("stripePK");
			String stripeSK = request.getParameter("stripeSK");

			ada.updateStripeTeam(companyId, id, stripePK, stripeSK);

			txManager.commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
			responseObject.put("message", e.getMessage());
			responseObject.put("success", false);
			// return responseObject;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
