//package au.corporateinteractive.qcloud.proposalbuilder.control;
//
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.HashSet;
//import java.util.Hashtable;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Set;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import au.corporateinteractive.qcloud.proposalbuilder.db.PlanDataAccess;
//import au.corporateinteractive.qcloud.proposalbuilder.model.pricing.PricingOptions;
//import au.net.webdirector.common.datalayer.base.db.DataMap;
//import au.net.webdirector.common.datalayer.client.ElementData;
//import au.net.webdirector.common.datalayer.client.ModuleAccess;
//import au.net.webdirector.common.datalayer.util.StringArrayUtils;
//
//@Controller
//public class PricingController extends AbstractController
//{
//
//	Logger logger = Logger.getLogger(PricingController.class);
//
//	@RequestMapping("/pricing/loadPricesAndDependencies")
//	@ResponseBody
//	public List<PricingOptions> loadPlanPricing(HttpServletRequest request, HttpServletResponse response, HttpSession session,
//			@RequestParam(required = false) String[] pricingOptions)
//	{
//		logger.info("load prices...");
//		if (pricingOptions == null)
//		{
//			pricingOptions = new String[] {};
//		}
//		Set<String> plans = cda.getCategoryIdByElements(PlanDataAccess.PLANS_MODULE, pricingOptions);
//
//		HashSet<Integer> pricingOptionSet = new HashSet<Integer>();
//		for (String s : pricingOptions)
//		{
//			pricingOptionSet.add(Integer.parseInt(s));
//		}
//
//		List<PricingOptions> matchedOptions = new LinkedList<PricingOptions>();
//		for (String plan : plans)
//		{
//			List<PricingOptions> matches = getPlanItems(plan);
//
//			//We ONLY want to restore the original pricing options.
//			// We must not return the matches which dont match what was passed in.
//			for (PricingOptions option : matches)
//			{
//				HashSet<Integer> restoreSetCopy = new HashSet<Integer>(pricingOptionSet);
//				restoreSetCopy.removeAll(option.getTerms().keySet());
//				if (restoreSetCopy.size() != pricingOptionSet.size())
//				{
//					matchedOptions.add(option);
//				}
//			}
//		}
//
//		return matchedOptions;
//	}
//
//	@RequestMapping("/pricing/loadPlanPricing")
//	@ResponseBody
//	public List<PricingOptions> loadPlanPricing(HttpServletRequest request, HttpServletResponse response, HttpSession session,
//			@RequestParam String plan)
//	{
//
//		List<PricingOptions> matches = getPlanItems(plan);
//
//		return matches;
//	}
//
//	private List<PricingOptions> getPlanItems(String planId)
//	{
//		List<PricingOptions> response = new LinkedList<PricingOptions>();
//		try
//		{
//			String[] planIds = StringUtils.split(planId, ",");
//			String sql = "select * from elements_" + PlanDataAccess.PLANS_MODULE + " e";
//			sql += " where " + standardLiveClauseForCustomQuery("e");
//			sql += " and e.category_Id in (" + StringUtils.join(StringArrayUtils.fillArray("?", planIds.length), ",") + ")  order by attr_headline";
//			List<ElementData> elements = ModuleAccess.getInstance().getElements(sql, planIds);
//
//			Hashtable<String, PricingOptions> uniqueResponses = new Hashtable<String, PricingOptions>();
//
//			for (ElementData element : elements)
//			{
//				String pricingOption = element.getName();
//				String cost = element.getString(PlanDataAccess.E_COST);
//
//				PricingOptions option = null;
//				if (uniqueResponses.containsKey(pricingOption))
//				{
//					option = uniqueResponses.get(pricingOption);
//					option.addTermPricing(element);
//				}
//				else
//				{
//					option = new PricingOptions(element);
//
//					uniqueResponses.put(pricingOption, option);
//					response.add(option);
//				}
//			}
//		}
//		catch (SQLException e)
//		{
//			logger.error("Error:", e);
//			// TODO Auto-generated catch block
//		}
//		return response;
//
//	}
//
//	@RequestMapping("/pricing/content")
//	@ResponseBody
//	public Hashtable<String, Object> content(HttpServletRequest request, HttpServletResponse response, HttpSession session,
//			@RequestParam(required = false) String id)
//	{
//		try
//		{
//			Hashtable<String, Object> ht = new Hashtable<String, Object>();
//			ElementData element = ModuleAccess.getInstance().getElementById("PRODUCTS", id);
//
//			if (element == null) {
//				return null;
//			}
//			Hashtable<String, String> misc = new Hashtable<String, String>();
//
//			ht.put("pricingId", id);
//			ht.put("title", element.getString("ATTR_headline"));
//			ht.put("type", "price");
//			ht.put("label", element.getString("ATTR_description"));
//			ht.put("unitPrice", element.get("ATTRCURRENCY_Price"));
//			ht.put("term", element.get("ATTR_term"));
//			ht.put("isOneOff", element.getBoolean("ATTRCHECK_oneOff"));
//			ht.put("note", element.get("ATTR_note") != null ? element.get("ATTR_note") : "");
//			ht.put("misc", misc);
//
//			misc.put("attr_customfield1", element.getString("attr_customfield1"));
//			misc.put("attr_customfield2", element.getString("attr_customfield2"));
//			misc.put("attr_customfield3", element.getString("attr_customfield3"));
//			misc.put("attr_customfield4", element.getString("attr_customfield4"));
//			misc.put("attr_customfield5", element.getString("attr_customfield5"));
//
//			List<DataMap> breaks = new ArrayList<>();
//
//			for (int i = 1; i < 6; i++)
//			{
//				if (StringUtils.isNotBlank(element.getString("attr_label" + i)))
//				{
//					DataMap _break = new DataMap();
//					_break.put("label", element.getString("attr_label" + i));
//					_break.put("qty", element.getDouble("attrfloat_qty" + i));
//					_break.put("price", element.getDouble("attrcurrency_price" + i));
//					breaks.add(_break);
//				}
//			}
//
//			Collections.sort(breaks, new Comparator<DataMap>()
//			{
//				@Override
//				public int compare(DataMap o1, DataMap o2)
//				{
//					return o1.getDecimal("qty").compareTo(o2.getDecimal("qty"));
//				}
//			});
//			ht.put("breaks", breaks);
//			return ht;
//		}
//		catch (SQLException e)
//		{
//			logger.error("Error:", e);
//		}
//
//		return null;
//	}
//}
