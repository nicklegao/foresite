package au.corporateinteractive.qcloud.proposalbuilder.control;

import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopobot.util.URL;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
//import au.corporateinteractive.qcloud.proposalbuilder.db.PlanDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.model.ContentLibraryNode;
import au.corporateinteractive.qcloud.proposalbuilder.model.ContentLibraryResponse;
import au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService;
import au.corporateinteractive.qcloud.proposalbuilder.service.PDFLibraryService;
import au.corporateinteractive.qcloud.proposalbuilder.service.ProductLibraryService;
import au.corporateinteractive.qcloud.proposalbuilder.service.SpreadsheetLibraryService;
import au.corporateinteractive.qcloud.proposalbuilder.service.TextLibraryService;
import au.corporateinteractive.qcloud.proposalbuilder.service.VideoLibraryService;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.MultiOptions;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.conf.ModuleAccessConfig;
import au.net.webdirector.common.datalayer.lucene.LuceneHelper;

@Controller
public class ContentLibraryController extends AbstractController
{
	private static final String MODE_PLAN = "pricing";
	private static final String MODE_PRODUCT = "product";
	private static final String MODE_GENERAL = "general";
	private static final String MODE_COVER = "cover";

	Logger logger = Logger.getLogger(ContentLibraryController.class);

	@RequestMapping("/library/content")
	@ResponseBody
	public ContentLibraryResponse libraryContent(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam(required = false) String parentId,
			@RequestParam(required = false) String type,
			@RequestParam(defaultValue = "") String search,
			@RequestParam boolean leaf,
			@RequestParam String mode) throws Exception
	{
		try (TransactionManager transactionManager = TransactionManager.getInstance())
		{
			CategoryData company = new TUserAccountDataAccess(transactionManager).getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR));
			transactionManager.commit();
			if ("0".equals(parentId))
			{
				parentId = "";
			}
			List<String> parentIds = new ArrayList(Arrays.asList(StringUtils.split(parentId, ",")));
			if (mode.equalsIgnoreCase(MODE_PLAN))
			{
				//			return processPlan(parentIds, companyId, leaf);
				return processGeneral(type, parentIds, MODE_PRODUCT, search, company.getId(), leaf, session);
			}
			//		if (mode.equalsIgnoreCase(MODE_PRODUCT))
			//		{
			//			return processGeneral("product", parentIds, MODE_PRODUCT, companyId, leaf);
			//		}
			if (mode.equalsIgnoreCase(MODE_GENERAL))
			{
				return processGeneral(type, parentIds, MODE_GENERAL, search, company.getId(), leaf, session);
			}
			if (mode.equalsIgnoreCase(MODE_COVER))
			{
				return processGeneral(type, parentIds, MODE_COVER, search, company.getId(), leaf, session);
			}
			return null;
		}
		catch (Exception e)
		{
			logger.fatal("Oh No!", e);
			throw e;
		}

	}

	private ContentLibraryResponse processGeneral(String type, List<String> parentIds, String mode, String search, String companyId, boolean leaf, HttpSession session)
	{

		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		CategoryData team = new UserAccountDataAccess().usersTeam(userId, companyId);
		List<ContentLibraryNode> nodes = new LinkedList<ContentLibraryNode>();
		List<ContentLibraryNode> teamNodes = new LinkedList<ContentLibraryNode>();

		if (mode == MODE_COVER)
		{
			logger.info("There should be no content on the coverpage.");
			return new ContentLibraryResponse(nodes);
		}

		//mode == MODE_GENERAL || mode == MODE_PRODUCT
		if (type == null)
		{
			if (mode == MODE_GENERAL)
			{
				addBasicNode("Text", "file-text", nodes, companyId);
				addBasicNode("Images", "image", nodes, companyId);
				addBasicNode("Videos", "video-camera", nodes, companyId);
				addBasicNode("PDF", "file-pdf-o", nodes, companyId);
				addBasicNode("Spreadsheet", "table", nodes, companyId);
			}
			else
			{
				addBasicNode("Products", "shopping-cart", nodes, companyId);
				addBasicNode("Text", "file-text", nodes, companyId);
				addBasicNode("Images", "image", nodes, companyId);
				addBasicNode("Videos", "video-camera", nodes, companyId);
				addBasicNode("PDF", "file-pdf-o", nodes, companyId);
				addBasicNode("Spreadsheet", "table", nodes, companyId);
			}
			return new ContentLibraryResponse(nodes);
		}

		String moduleName = getModuleName(type);
		String libraryDBRef = TUserAccountDataAccess.libraryDBRef(type);
		String librarySwitchDBRef = TUserAccountDataAccess.librarySwitchDBRef(type);
		boolean libraryActive = team.getBoolean(librarySwitchDBRef);
		List<String> luceneFilter = null;
		if (StringUtils.isNotBlank(search))
		{
			luceneFilter = getLuceneSearch(search, moduleName);
		}

		int level = Defaults.getModuleLevelsInt(moduleName);
		ModuleAccess ma = ModuleAccess.getInstance();
		if (!leaf)
		{
			//Category
			QueryBuffer qb = new QueryBuffer<>();
			qb.append(" select * from categories_" + moduleName + " c, elements_" + moduleName + " e ");
			qb.append(" where e.category_id = c.category_id ");
			if (luceneFilter != null)
			{
				qb.appendIn(" and e.element_Id in ", luceneFilter);
			}
			qb.appendIn(" and c.category_parentId in ", parentIds);
			qb.append(" order by attr_categoryName ");

			try
			{
				MultiOptions activeLibrary = ModuleAccess.getInstance().getMultiOptions(team, libraryDBRef, "users");
				List<CategoryData> folders = ma.getCategories(qb.getSQL(), qb.getParams());
				for (CategoryData folder : folders)
				{
					ContentLibraryNode node = new ContentLibraryNode();
					String id = folder.getId();
					String name = folder.getName();

					node.setType(type);
					node.setLabel(name);
					node.setLeaf(level == folder.getInt("folderLevel"));
					node.setHasChild(true);


					int idx = nodes.indexOf(node);
					if (idx == -1)
					{
						nodes.add(node);
					}
					else
					{
						node = nodes.get(idx);
					}
					node.addId(id);

					if (StringUtils.isNotBlank(team.getId()) && libraryActive == true)
					{
						for (String active : activeLibrary.getValues())
						{
							if (active.equals(folder.getId()))
							{
								int teamIdx = teamNodes.indexOf(node);
								if (teamIdx == -1)
								{
									teamNodes.add(node);
								}
								else
								{
									node = teamNodes.get(teamIdx);
								}
								node.addId(id);
							}
						}
					}
				}
			}
			catch (SQLException e)
			{
				logger.error("Error:", e);
				// TODO Auto-generated catch block
			}
			if (libraryActive == true)
			{
				return new ContentLibraryResponse(teamNodes);
			}
			else
			{
				return new ContentLibraryResponse(nodes);
			}
		}
		else
		{

			if (type.equals("Products") || type.equals("Spreadsheet"))
			{

				//element
				QueryBuffer qb = new QueryBuffer<>();
				qb.append(" select * from elements_" + moduleName + " e ");
				qb.appendIn(" where e.category_Id in ", parentIds);
				qb.append(" and e.live = 1 ");
				if (luceneFilter != null)
				{
					qb.appendIn(" and e.element_Id in ", luceneFilter);
				}
				qb.append(" order by attr_headline ");


				QueryBuffer qbNoFolder = new QueryBuffer<>();
				qbNoFolder.append(" select * from elements_" + moduleName + " e where ATTR_subFolder = '' AND");
				qbNoFolder.appendIn(" e.category_Id in ", parentIds);
				qbNoFolder.append(" and e.live = 1 ");
				if (luceneFilter != null)
				{
					qbNoFolder.appendIn(" AND e.element_Id in ", luceneFilter);
				}
				qbNoFolder.append(" order by attr_headline ");

				try
				{

					List<ElementData> elements = ma.getElements(qb.getSQL(), qb.getParams());
					List<ElementData> elementsNoFolder = ma.getElements(qbNoFolder.getSQL(), qbNoFolder.getParams());
					HashMap<String, List<ElementData>> thisingByVu = new HashMap<>();

					for (ElementData element : elements)
					{
						List<ElementData> thising = thisingByVu.get(element.get("subsub"));
						if (thising == null)
						{
							thising = new ArrayList<>();
						}
						thising.add(element);
						if (element.get("ATTR_subFolder") != null && ((String) element.get("ATTR_subFolder")).length() > 0)
						{
							thisingByVu.put((String) element.get("ATTR_subFolder"), thising);
						}
					}

					for (String vovo : thisingByVu.keySet())
					{
						ContentLibraryNode node = new ContentLibraryNode();
						node.setType(type + "Folder");
						node.addId(vovo);
						node.setLabel(vovo);
						node.setLeaf(true);
						node.setHasChild(true);
						node.addId(vovo);

						nodes.add(node);

					}

					for (ElementData elementNoFolder : elementsNoFolder)
					{
						String id = elementNoFolder.getId();
						String name = elementNoFolder.getName();
						ContentLibraryNode node = new ContentLibraryNode();
						node.setType(type);
						node.addId(id);
						node.setLabel(name);
						node.setLeaf(true);
						node.setHasChild(false);

						nodes.add(node);

					}

				}
				catch (SQLException e)
				{
					logger.error("Error:", e);
					// TODO Auto-generated catch block
				}
				return new ContentLibraryResponse(nodes);

			}
			else if (type.equals("ProductsFolder") || type.equals("SpreadsheetFolder"))
			{

				//element
				QueryBuffer qb = new QueryBuffer<>();
				qb.append(" select * from elements_" + moduleName + " e where ATTR_subFolder = '" + parentIds.get(0) + "' ");
//				qb.appendIn(" where ATTR_subFolder = ", parentIds);
				if (luceneFilter != null)
				{
					qb.appendIn(" and e.element_Id in ", luceneFilter);
				}
				qb.append(" order by attr_headline ");

				try
				{

					List<ElementData> elements = ma.getElements(qb.getSQL(), qb.getParams());

					for (ElementData element : elements)
					{
						String id = element.getId();
						String name = element.getName();
						ContentLibraryNode node = new ContentLibraryNode();
						node.setType(type.substring(0, type.length() - "Folder".length()));
						node.addId(id);
						node.setLabel(name);
						node.setLeaf(true);
						node.setHasChild(false);

						nodes.add(node);

					}

				}
				catch (SQLException e)
				{
					logger.error("Error:", e);
					// TODO Auto-generated catch block
				}
				return new ContentLibraryResponse(nodes);

			}
			else
			{
				//element
				QueryBuffer qb = new QueryBuffer<>();
				qb.append(" select * from elements_" + moduleName + " e ");
				qb.appendIn(" where e.category_Id in ", parentIds);
				qb.append(" and e.live = 1 ");
				if (luceneFilter != null)
				{
					qb.appendIn(" and e.element_Id in ", luceneFilter);
				}
				qb.append(" order by attr_headline ");

				try
				{
					List<ElementData> elements = ma.getElements(qb.getSQL(), qb.getParams());

					for (ElementData element : elements)
					{
						String id = element.getId();
						String name = element.getName();
						ContentLibraryNode node = new ContentLibraryNode();
						node.setType(type);
						node.setLabel(name);
						node.setLeaf(true);
						node.setHasChild(false);
						if (type.equals("Images"))
						{
							node.setThumbnail("/" + Defaults.getInstance().getStoreContext() + StringUtils.replace(element.getString("ATTRFILE_image"), "/ATTRFILE_image/", "/ATTRFILE_image/_thumb/"));
						}
						if (type.equals("Videos"))
						{
							String thumbPath = "";
							if (element.getString("ATTRFILE_thumbnail") == null || element.getString("ATTRFILE_thumbnail").equals(""))
							{
								String videoCode = new URL(element.getString("ATTR_videoLink")).getParameter("v", "");
								thumbPath = "http://img.youtube.com/vi/" + videoCode + "/default.jpg";
							}
							else
							{
								thumbPath = "/" + Defaults.getInstance().getStoreContext() + element.getString("ATTRFILE_thumbnail");
							}
							node.setThumbnail(thumbPath);
						}
						node.addId(id);
						nodes.add(node);

					}
				}
				catch (SQLException e)
				{
					logger.error("Error:", e);
					// TODO Auto-generated catch block
				}
				return new ContentLibraryResponse(nodes);
			}
		}
	}

	/**
	 * @param type
	 * @return
	 */
	String getModuleName(String type)
	{
		if (type.equalsIgnoreCase("Text"))
		{
			return TextLibraryService.TEXT_MODULE;
		}
		if (type.equalsIgnoreCase("Images"))
		{
			return ImageLibraryService.IMAGES_MODULE;
		}
		if (type.equalsIgnoreCase("Videos"))
		{
			return VideoLibraryService.VIDEOS_MODULE;
		}
		if (type.equalsIgnoreCase("Products") || type.equalsIgnoreCase("ProductsFolder"))
		{
			return ProductLibraryService.PRODUCTS_MODULE;
		}
		if (type.equalsIgnoreCase("PDF"))
		{
			return PDFLibraryService.PDF_MODULE;
		}
		if (type.equalsIgnoreCase("Spreadsheet") || type.equalsIgnoreCase("SpreadsheetFolder"))
		{
			return SpreadsheetLibraryService.SPREADSHEET_MODULE;
		}
		logger.fatal("UNABLE TO DETERMINE TYPE:... " + type);
		throw new InvalidParameterException("TYPE:" + type);
	}


	private List<String> getLuceneSearch(String search, String module)
	{
		LuceneHelper lucenecSearch = new LuceneHelper(module);
		List<String> luceneSearchResult = new ArrayList<String>();
		if (!"".equals(search))
		{
			logger.info("Searching " + module + " with \"" + search + "\"");
			luceneSearchResult = lucenecSearch.searchElements(search);
		}
		return luceneSearchResult;
	}

	private void addBasicNode(String label, String icon, List<ContentLibraryNode> nodes, String companyId)
	{
		String sql = "select category_id from categories_" + getModuleName(label);
		sql += " where folderLevel = 1 ";
		sql += " and attrdrop_companyid = ?";
		TransactionDataAccess da = TransactionDataAccess.getInstance();
		try
		{
			List<String> ids = da.selectStrings(sql, companyId);
			ContentLibraryNode textNode = new ContentLibraryNode();
			textNode.setType(label);
			textNode.setIcon(icon);
			textNode.setId(StringUtils.join(ids, ","));
			textNode.setLabel(label);
			textNode.setLeaf(false);
			textNode.setHasChild(true);
			nodes.add(textNode);
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
		}
	}

//	public ContentLibraryResponse processPlan(List<String> parentIds, String companyId, boolean leaf)
//	{
//		List<ContentLibraryNode> nodes = new LinkedList<ContentLibraryNode>();
//		int level = Defaults.getModuleLevelsInt(PlanDataAccess.PLANS_MODULE);
//		ModuleAccess ma = ModuleAccess.getInstance();
//
//		if (parentIds == null || parentIds.size() == 0)
//		{
//			String sql = "select category_id from categories_" + PlanDataAccess.PLANS_MODULE + " where attrdrop_companyid = ?";
//			try
//			{
//				parentIds = ma.getDataAccess().selectStrings(sql, new String[] { companyId });
//			}
//			catch (SQLException e)
//			{
//				logger.error("Error:", e);
//				return new ContentLibraryResponse(new LinkedList<ContentLibraryNode>());
//			}
//		}
//		// pick up all children based on parent id
//		String query = "select * from categories_" + PlanDataAccess.PLANS_MODULE + " c";
//		query += " where " + standardLiveClauseForCustomQuery("c");
//		query += " and c.category_parentId in (" + StringUtils.join(StringArrayUtils.fillArray("?", parentIds.size()), ",") + ") order by c.attr_categoryName";
//		try
//		{
//			List<CategoryData> folders = ma.getCategories(query, parentIds.toArray(new String[] {}));
//			for (CategoryData folder : folders)
//			{
//				String id = folder.getId();
//				String name = folder.getName();
//				int folderLevel = folder.getInt("folderLevel");
//				// skip folders which don't have live elements
//				String sql = "select count(1) from elements_" + PlanDataAccess.PLANS_MODULE + " e";
//				for (int i = folderLevel; i <= level; i++)
//				{
//					sql += ",categories_" + PlanDataAccess.PLANS_MODULE + " c" + i;
//				}
//				sql += " where e.category_id = c" + level + ".category_id ";
//				for (int i = level; i > folderLevel; i--)
//				{
//					sql += " and c" + i + ".category_parentid = c" + (i - 1) + ".category_id";
//				}
//				sql += " and " + standardLiveClauseForCustomQuery("e");
//				sql += " and c" + folderLevel + ".category_id = ?";
//				if (StringUtils.equals("0", ma.getDataAccess().selectString(sql, new String[] { id })))
//				{
//					continue;
//				}
//
//				ContentLibraryNode node = new ContentLibraryNode();
//				node.setLabel(name);
//				node.setLeaf(folderLevel >= (level - 1));
//				node.setHasChild(folderLevel < level);
//				int idx = nodes.indexOf(node);
//				if (idx == -1)
//				{
//					nodes.add(node);
//				}
//				else
//				{
//					node = nodes.get(idx);
//				}
//				node.addId(id);
//			}
//			return new ContentLibraryResponse(nodes);
//		}
//		catch (SQLException e)
//		{
//			logger.error("Error:", e);
//			return new ContentLibraryResponse(new LinkedList<ContentLibraryNode>());
//		}
//
//
//	}

	@RequestMapping("/library/dataitems")
	@ResponseBody
	public HashMap<String, HashMap<String, String>> getDataItems(@RequestParam String module)
	{
		try
		{
			HashMap<String, HashMap<String, String>> dataItemsMap = new HashMap<String, HashMap<String, String>>();
			String dataItemModuleName = "DataItems";
			ModuleAccess ma = ModuleAccess.getInstance();
			ma.setConfig(ModuleAccessConfig.logicalLive().merge(ModuleAccessConfig.orderByDisplay()));
			CategoryData condition = new CategoryData();
			condition.put("attrdrop_module", module);
			List<CategoryData> dataModules = ma.getCategories(dataItemModuleName, condition);
			for (CategoryData dataModule : dataModules)
			{
				List<CategoryData> groups = ma.getCategoriesByParentId(dataItemModuleName, dataModule.getId());
				for (CategoryData group : groups)
				{
					HashMap<String, String> dataItems = new HashMap<String, String>();
					List<ElementData> items = ma.getElementsByParentId(dataItemModuleName, group.getId());
					for (ElementData item : items)
					{
						dataItems.put(item.getName(), item.getString("ATTR_Value"));
					}
					dataItemsMap.put(group.getName(), dataItems);
				}
				return dataItemsMap;
			}
		}
		catch (SQLException e)
		{
			logger.error("Error: " + e);
		}
		return null;
	}
}
