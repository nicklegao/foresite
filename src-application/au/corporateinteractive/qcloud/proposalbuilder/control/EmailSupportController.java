package au.corporateinteractive.qcloud.proposalbuilder.control;

import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import org.apache.xpath.operations.Bool;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder;
import au.net.webdirector.common.utils.email.PendingEmail;

@Controller
public class EmailSupportController
{

	@RequestMapping("/emailSupport")
	public void emailSupport(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String message)
	{

		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		String accountAccessHelper = request.getParameter("accountAccessHelper");
		Boolean authedAccess = false;

		UserAccountDataAccess ada = new UserAccountDataAccess();

		Hashtable<String, String> user = ada.getUserWithId(userId);

		if (accountAccessHelper != null && accountAccessHelper.equalsIgnoreCase("on"))
		{
			authedAccess = true;
			try (TransactionManager transactionManager = TransactionManager.getInstance())
			{
				TransactionModuleAccess tma = TransactionModuleAccess.getInstance(transactionManager);
				ElementData ele = tma.getElementById(UserAccountDataAccess.USER_MODULE, userId);
				CategoryData team = tma.getCategoryById(UserAccountDataAccess.USER_MODULE, ele.getParentId());
				CategoryData company = tma.getCategoryById(UserAccountDataAccess.USER_MODULE, team.getParentId());
				Calendar c = Calendar.getInstance();
				c.setTime(new Date());
				c.add(Calendar.HOUR, (24*7));

				ele.put(UserAccountDataAccess.C1_SUPPORT_TIME, c.getTime());
				team.put(UserAccountDataAccess.C_SUPPORT_AUTH_TIME, c.getTime());
				company.put(UserAccountDataAccess.C_SUPPORT_AUTH_TIME, c.getTime());
				tma.updateElement(ele, UserAccountDataAccess.USER_MODULE);
				tma.updateCategory(team, UserAccountDataAccess.USER_MODULE);
				tma.updateCategory(company, UserAccountDataAccess.USER_MODULE);
				transactionManager.commit();
			}
			catch (Exception e)
			{
				System.out.println("Approved support time could not be added ERROROROROROROROOR");
			}

		}

		if (message != null)
		{
			message = message.replaceAll("\r\n|\r|\n", "<br/>");
		}

		PendingEmail emailToNewOwner = new EmailBuilder(request)
				.addModule(UserAccountDataAccess.USER_MODULE, user)
				.prepareSupportEmail(message, authedAccess);

		emailToNewOwner.addTo("support@corporateinteractive.com.au").doSend();

	}

}
