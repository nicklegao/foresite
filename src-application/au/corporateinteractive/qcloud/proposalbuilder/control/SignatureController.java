//package au.corporateinteractive.qcloud.proposalbuilder.control;
//
//import java.io.IOException;
//import java.net.URLEncoder;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Hashtable;
//import java.util.List;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsHistoryDataAccess;
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import com.oreilly.servlet.MultipartRequest;
//
//import au.corporateinteractive.qcloud.proposalbuilder.Constants;
//import au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess;
//import au.corporateinteractive.qcloud.proposalbuilder.db.SignaturesDataAccess;
//import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
//import au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content;
//import au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal;
//import au.corporateinteractive.qcloud.proposalbuilder.json.proposal.SignatureSetting;
//import au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder;
//import au.corporateinteractive.qcloud.proposalbuilder.service.SecurityKeyService;
//import au.corporateinteractive.qcloud.proposalbuilder.utils.AjaxResponse;
//import au.corporateinteractive.qcloud.proposalbuilder.utils.ChecksumUtils;
//import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
//import au.net.webdirector.common.datalayer.client.CategoryData;
//import au.net.webdirector.common.datalayer.client.ElementData;
//import io.jsonwebtoken.Claims;
//import io.jsonwebtoken.ExpiredJwtException;
//import io.jsonwebtoken.Jws;
//import io.jsonwebtoken.Jwts;
//
//@Controller
//public class SignatureController
//{
//	private static Logger logger = Logger.getLogger(SignatureController.class);
//
//	@RequestMapping("/secure/sign/send")
//	@ResponseBody
//
//	public AjaxResponse sendSignature(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException
//	{
//		MultipartRequest r = new MultipartRequest(request, System.getProperty("java.io.tmpdir"), Constants.MAX_ATTACHMENT_SIZE);
//
//
//		String jsonToken = r.getParameter("token").trim();
//		String type = r.getParameter("type");
//		String firstname = r.getParameter("firstname");
//		String lastname = r.getParameter("lastname");
//		String proposal = (String) r.getParameter("proposal");
//		String uuid = (String) r.getParameter("uuid");
//		String email = (String) r.getParameter("email");
////		String vKey = (String) claims.getBody().get("v-key");
//
//		String esignKey = (String) session.getAttribute("esignKey");
//		boolean validSignatory = false;
//		boolean anyReceipientSection = false;
//
//		TravelDocsDataAccess pda = new TravelDocsDataAccess();
//		UserAccountDataAccess uada = new UserAccountDataAccess();
//		Proposal p = pda.loadTravelDoc(proposal);
//		Hashtable<String, String> pInfo = pda.getTravelDocById(proposal);
//		Hashtable<String, String> uInfo = uada.getUserWithId(pInfo.get("ATTRDROP_ownerId"));
//		Content conInfo = p.findGeneratedContentOfType("con-info");
//		String contactEmail = conInfo.getMetadata().get("contactEmail");
//		String ccEmails = conInfo.getMetadata().get("cc-emails");
//
//
//		List<String> contactEmails = new ArrayList<>();
//		contactEmails.add(contactEmail);
//		if (StringUtils.isNotBlank(ccEmails)) {
//			contactEmails.addAll(Arrays.asList(StringUtils.split("," + ccEmails)));
//		}
//
//		int signatureCount = 0;
//
//		List<List<SignatureSetting>> signSettings = p.getSignSettings();
//
//		for (List<SignatureSetting> signSection : signSettings)
//		{
//			for (SignatureSetting settings : signSection)
//			{
//				signatureCount++;
//				if (settings.getUuid().equals(uuid) && (StringUtils.isBlank(settings.getEmail()) || settings.getEmail().equals(email)))
//				{
//					validSignatory = true;
//					if (StringUtils.isBlank(settings.getEmail()))
//					{
//						anyReceipientSection = true;
//					}
//					break;
//				}
//			}
//		}
//
//		if (StringUtils.isNotBlank("esignKey") && jsonToken.equals(esignKey) && validSignatory)
//		{
//			try
//			{
//				Hashtable<String, String> proposalHT = pda.getTravelDocById(proposal);
//				StoreFile jsonFile = new StoreFile(proposalHT.get(TravelDocsDataAccess.E_RAW_DATA));
//				String currentChecksum = ChecksumUtils.getFileChecksum(jsonFile.getStoredFile());
//
//				SignaturesDataAccess sda = new SignaturesDataAccess();
//				ElementData lastSign = sda.getLastSignature(proposal);
//				ElementData sign = sda.getSignature(proposal, uuid);
//
//				int signatureId = -1;
//
//				if (sign != null)
//				{
//					if (!anyReceipientSection)
//					{
//						return new AjaxResponse(false, "You have signed off this proposal already!");
//					}
//					else
//					{
//						return new AjaxResponse(false, "You or someone else has signed off on this section already!");
//					}
//				}
//				if (lastSign == null)
//				{
//					if (!proposalHT.get(TravelDocsDataAccess.E_CHECKSUM_JSON).equals(currentChecksum))
//					{
//						return new AjaxResponse(false, "We have detected a change in the proposal since it was issued. Please get the consultant to revise and reissue this proposal.");
//					}
//					else
//					{
//						Proposal proposalObject = pda.loadTravelDoc(proposal);
//						List<Content> userContent = proposalObject.findUserContentOfType("editor");
//						for (Content content : userContent)
//						{
//							if (content != null && content.getMetadata() != null && content.getMetadata().containsKey("savedstatesrc"))
//							{
//								StoreFile savedStateSrc = new StoreFile(content.getMetadata().get("savedstatesrc"));
//								String savedChecksum = content.getMetadata().get("savedstatechecksum");
//								if (!ChecksumUtils.getFileChecksum(savedStateSrc.getStoredFile()).equals(savedChecksum))
//								{
//
//									return new AjaxResponse(false, "We have detected a change in the proposal since it was issued. Please get the consultant to revise and reissue this proposal.");
//								}
//							}
//						}
//					}
//				}
//
//				if (StringUtils.equalsIgnoreCase(type, "TYPESIGN"))
//				{
//					signatureId = sda.createTypeSignature(proposal, uuid, firstname, lastname, getClientIp(request), request.getHeader("User-Agent"), r.getParameter("content"), r.getParameter("font"));
//				}
//				else if (StringUtils.equalsIgnoreCase(type, "DRAWSIGN"))
//				{
//					signatureId = sda.createDrawSignature(proposal, uuid, firstname, lastname, getClientIp(request), request.getHeader("User-Agent"), r.getParameter("imageData"));
//				}
//				else if (StringUtils.equalsIgnoreCase(type, "UPLOADSIGN"))
//				{
//					signatureId = sda.createUploadSignature(proposal, uuid, firstname, lastname, getClientIp(request), request.getHeader("User-Agent"), r.getFile("file"));
//				}
//				else
//				{
//					throw new UnsupportedOperationException(type);
//				}
//
//				TravelDocsHistoryDataAccess phda = new TravelDocsHistoryDataAccess();
//				phda.recordProposalSignature(proposal, firstname, lastname);
//
//				if (signatureId > 0)
//				{
//					String filePath = pda.generateDocument(request, proposal);
//					Hashtable<String, String> newProposalHT = pda.getTravelDocById(proposal);
//					StoreFile newPdf = new StoreFile(newProposalHT.get(TravelDocsDataAccess.E_FILE_CACHE));
//					String newChecksum = "";
//					newChecksum = ChecksumUtils.getFileChecksum(newPdf.getStoredFile());
//					sda.updateSignatureWithChecksum(String.valueOf(signatureId), newChecksum);
//					List<ElementData> signaturesList = sda.getSignaturesForProposal(proposal);
//					boolean fullySigned = signaturesList.size() == signatureCount;
////					pda.markTravelDocAsSigned(request, proposal, fullySigned);
//					session.removeAttribute("esignKey");
//					for (String toEmail : contactEmails)
//					{
//						boolean emailSent = new EmailBuilder().addModule(TravelDocsDataAccess.MODULE, proposalHT).prepareProposalSignedEmail(email, uuid, null).addTo(toEmail).doSend();
//					}
//					if (uInfo.get("ATTR_Headline") != null) {
//						boolean emailSent = new EmailBuilder().addModule(TravelDocsDataAccess.MODULE, proposalHT).prepareProposalSignedEmail(email, uuid, uInfo.get("ATTR_Headline")).addTo(uInfo.get("ATTR_Headline")).doSend();
//					}
//				}
//				return new AjaxResponse(true, "The proposal has been successfully signed.");
//			}
//			catch (Exception e)
//			{
//				e.printStackTrace();
//			}
//		}
//		else
//		{
//			return new AjaxResponse(false, "You do not have permission to sign this section.");
//		}
//		return new AjaxResponse(false, "This proposal could not be signed.");
//	}
//
//	@RequestMapping("/free/sign/verify")
//	@ResponseBody
//	public AjaxResponse verifySignatory(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException
//	{
//		String proposal = request.getParameter("proposal");
//		String email = request.getParameter("email");
//
//		UserAccountDataAccess uada = new UserAccountDataAccess();
//		TravelDocsDataAccess pda = new TravelDocsDataAccess();
//		Hashtable<String, String> proposalHT = pda.getTravelDocById(proposal);
//		Proposal p = pda.loadTravelDoc(proposal);
//		Hashtable<String, String> user = uada.getUserWithId(proposalHT.get("ATTRDROP_ownerId"));
//		CategoryData company = uada.getUserCompanyForUserId(proposalHT.get("ATTRDROP_ownerId"));
//
//		String randomKey = new SecurityKeyService().generateRandomKey(12, 6, "-");
//		//String keyFront = randomKey.substring(0, randomKey.indexOf("-") + 1);
//
//		session.setAttribute("esignKey", randomKey);
//
//		try
//		{
//			Content conInfo = p.findGeneratedContentOfType("con-info");
//			String contactEmail = conInfo.getMetadata().get("contactEmail");
//			String ccEmail = conInfo.getMetadata().get("cc-emails");
//			if (contactEmail.equalsIgnoreCase(email) || ccEmail.toLowerCase().contains(email.toLowerCase()))
//			{
//
//				boolean emailSent = new EmailBuilder().addModule(TravelDocsDataAccess.MODULE, proposalHT).prepareSignatoryVerificationEmail(randomKey, company, user).addTo(email).doSend();
//				if (emailSent)
//				{
//					return new AjaxResponse(true, "The signatory verification has been sent to your e-mail.");
//				}
//			}
//		}
//		catch (Exception e)
//
//		{
//			e.printStackTrace();
//		}
//		return new AjaxResponse(false, "The signatory verification e-mail could not be sent. Please try again later.");
//	}
//
//	@RequestMapping("/free/sign")
//	@ResponseBody
//	public void setSigningToken(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException
//	{
//		String token = request.getParameter("token");
//		try
//		{
//
//			Jws<Claims> claims = Jwts.parser().setSigningKey(Constants.TOKEN_KEY).parseClaimsJws(token);
//
//			String tokenEmail = (String) claims.getBody().get("email");
//			String vKey = (String) claims.getBody().get("v-key");
//			String proposalId = (String) claims.getBody().get("proposal");
//			String uuid = (String) claims.getBody().get("uuid");
//
//			TravelDocsDataAccess pda = new TravelDocsDataAccess();
//			SignaturesDataAccess sda = new SignaturesDataAccess();
//
//			Proposal proposal = pda.loadTravelDoc(proposalId);
//
//			Content conInfo = proposal.findGeneratedContentOfType("con-info");
//			String contactEmail = conInfo.getMetadata().get("contactEmail");
//			String ccEmail = conInfo.getMetadata().get("cc-emails");
//			ElementData sign = sda.getSignature(proposalId, uuid);
//			if (contactEmail.equalsIgnoreCase(tokenEmail) || ccEmail.toLowerCase().contains(tokenEmail.toLowerCase()))
//			{
//				if (sign == null)
//				{
//					session.setAttribute("signingToken", token);
//				}
//			}
//
//			String urlEncodedEmail = URLEncoder.encode(tokenEmail, "UTF-8");
//			String url = String.format("email=%s&proposal=%s&v-key=%s",
//					urlEncodedEmail,
//					proposalId,
//					vKey);
//			response.sendRedirect("/view?" + url);
//		}
//		catch (ExpiredJwtException e)
//		{
//			try
//			{
//				Claims claims = e.getClaims();
//				String email = (String) claims.get("email");
//				String vKey = (String) claims.get("vKey");
//				String proposalId = (String) claims.get("proposal");
//
//				String urlEncodedEmail = URLEncoder.encode(email, "UTF-8");
//				String url = String.format("email=%s&proposal=%s&v-key=%s",
//						urlEncodedEmail,
//						proposalId,
//						vKey);
//				response.sendRedirect("/view?" + url);
//			}
//			catch (Exception ex)
//			{
//				ex.printStackTrace();
//			}
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}
//	}
//
//	private static String getClientIp(HttpServletRequest request)
//	{
//
//		String remoteAddr = "";
//
//		if (request != null)
//		{
//			remoteAddr = request.getHeader("X-FORWARDED-FOR");
//			if (remoteAddr == null || "".equals(remoteAddr))
//			{
//				remoteAddr = request.getRemoteAddr();
//			}
//		}
//
//		return remoteAddr;
//	}
//}
