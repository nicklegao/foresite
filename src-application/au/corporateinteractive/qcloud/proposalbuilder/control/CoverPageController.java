package au.corporateinteractive.qcloud.proposalbuilder.control;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreilly.servlet.MultipartRequest;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.CoverPageDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.model.CoverPage;
import au.corporateinteractive.qcloud.proposalbuilder.model.CoverPageResponse;
import au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions;
import au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService;
import au.net.webdirector.common.datalayer.base.db.entity.MultiOptions;
import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;

@Controller
public class CoverPageController extends AbstractController
{

	Logger logger = Logger.getLogger(CoverPageController.class);

	@RequestMapping("/loadCoverPages")
	@ResponseBody
	public CoverPageResponse loadCoverPages(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception
	{
		try (TransactionManager transactionManager = TransactionManager.getInstance())
		{
			TUserAccountDataAccess uada = new TUserAccountDataAccess(transactionManager);
			CoverPageDataAccess cpda = new CoverPageDataAccess();

			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			CategoryData companySettings = uada.getUserCompanyForUserId(userId);
			String companyID = companySettings.getId();
			
			List<CoverPage> pages = coverPageList(companySettings, userId, companyID);
			
			transactionManager.commit();
			return new CoverPageResponse(pages);
		}
		catch (Exception e)
		{
			logger.fatal("Oh No!", e);
			throw e;
		}
	}

	public List<CoverPage> coverPageList(CategoryData companySettings, String userId, String companyID) throws Exception {
		List<CategoryData> teams = new UserAccountDataAccess().usersTeams(userId, companyID);

		List<LinkedHashMap<String, String>> coverPages = new ImageLibraryService().getCoverpageThumbs(companySettings.getId(), true);
		List<CoverPage> pages = new LinkedList<CoverPage>();
		List<CoverPage> teamPages = new LinkedList<CoverPage>();
		Boolean isCoverpageTeamActive = false;
		for (LinkedHashMap<String, String> coverData : coverPages) {
			CoverPage cover = new CoverPage();
			cover.setId(coverData.get("id"));
			cover.setThumbnail(coverData.get("file"));
			cover.setName(coverData.get("name"));
			pages.add(cover);
		}

		for (CategoryData team : teams) {
			if (!isCoverpageTeamActive)
			{
				isCoverpageTeamActive = team.getBoolean(TUserAccountDataAccess.E_TEAM_COVERPAGE_ACTIVE);
			}
			MultiOptions activeLibrary = ModuleAccess.getInstance().getMultiOptions(team, TUserAccountDataAccess.E_TEAM_COVERPAGE_FOLDERS,"users");
			for (CoverPage item : pages) {
				if (StringUtils.isNotBlank(team.getId())) {
					for (String active : activeLibrary.getValues()) {
						System.out.println(active);
						System.out.println(item.getId());
						if (active.equals(item.getId())) {
							teamPages.add(item);
						}
					}
				}
			}
		}
		return (isCoverpageTeamActive ? (teamPages.size() < 1 ? pages : teamPages) : pages);
	}

	public List<CoverPage> allCoverPageList(CategoryData companySettings, String userId, String companyID) throws Exception {
		List<CategoryData> teams = new UserAccountDataAccess().usersTeams(userId, companyID);
		List<LinkedHashMap<String, String>> coverPages = new ImageLibraryService().getCoverpageThumbs(companySettings.getId(), true);
		List<CoverPage> pages = new LinkedList<CoverPage>();
		List<CoverPage> teamPages = new LinkedList<CoverPage>();
		for (LinkedHashMap<String, String> coverData : coverPages) {
			CoverPage cover = new CoverPage();
			cover.setId(coverData.get("id"));
			cover.setThumbnail(coverData.get("file"));
			cover.setName(coverData.get("name"));
			pages.add(cover);
		}
		return pages;
	}

	@RequestMapping("/deleteCoverPage")
	public void deleteCoverPage(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String coverpageId)
	{
		try (TransactionManager transactionManager = TransactionManager.getInstance())
		{
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

			TUserRolesDataAccess urda = new TUserRolesDataAccess(transactionManager);
			UserPermissions permissions = urda.getPermissions(userId);
//			if (!permissions.hasManageCoversPermission())
//			{
//				//TODO: should return message to show in the browser (BaseResponse)
//				logger.error("User " + userId + " tried to manage covers but doesn't have permission for it.");
//				transactionManager.rollback();
//				return;
//			}
			CoverPageDataAccess cpda = new CoverPageDataAccess();
			cpda.removeCoverPage(coverpageId);
			transactionManager.commit();
		}
		catch (Exception e)
		{
			logger.fatal("Error deleting cover page!", e);
		}
	}

	@RequestMapping("/uploadCoverPage")
	@ResponseBody
	public boolean uploadCoverPage(HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{
		try (TransactionManager transactionManager = TransactionManager.getInstance())
		{
			TUserAccountDataAccess ada = new TUserAccountDataAccess(transactionManager);
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

			TUserRolesDataAccess urda = new TUserRolesDataAccess(transactionManager);
			UserPermissions permissions = urda.getPermissions(userId);
//			if (!permissions.hasManageCoversPermission())
//			{
//				//TODO: should return message to show in the browser (BaseResponse)
//				logger.error("User " + userId + " tried to manage covers but doesn't have permission for it.");
//				transactionManager.rollback();
//				return false;
//			}
			ElementData newElement = new ElementData();

			CoverPageDataAccess cpda = new CoverPageDataAccess();
			CategoryData company = ada.getUserCompanyForUserId(userId);
			MultipartRequest r = new MultipartRequest(request, System.getProperty("java.io.tmpdir"), Constants.MAX_ATTACHMENT_SIZE);
			File cover = r.getFile("cover");

			int offsetX = (int) Double.parseDouble(r.getParameter("x"));
			int offsetY = (int) Double.parseDouble(r.getParameter("y"));
			int width = (int) Double.parseDouble(r.getParameter("w"));
			int height = (int) Double.parseDouble(r.getParameter("h"));

			File croppedFile = cpda.cropFileForCoverPage(cover, offsetX, offsetY, width, height);

			TransactionModuleAccess tma = TransactionModuleAccess.getInstance(transactionManager);
			CategoryData conditions = new CategoryData();
			conditions.put("ATTRDROP_companyID", company.getId());
			String categoryId = tma.getCategory(cpda.COVER_PAGE_MODULE, conditions).getId();
			newElement.setName(cover.getName());
			newElement.setLive(true);
			newElement.setParentId(categoryId);
			newElement.put(cpda.E_COVER_PAGE, new StoreFile(croppedFile, CoverPageDataAccess.COVER_PAGE_MODULE, CoverPageDataAccess.E_COVER_PAGE, false));
			newElement = tma.insertElement(newElement, cpda.COVER_PAGE_MODULE);
			StoreFile file = (StoreFile) newElement.get(cpda.E_COVER_PAGE);
			transactionManager.commit();
			cpda.generateMediumThumb(file);
			return true;
		}
		catch (Exception e)
		{
			logger.fatal("Error uploading cover page:", e);
			return false;
		}
	}
}
