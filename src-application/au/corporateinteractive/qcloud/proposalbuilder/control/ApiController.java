/**
 * @author Jaysun Lee
 * @date 21 Sep 2015
 */
package au.corporateinteractive.qcloud.proposalbuilder.control;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class ApiController extends AbstractController
{
	private static Logger logger = Logger.getLogger(ApiController.class);


	@ExceptionHandler(Exception.class)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public Map<String, Object> handleException(Exception e)
	{
		Map<String, Object> errorObject = new HashMap<String, Object>();
		errorObject.put("success", false);
		errorObject.put("error", e.getMessage());
		return errorObject;
	}
}
