/**
 * @author Nick Yiming Gao
 * @date 20/03/2017 10:16:17 am
 */
package au.corporateinteractive.qcloud.proposalbuilder.control;

/**
 * 
 */

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.stream.JsonReader;
import com.stripe.model.Event;
import com.stripe.model.Invoice;
import com.stripe.model.Subscription;
import com.stripe.net.APIResource;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.InvoiceDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.model.bill.TDInvoice;
import au.corporateinteractive.qcloud.proposalbuilder.service.StripeService;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;

@Controller
public class StripeController
{

	private static Logger logger = Logger.getLogger(StripeController.class);

	@RequestMapping("/stripe/need-setup-payment")
	@ResponseBody
	public static boolean needSetupPayment(HttpSession session)
	{
		UserAccountDataAccess uada = new UserAccountDataAccess();
		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		CategoryData company = uada.getUserCompanyForUserId(userId);
		return StripeService.getInstance().needsSetupPayment(company.getId(), false);
	}

	@RequestMapping("/free/stripe/webhook")
	@ResponseBody
	public String webhook(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		StripeService service = StripeService.getInstance();
		JsonReader jReader = new JsonReader(request.getReader());
		System.out.println(request.getReader().toString());
		Event eventJson = APIResource.GSON.fromJson(jReader, Event.class);
		try
		{
			Event event = Event.retrieve(eventJson.getId());
			if (StringUtils.startsWith(event.getType(), "invoice."))
			{
				Invoice invoice = (Invoice) event.getData().getObject();
				ElementData invoiceCI = service.saveInvoice(invoice);
				if (invoiceCI == null)
				{
					throw new RuntimeException("No invoice is saved.");
				}
				service.generateInvoicePdf(invoiceCI, request);
				if (StringUtils.equals(event.getType(), "invoice.payment_failed"))
				{
					service.processPaymentFailedEvent(invoice);
				}
				else if (StringUtils.equals(event.getType(), "invoice.payment_succeeded"))
				{
					service.processPaymentSucceededEvent(invoice, invoiceCI, request);
				}
				else if (StringUtils.equals(event.getType(), "invoice.sent"))
				{
					// do nothing for now
				}
				else if (StringUtils.equals(event.getType(), "invoice.created"))
				{
					// do nothing as invoice is already saved.
//					service.processInvoiceCreated(invoice, invoiceCI, request);
				}
				else
				{
					throw new RuntimeException("Unsupported event type:" + event.getType());
				}
			}
			else if (StringUtils.startsWith(event.getType(), "customer."))
			{
				Subscription subscription = (Subscription) event.getData().getObject();

				if (StringUtils.equals(event.getType(), "customer.subscription.deleted"))
				{
//					service.processSubscriptionDeletedEvent(subscription);
				}
				else if (StringUtils.equals(event.getType(), "customer.subscription.trial_will_end"))
				{
//					service.processTrialWillEndEvent(subscription, request);
				}
				else if (StringUtils.equals(event.getType(), "customer.subscription.updated"))
				{
//					service.processSubscriptionUpdated(subscription, request);
				}
				else
				{
					throw new RuntimeException("Unsupported event type:" + event.getType());
				}
			}
			else
			{
				throw new RuntimeException("Unsupported event type");
			}
			// Do something with eventJson
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			logger.warn("Error in save invoice:" + APIResource.PRETTY_PRINT_GSON.toJson(eventJson));
		}
		response.setStatus(200);
		return APIResource.PRETTY_PRINT_GSON.toJson(eventJson);
	}

	@RequestMapping("/free/payment/invoice")
	public String paymentInvoice(HttpServletRequest request, HttpServletResponse response, HttpSession session, @RequestParam String invId) throws IOException, SQLException
	{
		ModuleAccess ma = ModuleAccess.getInstance();
		ElementData invoiceCI = ma.getElementById(InvoiceDataAccess.MODULE, invId);
		TDInvoice invoice = APIResource.GSON.fromJson(invoiceCI.getString(InvoiceDataAccess.E_INVOICE), TDInvoice.class);
		CategoryData invoiceCategory = ma.getCategoryById(InvoiceDataAccess.MODULE, invoiceCI.getParentId());

		CategoryData company = ma.getCategoryById(TUserAccountDataAccess.USER_MODULE, invoiceCategory.getString(InvoiceDataAccess.C_COMPANY_ID));
		request.setAttribute("invoice", invoice);
		request.setAttribute("invoiceCI", invoiceCI);
		request.setAttribute("company", company);
		QueryBuffer sql = new QueryBuffer();
		sql.append("select e.* from elements_agents e, categories_agents c where c.category_id = e.category_id and c.attrdrop_companyid = ? ", company.getId());
		List<ElementData> agents = ma.getElements(sql.getSQL(), sql.getParams());
		List<String> customers = new ArrayList<String>();
		request.setAttribute("customers", customers);
		return "invoice/invoice";
	}

	
}
