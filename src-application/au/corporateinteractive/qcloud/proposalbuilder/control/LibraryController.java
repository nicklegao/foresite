package au.corporateinteractive.qcloud.proposalbuilder.control;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.oreilly.servlet.MultipartRequest;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.CoverPageDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.ProjectsDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions;
import au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService;
import au.corporateinteractive.qcloud.proposalbuilder.service.PDFLibraryService;
import au.corporateinteractive.qcloud.proposalbuilder.utils.AjaxResponse;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.MultiOptions;
import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
import au.net.webdirector.common.datalayer.base.db.entity.TextFile;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.ModuleHelper;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import sbe.ShortUrls.ShortUrlManager;

@Controller
public class LibraryController extends AbstractController {
	private static final String SUB_FOLDER_PLACEHODER = "Sub folder placeholder [Do not touch]";
	private static final String DELETION_DENIED = "DELETION DENIED";
	private static final String TEMPLATE_USED = "TEMPLATE_USED";

	Logger logger = Logger.getLogger(LibraryController.class);

	@RequestMapping("/library/contentmanagement")
	@ResponseBody
	public Map<String, Object> libraryContentManagement(HttpSession session, HttpServletRequest request,
			@RequestParam String libraryType) throws SQLException {
		libraryType = libraryType.toLowerCase();
		if ("project".equals(libraryType)) {
			return projectsLibraryGet(request, session);
		}
		return null;
	}

	private Map<String, Object> projectsLibraryGet(HttpServletRequest request, HttpSession session)
			throws SQLException {
		String libraryType = "project";
		String module = getModuleName(libraryType);

		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		if (!userCanManageLibrary(userId, libraryType)) {
			throw new RuntimeException("Not allowed to manage the permissions");
		}
		String companyId = new UserAccountDataAccess()
				.getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR)).getId();
		ModuleAccess ma = ModuleAccess.getInstance();

		Map<String, Object> contentMap = new HashMap<>();
		List<ElementData> files = new ArrayList<>();
		List<CategoryData> folders = new ArrayList<CategoryData>();
		contentMap.put("libraryItems", files);
		contentMap.put("libraryFolders", folders);

		QueryBuffer qb = new QueryBuffer();
		qb.append("SELECT e.* FROM elements_" + module + " e ");
		qb.append("join categories_" + module + " c on e.Category_id = c.Category_id ");
		qb.append("where c.ATTRDROP_CompanyID = ? ", companyId);
		qb.append(" order by e.element_id desc ");
		List<ElementData> libraryItems = ma.getElements(qb.getSQL(), qb.getParams());
		files.addAll(libraryItems);
		return contentMap;
	}

	private boolean userCanManageLibrary(String userId, String libraryType) {

		try (TransactionManager transactionManager = TransactionManager.getInstance()) {
			TUserRolesDataAccess urda = new TUserRolesDataAccess(transactionManager);
			UserPermissions up = urda.getPermissions(userId);
			transactionManager.rollback();

			return up.hasManageProjectsPermission();
		} catch (Exception e) {
			logger.fatal("Oh No!", e);
		}
		return false;
	}

	public List<CategoryData> getTeamFolderList(HttpSession session, String libraryType)
			throws IOException, SQLException {
		String companyId = new UserAccountDataAccess()
				.getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR)).getId();
		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		List<CategoryData> teams = new UserAccountDataAccess().usersTeams(userId, companyId);
		String librarySwitchDBRef = TUserAccountDataAccess.librarySwitchDBRef(libraryType);
		String libraryDBRef = TUserAccountDataAccess.libraryDBRef(libraryType);
		ModuleAccess ma = ModuleAccess.getInstance();

		String sql = "SELECT c2.* FROM categories_" + getModuleName(libraryType) + " c2 " + "join categories_"
				+ getModuleName(libraryType) + " c1 on c2.Category_ParentID = c1.Category_id "
				+ "where c1.ATTRDROP_CompanyID = ? ";

		if (!getModuleName(libraryType).equalsIgnoreCase(ImageLibraryService.IMAGES_MODULE)) {
			sql += " order by c2.attr_categoryname ";
		} else {
			sql += " order by c2.category_id desc ";

		}

		List<CategoryData> libraryFolders = ma.getCategories(sql, new String[] { companyId });
		List<CategoryData> teamlibraryFolders = new ArrayList<CategoryData>();
		boolean libraryActive = teams.get(0).getBoolean(librarySwitchDBRef);
		if (libraryActive == true) {
			MultiOptions activeLibrary = ModuleAccess.getInstance().getMultiOptions(teams.get(0), libraryDBRef,
					"users");
			for (CategoryData folder : libraryFolders) {
				if (StringUtils.isNotBlank(teams.get(0).getId())) {
					for (String active : activeLibrary.getValues()) {
						if (active.equals(folder.getId())) {
							teamlibraryFolders.add(folder);
						}
					}
				}
			}
		}

		if (libraryActive == true) {
			return teamlibraryFolders;
		}
		return libraryFolders;

	}

	public String getModuleName(String type) {
		switch (type.toLowerCase()) {
		case "project":
			return ProjectsDataAccess.MODULE;
		default:
			logger.fatal("UNABLE TO DETERMINE TYPE:... " + type);
			throw new InvalidParameterException("TYPE:" + type);
		}
	}

	@RequestMapping(value = "/library/add-edit/{libraryType}/{elementId}", method = RequestMethod.GET)
	@ResponseBody
	public ElementData libraryAddEdit(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@PathVariable String libraryType, @PathVariable String elementId) throws IOException, SQLException {

		String module = getModuleName(libraryType);
		if (!checkContentAccess(elementId, null, module, session)) {
			response.setStatus(401);
			return null;
		}

		TransactionDataAccess da = TransactionDataAccess.getInstance();
		TransactionManager tm = TransactionManager.getInstance(da);
		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);

		ElementData conditions = new ElementData();
		conditions.setId(elementId);
		ElementData item = ma.getElement(module, conditions);

		// ######### IMAGE ATTRIBUTES ##########
		DecimalFormat decim = new DecimalFormat("0.00");

		StoreFile imageStoreFile = item.getStoreFile("attrfile_image");

		if (null != imageStoreFile) {
			File image = imageStoreFile.getStoredFile();
			BufferedImage bimg = ImageIO.read(image);
			int width = bimg.getWidth();
			int height = bimg.getHeight();
			double bytes = image.length();
			double kilobytes = (bytes / 1024);

			item.put("image_filename", image.getName());
			item.put("image_uploaded", item.getCreateDate().toString());
			item.put("image_type", URLConnection.guessContentTypeFromName(image.getName()));
			item.put("image_width", width);
			item.put("image_height", height);
			item.put("image_size", Double.parseDouble(decim.format(kilobytes)) + " KB");
		}
		// #####################################

		if (libraryType.equalsIgnoreCase("text")) {
			TextFile textBlock = item.getTextFile("attrlong_textblock");
			item.put("attrlong_textblock", textBlock.getContent());
		} else if (libraryType.equalsIgnoreCase("images")) {
			TextFile textBlock = item.getTextFile("attrlong_description");
			if (textBlock != null) {
				item.put("attrlong_description", textBlock.getContent());
			}
		} else if (libraryType.equalsIgnoreCase("spreadsheet")) {
			TextFile textBlock = item.getTextFile("attrtext_json");
			if (textBlock != null) {
				item.put("attrtext_json", new Gson().fromJson(textBlock.getContent(), Map.class));
			} else {
				item.put("attrtext_json", null);
			}
		}
		return item;
	}

	@RequestMapping(value = "/library/add-edit/{libraryType}", method = RequestMethod.GET)
	@ResponseBody
	public List<CategoryData> libraryAddEditGet(HttpSession session, @PathVariable String libraryType)
			throws IOException, SQLException {
		return getTeamFolderList(session, libraryType);
	}

	@RequestMapping(value = "/library/createFolder/{libraryType}", method = RequestMethod.POST)
	@ResponseBody
	public boolean libraryCreateFolder(HttpSession session, HttpServletRequest request,
			@PathVariable String libraryType) throws IOException, SQLException {
		if ("template".equals(libraryType)) {
			return templateCreateFolder(session, request);
		}
		TransactionManager tm = TransactionManager.getInstance(TransactionDataAccess.getInstance(),
				StoreFileAccessFactory.getInstance());
		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);

		String companyId = new UserAccountDataAccess()
				.getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR)).getId();
		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

		String module = getModuleName(libraryType);

		List<CategoryData> teams = new UserAccountDataAccess().usersTeams(userId, companyId);
		CategoryData conditions = new CategoryData();
		conditions.put("attrdrop_companyId", companyId);
		TUserAccountDataAccess ada = new TUserAccountDataAccess(tm);
		CategoryData companyCategory = ma.getCategory(module, conditions);

		String levels = request.getParameter("levels");
		String folderName = request.getParameter("folderName");
		String parentId = request.getParameter("parentId");
		String folderId = request.getParameter("folderId");

		CategoryData category = new CategoryData();
		ElementData subFolder = new ElementData();

		if (levels.equalsIgnoreCase("1")) {
			category.setParentId(companyCategory.getId());
			category.setName(folderName);
			category.setLive(true);
			category.setFolderLevel(2);
			category = ma.insertCategory(category, module);
			ada.addFolderToTeam(teams.get(0), libraryType, category.getId(), getTeamFolderList(session, libraryType));
		} else if (levels.equalsIgnoreCase("2") && checkContentAccess("", parentId, module, session)) {
			subFolder.setName(SUB_FOLDER_PLACEHODER);
			subFolder.put("ATTR_subfolder", folderName);
			subFolder.setLive(false);
			subFolder.setParentId(parentId);
			subFolder = ma.insertElement(subFolder, module);
		}

		return category != null || subFolder != null;
	}

	private boolean templateCreateFolder(HttpSession session, HttpServletRequest request)
			throws IOException, SQLException {
		String libraryType = "template";
		TransactionManager tm = TransactionManager.getInstance(TransactionDataAccess.getInstance(),
				StoreFileAccessFactory.getInstance());
		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);

		String companyId = new UserAccountDataAccess()
				.getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR)).getId();
		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

		String module = "traveldocs";

		List<CategoryData> teams = new UserAccountDataAccess().usersTeams(userId, companyId);
		TUserAccountDataAccess ada = new TUserAccountDataAccess(tm);
		CategoryData companyCategory = ma.getCategories(
				"select c1.* from categories_traveldocs c1, categories_traveldocs c0 where c0.category_id = c1.category_parentid and c0.attrdrop_companyid = ? and c1.attr_categoryname = 'Templates'",
				companyId).get(0);

		String levels = request.getParameter("levels");
		String folderName = request.getParameter("folderName");
		String parentId = request.getParameter("parentId");
		String folderId = request.getParameter("folderId");

		CategoryData category = new CategoryData();
		ElementData subFolder = new ElementData();

		if (levels.equalsIgnoreCase("1")) {
			category.setParentId(companyCategory.getId());
			category.setName(folderName);
			category.setLive(true);
			category.setFolderLevel(3);
			category = ma.insertCategory(category, module);
			ada.addFolderToTeam(teams.get(0), libraryType, category.getId(), getTeamFolderList(session, libraryType));
		} else if (levels.equalsIgnoreCase("2") && checkContentAccess("", parentId, module, session)) {
			subFolder.setName(SUB_FOLDER_PLACEHODER);
			subFolder.put("ATTR_subcategory", folderName);
			subFolder.setLive(false);
			subFolder.setParentId(parentId);
			subFolder = ma.insertElement(subFolder, module);
		}
		return category != null;
	}

	@RequestMapping(value = "/library/updateFolder/{libraryType}", method = RequestMethod.POST)
	@ResponseBody
	public boolean libraryUpdateFolder(HttpSession session, HttpServletRequest request,
			@PathVariable String libraryType) throws IOException, SQLException {
		// TODO: Subfolder implementation
		TransactionManager tm = TransactionManager.getInstance(TransactionDataAccess.getInstance(),
				StoreFileAccessFactory.getInstance());
		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);

		String companyId = new UserAccountDataAccess()
				.getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR)).getId();
		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

		String module = getModuleName(libraryType);

		String levels = request.getParameter("levels");
		String folderName = request.getParameter("folderName");
		String oldFolderName = request.getParameter("oldFolderName");
		String parentId = request.getParameter("parentId");
		String folderId = request.getParameter("folderId");

		CategoryData category = new CategoryData();
		int updated = 0;

		if (levels.equalsIgnoreCase("1") && StringUtils.isNotBlank(folderId)
				&& checkContentAccess("", folderId, module, session)) {
			category.setId(folderId);
			category.setName(folderName);
			updated = ma.updateCategory(category, module);
		} else if (levels.equalsIgnoreCase("2") && StringUtils.isNotBlank(parentId)
				&& checkContentAccess("", parentId, module, session)) {
			if (libraryType.equalsIgnoreCase("template")) {
				updated = db.updateData(
						"update elements_" + module
								+ " set ATTR_subCategory = ? where ATTR_subCategory = ? and category_id = ?",
						new String[] { folderName, oldFolderName, parentId });
			} else {
				updated = db.updateData(
						"update elements_" + module
								+ " set ATTR_subfolder = ? where ATTR_subfolder = ? and category_id = ?",
						new String[] { folderName, oldFolderName, parentId });
			}
		}

		return updated > 0;
	}

	@RequestMapping(value = "/library/deleteFolder/{libraryType}", method = RequestMethod.POST)
	@ResponseBody
	public boolean libraryDeleteFolder(HttpSession session, HttpServletRequest request, HttpServletResponse response,
			@PathVariable String libraryType, @RequestParam String folderId, @RequestParam String transferTo)
			throws IOException, SQLException {
		String module = getModuleName(libraryType);
		if (!checkContentAccess(null, folderId, module, session)
				|| !checkContentAccess(null, transferTo, module, session)) {
			response.setStatus(401);
			return false;
		}

		TransactionDataAccess da = TransactionDataAccess.getInstance();
		TransactionManager tm = TransactionManager.getInstance(da);
		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);

		ElementData conditions = new ElementData();
		conditions.setParentId(folderId);
		List<ElementData> items = ma.getElements(module, conditions);
		for (ElementData item : items) {
			item.setParentId(transferTo);
			ma.updateElement(item, getModuleName(libraryType));
		}

		CategoryData category = new CategoryData();
		category.setId(folderId);

		return 1 == ma.deleteCategories(getModuleName(libraryType), category);
	}

	@RequestMapping(value = "/library/add-edit/{libraryType}/subfolder/{categoryId}", method = RequestMethod.GET)
	@ResponseBody
	public List<String> libraryAddEditGetSubFolder(HttpSession session, @PathVariable String libraryType,
			@PathVariable String categoryId) throws IOException, SQLException {
		return getTeamSubFolderList(session, libraryType, categoryId);
	}

	public List<String> getTeamSubFolderList(HttpSession session, String libraryType, String categoryId)
			throws IOException, SQLException {
		DataAccess db = DataAccess.getInstance();

		String sqlSubFolder = "SELECT ATTR_subFolder FROM elements_" + getModuleName(libraryType)
				+ " WHERE Category_id = '" + categoryId + "' GROUP BY ATTR_subFolder";

		return db.select(sqlSubFolder, new StringMapper());
	}

	@RequestMapping("/library/delete")
	@ResponseBody
	public AjaxResponse libraryDelete(HttpSession session, HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map map) throws SQLException {
		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		String libraryType = ((String) map.get("libraryType")).toLowerCase();
		String transferto = (String) map.get("transferto");

		List<Map> elements = (List<Map>) map.get("elements");
		String module = getModuleName(libraryType);
		if (!userCanManageLibrary(userId, libraryType)) {
			return new AjaxResponse(false, "<p>You do not have the permissions to manage this library.</p>");
		}
		Set<String> result = new TreeSet<String>();
		try (TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false),
				StoreFileAccessFactory.getInstance(true))) {
			for (Map element : elements) {
				String r;
				String id = (String) element.get("elementId");
				if("project".equals(libraryType)) {
					r = deleteProject(id, userId, module, txManager);
				}else {
					r = deleteItem(id, userId, module, txManager);
				}
				result.add(r);
			}
			txManager.commit();
			return organise(result);
		} catch (RuntimeException e) {

			if (TEMPLATE_USED.equals(e.getMessage())) {
				return new AjaxResponse(false, "<p>Template is being used and cannot be deleted.</p>");
			}
			if (DELETION_DENIED.equals(e.getMessage())) {
				return new AjaxResponse(false, "<p>You do not have the permissions to delete this item.</p>");
			}
			logger.error("error:", e);
		} catch (Exception e) {
			logger.error("error:", e);
		}

		return new AjaxResponse(false, "<p>Something went wrong. Please try later.</p>");
	}

	private AjaxResponse organise(Set<String> result) {
		for (String s : result) {

		}
		return new AjaxResponse(true, "");
	}

	private String deleteSubFolder(String categoryId, String name, String module, TransactionManager txManager)
			throws SQLException {
		if (StringUtils.equalsIgnoreCase("traveldocs", module)) {
			return deleteTemplateSubFolder(categoryId, name, txManager);
		}
		txManager.getDataAccess()
				.update("delete from elements_" + module
						+ " where category_id = ? and attr_subfolder = ? and live = 0 and attr_headline = ? ",
						categoryId, name, SUB_FOLDER_PLACEHODER);
		txManager.getDataAccess()
				.update("update elements_" + module
						+ " set attr_subfolder = '' where category_id = ? and attr_subfolder = ? and live = 1",
						categoryId, name);
		return "deleted";
	}

	private String deleteTemplateSubFolder(String categoryId, String name, TransactionManager txManager)
			throws SQLException {
		txManager.getDataAccess().update(
				"delete from elements_traveldocs where category_id = ? and ATTR_subCategory = ? and live = 0 and attr_headline = ? ",
				categoryId, name, SUB_FOLDER_PLACEHODER);
		txManager.getDataAccess().update(
				"update elements_traveldocs set ATTR_subCategory = '' where category_id = ? and ATTR_subCategory = ? and live = 1",
				categoryId, name);
		return "deleted";
	}

	private String deleteFolder(String folderId, String transferto, String module, TransactionManager txManager)
			throws SQLException, IOException {
		if (StringUtils.isNotBlank(transferto)) {
			txManager.getDataAccess().update(
					"update elements_" + module + " set category_id = ? where category_id = ? ", transferto, folderId);
		}

		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
		CategoryData category = new CategoryData();
		category.setId(folderId);
		ma.deleteCategories(module, category);
		return "deleted";
	}

	private String deleteProject(String projectId, String userId, String module, TransactionManager txManager)
			throws SQLException, IOException {
		TransactionModuleAccess.getInstance(txManager).deleteElementById(module, projectId);
		TransactionDataAccess.getInstance(txManager).update("delete from elements_tasks where ATTR_ProjectID = ? ", projectId);
		return "deleted";
	}
	
	private String deleteItem(String elementId, String userId, String module, TransactionManager txManager)
			throws SQLException, IOException {
		TransactionModuleAccess.getInstance(txManager).deleteElementById(module, elementId);
		return "deleted";
	}

	@RequestMapping(value = "/library/add-edit/project", method = RequestMethod.POST)
	@ResponseBody
	public boolean projectsLibraryAddEditPost(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws IOException, SQLException {
		String libraryType = "project";
		String module = getModuleName(libraryType);

		MultipartRequest r = new MultipartRequest(request, System.getProperty("java.io.tmpdir"),
				Constants.MAX_ATTACHMENT_SIZE);
		String elementId = r.getParameter("elementId");

		if (!checkContentAccess(elementId, null, module, session)) {
			response.setStatus(401);
			return false;
		}
		String projectName = r.getParameter("projectName");
		CategoryData company = new UserAccountDataAccess()
				.getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR));

		ProjectsDataAccess pda = new ProjectsDataAccess();
		if (!StringUtils.isEmpty(elementId) && Integer.valueOf(elementId) > 0) {
			return pda.saveProject(elementId, projectName, company);
		}
		return pda.saveProject(projectName, company) != null;
	}

	private boolean checkContentAccess(String assetId, String folderId, String module, HttpSession session)
			throws AccessDeniedException {
		if (StringUtils.equalsIgnoreCase(module, "projects")) {
			return checkProjectAccess(assetId, folderId, session);
		}

		return false;
	}

	private boolean checkProjectAccess(String assetId, String folderId, HttpSession session)
			throws AccessDeniedException {
		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		String companyId = new UserAccountDataAccess().getUserCompanyForUserId(userId).getId();
		if (!StringUtils.isEmpty(assetId) && Integer.valueOf(assetId) > 0) {
			String query = "select c.ATTRDROP_companyID from categories_projects c, elements_projects e "
					+ "where c.Category_id=e.Category_id and e.Element_id=?";
			List<String> res = DataAccess.getInstance().select(query, new String[] { assetId }, new StringMapper());
			String ownerCompanyId = res.size() > 0 ? res.get(0) : null;
			if (ownerCompanyId == null || !StringUtils.equals(companyId, ownerCompanyId)) {
				logger.error("User " + userId + " tried to access traveldocs (" + folderId + ") and failed");
				return false;
			}
		}
		return true;
	}

	public List<CategoryData> getLibrary(String libraryType, String companyId) throws SQLException {

		ModuleAccess ma = ModuleAccess.getInstance();

		String sql = "SELECT c2.* FROM categories_" + getModuleName(libraryType) + " c2 " + "join categories_"
				+ getModuleName(libraryType) + " c1 on c2.Category_ParentID = c1.Category_id "
				+ "where c1.ATTRDROP_CompanyID = ? ";

		if (!getModuleName(libraryType).equalsIgnoreCase(ImageLibraryService.IMAGES_MODULE)) {
			sql += " order by c2.attr_categoryname ";
		} else {
			sql += " order by c2.category_id desc ";

		}

		return ma.getCategories(sql, new String[] { companyId });

	}

}
