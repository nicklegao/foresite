/**
 * 
 */
package au.corporateinteractive.qcloud.proposalbuilder.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;

/**
 * @author Jaysun Lee
 *
 */
@Controller
public class CompanyController
{

	Logger logger = Logger.getLogger(CompanyController.class);

	@ResponseBody
	@RequestMapping("/regenerateApiCredentials")
	public boolean regenerateApiCredentials(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception
	{
		try (TransactionManager transactionManager = TransactionManager.getInstance())
		{
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

			TUserAccountDataAccess uada = new TUserAccountDataAccess(transactionManager);
			CategoryData company = uada.getUserCompanyForUserId(userId);
			boolean success = uada.regenerateApiKeysWithCompanyId(company);
			transactionManager.commit();
			return success;
		}
		catch (Exception e)
		{
			logger.fatal("Oh No!", e);
			throw e;
		}

	}
}
