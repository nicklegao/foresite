package au.corporateinteractive.qcloud.proposalbuilder.control;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Hashtable;

@Controller
public class TutorialController extends AbstractController
{

	Logger logger = Logger.getLogger(TutorialController.class);



	@RequestMapping("/tutorial/endfirst")
	@ResponseBody
	public void dashboardLoad(HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{

		String userID = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		UserAccountDataAccess uda = new UserAccountDataAccess();
		uda.updateTutorialStatus(userID, uda.TUTORIAL_FIRST_ACCESS, false);

	}
}
