package au.corporateinteractive.qcloud.proposalbuilder.control;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.mandrill.QcloudMandrillHeaders;
import com.mandrill.response.EmailEventResponse;

import au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder;

@Controller
public class EmailEventController extends AbstractController
{

	Logger logger = Logger.getLogger(EmailEventController.class);

	@RequestMapping("/free/emailBounceEvent")
	public void emailEvent(HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{

		Gson gson = new Gson();
		try
		{
			//test if it's a mandrill internal test when the hook is created
			if (request.getParameter("mandrill_events") != null && !request.getParameter("mandrill_events").equals(""))
			{
				EmailEventResponse[] resObj = gson.fromJson(request.getParameter("mandrill_events"), EmailEventResponse[].class);

				for (EmailEventResponse res : resObj)
				{
					logger.info("Analizing mandrill api request: " + res.toString());
					if (res.hasMsg())
					{
						ArrayList<String> tags = (ArrayList<String>) res.getMsg().get("tags");
						logger.info("List of tags: " + tags.toString());
						if (tags.contains(QcloudMandrillHeaders.TAG_SYSTEM))
						{
							if (tags.contains(QcloudMandrillHeaders.TAG_PROPOSAL))
							{}
							else
							{
								logger.info("Request has a message but no relevant tag:" + tags.toString());
							}
						}
						else
						{
							logger.info("Email not relevant to Qcloud.");
						}
					}
				}
			}
		}
		catch (Exception ex)
		{
			logger.fatal("Mandrill Request: " + request.getParameter("mandrill_events"), ex);
			new EmailBuilder(request).prepareErrorEmailForContext("Mandril Webhook Error" + "<p>" + request.getParameter("mandrill_events") + "</p>", ex).doSend();
		}

	}

	@RequestMapping("/free/emailReceivedEvent")
	public void emaiOpenlEvent(HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{

		Gson gson = new Gson();
		try
		{
			if (request.getParameter("mandrill_events") != null)
			{
				EmailEventResponse[] resObj = gson.fromJson(request.getParameter("mandrill_events"), EmailEventResponse[].class);

				//Pattern to catch the proposal id inside the brackets
				Pattern pattern = Pattern.compile("\\[(.*?)\\]");

				for (EmailEventResponse res : resObj)
				{
					logger.debug(res.toString());
					if (res.getMsg().get("subject") != null)
					{
						Matcher matcher = pattern.matcher(res.getMsg().get("subject").toString());
						if (matcher.find())
						{
							logger.info("Email: '" + res.getMsg().get("subject") + "' was received");
						}
						else
						{
							logger.error("Received Email: '" + res.getMsg().get("subject") + "' - No Proposal ID found");
						}
					}
					else
					{
						logger.error("Something wrong: The message doesn't have a subject element.");
					}
				}
			}
		}
		catch (Exception ex)
		{
			logger.fatal("Mandrill Request: " + request.getParameter("mandrill_events"));
			new EmailBuilder(request).prepareErrorEmailForContext("Mandril Webhook Error <p>" + request.getParameter("mandrill_events") + "</p>", ex).doSend();
		}
	}

}