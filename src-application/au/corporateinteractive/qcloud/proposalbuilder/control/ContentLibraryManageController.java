///**
// *
// */
//package au.corporateinteractive.qcloud.proposalbuilder.control;
//
//import java.awt.image.BufferedImage;
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.io.Reader;
//import java.net.URL;
//import java.net.URLConnection;
//import java.nio.charset.Charset;
//import java.security.InvalidParameterException;
//import java.sql.SQLException;
//import java.sql.Timestamp;
//import java.text.DateFormat;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Date;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Objects;
//import java.util.Set;
//
//import javax.imageio.ImageIO;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import org.apache.commons.codec.binary.Base64;
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.io.FilenameUtils;
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//import org.apache.pdfbox.pdmodel.PDDocument;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//import org.springframework.security.access.AccessDeniedException;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import com.google.gson.Gson;
//import com.oreilly.servlet.MultipartRequest;
//
//import au.corporateinteractive.qcloud.pdf.service.PdfService2;
//import au.corporateinteractive.qcloud.proposalbuilder.Constants;
//import au.corporateinteractive.qcloud.proposalbuilder.db.CoverPageDataAccess;
//import au.corporateinteractive.qcloud.proposalbuilder.db.SymLinkDataAccess;
//import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
//import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess;
//import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess;
//import au.corporateinteractive.qcloud.proposalbuilder.model.AttachmentResponse;
//import au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions;
//import au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService;
//import au.corporateinteractive.qcloud.proposalbuilder.service.PDFLibraryService;
//import au.corporateinteractive.qcloud.proposalbuilder.service.ProductDataImportService;
//import au.corporateinteractive.qcloud.proposalbuilder.service.ProductLibraryService;
//import au.corporateinteractive.qcloud.proposalbuilder.service.SpreadsheetLibraryService;
//import au.corporateinteractive.qcloud.proposalbuilder.service.TextLibraryService;
//import au.corporateinteractive.qcloud.proposalbuilder.service.VideoLibraryService;
//import au.corporateinteractive.qcloud.proposalbuilder.utils.AjaxResponse;
//import au.net.webdirector.common.datalayer.admin.db.DataAccess;
//import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
//import au.net.webdirector.common.datalayer.base.db.entity.MultiOptions;
//import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
//import au.net.webdirector.common.datalayer.base.db.entity.TextFile;
//import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
//import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
//import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
//import au.net.webdirector.common.datalayer.client.CategoryData;
//import au.net.webdirector.common.datalayer.client.ElementData;
//import au.net.webdirector.common.datalayer.client.ModuleAccess;
//import au.net.webdirector.common.datalayer.client.ModuleHelper;
//import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
//import sbe.ShortUrls.ShortUrlManager;
//
///**
// * @author Jaysun
// *
// */
//@Controller
//public class ContentLibraryManageController extends AbstractController
//{
//	Logger logger = Logger.getLogger(ContentLibraryManageController.class);
//
//	private static final Set<String> supportedLibraries = new HashSet<>(Arrays.asList("images", "text", ""));
//
//	@RequestMapping("/library/manage")
//	public String libraryManage(HttpSession session,
//			@RequestParam String libraryType,
//			Model model) throws SQLException
//	{
//		libraryType = libraryType.toLowerCase();
//
//		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
//		if (userCanManageLibrary(userId, libraryType))
//		{
//
//			String companyId = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR)).getId();
//			List<CategoryData> teams = new UserAccountDataAccess().usersTeams(userId, companyId);
//			ModuleAccess ma = ModuleAccess.getInstance();
//
//
//			String sql = "SELECT e.*, c2.* FROM elements_" + getModuleName(libraryType) + " e "
//					+ "join categories_" + getModuleName(libraryType) + " c2 on e.Category_id = c2.Category_id "
//					+ "join categories_" + getModuleName(libraryType) + " c1 on c2.Category_ParentID = c1.Category_id "
//					+ "where c1.ATTRDROP_CompanyID = ? "
//					+ "and e.live = 1 ";
//
//			if (!getModuleName(libraryType).equalsIgnoreCase(ImageLibraryService.IMAGES_MODULE))
//			{
//				sql += " order by e.attr_headline ";
//			}
//			else
//			{
//				sql += " order by e.element_id desc ";
//			}
//
//			List<ElementData> libraryItems = ma.getElements(sql, new String[] { companyId });
//			List<ElementData> teamlibraryItems = new ArrayList<ElementData>();
//			String libraryDBRef = TUserAccountDataAccess.libraryDBRef(libraryType);
//			String librarySwitchDBRef = TUserAccountDataAccess.librarySwitchDBRef(libraryType);
//			boolean libraryActive = teams.get(0).getBoolean(librarySwitchDBRef);
//
//			if (libraryItems != null && libraryDBRef != null && libraryActive == true)
//			{
//				for (CategoryData team : teams)
//				{
//					MultiOptions activeLibrary = ModuleAccess.getInstance().getMultiOptions(team, libraryDBRef, "users");
//					for (ElementData item : libraryItems)
//					{
//						if (StringUtils.isNotBlank(team.getId()))
//						{
//							for (String active : activeLibrary.getValues())
//							{
//								if (active.equals(item.getParentId()))
//								{
//									teamlibraryItems.add(item);
//								}
//							}
//						}
//					}
//				}
//			}
//
//			if (libraryActive == true)
//			{
//				model.addAttribute("libraryItems", teamlibraryItems);
//				model.addAttribute("module", getModuleName(libraryType));
//			}
//			else
//			{
//				model.addAttribute("libraryItems", libraryItems);
//				model.addAttribute("module", getModuleName(libraryType));
//			}
//
//			sql = "SELECT c2.* FROM categories_" + getModuleName(libraryType) + " c2 "
//					+ "join categories_" + getModuleName(libraryType) + " c1 on c2.Category_ParentID = c1.Category_id "
//					+ "where c1.ATTRDROP_CompanyID = ? ";
//
//			if (!getModuleName(libraryType).equalsIgnoreCase(ImageLibraryService.IMAGES_MODULE))
//			{
//				sql += " order by c2.attr_categoryname ";
//			}
//			else
//			{
//				sql += " order by c2.category_id desc ";
//
//			}
//
//			List<CategoryData> libraryFolders = ma.getCategories(sql, new String[] { companyId });
//			List<CategoryData> teamlibraryFolders = new ArrayList<CategoryData>();
//			if (libraryFolders != null && libraryDBRef != null && libraryActive == true)
//			{
//				for (CategoryData team : teams)
//				{
//					MultiOptions activeLibrary = ModuleAccess.getInstance().getMultiOptions(team, libraryDBRef, "users");
//					for (CategoryData folder : libraryFolders)
//					{
//						if (StringUtils.isNotBlank(team.getId()))
//						{
//							for (String active : activeLibrary.getValues())
//							{
//								if (active.equals(folder.getId()))
//								{
//									teamlibraryFolders.add(folder);
//								}
//							}
//						}
//					}
//				}
//			}
//
//			if (libraryActive == true)
//			{
//				model.addAttribute("libraryFolders", teamlibraryFolders);
//				model.addAttribute("libraryType", libraryType);
//			}
//			else
//			{
//				model.addAttribute("libraryFolders", libraryFolders);
//				model.addAttribute("libraryType", libraryType);
//			}
//			return "dialog/contentLibrary/" + libraryType;
//		}
//		else
//		{
//			throw new RuntimeException("Not allowed to manage the permissions");
//		}
//	}
//
//	public ElementData getLibraryItems(String libraryType, String companyId, String itemName) throws SQLException
//	{
//
//		ModuleAccess ma = ModuleAccess.getInstance();
//		libraryType = libraryType.toLowerCase();
//		String sql = "SELECT e.*, c2.* FROM elements_" + getModuleName(libraryType) + " e "
//				+ "join categories_" + getModuleName(libraryType) + " c2 on e.Category_id = c2.Category_id "
//				+ "join categories_" + getModuleName(libraryType) + " c1 on c2.Category_ParentID = c1.Category_id "
//				+ "where e.ATTR_Headline = " + itemName + " && where c1.ATTRDROP_CompanyID = ?";
//
//		if (!getModuleName(libraryType).equalsIgnoreCase(ImageLibraryService.IMAGES_MODULE))
//		{
//			sql += " order by e.attr_headline ";
//		}
//		else
//		{
//			sql += " order by e.element_id desc ";
//		}
//
//		return ma.getElements(sql, new String[] { companyId }).get(0);
//	}
//
//	public List<CategoryData> getLibrary(String libraryType, String companyId) throws SQLException
//	{
//
//		ModuleAccess ma = ModuleAccess.getInstance();
//
//		String sql = "SELECT c2.* FROM categories_" + getModuleName(libraryType) + " c2 "
//				+ "join categories_" + getModuleName(libraryType) + " c1 on c2.Category_ParentID = c1.Category_id "
//				+ "where c1.ATTRDROP_CompanyID = ? ";
//
//		if (!getModuleName(libraryType).equalsIgnoreCase(ImageLibraryService.IMAGES_MODULE))
//		{
//			sql += " order by c2.attr_categoryname ";
//		}
//		else
//		{
//			sql += " order by c2.category_id desc ";
//
//		}
//
//		return ma.getCategories(sql, new String[] { companyId });
//
//	}
//
//	private boolean userCanManageLibrary(String userId, String libraryType)
//	{
//
//		try (TransactionManager transactionManager = TransactionManager.getInstance())
//		{
//			TUserRolesDataAccess urda = new TUserRolesDataAccess(transactionManager);
//			UserPermissions up = urda.getPermissions(userId);
//			transactionManager.rollback();
//
//			if (libraryType.equalsIgnoreCase("Text"))
//			{
//				return up.hasManageContentTextLibraryPermission();
//			}
//			if (libraryType.equalsIgnoreCase("Images"))
//			{
//				return up.hasManageContentImageLibraryPermission();
//			}
//			if (libraryType.equalsIgnoreCase("Videos"))
//			{
//				return up.hasManageContentVideoLibraryPermission();
//			}
//			if (libraryType.equalsIgnoreCase("Products"))
//			{
//				return up.hasManageContentProductLibraryPermission();
//			}
//			if (libraryType.equalsIgnoreCase("PDF"))
//			{
//				return up.hasManageContentPDFLibraryPermission();
//			}
//			if (libraryType.equalsIgnoreCase("Spreadsheet"))
//			{
//				return up.hasManageContentSpreadsheetLibraryPermission();
//			}
//		}
//		catch (Exception e)
//		{
//			logger.fatal("Oh No!", e);
//		}
//		return false;
//	}
//
//
//	@RequestMapping("/library/delete")
//	@ResponseBody
//	public AjaxResponse libraryDelete(HttpSession session, HttpServletRequest request,
//			@RequestParam("library-type") String libraryType) throws SQLException
//	{
//
//		String elementId = request.getParameter("element-id");
//		String[] elementIdList = request.getParameterValues("element-id-list");
//		libraryType = libraryType.toLowerCase();
//
//		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
//		AjaxResponse response = new AjaxResponse(false, "<p>This item could not be deleted.</p>");
//		if (userCanManageLibrary(userId, libraryType))
//		{
//			try (TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true)))
//			{
//				String companyId = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR)).getId();
//				SymLinkDataAccess slda = new SymLinkDataAccess();
//				String sql = "SELECT e.*, c2.* FROM elements_" + getModuleName(libraryType) + " e "
//						+ "join categories_" + getModuleName(libraryType) + " c2 on e.Category_id = c2.Category_id "
//						+ "join categories_" + getModuleName(libraryType) + " c1 on c2.Category_ParentID = c1.Category_id "
//						//+ "where " + standardLiveClauseForCustomQuery("e") + " "
//						+ "where c1.ATTRDROP_CompanyID = ? "
//						+ "and e.element_id = ? "
//						+ "order by e.attr_headline ";
//				List<ElementData> libraryItems = ModuleAccess.getInstance().getElements(sql, new String[] { companyId, elementId });
//				if (libraryItems.size() == 0)
//				{
//					response = new AjaxResponse(false, "<p>You do not have the permissions to edit this item.</p>");
//					return response;
//				}
//
//				ElementData item = new ElementData();
//				item.setId(elementId);
//				if (slda.isContentSymlinked(getModuleName(libraryType), elementId))
//				{
//					item.setLive(false);
//					TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
//					List<String> proposals = slda.getSymlinkedProposalsForContent(getModuleName(libraryType), elementId);
//					ma.updateElement(item, getModuleName(libraryType));
//					String usedProposals = "";
//					if (proposals.size() > 10)
//					{
//						usedProposals += "<ul style='display:inline-block; text-align: left;'>";
//						for (String proposalId : proposals.subList(0, 9))
//						{
//							usedProposals += "<li>" + proposalId + "</li>";
//						}
//						usedProposals += "</ul>";
//						usedProposals += "<p>and more...</p>";
//					}
//					else
//					{
//						for (String proposalId : proposals)
//						{
//							usedProposals += "<ul style='display:inline-block; text-align: left;'>";
//							usedProposals += "<li>" + proposalId + "</li>";
//							usedProposals += "</ul>";
//						}
//					}
//					response = new AjaxResponse(true, "<p>This content has been archived as it is used in proposals with the IDs: </p>" + usedProposals);
//				}
//				else
//				{
//					TransactionModuleAccess.getInstance(txManager).deleteElementById(getModuleName(libraryType), elementId);
//					response = new AjaxResponse(true, "<p>This item has been deleted.</p>");
//				}
//				txManager.commit();
//				return response;
//			}
//			catch (Exception e)
//			{
//				e.printStackTrace();
//			}
//		}
//		else
//		{
//			response = new AjaxResponse(false, "<p>You do not have the permissions to edit this library.</p>");
//		}
//		return response;
//	}
//
//	@RequestMapping("/library/deleteMulti")
//	@ResponseBody
//	public AjaxResponse libraryDeleteMulti(HttpSession session, HttpServletRequest request,
//			@RequestParam("library-type") String libraryType) throws SQLException
//	{
//
//		String[] elementIdList = request.getParameterValues("element-id-list[]");
//		libraryType = libraryType.toLowerCase();
//
//		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
//		AjaxResponse response = new AjaxResponse(false, "<p>This item could not be deleted.</p>");
//		if (userCanManageLibrary(userId, libraryType))
//		{
//			try (TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true)))
//			{
//				String companyId = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR)).getId();
//				SymLinkDataAccess slda = new SymLinkDataAccess();
//				List<List<ElementData>> libraryItemsArray = new ArrayList<>();
//				List<List<ElementData>> libraryItemsFailedArray = new ArrayList<>();
//				List<String> proposalsUsed = new ArrayList<>();
//				for (String elementId : elementIdList) {
//					String sql = "SELECT e.*, c2.* FROM elements_" + getModuleName(libraryType) + " e "
//							+ "join categories_" + getModuleName(libraryType) + " c2 on e.Category_id = c2.Category_id "
//							+ "join categories_" + getModuleName(libraryType) + " c1 on c2.Category_ParentID = c1.Category_id "
//							//+ "where " + standardLiveClauseForCustomQuery("e") + " "
//							+ "where c1.ATTRDROP_CompanyID = ? "
//							+ "and e.element_id = ? "
//							+ "order by e.attr_headline ";
//					List<ElementData> libraryItems = ModuleAccess.getInstance().getElements(sql, new String[] { companyId, elementId });
//
//					if (libraryItems.size() == 0)
//					{
//						continue;
//					}
//
//					ElementData item = new ElementData();
//					item.setId(elementId);
//					if (slda.isContentSymlinked(getModuleName(libraryType), elementId))
//					{
//						item.setLive(false);
//						TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
//						List<String> proposals = slda.getSymlinkedProposalsForContent(getModuleName(libraryType), elementId);
//						ma.updateElement(item, getModuleName(libraryType));
//						for (String proposal : proposals) {
//							if (Arrays.asList(proposalsUsed).contains(proposal)) {
//								proposalsUsed.add(proposal);
//							}
//						}
//						libraryItemsFailedArray.add(libraryItems);
//					}
//					else
//					{
//						libraryItemsArray.add(libraryItems);
//						TransactionModuleAccess.getInstance(txManager).deleteElementById(getModuleName(libraryType), elementId);
//					}
//
//				}
//
//				if (libraryItemsArray.size() > 0 && libraryItemsFailedArray.size() < 1) {
//					response = new AjaxResponse(true, "<p>All items have been deleted.</p>");
//				} else if (libraryItemsArray.size() > 0 && libraryItemsFailedArray.size() > 0) {
//					String usedProposals = "";
//					if (proposalsUsed.size() > 10)
//					{
//						usedProposals += "<ul style='display:inline-block; text-align: left;'>";
//						for (String proposalId : proposalsUsed.subList(0, 9))
//						{
//							usedProposals += "<li>" + proposalId + "</li>";
//						}
//						usedProposals += "</ul>";
//						usedProposals += "<p>and more...</p>";
//					}
//					else
//					{
//						for (String proposalId : proposalsUsed)
//						{
//							usedProposals += "<ul style='display:inline-block; text-align: left;'>";
//							usedProposals += "<li>" + proposalId + "</li>";
//							usedProposals += "</ul>";
//						}
//					}
//					response = new AjaxResponse(true, "<p>Some items have been removed, however " + libraryItemsFailedArray.size() + " have been archived because there used in proposals with these IDs: </p>" + usedProposals);
//				} else if (libraryItemsArray.size() < 1 && libraryItemsFailedArray.size() > 0) {
//					String usedProposals = "";
//					if (proposalsUsed.size() > 10)
//					{
//						usedProposals += "<ul style='display:inline-block; text-align: left;'>";
//						for (String proposalId : proposalsUsed.subList(0, 9))
//						{
//							usedProposals += "<li>" + proposalId + "</li>";
//						}
//						usedProposals += "</ul>";
//						usedProposals += "<p>and more...</p>";
//					}
//					else
//					{
//						for (String proposalId : proposalsUsed)
//						{
//							usedProposals += "<ul style='display:inline-block; text-align: left;'>";
//							usedProposals += "<li>" + proposalId + "</li>";
//							usedProposals += "</ul>";
//						}
//					}
//					response = new AjaxResponse(true, "<p>" + libraryItemsFailedArray.size() + " have been archived because there used in proposals with these IDs: </p>" + usedProposals);
//				} else {
//					response = new AjaxResponse(false, "<p>You do not have the permissions to edit this items.</p>");
//					return response;
//				}
//
//				txManager.commit();
//				return response;
//			}
//			catch (Exception e)
//			{
//				e.printStackTrace();
//			}
//		}
//		else
//		{
//			response = new AjaxResponse(false, "<p>You do not have the permissions to edit this library.</p>");
//		}
//		return response;
//	}
//
//	@RequestMapping(value = "/library/add-edit/{libraryType}/{elementId}", method = RequestMethod.GET)
//	@ResponseBody
//	public ElementData libraryAddEdit(HttpServletRequest request, HttpServletResponse response, HttpSession session,
//			@PathVariable String libraryType, @PathVariable String elementId) throws IOException, SQLException
//	{
//
//		String module = getModuleName(libraryType);
//		if (!checkContentAccess(elementId, null, module, session))
//		{
//			response.setStatus(401);
//			return null;
//		}
//
//		TransactionDataAccess da = TransactionDataAccess.getInstance();
//		TransactionManager tm = TransactionManager.getInstance(da);
//		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);
//
//		ElementData conditions = new ElementData();
//		conditions.setId(elementId);
//		ElementData item = ma.getElement(module, conditions);
//
//		// ######### IMAGE ATTRIBUTES ##########
//		DecimalFormat decim = new DecimalFormat("0.00");
//
//		StoreFile imageStoreFile = item.getStoreFile("attrfile_image");
//
//		if (null != imageStoreFile)
//		{
//			File image = imageStoreFile.getStoredFile();
//			BufferedImage bimg = ImageIO.read(image);
//			int width = bimg.getWidth();
//			int height = bimg.getHeight();
//			double bytes = image.length();
//			double kilobytes = (bytes / 1024);
//
//			item.put("image_filename", image.getName());
//			item.put("image_uploaded", item.getCreateDate().toString());
//			item.put("image_type", URLConnection.guessContentTypeFromName(image.getName()));
//			item.put("image_width", width);
//			item.put("image_height", height);
//			item.put("image_size", Double.parseDouble(decim.format(kilobytes)) + " KB");
//		}
//		// #####################################
//
//
//		if (libraryType.equalsIgnoreCase("text"))
//		{
//			TextFile textBlock = item.getTextFile("attrlong_textblock");
//			item.put("attrlong_textblock", textBlock.getContent());
//		}
//		else if (libraryType.equalsIgnoreCase("images"))
//		{
//			TextFile textBlock = item.getTextFile("attrlong_description");
//			if (textBlock != null)
//			{
//				item.put("attrlong_description", textBlock.getContent());
//			}
//		}
//		else if (libraryType.equalsIgnoreCase("spreadsheet"))
//		{
//			TextFile textBlock = item.getTextFile("attrtext_json");
//			if (textBlock != null)
//			{
//				item.put("attrtext_json", new Gson().fromJson(textBlock.getContent(), Map.class));
//			}
//			else
//			{
//				item.put("attrtext_json", null);
//			}
//		}
//		return item;
//	}
//
//	@RequestMapping(value = "/library/add-edit/{libraryType}", method = RequestMethod.GET)
//	@ResponseBody
//	public List<CategoryData> libraryAddEditGet(HttpSession session,
//			@PathVariable String libraryType) throws IOException, SQLException
//	{
//		return getTeamFolderList(session, libraryType);
//	}
//
//	@RequestMapping(value = "/library/add-edit/{libraryType}/subfolder/{categoryId}", method = RequestMethod.GET)
//	@ResponseBody
//	public List<String> libraryAddEditGetSubFolder(HttpSession session,
//			@PathVariable String libraryType, @PathVariable String categoryId) throws IOException, SQLException
//	{
//		return getTeamSubFolderList(session, libraryType, categoryId);
//	}
//
//	@RequestMapping(value = "/library/add-edit/products", method = RequestMethod.POST)
//	@ResponseBody
//	public boolean productsLibraryAddEditPost(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, SQLException
//	{
//		String libraryType = "products";
//		String module = getModuleName(libraryType);
//
//		MultipartRequest r = new MultipartRequest(request, System.getProperty("java.io.tmpdir"), Constants.MAX_ATTACHMENT_SIZE);
//		String elementId = r.getParameter("elementId");
//		String folder = r.getParameter("folder");
//
//		if (!checkContentAccess(elementId, folder, module, session))
//		{
//			response.setStatus(401);
//			return false;
//		}
//
//		String newFolder = r.getParameter("newFolder");
//		String subFolder = r.getParameter("subFolder");
//		String newSubFolder = r.getParameter("newSubFolder");
//		String productName = r.getParameter("productName");
//		String description = r.getParameter("description");
//		String price = r.getParameter("price");
//		String term = r.getParameter("term");
//		String image = r.getParameter("image");
//		boolean deleteImage = "on".equals(r.getParameter("deleteImage"));
//		boolean oneoff = "on".equals(r.getParameter("oneOff"));
//		String note = r.getParameter("note");
//		String attr_customfield1 = r.getParameter("attr_customfield1");
//		String attr_customfield2 = r.getParameter("attr_customfield2");
//		String attr_customfield3 = r.getParameter("attr_customfield3");
//		String attr_customfield4 = r.getParameter("attr_customfield4");
//		String attr_customfield5 = r.getParameter("attr_customfield5");
//
//		if (term == null)
//		{
//			term = "0";
//			oneoff = true;
//		}
//
//		String attr_label1 = r.getParameter("attr_label1");
//		String attrfloat_qty1 = StringUtils.isBlank(r.getParameter("attrfloat_qty1")) ? "0" : r.getParameter("attrfloat_qty1");
//		String attrcurrency_price1 = StringUtils.isBlank(r.getParameter("attrcurrency_price1")) ? "0" : r.getParameter("attrcurrency_price1");
//
//		String attr_label2 = r.getParameter("attr_label2");
//		String attrfloat_qty2 = StringUtils.isBlank(r.getParameter("attrfloat_qty2")) ? "0" : r.getParameter("attrfloat_qty2");
//		String attrcurrency_price2 = StringUtils.isBlank(r.getParameter("attrcurrency_price2")) ? "0" : r.getParameter("attrcurrency_price2");
//
//		String attr_label3 = r.getParameter("attr_label3");
//		String attrfloat_qty3 = StringUtils.isBlank(r.getParameter("attrfloat_qty3")) ? "0" : r.getParameter("attrfloat_qty3");
//		String attrcurrency_price3 = StringUtils.isBlank(r.getParameter("attrcurrency_price3")) ? "0" : r.getParameter("attrcurrency_price3");
//
//		String attr_label4 = r.getParameter("attr_label4");
//		String attrfloat_qty4 = StringUtils.isBlank(r.getParameter("attrfloat_qty4")) ? "0" : r.getParameter("attrfloat_qty4");
//		String attrcurrency_price4 = StringUtils.isBlank(r.getParameter("attrcurrency_price4")) ? "0" : r.getParameter("attrcurrency_price4");
//
//		String attr_label5 = r.getParameter("attr_label5");
//		String attrfloat_qty5 = StringUtils.isBlank(r.getParameter("attrfloat_qty5")) ? "0" : r.getParameter("attrfloat_qty5");
//		String attrcurrency_price5 = StringUtils.isBlank(r.getParameter("attrcurrency_price5")) ? "0" : r.getParameter("attrcurrency_price5");
//
//		TransactionDataAccess da = TransactionDataAccess.getInstance();
//		TransactionManager tm = TransactionManager.getInstance(da, StoreFileAccessFactory.getInstance(true));
//		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);
//
//		try
//		{
//			String companyId = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR)).getId();
//			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
//			List<CategoryData> teams = new UserAccountDataAccess().usersTeams(userId, companyId);
//			TUserAccountDataAccess ada = new TUserAccountDataAccess(tm);
//
//			CategoryData conditions = new CategoryData();
//			conditions.put("attrdrop_companyId", companyId);
//			CategoryData companyCategory = ma.getCategory(module, conditions);
//
//			CategoryData category = new CategoryData();
//			if (folder.equals("0"))
//			{
//				category.setParentId(companyCategory.getId());
//				category.setName(newFolder);
//				category.setLive(true);
//				category.setFolderLevel(2);
//				category = ma.insertCategory(category, module);
//				ada.addFolderToTeam(teams.get(0), libraryType, category.getId(), getTeamFolderList(session, libraryType));
//			}
//			else
//			{
//				category.setParentId(companyCategory.getId());
//				category.setId(folder);
//				category = ma.getCategory(module, category);
//			}
//			ElementData newItem = new ElementData();
//			if (StringUtils.isNotBlank(elementId))
//				newItem.setId(elementId);
//			newItem.setParentId(category.getId());
//			newItem.setName(productName);
//			newItem.setLive(true);
//			newItem.put("attr_description", description);
//			newItem.put("attrcurrency_price", price);
//			if (StringUtils.isNotBlank(image))
//			{
//				byte[] imageByte = Base64.decodeBase64(image.split(",")[1].getBytes());
//				File img = File.createTempFile("qcloud", ".png");
//				FileOutputStream outputStream = new FileOutputStream(img);
//				outputStream.write(imageByte);
//				outputStream.flush();
//				outputStream.close();
//				StoreFile sf = new StoreFile(img, module, "attrfile_image", false);
//				if (StringUtils.isNotBlank(elementId))
//					sf.setId(elementId);
//				newItem.put("attrfile_image", sf);
//			}
//			if (deleteImage)
//			{
//				newItem.put("attrfile_image", null);
//			}
//			if (subFolder == null || subFolder.equals("0"))
//			{
//				newItem.put("ATTR_subFolder", newSubFolder);
//			}
//			else
//			{
//				newItem.put("ATTR_subFolder", subFolder);
//			}
//			newItem.put("attr_term", term);
//			newItem.put("attrcheck_oneoff", oneoff);
//			newItem.put("attr_note", note);
//			newItem.put("attr_customfield1", attr_customfield1);
//			newItem.put("attr_customfield2", attr_customfield2);
//			newItem.put("attr_customfield3", attr_customfield3);
//			newItem.put("attr_customfield4", attr_customfield4);
//			newItem.put("attr_customfield5", attr_customfield5);
//
//			newItem.put("attr_label1", attr_label1);
//			newItem.put("attrfloat_qty1", attrfloat_qty1);
//			newItem.put("attrcurrency_price1", attrcurrency_price1);
//
//			newItem.put("attr_label2", attr_label2);
//			newItem.put("attrfloat_qty2", attrfloat_qty2);
//			newItem.put("attrcurrency_price2", attrcurrency_price2);
//
//			newItem.put("attr_label3", attr_label3);
//			newItem.put("attrfloat_qty3", attrfloat_qty3);
//			newItem.put("attrcurrency_price3", attrcurrency_price3);
//
//			newItem.put("attr_label4", attr_label4);
//			newItem.put("attrfloat_qty4", attrfloat_qty4);
//			newItem.put("attrcurrency_price4", attrcurrency_price4);
//
//			newItem.put("attr_label5", attr_label5);
//			newItem.put("attrfloat_qty5", attrfloat_qty5);
//			newItem.put("attrcurrency_price5", attrcurrency_price5);
//
//			if (StringUtils.isNotBlank(elementId))
//				return 1 == ma.updateElement(newItem, module);
//			else
//				newItem = ma.insertElement(newItem, module);
//
//			return newItem != null;
//		}
//		catch (RuntimeException e)
//		{
//			tm.rollback();
//			throw e;
//			// TODO Auto-generated catch block
//		}
//		finally
//		{
//			tm.commit();
//		}
//	}
//
//
//	@RequestMapping(value = "/library/add-edit/spreadsheet", method = RequestMethod.POST)
//	@ResponseBody
//	public boolean spreadsheetLibraryAddEditPost(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, SQLException
//	{
//		String libraryType = "spreadsheet";
//		String module = getModuleName(libraryType);
////		MultipartRequest r = new MultipartRequest(request, System.getProperty("java.io.tmpdir"), Constants.MAX_ATTACHMENT_SIZE);
//		MultipartRequest r = new MultipartRequest(request, System.getProperty("java.io.tmpdir"), Constants.MAX_ATTACHMENT_SIZE);
//		String elementId = r.getParameter("elementId");
//		String folder = r.getParameter("folder");
//
//		if (!checkContentAccess(elementId, folder, module, session))
//		{
//			response.setStatus(401);
//			return false;
//		}
//
//		String newFolder = r.getParameter("newFolder");
//		String subFolder = r.getParameter("subFolder");
//		String newSubFolder = r.getParameter("newSubFolder");
//		String productName = r.getParameter("spreadsheetName");
//		String spreadsheet = r.getParameter("spreadsheet");
//
//		TransactionDataAccess da = TransactionDataAccess.getInstance();
//		TransactionManager tm = TransactionManager.getInstance(da, StoreFileAccessFactory.getInstance(true));
//		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);
//
//		try
//		{
//			CategoryData company = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR));
//			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
//			List<CategoryData> teams = new UserAccountDataAccess().usersTeams(userId, company.getId());
//			TUserAccountDataAccess ada = new TUserAccountDataAccess(tm);
//
//			String companyCategoryId = String.valueOf(getOrCreateCategory(company.getId(), company.getName(), module));
//
//			CategoryData category = new CategoryData();
//			if (folder.equals("0"))
//			{
//				category.setParentId(companyCategoryId);
//				category.setName(newFolder);
//				category.setLive(true);
//				category.setFolderLevel(2);
//				category = ma.insertCategory(category, module);
//				ada.addFolderToTeam(teams.get(0), libraryType, category.getId(), getTeamFolderList(session, libraryType));
//			}
//			else
//			{
//				category.setParentId(companyCategoryId);
//				category.setId(folder);
//				category = ma.getCategory(module, category);
//			}
//			ElementData newItem = new ElementData();
//			newItem.setParentId(category.getId());
//			newItem.setName(productName);
//			newItem.setLive(true);
//
//			if (subFolder == null || subFolder.equals("0"))
//			{
//				newItem.put("ATTR_subFolder", newSubFolder);
//			}
//			else
//			{
//				newItem.put("ATTR_subFolder", subFolder);
//			}
//			TextFile jsonFile = new TextFile(spreadsheet, module, "ATTRTEXT_Json", false);
//			newItem.put("ATTRTEXT_Json", jsonFile);
//			if (StringUtils.isNotBlank(elementId))
//			{
//				newItem.setId(elementId);
//				jsonFile.setId(elementId);
//				return 1 == ma.updateElement(newItem, module);
//			}
//			newItem = ma.insertElement(newItem, module);
//			return newItem != null;
//		}
//		catch (RuntimeException e)
//		{
//			tm.rollback();
//			throw e;
//			// TODO Auto-generated catch block
//		}
//		finally
//		{
//			tm.commit();
//		}
//	}
//
//
//	@RequestMapping(value = "/library/add-edit/text", method = RequestMethod.POST)
//	@ResponseBody
//	public boolean textLibraryAddEditPost(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, SQLException
//	{
//		String libraryType = "text";
//		String module = getModuleName(libraryType);
//
//		MultipartRequest r = new MultipartRequest(request, System.getProperty("java.io.tmpdir"), Constants.MAX_ATTACHMENT_SIZE);
//		String elementId = r.getParameter("elementId");
//		String folder = r.getParameter("folder");
//		String newFolder = r.getParameter("newFolder");
//		String assetName = r.getParameter("assetName");
//		String textBlock = r.getParameter("textBlock");
//		String lockContent = r.getParameter("lockContent");
//
//		if (!checkContentAccess(elementId, folder, module, session))
//		{
//			response.setStatus(401); // check is valid
//			return false;
//		}
//		TransactionManager tm = TransactionManager.getInstance(TransactionDataAccess.getInstance(), StoreFileAccessFactory.getInstance());
//		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);
//
//		String companyId = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR)).getId();
//		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
//		List<CategoryData> teams = new UserAccountDataAccess().usersTeams(userId, companyId);
//		TUserAccountDataAccess ada = new TUserAccountDataAccess(tm);
//
//		CategoryData conditions = new CategoryData();
//		conditions.put("attrdrop_companyId", companyId);
//		CategoryData companyCategory = ma.getCategory(module, conditions);
//
//		CategoryData category = new CategoryData();
//		if (folder.equals("0"))
//		{
//			category.setParentId(companyCategory.getId());
//			category.setName(newFolder);
//			category.setLive(true);
//			category.setFolderLevel(2);
//			category = ma.insertCategory(category, module);
//			ada.addFolderToTeam(teams.get(0), libraryType, category.getId(), getTeamFolderList(session, libraryType));
//		}
//		else
//		{
//			category.setParentId(companyCategory.getId());
//			category.setId(folder);
//			category = ma.getCategory(module, category);
//		}
//
//		TextFile tf = new TextFile(textBlock, module, "attrlong_textblock", false);
//		ElementData newItem = new ElementData();
//		if (StringUtils.isNotBlank(elementId))
//		{
//			newItem.setId(elementId);
//			tf.setId(elementId);
//		}
//		newItem.put("attrlong_textblock", tf);
//		newItem.setParentId(category.getId());
//		newItem.setName(assetName);
//		newItem.setLive(true);
//		newItem.put("attrcheck_lock_content", lockContent == null ? 0 : 1);
//
//		if (StringUtils.isNotBlank(elementId))
//			return 1 == ma.updateElement(newItem, module);
//		else
//			newItem = ma.insertElement(newItem, module);
//
//		return newItem != null;
//	}
//
//	@RequestMapping(value = "/library/add-edit/images", method = RequestMethod.POST)
//	@ResponseBody
//	public boolean imagesLibraryAddEditPost(HttpServletRequest request, HttpServletResponse response, HttpSession session)
//	{
//
//		String libraryType = "images";
//		String module = getModuleName(libraryType);
//		CoverPageDataAccess cpda = new CoverPageDataAccess();
//
//		MultipartRequest r;
//		try
//		{
//			r = new MultipartRequest(request, System.getProperty("java.io.tmpdir"), Constants.MAX_ATTACHMENT_SIZE);
//			String elementId = r.getParameter("elementId");
//			String folder = r.getParameter("folder");
//			String newFolder = r.getParameter("newFolder");
//			String assetName = r.getParameter("assetName");
//			String description = r.getParameter("description");
//			String displayTitleSt = r.getParameter("displayTitle");
//			File image = r.getFile("image");
//
//			if (!checkContentAccess(elementId, folder, module, session))
//			{
//				response.setStatus(401);
//				return false;
//			}
//
//			File croppedFile = null;
//			if (StringUtils.isNotBlank(r.getParameter("w")) && StringUtils.isNotBlank(r.getParameter("h")) && null != image)
//			{
//				int offsetX = (int) Double.parseDouble(r.getParameter("x"));
//				int offsetY = (int) Double.parseDouble(r.getParameter("y"));
//				int width = (int) Double.parseDouble(r.getParameter("w"));
//				int height = (int) Double.parseDouble(r.getParameter("h"));
//
//				croppedFile = cpda.cropFile(image, offsetX, offsetY, width, height, false);
//			}
//
//			return createOrUpdateNewImageElement(session, module, libraryType, folder, newFolder, elementId, assetName, description, displayTitleSt, null != croppedFile ? croppedFile : image);
//
//		}
//		catch (IOException | SQLException e)
//		{
//			logger.fatal("Oh no!", e);
//		}
//
//		return false;
//	}
//
//	public CategoryData getFolderByID(String libraryType, String folderId) throws SQLException
//	{
//
//		ModuleAccess ma = ModuleAccess.getInstance();
//		String sql = "SELECT * FROM categories_" + getModuleName(libraryType) + " WHERE Category_id = ?";
//
//		CategoryData folder = ma.getCategories(sql, new String[] { folderId }).get(0);
//		return folder;
//	}
//
//	public ElementData getProductByID(String productID) throws SQLException
//	{
//
//		ModuleAccess ma = ModuleAccess.getInstance();
//		String sql = "SELECT * FROM elements_PRODUCTS WHERE Element_id = ?";
//
//		ElementData product = ma.getElements(sql, new String[] { productID }).get(0);
//		return product;
//	}
//
//	public List<String> getTeamSubFolderList(HttpSession session, String libraryType, String categoryId) throws IOException, SQLException
//	{
//		DataAccess db = DataAccess.getInstance();
//
//		String sqlSubFolder = "SELECT ATTR_subFolder FROM elements_" + getModuleName(libraryType) + " WHERE Category_id = '" + categoryId + "' GROUP BY ATTR_subFolder";
//
//		return db.select(sqlSubFolder, new StringMapper());
//	}
//
//
//	public List<CategoryData> getTeamFolderList(HttpSession session, String libraryType) throws IOException, SQLException
//	{
//		String companyId = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR)).getId();
//		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
//		List<CategoryData> teams = new UserAccountDataAccess().usersTeams(userId, companyId);
//		String librarySwitchDBRef = TUserAccountDataAccess.librarySwitchDBRef(libraryType);
//		String libraryDBRef = TUserAccountDataAccess.libraryDBRef(libraryType);
//		ModuleAccess ma = ModuleAccess.getInstance();
//		String sql = "SELECT c2.* FROM categories_" + getModuleName(libraryType) + " c2 "
//				+ "join categories_" + getModuleName(libraryType) + " c1 on c2.Category_ParentID = c1.Category_id "
//				+ "where c1.ATTRDROP_CompanyID = ? ";
//
//		if (!getModuleName(libraryType).equalsIgnoreCase(ImageLibraryService.IMAGES_MODULE))
//		{
//			sql += " order by c2.attr_categoryname ";
//		}
//		else
//		{
//			sql += " order by c2.category_id desc ";
//
//		}
//
//		List<CategoryData> libraryFolders = ma.getCategories(sql, new String[] { companyId });
//		List<CategoryData> teamlibraryFolders = new ArrayList<CategoryData>();
//		boolean libraryActive = teams.get(0).getBoolean(librarySwitchDBRef);
//		if (libraryActive == true)
//		{
//			MultiOptions activeLibrary = ModuleAccess.getInstance().getMultiOptions(teams.get(0), libraryDBRef, "users");
//			for (CategoryData folder : libraryFolders)
//			{
//				if (StringUtils.isNotBlank(teams.get(0).getId()))
//				{
//					for (String active : activeLibrary.getValues())
//					{
//						if (active.equals(folder.getId()))
//						{
//							teamlibraryFolders.add(folder);
//						}
//					}
//				}
//			}
//		}
//
//		if (libraryActive == true)
//		{
//			return teamlibraryFolders;
//		}
//		return libraryFolders;
//
//	}
//
//	private boolean createOrUpdateNewImageElement(HttpSession session, String module, String libraryType, String folder, String newFolder, String elementId,
//			String assetName, String description, String displayTitleSt, File image) throws SQLException, IOException
//	{
//
//		boolean displayTitle = null == displayTitleSt ? false : displayTitleSt.equalsIgnoreCase("on") ? true : false;
//
//		TransactionManager tm = TransactionManager.getInstance(TransactionDataAccess.getInstance(), StoreFileAccessFactory.getInstance());
//		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);
//		String companyId = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR)).getId();
//		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
//		List<CategoryData> teams = new UserAccountDataAccess().usersTeams(userId, companyId);
//		CategoryData conditions = new CategoryData();
//		conditions.put("attrdrop_companyId", companyId);
//		TUserAccountDataAccess ada = new TUserAccountDataAccess(tm);
//		CategoryData companyCategory = ma.getCategory(module, conditions);
//
//		CategoryData category = new CategoryData();
//		if (folder.equals("0"))
//		{
//			category.setParentId(companyCategory.getId());
//			category.setName(newFolder);
//			category.setLive(true);
//			category.setFolderLevel(2);
//			category = ma.insertCategory(category, module);
//			ada.addFolderToTeam(teams.get(0), libraryType, category.getId(), getTeamFolderList(session, libraryType));
//		}
//		else
//		{
//			category.setParentId(companyCategory.getId());
//			category.setId(folder);
//			category = ma.getCategory(module, category);
//		}
//
//		TextFile tf = new TextFile(description, module, ImageLibraryService.E_DESCRIPTION, false);
//		ElementData newItem = new ElementData();
//		if (StringUtils.isNotBlank(elementId))
//		{
//			newItem.setId(elementId);
//			tf.setId(elementId);
//		}
//		newItem.put(ImageLibraryService.E_DESCRIPTION, tf);
//		newItem.setParentId(category.getId());
//		newItem.setName(assetName);
//		newItem.put(ImageLibraryService.E_DISPLAY_NAME, displayTitle);
//		newItem.setLive(true);
//		if (image != null)
//		{
//			StoreFile sf = new StoreFile(image, module, ImageLibraryService.E_IMAGE_FILE, false);
//			if (StringUtils.isNotBlank(elementId))
//				sf.setId(elementId);
//
//			newItem.put(ImageLibraryService.E_IMAGE_FILE, sf);
//		}
//
//		if (StringUtils.isNotBlank(elementId))
//			return 1 == ma.updateElement(newItem, module);
//		else
//			newItem = ma.insertElement(newItem, module);
//		return newItem != null;
//	}
//
//	private boolean createOrUpdateNewPDFElement(HttpSession session, String module, String libraryType, String folder, String newFolder, String elementId, String assetName, String description, File PDF) throws SQLException, IOException
//	{
//
//		TransactionManager tm = TransactionManager.getInstance(TransactionDataAccess.getInstance(), StoreFileAccessFactory.getInstance());
//		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);
//		String companyId = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR)).getId();
//		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
//		List<CategoryData> teams = new UserAccountDataAccess().usersTeams(userId, companyId);
//		CategoryData conditions = new CategoryData();
//		conditions.put("ATTRDROP_companyID", companyId);
//		TUserAccountDataAccess ada = new TUserAccountDataAccess(tm);
//		CategoryData companyCategory = ma.getCategory(module, conditions);
//
//		if (assetName == null)
//		{
//			assetName = ShortUrlManager.friendly(FilenameUtils.removeExtension(PDF.getName()));
//
//			if (assetName.length() > 25)
//			{
//				assetName = assetName.substring(0, 20);
//			}
//		}
//
//		CategoryData category = new CategoryData();
//		if (folder.equals("0"))
//		{
//			category.setParentId(companyCategory.getId());
//			category.setName(newFolder);
//			category.setLive(true);
//			category.setFolderLevel(2);
//			category = ma.insertCategory(category, module);
//			ada.addFolderToTeam(teams.get(0), libraryType, category.getId(), getTeamFolderList(session, libraryType));
//		}
//		else
//		{
//			category.setParentId(companyCategory.getId());
//			category.setId(folder);
//			category = ma.getCategory(module, category);
//		}
//
//		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(tm);
//		ElementData newItem = tma.getElementById(module, elementId);
//
//		newItem.setParentId(category.getId());
//		newItem.setName(assetName);
//		newItem.setLive(true);
//		if (PDF != null)
//		{
//			StoreFile sf = new StoreFile(PDF, module, PDFLibraryService.PDF_FILE, false);
//			if (StringUtils.isNotBlank(elementId))
//				sf.setId(elementId);
//
//			newItem.put(PDFLibraryService.PDF_FILE, sf);
//		}
//
//		boolean saved = false;
//		if (StringUtils.isNotBlank(elementId))
//		{
//			newItem.setId(elementId);
//			saved = 1 == ma.updateElement(newItem, module);
//		}
//		else
//		{
//			newItem = ma.insertElement(newItem, module);
//			saved = newItem != null;
//		}
//
//		// generate images for pdf
//		if (saved && PDF != null)
//		{
//			final File savedPdf = ModuleHelper.getStoredFile(newItem.getStoreFile(PDFLibraryService.PDF_FILE).getPath());
//			new Thread(new Runnable()
//			{
//				@Override
//				public void run()
//				{
//					PdfService2.generateSnapShots(savedPdf);
//				}
//			}).start();
//		}
//		return saved;
//	}
//
//	@RequestMapping(value = "/library/add-edit/videos", method = RequestMethod.POST)
//	@ResponseBody
//	public boolean videosLibraryAddEditPost(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, SQLException
//	{
//		String libraryType = "videos";
//		String module = getModuleName(libraryType);
//
//		MultipartRequest r = new MultipartRequest(request, System.getProperty("java.io.tmpdir"), Constants.MAX_ATTACHMENT_SIZE);
//		String elementId = r.getParameter("elementId");
//		String folder = r.getParameter("folder");
//		String newFolder = r.getParameter("newFolder");
//		String assetName = r.getParameter("assetName");
//		String videoLink = r.getParameter("videoLink");
//		String caption = r.getParameter("caption");
//		File thumbnail = r.getFile("thumbnail");
//
//		if (!checkContentAccess(elementId, folder, module, session))
//		{
//			response.setStatus(401);
//			return false;
//		}
//		TransactionManager tm = TransactionManager.getInstance(TransactionDataAccess.getInstance(), StoreFileAccessFactory.getInstance());
//		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);
//
//		String companyId = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR)).getId();
//		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
//		List<CategoryData> teams = new UserAccountDataAccess().usersTeams(userId, companyId);
//		TUserAccountDataAccess ada = new TUserAccountDataAccess(tm);
//
//		CategoryData conditions = new CategoryData();
//		conditions.put("attrdrop_companyId", companyId);
//		CategoryData companyCategory = ma.getCategory(module, conditions);
//
//		CategoryData category = new CategoryData();
//		if (folder.equals("0"))
//		{
//			category.setParentId(companyCategory.getId());
//			category.setName(newFolder);
//			category.setLive(true);
//			category.setFolderLevel(2);
//			category = ma.insertCategory(category, module);
//			ada.addFolderToTeam(teams.get(0), libraryType, category.getId(), getTeamFolderList(session, libraryType));
//		}
//		else
//		{
//			category.setParentId(companyCategory.getId());
//			category.setId(folder);
//			category = ma.getCategory(module, category);
//		}
//
//		ElementData newItem = new ElementData();
//		if (StringUtils.isNotBlank(elementId))
//			newItem.setId(elementId);
//		newItem.setParentId(category.getId());
//		newItem.setName(assetName);
//		newItem.setLive(true);
//		newItem.put("attr_caption", caption);
//		newItem.put("attr_videolink", videoLink);
//		if (thumbnail == null)
//		{
//
//			if (videoLink.contains("youtube.com"))
//			{
//				thumbnail = new File(System.getProperty("java.io.tmpdir") + "thumb.jpg");
//				FileUtils.copyURLToFile(new URL("https://img.youtube.com/vi/" + videoLink.subSequence(videoLink.indexOf("v=") + 2, videoLink.length()) + "/0.jpg"), thumbnail);
//			}
//			else if (videoLink.contains("vimeo.com"))
//			{
//				String vidId = videoLink.substring(videoLink.lastIndexOf("/") + 1);
//				if (!vidId.equals("") && StringUtils.isNumeric(vidId))
//				{
//					thumbnail = new File(System.getProperty("java.io.tmpdir") + "tmp.jpg");
//					InputStream is = new URL("https://vimeo.com/api/v2/video/" + vidId + ".json").openStream();
//					try
//					{
//						BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
//						String jsonText = readAll(rd);
//						JSONArray jsonArray = new JSONArray(jsonText);
//						JSONObject json = jsonArray.getJSONObject(0);
//						FileUtils.copyURLToFile(new URL((String) json.get("thumbnail_large")), thumbnail);
//					}
//					catch (JSONException e)
//					{
//						e.printStackTrace();
//					}
//					finally
//					{
//						is.close();
//					}
//				}
//			}
//		}
//		if (thumbnail != null)
//		{
//			StoreFile sf = new StoreFile(thumbnail, module, "attrfile_thumbnail", false);
//			if (StringUtils.isNotBlank(elementId))
//				sf.setId(elementId);
//			newItem.put("attrfile_thumbnail", sf);
//		}
//
//		if (StringUtils.isNotBlank(elementId))
//			return 1 == ma.updateElement(newItem, module);
//		else
//			newItem = ma.insertElement(newItem, module);
//
//		return newItem != null;
//	}
//
//	private static String readAll(Reader rd) throws IOException
//	{
//		StringBuilder sb = new StringBuilder();
//		int cp;
//		while ((cp = rd.read()) != -1)
//		{
//			sb.append((char) cp);
//		}
//		return sb.toString();
//	}
//
//	@RequestMapping(value = "/library/folder/edit", method = RequestMethod.POST)
//	@ResponseBody
//	public boolean editFolder(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws SQLException, IOException
//	{
//		String folderId = request.getParameter("folderId");
//		String folderName = request.getParameter("folderName");
//		String libraryType = request.getParameter("libraryType");
//
//		String module = getModuleName(libraryType);
//
//		if (!checkContentAccess(null, folderId, module, session))
//		{
//			response.setStatus(401);
//			return false;
//		}
//
//		TransactionDataAccess da = TransactionDataAccess.getInstance();
//		TransactionManager tm = TransactionManager.getInstance(da);
//		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);
//
//		CategoryData company = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR));
//		int companyCategoryId = getOrCreateCategory(company.getId(), company.getName(), module);
//		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
//		List<CategoryData> teams = new UserAccountDataAccess().usersTeams(userId, company.getId());
//		TUserAccountDataAccess ada = new TUserAccountDataAccess(tm);
//
//		CategoryData category = new CategoryData();
//		if (folderId == "")
//		{
//			category.setParentId(String.valueOf(companyCategoryId));
//			category.setName(folderName);
//			category.setLive(true);
//			category.setFolderLevel(2);
//			category = ma.insertCategory(category, module);
//			ada.addFolderToTeam(teams.get(0), libraryType, category.getId(), getTeamFolderList(session, libraryType));
//			return true;
//		}
//		else
//		{
//			category.setId(folderId);
//			category.setName(folderName);
//
//			return 1 == ma.updateCategory(category, getModuleName(libraryType));
//		}
//	}
//
//	@RequestMapping(value = "/library/folder/delete", method = RequestMethod.POST)
//	@ResponseBody
//	public boolean deleteFolder(HttpServletResponse response, HttpSession session,
//			@RequestParam String folderId, @RequestParam String libraryType, @RequestParam String transferTo) throws SQLException, IOException
//	{
//		String module = getModuleName(libraryType);
//
//		if (!checkContentAccess(null, folderId, module, session) || !checkContentAccess(null, transferTo, module, session))
//		{
//			response.setStatus(401);
//			return false;
//		}
//
//		TransactionDataAccess da = TransactionDataAccess.getInstance();
//		TransactionManager tm = TransactionManager.getInstance(da);
//		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);
//
//		ElementData conditions = new ElementData();
//		conditions.setParentId(folderId);
//		List<ElementData> items = ma.getElements(module, conditions);
//		for (ElementData item : items)
//		{
//			item.setParentId(transferTo);
//			ma.updateElement(item, getModuleName(libraryType));
//		}
//
//		CategoryData category = new CategoryData();
//		category.setId(folderId);
//
//		return 1 == ma.deleteCategories(getModuleName(libraryType), category);
//	}
//
//	/**
//	 * @param type
//	 * @return
//	 */
//	public String getModuleName(String type)
//	{
//		if (type.equalsIgnoreCase("Text"))
//		{
//			return TextLibraryService.TEXT_MODULE;
//		}
//		if (type.equalsIgnoreCase("Images"))
//		{
//			return ImageLibraryService.IMAGES_MODULE;
//		}
//		if (type.equalsIgnoreCase("Videos"))
//		{
//			return VideoLibraryService.VIDEOS_MODULE;
//		}
//		if (type.equalsIgnoreCase("Products"))
//		{
//			return ProductLibraryService.PRODUCTS_MODULE;
//		}
//		if (type.equalsIgnoreCase("PDF"))
//		{
//			return PDFLibraryService.PDF_MODULE;
//		}
//		if (type.equalsIgnoreCase("Spreadsheet"))
//		{
//			return SpreadsheetLibraryService.SPREADSHEET_MODULE;
//		}
//		logger.fatal("UNABLE TO DETERMINE TYPE:... " + type);
//		throw new InvalidParameterException("TYPE:" + type);
//	}
//
//	@RequestMapping(value = "/library/add/images/bulk", method = RequestMethod.POST)
//	@ResponseBody
//	public AttachmentResponse imagesLibraryAddBulk(HttpServletRequest request, HttpSession session)
//	{
//		MultipartRequest r;
//		try
//		{
//			r = new MultipartRequest(request, System.getProperty("java.io.tmpdir"), Constants.MAX_ATTACHMENT_SIZE);
//			File f = r.getFile("file");
//
//			boolean imageUploaded = createOrUpdateNewImageElement(session, f);
//			if (imageUploaded)
//			{
//				return new AttachmentResponse(f.getName(), true);
//			}
//			else
//			{
//				return new AttachmentResponse(f.getName(), false);
//			}
//		}
//		catch (IOException e)
//		{
//			logger.fatal("Unable to add image to library.", e);
//		}
//
//		return new AttachmentResponse("", false);
//
//	}
//
//	@RequestMapping(value = "/library/add-edit/pdf", method = RequestMethod.POST)
//	@ResponseBody
//	public AjaxResponse pdfLibraryEdit(HttpServletRequest request, HttpSession session) throws IOException, SQLException
//	{
//		String libraryType = "pdf";
//		String module = getModuleName(libraryType);
//
//		MultipartRequest r = new MultipartRequest(request, System.getProperty("java.io.tmpdir"), Constants.MAX_ATTACHMENT_SIZE);
//		String folder = r.getParameter("folder");
//		String updateFolder = r.getParameter("assetRealName");
//		String assetName = r.getParameter("assetName");
//		String elementId = r.getParameter("elementId");
//		String newFolder = r.getParameter("newFolder");
//		File pdfFile = r.getFile("file");
//
//		if ((pdfFile == null) && (elementId != null))
//		{
//			TransactionDataAccess da = TransactionDataAccess.getInstance();
//			TransactionManager tm = TransactionManager.getInstance(da);
//			TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);
//			ElementData conditions = new ElementData();
//			conditions.setId(elementId);
//			ElementData item = ma.getElement(module, conditions);
//			StoreFile imageStoreFile = item.getStoreFile("attrfile_attached_file");
//			pdfFile = imageStoreFile.getStoredFile();
//
//		}
//
//		try
//		{
//			PDDocument doc = PDDocument.load(pdfFile);
//			if (doc == null) {
//				return new AjaxResponse(false, "The pdf you are uploading is corrupt");
//			}
//		}
//		catch (Exception e)
//		{
//			logger.error("Error:", e);
//			return new AjaxResponse(false, "The pdf you are uploading is corrupt");
//		}
//
//		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//		String description = "PDF-" + timestamp.toString();
//
//		if (folder.isEmpty() || folder.equalsIgnoreCase("Add new folder..."))
//		{
//			if (elementId != null)
//			{
//				return new AjaxResponse(createOrUpdateNewPDFElement(session, pdfFile, newFolder, elementId, assetName), "");
//			}
//			return new AjaxResponse(createOrUpdateNewPDFElement(session, pdfFile, newFolder, null, null), "");
//		}
//		else
//		{
//			if (elementId != null && !Objects.equals(elementId, ""))
//			{
//				if (newFolder != null && newFolder.length() > 0)
//				{
//					return new AjaxResponse(createOrUpdateNewPDFElement(session, pdfFile, newFolder, elementId, assetName), "");
//				}
//				return new AjaxResponse(createOrUpdateNewPDFElement(session, pdfFile, updateFolder, elementId, assetName), "");
//			}
//			return new AjaxResponse(createOrUpdateNewPDFElement(session, pdfFile, folder, null, null), "");
//		}
//
////			return 1 == ma.updateElement(newItem, module);
//
//
//	}
//
//	private boolean createOrUpdateNewImageElement(HttpSession session, File f)
//	{
//
//		if (null == f)
//		{
//			return false;
//		}
//
//		logger.debug("Loaded file:" + f.getAbsolutePath());
//
//		try
//		{
//			long now = new Date().getTime();
//			String filename = ShortUrlManager.friendly(FilenameUtils.removeExtension(f.getName()));
//
//			if (filename.length() > 25)
//			{
//				filename = filename.substring(0, 20);
//			}
//
//			File newFile = new File(f.getParent(), +now + filename + "." + FilenameUtils.getExtension(f.getName()));
//
//			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
//			Date date = new Date();
//			String FOLDER_NAME = Constants.UPLOADED_IMAGES_FOLDER_NAME + " - " + dateFormat.format(date);
//			List<CategoryData> folderList = getTeamFolderList(session, "images");
//			final String MODULE = ImageLibraryService.IMAGES_MODULE;
//
//			FileUtils.moveFile(f, newFile);
//
//			logger.debug("File moved:" + newFile.getAbsolutePath());
//
//			String folderId = "0";
//			TransactionManager tm = TransactionManager.getInstance(TransactionDataAccess.getInstance(), StoreFileAccessFactory.getInstance());
//			TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);
//
//			CategoryData company = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR));
//			CategoryData conditions = new CategoryData();
//			conditions.put("attrdrop_companyId", company.getId());
//			CategoryData companyCategory = ma.getCategory(MODULE, conditions);
//
//			if (null != companyCategory)
//			{
//				conditions = new CategoryData();
//				conditions.put("Category_ParentID", companyCategory.getId());
//				conditions.put("attr_categoryname", FOLDER_NAME);
//				CategoryData folderCategory = ma.getCategory(MODULE, conditions);
//
//				if (null != folderCategory)
//				{
//					folderId = folderCategory.getId();
//				}
//
//				return createOrUpdateNewImageElement(session, MODULE, "images", folderId, FOLDER_NAME, null, filename, "", "", newFile);
//			}
//		}
//		catch (IOException | SQLException e)
//		{
//			logger.fatal("Oh no ", e);
//		}
//
//		return false;
//	}
//
//	private boolean createOrUpdateNewPDFElement(HttpSession session, File f, String folderName, String elementId, String assetName)
//	{
//
//		if (null == f)
//		{
//			return false;
//		}
//
//		logger.debug("Loaded file:" + f.getAbsolutePath());
//
//		try
//		{
//			long now = new Date().getTime();
//			String filename = ShortUrlManager.friendly(FilenameUtils.removeExtension(f.getName()));
//			if (assetName != null)
//			{
//				filename = assetName;
//			}
//
//			if (filename.length() > 25)
//			{
//				filename = filename.substring(0, 20);
//			}
//
//			File newFile = new File(f.getParent(), +now + filename + "." + FilenameUtils.getExtension(f.getName()));
//
//			final String FOLDER_NAME = folderName;
//			final String MODULE = PDFLibraryService.PDF_MODULE;
//
//			FileUtils.moveFile(f, newFile);
//
//			logger.debug("File moved:" + newFile.getAbsolutePath());
//
//			String folderId = "0";
//			TransactionManager tm = TransactionManager.getInstance(TransactionDataAccess.getInstance(), StoreFileAccessFactory.getInstance());
//			TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);
//			PDFLibraryService pdfl = new PDFLibraryService();
//
//			CategoryData company = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR));
//			CategoryData conditions = new CategoryData();
//			int companyCategory = pdfl.getOrCreateCategory(company.getId(), company.getName(), MODULE);
//
//
//			conditions = new CategoryData();
//			conditions.put("Category_ParentID", companyCategory);
//			conditions.put("ATTR_categoryName", FOLDER_NAME);
//			CategoryData folderCategory = ma.getCategory(MODULE, conditions);
//
//			if (null != folderCategory)
//			{
//				folderId = folderCategory.getId();
//			}
//
//			if (elementId != null)
//			{
//				return createOrUpdateNewPDFElement(session, MODULE, "pdf", folderId, FOLDER_NAME, elementId, filename, "", newFile);
//			}
//			return createOrUpdateNewPDFElement(session, MODULE, "pdf", folderId, FOLDER_NAME, null, filename, "", newFile);
//
//		}
//		catch (IOException | SQLException e)
//		{
//			logger.fatal("Oh no ", e);
//		}
//
//		return false;
//	}
//
//	@RequestMapping(value = "/library/export/products")
//	public void productsExport(HttpServletRequest request, HttpServletResponse response, HttpSession session, @RequestParam String id, @RequestParam String row) throws IOException, SQLException
//	{
//		new ProductDataImportService().productsExport(request, response, session, id, row);
//	}
//
//	@RequestMapping(value = "/library/import/products", method = RequestMethod.POST)
//	@ResponseBody
//	public AjaxResponse productsImport(HttpServletRequest request, HttpSession session) throws IOException, SQLException
//	{
//		return new ProductDataImportService().productsImport(request, session);
//	}
//
//
//	private boolean checkContentAccess(String assetId, String folderId, String module, HttpSession session) throws AccessDeniedException
//	{
//		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
//		String companyId = new UserAccountDataAccess().getUserCompanyForUserId(userId).getId();
//		if (!StringUtils.isEmpty(assetId) && Integer.valueOf(assetId) > 0)
//		{
//			String query = "select c1.ATTRDROP_companyID from categories_" + module + " c1, categories_" + module + " c2, elements_" + module + " e "
//					+ "where c1.Category_id=c2.Category_ParentID and c2.Category_id=e.Category_id and e.Element_id=?";
//			List<String> res = DataAccess.getInstance().select(query, new String[] { assetId }, new StringMapper());
//			String ownerCompanyId = res.size() > 0 ? res.get(0) : null;
//			if (ownerCompanyId == null || !StringUtils.equals(companyId, ownerCompanyId))
//			{
//				logger.error("User " + userId + " tried to access " + module + " (" + folderId + ") and failed");
//				return false;
//			}
//		}
//
//		if (!StringUtils.isEmpty(folderId) && Integer.valueOf(folderId) > 0)
//		{
//			String query = "select c1.ATTRDROP_companyID from categories_" + module + " c1, categories_" + module + " c2 "
//					+ "where c1.Category_id=c2.Category_ParentID and c2.Category_id=?";
//			List<String> res = DataAccess.getInstance().select(query, new String[] { folderId }, new StringMapper());
//			String ownerCompanyId = res.size() > 0 ? res.get(0) : null;
//			if (ownerCompanyId == null || !StringUtils.equals(companyId, ownerCompanyId))
//			{
//				logger.error("User " + userId + " tried to access " + module + " folder (" + folderId + ") and failed");
//				return false;
//			}
//		}
//		return true;
//	}
//
//}
