package au.corporateinteractive.qcloud.proposalbuilder.control;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.service.TasksDataImportService;
import au.corporateinteractive.qcloud.proposalbuilder.utils.AjaxResponse;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.client.ElementData;

@Controller
public class TasksController
{
	private static Logger logger = Logger.getLogger(TasksController.class);

	@RequestMapping(value = "/tasks/import", method = RequestMethod.POST)
	@ResponseBody
	public AjaxResponse contactsImport(HttpServletRequest request, HttpSession session)
			throws IOException, SQLException
	{
		return new TasksDataImportService().tasksImport(request, session);
	}

	@RequestMapping(value = "/tasks/export")
	public void contactsExport(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam String row) throws IOException, SQLException
	{
		new TasksDataImportService().tasksExport(request, response, session, row);
	}
	
	@RequestMapping(value = "/tasks/update-status", method = RequestMethod.POST)
	@ResponseBody
	public AjaxResponse updateStatus(HttpServletRequest request, HttpSession session)
			throws IOException, SQLException
	{
		String id = request.getParameter("id");
		String status = request.getParameter("status");
		String reasonText = request.getParameter("reasonText");
		TransactionDataAccess.getInstance().update("update elements_tasks set attr_status = ?, attr_reason = ? where element_id = ?", status, reasonText, id);
		return new AjaxResponse(true, null);
	}
	
	@RequestMapping(value = "/tasks/complete", method = RequestMethod.POST)
	@ResponseBody
	public AjaxResponse completeTask(HttpServletRequest request, HttpSession session)
			throws IOException, SQLException
	{
		String id = request.getParameter("id");
		TransactionDataAccess.getInstance().update("update elements_tasks set attr_status = 'Completed' where element_id = ?", id);
		return new AjaxResponse(true, null);
	}
	
	@RequestMapping(value = "/tasks/incomplete", method = RequestMethod.POST)
	@ResponseBody
	public AjaxResponse incompleteTask(HttpServletRequest request, HttpSession session)
			throws IOException, SQLException
	{
		String id = request.getParameter("id");
		Map<String, String> reasons = getAllIncompleteReasons();
		String reasonCode = request.getParameter("reasonCode");
		String reasonText = reasons.containsKey(reasonCode) ? reasons.get(reasonCode) : reasonCode;
		String reasonDesc = request.getParameter("reasonDesc");
		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		TransactionDataAccess da = TransactionDataAccess.getInstance();
		da.update("update elements_tasks set attr_status = 'Incompleted' where element_id = ?", id);
		da.update("insert into f_task_incomplete_reasons (task_id, reason_code, reason_text, reason_desc, incomplete_time, user_id) values (?,?,?,?,now(),?)", id, reasonCode, reasonText, reasonDesc, userId);
		return new AjaxResponse(true, null);
	}
	

	@RequestMapping(value = "/tasks/details", method = RequestMethod.POST)
	@ResponseBody
	public static ElementData taskDetails(@RequestParam String taskId)
			throws IOException, SQLException
	{
		return null;
	}
	
	@RequestMapping(value = "/tasks/incomplete-reasons", method = RequestMethod.POST)
	@ResponseBody
	public static Map<String, String> getAllIncompleteReasons(){
		return null;
	}
	
	@RequestMapping(value = "/tasks/upload-images", method = RequestMethod.POST)
	@ResponseBody
	public AjaxResponse uploadImages(){
		return null;
	}
	
	@RequestMapping(value = "/tasks/delete-images", method = RequestMethod.POST)
	@ResponseBody
	public AjaxResponse deleteImages(){
		return null;
	}
	
	
}
