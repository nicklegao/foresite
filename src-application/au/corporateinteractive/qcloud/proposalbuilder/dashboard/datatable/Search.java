package au.corporateinteractive.qcloud.proposalbuilder.dashboard.datatable;

import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;

public class Search
{

	public final String value;
	public final String regex;

	public Search(HttpServletRequest request)
	{
		this.value = "";
		this.regex = "false";
	}

	public Search(HttpServletRequest request, int index)
	{
		this.value = ParamParser.readString(request, "columns", index, "search", "value").trim();
		this.regex = ParamParser.readString(request, "columns", index, "search", "regex").trim();
	}

	public Search(JSONObject obj) throws JSONException {
		this.value = obj.getString("value").trim();
		this.regex = obj.getString("regex").trim();
	}

	public Search()
	{
		this.value = "";
		this.regex = "false";
	}
}
