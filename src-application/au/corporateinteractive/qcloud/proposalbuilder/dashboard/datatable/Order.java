package au.corporateinteractive.qcloud.proposalbuilder.dashboard.datatable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class Order
{

	public final int column;
	public final String dir;

	public Order(HttpServletRequest request, int index)
	{
		this.column = ParamParser.readInt(request, "order", index, "column");
		this.dir = ParamParser.readString(request, "order", index, "dir");
	}

	public Order(JSONObject obj) throws JSONException {
		this.column = Integer.parseInt(obj.getString("column"));
		this.dir = obj.getString("dir");
	}

	public Order()
	{
		this.column = 5;
		this.dir = "desc";
	}

	public static Order[] readArray(HttpServletRequest request) throws JSONException {
		String order = request.getParameter("order");
		JSONArray arr = new JSONArray(order);

		List<Order> ret = new LinkedList<Order>();
		int count = arr.length();
		for (int i = 0; i < count; ++i)
		{
			JSONObject obj = new JSONObject(arr.getString(i));
			ret.add(new Order(obj));
		}
		return ret.toArray(new Order[0]);
	}
}
