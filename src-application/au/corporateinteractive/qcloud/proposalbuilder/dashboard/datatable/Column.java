package au.corporateinteractive.qcloud.proposalbuilder.dashboard.datatable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class Column
{

	public final String data;
	public final String name;
	public final Search search;

	public Column(HttpServletRequest request, int index)
	{
		this.data = ParamParser.readString(request, "columns", index, "data");
		this.name = ParamParser.readString(request, "columns", index, "name");
		this.search = new Search(request, index);
	}

	public Column(JSONObject obj) throws JSONException {
		this.data = obj.getString("data");
		this.name = obj.getString("name");
		this.search = new Search(obj.getJSONObject("search"));
	}

	public Column(String data, String name)
	{
		this.data = data;
		this.name = name;
		this.search = new Search();
	}

	public static Column[] readArray(HttpServletRequest request) throws JSONException {
		String columns = request.getParameter("columns");
		JSONArray arr = new JSONArray(columns);

		List<Column> ret = new LinkedList<Column>();
		int count = arr.length();
		for (int i = 0; i < count; ++i)
		{
			JSONObject obj = new JSONObject(arr.getString(i));
			ret.add(new Column(obj));
		}
		return ret.toArray(new Column[0]);
	}
}
