package au.corporateinteractive.qcloud.proposalbuilder.dashboard.datatable;

import org.json.JSONException;

import javax.servlet.http.HttpServletRequest;

/**
 * @see https://datatables.net/manual/server-side
 * @author Sushant Verma
 *
 */
public class DatatableProcessingRequest
{

	public final int draw;//ignore but dont discard

	public int length;
	public final int start;
	public final String startDate;
	public final String endDate;
	public String statusFilter;
	public String advanceFilter;

	public final Search search;
	public final Column[] columns;
	public final Order[] order;

	public DatatableProcessingRequest(HttpServletRequest request) throws JSONException {
		String start = request.getParameter("start");
		String length = request.getParameter("length");
		String endDate = request.getParameter("endDate");
		String startDate = request.getParameter("startDate");
		String statusFilter = request.getParameter("statusFilter");
		String advanceFilter = request.getParameter("advanceFilter");

		this.draw = 1;
		this.start = Integer.parseInt(start);
		this.length = Integer.parseInt(length);
		this.endDate = endDate;
		this.startDate = startDate;
		this.statusFilter = statusFilter;
		this.advanceFilter = advanceFilter;

		this.search = new Search(request);
		this.columns = Column.readArray(request);
		this.order = Order.readArray(request);
	}

	// construct a default object for fetching all proposals
	public DatatableProcessingRequest()
	{
		this.draw = 42;
		this.start = 0;
		this.length = -1;
		this.endDate = "";
		this.startDate = "";
		this.statusFilter = "";
		this.advanceFilter = "";

		this.search = new Search();
		this.columns = new Column[] {
				new Column("statusBucket", ""),
				new Column("1", ""),
//				new Column("clientCompany", ""),
				new Column("clientName", ""),
				new Column("totalMinimumCost", ""),
				new Column("createdDate", ""),
				new Column("modifiedDate", ""),
				new Column("expireDate", ""),
				new Column("status", ""),
				new Column("8", ""),
		};
		this.order = new Order[] { new Order() };
	}
}
