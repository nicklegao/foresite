package au.corporateinteractive.qcloud.proposalbuilder.validations;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleData;

public class UsersValidation
{
	private static Logger logger = Logger.getLogger(UsersValidation.class);

	private TransactionManager txManager;
	private ModuleData insertedData;

	public UsersValidation(TransactionManager txManager, ModuleData insertedData)
	{
		this.txManager = txManager;
		this.insertedData = insertedData;
	}

	public List<String> validateUsers(String companyId) throws SQLException
	{
		List<String> errors = new ArrayList<String>();
		if (insertedData != null && userExists())
		{
			errors.add("User already exists");
		}
		if (!userAdminExists())
		{
			errors.add("At least one user must have the manage users permission");
		}
		return errors;
	}

	private boolean userExists() throws SQLException
	{
		TransactionDataAccess tda = TransactionDataAccess.getInstance(txManager);
		QueryBuffer qb = new QueryBuffer();
		qb.append("select count(*) from elements_useraccounts where attr_headline = ? ", insertedData.getName());
		String userCount = tda.selectString(qb);
		return Integer.valueOf(userCount) > 1;
	}

	private boolean userAdminExists() throws SQLException
	{
		TUserRolesDataAccess turda = new TUserRolesDataAccess(txManager);
		TUserAccountDataAccess tuada = new TUserAccountDataAccess(txManager);
		CategoryData company = tuada.getUserCompanyForUserId(insertedData.getId());
		List<ElementData> users = turda.getUsersInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_USERS);
		return users.size() > 0;
	}
}
