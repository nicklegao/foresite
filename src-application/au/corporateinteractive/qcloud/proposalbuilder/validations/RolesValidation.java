package au.corporateinteractive.qcloud.proposalbuilder.validations;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleData;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;

public class RolesValidation
{
	private static Logger logger = Logger.getLogger(RolesValidation.class);

	private TransactionManager txManager;
	private ModuleData insertedData;

	public RolesValidation(TransactionManager txManager, ModuleData insertedData)
	{
		this.txManager = txManager;
		this.insertedData = insertedData;
	}

	//Used for validation after inserting
	public List<String> validateRoles(String companyId) throws SQLException
	{
		TUserRolesDataAccess urda = new TUserRolesDataAccess(txManager);
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		CategoryData cat = new CategoryData();
		cat.put(urda.C_COMPANY_ID, companyId);
		CategoryData company = tma.getCategory(urda.MODULE, cat);
		List<ElementData> roles = tma.getElementsByParentId(urda.MODULE, company.getId());
		return performChecks(roles);
	}

	//Role is to be updated
	public List<String> validateRoles(String companyId, ElementData input, boolean delete) throws SQLException
	{
		TUserRolesDataAccess urda = new TUserRolesDataAccess(txManager);
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		CategoryData cat = new CategoryData();
		cat.put(urda.C_COMPANY_ID, companyId);
		CategoryData company = tma.getCategory(urda.MODULE, cat);
		List<ElementData> roles = tma.getElementsByParentId(urda.MODULE, company.getId());
		ElementData updatedRole = null;
		for (ElementData role : roles)
		{
			if (role.getId().equals(input.getId()))
			{
				updatedRole = role;
			}
		}
		if (updatedRole != null)
		{
			roles.remove(updatedRole);
			if (!delete)
			{
				roles.add(input);
			}
		}

		return performChecks(roles);
	}

	private List<String> performChecks(List<ElementData> roles)
	{
		List<String> errors = new ArrayList<String>();
		if (!manageUserPermissionsCheck(roles))
		{
			errors.add("There must at least one role that can manage users");
		}
		return errors;
	}

	private boolean manageUserPermissionsCheck(List<ElementData> roles)
	{
		TUserRolesDataAccess urda = new TUserRolesDataAccess(txManager);
		int manageUsersCount = 0;
		for (ElementData role : roles)
		{
			if (role.getBoolean(urda.MANAGE_USERS))
			{
				manageUsersCount++;
			}
		}
		return manageUsersCount > 0;
	}
}
