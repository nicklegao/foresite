package au.corporateinteractive.qcloud.proposalbuilder.service;

import java.io.IOException;
import java.util.*;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class CurrencyService
{

	// essential URL structure is built using constants
	public static final String ACCESS_KEY = "f5acea9d3b55854d42492d4789f14ee3";
	public static final String BASE_URL = "http://apilayer.net/api/";
	public static final String ENDPOINT = "live";


	private static CurrencyService instance;

	private CurrencyService() {

	}

	public synchronized static CurrencyService getInstance() {
		if (instance == null) {
			instance = new CurrencyService();
		}
		return instance;
	}

	/**
	 *
	 * Notes:<br><br>
	 *
	 * A JSON response of the form {"key":"value"} is considered a simple Java JSONObject.<br>
	 * To get a simple value from the JSONObject, use: <JSONObject identifier>.get<Type>("key");<br>
	 *
	 * A JSON response of the form {"key":{"key":"value"}} is considered a complex Java JSONObject.<br>
	 * To get a complex value like another JSONObject, use: <JSONObject identifier>.getJSONObject("key")
	 *
	 * Values can also be JSONArray Objects. JSONArray objects are simple, consisting of multiple JSONObject Objects.
	 *
	 *
	 */

	public String getExchange(String countryName) {

		HttpClient httpClient = new DefaultHttpClient();

		try {

			String currencyCode = getCurrencyCode(countryName);

			String url = BASE_URL + ENDPOINT + "?access_key=" + ACCESS_KEY + "&source=AUD" + "&currencies=" + currencyCode;
			HttpGet get = new HttpGet(url);
			HttpResponse response = httpClient.execute(get);
			HttpEntity entity = response.getEntity();

			// the following line converts the JSON Response to an equivalent Java Object
			JSONObject exchangeRates = new JSONObject(EntityUtils.toString(entity));

			System.out.println("Live Currency Exchange Rates");

			return "1 " + exchangeRates.getString("source") + " in GBP : " + exchangeRates.getJSONObject("quotes").getDouble("AUD" + currencyCode);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	//to retrieve currency code
	public static String getCurrencyCode(String countryName) {

		Map<String, String> countries = new HashMap<>();
		for (String iso : Locale.getISOCountries()) {
			Locale l = new Locale("", iso);
			countries.put(l.getDisplayCountry().toLowerCase(), iso);
		}

		return Currency.getInstance(new Locale("", countries.get(countryName.toLowerCase()))).getCurrencyCode();
	}

	//to retrieve currency symbol
	public static String getCurrencySymbol(String countryName) {

		Map<String, String> countries = new HashMap<>();
		for (String iso : Locale.getISOCountries()) {
			Locale l = new Locale("", iso);
			countries.put(l.getDisplayCountry().toLowerCase(), iso);
		}

		return Currency.getInstance(new Locale("", countries.get(countryName.toLowerCase()))).getSymbol();
	}
}
