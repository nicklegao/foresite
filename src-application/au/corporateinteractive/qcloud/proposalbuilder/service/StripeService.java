/**
 * @author Nick Yiming Gao
 * @date 20/03/2017 10:17:13 am
 */
package au.corporateinteractive.qcloud.proposalbuilder.service;

import static au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess.C1_ALERTED_DATA_PLAN_QTY;
import static au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess.C1_DATE_FORMAT;
import static au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess.C1_STATUS;
import static au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess.C1_STRIPE_ID;
import static au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess.C_ADDRESS_COUNTRY;
import static au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess.C_BILLING_USERS;
import static au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess.C_PRICING_PLAN;
import static au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess.USER_MODULE;
import static au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess.isPayByCreditCard;

/**
 * 
 */

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.exception.StripeException;
import com.stripe.model.BalanceTransaction;
import com.stripe.model.Card;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.model.ExternalAccount;
import com.stripe.model.Invoice;
import com.stripe.model.InvoiceLineItem;
import com.stripe.model.Plan;
import com.stripe.model.Subscription;
import com.stripe.model.SubscriptionCollection;
import com.stripe.model.SubscriptionItem;
import com.stripe.net.APIResource;

import au.com.ci.sbe.util.ModuleVariables;
import au.corporateinteractive.qcloud.market.enums.SubscriptionStatus;
import au.corporateinteractive.qcloud.pdf.service.PdfService;
import au.corporateinteractive.qcloud.proposalbuilder.db.InvoiceDataAccess;
//import au.corporateinteractive.qcloud.proposalbuilder.db.MarketDataAccess;
//import au.corporateinteractive.qcloud.proposalbuilder.db.MarketSubscriberDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess;
//import au.corporateinteractive.qcloud.subscription.MarketSubscriptionPlan;
import au.corporateinteractive.qcloud.subscription.SubscriptionPlan;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.MultiOptions;
import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
import au.net.webdirector.common.datalayer.base.db.mapper.ElementDataMapper;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.common.utils.email.PendingEmail;

public class StripeService
{

	private static Logger logger = Logger.getLogger(StripeService.class);

	private static Properties properties;

	private static String mode;
	private static String securitKey;
	private static String publishableKey;

	private StripeService()
	{

	}

	static
	{
		try
		{
			properties = PropertiesLoaderUtils.loadAllProperties("config/stripe/stripe.properties");
			mode = properties.getProperty("mode");
			securitKey = properties.getProperty(mode + ".securitKey");
			publishableKey = properties.getProperty(mode + ".publishableKey");
			Stripe.apiKey = securitKey;
		}
		catch (IOException e)
		{
			logger.error("Error:", e);
			// TODO Auto-generated catch block
		}
	}

	public static boolean isProduct()
	{
		return "production".equals(mode) || "live".equals(mode) || "prod".equals(mode);
	}

	public static String getSecuritKey()
	{
		return securitKey;
	}

	public static String getPublishableKey()
	{
		return publishableKey;
	}

	public static StripeService getInstance()
	{
		return new StripeService();
	}

	public boolean upgradePlan(CategoryData company, SubscriptionPlan planA, TransactionManager transactionManager, HttpServletRequest request, SubscriptionPlan... planB)
	{
		try
		{
			if (SubscriptionPlan.isTrialPlan(planA.getName()))
			{
				return true;
			}
			TUserAccountDataAccess uada = new TUserAccountDataAccess(transactionManager);
			Customer customer = syncCustomer(company);
			Plan basePlan = syncPlan(planA);
			List<Plan> additionalPlans = new ArrayList<>();
			List<SubscriptionPlan> planMore = new ArrayList<>(Arrays.asList(planB));
			int addUserPlanQty = 0;
			int addDataPlanQty = 0;

			for (int i = 0; i < planMore.size(); i++)
			{
				if (planMore.get(i).getName().equals(SubscriptionPlan.PLAN_PER_USER))
				{
					addUserPlanQty = planMore.get(i).getQuantity();
				}
				if (planMore.get(i).getName().equals(SubscriptionPlan.PLAN_PER_DATA_USAGE))
				{
					addDataPlanQty = planMore.get(i).getQuantity();
				}
				additionalPlans.add(syncPlan(planMore.get(i)));
			}


			double gst = calcGstPercent(company);

			Map<String, Object> params = new HashMap<String, Object>();
			Map<String, Object> items = new HashMap<String, Object>();
			params.put("items", items);

			Subscription subscription = retrieveCurrentSubscriptionByCustomer(customer);
			boolean subscriptionChanged = false;
			if (subscription == null)
			{
				// create subscription
				params.put("customer", customer.getId());
				if (gst > 0)
				{
					params.put("tax_percent", gst);
				}

				Map<String, Object> itemA = new HashMap<String, Object>();
				itemA.put("plan", basePlan.getId());
				items.put(String.valueOf(items.size()), itemA);
				for (int i = 0; i < planMore.size(); i++)
				{
					SubscriptionPlan additionalPlan = planMore.get(i);
					if (additionalPlan.getQuantity() == 0)
					{
						continue;
					}
					Map<String, Object> itemB = new HashMap<String, Object>();
					itemB.put("plan", additionalPlans.get(i).getId());
					itemB.put("quantity", additionalPlan.getQuantity());
					items.put(String.valueOf(items.size()), itemB);
				}
				subscription = Subscription.create(params);
				subscriptionChanged = true;
			}
			else
			{
				// update subscription
				List<SubscriptionItem> oldItems = subscription.getSubscriptionItems().getData();
				SubscriptionItem oldItemA = oldItems.get(0);
				if (!StringUtils.equals(oldItemA.getPlan().getId(), basePlan.getId()))
				{
					Map<String, Object> itemA = new HashMap<String, Object>();
					String itemIdA = oldItems.get(0).getId();
					itemA.put("id", itemIdA);
					itemA.put("plan", basePlan.getId());
					items.put(String.valueOf(items.size()), itemA);
				}
				for (int i = 1; i < oldItems.size(); i++)
				{
					SubscriptionItem oldItemB = oldItems.get(i);
					SubscriptionPlan plan = removeSubscriptionPlan(additionalPlans, oldItemB.getPlan(), planMore);
					if (plan == null || plan.getQuantity() == 0)
					{
						oldItemB.delete();
						subscriptionChanged = true;
						continue;
					}
					else if (oldItemB.getQuantity() != plan.getQuantity())
					{
						Map<String, Object> itemB = new HashMap<String, Object>();
						itemB.put("id", oldItemB.getId());
						itemB.put("quantity", plan.getQuantity());
						items.put(String.valueOf(items.size()), itemB);
					}
				}
				for (int i = 0; i < planMore.size(); i++)
				{
					if (planMore.get(i).getQuantity() == 0)
					{
						continue;
					}
					Map<String, Object> itemB = new HashMap<String, Object>();
					itemB.put("plan", additionalPlans.get(i).getId());
					itemB.put("quantity", planMore.get(i).getQuantity());
					items.put(String.valueOf(items.size()), itemB);
				}
				if (items.size() > 0)
				{
					subscription.update(params);
					subscriptionChanged = true;
				}
			}
			if (subscriptionChanged)
			{
				transactionManager.getDataAccess().update("update categories_users set attrdrop_pricingplan = ?, ATTRINTEGER_AddUserPlanQty = ?, ATTRINTEGER_AddDataPlanQty = ?, ATTRINTEGER_AlertedDataPlanQty = ?, ATTRDATE_LastBillingDate = ? where category_id = ?", planA.getName(), addUserPlanQty, addDataPlanQty, addDataPlanQty, new Date(), company.getId());
				sendChangePlanSuccessEmail(company, planA, request, planB);
			}
			return true;
		}
		catch (CardException e)
		{
			// Since it's a decline, CardException will be caught
			logger.error("CardException:", e);
			try
			{
				transactionManager.getDataAccess().update("update categories_users set attrdrop_status = 'Suspended' where category_id = ?", company.getId());
			}
			catch (SQLException e1)
			{
				logger.error("Error:", e1);
			}
			sendAccountHasNoPaymentSourceEmail(company, request);
		}
		catch (InvalidRequestException e)
		{
			logger.error("InvalidRequestException:", e);
			if (StringUtils.startsWithIgnoreCase(e.getMessage(), "This customer has no attached payment source"))
			{
				try
				{
					transactionManager.getDataAccess().update("update categories_users set attrdrop_status = 'Suspended' where category_id = ?", company.getId());
				}
				catch (SQLException e1)
				{
					logger.error("Error:", e1);
				}
				sendAccountHasNoPaymentSourceEmail(company, request);
			}
			else
			{
				sendChangePlanFailureEmail(company, planA, request, planB);
			}
		}
		catch (Exception e)
		{
			logger.error("Exception:", e);
			sendChangePlanFailureEmail(company, planA, request, planB);
		}
		return false;
	}

	private SubscriptionPlan removeSubscriptionPlan(List<Plan> additionalPlans, Plan plan, List<SubscriptionPlan> planMore)
	{
		for (int i = 0; i < additionalPlans.size(); i++)
		{
			if (additionalPlans.get(i).getId().equals(plan.getId()))
			{
				additionalPlans.remove(i);
				return planMore.remove(i);
			}
		}
		return null;
	}

	public static void sendChangePlanFailureEmail(CategoryData company, SubscriptionPlan newPlan, HttpServletRequest request, SubscriptionPlan[] planB)
	{
		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");
		String[] receiverEmails = getUserEmailsInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_USERS);

		try
		{
			MultiOptions billingUsersList = ModuleAccess.getInstance().getMultiOptions(company, C_BILLING_USERS,
					USER_MODULE);

			if (billingUsersList.getValues().size() > 0)
			{
				receiverEmails = new String[billingUsersList.getValues().size()];
				receiverEmails = billingUsersList.getValues().toArray(receiverEmails);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		PendingEmail email = new EmailBuilder(request)
				.addModule(USER_MODULE, company)
				.addModule(SubscriptionPlan.MODULE, newPlan.toMap())
				.prepareSubscriptionChangeFailureEmail();
		if (receiverEmails.length == 0)
		{
			email.addTo(adminEmail.split(","));
		}
		else
		{
			email.addTo(receiverEmails);
			email.addBCC(adminEmail.split(","));
		}
		email.doSend();
	}

	private void sendUpgradePlanFailureEmail(CategoryData company, SubscriptionPlan basePlan, SubscriptionPlan additionalPlan, int qty, HttpServletRequest request)
	{
		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");
		String[] receiverEmails = getUserEmailsInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_USERS);

		try
		{
			MultiOptions billingUsersList = ModuleAccess.getInstance().getMultiOptions(company, C_BILLING_USERS,
					USER_MODULE);

			if (billingUsersList.getValues().size() > 0)
			{
				receiverEmails = new String[billingUsersList.getValues().size()];
				receiverEmails = billingUsersList.getValues().toArray(receiverEmails);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		PendingEmail email = new EmailBuilder(request)
				.addModule(USER_MODULE, company)
				.addModule(SubscriptionPlan.MODULE, basePlan.toMap())
				.prepareSubscriptionChangeFailureEmail();
		if (receiverEmails.length == 0)
		{
			email.addTo(adminEmail.split(","));
		}
		else
		{
			email.addTo(receiverEmails);
			email.addBCC(adminEmail.split(","));
		}
		email.doSend();
	}

//	public static void sendAppSubscribeFailureEmail(CategoryData company, MarketSubscriptionPlan appPlan, HttpServletRequest request)
//	{
//		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");
//		String[] receiverEmails = getUserEmailsInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_USERS);
//
//		try
//		{
//			MultiOptions billingUsersList = ModuleAccess.getInstance().getMultiOptions(company, C_BILLING_USERS,
//					USER_MODULE);
//
//			if (billingUsersList.getValues().size() > 0)
//			{
//				receiverEmails = new String[billingUsersList.getValues().size()];
//				receiverEmails = billingUsersList.getValues().toArray(receiverEmails);
//			}
//		}
//		catch (SQLException e)
//		{
//			e.printStackTrace();
//		}
//
//		PendingEmail email = new EmailBuilder(request)
//				.addModule(USER_MODULE, company)
//				.addModule(MarketSubscriptionPlan.MODULE, appPlan.toMap())
//				.prepareAppSubscriptionFailureEmail();
//		if (receiverEmails.length == 0)
//		{
//			email.addTo(adminEmail.split(","));
//		}
//		else
//		{
//			email.addTo(receiverEmails);
//			email.addBCC(adminEmail.split(","));
//		}
//		email.doSend();
//	}

	public static void sendChangePlanSuccessEmail(CategoryData company, SubscriptionPlan planA, HttpServletRequest request, SubscriptionPlan[] planB)
	{
		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");
		String[] receiverEmails = getUserEmailsInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_USERS);

		try
		{
			MultiOptions billingUsersList = ModuleAccess.getInstance().getMultiOptions(company, C_BILLING_USERS,
					USER_MODULE);

			if (billingUsersList.getValues().size() > 0)
			{
				receiverEmails = new String[billingUsersList.getValues().size()];
				receiverEmails = billingUsersList.getValues().toArray(receiverEmails);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		List<String> planNames = new ArrayList<>();
		planNames.add(planA.getDescription());
		double fee = planA.getPrice();
		for (SubscriptionPlan plan : planB)
		{
			if (plan.getQuantity() > 0)
			{
				planNames.add(plan.getDescription() + " x " + plan.getQuantity());
				fee += plan.getPrice() * plan.getQuantity();
			}
		}
		Map<String, Object> plans = new HashMap<>();
		plans.put("planNames", planNames);
		plans.put("fee", planA.getCurrencySymbol() + new DecimalFormat("0.00").format(fee));

		EmailBuilder builder = new EmailBuilder(request)
				.addModule(USER_MODULE, company)
				.addModule(SubscriptionPlan.MODULE, plans);
		PendingEmail email = builder
				.prepareSubscriptionChangeSuccessEmail();

		if (receiverEmails.length == 0)
		{
			email.addTo(adminEmail.split(","));
		}
		else
		{
			email.addTo(receiverEmails);
			email.addBCC(adminEmail.split(","));
		}
		email.doSend();
	}

	public void sendDataExceedNotification(CategoryData company, SubscriptionPlan planA, HttpServletRequest request, SubscriptionPlan[] planB, double diskUsageGB, double maxDiskUsageGB, int newDataPlanQty) throws SQLException
	{
		int alteredDataPlanQty = company.getInt(C1_ALERTED_DATA_PLAN_QTY);
		if (newDataPlanQty <= alteredDataPlanQty)
		{
			return;
		}
		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");
		String[] receiverEmails = getUserEmailsInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_USERS);

		try
		{
			MultiOptions billingUsersList = ModuleAccess.getInstance().getMultiOptions(company, C_BILLING_USERS,
					USER_MODULE);

			if (billingUsersList.getValues().size() > 0)
			{
				receiverEmails = new String[billingUsersList.getValues().size()];
				receiverEmails = billingUsersList.getValues().toArray(receiverEmails);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		List<String> planNames = new ArrayList<>();
		planNames.add(planA.getName());
		int userLimit = planA.getMaxUsers();
		double dataLimit = planA.getMaxData();
		double fee = planA.getPrice();
		double dataPrice = 0;
		double maxData = 0;
		for (SubscriptionPlan plan : planB)
		{
			if (StringUtils.equals(plan.getName(), SubscriptionPlan.PLAN_PER_DATA_USAGE))
			{
				dataPrice = plan.getPrice();
				maxData = plan.getMaxData();
			}
			if (plan.getQuantity() > 0)
			{
				planNames.add(plan.getName() + " x " + plan.getQuantity());
				userLimit += plan.getMaxUsers() * plan.getQuantity();
				dataLimit += plan.getMaxData() * plan.getQuantity();
				fee += plan.getPrice() * plan.getQuantity();
			}
		}
		Map<String, Object> plans = new HashMap<>();
		plans.put("planNames", planNames);
		plans.put("userLimit", userLimit);
		plans.put("dataLimit", dataLimit);
		plans.put("fee", planA.getCurrencySymbol() + new DecimalFormat("0.00").format(fee));
		plans.put("dataPrice", planA.getCurrencySymbol() + new DecimalFormat("0.00").format(dataPrice));
		plans.put("maxData", maxData);
		plans.put("diskUsageGB", diskUsageGB);
		plans.put("maxDiskUsageGB", maxDiskUsageGB);

		EmailBuilder builder = new EmailBuilder(request)
				.addModule(USER_MODULE, company)
				.addModule(SubscriptionPlan.MODULE, plans);
		PendingEmail email = builder
				.prepareDataExceedNotificationEmail();

		if (receiverEmails.length == 0)
		{
			email.addTo(adminEmail.split(","));
		}
		else
		{
			email.addTo(receiverEmails);
			email.addBCC(adminEmail.split(","));
		}
		email.doSend();
		company.put(C1_ALERTED_DATA_PLAN_QTY, newDataPlanQty);
		TransactionDataAccess.getInstance().update("update categories_users set ATTRINTEGER_AlertedDataPlanQty = ? where category_id = ?", newDataPlanQty, company.getId());

	}

//	public static void sendAppSubscribeChangedEmail(CategoryData company, MarketSubscriptionPlan appPlan, HttpServletRequest request)
//	{
//		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");
//		String[] receiverEmails = getUserEmailsInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_USERS);
//
//		try
//		{
//			MultiOptions billingUsersList = ModuleAccess.getInstance().getMultiOptions(company, C_BILLING_USERS,
//					USER_MODULE);
//
//			if (billingUsersList.getValues().size() > 0)
//			{
//				receiverEmails = new String[billingUsersList.getValues().size()];
//				receiverEmails = billingUsersList.getValues().toArray(receiverEmails);
//			}
//		}
//		catch (SQLException e)
//		{
//			e.printStackTrace();
//		}
//
//		EmailBuilder builder = new EmailBuilder(request)
//				.addModule(USER_MODULE, company);
//		PendingEmail email = builder.addModule(MarketSubscriptionPlan.MODULE, appPlan.toMap())
//				.prepareAppSubscriptionChangedEmail();
//
//		if (receiverEmails.length == 0)
//		{
//			email.addTo(adminEmail.split(","));
//		}
//		else
//		{
//			email.addTo(receiverEmails);
//			email.addBCC(adminEmail.split(","));
//		}
//		email.doSend();
//	}

//	public static void sendAppSubscribeSuccessEmail(CategoryData company, MarketSubscriptionPlan appPlan, HttpServletRequest request)
//	{
//		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");
//		String[] receiverEmails = getUserEmailsInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_USERS);
//
//		try
//		{
//			MultiOptions billingUsersList = ModuleAccess.getInstance().getMultiOptions(company, C_BILLING_USERS,
//					USER_MODULE);
//
//			if (billingUsersList.getValues().size() > 0)
//			{
//				receiverEmails = new String[billingUsersList.getValues().size()];
//				receiverEmails = billingUsersList.getValues().toArray(receiverEmails);
//			}
//		}
//		catch (SQLException e)
//		{
//			e.printStackTrace();
//		}
//
//		EmailBuilder builder = new EmailBuilder(request)
//				.addModule(USER_MODULE, company);
//		PendingEmail email = builder.addModule(MarketSubscriptionPlan.MODULE, appPlan.toMap())
//				.prepareAppSubscriptionSuccessEmail();
//
//		if (receiverEmails.length == 0)
//		{
//			email.addTo(adminEmail.split(","));
//		}
//		else
//		{
//			email.addTo(receiverEmails);
//			email.addBCC(adminEmail.split(","));
//		}
//		email.doSend();
//	}

	private void sendPaymentFailedEmail(CategoryData company, Invoice invoice, HttpServletRequest request, boolean isMarketSubscription)
	{
		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");
		String[] receiverEmails = getUserEmailsInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_USERS);

		try
		{
			MultiOptions billingUsersList = ModuleAccess.getInstance().getMultiOptions(company, C_BILLING_USERS,
					USER_MODULE);

			if (billingUsersList.getValues().size() > 0)
			{
				receiverEmails = new String[billingUsersList.getValues().size()];
				receiverEmails = billingUsersList.getValues().toArray(receiverEmails);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		DataMap info = new DataMap();
		info.put("attempt_count", formatCount(invoice.getAttemptCount()));
		info.put("secondAttemp", invoice.getAttemptCount() > 1);
		info.put("next_payment_attempt", formatDate(invoice.getNextPaymentAttempt()));

//		ElementData subscribedApp = MarketDataAccess.getInstance().getSubscribedApp(invoice.getSubscription());
//		info.put("market_app_label", isMarketSubscription ? "the" + subscribedApp.getName() + " App for " : "");

		PendingEmail email = new EmailBuilder(request)
				.addModule(USER_MODULE, company)
				.addModule("INFO", info)
				.preparePaymentFailedEmail();
		if (receiverEmails.length == 0)
		{
			email.addTo(adminEmail.split(","));
		}
		else
		{
			email.addTo(receiverEmails);
			email.addBCC(adminEmail.split(","));
		}
		email.doSend();
	}

	private String formatCount(Integer attemptCount)
	{
		switch (attemptCount)
		{
			case 1:
				return "first";
			default:
				return "second";
		}
	}

	/**
	 * @param nextPaymentAttempt
	 * @return
	 */
	private String formatDate(Long nextPaymentAttempt)
	{
		return new SimpleDateFormat("dd/MMM/yyyy").format(new Date(nextPaymentAttempt * 1000));
	}

	private void sendPaymentSucceededEmail(CategoryData company, Invoice invoice, HttpServletRequest request, File invoicePdf)
	{
		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");

		String[] receiverEmails = getUserEmailsInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_USERS);

		try
		{
			MultiOptions billingUsersList = ModuleAccess.getInstance().getMultiOptions(company, C_BILLING_USERS,
					USER_MODULE);

			if (billingUsersList.getValues().size() > 0)
			{
				receiverEmails = new String[billingUsersList.getValues().size()];
				receiverEmails = billingUsersList.getValues().toArray(receiverEmails);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		PendingEmail email = new EmailBuilder(request)
				.addModule(USER_MODULE, company)
				.preparePaymentSucceededEmail();
		if (receiverEmails.length == 0)
		{
			email.addTo(adminEmail.split(","));
		}
		else
		{
			email.addTo(receiverEmails);
			email.addBCC(adminEmail.split(","));
		}
		email.addAttachment(invoicePdf)
				.doSend();
	}

	private void sendAccountHasNoPaymentSourceEmail(CategoryData company, HttpServletRequest request)
	{
		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");
		String[] receiverEmails = getUserEmailsInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_USERS);

		try
		{
			MultiOptions billingUsersList = ModuleAccess.getInstance().getMultiOptions(company, C_BILLING_USERS,
					USER_MODULE);

			if (billingUsersList.getValues().size() > 0)
			{
				receiverEmails = new String[billingUsersList.getValues().size()];
				receiverEmails = billingUsersList.getValues().toArray(receiverEmails);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		PendingEmail email = new EmailBuilder(request)
				.addModule(USER_MODULE, company)
				.prepareAccountHasNoPaymentSourceEmail();
		if (receiverEmails.length == 0)
		{
			email.addTo(adminEmail.split(","));
		}
		else
		{
			email.addTo(receiverEmails);
			email.addBCC(adminEmail.split(","));
		}
		email.doSend();
	}

	private void sendAccountSuspendedEmail(CategoryData company, HttpServletRequest request)
	{
		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");
		String[] receiverEmails = getUserEmailsInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_USERS);

		try
		{
			MultiOptions billingUsersList = ModuleAccess.getInstance().getMultiOptions(company, C_BILLING_USERS,
					USER_MODULE);

			if (billingUsersList.getValues().size() > 0)
			{
				receiverEmails = new String[billingUsersList.getValues().size()];
				receiverEmails = billingUsersList.getValues().toArray(receiverEmails);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		PendingEmail email = new EmailBuilder(request)
				.addModule(USER_MODULE, company)
				.prepareAccountSuspendedEmail();
		if (receiverEmails.length == 0)
		{
			email.addTo(adminEmail.split(","));
		}
		else
		{
			email.addTo(receiverEmails);
			email.addBCC(adminEmail.split(","));
		}
		email.doSend();
	}

	private void sendApplicationSuspendedEmail(CategoryData company, Invoice invoice, HttpServletRequest request)
	{
//		ElementData subscribedApp = MarketDataAccess.getInstance().getSubscribedApp(invoice.getSubscription());
//		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");
//		String[] receiverEmails = getUserEmailsInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_USERS);
//
//		try
//		{
//			MultiOptions billingUsersList = ModuleAccess.getInstance().getMultiOptions(company, C_BILLING_USERS,
//					USER_MODULE);
//
//			if (billingUsersList.getValues().size() > 0)
//			{
//				receiverEmails = new String[billingUsersList.getValues().size()];
//				receiverEmails = billingUsersList.getValues().toArray(receiverEmails);
//			}
//		}
//		catch (SQLException e)
//		{
//			e.printStackTrace();
//		}
//
//		PendingEmail email = new EmailBuilder(request)
//				.addModule(USER_MODULE, company)
//				.prepareAppSuspendedEmail(subscribedApp.getName());
//		if (receiverEmails.length == 0)
//		{
//			email.addTo(adminEmail.split(","));
//		}
//		else
//		{
//			email.addTo(receiverEmails);
//			email.addBCC(adminEmail.split(","));
//		}
//		email.doSend();
	}

	public static boolean isCompanySuspended(String companyId)
	{
		CategoryData company = new UserAccountDataAccess().getCompanyForCompanyId(companyId);
		return isCompanySuspended(company);
	}

	public static boolean isCompanySuspended(CategoryData company)
	{
		return StringUtils.startsWithIgnoreCase(company.getString(C1_STATUS), "suspended");
	}

	public static boolean isCompanyCancelled(String companyId)
	{
		CategoryData company = new UserAccountDataAccess().getCompanyForCompanyId(companyId);
		return isCompanyCancelled(company);
	}

	public static boolean isCompanyCancelled(CategoryData company)
	{
		return StringUtils.startsWithIgnoreCase(company.getString(C1_STATUS), "cancelled");
	}

	public boolean needsSetupPayment(String companyId, boolean noNeedIfInTrialPlan)
	{
		CategoryData company = new UserAccountDataAccess().getCompanyForCompanyId(companyId);

		if (!isPayByCreditCard(company))
		{
			return false;
		}
		if (noNeedIfInTrialPlan && SubscriptionPlan.isTrialPlan(company.getString(C_PRICING_PLAN)))
		{
			return false;
		}
		//TODO: check if source is set for company
		Card card = getCard(retrieveStripeCustomer(company));
		if (card == null)
		{
			return true;
		}
		return StringUtils.isBlank(card.getLast4());
	}

	public boolean needsSetupPaymentForMarket(String companyId)
	{
		CategoryData company = new UserAccountDataAccess().getCompanyForCompanyId(companyId);

		if (!isPayByCreditCard(company))
		{
			return false;
		}
		Card card = getCard(retrieveStripeCustomer(company));
		if (card == null)
		{
			return true;
		}
		return StringUtils.isBlank(card.getLast4());
	}

	public Customer syncCustomer(CategoryData customerCI) throws StripeException
	{
		return syncCustomer("", customerCI);
	}

	public Customer syncCustomer(String source, CategoryData customerCI) throws StripeException
	{
//		List<ElementData> receivers = new TUserRolesDataAccess(transactionManager).getUsersInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_SUBSCRIPTIONS);
		try
		{
			Customer customer = retrieveStripeCustomer(customerCI);
			if (customer != null)
			{
				Map<String, Object> updateParams = new HashMap<String, Object>();
				if (!StringUtils.equals(customerCI.getName(), customer.getDescription()))
				{
					updateParams.put("description", customerCI.getName());
				}
				if (!StringUtils.isBlank(source))
				{
					updateParams.put("source", source);
				}
				if (updateParams.size() > 0)
				{
					customer = customer.update(updateParams);
					customerCI.put(C1_STRIPE_ID, customer.getId());
					TransactionDataAccess.getInstance().update("update categories_users set attr_stripeid = ? where category_id = ?", customer.getId(), customerCI.getId());
				}
				return customer;
			}

			Map<String, Object> customerParams = new HashMap<String, Object>();
			customerParams.put("description", customerCI.getName());
			if (!StringUtils.isBlank(source))
			{
				customerParams.put("source", source);
			}
			customer = Customer.create(customerParams);
			customerCI.put(C1_STRIPE_ID, customer.getId());
			TransactionDataAccess.getInstance().update("update categories_users set attr_stripeid = ? where category_id = ?", customer.getId(), customerCI.getId());
			return customer;
		}
		catch (StripeException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return null;
		}
	}

	public Customer retrieveStripeCustomer(CategoryData customerCI)
	{
		String stripeId = customerCI.getString(C1_STRIPE_ID);
		Customer customer = null;
		try
		{
			if (StringUtils.isNotBlank(stripeId))
			{
				customer = Customer.retrieve(stripeId);
				if (customer != null && customer.getDeleted() != null && customer.getDeleted())
				{
					customer = null;
				}
			}

		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			customer = null;
		}
		return customer;
	}

	public Subscription retrieveStripeSubscription(String subscriptionId)
	{
		Subscription sub = null;
		try
		{
			if (StringUtils.isNotBlank(subscriptionId))
			{
				sub = Subscription.retrieve(subscriptionId);
			}
		}
		catch (Exception e)
		{
			logger.error(e);
			sub = null;
		}
		return sub;
	}

	public static Card getCard(Customer customer)
	{
		if (customer == null)
		{
			return null;
		}
		if (customer.getSources() == null)
		{
			return null;
		}
		if (customer.getSources().getData() == null || customer.getSources().getData().size() == 0)
		{
			return null;
		}
		for (ExternalAccount account : customer.getSources().getData())
		{
			if (account instanceof Card)
			{
				return (Card) account;
			}
		}
		return null;
	}

	public static String getLast4CardNumber(Customer customer)
	{
		Card card = getCard(customer);
		if (card == null)
		{
			return "";
		}
		return card.getLast4();
	}

	public Plan syncPlan(SubscriptionPlan planCI)
	{
		return syncPlan(planCI, false);
	}

	public Plan syncPlan(SubscriptionPlan planCI, boolean isMarketApp)
	{
		String planModule = isMarketApp ? "market_subscriptions" : "subscriptions";
		try
		{
			Plan plan = retrieveStripePlan(planCI);
			if (plan != null)
			{
				Map<String, Object> updateParams = new HashMap<String, Object>();
				if (!StringUtils.equals(planCI.getLineItemDescription(), plan.getName()))
				{
					updateParams.put("name", planCI.getName());
				}
				if (updateParams.size() > 0)
				{
					plan = plan.update(updateParams);
					planCI.setStripeId(plan.getId());
					TransactionDataAccess.getInstance().update("update elements_" + planModule + " set attr_stripeId = ? where element_id = ?", plan.getId(), planCI.getId());
				}
				return plan;
			}

			Map<String, Object> planParams = new HashMap<String, Object>();
			planParams.put("amount", planCI.getPriceCents());
			planParams.put("interval", "month");
			planParams.put("name", planCI.getLineItemDescription());
			planParams.put("currency", planCI.getCurrencyCode());
			planParams.put("id", planCI.getName() + " - " + planCI.getId());

			plan = Plan.create(planParams);
			planCI.setStripeId(plan.getId());
			TransactionDataAccess.getInstance().update("update elements_" + planModule + " set attr_stripeid = ? where element_id = ?", plan.getId(), planCI.getId());
			return plan;
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return null;
		}
	}

	public Plan retrieveStripePlan(SubscriptionPlan planCI)
	{
		String stripeId = planCI.getStripeId();
		Plan plan = null;
		try
		{
			if (StringUtils.isNotBlank(stripeId))
			{
				plan = Plan.retrieve(stripeId);
			}

		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			plan = null;
		}
		return plan;
	}

	public Charge retrieveStripeCharge(String chargeId)
	{
		Charge charge = null;
		try
		{
			if (StringUtils.isNotBlank(chargeId))
			{
				charge = Charge.retrieve(chargeId);
			}

		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			charge = null;
		}
		return charge;
	}

	public Subscription retrieveCurrentSubscriptionByCustomer(Customer customer)
	{
		try
		{
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("customer", customer.getId());
			params.put("limit", 100);
			SubscriptionCollection subscriptions = Subscription.list(params);
			if (subscriptions == null || subscriptions.getData() == null || subscriptions.getData().size() == 0)
			{
				return null;
			}
			for (Subscription sub : subscriptions.getData())
			{
				//Imp Note: qcloud subscrption plan dont use metadata
				if (sub.getMetadata().size() == 0)
				{
					return sub;
				}
			}
			return null;
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return null;
		}
	}

	public List<Subscription> retrieveCurrentSubscriptionsByCustomer(Customer customer)
	{
		try
		{
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("customer", customer.getId());
			params.put("limit", 100);
			SubscriptionCollection subscriptions = Subscription.list(params);
			if (subscriptions == null || subscriptions.getData() == null || subscriptions.getData().size() == 0)
			{
				return null;
			}
			return subscriptions.getData();
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return null;
		}
	}

	/**
	 * @param invoice
	 * @throws SQLException
	 */
	public ElementData saveInvoice(Invoice invoice)
	{
		try
		{
			CategoryData company = findCompanyFromStripeId(invoice.getCustomer());
			String invoiceJson = APIResource.PRETTY_PRINT_GSON.toJson(invoice);
			Date date = parseDate(invoice.getDate());
			double amount = toDollar(invoice.getTotal());
			ElementData invoiceCI = new InvoiceDataAccess().saveInvoice(company.getId(), company.getName(), invoice.getId(), invoiceJson, date, amount, invoice.getCurrency());
			return invoiceCI;
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return null;
		}
	}

	public CategoryData findCompanyFromStripeId(String stripeCustomerId) throws SQLException
	{
		List<CategoryData> list = ModuleAccess.getInstance().getCategories("select * from categories_users where attr_stripeId = ?", stripeCustomerId);
		return list.size() > 0 ? list.get(0) : null;
	}

	public static boolean isCurrentPeriod(Invoice invoice)
	{
//		return invoice.getPeriodStart() * 1000 < System.currentTimeMillis()
//				&& invoice.getPeriodEnd() * 1000 > System.currentTimeMillis();
		return true;
	}


	private static List<ElementData> getUsersInCompanyByRole(String companyId, String role)
	{
		QueryBuffer qb = new QueryBuffer();
		qb.append("select u.* from categories_users c, categories_users g, elements_useraccounts u, drop_down_multi_selections r, elements_roles r1 ");
		qb.append("where 1 = 1 ");
		qb.append("and c.category_id = g.category_parentid ");
		qb.append("and g.category_id = u.category_id ");
		qb.append("and r.ELEMENT_ID = u.element_id ");
		qb.append("and r.ATTRMULTI_NAME='ATTRMULTI_roles' ");
		qb.append("and r.MODULE_NAME='users' ");
		qb.append("and r.module_type='element' ");
		qb.append("and r1.element_id = r.SELECTED_VALUE ");
		qb.append("and r1." + role + " = 1 ");
		qb.append("and c.category_id = ? ", companyId);

		TransactionDataAccess da = TransactionDataAccess.getInstance();
		try
		{
			return da.selectMaps(qb, new ElementDataMapper());
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return new ArrayList<ElementData>();
		}
	}

	public static String[] getUserEmailsInCompanyByRole(String companyId, String role)
	{
		List<ElementData> receivers = getUsersInCompanyByRole(companyId, role);
		String[] receiverEmails = new String[receivers.size()];
		for (int i = 0; i < receivers.size(); i++)
		{
			ElementData receiver = receivers.get(i);
			receiverEmails[i] = receiver.getName();
		}
		return receiverEmails;
	}

	public void processPaymentFailedEvent(Invoice invoice) throws Exception
	{
		// check period, check subscription status, set customer to suspended.
		if (!StripeService.isCurrentPeriod(invoice))
		{
			return;
		}
		CategoryData company = findCompanyFromStripeId(invoice.getCustomer());
		String subscriptionId = invoice.getSubscription();
		Subscription subscription = retrieveStripeSubscription(subscriptionId);

		//qcloud subscrption plan dont use metadata so size is 0
		boolean isMarketSubscription = subscription.getMetadata().size() > 0;

		if (StringUtils.equals(subscription.getStatus(), "unpaid"))
		{
			if (isMarketSubscription)
			{
				//suspend the application
				String query = "update elements_market_subscribers set ATTRDROP_status = 'Suspended' where ATTR_subscriptionId = ?";
				TransactionDataAccess.getInstance().update(query, subscriptionId);
				//send app suspended email to customer
				sendApplicationSuspendedEmail(company, invoice, null);
			}
			else
			{
				// set customer to suspended.
				TransactionDataAccess.getInstance().update("update categories_users set attrdrop_status = 'Suspended' where attr_stripeid = ?", invoice.getCustomer());
				// send suspend email to customer
				sendAccountSuspendedEmail(company, null);
			}

		}
		else
		{
			// send payment failed email to customer
			sendPaymentFailedEmail(company, invoice, null, isMarketSubscription);
		}

	}

	public void generateInvoicePdf(ElementData invoiceCI, HttpServletRequest request) throws Exception
	{
		// generate invoice pdf
		String htmlContentUrl = "";
		if (request != null)
		{
			int serverPort = request.getServerPort();
			String portString = "";
//								if (serverPort == 443)
//								{// hack for HTTPS connection, force to do HTTP connection because pdf has some http images or resources that will break https connection
//									serverPort = 80;
//								}
			if (serverPort != 80 && serverPort != 443)
			{
				portString = ":" + serverPort;
			}
			String server = "http://" + request.getServerName() + portString + request.getContextPath();
			htmlContentUrl = String.format("%s/api/free/payment/invoice?invId=%s", server, invoiceCI.getId());
			logger.warn("GENERATED PDF HTML Content Url: " + htmlContentUrl);
		}
		else
		{
			Defaults defaultSetting = Defaults.getInstance();
			String siteUrl = defaultSetting.getWebSiteURL();
			htmlContentUrl = String.format("%s/api/free/payment/invoice?invId=%s", siteUrl, invoiceCI.getId());
			logger.warn("DEFAULT PDF HTML Content Url: " + htmlContentUrl);
		}
		File invoicePdf = PdfService.getInstance().generatePDFByUrl(htmlContentUrl);
		invoiceCI.put(InvoiceDataAccess.E_INVOICE_PDF, new StoreFile(invoicePdf, InvoiceDataAccess.MODULE, InvoiceDataAccess.E_INVOICE_PDF, false, invoiceCI.getId()));
		try (TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true)))
		{
			TransactionModuleAccess.getInstance(txManager).updateElement(invoiceCI, InvoiceDataAccess.MODULE);
			txManager.commit();
		}

	}

	public void processPaymentSucceededEvent(Invoice invoice, ElementData invoiceCI, HttpServletRequest request) throws Exception
	{
		// check period.
		if (!StripeService.isCurrentPeriod(invoice))
		{
			return;
		}

		File invoicePdf = invoiceCI.getStoreFile(InvoiceDataAccess.E_INVOICE_PDF).getStoredFile();

		try (TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true)))
		{
			invoiceCI.put(InvoiceDataAccess.E_PAID, "1");
			TransactionModuleAccess.getInstance(txManager).updateElement(invoiceCI, InvoiceDataAccess.MODULE);
			// set customer to normal
			TransactionDataAccess.getInstance(txManager).update("update categories_users set attrdrop_status = 'Normal' where attr_stripeid = ?", invoice.getCustomer());
			txManager.commit();
		}

		if (invoice.getAmountDue() > 0)
		{
			// send to customer;
			CategoryData company = findCompanyFromStripeId(invoice.getCustomer());
			sendPaymentSucceededEmail(company, invoice, null, invoicePdf);
		}
	}

	public static Date parseDate(Long date)
	{
		if (date == null)
		{
			return null;
		}
		return new Date(date * 1000);
	}

	public static double calcGstPercent(CategoryData company)
	{
		if ("Australia".equalsIgnoreCase(company.getString(C_ADDRESS_COUNTRY))
				|| "Au".equalsIgnoreCase(company.getString(C_ADDRESS_COUNTRY)))
		{
			return 10.0;
		}
		return 0;
	}

	public static String formatMoney(Long cents)
	{
		return cents == null ? "0.00" : new DecimalFormat("#,##0.00").format(toDollar(cents));
	}

	public static String formatMoney(Integer cents)
	{
		return cents == null ? "0.00" : new DecimalFormat("#,##0.00").format(toDollar(cents));
	}

	public static double toDollar(Long cents)
	{
		return cents == null ? 0d : cents * 1.0d / 100;
	}

	public static double toDollar(Integer cents)
	{
		return cents == null ? 0d : cents * 1.0d / 100;
	}

	public List<BalanceTransaction> retrieveStripeBalanceTransactions(Date since)
	{
		try
		{
			Map<String, Object> balanceTransactionParams = new HashMap<String, Object>();
			Map<String, Object> createdParams = new HashMap<String, Object>();
			createdParams.put("gte", since.getTime() / 1000);
			balanceTransactionParams.put("created", createdParams);

			return BalanceTransaction.list(balanceTransactionParams).getData();
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return new ArrayList<BalanceTransaction>();
		}
	}

	public static boolean deleteCardFromCustomer(Customer customer)
	{
		try
		{
			Card card = getCard(customer);
			if (card == null)
			{
				return true;
			}
			card.delete();
			return true;
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return false;
		}

	}

	public static List<InvoiceLineItem> getSortedItems(Invoice invoice)
	{
		List<InvoiceLineItem> items = new ArrayList<InvoiceLineItem>(invoice.getLines().getData());
		if (StringUtils.startsWithIgnoreCase(invoice.getId(), "TD_"))
		{
			return items;
		}
		Collections.sort(items, new Comparator<InvoiceLineItem>()
		{
			@Override
			public int compare(InvoiceLineItem o1, InvoiceLineItem o2)
			{
				try
				{
					if (o1.getPeriod().getStart().longValue() != o2.getPeriod().getStart().longValue())
					{
						return (int) (o1.getPeriod().getStart().longValue() - o2.getPeriod().getStart().longValue());
					}
					if (o1.getPeriod().getEnd().longValue() != o2.getPeriod().getEnd().longValue())
					{
						return (int) (o1.getPeriod().getEnd().longValue() - o2.getPeriod().getEnd().longValue());
					}
				}
				catch (Exception e)
				{
					return 0;
				}
				return (int) (o1.getAmount().longValue() / Math.abs(o1.getAmount().longValue()) - o2.getAmount().longValue() / Math.abs(o2.getAmount().longValue()));
			}
		});
		return items;
	}

//	public Subscription subscribeApp(MarketSubscriptionPlan appPlan, ElementData appSubscription, CategoryData company, HttpServletRequest request, String userId, String appId)
//	{
//		MarketSubscriberDataAccess msda = new MarketSubscriberDataAccess();
//
//		Subscription subscription = null;
//		try
//		{
//			if (!StringUtils.equals("CANCELLED", appSubscription.getString(MarketSubscriberDataAccess.E_STATUS)))
//				subscription = Subscription.retrieve(appSubscription.getString(MarketSubscriberDataAccess.E_SUBSCRIPTION_ID));
//		}
//		catch (Exception e1)
//		{
//		}
//		try
//		{
//			Customer customer = syncCustomer(company);
//			Plan plan = syncPlan(appPlan, true);
//			// create app subscription
//			Map<String, Object> params = new HashMap<String, Object>();
//			if (subscription == null)
//			{
//				params.put("customer", customer.getId());
//				params.put("plan", plan.getId());
//				Map<String, String> metaData = new HashMap<String, String>();
//				metaData.put("subscriptionType", "market_app");
//				params.put("metadata", metaData);
//
//
//				double gst = calcGstPercent(company);
//				if (gst > 0)
//				{
//					params.put("tax_percent", gst);
//				}
//
//				if (appSubscription == null)
//				{
//					params.put("trial_period_days", 14);
//				}
//				params.put("quantity", appPlan.getQuantity());
//				subscription = Subscription.create(params);
//				msda.addSubscriptionRecord(userId, company, appId, subscription, appPlan.getQuantity());
////				sendAppSubscribeSuccessEmail(company, appPlan, request);
//			}
//			else
//			{
//				if (!StringUtils.equals(appPlan.getStripeId(), subscription.getSubscriptionItems().getData().get(0).getPlan().getId()))
//				{
//					Map<String, Object> items = new HashMap<String, Object>();
//					params.put("items", items);
//
//					List<SubscriptionItem> oldItems = subscription.getSubscriptionItems().getData();
//
//					Map<String, Object> itemA = new HashMap<String, Object>();
//					String itemIdA = oldItems.get(0).getId();
//					itemA.put("id", itemIdA);
//					itemA.put("plan", plan.getId());
//					itemA.put("quantity", appPlan.getQuantity());
//					items.put(String.valueOf(items.size()), itemA);
//
//					for (int i = 1; i < oldItems.size(); i++)
//					{
//						SubscriptionItem oldItemB = oldItems.get(i);
//						oldItemB.delete();
//					}
//
//					subscription = subscription.update(params);
//					msda.addSubscriptionRecord(userId, company, appId, subscription, appPlan.getQuantity());
////					sendAppSubscribeChangedEmail(company, appPlan, request);
//				}
//				else if (appPlan.getQuantity() != subscription.getSubscriptionItems().getData().get(0).getQuantity())
//				{
//					//params.put("id", subscription.getId());
//					params.put("quantity", appPlan.getQuantity());
//
//					subscription = subscription.update(params);
//					msda.addSubscriptionRecord(userId, company, appId, subscription, appPlan.getQuantity());
////					sendAppSubscribeChangedEmail(company, appPlan, request);
//				}
//			}
//			return subscription;
//		}
//		catch (Exception e)
//		{
//			logger.error("Error:", e);
////			sendAppSubscribeFailureEmail(company, appPlan, request);
//			return null;
//		}
//	}

//	public Subscription unsubscribeApp(String subscriptionId, HttpServletRequest request, boolean cancelNow)
//	{
//		try
//		{
//			Subscription subscription = Subscription.retrieve(subscriptionId);
//			Map<String, Object> params = new HashMap<String, Object>();
//			boolean isTrial = StringUtils.equals(subscription.getStatus(), "trialing");
//			params.put("at_period_end", cancelNow ? false : isTrial ? false : true);
//			return subscription.cancel(params);
//		}
//		catch (AuthenticationException | InvalidRequestException | APIConnectionException | CardException | APIException e)
//		{
//			logger.error("Error:", e);
//		}
//
//		return null;
//	}

//	public Subscription reactivateApp(CategoryData company, String subscriptionId, MarketSubscriptionPlan appPlan, HttpServletRequest request)
//	{
//		try
//		{
//			Subscription subscription = Subscription.retrieve(subscriptionId);
//			String itemID = subscription.getSubscriptionItems().getData().get(0).getId();
//
//			Map<String, Object> item = new HashMap<String, Object>();
//			item.put("id", itemID);
//			item.put("plan", appPlan.getStripeId());
//
//			Map<String, Object> items = new HashMap<String, Object>();
//			items.put("0", item);
//
//			Map<String, Object> params = new HashMap<String, Object>();
//			params.put("items", items);
//
//			subscription = subscription.update(params);
//			return subscription;
//		}
//		catch (AuthenticationException | InvalidRequestException | APIConnectionException | CardException | APIException e)
//		{
//			logger.error("Error:", e);
//		}
//
//		return null;
//	}

//	public static void sendAppReactivateSuccessEmail(CategoryData company, MarketSubscriptionPlan appPlan, HttpServletRequest request)
//	{
//		String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");
//		String[] receiverEmails = getUserEmailsInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_USERS);
//
//		try
//		{
//			MultiOptions billingUsersList = ModuleAccess.getInstance().getMultiOptions(company, C_BILLING_USERS,
//					USER_MODULE);
//
//			if (billingUsersList.getValues().size() > 0)
//			{
//				receiverEmails = new String[billingUsersList.getValues().size()];
//				receiverEmails = billingUsersList.getValues().toArray(receiverEmails);
//			}
//		}
//		catch (SQLException e)
//		{
//			e.printStackTrace();
//		}
//
//		EmailBuilder builder = new EmailBuilder(request)
//				.addModule(USER_MODULE, company);
//		PendingEmail email = builder.addModule(MarketSubscriptionPlan.MODULE, appPlan.toMap())
//				.prepareAppReactivateSuccessEmail();
//
//		if (receiverEmails.length == 0)
//		{
//			email.addTo(adminEmail.split(","));
//		}
//		else
//		{
//			email.addTo(receiverEmails);
//			email.addBCC(adminEmail.split(","));
//		}
//		email.doSend();
//	}

//	public void processSubscriptionDeletedEvent(Subscription subscription)
//	{
//		String stripeSubscriptionId = subscription.getId();
//		MarketSubscriberDataAccess msda = new MarketSubscriberDataAccess();
//		msda.updateSubscriptionRecordByStripeId(stripeSubscriptionId, "CANCELLED");
//	}

//	public void processTrialWillEndEvent(Subscription subscription, HttpServletRequest request)
//	{
//		String stripeSubscriptionId = subscription.getId();
//
//		try
//		{
//			MarketSubscriberDataAccess msda = new MarketSubscriberDataAccess();
//
//			ElementData marketSubscriber = msda.getAppSubscription(stripeSubscriptionId);
//			CategoryData company = msda.getSubscriberCompany(stripeSubscriptionId);
//			MarketSubscriptionPlan marketAppPlan = MarketSubscriptionPlan.getMarketAppPlan(company.getString(C_ADDRESS_COUNTRY), marketSubscriber.getString(MarketSubscriberDataAccess.E_APP_ID));
//
//			double tax = StripeService.calcGstPercent(company);
//			double cost = marketAppPlan.getPrice();
//			if (tax > 0)
//			{
//				cost += tax * cost / 100;
//			}
//			marketAppPlan.setPrice(new BigDecimal(cost).setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue()); //update plugin price to include gst
//
//			String adminEmail = ModuleVariables.getString("qcloud-prescreen-email");
//			String[] receiverEmails = getUserEmailsInCompanyByRole(company.getId(), TUserRolesDataAccess.MANAGE_USERS);
//
//			try
//			{
//				MultiOptions billingUsersList = ModuleAccess.getInstance().getMultiOptions(company, C_BILLING_USERS,
//						USER_MODULE);
//
//				if (billingUsersList.getValues().size() > 0)
//				{
//					receiverEmails = new String[billingUsersList.getValues().size()];
//					receiverEmails = billingUsersList.getValues().toArray(receiverEmails);
//				}
//			}
//			catch (SQLException e)
//			{
//				e.printStackTrace();
//			}
//
//			String companyDateFormat = company.getString(C1_DATE_FORMAT);
//			companyDateFormat = StringUtils.isEmpty(companyDateFormat) ? "dd/MM/yyyy" : companyDateFormat;
//			SimpleDateFormat sdf = new SimpleDateFormat(companyDateFormat);
//			Map<String, String> additionalFields = new HashMap<String, String>();
//			additionalFields.put("trial_end_date", sdf.format(new Date(subscription.getTrialEnd() * 1000)));
//
//
//			EmailBuilder builder = new EmailBuilder(request)
//					.addModule(USER_MODULE, company);
//			builder.addModule("subscription", additionalFields);
//
//			PendingEmail email = builder.addModule(MarketSubscriptionPlan.MODULE, marketAppPlan.toMap())
//					.prepareAppTrialWillEndEmail();
//
//			if (receiverEmails.length == 0)
//			{
//				email.addTo(adminEmail.split(","));
//			}
//			else
//			{
//				email.addTo(receiverEmails);
//				email.addBCC(adminEmail.split(","));
//			}
//			email.doSend();
//		}
//		catch (Exception e)
//		{
//			logger.error("Error: ", e);
//		}
//	}

//	public void processSubscriptionUpdated(Subscription subscription, HttpServletRequest request)
//	{
//		boolean isMarketSubscription = subscription.getMetadata().size() > 0;
//		if (!isMarketSubscription)
//		{
//			return;
//		}
//		String stripeSubscriptionId = subscription.getId();
//
//		MarketSubscriberDataAccess msda = new MarketSubscriberDataAccess();
//		ElementData marketSubscriber = msda.getAppSubscription(stripeSubscriptionId);
//
//		if (marketSubscriber != null)
//		{
//			String subUpdateStatus = subscription.getStatus();
//			if (StringUtils.equals(subUpdateStatus, "active")
//					&& StringUtils.equals(marketSubscriber.getString(MarketSubscriberDataAccess.E_STATUS), SubscriptionStatus.TRIAL.toString()))
//			{
//				msda.updateSubscriptionRecordByStripeId(stripeSubscriptionId, SubscriptionStatus.SUBSCRIBED.toString());
//			}
//		}
//
//	}

//	public void processInvoiceCreated(Invoice invoice, ElementData invoiceCI, HttpServletRequest request) throws SQLException, AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException
//	{
//		CategoryData company = findCompanyFromStripeId(invoice.getCustomer());
//		if (SubscriptionPlan.isTrialPlan(company.getString(C_PRICING_PLAN)))
//		{
//			String subscriptionId = invoice.getSubscription();
//			Subscription subscription = retrieveStripeSubscription(subscriptionId);
//
//			boolean isMarketSubscription = subscription.getMetadata().size() > 0;
//
//			if (isMarketSubscription)
//			{
//				Map<String, Object> updateParams = new HashMap<String, Object>();
//				updateParams.put("closed", "true");
//				Map<String, String> metaData = new HashMap<String, String>();
//				metaData.put("remark", "Closed because QCloud account not upgraded");
//				updateParams.put("metadata", metaData);
//				invoice.update(updateParams);
//
////				unsubscribeApp(subscriptionId, request, true);
//
//				ElementData condition = new ElementData();
//				condition.setId(invoice.getId());
//				TransactionManager.getInstance().getDataAccess().delete("Elements_" + InvoiceDataAccess.MODULE, condition);
//
//				new MarketSubscriberDataAccess().updateSubscriptionRecordByStripeId(subscriptionId, SubscriptionStatus.CANCELLED.toString());
//
//			}
//		}
//	}

	public List<Invoice> getAllUnpaidInvoices(CategoryData customer)
	{
		List<Invoice> ret = new ArrayList<Invoice>();
		List<Invoice> invoices = getAllInvoices(customer);
		for (Invoice invoice : invoices)
		{
			if (invoice.getPaid())
			{
				continue;
			}
			ret.add(invoice);
		}
		return ret;

	}

	public List<Invoice> getAllInvoices(CategoryData customer)
	{
		try
		{
			Map<String, Object> invoiceParams = new HashMap<String, Object>();
			invoiceParams.put("customer", customer.getString(C1_STRIPE_ID));
			return Invoice.list(invoiceParams).getData();
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			throw new RuntimeException("Cannot list all invoices for " + customer.getName() + " (" + customer.getId() + ")");
		}
	}

	public static void main(String[] args)
	{
		try
		{
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("customer", "cus_AQLzV2AWaCorrb");
			params.put("limit", 100);
			SubscriptionCollection subscriptions = Subscription.list(params);
			if (subscriptions == null || subscriptions.getData() == null || subscriptions.getData().size() == 0)
			{
				System.out.println("No subscriptions found");
				return;
			}
			if (subscriptions.getData().size() > 1)
			{
				for (Subscription sub : subscriptions.getData())
				{
					//Imp Note: qcloud subscrption plan dont use metadata
					if (sub.getMetadata().size() == 0)
					{

						System.out.println(sub);
						return;
					}
				}
			}
			System.out.println(subscriptions.getData().get(0));
			return;
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return;
		}
	}
}
