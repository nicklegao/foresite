package au.corporateinteractive.qcloud.proposalbuilder.service;

import java.util.Hashtable;
import java.util.Vector;

import com.shopobot.util.URL;

import au.corporateinteractive.qcloud.proposalbuilder.exception.NoLongerAvaliableException;
import au.corporateinteractive.qcloud.proposalbuilder.model.media.VideoDetails;
import au.net.webdirector.common.Defaults;
import webdirector.db.client.ManagedDataAccess;

public class VideoLibraryService extends ManagedDataAccess
{
	public static final String VIDEOS_MODULE = "gen_video";

	public static final String E_HEADLINE = "ATTR_Headline";
	public static final String E_LINK = "ATTR_videoLink";
	public static final String E_CAPTION = "ATTR_caption";
	public static final String E_THUMBNAIL = "ATTRFILE_thumbnail";

	public VideoDetails loadVideoById(String element_id, String proposalId) throws NoLongerAvaliableException
	{
		if (!checkContentAccess(element_id, VIDEOS_MODULE, proposalId))
		{
			throw new NoLongerAvaliableException("images");
		}
		Vector<Hashtable<String, String>> data = cda.getElement(VIDEOS_MODULE, element_id);
		if (data.size() > 0)
		{
			Hashtable<String, String> imageData = data.get(0);
			String title = imageData.get(E_HEADLINE);
			String videoLink = imageData.get(E_LINK);
			String caption = imageData.get(E_CAPTION);


			String videoCode = null;
			String videoEmbed = null;
			String videoThumbnail = imageData.get("ATTRFILE_thumbnail");
			if (videoLink.contains("vimeo.com"))
			{
				videoCode = videoLink.substring(videoLink.lastIndexOf("/") + 1, videoLink.length());
				videoEmbed = "//player.vimeo.com/video/" + videoCode + "?title=0&amp;byline=0&amp;portrait=0";
			}
			else
			{
				videoCode = new URL(videoLink).getParameter("v", "");
				videoEmbed = "//www.youtube.com/embed/" + videoCode + "?rel=0&amp;showinfo=0";
			}

			if (videoThumbnail == null || videoThumbnail.equals(""))
			{
				videoThumbnail = "https://img.youtube.com/vi/" + videoLink.subSequence(videoLink.indexOf("v=") + 2, videoLink.length()) + "/0.jpg";
			}
			else
			{
				videoThumbnail = "/" + Defaults.getInstance().getStoreContext() + videoThumbnail;
			}

			return new VideoDetails(element_id, title, videoCode, caption, videoEmbed, videoThumbnail, videoLink);
		}

		throw new NoLongerAvaliableException("video");
	}

	public int addVideo(String videoCategory, String title, String caption, String link)
	{
		int catID = getOrCreateCategory(videoCategory, VIDEOS_MODULE);

		Hashtable<String, String> ht = standardCreateElementHashtable(catID, title);
		ht.put(E_CAPTION, caption);
		ht.put(E_LINK, link);

		return createElement(ht, VIDEOS_MODULE);
	}
}
