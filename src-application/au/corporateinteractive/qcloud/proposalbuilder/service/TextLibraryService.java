package au.corporateinteractive.qcloud.proposalbuilder.service;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.web.util.HtmlUtils;

import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.model.media.TextDetails;
import au.corporateinteractive.qcloud.proposalbuilder.utils.DateFormatUtils;
import au.corporateinteractive.utils.FreeMarkerUtils;
import au.net.webdirector.common.datalayer.client.CategoryData;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import webdirector.db.client.ManagedDataAccess;

public class TextLibraryService extends ManagedDataAccess
{
	public static final String TEXT_MODULE = "gen_text";

	private static Logger logger = Logger.getLogger(TextLibraryService.class);

	
	public TextDetails generateTextForView(String templateStr, Map<String, String> userMetadata, Map<String, String> proposalMetadata, Map<String, String> fieldMetadata) throws ParseException
	{
		String templatedTextBlock = "";
		Template template = null;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat csdf = new SimpleDateFormat("dd/MM/yyyy");

		HashMap<String, String> mergedMetadata = new HashMap<String, String>();
		for (Map<String, String> d : new Map[] { userMetadata, fieldMetadata, proposalMetadata })
		{
			if (d != null)
			{
				for (String k : d.keySet())
				{
					if ((k.equalsIgnoreCase("proposalCreate") || k.equalsIgnoreCase("proposalExpiry")) && ((d.get(k)).length() > 16) ) {
						String raw = d.get(k);
						Date rawDate = null;
						String date = "";
						try {
							rawDate = sdf.parse(raw);
							date = csdf.format(rawDate);
						} catch (ParseException ex) {
							ex.printStackTrace();
						}
						
						String key = k.equalsIgnoreCase("proposalCreate") ? "traveldocCreate": "traveldocExpiry";
						if (rawDate == null) {
							mergedMetadata.put(key, HtmlUtils.htmlEscape(raw));
						} else {
							mergedMetadata.put(key, HtmlUtils.htmlEscape(date));
						}
					} else {
						String key = k;
						switch (k) {
						case "consultantName":
							key = "agentFirstName";
							break;
						case "consultantPhone":
							key = "agentPhone";
							break;
						case "consultantFullname":
							key = "agentFullname";
							break;
						case "consultantEmail":
							key = "agentEmail";
							break;
						case "consultantCompanyName":
							key = "agentCompanyName";
							break;
						case "consultantId":
							key = "agentId";
							break;
						case "consultantSurname":
							key = "agentSurname";
							break;
						case "consultantTeamName":
							key = "agentTeamName";
							break;
						case "proposalExtraInput":
							key = "traveldocSubtitle";
							break;
						case "proposalTitleInput":
							key = "traveldocTitle";
							break;
						case "proposalCost":
							key = "bookingCost";
							break;
						case "proposalId":
							key = "traveldocId";
							break;
						case "proposalStatus":
							key = "traveldocStatus";
							break;
						case "proposalCreate":
							key = "traveldocCreate";
							break;
						case "proposalExpiry":
							key = "traveldocExpiry";
							break;
						case "contactAddress":
							key = "travellerAddress";
							break;
						case "contactEmail":
							key = "travellerEmail";
							break;
						case "contactFirstName":
							key = "travellerFirstName";
							break;
						case "contactLastName":
							key = "travellerLastName";
							break;

						default:
							break;
						}
						mergedMetadata.put(key, HtmlUtils.htmlEscape(d.get(k)));
					}
				}
			}
		}

		try
		{
			template = new Template("Template", new StringReader(templateStr), new Configuration());

			templatedTextBlock = FreeMarkerUtils.processTemplateIntoString(template, mergedMetadata);

		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (TemplateException e)
		{
			logger.error("Error parsing template:", e);
			templatedTextBlock = template.toString();
		}

		return new TextDetails("0", templatedTextBlock, templateStr, false);
	}

	public static Map<String, String> formatCustomFields(Map<String, String> customFields, CategoryData company)
	{
		Locale locale = Locale.getDefault();
		try
		{
			locale = Locale.forLanguageTag(company.getString(UserAccountDataAccess.C1_LOCALE));
		}
		catch (Exception e)
		{

		}
		TimeZone timeZone = TimeZone.getDefault();
		DateFormatUtils dateFormatUtils = DateFormatUtils.getInstance(company.getString(UserAccountDataAccess.C1_DATE_FORMAT), locale);
		dateFormatUtils.setTimeZone(timeZone);
		Map<String, String> map = new TreeMap<>();
		for (Entry<String, String> entry : customFields.entrySet())
		{
			try
			{
				Date d = new SimpleDateFormat("yyyy-MM-dd").parse(entry.getValue());
				map.put(entry.getKey(), dateFormatUtils.format(d));
			}
			catch (Exception e)
			{
				map.put(entry.getKey(), entry.getValue());
			}
		}
		return map;
	}
}
