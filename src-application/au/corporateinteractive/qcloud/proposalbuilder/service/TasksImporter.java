/**
 * @author Nick Yiming Gao
 * @date 21/04/2017 11:47:55 am
 */
package au.corporateinteractive.qcloud.proposalbuilder.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.BatchInserter;
import au.net.webdirector.common.datalayer.base.db.BatchUpdater;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
import au.net.webdirector.common.process.ImportUtils;
import au.net.webdirector.common.process.ProcessStatus;
import au.net.webdirector.common.process.Processor;
import au.net.webdirector.common.process.Singleton;
import au.net.webdirector.common.utils.StreamGobbler;

@Singleton
public class TasksImporter implements Processor
{

	private static Logger logger = Logger.getLogger(TasksImporter.class);

	public class DataImportException extends Exception
	{

		public static final int MISSING_HEADER = 1;

		int code = 0;

		public DataImportException(int code, String message)
		{
			super(message);
			this.code = code;
		}

		public DataImportException(int code, String message, Throwable cause)
		{
			super(message, cause);
			this.code = code;
		}

		public int getCode()
		{
			return code;
		}

	}

	static String NAME = "elements_tasks";

	public static Map<String, String> getColumnFieldMappingForExport()
	{
		return getColumnFieldMapping();
	}

	protected Map<String, String> columnFieldMapping;

	protected Map<String, Integer> columnFields;

	protected String folderId;

	protected boolean deleteAll;
	protected boolean overWrite;

	protected String displayName = "Data File Import";

	protected Map<String, String> fieldColumnMapping;

	protected File file;

	protected String id;

	protected boolean ignoreFirstLine = true;

	protected ProcessStatus status = new ProcessStatus(ProcessStatus.NOT_STARTED, "Validating", 0, 0, false, 0, 0, "");

	protected String projectId;

	public TasksImporter(File file, boolean ignoreFirstLine, boolean deleteAll, boolean overWrite, String folderId, String projectId) throws SQLException
	{
		this.file = file;
		this.ignoreFirstLine = ignoreFirstLine;
		this.deleteAll = deleteAll;
		this.overWrite = overWrite;
		this.folderId = folderId;
		this.columnFieldMapping = getColumnFieldMapping();
		this.fieldColumnMapping = getFieldColumnMapping(this.columnFieldMapping);
		this.projectId = projectId;
	}

	/**
	 * @param data
	 */
	protected void beforeInsertProcess(DataMap data)
	{
		data.put("status_id", "1");
		data.put("lock_id", "1");
		data.put("version", "1");
		data.put("language_id", "1");
		data.put("live", "1");
		data.put("ATTR_status", "Draft");
		data.put("ATTR_ProjectId", projectId);
		data.put("ATTR_Key", new SecurityKeyService().generateKey());
		data.put("ATTRDATE_StartDate", parseDate(data.getString("ATTRDATE_StartDate")));
		data.put("ATTRDATE_EndDate", parseDate(data.getString("ATTRDATE_EndDate")));
	}

	protected DataMap convertDataMap(CSVRecord record, Map<String, Integer> columnFields)
	{
		if (record.size() != this.fieldColumnMapping.size())
		{
			throw new InvalidParameterException("Unexpected number of columns: " + record.size() + " instead of " + fieldColumnMapping.size() + "<br/> Please check the file has columns: <br/>" + StringUtils.join(fieldColumnMapping.values(), ",<br/>"));
		}
		DataMap d = new DataMap();
		d.setTable(getTableName());
		for (Entry<String, Integer> field : columnFields.entrySet())
		{
			String value = record.get(field.getValue());
			d.put(field.getKey(), value);
		}
		return d;
	}

	protected String deleteRest(String startTime) throws SQLException
	{
		// delete un-updated
		QueryBuffer sql = new QueryBuffer();
		sql.append("delete from elements_tasks where category_id = ? ", folderId);
		sql.append(" and last_update_date < ?", startTime);
		int deleted = TransactionDataAccess.getInstance().update(sql);

		return deleted > 0 ? deleted + " task(s) have been deleted" : "";
	}

	protected static Map<String, String> getColumnFieldMapping()
	{

		Map<String, String> stub = new LinkedHashMap<>();
//		stub.put("Contact ID (QuoteCloud internal use)", "element_id");
		stub.put("ID", "attr_externalid");
		stub.put("Name", "attr_headline");
		stub.put("Duration", "attr_duration");
		stub.put("Start_Date", "attrdate_startdate");
		stub.put("Finish_Date", "attrdate_enddate");
		stub.put("Resource_Names", "attr_resourcenames");
		stub.put("Predecessors ID", "attr_predecessors");
		stub.put("Parent ID", "attr_parentid");
		return stub;
	}

	protected CSVParser getCSVParser(Reader reader) throws IOException
	{
		return new CSVParser(reader, CSVFormat.EXCEL);
	}

	protected Map<String, String> getFieldColumnMapping(Map<String, String> columnFieldMapping)
	{
		Map<String, String> mapping = new LinkedHashMap<String, String>();
		for (Entry<String, String> entry : columnFieldMapping.entrySet())
		{
			mapping.put(entry.getValue(), entry.getKey());
		}
		return mapping;
	}

	@Override
	public ProcessStatus getStatus()
	{
		return this.status;
	}

	protected String getTableName()
	{
		return NAME;
	}

	protected void importing() throws Exception
	{
		status.setStage("Importing Data");
		List<String> errors = new ArrayList<>();

		String startTime = TransactionDataAccess.getInstance().selectString("select current_timestamp()");

		TransactionDataAccess da = TransactionDataAccess.getInstance(false);
		String deletedMsg = "";
		String deduplicateMsg = "";
		try (BatchInserter inserter = new BatchInserter(100, da);
				BatchUpdater updater = new BatchUpdater(100, da);
				FileInputStream fis = new FileInputStream(file);
				final Reader reader = new InputStreamReader(fis, "UTF-8");
				CSVParser parser = getCSVParser(reader))
		{
			List<CSVRecord> records = parser.getRecords();
			status.setTotal(ignoreFirstLine ? records.size() - 1 : records.size());
			status.setProcessed(0);
			status.setSucceed(0);
			status.setFailed(0);
			for (final CSVRecord r : records)
			{
				long recordNumber = r.getRecordNumber();
				if (ignoreFirstLine && recordNumber == 1)
				{
					continue;
				}
				status.processedPlusPlus();
				DataMap d = convertDataMap(r, this.columnFields);
				restrictToThisCompany(d);
				beforeInsertProcess(d);
				try
				{
					if (StringUtils.isBlank(d.getString("element_id")))
					{
						inserter.insert(d, getTableName());
					}
					else
					{
						updater.update(d, getTableName());
					}

					status.succeedPlusPlus();
				}
				catch (Exception e)
				{
					logger.error("Error:", e);
					errors.add("Cannot save line: " + recordNumber + ", Error:" + e.getMessage());
					status.setFailed(status.getTotal());
					status.setSucceed(0);
					status.setProcessed(status.getTotal());
					throw e;
				}

			}
			inserter.close();
			updater.close();
			da.commit();
			if (deleteAll)
			{
				deletedMsg = deleteRest(startTime);
			}
			TransactionDataAccess.getInstance().update("update elements_tasks set category_id = ? where category_id = ?", folderId, folderId);

		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			da.rollback();
		}

		String message = status.getSucceed() + " task(s) have been processed successfully!";
		message += StringUtils.isBlank(deduplicateMsg) ? "" : "<br/>" + deduplicateMsg;
		message += StringUtils.isBlank(deletedMsg) ? "" : "<br/>" + deletedMsg;
		if (status.getTotal() > status.getSucceed())
		{
			status.setSuccess(false);
			message += "<br/>" + (status.getTotal() - status.getSucceed()) + " task(s) failed to be processed! ";
			message += "<br/>" + ImportUtils.joinMessage(errors, 1);
		}
		else
		{
			status.setSuccess(true);
		}
		status.setMessage(message);
	}


	@Override
	public void process() throws Exception
	{
		status.setStatus(ProcessStatus.RUNNING);
		try
		{
			List<String> errors = new ArrayList<>();
			try
			{
				this.columnFields = readColumnFields(this.columnFieldMapping);
				errors = validate();
			}
			catch (Exception e)
			{
				errors.add("<p>" + e.getMessage() + "</p>");
			}
			if (errors.size() > 0)
			{
				status.setMessage(ImportUtils.joinMessage(errors, 1));
				throw new RuntimeException("Validation failed.");
			}
			importing();
			status.setStatus(ProcessStatus.FINISHED);
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			status.setStatus(ProcessStatus.STOPPED);
			throw e;
		}
		finally
		{
			//saveHistory();
		}

	}

	protected Map<String, Integer> readColumnFields(Map<String, String> mapping) throws IOException, DataImportException
	{
		FileInputStream fis = new FileInputStream(file);
		final Reader reader = new InputStreamReader(fis, "UTF-8");
		CSVParser parser = getCSVParser(reader);
		Map<String, Integer> fields = new LinkedHashMap<>();
		CSVRecord record = parser.getRecords().get(0);
		for (int i = 0; i < record.size(); i++)
		{
			String column = record.get(i);
			if (mapping.containsKey(column))
			{
				fields.put(mapping.get(column), i);
			}
		}
		if (fields.size() == 0)
		{
			parser.close();
			throw new DataImportException(DataImportException.MISSING_HEADER, "No valid header found in this csv file.");
		}
		parser.close();
		return fields;
	}


	protected boolean resizeImage(StoreFile file, String size)
	{
		File originalFile = file.getUploadedFile();

		// make thumb and all parent dirs

		int exitVal = -1;
		try
		{
			String[] cmd = new String[5];

			logger.info("Operating System: : " + System.getProperty("os.name"));

			cmd[0] = new File(Defaults.getInstance().getImageMagickDirectory(), "convert").getAbsolutePath();
			cmd[1] = originalFile.getAbsolutePath();
			cmd[2] = "-resize";
			cmd[3] = size;
			cmd[4] = originalFile.getAbsolutePath();

			Runtime rt = Runtime.getRuntime();
			for (int i = 0; i < cmd.length; i++)
			{
				logger.info(String.format("EXECUTING (%s/%s): %s", i, cmd.length, cmd[i]));
			}

			Process proc = rt.exec(cmd);
			// any error message?
			StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "ERROR");

			// any output?
			StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "OUTPUT");

			// kick them off
			errorGobbler.start();
			outputGobbler.start();

			// any error???
			exitVal = proc.waitFor();
			logger.info("ExitValue: " + exitVal);
		}
		catch (Throwable t)
		{
			logger.fatal("OH NO!", t);
		}

		if (exitVal == 0)
			return true;
		else
			return false;
	}

	/**
	 *
	 */
	protected void saveHistory()
	{
		ProcessStatus status = getStatus();
		TransactionDataAccess da = TransactionDataAccess.getInstance();
		QueryBuffer sql = new QueryBuffer();
		sql.append("insert into sys_import_history (PROC_ID, PROC_NAME, S_STATUS, S_STAGE, I_TOTAL, I_PROCESSED, I_IS_SUCCESS, I_SUCCEED, I_FAILED, D_START, D_ENDED, S_MESSAGE) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", id, displayName, status.getStatus(), status.getStage(), status.getTotal(), status.getProcessed(), status.isSuccess() ? 1 : 0, status.getSucceed(), status.getFailed(), status.getStart(), new Date(), status.getMessage());
		try
		{
			da.update(sql);
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
		}
	}

	@Override
	public void setId(String id)
	{
		this.id = id;
	}

	protected List<String> validate()
	{
		status.setStage("Validating");
		List<String> messages = new ArrayList<>();
		try (FileInputStream fis = new FileInputStream(file); final Reader reader = new InputStreamReader(fis, "UTF-8"))
		{
			long recordNumber = 0;
			try (CSVParser parser = getCSVParser(reader))
			{
				List<CSVRecord> records = parser.getRecords();
				status.setTotal(ignoreFirstLine ? records.size() - 1 : records.size());
				status.setProcessed(0);
				status.setSucceed(0);
				status.setFailed(0);
				for (final CSVRecord r : records)
				{
					recordNumber = r.getRecordNumber();
					if (ignoreFirstLine && recordNumber == 1)
					{
						continue;
					}
					status.processedPlusPlus();
					status.succeedPlusPlus();
					DataMap d = convertDataMap(r, this.columnFields);
					String msg = validate(d, this.fieldColumnMapping);
					if (StringUtils.isNotBlank(msg))
					{
						messages.add(ImportUtils.niceErrors4Row(recordNumber, msg));
					}

				}
			}
			catch (Exception e)
			{
				throw new RuntimeException("Cannot parse uploaded file: Line " + recordNumber + " " + e.getMessage(), e);
			}

		}
		catch (Exception e)
		{
			messages.add(e.getMessage());
		}
		return messages;
	}

	protected String validate(DataMap product, Map<String, String> fieldColumnMapping)
	{
		List<String> errors = new ArrayList<>();
		for (Entry<String, Object> entry : product.entrySet())
		{
			String key = entry.getKey();
			Object v = entry.getValue();
			if (!(v instanceof String))
			{
				continue;
			}
			String value = (String) v;

			if (StringUtils.equalsIgnoreCase(key, "attr_headline")
					&& value.length() > 500)
			{
				errors.add("Length of '" + fieldColumnMapping.get(key) + "' must be less than 500");
			}
			if (StringUtils.equalsIgnoreCase(key, "ATTR_ResourceNames")
					&& value.length() > 500)
			{
				errors.add("Length of '" + fieldColumnMapping.get(key) + "' must be less than 500");
			}
			if (StringUtils.equalsIgnoreCase(key, "ATTR_Predecessors")
					&& value.length() > 100)
			{
				errors.add("Length of '" + fieldColumnMapping.get(key) + "' must be less than 100");
			}
			if (StringUtils.equalsIgnoreCase(key, "attr_externalid")
					&& value.length() > 50)
			{
				errors.add("Length of '" + fieldColumnMapping.get(key) + "' must be less than 50");
			}
			if (StringUtils.equalsIgnoreCase(key, "attr_duration")
					&& value.length() > 50)
			{
				errors.add("Length of '" + fieldColumnMapping.get(key) + "' must be less than 50");
			}
			if (StringUtils.startsWithIgnoreCase(key, "ATTRDATE_") && !isValidDate(value))
			{
				errors.add("'" + fieldColumnMapping.get(key) + "' must be in the format 'dd MMMM yyyy K:mm a'");
			}
		}
		return ImportUtils.niceErrors(errors);
	}

	private Date parseDate(String value)
	{
		try
		{
			return new SimpleDateFormat("dd MMMM yyyy K:mm a").parse(value);
		}
		catch (Exception e)
		{
			return null;
		}
	}

	private boolean isValidDate(String value)
	{
		return parseDate(value) != null;
	}

	private void restrictToThisCompany(DataMap contact) throws SQLException
	{
		DataMap data = TransactionDataAccess.getInstance().selectMap("select e.element_id from elements_tasks e where e.attr_externalid = ? and e.category_id = ?", contact.getString("attr_externalid"), folderId);
		if (data != null && !overWrite)
		{
			contact.clear(); // don't overwrite values but still need to update the last update time.
			contact.put("element_id", data.getString("element_id"));
		}
		else if (data != null && StringUtils.isNotBlank(data.getString("element_id")) && overWrite)
		{
			contact.put("element_id", data.getString("element_id"));
		}
		else
		{
			contact.remove("element_id");
		}
		contact.put("category_id", folderId);

	}
}
