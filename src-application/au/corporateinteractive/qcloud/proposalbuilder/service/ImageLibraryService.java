package au.corporateinteractive.qcloud.proposalbuilder.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import au.corporateinteractive.qcloud.proposalbuilder.exception.NoLongerAvaliableException;
import au.corporateinteractive.qcloud.proposalbuilder.model.media.ImageDetails;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.utils.StreamGobbler;
import webdirector.db.client.ManagedDataAccess;


public class ImageLibraryService extends ManagedDataAccess
{
	private Logger logger = Logger.getLogger(ImageLibraryService.class);

	public static final String IMAGES_MODULE = "gen_images";

	public static final String COVER_PAGES_CATEGORY = "Cover Pages";

	public static final String COVER_PAGES_MODULE = "GEN_COVERPAGES";

	public static final String E_IMAGE_FILE = "ATTRFILE_image";
	public static final String E_DESCRIPTION = "ATTRLONG_description";
	public static final String E_DISPLAY_NAME = "ATTRCHECK_displayName";


	public static final int MAX_IMAGE_WIDTH_FOR_PDF_LANDSCAPE = 929;
	public static final int MAX_IMAGE_WIDTH_FOR_PREVIEW_LANDSCAPE = 962;
	public static final int MAX_IMAGE_WIDTH_FOR_PDF = 729;
	public static final int MAX_IMAGE_WIDTH_FOR_PREVIEW = 762;
	public static final int HALF_PAGE_WIDTH = MAX_IMAGE_WIDTH_FOR_PDF / 2;

	public ImageDetails loadImageById(String element_id, String proposalId) throws NoLongerAvaliableException
	{
		if (!checkContentAccess(element_id, IMAGES_MODULE, proposalId))
		{
			throw new NoLongerAvaliableException("images");
		}
		Vector<Hashtable<String, String>> data = cda.getElement(IMAGES_MODULE, element_id, false);
		if (data.size() > 0)
		{
			Hashtable<String, String> imageData = data.get(0);
			String title = imageData.get(E_HEADLINE);
			String imagePath = imageData.get(E_IMAGE_FILE);
			boolean displayTitle = StringUtils.equalsIgnoreCase("1", imageData.get(E_DISPLAY_NAME));
			String description = "";
			try
			{
				Path descriptionTextfile = Paths.get(
						Defaults.getInstance().getStoreDir(),
						IMAGES_MODULE,
						element_id,
						E_DESCRIPTION,
						E_DESCRIPTION + ".txt");
				description = new String(Files.readAllBytes(descriptionTextfile));
				description = Jsoup.clean(description, Whitelist.basic());
			}
			catch (IOException e)
			{
				logger.error(e.getMessage());
			}

			return new ImageDetails(element_id, title, imagePath, "", description, displayTitle);
		}

		throw new NoLongerAvaliableException("image");
	}

	public void addImage(String folderName, File image)
	{
		int categoryId = getOrCreateCategory(folderName, IMAGES_MODULE);
		String imageFileName = image.getName();

		Hashtable<String, String> ht = standardCreateElementHashtable(categoryId, imageFileName);
		int elementId = createElement(ht, IMAGES_MODULE);

		// \proposals\12\ATTRTEXT_rawData\ATTRTEXT_rawData.txt
		String storePath = String.format("/%s/%s/%s/%s",
				IMAGES_MODULE,
				elementId,
				E_IMAGE_FILE,
				imageFileName);
		String thumbPath = String.format("/%s/%s/%s/_thumb/%s",
				IMAGES_MODULE,
				elementId,
				E_IMAGE_FILE,
				imageFileName);

		compressAndMove(image, "560x560",
				new File(Defaults.getInstance().getStoreDir(), storePath));

		compressAndMove(image, "120x120",
				new File(Defaults.getInstance().getStoreDir(), thumbPath));

		// Update
		insertNotNull(ht, E_ID, elementId);
		insertNotNull(ht, E_IMAGE_FILE, storePath);

		updateElement(ht, IMAGES_MODULE);
	}

	public List<LinkedHashMap<String, String>> getCoverpageThumbs(String companyId) throws SQLException
	{
		return getCoverpageThumbs(companyId, false);
	}

	public List<LinkedHashMap<String, String>> getCoverpageThumbs(String companyId, boolean medium) throws SQLException
	{
		String store = "/" + Defaults.getInstance().getStoreContext();
		ModuleAccess ma = ModuleAccess.getInstance();
		CategoryData condition = new CategoryData();
		condition.put("attrdrop_companyid", companyId);
		CategoryData catCompany = ma.getCategory(COVER_PAGES_MODULE, condition);
		ElementData conditions = new ElementData();
		conditions.setParentId(catCompany.getId());
		conditions.setLive(true);
		List<ElementData> coverPages = ma.getElements(COVER_PAGES_MODULE, conditions);
		List<LinkedHashMap<String, String>> matches = new ArrayList<>();
		String size = medium ? "/_medium/" : "/_thumb/";
		for (ElementData ht : coverPages)
		{
			LinkedHashMap<String, String> info = new LinkedHashMap<String, String>();
			String storeImagePath = ht.getString(E_IMAGE_FILE);
			String storeThumbPath = storeImagePath.replace("/" + E_IMAGE_FILE + "/", "/" + E_IMAGE_FILE + size);
			if (medium)
			{
				boolean exists = (new File(Defaults.getInstance().getStoreDir() + storeThumbPath)).exists();
				if (!exists)
				{
					storeThumbPath = storeImagePath.replace("/" + E_IMAGE_FILE + "/", "/" + E_IMAGE_FILE + "/_thumb/");
				}
			}

			info.put("id", ht.getId());
			info.put("file", store + storeThumbPath);
			info.put("name", ht.getName());
			matches.add(info);
		}
		return matches;
	}

	public String getCoverImagePathForCoverPageWithID(String coverID)
	{
		Vector<Hashtable<String, String>> coverPages = cda.getElement(COVER_PAGES_MODULE, coverID);
		if (coverPages.size() > 0)
		{
			Hashtable<String, String> ht = coverPages.get(0);
			return "/" + Defaults.getInstance().getStoreContext() + ht.get(E_IMAGE_FILE);
		}
		return "/application/images/defaultCover.jpg";
	}

	public void compressAndMove(File src, String size, File to)
	{
		Defaults d = Defaults.getInstance();
		try
		{
			to.getParentFile().mkdirs();

			String osName = System.getProperty("os.name");
			String[] cmd = new String[5];

			cmd[0] = new File(Defaults.getInstance().getImageMagickDirectory(), "convert").getAbsolutePath();
			cmd[1] = src.getAbsolutePath();
			cmd[2] = "-resize";
			cmd[3] = size;
			cmd[4] = to.getAbsolutePath();

			Runtime rt = Runtime.getRuntime();
			Process proc = rt.exec(cmd);

			StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "ERROR");
			StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "OUTPUT");

			// kick them off
			errorGobbler.start();
			outputGobbler.start();

			// any error???
			int exitVal = proc.waitFor();

			logger.info("ExitValue: " + exitVal);
		}
		catch (Throwable t)
		{
			t.printStackTrace();
		}

	}


}
