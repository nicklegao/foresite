package au.corporateinteractive.qcloud.proposalbuilder.service;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.web.util.HtmlUtils;

import webdirector.db.client.ClientFileUtils;
import webdirector.db.client.ManagedDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.exception.NoLongerAvaliableException;
import au.corporateinteractive.qcloud.proposalbuilder.model.media.ImageDetails;
import au.corporateinteractive.qcloud.proposalbuilder.model.media.PDFDetails;
import au.corporateinteractive.qcloud.proposalbuilder.model.media.TextDetails;
import au.corporateinteractive.utils.FreeMarkerUtils;
import au.net.webdirector.common.Defaults;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class PDFLibraryService extends ManagedDataAccess
{
	public static final String PDF_MODULE = "GEN_PDF"; 
	public static final String PDF_DESCRIPTION = "ATTRLONG_description";
	public static final String PDF_FILE = "ATTRFILE_attached_file"; 
	public static final String PDF_NAME = "attr_headline"; 

	private static Logger logger = Logger.getLogger(PDFLibraryService.class);

	public PDFDetails loadPDFById(String element_id, String proposalId) throws NoLongerAvaliableException
	{
		if (!checkContentAccess(element_id, PDF_MODULE, proposalId))
		{
			throw new NoLongerAvaliableException("PDF");
		}
		Vector<Hashtable<String, String>> data = cda.getElement(PDF_MODULE, element_id);
		if (data.size() > 0)
		{
			Hashtable<String, String> pdfData = data.get(0);
			String title = pdfData.get(E_HEADLINE);
			String pdfPath = pdfData.get(PDF_FILE);
			boolean displayTitle = StringUtils.equalsIgnoreCase("1", pdfData.get(PDF_NAME));
			String description = "";
			try
			{
				Path descriptionTextfile = Paths.get(
						Defaults.getInstance().getStoreDir(),
						PDF_MODULE,
						element_id,
						PDF_DESCRIPTION,
						PDF_DESCRIPTION + ".txt");
				description = new String(Files.readAllBytes(descriptionTextfile));
				description = Jsoup.clean(description, Whitelist.basic());
			}
			catch (IOException e)
			{
				logger.error(e.getMessage());
			}

			return new PDFDetails(element_id, title, pdfPath, description);
		}

		throw new NoLongerAvaliableException("PDF");
	}
}