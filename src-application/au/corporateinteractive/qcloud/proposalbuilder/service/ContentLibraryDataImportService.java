package au.corporateinteractive.qcloud.proposalbuilder.service;

import java.io.File;
import java.io.FileFilter;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import webdirector.db.client.ManagedDataAccess;

public class ContentLibraryDataImportService extends ManagedDataAccess
{
	Logger logger = Logger.getLogger(ContentLibraryDataImportService.class);

	public boolean importImagesFromDirectory(String directory)
	{
		String folderName = FilenameUtils.getBaseName(directory);
		ImageLibraryService imageLibraryService = new ImageLibraryService();
		for (File image : new File(directory).listFiles(new ImageFileFilter()))
		{
			imageLibraryService.addImage(folderName, image);
		}

		return false;
	}

	public class ImageFileFilter implements FileFilter
	{

		public boolean accept(File pathname)
		{
			String extension = FilenameUtils.getExtension(pathname.getAbsolutePath());
			if (extension != null)
				return extension.equalsIgnoreCase("jpg")
						|| extension.equalsIgnoreCase("png")
						|| extension.equalsIgnoreCase("bmp");
			else
				return false;
		}
	}
}
