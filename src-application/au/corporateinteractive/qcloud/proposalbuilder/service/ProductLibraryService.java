package au.corporateinteractive.qcloud.proposalbuilder.service;

import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.exception.NoLongerAvaliableException;
import au.corporateinteractive.qcloud.proposalbuilder.model.media.ProductDetails;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import org.apache.log4j.Logger;
import webdirector.db.client.ManagedDataAccess;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

public class ProductLibraryService extends ManagedDataAccess
{
	private static Logger logger = Logger.getLogger(ProductLibraryService.class);

	public static final String PRODUCTS_MODULE = "PRODUCTS";

	public static final String E_MODEL = "ATTR_ModelNumber ";
	public static final String E_DESCRIPTION = "ATTR_description ";
	public static final String E_PRICE = "ATTRCURRENCY_Price";
	public static final String E_IMAGE_FILE = "ATTRFILE_image";
	public static final String E_TERM = "ATTR_term";
	public static final String E_ONE_OFF = "ATTRCHECK_oneOff";
	public static final String E_NOTE = "ATTR_note";

	public ProductDetails loadProductById(String element_id, String proposalId) throws NoLongerAvaliableException
	{
		if (!checkContentAccess(element_id, PRODUCTS_MODULE, proposalId))
		{
			throw new NoLongerAvaliableException("images");
		}
		Vector<Hashtable<String, String>> data = cda.getElement(PRODUCTS_MODULE, element_id);
		if (data.size() > 0)
		{
			Hashtable<String, String> productData = data.get(0);
			String title = productData.get(E_HEADLINE);
			String model = productData.get(E_MODEL);
			String description = productData.get(E_DESCRIPTION);
			float price = Float.parseFloat(productData.get(E_PRICE));
			String path = productData.get(E_IMAGE_FILE);

			return new ProductDetails(element_id, title, model, description, price, path);
		}

		throw new NoLongerAvaliableException("product");
	}

	// returns 0 if not found.
	public int doesProductNameExistForUser(String userId, String productName)
	{
		// This method is based on the internal-name of products being unique - we have to have a contract for Zapier to work.
		int elementId = 0;

		ModuleAccess ma = ModuleAccess.getInstance();
		CategoryData company = new UserAccountDataAccess().getUserCompanyForUserId(userId);
		String companyId = company.getId();

		String sql = "SELECT e.* FROM elements_"+PRODUCTS_MODULE+" e, categories_"+PRODUCTS_MODULE+
				" c1, categories_"+PRODUCTS_MODULE+" c2 where e.Category_id = c2.Category_id and c2.Category_ParentID = c1.Category_id and"+
				" c1.ATTRDROP_CompanyID = ? and attr_headline = ? ";

		try {
			List<ElementData> productItems = ma.getElements(sql, new String[]{companyId, productName});
			elementId = Integer.valueOf(productItems.get(0).getId());
		}
		catch (Exception e) {
			logger.error("Bargle ... "+e.toString());
		}

		return elementId;
	}

	public boolean addOrUpdateProduct(String folderName, int element_id, String shortName, String  lineItemName, String price,
		  boolean contractPricing, int term, String imageUrl, String descriptionNote, String userId)
	{
		ModuleAccess ma = new ModuleAccess();
		UserAccountDataAccess uaDA = new UserAccountDataAccess();
		CategoryData userCompany = uaDA.getUserCompanyForUserId(userId);
		int usersCompanyID = Integer.valueOf(userCompany.getId());

		int productParentFolderId = 0;
		try {
			CategoryData productParentCat = ma.getCategoryByField(PRODUCTS_MODULE, "ATTRDROP_CompanyID", usersCompanyID);
			productParentFolderId = Integer.valueOf(productParentCat.getId());
		}
		catch (Exception e) {
			logger.debug("ARGH "+e.toString());
		}

		// knowing the users ID and knowing ATTRDROP_CompanyID in the products module will hold this value - find the category_id
		boolean processedOk = false;
		if (element_id > 0)
		{
			processedOk = updateExistingProduct(folderName, element_id, shortName, lineItemName, price, contractPricing, term, imageUrl, descriptionNote, productParentFolderId);
		}
		else
		{
			// we're adding a new product as no element_id supplied
			processedOk = addNewProduct(folderName, shortName, lineItemName, price, contractPricing, term, imageUrl, descriptionNote, productParentFolderId);
		}
		return processedOk;
	}

	public boolean addNewProduct(String folderName, String shortName, String lineItemName, String price,
		boolean contractPricing, int term, String imageUrl, String descriptionNote, int parentId)
	{
		int catID = getOrCreateCategory(folderName, parentId, 2, PRODUCTS_MODULE);
		Hashtable<String, String> ht = standardCreateElementHashtable(catID, shortName);

		ht.put(E_DESCRIPTION, lineItemName);
		ht.put(E_PRICE, price);
		ht.put(E_ONE_OFF, contractPricing == true ? "1" : "0");
		ht.put(E_TERM, Integer.toString(term));
		ht.put(E_IMAGE_FILE, imageUrl);
		ht.put(E_NOTE, descriptionNote);

		int ID = createElement(ht, PRODUCTS_MODULE);

		return ID != 0 ? true : false;
	}

	public boolean updateExistingProduct(String folderName, int element_id, String shortName, String  lineItemName, String price,
		boolean contractPricing, int term, String imageUrl, String descriptionNote, int parentId)
	{
		int catID = getOrCreateCategory(folderName, parentId, 2, PRODUCTS_MODULE);
		Hashtable<String, String> ht = standardCreateElementHashtable(catID, shortName);

		ht.put("Element_id", String.valueOf(element_id));
		ht.put(E_DESCRIPTION, lineItemName);
		ht.put(E_PRICE, price);
		ht.put(E_ONE_OFF, contractPricing == true ? "1" : "0");
		ht.put(E_TERM, Integer.toString(term));
		ht.put(E_IMAGE_FILE, imageUrl);
		ht.put(E_NOTE, descriptionNote);

		int updatedCount = updateElement(ht, PRODUCTS_MODULE);

		return updatedCount != 0 ? true : false;
	}

}