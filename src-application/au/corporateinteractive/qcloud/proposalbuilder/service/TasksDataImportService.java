package au.corporateinteractive.qcloud.proposalbuilder.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.csvreader.CsvWriter;
import com.oreilly.servlet.MultipartRequest;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.ProjectsDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.TasksDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.utils.AjaxResponse;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.process.ProcessManager;

public class TasksDataImportService
{
	Logger logger = Logger.getLogger(TasksDataImportService.class);

	public void tasksExport(HttpServletRequest request, HttpServletResponse response, HttpSession session, String row) throws IOException, SQLException
	{
		CategoryData company = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR));
		String projectId = request.getParameter("projectId");
		int folderId = new TasksDataAccess().getOrCreateCategory(company.getId(), company.getName(), TasksDataAccess.MODULE);

		QueryBuffer sql = new QueryBuffer();

		Map<String, String> columnFieldMapping = TasksImporter.getColumnFieldMappingForExport();
		String contactsCols = StringUtils.join(columnFieldMapping.values(), ",");

		sql.append("select " + contactsCols + " from elements_tasks c where c.category_id = ? ", folderId);
		sql.append(" and attr_projectid = ? ", projectId);
		if (StringUtils.isNotBlank(row))
		{
			sql.append(" limit " + row);
		}

		List<DataMap> contacts = TransactionDataAccess.getInstance().selectMaps(sql);

		response.setContentType("text/csv");

		//set a cokie because of "jquery.fileDownload.js"	
		Cookie c = new Cookie("fileDownload", "true");
		c.setMaxAge(-1);
		c.setPath("/");
		response.addCookie(c);

		response.addHeader("Content-disposition", String.format("attachment;filename=contact_export_%s.csv", new Date().getTime()));
		CsvWriter writer = new CsvWriter(new BufferedOutputStream(response.getOutputStream()), ',', Charset.defaultCharset());
		writer.writeRecord(columnFieldMapping.keySet().toArray(new String[] {}));

		List<ElementData> projects = new ProjectsDataAccess().list(company);

		for (DataMap record : contacts)
		{
			String[] data = record.getValues(columnFieldMapping.values().toArray(new String[] {}));
			writer.writeRecord(data);
		}
		writer.close();

	}

	public AjaxResponse tasksImport(HttpServletRequest request, HttpSession session) throws IOException, SQLException
	{
		MultipartRequest r = new MultipartRequest(request, System.getProperty("java.io.tmpdir"), Constants.MAX_ATTACHMENT_SIZE);

		boolean deleteAll = "on".equals(r.getParameter("deleteAll"));
		boolean overWrite = true;

		CategoryData company = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR));
		String projectId = r.getParameter("projectId");
		if ("0".equals(projectId))
		{
			String projectName = r.getParameter("projectName");
			projectId = new ProjectsDataAccess().saveProject(projectName, company).getId();
		}
		TasksDataAccess tda = new TasksDataAccess();
		int companyFolderId = tda.getOrCreateCategory(company.getId(), company.getName(), TasksDataAccess.MODULE);
		int folderId = tda.getOrCreateCategory(projectId, companyFolderId, 2, TasksDataAccess.MODULE);

		File csvUpload = r.getFile("csvUpload");
		if (csvUpload == null || !csvUpload.exists())
		{
			return new AjaxResponse(false, "Uploaded file not found!");
		}

		TasksImporter importer = new TasksImporter(csvUpload, true, deleteAll, overWrite, String.valueOf(folderId), projectId);
		ProcessManager.getInstance().runProcessor(importer, company.getId());

		return new AjaxResponse(true, "Tasks are being imported.");

	}

	/**
	 * @param errors
	 * @return
	 */
	public static String joinMessage(List<String> errors, int max)
	{
		if (errors.size() < max)
			return StringUtils.join(errors, "<br/>");
		List<String> sub = errors.subList(0, max);
		sub.add("And " + (errors.size() - sub.size()) + " more ...");
		return StringUtils.join(sub, "<br/>");
	}

}
