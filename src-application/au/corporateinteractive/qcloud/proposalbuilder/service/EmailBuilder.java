package au.corporateinteractive.qcloud.proposalbuilder.service;

import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.util.HtmlUtils;

import com.mandrill.QcloudMandrillHeaders;

import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.service.templates.QcloudEmailTemplates;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.utils.SimpleDateFormat;
import au.net.webdirector.common.utils.context.ThreadContext;
import au.net.webdirector.common.utils.email.FileTemplateEmailBuilder;
import au.net.webdirector.common.utils.email.PendingEmail;
import au.net.webdirector.common.utils.password.PasswordUtils;
import sbe.ShortUrls.ShortUrlManager;
import webdirector.db.UserUtils;

public class EmailBuilder extends FileTemplateEmailBuilder
{
	private static final String ERRORS_EMAIL_ADDRESS = "support@corporateinteractive.com.au";

	public static final String ERROR_LOGS = "LOGS";

	public static final String PROPOSAL_READY_EMAIL_TEMPLATE_ELEMENT = "Itinerary Ready";
	public static final String PROPOSAL_REVISION_READY_EMAIL_TEMPLATE_ELEMENT = "Itinerary New Revision Ready";

	private static Logger logger = Logger.getLogger(EmailBuilder.class);

	private static final String MAIN_EMAIL_TEMPLATE_WRAPPER = "fragmentWrapper.html";

	public EmailBuilder()
	{
		super(QcloudEmailTemplates.getPath());
	}

	public EmailBuilder(HttpServletRequest request)
	{
		super(QcloudEmailTemplates.getPath(), request);
	}

	@Override
	public EmailBuilder addModule(String moduleName, Map<String, ? extends Object> ht)
	{
		super.addModule(moduleName, ht);
		return this;
	}


	public PendingEmail prepareAccountVerificationEmail(String activationCode)
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("accountActivation.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in prepareAccountVerificationEmail", e);
		}

		Map<String, Object> f = new HashMap<String, Object>();
		f.put("activationCode", activationCode);

		String subject = "Activate your TravelDocs account.";
		String emailBody = generateEmail(templateReader, subject, f);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "account-verification");
	}

	public PendingEmail prepareAccountChangeEmailVerification(String activationCode, String oldEmail, String newEmail)
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("accountChangeEmail.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in prepareAccountVerificationEmail", e);
		}

		Map<String, Object> f = new HashMap<String, Object>();
		f.put("activationCode", activationCode);
		f.put("newEmail", newEmail);
		f.put("newEmailLink", newEmail.replace("+", "%2B"));
		f.put("oldEmail", oldEmail);

		String subject = "Activate your new TravelDocs email.";
		String emailBody = generateEmail(templateReader, subject, f);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "account-email-verification");
	}

	public PendingEmail prepareNewUserVerificationEmail(String activationCode, String userID, String usersName)
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("accountNewUserEmail.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in prepareAccountVerificationEmail", e);
		}

		Map<String, Object> f = new HashMap<String, Object>();
		f.put("activationCode", activationCode);
		f.put("newEmail", userID);
		f.put("usersName", usersName);

		String subject = "Activate your TravelDocs account.";
		String emailBody = generateEmail(templateReader, subject, f);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "account-verification");
	}

	public PendingEmail prepareAccountPrescreenEmail(String email, String activationCode)
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("accountPrescreen.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in prepareAccountPrescreenEmail", e);
		}

		Map<String, Object> f = new HashMap<String, Object>();
		f.put("activationCode", activationCode);

		String subject = "New user registration verification needed: " + email;
		String emailBody = generateEmail(templateReader, subject, f);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "account-prescreen");
	}

	public PendingEmail preparePasswordResetVerificationEmail(String activationCode)
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("accountPasswordResetEmailVerification.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in preparePasswordResetVerificationEmail", e);
		}

		Map<String, Object> f = new HashMap<String, Object>();
		f.put("activationCode", activationCode);

		String subject = "Reset your TravelDocs password.";
		String emailBody = generateEmail(templateReader, subject, f);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "password-reset-verification");
	}

	public PendingEmail preparePasswordResetConfirmation()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("accountPasswordResetConfirmation.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in preparePasswordResetConfirmation", e);
		}

		String subject = "Password reset confirmation for TravelDocs.";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "password-reset");
	}

	public PendingEmail prepareRegistrationAcknowledgementEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("registrationAcknowledgement.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in prepareRegistrationAcknowledgementEmail", e);
		}

		String subject = "Registration acknowledgement for TravelDocs";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "registration-acknowledge");
	}

	public PendingEmail preparePostAccountActivationEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("postAccountActivation.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in preparePostAccountActivationEmail", e);
		}

		String subject = "Your TravelDocs account has been activated!";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "account-activation");
	}

	public PendingEmail preparePostAccountActivationMaestranoEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("postAccountActivationMaestrano.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in preparePostAccountActivationEmail", e);
		}

		String subject = "Your TravelDocs account has been activated!";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "post-account-activation-maestrano");
	}



	public PendingEmail prepareSupportEmail(String message, Boolean authedAccess)
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("emailSupport.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in prepareSupportEmail", e);
		}
		String subject = String.format("TravelDocs Support Request by %s", getModuleValue(UserAccountDataAccess.USER_MODULE, UserAccountDataAccess.E_HEADLINE));
		HashMap<String, Object> additionalFields = new HashMap<String, Object>();
		additionalFields.put("message", message);
		additionalFields.put("authedAccess", authedAccess ? "Access to this customers account has been approved for 24hrs from the receipt of this email." : "");
		String emailBody = generateEmail(templateReader, subject, additionalFields);
		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "support")
				.withReplyTo((String) getModuleValue(UserAccountDataAccess.USER_MODULE, UserAccountDataAccess.E_EMAIL_ADDRESS));
	}

	public PendingEmail prepareExpiredTrialEmail(Hashtable<String, String> user)
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("expiredTrial.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in prepareExpiredTrialEmail", e);
		}
		String subject = String.format("Your trial period on TravelDocs is expiring today.");
		HashMap<String, Object> additionalFields = new HashMap<String, Object>();
		additionalFields.put("user", user);
		String emailBody = generateEmail(templateReader, subject, additionalFields);
		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "expired-trial");
	}

	public PendingEmail prepareExpiredTrialReportEmail(List<Hashtable<String, String>> users)
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("expiredTrialReport.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in prepareExpiredTrialReportEmail", e);
		}

		String subject = String.format("%s admin account(s) with trial periods expiring today", users.size());
		Map<String, Object> f = new HashMap<String, Object>();
		f.put("todaysDate", new Date());
		f.put("users", users);
		String emailBody = generateEmail(templateReader, subject, f);

		UserAccountDataAccess uada = new UserAccountDataAccess();
		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "expired-trial-report");
	}




	public PendingEmail prepareErrorEmailForContext(String title)
	{
		return prepareErrorEmailForContext(title, null);
	}

	public PendingEmail prepareErrorEmailForContext(String title, Exception exception)
	{
		StringBuffer emailBody = new StringBuffer();
		ThreadContext.parseRequest();

		emailBody.append("<h1>" + HtmlUtils.htmlEscape(title) + "</h1>");

		String logs = (String) ThreadContext.getAttribute(ERROR_LOGS);
		if (logs != null)
		{
			try
			{
				emailBody.append("<h2>Logs</h2>");
				emailBody.append("<pre>");
				emailBody.append(HtmlUtils.htmlEscape(logs));
			}
			catch (Exception e)
			{
				emailBody.append("Unable to generate logs: " + e.getMessage());
			}
			finally
			{
				emailBody.append("</pre>");
			}
		}

		try
		{
			emailBody.append("<h2>Thread Metadata</h2>");
			emailBody.append("<pre>");
			emailBody.append(HtmlUtils.htmlEscape(ThreadContext.metadata().toJson()));
		}
		catch (Exception e)
		{
			emailBody.append("Unable to generate thread metadata " + e.getMessage());
		}
		finally
		{
			emailBody.append("</pre>");
		}

		String stackHash = "UNKNOWN HASH";
		try
		{
			emailBody.append("<h1>StackTrace</h1>");
			emailBody.append("<pre>");
			StringWriter errors = new StringWriter();
			exception.printStackTrace(new PrintWriter(errors));
			String errorsStr = errors.toString();
			emailBody.append(HtmlUtils.htmlEscape(errorsStr));

			String[] lines = StringUtils.split(errorsStr, "\n");
			String[] hashLines = Arrays.copyOf(lines, (lines.length / 3) + 1);
			stackHash = PasswordUtils.encrypt(StringUtils.join(hashLines));
		}
		catch (Exception e)
		{
			emailBody.append("Unable to generate StackTrace " + e.getMessage());
		}
		finally
		{
			emailBody.append("</pre>");
		}


		return new PendingEmail("Unhandled exception HASH:" + stackHash, emailBody.toString(), req_host)
				.addTo(ERRORS_EMAIL_ADDRESS);
	}

	public PendingEmail prepareErrorEmailForItineraryRender(String title, String errors)
	{
		StringBuffer emailBody = new StringBuffer();
		ThreadContext.parseRequest();

		emailBody.append("<h1>" + HtmlUtils.htmlEscape(title) + "</h1>");

		String logs = (String) ThreadContext.getAttribute(ERROR_LOGS);
		if (logs != null)
		{
			try
			{
				emailBody.append("<h2>Logs</h2>");
				emailBody.append("<pre>");
				emailBody.append(HtmlUtils.htmlEscape(logs));
			}
			catch (Exception e)
			{
				emailBody.append("Unable to generate logs: " + e.getMessage());
			}
			finally
			{
				emailBody.append("</pre>");
			}
		}

		try
		{
			emailBody.append("<h2>Thread Metadata</h2>");
			emailBody.append("<pre>");
			emailBody.append(HtmlUtils.htmlEscape(ThreadContext.metadata().toJson()));
		}
		catch (Exception e)
		{
			emailBody.append("Unable to generate thread metadata " + e.getMessage());
		}
		finally
		{
			emailBody.append("</pre>");
		}

		try
		{
			emailBody.append("<h1>StackTrace</h1>");
			emailBody.append("<pre>");
			emailBody.append(HtmlUtils.htmlEscape(errors));
		}
		catch (Exception e)
		{
			emailBody.append("Unable to generate StackTrace " + e.getMessage());
		}
		finally
		{
			emailBody.append("</pre>");
		}


		return new PendingEmail("TravelDoc Itinerary Failed to render", emailBody.toString(), req_host)
				.addTo(ERRORS_EMAIL_ADDRESS);
	}

	public PendingEmail prepareSubscriptionChangeFailureEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("subscriptionChangeFailure.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in subscriptionChangeFailureEmail", e);
		}

		String subject = "TravelDocs Alert: Important message from TravelDocs.";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "subscription-change-failure");
	}

	public PendingEmail prepareAppSubscriptionFailureEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("appSubscriptionFailure.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in appSubscriptionFailureEmail", e);
		}

		String subject = "TravelDocs Alert: Important message from TravelDocs.";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "application-subscription-failure");
	}

	public PendingEmail prepareSubscriptionChangeSuccessEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("subscriptionChangeSuccess.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in subscriptionChangeSuccessEmail", e);
		}

		String subject = "TravelDocs Alert: Your changing of subscrption is completed.";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "subscription-change-success");
	}

	public PendingEmail prepareDataExceedNotificationEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("dataExceedNotification.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in dataExceedNotificationEmail", e);
		}

		String subject = "TravelDocs Alert:  Your data storage has exceeded your data storage allowance";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "data-storage-exceeded");
	}

	public PendingEmail prepareAppSubscriptionChangedEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("appSubscriptionChanged.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in appSubscriptionChangedEmail", e);
		}

		String subject = "TravelDocs Alert: Plugin subscription changed.";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "application-subscription-changed");
	}

	public PendingEmail prepareAppSubscriptionSuccessEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("appSubscriptionSuccess.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in appSubscriptionSuccessEmail", e);
		}

		String subject = "TravelDocs Alert: Plugin subscribed successfully.";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "application-subscription-success");
	}

	public PendingEmail prepareAppReactivateSuccessEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("appReactivationSuccess.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in appReactivationSuccessEmail", e);
		}

		String subject = "TravelDocs Alert: Plugin reactivated successfully.";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "application-subscription-success");
	}


	/**
	 * @return
	 */

	public PendingEmail prepareAccountReaccessedEmail(ElementData user)
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("accountInvalidSubscriptionLogin.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in prepareExpiredTrialEmail", e);
		}
		String subject = String.format("QuoteCloud: Can we help you?");
		HashMap<String, Object> additionalFields = new HashMap<String, Object>();
		additionalFields.put("username", user.getString("attr_name"));
		String emailBody = generateEmail(templateReader, subject, additionalFields);
		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "account-accessed");
	}


	public PendingEmail prepareAccountHasNoPaymentSourceEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("accountHasNoPaymentSource.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in prepareAccountHasNoPaymentSourceEmail", e);
		}

		String subject = "TravelDocs Alert: Important message from TravelDocs.";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "account-suspended");
	}

	public PendingEmail prepareAccountSuspendedEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("accountSuspended.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in prepareAccountSuspendedEmail", e);
		}

		String subject = "TravelDocs Alert: Important message from TravelDocs.";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "account-suspended");
	}

	public PendingEmail prepareAppSuspendedEmail(String appName)
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("applicationSuspended.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in prepareAppSuspendedEmail", e);
		}

		Map<String, Object> additionalFields = new HashMap<String, Object>();
		additionalFields.put("appName", appName);

		String subject = "TravelDocs Alert: Important message from TravelDocs.";
		String emailBody = generateEmail(templateReader, subject, additionalFields);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "application-suspended");
	}

	public PendingEmail preparePaymentFailedEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("paymentFailed.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in paymentFailedEmail", e);
		}

		String subject = "TravelDocs Alert: Important message from TravelDocs.";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "payment-failure");
	}

	public PendingEmail preparePaymentSucceededEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("paymentSucceeded.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in subscriptionChangeSuccessEmail", e);
		}

		String subject = "QuoteCloud: Thank you for your payment. Your invoice is enclosed.";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "payment-success");
	}

	public PendingEmail prepareCancellationEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("cancellation.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in CancellationEmail", e);
		}

		String subject = "QuoteCloud: Sorry to see you leave. Hope to see you again one day.";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "cancellation-success");
	}

	public PendingEmail prepareUserLoginAfterExpiryEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("loginAfterExpiry.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in UserLoginAfterExpiryEmail", e);
		}

		String subject = "TravelDocs: One user has logged in after his trial account expired.";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "login-after-expiry");
	}

	public static void main(String[] args)
	{
		Locale locale = Locale.forLanguageTag("fr-CH");
		Currency cr = Currency.getInstance(locale);
		String totalMinimumCostForDisplay = NumberFormat.getCurrencyInstance(locale).format(5654627.7d);
		System.out.println(totalMinimumCostForDisplay);
		System.out.println(cr.getCurrencyCode());
	}

	public PendingEmail prepareAppTrialWillEndEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("appTrialWillEnd.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in appTrialWillEndEmail", e);
		}

		String subject = "TravelDocs Alert: Trial period ending soon.";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "application-subscription-success");
	}


	public PendingEmail prepareSingleSignupVerification(String verificationCode, String email)
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("singleSignupVerification.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in singleSignupVerification", e);
		}

		Map<String, Object> f = new HashMap<String, Object>();
		f.put("verificationCode", verificationCode);
		f.put("email", email);

		String subject = "Login verification";

		String emailBody = generateEmail(templateReader, subject, f);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "login-verification");
	}

	public PendingEmail prepareSingleSignupRequest(String user, String customer, Boolean authorised)
	{
		Reader templateReader;
		try
		{
			if (authorised)
			{
				templateReader = getTemplateReader("singleSignupAuthorised.html");
			}
			else
			{
				templateReader = getTemplateReader("singleSignupRequest.html");
			}
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in singleSignupRequest", e);
		}

		Map<String, Object> f = new HashMap<String, Object>();
		f.put("user", user);
		f.put("customer", customer);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:ss");

		f.put("requestedOn", sdf.format(new Date()));

		String subject = "Single signup requested";

		String emailBody = generateEmail(templateReader, subject, f);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "login-verification");
	}

	public PendingEmail prepareSingleSignupUserAuthRequest(String verificationCode, String teamMember, String customer) throws SQLException
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("singleSignupUserAuthRequest.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in singleSignupUserAuthRequest", e);
		}

		TransactionManager tm = TransactionManager.getInstance();
		TUserAccountDataAccess uada = new TUserAccountDataAccess(tm);
		String[] teamUserDetails = new UserUtils().getUserByEmail(teamMember);

		Map<String, Object> f = new HashMap<String, Object>();
		f.put("verificationCode", verificationCode);
		f.put("email", teamMember);
		f.put("userEmail", customer);
		f.put("userName", teamUserDetails[1]);
		f.put("userTeamEmail", teamUserDetails[2]);
		f.put("customerName", "");

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:ss");

		f.put("requestedOn", sdf.format(new Date()));

		String subject = "Account access requested by user";

		String emailBody = generateEmail(templateReader, subject, f);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "login-verification");
	}

	public PendingEmail prepareLoginTwoFactor(String verificationCode)
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("twoFactorAuthentication.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in twoFactorAuthentication", e);
		}

		Map<String, Object> f = new HashMap<String, Object>();
		f.put("token", verificationCode);
//		f.put("username", user);

		String subject = "Login verification";

		String emailBody = generateEmail(templateReader, subject, f);

		return new PendingEmail(subject, emailBody, this.req_host);
	}

	public PendingEmail prepareWebdirectorSignupVerification(String verificationCode, String user)
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("webdirectorSignupVerification.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in webdirectorSignupVerification", e);
		}

		Map<String, Object> f = new HashMap<String, Object>();
		f.put("verificationCode", verificationCode);
		f.put("username", user);

		String subject = "Login verification";

		String emailBody = generateEmail(templateReader, subject, f);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "login-verification");
	}

	public PendingEmail prepareInvoiceReadyEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("invoiceReady.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in prepareInvoiceReadyEmail", e);
		}

		String subject = "TravelDocs Subscription fees Invoice";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "invoice-ready");
	}

	public PendingEmail prepareAccountDeletionAlertEmail()
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("accountDeletionAlert.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in prepareAccountDeletionAlertEmail", e);
		}

		String subject = "TravelDocs Alert: Important message from TravelDocs.";
		String emailBody = generateEmail(templateReader, subject, null);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "account-due-to-deletion");
	}

	public PendingEmail prepareEmailAlert(ElementData rule, Map<String, Object> additionalFields)
	{
		Reader templateReader = null;
		String subject = rule.getString("ATTR_emailSubject");
		String ruleName = ShortUrlManager.friendly(rule.getName());
		try
		{
			templateReader = new FileReader(Defaults.getInstance().getStoreDir() + rule.getString("ATTRTEXT_emailContent"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		String emailBody = generateEmail(templateReader, subject, additionalFields);
		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "email-alert" + ruleName);
	}


	public PendingEmail prepareRegisterVerification(String verificationCode, String firstname, String lastname)
	{
		Reader templateReader;
		try
		{
			templateReader = getTemplateReader("registerVerification.html");
		}
		catch (TemplateNotFoundException e)
		{
			logger.error("Error:", e);
			return prepareErrorEmailForContext("Exception in prepareRegisterVerification", e);
		}

		Map<String, Object> f = new HashMap<String, Object>();
		f.put("verificationCode", verificationCode);
		f.put("firstname", firstname);
		f.put("lastname", lastname);

		String subject = "Register verification";

		String emailBody = generateEmail(templateReader, subject, f);

		return new PendingEmail(subject, emailBody, this.req_host)
				.addHeader(QcloudMandrillHeaders.TAGS, QcloudMandrillHeaders.TAG_SYSTEM)
				.addHeader(QcloudMandrillHeaders.TAGS, "register-verification");
	}
}
