package au.corporateinteractive.qcloud.proposalbuilder.service;

import java.security.SecureRandom;
import java.util.Random;
import java.util.UUID;

import org.apache.log4j.Logger;

import au.net.webdirector.common.utils.password.PasswordUtils;

public class SecurityKeyService
{
	private static Logger logger = Logger.getLogger(SecurityKeyService.class);

	private static final String ACTIVATION_SECRET = "pingPongPopper";
	private static final String PASSWORD_RESET_SECRET = "baaBaaBlackSheep";
	private static final String MOBILE_SECRET = "flinstones";

	private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	private final Random rng = new SecureRandom();

	public String generateKey()
	{
		return UUID.randomUUID().toString();
	}

	@Deprecated
	public String getMobileKey(String userEmail, String userPassword)
	{
		return PasswordUtils.encrypt(String.format("%s_%s_%s",
				userEmail,
				userPassword,
				MOBILE_SECRET));
	}

	public String getEditKey(int userId, int proposalId, String key)
	{
		return PasswordUtils.encrypt(String.format("%s_%s_%s", userId, proposalId, key));
	}

	public String getEditParameters(int userId, int proposalId, String key)
	{
		String eKey = getEditKey(userId, proposalId, key);

		return String.format("proposal=%s&e-key=%s", proposalId, eKey);
	}

	public String getViewParameters(String userEmail, int proposalId, String key, boolean includeEmail)
	{
		String vKey = getViewKey(userEmail, proposalId, key);
		return String.format("email=%s&traveldoc=%s&v-key=%s",
				userEmail,
				proposalId,
				vKey);
	}

	public String getViewParametersForCustomer(String userEmail, int proposalId, String key, boolean includeEmail)
	{
		return getViewParameters(userEmail, proposalId, key, includeEmail) + "&layout=customer";
	}

	public String getViewParametersForApproval(String userEmail, int proposalId, String key, boolean includeEmail)
	{
		return getViewParameters(userEmail, proposalId, key, includeEmail);
	}

	public String getViewKey(String companyID, int proposalId, String key)
	{
		return PasswordUtils.encrypt(String.format("%s_%s_%s",
				companyID,
				proposalId,
				key));
	}

	public String getActivationKey(String userEmail, String passwordHash)
	{
		return PasswordUtils.encrypt(String.format("%s_%s_%s",
				userEmail.trim().toLowerCase(),
				passwordHash,
				ACTIVATION_SECRET));
	}

	public String getForgotPasswordKey(String userEmail, String passwordHash)
	{
		return PasswordUtils.encrypt(String.format("%s_%s_%s",
				userEmail.trim().toLowerCase(),
				passwordHash,
				PASSWORD_RESET_SECRET));
	}

	public String generateRandomKey(int length, int spacing, String spacerChar)
	{
		StringBuilder sb = new StringBuilder();
		int spacer = 0;
		while (length > 0)
		{
			if (spacer == spacing)
			{
				sb.append(spacerChar);
				spacer = 0;
			}
			length--;
			spacer++;
			sb.append(randomChar());
		}
		return sb.toString();
	}

	private char randomChar()
	{
		return ALPHABET.charAt(rng.nextInt(ALPHABET.length()));
	}
}
