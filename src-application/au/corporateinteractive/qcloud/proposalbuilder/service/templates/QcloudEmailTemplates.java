/**
 * @author Nick Yiming Gao
 * @date 2015-9-18
 */
package au.corporateinteractive.qcloud.proposalbuilder.service.templates;

/**
 * 
 */

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class QcloudEmailTemplates
{

	private static Logger logger = Logger.getLogger(QcloudEmailTemplates.class);

	public static String getPath()
	{
		return "/" + StringUtils.replace(QcloudEmailTemplates.class.getPackage().getName(), ".", "/") + "/";
	}

}
