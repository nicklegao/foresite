package au.corporateinteractive.qcloud.proposalbuilder.service.templates;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class TravelDocsTemplates
{

	private static Logger logger = Logger.getLogger(TravelDocsTemplates.class);

	public static String getPath()
	{
		return "/" + StringUtils.replace(TravelDocsTemplates.class.getPackage().getName(), ".", "/") + "/";
	}
}
