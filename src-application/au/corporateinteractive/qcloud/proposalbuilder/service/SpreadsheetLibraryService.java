package au.corporateinteractive.qcloud.proposalbuilder.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.ModuleHelper;
import webdirector.db.client.ManagedDataAccess;


public class SpreadsheetLibraryService extends ManagedDataAccess
{
	private static Logger logger = Logger.getLogger(SpreadsheetLibraryService.class);

	public static final String SPREADSHEET_MODULE = "SPREADSHEET";

	public Map getSpreadsheet(String spreadsheetId) throws SQLException, IOException
	{
		ElementData data = ModuleAccess.getInstance().getElementById(SPREADSHEET_MODULE, spreadsheetId);
		String json = ModuleHelper.getInstance().readStringFromFile(data, "ATTRTEXT_Json");
		return new Gson().fromJson(json, Map.class);

	}

}