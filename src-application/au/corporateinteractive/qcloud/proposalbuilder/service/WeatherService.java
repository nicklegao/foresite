package au.corporateinteractive.qcloud.proposalbuilder.service;

		import java.io.IOException;
		import java.util.*;


		import org.apache.http.HttpEntity;
		import org.apache.http.HttpResponse;
		import org.apache.http.ParseException;
		import org.apache.http.client.ClientProtocolException;
		import org.apache.http.client.HttpClient;
		import org.apache.http.client.methods.HttpGet;
		import org.apache.http.client.methods.HttpPost;
		import org.apache.http.impl.client.DefaultHttpClient;
		import org.apache.http.util.EntityUtils;
		import org.json.JSONException;
		import org.json.JSONObject;

public class WeatherService
{

	// essential URL structure is built using constants
	public static final String ACCESS_KEY = "2b58001248874eaea3a25024182301";
	public static final String BASE_URL = "http://api.apixu.com/v1/";
	public static final String ENDPOINT = "current.json";


	private static WeatherService instance;

	private WeatherService() {

	}

	public synchronized static WeatherService getInstance() {
		if (instance == null) {
			instance = new WeatherService();
		}
		return instance;
	}

	/**
	 *
	 * Notes:<br><br>
	 *
	 * A JSON response of the form {"key":"value"} is considered a simple Java JSONObject.<br>
	 * To get a simple value from the JSONObject, use: <JSONObject identifier>.get<Type>("key");<br>
	 *
	 * A JSON response of the form {"key":{"key":"value"}} is considered a complex Java JSONObject.<br>
	 * To get a complex value like another JSONObject, use: <JSONObject identifier>.getJSONObject("key")
	 *
	 * Values can also be JSONArray Objects. JSONArray objects are simple, consisting of multiple JSONObject Objects.
	 *
	 *
	 */

	public JSONObject getWeatherInfo(String countryName) {

		HttpClient httpClient = new DefaultHttpClient();

		try {

			String url = BASE_URL + ENDPOINT + "?key=" + ACCESS_KEY + "&q=" + countryName.toLowerCase();
			HttpGet get = new HttpGet(url.replaceAll("\\s+",""));
			HttpResponse response = httpClient.execute(get);
			HttpEntity entity = response.getEntity();


			return new JSONObject(EntityUtils.toString(entity));
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
