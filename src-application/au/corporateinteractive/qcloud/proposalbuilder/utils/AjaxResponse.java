package au.corporateinteractive.qcloud.proposalbuilder.utils;

public class AjaxResponse
{
	boolean success;
	String message;
	Object data;
	String redirect;

	public AjaxResponse(boolean success, String message)
	{
		super();
		this.success = success;
		this.message = message;
	}

	public AjaxResponse(boolean success, String message, Object data)
	{
		super();
		this.success = success;
		this.message = message;
		this.data = data;
	}

	public AjaxResponse(boolean success, String message, Object data, String redirect)
	{
		super();
		this.success = success;
		this.message = message;
		this.data = data;
		this.redirect = redirect;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public String getMessage()
	{
		return message;
	}

	public Object getData()
	{
		return data;
	}

	public void setData(Object data)
	{
		this.data = data;
	}

	public String getRedirect()
	{
		return redirect;
	}

	public void setRedirect(String redirect)
	{
		this.redirect = redirect;
	}

}
