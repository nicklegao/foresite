/**
 * @author Nick Yiming Gao
 * @date 11/04/2017 9:56:28 am
 */
package au.corporateinteractive.qcloud.proposalbuilder.utils;

/**
 * 
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.log4j.Logger;

public class DateFormatUtils
{

	private static Logger logger = Logger.getLogger(DateFormatUtils.class);

	private static String[] codes = {
			"dd/MM/yyyy", "dd/MMM/yyyy",
			"MM/dd/yyyy", "MMM/dd/yyyy",
			"yyyy-MM-dd", "yyyy-MMM-dd",
			"EEE dd MMMM yyyy", "dd MMMM yyyy"
	};

	private static String[] javaPatterns = {
			"dd/MM/yyyy", "dd/MMM/yyyy",
			"MM/dd/yyyy", "MMM/dd/yyyy",
			"yyyy-MM-dd", "yyyy-MMM-dd",
			"EEE dd MMMM yyyy", "dd MMMM yyyy"
	};

	private static String[] jsPatterns = {
			"DD/MM/YYYY", "DD/MMM/YYYY",
			"MM/DD/YYYY", "MMM/DD/YYYY",
			"YYYY-MM-DD", "YYYY-MMM-DD",
			"ddd DD MMMM YYYY", "DD MMMM YYYY"
	};

	public static DateFormatUtils[] getAvailableInstances(Locale locale)
	{
		DateFormatUtils[] instances = new DateFormatUtils[codes.length];
		for (int i = 0; i < codes.length; i++)
		{
			instances[i] = new DateFormatUtils(codes[i], javaPatterns[i], jsPatterns[i], locale);
		}
		return instances;
	}

	public static DateFormatUtils getInstance(String code, Locale locale)
	{
		int i = Arrays.asList(codes).indexOf(code);
		if (i == -1)
		{
			i = 0;
		}
		return new DateFormatUtils(codes[i], javaPatterns[i], jsPatterns[i], locale);
	}

	private String code;
	private String javaPattern;
	private String jsPattern;
	private Locale locale;
	private TimeZone timeZone;

	private DateFormatUtils(String code, String javaPattern, String jsPattern, Locale locale)
	{
		super();
		this.code = code;
		this.javaPattern = javaPattern;
		this.jsPattern = jsPattern;
		this.locale = locale != null ? locale : Locale.getDefault();
	}

	public void setTimeZone(TimeZone timeZone)
	{
		this.timeZone = timeZone;
	}

	public TimeZone getTimeZone()
	{
		return this.timeZone == null ? TimeZone.getDefault() : this.timeZone;
	}

	public String getCode()
	{
		return code;
	}

	public String getJavaPattern()
	{
		return javaPattern;
	}

	public String getJsPattern()
	{
		return jsPattern;
	}

	public String format(Date date)
	{
		SimpleDateFormat df = new SimpleDateFormat(javaPattern, locale);
		df.setTimeZone(this.getTimeZone());
		return df.format(date);
	}

	public Date parse(String source) throws ParseException
	{
		SimpleDateFormat df = new SimpleDateFormat(javaPattern, locale);
		df.setTimeZone(this.getTimeZone());
		return df.parse(source);
	}

	public static void main(String[] args)
	{
		for (DateFormatUtils instance : getAvailableInstances(Locale.forLanguageTag("zh-CN")))
		{
			System.out.println(instance.format(new Date()));
		}
		for (String id : TimeZone.getAvailableIDs())
		{
			System.out.println(id);
		}

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println(new Date());
		System.out.println(df.format(new Date()));
		df.setTimeZone(TimeZone.getTimeZone("Etc/GMT-11"));
		System.out.println(df.format(new Date()));
	}
}
