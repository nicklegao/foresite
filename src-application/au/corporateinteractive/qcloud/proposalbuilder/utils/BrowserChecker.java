package au.corporateinteractive.qcloud.proposalbuilder.utils;

import net.sf.uadetector.ReadableDeviceCategory.Category;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;

public class BrowserChecker {
    public static boolean isBrowserSupported(String agentString) {
        UserAgentStringParser parser = UADetectorServiceFactory.getResourceModuleParser();
        ReadableUserAgent agent = parser.parse(agentString);

        String browserName = agent.getName();

        if (agent.getDeviceCategory().getCategory() == Category.TABLET)
        {
            if (browserName.equalsIgnoreCase("Mobile Safari"))
            {
                if (agent.getDeviceCategory().getCategory() == Category.TABLET){
                    //iPad
                    return Integer.parseInt(agent.getVersionNumber().getMajor()) >= 7;
                } 
            }
            if (browserName.equalsIgnoreCase("Chrome"))
            {
                int major = Integer.parseInt(agent.getVersionNumber().getMajor());
                return major >= 31;
            }
        }
        else if (agent.getDeviceCategory().getCategory() == Category.PERSONAL_COMPUTER)
        {
            if (browserName.equalsIgnoreCase("Chrome"))
            {
                return Integer.parseInt(agent.getVersionNumber().getMajor()) >= 12;
            }
            else if (browserName.equalsIgnoreCase("Firefox"))
            {
                return Integer.parseInt(agent.getVersionNumber().getMajor()) >= 6;
            }
            else if (browserName.equalsIgnoreCase("IE"))
            {
                //IE desktop and IE Touch (surface)
                return Integer.parseInt(agent.getVersionNumber().getMajor()) >= 9;
            }
        }
        else if (agent.getDeviceCategory().getCategory() == Category.SMARTPHONE)
        {
            if (browserName.equalsIgnoreCase("Android browser"))
            {
                //Android default browser (NOT CHROME)
                int major = Integer.parseInt(agent.getVersionNumber().getMajor());
                return major >= 4;
            }
        }
        
        return false;
    }
}
