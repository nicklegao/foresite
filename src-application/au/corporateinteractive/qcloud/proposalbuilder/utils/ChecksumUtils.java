package au.corporateinteractive.qcloud.proposalbuilder.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ChecksumUtils
{
	public static String getFileChecksum(File file)
	{
		return getFileChecksum(file, "SHA1");
	}

	public static String getFileChecksum(File file, String digestType)
	{
		try
		{
			MessageDigest md = MessageDigest.getInstance(digestType);
			FileInputStream fis = new FileInputStream(file);
			byte[] dataBytes = new byte[1024];

			int nread = 0;

			while ((nread = fis.read(dataBytes)) != -1)
			{
				md.update(dataBytes, 0, nread);
			}

			byte[] mdBytes = md.digest();

			StringBuffer sb = new StringBuffer("");
			for (int i = 0; i < mdBytes.length; i++)
			{
				sb.append(Integer.toString((mdBytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return "";
		}
	}

	public static String getTextHash(String text)
	{
		return getTextHash(text, "SHA1");
	}

	public static String getTextHash(String text, String digestType)
	{
		MessageDigest messageDigest = null;
		try
		{
			messageDigest = MessageDigest.getInstance(digestType);

			messageDigest.reset();

			messageDigest.update(text.getBytes("UTF-8"));
		}
		catch (NoSuchAlgorithmException e)
		{
			System.out.println("NoSuchAlgorithmException caught!");
			System.exit(-1);
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		byte[] byteArray = messageDigest.digest();

		StringBuffer encStrBuff = new StringBuffer();

		for (int i = 0; i < byteArray.length; i++)
		{
			if (Integer.toHexString(0xFF & byteArray[i]).length() == 1)
				encStrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
			else
				encStrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
		}

		return encStrBuff.toString();

	}
}
