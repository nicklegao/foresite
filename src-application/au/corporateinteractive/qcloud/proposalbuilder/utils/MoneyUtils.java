package au.corporateinteractive.qcloud.proposalbuilder.utils;


public class MoneyUtils
{


	public static class DisplayableMoney
	{
		private String integral;
		private String decimal;

		public String getIntegral()
		{
			return integral;
		}

		public void setIntegral(String integral)
		{
			this.integral = integral;
		}

		public String getDecimal()
		{
			return decimal;
		}

		public void setDecimal(String decimal)
		{
			this.decimal = decimal;
		}

		public String getIntegralAndDot()
		{
			if (isInteger())
			{
				return integral;
			}
			return integral + ".";
		}

		private boolean isInteger()
		{
			return "00".equals(decimal);
		}

		public String getDecimalIfHasValue()
		{
			if (isInteger())
			{
				return "";
			}
			return getDecimal();
		}

		public String toString()
		{
			return integral + "." + decimal;
		}

	}

	public static DisplayableMoney toDisplayableMoney(double money)
	{
		long low = (long) money;
		long high = Math.round(money);
		DisplayableMoney result = new DisplayableMoney();

		if (Math.abs(money - low) < 0.001 || Math.abs(money - high) < 0.001)
		{ // if money is actually an int
			result.setIntegral(high + "");
			result.setDecimal("00");
			return result;
		}

		result.setIntegral(low + "");
		long decimal = Math.round((money - low) * 100);
		if (decimal < 10)
		{
			result.setDecimal("0" + decimal);
		}
		else
		{
			result.setDecimal("" + decimal);
		}
		return result;
	}

	public static void main(String[] args)
	{
		DisplayableMoney money = MoneyUtils.toDisplayableMoney(0);
		System.out.println(money.integral);
		System.out.println(money.decimal);
	}

}
