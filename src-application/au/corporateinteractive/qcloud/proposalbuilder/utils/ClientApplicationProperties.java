/**
 * @author Sushant Verma
 * @date 05 Jan 2016
 */
package au.corporateinteractive.qcloud.proposalbuilder.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

public class ClientApplicationProperties
{
	private static ClientApplicationProperties instance;
	private static Object _sharedLock_ = new Object();

	public static ClientApplicationProperties sharedInstance()
	{
		if (instance == null)
		{
			synchronized (_sharedLock_)
			{
				if (instance == null)
				{
					instance = new ClientApplicationProperties();
				}
			}
		}
		return instance;
	}

	private String applicationVersion;

	private ClientApplicationProperties()
	{
		try
		{
			Properties prop = new Properties();
			prop.load(new FileInputStream(configFilePath("client-application.properties")));

			this.applicationVersion = prop.getProperty("application.version");
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private File configFilePath(String fileName)
	{
		URL url = getClass().getResource("/config/" + fileName);
		File f = null;

		if (url != null)
		{
			try
			{
				f = new File(url.toURI());
			}
			catch (URISyntaxException e)
			{
				f = new File(url.getPath());
			}

		}
		if (f != null)
		{
			System.out.println(f.getAbsolutePath());
		}
		return f;
	}

	public String getApplicationVersion()
	{
		return applicationVersion;
	}
}
