/**
 * @author Jaysun Lee
 * @date 24 Dec 2015
 */
package au.corporateinteractive.qcloud.proposalbuilder.moduleActions;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.subscription.SubscriptionPlan;
import au.net.webdirector.admin.triggers.ModuleAction;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;

public class UserSubscriptionEnforcer implements ModuleAction
{
	private static Logger logger = Logger.getLogger(UserSubscriptionEnforcer.class);

	private long asyncCounter = 0;
	private long totalAsyncExecutionTime = 0;

	@Override
	public String[] supportedModules()
	{
		return new String[] { UserAccountDataAccess.USER_MODULE };
	}

	@Override
	public long executionCount()
	{
		return asyncCounter;
	}

	@Override
	public long totalExecutionTime()
	{
		return totalAsyncExecutionTime;
	}

	@Override
	public long calculateAverageExecutionTime()
	{
		long count = executionCount();
		if (count > 0)
			return totalExecutionTime() / count;
		return 0;
	}

	@Override
	public void didInsertElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
		System.out.println("-> User inserted. Element_id: " + elementData.getId());
	}

	@Override
	public void didInsertCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
	}

	@Override
	public void didUpdateElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
		System.out.println("-> User updated. Element_id: " + elementData.getId());
	}

	@Override
	public void didUpdateCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
	}

	@Override
	public void didDeleteElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
		System.out.println("-> User deleted. Element_id: " + elementData.getId());
	}

	@Override
	public void didDeleteCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
	}

	@Override
	public void willInsertElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData) throws Exception
	{
		ModuleAccess ma = new ModuleAccess();
		UserAccountDataAccess uada = new UserAccountDataAccess();

		CategoryData salesTeam = ma.getCategoryById(UserAccountDataAccess.USER_MODULE, elementData.getParentId());
		CategoryData company = uada.getCompanyForCompanyId(salesTeam.getParentId());
		SubscriptionPlan plan = SubscriptionPlan.getPlanByName(company.getString(UserAccountDataAccess.C_ADDRESS_COUNTRY), company.getString(UserAccountDataAccess.C_PRICING_PLAN));

		int userCount = uada.getUserCountInCompanyWithTeamId(elementData.getParentId());

		if (plan.userLimitExceeded(userCount))
			throw new Exception("No more than " + plan.getMaxUsers() + " users allowed by the " + plan.getName() + " plan.");

	}

	@Override
	public void willInsertCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData) throws Exception
	{
	}

	@Override
	public void willUpdateElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData) throws Exception
	{
	}

	@Override
	public void willUpdateCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData) throws Exception
	{
	}

	@Override
	public void willDeleteElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData) throws Exception
	{
	}

	@Override
	public void willDeleteCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData) throws Exception
	{
	}

	@Override
	public void didMoveElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
	}

	@Override
	public void didMoveCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
	}

	@Override
	public void didCopyElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
	}

	@Override
	public void didCopyCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
	}

	@Override
	public void willMoveElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
	}

	@Override
	public void willMoveCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
	}

	@Override
	public void willCopyElementOnModule(ServletContext servletContext, String moduleName, ElementData elementData)
	{
	}

	@Override
	public void willCopyCategoryOnModule(ServletContext servletContext, String moduleName, CategoryData categoryData)
	{
	}


}
