//package au.corporateinteractive.qcloud.proposalbuilder.db;
//
//import java.util.ArrayList;
//import java.util.HashSet;
//import java.util.Hashtable;
//import java.util.LinkedHashMap;
//import java.util.LinkedHashSet;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.Vector;
//
//import org.apache.log4j.Logger;
//
//import au.corporateinteractive.qcloud.proposalbuilder.json.proposal.PlanClass;
//import au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Pricing;
//import au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal;
//import au.corporateinteractive.qcloud.proposalbuilder.model.FeaturesAndBenefitsResponse;
//import au.corporateinteractive.qcloud.proposalbuilder.model.UCResponse;
//import au.corporateinteractive.qcloud.proposalbuilder.model.fnb.FeatureAndBenefit;
//import au.corporateinteractive.qcloud.proposalbuilder.model.fnb.PlanCategory;
//import au.corporateinteractive.qcloud.proposalbuilder.model.uc.UpSellCrossSell;
//import au.net.webdirector.common.datalayer.admin.db.DataAccess;
//import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
//import au.net.webdirector.common.utils.module.StoreUtils;
//import webdirector.db.client.ManagedDataAccess;
//import webdirector.db.client.NotesDataAccess;
//
//public class PlanDataAccess extends ManagedDataAccess
//{
//	Logger logger = Logger.getLogger(PlanDataAccess.class);
//
//	public static final String PLANS_MODULE = "plans";
//	public static final String C_CIS = "ATTR_cis";
//	public static final String C_LEGAL_NAME = "ATTR_legalName";
//	public static final String C_EXTRAS_CATEGORY = "ATTRDROP_extrasCategory";
//
//	public static final String E_TERM = "ATTRINTEGER_term";
//	public static final String E_COST = "ATTRFLOAT_cost";
//	public static final String E_ONE_OFF = "ATTRCHECK_isOneOff";
//	public static final String E_DETAILED_DESCRIPTION = "ATTRTEXT_detailedDescription";
//
//	public Map<String, String> getTermsForPlans(String[] selectedPlans)
//	{
//		LinkedHashMap<String, String> matches = new LinkedHashMap<String, String>();
//		if (selectedPlans == null || selectedPlans.length == 0)
//		{
//			return matches;
//		}
//
//		Set<String> elementPlans = cda.getCategoryIdByElements(PLANS_MODULE, selectedPlans);
//		for (String plan : elementPlans)
//		{
//			Vector v = cda.getCategory(PLANS_MODULE, plan, false);
//			if (v.size() > 0)
//			{
//				Hashtable<String, String> ht = (Hashtable<String, String>) v.get(0);
//				String name = ht.get(C_LEGAL_NAME);
//				String cisUrl = ht.get(C_CIS);
//
//				if (name != null && cisUrl != null)
//				{
//					cisUrl.trim();
//					if (cisUrl.length() > 0)
//					{
//						matches.put(name, cisUrl);
//					}
//				}
//			}
//		}
//		return matches;
//	}
//
//	public Map<String, String> getTermsForPlans(String proposalID)
//	{
//		Proposal proposal = new TravelDocsDataAccess().loadTravelDoc(proposalID);
//		return getTermsForPlans(proposal);
//	}
//
//	public Map<String, String> getTermsForPlans(Proposal proposal)
//	{
//		Set<String> planIds = new HashSet<String>();
//		for (Pricing pricing : proposal.getPricings())
//		{
//			List<PlanClass> plans = pricing.getPlans();
//			for (PlanClass plan : plans)
//			{
//				planIds.add(plan.getPricingId());
//			}
//		}
//		PlanDataAccess pda = new PlanDataAccess();
//		Map<String, String> terms = pda.getTermsForPlans(planIds.toArray(new String[0]));
//		return terms;
//	}
//
//	public UCResponse getUCForPlans(String[] selectedPlans)
//	{
//		UCResponse matches = new UCResponse();
//
//		if (selectedPlans == null || selectedPlans.length == 0)
//		{
//			return matches;
//		}
//
//		Set<String> elementPlans = cda.getCategoryIdByElements(PLANS_MODULE, selectedPlans);
//		Set<String> elementCategories = new LinkedHashSet<String>();
//		for (String planId : elementPlans)
//		{
//			Vector v = cda.getCategory(PLANS_MODULE, planId, false);
//			// select * from notes where module_name = PLANS_MODULE and foriegnKey = planId and elementOrCategory = "Categories"
//
//			//planId = features
//			if (v.size() > 0)
//			{
//				Hashtable<String, String> ht = (Hashtable<String, String>) v.get(0);
//				elementCategories.add(ht.get(C_PARENT));
//			}
//		}
//
//		Set<String> extrasProcessed = new HashSet<String>();
//		for (String categoryId : elementCategories)
//		{
//			Vector v = cda.getCategory(PLANS_MODULE, categoryId, false);
//			if (v.size() > 0)
//			{
//				Hashtable<String, String> ht = (Hashtable<String, String>) v.get(0);
//				String ucCategory = ht.get(C_EXTRAS_CATEGORY);
//
//				if (ucCategory.length() > 0 && extrasProcessed.contains(ucCategory) == false)
//				{
//					//prevent duplication
//					extrasProcessed.add(ucCategory);
//
//					Vector<Hashtable<String, String>> extrasHt_V = cda.getCategory(PLANS_MODULE, ucCategory, true);
//					Hashtable<String, String> extrasHt = (Hashtable<String, String>) extrasHt_V.get(0);
//					Vector<Hashtable<String, String>> matchedUCs = cda.getCategoryChildren(PLANS_MODULE, ucCategory);
//					String name = extrasHt.get(C_NAME);
//
//					for (Hashtable<String, String> uc : matchedUCs)
//					{
//						Vector<Hashtable<String, String>> prices = cda.getCategoryElements(PLANS_MODULE, uc.get(C_ID));
//
//						if (prices.size() > 0)
//						{
////							matches.add(name, UpSellCrossSell.createFromCategory(uc, prices.get(0)));
//						}
//					}
//				}
//			}
//		}
//
//		return matches;
//	}
//
//	public FeaturesAndBenefitsResponse getFeaturesAndBenefitsForPlansAndProposal(String[] selectedPlans, int productCategoryId, Proposal proposal)
//	{
//		FeaturesAndBenefitsResponse response = getFeaturesAndBenefitsForPlans(selectedPlans);
//		List<PlanCategory> categories = response.getCategories();
//
//		List<String> whiteListBenefits = proposal.getBenefits();
//		List<String> whiteListFeatures = proposal.getFeatures();
//		List<PlanCategory> toRemoveCategories = new ArrayList<PlanCategory>();
//		for (PlanCategory category : categories)
//		{
//			if (category.getProductCategoryId() != productCategoryId)
//			{
//				toRemoveCategories.add(category);
//				continue;
//			}
//
//			List<FeatureAndBenefit> benefits = category.getBenefits();
//			List<FeatureAndBenefit> filteredBenefits = new LinkedList<FeatureAndBenefit>();
//			for (FeatureAndBenefit benefit : benefits)
//			{
//				if (whiteListBenefits.contains(benefit.getId()))
//				{
//					filteredBenefits.add(benefit);
//				}
//			}
//			category.setBenefits(filteredBenefits);
//
//			Map<String, List<FeatureAndBenefit>> featureMap = category.getFeatures();
//			List<String> toRemoveFeatureName = new ArrayList<String>();
//			for (String featureName : featureMap.keySet())
//			{
//				List<FeatureAndBenefit> features = featureMap.get(featureName);
//				List<FeatureAndBenefit> filteredFeatures = new LinkedList<FeatureAndBenefit>();
//				for (FeatureAndBenefit feature : features)
//				{
//					if (whiteListFeatures.contains(feature.getId()))
//					{
//						filteredFeatures.add(feature);
//					}
//				}
//				if (filteredFeatures.size() == 0)
//				{
//					toRemoveFeatureName.add(featureName);
//				}
//				else
//				{
//					featureMap.put(featureName, filteredFeatures);
//				}
//			}
//			for (String featureName : toRemoveFeatureName)
//			{
//				featureMap.remove(featureName);
//			}
//
//			if (category.getBenefits().size() == 0 && category.getFeatures().keySet().size() == 0)
//			{
//				toRemoveCategories.add(category);
//			}
//		}
//		for (PlanCategory category : toRemoveCategories)
//		{
//			categories.remove(category);
//		}
//
//		return response;
//	}
//
//	public FeaturesAndBenefitsResponse getFeaturesAndBenefitsForPlans(String[] selectedPlans)
//	{
//		FeaturesAndBenefitsResponse matches = new FeaturesAndBenefitsResponse();
//
//		if (selectedPlans == null || selectedPlans.length == 0)
//		{
//			return matches;
//		}
//
//		NotesDataAccess nda = new NotesDataAccess();
//		Set<String> elementPlans = cda.getCategoryIdByElements(PLANS_MODULE, selectedPlans);
//		for (String planId : elementPlans)
//		{
//			Vector matchedPlans = cda.getCategory(PLANS_MODULE, planId, false);
//			// select * from notes where module_name = PLANS_MODULE and foriegnKey = planId and elementOrCategory = "Categories"
//
//			//planId = features
//			if (matchedPlans.size() > 0)
//			{
//				Hashtable<String, String> planHt = (Hashtable<String, String>) matchedPlans.get(0);
//				String planCategoryId = planHt.get(C_PARENT); //TODO: GROUP BY THIS DUDE
//				String planName = planHt.get(C_NAME);
//
//				if (false == matches.hasProductCategoryBeenProcessed(planCategoryId))
//				{
//					String categoryName = null; //TODO: MAYBE THIS SHOULDNT BE NULL!
//
//					Vector v = cda.getCategory(PLANS_MODULE, planCategoryId, false);
//					if (v.size() > 0)
//					{
//						Hashtable<String, String> ht = (Hashtable<String, String>) v.get(0);
//						categoryName = ht.get(C_NAME);
//
//						List<Hashtable<String, String>> rawBenefits = nda.getNotesForCategory(PLANS_MODULE, planCategoryId, "ATTRNOTES_benefits");
//						for (Hashtable<String, String> note : rawBenefits)
//						{
//							matches.addBenefits(categoryName, planCategoryId, FeatureAndBenefit.createBenefitFromNote(note, planCategoryId));
//						}
//					}
//
//					processFeaturesUsingCategory(matches, elementPlans, planCategoryId, categoryName);
//				}
//			}
//		}
//
//		return matches;
//	}
//
//	private void processFeaturesUsingCategory(
//			FeaturesAndBenefitsResponse matches,
//			Set<String> elementPlans,
//			String planCategoryId,
//			String categoryName
//	)
//	{
//		NotesDataAccess nda = new NotesDataAccess();
//
//		Vector<Hashtable<String, String>> allPlans = cda.getCategoryChildren(PLANS_MODULE, planCategoryId);
//		for (Hashtable<String, String> planHT : allPlans)
//		{
//			String planId = planHT.get(C_ID);
//			String legalName = planHT.get(C_LEGAL_NAME);
//			if (elementPlans.contains(planId))
//			{
//				List<Hashtable<String, String>> rawFeatures = nda.getNotesForCategory(PLANS_MODULE, planId, "ATTRNOTES_features");
//				for (Hashtable<String, String> note : rawFeatures)
//				{
//					matches.addFeature(categoryName, planCategoryId, legalName, FeatureAndBenefit.createFeatureFromNote(note, planId));
//				}
//			}
//		}
//	}
//
//	public int getProductCategoryByName(String product, String category)
//	{
//		int productId = getProductByName(product);
//
//		String productCatId = cda.getCategoryIdByCategoryNameWithParent(PLANS_MODULE, category, Integer.toString(productId));
//		if (productCatId == null)
//		{
//			Hashtable<String, String> ht = standardCreateCategoryHashtable(category);
//			insertNotNull(ht, "Category_ParentID", productId);
//			insertNotNull(ht, "folderLevel", 2);
//			return createCategory(ht, PLANS_MODULE);
//		}
//
//		return Integer.parseInt(productCatId);
//	}
//
//	public int getProductByName(String product)
//	{
//		String productId = cda.getCategoryIdByCategoryName(PLANS_MODULE, product);
//		if (productId == null)
//		{
//			Hashtable<String, String> ht = standardCreateCategoryHashtable(product);
//			productId = Integer.toString(createCategory(ht, PLANS_MODULE));
//		}
//		return Integer.parseInt(productId);
//	}
//
//	public int getProductPlanByName(int productCatId, String plan)
//	{
//		String planId = cda.getCategoryIdByCategoryNameWithParent(PLANS_MODULE, plan, Integer.toString(productCatId));
//		if (planId == null)
//		{
//			Hashtable<String, String> ht = standardCreateCategoryHashtable(plan);
//			insertNotNull(ht, C_PARENT, productCatId);
//			insertNotNull(ht, C_FOLDER_LEVEL, 3);
//			return createCategory(ht, PLANS_MODULE);
//		}
//
//		return Integer.parseInt(planId);
//	}
//
//	public int addPricingOption(int planId, String title, double spend, int term, boolean isOneOff, String detailedDescription)
//	{
//		Hashtable<String, String> pricingOption = standardCreateElementHashtable(planId, title);
//
//		insertNotNull(pricingOption, E_COST, spend);
//		insertNotNull(pricingOption, E_TERM, term);
//		insertNotNull(pricingOption, E_ONE_OFF, isOneOff);
//
//		int newId = createElement(pricingOption, PLANS_MODULE);
//
//		if (detailedDescription != null && detailedDescription.length() > 0)
//		{
//			StoreUtils storeUtils = new StoreUtils();
//			String filePath = storeUtils.writeFieldToStores(PLANS_MODULE, newId, E_DETAILED_DESCRIPTION, detailedDescription);
//
//			insertNotNull(pricingOption, E_ID, newId);
//			insertNotNull(pricingOption, E_DETAILED_DESCRIPTION, filePath);
//			updateElement(pricingOption, PLANS_MODULE);
//		}
//
//		return newId;
//	}
//
//	public void setCIS(int planId, String title, String contents)
//	{
//		Vector v = cda.getCategory(PLANS_MODULE, Integer.toString(planId), false);
//		if (v != null && v.size() > 0)
//		{
//			Hashtable<String, String> ht = (Hashtable<String, String>) v.get(0);
//			ht.put(C_CIS, contents);
//			ht.put(C_LEGAL_NAME, title);
//			updateCategory(ht, PLANS_MODULE);
//		}
//	}
//
//	public void addBenefitForCategory(int categoryId, String title, String contents)
//	{
//		NotesDataAccess nda = new NotesDataAccess();
//		nda.addNoteForCategory(PLANS_MODULE, Integer.toString(categoryId),
//				"ATTRNOTES_benefits",
//				String.format("%s\n%s", title, contents));
//	}
//
//	public void addFeaturesForCategory(int categoryId, String contents)
//	{
//		NotesDataAccess nda = new NotesDataAccess();
//		nda.addNoteForCategory(PLANS_MODULE, Integer.toString(categoryId),
//				"ATTRNOTES_features",
//				String.format("-\n%s", contents));
//	}
//
//	public List<String> getProductCategoryIdsForPricingItems(List<PlanClass> pricingIDs)
//	{
//
//		if (pricingIDs == null || pricingIDs.size() == 0)
//		{
//			return new LinkedList<String>();
//		}
//
//		StringBuffer planPlaceholders = new StringBuffer();
//		LinkedList<String> preparedParams = new LinkedList<String>();
//		for (PlanClass pricing : pricingIDs)
//		{
//			if (planPlaceholders.length() > 0)
//			{
//				planPlaceholders.append(",");
//			}
//			preparedParams.add(pricing.getPricingId());
//			planPlaceholders.append("?");
//		}
//
//		DataAccess da = DataAccess.getInstance();
//		List<String> productCategoryIds = da.select("select c2.Category_id "//, c2.ATTR_categoryName, c2.folderLevel "
//						+ "from elements_plans e, categories_plans c3, categories_plans c2 "
//						+ "where e.Element_id in (" + planPlaceholders.toString() + ") "
//						+ "and e.Category_id = c3.Category_id "
//						+ "and c3.Category_ParentID = c2.Category_id "
//						+ "group by Category_id",
//				preparedParams.toArray(new String[] {}),
//				StringMapper.getInstance());
//
//		return productCategoryIds;
//	}
//}
