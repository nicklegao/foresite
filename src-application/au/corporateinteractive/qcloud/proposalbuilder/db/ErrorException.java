package au.corporateinteractive.qcloud.proposalbuilder.db;

public class ErrorException extends Exception{
  private static final long serialVersionUID = 2674823780676458116L;

  public ErrorException(String message) {
    super (message);
  }
}
