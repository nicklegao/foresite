//package au.corporateinteractive.qcloud.proposalbuilder.db;
//
//import java.io.File;
//import java.math.BigDecimal;
//import java.sql.SQLException;
//import java.util.Hashtable;
//import java.util.List;
//
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//
//import au.net.webdirector.common.Defaults;
//import au.net.webdirector.common.datalayer.admin.db.DataAccess;
//import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
//import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;
//import au.net.webdirector.common.datalayer.base.db.mapper.IntegerMapper;
//import au.net.webdirector.common.datalayer.client.ElementData;
//import au.net.webdirector.common.datalayer.client.ModuleAccess;
//import au.net.webdirector.dev.annotation.DbColumn;
//import au.net.webdirector.dev.annotation.Module;
//import webdirector.db.client.ManagedDataAccess;
//
//@Module(name = "disk_usage")
//public class DiskUsageDataAccess extends ManagedDataAccess
//{
//
//	Logger logger = Logger.getLogger(DiskUsageDataAccess.class);
//
//	public static final String DISK_USAGE_MODULE = "DISK_USAGE";
//
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_VIDEO_LIBRARY = "ATTRFLOAT_videoLibrary";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_COVER_PAGE = "ATTRFLOAT_coverPage";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE_LIBRARY = "ATTRFLOAT_imageLibrary";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_PDF = "ATTRFLOAT_pdf";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_TXT = "ATTRFLOAT_text";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_PRODUCT = "ATTRFLOAT_products";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_PROPOSAL = "ATTRFLOAT_proposals";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_PROPOSAL_ATTACHMENT = "ATTRFLOAT_proposalAttachment";
//
//	@DbColumn(type = DbColumn.TYPE_CATEGORY)
//	public static final String C_COMPANY_ID = "ATTRDROP_CompanyID";
//
//	public ElementData getDiskUsageForCompanyId(String companyId)
//	{
//		String sql = "select e.*  " + " from elements_" + DISK_USAGE_MODULE
//				+ " e, " + " categories_" + DISK_USAGE_MODULE + " c1, "
//				+ " categories_" + DISK_USAGE_MODULE + " c2 "
//				+ " where e.category_id = c1.category_id "
//				+ " and c1.category_parentid = c2.category_id "
//				+ " and c2.ATTRDROP_CompanyID = " + companyId
//				+ " order by e.Create_date desc limit 1";
//		List<ElementData> result;
//		try
//		{
//			result = ModuleAccess.getInstance().getElements(sql);
//			if (result.size() == 0)
//			{
//				return null;
//			}
//			else
//			{
//				return result.get(0);
//			}
//		}
//		catch (SQLException e)
//		{
//			logger.fatal("Oh No!", e);
//			return null;
//		}
//	}
//
//	public int getOrCreateCompanyCategory(String categoryName, String companyId)
//	{
//		String sql = "select c." + C_ID + " from categories_"
//				+ DISK_USAGE_MODULE + " c where c." + C_COMPANY_ID + "=?";
//		List categoryIds = DataAccess.getInstance()
//				.select(sql, new String[] { companyId }, new IntegerMapper());
//		Integer categoryId = categoryIds.size() > 0 ? (int) (categoryIds.get(0)) : null;
//
//		if (categoryId == null)
//		{
//			Hashtable<String, String> ht = new Hashtable<String, String>();
//			insertNotNull(ht, "Category_ParentID", 0);
//			insertNotNull(ht, "folderLevel", 1);
//			insertNotNull(ht, C_COMPANY_ID, companyId);
//			insertNotNull(ht, C_NAME, categoryName);
//			categoryId = createCategory(ht, DISK_USAGE_MODULE);
//		}
//
//		return categoryId;
//	}
//
//	public void addDiskUsageData(ElementData diskUsage, String companyId)
//	{
//		addVideoUsage(diskUsage, companyId);
//		addCoverPageUsage(diskUsage, companyId);
//		addImageUsage(diskUsage, companyId);
//		addPdfUsage(diskUsage, companyId);
//		addTextUsage(diskUsage, companyId);
//		addProductUsage(diskUsage, companyId);
//		addProposalUsage(diskUsage, companyId);
//		addProposalAttachmentUsage(diskUsage, companyId);
//	}
//
//	private void addVideoUsage(ElementData diskUsage, String companyId)
//	{
//		String gen_video_sql = "select e.category_id, e.ATTRFILE_thumbnail as ATTRFILE_path from categories_gen_video c1, categories_gen_video c2, elements_gen_video"
//				+ " e where c1.Category_id=e.Category_id and c1.Category_ParentID=c2.Category_id and c2.ATTRDROP_CompanyID = " + companyId;
//		addUsage(gen_video_sql, diskUsage, E_VIDEO_LIBRARY);
//	}
//
//	private void addCoverPageUsage(ElementData diskUsage, String companyId)
//	{
//		String gen_cover_sql = "select e.category_id, e.ATTRFILE_image as ATTRFILE_path from categories_gen_coverpages c, elements_gen_coverpages e"
//				+ " where c.Category_id=e.Category_id and c.ATTRDROP_CompanyID = " + companyId;
//		addUsage(gen_cover_sql, diskUsage, E_COVER_PAGE);
//	}
//
//	private void addImageUsage(ElementData diskUsage, String companyId)
//	{
//		String gen_image_sql = "select e.category_id, e.ATTRFILE_image as ATTRFILE_path from categories_gen_images c1, categories_gen_images c2, elements_gen_images e"
//				+ " where c1.Category_id=e.Category_id and c1.Category_ParentID=c2.Category_id and c2.ATTRDROP_CompanyID = " + companyId;
//		addUsage(gen_image_sql, diskUsage, E_IMAGE_LIBRARY);
//	}
//
//	private void addPdfUsage(ElementData diskUsage, String companyId)
//	{
//		String gen_pdf_sql = "select e.category_id, e.ATTRFILE_attached_file as ATTRFILE_path from categories_gen_pdf c1, categories_gen_pdf c2, elements_gen_pdf e"
//				+ " where c1.Category_id=e.Category_id and c1.Category_ParentID=c2.Category_id and c2.ATTRDROP_CompanyID = " + companyId;
//		addUsage(gen_pdf_sql, diskUsage, E_PDF);
//	}
//
//	private void addTextUsage(ElementData diskUsage, String companyId)
//	{
//		String gen_txt_sql = "select e.category_id, e.ATTRLONG_textBlock as ATTRFILE_path from categories_gen_text c1, categories_gen_text c2, elements_gen_text e"
//				+ " where c1.Category_id=e.Category_id and c1.Category_ParentID=c2.Category_id and c2.ATTRDROP_CompanyID = " + companyId;
//		addUsage(gen_txt_sql, diskUsage, E_TXT);
//	}
//
//	private void addProductUsage(ElementData diskUsage, String companyId)
//	{
//		String product_sql = "select e.category_id, e.ATTRFILE_image as ATTRFILE_path from categories_products c1, categories_products c2, elements_products e"
//				+ " where c1.Category_id=e.Category_id and c1.Category_ParentID=c2.Category_id and c2.ATTRDROP_CompanyID = " + companyId;
//		addUsage(product_sql, diskUsage, E_PRODUCT);
//	}
//
//	private void addProposalUsage(ElementData diskUsage, String companyId)
//	{
//		String proposal_sql = "select e.category_id, e.ATTRFILE_generatedDocument as ATTRFILE_path from categories_proposals c1, categories_proposals c2, categories_proposals c3, elements_proposals e"
//				+ " where c1.Category_id=e.Category_id and c1.Category_ParentID=c2.Category_id and c2.Category_ParentID=c3.Category_id and c3.ATTRDROP_CompanyID = " + companyId;
//		addUsage(proposal_sql, diskUsage, E_PROPOSAL);
//	}
//
//	private void addProposalAttachmentUsage(ElementData diskUsage, String companyId)
//	{
//		String proposal_attachment_sql = "select pa.category_id, pa.ATTRFILE_attachment as ATTRFILE_path from elements_proposal_attachments pa where ATTRDROP_proposalId in "
//				+ "(select e.Element_id as ATTRFILE_path from categories_proposals c1, categories_proposals c2, categories_proposals c3, elements_proposals e"
//				+ " where c1.Category_id=e.Category_id and c1.Category_ParentID=c2.Category_id and c2.Category_ParentID=c3.Category_id and c3.ATTRDROP_CompanyID =" + companyId + ")";
//		addUsage(proposal_attachment_sql, diskUsage, E_PROPOSAL_ATTACHMENT);
//	}
//
//	private void addUsage(String sql, ElementData diskUsage, String attributeKey)
//	{
//		logger.info("SQL: " + sql);
//		List<Hashtable<String, String>> htList = DataAccess.getInstance()
//				.select(sql, new HashtableMapper());
//		long dirSize = 0;
//		for (Hashtable<String, String> ht : htList)
//		{
//			String path = ht.get("ATTRFILE_path");
//			if (!StringUtils.isEmpty(path)
//					&& StringUtils.countMatches(path, "/") > 3)
//			{
//				File directory = new File(Defaults.getInstance().getStoreDir(), path.substring(0, StringUtils.ordinalIndexOf(path, "/", 3)));
//				if (directory.exists())
//				{
//					dirSize += FileUtils.sizeOfDirectory(directory);
//				}
//			}
//		}
//		BigDecimal sizeBD = BigDecimal.valueOf(dirSize);
//		sizeBD = sizeBD.divide(new BigDecimal(1048576));
//		diskUsage.put(attributeKey, sizeBD.setScale(1, BigDecimal.ROUND_HALF_UP));
//	}
//
//	public double getCurrentDiskUsageGB(String companyId)
//	{
//		try
//		{
//			String total = TransactionDataAccess.getInstance().selectString("select ATTRFLOAT_imageLibrary+ATTRFLOAT_videoLibrary+ATTRFLOAT_pdf+ATTRFLOAT_proposals+ATTRFLOAT_coverPage+ATTRFLOAT_text+ATTRFLOAT_products+ATTRFLOAT_proposalAttachment as total from elements_disk_usage e, categories_disk_usage c1, categories_disk_usage c2 where e.category_id = c2.category_id and c2.category_parentId = c1.category_id and c1.attrdrop_companyId = ? order by element_id desc limit 1", companyId);
//			if (StringUtils.isBlank(total))
//			{
//				return 0d;
//			}
//			BigDecimal sizeBD = new BigDecimal(total);
//			sizeBD = sizeBD.divide(new BigDecimal(1024));
//			return sizeBD.setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
//		}
//		catch (SQLException e)
//		{
//			logger.error("Error:", e);
//			return 0d;
//		}
//
//	}
//}
