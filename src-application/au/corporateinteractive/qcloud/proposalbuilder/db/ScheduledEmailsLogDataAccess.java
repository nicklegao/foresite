package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.util.Date;

import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import webdirector.db.client.ManagedDataAccess;

public class ScheduledEmailsLogDataAccess extends ManagedDataAccess
{
	public static final String MODULE = "SCHEDULEDEMAILSLOG";

	public static final String C_CATEGORY_NAME = "ATTR_categoryName";

	public static final String E_SENT = "ATTRCHECK_sent";
	public static final String E_SENT_TIME = "ATTRDATE_sentTime";
	public static final String E_EMAIL_TYPE = "ATTR_emailType";
	public static final String E_RULE_TYPE = "ATTRDROP_ruleType";
	public static final String E_HEADLINE = "ATTR_Headline";
	public static final String E_SENT_TO = "ATTRDROP_sentTo";
	public static final String E_SENT_TO_COMPANY = "ATTRDROP_sentToCompany";


	public void insertEmailLog(String userEmail, String userId, String emailType, String ruleId, Date sentTime, String companyId, String companyName, boolean sent)
	{
		try (TransactionManager txManager = TransactionManager.getInstance())
		{
			int categoryId = getEmailCategory(companyId, companyName);
			ElementData emailLog = new ElementData();
			emailLog.setParentId(String.valueOf(categoryId));
			emailLog.setName(emailType + " - " + userEmail);
			emailLog.put(E_SENT, sent);
			emailLog.put(E_SENT_TIME, sentTime);
			emailLog.put(E_EMAIL_TYPE, emailType);
			emailLog.put(E_RULE_TYPE, ruleId);
			emailLog.put(E_SENT_TO, userId);
			emailLog.put(E_SENT_TO_COMPANY, companyId);
			emailLog.setLive(true);

			TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
			tma.insertElement(emailLog, MODULE);
			txManager.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private int getEmailCategory(String companyId, String companyName)
	{
		int categoryId = getOrCreateCategory(companyId, companyName, MODULE);
		return categoryId;
	}
}