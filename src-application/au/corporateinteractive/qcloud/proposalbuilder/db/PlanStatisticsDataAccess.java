//package au.corporateinteractive.qcloud.proposalbuilder.db;
//
//import java.util.ArrayList;
//import java.util.Hashtable;
//import java.util.LinkedList;
//import java.util.List;
//
//import org.apache.log4j.Logger;
//
//import au.corporateinteractive.qcloud.proposalbuilder.json.proposal.PlanClass;
//import au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Pricing;
//import au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal;
//import au.net.webdirector.common.datalayer.admin.db.DataAccess;
//import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;
//import webdirector.db.client.ManagedDataAccess;
//
//public class PlanStatisticsDataAccess extends ManagedDataAccess
//{
//	private static Logger logger = Logger.getLogger(PlanStatisticsDataAccess.class);
//	public static final String STATISTICS_DATA_TABLE = "subtable_proposal_plan_statistics";
//
//	// Automatic status change fields:...
//	public static final String S_PROPOSAL_ID = "proposalID";
//	public static final String S_PLAN_CATEGORY_ID = "planCategoryID";
//	public static final String S_PLAN_FOLDER_LEVEL = "planCategoryFolderLevel";
//
//	public List<Hashtable<String, String>> getDataCategories()
//	{
//		DataAccess da = DataAccess.getInstance();
//		List<Hashtable<String, String>> items = da.select(
//				"select p.Category_id, p.ATTR_categoryName "
//						+ "from subtable_proposal_plan_statistics s, categories_plans p "
//						+ "where s.planCategoryID = p.Category_id "
//						+ "group by s.planCategoryID",
//				new String[] {},
//				HashtableMapper.getInstance());
//		return items;
//	}
//
//	public void clearProposalData(String proposalId)
//	{
//		DataAccess da = DataAccess.getInstance();
//		da.updateData("delete from " + STATISTICS_DATA_TABLE + " "
//				+ "where " + S_PROPOSAL_ID + " = ?", new String[] {
//				proposalId
//		});
//	}
//
//	public void updatePlanStatistics(String proposalId, Proposal proposal)
//	{
//		PlanDataAccess pda = new PlanDataAccess();
//		clearProposalData(proposalId);
//
//		List<PlanClass> plans = new ArrayList<>();
//		for (Pricing pricing : proposal.getPricings())
//		{
//			plans.addAll(pricing.getPlans());
//		}
//		List<String> categoryIDs = pda.getProductCategoryIdsForPricingItems(plans);
//		if (categoryIDs.size() > 0)
//		{
//			DataAccess da = DataAccess.getInstance();
//
//			LinkedList<String> preparedParams = new LinkedList<String>();
//			StringBuffer queryBuilder = new StringBuffer();
//			queryBuilder.append("insert into " + STATISTICS_DATA_TABLE + " ");
//			queryBuilder.append("(");
//			queryBuilder.append(S_PROPOSAL_ID).append(",");
//			queryBuilder.append(S_PLAN_CATEGORY_ID).append(",");
//			queryBuilder.append(S_PLAN_FOLDER_LEVEL);
//			queryBuilder.append(")");
//			queryBuilder.append("values");
//			boolean first = true;
//			for (String categoryID : categoryIDs)
//			{
//				if (!first)
//				{
//					queryBuilder.append(",");
//				}
//				queryBuilder.append("(?,?,?)");
//				preparedParams.add(proposalId);
//				preparedParams.add(categoryID);
//				preparedParams.add("2");
//
//				first = false;
//			}
//
//			da.updateData(queryBuilder.toString(), preparedParams.toArray(new String[] {}));
//		}
//	}
//}
