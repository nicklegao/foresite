/**
 * @author Nick Yiming Gao
 * @date 16/01/2017 2:43:42 pm
 */
package au.corporateinteractive.qcloud.proposalbuilder.db;

/**
 * 
 */

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import webdirector.db.client.ManagedDataAccess;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.conf.ModuleAccessConfig;
import au.net.webdirector.dev.annotation.DbColumn;
import au.net.webdirector.dev.annotation.Module;


@Module(name = "EMAIL_LOG")
public class EmailLogDataAccess extends ManagedDataAccess
{

	private static Logger logger = Logger.getLogger(EmailLogDataAccess.class);

	/**
	 * <b>The internal module name</b>
	 */
	public static final String MODULE = "EMAIL_LOG";


	/**
	 * <b>ATTRCHECK_BackupEmail</b><br/>
	 * Display: Backup Email?
	 * <b>CATEGORY LEVEL 1 ONLY</b><br/>
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_BACKUP_EMAIL = "ATTRCHECK_BackupEmail";

	/**
	 * <b>ATTRDROP_CompanyID</b><br/>
	 * Display: Company Id
	 * <b>CATEGORY LEVEL 1 ONLY</b><br/>
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_COMPANY_ID = "ATTRDROP_CompanyID";

	/**
	 * <b>ATTR_categoryName</b><br/>
	 * Display: Company Name
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_CATEGORY_NAME = "ATTR_categoryName";

	/**
	 * <b>ATTR_host</b><br/>
	 * Display: Host
	 * <b>CATEGORY LEVEL 1 ONLY</b><br/>
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_HOST = "ATTR_host";

	/**
	 * <b>ATTR_MailServerSMTP</b><br/>
	 * Display: Smtp Mail Server
	 * <b>CATEGORY LEVEL 1 ONLY</b><br/>
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_MAIL_SERVER_SMTP = "ATTR_MailServerSMTP";

	/**
	 * <b>ATTR_PasswordSMTPserver</b><br/>
	 * Display: Smtp Server Password
	 * <b>CATEGORY LEVEL 1 ONLY</b><br/>
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PASSWORD_SMTPSERVER = "ATTR_PasswordSMTPserver";

	/**
	 * <b>ATTR_SenderAddress</b><br/>
	 * Display: Sender Address
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SENDER_ADDRESS = "ATTR_SenderAddress";

	/**
	 * <b>ATTR_SenderName</b><br/>
	 * Display: Sender Name
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SENDER_NAME = "ATTR_SenderName";

	/**
	 * <b>ATTR_UsernameSMTPserver</b><br/>
	 * Display: Smtp Server Username
	 * <b>CATEGORY LEVEL 1 ONLY</b><br/>
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_USERNAME_SMTPSERVER = "ATTR_UsernameSMTPserver";

	/**
	 * <b>ATTR_webHook</b><br/>
	 * Display: Web Hook
	 * <b>CATEGORY LEVEL 1 ONLY</b><br/>
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_WEB_HOOK = "ATTR_webHook";

	/**
	 * <b>ATTRFILE_EmailBackup</b><br/>
	 * Display: Email Backup
	 */
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_EMAIL_BACKUP = "ATTRFILE_EmailBackup";

	/**
	 * <b>ATTR_EmailSubject</b><br/>
	 * Display: Email Subject
	 */
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_EMAIL_SUBJECT = "ATTR_EmailSubject";

	/**
	 * <b>ATTR_Headline</b><br/>
	 * Display: Email File Name
	 */
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_HEADLINE = "ATTR_Headline";

	public List<CategoryData> getUsingMandrillCompanies()
	{
		try
		{
			return ModuleAccess.getInstance().getCategories("select * from categories_email_log where ATTR_MailServerSMTP like 'smtp.mandrillapp.com%' and live = 1");
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return new ArrayList<CategoryData>();
		}
	}

	public Properties getCompanySettings(String companyId, String emailSubject, boolean shouldSend)
	{
		try
		{
			Properties prop = new Properties();
			ModuleAccess ma = ModuleAccess.getInstance();
			ma.setConfig(ModuleAccessConfig.logicalLive());
			CategoryData settings = ma.getCategoryByField(MODULE, C1_COMPANY_ID, companyId);
			if (settings == null)
			{
				return prop;
			}

			if (StringUtils.isNotBlank(settings.getString(C1_USERNAME_SMTPSERVER)))
			{
				prop.put("UsernameSMTPserver", settings.getString(C1_USERNAME_SMTPSERVER));
			}
			if (StringUtils.isNotBlank(settings.getString(C1_PASSWORD_SMTPSERVER)))
			{
				prop.put("PasswordSMTPserver", settings.getString(C1_PASSWORD_SMTPSERVER));
			}
			if (StringUtils.isNotBlank(settings.getString(C1_MAIL_SERVER_SMTP)))
			{
				prop.put("MailServerSMTP", settings.getString(C1_MAIL_SERVER_SMTP));
			}
			if (settings.getBoolean(C1_BACKUP_EMAIL) && shouldSend)
			{
				Date now = new Date();
				String date = new SimpleDateFormat("yyyy-MM-dd").format(now);
				String fileName = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(now) + "-" + RandomStringUtils.randomAlphanumeric(8) + ".eml";
				int c2id = getOrCreateCategory(date, Integer.valueOf(settings.getId()), 2, MODULE);
				Hashtable<String, String> log = standardCreateElementHashtable(c2id, fileName);
				insertNotNull(log, E_EMAIL_SUBJECT, emailSubject);
				int eid = createElement(log, MODULE);
				String logFilePath = String.format("/%s/%s/%s/%s", MODULE, eid, E_EMAIL_BACKUP, fileName);
				prop.put("BackupMailFile", logFilePath);
				insertNotNull(log, E_ID, eid);
				insertNotNull(log, E_EMAIL_BACKUP, logFilePath);
				updateElement(log, MODULE);
				prop.put("BackupMailFile", Defaults.getInstance().getStoreDir() + logFilePath);
			}
			if (StringUtils.isNotBlank(settings.getString(C1_SENDER_ADDRESS)))
			{
				prop.put("SenderAddress", settings.getString(C1_SENDER_ADDRESS));
			}
			if (StringUtils.isNotBlank(settings.getString(C1_SENDER_NAME)))
			{
				prop.put("SenderName", settings.getString(C1_SENDER_NAME));
			}
			return prop;
		}
		catch (Exception e)
		{
			logger.error("Error", e);
			return new Properties();
		}
	}
}
