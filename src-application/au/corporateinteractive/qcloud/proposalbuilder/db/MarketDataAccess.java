//package au.corporateinteractive.qcloud.proposalbuilder.db;
//
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.Hashtable;
//import java.util.List;
//
//import org.apache.log4j.Logger;
//
//import au.net.webdirector.common.datalayer.client.CategoryData;
//import au.net.webdirector.common.datalayer.client.ElementData;
//import au.net.webdirector.common.datalayer.client.ModuleAccess;
//import au.net.webdirector.common.datalayer.client.conf.ModuleAccessConfig;
//import au.net.webdirector.dev.annotation.DbColumn;
//import au.net.webdirector.dev.annotation.Module;
//import webdirector.db.client.ManagedDataAccess;
//
//import javax.servlet.http.HttpServletRequest;
//
///**
// * @author Sishir
// *
// */
//@Module(name = "market")
//public class MarketDataAccess extends ManagedDataAccess
//{
//	Logger logger = Logger.getLogger(MarketDataAccess.class);
//
//	public static final String MARKET_MODULE = "MARKET";
//
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_APP_NAME = "ATTR_Headline";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_VERSION = "ATTR_version";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_SUBSCRIBERS = "ATTRINTEGER_subscribers";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_UPDATE_ON = "ATTRDATE_updatedOn";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_ICON = "ATTRFILE_appIcon";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE1 = "ATTRFILE_appImage1";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE2 = "ATTRFILE_appImage2";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE3 = "ATTRFILE_appImage3";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE4 = "ATTRFILE_appImage4";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE5 = "ATTRFILE_appImage5";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_DESC = "ATTRLONG_appDescription";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_RELEASE = "ATTRCHECK_release";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_APP_DETAIL = "ATTRLONG_appDetail";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_TERMS = "ATTRLONG_terms";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_VIDEO_LINK = "ATTR_video_link";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE1_DESC = "ATTR_image1_desc";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE2_DESC = "ATTR_image2_desc";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE3_DESC = "ATTR_image3_desc";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE4_DESC = "ATTR_image4_desc";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE5_DESC = "ATTR_image5_desc";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE1_DESC_HTML = "ATTRLONG_image1_desc_html";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE2_DESC_HTML = "ATTRLONG_image2_desc_html";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE3_DESC_HTML = "ATTRLONG_image3_desc_html";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE4_DESC_HTML = "ATTRLONG_image4_desc_html";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE5_DESC_HTML = "ATTRLONG_image5_desc_html";
//
//	public static MarketDataAccess getInstance()
//	{
//		return new MarketDataAccess();
//	}
//
//	public ElementData getMarketApp(String appId)
//	{
//		String query = "select * from elements_market where element_id=?";
//		try
//		{
//			return ModuleAccess.getInstance().getElements(query, new String[] { appId }).get(0);
//		}
//		catch (SQLException e)
//		{
//			logger.error("Error", e);
//		}
//		return null;
//	}
//
//	public List<CategoryData> getMarketAppCategories()
//	{
//		List<CategoryData> appCategories = new ArrayList<CategoryData>();
//		try
//		{
//			appCategories = ModuleAccess.getInstance().getCategoriesByParentId(MARKET_MODULE, "0");
//		}
//		catch (SQLException e)
//		{
//			logger.error("Error", e);
//		}
//		return appCategories;
//	}
//
//	public List<ElementData> getMarketApps(HttpServletRequest request, String country)
//	{
//		String hostName = request.getServerName();
//		List<ElementData> appList = new ArrayList<ElementData>();
//		String query = "";
//		if (!hostName.toLowerCase().contains("awsbeta")) {
//			query = "select em.* from elements_market em, drop_down_multi_selections ddm, "
//					+ "categories_market_subscriptions cms, elements_market_subscriptions ems "
//					+ "where em.element_id=ems.ATTRDROP_appId and em.ATTRCHECK_BetaOnly=0 and ems.Category_id=cms.Category_id "
//					+ "and cms.Category_id=ddm.element_id and ddm.module_name='market_subscriptions' "
//					+ "and ddm.module_type='category' and ddm.ATTRMULTI_NAME='ATTRMULTI_SupportedCountries' "
//					+ "and ddm.selected_value=? and " + standardLiveClauseForCustomQuery("em");
//		} else {
//			query = "select em.* from elements_market em, drop_down_multi_selections ddm, "
//					+ "categories_market_subscriptions cms, elements_market_subscriptions ems "
//					+ "where em.element_id=ems.ATTRDROP_appId and ems.Category_id=cms.Category_id "
//					+ "and cms.Category_id=ddm.element_id and ddm.module_name='market_subscriptions' "
//					+ "and ddm.module_type='category' and ddm.ATTRMULTI_NAME='ATTRMULTI_SupportedCountries' "
//					+ "and ddm.selected_value=? and " + standardLiveClauseForCustomQuery("em");
//		}
//		try
//		{
//			logger.error(query);
//			logger.error(country);
//			appList = ModuleAccess.getInstance().getElements(query, new String[] { country });
//			logger.error(appList.size());
//		}
//		catch (SQLException e)
//		{
//			logger.error("Error", e);
//		}
//		return appList;
//	}
//
//	public List<ElementData> getMarketApps(String country, String categoryId)
//	{
//		List<ElementData> appList = new ArrayList<ElementData>();
//		String query = "select em.* from elements_market em, drop_down_multi_selections ddm, "
//				+ "categories_market_subscriptions cms, elements_market_subscriptions ems "
//				+ "where em.element_id=ems.ATTRDROP_appId and ems.Category_id=cms.Category_id "
//				+ "and cms.Category_id=ddm.element_id and ddm.module_name='market_subscriptions' "
//				+ "and ddm.module_type='category' and ddm.ATTRMULTI_NAME='ATTRMULTI_SupportedCountries' "
//				+ "and ddm.selected_value=? and em.Category_id=? and " + standardLiveClauseForCustomQuery("em");
//		try
//		{
//			appList = ModuleAccess.getInstance().getElements(query, new String[] { country, categoryId });
//		}
//		catch (SQLException e)
//		{
//			logger.error("Error", e);
//		}
//		return appList;
//	}
//
//	public ElementData getApplication(String appId, String country)
//	{
//
//		ElementData app = null;
//		try
//		{
//			ModuleAccess ma = ModuleAccess.getInstance();
//			ma.setConfig(ModuleAccessConfig.logicalLive());
//
//			String query = "Select em.*, cms.ATTR_categoryName countryCode, cms.Category_id cId, cms.ATTR_CurrencyCode currencyCode, "
//					+ "cms.ATTR_CurrencySymbol currencySymbol, cms.ATTR_Country country, ems.ATTRCURRENCY_price price, ems.ATTRCHECK_PerUserMonth, "
//					+ "ems.ATTR_invoiceLineDesc invoiceLineDesc, ems.ATTR_StripeID stripeId "
//					+ "from elements_market em, elements_market_subscriptions ems, categories_market_subscriptions cms, drop_down_multi_selections ddm "
//					+ "where em.Element_id=ems.ATTRDROP_appId and ems.Category_id=cms.Category_id and cms.Category_id=ddm.element_id "
//					+ "and ddm.module_name='market_subscriptions' and ddm.module_type='category' and ddm.ATTRMULTI_NAME='ATTRMULTI_SupportedCountries' "
//					+ "and em.Element_id=? and ddm.selected_value=?";
//
//			app = ma.getElements(query, new String[] { appId, country }).get(0);
//		}
//		catch (SQLException e)
//		{
//			logger.error("Error", e);
//		}
//		return app;
//	}
//
//	public ElementData getAppPlan(String appId, String country)
//	{
//
//		ElementData app = null;
//		try
//		{
//			ModuleAccess ma = ModuleAccess.getInstance();
//			ma.setConfig(ModuleAccessConfig.logicalLive());
//
//			String query = "Select ems.*, cms.ATTR_categoryName countryCode, cms.Category_id cId, cms.ATTR_CurrencyCode currencyCode, "
//					+ "cms.ATTR_CurrencySymbol currencySymbol, cms.ATTR_Country country, ems.ATTRCURRENCY_price price, "
//					+ "ems.ATTR_invoiceLineDesc invoiceLineDesc, ems.ATTR_StripeID stripeId "
//					+ "from elements_market em, elements_market_subscriptions ems, categories_market_subscriptions cms, drop_down_multi_selections ddm "
//					+ "where em.Element_id=ems.ATTRDROP_appId and ems.Category_id=cms.Category_id and cms.Category_id=ddm.element_id "
//					+ "and ddm.module_name='market_subscriptions' and ddm.module_type='category' and ddm.ATTRMULTI_NAME='ATTRMULTI_SupportedCountries' "
//					+ "and em.Element_id=? and ddm.selected_value=?";
//
//			app = ma.getElements(query, new String[] { appId, country }).get(0);
//		}
//		catch (SQLException e)
//		{
//			logger.error("Error", e);
//		}
//		return app;
//	}
//
//	public ElementData getSubscribedApp(String subscriptionId)
//	{
//		ElementData app = null;
//		try
//		{
//			ModuleAccess ma = ModuleAccess.getInstance();
//			String query = "select em.* from elements_market em, elements_market_subscribers ems "
//					+ "where em.Element_id=ems.ATTRDROP_application and ems.ATTR_subscriptionId=?";
//			app = ma.getElements(query, new String[] { subscriptionId }).get(0);
//		}
//		catch (SQLException e)
//		{
//			logger.error("Error", e);
//		}
//		return app;
//	}
//
//	public void updateSubscribersCount(String appId)
//	{
//		ModuleAccess ma = ModuleAccess.getInstance();
//		try
//		{
//			ElementData app = ma.getElementById(MARKET_MODULE, appId);
//			int subscribers = app.getInt(E_SUBSCRIBERS);
//			subscribers++;
//			Hashtable<String, String> updateApp = new Hashtable<String, String>();
//			updateApp.put(E_ID, app.getId());
//			updateApp.put(E_SUBSCRIBERS, String.valueOf(subscribers));
//			updateElement(updateApp, MARKET_MODULE);
//		}
//		catch (SQLException e)
//		{
//			logger.error("error: " + e);
//		}
//
//
//	}
//
//}
