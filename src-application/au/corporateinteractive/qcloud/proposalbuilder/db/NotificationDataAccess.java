///**
// *
// */
//package au.corporateinteractive.qcloud.proposalbuilder.db;
//
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//
//import au.corporateinteractive.qcloud.proposalbuilder.model.Notification;
//import au.net.webdirector.common.datalayer.admin.db.DataAccess;
//import au.net.webdirector.common.datalayer.base.db.mapper.ElementDataMapper;
//import au.net.webdirector.common.datalayer.client.ElementData;
//import au.net.webdirector.common.datalayer.client.ModuleAccess;
//import au.net.webdirector.common.datalayer.client.conf.ModuleAccessConfig;
//
///**
// * @author Alberto
// *
// */
//public class NotificationDataAccess {
//	private static Logger logger = Logger
//			.getLogger(NotificationDataAccess.class);
//
//	public static class Type {
//		public static final String INFORMATION = "info";
//		public static final String ALERT = "alert";
//		public static final String HELP = "help";
//	}
//
//	public static class Occurrence {
//		public static final String MULTIPLE_OCCURRENCES = "multiple";
//		public static final String SINGLE_OCCURRENCE = "single";
//	}
//
//	public static final String MODULE = "NOTIFICATIONS";
//	public static final String C_CATEGORY_NAME = "ATTR_categoryName";
//
//	public static final String E_OCCURRENCE = "ATTRDROP_occurrence";
//	public static final String E_TYPE = "ATTRDROP_type";
//	public static final String E_CONTENT = "ATTRLONG_content";
//	public static final String E_HEADLINE = "ATTR_Headline";
//
//	public List<Notification> getDailyNotificationsByUserId(String userId) {
//
//		List<Notification> notifications = new ArrayList<>();
//		Date today = new Date();
//
//		UserAccountDataAccess uda = new UserAccountDataAccess();
//
//		List<ElementData> userDataAccessList = DataAccess.getInstance().select(
//				"SELECT * FROM subtable_users_reporting where id = ?",
//				new String[] { userId }, new ElementDataMapper());
//
//		if (null != userDataAccessList && userDataAccessList.size() > 0) {
//			// Retrieve last time the user logged in.
//			Date lastLoggedIn = new Date(0); // dawn of time
//			try {
//				lastLoggedIn = uda.dbDateFormat
//						.parse((userDataAccessList.get(0))
//								.getString(UserAccountReportingDataAccess.REPORT_LAST_LOGIN));
//
//				Date lastViewedNotifications = new Date(0); // dawn of time
//				String lastViewedNotificationsString = (userDataAccessList
//						.get(0))
//						.getString(UserAccountReportingDataAccess.REPORT_LAST_VIEWED_NOTIFICATIONS);
//				if (!lastViewedNotificationsString.isEmpty()) {
//					lastViewedNotifications = uda.dbDateFormat
//							.parse(lastViewedNotificationsString);
//				}
//
//				ModuleAccess ma = ModuleAccess.getInstance();
//				ma.setConfig(ModuleAccessConfig.logicalLive(),
//						ModuleAccessConfig.orderby("Live_date", "DESC"));
//				List<ElementData> notificationsAvailable = ma.getElements(
//						MODULE, new ElementData());
//
//				if (null != notificationsAvailable
//						&& notificationsAvailable.size() > 0) {
//					for (ElementData e : notificationsAvailable) {
//						notifications.add(new Notification(e));
//					}
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//
//		}
//
//		return notifications;
//	}
//
//	public Notification getNotificationById(String id) {
//		if (StringUtils.isNotBlank(id)) {
//			try {
//				ElementData notificationData = ModuleAccess.getInstance()
//						.getElementById(NotificationDataAccess.MODULE, id);
//				return new Notification(notificationData);
//			} catch (SQLException e) {
//				logger.fatal("Oh NO!", e);
//			}
//		}
//		return null;
//	}
//}
