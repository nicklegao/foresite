/**
 * @author Jaysun Lee
 * @date 22 Jan 2016
 */
package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import webdirector.db.client.ManagedDataAccess;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.dev.annotation.DbColumn;
import au.net.webdirector.dev.annotation.Module;

@Module(name = "INVOICES")
public class InvoiceDataAccess extends ManagedDataAccess
{
	private static Logger logger = Logger.getLogger(InvoiceDataAccess.class);

	/**
	 * <b>The internal module name</b>
	 */
	public static final String MODULE = "INVOICES";


	/**
	 * <b>ATTRDROP_companyID</b><br/>
	 * Display: Company Id
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_COMPANY_ID = "ATTRDROP_companyID";

	/**
	 * <b>ATTR_categoryName</b><br/>
	 * Display: Company Name
	 */
	public static final String C_CATEGORY_NAME = "ATTR_categoryName";


	/**
	 * <b>ATTRFILE_InvoicePDF</b><br/>
	 * Display: Invoice Pdf
	 */
	public static final String E_INVOICE_PDF = "ATTRFILE_InvoicePDF";

	/**
	 * <b>ATTR_Headline</b><br/>
	 * Display: Invoice Number
	 */
	public static final String E_HEADLINE = "ATTR_Headline";

	/**
	 * <b>ATTR_Invoice</b><br/>
	 * Display: Invoice
	 */
	public static final String E_INVOICE = "ATTR_Invoice";

	/**
	 * <b>ATTR_StripeID</b><br/>
	 * Display: Stripe Id
	 */
	public static final String E_STRIPE_ID = "ATTR_StripeID";

	public static final String E_PAID = "ATTRCHECK_Paid";

	public static final String E_DATE = "ATTRDATE_Date";

	public static final String E_AMOUNT = "ATTRCURRENCY_Amount";

	public static final String E_CURRENCY = "ATTR_Currency";


	private CategoryData getCompanyFolder(String companyId, String companyName, TransactionManager txManager)
	{
		try
		{
			TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
			List<CategoryData> list = ma.getCategories("select * from categories_INVOICES where attrdrop_companyId = ?", companyId);
			if (list.size() > 0)
			{
				return list.get(0);
			}
			CategoryData company = new CategoryData();
			company.setLive(true);
			company.setFolderLevel(1);
			company.setParentId("0");
			company.put(C_COMPANY_ID, companyId);
			company.setName(companyName);
			company = ma.insertCategory(company, MODULE);
			return company;
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return null;
		}
	}

	public ElementData saveInvoice(String companyId, String companyName, String invoiceId, String invoiceJson, Date date, double amount, String currencyCode)
	{
		try (TransactionManager txManager = TransactionManager.getInstance())
		{
			TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
			List<ElementData> list = ma.getElements("select * from elements_INVOICES where " + E_STRIPE_ID + " = ?", invoiceId);
			if (list.size() > 0)
			{
				ElementData invoice = list.get(0);
				invoice.put(E_INVOICE, invoiceJson);
				invoice.put(E_DATE, date);
				invoice.put(E_AMOUNT, amount);
				invoice.put(E_CURRENCY, currencyCode.toUpperCase());
				ma.updateElement(invoice, MODULE);
				txManager.commit();
				return invoice;
			}
			CategoryData company = getCompanyFolder(companyId, companyName, txManager);
			ElementData invoice = new ElementData();
			invoice.setParentId(company.getId());
			invoice.put(E_INVOICE, invoiceJson);
			invoice.put(E_STRIPE_ID, invoiceId);
			invoice.put(E_DATE, date);
			invoice.put(E_AMOUNT, amount);
			invoice.put(E_CURRENCY, currencyCode.toUpperCase());
			invoice = ma.insertElement(invoice, MODULE);
			invoice.setName(new DecimalFormat("000000").format(Long.parseLong(invoice.getId())));
			ma.updateElement(invoice, MODULE);
			txManager.commit();
			return invoice;
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return null;
		}
	}
}
