package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;

import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.util.DataMapLegacyConverter;
import au.corporateinteractive.qcloud.proposalbuilder.db.util.TasksStatus;
import au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions;
import au.corporateinteractive.qcloud.proposalbuilder.model.dashboard.DashboardSummary;
import au.corporateinteractive.qcloud.proposalbuilder.service.SecurityKeyService;
import au.corporateinteractive.qcloud.subscription.SubscriptionPlan;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.MultiOptions;
import au.net.webdirector.common.datalayer.base.db.entity.TextFile;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.IntegerMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.common.utils.module.StoreUtils;
import au.net.webdirector.common.utils.password.PasswordUtils;
import au.net.webdirector.dev.annotation.DbColumn;
import au.net.webdirector.dev.annotation.Module;
import webdirector.db.client.ManagedDataAccess;


@Module(name = "USERACCOUNTS")
public class UserAccountDataAccess extends ManagedDataAccess
{
	Logger logger = Logger.getLogger(UserAccountDataAccess.class);

	public static final String USER_MODULE = "USERACCOUNTS";

	//Profile Fields
	// @DbColumn(type = DbColumn.TYPE_ELEMENT)
	// public static final String E_EMAIL = "ATTR_EmailAddress";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_NAME = "ATTR_name";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_SURNAME = "ATTR_surname";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_EMAIL_ADDRESS = "ATTR_EmailAddress";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_PASSWORD_EXPIRE = "ATTRDATE_passwordExpire";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_PASSWORD = "ATTR_password";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_RESET_PASSWORD = "ATTR_resetPassword";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_MOBILE_NUMBER = "ATTR_mobileNumber";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_USER_TYPE = "ATTRDROP_userType";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_USER_ROLES = "ATTRMULTI_roles";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_STORE_NAME = "ATTR_storeName";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_MANAGES_MULTI = "ATTRMULTI_manages";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_APPROVAL_USERS_MULTI = "ATTRMULTI_approvalUsers";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_EMAIL_DAILY_SUMMARY = "ATTRCHECK_notifyDailyEmailSummary";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_EMAIL_NEW_COMMENT = "ATTRCHECK_notifyNewComment";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_EMAIL_HANDOVER = "ATTRCHECK_notifyHandover";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_EMAIL_STATUS_CHANGE = "ATTRCHECK_notifyProposalStatusChange";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_EMAIL_OPENED = "ATTRCHECK_notifyProposalOpened";

	//BILLING EMAIL TO
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_BILLING_USERS = "ATTRMULTI_BillingUsers";

	// Address fields
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_ABN = "ATTR_abnacn";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_ADDRESS_LINE_1 = "ATTR_addressLine1";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_ADDRESS_LINE_2 = "ATTR_addressLine2";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_ADDRESS_CITY_SUBURB = "ATTR_citySuburb";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_ADDRESS_STATE = "ATTR_state";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_ADDRESS_POSTCODE = "ATTR_postcode";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_ADDRESS_COUNTRY = "ATTRDROP_country";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_PRICING_PLAN = "ATTRDROP_pricingPlan";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_TRIAL_PLAN_EXPIRY = "ATTRDATE_trialPlanExpiry";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_CRM_CONTRACT_ID = "ATTRINTEGER_crmContractId";

	// Company style settings
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_COVER_TITLE_FONT = "ATTRDROP_cover_title_font";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_COVER_TITLE_SIZE = "ATTRINTEGER_cover_title_size";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_COVER_COMPANY_FONT = "ATTRDROP_cover_company_font";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_COVER_COMPANY_SIZE = "ATTRINTEGER_cover_company_size";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_BODY_FONT = "ATTRDROP_bodyFont";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_BODY_SIZE = "ATTRINTEGER_bodySize";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_BODY_COLOR = "ATTR_bodyColor";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SECTION_FONT = "ATTRDROP_sectionFont";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SECTION_SIZE = "ATTRINTEGER_sectionSize";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SECTION_HEADER_COLOR = "ATTR_sectionHeaderColor";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SECTION_UPPERCASE = "ATTRCHECK_sectionUppercase";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SECTION_UNDERLINE = "ATTRCHECK_sectionUnderline";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SECTION_BOLD = "ATTRCHECK_sectionBold";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SECTION_CUSTOM_FONT_COLOR = "ATTRCHECK_sectionCustomFontColor";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H1_FONT = "ATTRDROP_h1Font";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H1_SIZE = "ATTRINTEGER_h1Size";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H1_HEADER_COLOR = "ATTR_h1HeaderColor";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H1_UPPERCASE = "ATTRCHECK_h1Uppercase";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H1_UNDERLINE = "ATTRCHECK_h1Underline";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H1_BOLD = "ATTRCHECK_h1Bold";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H1_CUSTOM_FONT_COLOR = "ATTRCHECK_h1CustomFontColor";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H2_FONT = "ATTRDROP_h2Font";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H2_SIZE = "ATTRINTEGER_h2Size";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H2_HEADER_COLOR = "ATTR_h2HeaderColor";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H2_UPPERCASE = "ATTRCHECK_h2Uppercase";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H2_UNDERLINE = "ATTRCHECK_h2Underline";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H2_BOLD = "ATTRCHECK_h2Bold";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H2_CUSTOM_FONT_COLOR = "ATTRCHECK_h2CustomFontColor";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H3_FONT = "ATTRDROP_h3Font";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H3_SIZE = "ATTRINTEGER_h3Size";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H3_HEADER_COLOR = "ATTR_h3HeaderColor";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H3_UPPERCASE = "ATTRCHECK_h3Uppercase";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H3_UNDERLINE = "ATTRCHECK_h3Underline";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H3_BOLD = "ATTRCHECK_h3Bold";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H3_CUSTOM_FONT_COLOR = "ATTRCHECK_h3CustomFontColor";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_TABLE_STYLE_LINE = "ATTR_tableStyleLine";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_TABLE_STYLE_COLOR = "ATTR_tableStyleColor";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_TABLE_STYLE_WEIGHT = "ATTRINTEGER_tableStyleWeight";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PALETTE1 = "ATTR_palette1";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PALETTE2 = "ATTR_palette2";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PALETTE3 = "ATTR_palette3";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PALETTE4 = "ATTR_palette4";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_BACKGROUND_COLOUR = "ATTR_backgroundColour";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SECTION_BACKGROUND_COLOUR = "ATTR_sectionBackgroundColour";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_LOGO = "ATTRFILE_Logo";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_EXPIRY_DATE = "ATTR_expiryDate";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PASSWORD_EXPIRY_DATE = "ATTRINTEGER_passwordExpiryDate";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_LOCALE = "ATTR_locale";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_DATE_FORMAT = "ATTR_DateFormat";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_CURRENCY_TYPE = "ATTRDROP_currency";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ORDER_NOW_REDIRECT_LINK = "ATTR_orderNowRedirectLink";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ORDER_NOW_LABEL = "ATTR_orderNowLabel";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_API_ID = "ATTR_apiId";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_API_KEY = "ATTR_apiKey";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_CUSTOM_FIELDS = "ATTRTEXT_customFields";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PRICING_DISCOUNTS = "ATTRTEXT_pricingThreshold";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PRICING_TEMPLATE = "ATTR_pricingTemplateName";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_CUSTOM_LABELS = "ATTRTEXT_customLabels";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_TABLE_STYLE = "ATTRDROP_tableStyle";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_TAB_STYLE = "ATTRDROP_tabStyle";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_SHOW_TABS = "ATTRCHECK_showTabs";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_HIDE_COVERPAGE = TUserAccountDataAccess.C1_PROPOSAL_HIDE_COVERPAGE;
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_HIDE_COVERPAGE_PROPOSAL_INFO = TUserAccountDataAccess.C1_PROPOSAL_HIDE_COVERPAGE_PROPOSAL_INFO;
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_HIDE_COVERPAGE_SENDER_INFO = TUserAccountDataAccess.C1_PROPOSAL_HIDE_COVERPAGE_SENDER_INFO;
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_HIDE_COVERPAGE_BOX_BACKGROUND = TUserAccountDataAccess.C1_PROPOSAL_HIDE_COVERPAGE_BOX_BACKGROUND;
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_COVERPAGE_FONT_COLOR = TUserAccountDataAccess.C1_PROPOSAL_COVERPAGE_FONT_COLOR;
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_PRICING_STYLE = TUserAccountDataAccess.C1_PROPOSAL_PRICING_STYLES;

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_CONTENT_PADDING_LEFT = TUserAccountDataAccess.C1_CONTENT_PADDING_LEFT;
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_CONTENT_PADDING_TOP = TUserAccountDataAccess.C1_CONTENT_PADDING_TOP;
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_CONTENT_PADDING_RIGHT = TUserAccountDataAccess.C1_CONTENT_PADDING_RIGHT;
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_CONTENT_PADDING_BOTTOM = TUserAccountDataAccess.C1_CONTENT_PADDING_BOTTOM;

	//This is a check to override a patch that stops users from editing each others proposals without handing over
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String PATCH_ALLOW_TEAM_EDITING = "ATTRCHECK_teamCanEdit";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String ALLOW_ENCRYPTED_DATA_SYNC = "ATTRCHECK_encryptedDataSync";
	// Billing info fields
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ACTIVATED = "ATTRCHECK_Activated";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_BILLING_START_DATE = "ATTRDATE_BillingStartDate";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PREAPPROVAL_KEY = "ATTR_PreapprovalKey";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_BILLING_PERIOD = "ATTRDROP_BillingPeriod";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_LAST_BILLING_DATE = "ATTRDATE_LastBillingDate";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_STATUS = "ATTRDROP_Status";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_RESELLER = "ATTRDROP_Reseller";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PAY_OPTION = "ATTRDROP_PayOption";


	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_HIDE_TERMS_ALWAYS = "ATTRCHECK_hideTermsAlways";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ESTIMATED_VALUE = "ATTRCHECK_estimatedValue";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ROUNDING = "ATTRCHECK_roundingValue";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ENABLE_TAX = "ATTRCHECK_enableTax";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_TAX_LABEL = "ATTR_taxLabel";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_TAX_PERCENTAGE = "ATTR_taxPercentage";


	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_DELETE_ITINERARY_SINGLE = "ATTRINTEGER_deleteInDaysSingle";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_DELETE_ITINERARY_RETURN = "ATTRINTEGER_deleteInDaysReturn";


	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_STRIPE_ID = "ATTR_StripeID";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_MULTI_PRICING = "ATTRCHECK_multiPricing";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PRICING_TITLE = "ATTR_pricingTitle";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_TIME_ZONE = "ATTR_timeZone";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_TEXT_MAPPING_FILE_NAME = "ATTR_textMappingFileName";

	//Company Support
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_SUPPORT_AUTH_TIME = "ATTRDATE_supportAuthTime";

	//User account type constants
	public static final String LOCAL_AREA_MANAGER = "localAreaManager";
	public static final String STORE_MANAGER = "storeManager";
	public static final String CONSULTANT = "consultant";

	public static final String ADMIN = "admin";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ENABLE_ADDITION = "ATTRCHECK_enableAddition";

	private StoreUtils su = new StoreUtils();
	private Defaults d = Defaults.getInstance();

	//Saved Dashboard settings
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String DASHBOARD_RECORD = "ATTR_DashboardRecord";

	//Default Email Status
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String EMAIL_STATUS_DEFAULT = "ATTR_defaultEmailStatus";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String EMAIL_STATUS_STRING = "ATTR_emailStatusString";

	//User Chat information
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String SOCKET_SUPPORT_ADMIN = "ATTRCHECK_SupportAdmin";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String MOBILE_LAST_ACCESS = "ATTRDATE_LastAccess";

	//Users QuoteCloud Tutorials --> Can be reset
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String TUTORIAL_FIRST_ACCESS = "ATTRCHECK_FirstAccessTutorial";

	// travel itineraries
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ITINERARY_TEMPLATE = "ATTRDROP_itineraryTemplateId";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_TEMPLATE = "ATTRDROP_proposalTemplateId";

	//Travelport details
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_TRAVELPORT_BRANCH_CODE = "ATTR_travelport_branchCode";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_TRAVELPORT_CONSULTANT_ID = "ATTR_travelport_consultantId";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_TRAVELPORT_CONSULTANT_PASSWORD = "ATTR_travelport_consultantPassword";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ADD_USER_PLAN_QTY = "ATTRINTEGER_AddUserPlanQty";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ADD_DATA_PLAN_QTY = "ATTRINTEGER_AddDataPlanQty";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ALERTED_DATA_PLAN_QTY = "ATTRINTEGER_AlertedDataPlanQty";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C2_STRIPE_PK = "ATTR_StripePK";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C2_STRIPE_SK = "ATTR_StripeSK";

	//QuoteCloud Support
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SUPPORT_TIME = "ATTRDATE_supportAuthTime";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ENCRYPT_CLIENT_DATA = "ATTRCHECK_encryptClientData";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_DELETION_ALERT_EMAIL_SENT = "ATTRDATE_deletionAlertEmailSent";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_ZERO_RISK_APIKEY = "ATTR_ZeroRiskAPIKey";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_ZERO_RISK_APISECRET = "ATTR_ZeroRiskAPISecret";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_SABRE_TYPE = "ATTRDROP_SabreType";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_SABRE_SAM_KEY = "ATTR_SabreSamKey";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_SABRE_SAM_SECRET = "ATTR_SabreSamSecret";


	public enum AuthenticationStatus
	{
		CHANGE_PASSWORD(false),
		TWO_FACTOR(false),
		TWO_FACTOR_INCORRECT(false),
		// BLOCKED(false),
		OK_USER(true),
		OK_AUTO(true),
		// OK_ADMIN(true),
		REJECTED(false),
		UNACTIVATED(false);

		private boolean isOK;

		private AuthenticationStatus(boolean isOK)
		{
			this.isOK = isOK;
		}

		public boolean isOK()
		{
			return isOK;
		}
	}

	public class AuthenticationResponse
	{
		public AuthenticationStatus status;
		public String userId;
		public String companyID;
		public String message;
		public ElementData userData;

		public AuthenticationResponse(AuthenticationStatus status, String userId)
		{
			this.status = status;
			this.userId = userId;
			this.message = "";
		}

		public AuthenticationResponse(AuthenticationStatus status, String userId, String message)
		{
			this.status = status;
			this.userId = userId;
			this.message = message;
		}

		public AuthenticationResponse(AuthenticationStatus status, String userId, String companyId, ElementData userData)
		{
			this.status = status;
			this.userId = userId;
			this.companyID = companyId;
			this.message = "";
			this.userData = userData;
		}
	}

	public AuthenticationResponse authenticate(String username, String password) throws SQLException
	{

		ElementData user = ModuleAccess.getInstance().getElementByName(USER_MODULE, username);
		if (user != null)
		{
			boolean twoFactorAuthenticated = false || "dev".equals(System.getProperty("au.net.webdirector.serverMode"));
			return authenticate(username, password, twoFactorAuthenticated);
		}
		return new AuthenticationResponse(AuthenticationStatus.REJECTED, "User not found.");
	}

	public AuthenticationResponse authenticate(String username, String password, boolean twoFactorAuthenticated) throws SQLException
	{

		ElementData user = ModuleAccess.getInstance().getElementByName(USER_MODULE, username);
		if (user == null)
		{
			return new AuthenticationResponse(AuthenticationStatus.REJECTED, "User not found.");
		}

		String resetPassword = user.getString(E_RESET_PASSWORD).trim();
		Date now = new Date();
		Date passwordExpiry = now;
		try
		{
			// Rest now if any issues parsing this value
			passwordExpiry = dbDateFormat.parse(user.getString(E_PASSWORD_EXPIRE));
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}

		// authenticate the user
		if (resetPassword.length() > 0)
		{
			// User has to change their password
			if (resetPassword.equals(password))
			{
				return new AuthenticationResponse(AuthenticationStatus.CHANGE_PASSWORD, user.getId(), "/changePassword");
			}
			else
			{
				return new AuthenticationResponse(AuthenticationStatus.REJECTED, "Wrong password.");
			}
		}
		else
		{
			// Standard login
			String dbPassword = user.getString(E_PASSWORD);
			String encPassword = PasswordUtils.encrypt(password);
			if (dbPassword.equals(encPassword))
			{
				if (!twoFactorAuthenticated)
				{
					return new AuthenticationResponse(AuthenticationStatus.TWO_FACTOR, "Two Factor Required");
				}
				else if (passwordExpiry.before(now))
				{
					return new AuthenticationResponse(AuthenticationStatus.CHANGE_PASSWORD, user.getId());
				}
				else if (!user.getBoolean("Live"))
				{
					return new AuthenticationResponse(AuthenticationStatus.UNACTIVATED, "Account hasn't been activated.");
				}
				else
				{
					return authenticateOK(user);
				}
			}
			else
			{
				return new AuthenticationResponse(AuthenticationStatus.REJECTED, "Wrong password.");
			}
		}
	}

	public AuthenticationResponse oldauthenticate(String username, String password) throws SQLException
	{
		ElementData user = ModuleAccess.getInstance().getElementByName(USER_MODULE, username);
		if (user == null)
		{
			return new AuthenticationResponse(AuthenticationStatus.REJECTED, "User not found.");
		}

		String resetPassword = user.getString(E_RESET_PASSWORD).trim();
		Date now = new Date();
		Date passwordExpiry = now;
		try
		{
			// Rest now if any issues parsing this value
			passwordExpiry = dbDateFormat.parse(user.getString(E_PASSWORD_EXPIRE));
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}

		// authenticate the user
		if (resetPassword.length() > 0)
		{
			// User has to change their password
			if (resetPassword.equals(password))
			{
				return new AuthenticationResponse(AuthenticationStatus.CHANGE_PASSWORD, user.getId());
			}
			else
			{
				return new AuthenticationResponse(AuthenticationStatus.REJECTED, "Wrong password.");
			}
		}
		else
		{
			// Standard login
			String dbPassword = user.getString(E_PASSWORD);
			String encPassword = PasswordUtils.encrypt(password);
			if (dbPassword.equals(encPassword))
			{
				if (passwordExpiry.before(now))
				{
					return new AuthenticationResponse(AuthenticationStatus.CHANGE_PASSWORD, user.getId());
				}
				else if (!user.getBoolean("Live"))
				{
					return new AuthenticationResponse(AuthenticationStatus.UNACTIVATED, "Account hasn't been activated.");
				}
				else
				{
					return authenticateOK(user);
				}
			}
			else
			{
				return new AuthenticationResponse(AuthenticationStatus.REJECTED, "Wrong password.");
			}
		}
	}

	public AuthenticationResponse authenticateOK(ElementData user)
	{
		new UserAccountReportingDataAccess().recordLastLogin(user.getId());
		CategoryData company = new UserAccountDataAccess().getUserCompanyForUserId(user.getId());

		return new AuthenticationResponse(AuthenticationStatus.OK_USER, user.getId(), company.getId(), user);
	}

	public AuthenticationResponse changePassword(String userId, String currentPassword,
			String newPassword) throws SQLException
	{
		if (passwordIsSecure(newPassword) == false)
		{
			return new AuthenticationResponse(AuthenticationStatus.REJECTED, userId,
					"Password must be at least 8 characters, contain uppercase, lowercase and a number");
		}

		ElementData user = ModuleAccess.getInstance().getElementById(USER_MODULE, userId);
		if (user == null)
		{
			return new AuthenticationResponse(AuthenticationStatus.REJECTED, userId, "User not found");
		}

		String tempPassword = user.getString(E_RESET_PASSWORD);
		String currentEncryptedPassword = user.getString(E_PASSWORD);

		if (StringUtils.isNotBlank(tempPassword) && tempPassword.equals(currentPassword)
				|| PasswordUtils.encrypt(currentPassword).equals(currentEncryptedPassword))
		{
			PasswordHistoryDataAccess paswordHistoryDA = PasswordHistoryDataAccess.getInstance();
			if (paswordHistoryDA.isNewPasswordUsedInHistory(newPassword, userId))
			{
				return new AuthenticationResponse(AuthenticationStatus.REJECTED, userId, "Last 8 passwords cannot be reused.");
			}

			Hashtable<String, String> writeHt = prepareUpdatedPasswordHt(newPassword, userId);

			if (updateElement(writeHt, USER_MODULE) > 0)
			{
				paswordHistoryDA.updatePasswordHistory(newPassword, userId);
				return authenticateOK(user);
			}
		}
		return new AuthenticationResponse(AuthenticationStatus.REJECTED, userId, "Password doesn't match.");
	}

	Hashtable<String, String> prepareUpdatedPasswordHt(String newPassword, String elementId) throws SQLException
	{
		TUserAccountDataAccess tada = new TUserAccountDataAccess(TransactionManager.getInstance());
		CategoryData companyData = tada.getUserCompanyForUserId(elementId);
		int passwordExpiry = Integer.parseInt(companyData.getString(C1_PASSWORD_EXPIRY_DATE));
		passwordExpiry = passwordExpiry == 0 ? 6 : passwordExpiry;
		Calendar expiryDate = GregorianCalendar.getInstance();
		expiryDate.add(Calendar.MONTH, passwordExpiry);
		Hashtable<String, String> writeHt = new Hashtable<String, String>();
		insertNotNull(writeHt, E_ID, elementId);
		insertNotNull(writeHt, E_PASSWORD, PasswordUtils.encrypt(newPassword));
		insertNotNull(writeHt, E_RESET_PASSWORD, "");
		insertNotNull(writeHt, E_PASSWORD_EXPIRE, expiryDate.getTime());
		return writeHt;
	}

	public int registerNewUser(
			String name,
			String surname,
			String email,
			String username,
			String tempPassword,
			String password,
			String mobileNumber, String company, String abnacn, Hashtable<String, String> address, String pricingPlan) throws ErrorException, SQLException, IOException
	{
		// Check email doesn't already exist
		Vector v = cda.getElementByName(USER_MODULE, username, false);
		if (v.size() > 0)
		{
			throw new ErrorException("User has already registered with email " + username);
		}

		int companyCategory = createCompany(company, abnacn, address, pricingPlan, email);
		int adminRoleId = createRolesFolder(company, companyCategory);
		int arbitraryCategory = createTeam(companyCategory, "My Sales Team");

		try (TransactionManager txManager = TransactionManager.getInstance())
		{
			TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
			CategoryData team = tma.getCategoryById(USER_MODULE, Integer.toString(arbitraryCategory));
			team.put(C_SUPPORT_AUTH_TIME, new Date());
			tma.updateCategory(team, USER_MODULE);
			txManager.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return createUser(name, surname, email, username, tempPassword, password, mobileNumber, arbitraryCategory, adminRoleId, 6); //default password expiry 6 months
	}

	public int createCompany(String company, String abnacn, Hashtable<String, String> address, String pricingPlan, String email)
	{
		// Create categories
		company = WordUtils.capitalize(company.trim());
		Hashtable<String, String> companyHt = standardCreateCategoryHashtable(company);
		insertNotNull(companyHt, "Category_ParentID", 0);
		insertNotNull(companyHt, "folderLevel", 1);
		insertNotNull(companyHt, C_LIVE, true);
		// New fields for Qcloud
		insertNotNull(companyHt, "ATTR_abnacn", abnacn);
		insertNotNull(companyHt, C_PRICING_PLAN, pricingPlan);
		if (SubscriptionPlan.isTrialPlan(pricingPlan))
		{
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 15);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			insertNotNull(companyHt, C_TRIAL_PLAN_EXPIRY, cal.getTime());
		}

		// billing user email
		insertNotNull(companyHt, C_BILLING_USERS, address.get(C_ADDRESS_LINE_1));

		// address fields
		insertNotNull(companyHt, C_ADDRESS_LINE_1, address.get(C_ADDRESS_LINE_1));
		insertNotNull(companyHt, C_ADDRESS_LINE_2, address.get(C_ADDRESS_LINE_2));
		insertNotNull(companyHt, C_ADDRESS_CITY_SUBURB, address.get(C_ADDRESS_CITY_SUBURB));
		insertNotNull(companyHt, C_ADDRESS_STATE, address.get(C_ADDRESS_STATE));
		insertNotNull(companyHt, C_ADDRESS_POSTCODE, address.get(C_ADDRESS_POSTCODE));
		insertNotNull(companyHt, C_ADDRESS_COUNTRY, address.get(C_ADDRESS_COUNTRY));
		insertNotNull(companyHt, C_SUPPORT_AUTH_TIME, new Date());

		insertNotNull(companyHt, C1_EXPIRY_DATE, 30);
		insertNotNull(companyHt, C1_PASSWORD_EXPIRY_DATE, 6);

		insertNotNull(companyHt, C1_LOCALE, getLocale(address.get(C_ADDRESS_COUNTRY)));

		insertNotNull(companyHt, C1_API_ID, TUserAccountDataAccess.generateApiId());
		insertNotNull(companyHt, C1_API_KEY, TUserAccountDataAccess.generateApiKey());

		insertNotNull(companyHt, C1_DELETE_ITINERARY_SINGLE, 90);
		insertNotNull(companyHt, C1_DELETE_ITINERARY_RETURN, 30);

		insertNotNull(companyHt, C1_ENCRYPT_CLIENT_DATA, true);

		//Get the latest json of the pricing table style
		String styleJson = "";

		int companyCategory = createCategory(companyHt, USER_MODULE);

		try (TransactionManager txManager = TransactionManager.getInstance())
		{
			TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
			CategoryData newCompany = tma.getCategoryById(UserAccountDataAccess.USER_MODULE, Integer.toString(companyCategory));
			CategoryData companyData = new CategoryData();
			companyData.setId(String.valueOf(companyCategory));
			TextFile textFile = new TextFile(styleJson, USER_MODULE, C1_PROPOSAL_PRICING_STYLE, String.valueOf(companyCategory), true);
			List<String> userList = new ArrayList<>();
			userList.add(email);
			MultiOptions billingEmailList = new MultiOptions(Integer.toString(companyCategory), UserAccountDataAccess.C_BILLING_USERS, UserAccountDataAccess.USER_MODULE, newCompany.getType(), userList);
			companyData.put(C1_PROPOSAL_PRICING_STYLE, textFile);
			companyData.put(C_BILLING_USERS, billingEmailList);
			tma.updateCategory(companyData, USER_MODULE);
			txManager.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}


		return companyCategory;
	}

	/**
	 * @param
	 * @return
	 */
	private String getLocale(String country)
	{
		List<String> locales = db.selectQuerySingleCol("select attr_locale from elements_countries where attr_headline = ?", new String[] { country });
		if (locales.size() == 0)
			return "en-AU";
		return locales.get(0);
	}

	public int createRolesFolder(String companyName, int companyCategory)
	{
		Hashtable<String, String> companyHt = standardCreateCategoryHashtable(companyName);
		insertNotNull(companyHt, TUserRolesDataAccess.C_COMPANY_ID, companyCategory);
		int rolesCategory = createCategory(companyHt, TUserRolesDataAccess.MODULE);

		Hashtable<String, String> adminHt = standardCreateElementHashtable(Integer.toString(rolesCategory), "Administrator");
//		insertNotNull(adminHt, TUserRolesDataAccess.MANAGE_TEMPLATES, "1");
		insertNotNull(adminHt, TUserRolesDataAccess.MANAGE_PROJECTS, "1");
		insertNotNull(adminHt, TUserRolesDataAccess.MANAGE_USERS, "1");
		insertNotNull(adminHt, TUserRolesDataAccess.MANAGE_SETTINGS, "1");

		int adminId = createElement(adminHt, TUserRolesDataAccess.MODULE);

		Hashtable<String, String> consultantHt = standardCreateElementHashtable(Integer.toString(rolesCategory), "Consultant");

		createElement(consultantHt, TUserRolesDataAccess.MODULE);

		return adminId;
	}

	/**
	 * @param companyCategory
	 */

	public boolean isUsersFirstAccess(String username)
	{
		Hashtable<String, String> userData = getUserByUserName(username);

		if ((userData.get(TUTORIAL_FIRST_ACCESS) != null) && (userData.get(TUTORIAL_FIRST_ACCESS).equalsIgnoreCase("1")))
		{
			return true;
		}

		return false;
	}

	public void updateTutorialStatus(String userID, String tutorial, Boolean status)
	{
		try (TransactionManager transactionManager = TransactionManager.getInstance())
		{
			TransactionModuleAccess tma = TransactionModuleAccess.getInstance(transactionManager);
			ElementData element = new ElementData();
			element.put(E_ID, userID);
			String elementID = tma.getElement(USER_MODULE, element).getId();

			ElementData newElement = new ElementData();

			newElement.setId(elementID);
			newElement.put(tutorial, status);

			int update = tma.updateElement(newElement, USER_MODULE);
			transactionManager.commit();
		}
		catch (Exception e)
		{
			logger.error("editHealthByJson Exception.", e);
		}
	}


	public Hashtable<String, String> getUsersBySocketSession(String sessionID)
	{
		try
		{
			List<DataMap> lusers = TransactionDataAccess.getInstance().selectMaps("select * from elements_useraccounts where ATTRCHECK_SocketConnected = true and ATTR_SocketSessionID = ?", sessionID);
			List<Hashtable<String, String>> users = new DataMapLegacyConverter().convertFromDataMaps(lusers);
			if (users.size() > 0)
			{
				return users.get(0);
			}
			return new Hashtable<String, String>();

		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return new Hashtable<String, String>();
		}
	}

	public int createTeam(String companyId, String teamName)
	{
		return createTeam(Integer.parseInt(companyId), teamName);
	}

	public CategoryData usersTeam(String userId, String companyId)
	{
		Hashtable<String, String> userData = getUserWithId(userId);
		List<CategoryData> teams = getCompanyTeams(companyId);
		for (CategoryData team : teams)
		{
			if (team.getId().equals(userData.get(C_ID)))
			{
				return team;
			}
		}
		return null;
	}

	public List<CategoryData> usersTeams(String userId, String companyId)
	{
		Hashtable<String, String> userData = getUserWithId(userId);
		List<CategoryData> usersTeams = new ArrayList<CategoryData>();
		List<CategoryData> teams = getCompanyTeams(companyId);
		for (CategoryData team : teams)
		{
			if (team.getId().equals(userData.get(C_ID)))
			{
				usersTeams.add(team);
			}
		}
		if (usersTeams.size() > 0)
		{
			return usersTeams;
		}
		else
		{
			return null;
		}
	}

	public int createTeam(int companyId, String teamName)
	{
		return getOrCreateCategory(teamName, companyId, 2, USER_MODULE);

	}

	public int checkTeamForReferences(TransactionManager txManager, String teamId) throws Exception
	{
		int references = 0;
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		List<ElementData> users = tma.getElementsByParentId(USER_MODULE, teamId);
		references += users.size();
		return references;
	}

	public void migrationsForTeam(TransactionManager txManager, String oldTeam, String newTeam) throws Exception
	{
		TransactionDataAccess tda = TransactionDataAccess.getInstance(txManager);
		QueryBuffer qb = new QueryBuffer();
		qb.append("UPDATE elements_useraccounts SET Category_id = ? WHERE Category_id = ?", newTeam, oldTeam);
		tda.update(qb);
	}

	public Hashtable<String, String> getTeamWithId(int teamId)
	{
		Vector<Hashtable<String, String>> v = cda.getCategory(
				USER_MODULE,
				Integer.toString(teamId),
				false);
		if (v.size() > 0)
		{
			return v.get(0);
		}
		return null;
	}

	public int createUser(
			String name,
			String surname,
			String email,
			String username,
			String tempPassword,
			String encryptedPassword,
			String mobileNumber,
			int teamCategoryId,
			int adminRoleId,
			int passwordExpiry) throws SQLException, IOException
	{
		Calendar expiryDate = GregorianCalendar.getInstance();
		expiryDate.add(Calendar.MONTH, passwordExpiry);

		Hashtable<String, String> ht = standardCreateElementHashtable(teamCategoryId, username);
		insertNotNull(ht, E_NAME, name);
		insertNotNull(ht, E_SURNAME, surname);
		insertNotNull(ht, E_EMAIL_ADDRESS, email);
		insertNotNull(ht, E_RESET_PASSWORD, tempPassword);
		insertNotNull(ht, E_PASSWORD, encryptedPassword);
		insertNotNull(ht, E_PASSWORD_EXPIRE, expiryDate.getTime());
		insertNotNull(ht, E_MOBILE_NUMBER, mobileNumber);

		insertNotNull(ht, E_USER_TYPE, ADMIN);

		insertNotNull(ht, E_EMAIL_DAILY_SUMMARY, false);
		insertNotNull(ht, E_EMAIL_NEW_COMMENT, true);
		insertNotNull(ht, E_EMAIL_HANDOVER, true);
		insertNotNull(ht, E_EMAIL_STATUS_CHANGE, true);
		insertNotNull(ht, E_EMAIL_OPENED, false);
		insertNotNull(ht, SOCKET_SUPPORT_ADMIN, false);
		insertNotNull(ht, TUTORIAL_FIRST_ACCESS, true);
		insertNotNull(ht, MOBILE_LAST_ACCESS, expiryDate.getTime());
		insertNotNull(ht, C1_SUPPORT_TIME, new Date());

		insertNotNull(ht, E_LIVE, false);

		int userId = createElement(ht, USER_MODULE);

		TransactionDataAccess tda = TransactionDataAccess.getInstance(false);
		TransactionManager tm = TransactionManager.getInstance(tda);
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(tm);
		ModuleAccess ma = ModuleAccess.getInstance();

		ElementData user = ma.getElementById(USER_MODULE, Integer.toString(userId));
		if (adminRoleId > 0)
		{
			MultiOptions mo;
			mo = new MultiOptions(Integer.toString(userId), E_USER_ROLES, USER_MODULE, user.getType(), Arrays.asList(Integer.toString(adminRoleId)));
			user.put(E_USER_ROLES, mo);
		}
		tma.updateElement(user, USER_MODULE);
		tm.commit();

		return userId;
	}

	public Hashtable<String, String> getLiveUserByEmail(String email)
	{
		if (email != null)
		{
			Vector<Hashtable<String, String>> v = cda.getElementByValue(USER_MODULE, E_EMAIL_ADDRESS, email.trim(), true);
			if (v.size() > 0)
			{
				return v.get(0);
			}
		}
		return null;
	}

	public Hashtable<String, String> getUserByUserName(String username)
	{
		return getUserByUserName(username, true);
	}

	public Hashtable<String, String> getUserByUserName(String username, boolean liveOnly)
	{
		if (username != null)
		{
			Vector<Hashtable<String, String>> v = cda.getElementByName(USER_MODULE, username.trim(), liveOnly);
			if (v.size() > 0)
			{
				return v.get(0);
			}
		}
		return null;
	}

	public ElementData getUserElementWithUserID(String userId)
	{

		ElementData result = null;
		try (TransactionManager transactionManager = TransactionManager.getInstance())
		{
			TUserAccountDataAccess tuada = new TUserAccountDataAccess(transactionManager);
			result = TransactionModuleAccess.getInstance(transactionManager).getElementById(USER_MODULE, userId);
			transactionManager.commit();
		}
		catch (Exception e)
		{
			logger.fatal("Oh No!", e);
		}

		return result;
	}

	public Hashtable<String, String> getUserWithId(String id)
	{
		return getUserWithId(Integer.parseInt(id));
	}

	public Hashtable<String, String> getUserWithId(int id)
	{
		Vector<Hashtable<String, String>> v = cda.getElement(
				USER_MODULE,
				Integer.toString(id),
				false);
		if (v.size() > 0)
		{
			return v.get(0);
		}
		return null;
	}

	public List<Hashtable<String, String>> getUsers(String... users)
	{
		List<ElementData> lusers = new ArrayList<>();
		try (TransactionManager transactionManager = TransactionManager.getInstance())
		{
			TUserAccountDataAccess tda = new TUserAccountDataAccess(transactionManager);
			lusers = tda.getUsers(users);
			transactionManager.commit();
		}
		catch (Exception e)
		{
			logger.fatal("Oh No!", e);
		}
		DataMapLegacyConverter dmlc = new DataMapLegacyConverter();
		return dmlc.convertFromDataMaps(lusers);
	}

	public Hashtable<String, String> getCompanyWithId(int id)
	{
		return getCompanyWithId(Integer.toString(id));
	}

	public Hashtable<String, String> getCompanyWithId(String id)
	{
		Vector<Hashtable<String, String>> v = cda.getCategory(
				USER_MODULE,
				id,
				false);
		if (v.size() > 0)
		{
			return v.get(0);
		}
		return null;
	}

	public String[] getUserAccessRights(String userID)
	{
		Set<String> matches = getUserAccessRightsSet(userID);
		return matches.toArray(new String[] {});
	}

	public Set<String> getUserAccessRightsSet(String userID)
	{
		Set<String> matches = new HashSet<String>();
		matches = getUserAccessRights(userID, matches);
		return matches;
	}

	private Set<String> getUserAccessRights(String userId, Set<String> matches)
	{
		UserRolesDataAccess urda = new UserRolesDataAccess();
		UserPermissions up = urda.getPermissions(userId);

		Hashtable<String, ElementData> allUsers = getCompanyUsers(getUserCompanyForUserId(userId).getId());
		for (String user : allUsers.keySet())
		{
			matches.add(user);
		}

		return matches;
	}

	public DashboardSummary getUserDashboardStats(Date datetime, String companyID) throws ParseException
	{

		DataAccess da = DataAccess.getInstance();
		List<Hashtable<String, String>> folderLevels = da.select("select ATTRDROP_status as status, ATTRDATE_travelStartDate as travelStart, ATTRDATE_travelEndDate as travelEnd " +
				"from elements_traveldocs " +
				"where ATTRDROP_ownerId = " + companyID + " " +
				"and ATTRCHECK_displayOnDashboard = true " +
				"and" + standardLiveClauseForCustomQuery() +
				"and ATTRDROP_status != 'template'",
				HashtableMapper.getInstance());
		DashboardSummary ds = new DashboardSummary();

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		int occurances = folderLevels.size();
		for (Hashtable<String, String> ht : folderLevels)
		{
			String status = ht.get("status");

			if (TasksStatus.Draft.getDbText().equalsIgnoreCase(status))
				ds.draft += 1;
			else if (TasksStatus.NotStarted.getDbText().equalsIgnoreCase(status))
				ds.notstarted += 1;
			else if (TasksStatus.WIP.getDbText().equalsIgnoreCase(status))
				ds.wip += 1;
			else if (TasksStatus.Complete.getDbText().equalsIgnoreCase(status)) //TravelDocsStatus.dueToTravel.getDbText().equalsIgnoreCase(status)
				ds.complete += 1;
			else if (TasksStatus.Incomplete.getDbText().equalsIgnoreCase(status))
				ds.incomplete += 1;
			else
			{
				logger.warn("Unable to create accurate dashboard summary. Unknown proposal status: " + status);
				ds.unknown += occurances;
			}
		}
		ds.calculateTotal();
		return ds;
	}

	public boolean activateNewUser(String userId, String key)
	{
		Vector findUser = cda.getElement(USER_MODULE, userId, false);
		if (findUser.size() == 0)
		{
			logger.info("Account not activated... user not found");
			return false;
		}

		ElementData user = getUserElementWithUserID(userId);
		if (user.isLive())
		{
			logger.info("Account already activated.");
			return false;
		}

		CategoryData company = getUserCompanyForUserId(userId);
		Hashtable<String, String> userHash = (Hashtable<String, String>) findUser.get(0);


		String username = userHash.get(E_HEADLINE);
		if (false == key.equals(new SecurityKeyService().getActivationKey(username,
				userHash.get(E_RESET_PASSWORD))))
		{
			logger.warn("Account not activated... incorrect key");
			return false;
		}

		// activate the account...
		try (TransactionManager transManager = TransactionManager.getInstance())
		{
			TransactionModuleAccess tma = TransactionModuleAccess.getInstance(transManager);

			ElementData activeUserElement = new ElementData();
			activeUserElement.setId(userId);
			activeUserElement.put(E_LIVE, "1");
			activeUserElement.put(C_SUPPORT_AUTH_TIME, new Date());
			activeUserElement.put(E_PASSWORD_EXPIRE, new Date());
			activeUserElement.put(MOBILE_LAST_ACCESS, new Date());
			activeUserElement.put(SOCKET_SUPPORT_ADMIN, "0");
			activeUserElement.put(TUTORIAL_FIRST_ACCESS, "1");
			activeUserElement.put(E_SURNAME, (userHash.get(UserAccountDataAccess.E_SURNAME).replace("(Pending Verification)", "")));

			tma.updateElement(activeUserElement, USER_MODULE);
			transManager.commit();
		}
		catch (Exception e)
		{
			logger.fatal("There was an issue inserting the User Event", e);
			return false;
		}

		new UserAccountReportingDataAccess().recordAccountCreation(userHash.get(E_ID));

		// update the user access rights
		DataAccess.getInstance().updateData(
				"delete from drop_down_multi_selections " +
						"where MODULE_NAME=? " +
						"and ATTRMULTI_NAME=?" +
						"and ELEMENT_ID=?",
				new String[] {
						USER_MODULE,
						E_MANAGES_MULTI,
						userHash.get(E_ID)
				});
		return true;


	}

	public boolean activateUser(String userId, String key)
	{
		Vector findUser = cda.getElement(USER_MODULE, userId, false);
		if (findUser.size() == 0)
		{
			logger.info("Account not activated... user not found");
			return false;
		}
		CategoryData company = getUserCompanyForUserId(userId);
		Hashtable<String, String> userHash = (Hashtable<String, String>) findUser.get(0);

		if ("1".equals(company.get(C1_ACTIVATED)))
		{
			logger.info("User " + userHash.get(E_HEADLINE) + " already activated");
			return false;
		}

		String username = userHash.get(E_HEADLINE);
		if (false == key.equals(new SecurityKeyService().getActivationKey(username,
				userHash.get(E_PASSWORD))))
		{
			logger.warn("Account not activated... incorrect key");
			return false;
		}
		Hashtable<String, String> updateCompany = new Hashtable<String, String>();
		insertNotNull(updateCompany, "Category_id", company.getId());
		insertNotNull(updateCompany, C1_ACTIVATED, true);
		updateCategory(updateCompany, USER_MODULE);
		insertNotNull(userHash, C_SUPPORT_AUTH_TIME, new Date());
		insertNotNull(userHash, "ATTRDROP_TwoFactorType", "email");
		// activate the account...
		insertNotNull(userHash, E_LIVE, true);
		// insertNotNull(userHash, E_ACTIVATED, true);
		updateElement(userHash, USER_MODULE);
		new UserAccountReportingDataAccess().recordAccountCreation(userHash.get(E_ID));

		// update the user access rights
		DataAccess.getInstance().updateData(
				"delete from drop_down_multi_selections " +
						"where MODULE_NAME=? " +
						"and ATTRMULTI_NAME=?" +
						"and ELEMENT_ID=?",
				new String[] {
						USER_MODULE,
						E_MANAGES_MULTI,
						userHash.get(E_ID)
				});
		return true;


	}

	public boolean activateUserNewEmail(String userId, String key, String newEmail)
	{
		Vector findUser = cda.getElement(USER_MODULE, userId, true);
		if (findUser.size() == 0)
		{
			logger.info("Account not activated... user not found");
			return false;
		}
		CategoryData company = getUserCompanyForUserId(userId);
		Hashtable<String, String> userHash = (Hashtable<String, String>) findUser.get(0);

		if ("0".equals(company.get(C1_ACTIVATED)))
		{
			logger.info("User " + userHash.get(E_HEADLINE) + " account not activated");
			return false;
		}

		if (false == key.equals(new SecurityKeyService().getActivationKey(newEmail,
				userHash.get(E_PASSWORD))))
		{
			logger.warn("New email not activated... incorrect key");
			return false;
		}

		Hashtable<String, String> updateHT = new Hashtable<String, String>();
		insertNotNull(updateHT, E_ID, userId);
		insertNotNull(updateHT, E_EMAIL_ADDRESS, newEmail);

		updateElement(updateHT, USER_MODULE);

		return true;


	}

	public void updateUserAccount(String userId, String name, String surname, String mobile)
	{
		Hashtable<String, String> updateHT = new Hashtable<String, String>();
		insertNotNull(updateHT, E_ID, userId);
		insertNotNull(updateHT, E_NAME, name);
		insertNotNull(updateHT, E_SURNAME, surname);
		insertNotNull(updateHT, E_MOBILE_NUMBER, mobile);

		//        insertNotNull(updateHT, E_EMAIL_FOOTER,
		//            new StoreUtils().writeFieldToStores(USER_MODULE, userId, E_EMAIL_FOOTER, emailFooter));

		updateElement(updateHT, USER_MODULE);
	}

	public void updateUserEmail(String userId, String email)
	{
		Hashtable<String, String> updateHT = new Hashtable<String, String>();
		insertNotNull(updateHT, E_ID, userId);
		insertNotNull(updateHT, E_EMAIL_ADDRESS, email);

		updateElement(updateHT, USER_MODULE);
	}

	public void updateDashboard(String userId, String data)
	{
		try (TransactionManager transactionManager = TransactionManager.getInstance())
		{
			TransactionModuleAccess tma = TransactionModuleAccess.getInstance(transactionManager);
			ElementData element = new ElementData();
			element.put(E_ID, userId);
			String elementID = tma.getElement(USER_MODULE, element).getId();

			ElementData newElement = new ElementData();

			newElement.setId(elementID);
			newElement.put(DASHBOARD_RECORD, data);

			int update = tma.updateElement(newElement, USER_MODULE);
			transactionManager.commit();
		}
		catch (Exception e)
		{
			logger.error("editHealthByJson Exception.", e);
		}
	}

	public void updateNotificationPreferences(
			String userId,
			boolean emailDailySummary,
			boolean emailNewComment,
			boolean emailHandover,
			boolean emailStatusChange,
			boolean emailProposalOpened)
	{
		Hashtable<String, String> updateHT = new Hashtable<String, String>();
		insertNotNull(updateHT, E_ID, userId);
		insertNotNull(updateHT, E_EMAIL_DAILY_SUMMARY, emailDailySummary);
		insertNotNull(updateHT, E_EMAIL_NEW_COMMENT, emailNewComment);
		insertNotNull(updateHT, E_EMAIL_HANDOVER, emailHandover);
		insertNotNull(updateHT, E_EMAIL_STATUS_CHANGE, emailStatusChange);
		insertNotNull(updateHT, E_EMAIL_OPENED, emailProposalOpened);

		updateElement(updateHT, USER_MODULE);
	}

	//    public StringBuffer getEmailFooterForUserId(String userId)
	//    {
	//      List<String> filePaths = DataAccess.getInstance().select("select "+E_EMAIL_FOOTER+
	//          " from elements_"+USER_MODULE+
	//          " where Element_ID=?",
	//          new String[]{userId},
	//          StringMapper.getInstance());
	//      if (filePaths.size() > 0)
	//      {
	//        ClientFileUtils clientFileUtils = new ClientFileUtils();
	//        return clientFileUtils.readTextContents(clientFileUtils.getFilePathFromStoreName(filePaths.get(0)));
	//      }
	//      return new StringBuffer();
	//    }

	protected boolean passwordIsSecure(String newPassword)
	{
		if (newPassword == null || newPassword.length() < 8)
			return false;
		if (!Pattern.compile("[0-9]").matcher(newPassword).find())
			return false;
		if (!Pattern.compile("[A-Z]").matcher(newPassword).find())
			return false;
		if (!Pattern.compile("[a-z]").matcher(newPassword).find())
			return false;

		return true;
	}

	public Map<String, String> getUserMetadata(String userId)
	{
		Hashtable<String, String> ht = getUserWithId(userId);
		CategoryData team = getUserTeamForUserId(userId);

		String name = ht.get(E_NAME);
		String surname = ht.get(E_SURNAME);
		String fullName = String.format("%s %s", name, surname).trim();

		HashMap<String, String> metadataFields = new HashMap<String, String>();
		metadataFields.put("consultantId", ht.get(E_ID));
		metadataFields.put("consultantName", name);
		metadataFields.put("consultantSurname", surname);
		metadataFields.put("consultantFullname", fullName);
		metadataFields.put("consultantPhone", ht.get(E_MOBILE_NUMBER));
		metadataFields.put("consultantEmail", ht.get(E_EMAIL_ADDRESS));
		metadataFields.put("consultantCompanyName", getUserCompanyNameForUserId(userId));
		metadataFields.put("consultantTeamName", team.getName());

		return metadataFields;
	}

	public boolean shouldSendUserNotification(String userId, String field)
	{
		List<String> result = DataAccess.getInstance().select(
				"select " + field
						+ " from elements_useraccounts"
						+ " where element_id = ?",
				new String[] { userId },
				new StringMapper());
		return result.size() > 0
				&& result.get(0).equals("1");
	}

	public boolean shouldSendUserNotification(Hashtable<String, String> userHt, String field)
	{
		return userHt.get(field).equals("1");
	}

	public String getUserForUserId(String userId)
	{
		String sql = "select c1.attr_categoryname from categories_" + USER_MODULE + " c1"
				+ ", categories_" + USER_MODULE + " c2"
				+ ", elements_" + USER_MODULE + " e"
				+ " where e.category_id = c2.category_id "
				+ " and c2.category_parentid = c1.category_id and e.element_id = ?";
		List<String> result = db.selectQuerySingleCol(sql, new String[] { userId });
		if (result.size() == 0)
		{
			return "";
		}
		return result.get(0);
	}

	public String getUserCompanyNameForUserId(String userId)
	{
		String sql = "select c1.attr_categoryname from categories_" + USER_MODULE + " c1"
				+ ", categories_" + USER_MODULE + " c2"
				+ ", elements_" + USER_MODULE + " e"
				+ " where e.category_id = c2.category_id "
				+ " and c2.category_parentid = c1.category_id and e.element_id = ?";
		List<String> result = db.selectQuerySingleCol(sql, new String[] { userId });
		if (result.size() == 0)
		{
			return "";
		}
		return result.get(0);
	}

	public CategoryData getUserCompanyForUserId(String userId)
	{
		String sql = "select c1.*  "
				+ " from categories_" + USER_MODULE + " c2, "
				+ " categories_" + USER_MODULE + " c1, "
				+ " elements_" + USER_MODULE + " e "
				+ " where e.category_id = c2.category_id "
				+ " and c1.category_id = c2.category_parentid "
				+ " and e.element_id = ?";
		try
		{
			List<CategoryData> result = ModuleAccess.getInstance().getCategories(sql, userId);
			if (result.size() == 0)
			{
				return null;
			}
			return result.get(0);
		}
		catch (Exception e)
		{
			return null;
		}

	}

	public Boolean addZeroRiskAPIDetailsToCompany(CategoryData company, String apiKey)
	{
		try (TransactionManager transactionManager = TransactionManager.getInstance())
		{
			TransactionModuleAccess tma = TransactionModuleAccess.getInstance(transactionManager);
			CategoryData updateCompany = new CategoryData();
			updateCompany.setId(company.getId());
			updateCompany.put(C_ZERO_RISK_APIKEY, apiKey);
			tma.updateCategory(updateCompany, USER_MODULE);
			transactionManager.commit();
			return true;
		}
		catch (Exception e)
		{
			logger.fatal("Oh No!", e);
		}
		return false;
	}

	public Boolean addSABRESAMAPIDetailsToCompany(CategoryData company, String apiKey, String apiSecret)
	{
		try (TransactionManager transactionManager = TransactionManager.getInstance())
		{
			TransactionModuleAccess tma = TransactionModuleAccess.getInstance(transactionManager);
			CategoryData updateCompany = new CategoryData();
			updateCompany.setId(company.getId());
			updateCompany.put(C_SABRE_SAM_KEY, apiKey);
			updateCompany.put(C_SABRE_SAM_SECRET, apiSecret);
			tma.updateCategory(updateCompany, USER_MODULE);
			transactionManager.commit();
			return true;
		}
		catch (Exception e)
		{
			logger.fatal("Oh No!", e);
		}
		return false;
	}

	public Boolean addGDSDetailsToCompany(CategoryData company, Boolean gds)
	{
		try (TransactionManager transactionManager = TransactionManager.getInstance())
		{
			TransactionModuleAccess tma = TransactionModuleAccess.getInstance(transactionManager);
			CategoryData updateCompany = new CategoryData();
			updateCompany.setId(company.getId());
			updateCompany.put(C_SABRE_TYPE, gds ? "GDS" : "SAM");
			tma.updateCategory(updateCompany, USER_MODULE);
			transactionManager.commit();
			return true;
		}
		catch (Exception e)
		{
			logger.fatal("Oh No!", e);
		}
		return false;
	}


	public CategoryData getUserTeamForUserId(String userId)
	{
		CategoryData company = null;
		try (TransactionManager transactionManager = TransactionManager.getInstance())
		{
			TUserAccountDataAccess tuada = new TUserAccountDataAccess(transactionManager);
			company = tuada.getUserTeamForUserId(userId);
			transactionManager.commit();
		}
		catch (Exception e)
		{
			logger.fatal("Oh No!", e);
		}
		return company;
	}

	public CategoryData getCompanyForCompanyId(String companyId)
	{
		CategoryData data = null;
		try (TransactionManager transactionManager = TransactionManager.getInstance())
		{
			data = new TUserAccountDataAccess(transactionManager).getCompanyForCompanyId(companyId);
			transactionManager.rollback();
			return data;
		}
		catch (Exception e)
		{
			logger.fatal("Oh No!", e);
		}
		return data;
	}

	public CategoryData getTeamForTeamId(String teamId)
	{
		CategoryData categoryData = null;
		try
		{
			categoryData = ModuleAccess.getInstance().getCategoryById(USER_MODULE, teamId);
			if (categoryData == null || categoryData.getFolderLevel() != 2)
			{
				return null;
			}
		}
		catch (SQLException e)
		{
			logger.fatal("Error:", e);

		}
		return categoryData;
	}

	public List<ElementData> getUsersInTeam(String teamId, String companyId)
	{
		List<ElementData> users = new ArrayList<>();
		try
		{
			List<CategoryData> cats = ModuleAccess.getInstance().getCategoriesByParentId(USER_MODULE, companyId);
			for (CategoryData cat : cats)
			{
				if (teamId.equals(cat.getId()) || "".equals(teamId))
					users.addAll(ModuleAccess.getInstance().getElementsByParentId(USER_MODULE, cat.getId()));
			}
		}
		catch (Exception ex)
		{
			logger.fatal("Error:", ex);
		}
		return users;
	}

	/*
	 * This is where we will check for teams and there content
	 */

	public Hashtable<String, ElementData> getCompanyUsers(int companyId)
	{
		return getCompanyUsers(companyId + "");
	}

	public Hashtable<String, ElementData> getCompanyUsers(String companyId)
	{
		Hashtable<String, ElementData> ht = new Hashtable<String, ElementData>();
		try
		{
			ModuleAccess ma = ModuleAccess.getInstance();
			List<CategoryData> teams = ma.getCategoriesByParentId(USER_MODULE, companyId);
			for (CategoryData team : teams)
			{
				List<ElementData> users = ma.getElementsByParentId(USER_MODULE, team.getId());
				for (ElementData user : users)
				{
					user.put("team", team);
					ht.put(user.getId(), user);
				}
			}
		}
		catch (SQLException e)
		{
			logger.fatal("Error:", e);

		}
		return ht;
	}

	public List<CategoryData> getCompanyTeams(int companyId)
	{
		return getCompanyTeams(companyId + "");
	}

	public List<CategoryData> getCompanyTeams(String companyId)
	{
		List<CategoryData> list = new ArrayList<CategoryData>();
		try
		{
			ModuleAccess ma = ModuleAccess.getInstance();
			list = ma.getCategoriesByParentId(USER_MODULE, companyId);
		}
		catch (SQLException e)
		{
			logger.fatal("Error:", e);
		}
		return list;
	}

	public static Hashtable<String, String> constructAddressHt(String addressLine1, String addressLine2, String citySuburb, String state, String postcode, String country)
	{
		Hashtable<String, String> address = new Hashtable<String, String>();

		address.put(UserAccountDataAccess.C_ADDRESS_LINE_1, addressLine1);
		address.put(UserAccountDataAccess.C_ADDRESS_LINE_2, addressLine2);
		address.put(UserAccountDataAccess.C_ADDRESS_CITY_SUBURB, citySuburb);
		address.put(UserAccountDataAccess.C_ADDRESS_STATE, state);
		address.put(UserAccountDataAccess.C_ADDRESS_POSTCODE, postcode);
		address.put(UserAccountDataAccess.C_ADDRESS_COUNTRY, country);

		return address;
	}

	public int getUserCountInCompanyWithCompanyId(String levelOneCategoryId)
	{
		int userCount = (int) DataAccess.getInstance().select(
				"select count(*) from elements_useraccounts e "
						+ " join categories_users c2 on e.category_id = c2.category_id "
						+ " where c2.category_parentid = ? ",
				new Object[] { levelOneCategoryId },
				new IntegerMapper()).get(0);

		return userCount;
	}

	public int getUserCountInCompanyWithTeamId(String levelTwoCategoryId)
	{
		int userCount = (int) DataAccess.getInstance().select(
				"select count(*) from elements_useraccounts e "
						+ " join categories_users c2 on e.category_id = c2.category_id "
						+ " where c2.category_parentid = "
						+ " (SELECT category_parentid FROM categories_users where category_id = ?) ",
				new Object[] { levelTwoCategoryId },
				new IntegerMapper()).get(0);

		return userCount;
	}

	public int getUserCountInCompanyWithUserId(String userId)
	{
		int userCount = (int) DataAccess.getInstance().select(
				"select count(*) from elements_useraccounts e "
						+ " join categories_users c2 on e.category_id = c2.category_id "
						+ " where c2.category_parentid = "

						+ " (SELECT cu.category_parentid FROM categories_users cu "
						+ " join elements_useraccounts eu on cu.category_id = eu.category_id "
						+ " where eu.element_id = ?) ",
				new Object[] { userId },
				new IntegerMapper()).get(0);

		return userCount;
	}

	public static boolean doesCompanyHaveCrmContract(CategoryData company)
	{
		return !StringUtils.isBlank(company.getString(UserAccountDataAccess.C_CRM_CONTRACT_ID))
				&& company.getInt(UserAccountDataAccess.C_CRM_CONTRACT_ID) != 0;
	}

	public static boolean isPayByTransfer(CategoryData company)
	{
		return StringUtils.equalsIgnoreCase(company.getString(C1_PAY_OPTION), "BANKTRANSFER");
	}

	public static boolean isPayByCreditCard(CategoryData company)
	{
		return StringUtils.isBlank(company.getString(C1_PAY_OPTION));
	}

	public static boolean isPayNeeded(CategoryData company)
	{
		return !StringUtils.equalsIgnoreCase(company.getString(C1_PAY_OPTION), "NONINVOICED");
	}

	public boolean isTrialPeriodOver(CategoryData company)
	{
		if (!SubscriptionPlan.isTrialPlan(company.getString(UserAccountDataAccess.C_PRICING_PLAN)))
			return false;

		Date today = new Date();
		Date trialEnd = company.getDate(UserAccountDataAccess.C_TRIAL_PLAN_EXPIRY);

		if (today.after(trialEnd))
			return true;
		else
			return false;
	}


	public int trialPeriodOverInWeek(CategoryData company)
	{
		if (!SubscriptionPlan.isTrialPlan(company.getString(UserAccountDataAccess.C_PRICING_PLAN)))
			return -1;

		Date today = new Date();
		Date trialEnd = company.getDate(UserAccountDataAccess.C_TRIAL_PLAN_EXPIRY);
		Calendar cal = Calendar.getInstance();
		cal.setTime(trialEnd);
		cal.add(Calendar.DAY_OF_MONTH, -7);
		Date weekBeforeTrialEnd = cal.getTime();

		if (today.before(trialEnd) && today.after(weekBeforeTrialEnd))
			return Days.daysBetween(new DateTime(today).toLocalDate(), new DateTime(trialEnd).toLocalDate()).getDays();
		else
			return -1;
	}


	public int trialPerioddayCounter(CategoryData company)
	{
		if (!SubscriptionPlan.isTrialPlan(company.getString(UserAccountDataAccess.C_PRICING_PLAN)))
			return -1;

		Date today = new Date();
		Date trialEnd = company.getDate(UserAccountDataAccess.C_TRIAL_PLAN_EXPIRY);


		if (trialEnd == null)
		{
			return -1;
		}

		int dayNum = Days.daysBetween(new DateTime(today).toLocalDate(), new DateTime(trialEnd).toLocalDate()).getDays();
		return dayNum;
	}

	public List<Hashtable<String, String>> getUsersByCompanyId(String companyId)
	{
		try
		{
			List<DataMap> lusers = TransactionDataAccess.getInstance().selectMaps("select * from elements_useraccounts e, categories_useraccounts c where e.category_id = c.category_id and c.category_parentId = ?", companyId);
			return new DataMapLegacyConverter().convertFromDataMaps(lusers);
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return new ArrayList<Hashtable<String, String>>();
		}
	}

	public ElementData getCompanyMainUserBy(String companyID)
	{
		String query = "select eu.* from elements_useraccounts eu, categories_useraccounts c1, categories_useraccounts c2 where eu.Category_id = c1.Category_id and c1.category_parentid = c2.Category_id and c2.Category_id = ? order by eu.element_id asc";
		try
		{
			return ModuleAccess.getInstance().getElements(query, new String[] { companyID }).get(0);
		}
		catch (SQLException e)
		{
			logger.error("Error", e);
			return null;
		}
	}

	public ElementData getCompanyUserBy(String agentId, String agencyCode)
	{
		String query = "select eu.* from elements_useraccounts eu, categories_users cu where eu.Category_id=cu.Category_id and eu.ATTR_travelport_consultantId=? and cu.ATTR_travelport_branchCode=?";
		try
		{
			return ModuleAccess.getInstance().getElements(query, new String[] { agentId, agencyCode }).get(0);
		}
		catch (SQLException e)
		{
			logger.error("Error", e);
			return null;
		}
	}

	public ElementData getUserBySabreCode(String agentCode, String teamId)
	{
		String query = "select eu.* from elements_useraccounts eu, elements_sabre es, categories_sabre cs where eu.Element_id=es.ATTRDROP_userId and es.ATTR_consultantId=? and es.Category_id=cs.Category_id and cs.ATTRDROP_teamId=?";
		try
		{
			List<ElementData> result = ModuleAccess.getInstance().getElements(query, new String[] { agentCode, teamId });
			return result.size() > 0 ? result.get(0) : null;
		}
		catch (SQLException e)
		{
			logger.error("Error", e);
			return null;
		}
	}

	public ElementData getUserByTravelportConsultantId(String agentCode)
	{
		String query = "select eu.* from elements_useraccounts eu, elements_travelport et where eu.Element_id=et.ATTRDROP_user and et.ATTR_consultantId=?";
		try
		{
			List<ElementData> result = ModuleAccess.getInstance().getElements(query, new String[] { agentCode });
			return result.size() > 0 ? result.get(0) : null;
		}
		catch (SQLException e)
		{
			logger.error("Error", e);
			return null;
		}
	}

	public List<CategoryData> getTeamsWithAutoDeleteForSabre(boolean autoDelete)
	{
		String query = "select cu.*, cs.ATTRINTEGER_deleteInMonths deleteInMonths  from categories_useraccounts cu, categories_sabre cs where cs.ATTRCHECK_autoDeleteItinerary=" + autoDelete + " and cs.folderLevel=2 and cu.Category_Id=cs.ATTRDROP_teamId";
		try
		{
			return ModuleAccess.getInstance().getCategories(query, new String[] {});
		}
		catch (SQLException e)
		{
			logger.error("Error", e);
			return null;
		}
	}

	public List<CategoryData> getTeamsWithAutoDeleteForTravelport(boolean autoDelete)
	{
		String query = "select cu.*, ct.ATTRINTEGER_deleteInMonths deleteInMonths from categories_useraccounts cu, categories_travelport ct where ct.ATTRCHECK_autoDeleteItinerary=" + autoDelete + " and ct.folderLevel=2 and cu.Category_Id=ct.ATTRDROP_team";
		try
		{
			return ModuleAccess.getInstance().getCategories(query, new String[] {});
		}
		catch (SQLException e)
		{
			logger.error("Error", e);
			return null;
		}
	}

	public static boolean isUserUsingMobileTFA(ElementData user)
	{
		return StringUtils.equalsIgnoreCase("mobile", user.getString("ATTRDROP_TwoFactorType"));
	}

	public static boolean isUserUsingMobileTFA(Map<String, String> user)
	{
		return StringUtils.equalsIgnoreCase("mobile", user.get("ATTRDROP_TwoFactorType"));
	}
}
