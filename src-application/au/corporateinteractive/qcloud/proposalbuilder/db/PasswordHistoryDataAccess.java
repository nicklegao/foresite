package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import webdirector.db.client.ManagedDataAccess;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;
import au.net.webdirector.common.utils.password.PasswordUtils;


public class PasswordHistoryDataAccess extends ManagedDataAccess
{

	private static final int MAXIMUM_PASSWORD_HISTORY_COUNT = 8;
	private volatile static PasswordHistoryDataAccess instance;
	private Logger logger;
	private DataAccess da;

	private PasswordHistoryDataAccess()
	{
		logger = Logger.getLogger(PasswordHistoryDataAccess.class);
		da = DataAccess.getInstance();
	}

	public static PasswordHistoryDataAccess getInstance()
	{
		if (instance == null)
		{
			synchronized (PasswordHistoryDataAccess.class)
			{
				if (instance == null)
				{
					instance = new PasswordHistoryDataAccess();
				}
			}
		}
		return instance;
	}

	@SuppressWarnings("unchecked")
	public boolean isNewPasswordUsedInHistory(String newPassword, String userId)
	{
		String encryptedNewPassword = PasswordUtils.encrypt(newPassword);
		String query = "select password_history_id as total from password_history where user_id = ? and encrypted_password = ?";
		Object[] args = new Object[] { userId, encryptedNewPassword };

		List<Hashtable<String, String>> results = da.select(query, args, new HashtableMapper());
		if (results == null || results.size() == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public void updatePasswordHistory(String newPassword, String userId)
	{
		String encryptedNewPassword = PasswordUtils.encrypt(newPassword);
		try
		{
			Hashtable<String, String> latestBucket = getLatestPasswordHistoryBucket(userId);
			if (latestBucket == null)
			{
				insertPasswordHistory(userId, encryptedNewPassword, 0);
				return;
			}
			String latestHistoryId = latestBucket.get("password_history_id");
			int bucketNumber = Integer.valueOf(latestBucket.get("bucket_number"));
			int nextBucket = (bucketNumber + 1) % MAXIMUM_PASSWORD_HISTORY_COUNT;

			deletePasswordHistoryByBucket(userId, nextBucket);

			insertPasswordHistory(userId, encryptedNewPassword, nextBucket);

			unsetLatestHistory(latestHistoryId);

		}
		catch (Exception e)
		{
			logger.error("Failed to update password history for user id: " + userId, e);
		}
	}

	private void unsetLatestHistory(String latestHistoryId)
	{
		String query = "update password_history set is_latest = 0 where password_history_id = ?";
		Object[] args = new Object[] { latestHistoryId };
		da.updateData(query, args);
	}

	private void insertPasswordHistory(String userId, String encryptedNewPassword, int bucketNumber)
	{
		String insertSql = "insert into password_history (user_id, encrypted_password, bucket_number, is_latest, last_update_date) values (?, ?, ?, 1, now())";
		Object[] insertArgs = new Object[] { userId, encryptedNewPassword, bucketNumber };
		da.updateData(insertSql, insertArgs);
	}

	private void deletePasswordHistoryByBucket(String userId, int nextBucket)
	{
		String deleteSql = "delete from password_history where user_id = ? and bucket_number = ?";
		Object[] deleteArgs = new Object[] { userId, nextBucket };
		da.updateData(deleteSql, deleteArgs);
	}

	@SuppressWarnings("unchecked")
	private Hashtable<String, String> getLatestPasswordHistoryBucket(String userId)
	{
		String selectSql = "select * from password_history where user_id = ? and is_latest = 1";
		Object[] selectArgs = new Object[] { userId };
		List<Hashtable<String, String>> latestBuckets = da.select(selectSql, selectArgs, new HashtableMapper());
		if (latestBuckets == null || latestBuckets.size() == 0)
		{
			return null;
		}
		return latestBuckets.get(0);
	}


}
