/**
 * @author Jaysun Lee
 * @date 22 Jan 2016
 */
package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import webdirector.db.client.ManagedDataAccess;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;

public class PaymentDataAccess extends ManagedDataAccess
{
	private static Logger logger = Logger.getLogger(PaymentDataAccess.class);

	/**
	 * <b>The internal module name</b>
	 */
	public static final String MODULE = "PAYMENT";


	/**
	 * <b>ATTRDROP_CompanyID</b><br/>
	 * Display: Company Id
	 * <b>CATEGORY LEVEL 1 ONLY</b><br/>
	 */
	public static final String C1_COMPANY_ID = "ATTRDROP_CompanyID";

	/**
	 * <b>ATTR_categoryName</b><br/>
	 * Display: Company Name / Preapproval Key
	 */
	public static final String C_CATEGORY_NAME = "ATTR_categoryName";

	/**
	 * <b>ATTR_Response</b><br/>
	 * Display: Response
	 * <b>CATEGORY LEVEL 2 ONLY</b><br/>
	 */
	public static final String C2_RESPONSE = "ATTR_Response";


	/**
	 * <b>ATTRCURRENCY_ChargeGstForReseller</b><br/>
	 * Display: Charge Gst For Reseller
	 */
	public static final String E_CHARGE_GST_FOR_RESELLER = "ATTRCURRENCY_ChargeGstForReseller";

	/**
	 * <b>ATTRCURRENCY_CrAdjust</b><br/>
	 * Display: Cr Adjust
	 */
	public static final String E_CR_ADJUST = "ATTRCURRENCY_CrAdjust";

	/**
	 * <b>ATTRCURRENCY_CrBalance</b><br/>
	 * Display: Cr Balance
	 */
	public static final String E_CR_BALANCE = "ATTRCURRENCY_CrBalance";

	/**
	 * <b>ATTRCURRENCY_CrCharged</b><br/>
	 * Display: Cr Charged
	 */
	public static final String E_CR_CHARGED = "ATTRCURRENCY_CrCharged";

	/**
	 * <b>ATTRCURRENCY_Due</b><br/>
	 * Display: Due
	 */
	public static final String E_DUE = "ATTRCURRENCY_Due";

	/**
	 * <b>ATTRCURRENCY_GST</b><br/>
	 * Display: Gst
	 */
	public static final String E_GST = "ATTRCURRENCY_GST";

	/**
	 * <b>ATTRCURRENCY_GstForAto</b><br/>
	 * Display: Gst For Ato
	 */
	public static final String E_GST_FOR_ATO = "ATTRCURRENCY_GstForAto";

	/**
	 * <b>ATTRCURRENCY_PaypalCharged</b><br/>
	 * Display: Paypal Charged
	 */
	public static final String E_PAYPAL_CHARGED = "ATTRCURRENCY_PaypalCharged";

	/**
	 * <b>ATTRCURRENCY_PaypalFee</b><br/>
	 * Display: Paypal Fee
	 */
	public static final String E_PAYPAL_FEE = "ATTRCURRENCY_PaypalFee";

	/**
	 * <b>ATTRCURRENCY_PayToReseller</b><br/>
	 * Display: Pay To Reseller
	 */
	public static final String E_PAY_TO_RESELLER = "ATTRCURRENCY_PayToReseller";

	/**
	 * <b>ATTRCURRENCY_Received</b><br/>
	 * Display: Received
	 */
	public static final String E_RECEIVED = "ATTRCURRENCY_Received";

	/**
	 * <b>ATTRDATE_ProcessedTime</b><br/>
	 * Display: Processed Time
	 */
	public static final String E_PROCESSED_TIME = "ATTRDATE_ProcessedTime";

	/**
	 * <b>ATTRDATE_TxDateTime</b><br/>
	 * Display: Tx Date Time
	 */
	public static final String E_TX_DATE_TIME = "ATTRDATE_TxDateTime";

	/**
	 * <b>ATTRDROP_Status</b><br/>
	 * Display: Status
	 */
	public static final String E_STATUS = "ATTRDROP_Status";

	/**
	 * <b>ATTRFILE_Invoice</b><br/>
	 * Display: Invoice
	 */
	public static final String E_INVOICE = "ATTRFILE_Invoice";

	/**
	 * <b>ATTR_Action</b><br/>
	 * Display: Action
	 */
	public static final String E_ACTION = "ATTR_Action";

	/**
	 * <b>ATTR_CurrencyCode</b><br/>
	 * Display: Currency Code
	 */
	public static final String E_CURRENCY_CODE = "ATTR_CurrencyCode";

	/**
	 * <b>ATTR_Cycle</b><br/>
	 * Display: Cycle
	 */
	public static final String E_CYCLE = "ATTR_Cycle";

	/**
	 * <b>ATTR_Headline</b><br/>
	 * Display: Transaction ID
	 */
	public static final String E_HEADLINE = "ATTR_Headline";

	/**
	 * <b>ATTR_IPN</b><br/>
	 * Display: Ipn
	 */
	public static final String E_IPN = "ATTR_IPN";

	/**
	 * <b>ATTR_Memo</b><br/>
	 * Display: Memo
	 */
	public static final String E_MEMO = "ATTR_Memo";

	/**
	 * <b>ATTR_Notes</b><br/>
	 * Display: Notes
	 */
	public static final String E_NOTES = "ATTR_Notes";

	/**
	 * <b>ATTR_Plan</b><br/>
	 * Display: Plan
	 */
	public static final String E_PLAN = "ATTR_Plan";

	/**
	 * <b>ATTR_PlanCode</b><br/>
	 * Display: Plan Code
	 */
	public static final String E_PLAN_CODE = "ATTR_PlanCode";

	/**
	 * <b>ATTR_Request</b><br/>
	 * Display: Request
	 */
	public static final String E_REQUEST = "ATTR_Request";

	/**
	 * <b>ATTR_Reseller</b><br/>
	 * Display: Reseller
	 */
	public static final String E_RESELLER = "ATTR_Reseller";

	/**
	 * <b>ATTR_Response</b><br/>
	 * Display: Response
	 */
	public static final String E_RESPONSE = "ATTR_Response";

	/**
	 * <b>ATTR_TransactionId</b><br/>
	 * Display: Transaction Id
	 */
	public static final String E_TRANSACTION_ID = "ATTR_TransactionId";

	/**
	 * <b>ATTR_UserId</b><br/>
	 * Display: User Id
	 */
	public static final String E_USER_ID = "ATTR_UserId";

	public List<ElementData> getPayments(String companyId, String[] dateRange)
	{
		QueryBuffer sql = new QueryBuffer();
		sql.append("select e.* from ");
		sql.append(" elements_payment e, categories_payment c1, categories_payment c2 ");
		sql.append(" where c1.category_id = c2.category_parentId ");
		sql.append(" and c2.category_id = e.category_id ");
		sql.append(" and c1.ATTRDROP_CompanyID = ? ", companyId);
		if (dateRange != null)
		{
			sql.append(" and Date(e.ATTRDATE_TxDateTime) between ? and ?", dateRange);
		}
		sql.append(" order by e.ATTRDATE_TxDateTime desc");
		try
		{
			return ModuleAccess.getInstance().getElements(sql.getSQL(), sql.getParams());
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return new ArrayList<ElementData>();
		}

	}
}
