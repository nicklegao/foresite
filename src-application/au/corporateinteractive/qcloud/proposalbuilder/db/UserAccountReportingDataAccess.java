package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import au.com.ci.system.classfinder.StringUtil;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
import au.net.webdirector.common.utils.SimpleDateFormat;
import webdirector.db.client.ManagedDataAccess;

public class UserAccountReportingDataAccess extends ManagedDataAccess
{
	private static Logger logger = Logger.getLogger(UserAccountReportingDataAccess.class);
	public static final String REPORTING_SUBTABLE = "subtable_users_reporting";

	// Reporting fields:...
	public static final String REPORT_ACCOUNT_CREATED = "accountCreated";
	public static final String REPORT_LAST_VIEWED_NOTIFICATIONS = "lastViewedNotifications";
	public static final String REPORT_LAST_LOGIN = "lastLogedIn";
	public static final String REPORT_LAST_CREATED_PROPOSAL = "lastCreatedProposal";
	public static final String REPORT_LAST_EDITED_PROPOSAL = "lastEditedProposal";
	public static final String REPORT_LAST_VIEWED_PROPOSAL = "lastViewedProposal";
	public static final String REPORT_LAST_MANAGED_PROPOSAL = "lastManagedProposal";
	public static final String REPORT_LAST_LOGIN_STORED = "previousLastLogin";

	public void createUserIfNeeded(String userId)
	{
		DataAccess da = DataAccess.getInstance();
		List<String> matches = da.select("select count(*) from " + REPORTING_SUBTABLE +
				" where id = ?", new String[] { userId }, StringMapper.getInstance());
		if (Integer.parseInt(matches.get(0)) == 0 )
		{
			logger.info("Creating user sub-table record for user with ID:" + userId);
			da.insertData("INSERT INTO " + REPORTING_SUBTABLE + " (id, accountCreated) VALUES (?, now())", new String[] {
					userId
			});
		}
	}

	private boolean recordDate(String userId, String field,
			Date date)
	{
		createUserIfNeeded(userId);

		DataAccess da = DataAccess.getInstance();
		boolean success = da.updateData("UPDATE " + REPORTING_SUBTABLE +
				" SET " + field + " = ?" +
				" WHERE id=?", new String[] {
				dbDateFormat.format(date),
				userId
		}) == 1;

		logger.info(String.format(
				"Recorded %s for user %s with success: %s",
				field, userId, success));
		return success;
	}

	private boolean recordLoginDate(String userId, String field,
			Date date)
	{
		createUserIfNeeded(userId);

		DataAccess da = DataAccess.getInstance();
		boolean success = false;

		List<String> matches = da.select("select " + REPORT_LAST_LOGIN + " from " + REPORTING_SUBTABLE +
				" where id = ?", new String[] { userId }, StringMapper.getInstance());
		if (matches.size() > 0 && !StringUtil.isNullOrEmpty(matches.get(0)))
		{
			success = da.updateData("UPDATE " + REPORTING_SUBTABLE +
					" SET " + field + " = ?, " + REPORT_LAST_LOGIN_STORED + " = \"" + matches.get(0) +
					"\" WHERE id=?", new String[] {
					dbDateFormat.format(date),
					userId
			}) == 1;
		}

		logger.info(String.format(
				"Recorded %s for user %s with success: %s",
				field, userId, success));
		return success;
	}

	public String getLastLoginDate(String userId) {

		createUserIfNeeded(userId);

		DataAccess da = DataAccess.getInstance();

		List<String> matches = da.select("select " + REPORT_LAST_LOGIN_STORED + " from " + REPORTING_SUBTABLE +
				" where id = ?", new String[] { userId }, StringMapper.getInstance());
		if (matches.size() > 0)
		{
			Date date = null;
			try
			{
				date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(matches.get(0));
			}
			catch (ParseException e)
			{
				e.printStackTrace();
				return null;
			}
			return new SimpleDateFormat("dd/MM/yyyy HH:mm aa").format(date).replace("AM", "am").replace("PM","pm");
		}
		return null;
	}

	public boolean recordAccountCreation(String userId)
	{
		return recordDate(userId, REPORT_ACCOUNT_CREATED, new Date());
	}

	public boolean recordLastLogin(String userId)
	{
		return recordLoginDate(userId, REPORT_LAST_LOGIN, new Date());
	}

	public boolean recordProposalCreation(String userId)
	{
		return recordDate(userId, REPORT_LAST_CREATED_PROPOSAL, new Date());
	}

	public boolean recordProposalEditing(String userId)
	{
		return recordDate(userId, REPORT_LAST_EDITED_PROPOSAL, new Date());
	}

	public boolean recordProposalViewing(String userId)
	{
		return recordDate(userId, REPORT_LAST_VIEWED_PROPOSAL, new Date());
	}

	public boolean recordProposalManagement(String userId)
	{
		return recordDate(userId, REPORT_LAST_MANAGED_PROPOSAL, new Date());
	}

	public boolean recordLastViewedNotifications(String userId)
	{
		return recordDate(userId, REPORT_LAST_VIEWED_NOTIFICATIONS, new Date());
	}

	
}
