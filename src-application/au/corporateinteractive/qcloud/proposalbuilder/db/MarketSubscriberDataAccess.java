package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import webdirector.db.client.ManagedDataAccess;
import au.corporateinteractive.qcloud.market.enums.Market;
import au.corporateinteractive.qcloud.market.enums.SubscriptionStatus;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.IntegerMapper;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.dev.annotation.DbColumn;
import au.net.webdirector.dev.annotation.Module;

import com.stripe.model.Subscription;

/**
 * @author Sishir
 *
 */
@Module(name = "market_subscribers")
public class MarketSubscriberDataAccess extends ManagedDataAccess
{

	Logger logger = Logger.getLogger(MarketSubscriberDataAccess.class);

	public static final String MARKET_SUBSCRIBER_MODULE = "MARKET_SUBSCRIBERS";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_COMPANY_ID = "ATTRDROP_companyId";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_APP_ID = "ATTRDROP_application";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_COST = "ATTRCURRENCY_cost";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_STATUS = "ATTRDROP_status";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_SUBSCRIPTION_ID = "ATTR_subscriptionId";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_SUBSCRIBER = "ATTRDROP_subscriber";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_QTY = "ATTRINTEGER_QTY";

	public static MarketSubscriberDataAccess getInstance()
	{
		return new MarketSubscriberDataAccess();
	}

//	public void addSubscriptionRecord(String userId, CategoryData company, String appId, Subscription subscription, int qty)
//	{
//		ElementData appSubsription = getAppSubscription(company.getId(), appId);
//
//		if (appSubsription != null)
//		{
//			Hashtable<String, String> subscriber = new Hashtable<String, String>();
//			subscriber.put(E_ID, appSubsription.getId());
//			subscriber.put(E_STATUS, "SUBSCRIBED");
//			subscriber.put(E_SUBSCRIPTION_ID, subscription == null ? "" : subscription.getId());
//			subscriber.put(E_SUBSCRIBER, userId);
//			subscriber.put(E_QTY, String.valueOf(qty));
//			updateElement(subscriber, MARKET_SUBSCRIBER_MODULE);
//		}
//		else
//		{
//			int categoryId = getOrCreateCategory(company.getId(), company.getName());
//			ElementData application = new MarketDataAccess().getApplication(appId, company.getString(UserAccountDataAccess.C_ADDRESS_COUNTRY));
//
//			Hashtable<String, String> ht = standardCreateElementHashtable(categoryId, application.getName());
//			insertNotNull(ht, E_APP_ID, application.getId());
//			insertNotNull(ht, E_COST, application.getString("price"));
//			insertNotNull(ht, E_STATUS, SubscriptionStatus.TRIAL.toString());
//			insertNotNull(ht, E_SUBSCRIPTION_ID, subscription == null ? "" : subscription.getId());
//			insertNotNull(ht, E_SUBSCRIBER, userId);
//			insertNotNull(ht, E_QTY, String.valueOf(qty));
//			createElement(ht, MARKET_SUBSCRIBER_MODULE);
//
//			//update app subscriber count
//			MarketDataAccess.getInstance().updateSubscribersCount(appId);
//		}
//	}


	@Override
	public int getOrCreateCategory(String companyId, String companyName)
	{
		String query = "select c." + C_ID + " from categories_"
				+ MARKET_SUBSCRIBER_MODULE + " c where c." + C_COMPANY_ID + "=?";
		List categoryIds = DataAccess.getInstance()
				.select(query, new String[] { companyId }, new IntegerMapper());
		Integer categoryId = categoryIds.size() > 0 ? (int) (categoryIds.get(0)) : null;

		if (categoryId == null)
		{
			Hashtable<String, String> ht = standardCreateCategoryHashtable(companyName);
			insertNotNull(ht, C_LIVE, true);
			insertNotNull(ht, C_COMPANY_ID, companyId);
			categoryId = createCategory(ht, MARKET_SUBSCRIBER_MODULE);
		}

		return categoryId;
	}

	public ElementData getAppSubscription(String companyId, String appId)
	{
		String query = "select ems.* from elements_market_subscribers ems, categories_market_subscribers cms "
				+ "where ems.Category_id=cms.Category_id and cms.ATTRDROP_companyId=? and ems.ATTRDROP_application=?";
		try
		{
			List<ElementData> res = ModuleAccess.getInstance().getElements(query, new String[] { companyId, appId });
			return res.size() > 0 ? res.get(0) : null;
		}
		catch (SQLException e)
		{
			logger.error("Error", e);
		}
		return null;
	}

	public ElementData getAppSubscription(String stripeSubscriptionId)
	{
		String query = "select ems.* from elements_market_subscribers ems "
				+ "where ems.ATTR_subscriptionId=?";
		try
		{
			List<ElementData> res = ModuleAccess.getInstance().getElements(query, new String[] { stripeSubscriptionId });
			if (res.size() == 0)
			{
				logger.warn("Cannot find app subscription id = " + stripeSubscriptionId);
				return null;
			}
			return res.get(0);
		}
		catch (SQLException e)
		{
			logger.error("Error", e);
			return null;
		}
	}

	public void updateSubscriptionRecord(String appSubscriptionId, String subscriptionStatus)
	{
		String query = "update elements_market_subscribers set ATTRDROP_status = ? where Element_id = ?";
		try
		{
			TransactionDataAccess.getInstance().update(query, subscriptionStatus, appSubscriptionId);
		}
		catch (SQLException e)
		{
			logger.error("Error: " + e);
		}
	}

	public void updateSubscriptionRecordByStripeId(String stripeSubscriptionId, String subscriptionStatus)
	{
		String query = "update elements_market_subscribers set ATTRDROP_status = ? where ATTR_subscriptionId = ?";
		try
		{
			TransactionDataAccess.getInstance().update(query, subscriptionStatus, stripeSubscriptionId);
		}
		catch (SQLException e)
		{
			logger.error("Error: " + e);
		}
	}

	public boolean checkAppAccess(String companyId, Market app)
	{

		String query = "select ems.* from elements_market_subscribers ems, categories_market_subscribers cms, elements_market em "
				+ "where ems.Category_id=cms.Category_id and cms.ATTRDROP_companyId=? and ems.ATTRDROP_application=em.Element_id and em.ATTR_appCode=?";
		try
		{
			List<ElementData> res = ModuleAccess.getInstance().getElements(query, new String[] { companyId, app.toString() });
			ElementData appSubscription = res.size() > 0 ? res.get(0) : null;

			if (appSubscription == null)
			{
				return false;
			}

			if (StringUtils.equals(appSubscription.getString(E_STATUS), SubscriptionStatus.SUSPENDED.toString())
					|| StringUtils.equals(appSubscription.getString(E_STATUS), SubscriptionStatus.CANCELLED.toString()))
			{
				return false;
			}
		}
		catch (SQLException e)
		{
			logger.error("Error", e);
			return false;
		}
		return true;
	}

	public boolean checkAppAccessForTeam(String teamId, Market app)
	{
		try
		{
			String companyId = ModuleAccess.getInstance().getCategoryById(UserAccountDataAccess.USER_MODULE, teamId).getParentId();
			return checkAppAccess(companyId, app);
		}
		catch (SQLException e)
		{
			logger.error("Error", e);
		}
		return false;
	}

	public CategoryData getSubscriberCompany(String stripeSubscriptionId)
	{
		String query = "select cu.* from categories_users cu, categories_market_subscribers cms, elements_market_subscribers ems where cu.Category_id=cms.ATTRDROP_companyId and cms.Category_id=ems.Category_id and ems.ATTR_subscriptionId=?";
		try
		{
			return ModuleAccess.getInstance().getCategories(query, new String[] { stripeSubscriptionId }).get(0);
		}
		catch (SQLException e)
		{
			logger.error("Error: " + e);
			return null;
		}
	}

	public ElementData getAppSubscriber(String stripeSubscriptionId)
	{
		String query = "select eu.* from elements_useraccounts eu, categories_market_subscribers cms, elements_market_subscribers ems where eu.Element_id=cms.ATTRDROP_subscriber and cms.Category_id=ems.Category_id and ems.ATTR_subscriptionId=?";
		try
		{
			return ModuleAccess.getInstance().getElements(query, new String[] { stripeSubscriptionId }).get(0);
		}
		catch (SQLException e)
		{
			logger.error("Error: " + e);
			return null;
		}
	}

	public List<CategoryData> getAppSubscribers(Market app)
	{
		String query = "select cms.* from categories_market_subscribers cms, elements_market_subscribers ems, elements_market em "
				+ "where cms.Category_id=ems.Category_id and ems.ATTRDROP_application=em.Element_id and em.ATTR_appCode=?";
		List<CategoryData> subscribers = new ArrayList<CategoryData>();
		try
		{
			subscribers = ModuleAccess.getInstance().getCategories(query, new String[] { app.toString() });
		}
		catch (Exception e)
		{
			logger.error("Error", e);
		}
		return subscribers;
	}


	public void processUserDelete(String userId)
	{

	}


}
