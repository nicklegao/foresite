//package au.corporateinteractive.qcloud.proposalbuilder.db;
//
//import java.io.File;
//import java.sql.SQLException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Hashtable;
//import java.util.List;
//
//import org.apache.log4j.Logger;
//
//import au.net.webdirector.common.datalayer.admin.db.DataAccess;
//import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
//import au.net.webdirector.common.datalayer.client.CategoryData;
//import au.net.webdirector.common.datalayer.client.ElementData;
//import au.net.webdirector.common.datalayer.client.ModuleAccess;
//import au.net.webdirector.common.utils.module.StoreUtils;
//import au.net.webdirector.dev.annotation.DbColumn;
//import au.net.webdirector.dev.annotation.Module;
//import webdirector.db.client.ManagedDataAccess;
//
//@Module(name = "signatures")
//public class SignaturesDataAccess extends ManagedDataAccess
//{
//	Logger logger = Logger.getLogger(SignaturesDataAccess.class);
//
//	public static final String SIGNATURES_MODULE = "SIGNATURES";
//	@DbColumn(type = DbColumn.TYPE_CATEGORY)
//	public static final String C_CATEGORY_NAME = "ATTR_categoryName";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_DATE = "ATTRDATE_Date";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_PROPOSAL_ID = "ATTRDROP_proposalId";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_FILE = "ATTRFILE_File";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_FIRST_NAME = "ATTR_FirstName";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_FONT = "ATTR_Font";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_HEADLINE = "ATTR_Headline";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_LAST_NAME = "ATTR_LastName";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_TEXT = "ATTR_Text";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_TYPE = "ATTR_Type";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IP = "ATTR_ip";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_CHECKSUM = "ATTR_Checksum";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_USER_AGENT = "ATTR_UserAgent";
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_IMAGE_DATA = "ATTR_imageData";
//
//	private StoreUtils su = new StoreUtils();
//
//	private Hashtable<String, String> prepareSignature(String proposalId, String uuid, String firstName, String lastName, String ip, String userAgent)
//	{
//		Date d = new Date();
//		int categoryId = getCategory(proposalId, d, SIGNATURES_MODULE);
//
//		Hashtable<String, String> ht = standardCreateElementHashtable(categoryId, uuid);
//		insertNotNull(ht, E_LIVE, true);
//		insertNotNull(ht, E_PROPOSAL_ID, proposalId);
//		insertNotNull(ht, E_FIRST_NAME, firstName);
//		insertNotNull(ht, E_LAST_NAME, lastName);
//		insertNotNull(ht, E_IP, ip);
//		insertNotNull(ht, E_USER_AGENT, userAgent);
//		insertNotNull(ht, E_DATE, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d));
//		return ht;
//	}
//
//	public int getCategory(String proposalId, Date date, String signaturesModule)
//	{
//		TravelDocsDataAccess pda = new TravelDocsDataAccess();
//		Hashtable<String, String> proposal = pda.getTravelDocById(proposalId);
//		CategoryData userCompany = pda.getCompanyByTravelDoc(proposal == null ? pda.getNewestTravelDocRevisionById(proposalId) : proposal);
//
//		int companyCategory = getOrCreateCategory(userCompany.getId(), userCompany.getName(), SIGNATURES_MODULE);
//		int yearID = getOrCreateCategory(new SimpleDateFormat("yyyy").format(date), companyCategory, 2, SIGNATURES_MODULE);
//		int monthID = getOrCreateCategory(new SimpleDateFormat("MM MMM").format(date), yearID, 3, SIGNATURES_MODULE);
//		int dayID = getOrCreateCategory(new SimpleDateFormat("dd EEE").format(date), monthID, 4, SIGNATURES_MODULE);
//
//		return dayID;
//	}
//
//	public int createUploadSignature(String proposalId, String uuid, String firstName, String lastName, String ip, String userAgent, File signature)
//	{
//		Hashtable<String, String> ht = prepareSignature(proposalId, uuid, firstName, lastName, ip, userAgent);
//		insertNotNull(ht, E_TYPE, "UPLOAD");
//		int eId = createElement(ht, SIGNATURES_MODULE);
//		insertNotNull(ht, E_ID, eId);
//		String storePath = su.moveFileToStores(SIGNATURES_MODULE, eId, E_FILE, signature);
//		insertNotNull(ht, E_FILE, storePath);
//		updateElement(ht, SIGNATURES_MODULE);
//		return eId;
//	}
//
//	public int createTypeSignature(String proposalId, String uuid, String firstName, String lastName, String ip, String userAgent, String text, String font)
//	{
//		Hashtable<String, String> ht = prepareSignature(proposalId, uuid, firstName, lastName, ip, userAgent);
//		insertNotNull(ht, E_TYPE, "TYPE");
//		insertNotNull(ht, E_TEXT, text);
//		insertNotNull(ht, E_FONT, font);
//		int eId = createElement(ht, SIGNATURES_MODULE);
//		return eId;
//	}
//
//	public int createDrawSignature(String proposalId, String uuid, String firstName, String lastName, String ip, String userAgent, String imageData)
//	{
//		Hashtable<String, String> ht = prepareSignature(proposalId, uuid, firstName, lastName, ip, userAgent);
//		insertNotNull(ht, E_TYPE, "DRAW");
//		insertNotNull(ht, E_IMAGE_DATA, imageData);
//		int eId = createElement(ht, SIGNATURES_MODULE);
//		return eId;
//	}
//
//	public void deleteAllSignaturesForProposal(String proposalId)
//	{
//		DataAccess da = DataAccess.getInstance();
//
//		List<String> toDelete = da.select("select Element_id " +
//				"from elements_signatures " +
//				"where ATTRDROP_proposalId = ?",
//				new String[] { proposalId }, StringMapper.getInstance());
//
//		for (String attachmentId : toDelete)
//		{
//			deleteElement(attachmentId, SIGNATURES_MODULE);
//		}
//	}
//
//	public ElementData getSignature(String proposalId, String uuid)
//	{
//		try
//		{
//			List<ElementData> list = ModuleAccess.getInstance().getElements("select * from elements_signatures where ATTRDROP_proposalId = ? and ATTR_Headline = ?", new String[] { proposalId, uuid });
//			if (list.size() == 0)
//			{
//				return null;
//			}
//			return list.get(0);
//		}
//		catch (SQLException e)
//		{
//			logger.error("Error:", e);
//			return null;
//		}
//	}
//
//	public ElementData getLastSignature(String proposalId)
//	{
//		try
//		{
//			List<ElementData> list = ModuleAccess.getInstance().getElements("select * from elements_signatures where ATTRDROP_proposalId = ? order by ATTRDATE_Date desc limit 1", new String[] { proposalId });
//			if (list.size() == 0)
//			{
//				return null;
//			}
//			return list.get(0);
//		}
//		catch (SQLException e)
//		{
//			logger.error("Error:", e);
//			return null;
//		}
//	}
//
//	public List<ElementData> getSignaturesForProposal(String proposalId)
//	{
//		try
//		{
//			List<ElementData> list = ModuleAccess.getInstance().getElements("select * from elements_signatures where ATTRDROP_proposalId = ?", new String[] { proposalId });
//			if (list.size() == 0)
//			{
//				return null;
//			}
//			return list;
//		}
//		catch (SQLException e)
//		{
//			logger.error("Error:", e);
//			return null;
//		}
//	}
//
//	public int updateSignatureWithChecksum(String elementId, String checksum)
//	{
//		Hashtable<String, String> ht = new Hashtable<>();
//		insertNotNull(ht, E_ID, elementId);
//		insertNotNull(ht, E_CHECKSUM, checksum);
//		int eId = updateElement(ht, SIGNATURES_MODULE);
//		return eId;
//	}
//
//	public static void deleteSignatureForProposal(String proposalId)
//	{
//		int updated = DataAccess.getInstance().updateData(
//				"delete from elements_" + SIGNATURES_MODULE +
//						" where ATTRDROP_proposalId=?",
//				new String[] { proposalId });
//	}
//}
