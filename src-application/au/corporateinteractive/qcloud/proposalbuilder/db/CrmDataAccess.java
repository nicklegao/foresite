/**
 * @author Jaysun Lee
 * @date 23 Dec 2015
 */
package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.sql.SQLException;
import java.util.Hashtable;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import au.corporateinteractive.qcloud.proposalbuilder.db.util.DataMapLegacyConverter;
import au.corporateinteractive.qcloud.subscription.Country;
import au.corporateinteractive.qcloud.subscription.SubscriptionPlan;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;

/**
 * Class to manage integration with the CRM system.
 * No operations on the local database are performed here,
 * only exchange of data via HTTP to the CRM.
 */
public class CrmDataAccess
{
	private static Logger logger = Logger.getLogger(CrmDataAccess.class);

	/**
	 * @param newPlan
	 * @param userTable
	 * @throws Exception
	 */
	public boolean createNewContract(Hashtable<String, String> userAndCompanyTable, SubscriptionPlan newPlan)
	{
		RestTemplate restTemplate = new RestTemplate();

		Country country = Country.valueOf(userAndCompanyTable.get(UserAccountDataAccess.C_ADDRESS_COUNTRY));
		userAndCompanyTable.put("price", Double.toString(newPlan.getPrice()));
		userAndCompanyTable.put("planDescription", newPlan.getDescription());

		HttpEntity<Hashtable<String, String>> entity = new HttpEntity<Hashtable<String, String>>(userAndCompanyTable);
		ResponseEntity<Integer> response = restTemplate.exchange(Defaults.getInstance().getCrmUrl() + "/wd/qcloud/createCustomerAndContract", HttpMethod.POST, entity, Integer.class);

		if (response.getStatusCode().equals(HttpStatus.OK))
		{
			int contractId = response.getBody();
			ModuleAccess ma = ModuleAccess.getInstance();
			try
			{
				CategoryData companyData = new CategoryData();
				companyData.put(UserAccountDataAccess.C_CRM_CONTRACT_ID, contractId);
				CategoryData conditions = new CategoryData();
				conditions.setId(userAndCompanyTable.get("Category_id"));

				ma.getDataAccess().update(companyData, "Categories_" + UserAccountDataAccess.USER_MODULE, conditions);
				return true;
			}
			catch (SQLException e)
			{
				logger.error("Error:", e);
				return false;
			}
		}
		else
		{
			logger.error("Error: Sending data to CRM system failed");
			logger.error("Error: " + response.getStatusCode());
			logger.error(userAndCompanyTable);
			return false;
		}

	}

	// Will create new contract with supplied pricingPlan param instead of 
	// using the pricing plan assigned to the company from before (eg. during registration)
	public boolean createNewContract(String userId, SubscriptionPlan newPlan)
	{
		UserAccountDataAccess ada = new UserAccountDataAccess();

		Hashtable<String, String> userTable = ada.getUserWithId(userId);
		CategoryData company = ada.getUserCompanyForUserId(userId);
		DataMapLegacyConverter dmlc = new DataMapLegacyConverter();

		return createNewContract(userTable, dmlc.convertFromDataMap(company), newPlan);
	}

	/**
	 * Wrapper function to combine retrieved data
	 * 
	 * @param newPlan
	 * 
	 * @param userId
	 * @return
	 */
	public boolean createNewContract(Hashtable<String, String> userTable, Hashtable<String, String> companyTable, SubscriptionPlan newPlan)
	{
		Hashtable<String, String> userAndCompanyTable = new Hashtable<String, String>(userTable);
		userAndCompanyTable.putAll(companyTable);

		return createNewContract(userAndCompanyTable, newPlan);
	}


	public boolean updateContract(int contractId, SubscriptionPlan subscription)
	{
		try
		{
			RestTemplate restTemplate = new RestTemplate();

			Hashtable<String, String> contractData = new Hashtable<String, String>();
			contractData.put("contractId", Integer.toString(contractId));
			contractData.put("planPrice", Double.toString(subscription.getPrice()));
			contractData.put("planDescription", subscription.getDescription());

			HttpEntity<Hashtable<String, String>> entity = new HttpEntity<Hashtable<String, String>>(contractData);
			ResponseEntity<Boolean> response = restTemplate.exchange(Defaults.getInstance().getCrmUrl() + "/wd/qcloud/updateContract", HttpMethod.POST, entity, Boolean.class);

			if (response.getStatusCode().equals(HttpStatus.OK))
			{
				boolean success = response.getBody();
				return success;
			}
			return false;
		}
		catch (Exception e)
		{
			logger.error("Error: ", e);
			return false;
		}


	}

	/**
	 * @param company
	 */
	public boolean cancelContract(String contractId)
	{
		if (StringUtils.isBlank(contractId) || "0".equals(contractId))
		{
			return false;
		}
		try
		{
			RestTemplate restTemplate = new RestTemplate();

			Hashtable<String, String> contractData = new Hashtable<String, String>();
			contractData.put("element_id", contractId);
			contractData.put("live", "0");

			HttpEntity<Hashtable<String, String>> entity = new HttpEntity<Hashtable<String, String>>(contractData);
			ResponseEntity<Boolean> response = restTemplate.exchange(Defaults.getInstance().getCrmUrl() + "/wd/qcloud/updateContractNew", HttpMethod.POST, entity, Boolean.class);

			if (response.getStatusCode().equals(HttpStatus.OK))
			{
				boolean success = response.getBody();
				return success;
			}
			return false;
		}
		catch (Exception e)
		{
			logger.error("Error: ", e);
			return false;
		}

	}
}
