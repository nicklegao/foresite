package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import webdirector.db.client.ExtendedClientDataAccess;
import webdirector.db.client.ManagedDataAccess;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.utils.Encryptor;

/**
 * Created by andrewdavidson on 15/05/2017.
 */
public class IntegrationEcoSystemDataAccess extends ManagedDataAccess
{
	private static Logger logger = Logger.getLogger(IntegrationEcoSystemDataAccess.class);

	public ElementData getCompanyContact(String CONTACTS_MODULE, String C_COMPANY_ID, String companyId, String contactId)
	{
		logger.debug("getCompanyContact " + companyId + " " + contactId + " " + CONTACTS_MODULE + " " + C_COMPANY_ID);
		try
		{
			ModuleAccess ma = ModuleAccess.getInstance();
			CategoryData companyFolder = ma.getCategoryByField(CONTACTS_MODULE, C_COMPANY_ID, companyId);
			if (companyFolder == null)
			{
				return null;
			}
			logger.debug("Getting Company Contacts for (String/Vector) " + companyId);
			ElementData condition = new ElementData();
			condition.setId(contactId);
			condition.setParentId(companyFolder.getId());
			List<ElementData> contacts = ma.getElements(CONTACTS_MODULE);
			if (contacts.size() == 0)
			{
				return null;
			}
			return contacts.get(0);
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return null;
		}
	}

	public List<ElementData> getCompanyContacts(String CONTACTS_MODULE, String C_COMPANY_ID, String companyId)
	{
		logger.debug("getCompanyContacts " + companyId + " " + CONTACTS_MODULE + " " + C_COMPANY_ID);
		try
		{
			ModuleAccess ma = ModuleAccess.getInstance();
			CategoryData companyFolder = ma.getCategoryByField(CONTACTS_MODULE, C_COMPANY_ID, companyId);
			if (companyFolder == null)
			{
				return new ArrayList<ElementData>();
			}
			logger.debug("Getting Company Contacts for (String/Vector) " + companyId);
			String contactCols = StringUtils.join(new ExtendedClientDataAccess().getElementCols(CONTACTS_MODULE), ", ");
			contactCols = modifyContactColsForDecryption(contactCols);

			String query = "select " + contactCols + " from elements_zapier_crm_contacts where category_id=?";

			return ma.getElements(query, new String[] { companyFolder.getId() });

		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return new ArrayList<ElementData>();
		}
	}

	private String modifyContactColsForDecryption(String contactCols)
	{
		contactCols = contactCols.replace("ATTR_Headline", "if(ATTRCHECK_clientDataEncrypted, aes_decrypt(unhex(ATTR_Headline), '" + Encryptor.getKey() + "'), ATTR_Headline) ATTR_Headline");
		contactCols = contactCols.replace("ATTR_FirstName", "if(ATTRCHECK_clientDataEncrypted, aes_decrypt(unhex(ATTR_FirstName), '" + Encryptor.getKey() + "'), ATTR_FirstName) ATTR_FirstName");
		contactCols = contactCols.replace("ATTR_SecondName", "if(ATTRCHECK_clientDataEncrypted, aes_decrypt(unhex(ATTR_SecondName), '" + Encryptor.getKey() + "'), ATTR_SecondName) ATTR_SecondName");
		contactCols = contactCols.replace("ATTR_CompanyName", "if(ATTRCHECK_clientDataEncrypted, aes_decrypt(unhex(ATTR_CompanyName), '" + Encryptor.getKey() + "'), ATTR_CompanyName) ATTR_CompanyName");
		contactCols = contactCols.replace("ATTR_Address", "if(ATTRCHECK_clientDataEncrypted, aes_decrypt(unhex(ATTR_Address), '" + Encryptor.getKey() + "'), ATTR_Address) ATTR_Address");
		return contactCols;
	}


}
