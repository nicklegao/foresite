package au.corporateinteractive.qcloud.proposalbuilder.db.util;

/**
 * Created by andrewdavidson on 19/05/2017.
 */
public enum NoteStatus
{
    accepted("accepted", "Status changed to Accepted" ),
    sent("sent", "Status changed to Sent"),
    creating("creating", "Created Proposal ID"),
    closed("closed", "Status changed to Closed"),
    read("read", "Status changed to Read"),
    lost("lost", "Status changed to Lost"); // status is still closed

    private String status;
    private String noteStatus;

    NoteStatus(String status, String noteStatus) {
        this.status = status;
        this.noteStatus = noteStatus;
    }

    public String status() {
        return status;
    }
    public String noteStatus() {
        return noteStatus;
    }
}
