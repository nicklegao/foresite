package au.corporateinteractive.qcloud.proposalbuilder.db.util;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.base.db.DataMap;

public class DataMapLegacyConverter
{
	private static Logger logger = Logger.getLogger(DataMapLegacyConverter.class);


//	public DataMap convertToDataMap(Hashtable<String, String> item)
//	{
//		DataMap convertedItem = new DataMap();
//		for (String key : item.keySet())
//		{
//			convertedItem.put(key, item.get(key));
//		}
//		return convertedItem;
//	}

	public <T extends DataMap> Hashtable<String, String> convertFromDataMap(T item)
	{
		Hashtable<String, String> convertedItem = new Hashtable<String, String>();
		for (String key : item.getFields())
		{
			convertedItem.put(key, item.getString(key));
		}
		return convertedItem;
	}

	public <T extends DataMap> Vector<Hashtable<String, String>> convertFromDataMaps(List<T> items)
	{
		Vector<Hashtable<String, String>> convertedItems = new Vector<Hashtable<String, String>>();
		for (DataMap item : items)
		{
			convertedItems.add(convertFromDataMap(item));
		}
		return convertedItems;
	}

}
