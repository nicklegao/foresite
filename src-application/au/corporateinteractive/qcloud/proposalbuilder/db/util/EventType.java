package au.corporateinteractive.qcloud.proposalbuilder.db.util;

public enum EventType{
    Consultant("consultant", "Consultant"),
    Customer("customer", "Customer"),
    System("system", "System");
    
    private String dbText;
    private String titleText;
    
    private EventType(String dbText, String titleText) {
        this.dbText = dbText;
        this.titleText = titleText;
    }
    
    public String getDbText() {
        return dbText;
    }
    
    public String getTitleText() {
        return titleText;
    }
    
    public static EventType typeWithText(String text) {
        for (EventType t : EventType.values())
        {
            if (t.getDbText().equalsIgnoreCase(text))
            {
                return t;
            }
        }
        return null;
    }
}
