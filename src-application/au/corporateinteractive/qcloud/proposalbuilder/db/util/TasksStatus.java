package au.corporateinteractive.qcloud.proposalbuilder.db.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public enum TasksStatus
{
	Error(0, "error", "err", false, false, false, false, null),
	Draft(100, "draft", "draft", false, true, true, false, null),
	NotStarted(200, "notstarted", "not-started", false, true, true, false, null),
	WIP(300, "wip", "wip", true, true, false, false, "WIP"),
	Complete(400, "complete", "complete", true, false, false, false, "Complete"),
	Incomplete(400, "incomplete", "needsAction", true, true, false, true, null);


	//@formatter:on

	private final int rank;
	private final String dbText;
	private final String bucketName;
	private final String reportAs;
	private final boolean rekeyOnChange;
	private final boolean editable;
	private final boolean deletable;
	private final boolean emailOnUp;

	private TasksStatus(int rank, String dbText, String bucketName, boolean rekeyOnChange, boolean editable, boolean deletable, boolean emailOnUp, String reportAs)
	{
		this.rank = rank;
		this.dbText = dbText;
		this.bucketName = bucketName;
		this.editable = editable;
		this.rekeyOnChange = rekeyOnChange;
		this.deletable = deletable;
		this.emailOnUp = emailOnUp;
		this.reportAs = reportAs;
	}

	public int getRank()
	{
		return rank;
	}

	public String getDbText()
	{
		return dbText;
	}

	public String getBucketName()
	{
		return bucketName;
	}

	public String getReportField()
	{
		return reportAs;
	}

	public boolean shoudRekeyOnChange()
	{
		return rekeyOnChange;
	}

	public boolean sendEmailOnUp()
	{
		return emailOnUp;
	}

	public boolean isEditable()
	{
		return editable;
	}

	public boolean isDeletable()
	{
		return deletable;
	}

	public static TasksStatus withValue(String value)
	{
		for (TasksStatus ps : TasksStatus.values())
		{
			String valuew = ps.dbText;
			if (valuew.equalsIgnoreCase(value))
			{
				return ps;
			}
		}
		return Error;
	}

	public static List<TasksStatus> inBucket(String value)
	{
		ArrayList<TasksStatus> list = new ArrayList<TasksStatus>();
		for (TasksStatus ps : TasksStatus.values())
		{
			if (ps.bucketName.equalsIgnoreCase(value))
			{
				list.add(ps);
			}
		}
		return list;
	}

	public static String getStatusesAboveAndEqualRankForDb(int rank)
	{
		ArrayList<String> relevantStatus = new ArrayList<String>();
		for (TasksStatus s : TasksStatus.values())
		{
			if (s.getRank() >= rank)
			{
				relevantStatus.add(s.getDbText());
			}
		}
		return "'" + StringUtils.join(relevantStatus, "', '") + "'";
	}

	public boolean canChangeTo(TasksStatus setStatusTo)
	{
		return this.rank < setStatusTo.rank;
	}
}