package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import webdirector.db.client.ManagedDataAccess;
import au.corporateinteractive.qcloud.market.enums.Market;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
import au.net.webdirector.dev.annotation.Module;

/**
 * @author Sishir
 *
 */
@Module(name = "plugin")
public class PluginSettingDataAccess extends ManagedDataAccess
{
	private static Logger logger = Logger.getLogger(PluginSettingDataAccess.class);

	public boolean checkSchedulerAccess(Market plugin) throws ParseException
	{
		String lock = getPluginSetting(plugin.toString(), "lock");
		Boolean check = StringUtils.equals(lock, "false") ? true : false;
		if (!check) {
			return checkTime(plugin.toString(), "lock", 15);
		}
		return check;

	}

	public Boolean checkTime(String pluginCode, String variable, Integer timeDiff) throws ParseException
	{
		String query = "select e.ATTRDATE_runTime from elements_plugin_settings e, categories_plugin_settings c "
				+ "where e.Category_Id=c.Category_Id and c.ATTRDROP_plugin=? and e.ATTR_variableName=?";
		String runTime = (String) DataAccess.getInstance().select(query, new String[] { pluginCode, variable }, new StringMapper()).get(0);
		SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date runDate = dbDateFormat.parse(runTime);
		return ((new Date().getTime() - runDate.getTime()) >= timeDiff*60*100) ? true : false;
	}

	private String getPluginSetting(String pluginCode, String variable)
	{
		String query = "select e.ATTR_value from elements_plugin_settings e, categories_plugin_settings c "
				+ "where e.Category_Id=c.Category_Id and c.ATTRDROP_plugin=? and e.ATTR_variableName=?";
		return (String) DataAccess.getInstance().select(query, new String[] { pluginCode, variable }, new StringMapper()).get(0);
	}

	public void freeScheduler(Market plugin)
	{
		String query = "update elements_plugin_settings e, categories_plugin_settings c set e.attr_value=? "
				+ "where e.Category_id=c.Category_id and c.ATTRDROP_plugin=? and e.ATTR_variableName=?;";
		try
		{
			TransactionDataAccess.getInstance().update(query, "false", plugin.toString(), "lock");
		}
		catch (SQLException e)
		{
			logger.error("Error", e);
		}

	}

	public void lockScheduler(Market plugin)
	{
		String query = "update elements_plugin_settings e, categories_plugin_settings c set e.attr_value=? "
				+ "where e.Category_id=c.Category_id and c.ATTRDROP_plugin=? and e.ATTR_variableName=?;";
		try
		{
			TransactionDataAccess.getInstance().update(query, "true", plugin.toString(), "lock");
		}
		catch (SQLException e)
		{
			logger.error("Error", e);
		}

	}

	public void setTimer(Market plugin)
	{
		String query = "update elements_plugin_settings e, categories_plugin_settings c set e.ATTRDATE_runTime=? "
				+ "where e.Category_id=c.Category_id and c.ATTRDROP_plugin=? and e.ATTR_variableName=?;";
		try
		{
			TransactionDataAccess.getInstance().update(query, new Date(), plugin.toString(), "lock");
		}
		catch (SQLException e)
		{
			logger.error("Error", e);
		}

	}
}
