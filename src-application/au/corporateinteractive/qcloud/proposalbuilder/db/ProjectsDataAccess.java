package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.dev.annotation.DbColumn;
import au.net.webdirector.dev.annotation.Module;
import webdirector.db.client.ManagedDataAccess;

@Module(name = "PROJECTS")
public class ProjectsDataAccess extends ManagedDataAccess
{
	public static final Logger logger = Logger.getLogger(ProjectsDataAccess.class);

	public static final String MODULE = "PROJECTS";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_COMPANY_ID = "ATTRDROP_companyID";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_END_DATE = "ATTRDATE_EndDate";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_START_DATE = "ATTRDATE_StartDate";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_SITE_ADDRESS = "ATTR_SiteAddress";

	public ElementData saveProject(String projectName, CategoryData company)
	{
		return saveProject(projectName, null, null, null, false, company);
	}
	
	public boolean saveProject(String projectId, String projectName, CategoryData company)
	{
		try {

			int i = getOrCreateCategory(company.getId(), company.getName(), MODULE);
			return 1==TransactionDataAccess.getInstance().update("update elements_projects set attr_headline = ? where category_id = ? and element_id = ?", projectName, String.valueOf(i), projectId);
		} catch (SQLException e) {
			return false;
		}
	}

	public ElementData saveProject(String projectName, String address, Date start, Date end, boolean updateIfExists, CategoryData company)
	{
		int cid = getOrCreateCategory(company.getId(), company.getName(), MODULE);
		try (TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true)))
		{
			TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
			List<ElementData> projects = ma.getElements("select * from elements_projects where category_id = ? and attr_headline = ?", String.valueOf(cid), projectName);
			ElementData project = null;
			if (updateIfExists && projects.size() > 0)
			{
				project = projects.get(0);
				ma.updateElement(project, MODULE);
			}
			else
			{
				project = new ElementData();
				project.setLive(true);
				project.setParentId(String.valueOf(cid));
			}

			project.setName(projectName);
			project.put(E_SITE_ADDRESS, address);
			project.put(E_START_DATE, start);
			project.put(E_END_DATE, end);
			if (StringUtils.isBlank(project.getId()))
			{
				ma.insertElement(project, MODULE);
			}
			else
			{
				ma.updateElement(project, MODULE);
			}
			txManager.commit();
			return project;
		}
		catch (Exception e)
		{
			logger.error("", e);
			return null;
		}
	}

	public List<ElementData> list(CategoryData company)
	{
		try
		{
			int cid = getOrCreateCategory(company.getId(), company.getName(), MODULE);
			return ModuleAccess.getInstance().getElementsByParentId(MODULE, String.valueOf(cid));
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return new ArrayList<>();
		}
	}

}
