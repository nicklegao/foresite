/**
 * @author Nick Yiming Gao
 * @date 16/01/2017 2:43:42 pm
 */
package au.corporateinteractive.qcloud.proposalbuilder.db;

/**
 * 
 */

import java.sql.SQLException;

import org.apache.log4j.Logger;

import webdirector.db.client.ManagedDataAccess;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;

public class ResellerDataAccess extends ManagedDataAccess
{

	private static Logger logger = Logger.getLogger(ResellerDataAccess.class);

	/**
	 * <b>The internal module name</b>
	 */
	public static final String MODULE = "RESELLER";


	/**
	 * <b>ATTR_categoryName</b><br/>
	 * Display: Folder Name
	 */
	public static final String C_CATEGORY_NAME = "ATTR_categoryName";


	/**
	 * <b>ATTRCHECK_GstRegistered</b><br/>
	 * Display: Gst Registered
	 */
	public static final String E_GST_REGISTERED = "ATTRCHECK_GstRegistered";

	/**
	 * <b>ATTRINTEGER_CommissionPercentage</b><br/>
	 * Display: Commission Percentage
	 */
	public static final String E_COMMISSION_PERCENTAGE = "ATTRINTEGER_CommissionPercentage";

	/**
	 * <b>ATTR_Headline</b><br/>
	 * Display: Email
	 */
	public static final String E_HEADLINE = "ATTR_Headline";

	/**
	 * <b>ATTR_Name</b><br/>
	 * Display: Name
	 */
	public static final String E_NAME = "ATTR_Name";

	/**
	 * @param string
	 * @return
	 */
	public ElementData getResellerByEmail(String email)
	{
		try
		{
			return ModuleAccess.getInstance().getElementByName(MODULE, email);
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return null;
		}
	}
}
