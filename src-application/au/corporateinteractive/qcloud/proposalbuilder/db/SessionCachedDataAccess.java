package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.util.HashMap;
import java.util.Set;

import javax.servlet.http.HttpSession;


public class SessionCachedDataAccess {

  private static final String USER_ID_TO_ACCESS_RIGHT_MAP = "userIdToAccessRightMap";
  private volatile static SessionCachedDataAccess instance;

  private SessionCachedDataAccess() {

  }

  public static SessionCachedDataAccess getInstance() {
    if (instance == null) {
      synchronized (SessionCachedDataAccess.class) {
        if (instance == null) {
          instance = new SessionCachedDataAccess();
        }
      }
    }
    return instance;
  }

  public Set<String> getUserAccessRightsSet(String userId, HttpSession session) {
    if (session.getAttribute(USER_ID_TO_ACCESS_RIGHT_MAP) == null) {
      session.setAttribute(USER_ID_TO_ACCESS_RIGHT_MAP, new HashMap<String, Set<String>>());
    }
    HashMap<String, Set<String>> userIdToAccessRightMap = (HashMap<String, Set<String>>)session.getAttribute(USER_ID_TO_ACCESS_RIGHT_MAP);
    Set<String> accessRightsSetFromSession = userIdToAccessRightMap.get(userId);
    if (accessRightsSetFromSession != null) {
      return accessRightsSetFromSession;
    }
    UserAccountDataAccess uada = new UserAccountDataAccess();
    Set<String> userAccessRightsSet = uada.getUserAccessRightsSet(userId);
    userIdToAccessRightMap.put(userId, userAccessRightsSet);
    return userAccessRightsSet;
  }
  
}
