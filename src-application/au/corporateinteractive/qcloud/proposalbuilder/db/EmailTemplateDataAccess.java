/**
 * @author Nick Yiming Gao
 * @date 2015-8-28
 */
package au.corporateinteractive.qcloud.proposalbuilder.db;

/**
 * 
 */

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.TextFile;
import au.net.webdirector.common.datalayer.base.db.mapper.StringArrayMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.common.utils.module.CategoryService;
import webdirector.db.client.ClientFileUtils;
import webdirector.db.client.ManagedDataAccess;

public class EmailTemplateDataAccess extends ManagedDataAccess
{

	private static Logger logger = Logger.getLogger(EmailTemplateDataAccess.class);

	public static final String EMAIL_TEMPLATES_MODULE = "EMAIL_TEMPLATES";
	public static final String ACTIVE_CATEGORY_NAME = "Editable";

	public static final String E_EMAIL_BODY_BLOCK = "ATTRLONG_email_body";

	private List<String[]> getTemplates(String templateName, String companyId)
	{
		QueryBuffer query = new QueryBuffer();
		query.append("select ATTRLONG_email_body, ATTR_Subject ");
		query.append("from ");
		query.append(" elements_email_templates e, categories_email_templates c1, categories_email_templates c2 ");
		query.append("where c2.category_parentId = c1.category_id and c2.category_id = e.category_id ");
		query.append("and e.ATTR_Headline = ? ", templateName);
		query.append("and ( ");
		query.append("  (c1.ATTRDROP_CompanyID = '-1') ");
		query.append("  or (c1.ATTRDROP_CompanyID = ?) ", companyId);
		query.append(") ");
		query.append("order by c1.ATTRDROP_CompanyID desc limit 1");

		return DataAccess.getInstance().select(query.getSQL(), query.getParams(), new StringArrayMapper());
	}

	public String readContentWithUserId(String companyID, String name) {

		List<String[]> list = getTemplates(name, companyID);
		if(list.size() != 0)
		{
			String filePath = list.get(0)[0];
			ClientFileUtils cfu = new ClientFileUtils();
			String content = cfu.readTextContents(cfu.getFilePathFromStoreName(filePath), true).toString();
			return content;
		}
		else
		{

		}
		return "";
	}

	public String readSubjectWithUserId(String companyID, String name)
	{

		List<String[]> templateData = getTemplates(name, companyID);

		if(templateData.size() > 0)
		{
			return templateData.get(0)[1];
		}
		return "";
	}

	public void updateTemplate(String userID, String templateName, String subject, String contents) throws IOException
	{
//		CategoryData company = new UserAccountDataAccess().getUserCompanyForUserId(userID);
//		getActiveTemplateCategory(company.getId(), company.getName());
//		String query = "select ATTRLONG_email_body from elements_email_templates e, categories_email_templates c1, categories_email_templates c2 where c2.category_parentId = c1.category_id and c2.category_id = e.category_id and e.ATTR_Headline = ? and c1.ATTRDROP_CompanyID = ?";
//		List list = DataAccess.getInstance().select(query, new String[] { templateName, company.getId() }, new StringMapper());
//		String filePath = (String) list.get(0);
//		ClientFileUtils cfu = new ClientFileUtils();
//		cfu.overwriteTextContents(cfu.getFilePathFromStoreName(filePath), contents);

		try
		{
			// create if not exists
			UserAccountDataAccess uada = new UserAccountDataAccess();
			CategoryData company = uada.getUserCompanyForUserId(userID);

//			int c1id = getOrCreateCategory(company.getId(), company.getName(), EMAIL_TEMPLATES_MODULE);
			int c2id = getActiveTemplateCategory(company.getId(), company.getName());

			ModuleAccess ma = ModuleAccess.getInstance();
			ElementData template = new ElementData();
			template.setName(templateName);
			template.setParentId(String.valueOf(c2id));
			template = ma.getElement(EMAIL_TEMPLATES_MODULE, template);
			if (template != null)
			{
				ma.getDataAccess().update("update elements_email_templates set attr_subject = ? where element_id = ?", subject, template.getId());
				ClientFileUtils cfu = new ClientFileUtils();
				cfu.overwriteTextContents(cfu.getFilePathFromStoreName(template.getString("ATTRLONG_email_body")), contents);
				return;
			}
			TextFile textFile = new TextFile(contents, EMAIL_TEMPLATES_MODULE, "ATTRLONG_email_body", false);
			ElementData newTemplate = new ElementData();
			newTemplate.setName(templateName);
			newTemplate.setParentId(String.valueOf(c2id));
			newTemplate.setLive(true);
			newTemplate.put("ATTRLONG_email_body", textFile);
			newTemplate.put("ATTR_Subject", subject);
			TransactionManager tx = TransactionManager.getInstance(TransactionDataAccess.getInstance(), StoreFileAccessFactory.getInstance());
			try
			{
				TransactionModuleAccess.getInstance(tx).insertElement(newTemplate, EMAIL_TEMPLATES_MODULE);
				tx.commit();
			}
			catch (Exception e)
			{
				tx.rollback();
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}


	public Map<String, String> readDefaultTemplate(String templateName)
	{
		Map<String, String> ret = new HashMap<String, String>();
		try
		{

			List<String[]> list = getTemplates(templateName, "-1");
			String filePath = list.get(0)[0];
			ClientFileUtils cfu = new ClientFileUtils();
			String content = cfu.readTextContents(cfu.getFilePathFromStoreName(filePath), true).toString();
			ret.put("subject", list.get(0)[1]);
			ret.put("content", content);
			return ret;
		}
		catch (Exception e)
		{
			return ret;
		}
	}

	private int getActiveTemplateCategory(String companyId, String companyName)
	{
		int categoryId = getOrCreateCategory(companyId, companyName, EMAIL_TEMPLATES_MODULE);
		return getOrCreateCategory(ACTIVE_CATEGORY_NAME, categoryId, 2, EMAIL_TEMPLATES_MODULE);
	}
}
