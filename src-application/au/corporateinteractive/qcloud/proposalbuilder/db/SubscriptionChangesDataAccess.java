package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import com.stripe.model.Invoice;
import com.stripe.net.APIResource;

import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.model.bill.BookingSummary;
import au.corporateinteractive.qcloud.proposalbuilder.model.bill.TDInvoice;
import au.corporateinteractive.qcloud.proposalbuilder.service.StripeService;
//import au.corporateinteractive.qcloud.subscription.MarketSubscriptionPlan;
import au.corporateinteractive.qcloud.subscription.SubscriptionPlan;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.ModuleHelper;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.dev.annotation.DbColumn;
import au.net.webdirector.dev.annotation.Module;
import webdirector.db.client.ManagedDataAccess;

@Module(name = "SUBSCRIPTION_CHANGES")
public class SubscriptionChangesDataAccess extends ManagedDataAccess
{
	private static Logger logger = Logger.getLogger(SubscriptionChangesDataAccess.class);
	private static final String MAIN = "main";
	private static final String MARKET = "market";
	/**
	 * <b>The internal module name</b>
	 */
	public static final String MODULE = "SUBSCRIPTION_CHANGES";

	/**
	 * <b>ATTRDATE_LastInvoiceDate</b><br/>
	 * Display: Last Invoice Date
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_LAST_INVOICE_DATE = "ATTRDATE_LastInvoiceDate";


	/**
	 * <b>ATTRDROP_CompanyID</b><br/>
	 * Display: Company Id
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_COMPANY_ID = "ATTRDROP_CompanyID";

	/**
	 * <b>ATTR_categoryName</b><br/>
	 * Display: Folder Name
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_CATEGORY_NAME = "ATTR_categoryName";

	/**
	 * <b>ATTRDROP_DiscountType</b><br/>
	 * Display: Discount Type
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_DISCOUNT_TYPE = "ATTRDROP_DiscountType";

	/**
	 * <b>ATTRFLOAT_DiscountAmount</b><br/>
	 * Display: Discount Amount
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_DISCOUNT_AMOUNT = "ATTRFLOAT_DiscountAmount";

	/**
	 * <b>ATTR_DiscountDesc</b><br/>
	 * Display: Discount Desc
	 */
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_DISCOUNT_DESC = "ATTR_DiscountDesc";


	/**
	 * <b>ATTRCHECK_AdditionalPlan</b><br/>
	 * Display: Additional Plan
	 */
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_ADDITIONAL_PLAN = "ATTRCHECK_AdditionalPlan";


	/**
	 * <b>ATTRDATE_BilledDate</b><br/>
	 * Display: Billed Date
	 */
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_BILLED_DATE = "ATTRDATE_BilledDate";

	/**
	 * <b>ATTRDATE_CancelledDate</b><br/>
	 * Display: Cancelled Date
	 */
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_CANCELLED_DATE = "ATTRDATE_CancelledDate";

	/**
	 * <b>ATTRDATE_EffectiveDate</b><br/>
	 * Display: Effective Date
	 */
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_EFFECTIVE_DATE = "ATTRDATE_EffectiveDate";

	/**
	 * <b>ATTRDROP_PlanId</b><br/>
	 * Display: Plan Id
	 */
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_PLAN_ID = "ATTRDROP_PlanId";

	/**
	 * <b>ATTRDROP_PlanType</b><br/>
	 * Display: Plan Type
	 */
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_PLAN_TYPE = "ATTRDROP_PlanType";

	/**
	 * <b>ATTRFLOAT_Price</b><br/>
	 * Display: Price
	 */
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_PRICE = "ATTRFLOAT_Price";

	/**
	 * <b>ATTRINTEGER_QTY</b><br/>
	 * Display: Qty
	 */
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_QTY = "ATTRINTEGER_QTY";

	/**
	 * <b>ATTRINTEGER_TaxPercentage</b><br/>
	 * Display: Tax Percentage
	 */
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_TAX_PERCENTAGE = "ATTRINTEGER_TaxPercentage";

	/**
	 * <b>ATTR_Currency</b><br/>
	 * Display: Currency
	 */
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_CURRENCY = "ATTR_Currency";

	/**
	 * <b>ATTR_Description</b><br/>
	 * Display: Description
	 */
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_DESCRIPTION = "ATTR_Description";

	/**
	 * <b>ATTR_Headline</b><br/>
	 * Display: Asset Name
	 */
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_HEADLINE = "ATTR_Headline";

	private List<ElementData> getActiveSubscriptions(String companyId) throws SQLException
	{
		QueryBuffer sql = new QueryBuffer();
		sql.append("select e.* from elements_subscription_changes e, categories_subscription_changes c where e.category_id = c.category_id and c.attrdrop_companyid = ? and e.ATTRDROP_PlanType = ? and e.ATTRDATE_CancelledDate is null", companyId, MAIN);
		return ModuleAccess.getInstance().getElements(sql.getSQL(), sql.getParams());
	}

	private ElementData getActiveMarketSubscription(String companyId, String planId) throws SQLException
	{
		return getMarketSubscription(companyId, planId, true);
	}

	public ElementData getMarketSubscription(String companyId, String planId, boolean activeOnly) throws SQLException
	{
		QueryBuffer sql = new QueryBuffer();
		sql.append("select e.* from elements_subscription_changes e, categories_subscription_changes c where e.category_id = c.category_id and c.attrdrop_companyid = ? and e.ATTRDROP_PlanType = ? and e.ATTRDROP_PlanId = ?", companyId, MARKET, planId);
		if (activeOnly)
		{
			sql.append(" and e.ATTRDATE_CancelledDate is null");
		}
		sql.append(" order by e.element_id desc");
		List<ElementData> list = ModuleAccess.getInstance().getElements(sql.getSQL(), sql.getParams());
		if (list.size() == 0)
			return null;
		return list.get(0);
	}

	private SubscriptionPlan findSubscriptionPlan(List<SubscriptionPlan> plans, ElementData elementData)
	{
		for (SubscriptionPlan plan : plans)
		{
			if (match(plan, elementData))
			{
				return plan;
			}
		}
		return null;
	}

	private ElementData findElementData(List<ElementData> elements, SubscriptionPlan plan)
	{
		for (ElementData element : elements)
		{
			if (match(plan, element))
			{
				return element;
			}
		}
		return null;
	}

	private boolean match(SubscriptionPlan plan, ElementData elementData)
	{
		//TODO: check plan type for market subscription plan later.
		return StringUtils.equals(plan.getId(), elementData.getString(E_PLAN_ID));
	}

	private void cancel(ElementData active, TransactionModuleAccess ma) throws SQLException, IOException
	{
		if (active.getDate(E_CANCELLED_DATE) != null)
		{
			return;
		}
		Date today = DateUtils.truncate(new Date(), Calendar.DATE);
		active.put(E_CANCELLED_DATE, today);
		if (active.getDate(E_EFFECTIVE_DATE).after(today))
		{
			active.put(E_EFFECTIVE_DATE, today);
		}
		ma.updateElement(active, MODULE);
	}

	private void create(CategoryData company, SubscriptionPlan plan, ElementData active, TransactionModuleAccess ma) throws SQLException, IOException
	{

		Date today = DateUtils.truncate(new Date(), Calendar.DATE);
		Date oneMonthAgo = DateUtils.addMonths(today, -1);

		int categoryId = getOrCreateCategory(company.getId(), company.getName(), MODULE);

		if (active == null)
		{
			active = new ElementData();
			ModuleHelper.putSystemColumn(active);
			active.setLive(true);
			active.setParentId(String.valueOf(categoryId));
			active.put("create_date", oneMonthAgo);
			active.put(E_EFFECTIVE_DATE, oneMonthAgo);
			active.put(E_PLAN_ID, plan.getId());
			active.put(E_PLAN_TYPE, MAIN);
			active.put(E_PRICE, plan.getPrice());
			active.put(E_QTY, plan.getQuantity());

			active.put(E_TAX_PERCENTAGE, StripeService.calcGstPercent(company));
			active.put(E_CURRENCY, plan.getCurrencyCode());
			active.put(E_DESCRIPTION, plan.getLineItemDescription());
			active.put(E_HEADLINE, plan.getDescription());
			active.put(E_ADDITIONAL_PLAN, "0");
		}
		else
		{
			active.put("create_date", today);
			active.put(E_EFFECTIVE_DATE, today);
			active.put(E_QTY, plan.getQuantity());
			active.put(E_BILLED_DATE, null);
			active.put(E_CANCELLED_DATE, null);
		}
		ma.insertElement(active, MODULE);
	}

	private void update(ElementData active, SubscriptionPlan appPlan, TransactionModuleAccess ma) throws SQLException, IOException
	{
		active.put(E_CANCELLED_DATE, null);
		active.put(E_QTY, appPlan.getQuantity());
		ma.updateElement(active, MODULE);
	}

	public boolean upgradePlan(CategoryData company, SubscriptionPlan planA, TransactionManager transactionManager, HttpServletRequest request, SubscriptionPlan... planB)
	{
		try
		{
			if (SubscriptionPlan.isTrialPlan(planA.getName()))
			{
				return true;
			}
			boolean subscriptionChanged = false;
			TransactionModuleAccess ma = TransactionModuleAccess.getInstance(transactionManager);
			List<SubscriptionPlan> plans = new ArrayList<>(Arrays.asList(planB));
			plans.add(0, planA);
			int addUserPlanQty = 0;
			int addDataPlanQty = 0;

			for (int i = 0; i < plans.size(); i++)
			{
				if (plans.get(i).getName().equals(SubscriptionPlan.PLAN_PER_USER))
				{
					addUserPlanQty = plans.get(i).getQuantity();
				}
				if (plans.get(i).getName().equals(SubscriptionPlan.PLAN_PER_DATA_USAGE))
				{
					addDataPlanQty = plans.get(i).getQuantity();
				}
			}

			List<ElementData> activeSubscriptions = getActiveSubscriptions(company.getId());
			for (ElementData active : activeSubscriptions)
			{

				SubscriptionPlan plan = findSubscriptionPlan(plans, active);
				if (plan == null || plan.getQuantity() == 0)
				{
					// to cancel
					cancel(active, ma);
					subscriptionChanged = true;
				}
				else if (plan.getQuantity() != active.getInt(E_QTY) && isEffectiveInThePast(active))
				{
					// to cancel then add new one
					cancel(active, ma);
					create(company, plan, active, ma);
					subscriptionChanged = true;
				}

				else if (plan.getQuantity() != active.getInt(E_QTY))
				{
					// to cancel then add new one
					update(active, plan, ma);
					subscriptionChanged = true;
				}
			}
			for (SubscriptionPlan plan : plans)
			{
				if (plan.getQuantity() == 0)
				{
					continue;
				}
				ElementData active = findElementData(activeSubscriptions, plan);
				if (active != null)
				{
					continue;
				}
				// to add
				create(company, plan, null, ma);
				subscriptionChanged = true;
			}
			if (subscriptionChanged)
			{
				transactionManager.getDataAccess().update("update categories_useraccounts set attrdrop_pricingplan = ?, ATTRINTEGER_AddUserPlanQty = ?, ATTRINTEGER_AddDataPlanQty = ?, ATTRINTEGER_AlertedDataPlanQty = ?, ATTRDATE_LastBillingDate = ? where category_id = ?", planA.getName(), addUserPlanQty, addDataPlanQty, addDataPlanQty, new Date(), company.getId());
				StripeService.sendChangePlanSuccessEmail(company, planA, request, planB);
			}
			return true;
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			StripeService.sendChangePlanFailureEmail(company, planA, request, planB);
			return false;
		}
	}

	private boolean isEffectiveInThePast(ElementData active)
	{
		return active.getDate(E_EFFECTIVE_DATE).before(DateUtils.truncate(new Date(), Calendar.DATE));
	}

//	public static boolean isCancelled(ElementData appSubscription)
//	{
//		if (appSubscription == null)
//		{
//			return false;
//		}
//		return StringUtils.equals("CANCELLED", appSubscription.getString(MarketSubscriberDataAccess.E_STATUS))
//				|| StringUtils.equals("UNSUBSCRIBED", appSubscription.getString(MarketSubscriberDataAccess.E_STATUS));
//	}

	public CategoryData getCategory(String companyId) throws SQLException
	{
		return ModuleAccess.getInstance().getCategoryByField(MODULE, C_COMPANY_ID, companyId);
	}

	public List<ElementData> getUnbilledSubscriptions(String companyId, Date lastInvoiceDate) throws SQLException
	{
		QueryBuffer sql = new QueryBuffer();
		sql.append("select e.* from elements_subscription_changes e, categories_subscription_changes c ");
		sql.append(" where e.category_id = c.category_id and c.attrdrop_companyid = ? ", companyId);
		sql.append(" and (e.ATTRDATE_BilledDate is null or e.ATTRDATE_BilledDate <= ?) ", new SimpleDateFormat("yyyy-MM-dd").format(lastInvoiceDate));
		sql.append(" and (e.ATTRDATE_CancelledDate is null or e.ATTRDATE_CancelledDate > ?) ", new SimpleDateFormat("yyyy-MM-dd").format(lastInvoiceDate));
		sql.append(" order by e.ATTRDROP_PlanType, e.attrcheck_additionalplan, e.attrdrop_planid, e.element_id");
		return ModuleAccess.getInstance().getElements(sql.getSQL(), sql.getParams());
	}

	public boolean needBill(CategoryData category, List<ElementData> unbilleds, CategoryData company, Date oneMonthAgo)
	{
		if (unbilleds.size() == 0)
			return false;
		if (company.getString(TUserAccountDataAccess.C_PRICING_PLAN).equalsIgnoreCase(SubscriptionPlan.TRIAL_PLAN))
			return false;
		ElementData unbilledMain = filterMainSubscription(unbilleds, company);
		if (unbilledMain == null)
			return false;

		Date lastInvoiceDate = category.getDate(C_LAST_INVOICE_DATE);
		if (lastInvoiceDate == null)
			return true;
		if (lastInvoiceDate.after(oneMonthAgo))
			return false;
		return true;
	}

	private ElementData filterMainSubscription(List<ElementData> unbilleds, CategoryData company)
	{
		SubscriptionPlan additionalUserPlan = SubscriptionPlan.getAdditionalUserPlan(company.getString(TUserAccountDataAccess.C_ADDRESS_COUNTRY));
		SubscriptionPlan additionalDataPlan = SubscriptionPlan.getAdditionalDataPlan(company.getString(TUserAccountDataAccess.C_ADDRESS_COUNTRY));
		for (ElementData unbilled : unbilleds)
		{
			if (!unbilled.getString(E_PLAN_TYPE).equals(MAIN))
				continue;
			if (unbilled.getString(E_PLAN_ID).equals(additionalUserPlan.getId()))
				continue;
			if (unbilled.getString(E_PLAN_ID).equals(additionalDataPlan.getId()))
				continue;
			return unbilled;
		}
		return null;
	}

	public void markAsBilled(CategoryData category, List<ElementData> unbilleds, Date invoiceDate, TransactionManager txManager) throws SQLException, IOException
	{
		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
		category.put(C_LAST_INVOICE_DATE, invoiceDate);
		ma.updateCategory(category, MODULE);
		for (ElementData unbilled : unbilleds)
		{
			unbilled.put(E_BILLED_DATE, invoiceDate);
			ma.updateElement(unbilled, MODULE);
		}

	}

	public TDInvoice constructInvoice(CategoryData companySubscription, List<ElementData> unbilleds, List<DataMap> usages, Date invoiceDate, List<BookingSummary> summary) throws SQLException
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		Date oneMonthAgo = DateUtils.addMonths(invoiceDate, -1);
		long thisBillingDays = getDifferenceDays(oneMonthAgo, invoiceDate);

		int totaltaxCents = 0;
		int subtotalCents = 0;
		Map<String, Object> invoice = new HashMap<>();
		invoice.put("id", "TD_" + fakeId());
		invoice.put("object", "invoice");
		invoice.put("date", invoiceDate.getTime() / 1000);
		Map<String, Object> lines = new HashMap<>();
		invoice.put("lines", lines);
		lines.put("total_count", unbilleds.size());
		List<Map<String, Object>> data = new ArrayList<>();
		lines.put("data", data);

		String currency = null;
		int taxPerc = 0;

		for (ElementData unbilled : unbilleds)
		{
			currency = unbilled.getString(E_CURRENCY).toLowerCase();
			taxPerc = unbilled.getInt(E_TAX_PERCENTAGE);
			invoice.put("currency", currency);
			Date start = unbilled.getDate(E_BILLED_DATE) != null ? unbilled.getDate(E_BILLED_DATE) : unbilled.getDate(E_EFFECTIVE_DATE);
			Date end = unbilled.getDate(E_CANCELLED_DATE) != null ? unbilled.getDate(E_CANCELLED_DATE) : invoiceDate;
			long usedDays = getDifferenceDays(start, end);
			double price = unbilled.getDouble(E_PRICE) * usedDays / thisBillingDays;
			int priceCents = getCents(price);
			int qty = unbilled.getInt(E_QTY);
			int amountCents = priceCents * qty;
			subtotalCents += amountCents;
			totaltaxCents += getCents(price * qty * taxPerc / 100.0);
			String description = unbilled.getString(E_DESCRIPTION);
			if (unbilled.getDate(E_CANCELLED_DATE) != null) // cancelled
			{
				description += " (" + sdf.format(start) + " to " + sdf.format(end) + ") ";

			}
			else if (start.after(oneMonthAgo)) // changed in middle
			{
				description += " (from " + sdf.format(start) + ")";
			}
			data.add(createItem(amountCents, qty, currency, description, start, end));

			ElementData discount = findDiscountByPlanId(companySubscription, unbilled.getString(E_PLAN_ID));
			if (discount != null)
			{
				if (StringUtils.equalsIgnoreCase(discount.getString("ATTRDROP_Type"), "amount"))
				{
					amountCents = -1 * getCents(discount.getDouble("ATTRFLOAT_Amount"));
				}
				else
				{
					amountCents = -1 * amountCents * discount.getInt("ATTRFLOAT_Amount") / 100;
				}
				subtotalCents += amountCents;
				totaltaxCents += amountCents * taxPerc / 100;
				description = discount.getString(E_DESCRIPTION);
				if (unbilled.getDate(E_CANCELLED_DATE) != null) // cancelled
				{
					description += " (" + sdf.format(start) + " to " + sdf.format(end) + ") ";

				}
				else if (start.after(oneMonthAgo)) // changed in middle
				{
					description += " (from " + sdf.format(start) + ")";
				}
				data.add(createItem(amountCents, qty, currency, description, start, end));
			}
		}

		for (DataMap usage : usages)
		{
			double price = usage.getDouble("price");
			int priceCents = getCents(price);
			int qty = usage.getInt("qty");
			int amountCents = priceCents * qty;
			subtotalCents += amountCents;
			totaltaxCents += getCents(price * qty * taxPerc / 100.0);
			String description = usage.getString("description");
			data.add(createItem(amountCents, qty, currency, description, oneMonthAgo, invoiceDate));

			ElementData discount = findDiscountByPlanId(companySubscription, usage.getString("planid"));
			if (discount != null)
			{
				if (StringUtils.equalsIgnoreCase(discount.getString("ATTRDROP_Type"), "amount"))
				{
					amountCents = -1 * getCents(discount.getDouble("ATTRFLOAT_Amount"));
				}
				else
				{
					amountCents = -1 * amountCents * discount.getInt("ATTRFLOAT_Amount") / 100;
				}
				subtotalCents += amountCents;
				totaltaxCents += amountCents * taxPerc / 100;
				description = discount.getString(E_DESCRIPTION);
				data.add(createItem(amountCents, qty, currency, description, oneMonthAgo, invoiceDate));
			}

		}

		if (StringUtils.isNotBlank(companySubscription.getString(C_DISCOUNT_TYPE)))
		{
			double price = -1 * (StringUtils.equalsIgnoreCase("Amount", companySubscription.getString(C_DISCOUNT_TYPE))
					? companySubscription.getDouble(C_DISCOUNT_AMOUNT)
					: (subtotalCents / 100.0 * companySubscription.getDouble(C_DISCOUNT_AMOUNT) / 100.0));
			int priceCents = getCents(price);
			int qty = 1;
			int amount = priceCents * qty;
			subtotalCents += amount;
			totaltaxCents += getCents(price * qty * taxPerc / 100.0);
			String description = companySubscription.getString(C_DISCOUNT_DESC);
			data.add(createItem(amount, qty, currency, description, oneMonthAgo, invoiceDate));
		}

		invoice.put("amount_due", totaltaxCents + subtotalCents);
		if (totaltaxCents > 0)
		{
			invoice.put("tax", totaltaxCents);
		}
		invoice.put("subtotal", subtotalCents);
		invoice.put("total", totaltaxCents + subtotalCents);

		Invoice stripeInvoice = APIResource.GSON.fromJson(APIResource.GSON.toJson(invoice), Invoice.class);
		return new TDInvoice(stripeInvoice, oneMonthAgo, invoiceDate, summary);

	}

	private ElementData findDiscountByPlanId(CategoryData companySubscription, String planId) throws SQLException
	{
		QueryBuffer sql = new QueryBuffer();
		sql.append("select e.* from elements_discounts e, categories_discounts c, elements_subscriptions s where c.category_id = e.category_id and s.attr_headline = e.attrdrop_planid ");
		sql.append(" and e.live = 1 and (e.live_date is null or e.live_date < now()) and (e.expire_date is null or e.expire_date > now()) ");
		sql.append(" and c.attrdrop_companyid = ? ", companySubscription.getString(C_COMPANY_ID));
		sql.append(" and s.element_id = ? ", planId);

		List<ElementData> discounts = ModuleAccess.getInstance().getElements(sql.getSQL(), sql.getParams());
		return discounts.isEmpty() ? null : discounts.get(0);
	}

	private Map<String, Object> createItem(int amount, int qty, String currency, String description, Date start, Date end)
	{
		Map<String, Object> item = new HashMap<>();
		item.put("id", fakeId());
		item.put("object", "line_item");
		item.put("amount", amount);
		item.put("currency", currency);
		item.put("quantity", qty);
		item.put("type", "subscription");
		item.put("description", description);
		Map<String, Object> period = new HashMap<>();
		item.put("period", period);
		period.put("start", start.getTime() / 1000);
		period.put("end", end.getTime() / 1000);
		return item;
	}

	private String fakeId()
	{
		return RandomStringUtils.randomAlphanumeric(16);
	}

	private static int getCents(double dollar)
	{
		return new BigDecimal(dollar * 100).setScale(0, BigDecimal.ROUND_HALF_DOWN).intValue();
	}

	private static long getDifferenceDays(Date d1, Date d2)
	{
		long diff = d2.getTime() - d1.getTime();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

	public List<BookingSummary> getBookingSummaries(CategoryData company, Date start, Date end, TransactionManager txManager) throws SQLException
	{
		// read from booking summary table
		// count amount of pnrs group by consultants 
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
		List<DataMap> list = txManager.getDataAccess().selectMaps("select sum(TOTAL_AMOUNT) as amount, GDS_CODE as gds, AGENT_CODE as agent, CONSULTANT_CODE as consultant from td_booking_summary where CUSTOMER_ID = ? and PROC_DATE >= ? and PROC_DATE < ? group by GDS_CODE, AGENT_CODE, CONSULTANT_CODE ", new String[] { company.getId(), df.format(start), df.format(end) });
		List<BookingSummary> summaries = new ArrayList<>();
		for (DataMap d : list)
		{
			BookingSummary s = new BookingSummary(d.getString("gds"), d.getString("agent"), d.getString("consultant"), d.getInt("amount"));
			summaries.add(s);
		}
		return summaries;
	}

	public List<DataMap> getUsages(CategoryData company, List<BookingSummary> summaries) throws SQLException
	{
		List<DataMap> usages = new ArrayList<>();
		if (summaries.size() == 0)
		{
			return usages;
		}
		SubscriptionPlan consultantPlan = SubscriptionPlan.getAdditionalConsultantPlan(company.getString(UserAccountDataAccess.C_ADDRESS_COUNTRY));
		DataMap consultant = new DataMap();
		consultant.put("price", consultantPlan.getPrice());
		consultant.put("qty", summaries.size());
		consultant.put("description", consultantPlan.getDescription());
		consultant.put("planid", consultantPlan.getId());
		usages.add(consultant);

		// must have plans for free 120 pnrs per consultant.		
		List<SubscriptionPlan> bookingPlans = SubscriptionPlan.getBookingUsagePlan(company.getString(UserAccountDataAccess.C_ADDRESS_COUNTRY));
		SubscriptionPlan currentBookingPlan = bookingPlans.get(0);
		int totalToBeChargedBookingAmount = 0;
		int totalChargedBookingAmount = 0;
		for (BookingSummary data : summaries)
		{
			int freeBookingAmount = Math.min(data.getAmount(), (int) currentBookingPlan.getMaxData());
			totalChargedBookingAmount += freeBookingAmount;
			totalToBeChargedBookingAmount += data.getAmount() - freeBookingAmount;
		}
		DataMap usage = new DataMap();
		usage.put("price", currentBookingPlan.getPrice());
		usage.put("qty", totalChargedBookingAmount);
		usage.put("description", currentBookingPlan.getDescription());
		usage.put("planid", currentBookingPlan.getId());
		usages.add(usage);

		for (int i = 1; i < bookingPlans.size() && totalToBeChargedBookingAmount > 0; i++)
		{
			currentBookingPlan = bookingPlans.get(i);
			usage = new DataMap();

			// ignore max data settings of the last plan.
			int toBeChargedBookingAmount = i == bookingPlans.size() - 1 ? totalToBeChargedBookingAmount : Math.min(totalToBeChargedBookingAmount, (int) currentBookingPlan.getMaxData() - totalChargedBookingAmount);
			if (toBeChargedBookingAmount <= 0)
			{
				continue;
			}
			totalChargedBookingAmount += toBeChargedBookingAmount;
			totalToBeChargedBookingAmount -= toBeChargedBookingAmount;
			usage.put("price", currentBookingPlan.getPrice());
			usage.put("qty", toBeChargedBookingAmount);
			usage.put("description", currentBookingPlan.getDescription());
			usage.put("planid", currentBookingPlan.getId());
			usages.add(usage);
		}
		return usages;
	}
}
