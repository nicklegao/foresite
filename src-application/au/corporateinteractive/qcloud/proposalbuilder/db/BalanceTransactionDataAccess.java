///**
// * @author Jaysun Lee
// * @date 22 Jan 2016
// */
//package au.corporateinteractive.qcloud.proposalbuilder.db;
//
//import org.apache.log4j.Logger;
//
//import webdirector.db.client.ManagedDataAccess;
//
//public class BalanceTransactionDataAccess extends ManagedDataAccess
//{
//	private static Logger logger = Logger.getLogger(BalanceTransactionDataAccess.class);
//
//	/**
//	 * <b>The internal module name</b>
//	 */
//	public static final String MODULE = "TRANSACTIONS";
//
//
//	/**
//	 * <b>ATTR_categoryName</b><br/>
//	 * Display: Folder Name
//	 */
//	public static final String C_CATEGORY_NAME = "ATTR_categoryName";
//
//
//	/**
//	 * <b>ATTRCURRENCY_Amount</b><br/>
//	 * Display: Amount
//	 */
//	public static final String E_AMOUNT = "ATTRCURRENCY_Amount";
//
//	/**
//	 * <b>ATTRCURRENCY_CustomerAmount</b><br/>
//	 * Display: Customer Amount
//	 */
//	public static final String E_CUSTOMER_AMOUNT = "ATTRCURRENCY_CustomerAmount";
//
//	/**
//	 * <b>ATTRCURRENCY_Fee</b><br/>
//	 * Display: Fee
//	 */
//	public static final String E_FEE = "ATTRCURRENCY_Fee";
//
//	/**
//	 * <b>ATTRCURRENCY_Gst</b><br/>
//	 * Display: Gst
//	 */
//	public static final String E_GST = "ATTRCURRENCY_Gst";
//
//	/**
//	 * <b>ATTRCURRENCY_Net</b><br/>
//	 * Display: Net
//	 */
//	public static final String E_NET = "ATTRCURRENCY_Net";
//
//	/**
//	 * <b>ATTRDATE_AvailableOn</b><br/>
//	 * Display: Available On
//	 */
//	public static final String E_AVAILABLE_ON = "ATTRDATE_AvailableOn";
//
//	/**
//	 * <b>ATTRDATE_Created</b><br/>
//	 * Display: Created
//	 */
//	public static final String E_CREATED = "ATTRDATE_Created";
//
//	/**
//	 * <b>ATTRDATE_ProcessedTime</b><br/>
//	 * Display: Processed Time
//	 */
//	public static final String E_PROCESSED_TIME = "ATTRDATE_ProcessedTime";
//
//	/**
//	 * <b>ATTR_CompanyID</b><br/>
//	 * Display: Company Id
//	 */
//	public static final String E_COMPANY_ID = "ATTR_CompanyID";
//
//	/**
//	 * <b>ATTR_CompnayName</b><br/>
//	 * Display: Compnay Name
//	 */
//	public static final String E_COMPANY_NAME = "ATTR_CompanyName";
//
//	/**
//	 * <b>ATTR_Currency</b><br/>
//	 * Display: Currency
//	 */
//	public static final String E_CURRENCY = "ATTR_Currency";
//
//	/**
//	 * <b>ATTR_CustomerCurrency</b><br/>
//	 * Display: Customer Currency
//	 */
//	public static final String E_CUSTOMER_CURRENCY = "ATTR_CustomerCurrency";
//
//	/**
//	 * <b>ATTR_Headline</b><br/>
//	 * Display: Asset Name
//	 */
//	public static final String E_HEADLINE = "ATTR_Headline";
//
//	/**
//	 * <b>ATTR_Source</b><br/>
//	 * Display: Source
//	 */
//	public static final String E_SOURCE = "ATTR_Source";
//}
