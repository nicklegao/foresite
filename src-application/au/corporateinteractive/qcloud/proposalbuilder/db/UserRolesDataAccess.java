package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.sql.SQLException;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.mapper.ElementDataMapper;
import au.net.webdirector.common.datalayer.client.ElementData;
import webdirector.db.client.ManagedDataAccess;

public class UserRolesDataAccess extends ManagedDataAccess {

	Logger logger = Logger.getLogger(TUserRolesDataAccess.class);

	public static final String MODULE = "ROLES";

	public static final String C_COMPANY_ID = "ATTRDROP_CompanyID";

	public static final String MANAGE_TEMPLATES = "ATTRCHECK_template";
	public static final String MANAGE_USERS = "ATTRCHECK_users";
	public static final String MANAGE_SETTINGS = "ATTRCHECK_companySettings";

	public static final String[] ALL_FIELDS = {
			MANAGE_TEMPLATES,
			MANAGE_USERS,
			MANAGE_SETTINGS,
	};
	public UserPermissions getPermissions(String userId)
	{
		Hashtable<String, Boolean> ht = getDefaultPermissions();


		DataAccess da = DataAccess.getInstance();

		String permissions = "role." + StringUtils.join(ALL_FIELDS, ", role.");

		QueryBuffer qb = new QueryBuffer();

		qb.append("select " + permissions + " from elements_USERACCOUNTS u "
				+ "join drop_down_multi_selections ddms on ddms.element_id = u.element_id "
				+ "join elements_roles role on ddms.selected_value = role.element_id "
				+ "where u.element_id = ? "
				+ "and ddms.ATTRMULTI_name = 'ATTRMULTI_roles' "
				+ "and ddms.module_name = 'USERACCOUNTS'", userId);

		List<ElementData> roles = da.select(qb.getSQL(), qb.getParams(), new ElementDataMapper());

		for (ElementData role : roles)
		{
			for (String str : ALL_FIELDS)
			{
				if (role.getBoolean(str))
				{
					ht.put(str, true);
				}
			}
		}

		return new UserPermissions(userId, ht);
	}
	
	private Hashtable<String, Boolean> getDefaultPermissions()
	{
		Hashtable<String, Boolean> ht = new Hashtable<String, Boolean>();

		for (String str : ALL_FIELDS)
		{
			ht.put(str, false);
		}

		return ht;
	}
}
