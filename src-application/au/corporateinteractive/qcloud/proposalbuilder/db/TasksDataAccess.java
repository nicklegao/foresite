package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.corporateinteractive.qcloud.proposalbuilder.db.util.TasksStatus;
import au.corporateinteractive.qcloud.proposalbuilder.service.SecurityKeyService;
import au.corporateinteractive.qcloud.proposalbuilder.utils.DateFormatUtils;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.dev.annotation.DbColumn;
import au.net.webdirector.dev.annotation.Module;
import webdirector.db.client.ManagedDataAccess;

@Module(name = "TASKS")
public class TasksDataAccess extends ManagedDataAccess
{

	public static final Logger logger = Logger.getLogger(TasksDataAccess.class);

	public static final String MODULE = "TASKS";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_COMPANY_ID = "ATTRDROP_companyID";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_END_DATE = "ATTRDATE_EndDate";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_START_DATE = "ATTRDATE_StartDate";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_EXTERNAL_ID = "ATTR_ExternalID";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_DURATION = "ATTR_Duration";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_RESOURCE_NAMES = "ATTR_ResourceNames";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_PREDECESSORS = "ATTR_Predecessors";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_PROJECT_ID = "ATTR_ProjectID";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_KEY = "ATTR_Key";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_STATUS = "ATTR_Status";

	public DataMap injectGeneratedFields(DataMap task, CategoryData company)
	{
		SecurityKeyService sks = new SecurityKeyService();

		addFormattedDates(task, company);

		//Only inject fields if you have access to this user.
		int proposalId = Integer.parseInt(task.getString(E_ID));
		TasksStatus tasksStatus = TasksStatus.withValue(task.getString(E_STATUS));

		if (tasksStatus.isEditable())
		{
			//can edit
			String eKey = sks.getEditKey(Integer.parseInt(company.getId()),
					proposalId, task.getString(E_KEY));
			String eParams = sks.getEditParameters(Integer.parseInt(company.getId()),
					proposalId, task.getString(E_KEY));
			task.put("eKey", eKey);
			task.put("eParams", eParams);//DELETE?
		}
		else
		{
			String vKey = sks.getViewKey(company.getId(), proposalId, task.getString(E_KEY));
			String vParams = sks.getViewParameters(company.getId(), proposalId,
					task.getString(E_KEY), false);
			task.put("vKey", vKey);
			task.put("vParams", vParams);//DELETE?
		}
		return task;
	}


	private static void addFormattedDates(DataMap ht, CategoryData company)
	{
		Locale locale = Locale.getDefault();
		try
		{
			locale = Locale.forLanguageTag(company.getString(UserAccountDataAccess.C1_LOCALE));
		}
		catch (Exception e)
		{

		}
		TimeZone timeZone = TimeZone.getDefault();
		try
		{
			if (StringUtils.isNotBlank(company.getString(UserAccountDataAccess.C1_TIME_ZONE)))
			{
				// TODO: if id isn't understandable, UTC is returned. But we want it to be sydney, maybe?
				timeZone = TimeZone.getTimeZone(company.getString(UserAccountDataAccess.C1_TIME_ZONE));
			}
		}
		catch (Exception e)
		{

		}
		DateFormatUtils dateFormatUtils = DateFormatUtils.getInstance(company.getString(UserAccountDataAccess.C1_DATE_FORMAT), locale);
		dateFormatUtils.setTimeZone(timeZone);
		ht.put(E_START_DATE, getFormattedDate(ht.getString(E_START_DATE), dateFormatUtils));
		ht.put(E_END_DATE, getFormattedDate(ht.getString(E_END_DATE), dateFormatUtils));
	}

	private static String getFormattedDate(String date, DateFormatUtils dateFormatUtils)
	{
		String modifiedForDisplay = "";
		try
		{
			Date modifiedDate = ManagedDataAccess.dbDateFormat.parse(date);
			modifiedForDisplay = dateFormatUtils.format(modifiedDate);
		}
		catch (ParseException e)
		{
			// Sadness
		}
		return modifiedForDisplay;
	}

	public static String fieldMapping(String field)
	{
		return "t." + field;
	}

}
