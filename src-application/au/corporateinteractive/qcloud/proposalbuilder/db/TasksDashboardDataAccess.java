/**
 *
 */
package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import au.corporateinteractive.qcloud.proposalbuilder.dashboard.datatable.Column;
import au.corporateinteractive.qcloud.proposalbuilder.dashboard.datatable.DatatableProcessingRequest;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
import au.net.webdirector.common.datalayer.lucene.LuceneHelper;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import webdirector.db.DatabaseTableMetaDataService;

/**
 * @author Sushant Verma
 *
 */
public class TasksDashboardDataAccess extends TasksDataAccess
{

	public String[] getDashboardFilterColumns()
	{
		String[] colList = null;
		String query = "select Label_InternalName from Labels_" + DatabaseValidation.encodeParam("SABRE_PARSE_INFO") + " order by Label_id";

		// check for a cache hit
		colList = DatabaseTableMetaDataService.labelsTableService(query);
		if (colList != null)
		{
			return colList;
		}

		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query);

		if (v.size() > 0)
		{
			colList = new String[v.size()];

			for (int i = 0; i < v.size(); i++)
			{
				String colName = (String) v.elementAt(i);
				colList[i] = colName;
			}
		}

		// add it into the cache
		DatabaseTableMetaDataService.labelsTableAddToCache(query, colList);

		return colList;
	}

	public List<DataMap> loadProposalsFiltered(Date date, DatatableProcessingRequest data, String project, String companyID, String userId) throws SQLException
	{
		/////////////// ORDER BY COLUMNS
		String orderByColumns = "";
		{
			if (data.order.length > 0)
			{
				orderByColumns = " order by ";
				List<String> orderbys = new ArrayList<>();
				for (int i = 0; i < data.order.length; i++)
				{
					String orderByColumn = data.columns[data.order[i].column].data;
					String dir = data.order[i].dir;
					String mappedOrderbyColumnField =  fieldMapping(orderByColumn);
					mappedOrderbyColumnField = StringUtils.equalsIgnoreCase(mappedOrderbyColumnField, "t.attr_externalid") ? "cast(t.attr_externalid as unsigned)" : mappedOrderbyColumnField;
					orderbys.add(mappedOrderbyColumnField + " " + dir);
				}
				orderByColumns += StringUtils.join(orderbys, ",");

			}
			else
			{
				orderByColumns = " order by cast(t.attr_externalid as unsigned) asc ";
			}
		}

		/////////////// LIMIT ROWS
		String limitRows = "";
		if (data.length > 0)
		{
			limitRows = " limit " + data.length + " offset " + data.start;
		}

		/////////////// BUILD QUERY
		QueryBuffer qb = new QueryBuffer<>();
		qb.append(" select t.* ");
		qb.append(searchJoins(date, project, companyID, userId));
		qb.append(searchFilters(data));

		qb.append(orderByColumns);
		qb.append(limitRows);

		/////////////// RUN QUERY
		DataAccess da = DataAccess.getInstance();
		List<DataMap> userProposals = TransactionDataAccess.getInstance().selectMaps(
				qb.getSQL(),
				qb.getParams());
		return userProposals;
	}

	public int countProposalsFiltered(Date date, DatatableProcessingRequest data, String project, String companyID, String userId)
	{
		/////////////// BUILD QUERY
		QueryBuffer qb = new QueryBuffer<>();
		qb.append(" select count(*) ");
		qb.append(searchJoins(date, project, companyID, userId));
		qb.append(searchFilters(data));

		/////////////// RUN QUERY
		DataAccess da = DataAccess.getInstance();
		List<String> userProposals = da.select(
				qb.getSQL(),
				qb.getParams(),
				StringMapper.getInstance());

		return Integer.parseInt(userProposals.get(0));
	}

	public int countProposalsTotal(Date date, String project, String companyID, String userId)
	{
		/////////////// BUILD QUERY
		QueryBuffer qb = new QueryBuffer<>();
		qb.append(" select count(*) ");
		qb.append(searchJoins( date, project,companyID, userId));

		/////////////// RUN QUERY
		DataAccess da = DataAccess.getInstance();
		List<String> userProposals = da.select(
				qb.getSQL(),
				qb.getParams(),
				StringMapper.getInstance());

		return Integer.parseInt(userProposals.get(0));
	}

	private QueryBuffer searchJoins(Date date, String project, String companyID, String userId)
	{
		QueryBuffer qb = new QueryBuffer<>();
		qb.append(" from elements_tasks t, categories_tasks c1, categories_tasks c2 ");
		qb.append(" where " + standardLiveClauseForCustomQuery("t") + " ");
		qb.append(" and t.ATTR_ProjectID = ? ", project);
		qb.append(" and t.Category_id = c2.Category_id ");
		qb.append(" and c1.category_id = c2.category_parentId");
		qb.append(" and c1.ATTRDROP_CompanyID = ?", companyID);
		qb.append(" and (");
		qb.append(" (t.attrdate_startdate <= ? and t.attrdate_enddate >= ?) ", date, date);
		qb.append(" or (t.attrdate_startdate <= ? and t.attr_status != 'complete') ", date);
		qb.append(" )");
		qb.append(" and t.ATTR_ResourceNames in (select r.ATTR_Headline from elements_roles r, drop_down_multi_selections m where r.element_id = m.SELECTED_VALUE and m.ELEMENT_ID = ? and m.MODULE_NAME = 'useraccounts' and m.ATTRMULTI_NAME = 'attrmulti_roles' and m.module_type = 'element') ", userId);
		return qb;
	}

	private QueryBuffer searchFilters(DatatableProcessingRequest data)
	{
		/////////////// LUCENE SEARCH BOX
		QueryBuffer andSearchFragment = new QueryBuffer<>();
		if (StringUtils.isNotBlank(data.search.value))
		{
			List<String> taskLuceneSearchResult = getLuceneSearch(data.search.value, MODULE);

			andSearchFragment.append(" and (");
			andSearchFragment.appendIn(" t.Element_id in ", taskLuceneSearchResult);
			andSearchFragment.append(" ) ");
		}


		/////////////// COLUMN FILTERING
		QueryBuffer andColumnFilters = new QueryBuffer<>();
		{
			for (Column col : data.columns)
			{
				if (StringUtils.isNotBlank(col.search.value))
				{
					QueryBuffer columnFilter = new QueryBuffer<>();
					String dbColName = fieldMapping(col.data);

					if (useLikeStatement(dbColName))
					{
						boolean isFirst = true;
						for (String word : col.search.value.split(" +"))
						{
							if (!isFirst)
							{
								columnFilter.append(" or ");
							}
							columnFilter.appendLike(dbColName + " like ? ", word);
							isFirst = false;
						}
					}
					else
					{
						columnFilter.append(dbColName + " = ? ", col.search.value);
					}

					andColumnFilters.append(" and ( ");
					andColumnFilters.append(columnFilter);
					andColumnFilters.append(" ) ");
				}
			}
		}

		/////////////// DATE FILTERS
		QueryBuffer andDateFilter = new QueryBuffer<>();
		if (StringUtils.isNotBlank(data.startDate)
				&& StringUtils.isNotBlank(data.endDate))
		{
			SimpleDateFormat tzDate = new SimpleDateFormat("yyyy-MM-dd Z");
			try
			{
				Date startDate = tzDate.parse(data.startDate);
				Calendar cal = GregorianCalendar.getInstance();
				cal.setTime(tzDate.parse(data.endDate));
				cal.add(Calendar.DAY_OF_MONTH, 1);
				Date endDate = cal.getTime();

				andDateFilter.append(" and (t.ATTRDATE_StartDate between ? and ? ", startDate, endDate);
				andDateFilter.append(" or t.ATTRDATE_EndDate between ? and ? ) ", startDate, endDate);
			}
			catch (ParseException e)
			{
				logger.fatal("Error:", e);
			}
		}

		/////////////// BUILD SUBQUERY
		QueryBuffer qb = new QueryBuffer<>();

		qb.append(andSearchFragment);
		qb.append(andColumnFilters);
//		qb.append(andDateFilter);

		return qb;
	}

	private boolean useLikeStatement(String dbColName) {
		// TODO Auto-generated method stub
		return true;
	}

	private List<String> getLuceneSearch(String search, String module)
	{
		LuceneHelper lucenecSearch = new LuceneHelper(module);
		List<String> luceneSearchResult = new ArrayList<String>();
		if (!"".equals(search))
		{
			logger.info("Searching " + module + " with \"" + search + "\"");
			luceneSearchResult = lucenecSearch.searchElements(search);
		}
		return luceneSearchResult;
	}

}
