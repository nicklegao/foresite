/**
 * @author Jaysun Lee
 * @date 7 Oct 2015
 */
package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService;
import au.corporateinteractive.qcloud.proposalbuilder.service.ProductLibraryService;
import au.corporateinteractive.qcloud.proposalbuilder.service.TextLibraryService;
import au.corporateinteractive.qcloud.proposalbuilder.service.VideoLibraryService;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.common.datatypes.domain.DataType;
import au.net.webdirector.common.datatypes.service.DataTypesService;
import au.net.webdirector.common.utils.module.AssetService;
import au.net.webdirector.common.utils.module.CategoryService;
import au.net.webdirector.common.utils.password.PasswordUtils;
import webdirector.db.UserUtils;
import webdirector.db.client.ManagedDataAccess;

public class WebdirectorDataAccess extends ManagedDataAccess
{
	private static Logger logger = Logger.getLogger(WebdirectorDataAccess.class);

	// Set the initial password to something arbitrary, won't give false positive as it won't match a sha1 md5 hash
	public static final String FIRST_LOGIN_PASSWORD_PARAMETER_CHECK = "qcloudreset";
	public static final String EMAIL_TEMPLATES_MODULE = "EMAIL_TEMPLATES";
//	public static final String STATIC_TEXT_MODULE = "STATIC_TEXT";

	private static final String[] MODULES_TO_MODIFY = {
//			ProductLibraryService.PRODUCTS_MODULE,
			ImageLibraryService.IMAGES_MODULE,
			TextLibraryService.TEXT_MODULE,
			VideoLibraryService.VIDEOS_MODULE,
//			PlanDataAccess.PLANS_MODULE,
			UserAccountDataAccess.USER_MODULE,
			CoverPageDataAccess.COVER_PAGE_MODULE,
			EMAIL_TEMPLATES_MODULE
//			STATIC_TEXT_MODULE
//			DiskUsageDataAccess.DISK_USAGE_MODULE
	};

	private static final String[] MODULES_WITH_DEFAULT_CONTENT = {
//			ProductLibraryService.PRODUCTS_MODULE,
//			ImageLibraryService.IMAGES_MODULE,
//			TextLibraryService.TEXT_MODULE,
//			VideoLibraryService.VIDEOS_MODULE,
//			CoverPageDataAccess.COVER_PAGE_MODULE,
			EMAIL_TEMPLATES_MODULE,
//			STATIC_TEXT_MODULE
	};

	private static final String[] MODULES_WITH_IMMUTABLE_CONTENT = {
			EMAIL_TEMPLATES_MODULE
//			STATIC_TEXT_MODULE
	};

	public static final String[] MODULES_TO_ASSIGN = {
			ProductLibraryService.PRODUCTS_MODULE,
			ImageLibraryService.IMAGES_MODULE,
			TextLibraryService.TEXT_MODULE,
			VideoLibraryService.VIDEOS_MODULE,
			UserAccountDataAccess.USER_MODULE,
			EMAIL_TEMPLATES_MODULE
//			STATIC_TEXT_MODULE
	};

	public static final String[] ADDITIONAL_MODULES = {
			"roles",
			"proposals",
			"INVOICES",

	};

	public static String automaticLoginWebdirector(String username)
	{
		String sql = "select wd.password from users wd, elements_" + UserAccountDataAccess.USER_MODULE + " qc "
				+ " where wd.user_name = qc." + UserAccountDataAccess.E_HEADLINE
				+ " and (wd.password = qc." + UserAccountDataAccess.E_PASSWORD + " or wd.password = ?) "
				+ " and wd.user_name = ? ";

		List<String> result = DataAccess.getInstance().select(
				sql, new Object[] { FIRST_LOGIN_PASSWORD_PARAMETER_CHECK, username }, new StringMapper());

		if (result.size() > 0)
			return result.get(0);

		return null;
	}

	/**
	 * @param userName
	 * @param newPassword
	 * @return
	 */
	public static boolean resetWebdirectorPasswordIfExistsAndFirstLogin(String userName, String newPassword)
	{
		List<String> l = DataAccess.getInstance().select("select user_id from users where user_name = ? and password = ?",
				new Object[] { userName, FIRST_LOGIN_PASSWORD_PARAMETER_CHECK },
				new StringMapper());

		if (l.size() > 0)
		{
			String wdUserId = l.get(0);
			return 1 == DataAccess.getInstance().updateData("update users set password = ? where user_id = ?",
					new Object[] { PasswordUtils.encrypt(newPassword), wdUserId });
		}

		return false;
	}

	/**
	 * @param userName
	 * @param newPassword
	 * @return
	 */
	public static boolean changeWebdirectorPassword(String userName, String newPassword)
	{
		try
		{
			return 1 == TransactionDataAccess.getInstance().update("update users set password = ? where user_name = ?",
					new Object[] { PasswordUtils.encrypt(newPassword), userName });
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return false;
		}
	}

	public String createWebdirectorUser(Map<String, String> userTable, String companyName, TransactionManager tm) throws SQLException
	{
		UserUtils ut = new UserUtils();
		UserUtils.UserModel user = ut.new UserModel();

		user.setCompany(companyName);

		user.setUserName(userTable.get("ATTR_Headline"));
		user.setEmail(userTable.get("ATTR_Headline"));
		user.setPhone(userTable.get("ATTR_mobileNumber"));
		user.setName(userTable.get("ATTR_name") + " " + userTable.get("ATTR_surname"));

		user.setDefaultLayout("Workbench");
		user.setModuleUser("on");

		// Don't assign Users module to Maestrano users
		ArrayList<String> modulesToAssign = new ArrayList<String>(Arrays.asList(MODULES_TO_ASSIGN));


		user.setModuleSelected(modulesToAssign.toArray(new String[modulesToAssign.size()]));
		user.setPassword(FIRST_LOGIN_PASSWORD_PARAMETER_CHECK);
		user.setCanChangePassword("yes");
		user.setCanEditAccount("yes");

		return Integer.toString(ut.addUser(user, tm));
	}


	public void addCompanyCategoryToModules(String companyCategoryId, String companyName, TransactionModuleAccess ma, TransactionManager tm) throws SQLException, IOException
	{
		// loop over qcloud webdirector modules relevant to proposal creation and administration
		// create a company category for the new registree in each module and handle special cases
		for (String module : MODULES_TO_MODIFY)
		{
			createCompanyCategoryToModule(module, companyCategoryId, companyName, ma, tm);
		}

	}

	private void createCompanyCategoryToModule(String module, String companyCategoryId, String companyName, TransactionModuleAccess ma, TransactionManager tm) throws SQLException, IOException
	{
		// special case for user module, we've already made the categories during registration
		if (module.equalsIgnoreCase(UserAccountDataAccess.USER_MODULE))
		{
			return;
		}

		// Create the Level 1 category with the Company's name
		CategoryData category = new CategoryData();
		category.setFolderLevel(1);
		category.setParentId("0");
		category.setLive(true);
		category.setName(companyName);
		category.put("attrdrop_companyId", companyCategoryId);
		category = ma.insertCategory(category, module);


		if (Arrays.asList(MODULES_WITH_DEFAULT_CONTENT).contains(module))
			copyDefaultModuleAssets(ma, tm, module, category);
	}

	public void changeCompanyCategoryToModules(String companyId, String companyNewName, TransactionModuleAccess ma) throws SQLException, IOException
	{
		// loop over qcloud webdirector modules relevant to proposal creation and administration
		// update company category for the new registree in each module and handle special cases


		for (String module : MODULES_TO_MODIFY)
		{
			updateCompanyCategoryToModule(module, companyId, companyNewName, ma);
		}

		for (String module : ADDITIONAL_MODULES)
		{
			updateCompanyCategoryToModule(module, companyId, companyNewName, ma);
		}

	}

	private void updateCompanyCategoryToModule(String module, String companyId, String companyNewName, TransactionModuleAccess ma) throws SQLException, IOException
	{
		CategoryData cat = null;
		if (module.equalsIgnoreCase(UserAccountDataAccess.USER_MODULE))
		{
			cat = new CategoryData();
			cat.setId(companyId);
		}
		else
		{
			cat = ma.getCategoryByField(module, "ATTRDROP_CompanyID", companyId);
		}
		if (null != cat)
		{
			cat.setName(companyNewName);
			ma.updateCategory(cat, module);
		}
	}

	private void copyDefaultModuleAssets(TransactionModuleAccess ma, TransactionManager tm,
			String module, CategoryData category) throws SQLException, IOException
	{
		CategoryData condition = new CategoryData();
		condition.setName("Default");
		condition.put("ATTRDROP_CompanyID", "-1");
		String publicCategoryId = ma.getCategory(module, condition).getId();

		List<CategoryData> categoriesUnderDefaultCategory = ma.getCategoriesByParentId(module, publicCategoryId);
		for (CategoryData categoryToCopyFrom : categoriesUnderDefaultCategory)
		{
			Integer categoryToCopy = Integer.parseInt(categoryToCopyFrom.getId());
			int freshCategoryID = new CategoryService().copyCategory(categoryToCopy, Integer.parseInt(category.getId()), module, tm, false);
		}

		List<ElementData> elementsUnderDefaultCategory = ma.getElementsByParentId(module, publicCategoryId);
		for (ElementData elementToCopyFrom : elementsUnderDefaultCategory)
		{
			List<DataType> columns = (new DataTypesService()).loadGenericDataTypes(module, false, "");
			new AssetService().copyAsset(elementToCopyFrom, category.getId(), module, ma, columns, false);
		}
	}

	public void postCommitUpdates(String companyId)
	{
		try
		{
			ModuleAccess ma = ModuleAccess.getInstance();
			CategoryData cat = new CategoryData();
			cat.put("attrdrop_companyId", companyId);
			cat = ma.getCategory(CoverPageDataAccess.COVER_PAGE_MODULE, cat);
			List<ElementData> elements = ma.getElementsByParentId(CoverPageDataAccess.COVER_PAGE_MODULE, cat.getId());
			CoverPageDataAccess cpda = new CoverPageDataAccess();
			for (ElementData el : elements)
			{
				StoreFile file = new StoreFile((String) el.get(cpda.E_COVER_PAGE));
				cpda.generateMediumThumb(file);
			}
		}
		catch (Exception e)
		{
			logger.info("Erro generating medium coverpage thumbs");
		}

	}

	public static void deleteCompanyCategoryFromModules(String companyCategoryId, TransactionModuleAccess ma) throws SQLException, IOException
	{
		for (String module : MODULES_TO_MODIFY)
		{
			deleteCompanyCategoryFromModule(module, companyCategoryId, ma);
		}

		for (String module : ADDITIONAL_MODULES)
		{
			deleteCompanyCategoryFromModule(module, companyCategoryId, ma);
		}
	}

	private static void deleteCompanyCategoryFromModule(String module, String companyCategoryId, TransactionModuleAccess ma) throws SQLException, IOException
	{
		CategoryData cat = new CategoryData();
		if (module.equalsIgnoreCase("INVOICES"))
		{
			// do not delete invoices for audit purposes.
			return;
		}
		if (module.equalsIgnoreCase(UserAccountDataAccess.USER_MODULE))
		{
			cat.setId(companyCategoryId);
		}
		else
		{
			cat.put("attrdrop_companyId", companyCategoryId);
		}
		ma.deleteCategories(module, cat);
	}
}
