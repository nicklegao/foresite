/**
 * @author Rafael
 * @date 26Sep.,2017
 */
package au.corporateinteractive.qcloud.proposalbuilder.db.analytics;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

public class AnalyticsByMonth extends FixedPeriod
{

	private static Logger logger = Logger.getLogger(AnalyticsByMonth.class);

	public String getSelect()
	{
		return "(MONTH(" + this.SELECTOR + ") - 1) AS " + getGroupBy() + ", ";
	}

	/* (non-Javadoc)
	 * @see au.corporateinteractive.telstra.proposalbuilder.db.analytics.ProposalAnalytics#getGroupBy()
	 */
	@Override
	public String getGroupBy()
	{
		return "month";
	}

	/* (non-Javadoc)
	 * @see au.corporateinteractive.telstra.proposalbuilder.db.analytics.ProposalAnalytics#getCalendarTerm()
	 */
	@Override
	public int getCalendarCurrentTerm()
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		return cal.get(Calendar.MONTH);
	}

	/* (non-Javadoc)
	 * Return 
	 */
	@Override
	public int getCalendarTerms()
	{
		return 12;
	}

	/* (non-Javadoc)
	 * @see au.corporateinteractive.telstra.proposalbuilder.db.analytics.ProposalAnalytics#getTermToString(int)
	 */
	@Override
	public String getTermToString(int term)
	{
		String month = new DateFormatSymbols().getMonths()[term];
		return month;
	}

	@Override
	public String getDateFilter()
	{
		return "and " + this.SELECTOR + " > last_day(date_sub(now(), Interval 365 day)) ";
	}

}
