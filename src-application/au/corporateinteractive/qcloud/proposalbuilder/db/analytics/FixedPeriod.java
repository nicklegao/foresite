/**
 * @author Rafael
 * @date 17Oct.,2017
 */
package au.corporateinteractive.qcloud.proposalbuilder.db.analytics;

import java.util.Hashtable;

import org.apache.log4j.Logger;

public abstract class FixedPeriod implements PeriodSelector
{
	private static Logger logger = Logger.getLogger(FixedPeriod.class);

	/* (non-Javadoc)
	 * @see au.corporateinteractive.qcloud.proposalbuilder.db.analytics.PeriodSelector#init()
	 */
	@Override
	public Hashtable<String, Hashtable> init(Hashtable template)
	{
		int totalTerms = getCalendarTerms();

		Hashtable<String, Hashtable> terms = new Hashtable<>();

		for (int term = 0; term < totalTerms; term++)
		{
			Hashtable t = new Hashtable<>();
			t.put("label", getTermToString(term));
			t.putAll(template);
			terms.put(Integer.toString(term), t);
		}

		return terms;
	}

	/* (non-Javadoc)
	 * @see au.corporateinteractive.qcloud.proposalbuilder.db.analytics.PeriodSelector#getDateFilter()
	 */
	@Override
	public String getDateFilter()
	{
		return "and p.ATTRDATE_createdDate <= now() and p.ATTRDATE_createdDate >= date_sub(now(), Interval 365 day) ";
	}

}
