/**
 * @author Rafael
 * @date 17Oct.,2017
 */
package au.corporateinteractive.qcloud.proposalbuilder.db.analytics;

import java.util.Hashtable;

public interface PeriodSelector
{
	public static final String SELECTOR = "if(p.attrdrop_status = 'accepted' or p.attrdrop_status = 'paid'  or p.attrdrop_status = 'depositTaken', pr.statusToAccepted,if(p.attrdrop_status = 'lost' or p.attrdrop_status = 'closed', pr.statusToClosed, p.attrdate_createddate))";

	public Hashtable<String, Hashtable> init(Hashtable template);

	public String getSelect();

	public int getCalendarCurrentTerm();

	public int getCalendarTerms();

	public String getGroupBy();

	public String getTermToString(int term);

	public String getDateFilter();
}
