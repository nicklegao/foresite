/**
 * @author Rafael
 * @date 26Sep.,2017
 */
package au.corporateinteractive.qcloud.proposalbuilder.db.analytics;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;

import org.apache.log4j.Logger;

public class AnalyticsByDay implements PeriodSelector
{
	private static Logger logger = Logger.getLogger(AnalyticsByDay.class);

	private int qty;
	private String start;
	private String end;

	public AnalyticsByDay(String start, String end)
	{
		this.start = start;
		this.end = end;
	}

	public Hashtable<String, Hashtable> init(Hashtable template)
	{
		Hashtable<String, Hashtable> terms = new Hashtable<>();

		try
		{
			SimpleDateFormat dbFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date dStart = dbFormat.parse(this.start);
			Calendar cEnd = Calendar.getInstance();
			cEnd.setTime(dbFormat.parse(this.end));
			cEnd.add(Calendar.DATE, 1);
			Calendar it = Calendar.getInstance();
			it.setTime(dStart);

			while (it.before(cEnd))
			{
				Date date = it.getTime();
				String label = dbFormat.format(date);

				Hashtable t = new Hashtable<>();
				t.put("label", label);
				t.putAll(template);

				terms.put(label, t);
				it.add(Calendar.DATE, 1);
			}
		}
		catch (ParseException ex)
		{
			logger.error("Error parsing dates: " + this.start + " to " + this.end, ex);
		}

		qty = terms.size();

		return terms;
	}

	/* (non-Javadoc)
	 * @see au.corporateinteractive.telstra.proposalbuilder.db.analytics.ProposalAnalytics#getSelect()
	 */
	@Override
	public String getSelect()
	{
		return "date(" + this.SELECTOR + ") AS '" + getGroupBy() + "', ";
	}

	/* (non-Javadoc)
	 * @see au.corporateinteractive.telstra.proposalbuilder.db.analytics.ProposalAnalytics#getGroupBy()
	 */
	@Override
	public String getGroupBy()
	{
		return "date";
	}

	/* (non-Javadoc)
	 * @see au.corporateinteractive.telstra.proposalbuilder.db.analytics.ProposalAnalytics#getCalendarTerm()
	 */
	@Override
	public int getCalendarCurrentTerm()
	{
		return qty - 1;
	}

	/* (non-Javadoc)
	 * @see au.corporateinteractive.telstra.proposalbuilder.db.analytics.ProposalAnalytics#getCalendarTerm()
	 */
	@Override
	public int getCalendarTerms()
	{
		return qty;
	}

	/* (non-Javadoc)
	 * @see au.corporateinteractive.telstra.proposalbuilder.db.analytics.ProposalAnalytics#getTermToString(int)
	 */
	@Override
	public String getTermToString(int term)
	{
		return "";
	}

	@Override
	public String getDateFilter()
	{
		return "and (" + this.SELECTOR + " between '" + this.start + "' AND  DATE_ADD('" + this.end + "',Interval 1 day)) ";
	}

}
