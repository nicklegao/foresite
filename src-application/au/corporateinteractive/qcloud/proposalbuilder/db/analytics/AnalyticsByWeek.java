/**
 * @author Rafael
 * @date 26Sep.,2017
 */
package au.corporateinteractive.qcloud.proposalbuilder.db.analytics;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

public class AnalyticsByWeek extends FixedPeriod
{

	private static Logger logger = Logger.getLogger(AnalyticsByWeek.class);

	/* (non-Javadoc)
	 * @see au.corporateinteractive.telstra.proposalbuilder.db.analytics.ProposalAnalytics#getSelect()
	 */
	@Override
	public String getSelect()
	{
		return "WEEKOFYEAR(" + this.SELECTOR + ") AS " + getGroupBy() + ", ";
	}

	/* (non-Javadoc)
	 * @see au.corporateinteractive.telstra.proposalbuilder.db.analytics.ProposalAnalytics#getGroupBy()
	 */
	@Override
	public String getGroupBy()
	{
		return "week";
	}

	/* (non-Javadoc)
	 * Return 
	 */
	@Override
	public int getCalendarCurrentTerm()
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		return cal.get(Calendar.WEEK_OF_YEAR) - 1;
	}

	/* (non-Javadoc)
	 * Return 
	 */
	@Override
	public int getCalendarTerms()
	{
		return 52;
	}

	/* (non-Javadoc)
	 * @see au.corporateinteractive.telstra.proposalbuilder.db.analytics.ProposalAnalytics#getTermToString(int)
	 */
	@Override
	public String getTermToString(int term)
	{
		return "Week " + (term + 1);
	}

	@Override
	public String getDateFilter()
	{
		return "and " + this.SELECTOR + " > DATE(date_sub(NOW(), Interval 365 day) + INTERVAL (6 - WEEKDAY(date_sub(NOW(), Interval 365 day))) DAY) ";
	}


}
