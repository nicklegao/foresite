/**
 * @author Rafael
 * @date 26Sep.,2017
 */
package au.corporateinteractive.qcloud.proposalbuilder.db.analytics;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

public class AnalyticsByQuarter extends FixedPeriod
{

	private static Logger logger = Logger.getLogger(AnalyticsByQuarter.class);

	/* (non-Javadoc)
	 * @see au.corporateinteractive.telstra.proposalbuilder.db.analytics.ProposalAnalytics#getSelect()
	 */
	@Override
	public String getSelect()
	{
		return "quarter(" + this.SELECTOR + ") - 1 AS " + getGroupBy() + ", ";
	}

	/* (non-Javadoc)
	 * @see au.corporateinteractive.telstra.proposalbuilder.db.analytics.ProposalAnalytics#getGroupBy()
	 */
	@Override
	public String getGroupBy()
	{
		return "quarter";
	}

	/* (non-Javadoc)
	 * @see au.corporateinteractive.telstra.proposalbuilder.db.analytics.ProposalAnalytics#getCalendarTerm()
	 */
	@Override
	public int getCalendarCurrentTerm()
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		switch (cal.get(Calendar.MONTH))
		{
			case 0:
			case 1:
			case 2:
				return 0;
			case 3:
			case 4:
			case 5:
				return 1;
			case 6:
			case 7:
			case 8:
				return 2;
			case 9:
			case 10:
			case 11:
				return 3;
			default:
				return 0;
		}
	}

	/* (non-Javadoc)
	 * Return 
	 */
	@Override
	public int getCalendarTerms()
	{
		return 4;
	}

	/* (non-Javadoc)
	 * @see au.corporateinteractive.telstra.proposalbuilder.db.analytics.ProposalAnalytics#getTermToString(int)
	 */
	@Override
	public String getTermToString(int term)
	{
		switch (term)
		{
			case 0:
				return "First";
			case 1:
				return "Second";
			case 2:
				return "Third";
			case 3:
				return "Fourth";
			default:
				return "";
		}
	}

	@Override
	public String getDateFilter()
	{
		return "and " + this.SELECTOR + " > MAKEDATE(YEAR(date_sub(NOW(), Interval 365 day)), 1) + INTERVAL QUARTER(date_sub(NOW(), Interval 365 day)) QUARTER - INTERVAL 1 DAY ";
	}


}
