/**
 * @author Rafael
 * @date 28Sep.,2017
 */
package au.corporateinteractive.qcloud.proposalbuilder.db.analytics;

import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;

public class ChannelAnalytics
{
	private static Logger logger = Logger.getLogger(ChannelAnalytics.class);

	public List<Hashtable> getProficiency()
	{
		String sql = "select c1.attr_categoryname as channel, count(pr.element_id) as proposalcount "
				+ "from elements_useraccounts us "
				+ "left join elements_proposals pr on pr.attrdrop_ownerid = us.element_id "
				+ "left join categories_users c2 on c2.Category_id = us.Category_id "
				+ "left join categories_users c1 on c1.Category_id = c2.Category_ParentID "
				+ "where us.live != 0 "
				+ "and pr.ATTRCHECK_displayOnDashboard = 1 "
				+ "group by channel "
				+ "order by proposalcount desc";

		return executeQuery(sql);
	}

	public List<Hashtable> getActiveUsers()
	{
		String sql = "select c1.attr_categoryname as channel, count(pr.Element_id) as userscount "
				+ "from elements_proposals pr "
				+ "left join elements_useraccounts us on us.Element_id = pr.ATTRDROP_ownerId "
				+ "left join categories_users c2 on c2.Category_id = us.Category_id "
				+ "left join categories_users c1 on c1.Category_id = c2.Category_ParentID "
				+ "where pr.attrdate_createddate >= DATE_SUB(NOW(), INTERVAL 1 MONTH) "
				+ "group by channel "
				+ "order by userscount desc";

		return executeQuery(sql);
	}

	private List<Hashtable> executeQuery(String sql)
	{
		DataAccess da = DataAccess.getInstance();
		List<Hashtable> items = da.select(sql, new String[] {}, HashtableMapper.getInstance());

		return items;
	}
}
