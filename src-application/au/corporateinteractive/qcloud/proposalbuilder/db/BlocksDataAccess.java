package au.corporateinteractive.qcloud.proposalbuilder.db;

import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.common.utils.module.StoreUtils;
import au.net.webdirector.dev.annotation.DbColumn;
import org.apache.log4j.Logger;
import webdirector.db.client.ManagedDataAccess;

import java.util.List;

public class BlocksDataAccess extends ManagedDataAccess
{
	Logger logger = Logger.getLogger(BlocksDataAccess.class);

	public static final String BLOCK_MODULE = "CONTENTBLOCKS";

	//DB Outlets -- CATEGORIES
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_BLOCK_NAME = "ATTR_categoryName";

	//DB Outlets -- ELEMENTS
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_BLOCK_NAME = "ATTR_Headline";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_BLOCK_HTML = "ATTRTEXT_html";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_BLOCK_CSS = "ATTRTEXT_css";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_BLOCK_JS = "ATTRTEXT_js";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_BLOCK_PREMIUM = "ATTRCHECK_premium";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_BLOCK_RELEASED = "ATTRCHECK_released";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_BLOCK_ICON = "ATTR_icon";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_BLOCK_OPTIONS = "ATTRMULTI_optionSelector";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_BLOCK_ID = "ATTR_blockID";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_BLOCK_MENUADDON = "ATTRTEXT_menuAddon";


	public List<ElementData> getAllValidBlocks(String companyID) {
		try 
		{
			ModuleAccess ma = ModuleAccess.getInstance();
			ElementData conditions = new ElementData();
			conditions.setLive(true);
//			conditions.put(E_BLOCK_RELEASED, "1");
			List<ElementData> blocks = ma.getElements(BLOCK_MODULE, conditions);

			for (ElementData block : blocks) {
				System.out.println(block);
			}
			return blocks;
		}
		catch (Exception e)
		{
			logger.error("There was an issue collection the blocks.", e);
			return null;
		}
	}

	public String getBlockContent(String blockId) {
		try 
		{
			ModuleAccess ma = ModuleAccess.getInstance();

			ElementData blocks = ma.getElementById(BLOCK_MODULE, blockId); 

			StoreUtils storeUtils = new StoreUtils();
			String blockFile = storeUtils.readFieldFromStores(blocks.getString(E_BLOCK_HTML));
			if (blockFile != null && blockFile.length() > 5) {
				return blockFile;
			}
			return "<div></div>";
		}
		catch (Exception e)
		{
			logger.error("There was an issue collection the blocks.", e);
			return null;
		}
	}
}
