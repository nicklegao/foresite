/**
 * @author Nick Yiming Gao
 * @date 2015-8-28
 */
package au.corporateinteractive.qcloud.proposalbuilder.db;

/**
 * 
 */

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
import webdirector.db.client.ClientFileUtils;
import webdirector.db.client.ManagedDataAccess;

public class StaticTextDataAccess extends ManagedDataAccess
{
	private static Logger logger = Logger.getLogger(StaticTextDataAccess.class);

	public static final String STATIC_TEXT_MODULE = "STATIC_TEXT";

	public static final String E_TEXT_BLOCK = "ATTRLONG_textBlock";

	@Deprecated
	public static final String EHEADLINE_SOLUTION_IN_DETAIL = "Solution in Detail";
	public static final String EHEADLINE_PRICING_WANT_MORE_DETAIL = "Pricing: Want more detail?";
	public static final String EHEADLINE_THINGS_YOU_SHOULD_KNOW = "Things You Should Know";
	public static final String EHEADLINE_PRICING = "Pricing";
	public static final String EHEADLINE_SECTION = "Section";

	public String readContentWithUserId(String userID, String name)
	{
		String companyId = new UserAccountDataAccess().getUserCompanyForUserId(userID).getId();

		String query = "select ATTRLONG_textBlock from elements_static_text e, categories_static_text c1, categories_static_text c2 "
				+ " where c2.category_parentId = c1.category_id and c2.category_id = e.category_id and e.ATTR_Headline = ? and c1.attrdrop_companyId = ?";
		List list = DataAccess.getInstance().select(query, new String[] { name, companyId }, new StringMapper());
		String filePath = (String) list.get(0);
		ClientFileUtils cfu = new ClientFileUtils();
		String content = cfu.readTextContents(cfu.getFilePathFromStoreName(filePath), true).toString();
		return content;
	}

	public void setContentWithId(String companyId, String name, String contents)
	{
		try
		{
			TransactionDataAccess tda = TransactionDataAccess.getInstance();
			ClientFileUtils cfu = new ClientFileUtils();

			QueryBuffer qb = new QueryBuffer<>();
			qb.append("select ATTRLONG_textBlock from elements_static_text e, categories_static_text c1, categories_static_text c2 "
					+ " where c2.category_parentId = c1.category_id and c2.category_id = e.category_id ");
			qb.append(" and e.ATTR_Headline = ? ", name);
			qb.append(" and c1.attrdrop_companyId = ?", companyId);
			String storePath = tda.selectString(qb);
			if (StringUtils.isNotEmpty(storePath))
			{
				cfu.overwriteTextContents(cfu.getFilePathFromStoreName(storePath), contents);
			}
			//Dont need to support "else"
//			else
//			{
//				insertContentWithId(companyId, name, contents);
//			}
		}
		catch (Exception e)
		{
			logger.fatal("Oh No!", e);
		}

	}

//	private void insertContentWithId(String companyId, String name, String contents)
//	{
//		String generatedCatId = getOrCreateGeneratedCategory(companyId);
//
//		try (TransactionManager transactionManager = TransactionManager.getInstance())
//		{
//			TransactionDataAccess tda = TransactionDataAccess.getInstance(transactionManager);
//
//			ElementData ed = new ElementData();
//			ed.setParentId(generatedCatId);
//			ed.setName(name);
//			ed.setLive(true);
//
//			TransactionModuleAccess tma = TransactionModuleAccess.getInstance(transactionManager);
//			ed = tma.insertElement(ed, STATIC_TEXT_MODULE);
//
//			TextFile tf = new TextFile(contents, STATIC_TEXT_MODULE, E_TEXT_BLOCK, ed.getId(), false);
//			ed.put(E_TEXT_BLOCK, ed);
//			tma.updateElement(ed, STATIC_TEXT_MODULE);
//
//			transactionManager.commit();
//		}
//		catch (Exception e)
//		{
//			logger.fatal("Oh No!", e);
//		}
//	}
//
//	private String getOrCreateGeneratedCategory(String companyId)
//	{
//		String cat1Id = getOrCreateCompanyCategory(companyId);
//
//		try (TransactionManager transactionManager = TransactionManager.getInstance())
//		{
//			TransactionDataAccess tda = TransactionDataAccess.getInstance(transactionManager);
//			String cat2ID = tda.selectString("select category_id "
//					+ " from categories_static_text c1, categories_static_text c2 "
//					+ " where c2.category_parentId = c1.category_id "
//					+ " and c2.attr_categoryname = ?"
//					+ " and c1.category_id = ? limit 1", "gen", cat1Id);
//
//			if (StringUtils.isEmpty(cat2ID))
//			{
//				CategoryData cd = new CategoryData();
//				cd.setParentId(cat1Id);
//				cd.setName("gen");
//				cd.setLive(true);
//				cd.setFolderLevel(2);
//
//				TransactionModuleAccess tma = TransactionModuleAccess.getInstance(transactionManager);
//				cd = tma.insertCategory(cd, STATIC_TEXT_MODULE);
//
//				cat2ID = cd.getId();
//			}
//
//			transactionManager.commit();
//
//			return cat2ID;
//		}
//		catch (Exception e)
//		{
//			logger.fatal("Oh No!", e);
//		}
//		return null;
//	}
//
//	private String getOrCreateCompanyCategory(String companyId)
//	{
//		try (TransactionManager transactionManager = TransactionManager.getInstance())
//		{
//			TransactionDataAccess tda = TransactionDataAccess.getInstance(transactionManager);
//			String cat1ID = tda.selectString("select category_id "
//					+ " from categories_static_text c1 "
//					+ " where c1.attrdrop_companyId = ? limit 1", companyId);
//
//			if (StringUtils.isEmpty(cat1ID))
//			{
//				CategoryData cd = new CategoryData();
//				cd.setName("Category for companyID " + companyId);
//				cd.put("attrdrop_companyId", companyId);
//				cd.setLive(true);
//				cd.setFolderLevel(1);
//
//				TransactionModuleAccess tma = TransactionModuleAccess.getInstance(transactionManager);
//				cd = tma.insertCategory(cd, STATIC_TEXT_MODULE);
//
//				cat1ID = cd.getId();
//			}
//
//			transactionManager.commit();
//
//			return cat1ID;
//		}
//		catch (Exception e)
//		{
//			logger.fatal("Oh No!", e);
//		}
//		return null;
//	}
}
