package au.corporateinteractive.qcloud.proposalbuilder.db;

import java.io.File;

import org.apache.log4j.Logger;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.common.utils.StreamGobbler;

public class CoverPageDataAccess
{
	private static Logger logger = Logger.getLogger(CoverPageDataAccess.class);

	public static final String COVER_PAGE_MODULE = "GEN_COVERPAGES";

	public static final String E_COVER_PAGE = "ATTRFILE_image";

	public boolean removeCoverPage(String coverpageId)
	{
		TransactionManager tm = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true));
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(tm);
		try
		{
			boolean retult = tma.deleteElementById(COVER_PAGE_MODULE, coverpageId) > 1;
			tm.commit();
			return retult;
		}
		catch (Exception e)
		{
			tm.rollback();
			return false;
		}
	}

	public boolean generateMediumThumb(StoreFile toThumb)
	{
		File originalFile = toThumb.getStoredFile();

		// make thumb and all parent dirs
		File thumbDir = new File(originalFile.getParentFile(), "_medium");
		thumbDir.mkdirs();

		File thumbnailFile = new File(thumbDir, originalFile.getName());

		int exitVal = -1;
		try
		{
			String[] cmd = new String[5];

			logger.info("Operating System: : " + System.getProperty("os.name"));

			cmd[0] = new File(Defaults.getInstance().getImageMagickDirectory(), "convert").getAbsolutePath();
			cmd[1] = originalFile.getAbsolutePath();
			cmd[2] = "-resize";
			cmd[3] = "150x212";
			cmd[4] = thumbnailFile.getAbsolutePath();

			Runtime rt = Runtime.getRuntime();
			for (int i = 0; i < cmd.length; i++)
			{
				logger.info(String.format("EXECUTING (%s/%s): %s", i, cmd.length, cmd[i]));
			}

			Process proc = rt.exec(cmd);
			// any error message?
			StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "ERROR");

			// any output?
			StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "OUTPUT");

			// kick them off
			errorGobbler.start();
			outputGobbler.start();

			// any error???
			exitVal = proc.waitFor();
			logger.info("ExitValue: " + exitVal);
		}
		catch (Throwable t)
		{
			logger.fatal("OH NO!", t);
		}

		if (exitVal == 0)
			return true;
		else
			return false;
	}

	public File cropFile(File toCrop, int offsetX, int offsetY, int width, int height, boolean coverPage)
	{
		String pdfSafeFileName = toCrop.getName().replaceAll("[^A-Za-z0-9\\.]", "");

		File croppedFile = new File(toCrop.getParentFile(), "cropped_" + pdfSafeFileName);

		int exitVal = 0;
		try
		{
			String[] cmd = new String[8];

			cmd[0] = new File(Defaults.getInstance().getImageMagickDirectory(), "convert").getAbsolutePath();
			cmd[1] = toCrop.getAbsolutePath();
			cmd[2] = "-crop";
			cmd[3] = width + "x" + height + "+" + offsetX + "+" + offsetY;
			cmd[4] = "-resize";
			if (coverPage)
			{
				cmd[5] = height > width ? "793x1122" : "1122x793";
			}
			else
			{
				cmd[5] = width + "x" + height;
			}

			cmd[6] = "-auto-orient";
			cmd[7] = croppedFile.getAbsolutePath();

			String a = "";
			for (int i = 0; i < cmd.length; i++)
				a += " " + cmd[i];

			Runtime rt = Runtime.getRuntime();
			Process proc = rt.exec(cmd);
			// any error message?
			StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "ERROR");

			// any output?
			StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "OUTPUT");

			// kick them off
			errorGobbler.start();
			outputGobbler.start();

			// any error???
			exitVal = proc.waitFor();
			logger.info("ExitValue: " + exitVal);
		}
		catch (Throwable t)
		{
			t.printStackTrace();
		}

		return croppedFile;
	}

	public File cropFileForCoverPage(File toCrop, int offsetX, int offsetY, int width, int height)
	{
		return cropFile(toCrop, offsetX, offsetY, width, height, true);
	}
}
