//package au.corporateinteractive.qcloud.proposalbuilder.db;
//
//
//import au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess;
//import au.corporateinteractive.qcloud.proposalbuilder.model.Notification;
//import au.net.webdirector.common.datalayer.admin.db.DBaccess;
//import au.net.webdirector.common.datalayer.admin.db.DataAccess;
//import au.net.webdirector.common.datalayer.base.db.DataMap;
//import au.net.webdirector.common.datalayer.base.db.mapper.ElementDataMapper;
//import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
//import au.net.webdirector.common.datalayer.client.ElementData;
//import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
//import au.net.webdirector.dev.annotation.DbColumn;
//import org.apache.log4j.Logger;
//import webdirector.db.client.ManagedDataAccess;
//
//import java.sql.Timestamp;
//import java.util.*;
//
//public class UserNotificationDataAccess extends ManagedDataAccess
//{
//	private static Logger logger = Logger
//			.getLogger(UserNotificationDataAccess.class);
//
//	public static final String MODULE = "USER_NOTIFICATIONS";
//
//	//DB Outlets -- CATEGORIES
//	@DbColumn(type = DbColumn.TYPE_CATEGORY)
//	public static final String C_NOTIFICATION_CATEGORY = "ATTR_categoryName";
//
//	//DB Outlets -- ELEMENTS
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_NOTIFICATION_TITLE = "ATTR_Headline";
//
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_NOTIFICATION_MESSAGE = "ATTR_TextMessage";
//
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_NOTIFICATION_SEEN = "ATTRCHECK_seen";
//
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_NOTIFICATION_DELIVERED = "ATTRCHECK_delivered";
//
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_NOTIFICATION_JSON_CONTENT = "ATTR_JSONContent";
//
//	@DbColumn(type = DbColumn.TYPE_ELEMENT)
//	public static final String E_NOTIFICATION_TOUSER = "ATTR_toUser";
//
//	protected DBaccess db = new DBaccess();
//
//
//	public int numberOfPendingNotificationByUserId(String userId) {
//
//		Vector v = db.selectQuerySingleCol("SELECT * FROM elements_USER_NOTIFICATIONS WHERE ATTRCHECK_seen = 0 AND ATTR_toUser = " + userId);
//
//		return v.size();
//	}
//
//	public List<ElementData> notificationNotDeliveredToUser(String userId) {
//		List<ElementData> notificationList = DataAccess.getInstance().select(
//				"SELECT * FROM elements_USER_NOTIFICATIONS WHERE ATTRCHECK_delivered = 0 AND ATTR_toUser = ?",
//				new String[] { userId }, new ElementDataMapper());
//
//		for (ElementData notification : notificationList) {
//			notificationDelivered(notification.getId());
//		}
//
//		return notificationList;
//
//	}
//
//	public Boolean notificationDelivered(String notificationID) {
//		try (TransactionManager transactionManager = TransactionManager.getInstance())
//		{
//			TransactionModuleAccess tma = TransactionModuleAccess.getInstance(transactionManager);
//			ElementData notification = tma.getElementById(MODULE, notificationID);
//			notification.put(E_NOTIFICATION_DELIVERED, true);
//			tma.updateElement(notification, MODULE);
//			transactionManager.commit();
//		}
//		catch (Exception e)
//		{
//			logger.fatal("Error setting notification to delivered:", e);
//		}
//		return null;
//	}
//
//	public Boolean notificationSeen(String notificationID) {
//		try (TransactionManager transactionManager = TransactionManager.getInstance())
//		{
//			TransactionModuleAccess tma = TransactionModuleAccess.getInstance(transactionManager);
//			ElementData notification = tma.getElementById(MODULE, notificationID);
//			notification.put(E_NOTIFICATION_SEEN, true);
//			tma.updateElement(notification, MODULE);
//			transactionManager.commit();
//		}
//		catch (Exception e)
//		{
//			logger.fatal("Error setting notification to read:", e);
//		}
//		return null;
//	}
//}
