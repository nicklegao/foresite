/**
 * 
 */
package au.corporateinteractive.qcloud.proposalbuilder.db;

import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import webdirector.db.client.ManagedDataAccess;

import java.sql.SQLException;

/**
 * @author Jaysun Lee
 *
 */
public class RestApiDataAccess extends ManagedDataAccess
{

	/**
	 * @param apiId
	 * @param apiKey
	 * @param proposalId
	 */
	public boolean verifyAccess(String apiId, String apiKey, int proposalId)
	{
		try
		{
			String result = TransactionDataAccess.getInstance().selectString(
					" SELECT count(*) > 0 "
							+ " FROM categories_users c1users"
							+ " JOIN categories_users c2users on c1users.category_id = c2users.category_parentId"
							+ " JOIN elements_useraccounts eusers on c2users.category_id = eusers.category_id"
							+ " JOIN elements_proposals eproposals on eusers.element_id = eproposals.ATTRDROP_ownerId "
							+ " WHERE c1users.attr_apiId = ? AND c1users.attr_apiKey = ? AND eproposals.element_id = ? ",
					apiId, apiKey, Integer.toString(proposalId));

			return Integer.parseInt(result) == 1;
		}
		catch (SQLException e)
		{
			return false;
		}

	}

	public boolean verifyAccess(String apiId, String apiKey, String username)
	{
		try
		{
			String result = TransactionDataAccess.getInstance().selectString(
					" SELECT count(*) > 0 "
							+ " FROM categories_users c1users"
							+ " JOIN categories_users c2users on c1users.category_id = c2users.category_parentId"
							+ " JOIN elements_useraccounts eusers on c2users.category_id = eusers.category_id"
							+ " WHERE c1users.attr_apiId = ? AND c1users.attr_apiKey = ? AND eusers.ATTR_Headline = ? ",
					apiId, apiKey, username);

			return Integer.parseInt(result) == 1;
		}
		catch (SQLException e)
		{
			return false;
		}

	}

	public boolean isAdmin(String apiId, String apiKey, String username)
	{
		try
		{
			String result = TransactionDataAccess.getInstance().selectString(
					" SELECT count(*) > 0  "
							+ " FROM categories_users c1users"
							+ " JOIN categories_users c2users on c1users.category_id = c2users.category_parentId"
							+ " JOIN elements_useraccounts eusers on c2users.category_id = eusers.category_id"
							+ " WHERE c1users.attr_apiId = ? AND c1users.attr_apiKey = ? AND eusers.ATTR_Headline = ? and ATTRDROP_userType = 'admin' ",
					apiId, apiKey, username);

			return Integer.parseInt(result) == 1;
		}
		catch (SQLException e)
		{
			return false;
		}

	}

	public String getUserId(String apiId, String apiKey, String username)
	{
		try
		{
			return TransactionDataAccess.getInstance().selectString(
					" SELECT element_id "
							+ " FROM categories_users c1users"
							+ " JOIN categories_users c2users on c1users.category_id = c2users.category_parentId"
							+ " JOIN elements_useraccounts eusers on c2users.category_id = eusers.category_id"
							+ " WHERE c1users.attr_apiId = ? AND c1users.attr_apiKey = ? AND eusers.ATTR_Headline = ? ",
					apiId, apiKey, username);
		}
		catch (SQLException e)
		{
			return null;
		}

	}
}
