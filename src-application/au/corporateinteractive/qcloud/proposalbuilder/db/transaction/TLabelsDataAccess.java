package au.corporateinteractive.qcloud.proposalbuilder.db.transaction;

import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.net.webdirector.common.datalayer.base.db.entity.TextFile;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;

public class TLabelsDataAccess extends TManagedDataAccess
{
	Logger logger = Logger.getLogger(TLabelsDataAccess.class);

	public static final String MODULE = "users";

	public static final String C1_CUSTOM_LABELS = "ATTRTEXT_customLabels";

	public TLabelsDataAccess(TransactionManager txManager)
	{
		super(txManager);
	}

	public HashMap<String, String> getCompanyCustomLabels(String companyId)
	{
		HashMap<String, String> ret = new HashMap<>();
		TUserAccountDataAccess uada = new TUserAccountDataAccess(txManager);

		CategoryData companySettings = uada.getCompanyForCompanyId(companyId);
		TextFile tf = companySettings.getTextFile(C1_CUSTOM_LABELS);
		Type mapType = new TypeToken<Map<String, String>>()
		{
		}.getType();
		if (tf != null)
		{
			if (tf.getContent() != null && !"".equals(tf.getContent().trim()))
			{
				return new Gson().fromJson(tf.getContent(), mapType);
			}
		}
		return null;
	}

	public int updateCompanyCustomLabels(HttpServletRequest request) throws SQLException, IOException
	{
		TUserAccountDataAccess tuada = new TUserAccountDataAccess(txManager);

		String userId = (String) request.getSession().getAttribute(Constants.PORTAL_USER_ID_ATTR);
		CategoryData company = tuada.getUserCompanyForUserId(userId);

		String jsonLabels = request.getParameter("labels");

		TextFile jsonLabelsFile = new TextFile(jsonLabels, MODULE, C1_CUSTOM_LABELS, company.getId(), true);
		company.put(C1_CUSTOM_LABELS, jsonLabelsFile);

		return TransactionModuleAccess.getInstance(txManager).updateCategory(company, MODULE);
	}
}
