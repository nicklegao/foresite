package au.corporateinteractive.qcloud.proposalbuilder.db.transaction;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.ElementDataMapper;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.common.utils.module.StoreUtils;

public class TUserRolesDataAccess extends TManagedDataAccess
{
	Logger logger = Logger.getLogger(TUserRolesDataAccess.class);

	public static final String MODULE = "ROLES";

	public static final String C_COMPANY_ID = "ATTRDROP_CompanyID";

//	public static final String MANAGE_STYLE = "ATTRCHECK_companyStyle";
//	public static final String MANAGE_COVERS = "ATTRCHECK_coverPages";
//	public static final String MANAGE_TEMPLATES = "ATTRCHECK_template";
	public static final String MANAGE_PROJECTS = "ATTRCHECK_project";
	public static final String MANAGE_USERS = "ATTRCHECK_users";
//	public static final String MANAGE_CONTENT_TEAM_LIBRARY = "ATTRCHECK_contentTeamFolder";
//	public static final String MANAGE_CONTENT_TEXT = "ATTRCHECK_contentText";
//	public static final String MANAGE_CONTENT_IMAGE = "ATTRCHECK_contentImage";
//	public static final String MANAGE_CONTENT_VIDEO = "ATTRCHECK_contentVideo";
//	public static final String MANAGE_CONTENT_PRODUCT = "ATTRCHECK_contentProduct";
//	public static final String MANAGE_CONTENT_PDF = "ATTRCHECK_contentPDF";
//	public static final String MANAGE_CONTENT_SPREADSHEET = "ATTRCHECK_contentSpreadsheet";
	public static final String MANAGE_SETTINGS = "ATTRCHECK_companySettings";
//	public static final String MANAGE_CUSTOM_FIELDS = "ATTRCHECK_customFields";
//	public static final String MANAGE_PICING_DISCOUNTS = "ATTRCHECK_pricingDiscounts";
//	public static final String VIEW_ALL_PROPOSALS = "ATTRCHECK_viewAllProposals";
//	public static final String VIEW_TEAM_PROPOSALS = "ATTRCHECK_viewAllTeamProposals";
//	public static final String CREATE_ON_BEHALF = "ATTRCHECK_createOnBehalf";
//	public static final String MANAGE_SABRE_INTEGRATION = "ATTRCHECK_SabreIntegration";
//	public static final String MANAGE_SUBSCRIPTIONS = "ATTRCHECK_subscriptions";
//	public static final String MANAGE_PRICING_STYLE = "ATTRCHECK_pricingStyle";
//	public static final String VIEW_ALL_ANALYTICS = "ATTRCHECK_viewAllAnalytics";
//	public static final String VIEW_TEAMS_ANALYTICS = "ATTRCHECK_viewTeamAnalytics";
//	public static final String MANAGE_CONTACT_ADDRESS_BOOK = "ATTRCHECK_contactAddressBook";

	public static final String[] ALL_FIELDS = {
//			MANAGE_STYLE,
//			MANAGE_COVERS,
//			MANAGE_TEMPLATES,
			MANAGE_PROJECTS,
			MANAGE_USERS,
//			MANAGE_CONTENT_TEAM_LIBRARY,
//			MANAGE_CONTENT_TEXT,
//			MANAGE_CONTENT_IMAGE,
//			MANAGE_CONTENT_VIDEO,
//			MANAGE_CONTENT_PRODUCT,
//			MANAGE_CONTENT_PDF,
//			MANAGE_CONTENT_SPREADSHEET,
			MANAGE_SETTINGS,
//			MANAGE_CUSTOM_FIELDS,
//			MANAGE_PICING_DISCOUNTS,
//			VIEW_ALL_PROPOSALS,
//			VIEW_TEAM_PROPOSALS,
//			CREATE_ON_BEHALF,
//			MANAGE_SABRE_INTEGRATION,
//			MANAGE_SUBSCRIPTIONS,
//			MANAGE_PRICING_STYLE,
//			VIEW_ALL_ANALYTICS,
//			VIEW_TEAMS_ANALYTICS,
//			MANAGE_CONTACT_ADDRESS_BOOK,
	};

	private StoreUtils su = new StoreUtils();
	private Defaults d = Defaults.getInstance();

	public TUserRolesDataAccess()
	{
		super();
	}

	public TUserRolesDataAccess(TransactionManager txManager)
	{
		super(txManager);
	}

	public List<ElementData> getCompanyRoles(String companyId) throws SQLException
	{
		List<ElementData> list = new ArrayList<ElementData>();

		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
		CategoryData cat = new CategoryData();
		cat.put(C_COMPANY_ID, companyId);
		CategoryData company = ma.getCategory(MODULE, cat);

		list = ma.getElementsByParentId(MODULE, company.getId());

		return list;
	}

	public CategoryData getRoleFolderCompanyId(String id) throws SQLException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		CategoryData categoryData = new CategoryData();
		categoryData.put(C_COMPANY_ID, id);
		categoryData = tma.getCategory(MODULE, categoryData);
		if (categoryData != null)
		{
			return categoryData;
		}
		return null;
	}

	public ElementData getRoleForId(String roleId) throws SQLException
	{
		ElementData elementData = null;

		elementData = TransactionModuleAccess.getInstance(txManager).getElementById(MODULE, roleId);

		return elementData;
	}

	public List<ElementData> getRolesForUserId(int userId) throws SQLException
	{
		return getRolesForUserId(userId + "");
	}

	public List<ElementData> getRolesForUserId(String userId) throws SQLException
	{
		DataAccess da = DataAccess.getInstance();

		String sql = "select r.* from elements_roles r "
				+ "join drop_down_multi_selections ddms on ddms.selected_value = r.element_id "
				+ "join elements_useraccounts users on ddms.element_id = users.element_id "
				+ "where users.element_id = ? "
				+ "and ddms.ATTRMULTI_name = 'ATTRMULTI_roles' "
				+ "and ddms.module_name = 'useraccounts' "
				+ "order by r.element_id ";

		ElementDataMapper edm = new ElementDataMapper();

		List<ElementData> roles = da.select(sql,
				new String[] { userId },
				edm);

		return roles;
	}

	public ElementData createRole(String roleCategoryId, String roleName,
			Boolean manageProjects,
			Boolean manageUsers,
			Boolean manageCompanySettings) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		ElementData role = new ElementData();
		role.setParentId(roleCategoryId);
		role.setLive(true);
		role.setName(roleName);
//		role.put(MANAGE_STYLE, manageCompanyStyle);
//		role.put(MANAGE_COVERS, manageCoverPages);
		// role.put(MANAGE_TEMPLATES, manageTemplates);
		role.put(MANAGE_PROJECTS, manageProjects);
		role.put(MANAGE_USERS, manageUsers);
//		role.put(MANAGE_CONTENT_TEXT, manageContentText);
//		role.put(MANAGE_CONTENT_IMAGE, manageContentImage);
//		role.put(MANAGE_CONTENT_VIDEO, manageContentVideo);
//		role.put(MANAGE_CONTENT_PDF, manageContentPDF);
		role.put(MANAGE_SETTINGS, manageCompanySettings);
//		role.put(VIEW_ALL_PROPOSALS, viewAllProposals);
//		role.put(MANAGE_SABRE_INTEGRATION, sabreIntegration);
//		role.put(MANAGE_SUBSCRIPTIONS, manageSubscriptions);
		tma.insertElement(role, MODULE);
		return role;
	}

	public ElementData updateRole(String companyId, String roleId, String roleName,
			Boolean manageProjects,
			Boolean manageUsers,
			Boolean manageCompanySettings) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		ElementData originalRole = tma.getElementById(MODULE, roleId);
		ElementData role = new ElementData();
		role.setId(originalRole.getId());
		role.put(E_HEADLINE, roleName);
//		role.put(MANAGE_STYLE, manageCompanyStyle);
//		role.put(MANAGE_COVERS, manageCoverPages);
//		role.put(MANAGE_TEMPLATES, manageTemplates);
		role.put(MANAGE_PROJECTS, manageProjects);
		role.put(MANAGE_USERS, manageUsers);
//		role.put(MANAGE_CONTENT_TEXT, manageContentText);
//		role.put(MANAGE_CONTENT_IMAGE, manageContentImage);
//		role.put(MANAGE_CONTENT_VIDEO, manageContentVideo);
//		role.put(MANAGE_CONTENT_PDF, manageContentPDF);
		role.put(MANAGE_SETTINGS, manageCompanySettings);
//		role.put(VIEW_ALL_PROPOSALS, viewAllProposals);
//		role.put(MANAGE_SABRE_INTEGRATION, sabreIntegration);
//		role.put(MANAGE_SUBSCRIPTIONS, manageSubscriptions);
		tma.updateElement(role, MODULE);
		return role;
	}

	public int deleteRole(String companyId, String roleId) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		TransactionDataAccess tda = TransactionDataAccess.getInstance(txManager);
		tma.deleteElementById(MODULE, roleId);
		QueryBuffer qb = new QueryBuffer();
		qb.append("DELETE FROM drop_down_multi_selections where SELECTED_VALUE = ? AND ATTRMULTI_NAME = 'ATTRMULTI_ROLES'", roleId);
		return tda.update(qb);
	}

	public UserPermissions getPermissions(int userId) throws SQLException
	{
		return getPermissions(Integer.toString(userId));
	}

	public UserPermissions getPermissions(String userId) throws SQLException
	{
		Hashtable<String, Boolean> ht = getDefaultPermissions();


		TransactionDataAccess da = TransactionDataAccess.getInstance(txManager);

		String permissions = "role." + StringUtils.join(ALL_FIELDS, ", role.");

		QueryBuffer qb = new QueryBuffer();

		qb.append("select " + permissions + " from elements_USERACCOUNTS u "
				+ "join drop_down_multi_selections ddms on ddms.element_id = u.element_id "
				+ "join elements_roles role on ddms.selected_value = role.element_id "
				+ "where u.element_id = ? "
				+ "and ddms.ATTRMULTI_name = 'ATTRMULTI_roles' "
				+ "and ddms.module_name = 'USERACCOUNTS'", userId);

		List<ElementData> roles = da.selectMaps(qb, new ElementDataMapper());

		for (ElementData role : roles)
		{
			for (String str : ALL_FIELDS)
			{
				if (role.getBoolean(str))
				{
					ht.put(str, true);
				}
			}
		}

		return new UserPermissions(userId, ht);
	}

	private Hashtable<String, Boolean> getDefaultPermissions()
	{
		Hashtable<String, Boolean> ht = new Hashtable<String, Boolean>();

		for (String str : ALL_FIELDS)
		{
			ht.put(str, false);
		}

		return ht;
	}

	public List<ElementData> getUsersInCompanyByRole(String companyId, String role) throws SQLException
	{
		QueryBuffer qb = new QueryBuffer();
		qb.append("select u.* from categories_useraccounts c, categories_useraccounts g, elements_useraccounts u, drop_down_multi_selections r, elements_roles r1 ");
		qb.append("where 1 = 1 ");
		qb.append("and c.category_id = g.category_parentid ");
		qb.append("and g.category_id = u.category_id ");
		qb.append("and r.ELEMENT_ID = u.element_id ");
		qb.append("and r.ATTRMULTI_NAME='ATTRMULTI_roles' ");
		qb.append("and r.MODULE_NAME='useraccounts' ");
		qb.append("and r.module_type='element' ");
		qb.append("and r1.element_id = r.SELECTED_VALUE ");
		qb.append("and r1." + role + " = 1 ");
		qb.append("and c.category_id = ? ", companyId);

		TransactionDataAccess da = TransactionDataAccess.getInstance(txManager);
		return da.selectMaps(qb, new ElementDataMapper());
	}

}
