package au.corporateinteractive.qcloud.proposalbuilder.db.transaction;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.common.utils.module.StoreUtils;

public class TCustomizableEditorDataAccess extends TManagedDataAccess
{


	Logger logger = Logger.getLogger(TCustomizableEditorDataAccess.class);
	public static final String MODULE = "USERS";
	public static final String C_CATEGORY_NAME = "ATTR_categoryName";
	public static final String C_PRICINGSTYLES = "ATTRTEXT_pricingStyles";

	public TCustomizableEditorDataAccess(TransactionManager txManager)
	{
		super(txManager);
	}

	public String getProposalStyles(String userId) throws Exception
	{
		TUserAccountDataAccess uada = new TUserAccountDataAccess(txManager);
		CategoryData company = uada.getUserCompanyForUserId(userId);

		StoreUtils storeUtils = new StoreUtils();
		String jsonFile = storeUtils.readFieldFromStores(company.getStoreFileUrl(C_PRICINGSTYLES));
		return jsonFile;
	}


	public void saveProposalStyles(String userId, String proposalStyle) throws Exception
	{

		TUserAccountDataAccess uada = new TUserAccountDataAccess(txManager);
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		CategoryData company = uada.getUserCompanyForUserId(userId);

		StoreUtils storeUtils = new StoreUtils();
		String jsonProposalFile = storeUtils.writeFieldToStores(MODULE, company.getId(), C_PRICINGSTYLES, proposalStyle, true);
		company.put(C_PRICINGSTYLES, jsonProposalFile);
		tma.updateCategory(company, MODULE);

	}
	
	
}
