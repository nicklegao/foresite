package au.corporateinteractive.qcloud.proposalbuilder.db.transaction;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.common.utils.module.StoreUtils;

public class TManagedDataAccess
{
	Logger logger = Logger.getLogger(TManagedDataAccess.class);

	public static final SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final String E_ID = "Element_id";
	public static final String E_CATEGORY_ID = "Category_id";
	public static final String E_HEADLINE = "ATTR_Headline";
	public static final String E_LIVE = "Live";

	public static final String C_ID = "Category_id";
	public static final String C_NAME = "ATTR_categoryName";
	public static final String C_PARENT = "Category_ParentID";
	public static final String C_FOLDER_LEVEL = "folderLevel";
	public static final String C_LIVE = E_LIVE;

	protected TransactionManager txManager = null;

	public TManagedDataAccess()
	{
		this.txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(true), StoreFileAccessFactory.getInstance(true));
	}

	public TManagedDataAccess(TransactionManager txManager)
	{
		this.txManager = txManager;
	}

	protected CategoryData standardCreateCategoryHashtable(String label)
	{
		return standardCreateCategoryHashtable(0, label);
	}

	protected CategoryData standardCreateCategoryHashtable(int parentId, String label)
	{
		return standardCreateCategoryHashtable(parentId, 1, label);
	}

	protected CategoryData standardCreateCategoryHashtable(int parentId, int folderLevel, String label)
	{
		CategoryData category = new CategoryData();
		category.setFolderLevel(folderLevel);
		category.setParentId(Integer.toString(parentId));
		category.setName(label);

		return category;
	}

	protected ElementData standardCreateElementHashtable(int categoryId, String label)
	{
		return standardCreateElementHashtable(Integer.toString(categoryId), label);
	}

	protected ElementData standardCreateElementHashtable(String categoryId, String label)
	{
		ElementData element = new ElementData();
		element.setLive(true);
		element.put("Language_id", "1");
		element.put("Status_id", "1");
		element.put("Version", "1");
		element.put("Lock_id", "1");
		element.setName(label);
		element.setParentId(categoryId);

		return element;
	}

	protected String standardLiveClauseForCustomQuery()
	{
		return standardLiveClauseForCustomQuery(null);
	}

	protected String standardLiveClauseForCustomQuery(String tableAlias)
	{
		String tablePrefix = "";
		if (tableAlias != null && tableAlias.length() > 0)
		{
			tablePrefix = tableAlias + ".";
		}
		return " " + tablePrefix + "Live = 1 "
				+ " and (" + tablePrefix + "Live_date <= now() or " + tablePrefix + "Live_date IS NULL ) "
				+ " and (" + tablePrefix + "Expire_date >= now() or " + tablePrefix + "Expire_date IS NULL ) ";
	}

	protected int createElement(ElementData element,
			String module) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		ElementData updatedElement = tma.insertElement(element, module);
		int id = Integer.valueOf(updatedElement.getId());
		return id;
	}

	protected int createCategory(CategoryData category,
			String module) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		CategoryData updatedElement = tma.insertCategory(category, module);
		int id = Integer.valueOf(updatedElement.getId());
		return id;
	}

	protected int updateElement(ElementData element,
			String module) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		return tma.updateElement(element, module);
	}

	protected int updateCategory(CategoryData category,
			String module) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		return tma.updateCategory(category, module);
		//db.updateData(ht, "Category_id", "categories_" + module);
	}

	protected boolean deleteElement(String element,
			String module) throws NumberFormatException, SQLException, IOException
	{
		return deleteElement(Integer.parseInt(element), module);
	}

	protected boolean deleteElement(int element,
			String module) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		StoreUtils su = new StoreUtils();
		boolean success = su.deleteElementFromStores(module, element);

		int updated = tma.deleteElementById(module, String.valueOf(element));
//		int updated = DataAccess.getInstance().updateData(
//				"delete from elements_" + module +
//						" where Element_id=?",
//				new String[] { Integer.toString(element) });
		//Uncomment and test the following when needed...
//	      int dropdowns = DataAccess.getInstance().updateData(
//	                      "delete from drop_down_multi_selections "+
//	                      "where MODULE_NAME="+module+" "+
//	                      "and ELEMENT_ID="+element, new String[]{});

		return success && updated > 0;
	}

	public int getOrCreateCategory(String categoryName, String module) throws SQLException, IOException
	{
		return getOrCreateCategory(categoryName, 0, 1, module);
	}

	public int getOrCreateCategory(String companyId, String companyName, String module) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		CategoryData category = tma.getCategoryById(module, companyId);
		if (category != null)
		{
			return Integer.parseInt(category.getId());
		}
//		List<String> list = db.selectQuerySingleCol("select category_id from categories_" + module + " where attrdrop_companyid = ?", new String[] { companyId });
//		if (list.size() > 0)
//		{
//			return Integer.parseInt(list.get(0));
//		}
		CategoryData newCategory = standardCreateCategoryHashtable(companyName);
		newCategory.setFolderLevel(1);
		newCategory.setParentId("0");
		newCategory.put("ATTRDROP_companyID", companyId);

		//Hashtable<String, String> ht = standardCreateCategoryHashtable(companyName);
//		insertNotNull(ht, "Category_ParentID", "0");
//		insertNotNull(ht, "folderLevel", "1");
//		insertNotNull(ht, "ATTRDROP_companyID", companyId);
		return createCategory(newCategory, module);
	}

	public int getOrCreateCategory(String categoryName, int parentId, int folderLevel, String module) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		CategoryData searchOptions = new CategoryData();
		searchOptions.setName(categoryName);
		searchOptions.setParentId(String.valueOf(parentId));
		CategoryData searchResult = tma.getCategory(module, searchOptions);
		//String categoryId = cda.getCategoryIdByCategoryNameWithParent(module, categoryName, parentId);
		String categoryId;
		if (searchResult == null)
		{
			CategoryData category = standardCreateCategoryHashtable(categoryName);
			category.setParentId(String.valueOf(parentId));
			category.setFolderLevel(folderLevel);
			categoryId = String.valueOf(createCategory(category, module));
		}
		else
		{
			categoryId = searchResult.getId();
		}
//		if (categoryId == null)
//		{
//			Hashtable<String, String> ht = standardCreateCategoryHashtable(categoryName);
//			insertNotNull(ht, "Category_ParentID", parentId);
//			insertNotNull(ht, "folderLevel", folderLevel);
//			categoryId = Integer.toString(createCategory(ht, module));
//		}
		return Integer.parseInt(categoryId);
	}


	/*
	 * This would be where we assign access to specific content libraries to teams
	 */

	/*
	 * Reusable utility functions
	 */

	protected int getYMDCategory(Date date, String module) throws SQLException, IOException
	{
		int yearID = getOrCreateCategory(new SimpleDateFormat("yyyy").format(date), module);
		int monthID = getOrCreateCategory(new SimpleDateFormat("MM MMM").format(date), yearID, 2, module);
		int dayID = getOrCreateCategory(new SimpleDateFormat("dd EEE").format(date), monthID, 3, module);
		return dayID;
	}

	public boolean checkContentAccess(String contenId, String module, String proposalId) throws SQLException
	{
		return false;


	}
}
