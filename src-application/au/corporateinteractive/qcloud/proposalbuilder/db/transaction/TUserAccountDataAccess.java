package au.corporateinteractive.qcloud.proposalbuilder.db.transaction;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.MultiOptions;
import au.net.webdirector.common.datalayer.base.db.entity.TextFile;
import au.net.webdirector.common.datalayer.base.db.mapper.ElementDataMapper;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.common.utils.module.StoreUtils;
import au.net.webdirector.dev.annotation.DbColumn;
import au.net.webdirector.dev.annotation.Module;

@Module(name = "USERACCOUNTS")
public class TUserAccountDataAccess extends TManagedDataAccess
{
	Logger logger = Logger.getLogger(TUserAccountDataAccess.class);

	public static final String USER_MODULE = "USERACCOUNTS";
	public static final String SPECIAL_CATEGORY_NAME = "Special";
	public static final String LAM_CATEGORY_NAME = "LAMs";
	public static final String INTERNAL_CATEGORY_NAME = "Internal";

	//Profile Fields
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_NAME = "ATTR_name";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_SURNAME = "ATTR_surname";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_PASSWORD_EXPIRE = "ATTRDATE_passwordExpire";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_PASSWORD = "ATTR_password";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_RESET_PASSWORD = "ATTR_resetPassword";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_MOBILE_NUMBER = "ATTR_mobileNumber";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_USER_TYPE = "ATTRDROP_userType";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_USER_ROLES = "ATTRMULTI_roles";

	//Image/video/text/produt multi-items
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String E_TEAM_PRODUCT_FOLDERS = "ATTRMULTI_product_library_folder";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String E_TEAM_TEXT_FOLDERS = "ATTRMULTI_text_library_folder";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String E_TEAM_IMAGE_FOLDERS = "ATTRMULTI_Image_library_folder";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String E_TEAM_VIDEO_FOLDERS = "ATTRMULTI_video_library_folder";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String E_TEAM_PDF_FOLDERS = "ATTRMULTI_pdf_library_folder";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String E_TEAM_SPREADSHEET_FOLDERS = "ATTRMULTI_spreadsheet_library_folder";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String E_TEAM_COVERPAGE_FOLDERS = "ATTRMULTI_coverpage_library_folder";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String E_TEAM_TEMPLATE_FOLDERS = "ATTRMULTI_template_library_folder";

	//Switch for team libraries
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String E_TEAM_IMAGE_ACTIVE = "ATTRCHECK_Image_library_activated";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String E_TEAM_VIDEO_ACTIVE = "ATTRCHECK_video_library_activated";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String E_TEAM_TEXT_ACTIVE = "ATTRCHECK_text_library_activated";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String E_TEAM_PRODUCT_ACTIVE = "ATTRCHECK_products_library_activated";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String E_TEAM_PDF_ACTIVE = "ATTRCHECK_pdf_library_activated";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String E_TEAM_SPREADSHEET_ACTIVE = "ATTRCHECK_spreadsheet_library_activated";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String E_TEAM_COVERPAGE_ACTIVE = "ATTRCHECK_coverpage_library_activated";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String E_TEAM_TEMPLATE_ACTIVE = "ATTRCHECK_template_library_activated";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_MANAGES_MULTI = "ATTRMULTI_manages";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_APPROVAL_USERS_MULTI = "ATTRMULTI_approvalUsers";

	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_EMAIL_DAILY_SUMMARY = "ATTRCHECK_notifyDailyEmailSummary";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_EMAIL_NEW_COMMENT = "ATTRCHECK_notifyNewComment";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_EMAIL_HANDOVER = "ATTRCHECK_notifyHandover";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_EMAIL_STATUS_CHANGE = "ATTRCHECK_notifyProposalStatusChange";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_EMAIL_OPENED = "ATTRCHECK_notifyProposalOpened";

	//BILLING EMAIL TO
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_BILLING_USERS = "ATTRMULTI_BillingUsers";

	// Address fields
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_ABN = "ATTR_abnacn";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_ADDRESS_LINE_1 = "ATTR_addressLine1";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_ADDRESS_LINE_2 = "ATTR_addressLine2";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_ADDRESS_CITY_SUBURB = "ATTR_citySuburb";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_ADDRESS_STATE = "ATTR_state";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_ADDRESS_POSTCODE = "ATTR_postcode";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_ADDRESS_COUNTRY = "ATTRDROP_country";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_PRICING_PLAN = "ATTRDROP_pricingPlan";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_TRIAL_PLAN_EXPIRY = "ATTRDATE_trialPlanExpiry";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_CRM_CONTRACT_ID = "ATTRINTEGER_crmContractId";

	// Company style settings
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_COVER_TITLE_FONT = "ATTRDROP_cover_title_font";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_COVER_TITLE_SIZE = "ATTRINTEGER_cover_title_size";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_COVER_COMPANY_FONT = "ATTRDROP_cover_company_font";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_COVER_COMPANY_SIZE = "ATTRINTEGER_cover_company_size";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_BODY_FONT = "ATTRDROP_bodyFont";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_BODY_SIZE = "ATTRINTEGER_bodySize";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_BODY_COLOR = "ATTR_bodyColor";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SECTION_FONT = "ATTRDROP_sectionFont";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SECTION_SIZE = "ATTRINTEGER_sectionSize";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SECTION_CUSTOM_FONT_COLOR = "ATTRCHECK_sectionCustomFontColor";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SECTION_HEADER_COLOR = "ATTR_sectionHeaderColor";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SECTION_UPPERCASE = "ATTRCHECK_sectionUppercase";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SECTION_UNDERLINE = "ATTRCHECK_sectionUnderline";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SECTION_BOLD = "ATTRCHECK_sectionBold";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H1_FONT = "ATTRDROP_h1Font";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H1_SIZE = "ATTRINTEGER_h1Size";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H1_CUSTOM_FONT_COLOR = "ATTRCHECK_h1CustomFontColor";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H1_HEADER_COLOR = "ATTR_h1HeaderColor";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H1_UPPERCASE = "ATTRCHECK_h1Uppercase";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H1_UNDERLINE = "ATTRCHECK_h1Underline";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H1_BOLD = "ATTRCHECK_h1Bold";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H2_FONT = "ATTRDROP_h2Font";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H2_SIZE = "ATTRINTEGER_h2Size";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H2_CUSTOM_FONT_COLOR = "ATTRCHECK_h2CustomFontColor";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H2_HEADER_COLOR = "ATTR_h2HeaderColor";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H2_UPPERCASE = "ATTRCHECK_h2Uppercase";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H2_UNDERLINE = "ATTRCHECK_h2Underline";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H2_BOLD = "ATTRCHECK_h2Bold";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H3_FONT = "ATTRDROP_h3Font";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H3_SIZE = "ATTRINTEGER_h3Size";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H3_CUSTOM_FONT_COLOR = "ATTRCHECK_h3CustomFontColor";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H3_HEADER_COLOR = "ATTR_h3HeaderColor";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H3_UPPERCASE = "ATTRCHECK_h3Uppercase";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H3_UNDERLINE = "ATTRCHECK_h3Underline";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_H3_BOLD = "ATTRCHECK_h3Bold";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PALETTE1 = "ATTR_palette1";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PALETTE2 = "ATTR_palette2";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PALETTE3 = "ATTR_palette3";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PALETTE4 = "ATTR_palette4";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_CONTENT_PADDING_LEFT = "ATTRINTEGER_contentPaddingLeft";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_CONTENT_PADDING_TOP = "ATTRINTEGER_contentPaddingTop";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_CONTENT_PADDING_RIGHT = "ATTRINTEGER_contentPaddingRight";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_CONTENT_PADDING_BOTTOM = "ATTRINTEGER_contentPaddingBottom";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_BACKGROUND_COLOUR = "ATTR_backgroundColour";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_SECTION_BACKGROUND_COLOUR = "ATTR_sectionBackgroundColour";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_LOGO = "ATTRFILE_Logo";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_EXPIRY_DATE = "ATTR_expiryDate";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PASSWORD_EXPIRY_DATE = "ATTRINTEGER_passwordExpiryDate";

	// Company Settings
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_HEADER_TOP_ACTIVE = "ATTRCHECK_HeaderTopActive";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_HEADER_BOTTOM_ACTIVE = "ATTRCHECK_HeaderBottomActive";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_HEADER_TOP_BACKGROUND = "ATTR_HeaderTopBackground";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_HEADER_BOTTOM_BACKGROUND = "ATTR_HeaderBottomBackground";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_HEADER_TOP_FONT_COLOR = "ATTR_HeaderTopFontColor";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_HEADER_BOTTOM_FONT_COLOR = "ATTR_HeaderBottomFontColor";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_HEADER_TOP_DATA = "ATTR_HeaderTOPData";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_HEADER_BOTTOM_DATA = "ATTR_HeaderBottomData";

	// Company Settings
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_LOCALE = "ATTR_locale";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_DATE_FORMAT = "ATTR_DateFormat";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ORDER_NOW_REDIRECT_LINK = "ATTR_orderNowRedirectLink";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ORDER_NOW_LABEL = "ATTR_orderNowLabel";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_API_ID = "ATTR_apiId";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_API_KEY = "ATTR_apiKey";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_CUSTOM_FIELDS = "ATTRTEXT_customFields";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PRICING_DISCOUNTS = "ATTRTEXT_pricingThreshold";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PRICING_TEMPLATE = "ATTR_pricingTemplateName";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ESTIMATED_VALUE = "ATTRCHECK_estimatedValue";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_HIDE_TERMS_ALWAYS = "ATTRCHECK_hideTermsAlways";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ROUNDING = "ATTRCHECK_roundingValue";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String PATCH_ALLOW_TEAM_EDITING = "ATTRCHECK_teamCanEdit";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String ALLOW_ENCRYPTED_DATA_SYNC = "ATTRCHECK_encryptedDataSync";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ENABLE_TAX = "ATTRCHECK_enableTax";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ENABLE_ADDITION = "ATTRCHECK_enableAddition";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_TAX_LABEL = "ATTR_taxLabel";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_TAX_PERCENTAGE = "ATTR_taxPercentage";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_FIRST_LOGIN = "ATTRCHECK_firstLogin";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_TABLE_STYLE = "ATTRDROP_tableStyle";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_TAB_STYLE = "ATTRDROP_tabStyle";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_SHOW_TABS = "ATTRCHECK_showTabs";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_HIDE_COVERPAGE = "ATTRCHECK_hideCoverPage";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_HIDE_COVERPAGE_PROPOSAL_INFO = "ATTRCHECK_hideCoverProposalInfo";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_HIDE_COVERPAGE_SENDER_INFO = "ATTRCHECK_hideCoverSenderInfo";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_HIDE_COVERPAGE_BOX_BACKGROUND = "ATTRCHECK_hideCoverBoxBackground";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_COVERPAGE_FONT_COLOR = "ATTR_coverFontColor";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_PRICING_STYLES = "ATTRTEXT_pricingStyles";

	// Billing info fields
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ACTIVATED = "ATTRCHECK_Activated";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_BILLING_START_DATE = "ATTRDATE_BillingStartDate";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PREAPPROVAL_KEY = "ATTR_PreapprovalKey";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_BILLING_PERIOD = "ATTRDROP_BillingPeriod";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_LAST_BILLING_DATE = "ATTRDATE_LastBillingDate";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_STATUS = "ATTRDROP_Status";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_RESELLER = "ATTRDROP_Reseller";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PAY_OPTION = "ATTRDROP_PayOption";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_STRIPE_ID = "ATTR_StripeID";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_MULTI_PRICING = "ATTRCHECK_multiPricing";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_TIME_ZONE = "ATTR_timeZone";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_TEXT_MAPPING_FILE_NAME = "ATTR_textMappingFileName";


	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_EMAIL_STATUS_DEFAULT = "ATTR_defaultEmailStatus";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_EMAIL_STATUS_STRING = "ATTR_emailStatusString";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_STATUS_REASON_TEXT = "ATTRTEXT_statusReason";

	// travel itineraries
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ITINERARY_TEMPLATE = "ATTRDROP_itineraryTemplateId";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_PROPOSAL_TEMPLATE = "ATTRDROP_proposalTemplateId";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_AUTO_SEND_ITINERARY = "ATTRCHECK_autoSendItinerary";

	//Travelport details
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_TRAVELPORT_BRANCH_CODE = "ATTR_travelport_branchCode";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_TRAVELPORT_CONSULTANT_ID = "ATTR_travelport_consultantId";
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String E_TRAVELPORT_CONSULTANT_PASSWORD = "ATTR_travelport_consultantPassword";

	//ArrivalGuide
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ARRIVALGUIDE_API = "ATTR_arrivalguide_api_key";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ADD_USER_PLAN_QTY = "ATTRINTEGER_AddUserPlanQty";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ADD_DATA_PLAN_QTY = "ATTRINTEGER_AddDataPlanQty";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ALERTED_DATA_PLAN_QTY = "ATTRINTEGER_AlertedDataPlanQty";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C2_STRIPE_PK = "ATTR_StripePK";
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C2_STRIPE_SK = "ATTR_StripeSK";


	//Company Support
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_SUPPORT_AUTH_TIME = "ATTRDATE_supportAuthTime";

	//QuoteCloud Support
	@DbColumn(type = DbColumn.TYPE_ELEMENT)
	public static final String C1_SUPPORT_TIME = "ATTRDATE_supportAuthTime";

	//Client data encryption
	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C1_ENCRYPT_CLIENT_DATA = "ATTRCHECK_encryptClientData";

	@DbColumn(type = DbColumn.TYPE_CATEGORY)
	public static final String C_DELETION_ALERT_EMAIL_SENT = "ATTRDATE_deletionAlertEmailSent";

	private StoreUtils su = new StoreUtils();
	private Defaults d = Defaults.getInstance();


	public static String libraryDBRef(String library)
	{
		if (library.equalsIgnoreCase("products") || library.equalsIgnoreCase("productFolder"))
		{
			return TUserAccountDataAccess.E_TEAM_PRODUCT_FOLDERS;
		}
		else if (library.equalsIgnoreCase("text"))
		{
			return TUserAccountDataAccess.E_TEAM_TEXT_FOLDERS;
		}
		else if (library.equalsIgnoreCase("images"))
		{
			return TUserAccountDataAccess.E_TEAM_IMAGE_FOLDERS;
		}
		else if (library.equalsIgnoreCase("videos"))
		{
			return TUserAccountDataAccess.E_TEAM_VIDEO_FOLDERS;
		}
		else if (library.equalsIgnoreCase("pdf"))
		{
			return TUserAccountDataAccess.E_TEAM_PDF_FOLDERS;
		}
		else if (library.equalsIgnoreCase("coverpage"))
		{
			return TUserAccountDataAccess.E_TEAM_VIDEO_FOLDERS;
		}
		else if (library.equalsIgnoreCase("templates"))
		{
			return TUserAccountDataAccess.E_TEAM_VIDEO_FOLDERS;
		}
		return null;
	}

	public static String librarySwitchDBRef(String library)
	{
		if (library.equalsIgnoreCase("products"))
		{
			return TUserAccountDataAccess.E_TEAM_PRODUCT_ACTIVE;
		}
		else if (library.equalsIgnoreCase("text"))
		{
			return TUserAccountDataAccess.E_TEAM_TEXT_ACTIVE;
		}
		else if (library.equalsIgnoreCase("images"))
		{
			return TUserAccountDataAccess.E_TEAM_IMAGE_ACTIVE;
		}
		else if (library.equalsIgnoreCase("videos"))
		{
			return TUserAccountDataAccess.E_TEAM_VIDEO_ACTIVE;
		}
		else if (library.equalsIgnoreCase("pdf"))
		{
			return TUserAccountDataAccess.E_TEAM_PDF_ACTIVE;
		}
		else if (library.equalsIgnoreCase("coverpage"))
		{
			return TUserAccountDataAccess.E_TEAM_COVERPAGE_ACTIVE;
		}
		else if (library.equalsIgnoreCase("templates"))
		{
			return TUserAccountDataAccess.E_TEAM_TEMPLATE_ACTIVE;
		}
		return null;
	}

	public TUserAccountDataAccess()
	{
		super();
	}

	public TUserAccountDataAccess(TransactionManager txManager)
	{
		super(txManager);
	}

	public ElementData createUser(String teamId, String username, String email, String name, String surname, String password, String phone, List<String> roles, List<String> manages, List<String> approvalUsers) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		ElementData user = new ElementData();
		user.setParentId(teamId);
		user.put(E_HEADLINE, username);
		user.put(UserAccountDataAccess.E_EMAIL_ADDRESS, email);
		user.put(E_NAME, name);
		user.put(E_SURNAME, surname);
		user.put(E_RESET_PASSWORD, password);
		user.put(E_MOBILE_NUMBER, phone);

		user.setLive(false);
		ElementData updatedUser = tma.insertElement(user, USER_MODULE);
		MultiOptions role = new MultiOptions(updatedUser.getId(), E_USER_ROLES, USER_MODULE, updatedUser.getType(), roles);
		MultiOptions manage = new MultiOptions(updatedUser.getId(), E_MANAGES_MULTI, USER_MODULE, updatedUser.getType(), manages);
		MultiOptions approvalUser = new MultiOptions(updatedUser.getId(), E_APPROVAL_USERS_MULTI, USER_MODULE, updatedUser.getType(), approvalUsers);
		updatedUser.put(E_USER_ROLES, role);
		updatedUser.put(E_MANAGES_MULTI, manage);
		updatedUser.put(E_APPROVAL_USERS_MULTI, approvalUser);
		tma.updateElement(updatedUser, USER_MODULE);

		return updatedUser;
	}

	public ElementData updateUser(String userId, String teamId, String name, String surname, String password, String phone, List<String> roles, List<String> manages, List<String> approvalUsers) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		ElementData user = tma.getElementById(USER_MODULE, userId);
		MultiOptions role = new MultiOptions(userId, E_USER_ROLES, USER_MODULE, user.getType(), roles);
		MultiOptions manage = new MultiOptions(userId, E_MANAGES_MULTI, USER_MODULE, user.getType(), manages);
		MultiOptions approvalUser = new MultiOptions(userId, E_APPROVAL_USERS_MULTI, USER_MODULE, user.getType(), approvalUsers);
		user.setParentId(teamId);
		user.put(E_NAME, name);
		user.put(E_SURNAME, surname);
		user.put(E_MOBILE_NUMBER, phone);
		if (StringUtils.isNotBlank(password))
		{
			user.put(E_RESET_PASSWORD, password);
		}
		user.put(E_USER_ROLES, role);
		user.put(E_MANAGES_MULTI, manage);
		user.put(E_APPROVAL_USERS_MULTI, approvalUser);

		tma.updateElement(user, USER_MODULE);

		return user;
	}

	public void updateTwoFactorType(String userId, String type) throws Exception
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		ElementData user = tma.getElementById(USER_MODULE, userId);

		user.put("ATTRDROP_TwoFactorType", type);

		tma.updateElement(user, USER_MODULE);

	}

	public void updateEmailTwoFactor(String userId, boolean updateEmailVerification) throws Exception
	{
		updateTwoFactorType(userId, "email");
	}

	public void updateMobileTwoFactor(String userId, boolean enableTwoFactor) throws Exception
	{
		updateTwoFactorType(userId, "mobile");
	}

	public int deleteUser(String companyId, String userId) throws SQLException, IOException
	{
		TransactionDataAccess tda = TransactionDataAccess.getInstance(txManager);
		QueryBuffer qb = new QueryBuffer();
		qb.append("DELETE FROM drop_down_multi_selections where SELECTED_VALUE = ? AND ATTRMULTI_NAME = ?", userId, E_MANAGES_MULTI);
		tda.update(qb);
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		int delete = tma.deleteElementById(USER_MODULE, userId);
		if (delete > 0)
		{
//			MarketSubscriberDataAccess.getInstance().processUserDelete(userId);
		}
		return delete;
	}

	public int checkUserForReferences(String userId) throws SQLException
	{
		int references = 0;
		return references;
	}

	public void migrationsForUser(HttpServletRequest request, String oldUser, String newUserId) throws Exception
	{
	}

	public ElementData getUserWithId(String id) throws NumberFormatException, SQLException
	{
		return getUserWithId(Integer.parseInt(id));
	}

	public ElementData getUserWithId(int id) throws SQLException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		ElementData user = tma.getElementById(USER_MODULE, String.valueOf(id));
		if (user != null)
		{
			return user;
		}
		return null;
	}

	public Hashtable<String, ElementData> getCompanyUsers(int companyId) throws SQLException
	{
		return getCompanyUsers(companyId + "");
	}

	public Hashtable<String, ElementData> getCompanyUsers(String companyId) throws SQLException
	{
		Hashtable<String, ElementData> ht = new Hashtable<String, ElementData>();
		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
		List<CategoryData> teams = ma.getCategoriesByParentId(USER_MODULE, companyId);
		for (CategoryData team : teams)
		{
			List<ElementData> users = ma.getElementsByParentId(USER_MODULE, team.getId());
			for (ElementData user : users)
			{
				user.put("team", team);
				ht.put(user.getId(), user);
			}
		}
		return ht;
	}

	public CategoryData getUserCompanyForUserId(String userId) throws SQLException
	{
		String sql = "select c1.*  "
				+ " from categories_" + USER_MODULE + " c2, "
				+ " categories_" + USER_MODULE + " c1, "
				+ " elements_" + USER_MODULE + " e "
				+ " where e.category_id = c2.category_id "
				+ " and c1.category_id = c2.category_parentid "
				+ " and e.element_id = ?";
		List<CategoryData> result = TransactionModuleAccess.getInstance(txManager).getCategories(sql, userId);
		if (result.size() == 0)
		{
			return null;
		}
		return result.get(0);
	}

	public CategoryData getUserTeamForUserId(String userId) throws SQLException
	{
		String sql = "select c2.*  "
				+ " from categories_" + USER_MODULE + " c2, "
				+ " elements_" + USER_MODULE + " e "
				+ " where e.category_id = c2.category_id "
				+ " and e.element_id = ?";
		List<CategoryData> result = TransactionModuleAccess.getInstance(txManager).getCategories(sql, userId);
		if (result.size() == 0)
		{
			return null;
		}
		return result.get(0);
	}

	public int createTeam(String companyId, String teamName, List<String> productFolders, List<String> textFolders, List<String> imageFolders, List<String> videoFolders, List<String> pdfFolders,
			List<String> coverPagesFiles, List<String> templateFiles, boolean productSwitch, boolean textSwitch, boolean imageSwitch, boolean videoSwitch,
			boolean pdfSwitch, boolean coverPageSwitch, boolean templateSwitch,
			String itiStyle, String proposalTemplate, String arrivalGuideApiKey,
			boolean autoSendItinerary) throws NumberFormatException, SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		CategoryData searchOptions = new CategoryData();
		searchOptions.setName(teamName);
		searchOptions.setParentId(String.valueOf(Integer.parseInt(companyId)));
		CategoryData searchResult = tma.getCategory(USER_MODULE, searchOptions);

		String categoryId;
		if (searchResult == null)
		{

			CategoryData category = standardCreateCategoryHashtable(teamName);
			category.setParentId(String.valueOf(Integer.parseInt(companyId)));
			category.setFolderLevel(2);
			category.put(E_TEAM_PRODUCT_ACTIVE, productSwitch);
			category.put(E_TEAM_TEXT_ACTIVE, textSwitch);
			category.put(E_TEAM_IMAGE_ACTIVE, imageSwitch);
			category.put(E_TEAM_VIDEO_ACTIVE, videoSwitch);
			category.put(E_TEAM_VIDEO_ACTIVE, pdfSwitch);
			category.put(E_TEAM_VIDEO_ACTIVE, coverPageSwitch);
			category.put(E_TEAM_VIDEO_ACTIVE, templateSwitch);
			categoryId = String.valueOf(createCategory(category, USER_MODULE));
			if (productFolders != null || textFolders != null || imageFolders != null || videoFolders != null)
			{
				updateTeam(companyId, categoryId, teamName, productFolders, textFolders, imageFolders, videoFolders, pdfFolders, coverPagesFiles, templateFiles, productSwitch, textSwitch, imageSwitch, videoSwitch, pdfSwitch, coverPageSwitch, templateSwitch, itiStyle, proposalTemplate, autoSendItinerary, arrivalGuideApiKey);
			}
		}
		else
		{
			categoryId = searchResult.getId();
		}
		return Integer.parseInt(categoryId);

	}

	public void updateTeam(String companyId, String teamId, String teamName, List<String> productFolders, List<String> textFolders, List<String> imageFolders,
			List<String> videoFolders, List<String> pdfFolders, List<String> coverPagesFiles, List<String> templateFiles, boolean productSwitch, boolean textSwitch,
			boolean imageSwitch, boolean videoSwitch, boolean pdfSwitch, boolean coverPageSwitch, boolean templateSwitch,
			String itiStyle, String proposalTemplate, boolean autoSendItinerary, String arrivalGuideApiKey) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		//Hashtable<String, String> team = new Hashtable<String, String>();
		CategoryData team = tma.getCategoryById(USER_MODULE, teamId);
		MultiOptions productFolder = new MultiOptions(teamId, E_TEAM_PRODUCT_FOLDERS, USER_MODULE, team.getType(), productFolders);
		MultiOptions textFolder = new MultiOptions(teamId, E_TEAM_TEXT_FOLDERS, USER_MODULE, team.getType(), textFolders);
		MultiOptions imageFolder = new MultiOptions(teamId, E_TEAM_IMAGE_FOLDERS, USER_MODULE, team.getType(), imageFolders);
		MultiOptions videoFolder = new MultiOptions(teamId, E_TEAM_VIDEO_FOLDERS, USER_MODULE, team.getType(), videoFolders);
		MultiOptions pdfFolder = new MultiOptions(teamId, E_TEAM_PDF_FOLDERS, USER_MODULE, team.getType(), pdfFolders);
		MultiOptions coverPageFile = new MultiOptions(teamId, E_TEAM_COVERPAGE_FOLDERS, USER_MODULE, team.getType(), coverPagesFiles);
		MultiOptions templateFile = new MultiOptions(teamId, E_TEAM_TEMPLATE_FOLDERS, USER_MODULE, team.getType(), templateFiles);

		System.out.println(productSwitch);

		team.setName(teamName);
		team.put(E_TEAM_PRODUCT_FOLDERS, productFolder);
		team.put(E_TEAM_TEXT_FOLDERS, textFolder);
		team.put(E_TEAM_IMAGE_FOLDERS, imageFolder);
		team.put(E_TEAM_VIDEO_FOLDERS, videoFolder);
		team.put(E_TEAM_PDF_FOLDERS, pdfFolder);
		team.put(E_TEAM_COVERPAGE_FOLDERS, coverPageFile);
		team.put(E_TEAM_TEMPLATE_FOLDERS, templateFile);
		team.put(E_TEAM_PRODUCT_ACTIVE, productSwitch);
		team.put(E_TEAM_TEXT_ACTIVE, textSwitch);
		team.put(E_TEAM_IMAGE_ACTIVE, imageSwitch);
		team.put(E_TEAM_VIDEO_ACTIVE, videoSwitch);
		team.put(E_TEAM_PDF_ACTIVE, pdfSwitch);
		team.put(E_TEAM_COVERPAGE_ACTIVE, coverPageSwitch);
		team.put(E_TEAM_TEMPLATE_ACTIVE, templateSwitch);

		team.put(C1_PROPOSAL_TEMPLATE, proposalTemplate);
		team.put(C1_ITINERARY_TEMPLATE, itiStyle);
		team.put(C1_AUTO_SEND_ITINERARY, autoSendItinerary);
		team.put(C1_ARRIVALGUIDE_API, arrivalGuideApiKey);

		updateCategory(team, USER_MODULE);

	}

	public void updateSabreTeam(String companyId, String teamId, String teamName, String sabreAgencyCode, String sabrePassword,
			String itiStyle, String proposalTemplate, boolean autoSendItinerary, boolean autoDeleteItinerary, String noOfMonths) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		CategoryData team = tma.getCategoryById(USER_MODULE, teamId);
		team.setName(teamName);

		team.put(C1_PROPOSAL_TEMPLATE, proposalTemplate);
		team.put(C1_ITINERARY_TEMPLATE, itiStyle);
		team.put(C1_AUTO_SEND_ITINERARY, autoSendItinerary);

		updateCategory(team, USER_MODULE);

		if (StringUtils.isNotBlank(sabreAgencyCode) || StringUtils.isNotBlank(sabrePassword))
		{
//			if (MarketSubscriberDataAccess.getInstance().checkAppAccess(team.getParentId(), Market.QCSABRE))
//			{
//				new SabreDataAccess().updateSabreCredentialsForTeam(companyId, teamId, teamName, sabreAgencyCode, sabrePassword, autoDeleteItinerary, noOfMonths);
//			}
		}
	}

	public void updateTravelportTeam(String companyId, String teamId, String teamName, String travelportBranchCode, String travelportPassword,
			String itiStyle, String proposalTemplate, boolean autoSendItinerary, boolean autoDeleteItinerary, String noOfMonths) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		CategoryData team = tma.getCategoryById(USER_MODULE, teamId);
		team.setName(teamName);

		team.put(C1_PROPOSAL_TEMPLATE, proposalTemplate);
		team.put(C1_ITINERARY_TEMPLATE, itiStyle);
		team.put(C1_AUTO_SEND_ITINERARY, autoSendItinerary);

		updateCategory(team, USER_MODULE);

		if (StringUtils.isNotBlank(travelportBranchCode) || StringUtils.isNotBlank(travelportPassword))
		{
//			if (MarketSubscriberDataAccess.getInstance().checkAppAccess(team.getParentId(), Market.QCTRAVELPORT))
//			{
//				new TravelportDataAccess().updateTravelportCredentialsForTeam(companyId, teamId, team.getName(), travelportBranchCode, travelportPassword, autoDeleteItinerary, noOfMonths);
//			}
		}
	}

	public void updateStripeTeam(String companyId, String teamId, String stripePK, String stripeSK) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		CategoryData team = tma.getCategoryById(USER_MODULE, teamId);

		team.put(C2_STRIPE_PK, stripePK);
		team.put(C2_STRIPE_SK, stripeSK);

		updateCategory(team, USER_MODULE);
	}

	public void addFolderToTeam(CategoryData team, String libraryName, String libraryFolder, List<CategoryData> libraryFolders) throws SQLException, IOException
	{


		System.out.println(libraryFolders.size());
		List<String> teamlibraryFoldersIds = new ArrayList<String>();
		String libraryType = TUserAccountDataAccess.libraryDBRef(libraryName);
		MultiOptions activeLibrary = ModuleAccess.getInstance().getMultiOptions(team, libraryType, USER_MODULE);
		for (CategoryData folder : libraryFolders)
		{
			if (StringUtils.isNotBlank(team.getId()))
			{
				for (String active : activeLibrary.getValues())
				{
					if (active.equals(folder.getId()))
					{
						teamlibraryFoldersIds.add(folder.getId());
					}
				}
			}
		}
		teamlibraryFoldersIds.add(libraryFolder);
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		MultiOptions folder = new MultiOptions(team.getId(), libraryType, USER_MODULE, team.getType(), teamlibraryFoldersIds);

		team.put(E_TEAM_PRODUCT_FOLDERS, folder);

		updateCategory(team, USER_MODULE);
	}

	public int deleteTeam(String teamId) throws Exception
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		int deleted = tma.deleteCategoryById(USER_MODULE, teamId);
		return deleted;
	}

	public List<CategoryData> getCompanyTeams(int companyId) throws SQLException
	{
		return getCompanyTeams(String.valueOf(companyId));
	}

	public List<CategoryData> getCompanyTeams(String companyId) throws SQLException
	{
		List<CategoryData> list = new ArrayList<CategoryData>();
		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
		list = ma.getCategoriesByParentId(USER_MODULE, companyId);
		if (list.size() > 0)
		{
			return list;
		}
		return null;
	}

	public int checkTeamForReferences(String teamId) throws Exception
	{
		int references = 0;
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		List<ElementData> users = tma.getElementsByParentId(USER_MODULE, teamId);
		references += users.size();
		return references;
	}

	public void migrationsForTeam(String oldTeam, String newTeam) throws Exception
	{
		TransactionDataAccess tda = TransactionDataAccess.getInstance(txManager);
		QueryBuffer qb = new QueryBuffer();
		qb.append("UPDATE elements_useraccounts SET Category_id = ? WHERE Category_id = ?", newTeam, oldTeam);
		tda.update(qb);
	}

	public CategoryData getTeamForTeamId(String teamId) throws SQLException
	{
		CategoryData categoryData = null;
		categoryData = TransactionModuleAccess.getInstance(txManager).getCategoryById(USER_MODULE, teamId);
		if (categoryData == null || categoryData.getFolderLevel() != 2)
		{
			return null;
		}

		return categoryData;
	}

	public CategoryData getCompanyWithId(int id) throws SQLException
	{
		return getCompanyWithId(Integer.toString(id));
	}

	public CategoryData getCompanyWithId(String id) throws SQLException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		CategoryData company = tma.getCategoryById(USER_MODULE, id);
		if (company != null)
		{
			return company;
		}
		return null;
	}

	public int getUserCountInCompanyWithCompanyId(String levelOneCategoryId) throws SQLException
	{
		TransactionDataAccess tda = TransactionDataAccess.getInstance(txManager);
		String userCount = tda.selectString(
				"select count(*) from elements_useraccounts e "
						+ " join categories_users c2 on e.category_id = c2.category_id "
						+ " where c2.category_parentid = ? ",
				new String[] { levelOneCategoryId });

		return Integer.valueOf(userCount);
	}

	public void updateCompanySettings(final CategoryData company) throws SQLException, IOException
	{
		updateCompanySettings(company, null);
	}

	public void updateCompanySettings(final CategoryData company, final ServletContext servletContext) throws SQLException, IOException
	{
		updateCategory(company, USER_MODULE);
	}


	public CategoryData getCompanyForCompanyId(String companyId)
	{
		CategoryData categoryData = null;
		try
		{
			categoryData = TransactionModuleAccess.getInstance(txManager).getCategoryById(USER_MODULE, companyId);
			if (categoryData.getFolderLevel() != 1)
			{
				return null;
			}
		}
		catch (SQLException e)
		{
			logger.fatal("Error:", e);

		}
		return categoryData;
	}

	/**
	 * @param company
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public boolean regenerateApiKeysWithCompanyId(CategoryData company) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);

		company.put(UserAccountDataAccess.C1_API_ID, generateApiId());
		company.put(UserAccountDataAccess.C1_API_KEY, generateApiKey());
		int result = tma.updateCategory(company, USER_MODULE);
		return 1 == result;
	}

	public boolean updateCompanySystemSettings(CategoryData company, String nonReturnExpiry, String returnExpiry) throws SQLException, IOException
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);

		CategoryData companyUpdate = new CategoryData();
		companyUpdate.setId(company.getId());
		companyUpdate.put(UserAccountDataAccess.C1_DELETE_ITINERARY_SINGLE, nonReturnExpiry);
		companyUpdate.put(UserAccountDataAccess.C1_DELETE_ITINERARY_RETURN, returnExpiry);
		int result = tma.updateCategory(companyUpdate, USER_MODULE);
		return 1 == result;
	}

	public static String generateApiId()
	{
		return UUID.randomUUID().toString();
	}

	public static String generateApiKey()
	{
		return UUID.randomUUID().toString();
	}

	public List<ElementData> getUsers(String... users) throws SQLException
	{
		TransactionDataAccess tda = TransactionDataAccess.getInstance(txManager);
		QueryBuffer qb = new QueryBuffer();

		qb.append(" SELECT * from elements_useraccounts ");
		qb.appendIn(" where Element_id in ", Arrays.asList(users));
		qb.append(" order by ATTR_name, ATTR_surname");

		return tda.selectMaps(qb, new ElementDataMapper());
	}

	public String[] getUserAccessRights(String userID) throws SQLException
	{
		Set<String> matches = getUserAccessRightsSet(userID);
		return matches.toArray(new String[] {});
	}

	public Set<String> getUserAccessRightsSet(String userID) throws SQLException
	{
		Set<String> matches = new HashSet<String>();
		TransactionDataAccess tda = TransactionDataAccess.getInstance(txManager);
		matches = getUserAccessRights(userID, tda, matches);
		return matches;
	}

	private Set<String> getUserAccessRights(String userId, TransactionDataAccess tda, Set<String> matches) throws SQLException
	{
		TUserRolesDataAccess urda = new TUserRolesDataAccess(txManager);
		UserPermissions up = urda.getPermissions(userId);

		Hashtable<String, ElementData> allUsers = getCompanyUsers(getUserCompanyForUserId(userId).getId());
		for (String user : allUsers.keySet())
		{
			matches.add(user);
		}

		return matches;
	}

	public String getProposalStyles(String userId) throws Exception
	{
		CategoryData company = getUserCompanyForUserId(userId);

		StoreUtils storeUtils = new StoreUtils();
		String jsonFile = storeUtils.readFieldFromStores(company.getStoreFileUrl(C1_PROPOSAL_PRICING_STYLES));
		return jsonFile;
	}

	public void saveProposalStyles(String userId, String proposalStyle) throws Exception
	{
		TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
		CategoryData company = getUserCompanyForUserId(userId);

		TextFile textFile = new TextFile(proposalStyle, USER_MODULE, C1_PROPOSAL_PRICING_STYLES, company.getId(), true);
		company.put(C1_PROPOSAL_PRICING_STYLES, textFile);
		tma.updateCategory(company, USER_MODULE);

	}


	public int countUsers(String companyId) throws SQLException
	{
		TransactionDataAccess tda = TransactionDataAccess.getInstance(txManager);
		QueryBuffer qb = new QueryBuffer();
		qb.append("select count(*) from elements_useraccounts e join categories_useraccounts c2 on e.category_id = c2.category_id where c2.category_parentid = ? ", companyId);
		String userCount = tda.selectString(qb);
		return Integer.valueOf(userCount);
	}
}
