package au.corporateinteractive.qcloud.proposalbuilder.filter;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.oreilly.servlet.MultipartRequest;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;

public class OperationAuditFilter implements Filter
{
	private static Logger logger = Logger.getLogger(OperationAuditFilter.class);

	private static String[] forceURIs = new String[] { "/api/free/logout" };
	private static String[] exemptURIs = new String[] {};

	@Override
	public void init(FilterConfig arg0) throws ServletException
	{
		// Nothing to do...
	}

	@Override
	public void destroy()
	{
		// Nothing to do...
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException,
			ServletException
	{
		HttpServletRequest httpReq = (HttpServletRequest) req;
		ResettableStreamHttpServletRequest resetReq = new ResettableStreamHttpServletRequest(httpReq);
		String uri = httpReq.getRequestURI();
		if (!forceAudit(uri) && (!isPost(httpReq) || exempt(uri)))
		{
			chain.doFilter(resetReq, resp);
			return;
		}
		Map<String, String> requestData = parseRequest(resetReq);
		HttpSession session = httpReq.getSession();
		String userName = (String) session.getAttribute(Constants.PORTAL_USERNAME_ATTR);
		userName = userName == null ? "" : userName;
		String sessionID = session.getId();
		String clientIp = getClientIp(httpReq);
		try
		{
			chain.doFilter(resetReq, resp);
		}
		finally
		{
			try
			{
				logger.info("Auditing request begin");
				HttpServletResponse response = (HttpServletResponse) resp;
				String respCode = String.valueOf(response.getStatus());
				QueryBuffer sql = new QueryBuffer();
				sql.append("insert into td_oper_audit (op_url, op_data, op_date_time, op_user, op_ip, op_session_id, op_resp_code) values (?, ?, now(), ?, ?, ?, ?)", uri, new Gson().toJson(requestData), userName, clientIp, sessionID, respCode);
				TransactionDataAccess.getInstance().insert(sql);
				logger.info("Auditing request end");
			}
			catch (Exception e)
			{
				logger.error("", e);
			}
		}
	}

	private String getClientIp(HttpServletRequest httpReq)
	{
		String clientIp = httpReq.getHeader("X-Forwarded-For");
		if (StringUtils.isBlank(clientIp))
		{
			clientIp = httpReq.getRemoteAddr();
		}
		if (StringUtils.equals("0:0:0:0:0:0:0:1", clientIp))
		{
			clientIp = "127.0.0.1";
		}
		return clientIp;
	}

	private boolean forceAudit(String uri)
	{
		return ArrayUtils.contains(forceURIs, uri);
	}

	private boolean isPost(HttpServletRequest httpReq)
	{
		return StringUtils.equalsIgnoreCase("POST", httpReq.getMethod());
	}

	private boolean exempt(String uri)
	{
		return ArrayUtils.contains(exemptURIs, uri);
	}

	private Map<String, String> parseRequest(ResettableStreamHttpServletRequest httpReq)
	{
		Map<String, String> request = new HashMap<String, String>();
		try
		{
			MultipartRequest r = new MultipartRequest(httpReq, System.getProperty("java.io.tmpdir"), Constants.MAX_ATTACHMENT_SIZE);
			Enumeration<String> names = r.getParameterNames();
			while (names.hasMoreElements())
			{
				String name = names.nextElement();
				request.put(name, r.getParameter(name));
			}
		}
		catch (IOException e)
		{
			Enumeration<String> names = httpReq.getParameterNames();
			while (names.hasMoreElements())
			{
				String name = names.nextElement();
				request.put(name, httpReq.getParameter(name));
			}
		}
		httpReq.resetInputStream();
		return request;
	}

}
