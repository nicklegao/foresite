package au.corporateinteractive.qcloud.proposalbuilder.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.net.webdirector.common.datalayer.client.CategoryData;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;

public class ProposalAPIFilter implements Filter
{
	private static final Logger logger = Logger.getLogger(ProposalAPIFilter.class);

	@Override
	public void init(FilterConfig arg0) throws ServletException
	{
//	    exempt.add("/api/register");
//	    exempt.add("/api/activate");
//		exempt.add("/api/logging");
//		exempt.add("/api/login");
//		exempt.add("/api/changePassword");
//		exempt.add("/api/logout");
//		exempt.add("/api/storeCodes");
	}

	@Override
	public void destroy()
	{
		// Nothing to do...
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException
	{
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		HttpSession session = request.getSession();

		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		String mobileTestToken = "MOBILE1132345523838383949";
		String mobileToken = ((HttpServletRequest) req).getHeader("mobileToken");
		String authorization = request.getHeader("Authorization");
		if (authorization != null && authorization.startsWith("Bearer"))
		{
			mobileToken = authorization.substring("Bearer".length()).trim();
		}

		response.addHeader("Access-Control-Allow-Origin", "*");

		if (mobileToken == null) {
			mobileToken = "";
		}

		if (isExempt(request) || userId != null)
		{
			chain.doFilter(req, resp);
		}
		else if (StringUtils.isNotBlank(mobileToken))
		{
			try
			{
				Jws<Claims> claims = Jwts.parser().setSigningKey(Constants.TOKEN_KEY).parseClaimsJws(mobileToken);
				String id = (String) claims.getBody().get("userId");
				UserAccountDataAccess uada = new UserAccountDataAccess();
				CategoryData company = uada.getUserCompanyForUserId(id);
				String email = (String) claims.getBody().get("userEmail");
				session.setAttribute(Constants.PORTAL_USERNAME_ATTR, email);
				session.setAttribute(Constants.PORTAL_USER_ID_ATTR, id);
				session.setAttribute(Constants.PORTAL_COMPANY_ID_ATTR, company.getId());
				chain.doFilter(req, resp);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			loginRedirect(request, response);
		}

		response.setHeader("session-lifetime", Integer.toString(session.getMaxInactiveInterval()));
	}

	private boolean isExempt(HttpServletRequest request)
	{
		String uri = request.getRequestURI();
		return uri.startsWith("/api/free/") || uri.startsWith("/api/secure/") || uri.startsWith("/api/webdirector/")
				|| uri.startsWith("/api/v1/") || uri.startsWith("/api/mobile/setupProposalChatroom") || uri.startsWith("/api/mobile/getAllMessagesWeb") || uri.startsWith("/api/mobile/getMessage")
				|| uri.startsWith("/api/mobile/sendMessage") || uri.startsWith("/api/mobilpre/getMessage") || uri.startsWith("/api/mobile/sendEmailNotification");
	}

	private void loginRedirect(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		logger.info("Blocking unauthenticated API request from: " + request.getRequestURI());
		response.setStatus(401); //I'm a teapot. Setting this status code to avoid error email sending when invalid access happens
	}
}
