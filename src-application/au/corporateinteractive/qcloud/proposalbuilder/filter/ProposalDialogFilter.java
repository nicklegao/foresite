package au.corporateinteractive.qcloud.proposalbuilder.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;

public class ProposalDialogFilter implements Filter {
	private static final Logger logger = Logger.getLogger(ProposalDialogFilter.class); 
	
	public void init(FilterConfig arg0) throws ServletException {
		// Nothing to do...
	}

	public void destroy() {
		// Nothing to do...
	}

	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest )req;
		HttpServletResponse response = (HttpServletResponse)resp;
		HttpSession session = request.getSession();
		
		String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		
		if (userId == null)
		{
			loginRedirect(request, response, session);
		}
		else
		{
			chain.doFilter(req, resp);
		}
	}
	
	private void loginRedirect(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, ServletException {
		logger.info("Redirecting unauthenticated dialog request for: "+request.getRequestURI());
		response.setStatus(401);
	}
}
