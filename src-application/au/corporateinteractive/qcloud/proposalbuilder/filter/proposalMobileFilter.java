package au.corporateinteractive.qcloud.proposalbuilder.filter;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by coreybaines on 1/7/17.
 */
public class proposalMobileFilter implements Filter
{
    private static final Logger logger = Logger.getLogger(ProposalAPIFilter.class);

    @Override
    public void init(FilterConfig arg0) throws ServletException
    {
    }

    @Override
    public void destroy()
    {
        // Nothing to do...
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException
    {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpSession session = request.getSession();

        String mobileId = (String) ((HttpServletRequest) req).getHeader(Constants.PORTAL_USER_MOBILE_ID_ATTR); //  session.getAttribute(Constants.PORTAL_USER_MOBILE_ID_ATTR);

        response.addHeader("Access-Control-Allow-Origin", "*");

        if (isExempt(request) || mobileId != null)
        {
            chain.doFilter(req, resp);
        }
        else
        {
            loginRedirect(request, response);
        }
    }

    private boolean isExempt(HttpServletRequest request)
    {
        String uri = request.getRequestURI();
        return uri.startsWith("/mobileapi/free/");
    }

    private void loginRedirect(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        logger.info("Blocking unauthenticated API request from: " + request.getRequestURI());
        response.setStatus(401); //I'm a teapot. Setting this status code to avoid error email sending when invalid access happens
    }
}
