package au.corporateinteractive.qcloud.proposalbuilder.filter;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import au.corporateinteractive.qcloud.proposalbuilder.Constants;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserRolesDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions;

public class ProposalPortalFilter implements Filter
{
	private static final Logger logger = Logger.getLogger(ProposalPortalFilter.class);

	@Override
	public void init(FilterConfig arg0) throws ServletException
	{
		// Nothing to do...
	}

	@Override
	public void destroy()
	{
		// Nothing to do...
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException
	{
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		HttpSession session = request.getSession();
		UserRolesDataAccess urda = new UserRolesDataAccess();

		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

		if (userId == null)
		{
			loginRedirect(request, response, session);
		}
		else
		{
			//filter other pages in the portal: sales forecast, analytics
			UserPermissions up = null;
			try
			{
				up = urda.getPermissions(userId);

				boolean ok = true;

				//ideally, this if should be a switch with cases for each permission verification
				if ("/analytics".equals(request.getRequestURI()))
					ok = false;

				if (ok)
					chain.doFilter(req, resp);
				else
				{
					logger.info("Blocking unauthenticated API request from: " + request.getRequestURI());
					response.setStatus(401); //I'm a teapot. Setting this status code to avoid error email sending when invalid access happens
					request.getRequestDispatcher("/application/errorpage.jsp").forward(request, response);
				}
			}
			catch (Exception ex)
			{
				logger.error("Error getting permissions for user " + userId, ex);
			}
		}
	}

	static void loginRedirect(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, ServletException
	{
		logger.info("Redirecting unauthenticated request from: " + request.getRequestURI() + " to TravelDocs Portal.");
		String query = "";
		if (request.getQueryString() != null)
		{
			query = "?" + request.getQueryString();
		}
		response.sendRedirect("/?" + Constants.PORTAL_LOGIN_REDIRECT_PARAM + "=" + URLEncoder.encode(request.getRequestURI() + query, "UTF-8"));
	}
}
