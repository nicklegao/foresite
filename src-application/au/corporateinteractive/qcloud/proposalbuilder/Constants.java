package au.corporateinteractive.qcloud.proposalbuilder;

import au.net.webdirector.common.Defaults;

public class Constants
{

	public static final String PORTAL_USERNAME_ATTR = "portalUsername";

	public static final String PORTAL_USER_ID_ATTR = "portalUserId";

	public static final String PORTAL_COMPANY_ID_ATTR = "portalCompanyId";

	public static final String PORTAL_USER_MOBILE_ID_ATTR = "portalUserMobileId";

	public static final String PORTAL_SSO_USER_ID_ATTR = "PORTAL_SSO_USER_ID_ATTR";

	public static final String PORTAL_SSO_SESSION_ID_ATTR = "PORTAL_SSO_SESSION_ID_ATTR";

	public static final String PORTAL_CHANGE_PASSWORD_ATTR = "portalChangePassword";

	public static final String PORTAL_LOGIN_REDIRECT_PARAM = "rditedirect";

	public static final String PORTAL_ALLOW_SEND_ATTR = "true";

	public static final String LOGIN_ERROR = "loginError";

	public static final String VERIFICATION_ERROR = "verificationError";

	public static final String LOGIN_STATUS = "loginStatus";

	public static final String LOGIN_ERROR_USERNAME = "loginErrorUsername";

	public static final String LOG4J_LOGS = "log4j Logging";

	public static final String SESSION_CHANGE_PASSWORD_ERROR_MSG = "changePasswordErrorMessage";

	public static final String PRINCING_TEMPLATE_PATH = Defaults.getInstance().getWebDir() + "/application/pdf/section/plan/style";

	public static final String TOKEN_KEY = "[B@4a931de2";

	public static final int MAX_ATTACHMENT_SIZE = 1000 * 1000 * 12;//MB
	public static final int MAX_PDF_SIZE = 1000 * 1000 * 12;//MB

	public static final String UPLOADED_IMAGES_FOLDER_NAME = "New Folder";

	public static final String LOGIN_AS = "loginAs";

	public static final String SCHEDULE = "System Schedule";


}
