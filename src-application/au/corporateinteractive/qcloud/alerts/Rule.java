package au.corporateinteractive.qcloud.alerts;

import java.util.Map;

public class Rule
{
	public String queryRule;
	public Map<Integer, Integer> dateRule;
	private String emailAlert;
	private String templateName;
	private String emailTitle;
	private boolean sendOnce;

	public String getQueryRule()
	{
		return queryRule;
	}

	public void setQueryRule(String queryRule)
	{
		this.queryRule = queryRule;
	}

	public Map<Integer, Integer> getDateRule()
	{
		return dateRule;
	}

	public void setDateRule(Map<Integer, Integer> dateRule)
	{
		this.dateRule = dateRule;
	}

	public String getEmailAlert()
	{
		return emailAlert;
	}

	public void setEmailAlert(String emailAlert)
	{
		this.emailAlert = emailAlert;
	}

	public String getTemplateName()
	{
		return templateName;
	}

	public void setTemplateName(String templateName)
	{
		this.templateName = templateName;
	}

	public String getEmailTitle()
	{
		return emailTitle;
	}

	public void setEmailTitle(String emailTitle)
	{
		this.emailTitle = emailTitle;
	}

	public boolean isSendOnce()
	{
		return sendOnce;
	}

	public void setSendOnce(boolean sendOnce)
	{
		this.sendOnce = sendOnce;
	}
}
