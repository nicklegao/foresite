package au.corporateinteractive.qcloud.alerts;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import au.corporateinteractive.qcloud.proposalbuilder.db.ScheduledEmailsLogDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.util.text.DefaultDateFormat;
import au.net.webdirector.common.utils.email.PendingEmail;
import au.net.webdirector.common.utils.module.StoreUtils;
import sbe.ShortUrls.ShortUrlManager;
import webdirector.db.client.ClientFileUtils;

public class Rules
{
	public static boolean checkRule(ElementData rule)
	{
		Calendar calendar = Calendar.getInstance();
		int days = rule.getInt("ATTRINTEGER_dayoffset");
		calendar.add(Calendar.DATE, days);
		ClientFileUtils cf = new ClientFileUtils();
		String query = new StoreUtils().readFieldFromStores(rule.getString("ATTRTEXT_query"));
		QueryBuffer qb = new QueryBuffer();
		qb.append(query, DefaultDateFormat.formatDate(calendar.getTime()));
//		if (rule.getBoolean("ATTRCHECK_sendToCompany"))
//		{
//			qb.append(" AND ATTRDROP_userType = 'admin'");
//		}
		try
		{
			TransactionDataAccess tda = TransactionDataAccess.getInstance();
			List<DataMap> results = tda.selectMaps(qb);
			sendEmails(rule, results);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}

	public static boolean sendEmails(ElementData rule, List<DataMap> users)
	{
		try
		{
			for (DataMap user : users)
			{
				UserAccountDataAccess uada = new UserAccountDataAccess();
				CategoryData company = uada.getUserCompanyForUserId(user.getString("element_id"));
				Map<String, Object> additionalFields = new HashMap<String, Object>();
				additionalFields.put("company", company);
				additionalFields.put("user", user);
				additionalFields.put("time", Math.abs(rule.getInt("ATTRINTEGER_dayoffset")));
				EmailBuilder emailBuilder = new EmailBuilder();
				emailBuilder.prepareEmailAlert(rule, additionalFields);
				String id = rule.getBoolean("ATTRCHECK_sendToCompany") ? company.getId() : user.getString("element_id");
				if (canSendAlert(rule.getId(), id, rule.getBoolean("ATTRCHECK_sendOnce"), rule.getBoolean("ATTRCHECK_sendToCompany")))
				{
					PendingEmail email = emailBuilder.prepareEmailAlert(rule, additionalFields);

					email.addTo(user.getString("ATTR_Headline"));
					boolean sent = email.doSend();
					ScheduledEmailsLogDataAccess selda = new ScheduledEmailsLogDataAccess();
					selda.insertEmailLog(user.getString("ATTR_Headline"), user.getString("element_id"), ShortUrlManager.friendly(rule.getName()), rule.getId(), new Date(), company.getId(), company.getString("ATTR_categoryName"), sent);
				}
			}
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}

	private static boolean canSendAlert(String ruleId, String id, boolean sendOnce, boolean companyLevel)
	{
		if (sendOnce)
		{
			QueryBuffer sentCheck = new QueryBuffer();
			sentCheck.append("select * from elements_" + ScheduledEmailsLogDataAccess.MODULE);
			sentCheck.append(" WHERE " + ScheduledEmailsLogDataAccess.E_RULE_TYPE + " = ? ", ruleId);
			if (companyLevel)
			{
				sentCheck.append(" AND " + ScheduledEmailsLogDataAccess.E_SENT_TO_COMPANY + " = ? ", id);
			}
			else
			{
				sentCheck.append(" AND " + ScheduledEmailsLogDataAccess.E_SENT_TO + " = ? ", id);
			}
			sentCheck.append(" AND " + ScheduledEmailsLogDataAccess.E_SENT + " = '1'");
			try
			{
				TransactionDataAccess tda = TransactionDataAccess.getInstance();
				List<DataMap> emails = tda.selectMaps(sentCheck);
				return emails.size() == 0;
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			return true;
		}
		return false;
	}

	public static void main(String[] args)
	{
		//checkRule(trialExpiryRule);
	}
}
