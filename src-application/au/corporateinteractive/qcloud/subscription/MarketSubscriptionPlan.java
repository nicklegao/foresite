//package au.corporateinteractive.qcloud.subscription;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import au.corporateinteractive.qcloud.proposalbuilder.db.MarketDataAccess;
//import au.net.webdirector.common.datalayer.client.ElementData;
//
///**
// * @author Sishir
// *
// */
//public class MarketSubscriptionPlan extends SubscriptionPlan
//{
//
//	public static final String MODULE = "MARKET_SUBSCRIPTIONS";
//
//	private boolean perUserMonthPriced = true;
//
//	public MarketSubscriptionPlan(ElementData appPlanData)
//	{
//		setId(appPlanData.getId());
//		setCid(appPlanData.getString("cId"));
//		setCountryCode(appPlanData.getString("countryCode"));
//		setCurrencyCode(appPlanData.getString("currencyCode"));
//		setCurrencySymbol(appPlanData.getString("currencySymbol"));
//		setCountry(appPlanData.getString("country"));
//		setName(appPlanData.getName());
//		setPrice(appPlanData.getDouble("price"));
//		setLineItemDescription(appPlanData.getString("invoiceLineDesc"));
//		setStripeId(appPlanData.getString("stripeId"));
//		setPerUserMonthPriced(appPlanData.getBoolean("ATTRCHECK_PerUserMonth"));
//	}
//
//	@Override
//	public Map<String, Object> toMap()
//	{
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put(E_HEADLINE, getName());
//		map.put(E_PRICE, getPrice());
//		map.put(E_GST, getGst());
//		map.put(E_INVOICE_LINE_DESC, getLineItemDescription());
//		map.put(C_CURRENCY_CODE, getCurrencyCode());
//		map.put(C_CURRENCY_SYMBOL, getCurrencySymbol());
//		map.put("QTY", getQuantity());
//		map.put("FEE", getPrice() * getQuantity());
//		return map;
//	}
//
//	public static MarketSubscriptionPlan getMarketAppPlan(String country, String appId)
//	{
//		ElementData appPlanData = new MarketDataAccess().getAppPlan(appId, country);
//		MarketSubscriptionPlan appPlan = new MarketSubscriptionPlan(appPlanData);
//		return appPlan;
//	}
//
//	public boolean isPerUserMonthPriced()
//	{
//		return perUserMonthPriced;
//	}
//
//	public void setPerUserMonthPriced(boolean perUserMonthPriced)
//	{
//		this.perUserMonthPriced = perUserMonthPriced;
//	}
//
//
//}
