/**
 * @author Jaysun Lee
 * @date 5 Jan 2016
 */
package au.corporateinteractive.qcloud.subscription;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.conf.ModuleAccessConfig;

public class SubscriptionPlan
{
	private static Logger logger = Logger.getLogger(SubscriptionPlan.class);

	public static final String TRIAL_PLAN = "TRIAL";

	public static final String PLAN_PER_USER = "ADDITIONAL FEE PER USER";

	public static final String PLAN_PER_DATA_USAGE = "ADDITIONAL DATA USAGE FEE";

	public static final String PLAN_PER_BOOKING_USAGE = "ADDITIONAL BOOKING USAGE FEE";

	public static final String PLAN_PER_CONSULTANT = "ADDITIONAL FEE PER CONSULTANT";
	/**
	 * <b>The internal module name</b>
	 */
	public static final String MODULE = "SUBSCRIPTIONS";


	/**
	 * <b>ATTR_categoryName</b><br/>
	 * Display: Folder Name
	 */
	public static final String C_CATEGORY_NAME = "ATTR_categoryName";

	/**
	 * <b>ATTR_Country</b><br/>
	 * Display: Country
	 */
	public static final String C_COUNTRY = "ATTR_Country";

	/**
	 * <b>ATTR_CurrencyCode</b><br/>
	 * Display: Currency Code
	 */
	public static final String C_CURRENCY_CODE = "ATTR_CurrencyCode";

	/**
	 * <b>ATTR_CurrencySymbol</b><br/>
	 * Display: Currency Symbol
	 */
	public static final String C_CURRENCY_SYMBOL = "ATTR_CurrencySymbol";


	/**
	 * <b>ATTRCURRENCY_GST</b><br/>
	 * Display: Gst
	 */
	public static final String E_GST = "ATTRCURRENCY_GST";

	/**
	 * <b>ATTRCURRENCY_price</b><br/>
	 * Display: Price
	 */
	public static final String E_PRICE = "ATTRCURRENCY_price";

	/**
	 * <b>ATTRCURRENCY_priceUK</b><br/>
	 * Display: Price Uk
	 */
	public static final String E_PRICE_UK = "ATTRCURRENCY_priceUK";

	/**
	 * <b>ATTRCURRENCY_priceUSD</b><br/>
	 * Display: Price Usd
	 */
	public static final String E_PRICE_USD = "ATTRCURRENCY_priceUSD";

	/**
	 * <b>ATTRFLOAT_DataLimit</b><br/>
	 * Display: Data Limit
	 */
	public static final String E_DATA_LIMIT = "ATTRFLOAT_DataLimit";

	/**
	 * <b>ATTRINTEGER_userLimit</b><br/>
	 * Display: User Limit
	 */
	public static final String E_USER_LIMIT = "ATTRINTEGER_userLimit";

	/**
	 * <b>ATTR_Headline</b><br/>
	 * Display: Asset Name
	 */
	public static final String E_HEADLINE = "ATTR_Headline";

	/**
	 * <b>ATTR_invoiceLineDesc</b><br/>
	 * Display: Invoice Line Desc
	 */
	public static final String E_INVOICE_LINE_DESC = "ATTR_invoiceLineDesc";

	public static final String E_STRIPE_ID = "ATTR_StripeID";

	private String id;
	private String cid;
	private String name;
	private int maxUsers;
	private double maxData;
	private double price;
	private double gst;
	private String country;
	private String countryCode;
	private String currencyCode;
	private String currencySymbol;
	private String lineItemDescription;
	private String stripeId;

	private int quantity = 0;

	public SubscriptionPlan(CategoryData countryData, ElementData elementData)
	{
		this.id = elementData.getId();
		this.cid = countryData.getId();
		this.countryCode = countryData.getName();
		this.currencyCode = countryData.getString(C_CURRENCY_CODE);
		this.currencySymbol = countryData.getString(C_CURRENCY_SYMBOL);
		this.country = countryData.getString(C_COUNTRY);

		this.name = elementData.getName();
		this.maxUsers = elementData.getInt(E_USER_LIMIT);
		this.maxData = elementData.getDouble(E_DATA_LIMIT);
		this.price = elementData.getDouble(E_PRICE);
		this.gst = elementData.getDouble(E_GST);
		this.lineItemDescription = elementData.getString(E_INVOICE_LINE_DESC);
		this.stripeId = elementData.getString(E_STRIPE_ID);
	}

	protected SubscriptionPlan()
	{
	}


	public Map<String, Object> toMap()
	{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(E_HEADLINE, this.name);
		map.put(E_USER_LIMIT, this.maxUsers);
		map.put(E_DATA_LIMIT, this.maxData);
		map.put(E_PRICE, this.price);
		map.put(E_GST, this.gst);
		map.put(E_INVOICE_LINE_DESC, this.lineItemDescription);
		map.put("QTY", getQuantity());
		return map;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public void setCid(String cid)
	{
		this.cid = cid;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	public void setGst(double gst)
	{
		this.gst = gst;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public void setCountryCode(String countryCode)
	{
		this.countryCode = countryCode;
	}

	public void setCurrencyCode(String currencyCode)
	{
		this.currencyCode = currencyCode;
	}

	public void setCurrencySymbol(String currencySymbol)
	{
		this.currencySymbol = currencySymbol;
	}

	public void setLineItemDescription(String lineItemDescription)
	{
		this.lineItemDescription = lineItemDescription;
	}

	public String getName()
	{
		return this.name;
	}

	public int getMaxUsers()
	{
		return this.maxUsers;
	}

	public double getPrice()
	{
		return this.price;
	}

	public double getGst()
	{
		return this.gst;
	}

	public int getPriceCents()
	{
		return new BigDecimal(price * 100).setScale(0, BigDecimal.ROUND_HALF_DOWN).intValue();
	}

	public String getDescription()
	{
		return this.lineItemDescription;
	}

	public double getMaxData()
	{
		return maxData;
	}

	public String getCountry()
	{
		return country;
	}

	public String getCountryCode()
	{
		return countryCode;
	}

	public String getCurrencyCode()
	{
		return currencyCode;
	}

	public String getCurrencySymbol()
	{
		return currencySymbol;
	}

	public String getLineItemDescription()
	{
		return lineItemDescription;
	}

	public String getStripeId()
	{
		return stripeId;
	}

	public void setStripeId(String stripeId)
	{
		this.stripeId = stripeId;
	}

	public String getId()
	{
		return id;
	}

	public String getCid()
	{
		return cid;
	}

	public int getQuantity()
	{
		return Math.max(0, quantity);
	}

	public void setQuantity(int quantity)
	{
		this.quantity = quantity;
	}

	public boolean userLimitExceeded(int currentUserCount)
	{
		return currentUserCount >= this.getMaxUsers();
	}

	public boolean willExceedUserLimit(int currentUserCount)
	{
		return currentUserCount > this.getMaxUsers();
	}

	public static boolean isTrialPlan(String planName)
	{
		return StringUtils.equalsIgnoreCase(planName, TRIAL_PLAN);
	}

	public static List<String> getSupportCountries()
	{
		try
		{
			return TransactionDataAccess.getInstance().selectStrings("select distinct selected_value from drop_down_multi_selections where module_name = 'subscriptions' and attrmulti_name = 'attrmulti_supportedcountries' and module_type = 'category' order by 1");
		}
		catch (SQLException e)
		{
			return new ArrayList<String>();
		}
	}

	public static List<SubscriptionPlan> getCurrentSubscriptionPlans(String country)
	{
		ArrayList<SubscriptionPlan> plans = new ArrayList<SubscriptionPlan>();
		if (StringUtils.isBlank(country))
		{
			return plans;
		}
		ModuleAccess ma = ModuleAccess.getInstance();
		ma.setConfig(ModuleAccessConfig.logicalLive().merge(ModuleAccessConfig.orderByDisplay()));

		try
		{
			String categoryId = TransactionDataAccess.getInstance().selectString("select element_id from drop_down_multi_selections where module_name = 'subscriptions' and attrmulti_name = 'attrmulti_supportedcountries' and module_type = 'category' and selected_value = ? order by 1", country);
			CategoryData current = ma.getCategoryById(MODULE, categoryId);
			List<ElementData> elementDatas = ma.getElementsByParentId(MODULE, current.getId());
			for (ElementData ed : elementDatas)
			{
				SubscriptionPlan plan = new SubscriptionPlan(current, ed);
				plans.add(plan);
			}
		}
		catch (SQLException e)
		{
			logger.error("Error: ", e);
		}

		return plans;
	}

	public static List<SubscriptionPlan> getCurrentSubscriptionPlansWithoutTrial(String country)
	{
		List<SubscriptionPlan> plans = getCurrentSubscriptionPlans(country);
		SubscriptionPlan trial = getPlanByName(country, TRIAL_PLAN);
		plans.remove(trial);

		return plans;
	}

	public static SubscriptionPlan getPlanByName(String country, String name)
	{
		return getPlanByName(country, name, true);
	}

	private static SubscriptionPlan getPlanByName(String country, String name, boolean liveOnly)
	{
		ModuleAccess ma = ModuleAccess.getInstance();
		if (liveOnly)
			ma.setConfig(ModuleAccessConfig.logicalLive());

		try
		{
			String categoryId = TransactionDataAccess.getInstance().selectString("select element_id from drop_down_multi_selections where module_name = 'subscriptions' and attrmulti_name = 'attrmulti_supportedcountries' and module_type = 'category' and selected_value = ? order by 1", country);
			CategoryData current = ma.getCategoryById(MODULE, categoryId);
			ElementData condition = new ElementData();
			condition.setParentId(current.getId());
			condition.setName(name);
			ElementData ed = ma.getElement(MODULE, condition);
			SubscriptionPlan plan = new SubscriptionPlan(current, ed);
			return plan;
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
		}
		return null;
	}

	private static List<SubscriptionPlan> getPlansByName(String country, String name, boolean liveOnly)
	{
		ModuleAccess ma = ModuleAccess.getInstance();
		if (liveOnly)
			ma.setConfig(ModuleAccessConfig.logicalLive());

		try
		{
			String categoryId = TransactionDataAccess.getInstance().selectString("select element_id from drop_down_multi_selections where module_name = 'subscriptions' and attrmulti_name = 'attrmulti_supportedcountries' and module_type = 'category' and selected_value = ? order by 1", country);
			CategoryData current = ma.getCategoryById(MODULE, categoryId);
			ElementData condition = new ElementData();
			condition.setParentId(current.getId());
			condition.setName(name);

			List<ElementData> eds = ma.getElements(MODULE, condition);
			List<SubscriptionPlan> plans = new ArrayList<>();
			for (ElementData ed : eds)
			{
				plans.add(new SubscriptionPlan(current, ed));
			}
			return plans;
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
		}
		return null;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		final SubscriptionPlan other = (SubscriptionPlan) obj;

		if (StringUtils.equals(this.getName(), other.getName()))
			return true;
		else
			return false;
	}

	public static SubscriptionPlan getAdditionalUserPlan(String country)
	{
		return getPlanByName(country, PLAN_PER_USER, false);
	}

	public static SubscriptionPlan getAdditionalConsultantPlan(String country)
	{
		return getPlanByName(country, PLAN_PER_CONSULTANT, false);
	}

	public static SubscriptionPlan getAdditionalDataPlan(String country)
	{
		return getPlanByName(country, PLAN_PER_DATA_USAGE, false);
	}

	public static List<SubscriptionPlan> getBookingUsagePlan(String country)
	{
		List<SubscriptionPlan> plans = getPlansByName(country, PLAN_PER_BOOKING_USAGE, false);
		Collections.sort(plans, new Comparator<SubscriptionPlan>()
		{

			@Override
			public int compare(SubscriptionPlan o1, SubscriptionPlan o2)
			{
				return o1.getMaxData() < o2.getMaxData() ? -1 : o1.getMaxData() > o2.getMaxData() ? 1 : 0;
			}
		});
		return plans;
	}

	public static String getCurrencyCode(String country)
	{
		try
		{
			String sql = "select s.attr_currencycode from categories_subscriptions s,drop_down_multi_selections m " +
					"where m.module_name = 'subscriptions' " +
					"and m.attrmulti_name = 'attrmulti_supportedcountries' " +
					"and m.module_type = 'category' " +
					"and m.element_id = s.category_id " +
					"and m.selected_value = ? ";
			String currency = TransactionDataAccess.getInstance().selectString(sql, country);
			if (currency == null)
			{
				return "USD";
			}
			return currency;
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return "USD";
		}

	}
}
