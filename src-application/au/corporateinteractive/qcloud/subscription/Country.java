/**
 * 
 */
package au.corporateinteractive.qcloud.subscription;

/**
 * @author Jaysun
 *
 */
public enum Country
{
	AU,
	UK,
	INT
}