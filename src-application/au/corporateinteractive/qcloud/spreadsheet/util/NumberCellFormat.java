package au.corporateinteractive.qcloud.spreadsheet.util;

import java.text.DecimalFormat;

import org.apache.log4j.Logger;

public class NumberCellFormat extends CellFormat
{

//	 #,0.00 - 3.0 - java.lang.Double
//	 #.00 - 13.0 - java.lang.Double
//	 #,###.00 - 14.0 - java.lang.Double

	public NumberCellFormat(String format)
	{
		super(format);
	}

	private static Logger logger = Logger.getLogger(NumberCellFormat.class);

	@Override
	public String format(Object value)
	{
		return new DecimalFormat(format).format(value);
	}

	public static void main(String[] args)
	{
		System.out.println(new DecimalFormat("#,0.00").format(4.0d));
		System.out.println(new DecimalFormat("#.00").format(0.002d));
		System.out.println(new DecimalFormat("#,###.00").format(4.0d));
		System.out.println(new DecimalFormat("#").format(4.0d));
		System.out.println(new DecimalFormat("#.0").format(4.0d));
	}

}
