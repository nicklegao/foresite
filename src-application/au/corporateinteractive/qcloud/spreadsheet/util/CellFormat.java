package au.corporateinteractive.qcloud.spreadsheet.util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public abstract class CellFormat
{
	private static Logger logger = Logger.getLogger(CellFormat.class);

	String format;


	public CellFormat(String format)
	{
		super();
		this.format = format;
	}

	public static CellFormat getInstance(String format, Object value)
	{
		String type = getType(format, value);
		switch (type)
		{
			case "Percent":
				return new PercentCellFormat(format);
			case "Currency":
				return new CurrencyCellFormat(format);
			case "Number":
				return new NumberCellFormat(format);
			case "Duration":
				return new DurationCellFormat(format);
			case "Date":
				return new DateCellFormat(format);
			default:
				return new DefaultCellFormat(format);
		}
	}

	public abstract String format(Object value);

	public static String getType(String format, Object value)
	{
//		 0.00% - 4.0 - java.lang.Double
//		 #.00% - 11.0 - java.lang.Double
//		 #% - 12.0 - java.lang.Double
		if (StringUtils.contains(format, "%") && value instanceof Double)
		{
			return "Percent";
		}
//		 _("$"* #,##0.00_);_("$"* (#,##0.00);_("$"* "-"??_);_(@_) - 5.0 - java.lang.Double
//		 $#,##0.00;[Red]$#,##0.00 - 6.0 - java.lang.Double
//		 $?.00 - 21.0 - java.lang.Double
//		 $? - 23.0 - java.lang.Double
//		 "USD" ?.00 - 22.0 - java.lang.Double
		if ((StringUtils.contains(format, "$") || StringUtils.contains(format, "?")) && value instanceof Double)
		{
			return "Currency";
		}
//		 #,0.00 - 3.0 - java.lang.Double
//		 #.00 - 13.0 - java.lang.Double
//		 #,###.00 - 14.0 - java.lang.Double
		if (StringUtils.contains(format, "#") && value instanceof Double)
		{
			return "Number";
		}
//		 [h]:mm:ss - 10.0 - java.lang.Double
		if (StringUtils.contains(format, "[h]") && value instanceof Double)
		{
			return "Duration";
		}
//		 m/d/yyyy - 7.0 - java.lang.Double
//		 m/d/yyyy h:mm - 9.0 - java.lang.Double
//		 m/d/yyyy - 43087.0 - java.lang.Double
//		 dddd, mmmm dd, yyyy - 43087.0 - java.lang.Double
//		 dddd, mmmm dd, yyyy h:mm:ss AM/PM - 43087.0 - java.lang.Double
//		 m/d/yyyy h:mm AM/PM - 43087.0 - java.lang.Double
//		 m/d/yyyy h:mm:ss AM/PM - 43087.0 - java.lang.Double
//		 mmmm, yyyy - 43087.0 - java.lang.Double
//		 m/d/yyyy - 43087.0 - java.lang.Double
//		 h:mm:ss AM/PM - 8.0 - java.lang.Double
//		 mmmm dd - 43087.0 - java.lang.Double
//		 h:mm AM/PM - 43087.0 - java.lang.Double
//		 h:mm:ss AM/PM - 43087.0 - java.lang.Double
		if ((StringUtils.contains(format, "yyyy") || StringUtils.contains(format, "mm")) && value instanceof Double)
		{
			return "Date";
		}


//		 null - 1.0 - java.lang.Double
//		 @ - 2.0 - java.lang.Double
		return "Default";
	}

}
