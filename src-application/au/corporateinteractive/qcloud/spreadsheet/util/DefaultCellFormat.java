package au.corporateinteractive.qcloud.spreadsheet.util;

import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;

public class DefaultCellFormat extends CellFormat
{
	public DefaultCellFormat(String format)
	{
		super(format);
	}

	private static Logger logger = Logger.getLogger(DefaultCellFormat.class);

	@Override
	public String format(Object value)
	{
		if (value instanceof Double && (Double) value == ((Double) value).intValue())
		{
			return "" + ((Double) value).intValue();
		}
		return ESAPI.encoder().encodeForHTML(value.toString());
	}


}
