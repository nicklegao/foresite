package au.corporateinteractive.qcloud.spreadsheet.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.apache.log4j.Logger;

public class DurationCellFormat extends CellFormat
{

//	 [h]:mm:ss - 10.0 - java.lang.Double

	public DurationCellFormat(String format)
	{
		super(format);
	}

	private static Logger logger = Logger.getLogger(DurationCellFormat.class);

	@Override
	public String format(Object value)
	{
		int totalSeconds = (int) ((Double) value * 24 * 3600);
		int hours = totalSeconds / 3600;
		totalSeconds = totalSeconds % 3600;
		int mins = totalSeconds / 60;
		totalSeconds = totalSeconds % 60;
		NumberFormat nf = new DecimalFormat("00");
		return hours + ":" + nf.format(mins) + ":" + nf.format(totalSeconds);
	}


}
