package au.corporateinteractive.qcloud.spreadsheet.util;

import java.text.DecimalFormat;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class CurrencyCellFormat extends CellFormat
{
//	 $?.00 - 21.0 - java.lang.Double
//	 $? - 23.0 - java.lang.Double
//	 "USD" ?.00 - 22.0 - java.lang.Double

//	 _("$"* #,##0.00_);_("$"* (#,##0.00);_("$"* "-"??_);_(@_) - 5.0 - java.lang.Double
//	 $#,##0.00;[Red]$#,##0.00 - 6.0 - java.lang.Double

	public CurrencyCellFormat(String format)
	{
		super(format);

	}

	private static Logger logger = Logger.getLogger(CurrencyCellFormat.class);

	@Override
	public String format(Object value)
	{
		if (StringUtils.contains(format, "[Red]"))
		{
			String f = StringUtils.split(format, "[Red]")[0];
			String ret = "<span";
			if ((Double) value < 0)
			{
				ret += " style=\"color:red;\"";
			}
			ret += ">" + new DecimalFormat(f).format(Math.abs((Double) value)) + "</span>";
			return ret;
		}
		if (StringUtils.contains(format, ";"))
		{
			String f = (Double) value < 0 ? "$ (#,##0.00)" : "$ #,##0.00";
			return new DecimalFormat(f).format(value);
		}
		String f = StringUtils.replace(format, "?", "#");
		f = StringUtils.replace(f, "\"", "");
		return new DecimalFormat(f).format(value);
	}

	public static void main(String[] args)
	{
		System.out.println(new DecimalFormat("$#.00").format(4.0d));
		System.out.println(new DecimalFormat("$#").format(4.0d));
		System.out.println(new DecimalFormat("USD #.00").format(4.0d));
	}


}
