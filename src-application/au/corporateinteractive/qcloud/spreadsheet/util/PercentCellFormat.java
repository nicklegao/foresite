package au.corporateinteractive.qcloud.spreadsheet.util;

import java.text.DecimalFormat;

import org.apache.log4j.Logger;

public class PercentCellFormat extends CellFormat
{
//	 0.00% - 4.0 - java.lang.Double
//	 #.00% - 11.0 - java.lang.Double
//	 #% - 12.0 - java.lang.Double

	public PercentCellFormat(String format)
	{
		super(format);
	}

	private static Logger logger = Logger.getLogger(PercentCellFormat.class);

	@Override
	public String format(Object value)
	{
		return new DecimalFormat(format).format(value);
	}

	public static void main(String[] args)
	{
		System.out.println(new DecimalFormat("0.00%").format(4.0d));
		System.out.println(new DecimalFormat("#.00%").format(0.002d));
		System.out.println(new DecimalFormat("#%").format(4.0d));
	}
}
