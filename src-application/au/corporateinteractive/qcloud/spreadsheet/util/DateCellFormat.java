package au.corporateinteractive.qcloud.spreadsheet.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

public class DateCellFormat extends CellFormat
{

//	 m/d/yyyy - 7.0 - java.lang.Double
//	 m/d/yyyy h:mm - 9.0 - java.lang.Double
//	 m/d/yyyy - 43087.0 - java.lang.Double
//	 dddd, mmmm dd, yyyy - 43087.0 - java.lang.Double
//	 dddd, mmmm dd, yyyy h:mm:ss AM/PM - 43087.0 - java.lang.Double
//	 m/d/yyyy h:mm AM/PM - 43087.0 - java.lang.Double
//	 m/d/yyyy h:mm:ss AM/PM - 43087.0 - java.lang.Double
//	 mmmm, yyyy - 43087.0 - java.lang.Double
//	 m/d/yyyy - 43087.0 - java.lang.Double
//	 h:mm:ss AM/PM - 8.0 - java.lang.Double
//	 mmmm dd - 43087.0 - java.lang.Double
//	 h:mm AM/PM - 43087.0 - java.lang.Double
//	 h:mm:ss AM/PM - 43087.0 - java.lang.Double

	public DateCellFormat(String format)
	{
		super(format);
	}

	private static Logger logger = Logger.getLogger(DateCellFormat.class);

	@Override
	public String format(Object value)
	{
		String f = StringUtils.replace(format, "mmmm", "MMMM");
		f = StringUtils.replace(f, "mm", "ii");
		f = StringUtils.replace(f, "m", "M");
		f = StringUtils.replace(f, "ii", "mm");
		f = StringUtils.replace(f, "dddd", "EEEE");
		boolean am = StringUtils.contains(f, "AM/PM");
		f = StringUtils.replace(f, "h", am ? "h" : "H");
		f = StringUtils.replace(f, "AM/PM", "a");

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.truncate(new Date(), Calendar.DATE));
		c.set(Calendar.YEAR, 1899);
		c.set(Calendar.MONTH, 11);
		c.set(Calendar.DATE, 30);
		int daysInt = ((Double) value).intValue();
		BigDecimal decimal = new BigDecimal((Double) value).subtract(new BigDecimal(daysInt));
		BigDecimal seconds = decimal.multiply(new BigDecimal(24 * 3600));
		c.add(Calendar.DATE, daysInt);
		c.add(Calendar.SECOND, seconds.setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
		logger.info(f + ":" + value + ":" + c.getTime());
		return new SimpleDateFormat(f).format(c.getTime());
	}


}
