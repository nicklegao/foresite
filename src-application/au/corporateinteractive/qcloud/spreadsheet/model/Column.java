package au.corporateinteractive.qcloud.spreadsheet.model;

public class Column
{
	int index;
	double width;
	String hidden;

	public int getIndex()
	{
		return index;
	}

	public void setIndex(int index)
	{
		this.index = index;
	}

	public double getWidth()
	{
		return width;
	}

	public void setWidth(double width)
	{
		this.width = width;
	}

	public String getHidden()
	{
		return hidden;
	}

	public void setHidden(String hidden)
	{
		this.hidden = hidden;
	}
}
