package au.corporateinteractive.qcloud.spreadsheet.model;

import java.util.List;

public class Spreadsheet
{
	int columnWidth;
	int rowHeight;
	List<Sheet> sheets;

	public int getColumnWidth()
	{
		return columnWidth;
	}

	public void setColumnWidth(int columnWidth)
	{
		this.columnWidth = columnWidth;
	}

	public int getRowHeight()
	{
		return rowHeight;
	}

	public void setRowHeight(int rowHeight)
	{
		this.rowHeight = rowHeight;
	}

	public List<Sheet> getSheets()
	{
		return sheets;
	}

	public void setSheets(List<Sheet> sheets)
	{
		this.sheets = sheets;
	}

}
