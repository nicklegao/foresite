package au.corporateinteractive.qcloud.spreadsheet.model;

public class Border
{
	int size;
	String color;

	public int getSize()
	{
		return size;
	}

	public void setSize(int size)
	{
		this.size = size;
	}

	public String getColor()
	{
		return color;
	}

	public void setColor(String color)
	{
		this.color = color;
	}

}
