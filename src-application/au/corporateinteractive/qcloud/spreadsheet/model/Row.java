package au.corporateinteractive.qcloud.spreadsheet.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Row
{
	int index;

	double height;

	int printableCellsAmount;

	String hidden;

	List<Cell> cells;

	public String getHidden()
	{
		return hidden;
	}

	public void setHidden(String hidden)
	{
		this.hidden = hidden;
	}

	public double getHeight()
	{
		return height;
	}

	public void setHeight(double height)
	{
		this.height = height;
	}

	public int getIndex()
	{
		return index;
	}

	public void setIndex(int index)
	{
		this.index = index;
	}

	public List<Cell> getCells()
	{
		Collections.sort(this.cells, new Comparator<Cell>()
		{
			@Override
			public int compare(Cell o1, Cell o2)
			{
				return o1.getIndex() - o2.getIndex();
			}
		});
		return cells;
	}

	public List<Cell> getCells(int limit)
	{
		List<Cell> cells = getCells();
		if (cells.size() < limit)
		{
			return cells;
		}
		return cells.subList(0, limit);
	}


	public void setCells(List<Cell> cells)
	{
		this.cells = cells;
	}

	public int getPrintableCellsAmount()
	{
		return printableCellsAmount;
	}

	public int countPrintableCells()
	{
		this.printableCellsAmount = 0;
		List<Cell> cells = getCells();
		for (int i = 0; i < cells.size(); i++)
		{
			Cell cell = cells.get(i);
			if (cell.isPrintable())
			{
				//Plus 1 because we want size not index
				printableCellsAmount = Math.max((cell.getIndex() + 1), printableCellsAmount);
			}
		}
		return printableCellsAmount;
	}

}
