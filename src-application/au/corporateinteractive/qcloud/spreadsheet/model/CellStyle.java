package au.corporateinteractive.qcloud.spreadsheet.model;

public class CellStyle
{
	String fontFamily;
	int fontSize;

	public String getFontFamily()
	{
		return fontFamily;
	}

	public void setFontFamily(String fontFamily)
	{
		this.fontFamily = fontFamily;
	}

	public int getFontSize()
	{
		return fontSize;
	}

	public void setFontSize(int fontSize)
	{
		this.fontSize = fontSize;
	}

}
