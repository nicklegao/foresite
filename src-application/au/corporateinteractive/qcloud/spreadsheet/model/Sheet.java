package au.corporateinteractive.qcloud.spreadsheet.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Sheet
{
	String name;

	CellStyle defaultCellStyle;

	List<String> mergedCells;

	List<Column> columns;

	List<Row> rows;

	int printableColumnsAmount = 0;
	int printableRowsAmount = 0;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public CellStyle getDefaultCellStyle()
	{
		return defaultCellStyle;
	}

	public void setDefaultCellStyle(CellStyle defaultCellStyle)
	{
		this.defaultCellStyle = defaultCellStyle;
	}

	public List<String> getMergedCells()
	{
		return mergedCells;
	}

	public void setMergedCells(List<String> mergedCells)
	{
		this.mergedCells = mergedCells;
	}

	public List<Column> getColumns()
	{
		Collections.sort(this.columns, new Comparator<Column>()
		{
			@Override
			public int compare(Column o1, Column o2)
			{
				return o1.getIndex() - o2.getIndex();
			}

		});
		return columns;
	}

	public void setColumns(List<Column> columns)
	{
		this.columns = columns;
	}

	public List<Row> getRows()
	{
		Collections.sort(this.rows, new Comparator<Row>()
		{
			@Override
			public int compare(Row o1, Row o2)
			{
				return o1.getIndex() - o2.getIndex();
			}
		});
		return rows;
	}

	public void setRows(List<Row> rows)
	{
		this.rows = rows;
	}

	public Cell cellAt(int rowIndex, int columnIndex)
	{
		List<Row> rows = getRows();
		Row row = rows.get(rowIndex);
		return row.getCells().get(columnIndex);

	}

	public int getPrintableColumnsAmount()
	{
		return printableColumnsAmount;
	}

	public int getPrintableRowsAmount()
	{
		return printableRowsAmount;
	}

	public List<Row> printableRows()
	{
		printableRowsAmount = 0;
		printableColumnsAmount = 0;
		List<Row> rows = getRows();
		for (int i = 0; i < rows.size(); i++)
		{
			Row row = rows.get(i);
			int temp = row.countPrintableCells();
			printableColumnsAmount = Math.max(temp, printableColumnsAmount);
			if (temp > 0)
			{
				printableRowsAmount = i + 1;
			}
		}
		return rows.subList(0, printableRowsAmount);
	}

	public int countPrintableRows()
	{
		printableRowsAmount = 0;
		printableColumnsAmount = 0;
		List<Row> rows = getRows();
		for (int i = 0; i < rows.size(); i++)
		{
			Row row = rows.get(i);
			int temp = row.countPrintableCells();
			printableColumnsAmount = Math.max(temp, printableColumnsAmount);
			if (temp > 0)
			{
				printableRowsAmount = Math.max((row.getIndex() + 1), printableRowsAmount);
			}
		}
		return printableRowsAmount;
	}
}
