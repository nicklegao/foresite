package au.corporateinteractive.qcloud.spreadsheet.model;

import org.apache.commons.lang.StringUtils;
import org.owasp.esapi.ESAPI;

import au.corporateinteractive.qcloud.spreadsheet.util.CellFormat;

public class Cell
{
	int index;
	double width;

	String format;

	Object value;
	String color;
	String background;
	Border borderLeft;
	Border borderRight;
	Border borderTop;
	Border borderBottom;
	String textAlign;
	String verticalAlign;
	boolean wrap;

	boolean underline;
	boolean italic;
	boolean bold;

	int fontSize;

	boolean hidden;
	int colspan = 1;
	int rowspan = 1;

	public int getIndex()
	{
		return index;
	}

	public void setIndex(int index)
	{
		this.index = index;
	}

	public String getFormat()
	{
		return format;
	}

	public void setFormat(String format)
	{
		this.format = format;
	}

	public Object getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public Border getBorderLeft()
	{
		return borderLeft;
	}

	public void setBorderLeft(Border borderLeft)
	{
		this.borderLeft = borderLeft;
	}

	public Border getBorderRight()
	{
		return borderRight;
	}

	public void setBorderRight(Border borderRight)
	{
		this.borderRight = borderRight;
	}

	public Border getBorderTop()
	{
		return borderTop;
	}

	public void setBorderTop(Border borderTop)
	{
		this.borderTop = borderTop;
	}

	public Border getBorderBottom()
	{
		return borderBottom;
	}

	public void setBorderBottom(Border borderBottom)
	{
		this.borderBottom = borderBottom;
	}

	public boolean isHidden()
	{
		return hidden;
	}

	public void setHidden(boolean hidden)
	{
		this.hidden = hidden;
	}

	public int getColspan()
	{
		return colspan;
	}

	public void setColspan(int colspan)
	{
		this.colspan = colspan;
	}

	public int getRowspan()
	{
		return rowspan;
	}

	public void setRowspan(int rowspan)
	{
		this.rowspan = rowspan;
	}

	public String formatValue()
	{
		if (value == null)
		{
			return "";
		}
		return CellFormat.getInstance(format, value).format(value);
	}

	public String getColor()
	{
		return color;
	}

	public void setColor(String color)
	{
		this.color = color;
	}

	public String getBackground()
	{
		return background;
	}

	public void setBackground(String background)
	{
		this.background = background;
	}

	public String getTextAlign()
	{
		return textAlign;
	}

	public void setTextAlign(String textAlign)
	{
		this.textAlign = textAlign;
	}

	public String getVerticalAlign()
	{
		return verticalAlign;
	}

	public void setVerticalAlign(String verticalAlign)
	{
		this.verticalAlign = verticalAlign;
	}

	public boolean isWrap()
	{
		return wrap;
	}

	public void setWrap(boolean wrap)
	{
		this.wrap = wrap;
	}

	public boolean isUnderline()
	{
		return underline;
	}

	public void setUnderline(boolean underline)
	{
		this.underline = underline;
	}

	public boolean isItalic()
	{
		return italic;
	}

	public void setItalic(boolean italic)
	{
		this.italic = italic;
	}

	public boolean isBold()
	{
		return bold;
	}

	public void setBold(boolean bold)
	{
		this.bold = bold;
	}

	public int getFontSize()
	{
		return fontSize;
	}

	public void setFontSize(int fontSize)
	{
		this.fontSize = fontSize;
	}

	public double getWidth()
	{
		return width;
	}

	public void setWidth(double width)
	{
		this.width = width;
	}

	public boolean isPrintable()
	{
		if (value != null && StringUtils.isNotBlank(value.toString()))
		{
			return true;
		}
		if (StringUtils.isNotBlank(getBackground()))
		{
			return true;
		}
		if (getBorderTop() != null || getBorderRight() != null || getBorderBottom() != null || getBorderLeft() != null)
		{
			return true;
		}
		return false;
	}

	public String render(double columnWidth, double rowHeight)
	{
		if (this.isHidden())
		{
			return "";
		}
		StringBuffer sb = new StringBuffer();
		sb.append("<td");
		if (this.getColspan() > 1)
		{
			sb.append(" colspan=\"" + this.getColspan() + "\"");
		}
		if (this.getRowspan() > 1)
		{
			sb.append(" rowspan=\"" + this.getRowspan() + "\"");
		}
		sb.append(" data-format=\"" + ESAPI.encoder().encodeForHTMLAttribute(this.getFormat()) + "\"");
		sb.append(" style=\"");
		sb.append("height:" + rowHeight + "px;");
		if (this.getFontSize() > 0)
		{
			sb.append("font-size:" + this.getFontSize() + "px;");
		}
		if (!this.isWrap())
		{
			sb.append("white-space:nowrap;");
		}
		if (this.isBold())
		{
			sb.append("font-weight:bold;");
		}
		if (this.isItalic())
		{
			sb.append("font-style:italic;");
		}
		if (StringUtils.isNotBlank(this.getBackground()))
		{
			sb.append("background-color:" + this.getBackground() + ";");
		}
		if (StringUtils.isNotBlank(this.getColor()))
		{
			sb.append("color:" + this.getColor() + ";");
		}
		if (StringUtils.isNotBlank(this.getVerticalAlign()))
		{
			sb.append("vertical-align:" + this.getVerticalAlign() + ";");
		}
		else
		{
			sb.append("vertical-align:bottom;");
		}
		if (this.getBorderTop() != null)
		{
			sb.append("border-top:solid " + this.getBorderTop().getSize() + "px " + this.getBorderTop().getColor() + ";");
		}
		if (this.getBorderRight() != null)
		{
			sb.append("border-right:solid " + this.getBorderRight().getSize() + "px " + this.getBorderRight().getColor() + ";");
		}
		if (this.getBorderBottom() != null)
		{
			sb.append("border-bottom:solid " + this.getBorderBottom().getSize() + "px " + this.getBorderBottom().getColor() + ";");
		}
		if (this.getBorderLeft() != null)
		{
			sb.append("border-left:solid " + this.getBorderLeft().getSize() + "px " + this.getBorderLeft().getColor() + ";");
		}
		sb.append("\"");
		sb.append(">");
		sb.append("<div style=\"");
		sb.append("width:" + (getWidth() == 0 ? columnWidth : getWidth()) + "px;");
		sb.append("max-width:" + (getWidth() == 0 ? columnWidth : getWidth()) + "px;");
		if (StringUtils.isNotBlank(this.getTextAlign()))
		{
			sb.append("text-align:" + this.getTextAlign() + ";");
		}
		else if (this.value instanceof Double && !StringUtils.equals("@", this.format))
		{
			sb.append("text-align:right;");
		}
		sb.append("\"");
		sb.append(">");
		if (this.isUnderline())
		{
			sb.append("<u>");
		}
		sb.append(this.formatValue());
		if (this.isUnderline())
		{
			sb.append("</u>");
		}
		sb.append("</div>");
		sb.append("</td>");
		return sb.toString();
	}

}
