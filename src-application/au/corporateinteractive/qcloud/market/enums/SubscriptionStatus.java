package au.corporateinteractive.qcloud.market.enums;

/**
 * @author Sishir
 *
 */
public enum SubscriptionStatus
{
	TRIAL, SUBSCRIBED, UNSUBSCRIBED, SUSPENDED, PAY_DUE, CANCELLED;
}
