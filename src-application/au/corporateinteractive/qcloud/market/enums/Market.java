package au.corporateinteractive.qcloud.market.enums;

/**
 * @author Sishir
 *
 */
public enum Market
{
	QCSABRE, QCTRAVELPORT, QCPRICETABLE, QCESIGN, QCPAYMENT, QCLIVECHAT, QCARRIVALGUIDE, QCWORKFLOW;

}
