package au.corporateinteractive.qcloud.market.utils;

public abstract class Constant
{

	public static final String SEGMENT_FLIGHT = "Flight";
	public static final String SEGMENT_HOTEL = "Hotel";
	public static final String SEGMENT_CAR_HIRE = "Car Hire";
	public static final String SEGMENT_COACH = "Coach";
	public static final String SEGMENT_BUS = "Bus";
	public static final String SEGMENT_TOUR = "Tour";
	public static final String SEGMENT_TRANSFER = "Transfer";
	public static final String SEGMENT_BLANK = "Blank";
	public static final String SEGMENT_RAIL = "Rail";
	public static final String SEGMENT_GENERAL = "General";
	public static final String SEGMENT_CRUISE = "Cruise";
	public static final String SEGMENT_AIR_TAXI = "Air Taxi";
	public static final String SEGMENT_SURFACE = "Surface";
	public static final String SEGMENT_ANCILLARY_SERVICES = "Ancillary Services";
	public static final String SEGMENT_INSURANCE = "Insurance";
	public static final String SEGMENT_MISCELLANEOUS = "Miscellaneous";
	public static final String SEGMENT_PACKAGE = "Package";
	public static final String SEGMENT_TRAVELLERS_CHEQUE = "Travellers cheque";
	public static final String SEGMENT_TRANSACTION_FEE = "Transaction Fee";
	public static final String SEGMENT_MERCHANT_FEE = "Merchant Fee";
	public static final String SEGMENT_AMENDMENT_FEE = "Amendment Fee";
	public static final String SEGMENT_SERVICE_FEE = "Service Fee";
	public static final String SEGMENT_MANAGEMENT_FEE = "Management Fee";
	public static final String SEGMENT_CANCELLATION_FEE = "Cancellation Fee";
	public static final String SEGMENT_ACTIVITY = "Activity";
	public static final String SEGMENT_FERRY = "Ferry";
	public static final String SEGMENT_OWN_ARRANGEMENT = "Own Arrangement";

	public static final String CCTE_TRIP_PHONE_USAGE_EMAIL = "E-mail";
	public static final String CCTE_TRIP_PHONE_USAGE_MOBILE = "Mobile";

	public static final String CCTE_TRIP_ADDRESS_USAGE_BUSINESS = "Business";

	public static final String CI_WORKFLOW_STATUS_PENDING = "Pending";
	public static final String CI_WORKFLOW_STATUS_SENT = "Sent";

	public static final String BOOKING_SYTEM_TYPE_SABRE = "Sabre";
	public static final String BOOKING_SYTEM_TYPE_SABRE_API = "SabreAPI";
	public static final String BOOKING_SYTEM_TYPE_CCTE = "CCTE";
	public static final String BOOKING_SYTEM_TYPE_AMADEUS = "Amadeus";

	public static final String CURRENCY_URL = "CURRENCY_URL";
	public static final String CURRENCY_XML_STORED_FOLDER = "CURRENCY_XML_STORED_FOLDER";

	public static final String GUIDE_CITY_FLAG = "GUIDE_CITY_FLAG";
	public static final String GUIDE_CITY_FLAG_OPEN = "OPEN";
	public static final String GUIDE_CITY_FLAG_CLOSED = "CLOSED";

	public static final String BOOKING_STATUS_QUOTE = "Quote";
	public static final String BOOKING_STATUS_ACTIVE = "Active";

	public static final String XML_ERROR_SEVERITY_LEVEL_CATASTROPHIC = "Catastrophic";
	public static final String XML_ERROR_SEVERITY_LEVEL_CRITICAL = "Critical";
	public static final String XML_ERROR_SEVERITY_LEVEL_HIGH = "High";

	public static final int SABRE_MAX_TRIES = 10;
	public static final int CCTE_MAX_TRIES = 60;
	public static final int AMADEUS_MAX_TRIES = 10;

	public static final int CCTE_MAX_ATTEMPTS = 3;

	public static final String HLO_USER_AGENCY = "HLO_USER_AGENCY";

	public static final String AGENCY_TYPE_BRANDED = "Branded";
	public static final String AGENCY_TYPE_ASSOCIATE_MEMBEROF = "Associate-Member Of";
	public static final String AGENCY_TYPE_ASSOCIATE_AMEX = "Associate-AMEX";
	public static final String AGENCY_TYPE_AFFILIATE = "Affiliate";
	public static final String AGENCY_TYPE_QBT = "QBT";

	public static final String CHARGE_TYPE_CHARGEABLE = "CHARGEABLE";
	public static final String CHARGE_TYPE_PAYDIRECT = "PAY_DIRECT";

	public static final String CHAT_EMAIL_TEMPLATE = "<table style=\"max-width:600px;max-height:40px;min-height:40px;height:auto;width:100%;margin:0 auto;background-color: #eef9ff;border-bottom-color: #c6dce8;border-bottom-style: solid;border-bottom-width: 1px;\">\n"
			+ "    <tbody>\n"
			+ "        <tr>\n"
			+ "            <td style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight: normal;padding: 10px 20px;vertical-align: middle;\" valign=\"middle\">\n"
			+ "                <span>${chatUserName} from ${chatUserCompany}</span>\n"
			+ "            </td>\n"
			+ "            <td style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight: normal;padding: 10px 20px;vertical-align: middle;text-align:right;\" valign=\"middle\">\n"
			+ "                <span>View Conversation on Quotecloud Mobile</span>\n"
			+ "            </td>\n"
			+ "        </tr>\n"
			+ "    </tbody>\n"
			+ "</table>\n"
			+ "<table style=\"background-color: #f0f4f7;font-size: 12px;height: auto;line-height: 18px;margin: 0 auto;max-width: 600px;overflow: hidden;width: 100%;\">\n"
			+ "    <tbody>\n"
			+ "        <tr>\n"
			+ "            <td style=\"font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:1px;font-weight:normal;height:40px;line-height:1;max-height:40px;padding:0\">&nbsp;</td>\n"
			+ "        </tr>\n"
			+ "        <tr>\n"
			+ "                <td style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight: normal;padding: 0;\">\n"
			+ "                    <div>\n"
			+ "                            ${chatUserMessage}\n"
			+ "                        <table bgcolor=\"#B1B8BE\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" valign=\"top\" align=\"center\" style=\"background-color:#b1b8be;font-size:12px;height:40px;line-height:18px;margin:0 auto;max-width:600px;width:100%\">\n"
			+ "                            <tbody>\n"
			+ "                                <tr>\n"
			+ "                                    <td style=\"color:#ffffff;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;height:20px;width:20px;padding:0 0 0 20px\">\n"
			+ "                                        <a href=\"https://dashboard.traveldocs.cloud\" style=\"color:#ffffff;font-weight:bold\" target=\"_blank\" data-saferedirecturl=\"https://dashboard.traveldocs.cloud\">\n"
			+ "                                            <img width=\"25\" height=\"25\" src=\"https://d1la8ydat4j0a.cloudfront.net/quotecloud/images/qCloud.png\" alt=\"QuoteCloud logo white 25x25 at 2x\" style=\"border:none;\" class=\"CToWUd\">\n"
			+ "                                        </a>\n"
			+ "                                    </td>\n"
			+ "                                    <td style=\"color:#ffffff;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;padding:0 0 0 10px\">\n"
			+ "                                        This email was sent from <a href=\"https://dashboard.traveldocs.cloud\" style=\"color:#ffffff;font-weight:bold\" target=\"_blank\" data-saferedirecturl=\"https://dashboard.traveldocs.cloud\"><span class=\"il\">QuoteCloud</span></a>\n"
			+ "                                    </td>\n"
			+ "                                </tr>\n"
			+ "                          </tbody>\n"
			+ "                        </table>\n"
			+ "                    </div>\n"
			+ "                </td>\n"
			+ "            </tr>\n"
			+ "    </tbody>\n"
			+ "</table>";

	public static final String CHAT_MESSAGE_EMAIL_TEMPLATE = "<table style=\"border-collapse: separate;border-spacing: 0;font-size: 12px;line-height: 18px;margin: 0 auto;max-width: 560px;width: 90%;\">\n"
			+ "                            <tbody>\n"
			+ "                                <tr>\n"
			+ "                                    <td style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight: normal;padding: 0;vertical-align: top;width: 32px;\">\n"
			+ "                                        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"font-size: 12px;line-height: 18px;width: 32px;\">\n"
			+ "                                            <tbody>\n"
			+ "                                                <tr>\n"
			+ "                                                    <td style=\"border-radius: 2px;color: white;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight: bold;height: 32px;padding: 1px 0 0; text-align: center;vertical-align: middle;width: 32px;\" align=\"center\" valign=\"middle\">\n"
			+ "                                                        <img width=\"32\" height=\"32\" src=\"${chatUserMessageImage}application/images/conversation.png\" style=\"border-radius:2px\">\n"
			+ "                                                    </td>\n"
			+ "                                                </tr>\n"
			+ "                                            </tbody>\n"
			+ "                                        </table>\n"
			+ "                                    </td>\n"
			+ "                                    <td style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;font-size: 1px;font-weight: normal;padding: 0;vertical-align: top;width: 10px;\">\n"
			+ "                                    </td>\n"
			+ "                                    <td style=\"background-color: #ffffff;border: 1px solid #d5dce2;border-radius: 6px;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight: normal;padding: 0;word-wrap: break-word;\">\n"
			+ "                                        <table style=\"font-size: 12px;line-height: 18px;table-layout: fixed;width: 100%;\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n"
			+ "                                            <tbody>\n"
			+ "                                                <tr>\n"
			+ "                                                    <td style=\"color: #3e474c;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;font-size: 14px;font-weight: normal;padding: 10px 13px;\">\n"
			+ "                                                        <p style=\"margin:0;padding:3px 0\">${chatUserMessageText}</p>\n"
			+ "                                                    </td>\n"
			+ "                                                </tr>\n"
			+ "                                            </tbody>\n"
			+ "                                        </table>\n"
			+ "                                    </td>\n"
			+ "                                </tr>\n"
			+ "                                <tr>\n"
			+ "                                    <td style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight: normal;padding: 0;\"></td>\n"
			+ "                                    <td style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight: normal;padding: 0;\"></td>\n"
			+ "                                    <td style=\"color: #b6c5ce;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;font-size: 13px;font-weight: normal;padding: 5px 5px 0;\">\n"
			+ "                                        ${chatUserMessageTime}\n"
			+ "                                    </td>\n"
			+ "                                </tr>\n"
			+ "                                <tr>\n"
			+ "                                    <td colspan=\"3\" style=\"font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:1px;font-weight:normal;height:6px;line-height:1;max-height:40px;padding:0\">&nbsp;</td>\n"
			+ "                                </tr>\n"
			+ "                            </tbody>\n"
			+ "                        </table>";

}
