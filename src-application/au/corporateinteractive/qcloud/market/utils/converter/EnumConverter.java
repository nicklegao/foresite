package au.corporateinteractive.qcloud.market.utils.converter;

import org.apache.commons.beanutils.converters.AbstractConverter;
import org.apache.log4j.Logger;

/**
 * @author Sishir
 *
 */
public class EnumConverter extends AbstractConverter
{

	private static Logger logger = Logger.getLogger(EnumConverter.class);

	@Override
	protected String convertToString(final Object pValue) throws Throwable
	{
		return ((Enum) pValue).name();
	}

	@Override
	protected Object convertToType(final Class pType, final Object pValue)
			throws Throwable
	{
		// NOTE: Convert to String is handled elsewhere

		final Class<? extends Enum> type = pType;
		try
		{
			return Enum.valueOf(type, pValue.toString());
		}
		catch (final IllegalArgumentException e)
		{
			logger.warn("No enum value \""
					+ pValue
					+ "\" for "
					+ type.getName());
		}

		return null;
	}

	@Override
	protected Class getDefaultType()
	{
		return null;
	}

}
