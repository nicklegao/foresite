package au.corporateinteractive.qcloud.market.utils.converter;

import org.apache.commons.beanutils.ConvertUtilsBean2;
import org.apache.commons.beanutils.Converter;

/**
 * @author Sishir
 *
 */
public class EnumAwareConvertUtilsBean extends ConvertUtilsBean2
{

	private static final EnumConverter ENUM_CONVERTER = new EnumConverter();

	@Override
	public Converter lookup(Class pClazz)
	{
		final Converter converter = super.lookup(pClazz);

		if (converter == null && pClazz.isEnum())
		{
			return ENUM_CONVERTER;
		}
		else
		{
			return converter;
		}
	}

}
