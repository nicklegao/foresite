package au.corporateinteractive.qcloud.market.utils;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.XMLGregorianCalendar;


/**
 * @author Sishir
 *
 */
public class Utils
{

	public static Date combineDateAndTime(Timestamp date, Time time)
	{
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeInMillis(date.getTime());
		cal.add(Calendar.HOUR, time.getHours());
		cal.add(Calendar.MINUTE, time.getMinutes());
		cal.add(Calendar.SECOND, time.getSeconds());

		Date merged = cal.getTime();
		return merged;
	}

	public static Timestamp convertToTimestamp(XMLGregorianCalendar cal)
	{
		Timestamp timestamp = null;
		if (cal == null)
		{
			return null;
		}
		try
		{
			timestamp = new Timestamp(cal.toGregorianCalendar().getTimeInMillis());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return timestamp;
	}

	public static java.sql.Date convertToDate(XMLGregorianCalendar cal)
	{
		java.sql.Date date = null;
		if (cal == null)
		{
			return null;
		}
		try
		{
			date = new java.sql.Date(cal.toGregorianCalendar().getTimeInMillis());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return date;
	}
}
