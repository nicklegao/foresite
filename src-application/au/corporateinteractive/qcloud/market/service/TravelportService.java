//package au.corporateinteractive.qcloud.market.service;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.io.Reader;
//import java.util.Properties;
//
//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Unmarshaller;
//
//import org.apache.commons.io.FilenameUtils;
//import org.apache.commons.io.IOUtils;
//import org.apache.log4j.Logger;
//import org.springframework.core.io.support.PropertiesLoaderUtils;
//
//import au.corporateinteractive.qcloud.market.model.travel.core.BookingCI;
//import au.corporateinteractive.qcloud.market.travelport.schema.booking.BookingCCTE;
//import au.corporateinteractive.qcloud.market.utils.converter.CCTEToCoreConverter;
//import au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess;
//import au.net.webdirector.common.Defaults;
//import au.net.webdirector.common.datalayer.base.file.StoreFileAccess;
//import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
//
//import com.google.gson.GsonBuilder;
//
///**
// * @author Sishir
// *
// */
//public class TravelportService
//{
//	Logger logger = Logger.getLogger(TravelportService.class);
//
//	public void processScheduleTask()
//	{
//		Properties properties;
//		try
//		{
//			properties = PropertiesLoaderUtils.loadAllProperties("config/market/travelport.properties");
//			File xmlFolder = new File(Defaults.getInstance().getStoreDir() + properties.getProperty("travelport.xml_folder"));
//			StoreFileAccess sfa = StoreFileAccessFactory.getInstance();
//			for (File file : xmlFolder.listFiles())
//			{
//				String moveToFolderPath = Defaults.getInstance().getStoreDir() + properties.getProperty("travelport.xml_folder");
//				if (!file.isDirectory())
//				{
//					boolean error = true;
//
//					logger.info("Processing xml file, " + file);
//					BookingCCTE bookingCCTE = processXML(file);
//					if (bookingCCTE != null)
//					{
//						BookingCI booking = new CCTEToCoreConverter().convertToCore(bookingCCTE, file.getName());
//						String s = new GsonBuilder().create().toJson(booking, BookingCI.class);
//						boolean success = new TravelDocsDataAccess().createItineraryTravelDoc(booking, "Travelport", "", "", null);
//						if (success)
//						{
//							moveToFolderPath += "processed" + File.separator;
//							error = false;
//						}
//					}
//
//
//					if (error)
//					{
//						moveToFolderPath += "error" + File.separator;
//					}
//
//					String fileName = file.getName();
//					if (sfa.exists(new File(moveToFolderPath, file.getName())))
//					{
//						fileName = FilenameUtils.removeExtension(fileName) + "_" + System.currentTimeMillis() + ".xml";
//						File renameFile = new File(xmlFolder, fileName);
//						file.renameTo(renameFile);
//						file = new File(xmlFolder, fileName);
//					}
//
//					sfa.copyTo(file, new File(moveToFolderPath));
//					sfa.delete(file);
//				}
//			}
//		}
//		catch (IOException e)
//		{
//			logger.error("Error", e);
//		}
//	}
//
//	private BookingCCTE processXML(File xmlFile)
//	{
//		try
//		{
//			return getItineraryFromFile(xmlFile);
//		}
//		catch (Exception e)
//		{
//			logger.error("Error", e);
//			return null;
//		}
//	}
//
//	public BookingCCTE getItineraryFromFile(File file) throws Exception
//	{
//		JAXBContext jaxbContext;
//		BookingCCTE root = null;
//		InputStream inputStream = null;
//		Reader reader = null;
//		try
//		{
//			jaxbContext = JAXBContext.newInstance("au.corporateinteractive.qcloud.market.travelport.schema.booking");
//			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
//
//			inputStream = new FileInputStream(file);
//			reader = new InputStreamReader(inputStream, "ISO-8859-1");
//
//			root = (BookingCCTE) unmarshaller.unmarshal(reader);
//
//		}
//		catch (JAXBException e)
//		{
//			e.printStackTrace();
//			logger.fatal("Exception happened during unmarshalling itinerary file.", e);
//			throw new Exception("Exception happened during unmarshalling itinerary file", e);
//		}
//		finally
//		{
//			IOUtils.closeQuietly(inputStream);
//			IOUtils.closeQuietly(reader);
//		}
//		return root;
//	}
//
//}
