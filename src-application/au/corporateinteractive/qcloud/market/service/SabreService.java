//package au.corporateinteractive.qcloud.market.service;
//
//import java.text.ParseException;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.GregorianCalendar;
//import java.util.List;
//
//import javax.xml.datatype.DatatypeFactory;
//import javax.xml.ws.soap.SOAPFaultException;
//
//import au.corporateinteractive.qcloud.proposalbuilder.db.*;
//import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
//import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//
//import au.corporateinteractive.qcloud.market.enums.Market;
//import au.corporateinteractive.qcloud.market.model.travel.core.BookingCI;
//import au.corporateinteractive.qcloud.market.sabre.util.SabreWebServiceClient;
//import au.corporateinteractive.qcloud.market.sabre.wsdl.booking.v4.GetBookingResponse;
//import au.corporateinteractive.qcloud.market.utils.converter.SabreToCoreConverter;
//import au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess;
//import au.net.webdirector.common.datalayer.client.CategoryData;
//
///**
// * @author Sishir
// *
// */
//public class SabreService
//{
//
//	Logger logger = Logger.getLogger(SabreService.class);
//
//	public void syncBooking()
//	{
//		SabreDataAccess sda = new SabreDataAccess();
//
//		try
//		{
//			if (schedulerAvailable(1))
//			{
//				try
//				{
//					lockScheduler();
//
//					try (TransactionManager txManager = TransactionManager.getInstance())
//					{
//						TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
//						UserAccountDataAccess uda = new UserAccountDataAccess();
//						List<CategoryData> companys = tma.getCategoriesByParentId(UserAccountDataAccess.USER_MODULE, "0");
//
//						for (CategoryData subscriber : companys)
//						{
//							String companyId = subscriber.getId();
//
//
//							List<CategoryData> teams = new UserAccountDataAccess().getCompanyTeams(companyId);
//
//							for (CategoryData team : teams)
//							{
//								CategoryData sabreTeamCategory = sda.getCategoryByTeamId(team.getId());
//								if (sabreTeamCategory != null)
//								{
//									Date lastSyncDate = sabreTeamCategory.getDate("ATTRDATE_lastSyncDate");
//									if (lastSyncDate == null)
//									{
//										lastSyncDate = sabreTeamCategory.getDate("Create_date");
//									}
//
//									String sabreUserName = sabreTeamCategory.getString(SabreDataAccess.C_AGENCY_CODE);
//									String sabrePassword = sabreTeamCategory.getString(SabreDataAccess.C_PASSWORD);
//
//									if (StringUtils.isNotEmpty(sabreUserName) && StringUtils.isNotEmpty(sabrePassword))
//									{
//										SabreWebServiceClient sabreClient = new SabreWebServiceClient(sabreUserName, sabrePassword);
//										List<Long> response = new ArrayList<Long>();
//										Long bookingRef = new Long(0);
//
//										try
//										{
//											response = sabreClient.identifyBookingsMarkedForHandover(lastSyncDate);
//
//											for (Long bookingId : response)
//											{
//												bookingRef = bookingId;
//												Date d1 = new Date();
//												GetBookingResponse bookingResponse = sabreClient.getBookingFromSabre(bookingId);
//
//												if (sabreUserName != null && bookingResponse != null && bookingResponse.getBooking() != null) {
//													BookingCI bookingCI = new SabreToCoreConverter().convertToCore(sabreUserName, bookingResponse);
////													boolean success = new TravelDocsDataAccess().createItineraryTravelDoc(bookingCI, "Sabre", sabreUserName, team.getId(), null);
//													Date d2 = new Date();
//													System.out.println("time taken: " + (d2.getTime() - d1.getTime()));
//													System.out.println("Job success: " + success);
//												} else {
//													Date d2 = new Date();
//													System.out.println("time taken: " + (d2.getTime() - d1.getTime()));
//													System.out.println("Job Failed: Sabre Username was null!!!");
//												}
//
//											}
//
//											sda.updateLastSyncDate(team.getId(), DatatypeFactory.newInstance().newXMLGregorianCalendar((GregorianCalendar) GregorianCalendar.getInstance()));
//										}
//										catch (SOAPFaultException e)
//										{
//											logger.error("Error", e);
//											//If error occurs handle
//											System.out.println(bookingRef);
//										}
//
//									}
//								}
//								else
//								{
//									logger.info(String.format("No sabre record found for team [%s]", team.getId()));
//								}
//							}
//						}
//						txManager.commit();
//					}
//					catch (Exception e)
//					{
//						logger.error("Error", e);
//					}
//
//				}
//				catch (Exception e)
//				{
//					logger.error("Error", e);
//				}
//				finally
//				{
//					freeScheduler();
//				}
//
//			}
//			else
//			{
//				logger.info("Scheduler already running, try next time");
//			}
//		}
//		catch (ParseException e)
//		{
//			e.printStackTrace();
//		}
//
//	}
//
//	private void lockScheduler()
//	{
//		PluginSettingDataAccess psda = new PluginSettingDataAccess();
//		psda.lockScheduler(Market.QCSABRE);
//	}
//
//	private void freeScheduler()
//	{
//		PluginSettingDataAccess psda = new PluginSettingDataAccess();
//		psda.freeScheduler(Market.QCSABRE);
//	}
//
//	private boolean schedulerAvailable(int tryCount) throws ParseException
//	{
//		PluginSettingDataAccess psda = new PluginSettingDataAccess();
//		if (psda.checkSchedulerAccess(Market.QCSABRE))
//		{
//			psda.setTimer(Market.QCSABRE);
//			return true;
//		}
//
//		if (tryCount <= 6)
//		{
//			try
//			{
//				Thread.sleep(10000);
//			}
//			catch (InterruptedException e)
//			{
//				logger.error("Error", e);
//			}
//			return schedulerAvailable(++tryCount);
//		}
//		return false;
//	}
//}
