package au.corporateinteractive.qcloud.pdf.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;


public class DownloadPdf extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(DownloadPdf.class);
	public static String ROOT_PATH;

	public DownloadPdf()
	{
		super();
	}

	@Override
	public void init() throws ServletException
	{
		super.init();

		ROOT_PATH = getServletContext().getRealPath("/");
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{}

	private static String convertToFilesafeCharacters(String part)
	{
		return WordUtils.capitalizeFully(part) // Turn into upper camel case : Sushant's Awesome Proposal
				.replaceAll("[\\s\\p{Punct}]", ""); // Strip Punctuation : Sushants Awesome Proposal
	}
}
