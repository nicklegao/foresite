package au.corporateinteractive.qcloud.pdf.service;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.util.XRRuntimeException;

import com.lowagie.text.pdf.BaseFont;

import au.corporateinteractive.qcloud.pdf.controller.DownloadPdf;
import au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder;
import au.net.webdirector.common.Defaults;

public class PdfService
{

	private static PdfService instance;
	private Logger logger;

	private PdfService()
	{
		logger = Logger.getLogger(PdfService.class);
	}

	public synchronized static PdfService getInstance()
	{
		if (instance == null)
		{
			instance = new PdfService();
		}
		return instance;
	}


	public File generatePDFByUrl(String htmlContentUrl)
	{
		String filePath = getTempPdfFilePath();
		return generatePDFByUrl(htmlContentUrl, filePath);
	}

	String getTempPdfFilePath()
	{
		String tempDir = Defaults.getInstance().getStoreDir();
		String filePath = tempDir + "/temp/" + new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date()) + RandomStringUtils.randomAlphabetic(6) + ".pdf";
		return filePath;
	}

	public File generatePDFByUrl(String htmlContentUrl, String filePath)
	{
		try
		{
			String storeDir = Defaults.getInstance().getStoreDir();
			File pdfFile = new File(filePath);

			OutputStream os = null;
			try
			{
				os = new FileOutputStream(pdfFile);

				ITextRenderer renderer = new ITextRenderer();

				File fontDir = new File(storeDir, "_fonts");
				File unicodeFontDir = new File(storeDir, "_fonts/unicode");
				FileFilter acceptedFontFiles = new FileFilter()
				{
					@Override
					public boolean accept(File pathname)
					{
						return pathname.getName().toLowerCase().endsWith(".otf") || pathname.getName().toLowerCase().endsWith(".ttf");
					}
				};
				File[] fontFiles = fontDir.listFiles(acceptedFontFiles);
				if (fontFiles != null)
				{
					for (File fontFile : fontFiles)
					{
						renderer.getFontResolver().addFont(fontFile.getAbsolutePath(), BaseFont.EMBEDDED);
					}
				}
				// renderer.getFontResolver().addFont("C:\\WINDOWS\\FONTS\\ARIALUNI.TTF", BaseFont.IDENTITY_H, true);

				File[] unicodeFontFiles = unicodeFontDir.listFiles(acceptedFontFiles);
				if (unicodeFontFiles != null)
				{
					for (File fontFile : unicodeFontFiles)
					{
						renderer.getFontResolver().addFont(fontFile.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
					}
				}
				
				File[] fontDirs = new File[] {
						new File(DownloadPdf.ROOT_PATH, "application" + File.separator + "assets" + File.separator + "new" + File.separator + "vendor" + File.separator + "fontawesome-pro-5.8.1-web" + File.separator + "webfonts"),
						new File(DownloadPdf.ROOT_PATH, "application" + File.separator + "assets" + File.separator + "plugins" + File.separator + "qcloud-iconfont" + File.separator + "fonts"),
						new File(DownloadPdf.ROOT_PATH, "application" + File.separator + "assets" + File.separator + "fonts" + File.separator + "fonts")
				};
				for (File dirPath : fontDirs)
				{
					File[] ttfFiles = dirPath.listFiles(acceptedFontFiles);
					if (ttfFiles != null)
					{
						for (File fontFile : ttfFiles)
						{
							renderer.getFontResolver().addFont(fontFile.getAbsolutePath(), BaseFont.IDENTITY_H, true);
						}
					}
				}

				// Load the document
				logger.debug("Setting PDF Document: " + htmlContentUrl);
				renderer.setDocument(htmlContentUrl);
				logger.debug("Laying out PDF");
				renderer.layout();
				logger.debug("Creating PDF");
				renderer.createPDF(os);
				logger.info(String.format("PDF (%s) Created. Size: %s bytes",
						pdfFile.getAbsolutePath(),
						pdfFile.length()));
			}
			finally
			{
				if (os != null)
				{
					try
					{
						os.close();
					}
					catch (IOException e2)
					{
						logger.fatal("Unable to close output stream?!?!?", e2);
					}
				}
				else
				{
					logger.fatal("Output stream closed?!?!?");
				}
			}
			logger.info("Returning generated PDF file " + pdfFile.getAbsolutePath());
			return pdfFile;
		}
		catch (XRRuntimeException e)
		{

			logger.fatal("PDF Generator failed (type A) HTML URL: " + htmlContentUrl, e);
			new EmailBuilder().prepareErrorEmailForContext("PDF Generator failed (type A) HTML URL: " + htmlContentUrl, e).doSend();
		}
		catch (Exception e)
		{
			logger.fatal("PDF Generator failed (type B) HTML URL: " + htmlContentUrl, e);
			new EmailBuilder().prepareErrorEmailForContext("PDF Generator failed (type B)", e).doSend();
		}
		logger.fatal("NO PDF GENERATED!!! Returning null");
		return null;
	}

	public File downloadPdf(String url) throws IOException
	{
		logger.info("Start downloading external file: " + url);
		URL website = new URL(url);
		ReadableByteChannel rbc = Channels.newChannel(website.openStream());
		File result = new File(getTempPdfFilePath());
		FileOutputStream fos = null;
		try
		{
			fos = new FileOutputStream(result);
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		}
		finally
		{
			if (fos != null)
			{
				fos.close();
			}
		}
		logger.info("End downloading external file: " + url);
		return result;
	}

	public void mergePdf(List<File> files, OutputStream out) throws IOException
	{
		PDFMergerUtility merger = new PDFMergerUtility();
		for (File file : files)
		{
			merger.addSource(file);
		}
		merger.setDestinationStream(out);
		merger.mergeDocuments(MemoryUsageSetting.setupTempFileOnly());
	}

	private void clonePdfIntoDebugCompany()
	{
	}
	
	
	static HashMap<String, Double> colMap = new HashMap<String, Double>(){{
		put("col-1", 12.005);
		put("col-2", 6.0005);
		put("col-3", 4.001);
		put("col-4", 3.001);
		put("col-5", 2.4);
		put("col-6", 2.0);
		put("col-7", 1.714285714285714);
		put("col-8", 1.5);
		put("col-9", 1.333333333333333);
		put("col-10", 1.2);
		put("col-11", 1.090909090909091);
		put("col-12", 1.0);
	}};
	
	
	static HashMap<String, HashMap<String, Integer>> aspectRatioMap = new HashMap<String, HashMap<String, Integer>>(){{
		put("16x9", new HashMap<String, Integer>() {{put("w", 16);put("h", 9);}});
		put("21x9", new HashMap<String, Integer>() {{put("w", 21);put("h", 9);}});
		put("4x3", new HashMap<String, Integer>() {{put("w", 4);put("h", 3);}});
		put("3x2", new HashMap<String, Integer>() {{put("w", 3);put("h", 2);}});
		put("1x1", new HashMap<String, Integer>() {{put("w", 1);put("h", 1);}});
	}};
	
	public static String getResponsiveGridCss(double rowWidth, String parent, int level){
		String responsiveGridCss = "";
		responsiveGridCss += parent+" .row.row_container{width:"+rowWidth+"px;}";
		for(String col : colMap.keySet()) {
			
			double colWidth = rowWidth/colMap.get(col);
			responsiveGridCss += ".row.row_container "+parent+" ."+col+","+".row.row_container "+parent+" ."+col+" .video-image-wrapper a{width:"+colWidth+"px !important;}"+".row.row_container "+parent+" ."+col+" .panel-froala p img{max-width:"+colWidth+"px;}";
			for(String ar : aspectRatioMap.keySet()) {
				HashMap<String, Integer> ratio = aspectRatioMap.get(ar);
				responsiveGridCss += ".row.row_container "+parent+" ."+col+" .videoContainer.ratio_"+ar+" .video-image-wrapper a{height:"+(colWidth*ratio.get("h")/ratio.get("w"))+"px;}";
			}
			if(level>0) {
				responsiveGridCss += getResponsiveGridCss(colWidth, parent+" ."+col, level-1);
			}
		}
		return responsiveGridCss;
	}

	public static void main(String[] args)
	{
		File file = PdfService.getInstance().generatePDFByUrl("https://nickgao.webdirector.net.au/api/free/payment/invoice?invId=11", "/temp/test.pdf");
		System.out.print(file);
	}
}
