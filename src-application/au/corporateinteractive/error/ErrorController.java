/**
 * @author Sushant Verma
 * @date March 30, 2017
 */
package au.corporateinteractive.error;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder;

@Controller
public class ErrorController
{
	private static Logger logger = Logger.getLogger(ErrorController.class);
	
	private static Set<String> emailURIRegexBlacklist = new HashSet<>(Arrays.asList(
			"/favicon.ico$", 
			"/browserconfig.xml$"));

	@RequestMapping(value = "*")
	public String error(
			HttpServletRequest request,
			HttpServletResponse response,
			Map<String, Object> model)
	{
		
		int errorCode = (int) request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		String uri = (String) request.getAttribute(RequestDispatcher.ERROR_REQUEST_URI);
		Exception exception = (Exception) request.getAttribute(RequestDispatcher.ERROR_EXCEPTION);
		Cookie[] cookies = request.getCookies();
		if (null != cookies && cookies.length > 0)
		{
			if (shouldSendEmail(errorCode, uri) && !uri.contains(".map"))
			{
				new EmailBuilder().prepareErrorEmailForContext("ERROR... HTTP " + errorCode + " for URI "+ uri, exception).doSend();
			}
		}

		model.put("errorcode", errorCode);
		return "errorpage";
	}

	private boolean shouldSendEmail(int errorCode, String uri) {
		for (String regex:emailURIRegexBlacklist)
		{
			if (uri.matches(regex))
			{
				logger.info("Should not send email for URI: "+uri+" because it matches regex: "+ regex);
				return false;
			}
		}

		if(errorCode == 404)
		{
			return false;
		}
		
		//can add checks for status code here
		return true;
	}
}
