package au.corporateinteractive.utils;


public class PDFUtils
{
	public static String formatTextblock(String html)
	{
		if (html == null)
		{
			return "";
		}
		html = html.replaceAll("<br[^/>]*>", "<br/>");
		if (html.contains("<tr>"))
		{
			StringBuilder sb = new StringBuilder();
			String[] fragments = html.split("<tbody>\\s*\\n*\\t*<tr>");
			int index = 0;
			for (String fragment : fragments)
			{
				if (index == 0)
				{
					sb.append(fragment);
				}
				else
				{
					sb.append("<tbody><tr class='titleRow'>");
					sb.append(fragment);
				}
				index += 1;
			}
			html = sb.toString();
		}

		return html;
	}
}
