package au.corporateinteractive.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WhatsNextUtils {
	private static final String ORDER_NOW = "Order Now";
	private static final String DISCUSSION = "Discussion";

	public static String detectIconTextForTextblock(String textblockHtml, int blockIndex) {
		if (blockIndex==0)
			return ORDER_NOW;
		
		Pattern titlePattern = Pattern.compile(".*<h3>(.*)</h3>.*", Pattern.CASE_INSENSITIVE);
		Matcher matches = titlePattern.matcher(textblockHtml);
		if (matches.matches() && matches.groupCount() > 0)
		{
			String firstMatch = matches.group(1).trim().toLowerCase();
			if (firstMatch.startsWith("let's chat")
					|| firstMatch.startsWith("lets chat"))
			{
				return DISCUSSION;
			}
		}
		return null;
	}
	
	public static String imageNameForIcon(String icon)
	{
		if (icon.equals(ORDER_NOW))
		{
			return "accept.png";
		}
		else if (icon.equals(DISCUSSION))
		{
			return "chat.png";
		}
		
		return null;
	}
}
