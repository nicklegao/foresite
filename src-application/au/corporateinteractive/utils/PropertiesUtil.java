package au.corporateinteractive.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class PropertiesUtil {
	
	public static String formatPropertyForStyle(HashMap<String, Object> properties)
	{
		return formatPropertyForStyle(properties, null, null);
	}


	public static String formatPropertyForStyle(HashMap<String, Object> properties, List<String> inclusions, List<String> exclusions)
	{
		String css = "";
		String tempCSS = "";
		String tempColor = "";
		if (inclusions == null)
		{
			inclusions = new ArrayList<>();
		}
		if (exclusions == null)
		{
			exclusions = new ArrayList<>();
		}
		for (String key : properties.keySet())
		{
			if (!exclusions.contains(key) || inclusions.contains(key))
			{
				if (!key.equalsIgnoreCase("border_style")
						&& !key.equalsIgnoreCase("background_style")
						&& !key.equalsIgnoreCase("background_color")
						&& !key.equalsIgnoreCase("textcolor_style")
						&& !key.equalsIgnoreCase("text_color"))
				{
					css += getCssProperty(key, properties.get(key));
				}
				else if (key.equalsIgnoreCase("background_color"))
				{
					tempCSS += getCssProperty(key, properties.get(key));
				}
				else if (key.equalsIgnoreCase("text_color"))
				{
					tempColor += getCssProperty(key, properties.get(key));
				}
				else
				{
					css += StringUtils.isEmpty((String) properties.get(key)) ? "" : getDependantCSSProperty(key, properties);
				}
			}
		}
		if (StringUtils.isNotEmpty(tempCSS))
		{
			if (!css.toLowerCase().contains("background-color") && !css.toLowerCase().contains("background:"))
			{
				css += tempCSS;
			}
		}
		if (StringUtils.isNotEmpty(tempColor))
		{
			if (!css.toLowerCase().contains(" color:"))
			{
				css += tempColor;
			}
		}
		return css;
	}

	public static String getCssProperty(String property, Object value)
	{
		if (value != null)
		{
			if (property.equals("margin_top"))
				return " margin-top: " + getCSSLength(value) + ";";
			if (property.equals("margin_bottom"))
				return " margin-bottom: " + getCSSLength(value) + ";";
			if (property.equals("margin_left"))
				return " margin-left: " + getCSSLength(value) + ";";
			if (property.equals("margin_right"))
				return " margin-right: " + getCSSLength(value) + ";";
			if (property.equals("padding_top"))
				return " padding-top: " + getCSSLength(value) + ";";
			if (property.equals("padding_bottom"))
				return " padding-bottom: " + getCSSLength(value) + ";";
			if (property.equals("padding_left"))
				return " padding-left: " + getCSSLength(value) + ";";
			if (property.equals("padding_right"))
				return " padding-right: " + getCSSLength(value) + ";";
			if (property.equals("border_color"))
				return " border-color: " + value + ";";
			if (property.equals("border_radius"))
				return " border-radius: " + getCSSLength(value) + ";";
			if (property.equals("background_color"))
				return " background-color: " + value + ";";
			if (property.equals("height"))
				return " height: " + getCSSLength(value) + ";";
			if (property.equals("alignment"))
				return " text-align: " + value + ";";
			if (property.equals("button_color"))
				return " background-color: " + value + ";";
			if (property.equals("color") || property.equals("text_color"))
			{
				return " color: " + value + ";";
			}
			if (property.equals("fontSize"))
				return " font-size: " + getCSSLength(value) + ";";
			if (property.equals("max_width"))
				return " max-width: " + getCSSLength(value) + ";";
			if(property.equals("line_height")) {
				return " line-height: " + getCSSLength(value) + ";";
			}
		}
		return " ";
	}

	public static String fallbackForCss(String property, String value)
	{
		return " " + property.replaceAll("_", "-") + ": " + value + ";";
	}

	public static String getDependantCSSProperty(String key, HashMap<String, Object> properties)
	{
		String cssValue = "";
		if (key.equals("background_style"))
		{
			if (StringUtils.equalsIgnoreCase("single-color", (String) properties.get("background_style")))
			{
				cssValue += " background-color: " + properties.get("background_single_color") + ";";
			}
			else if (StringUtils.equalsIgnoreCase("image", (String) properties.get("background_style")))
			{
				cssValue += " background-image: url(\"" + properties.get("background_image") + "\");";
				cssValue += " background-size: cover;";
				cssValue += " background-position: center;";
			}
		}
		else if (key.equals("border_style"))
		{
			if (StringUtils.isNotBlank((String) properties.get("border_top")))
			{
				cssValue += " border-top: " + getCSSLength((String) properties.get("border_top")) + " " + properties.get("border_style") + ";";
			}
			if (StringUtils.isNotBlank((String) properties.get("border_bottom")))
			{
				cssValue += " border-bottom: " + getCSSLength((String) properties.get("border_bottom")) + " " + properties.get("border_style") + ";";
			}
			if (StringUtils.isNotBlank((String) properties.get("border_left")))
			{
				cssValue += " border-left: " + getCSSLength((String) properties.get("border_left")) + " " + properties.get("border_style") + ";";
			}
			if (StringUtils.isNotBlank((String) properties.get("border_right")))
			{
				cssValue += " border-right: " + getCSSLength((String) properties.get("border_right")) + " " + properties.get("border_style") + ";";
			}
		}
		else if (key.equals("textcolor_style"))
		{
			if (StringUtils.equalsIgnoreCase("inherit", (String) properties.get("textcolor_style")))
			{
				cssValue += " color:inherit;";
			}
			else
			{
				cssValue += " color:" + properties.get("text_color") + ";";
			}
		}
		return cssValue;
	}

	public static String formatPropertyForClassNames(HashMap<String, Object> properties)
	{
		String classNames = "";
		for (String key : properties.keySet())
		{
			String className = formatPropertyForClassNames(key, properties);
			if (StringUtils.isNotBlank(className))
			{
				classNames += " " + className;
			}
		}
		return classNames;
	}

	public static String formatPropertyForClassNames(String property, HashMap<String, Object> properties)
	{
		if (property.equals("column_width"))
		{
			System.out.println(properties.get(property));
			String responsiveClass = "";
			if (properties.get(property).getClass().equals(String.class)) {
				 responsiveClass = " col-" + Integer.valueOf((String) properties.get(property));
			} else if (properties.get(property).getClass().equals(Double.class)) {
				 responsiveClass = " col-" + ((Double) properties.get(property)).intValue();
			}
//			String responsiveClass = " col-md-" + properties.get(property);
//			responsiveClass += " " + getColumn_property_lg(properties, "col");
//			responsiveClass += " " + getColumn_property_sm(properties, "col");
//			responsiveClass += " " + getColumn_property_xs(properties, "col");
//
//			responsiveClass += " " + getColumn_property_lg(properties, "offset");
//			responsiveClass += " " + getColumn_property_md(properties, "offset");
//			responsiveClass += " " + getColumn_property_sm(properties, "offset");
//			responsiveClass += " " + getColumn_property_xs(properties, "offset");
//
//			responsiveClass += " d-lg-" + (StringUtils.equals("yes", properties.get("column_hidden_lg")) ? "none" : "block");
//			responsiveClass += " d-md-" + (StringUtils.equals("yes", properties.get("column_hidden_md")) ? "none" : "block");
//			responsiveClass += " d-sm-" + (StringUtils.equals("yes", properties.get("column_hidden_sm")) ? "none" : "block");
//			responsiveClass += " d-xs-" + (StringUtils.equals("yes", properties.get("column_hidden_xs")) ? "none" : "block");

			return responsiveClass;
		}
		else if (property.equals("className"))
		{
			return " " + properties.get(property);
		}
		else if (property.equals("animation"))
		{
			return " animate-start animation-" + properties.get(property) + " " + properties.get("animation_duration") + " " + properties.get("animation_delay");
		}
		return "";
	}

	public static String getColumn_property_lg(HashMap<String, String> properties, String name)
	{
		if (StringUtils.isNotBlank(properties.get("column_" + name + "_lg")))
		{
			if (!StringUtils.equals(properties.get("column_" + name + "_lg"), "inherit"))
			{
				return StringUtils.equals(properties.get("column_" + name + "_lg"), "0") ? "" : name + "-lg-" + properties.get("column_" + name + "_lg");
			}
			return getColumn_property_md(properties, name).replace("md", "lg");
		}
		return "";
	}

	public static String getColumn_property_md(HashMap<String, String> properties, String name)
	{
		if (StringUtils.equals(name, "col"))
		{
			return name + "-md-" + properties.get("column_width");
		}
		if (StringUtils.isNotBlank(properties.get("column_" + name + "_md")))
		{
			if (!StringUtils.equals(properties.get("column_" + name + "_md"), "inherit"))
			{
				return StringUtils.equals(properties.get("column_" + name + "_md"), "0") ? "" : name + "-md-" + properties.get("column_" + name + "_md");
			}
			return getColumn_property_sm(properties, name).replace("sm", "md");
		}
		return "";
	}

	public static String getColumn_property_sm(HashMap<String, String> properties, String name)
	{
		if (StringUtils.isNotBlank(properties.get("column_" + name + "_sm")))
		{
			if (!StringUtils.equals(properties.get("column_" + name + "_sm"), "inherit"))
			{
				return StringUtils.equals(properties.get("column_" + name + "_sm"), "0") ? "" : name + "-sm-" + properties.get("column_" + name + "_sm");
			}
			String property_xs = getColumn_property_xs(properties, name);
			return StringUtils.isBlank(property_xs) ? "" : property_xs.substring(0, property_xs.indexOf("-")) + "-sm" + property_xs.substring(property_xs.indexOf("-"));
		}
		return "";
	}

	public static String getColumn_property_xs(HashMap<String, String> properties, String name)
	{
		if (StringUtils.isNotBlank(properties.get("column_" + name + "_xs")))
		{
			return StringUtils.equals(properties.get("column_" + name + "_xs"), "0") ? "" : name + "-xs-" + properties.get("column_" + name + "_xs");
		}
		return "";
	}

	public static String getCSSLength(Object value)
	{
		try
		{
			if (value.getClass().equals(String.class))
				Double.parseDouble((String) value);
			return value + "px";
		}
		catch (Exception e)
		{
			return value + "";
		}
	}
}
