package au.corporateinteractive.utils;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import au.corporateinteractive.qcloud.spreadsheet.model.Cell;
import au.corporateinteractive.qcloud.spreadsheet.model.Column;
import au.corporateinteractive.qcloud.spreadsheet.model.Row;
import au.corporateinteractive.qcloud.spreadsheet.model.Sheet;
import au.corporateinteractive.qcloud.spreadsheet.model.Spreadsheet;

public class KendoSpreadsheetUtils
{
	private static Logger logger = Logger.getLogger(KendoSpreadsheetUtils.class);

	private static final int MAX_WIDTH = 800;

	public static void renderSpreadsheet(Map<String, Object> spreadsheet, Writer out) throws IOException
	{
		Gson gson = new Gson();
		Spreadsheet spread = gson.fromJson(gson.toJson(spreadsheet), Spreadsheet.class);
		render(spread.getSheets().get(0), getDefaultColumnWidth(spread), getDefaultRowHeight(spread), out);
	}

	private static int getDefaultColumnWidth(Spreadsheet spreadsheet)
	{
		return spreadsheet.getColumnWidth();
	}

	private static int getDefaultRowHeight(Spreadsheet spreadsheet)
	{
		return spreadsheet.getRowHeight();
	}


	private static void render(Sheet sheet, int defaultColumnWidth, int defaultRowHeight, Writer out) throws IOException
	{
		insertBlankCells(sheet);
		for (String mergedCell : sheet.getMergedCells())
		{
			mergeCell(sheet, mergedCell, defaultColumnWidth);
		}
		out.write("<table class=\"k-spreadsheet\"><tbody>\n");
		for (Row row : sheet.printableRows())
		{
			if (StringUtils.isBlank(row.getHidden()))
				render(row, defaultColumnWidth, defaultRowHeight, sheet.getColumns(), sheet.getPrintableColumnsAmount(), out);
		}
		out.write("</tbody></table>\n");
	}

	private static void render(Row row, double defaultColumnWidth, double defaultRowHeight, List<Column> columns, int columnsLimit, Writer out) throws IOException
	{
		double rowHeight = row.getHeight() > 0 ? row.getHeight() : defaultRowHeight;
		out.write("<tr style=\"height:" + rowHeight + "px;\">\n");
		for (Cell cell : row.getCells(columnsLimit))
		{
			boolean isHidden = false;
			for (Column column : columns)
			{
				if (column.getIndex() == cell.getIndex() && (StringUtils.isNotBlank(column.getHidden()) || cell.isHidden()))
				{
					isHidden = true;
				}
			}
			if (!isHidden)
			{
				double columnWidth = getColumnWidth(columns, cell.getIndex(), defaultColumnWidth);
				out.write(cell.render(columnWidth, rowHeight));
			}
		}
		out.write("</tr>\n");
	}

	private static double getColumnWidth(List<Column> columns, int index, double defaultColumnWidth)
	{
		for (Column column : columns)
		{
			if (column.getIndex() == index)
			{
				return column.getWidth();
			}
		}
		return defaultColumnWidth;
	}

	private static void mergeCell(Sheet sheet, String mergedCell, int defaultColumnWidth)
	{
		String[] mCells = StringUtils.split(mergedCell, ":");
		int[] start = cellIndexes(mCells[0]);
		int[] end = cellIndexes(mCells[1]);
		Cell showCell = null;
		for (int row = start[0]; row <= end[0]; row++)
		{
			for (int column = start[1]; column <= end[1]; column++)
			{
				if (column < sheet.getRows().get(row).getCells().size())
				{
					Cell cell = sheet.cellAt(row, column);
					if (row == start[0] && column == start[1])
					{
						showCell = cell;
						cell.setRowspan(end[0] - start[0] + 1);
						cell.setColspan(end[1] - start[1] + 1);
					}
					else
					{
						cell.setHidden(true);
					}
					//Make sure it is the first row
					if (row == start[0])
					{
						double columnWidth = getColumnWidth(sheet.getColumns(), cell.getIndex(), defaultColumnWidth);
						showCell.setWidth(showCell.getWidth() + columnWidth);
					}
					}
				}
			}

	}

	private static void insertBlankCells(Sheet sheet)
	{
		List<Integer> cellIndexes = null;
		List<Integer> rowIndexes = new ArrayList<Integer>();
		sheet.countPrintableRows();
		for (Row row : sheet.getRows())
		{
			rowIndexes.add(row.getIndex());
		}
		for (Integer x = 0; x < sheet.getPrintableRowsAmount(); x++)
		{
			List<Row> rows = sheet.getRows();
			if (!rowIndexes.contains(x))
			{
				Row newRow = new Row();
				newRow.setIndex(x);
				newRow.setCells(new ArrayList<Cell>());
				rows.add(x, newRow);
			}
		}
		for (Row row : sheet.printableRows())
		{
			cellIndexes = new ArrayList<Integer>();
			List<Cell> cells = row.getCells();
			for (Cell cell : row.getCells())
			{
				cellIndexes.add(cell.getIndex());
			}
			for (Integer i = 0; i < sheet.getPrintableColumnsAmount(); i++)
			{
				if (!cellIndexes.contains(i))
				{
					Cell cell = new Cell();
					cell.setIndex(i);
					cells.add(i, cell);
				}
			}
		}
	}

	public static int[] cellIndexes(String cellName)
	{
		int row = Integer.parseInt(cellName.replaceAll("[a-zA-Z]", "")) - 1;
		int column = columnString2Int(cellName.replaceAll("[0-9]", ""));
		return new int[] { row, column };
	}

	public static int columnString2Int(String columnName)
	{
		String stub = "-ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		char[] chars = new StringBuilder(columnName).reverse().toString().toUpperCase().toCharArray();
		int index = 0;
		for (int i = 0; i < chars.length; i++)
		{
			int at = stub.indexOf(chars[i]);
			if (at < 1)
			{
				throw new IllegalArgumentException("Invalid column name:" + columnName);
			}
			index += Math.pow(26, i) * at;
		}
		return index - 1;
	}

}
