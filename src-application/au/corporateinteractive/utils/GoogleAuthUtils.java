package au.corporateinteractive.utils;

import java.util.Hashtable;
import java.util.List;

import com.warrenstrange.googleauth.ICredentialRepository;

import au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;

public class GoogleAuthUtils implements ICredentialRepository
{
	@Override public String getSecretKey(String username)
	{
		UserAccountDataAccess uada = new UserAccountDataAccess();
		Hashtable<String, String> userData = uada.getUserWithId(username);
		return userData.get("ATTR_totpSecret");
	}



	@Override public void saveUserCredentials(String username, String key, int validationCodes, List<Integer> scratchCodes)
	{
		UserAccountDataAccess uada = new UserAccountDataAccess();
		ElementData userData = new ElementData();
		Hashtable<String, String> userHash = uada.getUserWithId(username);
		userData.setId(userHash.get("Element_id"));
		userData.put("ATTR_totpSecret", key);
		try(TransactionManager txManager = TransactionManager.getInstance()) {
			TransactionModuleAccess tma = TransactionModuleAccess.getInstance(txManager);
			tma.updateElement(userData, UserAccountDataAccess.USER_MODULE);
			txManager.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
