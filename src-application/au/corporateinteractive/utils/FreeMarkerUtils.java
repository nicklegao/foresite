package au.corporateinteractive.utils;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import freemarker.core.TemplateElement;
import freemarker.template.Template;

public class FreeMarkerUtils extends FreeMarkerTemplateUtils {
	public static List<String> extractTags(Template template)
	{
		List<String> list = new ArrayList<String>();
		
		TemplateElement te = template.getRootTreeNode();
		Enumeration en = te.children();
		while (en.hasMoreElements()) {
			Object object = (Object) en.nextElement();
			String clazz = object.getClass().getName();
			if (clazz.endsWith(".DollarVariable"))
			{
				String token = object.toString();
				list.add(token.substring(2, token.length()-1));
			}
		}

		
		return list;
	}
}
