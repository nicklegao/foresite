package au.corporateinteractive.service;

import java.io.IOException;
import java.util.Collection;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Jaysun
 *
 */
public class RecaptchaService
{
	private static final Logger logger = Logger.getLogger(RecaptchaService.class);

	private final RestTemplate restTemplate;

	private String recaptchaMode;
	private String recaptchaUrl;
	private String recaptchaSecretKey;
	private String recaptchaPublicKey;
	private String[] recaptchaSites;

	private static RecaptchaService instance;
	private static Object _sharedLock_ = new Object();

	public static RecaptchaService sharedInstance()
	{
		if (instance == null)
		{
			synchronized (_sharedLock_)
			{
				if (instance == null)
				{
					instance = new RecaptchaService();
				}
			}
		}
		return instance;
	}

	private RecaptchaService()
	{
		logger.info("Creating singleton of class: " + getClass().getName());

		this.restTemplate = new RestTemplate();

		try
		{
			Resource resource = new ClassPathResource("/config/google/recaptcha.properties");
			Properties props = PropertiesLoaderUtils.loadProperties(resource);
			recaptchaMode = props.getProperty("recaptcha.mode");
			recaptchaUrl = props.getProperty(recaptchaMode + ".recaptcha.url");
			recaptchaSecretKey = props.getProperty(recaptchaMode + ".recaptcha.secret-key");
			recaptchaPublicKey = props.getProperty(recaptchaMode + ".recaptcha.public-key");
			recaptchaSites = StringUtils.split(props.getProperty(recaptchaMode + ".sites"), ",");
		}
		catch (IOException e)
		{
			logger.error("Problem with recaptcha.properties file. Should be in path /config/google/recaptcha.properties");
			logger.error(e.getMessage());
		}
	}

	public boolean isDisabled(HttpServletRequest request)
	{
		if (recaptchaSites == null || recaptchaSites.length == 0)
		{
			return false;
		}
		for (String site : recaptchaSites)
		{
			if (site.indexOf(request.getServerName()) > -1)
			{
				return false;
			}
		}
		return true;
	}

	public boolean isResponseValid(HttpServletRequest request, String response)
	{
		if (isDisabled(request))
		{
			return true;
		}
		RecaptchaResponse recaptchaResponse;

		recaptchaResponse = restTemplate.postForEntity(
				recaptchaUrl, createBody(recaptchaSecretKey, request.getRemoteHost(), response), RecaptchaResponse.class)
				.getBody();

		return recaptchaResponse.success;
	}

	public String getRecaptchaPublicKey()
	{
		return this.recaptchaPublicKey;
	}

	private MultiValueMap<String, String> createBody(String secret, String remoteIp, String response)
	{
		MultiValueMap<String, String> form = new LinkedMultiValueMap<>();
		form.add("secret", secret);
		form.add("remoteip", remoteIp);
		form.add("response", response);
		return form;
	}

	private static class RecaptchaResponse
	{
		@JsonProperty("success")
		private boolean success;
		@JsonProperty("error-codes")
		private Collection<String> errorCodes;
	}

}