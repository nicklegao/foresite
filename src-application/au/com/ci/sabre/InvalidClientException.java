package au.com.ci.sabre;

import org.apache.log4j.Logger;

public class InvalidClientException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5825639592761487386L;
	private static Logger logger = Logger.getLogger(InvalidClientException.class);

	public InvalidClientException()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InvalidClientException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidClientException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidClientException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}


}
