package au.com.ci.sabre;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.log4j.Logger;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

public class SabreClient
{
	protected static Logger logger = Logger.getLogger(SabreClient.class);

	protected static final String version = "V1";
	protected static final String baseUrl = "https://api-crt.cert.havail.sabre.com";
//	protected static final String baseUrl = "https://api.havail.sabre.com";

	// https://beta.developer.sabre.com/docs/travel-agency/guides/how-to/rest-apis-token-credentials

	protected String password; //Sabre APIs password.

	protected RestTemplate restTemplate = new RestTemplate();

	protected String token;

	protected String clientId;

	public SabreClient(String clientId, String password) throws InvalidClientException
	{
		super();
		this.clientId = clientId;
		this.password = password;
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory()
		{
			@Override
			protected HttpUriRequest createHttpUriRequest(HttpMethod httpMethod, URI uri)
			{
				if (HttpMethod.DELETE == httpMethod)
				{
					return new HttpEntityEnclosingDeleteRequest(uri);
				}
				return super.createHttpUriRequest(httpMethod, uri);
			}
		});
		token = getToken();
	}

	public SabreClient(String userId, String group, String domain, String password) throws InvalidClientException
	{
		this(StringUtils.join(new String[] { version, userId, group, domain }, ":"), password);

	}

	protected String constructTokenCredentials()
	{
		String token = new String(Base64.encodeBase64(clientId.getBytes())) + ":" + new String(Base64.encodeBase64(password.getBytes()));
		return new String(Base64.encodeBase64(token.getBytes()));
	}

	public String getToken() throws InvalidClientException
	{
		String credentials = constructTokenCredentials();
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + credentials);
		headers.add("Content-Type", "application/x-www-form-urlencoded");
		headers.add("grant_type", "client_credentials");
		HttpEntity<String> request = new HttpEntity<String>(headers);
		try
		{
			ResponseEntity<Map<String, String>> result = restTemplate.exchange(baseUrl + "/v2/auth/token", HttpMethod.POST, request, new ParameterizedTypeReference<Map<String, String>>()
			{
			});
			return result.getBody().get("access_token");
		}
		catch (HttpStatusCodeException e)
		{
			logger.error("error", e);
			if (e.getRawStatusCode() == 401)
				throw new InvalidClientException(e.getMessage() + " : " + e.getResponseBodyAsString(), e);
			throw new RuntimeException(e.getMessage() + " : " + e.getResponseBodyAsString());
		}
	}

	public Map<String, Object> post(String url, Map<String, Object> body)
	{
		try
		{
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", "Bearer " + token);
			headers.add("Content-Type", "application/json");
			HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(body, headers);
			ResponseEntity<Map<String, Object>> result = restTemplate.exchange(baseUrl + url, HttpMethod.POST, request, new ParameterizedTypeReference<Map<String, Object>>()
			{
			});
			return result.getBody();
		}
		catch (HttpStatusCodeException e)
		{
			logger.error(e.getMessage() + " : " + e.getResponseBodyAsString(), e);
			throw e;
		}
	}

	public Map<String, Object> get(String url) throws InvalidClientException
	{
		try
		{
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", "Bearer " + token);
			HttpEntity<String> request = new HttpEntity<String>(headers);
			ResponseEntity<Map<String, Object>> result = restTemplate.exchange(baseUrl + url, HttpMethod.GET, request, new ParameterizedTypeReference<Map<String, Object>>()
			{
			});
			return result.getBody();
		}
		catch (HttpStatusCodeException e)
		{
			logger.error(e.getMessage() + " : " + e.getResponseBodyAsString(), e);
			throw e;
		}
	}

	public static void main(String[] args) throws InvalidClientException
	{
		SabreClient client = new SabreClient("V1:415263:ZG9J:AA", "WS493182");
//		SabreClient client = new SabreClient("V1:wyu4n33qo9duxubg:DEVCENTER:EXT", "lYi7V8Nj");
		//lookup(client);
//		lookupAirport(client, "BJS");
		// preview(client);
		lookupCountry(client, "AU");

	}

	private static void lookupCountry(SabreClient client, String code) throws InvalidClientException
	{
		System.out.println(client.get("/v1/lists/supported/countries?pointofsalecountry=" + code));
	}

	private static void lookup(SabreClient client) throws InvalidClientException
	{
		System.out.println(client.get("/v1/lists/supported/cities"));
	}

	private static void preview(SabreClient client)
	{
		Map<String, Object> body = new LinkedHashMap<>();
		Map<String, Object> params = new LinkedHashMap<>();
		body.put("method", "getTripPreview");
		body.put("id", "1");
		params.put("pnr", "TJUYFB");
		params.put("language", "en");
		params.put("timeFormat", "24");
		body.put("params", params);
		body.put("jsonrpc", "2.0");
		System.out.println(client.post("/v1/document/preview", body));

	}

	private static void lookupAirport(SabreClient client, String airportCode)
	{
		Map<String, Object> body = new LinkedHashMap<>();

		Map<String, Object> GeoSearchRQ = new LinkedHashMap<>();
		Map<String, Object> GeoRef = new LinkedHashMap<>();
		GeoRef.put("AirportCode", airportCode);
		GeoRef.put("Category", "AIR");
		GeoRef.put("UOM", "KM");
		GeoRef.put("Radius", 1);
		body.put("GeoSearchRQ", GeoSearchRQ);
		GeoSearchRQ.put("version", "1");
		GeoSearchRQ.put("GeoRef", GeoRef);
		System.out.println(client.post("/v1.0.0/lists/utilities/geosearch/locations?mode=geosearch", body));

	}


	public static class HttpEntityEnclosingDeleteRequest extends HttpEntityEnclosingRequestBase
	{

		public HttpEntityEnclosingDeleteRequest(final URI uri)
		{
			super();
			setURI(uri);
		}

		@Override
		public String getMethod()
		{
			return "DELETE";
		}
	}
}
