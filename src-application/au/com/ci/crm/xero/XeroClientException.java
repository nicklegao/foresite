/*
 *  Copyright 2011 Ross Jourdain
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package au.com.ci.crm.xero;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import net.oauth.OAuthProblemException;

import org.w3c.dom.Element;

import au.com.ci.crm.xero.model.ApiException;
import au.com.ci.crm.xero.model.ApiExceptionExtended;
import au.com.ci.crm.xero.model.Contact;
import au.com.ci.crm.xero.model.DataContractBase;
import au.com.ci.crm.xero.model.Invoice;
import au.com.ci.crm.xero.model.ResponseType;
import au.com.ci.crm.xero.model.ValidationError;
import au.com.ci.crm.xero.model.Warning;

/**
 * 
 * @author ross
 */
public class XeroClientException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -263872001250564755L;
	private ApiExceptionExtended apiException;
	private List<ExceptionDetail> details = new ArrayList<ExceptionDetail>();

	public XeroClientException(String message, OAuthProblemException oAuthProblemException)
	{

		super(message, oAuthProblemException);

		String oAuthProblemExceptionString = null;

		oAuthProblemExceptionString = XeroXmlManager.xmlFromOAuthProblemException(oAuthProblemException);
		apiException = (ApiExceptionExtended) XeroXmlManager.xmlToException(oAuthProblemExceptionString);
		unmarshalAdditionalData();

		/* Add this back in if you need more details on the exception */
		// sb.append("\n").append("" + oAuthProblemExceptionString);
		// sb.append("\n").append("");
	}

	public ApiException getApiException()
	{
		return apiException;
	}


	public List<ExceptionDetail> getDetails()
	{
		return details;
	}

	@SuppressWarnings("restriction")
	private void unmarshalAdditionalData()
	{

		List<Object> list = (List<Object>) apiException.getElements().getDataContractBase();
		for (Object o : list)
		{
			try
			{
				Element e = (Element) o;
				details.add(new ExceptionDetail(e));
			}
			catch (JAXBException ex)
			{
				ex.printStackTrace();
			}
		}
	}

	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(this.getMessage());
		for (ExceptionDetail detail : details)
		{
			sb.append(detail.toString());
		}
		return sb.toString();
	}

	public class ExceptionDetail
	{
		private List<ValidationError> validationErrors;
		private List<Warning> warnings;
		private Object modelObject;

		private ExceptionDetail(Element e) throws JAXBException
		{
			String elementType = e.getAttribute("xsi:type");

			JAXBContext context = JAXBContext.newInstance(ResponseType.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();

			// unmarshaller.setEventHandler(new
			// DefaultValidationEventHandler());

			JAXBElement jaxbElement = null;

			if ("Invoice".equals(elementType))
			{
				jaxbElement = unmarshaller.unmarshal(e, Invoice.class);
				modelObject = (Invoice) jaxbElement.getValue();
			}
			else if ("Contact".equals(elementType))
			{
				jaxbElement = unmarshaller.unmarshal(e, Contact.class);
				modelObject = (Contact) jaxbElement.getValue();
			}
			else
			{
				throw new RuntimeException("Unrecognised type: " + elementType);
			}

			if (jaxbElement != null)
			{
				DataContractBase dataContractBase = (DataContractBase) jaxbElement.getValue();

				if (dataContractBase.getWarnings() != null && dataContractBase.getWarnings().getWarning() != null)
				{
					warnings = dataContractBase.getWarnings().getWarning();
				}
				if (dataContractBase.getValidationErrors() != null && dataContractBase.getValidationErrors().getValidationError() != null)
				{
					validationErrors = dataContractBase.getValidationErrors().getValidationError();
				}
			}

		}

		public List<ValidationError> getValidationErrors()
		{
			return validationErrors;
		}

		public List<Warning> getWarnings()
		{
			return warnings;
		}

		public Object getModelObject()
		{
			return modelObject;
		}

		public String toString()
		{
			StringBuffer sb = new StringBuffer();
			sb.append("\n").append("Message: " + apiException.getMessage());

			if (warnings != null)
			{
				for (int i = 0; i < warnings.size(); i++)
				{
					Warning warning = warnings.get(i);
					sb.append("\n").append("Warning " + (i + 1) + ": " + warning.getMessage());
				}
			}

			if (validationErrors != null)
			{
				for (int i = 0; i < validationErrors.size(); i++)
				{
					ValidationError validationError = validationErrors.get(i);
					sb.append("\n").append("Validation Error " + (i + 1) + ": " + validationError.getMessage());
				}
			}

			sb.append("\n").append("Error " + apiException.getErrorNumber() + ": " + apiException.getType());
			if (modelObject instanceof Invoice)
			{
				Invoice invoice = (Invoice) modelObject;
				sb.append("\n").append("Invoice Number: " + invoice.getInvoiceNumber());
				if (invoice.getDate() != null)
				{
					sb.append("\n").append("Invoice Date: " + invoice.getDate().getTime());
				}
			}
			else if (modelObject instanceof Contact)
			{
				Contact contact = (Contact) modelObject;
				sb.append("\n").append("Contact ID: " + contact.getContactID());
				sb.append("\n").append("Contact Number: " + contact.getContactNumber());
			}
			else
			{
				sb.append("\n").append("Unrecognised type: " + modelObject);
			}
			return sb.toString();
		}
	}
}
