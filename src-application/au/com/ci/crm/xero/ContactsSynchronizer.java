package au.com.ci.crm.xero;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.ci.crm.xero.XeroClientException.ExceptionDetail;
import au.com.ci.crm.xero.model.Address;
import au.com.ci.crm.xero.model.AddressType;
import au.com.ci.crm.xero.model.ArrayOfAddress;
import au.com.ci.crm.xero.model.ArrayOfContact;
import au.com.ci.crm.xero.model.ArrayOfContactPerson;
import au.com.ci.crm.xero.model.Contact;
import au.com.ci.crm.xero.model.ContactPerson;
import au.com.ci.crm.xero.model.CurrencyCode;
import au.corporateinteractive.qcloud.subscription.SubscriptionPlan;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;

public class ContactsSynchronizer extends ObjectSynchronizer<Contact>
{

	public ContactsSynchronizer(Logger logger, String countryCode)
	{
		super(logger, countryCode);
	}

	@Override
	protected List<Map<String, String>> getRecordsFromDB(Date date)
	{
		return DataAccess.getInstance().select("select c.* from categories_useraccounts c where c.ATTRDROP_pricingPlan = 'STARTER'", new String[] {}, new HashtableMapper());
	}

	@Override
	protected List<Contact> getRecordsFromXero(Date date) throws XeroClientUnexpectedException
	{
		List<Contact> list = new ArrayList<Contact>();
		try
		{
			ArrayOfContact array = xeroClient.getContacts(date);
			if (array != null && array.getContact() != null)
			{
				for (Contact contact : array.getContact())
				{
					logger.info(contact.getAccountNumber());
				}
				return array.getContact();
			}
			return list;
		}
		catch (XeroClientException e)
		{
			return list;
		}
	}

	@Override
	protected String getID(Contact contact)
	{
		return contact.getAccountNumber();
	}

	@Override
	protected String getID(Map<String, String> record)
	{
		return "TD" + record.get("Category_id");
	}

	@Override
	protected List<String[]> getFailedResultFromException(XeroClientException e)
	{
		List<String[]> result = new ArrayList<String[]>();
		List<ExceptionDetail> details = e.getDetails();
		for (ExceptionDetail detail : details)
		{
			Contact contact = (Contact) detail.getModelObject();
			result.add(new String[] { contact.getContactNumber(), detail.toString() });
		}
		return result;
	}

	@Override
	protected void insertToXero(List<Map<String, String>> records) throws XeroClientException, XeroClientUnexpectedException
	{
		logger.info("insert contracts: " + records);
		ArrayOfContact array = mapsToContacts(records);
		if (array.getContact().size() > 0)
		{
			xeroClient.postContacts(array);
		}
	}

	@Override
	protected void updateToXero(List<Map<String, String>> records) throws XeroClientException, XeroClientUnexpectedException
	{
		ArrayOfContact array = mapsToContacts(records);
		if (array.getContact().size() > 0)
		{
			xeroClient.postContacts(array);
		}
	}

	@Override
	protected void deleteFromXero(List<Contact> needToBeDeleted) throws XeroClientException, XeroClientUnexpectedException
	{
	}

	public ArrayOfContact mapsToContacts(List<Map<String, String>> records)
	{

		ArrayOfContact array = new ArrayOfContact();
		List<Contact> contacts = array.getContact();
		for (Map<String, String> record : records)
		{
			Contact contact = mapToContact(record);
			if (contact == null)
			{
				logger.warn("Cannot convert to contract: " + record);
				continue;
			}
			contacts.add(contact);
		}
		return array;
	}

	public Contact mapToContact(Map<String, String> record)
	{
		String sql = "select e.* from elements_useraccounts e, categories_useraccounts c where e.category_id = c.category_id and c.category_parentid = ? order by element_id limit 1";
		List<Map<String, String>> details = DataAccess.getInstance().select(sql, new String[] { record.get("Category_id") }, new HashtableMapper());
		if (details.size() == 0)
		{
			logger.warn("Cannot find record: " + sql + " [" + record.get("Category_id") + "]");
			return null;
		}

		Contact contact = new Contact();
		if (StringUtils.isNotBlank(getKey(record)))
		{
			contact.setContactID(getKey(record));
		}
		contact.setContactNumber(getID(record));
		contact.setAccountNumber(getID(record));
		contact.setName(record.get("ATTR_categoryName") + " (" + getID(record) + ")");
		contact.setContactStatus("ACTIVE");
		contact.setTaxNumber(record.get("ATTR_abnacn"));
		CurrencyCode currency = CurrencyCode.fromValue(SubscriptionPlan.getCurrencyCode(record.get("ATTRDROP_country")));
		contact.setDefaultCurrency(currency);
		ArrayOfContactPerson ac = new ArrayOfContactPerson();
		contact.setContactPersons(ac);
		Map<String, String> mainPerson = details.get(0);
		for (Map<String, String> person : details)
		{
			ContactPerson contactPerson = new ContactPerson();
			ac.getContactPerson().add(contactPerson);
			contactPerson.setFirstName(person.get("ATTR_name"));
			contactPerson.setLastName(person.get("ATTR_surname"));
			contactPerson.setEmailAddress(person.get("ATTR_EmailAddress"));
		}

		contact.setLastName(mainPerson.get("ATTR_name"));
		contact.setFirstName(mainPerson.get("ATTR_surname"));
		contact.setEmailAddress(mainPerson.get("ATTR_EmailAddress"));

		{// add addresses
			ArrayOfAddress addresses = new ArrayOfAddress();
			contact.setAddresses(addresses);

			Address address = new Address();
			addresses.getAddress().add(address);

			address.setAddressType(AddressType.STREET);
			address.setAddressLine1(record.get("ATTR_addressLine1") + " " + record.get("ATTR_addressLine2"));
			address.setCity(record.get("ATTR_citySuburb"));
			address.setRegion(record.get("ATTR_state"));
			address.setPostalCode(record.get("ATTR_postcode"));
			address.setCountry(record.get("ATTRDROP_country"));

			address = new Address();
			addresses.getAddress().add(address);

			address.setAddressType(AddressType.POBOX);
			address.setAddressLine1(record.get("ATTR_addressLine1") + " " + record.get("ATTR_addressLine2"));
			address.setCity(record.get("ATTR_citySuburb"));
			address.setRegion(record.get("ATTR_state"));
			address.setPostalCode(record.get("ATTR_postcode"));
			address.setCountry(record.get("ATTRDROP_country"));
		}

		return contact;
	}

	@Override
	protected List<Map<String, String>> markAsSynced(List<Map<String, String>> db, List<String[]> deleteFailed, List<String[]> insertFailed, List<String[]> updateFailed) throws XeroClientUnexpectedException
	{
		List<Map<String, String>> ret = new ArrayList<Map<String, String>>();
		for (Map<String, String> record : db)
		{
			String key = "TD" + record.get("Category_id");
			if (containsId(key, deleteFailed))
			{
				continue;
			}
			if (containsId(key, insertFailed))
			{
				continue;
			}
			if (containsId(key, updateFailed))
			{
				continue;
			}
			ret.add(record);
		}
		return ret;
	}

	@Override
	protected String getName()
	{
		return "Contacts synchroniser";
	}

	@Override
	protected String getName(Map<String, String> record)
	{
		return record.get("ATTR_categoryName") + "(" + getID(record) + ")";
	}

	@Override
	protected String getKey(Contact record)
	{
		return record.getContactID();
	}

	@Override
	protected String getKey(Map<String, String> record)
	{
		return record.get("Contact_id");
	}

	@Override
	protected void setKey(Map<String, String> record, String value)
	{
		record.put("Contact_id", value);
	}
}
