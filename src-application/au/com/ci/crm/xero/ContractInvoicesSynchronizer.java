package au.com.ci.crm.xero;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.ci.crm.xero.model.ArrayOfLineItem;
import au.com.ci.crm.xero.model.Contact;
import au.com.ci.crm.xero.model.Invoice;
import au.com.ci.crm.xero.model.InvoiceStatus;
import au.com.ci.crm.xero.model.InvoiceType;
import au.com.ci.crm.xero.model.LineItem;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;

public class ContractInvoicesSynchronizer extends InvoicesSynchronizer
{
	public static final int DAY_STARTING_NEW_BILLING_CYCLE = 25;

	public ContractInvoicesSynchronizer(Logger logger, String countryCode)
	{
		super(logger, countryCode);
	}

	@Override
	protected List<Map<String, String>> getRecordsFromDB(Date date)
	{

		String sql = "select i.* from categories_bill_invoices i where i.ATTR_SyncStatus is null";
		if (date != null)
		{
			sql += " and i.ATTRDATE_OrderDate >= '" + new SimpleDateFormat("yyyy-MM-dd") + "'";
		}
		return (List<Map<String, String>>) DataAccess.getInstance().select(sql, new String[] {}, new HashtableMapper());
	}

	private List<Map<String, String>> getLineItemsFromDB(Map<String, String> record)
	{
		String id = record.get("Category_id");
		String sql = "select * from elements_bill_invoices where category_id = ?";
		return (List<Map<String, String>>) DataAccess.getInstance().select(sql, new Object[] { id }, new HashtableMapper());
	}

	@Override
	protected String getID(Map<String, String> record)
	{
		return record.get("ATTR_OrderNumber");
	}

	public Invoice mapToInvoice(Map<String, String> record)
	{
		List<Map<String, String>> items = getLineItemsFromDB(record);
		Invoice invoice = new Invoice();
		if (StringUtils.isNotBlank(getKey(record)))
		{
			invoice.setInvoiceID(getKey(record));
		}

		Contact contact = new Contact();
		invoice.setReference(record.get("ATTR_AccountReference"));
		invoice.setSentToContact(Boolean.TRUE);
		invoice.setContact(contact);

		contact.setContactNumber(record.get("ATTR_AccountReference"));


		ArrayOfLineItem array = new ArrayOfLineItem();
		invoice.setLineItems(array);

		List<LineItem> lineItems = array.getLineItem();
		for (Map<String, String> item : items)
		{
			String accountCode = getAccountCode(item);
			LineItem lineItem = new LineItem();
			lineItems.add(lineItem);
			lineItem.setAccountCode(accountCode);
			BigDecimal qty = new BigDecimal(item.get("ATTR_OrderQty"));
			lineItem.setQuantity(qty);
			BigDecimal amnt = new BigDecimal(item.get("ATTRCURRENCY_UnitPrice"));
			lineItem.setUnitAmount(amnt);
			lineItem.setDescription(item.get("ATTR_Headline"));
			lineItem.setLineAmount(qty.multiply(amnt));
			lineItem.setTaxAmount(qty.multiply(amnt).multiply(new BigDecimal(0.1)));
		}

		Calendar orderDate = getCalendar(record.get("ATTRDATE_OrderDate"));
		invoice.setDate(orderDate);
		invoice.setInvoiceNumber(getID(record));

		Calendar due = getCalendar(record.get("ATTRDATE_OrderDate"));
		if (due.get(Calendar.DAY_OF_MONTH) >= DAY_STARTING_NEW_BILLING_CYCLE)
			due.add(Calendar.MONTH, 2);
		else
			due.add(Calendar.MONTH, 1);
		due.set(Calendar.DATE, 11);
		invoice.setDueDate(due);
		invoice.getLineAmountTypes().add("Inclusive");
		invoice.setType(InvoiceType.ACCREC);
		invoice.setStatus(InvoiceStatus.AUTHORISED);

		return invoice;
	}

	@Override
	protected String getAccountCode(Map<String, String> item)
	{
		if (StringUtils.equals("4002", item.get("ATTR_nominalCode")))
		{
			return "202";
		}
		if (StringUtils.equals("4003", item.get("ATTR_nominalCode")))
		{
			return "203";
		}
		return "260";
	}

	@Override
	protected List<Map<String, String>> markAsSynced(List<Map<String, String>> db, List<String[]> deleteFailed, List<String[]> insertFailed, List<String[]> updateFailed) throws XeroClientUnexpectedException
	{
		List<Map<String, String>> ret = new ArrayList<Map<String, String>>();
		for (Map<String, String> record : db)
		{
			String key = "Category_id";
			if (containsId(getID(record), deleteFailed))
			{
				continue;
			}
			if (containsId(getID(record), insertFailed))
			{
				continue;
			}
			if (containsId(getID(record), updateFailed))
			{
				continue;
			}
			new DBaccess().updateData("update categories_bill_invoices set ATTR_SyncStatus = 'Synced' where " + key + " = '" + record.get(key) + "'");
			ret.add(record);
		}
		return ret;
	}

	@Override
	protected String getName()
	{
		return "Contract invoices synchroniser";
	}
}
