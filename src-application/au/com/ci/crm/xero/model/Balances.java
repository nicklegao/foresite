//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.07.21 at 11:41:58 AM EST 
//


package au.com.ci.crm.xero.model;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Balances complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Balances">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="AccountsReceivable" type="{}AccountsReceivable" minOccurs="0"/>
 *         &lt;element name="AccountsPayable" type="{}AccountsPayable" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Balances", propOrder = {

})
public class Balances {

    @XmlElementRef(name = "AccountsReceivable", type = JAXBElement.class)
    protected JAXBElement<AccountsReceivable> accountsReceivable;
    @XmlElementRef(name = "AccountsPayable", type = JAXBElement.class)
    protected JAXBElement<AccountsPayable> accountsPayable;

    /**
     * Gets the value of the accountsReceivable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccountsReceivable }{@code >}
     *     
     */
    public JAXBElement<AccountsReceivable> getAccountsReceivable() {
        return accountsReceivable;
    }

    /**
     * Sets the value of the accountsReceivable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccountsReceivable }{@code >}
     *     
     */
    public void setAccountsReceivable(JAXBElement<AccountsReceivable> value) {
        this.accountsReceivable = ((JAXBElement<AccountsReceivable> ) value);
    }

    /**
     * Gets the value of the accountsPayable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccountsPayable }{@code >}
     *     
     */
    public JAXBElement<AccountsPayable> getAccountsPayable() {
        return accountsPayable;
    }

    /**
     * Sets the value of the accountsPayable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccountsPayable }{@code >}
     *     
     */
    public void setAccountsPayable(JAXBElement<AccountsPayable> value) {
        this.accountsPayable = ((JAXBElement<AccountsPayable> ) value);
    }

}
