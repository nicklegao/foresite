package au.com.ci.crm.xero;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.stripe.model.InvoiceLineItem;
import com.stripe.net.APIResource;

import au.com.ci.crm.xero.model.ArrayOfLineItem;
import au.com.ci.crm.xero.model.Contact;
import au.com.ci.crm.xero.model.CurrencyCode;
import au.com.ci.crm.xero.model.Invoice;
import au.com.ci.crm.xero.model.InvoiceStatus;
import au.com.ci.crm.xero.model.InvoiceType;
import au.com.ci.crm.xero.model.LineItem;
import au.corporateinteractive.qcloud.proposalbuilder.db.InvoiceDataAccess;
import au.corporateinteractive.qcloud.proposalbuilder.model.bill.TDInvoice;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;

public class TDInvoicesSynchronizer extends InvoicesSynchronizer
{
	public static final int DAY_STARTING_NEW_BILLING_CYCLE = 25;

	public TDInvoicesSynchronizer(Logger logger, String countryCode)
	{
		super(logger, countryCode);
	}

	@Override
	protected List<Map<String, String>> getRecordsFromDB(Date date)
	{

		String sql = "select e.*, c.attrdrop_companyid, u.ATTRDROP_country from elements_invoices e, categories_invoices c, categories_useraccounts u where u.category_id = c.attrdrop_companyid and e.category_id = c.category_id and e.attr_stripeid like 'TD_%' and e.attr_syncstatus is null";
		if (date != null)
		{
			sql += " and e.ATTRDATE_Date >= '" + new SimpleDateFormat("yyyy-MM-dd") + "'";
		}
		return DataAccess.getInstance().select(sql, new String[] {}, new HashtableMapper());
	}

	@Override
	protected String getID(Map<String, String> record)
	{
		return "TD_" + record.get("ATTR_Headline");
	}

	@Override
	public Invoice mapToInvoice(Map<String, String> record)
	{
		TDInvoice tdInvoice = APIResource.GSON.fromJson(record.get(InvoiceDataAccess.E_INVOICE), TDInvoice.class);
		com.stripe.model.Invoice invoiceStripe = tdInvoice.getInvoice();
		Invoice invoice = new Invoice();
		if (StringUtils.isNotBlank(getKey(record)))
		{
			invoice.setInvoiceID(getKey(record));
		}
		invoice.setCurrencyCode(CurrencyCode.fromValue(invoiceStripe.getCurrency().toUpperCase()));

		Contact contact = new Contact();
		invoice.setReference("TD" + record.get("attrdrop_companyid"));
		invoice.setSentToContact(Boolean.TRUE);
		invoice.setContact(contact);

		contact.setContactNumber("TD" + record.get("attrdrop_companyid"));


		ArrayOfLineItem array = new ArrayOfLineItem();
		invoice.setLineItems(array);

		List<LineItem> lineItems = array.getLineItem();

		BigDecimal tax = new BigDecimal(gstFree(record) ? 0 : 0.1);

		for (InvoiceLineItem item : invoiceStripe.getLines().getData())
		{
			String accountCode = gstFree(record) ? "205" : "204";
			LineItem lineItem = new LineItem();
			lineItems.add(lineItem);
			lineItem.setAccountCode(accountCode);

			BigDecimal qty = new BigDecimal(item.getQuantity());
			BigDecimal amnt = new BigDecimal(item.getAmount()).divide(new BigDecimal(100));
			BigDecimal total = amnt.add(amnt.multiply(tax));

			lineItem.setQuantity(qty);
			lineItem.setUnitAmount(total.divide(qty));
			lineItem.setDescription(item.getDescription());
			lineItem.setLineAmount(total);
			lineItem.setTaxAmount(amnt.multiply(tax));
		}

		Calendar orderDate = getCalendar(record.get("ATTRDATE_Date"));
		invoice.setDate(orderDate);
		invoice.setInvoiceNumber(getID(record));

		Calendar due = getCalendar(record.get("ATTRDATE_Date"));
		if (due.get(Calendar.DAY_OF_MONTH) >= DAY_STARTING_NEW_BILLING_CYCLE)
			due.add(Calendar.MONTH, 2);
		else
			due.add(Calendar.MONTH, 1);
		due.set(Calendar.DATE, 11);
		invoice.setDueDate(due);
		invoice.getLineAmountTypes().add("Inclusive");
		invoice.setType(InvoiceType.ACCREC);
		invoice.setStatus(InvoiceStatus.AUTHORISED);

		return invoice;
	}

	private boolean gstFree(Map<String, String> record)
	{
		return !StringUtils.equalsIgnoreCase("Australia", record.get("ATTRDROP_country"))
				&& !StringUtils.equalsIgnoreCase("AU", record.get("ATTRDROP_country"));
	}

	@Override
	protected String getAccountCode(Map<String, String> item)
	{
		return "204";
	}

	@Override
	protected List<Map<String, String>> markAsSynced(List<Map<String, String>> db, List<String[]> deleteFailed, List<String[]> insertFailed, List<String[]> updateFailed) throws XeroClientUnexpectedException
	{
		List<Map<String, String>> ret = new ArrayList<Map<String, String>>();
		for (Map<String, String> record : db)
		{
			if (containsId(getID(record), deleteFailed))
			{
				continue;
			}
			if (containsId(getID(record), insertFailed))
			{
				continue;
			}
			if (containsId(getID(record), updateFailed))
			{
				continue;
			}
			new DBaccess().updateData("update elements_invoices set ATTR_SyncStatus = 'Synced' where element_id = '" + record.get("Element_id") + "'");
			ret.add(record);
		}
		return ret;
	}

	@Override
	protected String getName()
	{
		return "Qcloud invoices synchroniser";
	}
}
