/*
 *  Copyright 2011 Ross Jourdain
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package au.com.ci.crm.xero;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.client.OAuthClient;
import net.oauth.client.httpclient3.HttpClient3;
import net.oauth.signature.RSA_SHA1;

import org.apache.log4j.Logger;

import au.com.ci.crm.xero.model.ArrayOfContact;
import au.com.ci.crm.xero.model.ArrayOfInvoice;

/**
 * 
 * @author ross
 */
public class XeroClient
{

	protected Logger logger;
	private String endpointUrl;
	private String consumerKey;
	private String consumerSecret;
	private String privateKey;


	public XeroClient(XeroClientProperties clientProperties, Logger logger)
	{
		this.endpointUrl = clientProperties.getEndpointUrl();
		this.consumerKey = clientProperties.getConsumerKey();
		this.consumerSecret = clientProperties.getConsumerSecret();
		this.privateKey = clientProperties.getPrivateKey();
		this.logger = logger;
	}

	public OAuthAccessor buildAccessor()
	{

		OAuthConsumer consumer = new OAuthConsumer(null, consumerKey, null, null);
		consumer.setProperty(RSA_SHA1.PRIVATE_KEY, privateKey);
		consumer.setProperty(OAuth.OAUTH_SIGNATURE_METHOD, OAuth.RSA_SHA1);

		OAuthAccessor accessor = new OAuthAccessor(consumer);
		accessor.accessToken = consumerKey;
		accessor.tokenSecret = consumerSecret;

		return accessor;
	}

	private OAuthMessage get(String subUrl) throws XeroClientException, XeroClientUnexpectedException
	{
		logger.info("get url = " + subUrl);
		try
		{
			OAuthClient client = new OAuthClient(new HttpClient3());
			OAuthAccessor accessor = buildAccessor();
			OAuthMessage response = client.invoke(accessor, OAuthMessage.GET, endpointUrl + subUrl, null);
			return response;
		}
		catch (OAuthProblemException ex)
		{
			throw new XeroClientException("Error getting invoices", ex);
		}
		catch (Exception ex)
		{
			throw new XeroClientUnexpectedException("", ex);
		}
	}

	public ArrayOfInvoice getInvoices(Date date) throws XeroClientException, XeroClientUnexpectedException
	{
		String subUrl = "Invoices";
		ArrayOfInvoice array = null;
		if (date != null)
		{
			try
			{
				subUrl += "?where=" + URLEncoder.encode("Date>DateTime(" + new SimpleDateFormat("yyyy,MM,dd").format(date) + ")", "UTF-8");
			}
			catch (UnsupportedEncodingException e)
			{

			}
		}
		try
		{
			OAuthMessage response = get(subUrl);
			array = XeroXmlManager.xmlToInvoices(response.getBodyAsStream());
		}
		catch (Exception ex)
		{
			throw new XeroClientUnexpectedException("", ex);
		}
		return array;
	}

	public ArrayOfInvoice getInvoices() throws XeroClientException, XeroClientUnexpectedException
	{
		return getInvoices(null);
	}

	public ArrayOfContact getContacts(Date date) throws XeroClientException, XeroClientUnexpectedException
	{
		String subUrl = "Contacts";
		ArrayOfContact array = null;

		try
		{
			OAuthMessage response = get(subUrl);
			array = XeroXmlManager.xmlToContracts(response.getBodyAsStream());
		}
		catch (Exception ex)
		{
			throw new XeroClientUnexpectedException("", ex);
		}
		return array;
	}

	public ArrayOfContact getContacts() throws XeroClientException, XeroClientUnexpectedException
	{
		return getContacts(null);
	}

	private void post(String subUrl, String xmlString) throws XeroClientUnexpectedException, XeroClientException
	{
		logger.info("post url = " + subUrl);
		logger.info("post length = " + xmlString.length());
		logger.info(xmlString);
		try
		{
			OAuthClient client = new OAuthClient(new HttpClient3());
			OAuthAccessor accessor = buildAccessor();
			client.invoke(accessor, OAuthMessage.POST, endpointUrl + subUrl, OAuth.newList("xml", xmlString));
		}
		catch (OAuthProblemException ex)
		{
			logger.error("Error:", ex);
			XeroClientException e = new XeroClientException("Error posting " + subUrl, ex);
			throw e;
		}
		catch (Exception ex)
		{
			throw new XeroClientUnexpectedException("", ex);
		}
	}

	public void postInvoices(ArrayOfInvoice arrayOfInvoices) throws XeroClientException, XeroClientUnexpectedException
	{
		String xmlString = XeroXmlManager.xmlFromInvoices(arrayOfInvoices);
		post("Invoices", xmlString);
	}

	public void postContacts(ArrayOfContact arrayOfContact) throws XeroClientException, XeroClientUnexpectedException
	{
		String xmlString = XeroXmlManager.xmlFromContacts(arrayOfContact);
		post("Contacts", xmlString);
	}

}
