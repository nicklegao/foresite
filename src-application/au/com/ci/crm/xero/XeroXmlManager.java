/*
 *  Copyright 2011 Ross Jourdain
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package au.com.ci.crm.xero;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import net.oauth.OAuthProblemException;
import au.com.ci.crm.xero.model.ApiExceptionExtended;
import au.com.ci.crm.xero.model.ArrayOfContact;
import au.com.ci.crm.xero.model.ArrayOfInvoice;
import au.com.ci.crm.xero.model.ArrayOfPayment;
import au.com.ci.crm.xero.model.ObjectFactory;
import au.com.ci.crm.xero.model.ResponseType;

/**
 * 
 * @author ross
 */
public class XeroXmlManager
{

	public static ArrayOfInvoice xmlToInvoices(InputStream responseStream)
	{
		return xmlToResponse(responseStream).getInvoices();
	}

	public static ArrayOfContact xmlToContracts(InputStream responseStream)
	{
		return xmlToResponse(responseStream).getContacts();
	}

	public static ResponseType xmlToResponse(InputStream responseStream)
	{
		return xmlToObject(responseStream, ResponseType.class);
	}

	private static <T> T xmlToObject(InputStream input, Class<T> clazz)
	{
		return xmlToObject(new InputStreamReader(input), clazz);
	}

	private static <T> T xmlToObject(Reader reader, Class<T> clazz)
	{
		T response = null;
		try
		{
			JAXBContext context = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			JAXBElement<T> element = unmarshaller.unmarshal(new StreamSource(reader), clazz);
			response = element.getValue();

		}
		catch (JAXBException ex)
		{
			ex.printStackTrace();
		}

		return response;
	}

	public static ApiExceptionExtended xmlToException(String exceptionString)
	{
		return xmlToObject(new StringReader(exceptionString), ApiExceptionExtended.class);
	}

	public static String xmlFromOAuthProblemException(OAuthProblemException authProblemException)
	{

		String oAuthProblemExceptionString = null;

		Map<String, Object> params = authProblemException.getParameters();
		for (String key : params.keySet())
		{
			Object o = params.get(key);
			if (key.contains("ApiException"))
			{
				oAuthProblemExceptionString = key + "=" + o.toString();
			}
		}

		return oAuthProblemExceptionString;
	}

	private static <T> String xmlFromJaxBElement(JAXBElement<T> element)
	{
		String string = null;
		try
		{
			JAXBContext context = JAXBContext.newInstance(ResponseType.class);
			Marshaller marshaller = context.createMarshaller();
			//marshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
			StringWriter stringWriter = new StringWriter();
			marshaller.marshal(element, stringWriter);
			string = stringWriter.toString();
		}
		catch (JAXBException ex)
		{
			ex.printStackTrace();
		}

		return string;
	}

	public static String xmlFromContacts(ArrayOfContact arrayOfContacts)
	{
		return xmlFromJaxBElement(new ObjectFactory().createContacts(arrayOfContacts));
	}

	public static String xmlFromInvoices(ArrayOfInvoice arrayOfInvoices)
	{
		return xmlFromJaxBElement(new ObjectFactory().createInvoices(arrayOfInvoices));
	}

	public static String xmlFromPayments(ArrayOfPayment arrayOfPayment)
	{
		return xmlFromJaxBElement(new ObjectFactory().createPayments(arrayOfPayment));
	}

}
