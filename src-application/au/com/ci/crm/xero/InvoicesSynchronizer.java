package au.com.ci.crm.xero;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.ci.crm.xero.XeroClientException.ExceptionDetail;
import au.com.ci.crm.xero.model.ArrayOfInvoice;
import au.com.ci.crm.xero.model.ArrayOfLineItem;
import au.com.ci.crm.xero.model.Contact;
import au.com.ci.crm.xero.model.Invoice;
import au.com.ci.crm.xero.model.InvoiceStatus;
import au.com.ci.crm.xero.model.InvoiceType;
import au.com.ci.crm.xero.model.LineItem;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;

public class InvoicesSynchronizer extends ObjectSynchronizer<Invoice>
{

	public InvoicesSynchronizer(Logger logger, String countryCode)
	{
		super(logger, countryCode);
	}

	@Override
	protected List<Map<String, String>> getRecordsFromDB(Date date)
	{

		String sql = "select i.* from categories_salesorders i, elements_subscribe j, categories_subscribe c1, categories_subscribe c2 where c1.category_id = c2.category_parentId and c2.category_id = j.category_id and c1.attr_categoryName = ? and i.ATTR_EmailAddress = concat(j.attr_headline, ' (', j.element_id ,')')  and i.ATTR_SyncStatus is null";
		if (date != null)
		{
			sql += " and i.ATTRDATE_OrderDate >= '" + new SimpleDateFormat("yyyy-MM-dd") + "'";
		}
		return (List<Map<String, String>>) DataAccess.getInstance().select(sql, new String[] { countryCode }, new HashtableMapper());
	}

	private boolean isAgile(Map<String, String> record)
	{
		try
		{
			String module = record.get("ATTR_AccountReference").split("-")[0];
			String id = record.get("ATTR_AccountReference").split("-")[1];
			if (!StringUtils.equalsIgnoreCase("flex15", module))
			{
				return false;
			}
			String sql = "select ATTRDROP_jobType from elements_flex15 where element_id = " + id + "";
			return "AGILE".equals(new DBaccess().select(sql).get(0));
		}
		catch (Exception e)
		{
			return false;
		}
	}

	private List<Map<String, String>> getLineItemsFromDB(Map<String, String> record)
	{
		String id = record.get("Category_id");
		String sql = "select * from elements_salesorders where category_id = '" + id + "'";
		return (List<Map<String, String>>) DataAccess.getInstance().select(sql, new HashtableMapper());
	}

	@Override
	protected String getID(Invoice record)
	{
		return record.getInvoiceNumber();
	}

	@Override
	protected String getID(Map<String, String> record)
	{
		String reference = record.get("ATTR_AccountReference");
		if (reference != null && reference.toLowerCase().startsWith("flex13"))
		{
			return "T" + record.get("Category_id");
		}
		return "U" + record.get("Category_id");
	}

	@Override
	protected List<Invoice> getRecordsFromXero(Date date) throws XeroClientUnexpectedException
	{
		// ArrayOfInvoice array = xeroClient.getInvoices(date);
		// if (array != null && array.getInvoice() != null) {
		// return array.getInvoice();
		// }
		return new ArrayList<Invoice>();
	}

	@Override
	protected void insertToXero(List<Map<String, String>> records) throws XeroClientUnexpectedException, XeroClientException
	{
		xeroClient.postInvoices(mapsToInvoices(records));
	}

	@Override
	protected void updateToXero(List<Map<String, String>> records) throws XeroClientUnexpectedException, XeroClientException
	{
		xeroClient.postInvoices(mapsToInvoices(records));
	}

	@Override
	protected void deleteFromXero(List<Invoice> needToBeDeleted) throws XeroClientUnexpectedException, XeroClientException
	{
		// TODO Auto-generated method stub
	}

	public ArrayOfInvoice mapsToInvoices(List<Map<String, String>> records)
	{
		ArrayOfInvoice array = new ArrayOfInvoice();
		for (Map<String, String> record : records)
		{
			array.getInvoice().add(mapToInvoice(record));
		}
		return array;
	}

	public Invoice mapToInvoice(Map<String, String> record)
	{
		String accountCode = getAccountCode(record);
		String invoiceNumber = getID(record);
		List<Map<String, String>> items = getLineItemsFromDB(record);
		Invoice invoice = new Invoice();
		if (StringUtils.isNotBlank(getKey(record)))
		{
			invoice.setInvoiceID(getKey(record));
		}
		Contact contact = new Contact();
		invoice.setSentToContact(Boolean.TRUE);
		invoice.setContact(contact);
		String email = record.get("ATTR_EmailAddress");
		String elementID = email.substring(email.indexOf("(") + 1, email.indexOf(")"));
		String contactNumber = new DBaccess().selectQuerySingleCol("select c.attr_categorydesc from elements_subscribe e, categories_subscribe c where e.category_id = c.category_id and e.element_id = ?", new String[] { elementID }).get(0);
		invoice.setReference(contactNumber);
		contact.setContactNumber(contactNumber);
		// contact.setName(record.get("ATTR_CustomerName"));
		// contact.setFirstName(record.get("ATTR_ContactName"));
		// if (email != null && email.indexOf("(") != -1) {
		// email = email.substring(0, email.indexOf("(") - 1);
		// }
		// contact.setEmailAddress(email);
		//
		// { // add addresses
		// ArrayOfAddress addresses = new ArrayOfAddress();
		// contact.setAddresses(addresses);
		//
		// Address address = new Address();
		// addresses.getAddress().add(address);
		//
		// address.setAddressType(AddressType.STREET);
		// address.setAddressLine1(record.get("ATTR_DeliveryAddressLine2"));
		// address.setCity(record.get("ATTR_DeliveryAddressLine1"));
		// address.setRegion(record.get("ATTR_DeliveryAddressLine3"));
		// address.setPostalCode(record.get("ATTR_DeliveryPostCode"));
		// }

		ArrayOfLineItem array = new ArrayOfLineItem();
		invoice.setLineItems(array);

		List<LineItem> lineItems = array.getLineItem();
		for (Map<String, String> item : items)
		{
			LineItem lineItem = new LineItem();
			lineItems.add(lineItem);
			lineItem.setAccountCode(accountCode);
			BigDecimal qty = new BigDecimal(item.get("ATTR_OrderQty"));
			if (StringUtils.startsWith(invoiceNumber, "T"))
			{
				qty = new BigDecimal(1);
			}
			lineItem.setQuantity(qty);
			BigDecimal amnt = new BigDecimal(item.get("ATTRCURRENCY_UnitPrice"));
			lineItem.setUnitAmount(amnt);
			lineItem.setDescription(item.get("ATTR_Headline"));
			lineItem.setLineAmount(qty.multiply(amnt));
			lineItem.setTaxAmount(qty.multiply(amnt).multiply(new BigDecimal(0.1)));
		}

		Calendar orderDate = getCalendar(record.get("ATTRDATE_OrderDate"));
		invoice.setDate(orderDate);
		invoice.setInvoiceNumber(invoiceNumber);

		Calendar due = getCalendar(record.get("ATTRDATE_OrderDate"));
		if (!isAgile(record))
		{
			due.add(Calendar.DATE, 14);
		}
		invoice.setDueDate(due);
		invoice.getLineAmountTypes().add("Exclusive");
		invoice.setType(InvoiceType.ACCREC);
		invoice.setStatus(InvoiceStatus.AUTHORISED);

		return invoice;
	}

	/**
	 * @param record
	 * @return
	 */
	protected String getAccountCode(Map<String, String> record)
	{
		if (getID(record).startsWith("U"))
		{
			return "200";
		}
		if (getID(record).startsWith("T"))
		{
			return "201";
		}
		return "260";
	}

	protected Calendar getCalendar(String time)
	{
		try
		{
			String t = time.length() < 19 ? time.substring(0, 10) : time.substring(0, 19);
			Calendar c = Calendar.getInstance();
			c.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(t));
			return c;
		}
		catch (Exception e)
		{
			return null;
		}
	}

	@Override
	protected List<Map<String, String>> markAsSynced(List<Map<String, String>> db, List<String[]> deleteFailed, List<String[]> insertFailed, List<String[]> updateFailed) throws XeroClientUnexpectedException
	{
		List<Map<String, String>> ret = new ArrayList<Map<String, String>>();
		for (Map<String, String> record : db)
		{
			if (containsId(getID(record), deleteFailed))
			{
				continue;
			}
			if (containsId(getID(record), insertFailed))
			{
				continue;
			}
			if (containsId(getID(record), updateFailed))
			{
				continue;
			}
			String key = "Category_id";
			new DBaccess().updateData("update categories_salesorders set ATTR_SyncStatus = 'Synced' where category_id = '" + record.get(key) + "'");
			ret.add(record);
		}
		return ret;
	}

	@Override
	protected List<String[]> getFailedResultFromException(XeroClientException e)
	{
		List<String[]> result = new ArrayList<String[]>();
		List<ExceptionDetail> details = e.getDetails();
		for (ExceptionDetail detail : details)
		{
			Invoice invoice = (Invoice) detail.getModelObject();
			result.add(new String[] { invoice.getInvoiceNumber(), detail.toString() });
		}
		return result;
	}

	@Override
	protected String getName()
	{
		return "Sales invoices synchroniser";
	}


	@Override
	protected String getName(Map<String, String> record)
	{
		return getID(record);
	}

	@Override
	protected String getKey(Invoice record)
	{
		return record.getInvoiceID();
	}

	@Override
	protected String getKey(Map<String, String> record)
	{
		return record.get("Invoice_id");
	}

	@Override
	protected void setKey(Map<String, String> record, String value)
	{
		record.put("Invoice_id", value);
	}

}
