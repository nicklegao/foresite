package au.com.ci.crm.xero;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.utils.email.EmailUtils;

public abstract class ObjectSynchronizer<T>
{

	protected Logger logger;

	XeroClient xeroClient;

	protected int total;
	protected int succeed;
	protected int failed;

	protected String countryCode;

	protected ObjectSynchronizer(Logger logger, String countryCode)
	{
		this.logger = logger;
		this.countryCode = countryCode;
	}

	public int getTotal()
	{
		return total;
	}

	public int getSucceed()
	{
		return succeed;
	}

	public int getFailed()
	{
		return failed;
	}

	public void sendSyncResult(Date date, String email)
	{
		String notification = sync(date);
		sendNotification(notification, email);
	}

	public String sync(Date date)
	{
		try
		{
			List<Map<String, String>> db = getRecordsFromDB(date);
			List<String[]> deleteFailed = new ArrayList<String[]>();
			List<String[]> insertFailed = new ArrayList<String[]>();
			List<String[]> updateFailed = new ArrayList<String[]>();
			List<Map<String, String>> needToBeInserted = new ArrayList<Map<String, String>>();
			List<Map<String, String>> needToBeUpdateed = new ArrayList<Map<String, String>>();
			List<T> needToBeDeleted = new ArrayList<T>();
			try
			{
				XeroClientProperties clientProperties = new XeroClientProperties();

				clientProperties.load(this.getClass().getClassLoader().getResourceAsStream("/xeroApi_" + countryCode + ".properties"));
				xeroClient = new XeroClient(clientProperties, logger);

				total = db.size();
				List<T> xero = getRecordsFromXero(date);
				for (T record : xero)
				{
					if (!exist(record, db))
					{
						needToBeDeleted.add(record);
					}
				}
				for (Map<String, String> record : db)
				{
					if (!exist(record, xero))
					{
						needToBeInserted.add(record);
					}
					else
					{
						needToBeUpdateed.add(record);
					}
				}
			}
			catch (Exception ex)
			{
				logger.error("Error", ex);
				return generateNotification(db);
			}
			try
			{
				if (needToBeDeleted.size() > 0)
				{
					logger.info("delete " + needToBeDeleted.size() + " record(s)");
					try
					{
						deleteFromXero(needToBeDeleted);
					}
					catch (XeroClientException e)
					{
						deleteFailed = getFailedResultFromException(e);
					}
					logger.info("delete completed");
				}
			}
			catch (Exception ex)
			{
				logger.error("Error", ex);
				for (T xeroRecord : needToBeDeleted)
				{
					deleteFailed.add(new String[] { getID(xeroRecord), "" + ex.getMessage() });
				}
			}
			try
			{
				if (needToBeInserted.size() > 0)
				{
					logger.info("insert " + needToBeInserted.size() + " record(s)");
					try
					{
						insertToXero(needToBeInserted);
					}
					catch (XeroClientException e)
					{
						insertFailed = getFailedResultFromException(e);
					}
					logger.info("insert completed");
				}
			}
			catch (Exception ex)
			{
				logger.error("Error", ex);
				for (Map<String, String> record : needToBeInserted)
				{
					insertFailed.add(new String[] { getID(record), "" + ex.getMessage() });
				}
			}
			try
			{
				if (needToBeUpdateed.size() > 0)
				{
					logger.info("update " + needToBeUpdateed.size() + " record(s)");
					try
					{
						updateToXero(needToBeUpdateed);
					}
					catch (XeroClientException e)
					{
						updateFailed = getFailedResultFromException(e);
					}
					logger.info("update completed");
				}
			}
			catch (Exception ex)
			{
				logger.error("Error", ex);
				for (Map<String, String> record : needToBeUpdateed)
				{
					updateFailed.add(new String[] { getID(record), "" + ex.getMessage() });
				}
			}
			try
			{
				if (db.size() > 0)
				{
					markAsSynced(db, deleteFailed, insertFailed, updateFailed);
				}
				failed = deleteFailed.size() + insertFailed.size() + updateFailed.size();
				succeed = total - failed;

			}
			catch (Exception ex)
			{
				logger.error("Error", ex);
			}
			return generateNotification(db, deleteFailed, insertFailed, updateFailed);
		}
		catch (Throwable t)
		{
			logger.error("", t);
			return "Unknow error. Please see log.";
		}
	}

	protected void sendNotification(String notification, String email)
	{
		if (StringUtils.isNotBlank(email))
		{
			new EmailUtils().sendMail(new String[] { email }, null, null, email, getName() + " report", notification, null);
		}
	}

	private String generateNotification(List<Map<String, String>> db)
	{
		String message = getName() + " failed to synchronise following records with Xero: <br>\n";
		message += extractNames(db) + "<br><br>\n";
		message += "Please see log for details \n";
		message += new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		return message;
	}

	private String generateNotification(List<Map<String, String>> db, List<String[]> deleteFailed, List<String[]> insertFailed, List<String[]> updateFailed)
	{
		String message = getName() + " synchronised following records with Xero: <br>\n";
		message += extractNames(db) + "<br><br>\n";
		message += "Failed to delete (list of id): <br>\n";
		message += extractId(deleteFailed) + "<br><br>\n";
		message += "Failed to insert (list of id): <br>\n";
		message += extractId(insertFailed) + "<br><br>\n";
		message += "Failed to update (list of id): <br>\n";
		message += extractId(updateFailed) + "<br><br>\n";
		message += "Please see log for details \n";
		message += new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		return message;
	}

	private List<String> extractNames(List<Map<String, String>> db)
	{
		List<String> ret = new ArrayList<String>();
		for (Map<String, String> rec : db)
		{
			ret.add(getName(rec));
		}
		return ret;
	}

	private List<String> extractId(List<String[]> list)
	{
		List<String> ret = new ArrayList<String>();
		for (String[] a : list)
		{
			ret.add(a[0]);
		}
		return ret;
	}

	protected boolean exist(Map<String, String> dbRecord, List<T> xero)
	{
		String dbId = getID(dbRecord);
		for (T record : xero)
		{
			if (dbId.equalsIgnoreCase(getID(record)))
			{
				// VERY IMPORTANT!! : update by number doen't work somehow
				// have to update by id
				setKey(dbRecord, getKey(record));
				return true;
			}
		}
		return false;
	}

	protected boolean exist(T xeroRecord, List<Map<String, String>> db)
	{
		String xeroId = getID(xeroRecord);
		if (xeroId == null)
		{
			return false;
		}
		for (Map<String, String> dbRecord : db)
		{
			if (xeroId.equalsIgnoreCase(getID(dbRecord)))
			{
				return true;
			}
		}
		return false;
	}

	protected boolean containsId(String value, List<String[]> list)
	{
		for (String[] rec : list)
		{
			if (StringUtils.equals(rec[0], value))
			{
				return true;
			}
		}
		return false;
	}

	protected abstract List<Map<String, String>> getRecordsFromDB(Date date);

	protected abstract String getID(T record);

	protected abstract String getID(Map<String, String> record);

	protected abstract String getName(Map<String, String> record);

	protected abstract List<T> getRecordsFromXero(Date date) throws XeroClientUnexpectedException;

	protected abstract void insertToXero(List<Map<String, String>> db) throws XeroClientException, XeroClientUnexpectedException;

	protected abstract void updateToXero(List<Map<String, String>> db) throws XeroClientException, XeroClientUnexpectedException;

	protected abstract void deleteFromXero(List<T> needToBeDeleted) throws XeroClientException, XeroClientUnexpectedException;

	protected abstract List<Map<String, String>> markAsSynced(List<Map<String, String>> db, List<String[]> deleteFailed, List<String[]> insertFailed, List<String[]> updateFailed) throws XeroClientUnexpectedException;

	protected abstract List<String[]> getFailedResultFromException(XeroClientException e);

	protected abstract String getName();

	protected abstract String getKey(T record);

	protected abstract String getKey(Map<String, String> record);

	protected abstract void setKey(Map<String, String> record, String value);
}
