package au.net.webdirector.common.search;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import webdirector.db.client.ClientDataAccess;
import au.com.ci.sbe.util.GlobalVariables;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.utils.SimpleDateFormat;
import au.net.webdirector.common.utils.email.EmailUtils;

/**
 * Created by IntelliJ IDEA.
 * User: CorpInteractive
 * Date: 14/01/2008
 * Time: 15:50:30
 * To change this template use File | Settings | File Templates.
 */
public class SearchUtils
{
	private static Defaults d = Defaults.getInstance();
	private ClientDataAccess DA = new ClientDataAccess();
	static Logger logger = Logger.getLogger(SearchUtils.class);

	public static void createIndexFile(String moduleName)
	{
		logger.debug("IN CreateIndexFile Method");
		logger.debug("Module Name" + moduleName);
		if (moduleName != null && !moduleName.equals("") && !moduleName.equals("null"))
		{

			File indexFile = new File(d.getLuceneIndexDir() + moduleName);
			if (!indexFile.exists())
				indexFile.mkdirs();
		}
	}

	public boolean deleteIndexFile(String moduleName, String email)
	{
		logger.debug("IN deleteIndexFile Method");
		String lucenceStoreDir = d.getLuceneIndexDir();
		logger.debug("stordirector" + lucenceStoreDir);
		boolean success;
		try
		{
			success = d.nuke(new File(lucenceStoreDir + File.separator + moduleName), true);
			logger.debug("In DeleteIndexing File===>>>>>>");
			if (success)
			{
				success = reindexModule(moduleName, email);
			}
		}
		catch (Exception E)
		{
			logger.error("Exception" + E.getMessage());
			E.printStackTrace();
			return false;
		}
		return success;
	}

	private boolean reindexModule(String moduleName, final String email)
	{
		final String TmoduleName = moduleName;

		Thread t1 = new Thread()
		{
			public void run()
			{
				Date start = new Date();
				boolean flagElements = false, flagCategory = false;
				logger.debug("ModuleName" + TmoduleName);
				SearchContent s = new SearchContent(TmoduleName);
				List<ElementData> vElement = new ArrayList<ElementData>();
				List<CategoryData> vCategory = new ArrayList<CategoryData>();
				String message = "";
				try
				{
					ModuleAccess ma = ModuleAccess.getInstance();
					vElement = ma.getElements(TmoduleName, new ElementData());
					//Vector vElement = DA.getAllElements(TmoduleName);
					logger.debug("Size of vElement" + vElement.size());
					flagElements = s.insertBulkAsset(vElement, true);

					vCategory = ma.getCategories(TmoduleName, new CategoryData());
					//Vector vCategory = DA.getAllCategories(TmoduleName);
					flagCategory = s.insertBulkAsset(vCategory, false);
				}
				catch (Exception e)
				{
					logger.error("Error:", e);
					message = e.getMessage();
				}
				//String emailAddress[] = {d.getContactUsEmail()};

				String emailAddress[] = { StringUtils.isNotBlank(email) ? email : GlobalVariables.getString("LuceneReindexResultToEmailAddress") };
				String fromEmailAddress = GlobalVariables.getString("LuceneReindexResultFromEmailAddress");
				EmailUtils Email = new EmailUtils();
				String messageSubject = "Reindexing search Index of Module " + TmoduleName + " (" + d.getWebSiteURL() + ")";
				String bodyText = "";
				if (flagElements && flagCategory)
				{
					bodyText = messageSubject + "<BR> Reindexing of Search Index has been done successfully";
					bodyText += "<br>Number of elements indexed : " + vElement.size();
					bodyText += "<br>Number of categories indexed : " + vCategory.size();
				}
				else
				{
					bodyText = messageSubject + "<BR>There were some Error while reindexing search Index";
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				bodyText += "<br>Start Time: " + sdf.format(start);
				bodyText += "<br>End Time: " + sdf.format(new Date());
				boolean userInfo = Email.sendMail(emailAddress, null, null, fromEmailAddress, messageSubject, bodyText, null);
				//return true;
			}
		};
		t1.start();

		return true;
	}


	private String emptyrating = "<img src=\'../images/ratings_emptyStar.gif\'>";
	private String halfrating = "<img src=\'../images/ratings_halfStar.gif\'>";
	private String fullrating = "<img src=\'../images/ratings_fullStar.gif\'>";


	public SearchUtils()
	{


	}

	private int[][] rating = {
			{ 0, 1, 4 },
			{ 1, 0, 4 },
			{ 1, 1, 3 },
			{ 2, 0, 3 },
			{ 2, 1, 2 },
			{ 3, 0, 2 },
			{ 3, 1, 1 },
			{ 4, 0, 1 },
			{ 4, 1, 0 },
			{ 5, 0, 0 }
	};

	public String getRatingImagePath(int index)
	{
		System.out.println("Index" + index);
		if (index == 0)
		{
			index = 1;
		}
		int in = 9;
		String temp = "";
		for (int j = 0; j < rating[index - 1][0]; j++)
		{
			temp = temp.concat(fullrating);
		}
		for (int i = 0; i < rating[index - 1][1]; i++)
		{
			temp = temp.concat(halfrating);
		}
		for (int i = 0; i < rating[index - 1][2]; i++)
		{
			temp = temp.concat(emptyrating);
		}
		return temp;
	}
}
