package au.net.webdirector.common.search;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;

import com.fathzer.soft.javaluator.AbstractEvaluator;
import com.fathzer.soft.javaluator.BracketPair;
import com.fathzer.soft.javaluator.Operator;
import com.fathzer.soft.javaluator.Parameters;

public class BooleanQueryEvaluator extends AbstractEvaluator<BooleanQuery>
{
	/** The negate unary operator. */
	public final static List<Operator> NEGATE = Arrays.asList(new Operator[] { new Operator("NOT", 1, Operator.Associativity.RIGHT, 3), new Operator("not", 1, Operator.Associativity.RIGHT, 3), new Operator("!", 1, Operator.Associativity.RIGHT, 3) });
	/** The logical AND operator. */
	public final static List<Operator> AND = Arrays.asList(new Operator[] { new Operator("AND", 2, Operator.Associativity.LEFT, 2), new Operator("and", 2, Operator.Associativity.LEFT, 2), new Operator("&&", 2, Operator.Associativity.LEFT, 2) });
	/** The logical OR operator. */
	public final static List<Operator> OR = Arrays.asList(new Operator[] { new Operator("OR", 2, Operator.Associativity.LEFT, 1), new Operator("or", 2, Operator.Associativity.LEFT, 1), new Operator("||", 2, Operator.Associativity.LEFT, 1) });

	private static final Parameters PARAMETERS;

	static
	{
		// Create the evaluator's parameters
		PARAMETERS = new Parameters();
		// Add the supported operators
		PARAMETERS.addOperators(AND);
		PARAMETERS.addOperators(OR);
		// PARAMETERS.addOperators(NEGATE);
		PARAMETERS.addExpressionBracket(BracketPair.PARENTHESES);
	}

	private Search search;
	private String keyColumn;

	public BooleanQueryEvaluator(Search search, String keyColumn)
	{
		super(PARAMETERS);
		this.search = search;
		this.keyColumn = keyColumn;
	}

	@Override
	protected BooleanQuery toValue(String literal, Object evaluationContext)
	{
		try
		{
			return search.getLuceneQuery(literal, keyColumn);
		}
		catch (ParseException e)
		{
			throw new RuntimeException(e);
		}
	}

	@Override
	protected BooleanQuery evaluate(Operator operator, Iterator<BooleanQuery> operands, Object evaluationContext)
	{
		if (NEGATE.contains(operator))
		{
			BooleanQuery not = new BooleanQuery();
			not.add(operands.next(), BooleanClause.Occur.MUST_NOT);
			return not;
		}
		else if (OR.contains(operator))
		{
			BooleanQuery or = new BooleanQuery();
			or.add(operands.next(), BooleanClause.Occur.SHOULD);
			or.add(operands.next(), BooleanClause.Occur.SHOULD);
			return or;
		}
		else if (AND.contains(operator))
		{
			BooleanQuery and = new BooleanQuery();
			and.add(operands.next(), BooleanClause.Occur.MUST);
			and.add(operands.next(), BooleanClause.Occur.MUST);
			return and;
		}
		else
		{
			return super.evaluate(operator, operands, evaluationContext);
		}
	}

}