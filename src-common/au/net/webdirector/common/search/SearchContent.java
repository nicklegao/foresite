package au.net.webdirector.common.search;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.ModuleData;
import au.net.webdirector.common.datalayer.lucene.ModuleFieldRanker;


/**
 * Created by IntelliJ IDEA. User: Administrator Date: 3/01/2008 Time: 13:25:21
 * To change this template use File | Settings | File Templates.
 */
public class SearchContent
{

	private static Logger logger = Logger.getLogger(SearchContent.class);
	private static Defaults d = Defaults.getInstance();
	boolean isElement;
	String moduleName;
	String id;

	File indexFile;
	ModuleData record;
	String keyColumn;

	public SearchContent(String modName)
	{
		this.moduleName = modName;
		SearchUtils.createIndexFile(moduleName);
		this.indexFile = new File(d.getLuceneIndexDir() + moduleName);
	}

	public SearchContent(boolean isEle, String modName, String assetId)
	{
		this(modName);
		this.isElement = isEle;
		this.id = assetId;

		if (isElement)
		{
			try
			{
				record = ModuleAccess.getInstance().getElementById(moduleName, id);
			}
			catch (SQLException e)
			{
				logger.error("Error:", e);
			}
			keyColumn = "element";
		}
		else
		{
			try
			{
				record = ModuleAccess.getInstance().getCategoryById(moduleName, id);
			}
			catch (SQLException e)
			{
				logger.error("Error:", e);
			}
			keyColumn = "category";
		}

	}

	/**
	 * in the web.xml the lucene index dir is set to "" to switch off all
	 * indexing, otherwise indexing will occur.
	 * 
	 * @return
	 */
	public static boolean luceneSearchOn()
	{
		return !d.getLuceneIndexDir().equals("");
	}

	/**
	 * Uses module specific index
	 * 
	 * @return true if successfully added to index
	 */
	public boolean insertAsset()
	{
		IndexWriter indexWriter = null;
		try
		{
			// below will create a new index based on the moduleName if it does
			// not exist or append to an existing one.
			Directory directory = FSDirectory.open(indexFile);
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_46, new WhitespaceAnalyzer(Version.LUCENE_46));
			indexWriter = new IndexWriter(directory, config);
			Document doc = createDocumentFromAsset();
			indexWriter.addDocument(doc);
			indexWriter.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally
		{
			try
			{
				indexWriter.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}

		return true;
	}

	/**
	 * Use this method to reindex particular module in a one go...
	 * 
	 * @param allFromTable
	 *            :: Contain all assets of table of particular module
	 * @return true if successfully added to index
	 */
	public boolean insertBulkAsset(List<? extends ModuleData> records, boolean isEle)
	{
		isElement = isEle;
		String keyColumn = isElement ? "element" : "category";

		IndexWriter indexWriter = null;
		try
		{
			// below will create a new index based on the moduleName if it does
			// not exist or append to an existing one.
			Directory directory = FSDirectory.open(indexFile);
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_46, new WhitespaceAnalyzer(Version.LUCENE_46));
			indexWriter = new IndexWriter(directory, config);
			for (int i = 0; i < records.size(); i++)
			{
				this.record = records.get(i);
				this.id = this.record.getId();
				Document doc = createDocumentFromAsset();
				indexWriter.addDocument(doc);
				logger.info(i + " of " + records.size() + " finished");
				logger.info("finish lucene index (" + (i + 1) + " of " + records.size() + ")");
			}
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally
		{
			try
			{
				indexWriter.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public boolean updateAsset()
	{
		IndexWriter indexWriter = null;
		try
		{
			// below will create a new index based on the moduleName if it does
			// not exist or append to an existing one.
			Directory directory = FSDirectory.open(indexFile);
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_46, new WhitespaceAnalyzer(Version.LUCENE_46));
			indexWriter = new IndexWriter(directory, config);
			Document doc = createDocumentFromAsset();
			Term term = new Term("id", id);
			indexWriter.updateDocument(term, doc);
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally
		{
			try
			{
				indexWriter.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public boolean deleteAsset()
	{
		IndexWriter indexWriter;
		try
		{
			// below will create a new index based on the moduleName if it does
			// not exist or append to an existing one.
			Directory directory = FSDirectory.open(indexFile);
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_46, new WhitespaceAnalyzer(Version.LUCENE_46));
			indexWriter = new IndexWriter(directory, config);
			Term term = new Term("id", id);
			indexWriter.deleteDocuments(term);
			indexWriter.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			indexWriter = null;
		}
		return true;
	}

	private Document createDocumentFromAsset() throws Exception
	{
		Document doc = new Document();

		if (record != null)
		{
			ModuleFieldRanker ranker = new ModuleFieldRanker(moduleName, isElement, ModuleAccess.getInstance());
			List<Field> fields = ranker.getLuceneFields(record);
			for (Field field : fields)
			{
				doc.add(field);
			}
		}
		else
		{
			throw new Exception("No Element/Category for " + moduleName + " " + id + " " + isElement);
		}

		return doc;
	}
}
