package au.net.webdirector.common.search;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.MultiReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.lucene.ModuleFieldRanker;
import au.net.webdirector.common.datalayer.lucene.ModuleFieldRanker.RankedField;

/**
 * Created by IntelliJ IDEA. User: Administrator Date: 3/01/2008 Time: 14:48:18
 * To change this template use File | Settings | File Templates.
 */
public class Search
{
	static Logger logger = Logger.getLogger(Search.class);
	static int MAX_DOCS = 1000;

	// if moduleList.length == 0 then it's for all modules
	String moduleList[];

	IndexSearcher multiIndexSearcher = null;

	Analyzer analyzer = null;

	public Search()
	{
		this.moduleList = getIndexedModules();
		analyzer = new WhitespaceAnalyzer(Version.LUCENE_46);
	}

	public Search(String[] moduleList)
	{
		this.moduleList = moduleList;
		analyzer = new WhitespaceAnalyzer(Version.LUCENE_46);
	}

	public Search(String moduleName)
	{
		this(new String[] { moduleName });
	}

	public boolean openSearchIndex()
	{
		return this.openSearchIndex(new WhitespaceAnalyzer(Version.LUCENE_46));
	}

	private String[] getIndexedModules()
	{
		List<String> modules = new ArrayList<String>();
		File[] listOfFiles = new File(Defaults.getInstance().getLuceneIndexDir()).listFiles();
		for (File dir : listOfFiles)
		{
			if (!dir.isDirectory())
			{
				continue;
			}
			modules.add(dir.getName());
		}
		return modules.toArray(new String[] {});
	}

	public boolean openSearchIndex(Analyzer analyser)
	{
		this.analyzer = analyser;
		Defaults d = Defaults.getInstance();
		try
		{
			List<IndexReader> is = new ArrayList<IndexReader>();
			for (String module : moduleList)
			{
				File dir = new File(d.getLuceneIndexDir() + module);
				if (!dir.exists())
				{
					continue;
				}
				is.add(DirectoryReader.open(FSDirectory.open(dir)));
			}

			multiIndexSearcher = new IndexSearcher(new MultiReader(is.toArray(new IndexReader[] {})));
		}
		catch (Exception e)
		{
			logger.error(e);
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void closeSearchIndex()
	{
		try
		{
			if (multiIndexSearcher != null)
				multiIndexSearcher.getIndexReader().close();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public IndexSearcher getIndexSearcher()
	{
		return multiIndexSearcher;
	}

	public TopDocs performSearch(String queryString)
	{
		return performSearchbyType(queryString, "", false);
	}

	public TopDocs performSearchbyType(String queryString, String keyColumn, boolean liveOnly)
	{
		TopDocs topDocs = null;
		try
		{
			BooleanQuery finalQuery = new BooleanQuery();
			BooleanQuery luceneQuery = new BooleanQueryEvaluator(this, keyColumn).evaluate(queryString);
			finalQuery.add(luceneQuery, BooleanClause.Occur.MUST);

			if (!StringUtils.isBlank(keyColumn))
			{
				Query queryMust = new TermQuery(new Term("keyColumn", keyColumn));
				finalQuery.add(queryMust, BooleanClause.Occur.MUST);
			}
			if (liveOnly)
			{
				Query queryMust = new TermQuery(new Term("live", "1"));
				finalQuery.add(queryMust, BooleanClause.Occur.MUST);
			}
			topDocs = multiIndexSearcher.search(new QueryParser(Version.LUCENE_46, "content", analyzer).parse(finalQuery.toString()), MAX_DOCS);
		}
		catch (FileNotFoundException e)
		{
			logger.info("Exception::" + e.getMessage());
		}
		catch (Exception e)
		{
			logger.info("Exception " + e.getMessage());
			e.printStackTrace();
		}
		return topDocs;
	}

	public BooleanQuery getLuceneQuery(String queryString, String keyColumn) throws ParseException
	{
		String newQueryString = prepareSearchText(queryString);
		boolean accurate = StringUtils.startsWith(newQueryString, "\"") && StringUtils.endsWith(newQueryString, "\"");
		logger.info("In performSearchType Method");
		Map<String, Integer> rankedFields = new LinkedHashMap<String, Integer>();
		if (StringUtils.isBlank(keyColumn) || StringUtils.equalsIgnoreCase("element", keyColumn))
		{
			for (String moduleName : moduleList)
			{
				ModuleFieldRanker ranker = new ModuleFieldRanker(moduleName, true, ModuleAccess.getInstance());
				for (RankedField rankedField : ranker.getRankedFields().values())
				{
					if (!rankedFields.containsKey(rankedField.getRankedName()))
					{
						rankedFields.put(rankedField.getRankedName(), rankedField.getRank());
					}
				}
			}
		}
		if (StringUtils.isBlank(keyColumn) || StringUtils.equalsIgnoreCase("category", keyColumn))
		{
			for (String moduleName : moduleList)
			{
				ModuleFieldRanker ranker = new ModuleFieldRanker(moduleName, false, ModuleAccess.getInstance());
				for (RankedField rankedField : ranker.getRankedFields().values())
				{
					if (!rankedFields.containsKey(rankedField.getRankedName()))
					{
						rankedFields.put(rankedField.getRankedName(), rankedField.getRank());
					}
				}
			}
		}
		rankedFields.put("content", 0);
		BooleanQuery queryFinal = new BooleanQuery();
		BooleanQuery queryOr = new BooleanQuery();
		for (String field : rankedFields.keySet())
		{

			String[] subQuerys = accurate ? new String[] { newQueryString } : StringUtils.split(newQueryString, " ");
			BooleanQuery queryAnd = new BooleanQuery();
			for (String subQuery : subQuerys)
			{
				if (!accurate)
				{
					Query query = new WildcardQuery(new Term(field, subQuery + "*"));
					query.setBoost(1);
					Occur occur = BooleanClause.Occur.MUST;
					queryAnd.add(query, occur);
				}
				else
				{
					PhraseQuery query = new PhraseQuery();
					query.add(new Term(field, StringUtils.substring(subQuery, 1, subQuery.length() - 1)));
					query.setBoost(1);
					Occur occur = BooleanClause.Occur.MUST;
					queryAnd.add(query, occur);

				}
			}
			queryAnd.setBoost(rankedFields.get(field));
			queryOr.add(queryAnd, BooleanClause.Occur.SHOULD);
		}
		queryFinal.add(queryOr, BooleanClause.Occur.MUST);
		return queryFinal;

	}

	/**
	 * perform multiple module search
	 * 
	 * ALways must use the openSearchIndex method before using this one and of
	 * course close the IndexSearcher when finished using the closeSearchIndex
	 * method.
	 * 
	 * @param queryString
	 * @return Hits for a search
	 */
	public TopDocs multiPerformSearch(String queryString)
	{
		logger.info("In multiPerformSearch Method");
		TopDocs topDocs = null;
		try
		{
			Query query = new WildcardQuery(new Term("content", queryString));
			topDocs = multiIndexSearcher.search(query, MAX_DOCS);
			logger.info("lengh of Hits" + topDocs.totalHits);
		}
		catch (FileNotFoundException e)
		{
			logger.info("Exception::" + e.getMessage());
		}
		catch (Exception e)
		{
			logger.info("Exception " + e.getMessage());
			e.printStackTrace();
		}
		return topDocs;
	}

	public Set<String> searchTree(String queryString, String keyColumn)
	{
		Set<String> result = new HashSet<String>();
		this.openSearchIndex();
		try
		{
			TopDocs hits = this.performSearchbyType(queryString, keyColumn, false);
			for (ScoreDoc scoreDoc : hits.scoreDocs)
			{
				Document doc = multiIndexSearcher.doc(scoreDoc.doc);
				String id = doc.get("id");
				result.add(id);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			this.closeSearchIndex();
		}
		return result;
	}

	public TopDocs performFuzzySearch(String queryString)
	{
		if (multiIndexSearcher == null)
		{
			return null;
		}
		TopDocs hits = null;
		try
		{
			logger.info("analyzer = " + analyzer);
			Term term = new Term("content", queryString);
			Query fq = new WildcardQuery(term);
			hits = multiIndexSearcher.search(fq, MAX_DOCS);
		}
		catch (FileNotFoundException e)
		{
			logger.info("Exception::" + e.getMessage());
		}
		catch (Exception e)
		{
			logger.info("Exception " + e.getMessage());
			e.printStackTrace();
		}
		return hits;
	}

	public static String prepareSearchText(String searchText)
	{
		return searchText.toLowerCase();
	}
}
