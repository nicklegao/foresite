package au.net.webdirector.common;

import java.io.File;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.client.ModuleHelper;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import au.net.webdirector.common.utils.SimpleDateFormat;
import au.net.webdirector.common.utils.StreamGobbler;

public class Defaults implements Serializable
{

	static Logger logger = Logger.getLogger(Defaults.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = -7372466825472903841L;
	private static String brand_name = null;
	private static String brand_id = null;
	private static String storeDir = null;// web served directory
	private static String storeContext = null;// web context used to access store.
	private static int pageSize = 0;
	private static String defaultImg = null;
	private static String infoImg = null;
	private static String downloads = null;
	private static String uploadServlet = null;

	private static String warName = null;
	private static String siteURL = null;
	private static String autoUnsubscribe = null;
	private static String LuceneIndexDir = null;

	private static String contactUsEmail = null;
	private static String whereToBuyEmail = null;
	private static String licence = null;// valid till 2003
	private static String ODBC = null;
	private static String thumbnailWidth = null;
	private static String thumbnailHeight = null;
	private static String logoThumbnailWidth = null;
	private static String logoThumbnailHeight = null;
	private static String imageMagickInstallDir = null;
	private static String lesscBinaryPath = null;
	private static String crmUrl = null;

	private static String mailServerSMTP = null;
	private static String fromEmailAddressSubscribeModule = null;
	private static String fromEmailAddressRecallModule = null;
	private static String shopOrderEmailAddress = null;
	private static String usernameSMTPserver = null;
	private static String passwordSMTPserver = null;
	private static String fromEmailRealName = null;

	private static boolean DEBUG = false;

	private static String DBProvider = "";
	private static String TomcatVersion = "";
	private static String OSType = "";
	private static String TempLinux = "";
	private static String TempWindows = "";
	private String viewer = "viewer.jsp";
	private String dirHandler = "Dir.jsp";
	private String path = "";

	public String clientCol = "";

	private static String dirEle = null;
	private static String dirCat = null;
	private static String dirHier = null;

	private static boolean recycleIcon = false;
	private static boolean inboxIcon = false;

	private static String GoogleAnalyticsUser = "webdirector.ga@gmail.com";
	private static String GoogleAnalyticsPass = "webdirector";
	private static String GoogleAnalyticsProfileName = "www.smai.com.au";
	private static Set<String> validImageExtensions = new HashSet<String>(Arrays.asList(
			new String[] { "jpg", "jpeg", "bmp", "gif", "png", "tiff" }));


	// colour scheme
	private static String darkColour = null;// general backgrounds which are dark yellow
	private static String lightColour = null;// general backgrounds which are light yellow
	private static String textString = null;// text definition string

	private static String AllocateCategoryAcess = "off";

	private static String[] uploadFileExtWhiteList = new String[] {};

	private static Defaults singleton = null;

	private static int categoryNumberPerLoadInFolderTree = 20;
	private static int assetNumberPerLoadInFolderTree = 20;

	public static int getCategoryNumberPerLoadInFolderTree()
	{
		return categoryNumberPerLoadInFolderTree;
	}

	public static int getAssetNumberPerLoadInFolderTree()
	{
		return assetNumberPerLoadInFolderTree;
	}

	public String getAllocateCategoryAcess()
	{
		return AllocateCategoryAcess;
	}

	public static Defaults getInstance()
	{
		if (singleton == null)
		{
			synchronized (Defaults.class)
			{
				if (singleton == null)
				{
					singleton = new Defaults();
				}
			}
		}
		return singleton;
	}

	private Defaults()
	{
		// PropertyConfigurator.configureAndWatch("c:/log4j.properties");
	}

	public boolean getInboxValid()
	{
		return (recycleIcon);
	}

	public boolean getRecycleValid()
	{
		return (inboxIcon);
	}

	public boolean nuke(File dir)
	{
		if (dir.isDirectory())
		{
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++)
			{
				boolean success = nuke(new File(dir, children[i]));
				if (!success)
				{
					return false;
				}
			}
		}

		// The directory is now empty so delete it
		return dir.delete();
	}

	public boolean nuke(File dir, String category)
	{

		if (dir.isDirectory())
		{
			String[] children = dir.list();

			for (int i = 0; i < children.length; i++)
			{
				if (!children[i].equalsIgnoreCase(category))
				{
					logger.info("vb childern" + children[i]);
					boolean success = nuke(new File(dir, children[i]));
					if (!success)
					{
						return false;
					}
				}
			}
		}

		// The directory is now empty so delete it
		return true;
	}

	// Adding new nuke method to just delete content of directory ,not to delete directory itself
	public boolean nuke(File dir, boolean temp)
	{
		if (dir.isDirectory())
		{
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++)
			{
				boolean success = nuke(new File(dir, children[i]));
				if (!success)
				{
					return false;
				}
			}
		}

		return true;
	}

	public boolean removeExistingFile(int id, String attr, String module, String tablePrefix)
	{
		DBaccess db = new DBaccess();
		module = DatabaseValidation.encodeParam(module);
		tablePrefix = DatabaseValidation.encodeParam(tablePrefix);
		String query = "";
		if (tablePrefix.equals("Categories"))
			query = "select " + attr + " from " + tablePrefix + "_" + module + " where Category_id = ?";
		else
			query = "select " + attr + " from " + tablePrefix + "_" + module + " where Element_id = ?";

		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query, new Object[] { id });

		if (v != null && v.size() > 0)
		{
			String fileName = (String) v.elementAt(0);
			logger.info("file " + fileName);
			File f = new File(storeDir, fileName);
			logger.info("f  " + f.getPath());
			if (f.exists())
				f.delete();

			f = new File(f.getParent() + "/_thumb/" + f.getName());
			logger.info("f  " + f.getPath());
			if (f.exists())
				f.delete();
		}

		return true;
	}

	public String getStoreThumbPath(String parent, String file)
	{
		// build store path for thumbnail
		// /stores/NEWS/554/ATTRFILE_File1/_thumb/bob.gif
		String path = "/" + getStoreContext() + "/" + parent + "/_thumb/" + file;
		logger.info("X" + path);
		return path;
	}

	// physically checks for thumb file in stores
	// parent is <module>/<id>/<attr>
	public boolean doesThumbFileExist(String parent, String file)
	{
		String path = "/" + getStoreDir() + "/" + parent + "/_thumb/" + file;
		String ext = FilenameUtils.getExtension(path);

		File f = new File(path);

		if (f.exists() && validImageExtensions.contains(ext))
			return true;
		else
			return false;
	}

	public boolean doesFileExist(String parent, String file)
	{
		String path = "/" + getStoreDir() + "/" + parent + "/" + file;
		File f = new File(path);

		if (f.exists())
			return true;
		else
			return false;
	}

	public String getStorePath(String parent, String file)
	{
		// build store path for image
		// /stores/NEWS/554/ATTRFILE_File1/bob.gif
		String path = "/" + getStoreContext() + "/" + parent + "/" + file;
		return path;
	}

	public void setClientCol(String col)
	{
		clientCol = col;
	}

	public String getClientCol()
	{
		return (clientCol);
	}

	public boolean generateThumb(File source)
	{
		String sep1 = "\\";
		String OSType = getOSType();
		if (OSType.equals("Linux"))
			sep1 = "/";

		String parentDir = source.getParent();
		String fileName = source.getName();

		logger.info("thumb dir : " + parentDir + "/_thumb/" + fileName);

		// make thumb and all parent dirs
		File thumbDir = new File(parentDir + File.separator + "_thumb" + File.separator);
		thumbDir.mkdirs();

		int exitVal = 0;
		try
		{
			String osName = System.getProperty("os.name");
			String[] cmd = new String[5];

			logger.info(osName);

			cmd[0] = imageMagickInstallDir + File.separator + "convert";
			cmd[1] = parentDir + File.separator + fileName;
			cmd[2] = "-resize";
			cmd[3] = logoThumbnailWidth + "x" + logoThumbnailHeight;// "96x120";
			cmd[4] = parentDir + File.separator + "_thumb" + File.separator + fileName;

			Runtime rt = Runtime.getRuntime();
			String a = "";
			for (int i = 0; i < cmd.length; i++)
				a += " " + cmd[i];

			logger.info("Executing " + a);
			Process proc = rt.exec(cmd);
			// any error message?
			StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "ERROR");

			// any output?
			StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "OUTPUT");

			// kick them off
			errorGobbler.start();
			outputGobbler.start();

			// any error???
			exitVal = proc.waitFor();
			logger.info("ExitValue: " + exitVal);
		}
		catch (Throwable t)
		{
			t.printStackTrace();
		}

		if (exitVal == 0)
			return true;
		else
			return false;
	}

	public String titleBar(String mess)
	{
		StringBuffer sb = new StringBuffer(200);

		sb.append("<table cellspacing=0 cellpadding=0 width=100%>");
		sb.append("<tr>");
		sb.append("<td height=1 bgcolor=\"darkgray\"></td>");
		sb.append("</tr>");
		sb.append("<tr>");
		sb.append("<td height=1 bgcolor=\"white\"></td>");
		sb.append("</tr>");
		sb.append("<tr>");
		sb.append("<td bgcolor=\"#D6D6CE\"><font size=2 face=Arial>&nbsp;" + mess + "&nbsp;</td>");
		sb.append("</tr>");
		sb.append("<tr height=1>");
		sb.append("<td bgcolor=\"darkgray\"></td>");
		sb.append("</TR>");
		sb.append("</table>");

		return (sb.toString());
	}

	public String dispFile(String filename, int height, int width)
	{
		StringBuffer sb = new StringBuffer(200);

		if (width == 0)
			width = 500;
		if (height == 0)
			height = 600;

		sb.append("<table cellspacing=0 cellpadding=0 width=\"100%\"><tr><td>");

		String ending = filename.substring(filename.length() - 4);

		if (getDEBUG())
			logger.info("ENDING " + ending);

		if (ending.equalsIgnoreCase(".mov"))
		{
			width = 464;
			height = 282 + 24;
			/*
			 * sb.append("<img width=23 src=\"/MediaStore/images/frameTL.gif\"></td>"); sb.append("<td><img width=\"100%\" height=\"24\" src=\"/MediaStore/images/frame-top-bottom.gif\"></td>"); sb.append("<td><img src=\"/MediaStore/images/frameTR.gif\"></td></tr>");
			 * 
			 * sb.append("<tr><td><img width=23 height=100% src=\"/MediaStore/images/frame-right-left.gif\"></td>"); sb.append("<td>");
			 */

			sb.append("<embed border=1 controller=true width=\"" + String.valueOf(width) + "\" height=\"" + String.valueOf(height) + "\" src=\"/stores" + filename + "\"></embed>");

			/*
			 * sb.append("</td><td><img width=23 height=100% src=\"/MediaStore/images/frame-right-left.gif\"></td></tr>");
			 * 
			 * sb.append("<tr><td><img src=\"/MediaStore/images/frameBL.gif\"></td>"); sb.append("<td><img width=\"100%\" height=\"24\" src=\"/MediaStore/images/frame-top-bottom.gif\"></td>"); sb.append("<td><img src=\"/MediaStore/images/frameBR.gif\">");
			 */

		}
		else if (ending.equalsIgnoreCase(".pdf"))
			sb.append("<embed width=\"" + String.valueOf(width) + "\" height=\"" + String.valueOf(height) + "\" src=\"/stores" + filename + "\"></embed>");
		else if (ending.equalsIgnoreCase(".jpg") || ending.equalsIgnoreCase(".gif") || ending.equalsIgnoreCase(".png"))
			sb.append("<img width=\"" + String.valueOf(width) + "\" src=\"/stores" + filename + "\">");
		else
			sb.append("<font size=2 face=Arial>Preview of this file type is unsupported.");

		sb.append("</td></tr></table>");

		return (sb.toString());
	}

	public void changeFilePaths(String oldPath, String newPath)
	{
		// go through Files table changing fullPath names
		DBaccess db = new DBaccess();
		String query = "select files_id, fullPath from files";
		String[] cols = { "files_id", "fullPath" };
		//Vector v = db.select(query, cols);
		// TODO validated
		Vector v = db.selectQuery(query);

		for (int i = 0; i < v.size(); i++)
		{
			String[] fullPath = (String[]) v.elementAt(i);

			if (fullPath[1].startsWith(oldPath))
			{
				String ending = fullPath[1].substring(oldPath.length());
				query = "update files set fullPath = ? where files_id = ?";
				db.updateData(query, new Object[] { newPath + ending, fullPath[0] });
			}
		}
	}

	public void setProperties(ServletContext servletContext)
	{
		// logger.info("setting properties from web.xml file");
		System.setProperty("javax.xml.transform.TransformerFactory", "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");
//		System.setProperty("mail.mime.splitlongparameters", "false");
		System.setProperty("mail.mime.encodeparameters", "false");

		if (warName == null)
		{
			try
			{
				warName = servletContext.getInitParameter("warName");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : warName ");
			}

			try
			{
				autoUnsubscribe = servletContext.getInitParameter("autoUnsubscribe");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : autoUnsubscribe ");
			}

			try
			{
				storeContext = servletContext.getInitParameter("storeContext");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : storeContext");
			}
			try
			{
				storeDir = servletContext.getInitParameter("storeDir");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : storeDir");
			}
			try
			{
				pageSize = Integer.parseInt(servletContext.getInitParameter("pageSize"));
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : pageSize");
			}
			try
			{
				defaultImg = servletContext.getInitParameter("defaultImg");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : defaultImg");
			}
			try
			{
				infoImg = servletContext.getInitParameter("infoImg");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : infoImg");
			}

			try
			{
				downloads = servletContext.getInitParameter("downloads");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : donwloads");
			}

//			try {
//				DBuser = servletContext.getInitParameter("DBuser");
//			} catch (Exception e) {
//				System.out.println("ERROR in web.xml : DBuser");
//			}
//			try {
//				DBpass = servletContext.getInitParameter("DBpass");
//			} catch (Exception e) {
//				System.out.println("ERROR in web.xml : DBpass");
//			}
			try
			{
				ODBC = servletContext.getInitParameter("ODBC");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : ODBC");
			}
			try
			{
				licence = servletContext.getInitParameter("licence");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : licence");
			}

			try
			{
				darkColour = servletContext.getInitParameter("darkColour");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : darkColour");
			}
			try
			{
				lightColour = servletContext.getInitParameter("lightColour");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : lightColour");
			}
			try
			{
				textString = servletContext.getInitParameter("textString");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : textString");
			}

			try
			{
				siteURL = servletContext.getInitParameter("siteURL");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : siteURL");
			}

			try
			{
				thumbnailWidth = servletContext.getInitParameter("thumbnailWidth");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : thumbnailWidth");
			}

			try
			{
				logoThumbnailWidth = servletContext.getInitParameter("logoThumbnailWidth");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : logoThumbnailWidth");
			}

			try
			{
				logoThumbnailHeight = servletContext.getInitParameter("logoThumbnailHeight");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : logoThumbnailHeight");
			}

			try
			{
				thumbnailHeight = servletContext.getInitParameter("thumbnailHeight");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : thumbnailHeight");
			}
			try
			{
				imageMagickInstallDir = servletContext.getInitParameter("imageMagickInstallDir");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : imageMagickInstallDir");
			}
			try
			{
				lesscBinaryPath = servletContext.getInitParameter("lesscBinaryPath");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : lesscBinaryPath");
			}
			try
			{
				crmUrl = servletContext.getInitParameter("crmUrl");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : crmUrl");
			}
			try
			{
				uploadServlet = servletContext.getInitParameter("uploadServlet");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : uploadServlet");
			}
			try
			{
				mailServerSMTP = servletContext.getInitParameter("mailServerSMTP");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : mailServerSMTP");
			}
			try
			{
				fromEmailAddressSubscribeModule = servletContext.getInitParameter("fromEmailAddressSubscribeModule");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : fromEmailAddressSubscribeModule");
			}
			try
			{
				fromEmailAddressRecallModule = servletContext.getInitParameter("fromEmailAddressRecallModule");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : fromEmailAddressRecallModule");
			}
			try
			{
				shopOrderEmailAddress = servletContext.getInitParameter("shopOrderEmailAddress");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : shopOrderEmailAddress");
			}
			try
			{
				usernameSMTPserver = servletContext.getInitParameter("usernameSMTPserver");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : usernameSMTPserver");
			}
			try
			{
				passwordSMTPserver = servletContext.getInitParameter("passwordSMTPserver");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : passwordSMTPserver");
			}
			try
			{
				fromEmailRealName = servletContext.getInitParameter("fromEmailRealName");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : fromEmailRealName");
			}
			try
			{
				contactUsEmail = servletContext.getInitParameter("contactUsEmail");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : contactUsEmail");
			}
			try
			{
				whereToBuyEmail = servletContext.getInitParameter("whereToBuyEmail");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : whereToBuyEmail");
			}

			try
			{
				GoogleAnalyticsUser = servletContext.getInitParameter("GoogleAnalyticsUser");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : GoogleAnalyticsUser");
			}
			try
			{
				GoogleAnalyticsPass = servletContext.getInitParameter("GoogleAnalyticsPass");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : GoogleAnalyticsPass");
			}
			try
			{
				GoogleAnalyticsProfileName = servletContext.getInitParameter("GoogleAnalyticsProfileName");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : GoogleAnalyticsProfileName");
			}

			try
			{
				DBProvider = servletContext.getInitParameter("DBProvider");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : DBProvider");
			}
			try
			{
				TomcatVersion = servletContext.getInitParameter("TomcatVersion");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : TomcatVersion");
			}
			try
			{
				OSType = System.getProperty("os.name");
			}
			catch (Exception e)
			{
				logger.info("ERROR in System Ostype : OSType");
			}
			try
			{
				TempLinux = servletContext.getInitParameter("TempLinux");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : Templinux");
			}
			try
			{
				TempWindows = servletContext.getInitParameter("TempWindows");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : TempWindows");
			}
			try
			{
				LuceneIndexDir = servletContext.getInitParameter("LuceneIndexDir");
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : LuceneIndexDir");
			}
			try
			{
				AllocateCategoryAcess = servletContext.getInitParameter("Allocate Member Category Access");
				if (AllocateCategoryAcess == null)
					AllocateCategoryAcess = "off";
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : Allocate Member Category Access");
			}
			try
			{
				categoryNumberPerLoadInFolderTree = Integer.parseInt(servletContext.getInitParameter("CategoryNumberPerLoadInFolderTree"));
			}
			catch (Exception e)
			{
				categoryNumberPerLoadInFolderTree = 20;
				logger.info("ERROR in web.xml : CategoryNumberLoadInFolderTree");
			}
			try
			{
				assetNumberPerLoadInFolderTree = Integer.parseInt(servletContext.getInitParameter("AssetNumberPerLoadInFolderTree"));
			}
			catch (Exception e)
			{
				assetNumberPerLoadInFolderTree = 20;
				logger.info("ERROR in web.xml : AssetNumberLoadInFolderTree");
			}
			try
			{
				uploadFileExtWhiteList = StringUtils.split(servletContext.getInitParameter("UploadFileExtWhiteList"), ",");
			}
			catch (Exception e)
			{
				uploadFileExtWhiteList = new String[] {};
				logger.info("ERROR in web.xml : UploadFileExtWhiteList");
			}
			try
			{
				validImageExtensions = new HashSet<String>(new ArrayList<String>(Arrays.asList(StringUtils.split(servletContext.getInitParameter("validImageExtensions"), ","))));
			}
			catch (Exception e)
			{
				logger.info("ERROR in web.xml : validImageExtensions");
			}

			if (servletContext.getInitParameter("DEBUG").equals("true"))
				DEBUG = true;
		}
	}

	private String getTextValue(String nodeName, Document doc)
	{
		String propVal = null;
		NodeList nlist = doc.getElementsByTagName(nodeName);
		Node n = nlist.item(0);

		if (n.hasChildNodes())
		{
			Node text = n.getFirstChild();

			// logger.info( n.getNodeName() + " = "+text.getNodeValue());

			propVal = text.getNodeValue();
		}

		return (propVal);
	}

	/**
	 * returns TRUE if the user specified should NOT have access to the level
	 * requested in accessLevelRequired
	 */
	public boolean illegalAccess(String user, String accessLevelRequired)
	{
		if (getDEBUG())
			logger.info(user + " XXX " + accessLevelRequired);
		String query = "select AccessLevel_name from accesslevels al, useraccess a, users u, status s where s.status_id = u.status_id and s.status_name = 'Accepted' and a.user_id = u.user_id and al.accesslevel_id = a.accesslevel_id and user_name = ?";
		DBaccess db = new DBaccess();

		//Vector accessLevels = db.select(query);
		Vector accessLevels = db.selectQuerySingleCol(query, new String[] { user });

		if (accessLevels.contains(accessLevelRequired) || accessLevels.contains("Admin"))
			return false;
		else
			return true;
	}

	public String getWarName()
	{
		return (warName);
	}

	public String getAutoUnsubscribe()
	{
		return (autoUnsubscribe);
	}

	@Deprecated
	public static String getModuleLevels(String moduleName)
	{
		return String.valueOf(ModuleHelper.getInstance().getModuleLevels(moduleName));
	}

	@Deprecated
	public static int getModuleLevelsInt(String moduleName)
	{
		return ModuleHelper.getInstance().getModuleLevels(moduleName);
	}

	public boolean isExpireDateValid()
	{
		return true;
	}

	public boolean isStartDateValid()
	{
		return true;
	}

	public boolean isPreviewValid()
	{
		return true;
	}

	public void setDirEle(String n)
	{
		dirEle = n;
	}

	public void setDirCat(String n)
	{
		dirCat = n;
	}

	public void setDirHier(String n)
	{
		dirHier = n;
	}

	public String getDirEle()
	{
		return (dirEle);
	}

	public String getDirCat()
	{
		return (dirCat);
	}

	public String getDirHier()
	{
		return (dirHier);
	}

	public String getImageMagickDirectory()
	{
		return imageMagickInstallDir;
	}

	public String getLesscBinaryPath()
	{
		return lesscBinaryPath;
	}

	public String getCrmUrl()
	{
		return crmUrl;
	}

	// handlers
	public void setPath(String p)
	{
		path = p;
	}

	public String getPath()
	{
		return path;
	}

	public void setDirHandler(String d)
	{
		dirHandler = d;
	}

	public String getDirHandler()
	{
		return dirHandler;
	}

	public void setViewer(String v)
	{
		viewer = v;
	}

	public String getViewer()
	{
		return viewer;
	}

	// colour scheme
	public String getDarkColour()
	{
		return (darkColour);
	}

	public String getLightColour()
	{
		return (lightColour);
	}

	public String getTextString()
	{
		return (textString);
	}

	// email options
	public String getMailServerSMTP()
	{
		return (mailServerSMTP);
	}

	public String getFromEmailAddressSubscribeModule()
	{
		return (fromEmailAddressSubscribeModule);
	}

	public String getFromEmailAddressRecallModule()
	{
		return (fromEmailAddressRecallModule);
	}

	public String getUsernameSMTPserver()
	{
		return (usernameSMTPserver);
	}

	public String getPasswordSMTPserver()
	{
		return (passwordSMTPserver);
	}

	public String getFromEmailRealName()
	{
		if (fromEmailRealName == null)
			return "";
		else
			return (fromEmailRealName);
	}

	// login info
	public String getLic()
	{
		return (licence);
	}

//	public String DBpass() {
//		return (DBpass);
//	}
//
//	public String DBuser() {
//		return (DBuser);
//	}

	public String getOdbcConString()
	{
		return (ODBC);
	}

	public void setOdbcConString(String context)
	{
		String odbc = "jdbc:odbc:" + context;
		this.ODBC = odbc;
	}

	public String getShopOrderEmailAddress()
	{
		return (shopOrderEmailAddress);
	}

	public String getContactUsEmail()
	{
		return (contactUsEmail);
	}

	public String getWhereToBuyEmail()
	{
		return (whereToBuyEmail);
	}

	// brand / hierarchy bits
	public void setName(String x)
	{
		brand_name = x;
	}

	public void setID(String x)
	{
		brand_id = x;
	}

	public String getName()
	{
		return (brand_name);
	}

	public String getID()
	{
		return (brand_id);
	}

	public void setDEBUG(boolean on)
	{
		if (on)
			DEBUG = true;
		else
			DEBUG = false;
	}

	public String getDBProvider()
	{
		return (DBProvider);
	}

	public String getTomcatVersion()
	{
		return (TomcatVersion);
	}

	public boolean getDEBUG()
	{
		return (DEBUG);
	}

	public String getStoreContext()
	{
		return (storeContext);
	}

	public String getStoreDir()
	{
		return (storeDir);
	}

	public String getOSType()
	{
		return (OSType);
	}

	public String getOStypeTempDir()
	{

		if (OSType.equals("Linux"))
			return (TempLinux);
		else
			return (TempWindows);
	}

	public String getLuceneIndexDir()
	{
		return (LuceneIndexDir);
	}

	public String getOSTypeDirSep()
	{
		return File.separator;
	}

	public int getPageSize()
	{
		return (pageSize);
	}

	public String getDefImg()
	{
		return (defaultImg);
	}

	public String getInfoImg()
	{
		return (infoImg);
	}

	public String getUploadServlet()
	{
		return (uploadServlet);
	}

	public String getDownloadsDir()
	{
		return (downloads);
	}

	public String getWebSiteURL()
	{
		return siteURL;
	}

	public String getGoogleAnalyticsUser()
	{
		return GoogleAnalyticsUser;
	}

	public String getGoogleAnalyticsPass()
	{
		return GoogleAnalyticsPass;
	}

	public String getGoogleAnalyticsProfileName()
	{
		return GoogleAnalyticsProfileName;
	}

	public String formatDate(String fullDate, String formatStr)
	{
		// Need to extend this sometime so it handles real date formatting.
		// Date d = new GregorianCalendar()
		// SimpleDateFormat sdf = new SimpleDateFormat(formatStr);
		// logger.info("full DATE "+fullDate);
		if (fullDate == null || fullDate.equals(""))
			return ("");

		SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

		SimpleDateFormat screenFormat = new SimpleDateFormat(formatStr);// can place format string here
		Date currentDate = null;
		try
		{
			currentDate = dbDateFormat.parse(fullDate);
			// logger.info("current DATE "+currentDate.toString());
		}
		catch (ParseException pe)
		{
			logger.info("DB date format incorrect");
		}
		String startDate = screenFormat.format(currentDate);
		// logger.info("DATE "+startDate);
		return (startDate);
	}

	public String strCheck(String value, String intName)
	{
		String str = "";

		if (intName.startsWith("ATTRDATE_"))
			str = formatDate(value, "yyyy-MM-dd");
		else
			str = value;

		return str;
	}

	public void removeFile(String file, String id, String module, String col, String tableType)
	{
		// delete physical file
		File f = new File(file);
		String path = f.getPath();
		String parent = f.getParent();
		String fileName = f.getName();

		String p = getStoreDir() + "/" + path;
		File realFile = new File(p);
		boolean b = realFile.delete();
		logger.info("File delete " + p + " = " + b);
		// now remove thumb
		String thumbPath = "/" + getStoreDir() + "/" + parent + "/_thumb/" + fileName;
		File thumb = new File(thumbPath);
		b = thumb.delete();
		logger.info("File delete " + thumbPath + " = " + b);

		// are we dealing with a category or an element ?

		String table = "Elements_";
		String column = "Element_id";
		if (tableType.equalsIgnoreCase("Category") || tableType.equalsIgnoreCase("Categories"))
		{
			table = "Categories_";
			column = "Category_id";
		}
		else if (tableType.equals("emailmarketing"))
		{
			table = "";
		}

		// now remove attribute value
		String query = "update " + table + module + " set " + col + "='' where " + column + " = ?";
		DBaccess db = new DBaccess();
		int rows = db.updateData(query, new Object[] { id });
		logger.info(query + "\nrows updated = " + rows);

	}

	public String getModuleSearchIndex(String moduleName)
	{
		return "";
	}


	public static boolean isFileExtAllowed(String fileName)
	{
		if (uploadFileExtWhiteList == null)
		{
			return true;
		}
		for (String allowed : uploadFileExtWhiteList)
		{
			if (fileName.toLowerCase().endsWith(allowed.toLowerCase().trim()))
			{
				return true;
			}
		}
		return false;
	}

	public static String getWebDir()
	{
		return new File(Defaults.class.getClassLoader().getResource("ESAPI.properties").getFile()).getParentFile().getParentFile().getParentFile().getAbsolutePath();
	}
}
