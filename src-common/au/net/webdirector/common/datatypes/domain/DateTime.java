package au.net.webdirector.common.datatypes.domain;

import org.owasp.esapi.ESAPI;

public class DateTime extends DataType
{
	@Override
	public String getTypeName()
	{
		return "Datetime";
	}

	@Override
	public String getMarkup()
	{
		StringBuffer sb = new StringBuffer();
		String s = getValue();
		sb.append("<input class=\"form-control input-datetime " + getExtraClassName() + "\" type=text name='" + getInternalName() + "' value='" + s + "' id=\"" + getInternalName() + "\" " + getReadOnlyAttribute() + " " + getRequiredAttribute() + ">");

		return sb.toString();
	}

	@Override
	public String getValue()
	{
		String data = super.getValue();
		if (data != null & !data.equals(""))
			data = data.substring(0, 16);
		return ESAPI.encoder().encodeForHTMLAttribute(data);
	}

}
