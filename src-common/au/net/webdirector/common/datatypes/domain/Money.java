package au.net.webdirector.common.datatypes.domain;

import java.text.DecimalFormat;

public class Money extends AttributeValueDataType
{
	@Override
	public String getTypeName()
	{
		return "Money";
	}

	public static void main(String[] args)
	{
		String data = "199.198";
		System.out.println(new DecimalFormat("0.00").format(Double.parseDouble(data)));
		System.out.println(new DecimalFormat("0").format(Double.parseDouble(data)));
	}

	@Override
	public String getMarkup()
	{
		String s = "0.00";
		try
		{
			s = new DecimalFormat("0.00").format(Double.parseDouble(getValue()));
		}
		catch (Exception e)
		{
			s = "0.00";
		}
		return "<input class=\"form-control input-money " + getExtraClassName() + "\" type=\"text\" name=\"" + getInternalName() + "\" value=\"" + s + "\" id=\"" + getInternalName() + "\" data-max-length=\"20\" " + getReadOnlyAttribute() + " " + getRequiredAttribute() + ">";
	}
}
