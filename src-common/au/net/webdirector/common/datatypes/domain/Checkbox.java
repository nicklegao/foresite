package au.net.webdirector.common.datatypes.domain;

public class Checkbox extends AttributeValueDataType
{
	@Override
	public String getTypeName()
	{
		return "Checkbox";
	}

	@Override
	public String getMarkup()
	{
		StringBuffer sb = new StringBuffer();
		String data = getValue();
		if (!data.equals("1"))
			data = "0";
		sb.append("<label class='control-label'>");
		sb.append("<input class='form-control input-checkbox checkbox style-0 " + getExtraClassName() + "' type='checkbox' name='DUMMY_" + getInternalName() + "' " + ("1".equals(data) ? "checked" : "") + " " + getDisabledAttribute() + " " + getRequiredAttribute() + ">");
		sb.append("<span></span>");
		sb.append("<input type='hidden' name='" + getInternalName() + "' value='" + data + "'>");
		sb.append("</label>");
		return sb.toString();
	}

}
