package au.net.webdirector.common.datatypes.domain;

import java.util.HashMap;
import java.util.Map;

import au.net.webdirector.common.utils.HTMLUtils;
import au.net.webdirector.common.utils.module.DropDownBuilder;

public class DropDown extends DataType
{
	@Override
	public String getTypeName()
	{
		return "Dropdown";
	}

	@Override
	public String getMarkup()
	{
		HTMLUtils hu = new HTMLUtils();
		DropDownBuilder ddb = new DropDownBuilder();
		String s = "";
		StringBuffer sb = new StringBuffer();
		String data = getValue();
		if (data != null && !data.equals(""))
		{
			s = data;
		}
		Map<String, String> context = new HashMap<String, String>();
		context.put("ID", getId());
		DropDownBuilder.setSqlParamsToThreadContext(context);
		try
		{
			sb.append(hu.select(ddb.getDropDownValuesWithoutIdButWithPermission(getModule(), getInternalName(), getUser(), getTableType()), getInternalName(), s, " class=\"form-control input-dropdown " + getExtraClassName() + "\" id=\"" + getInternalName() + "\" " + getDisabledAttribute() + " " + getRequiredAttribute()));
			sb.append("<span class=\"input-group-btn lookup-btn\"><span class=\"btn btn-default\">Lookup</span></span>");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return sb.toString();
	}
}
