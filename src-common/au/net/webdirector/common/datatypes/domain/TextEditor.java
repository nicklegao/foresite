package au.net.webdirector.common.datatypes.domain;


public class TextEditor extends DataType
{
	@Override
	public String getTypeName()
	{
		return "Text (Editor)";
	}

	@Override
	public String getMarkup()
	{
		String editIcon = "<input type=\"button\" class=\"btn btn-default open-editor " + getExtraClassName() + "\" value=\"Open Content Editor\" data-src=\"" + getValue() + "\" id=\"" + getInternalName() + "\" " + getDisabledAttribute() + " " + getRequiredAttribute() + "/>";

		if (getId().equals("0"))
		{
			editIcon += "<textarea name='" + getInternalName() + "' style='display:none;'></textarea>";
		}
		else
		{
			editIcon += " <a data-toggle='modal' href='/webdirector/template/save/dialoge?formName=formBuilder&srcFile=" + getValue() + "' data-target='#myModal' class='btn btn-success' > Save Template </a>";
			editIcon += " <a data-toggle='modal' href='/webdirector/attrlong/delete/dialoge?srcFile=" + getValue() + "' data-target='#myModal' class='btn btn-warning' > <i class='fa fa-trash-o'></i> Delete </a>";
		}

		return editIcon;
	}

}
