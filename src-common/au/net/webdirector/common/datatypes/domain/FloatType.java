package au.net.webdirector.common.datatypes.domain;

import java.text.DecimalFormat;

public class FloatType extends AttributeValueDataType
{
	@Override
	public String getTypeName()
	{
		return "Float";
	}

	@Override
	public String getMarkup()
	{
		String s = "0.00";
		try
		{
			s = new DecimalFormat("0.00").format(Double.parseDouble(getValue()));
		}
		catch (Exception e)
		{
			s = "0.00";
		}
		return "<input class=\"form-control input-float " + getExtraClassName() + "\" type=\"text\" name=\"" + getInternalName() + "\" value=\"" + s + "\" id=\"" + getInternalName() + "\" data-max-length=\"20\" " + getReadOnlyAttribute() + " " + getRequiredAttribute() + ">";
	}

}
