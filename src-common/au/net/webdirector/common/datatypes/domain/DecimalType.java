package au.net.webdirector.common.datatypes.domain;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;

public class DecimalType extends AttributeValueDataType
{
	String size;

	public DecimalType(String size)
	{
		this.size = size;
	}

	@Override
	public String getTypeName()
	{
		return "Decimal(" + size + ")";
	}

	@Override
	public String getMarkup()
	{
		String s = StringUtils.isBlank(getValue()) ? "0.0" : getValue();
		return "<input class=\"form-control input-float " + getExtraClassName() + "\" type=\"text\" name=\"" + getInternalName() + "\" value=\"" + s + "\" id=\"" + getInternalName() + "\" data-max-length=\"20\" " + getReadOnlyAttribute() + " " + getRequiredAttribute() + ">";
	}

	public static void main(String[] args)
	{
		System.out.println(new BigDecimal("0.000000010").toString());
		System.out.println(String.format("%e", new BigDecimal("0.000000010").doubleValue()));
		System.out.println(new BigDecimal(Double.parseDouble("0.000000010")));
	}
}
