/**
 * @author Nick Yiming Gao
 * @date 2015-09-10
 */
package au.net.webdirector.common.datatypes.domain;

/**
 * 
 */

import org.apache.log4j.Logger;

public class Hidden extends AttributeValueDataType
{

	private static Logger logger = Logger.getLogger(Hidden.class);

	/* (non-Javadoc)
	 * @see au.net.webdirector.common.datatypes.domain.DataType#getMarkup()
	 */
	@Override
	public String getMarkup()
	{
		// TODO Auto-generated method stub
		return "<input type=\"hidden\" class=\"form-control input-hidden " + getExtraClassName() + "\" id=\"" + getInternalName() + "\" name=\"" + getInternalName() + "\" value=\"" + getValue() + "\" " + getRequiredAttribute() + "/>";
	}

	/* (non-Javadoc)
	 * @see au.net.webdirector.common.datatypes.domain.DataType#getTypeName()
	 */
	@Override
	public String getTypeName()
	{
		// TODO Auto-generated method stub
		return "Hidden";
	}
}
