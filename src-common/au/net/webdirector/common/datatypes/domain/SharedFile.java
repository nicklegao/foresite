/**
 * @author Nick Yiming Gao
 * @date 2015-5-8
 */
package au.net.webdirector.common.datatypes.domain;

/**
 * 
 */

import java.io.File;

import org.apache.log4j.Logger;

import webdirector.db.client.ClientFileUtils;

public class SharedFile extends DataType
{

	private static Logger logger = Logger.getLogger(SharedFile.class);

	/* (non-Javadoc)
	 * @see au.net.webdirector.common.datatypes.domain.DataType#getMarkup()
	 */
	@Override
	public String getMarkup()
	{
		ClientFileUtils cfu = new ClientFileUtils();
		String data = getValue();
		String urlPath = "";
		String storePath = "/admin/img/thumbnail/noThumb.gif";
		String fileSize = "";
		String filename = "";
		String display = "display:none;";
		boolean hasThumb = false;
		if (data != null && !data.equals(""))
		{
			File f = new File(data);
			try
			{
				urlPath = "/" + d.getStoreContext() + data.substring(0, data.lastIndexOf("/")) + "/" + f.getName();
			}
			catch (Exception e)
			{
				urlPath = "";
			}
			if (d.doesFileExist(f.getParent(), f.getName()))
			{
				display = "";
				filename = f.getName();
				fileSize = "(" + cfu.getFileSizeInStore(data) + ")";
				if (isImage(f.getName()))
				{
					hasThumb = true;
					storePath = urlPath;
				}
			}
		}
		StringBuffer sb = new StringBuffer();
		sb.append("<div id='img_" + getInternalName() + "_" + getId() + "'>");
		sb.append("  <div class='thumb-container' style='" + display + "'>");
		sb.append("   <div class='delete-actions'>");
		sb.append("     <a class='thumb' target='_new' href=\"" + urlPath + "\">");
		if (hasThumb)
		{
			sb.append("     <img src='" + storePath + "'>");
		}
		else
		{
			sb.append("     <i class='fa fa-file-o'></i>");
		}
		sb.append("     </a>");
		sb.append("   </div>");
		sb.append("   <div class='delete-actions' id='actions'><a class='removeSharedFileAnchor trash' href='#' data-field='" + getInternalName() + "' data-id='" + getId() + "' data-file='" + data + "'><button type='button' class='btn btn-danger'>Clear</button></a></div>");
		sb.append("   <div class='desc'>");
		sb.append("     <span class=\"filename\">" + filename + "</span><span class=\"filesize\">" + fileSize + "</span>");
		sb.append("   </div>");
		sb.append("  </div>");
		sb.append("</div>");
		sb.append("<input type=\"hidden\" class=\"input-sharedfile\" name='" + getInternalName() + "' style='display:none;' value=\"" + data + "\" " + getRequiredAttribute() + ">");
		sb.append("<input type=\"button\" class=\"btn btn-default open-sharedfile-manager " + getExtraClassName() + "\" value=\"Open File Manager\" id=\"" + getInternalName() + "\" " + getDisabledAttribute() + "/>");
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see au.net.webdirector.common.datatypes.domain.DataType#getTypeName()
	 */
	@Override
	public String getTypeName()
	{
		return "SharedFile";
	}

	private boolean isImage(String fileName)
	{
		int pos = fileName.lastIndexOf(".");
		String extName = pos == -1 ? "" : fileName.substring(pos + 1);
		return "jpg,jpeg,gif,png".indexOf(extName.toLowerCase()) != -1;
	}
}
