package au.net.webdirector.common.datatypes.domain;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

public class DropDownData implements Comparable
{
//	public static final String	SELECTOR_VALUE	= "SELECTOR_VALUE";
	public static final String SELECTOR_TEXT = "SELECTOR_TEXT";
	String value;
	String text;
	Map<String, String> dataFromTable = new LinkedHashMap<String, String>();
	Vector<String[]> columnName = new Vector<String[]>();

	public Vector<String[]> getColumnName()
	{
		return columnName;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public Map<String, String> getDataFromTable()
	{
		return dataFromTable;
	}

	public void setDataFromTable(Vector<String[]> columnName, Map<String, String> dataFromTable)
	{
		this.dataFromTable = dataFromTable;
		this.columnName = columnName;
	}

	public int compareTo(Object obj)
	{
		if (obj instanceof DropDownData)
		{
			DropDownData another = (DropDownData) obj;
			return this.text.toUpperCase().compareTo(another.getText().toUpperCase());
		}
		throw new InvalidParameterException("Illegal parameter:" + obj.getClass().getName());
	}

	public boolean contains(String columnName, String value)
	{
//		if (columnName.equalsIgnoreCase(SELECTOR_VALUE)) {
//			return this.value.toLowerCase().indexOf(value.toLowerCase()) != -1;
//		}
		if (columnName.equalsIgnoreCase(SELECTOR_TEXT))
		{
			return this.text.toLowerCase().indexOf(value.toLowerCase()) != -1;
		}
		if (!dataFromTable.containsKey(columnName))
		{
			return false;
		}
		return dataFromTable.get(columnName).toLowerCase().indexOf(value.toLowerCase()) != -1;
	}

	public boolean contains(String value)
	{
		if (contains(SELECTOR_TEXT, value))
		{
			return true;
		}
		for (String[] colName : this.columnName)
		{
			if (contains(colName[0], value))
			{
				return true;
			}
		}
		return false;
	}

	public boolean contains(int[] cols, String value)
	{
		if (contains(SELECTOR_TEXT, value))
		{
			return true;
		}
		for (int colidx : cols)
		{
			if (contains(this.columnName.get(colidx)[0], value))
			{
				return true;
			}
		}
		return false;
	}

	public String printTable()
	{
		int[] cols = new int[this.columnName.size()];
		for (int i = 0; i < cols.length; i++)
		{
			cols[i] = i;
		}
		return printTable(cols);
	}

	public String printTable(int[] cols)
	{
		StringBuffer sb = new StringBuffer("<tr>");
		sb.append("<td style=\"display:none;\">").append(getValue()).append("</td>");
		sb.append("<td>").append(getText()).append("</td>");
		for (int col : cols)
		{
			sb.append("<td>").append(dataFromTable.get(columnName.get(col)[0])).append("</td>");
		}
		sb.append("</tr>");
		return sb.toString();
	}

	public int[] getShownColumnsOld(HttpServletRequest request)
	{
		Enumeration<String> names = request.getParameterNames();
		List<Integer> temp = new ArrayList<Integer>();
		Integer switchOnCol = -1;
		Integer switchOffCol = -1;
		while (names.hasMoreElements())
		{
			String name = names.nextElement();
			if (name.startsWith("show_"))
			{
				String showColName = name.substring("show_".length());
				for (int i = 0; i < columnName.size(); i++)
				{
					if (showColName.equalsIgnoreCase(columnName.get(i)[0]))
					{
						temp.add(i);
					}
				}
			}
			else if (name.equals("columnSelector"))
			{
				switchOnCol = new Integer(request.getParameter(name));
			}
			else if (name.equals("shutOffCol"))
			{
				switchOffCol = new Integer(request.getParameter(name));
			}
		}
		if (switchOnCol != -1)
		{
			if (!temp.contains(switchOnCol))
			{
				temp.add(switchOnCol);
			}
		}

		if (switchOffCol != -1)
		{
			if (temp.contains(switchOffCol))
			{
				temp.remove(switchOffCol);
			}
		}
		Collections.sort(temp);
		int[] ret = new int[temp.size()];
		for (int i = 0; i < ret.length; i++)
		{
			ret[i] = temp.get(i).intValue();
		}
		return ret;
	}

	public int[] getShownColumns(HttpServletRequest request)
	{
		String str = request.getParameter("showCols");
		if (str.equals(","))
		{
			return new int[0];
		}
		str = str.substring(1, str.length() - 1);
		String[] showCols = str.split(",");
		List<Integer> temp = new ArrayList<Integer>();
		for (String showCol : showCols)
		{
			temp.add(new Integer(showCol));
		}
		Collections.sort(temp);
		int[] ret = new int[temp.size()];
		for (int i = 0; i < ret.length; i++)
		{
			ret[i] = temp.get(i).intValue();
		}
		return ret;
	}

}
