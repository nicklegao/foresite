package au.net.webdirector.common.datatypes.domain;

import java.util.HashMap;
import java.util.Map;

import au.net.webdirector.common.utils.HTMLUtils;
import au.net.webdirector.common.utils.module.DropDownBuilder;

public class DropDownMulti extends DataType
{
	@Override
	public String getTypeName()
	{
		return "Multi Drop";
	}

	@Override
	public String getMarkup()
	{
		HTMLUtils hu = new HTMLUtils();
		DropDownBuilder ddb = new DropDownBuilder();
		StringBuffer sb = new StringBuffer();
		String data = getValue();
		String s = "";
		if (data != null && !data.equals(""))
		{
			s = data;
		}
		Map<String, String> context = new HashMap<String, String>();
		context.put("ID", getId());
		DropDownBuilder.setSqlParamsToThreadContext(context);
		try
		{
			sb.append(hu.multiSelectFromMapEntries(ddb.getDropDownValuesWithoutIdButWithPermission(getModule(), getInternalName(), getUser(), getTableType()).entrySet().iterator(), getInternalName(),
					"multiple='true' class=\"form-control input-multidrop " + getExtraClassName() + "\" " + getDisabledAttribute() + " " + getRequiredAttribute(), "", getModule(), getId(), getTableType()));
			sb.append("<input type=\"hidden\" name=\"DUMMY" + getInternalName() + "\" value=\"\">");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return sb.toString();
	}

}
