/**
 * 5:26:42 PM 30/04/2012
 * @author Vito Lefemine
 */
package au.net.webdirector.common.datatypes.domain;

/**
 * @author Vito Lefemine
 * 
 */
public class PhoneNumber extends AttributeValueDataType
{
	@Override
	public String getTypeName()
	{
		return "PhoneNumber(20)";
	}

	@Override
	public String getMarkup()
	{
		return "<input class=\"form-control input-phonenumber " + getExtraClassName() + "\" type=text name='" + getInternalName() + "' value='" + getValue() + "' id=\"" + getInternalName() + "\" " + getReadOnlyAttribute() + " " + getRequiredAttribute() + ">";
	}

}
