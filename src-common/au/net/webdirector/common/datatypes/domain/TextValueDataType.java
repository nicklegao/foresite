/**
 * @author Nick Yiming Gao
 * @date 2015-9-24
 */
package au.net.webdirector.common.datatypes.domain;

/**
 * 
 */

import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;

public abstract class TextValueDataType extends DataType
{

	private static Logger logger = Logger.getLogger(TextValueDataType.class);

	@Override
	public String getValue()
	{
		return ESAPI.encoder().encodeForHTML(super.getValue());
	}
}
