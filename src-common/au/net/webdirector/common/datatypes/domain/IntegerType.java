package au.net.webdirector.common.datatypes.domain;

import java.text.DecimalFormat;

public class IntegerType extends AttributeValueDataType
{
	@Override
	public String getTypeName()
	{
		return "Integer";
	}

	@Override
	public String getMarkup()
	{
		String s = "0";
		try
		{
			s = new DecimalFormat("0").format(Double.parseDouble(getValue()));
		}
		catch (Exception e)
		{
			s = "0";
		}
		return "<input class=\"form-control input-integer " + getExtraClassName() + "\" type=\"text\" name=\"" + getInternalName() + "\" value=\"" + s + "\" id=\"" + getInternalName() + "\" data-max-length=\"20\" " + getReadOnlyAttribute() + " " + getRequiredAttribute() + ">";
	}

}
