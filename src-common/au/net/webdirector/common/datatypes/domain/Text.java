package au.net.webdirector.common.datatypes.domain;

import org.owasp.esapi.ESAPI;

import webdirector.db.client.ClientFileUtils;

public class Text extends DataType
{
	@Override
	public String getTypeName()
	{
		return "Text (Non-Editor)";
	}

	@Override
	public String getMarkup()
	{
		StringBuffer sb = new StringBuffer();
		String data = getValue();
		ClientFileUtils cfu = new ClientFileUtils();

		// first we need to read in file contents
		if (data != null && !data.equals(""))
		{
			String field = ESAPI.encoder().encodeForHTML(cfu.readTextContents(cfu.getFilePathFromStoreName(data), true).toString());
			sb.append("<textarea class=\"form-control input-text " + getExtraClassName() + "\" name=\"" + getInternalName() + "\" id=\"" + getInternalName() + "\" " + getReadOnlyAttribute() + " " + getRequiredAttribute() + ">" + field + "</textarea>");
		}
		else
		{
			sb.append("<textarea class=\"form-control input-text " + getExtraClassName() + "\" name=\"" + getInternalName() + "\" id=\"" + getInternalName() + "\" " + getReadOnlyAttribute() + " " + getRequiredAttribute() + "></textarea>");
		}
		return sb.toString();
	}

}
