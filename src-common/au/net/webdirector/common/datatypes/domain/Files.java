package au.net.webdirector.common.datatypes.domain;

import java.io.File;

import webdirector.db.client.ClientFileUtils;

public class Files extends DataType
{
	@Override
	public String getTypeName()
	{
		return "File";
	}

	@Override
	public String getMarkup()
	{
		StringBuffer sb = new StringBuffer();
		ClientFileUtils cfu = new ClientFileUtils();
		String data = getValue();
		if (data != null && !data.equals(""))
		{
			File f = new File(data);

			String storePath = "/admin/img/thumbnail/noThumb.gif";
			String urlPath;
			try
			{
				urlPath = "/" + d.getStoreContext() + data.substring(0, data.lastIndexOf("/")) + "/" + f.getName();
			}
			catch (Exception e)
			{
				urlPath = "";
			}
			if (d.doesFileExist(f.getParent(), f.getName()))
			{
				System.out.println("BM File name:::" + f.getName());
				if (d.doesThumbFileExist(f.getParent(), f.getName()))
				{
					storePath = "/" + d.getStoreContext() + data.substring(0, data.lastIndexOf("/")) + "/_thumb/" + f.getName();
				}

				String fileSize = "(" + cfu.getFileSizeInStore(data) + ")";

				sb.append("<div id='img_" + getInternalName() + "_" + getId() + "'>");
				sb.append("<div class='thumb-container'>");
				sb.append("<div class='delete-actions'><a class='thumb' target='_new' href=\"" + urlPath + "\"><img src='" + storePath + "'></a></div>");
				sb.append("<div class='delete-actions' id='actions'><a class='removeFileAnchor trash' href='#' data-field='" + getInternalName() + "' data-id='" + getId() + "' data-file='" + data + "'><button type='button' class='btn btn-danger'>Delete</button></a></div>");
				sb.append("<div class='desc'>");
				sb.append("<span class=\"filename\">Filename: " + f.getName() + "</span><span>" + fileSize + "</span>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
			}
		}
		sb.append("<input class=\"btn btn-default input-file " + getExtraClassName() + "\" type=\"file\" name=" + getInternalName() + " id=\"" + getInternalName() + "\" " + getDisabledAttribute() + " " + getRequiredAttribute() + ">");
		return sb.toString();
	}

}
