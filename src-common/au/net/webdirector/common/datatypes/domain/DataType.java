package au.net.webdirector.common.datatypes.domain;


import org.apache.log4j.Logger;

import au.net.webdirector.common.Defaults;

public abstract class DataType
{
	static Logger logger = Logger.getLogger(DataType.class);

	protected boolean unique;
	protected boolean index;
	protected boolean mandatory;
	protected String internalName;
	protected String externalName;
	protected String labelInfo;
	protected String value = "";
	protected String id = "0";
	protected String tableType;
	protected String user;
	protected String module;
	protected String defaultValue;
	protected int displayOrder;
	protected boolean on;
	protected String labelId;
	protected String loginRequired;
	protected String export;
	protected String categoryAttributeLevel;
	protected String tabName;
	protected int luceneRank;
	protected boolean luceneTokenized;
	protected boolean readOnly;
	protected boolean noChange;

	protected Defaults d = Defaults.getInstance();


	public int getLuceneRank()
	{
		return luceneRank;
	}

	public void setLuceneRank(int luceneRank)
	{
		this.luceneRank = luceneRank;
	}

	public boolean isLuceneTokenized()
	{
		return luceneTokenized;
	}

	public void setLuceneTokenized(boolean luceneTokenized)
	{
		this.luceneTokenized = luceneTokenized;
	}

	public boolean isIndex()
	{
		return index;
	}

	public void setIndex(boolean index)
	{
		this.index = index;
	}

	public String getDefaultValue()
	{
		if (defaultValue == null)
		{
			return "";
		}
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue)
	{
		this.defaultValue = defaultValue;
	}

	public boolean isMandatory()
	{
		return mandatory;
	}

	public boolean isUnique()
	{
		return unique;
	}

	public void setUnique(boolean unique)
	{
		this.unique = unique;
	}

	public void setMandatory(boolean mandatory)
	{
		this.mandatory = mandatory;
	}

	public String getInternalName()
	{
		return internalName;
	}

	public void setInternalName(String internalName)
	{
		this.internalName = internalName;
	}

	public String getExternalName()
	{
		return externalName;
	}

	public void setExternalName(String externalName)
	{
		this.externalName = externalName;
	}

	public String getLabelInfo()
	{
		return labelInfo;
	}

	public void setLabelInfo(String labelInfo)
	{
		this.labelInfo = labelInfo;
	}

	public String getValue()
	{
		if (id.equals("0"))
		{
			return getDefaultValue();
		}
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getTableType()
	{
		return tableType;
	}

	public void setTableType(String tableType)
	{
		this.tableType = tableType;
	}

	public String getUser()
	{
		return user;
	}

	public void setUser(String user)
	{
		this.user = user;
	}

	public String getModule()
	{
		return module;
	}

	public void setModule(String module)
	{
		this.module = module;
	}

	public int getDisplayOrder()
	{
		return displayOrder;
	}

	public void setDisplayOrder(int displayOrder)
	{
		this.displayOrder = displayOrder;
	}

	public boolean isOn()
	{
		return on;
	}

	public void setOn(boolean on)
	{
		this.on = on;
	}

	public String getLabelId()
	{
		return labelId;
	}

	public void setLabelId(String labelId)
	{
		this.labelId = labelId;
	}

	public String getLoginRequired()
	{
		return loginRequired;
	}

	public void setLoginRequired(String loginRequired)
	{
		this.loginRequired = loginRequired;
	}

	public String getExport()
	{
		return export;
	}

	public void setExport(String export)
	{
		this.export = export;
	}

	/**
	 * @return the readOnly
	 */
	public boolean isReadOnly()
	{
		return readOnly;
	}

	/**
	 * @param readOnly
	 *            the readOnly to set
	 */
	public void setReadOnly(boolean readOnly)
	{
		this.readOnly = readOnly;
	}

	public boolean isNoChange()
	{
		return noChange;
	}

	public void setNoChange(boolean noChange)
	{
		this.noChange = noChange;
	}

	public String getExtraClassName()
	{
		if (this.noChange)
		{
			return "wd-no-change";
		}
		return "";
	}

	public String getReadOnlyAttribute()
	{
		if (this.readOnly)
			return "readonly";
		else
			return "";
	}

	public String getRequiredAttribute()
	{
		if (this.mandatory)
			return "required";
		else
			return "";
	}

	public String getDisabledAttribute()
	{
		if (this.readOnly)
			return "disabled";
		else
			return "";
	}

	public String getCategoryAttributeLevel()
	{
		return categoryAttributeLevel;
	}

	public void setCategoryAttributeLevel(String categoryAttributeLevel)
	{
		this.categoryAttributeLevel = categoryAttributeLevel;
	}

	public String getTabName()
	{
		return tabName;
	}

	public void setTabName(String tabName)
	{
		this.tabName = tabName;
	}

	public abstract String getMarkup();

	public abstract String getTypeName();

	/**
	 * 
	 * Factory Method to get appropriate datatype
	 * 
	 * @param label
	 * @param ColSize
	 * @return
	 */
	public static DataType getDataType(String label, String ColSize)
	{
		if (label.startsWith("ATTRFILE"))
		{
			return new Files();
		}
		if (label.startsWith("ATTRDATE"))
		{
			return new DateTime();
		}
		if (label.startsWith("ATTRINTEGER"))
		{
			return new IntegerType();
		}
		if (label.startsWith("ATTRFLOAT"))
		{
			return new FloatType();
		}
		if (label.startsWith("ATTRDECIMAL"))
		{
			return new DecimalType(ColSize);
		}
		if (label.startsWith("ATTRCURRENCY"))
		{
			return new Money();
		}
		if (label.startsWith("ATTRLONG"))
		{
			return new TextEditor();
		}
		if (label.startsWith("ATTRTEXT"))
		{
			return new Text();
		}
		if (label.startsWith("ATTRCHECK"))
		{
			return new Checkbox();
		}
		if (label.startsWith("ATTRDROP"))
		{
			return new DropDown();
		}
		if (label.startsWith("ATTRMULTI"))
		{
			return new DropDownMulti();
		}
		if (label.startsWith("ATTRNOTES"))
		{
			return new Notes();
		}
		if (label.startsWith("ATTRPASS"))
		{
			return new Password();
		}
		if (label.startsWith("ATTRSHAREDFILE"))
		{
			return new SharedFile();
		}
		return new Char(Integer.parseInt(ColSize));
	}

}
