package au.net.webdirector.common.datatypes.domain;

import au.net.webdirector.common.utils.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import org.owasp.esapi.ESAPI;

import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.StringArrayMapper;


public class Notes extends DataType
{
	@Override
	public String getTypeName()
	{
		return "Notes";
	}


	@Override
	public String getMarkup()
	{
		StringBuffer sb = new StringBuffer();
		DataAccess db = DataAccess.getInstance();

		// using the asset ID look up the foriegn key in the notes table and display in a table
		//String cols[] = { "createDate", "content", "who", "id" }; // why is this here
		String query = "select createDate, content, who, id, live, live_date, expire_date from notes where foriegnKey = ? and moduleName = ? and elementOrCategory = ? and fieldName like ? order by createDate DESC, id DESC";
		Vector results = new Vector(db.select(query, new Object[] { getId(), getModule(), getTableType(), getInternalName() + "%" }, new StringArrayMapper()));

		sb.append("<div>");

		// new row
		//
		int newRow = 0;
		if (results != null && results.size() > 0)
			newRow = 0;

		sb.append("<div class='note new_note'>");
		sb.append("<input type='hidden' name='noteWho-" + newRow + "' id='noteWho-" + newRow + "' value='" + getUser() + "'>");
		sb.append("<input type='hidden' name='noteCreate-" + newRow + "' id='noteCreate-" + newRow + "' value='" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "'>");

		sb.append("<div class='desc'>");
		sb.append("<span>Create new:</span>");
		sb.append("</div>");
		sb.append("<div class='note-content'><textarea class='form-control input-note " + getExtraClassName() + "' name='note-" + getInternalName() + "-" + newRow + "' id='note-" + getInternalName() + "-" + newRow + "' " + getReadOnlyAttribute() + " " + getRequiredAttribute() + "></textarea>");
		sb.append("<p class='note'>" + this.getLabelInfo() + "</p></div>");

		sb.append("<div class='row'>");

		sb.append("<div class='col-xs-2'>");
		sb.append("<label><input type='checkbox' class='checkbox input-checkbox' name='noteLive-" + getInternalName() + "' checked><span> Live </span></label>");
		sb.append("</div>");
		sb.append("<div class='col-xs-5'>");
		sb.append("<label class='pos-relative'><span style='margin: 4px; float: left;'>Live Date: </span><input type='text' name='noteLiveDate-" + getInternalName() + "' class='short-input input-datetime form-control input-xs'></label>");
		sb.append("</div>");
		sb.append("<div class='col-xs-5'>");
		sb.append("<label class='pos-relative'><span style='margin: 4px; float: left;'>Expire Date: </span><input type='text' name='noteExpireDate-" + getInternalName() + "' class='short-input input-datetime form-control input-xs'></label>");
		sb.append("</div>");

		sb.append("</div>");

		sb.append("<hr>");
		sb.append("</div>");


		for (int i = 0; i < results.size(); i++)
		{
			String[] columns = (String[]) results.elementAt(i);
			sb.append("<div class='note' id='trNote" + columns[3] + "'>");
			sb.append("<input type='hidden' name='noteWho-" + columns[3] + "' id='noteWho-" + columns[3] + "' value='" + columns[2] + "'>");
			sb.append("<input type='hidden' name='noteCreate-" + columns[3] + "' id='noteCreate-" + i + "' value='" + columns[0].substring(0, 19) + "'>");

			sb.append("<div class='desc'>");
			sb.append("<span class='note-who'>" + columns[2] + "</span>");
			sb.append("<span class='note-time'>" + columns[0].substring(0, 19) + "</span>");
			sb.append("<a class='removeNoteAnchor trash btn btn-danger btn-xs' id='" + columns[3] + "' href='#'><i class='fa fa-trash-o'></i></a>");
			sb.append("</div>");
			sb.append("<div class='note-content'><textarea rows='3' class='form-control input-note " + getExtraClassName() + "' name='note-" + getInternalName() + "-" + columns[3]
					+ "' id='note-" + getInternalName() + "-" + columns[3] + "' " + getReadOnlyAttribute() + " " + getRequiredAttribute() + ">"
					+ ESAPI.encoder().encodeForHTML(columns[1]) + "</textarea>");
			sb.append("<p class='note'>" + this.getLabelInfo() + "</p></div>");

			sb.append("<div class='row'>");

			sb.append("<div class='col-xs-2'>");
			sb.append("<label><input type='checkbox' class='checkbox input-checkbox' name='noteLive-" + columns[3] + "' " + (columns[4].equals("1") ? "checked" : "") + "><span> Live </span></label>");
			sb.append("</div>");
			sb.append("<div class='col-xs-5'>");
			sb.append("<label class='pos-relative'><span style='margin: 4px; float: left;'>Live Date: </span><input type='text' name='noteLiveDate-" + columns[3] + "' value='" + columns[5] + "' class='short-input input-datetime form-control input-xs'></label>");
			sb.append("</div>");
			sb.append("<div class='col-xs-5'>");
			sb.append("<label class='pos-relative'><span style='margin: 4px; float: left;'>Expire Date: </span><input type='text' name='noteExpireDate-" + columns[3] + "'  value='" + columns[6] + "' class='short-input input-datetime form-control input-xs'></label>");
			sb.append("</div>");

			sb.append("</div>");

			sb.append("<hr>");
			sb.append("</div>");

		}

		sb.append("</div>");
		return sb.toString();
	}



}
