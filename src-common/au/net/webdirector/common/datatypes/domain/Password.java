package au.net.webdirector.common.datatypes.domain;

import org.apache.commons.lang.StringUtils;

import au.net.webdirector.common.utils.password.PasswordUtils;

public class Password extends AttributeValueDataType
{
	@Override
	public String getTypeName()
	{
		return "Password";
	}

	@Override
	public String getValue()
	{
		if (StringUtils.isBlank(value))
		{
			return "";
		}
		return PasswordUtils.FAKE_PASSWD;
	}

	@Override
	public String getMarkup()
	{
		return "<input class=\"form-control input-password " + getExtraClassName() + "\" type=\"password\" name=\"" + getInternalName() + "\" value=\"" + getValue() + "\" id=\"" + getInternalName() + "\" data-max-length=\"20\" " + getReadOnlyAttribute() + " " + getRequiredAttribute() + ">";
	}

}
