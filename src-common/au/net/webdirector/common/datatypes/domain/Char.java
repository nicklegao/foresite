package au.net.webdirector.common.datatypes.domain;

import org.owasp.esapi.ESAPI;

public class Char extends DataType
{

	int size;

	Char(int size)
	{
		this.size = size;
	}

	@Override
	public String getMarkup()
	{
		if (size >= 255)
		{
			return "<textarea class=\"form-control input-text " + getRequiredAttribute() + " " + getExtraClassName() + "\" name=\"" + getInternalName() + "\" id=\"" + getInternalName() + "\" data-max-length=\"" + size + "\" " + getReadOnlyAttribute() + ">" + getValue() + "</textarea>";
		}
		return "<input class=\"form-control input-char " + getRequiredAttribute() + " " + getExtraClassName() + "\" type=\"text\" name=\"" + getInternalName() + "\" value=\"" + getValue() + "\" id=\"" + getInternalName() + "\" data-max-length=\"" + size + "\" " + getReadOnlyAttribute() + ">";
	}

	@Override
	public String getTypeName()
	{
		if (size > 255)
			return "Varchar(" + size + ")";
		// TODO Auto-generated method stub
		return "Char(" + size + ")";
	}

	@Override
	public String getValue()
	{
		if (size >= 255)
			return ESAPI.encoder().encodeForHTMLAttribute(super.getValue());
		return ESAPI.encoder().encodeForHTML(super.getValue());
	}
}
