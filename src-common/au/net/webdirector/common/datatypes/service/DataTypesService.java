package au.net.webdirector.common.datatypes.service;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
import au.net.webdirector.common.datatypes.domain.DataType;

public class DataTypesService
{

	static Logger logger = Logger.getLogger(DataTypesService.class);

	DataAccess DA = DataAccess.getInstance();

	/**
	 * 
	 * Loads a list of labels in module and converts them to the correct data
	 * type
	 * 
	 * @param module
	 * @param isCategory
	 * @param user
	 * @return
	 */
	public List<DataType> loadGenericDataTypes(String module, boolean isCategory, String user)
	{
		String tableName = isCategory ? "categorylabels_" + module : "labels_" + module;
		String query = "select * from " + tableName + " order by display_order ";
		List<Hashtable<String, String>> labels = DA.select(query, new HashtableMapper());
		Vector<String[]> metaData = DA.getColsMetaData(isCategory ? "categories_" + module : "elements_" + module);

		List<DataType> types = new ArrayList<DataType>();
		for (Hashtable<String, String> label : labels)
		{
			int colSize = 0;
			for (String[] meta : metaData)
			{
				if (meta[0].equalsIgnoreCase((label.get("Label_InternalName"))))
				{
					DataType data = DataType.getDataType(label.get("Label_InternalName"), meta[2]);
					data.setModule(module);
					data.setExternalName(label.get("Label_ExternalName"));
					data.setDisplayOrder(label.get("display_order").equals("") ? 0 : Integer.parseInt(label.get("display_order")));
					data.setInternalName(label.get("Label_InternalName"));
					data.setLabelInfo(label.get("Label_Info"));
					data.setTabName(label.get("Label_TabName"));
					data.setDefaultValue(label.get("Label_DefaultValue"));
					data.setUnique(label.get("Label_Unique").equals("1"));
					data.setIndex(label.get("Label_Index").equals("1"));
					data.setLuceneRank(Integer.parseInt(label.get("Label_rank")));
					data.setLuceneTokenized(label.get("Label_tokenized").equals("1"));
					data.setMandatory(label.get("Label_Mandatory").equals("1"));
					data.setOn(label.get("Label_OnOff").equals("1"));
					data.setTableType(isCategory ? "Categories" : "Elements");
					data.setUser(user);
					data.setLabelId(label.get("Label_id"));
					data.setExport(label.get("Label_Export"));
					data.setLoginRequired(label.get("Label_LoginRequired"));
					data.setReadOnly(label.get("Label_ReadOnly").equals("1"));
					data.setNoChange("1".equals(label.get("Label_NoChange")));
					if (isCategory)
					{
						data.setCategoryAttributeLevel(label.get("Attribute_Level"));
					}
					types.add(data);
					break;
				}
			}
		}
		return types;
	}

	/**
	 * Loads all the module labels, but fills the DataTypes with the content
	 * contained in the hashtable
	 * 
	 * @param module
	 * @param isCategory
	 * @param user
	 * @param row
	 * @return
	 */
	public List<DataType> loadDataTypesWithContent(String module, boolean isCategory, String user, DataMap row)
	{
		String selector = isCategory ? "Category_id" : "Element_id";
		List<DataType> dataTypes = loadGenericDataTypes(module, isCategory, user);
		for (DataType data : dataTypes)
		{
			data.setValue(row.getString(data.getInternalName()));
			data.setId(row.getString(selector));
		}
		return dataTypes;
	}

	public List<DataType> filterOutHiddenDataTypes(List<DataType> types)
	{
		List<DataType> notHidden = new ArrayList<DataType>();
		for (DataType type : types)
		{
			if (type.isOn())
			{
				notHidden.add(type);
			}
		}
		return notHidden;
	}

	public List<DataType> filterOutHiddenDataTypesAndCatLevels(List<DataType> types, String displayLevel)
	{
		List<DataType> notHidden = new ArrayList<DataType>();
		for (DataType type : types)
		{
			logger.info("TYP " + type);
			logger.info("displayLevel  " + displayLevel);
			if (null != type && null != displayLevel && null != type.getCategoryAttributeLevel() && type.isOn() && ("0".equals(type.getCategoryAttributeLevel()) || "".equals(type.getCategoryAttributeLevel()) || displayLevel.equals(type.getCategoryAttributeLevel())))
			{
				notHidden.add(type);
			}
		}
		return notHidden;
	}

	public int getCategoryLevelFromId(String id, String module)
	{
		String query = "select folderLevel from Categories_" + module + " where Category_id = ?";
		try
		{
			return Integer.parseInt((String) DA.select(query, new Object[] { id }, new StringMapper()).get(0));
		}
		catch (Exception e)
		{
			//will always go in here when inserting top level category
			return 0;
		}
	}

}
