package au.net.webdirector.common.utils.password;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Sushant Verma
 * 
 */
public class PasswordUtils
{

	public static final String FAKE_PASSWD = "FAKE_PASSWD";

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		System.out.println(encrypt("test1234"));
	}

	public static String encrypt(String passwd)
	{
		if (passwd == null)
		{
			return null;
		}

		// Can migrate MD5 password data to double SHA1 encryption 
		//  by running the following SQL code:
		/**
		 * update users
		 * set users.password = CAST(sha1(users.password) AS CHAR(50) CHARACTER
		 * SET utf8)
		 * where length(users.password) = 32
		 */
		return encryptSHA1(encryptMD5(passwd));
	}

	//Gives a password of length 32
	private static String encryptMD5(String passwd)
	{
		return encryptWithDigest(passwd, "MD5");
	}

	//Gives a password of length 40
	private static String encryptSHA1(String passwd)
	{
		return encryptWithDigest(passwd, "SHA1");
	}

	private static String encryptWithDigest(String passwd, String digest)
	{
		MessageDigest messageDigest = null;
		try
		{
			messageDigest = MessageDigest.getInstance(digest);

			messageDigest.reset();

			messageDigest.update(passwd.getBytes("UTF-8"));
		}
		catch (NoSuchAlgorithmException e)
		{
			System.out.println("NoSuchAlgorithmException caught!");
			System.exit(-1);
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		byte[] byteArray = messageDigest.digest();

		StringBuffer encStrBuff = new StringBuffer();

		for (int i = 0; i < byteArray.length; i++)
		{
			if (Integer.toHexString(0xFF & byteArray[i]).length() == 1)
				encStrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
			else
				encStrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
		}

		return encStrBuff.toString();

	}
}
