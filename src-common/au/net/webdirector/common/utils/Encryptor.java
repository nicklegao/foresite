package au.net.webdirector.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.RandomStringUtils;


public class Encryptor
{

	private static final String PREFIX = "-----ENCRYPTED TEXT-----\n";
	private static final String KEY = "C-I-T-2017-12-22"; // 128 bit key
	private static final String INIT_VECTOR = "RandomInitVector"; // 16 bytes IV


	public static String encrypt(String value)
	{
		try
		{
			IvParameterSpec iv = new IvParameterSpec(INIT_VECTOR.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(KEY.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

			byte[] encrypted = cipher.doFinal(value.getBytes());
			String ret = new String(Base64.encodeBase64(encrypted), "UTF-8");
//			System.out.println("encrypted string: "
//					+ ret);
			ret = PREFIX + ret;
			return ret;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

		return null;
	}

	public static String decrypt(String encrypted)
	{
		try
		{
			if (encrypted == null)
			{
				return null;
			}
			if (!encrypted.startsWith(PREFIX))
			{
				return encrypted;
			}
			encrypted = encrypted.substring(PREFIX.length());
			IvParameterSpec iv = new IvParameterSpec(INIT_VECTOR.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(KEY.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
			byte[] b = Base64.decodeBase64(encrypted.getBytes("UTF-8"));
			byte[] original = cipher.doFinal(b);

			return new String(original);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

		return null;
	}


	public static void encrypt(File input, File output)
	{
		try
		{
			IvParameterSpec iv = new IvParameterSpec(INIT_VECTOR.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(KEY.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

			FileInputStream inputStream = new FileInputStream(input);
			byte[] inputBytes = new byte[(int) input.length()];
			inputStream.read(inputBytes);

			byte[] outputBytes = cipher.doFinal(inputBytes);

			FileOutputStream outputStream = new FileOutputStream(output);
			outputStream.write(outputBytes);

			inputStream.close();
			outputStream.close();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void decrypt(File input, File output)
	{
		try
		{
			IvParameterSpec iv = new IvParameterSpec(INIT_VECTOR.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(KEY.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

			FileInputStream inputStream = new FileInputStream(input);
			byte[] inputBytes = new byte[(int) input.length()];
			inputStream.read(inputBytes);

			byte[] outputBytes = cipher.doFinal(inputBytes);

			FileOutputStream outputStream = new FileOutputStream(output);
			outputStream.write(outputBytes);

			inputStream.close();
			outputStream.close();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception
	{
		String encrypted = aes_encrypt("23allprojectsinc@gmail.com", KEY);//aes_encrypt("!@#$%^&*()_+-=0987654321`~?\\/:;'\"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", KEY);

		System.out.println(encrypted);
		System.out.println(aes_decrypt(encrypted, KEY));
	}

	private static String getEncryptFileName(File decrypted)
	{
		return decrypted.getName() + ".encrypted";
	}

	public static File createEncryptedFile(File decrypted)
	{
		File encrypted = new File(decrypted.getParentFile(), Encryptor.getEncryptFileName(decrypted));
		Encryptor.encrypt(decrypted, encrypted);
		return encrypted;
	}

	private static String getDecryptFileName(File encrypted)
	{
		return encrypted.getName().substring(0, encrypted.getName().length() - ".encrypted".length());
	}

	public static File createTempDecryptFile(File encrypted)
	{
		File decrypted = new File(System.getProperty("java.io.tmpdir"), RandomStringUtils.randomAlphanumeric(8) + "_" + Encryptor.getDecryptFileName(encrypted));
		decrypt(encrypted, decrypted);
		return decrypted;
	}

	public static boolean isEncrypted(File file)
	{
		return file.getName().endsWith(".encrypted");
	}

	public static String getKey()
	{
		return KEY;
	}

	public static String aes_encrypt(String decrypted)
	{
		return aes_encrypt(decrypted, KEY);
	}

	public static String aes_encrypt(String decrypted, String strKey)
	{
		try
		{
			byte[] keyBytes = Arrays.copyOf(strKey.getBytes("ASCII"), 16);

			SecretKey key = new SecretKeySpec(keyBytes, "AES");
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, key);

			byte[] cleartext = decrypted.getBytes("UTF-8");
			byte[] ciphertextBytes = cipher.doFinal(cleartext);

			return new String(Hex.encodeHex(ciphertextBytes));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}

	}

	public static String aes_decrypt(String encrypted)
	{
		return aes_decrypt(encrypted, KEY);
	}

	public static String aes_decrypt(String encrypted, String strKey)
	{
		try
		{
			byte[] keyBytes = Arrays.copyOf(strKey.getBytes("ASCII"), 16);

			SecretKey key = new SecretKeySpec(keyBytes, "AES");
			Cipher decipher = Cipher.getInstance("AES");

			decipher.init(Cipher.DECRYPT_MODE, key);

			char[] cleartext = encrypted.toCharArray();

			byte[] decodeHex = Hex.decodeHex(cleartext);

			byte[] ciphertextBytes = decipher.doFinal(decodeHex);

			return new String(ciphertextBytes);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}