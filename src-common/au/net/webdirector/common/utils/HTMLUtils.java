package au.net.webdirector.common.utils;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import org.owasp.esapi.ESAPI;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;

public class HTMLUtils
{
	/**
	 * Renders a HTML select box with a blank entry
	 * 
	 * @see #select(Map entries, String name, String defaultValue, String
	 *      attributes, boolean blankEntry)
	 */
	public String select(Map entries, String name, String defaultValue,
			String attributes) throws Exception
	{
		return select(entries, name, defaultValue, attributes, true);
	}

	/**
	 * Renders a HTML select box.
	 * 
	 * @param entries
	 *            a map of codes and values to display in the select box
	 */
	public String select(Map entries, String name, String defaultValue,
			String attributes, String optionAttributes, boolean blankEntry) throws Exception
	{
		return selectFromMapEntries(entries.entrySet().iterator(), name, defaultValue, attributes, optionAttributes, blankEntry);
	}

	/**
	 * Renders a HTML select box.
	 * 
	 * @param entries
	 *            a map of codes and values to display in the select box
	 */
	public String select(Map entries, String name, String defaultValue,
			String attributes, boolean blankEntry) throws Exception
	{
		return selectFromMapEntries(entries.entrySet().iterator(), name, defaultValue, attributes, "", blankEntry);
	}

	/**
	 * Renders an HTML select box.
	 * 
	 * @param entries
	 *            an iterator over Map.Entry objects containing code and
	 *            value
	 */
	public String selectFromMapEntries(Iterator entries, String name, String defaultValue,
			String attributes) throws Exception
	{
		return selectFromMapEntries(entries, name, defaultValue, attributes, "", true);
	}


	/**
	 * Renders an HTML select box - allowing you to choose whether or not to
	 * add a blank option.
	 * 
	 * @param entries
	 *            an iterator over Map.Entry objects containing code and
	 *            value
	 */
	public String selectFromMapEntries(Iterator entries, String name, String defaultValue,
			String attributes, String optionAttributes, boolean blankEntry) throws Exception
	{
		boolean defaultPresent = false;
		String selected = "";
		StringBuffer sb = new StringBuffer(1024);
		sb.append("<SELECT name='").append(name).append("' ").append(attributes).append(">");
		if (blankEntry)
		{
			sb.append("<OPTION value=''>--Select--</OPTION>");
		}
		while (entries.hasNext())
		{
			Map.Entry entry = (Map.Entry) entries.next();

			if (entry.getKey().equals(defaultValue))
			{
				selected = "selected";
				defaultPresent = true;
			}
			else
			{
				selected = "";
			}
			sb.append("<OPTION ").append(optionAttributes).append(" value='").append(ESAPI.encoder().encodeForHTMLAttribute(entry.getKey().toString())).append("' ");
			sb.append(selected).append(">").append(ESAPI.encoder().encodeForHTML(entry.getValue().toString())).append("</OPTION>");
		}
		if (!defaultPresent && defaultValue != null && defaultValue.length() > 0)
		{
			sb.append("<OPTION ").append(optionAttributes).append(" value='").append(ESAPI.encoder().encodeForHTMLAttribute(defaultValue)).append("' selected>").append(ESAPI.encoder().encodeForHTML(defaultValue)).append("</OPTION>");
		}
		sb.append("</SELECT>");
		return sb.toString();
	}

	/**
	 * As above but using a multi select and without some of the default stuff
	 * 
	 * @param entries
	 * @param name
	 * @param attributes
	 * @param optionAttributes
	 * 
	 * @return
	 * @throws Exception
	 */
	public String multiSelectFromMapEntries(Iterator entries, String name,
			String attributes, String optionAttributes, String module, String assetId, String tableType) throws Exception
	{
		String type = tableType.toLowerCase().startsWith("categories") ? "category" : "element";
		List pairs = new ArrayList();
		while (entries.hasNext())
		{
			Map.Entry entry = (Map.Entry) entries.next();
			pairs.add(new Pair(entry));
		}
		Collections.sort(pairs);

		String selected = "";
		StringBuffer sb = new StringBuffer(1024);
		Map<String, String> selectedOptions = new HashMap<String, String>();
		sb.append("<SELECT name='").append(name).append("' ").append(attributes).append(">");

		if (name.startsWith("ATTRMULTI"))
		{
			Vector selectedItems = getSelectedItems(name, module, assetId, type);
			for (int i = 0; i < pairs.size(); i++)
			{
				Map.Entry entry = ((Pair) pairs.get(i)).getEntry();
				if (selectedItems.contains(entry.getKey()))
				{
					StringBuffer option = new StringBuffer();
					option.append("<OPTION ").append(optionAttributes).append(" value='").append(ESAPI.encoder().encodeForHTMLAttribute(entry.getKey().toString())).append("' selected>").append(ESAPI.encoder().encodeForHTML(entry.getValue().toString())).append("</OPTION>");
					selectedOptions.put((String) entry.getKey(), option.toString());
				}
				else
				{
					sb.append("<OPTION ").append(optionAttributes).append(" value='").append(ESAPI.encoder().encodeForHTMLAttribute(entry.getKey().toString())).append("'>").append(ESAPI.encoder().encodeForHTML(entry.getValue().toString())).append("</OPTION>");
				}
			}
			// put the selected options to the end, and keep the order from database
			for (int i = 0; i < selectedItems.size(); i++)
			{
				String key = (String) selectedItems.get(i);
				if (selectedOptions.containsKey(key))
				{
					sb.append(selectedOptions.get(key));
				}
				else
				{
					sb.append("<OPTION ").append(optionAttributes).append(" value='").append(ESAPI.encoder().encodeForHTMLAttribute(ESAPI.encoder().encodeForHTML(key))).append("' selected>").append(key).append("</OPTION>");
				}
			}
		}
		sb.append("</SELECT>");
		return sb.toString();
	}

	private Vector getSelectedItems(String name, String module, String assetId, String type)
	{

		DBaccess db = new DBaccess();
		String sql = "select SELECTED_VALUE from drop_down_multi_selections where element_id = ? " +
				" and attrmulti_name = ? and module_name = ? and module_type = ? ";

		//return db.select(sql);
		return db.selectQuerySingleCol(sql, new String[] { assetId, name, module, type });
	}

	/**
	 * New Method for rendering select box with preselected options base upon
	 * Key contains in Map
	 * Renders an HTML select box - allowing you to choose whether or not to
	 * add a blank option.
	 * 
	 * @param entries
	 *            an iterator over Map.Entry objects containing code and
	 *            value
	 */
	public String selectFromMapEntriesForMembers(Iterator entries, Map map, String name, String defaultValue,
			String attributes, String optionAttributes, boolean blankEntry) throws Exception
	{
		boolean defaultPresent = false;
		String selected = "";
		StringBuffer sb = new StringBuffer(1024);
		sb.append("<SELECT name='").append(name).append("' ").append(attributes).append(">");
		if (blankEntry)
		{
			sb.append("<OPTION value=''>--Select--</OPTION>");
		}
		while (entries.hasNext())
		{
			Map.Entry entry = (Map.Entry) entries.next();

			if (entry.getKey().equals(defaultValue))
			{
				selected = "selected";
				defaultPresent = true;
			}
			else
			{
				selected = "";
			}
			if (map.containsKey(entry.getKey()))
			{
				selected = "selected";
			}
			sb.append("<OPTION ").append(optionAttributes).append(" value='").append(entry.getKey()).append("' ");
			sb.append(selected).append(">").append(entry.getValue()).append("</OPTION>");
		}
		if (!defaultPresent && defaultValue != null && defaultValue.length() > 0)
		{
			sb.append("<OPTION ").append(optionAttributes).append(" value='").append(defaultValue).append("' selected>").append(defaultValue).append("</OPTION>");
		}
		sb.append("</SELECT>");
		return sb.toString();
	}

	public String displayCheckBoxes(Map mapOfValues, String name, String defaultValue)
	{
		Iterator entries = mapOfValues.entrySet().iterator();
		StringBuffer sb = new StringBuffer("<table>");
		List listOfDefaultValue = parseDefaultValue(defaultValue);
		System.out.println("List of default Value: " + listOfDefaultValue);
		while (entries.hasNext())
		{
			Map.Entry entry = (Map.Entry) entries.next();
			sb.append("<tr>");
			sb.append("<td>").append(entry.getValue()).append("</td>");
			String checked = "";
			if (listOfDefaultValue.contains(entry.getValue()))
			{
				checked = "checked";
			}
			sb.append("<td>").append("<input type=\"checkbox\" name=\"" + name + "\" value=\"" + entry.getValue() + "\" " + checked + "/>").append("</td>");
			sb.append("</tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	public List parseDefaultValue(String defaultValue)
	{
		List listOfDefaultValue = new ArrayList();
		StringTokenizer st = new StringTokenizer(defaultValue, "|");
		while (st.hasMoreTokens())
		{
			listOfDefaultValue.add(st.nextToken());
		}
		return listOfDefaultValue;
	}
}

class Pair implements Comparable
{
	private Map.Entry entry;

	public Pair(Map.Entry e)
	{
		this.entry = e;
	}

	public Map.Entry getEntry()
	{
		return this.entry;
	}

	public int compareTo(Object o)
	{
		if (!(o instanceof Pair))
		{
			throw new InvalidParameterException();
		}
		String value = (String) getEntry().getValue();
		String another = (String) ((Pair) o).getEntry().getValue();
		return value.compareTo(another);
	}

}
