/**
 * Created by IntelliJ IDEA.
 * User: Andrew Davidson
 * Date: Mar 13, 2004
 * Time: 4:06:23 AM
 * To change this template use Options | File Templates.
 */
package au.net.webdirector.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import au.net.webdirector.common.Defaults;

@Deprecated
public class FileUtils
{
	public FileUtils()
	{
	}

	public void copyFiles(File src, File dest)
			throws IOException
	{
		if (!src.exists())
			return;
		if (src.isDirectory())
		{
			dest.mkdirs();
			String list[] = src.list();
			for (int i = 0; i < list.length; i++)
			{
				File src1 = new File(src, list[i]);
				File dest1 = new File(dest, list[i]);
				copyFiles(src1, dest1);
			}

		}
		else
		{
			FileInputStream fis = new FileInputStream(src);
			FileOutputStream fos = new FileOutputStream(dest);
			FileChannel fcin = fis.getChannel();
			FileChannel fcout = fos.getChannel();
			fcin.transferTo(0L, fcin.size(), fcout);
			fcin.close();
			fcout.close();
			fis.close();
			fos.close();
		}
	}

	public void clearFileContents(String f)
	{
		// delete physical file
		Defaults d = Defaults.getInstance();

		File file = new File(d.getStoreDir() + "/" + f);
		boolean fileDeleted = file.delete();
		boolean fileRecreated = false;
		try
		{
			fileRecreated = file.createNewFile();
		}
		catch (IOException io)
		{
			System.out.println("FileUtils:clearFileContents(" + f + ") failed to recreate file");
			System.out.println("FileUtils:clearFileContents(" + f + ") full file " + file.toString());
			System.out.println("FileUtils:clearFileContents(" + f + ") fileDeleted = " + fileDeleted);
			System.out.println("FileUtils:clearFileContents(" + f + ") fileRecreated = " + fileRecreated);
			System.out.println("FileUtils:clearFileContents(" + f + ") " + io.getMessage());
		}
	}

}