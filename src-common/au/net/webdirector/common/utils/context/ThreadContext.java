/**
 * @author Sushant Verma
 * @date 2 Oct 2015
 */
package au.net.webdirector.common.utils.context;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

public class ThreadContext
{
	private static final String REQUEST_URI = "REQUEST_URI";
	private static final String REQUEST_URL = "REQUEST_URL";
	private static final String REQUEST_PARAMS = "REQUEST_PARAMS";
	private static final String REQUEST_HEADER = "REQUEST_HEADER";
	private static final String REMOTE_CLIENT = "REMOTE_CLIENT";
	private static final String SYSTEM_PROPERTIES = "SYSTEM_PROPERTIES";
	private static final String REQUEST_ATTRIBUTES = "REQUEST_ATTRIBUTES";
	private static final String SESSION_ATTRIBUTES = "SESSION_ATTRIBUTES";
	private static final String SERVER_CONTEXT = "SERVER_CONTEXT";
	private static final String SERVLET_REQUEST = "SERVLET_REQUEST";

	public static final int MAXIMUM_CONTEXT_COUNT = 50;

	private static Logger logger = Logger.getLogger(ThreadContext.class);

	private static Map<Long, ThreadContext> contextMap = Collections.synchronizedMap(new LinkedHashMap<Long, ThreadContext>());

	private ThreadMetadata threadMetadata = null;
	private Map<String, Object> threadAttributes = null;

	private ThreadContext()
	{
		threadMetadata = new ThreadMetadata();
		threadAttributes = new HashMap<String, Object>();
	}

	public static long currentThreadID()
	{
		return Thread.currentThread().getId();
	}

	private static ThreadContext getOrCreateContext()
	{
		long threadID = currentThreadID();
		ThreadContext context = contextMap.get(threadID);
		if (context == null)
		{
			context = new ThreadContext();
			contextMap.put(threadID, context);

			if (contextMap.size() > MAXIMUM_CONTEXT_COUNT)
			{
				long oldThread = contextMap.keySet().iterator().next();
				logger.fatal("Forcefully discarding thread context for thread: " + oldThread + "... Maximum allowed: " + MAXIMUM_CONTEXT_COUNT);
				reset(oldThread);
			}
		}
		return context;
	}

	public static void reset()
	{
		reset(currentThreadID());
	}

	public static void reset(long threadID)
	{
		contextMap.put(threadID, new ThreadContext());
	}

	public static void clear()
	{
		clear(currentThreadID());
	}

	public static void clear(long threadID)
	{
		contextMap.remove(threadID);
	}

	public static ThreadMetadata metadata()
	{
		return getOrCreateContext().threadMetadata;
	}

	public static Object setMetadata(String key, Object value)
	{
		return metadata().put(key, value);
	}

	public static Object getMetadata(String key)
	{
		return metadata().get(key);
	}

	public static Object setAttribute(String key, Object value)
	{
		return getOrCreateContext().threadAttributes.put(key, value);
	}

	public static Object getAttribute(String key)
	{
		return getOrCreateContext().threadAttributes.get(key);
	}

	public static void setRequest(HttpServletRequest request)
	{
		ServerContext serverContext = new RequestServerContext(request);
		setMetadata(SERVER_CONTEXT, serverContext);

		setAttribute(SERVLET_REQUEST, request);
	}

	public static void parseRequest()
	{
		try
		{
			HttpServletRequest request = getRequest();

			setMetadata(REQUEST_URI, request.getRequestURI());
			setMetadata(REQUEST_URL, request.getRequestURL().toString());

			{
				Map<String, String> mapData = new LinkedHashMap<String, String>();
				mapData.put("remoteAddr", request.getRemoteAddr());
				mapData.put("remoteHost", request.getRemoteHost());
				mapData.put("remotePort", Integer.toString(request.getRemotePort()));
				mapData.put("remoteUser", request.getRemoteUser());

				setMetadata(REMOTE_CLIENT, mapData);
			}

			{
				Map<String, String> mapData = new LinkedHashMap<String, String>();
				Enumeration<String> names = request.getHeaderNames();
				while (names.hasMoreElements())
				{
					String key = names.nextElement();
					Object value = request.getHeader(key);
					mapData.put(key, value.toString());
				}
				setMetadata(REQUEST_HEADER, mapData);
			}

			{
				Map<String, String> mapData = new LinkedHashMap<String, String>();
				Enumeration<String> names = request.getAttributeNames();
				while (names.hasMoreElements())
				{
					String key = names.nextElement();
					Object value = request.getAttribute(key);
					mapData.put(key, value.toString());
				}
				setMetadata(REQUEST_ATTRIBUTES, mapData);
			}

			{
				Map<String, String> mapData = new LinkedHashMap<String, String>();
				Enumeration<Object> keys = System.getProperties().keys();
				while (keys.hasMoreElements())
				{
					String key = keys.nextElement().toString();
					Object value = System.getProperty(key);
					mapData.put(key, value.toString());
				}
				setMetadata(SYSTEM_PROPERTIES, mapData);
			}

			setMetadata(REQUEST_PARAMS, request.getParameterMap());

			if (request.getSession() != null)
			{
				Map<String, String> mapData = new TreeMap<String, String>();
				Enumeration<String> names = request.getSession().getAttributeNames();
				while (names.hasMoreElements())
				{
					String key = names.nextElement();
					Object value = request.getSession().getAttribute(key);
					mapData.put(key, value.toString());
				}
				setMetadata(SESSION_ATTRIBUTES, mapData);
			}
		}
		catch (ContextItemNotAvaliableException e)
		{
			logger.error("Error:", e);
		}
	}

	public static HttpServletRequest getRequest() throws ContextItemNotAvaliableException
	{
		HttpServletRequest request = (HttpServletRequest) getAttribute(SERVLET_REQUEST);
		if (request != null)
		{
			return request;
		}

		throw new ContextItemNotAvaliableException();
	}

	public static ServerContext getServerContext()
	{
		ServerContext serverContext = (ServerContext) getMetadata(SERVER_CONTEXT);
		return serverContext;
	}

	public static void main(String[] args)
	{
		Map<String, String> something = new HashMap<String, String>();
		something.put("W", "www");
		something.put("X", "xxx");
		something.put("Y", "123");

		ThreadContext.setAttribute("A", "complexObject");

		ThreadContext.setMetadata("A", "asdf");
//		ThreadContext.metadata().clear();
		ThreadContext.setMetadata("number", 2);
		ThreadContext.setMetadata("some object", something);
		System.out.println(ThreadContext.metadata().toJson());
	}
}
