/**
 * @author Sushant Verma
 * @date 2 Oct 2015
 */
package au.net.webdirector.common.utils.context;

public abstract class ServerContext
{
	private static final int HTTP_PORT = 80;
	private static final int HTTPS_PORT = 443;

	protected String scheme;
	protected String host;
	protected int port;
	protected String context;
	protected String serverBase;
	protected boolean isStandardPort;

	protected void generateBase()
	{
		isStandardPort = true;
		if (scheme.equalsIgnoreCase("http"))
		{
			isStandardPort = (port == HTTP_PORT);
		}
		else if (scheme.equalsIgnoreCase("https"))
		{
			isStandardPort = (port == HTTPS_PORT);
		}
		String portForDisplay = "";
		if (!isStandardPort && port > 0)
		{
			portForDisplay = ":" + port;
		}
		serverBase = scheme + "://" + host + portForDisplay + context;
	}
}
