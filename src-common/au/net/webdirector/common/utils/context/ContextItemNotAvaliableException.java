/**
 * @author Sushant Verma
 * @date 2 Oct 2015
 */
package au.net.webdirector.common.utils.context;

import org.apache.log4j.Logger;

public class ContextItemNotAvaliableException extends Exception
{
}
