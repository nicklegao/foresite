/**
 * @author Sushant Verma
 * @date 2 Oct 2015
 */
package au.net.webdirector.common.utils.context;

import javax.servlet.http.HttpServletRequest;

public class RequestServerContext extends ServerContext
{
	public RequestServerContext(HttpServletRequest request)
	{
		scheme = request.getScheme();
		host = request.getServerName();
		port = request.getServerPort();
		context = request.getContextPath();

		generateBase();
	}
}
