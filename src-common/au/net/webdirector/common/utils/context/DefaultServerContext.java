/**
 * @author Sushant Verma
 * @date 2 Oct 2015
 */
package au.net.webdirector.common.utils.context;

import org.apache.log4j.Logger;

import au.net.webdirector.common.Defaults;

public class DefaultServerContext extends ServerContext
{
	private static Logger logger = Logger.getLogger(DefaultServerContext.class);

	public DefaultServerContext()
	{
		String websiteUrl = Defaults.getInstance().getWebSiteURL().toLowerCase();
		configureForURL(websiteUrl);
	}

	public DefaultServerContext(String baseURL)
	{
		configureForURL(baseURL);
	}

	private void configureForURL(String websiteUrl)
	{
		websiteUrl = websiteUrl.toLowerCase().trim();
		while (websiteUrl.endsWith("/"))
		{
			websiteUrl = websiteUrl.substring(0, websiteUrl.length() - 1);

		}

		scheme = websiteUrl.substring(0, websiteUrl.indexOf("://"));

		String rhs = websiteUrl.substring(websiteUrl.indexOf("://") + 3).trim();
		int slashIndex = rhs.indexOf("/");

		String possibleHost;
		if (slashIndex > 0)
		{
			context = rhs.substring(slashIndex);
			possibleHost = rhs.substring(0, slashIndex);
		}
		else
		{
			context = "";
			possibleHost = rhs;
		}

		int portIndex = possibleHost.lastIndexOf(":");
		if (portIndex > 0)
		{
			port = Integer.parseInt(possibleHost.substring(portIndex + 1));
			host = possibleHost.substring(0, portIndex);
		}
		else
		{
			port = 0;
			host = possibleHost;
		}

		generateBase();
	}
}
