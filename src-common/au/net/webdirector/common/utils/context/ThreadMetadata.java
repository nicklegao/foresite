/**
 * @author Sushant Verma
 * @date 1 Oct 2015
 */
package au.net.webdirector.common.utils.context;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ThreadMetadata extends HashMap<String, Object>
{
	private static final long serialVersionUID = 1554917099278738141L;
	private static Logger logger = Logger.getLogger(ThreadMetadata.class);
	private static Gson defaultGson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();

	public ThreadMetadata()
	{
	}

	public Map<String, Object> mappedMetadata()
	{
		return new TreeMap<String, Object>(this);
	}

	public String toJson()
	{
		try
		{
			return defaultGson.toJson(mappedMetadata());
		}
		catch (Exception e)
		{
			logger.fatal("Unable to generate JSON.", e);
			return "UNABLE to generate JSON. " + e.toString();
		}
	}
}
