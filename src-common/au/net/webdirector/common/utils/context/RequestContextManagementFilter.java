/**
 * @author Sushant Verma
 * @date 2 Oct 2015
 */
package au.net.webdirector.common.utils.context;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class RequestContextManagementFilter implements Filter
{
	@Override
	public void init(FilterConfig arg0) throws ServletException
	{
	}

	@Override
	public void destroy()
	{
	}

	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException
	{
		ThreadContext.reset();//just incase

		ThreadContext.setRequest((HttpServletRequest) req);

		chain.doFilter(req, resp);

		ThreadContext.clear();//free up the memory
	}
}
