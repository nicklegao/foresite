package au.net.webdirector.common.utils.taskQueue;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class QueuedThreadService extends AbstractManagedQueuedTaskService<Runnable>
{

	private static Map<String, QueuedThreadService> instances = new ConcurrentHashMap<String, QueuedThreadService>();

	public static synchronized QueuedThreadService getInstance(String name)
	{
		if (instances.containsKey(name))
		{
			return instances.get(name);
		}
		QueuedThreadService instance = new QueuedThreadService();
		instances.put(name, instance);
		return instance;
	}

	@Override
	protected int pollingInterval()
	{
		// TODO Auto-generated method stub
		return 500;
	}

	@Override
	protected void onProcessItem(Runnable item)
	{
		item.run();
	}

}
