package au.net.webdirector.common.utils.taskQueue;

import org.apache.log4j.Logger;

public class QueuedConsoleLogger extends AbstractManagedQueuedTaskService<String>
{

	private Logger logger;

	private static QueuedConsoleLogger instance;

	private QueuedConsoleLogger()
	{
		logger = Logger.getLogger(this.getClass());
	}

	public static synchronized QueuedConsoleLogger getInstance()
	{
		if (instance == null)
			instance = new QueuedConsoleLogger();
		return instance;
	}

	@Override
	protected int pollingInterval()
	{
		return 500;
	}

	@Override
	protected void onProcessItem(String item)
	{
		logger.info(item);
	}

	public static void main(String[] args)
	{
		QueuedConsoleLogger.getInstance().addToQueue("A", "B", "C", "D");
	}
}
