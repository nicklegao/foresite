package au.net.webdirector.common.utils.taskQueue;

import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

/**
 * Implementation class AbstractManagedService
 * 
 */
public abstract class AbstractManagedQueuedTaskService<T> implements Runnable
{
	@SuppressWarnings("unused")
	private static final long serialVersionUID = -3840419343744600954L;
	protected static Logger logger = Logger.getLogger(AbstractManagedQueuedTaskService.class);

	private Object lock = new Object();
	private Thread activeThread = null;
	private LinkedBlockingQueue<T> processQueue = new LinkedBlockingQueue<T>();

	public void addToQueue(@SuppressWarnings("unchecked") T... items)
	{
		try
		{
			for (T item : items)
			{
				processQueue.put(item);
			}
			startThreadIfNeeded();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	private void startThreadIfNeeded()
	{
		synchronized (lock)
		{
			if (activeThread == null && processQueue.size() > 0)
			{
				logger.info("New thread created");
				activeThread = new Thread(this);
				activeThread.start();
			}
		}
	}

	private void stopThread()
	{
		synchronized (lock)
		{
			activeThread = null;
		}
	}

	/**
	 * Runnable
	 */
	@Override
	public void run()
	{
		logger.info("Started");
		while (processQueue.size() > 0)
		{
			try
			{
				T item = processQueue.poll();
				logger.info(item);
				if (item != null)
				{
					long start = System.currentTimeMillis();
					logger.info("START Processing item.");
					onProcessItem(item);
					long finish = System.currentTimeMillis();
					long execution = finish - start;
					logger.info("FINISH Processing item. Took: " + execution + " milliseconds");
				}

				if (processQueue.size() == 0)
				{
					Thread.sleep(pollingInterval());
				}
				logger.info("Queue contains " + processQueue.size() + " items");

			}
			catch (Exception e)
			{
				logger.fatal("OH shit!!", e);
			}
		}
		//nothing in the queue
		//---------------------- another thread adds item
		stopThread();
		//---------------------- another thread adds item
		logger.info("Stopped");

		startThreadIfNeeded();
	}

	/**
	 * @return the processQueue
	 */
	public LinkedBlockingQueue<T> getProcessQueue()
	{
		return processQueue;
	}

	protected abstract int pollingInterval();

	protected abstract void onProcessItem(T item);
}
