/**
 * @author Sushant Verma
 * @date 14 Feb 2017
 */
package au.net.webdirector.common.utils;

import java.io.File;

import org.apache.log4j.Logger;

import au.net.webdirector.common.Defaults;

public class ImageMagickUtils
{
	private static Logger logger = Logger.getLogger(ImageMagickUtils.class);

	private static ImageMagickUtils instance;
	private static Object _sharedLock_ = new Object();

	public static ImageMagickUtils sharedInstance()
	{
		if (instance == null)
		{
			synchronized (_sharedLock_)
			{
				if (instance == null)
				{
					instance = new ImageMagickUtils();
				}
			}
		}
		return instance;
	}

	private ImageMagickUtils()
	{
		logger.info("Creating singleton of class: " + getClass().getName());
	}

	public boolean resizeImage(File source, File dest, String width, String height)
	{
		if (dest.getParentFile().getParentFile() != null
				&& dest.getParentFile().exists() == false)
		{
			dest.mkdirs();
		}

		File imageMagickHome = new File(Defaults.getInstance().getImageMagickDirectory());
		int exitVal = 0;
		try
		{
			String[] cmd = new String[5];

			cmd[0] = new File(imageMagickHome, "convert").getAbsolutePath();
			cmd[1] = source.getAbsolutePath();
			cmd[2] = "-resize";
			cmd[3] = width + "x" + height;
			cmd[4] = dest.getAbsolutePath();

			Runtime rt = Runtime.getRuntime();
			String a = "";
			for (int i = 0; i < cmd.length; i++)
				a += " " + cmd[i];

			logger.info("Executing " + a);
			Process proc = rt.exec(cmd);
			// any error message?
			StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "ERROR");

			// any output?
			StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "OUTPUT");

			// kick them off
			errorGobbler.start();
			outputGobbler.start();

			// any error???
			exitVal = proc.waitFor();
			logger.info("ExitValue: " + exitVal);
		}
		catch (Throwable t)
		{
			t.printStackTrace();
		}

		if (exitVal == 0)
			return true;
		else
			return false;
	}

}
