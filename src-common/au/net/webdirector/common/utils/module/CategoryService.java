package au.net.webdirector.common.utils.module;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.RecoverableDataAccessException;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import au.net.webdirector.common.datatypes.service.DataTypesService;

public class CategoryService
{

	static Logger logger = Logger.getLogger(CategoryService.class);

	public boolean deleteCategory(String module, int id)
	{
		TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true));
		try
		{
			TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
			reorderCategories(Integer.toString(id), module, txManager.getDataAccess());
			return ma.deleteCategoryById(module, String.valueOf(id)) > 0;
		}
		catch (Exception e)
		{
			txManager.rollback();
			throw new RecoverableDataAccessException("Cannot delete category.", e);
		}
		finally
		{
			txManager.commit();
		}
	}

	private boolean reorderCategories(String categoryID, String module, TransactionDataAccess da) throws SQLException
	{
		// find the order element was set at
		String query = "select display_order, Category_ParentID from Categories_" + DatabaseValidation.encodeParam(module) + " where Category_id = ?";
		String[] results = da.selectStringArray(query, new String[] { categoryID });
		int rows = 0;
		if (results[0] != null && !results[0].equals("0") && !results[0].equals(""))
		{
			query = "update Categories_" + module + " set display_order = display_order-1 where display_order > ? and Category_ParentID = ?";
			System.out.println(query);
			rows = da.update(query, new Object[] { results[0], results[1] });
		}
		return rows > 0;
	}

	public boolean moveCategory(int childCategoryId, int parentCategoryId, String module)
	{
		TransactionDataAccess da = TransactionDataAccess.getInstance(false);
		try
		{
			reorderCategories(String.valueOf(childCategoryId), module, da);
			int displayOrder = getChildrenCount(module, parentCategoryId, da);
			String query = "update Categories_" + module + " set Category_parentId = ?, display_order = ? where Category_id = ?";
			int count = da.update(query, new Object[] { parentCategoryId, displayOrder, childCategoryId });
			return count == 1;
		}
		catch (Exception e)
		{
			da.rollback();
			throw new RecoverableDataAccessException("Cannot move category.", e);
		}
		finally
		{
			da.commit();
		}
	}

	public int copyCategory(Integer id, int parentId, String module)
	{
		return copyCategory(id, parentId, module, true);
	}

	public int copyCategory(Integer id, int parentId, String module, boolean makeUnlive)
	{
		TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true));
		try
		{
			TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
			CategoryData categoryData = ma.getCategoryById(module, String.valueOf(id));
			List categoryColumns = new DataTypesService().loadGenericDataTypes(module, true, "");
			List elementColumns = new DataTypesService().loadGenericDataTypes(module, false, "");
			CategoryData newCate = copyCategory(categoryData, String.valueOf(parentId), module, ma, categoryColumns, elementColumns, makeUnlive);
			return Integer.parseInt(newCate.getId());
		}
		catch (Exception e)
		{
			txManager.rollback();
			throw new RecoverableDataAccessException("Cannot copy category.", e);
		}
		finally
		{
			txManager.commit();
		}

	}

	public int copyCategory(Integer id, int parentId, String module, TransactionManager txManager) throws SQLException, IOException
	{
		return copyCategory(id, parentId, module, txManager, true);
	}

	public int copyCategory(Integer id, int parentId, String module, TransactionManager txManager, boolean makeUnlive) throws SQLException, IOException
	{
		TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
		CategoryData categoryData = ma.getCategoryById(module, String.valueOf(id));
		List categoryColumns = new DataTypesService().loadGenericDataTypes(module, false, "");
		List elementColumns = new DataTypesService().loadGenericDataTypes(module, true, "");
		CategoryData newCate = copyCategory(categoryData, String.valueOf(parentId), module, ma, categoryColumns, elementColumns, makeUnlive);
		return Integer.parseInt(newCate.getId());
	}

	public CategoryData copyCategory(CategoryData category, String parentId, String module, TransactionModuleAccess ma, List categoryColumns, List elementColumns) throws SQLException, IOException
	{
		return copyCategory(category, parentId, module, ma, categoryColumns, elementColumns, true);
	}

	public CategoryData copyCategory(CategoryData category, String parentId, String module, TransactionModuleAccess ma, List categoryColumns, List elementColumns, boolean makeUnlive) throws SQLException, IOException
	{
		CategoryData clone = (CategoryData) category.clone();
		clone.setParentId(String.valueOf(parentId));
		if (makeUnlive)
		{
			clone.setLive(false);
		}
		for (String key : clone.keySet())
		{
			if (!key.startsWith("attrinteger_") && !key.startsWith("attr_"))
			{
				continue;
			}
			if (!AssetService.isUnique(categoryColumns, module, key, ""))
			{
				continue;
			}
			if (key.startsWith("attrinteger_") && StringUtils.isNotBlank(clone.getString(key)))
			{
				clone.put(key, AssetService.getNextID(key, "Categories_" + module));
				continue;
			}
			if (key.startsWith("attr_"))
			{
				String val = clone.getString(key);
				String newVal = "Copy of " + val + " (" + AssetService.getNextID("Category_id", "Categories_" + module) + ")";
				if (val.indexOf("Copy of ") == 0 && val.lastIndexOf("(") != -1 && val.lastIndexOf(")") == val.length() - 1)
				{
					// 'Copy of filename (n)'
					newVal = val.substring(0, val.lastIndexOf("(")) + AssetService.getNextID("Category_id", "Categories_" + module) + ")";
				}
				clone.put(key, newVal);
				continue;
			}
		}
		CategoryData newCate = ma.insertCategory(clone, module);

		// insert sub categories
		List<CategoryData> subCategories = ma.getCategoriesByParentId(module, category.getId());
		for (CategoryData sub : subCategories)
		{
			copyCategory(sub, newCate.getId(), module, ma, categoryColumns, elementColumns, makeUnlive);
		}
		// insert elements
		AssetService as = new AssetService();
		List<ElementData> elements = ma.getElementsByParentId(module, category.getId());

		for (ElementData element : elements)
		{
			as.copyAsset(element, newCate.getId(), module, ma, elementColumns, makeUnlive);
		}
		return newCate;
	}


	public boolean isLineal(String module, int categoryID, int another)
	{
		DBaccess db = new DBaccess();
		if (categoryID == another)
		{
			return true;
		}
		if (categoryID == 0 || another == 0)
		{
			return true;
		}
		String sql = "select Category_id, folderLevel from categories_" + DatabaseValidation.encodeParam(module) + " where category_id in (?, ?) order by folderLevel";
		//List<String[]> list = db.select(sql, new String[]{"a", "b"});
		List<String[]> list = db.selectQuery(sql, new Object[] { categoryID, another });
		if (list.size() < 2)
		{
			return false;
		}
		String oldGenID = list.get(0)[0];
		int oldGenLevel = Integer.parseInt(list.get(0)[1]);
		String newGenID = list.get(1)[0];
		int newGenLevel = Integer.parseInt(list.get(1)[1]);
		if (oldGenLevel == newGenLevel)
		{
			return false;
		}
		String tempID = newGenID;
		for (int i = newGenLevel; i > oldGenLevel; i--)
		{
			sql = "select category_parentID from categories_" + DatabaseValidation.encodeParam(module) + " where category_id = ?";
			//List<String> rs = db.select(sql);
			List<String> rs = db.selectQuerySingleCol(sql, new String[] { tempID });

			if (rs.size() == 0)
			{
				return false;
			}
			tempID = rs.get(0);
		}
		return oldGenID.equals(tempID);
	}

	private int getChildrenCount(String tableName, int parent, TransactionDataAccess da)
	{
		String s = "select max(display_order) +1 from categories_" + DatabaseValidation.encodeParam(tableName) + " where category_parentid = ?";
		int count = 1;
		try
		{
			String order = da.selectString(s, new String[] { String.valueOf(parent) });
			count = (order == null ? 1 : Integer.parseInt(order));
		}
		catch (Exception nfe)
		{

		}
		return count;
	}

}
