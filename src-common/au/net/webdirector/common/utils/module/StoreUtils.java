package au.net.webdirector.common.utils.module;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.utils.Encryptor;

/**
 * Created by IntelliJ IDEA. User: Administrator Date: 24/07/2008 Time: 19:19:57
 * To change this template use File | Settings | File Templates.
 */
public class StoreUtils
{
	private static Logger logger = Logger.getLogger(StoreUtils.class);

	private au.net.webdirector.common.Defaults d = au.net.webdirector.common.Defaults.getInstance();

	public static String replaceIdInPartialStorePath(String partialPath,
			String newId, boolean includeFileName)
	{
		String returnPath = "";
		try
		{
			// expecting something like
			// /campaigns/15/ATTRLONG_Message/ATTRLONG_Message.txt
			String[] newPartialpathParts = partialPath.split("/");
			if (includeFileName)
				returnPath = "/" + newPartialpathParts[1] + "/" + newId + "/" + newPartialpathParts[3] + "/" + newPartialpathParts[4];
			else
				returnPath = "/" + newPartialpathParts[1] + "/" + newId + "/" + newPartialpathParts[3];
		}
		catch (Exception e)
		{
			System.out.println("Partial path is incorrectly formed: returning empty string");
			return "";
		}

		return returnPath;
	}

	public String writeFieldToStores(String module, String id, String field,
			String content)
	{
		return writeFieldToStores(module, Integer.parseInt(id), field, content, false);
	}

	public String writeFieldToStores(String module, String id, String field,
			String content, boolean category)
	{
		return writeFieldToStores(module, Integer.parseInt(id), field, content, category);
	}

	public String writeFieldToStores(String module, int id, String field,
			String content)
	{
		return writeFieldToStores(module, id, field, content, false);
	}

	public String writeFieldToStores(String module, int id, String field,
			String content, boolean category)
	{
		content = Encryptor.encrypt(content);
		String itemDir = String.format("/%s/%s/%s", module, id, field);
		if (category)
		{
			itemDir = String.format("/%s/Categories/%s/%s", module, id, field);
		}
		String storePath = String.format("%s/%s.txt", itemDir, field);

		String storeFile = d.getStoreDir() + storePath;
		File f = new File(storeFile);

		BufferedWriter bw;
		try
		{
			boolean success = new File(d.getStoreDir() + itemDir).mkdirs();
			bw = new BufferedWriter(new FileWriter(f));
			bw.write(content);
			bw.close();
			return storePath;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}


	public String moveFileToStores(String module, int id, String field,
			File srcFile)
	{
		String itemDir = String.format("/%s/%s/%s", module, id, field);
		String relativeStorePath = String.format("%s/%s", itemDir, srcFile.getName());

		File storeFile = new File(d.getStoreDir(), relativeStorePath);

		if (!storeFile.getParentFile().mkdirs())
		{
			logger.fatal("Unable to create directory! " + storeFile.getAbsolutePath());
		}

		try
		{
			FileUtils.moveFile(srcFile, storeFile);
			logger.info("Moved file to: " + storeFile.getAbsolutePath());
		}
		catch (IOException e)
		{
			logger.fatal("Unable to move file from: " + srcFile.getAbsolutePath() + " to: " + storeFile.getAbsolutePath(), e);
		}

		return relativeStorePath;
	}

	public String moveFileToCategoriesStores(String module, int id, String field,
			File srcFile)
	{
		String itemDir = String.format("/%s/Categories/%s/%s", module, id, field);
		String storePath = String.format("%s/%s", itemDir, srcFile.getName());

		String storeFile = d.getStoreDir() + storePath;
		File f = new File(storeFile);
		boolean createdDirs = new File(d.getStoreDir() + itemDir).mkdirs();

		File dest = new File(d.getStoreDir(), storePath);
		try
		{
			FileUtils.moveFile(srcFile, dest);
		}
		catch (IOException e)
		{
			logger.fatal("Unable to move file from: " + srcFile.getAbsolutePath() + " to: " + dest.getAbsolutePath(), e);
		}

		return storePath;
	}

	public boolean deleteElementFromStores(String module, int id)
	{
		String itemDir = String.format("/%s/%s", module, id);

		String storeFile = d.getStoreDir() + itemDir;
		File f = new File(storeFile);

		return d.nuke(f);
	}

	public String readFieldFromStores(String storePath)
	{
		String fullPath = d.getStoreDir() + storePath;
		StringBuffer sb = new StringBuffer();
		File f = new File(fullPath);
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new FileReader(f));
			String line = null;

			while ((line = br.readLine()) != null)
			{
				if (sb.length() != 0)
				{
					sb.append("\n");
				}
				sb.append(line);
			}
			String s = sb.toString();
			return Encryptor.decrypt(s);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (br != null)
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	/**
	 * @param s
	 *            - full path to src file
	 * @param destArg
	 *            - full path to dest directory - not including filename!
	 * @param generateThumbs
	 *            - true if an image and you want thumb (false for txt and no
	 *            thumb)
	 * @return true if the copy and optional thumb generated ok
	 */
	public boolean copyToStores(String s, String destArg, boolean generateThumbs)
	{
		System.out.println(">>>>>>>>>>>>>BMIMP it is destiArg::::::>>>>>" + destArg);
		FileUtils fUtils = new FileUtils();
		// copy s to d
		File srcFile = new File(s);
		String fileName = srcFile.getName();

		System.out.println("BMIMP dest " + destArg);
		System.out.println("src " + s);

		File dest = new File(destArg);
		// do not check ret bool as it returns false if the dir already exists
		dest.mkdirs();

		File renameLocation = new File(dest, fileName);
		System.out.println("BMIMP After rename Location::::>>>>>>>SRC" + srcFile);
		try
		{

			FileInputStream in = new FileInputStream(srcFile);
			FileOutputStream fOut = new FileOutputStream(renameLocation);
			int r = 0;
			byte[] b1 = new byte[1024];
			while ((r = in.read(b1)) != -1)
			{
				fOut.write(b1, 0, r);
			}
			fOut.close();
			in.close();

		}
		catch (Exception E)
		{
			System.out.println("Inside exception::::" + E.getMessage());
			E.getMessage();
			E.printStackTrace();
			return false;
		}

		String destination = destArg + File.separator + "ATTRLONG_PageText.txt";
		System.out.println("BMIMP Destination:::" + destination);
		File destFile = new File(destination);
		if (!generateThumbs)
		{
			try
			{
				FileUtils.moveFile(renameLocation, destFile);
			}
			catch (IOException e)
			{
				logger.fatal("Unable to move file from: " + renameLocation.getAbsolutePath() + " to: " + destFile.getAbsolutePath(), e);
			}
		}

		if (generateThumbs)
		{
			d.generateThumb(destFile);
		}

		return true;
	}
}
