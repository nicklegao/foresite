package au.net.webdirector.common.utils.module;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import webdirector.db.client.ClientDataAccess;
import webdirector.db.client.UserDetails;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.StringArrayMapper;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import au.net.webdirector.common.datatypes.domain.DropDownData;
import au.net.webdirector.common.utils.context.ThreadContext;

public class DropDownBuilder
{
	private static Logger logger = Logger.getLogger(DropDownBuilder.class);

	DBaccess du = new DBaccess();
	ClientDataAccess cda = new ClientDataAccess();

	/**
	 * Used internally by the
	 * /webdirector/secure/module/configure/dropdown/values page -
	 * essentially the drop down
	 * builder
	 * 
	 * @param moduleName
	 * @param colName
	 * @return
	 */
	public Map getDropDownValues(String moduleName, String colName, String type)
	{
		String col[] = { "ID", "DROPDOWN_NAME", "DROPDOWN_VALUE" };
		String SQL_QUERY = "select ID, DROPDOWN_NAME, DROPDOWN_VALUE from DROP_DOWN where MODULE_NAME=? and ATTRDROP_NAME=? and MODULE_TYPE = ? order by DROPDOWN_VALUE";
		//Vector vt = du.select(SQL_QUERY, col);
		Vector vt = du.selectQuery(SQL_QUERY, new String[] { moduleName, colName, type });
		Map map = new LinkedHashMap();
		for (int k = 0; k < vt.size(); k++)
		{
			String[] s = (String[]) vt.get(k);
			map.put(s[0], s[1] + "|" + s[2]);
		}
		return map;
	}

	/**
	 * Used by the formBuilder when rendering drop downs
	 * 
	 * @param moduleName
	 * @param colName
	 * @return
	 */
	public Map<String, String> getDropDownValuesWithoutId(String moduleName, String colName, String tableType)
	{
		return getDropDownValuesWithoutIdButWithPermission(moduleName, colName, null, tableType);
	}

	public Map<String, String> getDropDownValuesWithoutIdButWithPermission(String moduleName, String colName, String userName, String tableType)
	{
		Map<String, String> map = new LinkedHashMap<String, String>();
		try
		{
			List<DropDownData> data = getDropDownDatas(moduleName, colName, userName, tableType);
			for (DropDownData e : data)
			{
				map.put(e.getValue(), e.getText());
			}
		}
		catch (Exception e)
		{
			logger.error("Error", e);
		}
		return map;
	}

	public List<DropDownData> getDropDownDatas(String moduleName, String colName, String userName, String tableType)
	{
		String type = tableType.toLowerCase().startsWith("categories") ? "category" : "element";
		String col[] = { "DROPDOWN_NAME", "DROPDOWN_VALUE" };
		String SQL_QUERY = "select DROPDOWN_NAME, DROPDOWN_VALUE from DROP_DOWN where MODULE_NAME=? and ATTRDROP_NAME=? and module_type = ?";
		//Vector vt = du.select(SQL_QUERY, col);
		Vector vt = du.selectQuery(SQL_QUERY, new String[] { moduleName, colName, type });

		List<DropDownData> ret = translateDropDownValues(vt, userName);
		Collections.sort(ret);
		return ret;
	}

	private List<DropDownData> translateDropDownValues(Vector param, String userName)
	{

		// format
		// Show and store ATTR_Headline+(Element_id) [[PRODUCTS]] na
		// Show ATTR_Headline but store Element_id [[PRODUCTS]]
		// [[Element_id,ATTR_Headline]]
		// Get Category 1 [[FLEX3,C1]] [[Category_id,ATTR_categoryName]]
		// Get all Category 2 [[FLEX3,C2]] [[Category_id,ATTR_categoryName]]
		// Get C2 under C1 [[FLEX3,C2]]
		// [[Category_id,ATTR_categoryName;parentname="Home Page"]]
		List<DropDownData> ret = new ArrayList<DropDownData>();
		if (param.size() == 0)
		{
			return ret;
		}
		String[] vals = (String[]) param.get(0);
		// First check if we're dealing with a normal DROP_DOWN or a module
		// reference [[...]]
		if (!vals[0].startsWith("[[") || !vals[0].endsWith("]]"))
		{ // not a
			// module
			// reference
			for (int k = 0; k < param.size(); k++)
			{
				String[] s = (String[]) param.get(k);
				DropDownData ddd = new DropDownData();
//        ddd.setValue(StringEscapeUtils.escapeHtml(s[0]));
//        ddd.setText(StringEscapeUtils.escapeHtml(s[1]));
				ddd.setValue(s[0]);
				ddd.setText(s[1]);
				ret.add(ddd);
			}
			return ret;
		}
		// a module reference

		if ("[[MACRO]]".equalsIgnoreCase(vals[0]))
		{
			Map<String, String> context = getSqlParamsFromThreadContext();

			String sql = vals[1].substring(2, vals[1].length() - 2);
			for (Entry<String, String> entry : context.entrySet())
			{
				sql = StringUtils.replace(sql, "{" + entry.getKey() + "}", entry.getValue());
			}

			List<String[]> results = DataAccess.getInstance().select(sql, new StringArrayMapper());
			for (String[] result : results)
			{
				DropDownData ddd = new DropDownData();
//        ddd.setValue(StringEscapeUtils.escapeHtml(result[1]));
//        ddd.setText(StringEscapeUtils.escapeHtml(result[0]));
				ddd.setValue(result[1]);
				ddd.setText(result[0]);
				ret.add(ddd);
			}
			return ret;
		}

		boolean isElement = (vals[0].indexOf(",") == -1);
		String module;
		if (isElement)
		{ // [[modulename]]
			module = vals[0].substring(2, vals[0].length() - 2);
		}
		else
		{ // [[modulename,C1|C2]]
			module = vals[0].substring(2, vals[0].indexOf(","));
		}
		Vector<String[]> tableColumnName = null;
		// check is the vals[1] has an [[...]] ID in it then use this in the
		// following query.
		if (vals[1].startsWith("[[") && vals[1].endsWith("]]"))
		{
			tableColumnName = getElementColsNames(module);
			String id = vals[1].substring(2, vals[1].length() - 2);
			String[] nameArray = { "", "" };
			if (isElement)
			{
				if (id.indexOf(",") == -1)
				{
					Vector v = cda.getElement(module, id, true);
					if (v.size() > 0)
					{
						Map<String, String> m = (Map<String, String>) v.get(0);
						DropDownData ddd = new DropDownData();
//            ddd.setValue(StringEscapeUtils.escapeHtml(m.get("ATTR_Headline")));
//            ddd.setText(StringEscapeUtils.escapeHtml(m.get("ATTR_Headline")));
						ddd.setValue(m.get("ATTR_Headline"));
						ddd.setText(m.get("ATTR_Headline"));
						ddd.setDataFromTable(tableColumnName, m);
						ret.add(ddd);
					}
				}
				else
				{
					int level = Defaults.getInstance().getModuleLevelsInt(module);
					String[] optNcond = StringUtils.split(id, ";");
					String opt = optNcond[0];
					String cond = "";
					if (optNcond.length > 1)
					{
						cond = optNcond[1];
					}
					String optValue = opt.substring(0, opt.indexOf(','));
					String optText = opt.substring(opt.indexOf(',') + 1);
					String sql = "";
					if (level > 1)
					{
						sql = "select " + translateToSQL(optValue) + "," + translateToSQL(optText) + selectColumns(tableColumnName) + " from elements_" + module + " element, categories_" + module + " parent, categories_" + module
								+ " grandparent where element.category_id = parent.category_id and grandparent.category_id = parent.Category_ParentID";
					}
					else
					{
						sql = "select " + translateToSQL(optValue) + "," + translateToSQL(optText) + selectColumns(tableColumnName) + " from elements_" + module + " element, categories_" + module + " parent where element.category_id = parent.category_id";
					}
					if (!StringUtils.isBlank(cond))
					{
						sql += " and (" + cond.replace("\"", "'") + ")";
					}
					System.out.println("DropDownBuilder: " + sql);
					List<String[]> allLiveElements = DataAccess.getInstance().select(sql, new StringArrayMapper());
					for (String[] element : allLiveElements)
					{
						String[] nameAArray = { "", "" };
						DropDownData ddd = new DropDownData();
//            ddd.setValue(StringEscapeUtils.escapeHtml(element[0]));
//            ddd.setText(StringEscapeUtils.escapeHtml(element[1]));
						ddd.setValue(element[0]);
						ddd.setText(element[1]);
						Hashtable<String, String> ht = new Hashtable<String, String>();
						ht.put(optValue, element[0]);
						ht.put(optText, element[1]);
						for(int i=0; i< tableColumnName.size(); i++){
							ht.put(tableColumnName.get(i)[0], element[i+2]);
						}
						ddd.setDataFromTable(tableColumnName, ht);
						ret.add(ddd);
					}
				}
				return ret;
			}
			tableColumnName = getCategoryColsNames(module);
			String[] colsAndConditions = id.split(";");
			String selectedCols4Sql = getSelectedCols4Sql(tableColumnName, false);
			String sql = "";
			if (vals[0].split(",")[1].substring(0, vals[0].split(",")[1].length() - 2).equalsIgnoreCase("C1"))
			{
				sql = " select distinct " + selectedCols4Sql + " from Categories_" + module + " where folderLevel = 1 ";
				if (colsAndConditions.length > 1)
				{
					String[] strArray = colsAndConditions[1].split(",");
					for (int i = 0; i < strArray.length; i++)
					{
						sql += " and (" + strArray[i].trim().replace("\"", "'") + ")";
					}
				}
				sql += " order by attr_categoryname ";
			}
			else
			{
				sql = "select distinct " + selectedCols4Sql + " from (" + " select " + "   p.category_id as c1_id, " + "   p.ATTR_categoryName as c1_name, " + "   c.* " + " from categories_" + module
						+ " c, categories_" + module + " p " + " where c.category_parentID = p.category_id " + " ) as a where 1 = 1 ";
				if (colsAndConditions.length > 1)
				{
					String[] strArray = colsAndConditions[1].split(",");
					for (int i = 0; i < strArray.length; i++)
					{
						String condition = strArray[i];
						if (condition.toUpperCase().startsWith("PARENTNAME="))
						{
							condition = "c1_name=" + condition.substring("PARENTNAME=".length());
						}
						sql += " and (" + condition.replace("\"", "'") + ")";
					}
				}
				sql += " order by attr_categoryname ";
			}
			String[] cols = selectedCols4Sql.split(",");
			String[] valueAndText = colsAndConditions[0].split(",");
			//Vector v = new DBaccess().select(sql, cols, false, false);
			Vector v = new DBaccess().selectQuery(sql);
			for (int i = 0; i < v.size(); i++)
			{
				String[] values = (String[]) v.elementAt(i);
				Map<String, String> m = new Hashtable<String, String>();
				for (int j = 0; j < cols.length; j++)
				{
					m.put(cols[j], values[j]);
				}
				DropDownData ddd = new DropDownData();
//        ddd.setValue(StringEscapeUtils.escapeHtml(m.get(valueAndText[0])));
//        ddd.setText(StringEscapeUtils.escapeHtml(m.get(valueAndText[1])));
				ddd.setValue(m.get(valueAndText[0]));
				ddd.setText(m.get(valueAndText[1]));
				ddd.setDataFromTable(tableColumnName, m);
				ret.add(ddd);
			}
			return ret;
		}
		if (vals[1] != null && vals[1].startsWith("c."))
		{
			String folderLevel = vals[1].substring("c.".length());
			Vector result = cda.getColumns(cda.getCategoryCols(module), module, "Categories", "folderLevel", folderLevel, false, true, true, null, "ATTR_categoryName", true);
			for (int i = 0; i < result.size(); i++)
			{
				Hashtable h = (Hashtable) result.get(i);
				DropDownData ddd = new DropDownData();
//        ddd.setValue(StringEscapeUtils.escapeHtml(h.get("ATTR_categoryName") + " (" + h.get("Category_id") + ")"));
//        ddd.setText(StringEscapeUtils.escapeHtml(h.get("ATTR_categoryName") + " (" + h.get("Category_id") + ")"));
				ddd.setValue(h.get("ATTR_categoryName") + " (" + h.get("Category_id") + ")");
				ddd.setText(h.get("ATTR_categoryName") + " (" + h.get("Category_id") + ")");
				ddd.setDataFromTable(tableColumnName, h);
				ret.add(ddd);
			}
			return ret;
		}

		// use a normal method from ClientDataAccess to get only the live assets
		// from this module
		String[] cols = cda.getElementCols(module);
		tableColumnName = getElementColsNames(module);
		Vector result = null;
		if (userName != null)
		{
			UserDetails userDetail = new UserDetails(userName);
			// administrator has no restraint accessing the categories
			// administrator's UserLevel_id is 1
			if (!userDetail.getUserLevel_id().equals("1"))
			{

				String userId = userDetail.getUser_id();
				String sql_getPermittedCategoryIds = "SELECT CATEGORY_ID FROM user_module_access WHERE USER_ID= ? AND MODULE_NAME= ? ";
				//Vector vPermittedCategoryIds = du.select(sql_getPermittedCategoryIds);
				Vector vPermittedCategoryIds = du.selectQuerySingleCol(sql_getPermittedCategoryIds, new String[] { userId, module });
				String permittedCategoryIds = "";
				for (int i = 0; i < vPermittedCategoryIds.size(); i++)
				{
					String val = (String) vPermittedCategoryIds.get(i);
					permittedCategoryIds += val;
					if (i != vPermittedCategoryIds.size() - 1)
					{
						permittedCategoryIds += ",";
					}
				}

				String keyCol = "1";
				String keyVal = null;
				if (!permittedCategoryIds.equals(""))
				{
					keyVal = "1 AND Category_id IN (" + permittedCategoryIds + ")";
				}
				else
				{
					keyVal = "1";
				}
				result = cda.getColumns(cols, module, "Elements", keyCol, keyVal, false, true, true, null, "ATTR_Headline", true);
			}
			else
			{
				result = cda.getColumns(cols, module, "Elements", null, null, false, true, true, null, "ATTR_Headline", true);
			}
		}
		else
		{
			result = cda.getColumns(cols, module, "Elements", null, null, false, true, true, null, "ATTR_Headline", true);
		}
		// Vector result = cda.getCategoryElementsWithSpecifiedOrder(module,
		// "'%'", "ATTR_Headline");
		for (int i = 0; i < result.size(); i++)
		{
			Hashtable h = (Hashtable) result.get(i);
			DropDownData ddd = new DropDownData();
//      ddd.setValue(StringEscapeUtils.escapeHtml(h.get("ATTR_Headline") + " (" + h.get("Element_id") + ")"));
//      ddd.setText(StringEscapeUtils.escapeHtml(h.get("ATTR_Headline") + " (" + h.get("Element_id") + ")"));
			ddd.setValue(h.get("ATTR_Headline") + " (" + h.get("Element_id") + ")");
			ddd.setText(h.get("ATTR_Headline") + " (" + h.get("Element_id") + ")");
			ddd.setDataFromTable(tableColumnName, h);
			ret.add(ddd);
		}
		return ret;
	}

	private String selectColumns(Vector<String[]> tableColumnName)
	{
		String selectQuery = "";
		for(int i=0; i<tableColumnName.size(); i++) {
			selectQuery += ", element." + tableColumnName.get(i)[0];
		}
		return selectQuery;
	}
	
	// given string like attr_headline - (attr_modelnumber)
	private String translateToSQL(String str)
	{
		List<Integer> breaks = new ArrayList<Integer>();
		String validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890_";
		StringBuffer current = new StringBuffer();
		boolean lastFlag = false;
		for (int i = 0; i < str.length(); i++)
		{
			if (i == 0 || (validChars.indexOf(str.charAt(i)) == -1) != lastFlag)
			{
				breaks.add(i);
			}
			lastFlag = validChars.indexOf(str.charAt(i)) == -1;
		}
		List<String> tokens = new ArrayList<String>();
		for (int i = 0; i < breaks.size(); i++)
		{
			if (i == breaks.size() - 1)
			{
				tokens.add(str.substring(breaks.get(i)));
			}
			else
			{
				tokens.add(str.substring(breaks.get(i), breaks.get(i + 1)));
			}
		}
		List<String> sqls = new ArrayList<String>();
		for (String token : tokens)
		{
			if (token.length() == 0 || validChars.indexOf(token.charAt(0)) == -1)
			{
				sqls.add("'" + token + "'");
			}
			else
			{
				sqls.add("element." + token);
			}
		}
		if (sqls.size() > 1)
		{
			return "concat(" + StringUtils.join(sqls, ",") + ")";
		}
		else
		{
			return sqls.get(0);
		}
	}

	/**
	 * This method translates a Vector or results into a map. However it also
	 * checks what the first drop down name/value pair is and if they are
	 * surrounded by [[...]] then it uses these as a reference to produce
	 * dynamic lists from other tables.
	 * 
	 * @param vt
	 * @return
	 */
	private List<DropDownData> translateDropDownValues(Vector vt)
	{
		return translateDropDownValues(vt, null);
	}

	public void setDropDownValue(String moduleName, String colName, String dropDownName, String dropDownValue, String type)
	{
		Hashtable ht = new Hashtable();
		ht.put("MODULE_NAME", moduleName);
		ht.put("ATTRDROP_NAME", colName);
		ht.put("DROPDOWN_NAME", dropDownName);
		ht.put("DROPDOWN_VALUE", dropDownValue);
		ht.put("MODULE_TYPE", type);
		du.insertData(ht, "id", "DROP_DOWN");
	}

	public void updateDropDownValue(String id, String dropDownName, String dropDownValue)
	{
		Hashtable ht = new Hashtable();
		ht.put("DROPDOWN_NAME", dropDownName);
		ht.put("id", id);
		ht.put("DROPDOWN_VALUE", dropDownValue);
		du.updateData(ht, "id", "DROP_DOWN");
	}

	public boolean doValueExist(String moduleName, String colName, String dropDownName, String dropDownValue)
	{
		return false;
	}

	public void deleteDropDownValue(String id)
	{
		String SQL_QUERY = "delete from DROP_DOWN where id in (" + id + ")";
		du.updateData(SQL_QUERY);
	}

	/**
	 * Used internally by the
	 * /webdirector/secure/module/configure/dropdown/values page -
	 * essentially the drop down
	 * builder
	 * 
	 * @param id
	 * @return
	 */
	public Map getExistingNameAndValue(String id)
	{
		String col[] = { "DROPDOWN_NAME", "DROPDOWN_VALUE" };
		String SQL_QUERY = "select DROPDOWN_NAME, DROPDOWN_VALUE from DROP_DOWN where ID=? ";
		//Vector vt = du.select(SQL_QUERY, col);
		Vector vt = du.selectQuery(SQL_QUERY, new String[] { id });
		Map map = new LinkedHashMap();
		for (int k = 0; k < vt.size(); k++)
		{
			String[] s = (String[]) vt.get(k);
			map.put(s[0], s[1]);
		}
		return map;
	}

	private String replaceCarriageReturns(String value)
	{
		int s = value.indexOf("\n");
		while (s != -1)
		{
			try
			{
				String start = value.substring(0, s);
				String end = value.substring(++s);
				// if statment missing here to check length before s
				String otherCR = value.substring(s - 6, s - 2);

				/* if there are <br> around then don't insert another one */
				if (otherCR.equalsIgnoreCase("</P>") || otherCR.equalsIgnoreCase("<br>") || otherCR.matches(".<[pP]>"))
				{
					value = start + end;
				}
				else
					value = start + "<br/>" + end;

				s = value.indexOf("\n");

			}
			catch (Exception e)
			{
				e.printStackTrace();
				s = -1;
			}

		}

		return value;
	}

	private Vector<String[]> getCategoryColsNames(String module)
	{
		String[] cols = new String[] { "Label_InternalName", "Label_ExternalName" };
		String sql = "Select Label_InternalName, Label_ExternalName from CategoryLabels_" + DatabaseValidation.encodeParam(module) + " where Label_OnOff = 1 order by display_order";
		return new DBaccess().selectQuery(sql);
	}

	private Vector<String[]> getElementColsNames(String module)
	{
		String[] cols = new String[] { "Label_InternalName", "Label_ExternalName" };
		String sql = "Select Label_InternalName, Label_ExternalName from Labels_" + DatabaseValidation.encodeParam(module) + " where Label_OnOff = 1 order by display_order";
		return new DBaccess().selectQuery(sql);
	}

	private String getSelectedCols4Sql(Vector<String[]> colsNames, boolean isElement)
	{
		StringBuffer ret = new StringBuffer();
		if (isElement)
		{
			ret.append("Element_id");
		}
		else
		{
			ret.append("Category_id");
		}
		for (String[] name : colsNames)
		{
			ret.append(",");
			ret.append(name[0]);
		}
		return ret.toString();
	}

	public static void main(String[] args)
	{
		System.out.println(new DropDownBuilder().translateToSQL("ATTR_Headline"));
	}

	public static void setSqlParamsToThreadContext(Map<String, String> context)
	{
		ThreadContext.setAttribute("SqlParam", context);
	}

	public static Map<String, String> getSqlParamsFromThreadContext()
	{
		Map<String, String> context = (Map<String, String>) ThreadContext.getAttribute("SqlParam");
		if (context == null)
		{
			context = new HashMap<String, String>();
			context.put("ID", "0");
		}
		return context;
	}
}
