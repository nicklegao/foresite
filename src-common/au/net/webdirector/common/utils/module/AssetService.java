// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   AssetService.java

package au.net.webdirector.common.utils.module;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.RecoverableDataAccessException;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.TransactionModuleAccess;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import au.net.webdirector.common.datatypes.domain.DataType;
import au.net.webdirector.common.datatypes.service.DataTypesService;

public class AssetService
{

	public AssetService()
	{
	}

	public int copyAsset(int elementID, int category, String module)
	{
		return copyAsset(elementID, category, module, true);
	}

	public int copyAsset(int elementID, int category, String module, boolean makeUnlive)
	{
		TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true));
		try
		{
			TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
			ElementData element = ma.getElementById(module, String.valueOf(elementID));
			List columns = (new DataTypesService()).loadGenericDataTypes(module, false, "");
			ElementData newEle = copyAsset(element, String.valueOf(category), module, ma, columns, makeUnlive);
			return Integer.parseInt(newEle.getId());
		}
		catch (Exception e)
		{
			txManager.rollback();
			throw new RecoverableDataAccessException("Cannot copy asset.", e);
		}
		finally
		{
			txManager.commit();
		}
	}

	public ElementData copyAsset(ElementData element, String categoryId, String module, TransactionModuleAccess ma, List elementColumns) throws SQLException, IOException
	{
		return copyAsset(element, categoryId, module, ma, elementColumns, true);
	}

	public ElementData copyAsset(ElementData element, String categoryId, String module, TransactionModuleAccess ma, List elementColumns, boolean makeUnlive) throws SQLException, IOException
	{
		element.setParentId(categoryId);
		if (makeUnlive)
		{
			element.setLive(false);
		}
		for (String key : element.keySet())
		{
			if (!key.startsWith("attrinteger_") && !key.startsWith("attr_"))
			{
				continue;
			}
			if (!isUnique(elementColumns, module, key, ""))
			{
				continue;
			}
			if (key.startsWith("attrinteger_") && StringUtils.isNotBlank(element.getString(key)))
			{
				element.put(key, getNextID(key, "Elements_" + module));
				continue;
			}
			if (key.startsWith("attr_"))
			{
				String val = element.getString(key);
				String newVal = "Copy of " + val + " (" + getNextID("Element_id", "Elements_" + module) + ")";
				if (val.indexOf("Copy of ") == 0 && val.lastIndexOf("(") != -1 && val.lastIndexOf(")") == val.length() - 1)
				{
					// 'Copy of filename (n)'
					newVal = val.substring(0, val.lastIndexOf("(")) + getNextID("Element_id", "Elements_" + module) + ")";
				}
				element.put(key, newVal);
				continue;
			}
		}
		ElementData newEle = ma.insertElement(element, module);
		return newEle;
	}

	public boolean moveAsset(int elementID, int category, String module)
	{
		TransactionDataAccess da = TransactionDataAccess.getInstance(false);
		try
		{
			reorderElements(elementID, module, da);

			int displayOrder = getChildrenCount(module, category, da);
			String query = "update Elements_" + module + " set Category_id = ?, display_order= ? where Element_id = ?";
			int count = da.update(query, new Object[] { category, displayOrder, elementID });
			return count == 1;
		}
		catch (Exception e)
		{
			da.rollback();
			throw new RecoverableDataAccessException("Cannot copy asset.", e);
		}
		finally
		{
			da.commit();
		}

	}

	public boolean deleteAsset(String module, int id)
	{
		TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false), StoreFileAccessFactory.getInstance(true));
		try
		{
			TransactionModuleAccess ma = TransactionModuleAccess.getInstance(txManager);
			reorderElements(id, module, txManager.getDataAccess());
			return ma.deleteElementById(module, String.valueOf(id)) > 0;
		}
		catch (Exception e)
		{
			txManager.rollback();
			throw new RecoverableDataAccessException("Cannot copy asset.", e);
		}
		finally
		{
			txManager.commit();
		}
	}

	private boolean reorderElements(int elementID, String module, TransactionDataAccess da) throws SQLException
	{
		String query = "select display_order, category_id from Elements_" + module + " where element_id = ? ";
		String cols[] = { "display_order", "category_id" };
		//Vector v = db.select(query, cols);
		String[] results = da.selectStringArray(query, new String[] { String.valueOf(elementID) });

		int rows = 0;
		if (results[0] != null && !results[0].equals("0") && !results[0].equals(""))
		{
			query = "update Elements_" + module + " set display_order = display_order-1 where display_order > ? and Live = 1 and category_id = ?";
			System.out.println(query);
			rows = da.update(query, new Object[] { results[0], results[1] });
		}
		return rows > 0;
	}

	public static boolean isUnique(List columns, String module, String fieldName, String user)
	{
		for (Iterator iterator = columns.iterator(); iterator.hasNext();)
		{
			DataType col = (DataType) iterator.next();
			if (col.getInternalName().equalsIgnoreCase(fieldName))
				return col.isUnique();
		}
		return false;
	}

	public static int getNextID(String keyColumn, String tableName)
	{
		String query;
		List v;
		keyColumn = DatabaseValidation.encodeParam(keyColumn);
		tableName = DatabaseValidation.encodeParam(tableName);

		if (keyColumn.equalsIgnoreCase("Element_id") || keyColumn.equalsIgnoreCase("Category_id"))
		{
			query = (new StringBuilder("SHOW TABLE STATUS WHERE Name = ?")).toString();
			v = DataAccess.getInstance().select(query, new Object[] { tableName }, new HashtableMapper());
			if (v == null || v.size() == 0)
				return 0;
			else
				return Integer.parseInt((String) ((Hashtable) v.get(0)).get("Auto_increment"));
		}
		query = (new StringBuilder("select max(")).append(keyColumn).append(") from ").append(tableName).toString();
		//v = (new DBaccess()).select(query);
		v = (new DBaccess()).selectQuerySingleCol(query);
		if (v != null && v.size() != 0)
			return Integer.parseInt((String) v.get(0)) + 1;
		else
			return 0;
	}

	private int getChildrenCount(String tableName, int categoryId, TransactionDataAccess da)
	{
		String s = "select max(display_order) +1 from elements_" + DatabaseValidation.encodeParam(tableName) + " where category_id = ? and live= 1";
		int count = 1;
		try
		{
			String order = da.selectString(s, new String[] { String.valueOf(categoryId) });
			count = (order == null ? 1 : Integer.parseInt(order));
		}
		catch (Exception nfe)
		{

		}
		return count;
	}

	static Logger logger = Logger.getLogger(AssetService.class);

}
