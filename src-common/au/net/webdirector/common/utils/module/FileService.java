package au.net.webdirector.common.utils.module;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.Defaults;

public class FileService
{

	private static Defaults d = Defaults.getInstance();
	private static Logger logger = Logger.getLogger(FileService.class);

	/**
	 * physically copy files between two locations in the stores but uses the
	 * elementID's to work out what the new and old store paths should be.
	 * 
	 * @param newElementID
	 * @param oldElementID
	 * @param module
	 * @return
	 */

	public static boolean copyFilesFromAssetToAsset(int newElementID, int oldElementID, String module)
	{
		File srcPath = new File(d.getStoreDir() + File.separatorChar + module + File.separatorChar + oldElementID);
		File dstPath = new File(d.getStoreDir() + File.separatorChar + module + File.separatorChar + newElementID);

		if (srcPath.exists() && srcPath.isDirectory())
			return copyDirectory(srcPath, dstPath);

		return true;
	}

	public static boolean copyFilesFromCateToCate(int newElementID, int oldElementID, String module)
	{
		File srcPath = new File(d.getStoreDir() + File.separatorChar + module + File.separatorChar + "Categories" + File.separatorChar + oldElementID);
		File dstPath = new File(d.getStoreDir() + File.separatorChar + module + File.separatorChar + "Categories" + File.separatorChar + newElementID);

		if (srcPath.exists() && srcPath.isDirectory())
			return copyDirectory(srcPath, dstPath);

		return true;
	}

	public static boolean copyFilesFromNoteToNote(int newElementID, int oldElementID)
	{
		File srcPath = new File(d.getStoreDir() + File.separatorChar + "NOTES" + File.separatorChar + File.separatorChar + oldElementID);
		File dstPath = new File(d.getStoreDir() + File.separatorChar + "NOTES" + File.separatorChar + File.separatorChar + newElementID);

		if (srcPath.exists() && srcPath.isDirectory())
			return copyDirectory(srcPath, dstPath);

		return true;
	}

	public static boolean copyDirectory(File from, File to)
	{
		try
		{

			FileUtils.copyDirectory(from, to);
			return true;

		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
			e.printStackTrace();
		}

		return false;
	}

	public static boolean deleteAssetFiles(int id, String module)
	{
		File srcPath = new File(d.getStoreDir() + File.separatorChar + module + File.separatorChar + id);
		if (srcPath.exists() && srcPath.isDirectory())
			return deleteDirectory(srcPath);

		return true;
	}

	public static boolean deleteCategoryFiles(int id, String module)
	{
		File srcPath = new File(d.getStoreDir() + File.separatorChar + module + File.separatorChar + "Categories" + File.separatorChar + id);
		return deleteDirectory(srcPath);
	}

	public static boolean deleteDirectory(File dir)
	{
		try
		{
			FileUtils.deleteDirectory(dir);
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}

	public boolean writeToFile(StringBuffer sb, File file)
	{
		try
		{
			FileWriter fstream = new FileWriter(file);
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(sb.toString());
			out.close();
			return true;
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
		}
		return false;
	}

}
