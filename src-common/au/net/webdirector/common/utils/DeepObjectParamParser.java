package au.net.webdirector.common.utils;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

public class DeepObjectParamParser
{

	private static String paramPath(String base, Object[] args)
	{
		if (args.length == 0)
		{
			return base;
		}
		String generated = String.format("%s[%s]", base, StringUtils.join(args, "]["));
		return generated;
	}

	public static String readString(HttpServletRequest request, String base, Object... args)
	{
		String val = request.getParameter(paramPath(base, args));
		return val;
	}

	public static int readInt(HttpServletRequest request, String base, Object... args)
	{
		return Integer.parseInt(readString(request, base, args));
	}

	public static int countArray(HttpServletRequest request, String base, String test)
	{

		int i = 0;

		while (DeepObjectParamParser.readString(request, base, i, test) != null)
		{
			++i;
		}
		return i;
	}
}
