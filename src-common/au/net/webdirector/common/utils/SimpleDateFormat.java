/**
 * @author Nick Yiming Gao
 * @date 2016-5-10
 */
package au.net.webdirector.common.utils;

/**
 * 
 */

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;

public class SimpleDateFormat
{

	private static Logger logger = Logger.getLogger(SimpleDateFormat.class);
	private String pattern;
	private Locale locale;

	public SimpleDateFormat(String pattern)
	{
		this.pattern = pattern;
	}

	public SimpleDateFormat(String pattern, Locale locale)
	{
		this.pattern = pattern;
		this.locale = locale;
	}

	public Date parse(String source) throws ParseException
	{
		return initSimpleDateFormat().parse(source);
	}

	public final String format(Date date)
	{
		return initSimpleDateFormat().format(date);
	}

	public final String format(Object obj)
	{
		return initSimpleDateFormat().format(obj);
	}

	private java.text.SimpleDateFormat initSimpleDateFormat()
	{
		if (locale != null)
		{
			return new java.text.SimpleDateFormat(pattern, locale);
		}
		return new java.text.SimpleDateFormat(pattern);
	}
}
