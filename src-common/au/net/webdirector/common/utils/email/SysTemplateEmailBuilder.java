/**
 * @author Nick Yiming Gao
 * @date 2015-9-17
 */
package au.net.webdirector.common.utils.email;

/**
 * 
 */

import java.io.Reader;
import java.io.StringReader;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.ModuleData;
import au.net.webdirector.common.datalayer.client.ModuleHelper;

public class SysTemplateEmailBuilder extends AbstractEmailBuilder
{
	protected String module = "SYS_TEMPLATE";
	protected String templateField = "attrlong_content";
	protected boolean isCategory = false;

	public SysTemplateEmailBuilder()
	{
		super();
	}

	public SysTemplateEmailBuilder(HttpServletRequest request)
	{
		super(request);
	}

	public SysTemplateEmailBuilder(String module, String templateField, boolean isCategory)
	{
		super();
		this.isCategory = isCategory;
		this.module = module;
		this.templateField = templateField;
	}

	public SysTemplateEmailBuilder(String module, String templateField, boolean isCategory, HttpServletRequest request)
	{
		super(request);
		this.isCategory = isCategory;
		this.module = module;
		this.templateField = templateField;
	}

	@Override
	public SysTemplateEmailBuilder addModule(String moduleName, Map<String, ? extends Object> map)
	{
		super.addModule(moduleName, map);
		return this;
	}

	@Override
	protected Reader getTemplateReader(String templateName) throws TemplateNotFoundException
	{
		try
		{
			ModuleAccess ma = ModuleAccess.getInstance();
			ModuleData page = isCategory ? ma.getCategoryByName(module, templateName) : ma.getElementByName(module, templateName);
			if (page == null)
			{
				throw new TemplateNotFoundException("Unable to find template " + templateName);
			}
			return new StringReader(ModuleHelper.readStringFromFile(page, templateField));
		}
		catch (Exception e)
		{
			throw new TemplateNotFoundException("Unable to find template " + templateName, e);
		}
	}
}
