/**
 * @author Nick Yiming Gao
 * @date 2015-9-17
 */
package au.net.webdirector.common.utils.email;

/**
 * 
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class FileTemplateEmailBuilder extends AbstractEmailBuilder
{

	protected String templatesPackage;

	public FileTemplateEmailBuilder(String templatesPackage)
	{
		super();
		this.templatesPackage = templatesPackage;
	}

	public FileTemplateEmailBuilder(String templatesPackage, HttpServletRequest request)
	{
		super(request);
		this.templatesPackage = templatesPackage;
	}

	@Override
	protected Reader getTemplateReader(String templateName) throws TemplateNotFoundException
	{
		URL url = getClass().getResource(templatesPackage + templateName);
		if (url == null)
		{
			throw new TemplateNotFoundException("Unable to find email template " + templateName);
		}
		File f = null;
		try
		{
			f = new File(url.toURI());
		}
		catch (URISyntaxException e)
		{
			f = new File(url.getPath());
		}
		try
		{
			return new FileReader(f);
		}
		catch (FileNotFoundException e)
		{
			throw new TemplateNotFoundException("Unable to find email template " + templateName, e);
		}
	}

	@Override
	public FileTemplateEmailBuilder addModule(String moduleName, Map<String, ? extends Object> map)
	{
		return (FileTemplateEmailBuilder) super.addModule(moduleName, map);
	}
}
