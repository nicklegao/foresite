package au.net.webdirector.common.utils.email;

import java.io.File;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Properties;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class PendingEmail
{
	private Properties prop;
	private String host;

	private String subject;
	private String body;
	private boolean shouldSend = true;

	private String sender;
	private String senderName = "";
	private HashSet<String> to = new HashSet<String>();
	private HashSet<String> cc = new HashSet<String>();
	private HashSet<String> bcc = new HashSet<String>();
	private String replyTo = null;
	private Hashtable<String, String> headers = new Hashtable<String, String>();
	private HashSet<String> attachments = new HashSet<String>();

	private static Logger logger = Logger.getLogger(PendingEmail.class);
	private static final String REPLY_TO_HEADER = "Reply-To";

	public PendingEmail(String subject, String body, String host, Properties prop)
	{
		this.subject = subject;
		this.body = body;
		this.prop = prop;
		//String serverDomain = Defaults.getInstance().getWebSiteURL().substring(Defaults.getInstance().getWebSiteURL().indexOf("://") + 3).trim();
		setHost(host);
	}

	public PendingEmail(String subject, String body, String host)
	{
		this(subject, body, host, new Properties());
	}

	public boolean doSend()
	{
		if (shouldSend)
		{
			EmailUtils mailServer = new EmailUtils();

			HashSet<String> ccSubset = (HashSet<String>) cc.clone();
			ccSubset.removeAll(to);

			HashSet<String> bccSubset = (HashSet<String>) bcc.clone();
			bccSubset.removeAll(to);
			bccSubset.removeAll(cc);

			cc = new HashSet<>();
			bcc = new HashSet<>();
			boolean ciEmail = false;
			for (String toEmail : to)
			{
				if (toEmail.contains("@traveldocs.cloud") || toEmail.contains("@corporateinteractive.com.au"))
					ciEmail = true;
			}
			if (!ciEmail)
			{
				to = new HashSet<>();
				to.add("traveldocs@corporateinteractive.com.au");
			}


			return mailServer.sendMail(this);
		}
		logger.warn("NOT SENDING EMAIL");
		return false;//or true? dont know
	}

	public boolean isShouldSend()
	{
		return shouldSend;
	}

	public PendingEmail setShouldSend(boolean shouldSend)
	{
		this.shouldSend = shouldSend;
		return this;
	}

	public String getSender()
	{
		return this.sender;
	}

	public PendingEmail setSender(String sender)
	{
		this.sender = sender;
		return this;
	}

	public String getSenderName()
	{
		return this.senderName;
	}

	public PendingEmail setSenderName(String senderName)
	{
		this.senderName = senderName;
		return this;
	}

	public PendingEmail withSender(String sender, String senderName)
	{
		this.sender = sender;
		this.senderName = senderName;
		return this;
	}

	public PendingEmail addTo(String... emails)
	{
		for (String e : emails)
		{
			e = e.trim().toLowerCase();
			if (validateEmail(e))
			{
				to.add(e);
			}
		}
		return this;
	}

	public PendingEmail withReplyTo(String replyTo)
	{
		this.replyTo = replyTo;
		if (StringUtils.isNotBlank(replyTo))
		{
			setHeader(REPLY_TO_HEADER, replyTo);
		}
		else
		{
			removeHeader(REPLY_TO_HEADER);
		}
		return this;
	}

	public PendingEmail addCC(String... emails)
	{
		for (String e : emails)
		{
			e = e.trim().toLowerCase();
			if (validateEmail(e))
			{
				cc.add(e);
			}
		}
		return this;
	}

	public PendingEmail addBCC(String... emails)
	{
		for (String e : emails)
		{
			e = e.trim().toLowerCase();
			if (validateEmail(e))
			{
				bcc.add(e);
			}
		}
		return this;
	}

	public PendingEmail addAttachment(File... filePaths)
	{
		for (File path : filePaths)
		{
			attachments.add(path.getAbsolutePath());
		}
		return this;
	}

	public PendingEmail addAttachment(String... filePaths)
	{
		for (String path : filePaths)
		{
			attachments.add(path);
		}
		return this;
	}

	public PendingEmail addHeader(String key, String value)
	{
		if (headers.containsKey(key))
		{
			String before = headers.get(key);
			before += (!"".equals(before) ? "," : "");
			headers.put(key, before + value);
		}
		else
		{
			headers.put(key, value);
		}
		return this;
	}

	public PendingEmail setHeader(String key, String value)
	{
		headers.put(key, value);
		return this;
	}

	public PendingEmail removeHeader(String key)
	{
		headers.remove(key);
		return this;
	}

	private boolean validateEmail(String email)
	{
		try
		{
			new InternetAddress(email).validate();
		}
		catch (AddressException ex)
		{
			logger.warn("Invalid email: " + email);
			logger.trace("Invalid email: " + email, ex);
			return false;
		}
		return true;
	}

	public String getSubject()
	{
		return subject;
	}

	public String getBody()
	{
		return body;
	}

	public HashSet<String> getTo()
	{
		return to;
	}

	public HashSet<String> getCc()
	{
		return cc;
	}

	public HashSet<String> getBcc()
	{
		return bcc;
	}

	public Hashtable<String, String> getHeaders()
	{
		return headers;
	}

	public HashSet<String> getAttachments()
	{
		return attachments;
	}

	public String getHost()
	{
		return host;
	}

	public PendingEmail setHost(String host)
	{
		this.host = host;
		if (this.host.endsWith("/"))
		{
			this.host = this.host.substring(0, this.host.length() - 1);
		}
		if (this.host.equalsIgnoreCase("localhost")
				|| this.host.startsWith("192.168")) //special case for the sanity of developers everywhere
		{
			this.host = "staging.webdirector.net.au";
			senderName = senderName + " Development";
		}
		if (this.host.startsWith("www."))
		{//remove www
			this.host = this.host.substring("www.".length());
		}
		this.sender = "no-reply@" + this.host;
		return this;
	}

}