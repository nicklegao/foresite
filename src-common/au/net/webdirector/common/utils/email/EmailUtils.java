package au.net.webdirector.common.utils.email;

import java.io.File;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.Defaults;

public class EmailUtils
{

	protected static Logger logger = Logger.getLogger(EmailUtils.class);

	/**
	 * Remove duplicates from the email batch list and also sort this list (for
	 * debugging really)
	 *
	 * @param bcc
	 * @return String
	 */
	protected String[] rationaliseBCCList(String[] bcc)
	{
		Set set = new HashSet();
		for (int i = 0; i < bcc.length; i++)
			set.add(bcc[i]);

		Set sortedSet = new TreeSet(set);
		int size = sortedSet.size();

		return (String[]) sortedSet.toArray(new String[size]);
	}

	public void sendMailUsingThread(final String[] to, final String from, final String subject, final String body, final String attachment)
	{
		Thread t1 = new Thread()
		{

			@Override
			public void run()
			{
				for (int i = 0; i < to.length; i++)
				{
					// create a single element array for passing into sendMail
					// method
					boolean sentOK = new EmailUtils().sendMail(new String[] { to[i] }, null, null, from, subject, body, attachment);
					logger.info("sent email to '" + to[i] + "' = " + sentOK);
				}
			}
		};
		t1.start();
	}

	public boolean sendMail(PendingEmail mail)
	{
		String[] to = mail.getTo().toArray(new String[] {});
		String[] cc = mail.getCc().toArray(new String[] {});
		String[] bcc = mail.getBcc().toArray(new String[] {});
		Object attach = mail.getAttachments().toArray(new String[] {});
		Hashtable<String, String> headers = mail.getHeaders();

		String[] attachment = null;
		if (attach != null)
		{
			if (attach instanceof String)
			{
				attachment = new String[1];
				attachment[0] = (String) attach;
			}
			else if (attach instanceof String[])
			{
				attachment = (String[]) attach;
			}
		}

		Defaults d = Defaults.getInstance();
		boolean sentOK = true;

		final String username = d.getUsernameSMTPserver();
		final String password = d.getPasswordSMTPserver();
		String host = d.getMailServerSMTP();

		MimeMessage message = null;
		try
		{
			// Get system properties
			Properties props = System.getProperties();
			//

			// Setup mail server
			logger.debug("HOST " + host);
			props.put("mail.smtp.host", host);
			// props.put("mail.smtp.port", "1025");

			props.put("mail.smtp.auth", "true");

			Session session = Session.getInstance(props,
					new javax.mail.Authenticator()
					{
						@Override
						protected PasswordAuthentication getPasswordAuthentication()
						{
							return new PasswordAuthentication(username, password);
						}
					});

//			Transport transport = session.getTransport("smtp");
//			transport.connect(host, username, password);
			// Define message
			message = new MimeMessage(session);

			if (headers != null)
			{
				for (String key : headers.keySet())
				{
					logger.debug(" Adding Header " + key + ":" + headers.get(key));
					message.addHeader(key, headers.get(key));
				}
			}

			if (to != null)
			{
				for (int i = 0; i < to.length; i++)
				{
					logger.debug(" Sending mail to " + to[i]);
					message.addRecipient(Message.RecipientType.TO, new InternetAddress(to[i]));
				}
			}
			if (cc != null)
			{
				for (int i = 0; i < cc.length; i++)
				{
					logger.debug("sendMail(): Sending mail cc " + cc[i]);
					message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc[i]));
				}
			}
			if (bcc != null)
			{
				for (int i = 0; i < bcc.length; i++)
				{
					logger.debug("len " + bcc.length);
					logger.debug("sendMail(): Sending mail bcc " + bcc[i]);
					InternetAddress ia = null;
					try
					{
						ia = new InternetAddress(bcc[i]);
						message.addRecipient(isMandrill(host) ? Message.RecipientType.CC : Message.RecipientType.BCC, ia);
					}
					catch (AddressException ae)
					{
						logger.debug("Address invalid " + bcc[i] + "\nThrew error " + ae.toString());
					}
				}
			}

			message.setFrom(StringUtils.isNotBlank(mail.getSenderName()) ? new InternetAddress(mail.getSender(), mail.getSenderName()) : new InternetAddress(mail.getSender()));
			message.setSubject(mail.getSubject());
			// create the message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			// fill message with body and set type
			messageBodyPart.setContent(mail.getBody(), "text/html");
			// messageBodyPart.setText(body);
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			if (attachment != null)
			{

				for (int i = 0; i < attachment.length; i++)
				{
					messageBodyPart = new MimeBodyPart();
					logger.debug(" Atachement " + attachment[i]);
					File f = new File(attachment[i]);
					DataSource source = new FileDataSource(f);
					messageBodyPart.setDataHandler(new DataHandler(source));
					messageBodyPart.setFileName(f.getName());
					multipart.addBodyPart(messageBodyPart);
				}
			}

			// Put parts in message
			message.setContent(multipart);

			// Send message
			message.saveChanges();// implicit with send()

			// send message
//			transport.sendMessage(message, message.getAllRecipients());
//			transport.close();
			Transport.send(message);

		}
		catch (Exception e)
		{
			logger.error("emailUtils.java: sendMail() EXCEPTION " + e.getMessage());
			e.printStackTrace();
			sentOK = false;
		}

		if (to != null)
			logger.debug("Sent mail to " + to.toString() + " and it was " + sentOK);
		if (cc != null)
			logger.debug("Sent mail to " + cc.toString() + " and it was " + sentOK);
		if (bcc != null)
			logger.debug("Sent mail to " + bcc.toString() + " and it was " + sentOK);

		return sentOK;
	}

	private static boolean isMandrill(String host)
	{
		// !important: Mandrill doesn't support bcc from May/2019, so we use CC instead of BCC. It's safe because Mandrill sends separated emails to each recipient.   
		return StringUtils.containsIgnoreCase(host, "mandrill");
	}

	@Deprecated
	public boolean sendMail(String[] to, String[] cc, String[] bcc, String from, String subject, String body, Object attach)
	{
		Defaults d = Defaults.getInstance();
		return sendMail(to, cc, bcc, from, d.getFromEmailRealName(), subject, body, attach);
	}

	@Deprecated
	public boolean sendMail(String[] to, String[] cc, String[] bcc, String from, String senderName, String subject, String body, Object attach)
	{
		String[] attachment = null;
		if (attach != null)
		{
			if (attach instanceof String)
			{
				attachment = new String[1];
				attachment[0] = (String) attach;
			}
			else if (attach instanceof String[])
			{
				attachment = (String[]) attach;
			}
		}

		Defaults d = Defaults.getInstance();
		boolean sentOK = true;

		final String username = d.getUsernameSMTPserver();
		final String password = d.getPasswordSMTPserver();
		String host = d.getMailServerSMTP();
		MimeMessage message = null;
		try
		{
			// Get system properties
			Properties props = System.getProperties();
			//

			// Setup mail server
			logger.debug("HOST " + host);
			props.put("mail.smtp.host", host);

			props.put("mail.smtp.auth", "true");
//	    props.put("mail.smtp.port", "587");

			Session session = Session.getInstance(props,
					new javax.mail.Authenticator()
					{
						@Override
						protected PasswordAuthentication getPasswordAuthentication()
						{
							return new PasswordAuthentication(username, password);
						}
					});

//			Transport transport = session.getTransport("smtp");
//			transport.connect(host, username, password);
			// Define message
			message = new MimeMessage(session);

			if (to != null)
			{
				for (int i = 0; i < to.length; i++)
				{
					logger.debug(" Sending mail to " + to[i]);
					message.addRecipient(Message.RecipientType.TO, new InternetAddress(to[i]));
				}
			}
			if (cc != null)
			{
				for (int i = 0; i < cc.length; i++)
				{
					logger.debug("sendMail(): Sending mail cc " + cc[i]);
					message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc[i]));
				}
			}
			if (bcc != null)
			{
				for (int i = 0; i < bcc.length; i++)
				{
					logger.debug("len " + bcc.length);
					logger.debug("sendMail(): Sending mail bcc " + bcc[i]);
					InternetAddress ia = null;
					try
					{
						ia = new InternetAddress(bcc[i]);
						message.addRecipient(isMandrill(host) ? Message.RecipientType.CC : Message.RecipientType.BCC, ia);
					}
					catch (AddressException ae)
					{
						logger.debug("Address invalid " + bcc[i] + "\nThrew error " + ae.toString());
					}
				}
			}

			message.setFrom(new InternetAddress(from, senderName));
			message.setSubject(subject);
			// create the message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			// fill message with body and set type
			messageBodyPart.setContent(body, "text/html");
			// messageBodyPart.setText(body);
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			if (attachment != null)
			{

				for (int i = 0; i < attachment.length; i++)
				{
					messageBodyPart = new MimeBodyPart();
					logger.debug(" Atachement " + attachment[i]);
					File f = new File(attachment[i]);
					DataSource source = new FileDataSource(f);
					messageBodyPart.setDataHandler(new DataHandler(source));
					messageBodyPart.setFileName(f.getName());
					multipart.addBodyPart(messageBodyPart);
				}
			}

			// Put parts in message
			message.setContent(multipart);

			// Send message
			message.saveChanges();// implicit with send()

			// send message
//			transport.sendMessage(message, message.getAllRecipients());
//			transport.close();
			Transport.send(message);

		}
		catch (Exception e)
		{
			logger.error("emailUtils.java: sendMail() EXCEPTION " + e.getMessage());
			e.printStackTrace();
			sentOK = false;
		}

		if (to != null)
			logger.debug("Sent mail to " + to.toString() + " and it was " + sentOK);
		if (cc != null)
			logger.debug("Sent mail to " + cc.toString() + " and it was " + sentOK);
		if (bcc != null)
			logger.debug("Sent mail to " + bcc.toString() + " and it was " + sentOK);

		return sentOK;
	}

	public static void main(String[] args)
	{

		final String username = "smtp.mandrillapp.com";
		final String password = "lRpyt4EHlKRJyEcJEwXm0A";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.host", "smtp.mandrillapp.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator()
				{
					@Override
					protected PasswordAuthentication getPasswordAuthentication()
					{
						return new PasswordAuthentication(username, password);
					}
				});

		try
		{

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("from-email@demo.webdirector.net.au"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse("eric@corporateinteractive.com"));
			message.setSubject("Testing Subject");
			message.setText("Dear Mail Crawler,"
					+ "\n\n No spam to my email, please!");

			Transport.send(message);

			System.out.println("Done");

		}
		catch (MessagingException e)
		{
			throw new RuntimeException(e);
		}
	}

	public static boolean isValidEmailAddress(String email)
	{
		boolean result = true;
		try
		{
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		}
		catch (AddressException ex)
		{
			result = false;
		}
		return result;
	}
}
