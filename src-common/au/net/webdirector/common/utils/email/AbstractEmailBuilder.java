package au.net.webdirector.common.utils.email;

import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import au.net.webdirector.common.Defaults;
import freemarker.template.Configuration;
import freemarker.template.Template;

public abstract class AbstractEmailBuilder
{
	private static Logger logger = Logger.getLogger(AbstractEmailBuilder.class);

	protected Map<String, Object> universalFields = new HashMap<String, Object>();

	protected String req_host;
	protected String req_port;
	protected String req_ctx;
	protected String req_serverBase;

	public AbstractEmailBuilder()
	{
		configureAsDefault();
	}

	public AbstractEmailBuilder(HttpServletRequest request)
	{
		if (request != null)
		{
			req_host = request.getServerName();
			req_port = (request.getServerPort() == 80 || request.getServerPort() == 443) ? "" : ":" + request.getServerPort();
			req_ctx = request.getContextPath();
			req_serverBase = request.getScheme() + "://" + req_host + req_port + req_ctx;

			universalFields.put("serverHost", req_host);
			universalFields.put("serverPort", req_port);
			universalFields.put("serverCtx", req_ctx);
			universalFields.put("serverBase", req_serverBase);
		}
		else
		{
			configureAsDefault();
		}
	}

	private void configureAsDefault()
	{
		req_host = Defaults.getInstance().getWebSiteURL().substring(Defaults.getInstance().getWebSiteURL().indexOf("://") + 3).trim();
		if (req_host.endsWith("/"))
		{
			req_host = req_host.substring(0, req_host.length() - 1);
		}
		req_port = "";
		req_ctx = "";
		req_serverBase = Defaults.getInstance().getWebSiteURL();

		universalFields.put("serverHost", req_host);
		universalFields.put("serverPort", req_port);
		universalFields.put("serverCtx", req_ctx);
		universalFields.put("serverBase", req_serverBase);
	}

	public AbstractEmailBuilder addModule(String moduleName, Map<String, ? extends Object> map)
	{
		if (map != null)
		{
			Map<String, Object> fields = new HashMap<String, Object>();
			moduleName = moduleName.trim().toUpperCase();
			for (String key : map.keySet())
			{
				fields.put(String.format("%s_%s", moduleName, key), map.get(key));
			}
			universalFields = mergedFields(fields);
		}
		else
		{
			logger.warn("EMPTY MODULE RECIEVED... Name:" + moduleName);
		}
		return this;
	}

	public PendingEmail prepareEmail(String templateName, String subject, Map<String, Object> f) throws TemplateNotFoundException
	{
		Reader templateReader = getTemplateReader(templateName);

		String emailBody = generateEmail(templateReader, subject, f);

		return new PendingEmail(subject, emailBody, req_host);
	}

	protected Object getModuleValue(String moduleName, String attribute)
	{
		String key = String.format("%s_%s", moduleName.trim().toUpperCase(), attribute);
		return universalFields.get(key);
	}

	protected String generateEmail(Reader templateReader, String subject, Map<String, Object> additionalFields)
	{
		// clone the current fields just for this email
		Map<String, Object> currentFields = new HashMap<String, Object>();
		currentFields.put("emailSubject", subject);

		Map<String, Object> finalFields = mergedFields(additionalFields, currentFields);
		Map<String, Object> excapedFields = urlEscapeFields(finalFields);
		try
		{
			Template emailTemplate = new Template("email template", templateReader, new Configuration());
			return FreeMarkerTemplateUtils.processTemplateIntoString(emailTemplate, excapedFields);
		}
		catch (Exception e)
		{
			handleException("Exception in generateEmail", e);
		}
		return null;
	}

	protected void handleException(String additionalMsg, Exception e)
	{
		logger.error("Error:" + additionalMsg, e);
	}

	protected Map<String, Object> urlEscapeFields(Map<String, Object> fields)
	{
		Map<String, Object> f = new HashMap<String, Object>();
		for (String s : fields.keySet())
		{
			f.put(s, fields.get(s));
			if (fields.get(s) instanceof String)
			{
				String new_name = (String) fields.get(s);
				try
				{
					f.put("urlEncoded_" + s, URLEncoder.encode(new_name, "UTF-8"));
				}
				catch (UnsupportedEncodingException e)
				{
					logger.error("Unable to internally encode.", e);
				}
			}
		}
		return f;
	}

	protected Map<String, Object> mergedFields(Map<String, Object>... additionalFieldsArr)
	{
		Map<String, Object> mergedFields = new HashMap<String, Object>(universalFields);

		for (Map<String, Object> additionalFields : additionalFieldsArr)
		{
			if (additionalFields != null)
			{
				for (String key : additionalFields.keySet())
				{
					if (mergedFields.containsKey(key))
					{
						logger.warn(String.format("Replacing existing key: %s, Original value: %s, New value: %s",
								key,
								mergedFields.get(key),
								additionalFields.get(key)));
					}
					mergedFields.put(key, additionalFields.get(key));
				}
			}
		}

		return mergedFields;
	}

	protected String getFullUniversalFieldKey(String module, String field)
	{
		Object o = universalFields.get(module.toUpperCase() + "_" + field);
		if (o == null)
		{
			return null;
		}
		return o.toString();
	}

	protected abstract Reader getTemplateReader(String templateName) throws TemplateNotFoundException;

	public class TemplateNotFoundException extends Exception
	{

		public TemplateNotFoundException()
		{
			super();
			// TODO Auto-generated constructor stub
		}

		public TemplateNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
		{
			super(message, cause, enableSuppression, writableStackTrace);
			// TODO Auto-generated constructor stub
		}

		public TemplateNotFoundException(String message, Throwable cause)
		{
			super(message, cause);
			// TODO Auto-generated constructor stub
		}

		public TemplateNotFoundException(String message)
		{
			super(message);
			// TODO Auto-generated constructor stub
		}

		public TemplateNotFoundException(Throwable cause)
		{
			super(cause);
			// TODO Auto-generated constructor stub
		}

	}
}
