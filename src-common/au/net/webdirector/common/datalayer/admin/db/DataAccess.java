package au.net.webdirector.common.datalayer.admin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletContext;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.web.context.support.XmlWebApplicationContext;

/**
 * @author Daniel
 */
public class DataAccess
{

	private static DataAccess[] singleton = null;

	/**
	 * Make class accessible only by getInstance
	 */
	private DataAccess()
	{

	}

	public static DataAccess getInstance()
	{
		return getInstance(false);
	}

	public static DataAccess getInstance(boolean useCommDB)
	{
		// TODO: The listener
		// (org.springframework.web.context.ContextLoaderListener) in web.xml
		// always starts before startupUtility, therefore this method will be
		// invoked when singleton is still null. Need to resolve this problem.
		if (singleton == null)
		{
			return null;
		}
		if (useCommDB)
		{
			return singleton[1];
		}
		return singleton[0];
	}

	public static void setUpInstance(ServletContext servletContext)
	{
		if (singleton == null)
		{
			XmlWebApplicationContext appContext = new XmlWebApplicationContext();
			appContext.setServletContext(servletContext);
			appContext.refresh();
			singleton = new DataAccess[2];
			singleton[0] = (DataAccess) appContext.getBean("WebDirectorDataAccess");
			singleton[1] = (DataAccess) appContext.getBean("WebDirectorDataAccess");
			singleton[0].jdbcTemplate = singleton[0].webdirectorJdbcTemplate;
			singleton[1].jdbcTemplate = singleton[1].commonJdbcTemplate;

		}
	}

	private JdbcTemplate jdbc;

	private JdbcTemplate jdbcTemplate;

	private JdbcTemplate webdirectorJdbcTemplate;

	/**
	 * Locations module (and maybe others) uses a common db defined in
	 * server.xml
	 */
	private JdbcTemplate commonJdbcTemplate;

	/**
	 * Sets spring dataSource from spring container
	 * 
	 * @param dataSource
	 */

	public void setCommonJdbcTemplate(DataSource dataSource)
	{
		this.commonJdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * Default instance uses webdirector datasource
	 * 
	 * @param dataSource
	 */
	public void setWebdirectorJdbcTemplate(DataSource dataSource)
	{
		this.webdirectorJdbcTemplate = new JdbcTemplate(dataSource);
		this.jdbcTemplate = this.webdirectorJdbcTemplate;
		this.jdbc = new JdbcTemplate(dataSource);
	}

	/**
	 * This method should not be used any more. It will throw an exception
	 * anyway.
	 * Use DataAccess.getInstance(true|false) instead.
	 * 
	 * @param useCommonDB
	 */
	@Deprecated
	public void useCommonDb(boolean useCommonDB)
	{
		throw new RuntimeException("Use DataAccess.getInstance(true|false) instead");
//		if (useCommonDB) {
//			this.jdbcTemplate = commonJdbcTemplate;
//		} else {
//			this.jdbcTemplate = webdirectorJdbcTemplate;
//		}
	}

	/**
	 * DO NOT USE THIS. IT IS PROVIDED PURELY FOR BACKWARDS COMPATIBILITY FOR
	 * THE DBACCESS CLASS. IF YOU USE THIS, A MAD AXEMAN WILL COME AND HUNT YOU
	 * DOWN, STAB YOU, AND SCATTER YOUR ENTRAILS Comment by dan - partially
	 * drunk
	 * 
	 * Only works in webdirector data access, not common
	 */
	public Connection getConnection()
	{
		try
		{
			return jdbc.getDataSource().getConnection();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public DataSource getDataSource()
	{
		try
		{
			return jdbc.getDataSource();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * 
	 * Given a table name, this method returns a vector of String[]. Each array
	 * represents a column in the table. Each array holds Column name in [0],
	 * ColumnType in [1], and ColumnSize in [2]
	 * 
	 * @param tableName
	 * @return
	 */
	public Vector<String[]> getColsMetaData(String tableName)
	{
		String sql = "SELECT * from " + tableName + " limit 1";
		JdbcTemplate jdbc = (JdbcTemplate) jdbcTemplate;
		Vector<String[]> metaData = jdbc.query(sql, new ResultSetExtractor<Vector<String[]>>()
		{

			public Vector<String[]> extractData(ResultSet resultSet) throws SQLException
			{
				ResultSetMetaData rsmd = resultSet.getMetaData();
				Vector<String[]> cols = new Vector<String[]>();
				for (int i = 1; i <= rsmd.getColumnCount(); i++)
				{
					String[] dbStruct = new String[3];

					dbStruct[0] = rsmd.getColumnName(i);
					dbStruct[1] = rsmd.getColumnTypeName(i);
					dbStruct[2] = String.valueOf(rsmd.getColumnDisplaySize(i));
					if (StringUtils.endsWithIgnoreCase("decimal", dbStruct[1]) || StringUtils.endsWithIgnoreCase("numeric", dbStruct[1]))
					{
						dbStruct[2] = rsmd.getPrecision(i) + "," + rsmd.getScale(i);
					}

					cols.addElement(dbStruct);
				}
				return cols;
			}

		});

		return metaData;
	}

	/**
	 * Issue a single SQL update operation (such as an insert, update or delete
	 * statement) via
	 * a prepared statement, binding the given arguments.
	 * 
	 * @param query
	 * @return the number of rows the update query effected.
	 */
	public int updateData(String query, Object[] args)
	{
		return jdbcTemplate.update(query, args);
	}

	/**
	 * Runs the query given (which must be an update query) and returns the
	 * number of rows effected by it.
	 * 
	 * @param query
	 * @return the number of rows the update query effected.
	 */
	public int updateDataMap(String query, Map<String, Object> args)
	{
		return jdbcTemplate.update(query, args);
	}

	/**
	 * reutrns the id of the record inserted
	 * 
	 * @return
	 */
	public int insertData(final String sql, final Object[] args)
	{
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator()
		{
			public PreparedStatement createPreparedStatement(Connection conn) throws SQLException
			{
				PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				for (int i = 1; i <= args.length; i++)
				{
					statement.setString(i, args[i - 1].toString());
				}
				return statement;
			}
		}, keyHolder);
		return keyHolder.getKey().intValue();
	}

	/**
	 * 
	 * Main Select Statement
	 * 
	 * @param query
	 * @param args
	 * @param rowMapper
	 * @return
	 */
	public List select(String query, Object[] args, RowMapper rowMapper)
	{
		return jdbcTemplate.query(query, rowMapper, args);
	}

	/**
	 * 
	 * Main Select Statement
	 * 
	 * @param query
	 * @param rowMapper
	 * @return
	 */
	public List select(String query, RowMapper rowMapper)
	{
		return jdbcTemplate.query(query, rowMapper);
	}

	/**
	 * Use this method if require the generated id.
	 * Issue a single SQL update operation (such as an insert, update or delete
	 * statement) via
	 * a prepared statement, binding the given arguments.
	 * 
	 * @param query
	 * @return the number of rows the update query effected.
	 */
	public int insertDataQuery(String query, Object[] args)
	{
		jdbcTemplate.update(query, args);
		return jdbcTemplate.queryForObject("select last_insert_id()", Integer.class);
	}


}
