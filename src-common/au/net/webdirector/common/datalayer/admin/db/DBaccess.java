package au.net.webdirector.common.datalayer.admin.db;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.mapper.StringArrayMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;

public class DBaccess implements Serializable
{
	static Logger logger = Logger.getLogger(DBaccess.class);
	private static final long serialVersionUID = 1L;

	protected static boolean keyUpdateLock = false;

	protected static String keyUpdateLockquery = "";

	protected Defaults d = Defaults.getInstance();

	private DataAccess dataAccess = DataAccess.getInstance();

	private boolean useCommonDB = false;

	public DBaccess()
	{
		this.dataAccess = DataAccess.getInstance();
	}

	public DBaccess(boolean useCommonDBi)
	{
		this.useCommonDB = useCommonDBi;
		this.dataAccess = DataAccess.getInstance(useCommonDBi);
	}

	public Vector getColsMetaData(String table)
	{
		table = DatabaseValidation.encodeParam(table);
		return this.dataAccess.getColsMetaData(table);
	}

	protected Vector getMetaData(ResultSet rs)
	{
		Vector tmpCols = new Vector();
		try
		{
			ResultSetMetaData rsmd = rs.getMetaData();
			int numCols = rsmd.getColumnCount();

			for (int i = 1; i <= numCols; i++)
			{
				String[] dbStruct = new String[3];

				dbStruct[0] = rsmd.getColumnName(i);
				dbStruct[1] = rsmd.getColumnTypeName(i);
				dbStruct[2] = String.valueOf(rsmd.getColumnDisplaySize(i));

				tmpCols.addElement(dbStruct);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return tmpCols;
	}

	public Connection getDBConnection()
	{
		return this.dataAccess.getConnection();
	}

	public int updateData(String query)
	{
		logger.info("q " + query);
		if (null == dataAccess)
			dataAccess = DataAccess.getInstance();
		return this.dataAccess.updateData(query, new Object[0]);
	}

	public int insertData(Hashtable record, String keyColumn, String tableName)
	{

		List<String> keys = new ArrayList<String>(record.keySet());
		List<String> values = new ArrayList<String>();

		StringBuffer insertQuery = new StringBuffer("INSERT INTO ");
		insertQuery.append(tableName);
		insertQuery.append(" ( ").append(StringUtils.join(keys, ",")).append(")");

		StringBuilder value = new StringBuilder("");
		for (String key : keys)
		{
			String value1 = sq(record.get(key).toString());
			if (!value1.equalsIgnoreCase("NULL") && !value1.equalsIgnoreCase("NOW()"))
			{
				value.append("?, ");
			}
			else
			{
				value.append(value1 + ", ");
				record.remove(key);
			}
		}
		insertQuery.append(" VALUES ( ").append(value.substring(0, value.length() - 2)).append(")");

		//int id = insertData(insertQuery.toString(), keyColumn, tableName);
		int id = this.dataAccess.insertDataQuery(insertQuery.toString(), record.values().toArray());
		logger.debug("INSERTED key " + id + " record using:" + insertQuery.toString());
		return id;
	}

	/**
	 * 
	 * @param sourceStr
	 * @param matchString
	 * @param encodedString
	 * @return
	 */
	public final String encode(String sourceStr, String matchString, String encodedString)
	{
		if (sourceStr == null)
		{
			return "";
		}
		return StringUtils.replace(sourceStr, matchString, encodedString);
	}

	/**
	 * 
	 * @param sourceStr
	 * @return
	 */
	public final String encodeAmpersand(String sourceStr)
	{
		String token = null;
		StringBuffer returnString = new StringBuffer();

		String REGEX = "&";

		logger.debug("source " + sourceStr);
		Pattern p = Pattern.compile(REGEX);
		String[] items = p.split(sourceStr);
		logger.debug("items " + items.length);
		for (int i = 0; (items.length > 1) && (i < items.length - 1); i++)
		{
			logger.debug(">> " + items[i]);

			if (items[(i + 1)].matches("#...;.*"))
			{
				returnString.append(items[i]);
				items[(i + 1)] = ("&" + items[(i + 1)]);
			}
			else
			{
				returnString.append(items[i] + "&amp;");
			}
			logger.debug("ret " + returnString);
		}
		if (items.length < 2)
			returnString.append(sourceStr);
		else
		{
			returnString.append(items[(items.length - 1)]);
		}
		logger.debug("final ret " + returnString);

		return returnString.toString();
	}

	/**
	 * Replacing single quote ' with double quote ''
	 * 
	 * @param str
	 * @return
	 */
	public final String sq(String str)
	{
		String SQ = "'";
		if (this.d.getDEBUG())
		{
			logger.debug(str);
		}

		String token = null;
		StringBuffer returnString = new StringBuffer();

		if (str != null)
		{
			StringTokenizer st = new StringTokenizer(str, SQ, true);
			while (st.hasMoreTokens())
			{
				token = st.nextToken();
				if (token.equals(SQ))
				{
					returnString.append(token);
					returnString.append(SQ);
				}
				else
				{
					returnString.append(token);
				}
			}
		}

		return returnString.toString();
	}

	/**
	 * 
	 * @param str
	 * @param meta
	 * @param colName
	 * @return
	 */
	public String parseAndEncode(String str, Vector meta, String colName)
	{
		String encodedStr = str;


		for (int i = 0; i < meta.size(); i++)
		{
			String[] dbStruct = (String[]) meta.elementAt(i);

			if (!dbStruct[0].equals(colName))
			{
				continue;
			}
			int colSize = Integer.parseInt(dbStruct[2]);

			if ((colSize > 255) && (colSize != 1001))
			{
				continue;
			}
			encodedStr = StringEscapeUtils.escapeXml(encodedStr);
			encodedStr = encode(encodedStr, "\\", "\\\\");
			encodedStr = encode(encodedStr, "&apos;", "&#39;");
		}

		return encodedStr;
	}

	/**
	 * 
	 * @param record
	 * @param keyColumn
	 * @param tableName
	 * @return
	 */
	public int updateData(Hashtable record, String keyColumn, String tableName)
	{
		logger.debug(">>> DBaccess:updateData( column: " + keyColumn + ", table: " + tableName);

		StringBuffer updateQuery = new StringBuffer("UPDATE ");
		updateQuery.append(tableName);
		updateQuery.append(" SET ");

		List<String> keys = new ArrayList<String>(record.keySet());

		for (String key : keys)
		{
			updateQuery.append(key);
			updateQuery.append(" = ");
			String value1 = sq(record.get(key).toString());
			// Removing quote ' while inserting null or current time for date values 
			if (!value1.equalsIgnoreCase("NULL") && !value1.equalsIgnoreCase("NOW()"))
			{
				updateQuery.append("?, ");
			}
			else
			{
				updateQuery.append(value1 + ", ");
				record.remove(key);
			}
		}
		// removing the trailing comma
		updateQuery = updateQuery.delete(updateQuery.length() - 2, updateQuery.length());

		// Where clause
		updateQuery.append(" WHERE ");
		updateQuery.append(keyColumn);
		updateQuery.append(" = ");
		updateQuery.append("'");
		updateQuery.append(record.get(keyColumn).toString());
		updateQuery.append("' ");
		// end

		//int count = updateData(updateQuery.toString());
		int count = this.dataAccess.updateData(updateQuery.toString(), record.values().toArray());
		logger.debug("UPDATED " + count + " record(s) using:" + updateQuery.toString());
		logger.debug(">>>DBaccess:updateData()");
		return count;
	}

	/**
	 * Depricated method , use insertQuery version instead.
	 * 
	 * @param query
	 * @param keyColumn
	 * @param tableName
	 * @return
	 */
	@Deprecated
	public int insertData(String query, String keyColumn, String tableName)
	{
		return this.dataAccess.insertData(query, new Object[0]);
	}

	/**
	 * Depricated method , use selectQuery version instead.
	 * 
	 * @param query
	 * @return
	 */
	@Deprecated
	public Vector select(String query)
	{
		return select(query, false, false);
	}

	/**
	 * Depricated method , use selectQuery version instead.
	 * 
	 * @param query
	 * @param getMetaData
	 * @param useCache
	 * @return
	 */
	@Deprecated
	public Vector<String> select(String query, boolean getMetaData, boolean useCache)
	{

		if (null == dataAccess)
			dataAccess = DataAccess.getInstance();

		List results = this.dataAccess.select(query, new Object[0], new StringMapper());

		return new Vector(results);
	}

	/**
	 * Depricated method , use selectQuery version instead.
	 * 
	 * @param query
	 * @param cols
	 * @return
	 */
	@Deprecated
	public Vector select(String query, String[] cols)
	{
		return select(query, cols, false, false);
	}

	/**
	 * Depricated method , use selectQuery version instead.
	 * 
	 * @param query
	 * @param cols
	 * @param getMetaData
	 * @param useCache
	 * @return
	 */
	@Deprecated
	public Vector<String[]> select(String query, String[] cols, boolean getMetaData, boolean useCache)
	{
		List results = this.dataAccess.select(query, new Object[0], new RowMapper()
		{
			public String[] mapRow(ResultSet rs, int arg1) throws SQLException
			{
				if (rs != null)
				{
					String[] data = new String[rs.getMetaData().getColumnCount()];
					for (int i = 0; i < rs.getMetaData().getColumnCount(); i++)
					{
						Object x = rs.getString(i + 1);
						if ((String) x == null)
						{
							data[i] = "";
						}
						else
						{
							String s = (String) x;
							data[i] = s.trim();
						}
					}
					return data;
				}
				return new String[0];
			}
		});
		return new Vector(results);
	}

	/**
	 * 
	 * @param query
	 * @return
	 */
	public Vector<String[]> selectQuery(String query)
	{
		return selectQuery(query, new String[] {});
	}

	/**
	 * 
	 * @param query
	 * @param parameter
	 * @return
	 */
	public Vector<String[]> selectQuery(String query, String[] parameter)
	{
		if (null == dataAccess)
			dataAccess = DataAccess.getInstance();
		List results = this.dataAccess.select(query, parameter, new StringArrayMapper());
		return new Vector(results);
	}

	/**
	 * 
	 * @param query
	 * @return
	 */
	public Vector<String> selectQuerySingleCol(String query)
	{
		return selectQuerySingleCol(query, new String[] {});
	}

	/**
	 * 
	 * @param query
	 * @param parameter
	 * @return
	 */
	public Vector<String> selectQuerySingleCol(String query, String[] parameter)
	{
		if (null == dataAccess)
			dataAccess = DataAccess.getInstance();

		List results = this.dataAccess.select(query, parameter, new StringMapper());
		return new Vector(results);
	}

	/**
	 * 
	 * @param query
	 * @param parameter
	 * @return
	 */
	public Vector<String[]> selectQuery(String query, Object[] parameter)
	{
		if (null == dataAccess)
			dataAccess = DataAccess.getInstance();
		List results = this.dataAccess.select(query, parameter, new StringArrayMapper());
		return new Vector(results);
	}

	/**
	 * 
	 * @param query
	 * @param parameter
	 * @return
	 */
	public Vector<String> selectQuerySingleCol(String query, Object[] parameter)
	{
		if (null == dataAccess)
			dataAccess = DataAccess.getInstance();
		List results = this.dataAccess.select(query, parameter, new StringMapper());
		return new Vector(results);
	}

	public int insertQuery(String query, Object[] parameters)
	{
		return this.dataAccess.insertData(query, parameters);
	}

	public int updateData(String query, Object[] parameters)
	{
		logger.info("q " + query);
		if (null == dataAccess)
			dataAccess = DataAccess.getInstance();
		return this.dataAccess.updateData(query, parameters);
	}


	public boolean isUseCommonDB()
	{
		return this.useCommonDB;
	}
}