package au.net.webdirector.common.datalayer.client.module;

import java.util.List;
import java.util.Map;

public class ModuleAttributes
{
	private String internal;
	private String internalOrig;
	private String external;
	private String tab;
	private boolean mandatory;
	private boolean readonly;
	private boolean nochange;
	private int size;
	private List<Map<String, String>> options;

	public ModuleAttributes(
			String internal,
			String internalOrig,
			String external,
			String tab,
			boolean mandatory,
			boolean readonly,
			boolean nochange)
	{
		this.internal = internal;
		this.internalOrig = internalOrig;
		this.external = external;
		this.tab = tab;
		this.mandatory = mandatory;
		this.readonly = readonly;
		this.nochange = nochange;
	}

	public String getInternal()
	{
		return internal;
	}

	public void setInternal(String internal)
	{
		this.internal = internal;
	}

	public String getInternalOrig()
	{
		return internalOrig;
	}

	public void setInternalOrig(String internalOrig)
	{
		this.internalOrig = internalOrig;
	}

	public String getExternal()
	{
		return external;
	}

	public void setExternal(String external)
	{
		this.external = external;
	}

	public String getTab()
	{
		return tab;
	}

	public void setTab(String tab)
	{
		this.tab = tab;
	}

	public boolean getMandatory()
	{
		return mandatory;
	}

	public void setMandatory(boolean mandatory)
	{
		this.mandatory = mandatory;
	}

	public int getSize()
	{
		return size;
	}

	public void setSize(int size)
	{
		this.size = size;
	}

	public List<Map<String, String>> getOptions()
	{
		return options;
	}

	public void setOptions(List<Map<String, String>> options)
	{
		this.options = options;
	}

	public boolean getReadonly()
	{
		return readonly;
	}

	public void setReadonly(boolean readonly)
	{
		this.readonly = readonly;
	}

	public boolean isNochange()
	{
		return nochange;
	}

	public void setNochange(boolean nochange)
	{
		this.nochange = nochange;
	}

}
