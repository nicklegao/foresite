package au.net.webdirector.common.datalayer.client;

public enum EntityType
{
	ELEMENT, CATEGORY
}
