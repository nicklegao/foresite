/**
 * @author Nick Yiming Gao
 * @date 26/11/2015
 */
package au.net.webdirector.common.datalayer.client.conf;

/**
 * 
 */

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.conf.Condition;
import au.net.webdirector.common.datalayer.base.db.conf.OrderBy;

public class ModuleAccessConfig
{

	private static Logger logger = Logger.getLogger(ModuleAccessConfig.class);

	protected Condition condition = Condition.emptyInst();

	protected List<OrderBy> orders = new ArrayList<OrderBy>();

	public Condition getCondition()
	{
		Condition newOne = Condition.emptyInst();
		return newOne.and(this.condition);
	}

	public void setCondition(Condition condition)
	{
		this.condition = condition;
	}

	public List<OrderBy> getOrders()
	{
		return new ArrayList<OrderBy>(this.orders);
	}

	public void setOrders(List<OrderBy> orders)
	{
		this.orders = orders;
	}

	public Condition createCondition(DataMap data)
	{
		return getCondition().and(Condition.instance(data));
	}

	public ModuleAccessConfig merge(ModuleAccessConfig... another)
	{
		if (another == null)
		{
			return this;
		}
		for (ModuleAccessConfig conf : another)
		{
			if (conf == null)
			{
				continue;
			}
			this.condition.and(conf.getCondition());
			this.orders.addAll(conf.getOrders());
		}
		return this;
	}

	public static ModuleAccessConfig emptyInstance()
	{
		return new ModuleAccessConfig();
	}

	public static ModuleAccessConfig logicalLive()
	{
		ModuleAccessConfig config = new ModuleAccessConfig();
		Condition newOne = new Condition("Live=1").and(new Condition("live_date is null").or(new Condition("live_date < now()")), new Condition("expire_date is null").or(new Condition("expire_date > now()")));
		config.setCondition(newOne);
		return config;
	}

	public static ModuleAccessConfig orderby(String field, String option)
	{
		ModuleAccessConfig config = new ModuleAccessConfig();
		List<OrderBy> orders = new ArrayList<OrderBy>();
		orders.add(StringUtils.equalsIgnoreCase(option, OrderBy.ASC) ? OrderBy.asc(field) : OrderBy.desc(field));
		config.setOrders(orders);
		return config;
	}

	public static ModuleAccessConfig orderByDisplay()
	{
		return orderby("display_order", OrderBy.ASC);
	}
}
