package au.net.webdirector.common.datalayer.client;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.admin.bulkEdit.domain.datatable.Column;
import au.net.webdirector.admin.modules.privileges.PrivilegeUtils;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.ConnectionPoolManager;
import au.net.webdirector.common.datalayer.base.db.DataHelper;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.QueryBuffer;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.AbstractFile;
import au.net.webdirector.common.datalayer.base.db.entity.Password;
import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.client.module.ModuleAttributes;
import au.net.webdirector.common.datalayer.lucene.LuceneHelper;
import au.net.webdirector.common.datalayer.util.text.DataFormat;
import au.net.webdirector.common.datalayer.util.text.DefaultObjectFormat;
import au.net.webdirector.common.datalayer.util.text.DefaultPasswordFormat;
import au.net.webdirector.common.utils.module.DropDownBuilder;

public class ModuleHelper extends DataHelper
{
	private static final Logger logger = Logger.getLogger(ModuleHelper.class);

	public static ModuleHelper getInstance()
	{
		return getInstance(TransactionManager.getInstance(TransactionDataAccess.getInstance()));
	}

	public static ModuleHelper getInstance(TransactionManager txManager)
	{
		return new ModuleHelper(txManager);
	}

	public static void putSystemColumn(ModuleData data)
	{
		data.put("status_id", "1");
		data.put("lock_id", "1");
		data.put("version", "1");
		data.put("language_id", "1");
	}

	public static String readString(InputStream input)
	{
		try
		{
			return IOUtils.toString(input);
		}
		catch (IOException e)
		{
			return "Error reading: " + e.getClass().getCanonicalName() + " (" + e.getMessage() + ")";
		}
	}

	public static byte[] readBytes(InputStream input)
	{
		try
		{
			return IOUtils.toByteArray(input);
		}
		catch (IOException e)
		{
			return null;
		}
	}

	public static String getStoreFileUrl(String path)
	{
		return "/" + Defaults.getInstance().getStoreContext() + path;
	}

	public static File getStoredFile(String path)
	{
		return new File(Defaults.getInstance().getStoreDir(), path);
	}

	public static File getStoredFolder(AbstractFile storeFile)
	{
		if (storeFile.isCategory())
		{
			return getStoredFile("/" + StringUtils.join(new String[] { storeFile.getModule(), "Categories", storeFile.getId(), storeFile.getField() }, "/"));
		}
		return getStoredFile("/" + StringUtils.join(new String[] { storeFile.getModule(), storeFile.getId(), storeFile.getField() }, "/"));
	}

	public static InputStream openInputStream(File file) throws IOException
	{
		return StoreFileAccessFactory.getInstance().openInputStream(file);
	}

	public static String readStringFromFile(ModuleData data, String key) throws IOException
	{
		StoreFile storeFile = data.getStoreFile(key);
		if (storeFile == null)
		{
			return "";
		}
		InputStream in = null;
		try
		{
			in = openInputStream(storeFile.getStoredFile());
			return readString(in);
		}
		finally
		{
			IOUtils.closeQuietly(in);
		}
	}

	public static String readString(File file) throws IOException
	{
		InputStream in = null;
		try
		{
			in = openInputStream(file);
			return readString(in);
		}
		finally
		{
			IOUtils.closeQuietly(in);
		}
	}

	TransactionManager txManager;

	public ModuleHelper(TransactionManager txManager)
	{
		this.txManager = txManager;
	}

	@Override
	protected Class[] getSupportedClasses()
	{
		// TODO: supplement the rest
		Class[] superSupports = super.getSupportedClasses();
		Class[] iSupports = new Class[] { Password.class };
		return (Class[]) ArrayUtils.addAll(superSupports, iSupports);
	}

	@Override
	public DataFormat getDefaultDataFormat(Object object)
	{
		if (support(object, super.getSupportedClasses()))
		{
			return super.getDefaultDataFormat(object);
		}
		if (object instanceof Password)
		{
			return new DefaultPasswordFormat();
		}
		return new DefaultObjectFormat();
	}

	private TransactionDataAccess getDataAccess()
	{
		return txManager.getDataAccess();
	}

	public List<Map<String, String>> getModules() throws SQLException
	{
		String sql = "SELECT internal_module_name, client_module_name FROM sys_moduleconfig s;";
		List<Map<String, String>> modules = DataAccess.getInstance().select(sql, new HashtableMapper());
		return modules;
	}

	public String[] getElementSharedFileColumns(String module) throws SQLException
	{
		String sql = "select label_internalname from labels_" + module + " where label_internalname like '%ATTRSHAREDFILE%'";
		List<String> modules = getDataAccess().selectStrings(sql);
		return modules.toArray(new String[modules.size()]);
	}

	public String[] getCategorySharedFileColumns(String module) throws SQLException
	{
		String sql = "select label_internalname from categorylabels_" + module + " where label_internalname like '%ATTRSHAREDFILE%'";
		List<String> modules = getDataAccess().selectStrings(sql);
		return modules.toArray(new String[modules.size()]);
	}

	public String[] getElementColumns(String module) throws SQLException
	{
		String[] fixed = new String[] { "display_order", "create_date", "last_update_date", "live_date", "expire_date", "live", "category_id", "element_id" };
		String sql = "select label_internalname from labels_" + module + " order by label_onoff desc, display_order";
		List<String> labels = getDataAccess().selectStrings(sql);
		new ArrayList<String>().toArray(new String[] {});
		return (String[]) ArrayUtils.addAll(fixed, labels.toArray());
	}

	public String[] getCategoryColumns(String module) throws SQLException
	{
		String[] fixed = new String[] { "category_id", "category_parentid", "folderlevel", "display_order", "create_date", "last_update_date", "live_date", "expire_date", "live" };
		String sql = "select label_internalname from categorylabels_" + module + " order by label_onoff desc, display_order";
		List<String> labels = getDataAccess().selectStrings(sql);
		return (String[]) ArrayUtils.addAll(fixed, labels.toArray());
	}

	public String[] getColumns(EntityType type, String module) throws SQLException
	{
		if (EntityType.CATEGORY.equals(type))
		{
			return getCategoryColumns(module);
		}
		if (EntityType.ELEMENT.equals(type))
		{
			return getCategoryColumns(module);
		}
		throw new InvalidParameterException("Entity type:[" + type + "] not supported!");
	}

	public List<ModuleAttributes> getElementColumnsInfo(String module) throws SQLException
	{
		return getElementColumnsInfo(module, true);
	}

	public List<ModuleAttributes> getElementColumnsInfo(String module, boolean showOnOnly) throws SQLException
	{
		List<ModuleAttributes> columnInformation = new LinkedList<ModuleAttributes>();
		columnInformation.add(new ModuleAttributes("element_id", "Element_id", "ID", "", true, true, true));
		columnInformation.add(new ModuleAttributes("live", "Live", "Live", "", false, false, false));
		columnInformation.add(new ModuleAttributes("live_date", "Live_date", "Live date", "", false, false, false));
		columnInformation.add(new ModuleAttributes("expire_date", "Expire_date", "Expire date", "", false, false, false));

		String sql = "select label_internalname as internal,"
				+ " label_externalname as external,"
				+ " label_tabname as tab,"
				+ " Label_Mandatory as mandatory,"
				+ " label_readonly as readonly,"
				+ " label_nochange as nochange"
				+ " from labels_" + module + " where 1 = 1 ";
		if (showOnOnly)
		{
			sql += " and label_onoff = '1'";
		}
		sql += " order by display_order";
		List<DataMap> otherColumns = getDataAccess().selectMaps(sql);
		for (DataMap dataMap : otherColumns)
		{
			columnInformation.add(new ModuleAttributes(
					dataMap.getString("internal").toLowerCase(),
					dataMap.getString("internal"),
					dataMap.getString("external"),
					dataMap.getString("tab"),
					dataMap.getBoolean("mandatory"),
					dataMap.getBoolean("readonly"),
					dataMap.getBoolean("nochange")));
		}

		generateColumnMetadata(module, "elements", columnInformation);

		return columnInformation;
	}

	private Map<String, ModuleAttributes> generateColumnMetadata(
			String module,
			String tableType,
			List<ModuleAttributes> columnInformation) throws SQLException
	{
		final Map<String, ModuleAttributes> columns = new HashMap<String, ModuleAttributes>();
		for (ModuleAttributes ma : columnInformation)
		{
			columns.put(ma.getInternal(), ma);

			//generate the dropdown values...
			if (ma.getInternal().startsWith("attrdrop_")
					|| ma.getInternal().startsWith("attrmulti_"))
			{
				Iterator<Entry<String, String>> entries = new DropDownBuilder().getDropDownValuesWithoutId(module, ma.getInternal(), tableType).entrySet().iterator();
				List<Pair> pairs = new ArrayList<Pair>();
				while (entries.hasNext())
				{
					Map.Entry<String, String> entry = entries.next();
					pairs.add(new Pair(entry));
				}
				Collections.sort(pairs);
				List<Map<String, String>> options = new ArrayList<Map<String, String>>();

				for (int i = 0; i < pairs.size(); i++)
				{
					Map.Entry<String, String> entry = pairs.get(i).getEntry();
					Map<String, String> option = new HashMap<String, String>();
					option.put("value", entry.getKey());
					option.put("text", entry.getValue());
					options.add(option);
				}
				ma.setOptions(options);
			}
		}
		final String sqlDataMeta = "select " + StringUtils.join(columns.keySet(), ",") + " from " + tableType + "_" + module + " where 1!=1";

		ConnectionPoolManager.getInstance().managedConnection(new ConnectionPoolManager.ManagedConnection()
		{
			@Override
			public void useConnection(Connection conn) throws SQLException
			{
				PreparedStatement prst = conn.prepareStatement(sqlDataMeta);
				ResultSet rst = prst.executeQuery();

				ResultSetMetaData meta = rst.getMetaData();
				for (int i = 0; i < meta.getColumnCount(); i++)
				{
					String name = meta.getColumnName(i + 1).toLowerCase();
					//String type = meta.getColumnTypeName(i+1);
					int size = meta.getColumnDisplaySize(i + 1);

					if (columns.containsKey(name))
					{
						columns.get(name).setSize(size);
					}
				}
			}
		});
		return columns;
	}

	public List<ModuleAttributes> getAllCategoryColumnsInfo(String module) throws SQLException
	{
		return getCategoryColumnsInfo(module, null, false);
	}

	public List<ModuleAttributes> getCategoryColumnsInfo(String module, String level) throws SQLException
	{
		return getCategoryColumnsInfo(module, level, true);
	}

	public List<ModuleAttributes> getCategoryColumnsInfo(String module, String level, boolean showOnOnly) throws SQLException
	{
		List<ModuleAttributes> columnInformation = new LinkedList<ModuleAttributes>();
		columnInformation.add(new ModuleAttributes("category_id", "Category_id", "Category", "", true, true, true));
		columnInformation.add(new ModuleAttributes("live", "Live", "Live", "", false, false, false));
		columnInformation.add(new ModuleAttributes("live_date", "Live_date", "Live date", "", false, false, false));
		columnInformation.add(new ModuleAttributes("expire_date", "Expire_date", "Expire date", "", false, false, false));

		String sql = "select label_internalname as internal,"
				+ " label_externalname as external,"
				+ " label_tabname as tab,"
				+ " Label_Mandatory as mandatory,"
				+ " Label_readonly as readonly,"
				+ " Label_nochange as nochange"
				+ " from categorylabels_" + module + " where 1 = 1";
		if (showOnOnly)
		{
			sql += " and label_onoff = '1'";
		}
		if (StringUtils.isNotBlank(level))
		{
			sql += " and (Attribute_Level = ? or Attribute_Level = '0')";
		}
		sql += " order by display_order";

		TransactionDataAccess tda = getDataAccess();
		List<DataMap> otherColumns = getDataAccess().selectMaps(sql, StringUtils.isNotBlank(level) ? new String[] { level } : new String[] {});
		for (DataMap dataMap : otherColumns)
		{
			columnInformation.add(new ModuleAttributes(
					dataMap.getString("internal").toLowerCase(),
					dataMap.getString("internal"),
					dataMap.getString("external"),
					dataMap.getString("tab"),
					dataMap.getBoolean("mandatory"),
					dataMap.getBoolean("readonly"),
					dataMap.getBoolean("nochange")));
		}

		generateColumnMetadata(module, "categories", columnInformation);
		return columnInformation;
	}

	public boolean columnIsDate(String internalColumnName)
	{
		internalColumnName = internalColumnName.toUpperCase();
		return internalColumnName.equalsIgnoreCase("live_date")
				|| internalColumnName.equalsIgnoreCase("expire_date")
				|| internalColumnName.startsWith("ATTRDATE_");
	}

	public boolean columnIsFile(String internalColumnName)
	{
		internalColumnName = internalColumnName.toUpperCase();
		return internalColumnName.startsWith("ATTRTEXT_")
				|| internalColumnName.startsWith("ATTRLONG_")
				|| internalColumnName.startsWith("ATTRFILE_");
	}

	private class Pair implements Comparable
	{
		private Map.Entry<String, String> entry;

		public Pair(Map.Entry<String, String> e)
		{
			this.entry = e;
		}

		public Map.Entry<String, String> getEntry()
		{
			return this.entry;
		}

		@Override
		public int compareTo(Object o)
		{
			if (!(o instanceof Pair))
			{
				throw new InvalidParameterException();
			}
			String value = getEntry().getValue();
			String another = ((Pair) o).getEntry().getValue();
			return value.compareTo(another);
		}

	}

	public String getFolderLevelForCategory(String module, String id)
	{
		String sql = "select folderLevel"
				+ " from categories_" + module
				+ " where Category_id = ? ";
		String folderLevel = "0";
		try
		{
			folderLevel = getDataAccess().selectString(sql, id);
			if (folderLevel == null)
				folderLevel = "0";
		}
		catch (SQLException e)
		{
			logger.fatal(e);
		}
		return folderLevel;
	}

	public int countElements(String module, String categoryParentID)
	{
		return countElements(module, categoryParentID, null, null);
	}

	public int countElements(String module, String categoryParentID, Set<String> filterItems, Column[] columns)
	{
		return countElementsRecursively(module, categoryParentID, filterItems, columns);
	}

	private int countElementsRecursively(String module, String categoryParentID, Set<String> filterItems,
			Column[] columns)
	{
		String tablePrefixSub = "e";
		QueryBuffer sqlBuffer = new QueryBuffer();
		QueryBuffer sqlDdmBuffer = new QueryBuffer();

		// lookup total level
		int moduleLevel = getModuleLevels(module);
		// lookup current level
		int parentLevel = getCategoryLevel(module, categoryParentID);

		sqlBuffer.append("select count(*) from elements_" + module + " " + tablePrefixSub);

		//Multi dropdown WHERE
		int joins = 0;

		if (null != columns)
		{
			for (Column column : columns)
			{
				if (StringUtils.isNotBlank(column.search.value))
				{
					if (column.data.startsWith("attrmulti"))
					{
						joins++;

						sqlBuffer.append(" , drop_down_multi_selections " + "ddms" + joins);
						sqlDdmBuffer.append(" AND " + tablePrefixSub + ".element_id = ddms" + joins + ".element_id ");
					}

				}
			}
			joins = 0;
		}


		if (moduleLevel > parentLevel)
		{
			for (int i = moduleLevel; i > parentLevel; i--)
			{
				sqlBuffer.append(" , categories_" + module + " c" + i);
			}
			sqlBuffer.append(" where c" + (parentLevel + 1) + ".Category_parentId = ? ", categoryParentID);
			for (int i = moduleLevel; i > parentLevel + 1; i--)
			{
				sqlBuffer.append(" and c" + i + ".Category_parentId = c" + (i - 1) + ".Category_id");
			}
			sqlBuffer.append(" and c" + moduleLevel + ".Category_id = e.Category_id");
		}
		else
		{
			sqlBuffer.append(" where e.Category_Id = ? ", categoryParentID);
		}


		sqlBuffer.append(sqlDdmBuffer);

		//Columns conditions
		if (null != columns)
		{
			for (Column column : columns)
			{
				if (StringUtils.isNotBlank(column.search.value))
				{
					if (column.data.startsWith("attrinteger") || column.data.startsWith("attrdrop") || "element_id".equals(column.data) || "category_id".equals(column.data) || "category_parentid".equals(column.data))
					{
						sqlBuffer
								.append(" AND ")
								.append(tablePrefixSub + "." + column.data + " = ?", column.search.value);
					}
					if (column.data.startsWith("attrfloat") ||
							column.data.startsWith("attrcurrency"))
					{
						sqlBuffer
								.append(" AND ")
								.append(" FORMAT(" + tablePrefixSub + "." + column.data + ",2) = FORMAT(?,2)", column.search.value);
					}
					else if (column.data.startsWith("attrmulti"))
					{
						joins++;

						sqlBuffer
								.append(" AND ddms" + joins + ".SELECTED_VALUE = ? ", column.search.value)
								.append(" AND ddms" + joins + ".ATTRMULTI_NAME = ? ", column.data);
					}
					else
					{
						sqlBuffer.append(" AND ")
								.append(tablePrefixSub + "." + column.data + " LIKE ? ", "%" + column.search.value + "%");
					}
				}
			}
		}


		if (filterItems != null && filterItems.size() > 0)
		{
			//Append Only search through the required IDs
			sqlBuffer.appendIn(" AND " + tablePrefixSub + ".Element_id in ", new ArrayList<String>(filterItems));
		}

		String count = "0";
		try
		{
			count = getDataAccess().selectString(sqlBuffer);
		}
		catch (SQLException e)
		{
			throw new RuntimeException(e);
		}
		return Integer.parseInt(count);
	}

	public List<String> searchElements(String module, String filterQuery)
	{
		if (StringUtils.isNotBlank(filterQuery))
		{
			logger.info("Adding Lucene filter for element query: " + filterQuery);
			List<String> ids = new LuceneHelper(module).searchElements(filterQuery);
			logger.info("Matched " + ids.size() + " elements.");

			return ids;
		}
		return null;
	}

	public List<String> searchCategories(String module, String filterQuery)
	{
		if (StringUtils.isNotBlank(filterQuery))
		{
			logger.info("Adding Lucene filter for category query: " + filterQuery);
			List<String> ids = new LuceneHelper(module).searchCategories(filterQuery);
			logger.info("Matched " + ids.size() + " elements.");

			return ids;
		}
		return null;
	}

	private int countCategoriesRecursively(String module, String categoryParentID, int showLevel, boolean mustUseInClause, Set<String> filterIDs, Column[] columns)
	{
		String tablePrefixSub = "c" + showLevel;
		QueryBuffer sqlBuffer = new QueryBuffer();
		QueryBuffer sqlDdmBuffer = new QueryBuffer();

		// lookup current level
		int parentLevel = getCategoryLevel(module, categoryParentID);

		sqlBuffer.append("select count(*)"
				+ " from categories_" + module + " " + tablePrefixSub);

		//Multi dropdown WHERE
		int joins = 0;

		if (null != columns)
		{
			for (Column column : columns)
			{
				if (StringUtils.isNotBlank(column.search.value))
				{
					if (column.data.startsWith("attrmulti"))
					{
						joins++;

						sqlBuffer.append(" , drop_down_multi_selections " + "ddms" + joins);
						sqlDdmBuffer.append(" AND " + tablePrefixSub + ".element_id = ddms" + joins + ".element_id ");
					}

				}
			}
			joins = 0;
		}


		if (showLevel - 1 > parentLevel)
		{
			for (int i = showLevel - 1; i > parentLevel; i--)
			{
				sqlBuffer.append(" , categories_" + module + " c" + i);
			}
			sqlBuffer.append(" where c" + (parentLevel + 1) + ".Category_parentId = ?", categoryParentID);
			for (int i = showLevel; i > parentLevel + 1; i--)
			{
				sqlBuffer.append(" and c" + i + ".Category_parentId = c" + (i - 1) + ".Category_id");
			}
		}
		else
		{
			sqlBuffer.append(" where " + tablePrefixSub + ".Category_parentId = ?", categoryParentID);
		}


		sqlBuffer.append(sqlDdmBuffer);

		//Columns conditions
		if (null != columns)
		{
			for (Column column : columns)
			{
				if (StringUtils.isNotBlank(column.search.value))
				{
					if (column.data.startsWith("attrinteger") || column.data.startsWith("attrdrop") || "element_id".equals(column.data) || "category_id".equals(column.data) || "category_parentid".equals(column.data))
					{
						sqlBuffer
								.append(" AND ")
								.append(tablePrefixSub + "." + column.data + " = ?", column.search.value);
					}
					if (column.data.startsWith("attrfloat") ||
							column.data.startsWith("attrcurrency"))
					{
						sqlBuffer
								.append(" AND ")
								.append(" FORMAT(" + tablePrefixSub + "." + column.data + ",2) = FORMAT(?,2)", column.search.value);
					}
					else if (column.data.startsWith("attrmulti"))
					{
						joins++;

						sqlBuffer
								.append(" AND ddms" + joins + ".SELECTED_VALUE = ? ", column.search.value)
								.append(" AND ddms" + joins + ".ATTRMULTI_NAME = ? ", column.data);
					}
					else
					{
						sqlBuffer.append(" AND ")
								.append(tablePrefixSub + "." + column.data + " LIKE ? ", "%" + column.search.value + "%");
					}
				}
			}
		}

		if (mustUseInClause && filterIDs.size() > 0)
		{
			sqlBuffer.appendIn(" and " + tablePrefixSub + ".Category_id in ", new ArrayList<String>(filterIDs));
		}

		try
		{
			String count = TransactionDataAccess.getInstance().selectString(sqlBuffer);
			return Integer.parseInt(count);
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * 
	 * @param module
	 * @param categoryParentID
	 * @param filterQuery
	 *            - A String to filter search results by.
	 * @param userID
	 *            - UserID who we are counting for
	 * @param permissions
	 *            - The desired permission level we require
	 * @return
	 */
	public int countCategories(String module, String categoryParentID, String filterQuery, PrivilegeUtils privilageUtils, int permissions, int showLevel, Column[] columns)
	{

		try
		{
			Set<String> filterIDs = new HashSet<String>();

			boolean hasUnrestrictedPermissions = privilageUtils.isUnrestricted();
			boolean mustUseInClause = false;

			//Search lucene for matching category indexes
			if (StringUtils.isNotBlank(filterQuery))
			{
				logger.info("Adding Lucene filter for category query: " + filterQuery);

				List<String> ids = new LuceneHelper(module).searchCategories(filterQuery);
				filterIDs.addAll(ids);

				mustUseInClause = true;
				logger.info("Matched " + ids.size() + " categories.");
			}

			//filter by permissions
			if (!hasUnrestrictedPermissions)
			{
				logger.info("Adding users based on permissions for categoryID: " + categoryParentID);

				Set<String> categoryIDs = privilageUtils.categoriesWithPrivilages(permissions);
				filterIDs.addAll(categoryIDs);

				mustUseInClause = true;
				logger.info("Matched " + categoryIDs.size() + " categories.");
			}

			if (mustUseInClause && filterIDs.size() == 0)
			{
				logger.info("No categories to filter");
				return 0;
			}

			return countCategoriesRecursively(module, categoryParentID, showLevel, mustUseInClause, filterIDs, columns);
		}
		catch (Exception e)
		{
			logger.fatal("Error", e);
		}

		return 0;
	}

	public List<? extends ModuleData> getDataPage(
			String module,
			String tablePrefix,
			String dataId,
			List<String> internalNames,
			String orderBy,
			String orderDirection,
			int limit,
			int offset,
			Set<String> filter,
			int showLevel,
			Column[] columns)
	{
		if (filter != null && filter.size() == 0)
		{
			return new LinkedList<ModuleData>();
		}

		if ("elements".equals(tablePrefix))
		{
			return getDataPageForElementsRecursively(module, dataId, internalNames, orderBy, orderDirection, limit, offset, filter, columns);
		}
		return getDataPageForCategoriesRecursively(module, dataId, internalNames, orderBy, orderDirection, limit, offset, filter, showLevel, columns);

	}

	private List<? extends ModuleData> getDataPageForCategoriesRecursively(
			String module,
			String dataId,
			List<String> internalNames,
			String orderBy,
			String orderDirection,
			int limit,
			int offset,
			Set<String> filter,
			int showLevel,
			Column[] columns)
	{
		String tablePrefixSub = "c" + showLevel;

		// lookup current level
		int parentLevel = getCategoryLevel(module, dataId);

		boolean orderByEnabled = !StringUtils.isEmpty(orderDirection);
		boolean pagingEnabled = limit > 0;

		QueryBuffer sqlBuffer = new QueryBuffer();
		QueryBuffer sqlDdmBuffer = new QueryBuffer();


		sqlBuffer
				.append("SELECT " + tablePrefixSub + ".category_parentid, ")
				.append(StringUtils.join(prefix(internalNames, tablePrefixSub + "."), ","))
				.append(" FROM categories_" + module + " " + tablePrefixSub);


		//Multi dropdown WHERE
		int joins = 0;

		if (null != columns)
		{
			for (Column column : columns)
			{
				if (StringUtils.isNotBlank(column.search.value))
				{
					if (column.data.startsWith("attrmulti"))
					{
						joins++;

						sqlBuffer.append(" , drop_down_multi_selections " + "ddms" + joins);
						sqlDdmBuffer.append(" AND " + tablePrefixSub + ".element_id = ddms" + joins + ".element_id ");
					}

				}
			}
			joins = 0;
		}


		if (showLevel - 1 > parentLevel)
		{
			for (int i = showLevel - 1; i > parentLevel; i--)
			{
				sqlBuffer.append(" , categories_" + module + " c" + i);
			}

			sqlBuffer.append(" WHERE c" + (parentLevel + 1) + ".Category_parentId = ? ", dataId);
			for (int i = showLevel; i > parentLevel + 1; i--)
			{
				sqlBuffer.append(" and c" + i + ".Category_parentId = c" + (i - 1) + ".Category_id");
			}
		}
		else
		{
			sqlBuffer.append(" where " + tablePrefixSub + ".Category_parentId = ?", dataId);
		}


		sqlBuffer.append(sqlDdmBuffer);

		//Columns conditions
		if (null != columns)
		{
			for (Column column : columns)
			{
				if (StringUtils.isNotBlank(column.search.value))
				{
					if (column.data.startsWith("attrinteger") || column.data.startsWith("attrdrop") || "element_id".equals(column.data) || "category_id".equals(column.data) || "category_parentid".equals(column.data))
					{
						sqlBuffer
								.append(" AND ")
								.append(tablePrefixSub + "." + column.data + " = ?", column.search.value);
					}
					if (column.data.startsWith("attrfloat") ||
							column.data.startsWith("attrcurrency"))
					{
						sqlBuffer
								.append(" AND ")
								.append(" FORMAT(" + tablePrefixSub + "." + column.data + ",2) = FORMAT(?,2)", column.search.value);
					}
					else if (column.data.startsWith("attrmulti"))
					{
						joins++;

						sqlBuffer
								.append(" AND ddms" + joins + ".SELECTED_VALUE = ? ", column.search.value)
								.append(" AND ddms" + joins + ".ATTRMULTI_NAME = ? ", column.data);
					}
					else
					{
						sqlBuffer.append(" AND ")
								.append(tablePrefixSub + "." + column.data + " LIKE ? ", "%" + column.search.value + "%");
					}
				}
			}
		}


		if (filter != null)
		{
			sqlBuffer
					.appendIn(" AND " + tablePrefixSub + ".Category_id IN ", new ArrayList<String>(filter));
		}

		if (orderByEnabled)
		{
			sqlBuffer
					.append(" ORDER BY ")
					.append(tablePrefixSub + "." + orderBy)
					.append(" ")
					.append(orderDirection);
		}
		if (pagingEnabled)
		{
			sqlBuffer
					.append(" LIMIT ")
					.append(String.valueOf(limit))
					.append(" OFFSET ")
					.append(String.valueOf(offset));
		}
		try
		{
			List<? extends ModuleData> data = ModuleAccess.getInstance().getCategories(sqlBuffer.getSQL(), sqlBuffer.getParams());
			return data;
		}
		catch (Exception e)
		{
			logger.fatal("Error", e);
		}
		return null;
	}

	private List<? extends ModuleData> getDataPageForElementsRecursively(
			String module,
			String dataId,
			List<String> internalNames,
			String orderBy,
			String orderDirection,
			int limit,
			int offset,
			Set<String> filter,
			Column[] columns)
	{
		String tablePrefixSub = "e";

		// lookup total level
		int moduleLevel = getModuleLevels(module);
		// lookup current level
		int parentLevel = getCategoryLevel(module, dataId);

		boolean orderByEnabled = !StringUtils.isEmpty(orderDirection);
		boolean pagingEnabled = limit > 0;

		QueryBuffer sqlDdmBuffer = new QueryBuffer();
		QueryBuffer sqlBuffer = new QueryBuffer();

		sqlBuffer
				.append("SELECT " + tablePrefixSub + ".category_id, ")
				.append(StringUtils.join(prefix(internalNames, tablePrefixSub + "."), ","))
				.append(" FROM elements_" + module + " " + tablePrefixSub);

		//Multi dropdown WHERE
		int joins = 0;

		if (null != columns)
		{
			for (Column column : columns)
			{
				if (StringUtils.isNotBlank(column.search.value))
				{
					if (column.data.startsWith("attrmulti"))
					{
						joins++;

						sqlBuffer.append(" , drop_down_multi_selections " + "ddms" + joins);
						sqlDdmBuffer.append(" AND " + tablePrefixSub + ".element_id = ddms" + joins + ".element_id ");
					}

				}
			}
			joins = 0;
		}

		if (moduleLevel > parentLevel)
		{
			for (int i = moduleLevel; i > parentLevel; i--)
			{
				sqlBuffer.append(" , categories_" + module + " c" + i);
			}
			sqlBuffer.append(" where c" + (parentLevel + 1) + ".Category_parentId = ?", dataId);
			for (int i = moduleLevel; i > parentLevel + 1; i--)
			{
				sqlBuffer.append(" and c" + i + ".Category_parentId = c" + (i - 1) + ".Category_id");
			}
			sqlBuffer.append(" and c" + moduleLevel + ".Category_id = " + tablePrefixSub + ".Category_id");
		}
		else
		{
			sqlBuffer.append(" where " + tablePrefixSub + ".Category_Id = ?", dataId);
		}

		sqlBuffer.append(sqlDdmBuffer);

		//Columns conditions
		if (null != columns)
		{
			for (Column column : columns)
			{
				if (StringUtils.isNotBlank(column.search.value))
				{
					if (column.data.startsWith("attrinteger") || column.data.startsWith("attrdrop") || "element_id".equals(column.data) || "category_id".equals(column.data) || "category_parentid".equals(column.data))
					{
						sqlBuffer
								.append(" AND ")
								.append(tablePrefixSub + "." + column.data + " = ?", column.search.value);
					}
					if (column.data.startsWith("attrfloat") ||
							column.data.startsWith("attrcurrency"))
					{
						sqlBuffer
								.append(" AND ")
								.append(" FORMAT(" + tablePrefixSub + "." + column.data + ",2) = FORMAT(?,2)", column.search.value);
					}
					else if (column.data.startsWith("attrmulti"))
					{
						joins++;

						sqlBuffer
								.append(" AND ddms" + joins + ".SELECTED_VALUE = ? ", column.search.value)
								.append(" AND ddms" + joins + ".ATTRMULTI_NAME = ? ", column.data);
					}
					else
					{
						sqlBuffer.append(" AND ")
								.append(tablePrefixSub + "." + column.data + " LIKE ? ", "%" + column.search.value + "%");
					}
				}
			}
		}

		if (filter != null)
		{
			sqlBuffer
					.appendIn(" AND " + tablePrefixSub + ".Element_ID IN ", new ArrayList<String>(filter));
		}

		if (orderByEnabled)
		{
			sqlBuffer
					.append(" ORDER BY ")
					.append(tablePrefixSub + "." + orderBy)
					.append(" ")
					.append(orderDirection);
		}
		if (pagingEnabled)
		{
			sqlBuffer
					.append(" LIMIT ")
					.append(String.valueOf(limit))
					.append(" OFFSET ")
					.append(String.valueOf(offset));
		}
		try
		{
			List<? extends ModuleData> data = ModuleAccess.getInstance().getElements(sqlBuffer.getSQL(), sqlBuffer.getParams());
			return data;
		}
		catch (Exception e)
		{
			logger.fatal("Error", e);
		}
		return null;
	}

	/**
	 * @param internalNames
	 * @param string
	 * @return
	 */
	private List<String> prefix(List<String> list, String prefix)
	{
		List<String> newList = new ArrayList<String>();
		for (String value : list)
		{
			newList.add(prefix + value);
		}
		return newList;
	}

	public List<String> getDBColumns(String tableType, String module) throws SQLException
	{
		List<String> columns = new ArrayList<String>();
		List<DataMap> dbColumns = getDataAccess().selectMaps("show columns in " + tableType + "_" + module);
		for (DataMap dbColumn : dbColumns)
		{
			columns.add(dbColumn.getString("field").toLowerCase());
		}
		return columns;
	}

	/**
	 * @param module
	 * @param dataId
	 * @param value
	 * @return not null -> set of matched elementIDs (can be empty)
	 * @return null -> No filters applied
	 */


	public int getModuleLevels(String moduleName)
	{
		String sql = "select levels from sys_moduleconfig where internal_module_name = ?";

		List<String> result;
		try
		{
			result = getDataAccess().selectStrings(sql, moduleName);
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return 0;
		}
		return Integer.parseInt(result.get(0));
	}

	public int getCategoryLevel(String moduleName, String categoryId)
	{
		if (StringUtils.equals(categoryId, "0"))
		{
			return 0;
		}
		String sql = "select folderLevel from categories_" + moduleName + " where category_id = ?";

		try
		{
			List<String> result = getDataAccess().selectStrings(sql, categoryId);
			return Integer.parseInt(result.get(0));
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	public static void trim(List<? extends DataMap> data, String... fields)
	{
		for (DataMap record : data)
		{
			trim(record, fields);
		}
	}

	public static void trim(DataMap record, String... fields)
	{
		Iterator<Entry<String, Object>> it = record.entrySet().iterator();
		while (it.hasNext())
		{
			Entry<String, Object> entry = it.next();
			boolean keyFound = false;
			for (String field : fields)
			{
				if (StringUtils.endsWithIgnoreCase(field, entry.getKey()))
				{
					keyFound = true;
					break;
				}
			}
			if (!keyFound)
			{
				it.remove();
			}
		}
	}
}
