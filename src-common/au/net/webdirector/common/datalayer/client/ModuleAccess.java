package au.net.webdirector.common.datalayer.client;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.MultiOptions;
import au.net.webdirector.common.datalayer.base.db.entity.Note;
import au.net.webdirector.common.datalayer.base.db.entity.Notes;
import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
import au.net.webdirector.common.datalayer.base.db.entity.TextFile;
import au.net.webdirector.common.datalayer.base.db.mapper.CategoryDataMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.ElementDataMapper;
import au.net.webdirector.common.datalayer.client.conf.ModuleAccessConfig;

public class ModuleAccess
{

	protected static Logger logger = Logger.getLogger(ModuleAccess.class);

	protected ModuleAccessConfig config = new ModuleAccessConfig();

	public static ModuleAccess getInstance()
	{
		return new ModuleAccess();
	}

	public static void organiseModuleDataFromDB(ModuleData m)
	{
		for (Entry<String, Object> entry : m.entrySet())
		{
			String key = entry.getKey();
			if (key.startsWith("attrfile_"))
			{
				StoreFile storeFile = new StoreFile(m.getString(key));
				storeFile.setId(m.getId());
				m.put(key, storeFile);
				continue;
			}
			if (key.startsWith("attrtext_") || key.startsWith("attrlong_"))
			{
				TextFile textFile = new TextFile(m.getString(key));
				textFile.setId(m.getId());
				m.put(key, textFile);
				continue;
			}
		}
	}

	protected String getNotesTable()
	{
		return "notes";
	}

	protected String getMultiOptionsTable()
	{
		return "drop_down_multi_selections";
	}

	protected String getElementsTable(String module)
	{
		return "Elements_" + module;
	}

	protected String getCategoriesTable(String module)
	{
		return "Categories_" + module;
	}

	protected Note createNote()
	{
		return new Note();
	}

	protected String getForiegnKeyOfNote()
	{
		return "foriegnKey";
	}

	protected String getPrimaryKeyOfModuleData()
	{
		return "";
	}

	protected String getForiegnKeyOfMultiDrop()
	{
		return "ELEMENT_ID";
	}

	public void setConfig(ModuleAccessConfig config)
	{
		this.config = config;
	}

	public void setConfig(ModuleAccessConfig... configs)
	{
		this.config = ModuleAccessConfig.emptyInstance();
		for (ModuleAccessConfig config : configs)
		{
			this.config.merge(config);
		}
	}

	public ModuleAccessConfig getConfig()
	{
		return this.config;
	}

	public Notes getNotes(ModuleData data, String key, String module) throws SQLException
	{
		String notesTable = getNotesTable();
		Notes notes = new Notes();
		notes.setElementOrCategory(data.isElement() ? "Elements" : "Categories");
		notes.setField(key);
		notes.setModule(module);
		notes.setId(data.getId());
		List<Note> lNotes = new ArrayList<Note>();
		notes.setNotes(lNotes);

		String foriegnKey = getForiegnKeyOfNote();
		String primaryKey = getPrimaryKeyOfModuleData();
		String sql = "select * from " + notesTable + " where fieldName like '" + notes.getField() + "%' and moduleName = ? and elementOrCategory = ? ";
		sql += " and " + foriegnKey + "= ?";
		String[] args = new String[] { notes.getModule(), notes.getElementOrCategory(), StringUtils.isBlank(primaryKey) ? data.getId() : data.getString(primaryKey) };

		List<DataMap> list = getDataAccess().selectMaps(sql, args);
		for (DataMap n : list)
		{
			Note note = createNote();
			note.setCreateDate(n.getDate("createDate"));
			note.setContent(n.getString("content"));
			note.setFollowUpDate(n.getDate("followUpDate"));
			note.setId(n.getString("id"));
			note.setWho(n.getString("who"));
			lNotes.add(note);
		}
		return notes;
	}

	public MultiOptions getMultiOptions(ModuleData data, String key, String module) throws SQLException
	{
		String multiOptionsTable = getMultiOptionsTable();
		MultiOptions options = new MultiOptions();
		options.setId(data.getId());
		options.setModule(module);
		options.setField(key);
		options.setType(data.getType());

		String foriegnKey = getForiegnKeyOfMultiDrop();
		String primaryKey = getPrimaryKeyOfModuleData();
		String sql = "select selected_value from " + multiOptionsTable + " where ATTRMULTI_NAME = ? and MODULE_NAME = ? and MODULE_TYPE = ?";
		sql += " and " + foriegnKey + "= ? order by selected_value";
		String[] args = new String[] { options.getField(), options.getModule(), options.getType(), StringUtils.isBlank(primaryKey) ? data.getId() : data.getString(primaryKey) };
		options.setValues(getDataAccess().selectStrings(sql, args));

		return options;
	}

	public TransactionDataAccess getDataAccess()
	{
		return TransactionDataAccess.getInstance();
	}


	/*
	 * basic methods of CRUD
	 */

	public List<ElementData> getElements(String module, ModuleData conditions) throws SQLException
	{
		String[] columns = ModuleHelper.getInstance().getElementColumns(module);
		List<ElementData> result = getDataAccess().selectMaps(columns, getElementsTable(module), config.createCondition(conditions), config.getOrders(), new ElementDataMapper());
		for (ElementData element : result)
		{
			element.putAll(conditions);
		}
		return result;
	}


	public List<ElementData> getElements(String sql, String... args) throws SQLException
	{
		List<ElementData> result = getDataAccess().selectMaps(sql, new ElementDataMapper(), args);
		return result;
	}


	public List<CategoryData> getCategories(String module, CategoryData conditions) throws SQLException
	{
		String[] columns = ModuleHelper.getInstance().getCategoryColumns(module);
		List<CategoryData> result = getDataAccess().selectMaps(columns, getCategoriesTable(module), config.createCondition(conditions), config.getOrders(), new CategoryDataMapper());
		for (CategoryData category : result)
		{
			category.putAll(conditions);
		}
		return result;
	}


	public List<CategoryData> getCategories(String sql, String... args) throws SQLException
	{
		List<CategoryData> result = getDataAccess().selectMaps(sql, new CategoryDataMapper(), args);
		return result;
	}

	/*
	 * derived methods to improve usability
	 */

	public ElementData getElement(String module, ElementData conditions) throws SQLException
	{
		List<ElementData> list = getElements(module, conditions);
		return list.size() > 0 ? list.get(0) : null;
	}

	public ElementData getElementById(String module, String id) throws SQLException
	{
		ElementData conditions = new ElementData();
		conditions.setId(id);
		return getElement(module, conditions);
	}

	public List<ElementData> getElementsByParentId(String module, String parentId) throws SQLException
	{
		ElementData conditions = new ElementData();
		conditions.setParentId(parentId);
		return getElements(module, conditions);
	}

	public List<ElementData> getElementsByName(String module, String name) throws SQLException
	{
		ElementData conditions = new ElementData();
		conditions.setName(name);
		return getElements(module, conditions);
	}

	public List<ElementData> getElementsByField(String module, String field, Object value) throws SQLException
	{
		ElementData conditions = new ElementData();
		conditions.put(field, value);
		return getElements(module, conditions);
	}

	public ElementData getElementByField(String module, String field, Object value) throws SQLException
	{
		List<ElementData> list = getElementsByField(module, field, value);
		return list.size() > 0 ? list.get(0) : null;
	}

	public ElementData getElementByName(String module, String name) throws SQLException
	{
		List<ElementData> list = getElementsByName(module, name);
		return list.size() > 0 ? list.get(0) : null;
	}

	public List<ElementData> getElementsByName(String module, String name, boolean logicalLiveOnly) throws SQLException
	{
		ElementData conditions = new ElementData();
		conditions.setName(name);
		conditions.setLogicalLive(logicalLiveOnly);
		return getElements(module, conditions);
	}

	public List<ElementData> getElementsByParentName(String module, String... parentsName) throws SQLException
	{
		List<CategoryData> parents = null;
		for (String name : parentsName)
		{
			if (parents == null)
			{
				parents = getCategoriesByName(module, name, new CategoryData[] {});
			}
			else
			{
				parents = getCategoriesByName(module, name, parents.toArray(new CategoryData[] {}));
			}

		}
		List<ElementData> result = new ArrayList<ElementData>();
		for (CategoryData parent : parents)
		{
			result.addAll(getElementsByParentId(module, parent.getId()));
		}
		return result;
	}

	public CategoryData getCategory(String module, CategoryData conditions) throws SQLException
	{
		List<CategoryData> list = getCategories(module, conditions);
		return list.size() > 0 ? list.get(0) : null;
	}


	public CategoryData getCategoryById(String module, String id) throws SQLException
	{
		CategoryData conditions = new CategoryData();
		conditions.setId(id);
		return getCategory(module, conditions);
	}

	public List<CategoryData> getCategoriesByParentId(String module, String parentId) throws SQLException
	{
		CategoryData conditions = new CategoryData();
		conditions.setParentId(parentId);
		return getCategories(module, conditions);
	}

	public CategoryData getCategoryByName(String module, String name) throws SQLException
	{
		List<CategoryData> list = getCategoriesByName(module, name);
		return list.size() > 0 ? list.get(0) : null;
	}

	public List<CategoryData> getCategoriesByName(String module, String name) throws SQLException
	{
		CategoryData conditions = new CategoryData();
		conditions.setName(name);
		return getCategories(module, conditions);
	}

	public List<CategoryData> getCategoriesByField(String module, String field, Object value) throws SQLException
	{
		CategoryData conditions = new CategoryData();
		conditions.put(field, value);
		return getCategories(module, conditions);
	}

	public CategoryData getCategoryByField(String module, String field, Object value) throws SQLException
	{
		List<CategoryData> list = getCategoriesByField(module, field, value);
		return list.size() > 0 ? list.get(0) : null;
	}

	public List<CategoryData> getCategoriesByParentName(String module, String... parentsName) throws SQLException
	{
		List<CategoryData> parents = null;
		for (String name : parentsName)
		{
			if (parents == null)
			{
				parents = getCategoriesByName(module, name, new CategoryData[] {});
			}
			else
			{
				parents = getCategoriesByName(module, name, parents.toArray(new CategoryData[] {}));
			}

		}
		List<CategoryData> result = new ArrayList<CategoryData>();
		for (CategoryData parent : parents)
		{
			result.addAll(getCategoriesByParentId(module, parent.getId()));
		}
		return result;
	}

	protected List<CategoryData> getCategoriesByName(String module, String name, CategoryData... parents) throws SQLException
	{
		if (parents == null || parents.length == 0)
		{
			return getCategoriesByName(module, name);
		}
		List<CategoryData> result = new ArrayList<CategoryData>();
		for (CategoryData parent : parents)
		{
			CategoryData condition = new CategoryData();
			condition.setParentId(parent.getId());
			condition.setName(name);
			result.addAll(getCategories(module, condition));
		}
		return result;
	}


	// and more ...
}
