package au.net.webdirector.common.datalayer.client;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

public class CategoryData extends ModuleData
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1916286218177428657L;

	public static final String TYPE = "category";

	public int getFolderLevel()
	{
		return getInt("folderLevel");
	}

	public void setFolderLevel(int folderLevel)
	{
		put("folderLevel", folderLevel);
	}
	
	public boolean getBoolean(String key)
	{
		Object o = get(key);
		if (o == null)
		{
			return false;
		}
		String value = o.toString();
		return StringUtils.equalsIgnoreCase("1", value) || StringUtils.equalsIgnoreCase("YES", value) || StringUtils.equalsIgnoreCase("TRUE", value) || StringUtils.equalsIgnoreCase("Y", value);
	}

	@Override
	public boolean isElement()
	{
		return false;
	}

	@Override
	public boolean isCategory()
	{
		return true;
	}

	@Override
	public String getId()
	{
		return getString("category_id");
	}

	@Override
	public String getName()
	{
		return getString("attr_categoryname");
	}

	@Override
	public String getParentId()
	{
		return getString("category_parentid");
	}

	@Override
	public void removeId()
	{
		remove("category_id");
	}

	@Override
	public void setId(String id)
	{
		if (StringUtils.isBlank(id))
		{
			removeId();
			return;
		}
		put("category_id", id);
	}

	@Override
	public void setName(String name)
	{
		put("attr_categoryname", name);
	}

	@Override
	public void setParentId(String parentId)
	{
		put("category_parentid", parentId);
	}

	@Override
	public boolean logicalLive()
	{
		if (containsKey("live") && !getBoolean("live"))
		{
			return false;
		}
		Date liveDate = getLiveDate();
		if (liveDate != null && liveDate.after(new Date()))
		{
			return false;
		}
		Date expireDate = getExpireDate();
		if (expireDate != null && expireDate.before(new Date()))
		{
			return false;
		}
		return true;
	}

	@Override
	public boolean isLive()
	{
		return getBoolean("live");
	}

	@Override
	public void setLive(boolean live)
	{
		put("live", live ? new Integer(1) : new Integer(0));
	}

	@Override
	public ModuleData minimum()
	{
		CategoryData data = new CategoryData();
		ModuleHelper.putSystemColumn(data);
		data.setParentId(getParentId());
		data.setName(getName());
		data.setFolderLevel(getFolderLevel());
		data.setLive(isLive());
		data.put("create_date", new Date());
		return data;
	}

	@Override
	public String getType()
	{
		return TYPE;
	}

}
