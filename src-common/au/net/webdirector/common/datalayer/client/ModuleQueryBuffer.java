/**
 * @author Sushant Verma
 * @date 3 Mar 2016
 */
package au.net.webdirector.common.datalayer.client;

import au.net.webdirector.common.utils.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.base.db.QueryBuffer;

public class ModuleQueryBuffer extends QueryBuffer<ModuleQueryBuffer>
{
	private static Logger logger = Logger.getLogger(ModuleQueryBuffer.class);

	private static final SimpleDateFormat LIVE_CLAUSE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");

	public ModuleQueryBuffer()
	{
	}

	public ModuleQueryBuffer standardLiveClause()
	{
		return standardLiveClause(null, new Date());
	}

	public ModuleQueryBuffer standardLiveClause(String tableAlias, Date nowDate)
	{
		String tablePrefix = "";
		if (StringUtils.isNotEmpty(tableAlias))
		{
			tablePrefix = tableAlias + ".";
		}
		String formattedDate = LIVE_CLAUSE_DATE_FORMAT.format(nowDate);

		append(" ( ");
		append(tablePrefix).append("Live = 1 ");
		append(" and ( ");

		append(tablePrefix).append("Live_date <= ? or ", formattedDate);
		append(tablePrefix).append("Live_date IS NULL ");

		append(" ) and ( ");
		append(tablePrefix).append("Expire_date >= ? or ", formattedDate);
		append(tablePrefix).append("Expire_date IS NULL ");
		append(" ) ");

		append(" ) ");

		return this;
	}
}
