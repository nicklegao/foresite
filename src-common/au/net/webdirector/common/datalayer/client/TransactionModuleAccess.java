package au.net.webdirector.common.datalayer.client;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.MultiOptions;
import au.net.webdirector.common.datalayer.base.db.entity.Note;
import au.net.webdirector.common.datalayer.base.db.entity.Notes;
import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
import au.net.webdirector.common.datalayer.base.db.entity.TextFile;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccess;
import au.net.webdirector.common.datalayer.base.transaction.CommittingAction;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.lucene.LuceneHelper;
import au.net.webdirector.common.datalayer.util.text.DefaultDateFormat;
import au.net.webdirector.common.utils.taskQueue.QueuedThreadService;

public class TransactionModuleAccess extends ModuleAccess
{

	protected static Logger logger = Logger.getLogger(TransactionModuleAccess.class);
	protected TransactionManager txManager;
	protected ModuleHelper helper;
	protected boolean indexLucene = true;

	public static TransactionModuleAccess getInstance(TransactionManager txManager)
	{
		return new TransactionModuleAccess(txManager);
	}

	protected TransactionModuleAccess(TransactionManager txManager)
	{
		this.txManager = txManager;
		this.helper = ModuleHelper.getInstance(txManager);
	}

	protected String getDataTable(String module, EntityType type)
	{
		return (EntityType.ELEMENT.equals(type) ? "elements_" : "categories_") + module;
	}

	protected TextFile createTextFileFromPath(String path)
	{
		return new TextFile(path);
	}

	protected StoreFile createStoreFileFromPath(String path)
	{
		return new StoreFile(path);
	}

	public boolean isIndexLucene()
	{
		return indexLucene;
	}

	public void setIndexLucene(boolean indexLucene)
	{
		this.indexLucene = indexLucene;
	}

	public TransactionManager getTxManager()
	{
		return txManager;
	}

	public TransactionDataAccess getDataAccess()
	{
		return txManager.getDataAccess();
	}

	public StoreFileAccess getFileAccess()
	{
		return txManager.getFileAccess();
	}

	protected DataMap converToDataMap(ModuleData data, String module) throws IOException, SQLException
	{
		DataMap map = new DataMap();
		for (Entry<String, Object> entry : data.entrySet())
		{
			String key = entry.getKey();
			Object o = entry.getValue();
			map.put(key, o);
			if (o == null)
			{
				continue;
			}
			if (key.startsWith("attrnotes_"))
			{
				processNotes(data, key, module);
				continue;
			}
			if (key.startsWith("attrmulti_"))
			{
				processMultiOptions(data, key, module);
				continue;
			}
			if (key.startsWith("attrfile_"))
			{
				processStoreFile(data, key, map);
				continue;
			}
			if (key.startsWith("attrlong_") || key.startsWith("attrtext_"))
			{
				processTextFile(data, key, map);
				continue;
			}
		}
		return map;
	}

	protected void processMultiOptions(ModuleData data, String key, String module) throws SQLException
	{
		if (!data.get(key).getClass().isAssignableFrom(MultiOptions.class))
		{
			return;
		}
		MultiOptions options = (MultiOptions) data.get(key);
		String tableName = getMultiOptionsTable();
		TransactionDataAccess da = txManager.getDataAccess();
		DataMap map = new DataMap();
		map.put("element_id", options.getId());
		map.put("attrmulti_name", options.getField());
		map.put("module_name", module);
		map.put("module_type", data.getType());

		// delete existing
		da.delete(tableName, map);

		for (String value : options.getValues())
		{
			map.put("selected_value", value);
			da.insert(map, tableName);
		}
	}

	protected void processNotes(ModuleData data, String key, String module) throws SQLException, IOException
	{
		if (!data.get(key).getClass().isAssignableFrom(Notes.class))
		{
			return;
		}
		String noteTable = getNotesTable();
		Notes notes = (Notes) data.get(key);
		TransactionDataAccess da = txManager.getDataAccess();
		//StoreFileAccess fa = txManager.getFileAccess(); // no longer needed now that notes are stored in db and not fs
		List<String> processed = new ArrayList<String>();
		for (Note note : notes.getNotes())
		{
			String id = note.getId();
			if (id == null)
			{
				// add new
				String insert = "insert into " + noteTable + " (modulename, foriegnkey, elementOrCategory, createDate, content, who, live, live_date, expire_date) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
				Object[] args = new Object[] {
						notes.getModule(),
						notes.getId(),
						notes.getElementOrCategory(),
						new DefaultDateFormat().format(new Date()),
						note.getContent(),
						note.getWho(),
						note.getLive(),
						note.getLive_dateFormatted(),
						note.getExpire_dateFormatted()
				};
				int result = da.insert(insert, args);
				id = String.valueOf(result);
				da.update("update " + noteTable + " set fieldname = ? where id = ?", new Object[] { notes.getField() + "-" + id, id });
			}
			else
			{
				da.update("update " + noteTable + " set createDate = ?, content=?, who=?, live = ?, live_date = ?, expire_date = ? where id = ?",
						new Object[] {
								new DefaultDateFormat().format(new Date()),
								note.getContent(),
								note.getWho(),
								note.getLive(),
								note.getLive_dateFormatted(),
								note.getExpire_dateFormatted(),
								id }
						);
			}
			processed.add(id);
		}
		// delete not processed
		String[] args = new String[] { notes.getModule(), notes.getId(), notes.getElementOrCategory() };
		List<DataMap> toBeDeleted = da.selectMaps("select * from " + noteTable + " where id not in ('" + StringUtils.join(processed, "','") + "') and fieldName like '" + notes.getField()
				+ "%' and moduleName = ? and foriegnKey = ? and elementOrCategory = ? ", args);
		for (DataMap note : toBeDeleted)
		{
			da.update("delete from " + noteTable + " where id = ?", new String[] { note.getString("id") });
		}

	}

	protected void processStoreFile(ModuleData data, String key, DataMap map) throws IOException
	{
		Object o = data.get(key);
		if (o.getClass().isAssignableFrom(String.class))
		{
			StoreFile storeFile = createStoreFileFromPath((String) o);
			StoreFile newStoreFile = createStoreFileFromPath((String) o);
			newStoreFile.setId(data.getId());

			if (StringUtils.isNotBlank(storeFile.getFileName()) && !StringUtils.equals(storeFile.getId(), newStoreFile.getId()))
			{
				File dest = ModuleHelper.getStoredFile(newStoreFile.getPath());
				if (dest.getParentFile().exists())
				{
					txManager.getFileAccess().cleanDirectory(dest.getParentFile());
				}
				txManager.getFileAccess().copy(ModuleHelper.getStoredFile(storeFile.getPath()), dest);
				if (ModuleHelper.getStoredFile(storeFile.getThumbNailPath()).exists())
				{
					txManager.getFileAccess().copy(ModuleHelper.getStoredFile(storeFile.getThumbNailPath()), ModuleHelper.getStoredFile(newStoreFile.getThumbNailPath()));
				}
			}
			map.put(key, newStoreFile);
			return;
		}
		StoreFile storeFile = (StoreFile) o;
		if (storeFile.getUploadedFile() == null && StringUtils.isBlank(storeFile.getPath()))
		{ // to delete the current file, put a storefile object with module/id/field/isCategory data.
			File destFolder = ModuleHelper.getStoredFolder(storeFile);
			if (destFolder.exists() && destFolder.isDirectory())
			{
				txManager.getFileAccess().cleanDirectory(destFolder);
			}
			map.put(key, null);
			return;
		}
		storeFile.setId(data.getId());
		File dest = ModuleHelper.getStoredFile(storeFile.getPath());
		if (dest.getParentFile().exists() && !dest.getAbsolutePath().toLowerCase().contains("attrfile_attached_file"))
		{
			txManager.getFileAccess().cleanDirectory(dest.getParentFile());
		}
		if (!dest.getAbsolutePath().contains(storeFile.getUploadedFile().getAbsolutePath())) {
			txManager.getFileAccess().copy(storeFile.getUploadedFile(), dest);
		}
		String extention = storeFile.getUrl().substring(storeFile.getUrl().lastIndexOf(".")+1);
		if (!extention.equalsIgnoreCase("pdf")) {
			Defaults.getInstance().generateThumb(storeFile.getUploadedFile());
			File newThumbNail = getThumbnailFile(storeFile.getUploadedFile());
			if (newThumbNail.exists())
			{
				txManager.getFileAccess().copy(newThumbNail, ModuleHelper.getStoredFile(storeFile.getThumbNailPath()));
			}
		}
	}

	protected static File getThumbnailFile(File source)
	{
		File parent = source.getParentFile();
		String fileName = source.getName();
		return new File(parent, "_thumb/" + fileName);
	}


	protected void processTextFile(ModuleData data, String key, DataMap map) throws IOException
	{
		Object o = data.get(key);
		if (o.getClass().isAssignableFrom(String.class))
		{
			TextFile textFile = createTextFileFromPath((String) o);
			TextFile newTextFile = createTextFileFromPath((String) o);
			newTextFile.setId(data.getId());

			if (StringUtils.isNotBlank(textFile.getFileName()) && !StringUtils.equals(textFile.getId(), newTextFile.getId()))
			{
				txManager.getFileAccess().copy(ModuleHelper.getStoredFile(textFile.getPath()), ModuleHelper.getStoredFile(newTextFile.getPath()));
			}
			map.put(key, newTextFile);
			return;
		}
		TextFile textFile = (TextFile) o;
		textFile.setId(data.getId());
		txManager.getFileAccess().writeStringToFile(ModuleHelper.getStoredFile(textFile.getPath()), textFile.getContent());
	}

	protected void cloneMultipleDataFields(ModuleData data, String module, ModuleData newData) throws SQLException, IOException
	{
		for (Entry<String, Object> entry : newData.entrySet())
		{
			String key = entry.getKey();
			if (key.startsWith("attrnotes_"))
			{
				Notes notes;
				if (data.get(key) == null || !(data.get(key) instanceof Notes))
				{
					notes = getNotes(data, key, module);
				}
				else
				{
					notes = (Notes) data.get(key);
				}
				notes.setId(newData.getId());
				for (Note note : notes.getNotes())
				{
					note.setId(null);
				}
				newData.put(key, notes);
				continue;
			}
			if (key.startsWith("attrmulti_"))
			{
				MultiOptions options;
				if (data.get(key) == null || !(data.get(key) instanceof MultiOptions))
				{
					options = getMultiOptions(data, key, module);
				}
				else
				{
					options = (MultiOptions) data.get(key);
				}
				options.setId(newData.getId());
				newData.put(key, options);
				continue;
			}
		}
	}


	protected int updateModuleDataToDB(ModuleData moduleData, String module, EntityType type) throws SQLException, IOException
	{
		DataMap data = converToDataMap(moduleData, module);
		ModuleData conditions = EntityType.ELEMENT.equals(type) ? new ElementData() : new CategoryData();
		conditions.setId(moduleData.getId());
		return txManager.getDataAccess().update(data, getDataTable(module, type), conditions);
	}

	public ElementData insertElementUpdateReference(ElementData referencedElement, String module) throws SQLException, IOException
	{
		ElementData newElement = insertElement(referencedElement, module);
		referencedElement.putAll(newElement);

		return newElement;
	}

	/*
	 * base methods of CUD;
	 */
	public ElementData insertElement(ElementData element, String module) throws SQLException, IOException
	{
		// logger.info("insertElement : " + element);
		ElementData clone = (ElementData) element.clone();
		ElementData newElement = (ElementData) clone.minimum();
		int id = txManager.getDataAccess().insert(newElement, getElementsTable(module));
		newElement.putAll(clone);
		newElement.setId(String.valueOf(id));
		cloneMultipleDataFields(clone, module, newElement);

		updateModuleDataToDB(newElement, module, EntityType.ELEMENT);
		insertLuceneIndex(EntityType.ELEMENT, module, String.valueOf(id));
		return newElement;
	}

	public int updateElement(ElementData element, String module) throws SQLException, IOException
	{
		logger.info("UpdateElement : " + element);
		ElementData clone = (ElementData) element.clone();
		int result = updateModuleDataToDB(clone, module, EntityType.ELEMENT);
		logger.info("Re-indexing lucene... ");
		updateLuceneIndex(EntityType.ELEMENT, module, clone.getId());
		logger.info("UpdateElement done ");
		return result;
	}

	public CategoryData insertCategory(CategoryData category, String module) throws SQLException, IOException
	{
		CategoryData clone = (CategoryData) category.clone();
		CategoryData newCate = (CategoryData) clone.minimum();
		int id = txManager.getDataAccess().insert(newCate, getCategoriesTable(module));
		newCate.putAll(clone);
		newCate.setId(String.valueOf(id));
		cloneMultipleDataFields(clone, module, newCate);
		updateModuleDataToDB(newCate, module, EntityType.CATEGORY);
		insertLuceneIndex(EntityType.CATEGORY, module, String.valueOf(id));
		return newCate;

	}

	public int updateCategory(CategoryData category, String module) throws SQLException, IOException
	{
		logger.info("updateCategory : " + category.getId());
		CategoryData clone = (CategoryData) category.clone();
		int result = updateModuleDataToDB(clone, module, EntityType.CATEGORY);
		updateLuceneIndex(EntityType.CATEGORY, module, clone.getId());
		return result;
	}

	protected void insertLuceneIndex(final EntityType type, final String module, final String id)
	{
		if (!indexLucene)
		{
			return;
		}
		txManager.registerCommittingAction(new CommittingAction()
		{
			@Override
			public void doAction()
			{
				QueuedThreadService.getInstance(LuceneHelper.QUEUE_NAME).addToQueue(new Runnable()
				{
					@Override
					public void run()
					{
						if (LuceneHelper.luceneSearchOn())
						{
							LuceneHelper helper = new LuceneHelper(module);
							helper.insert(type, id);
						}
					}
				});
			}
		});

	}

	protected void updateLuceneIndex(final EntityType type, final String module, final String id)
	{
		if (!indexLucene)
		{
			return;
		}
		txManager.registerCommittingAction(new CommittingAction()
		{
			@Override
			public void doAction()
			{
				QueuedThreadService.getInstance(LuceneHelper.QUEUE_NAME).addToQueue(new Runnable()
				{
					@Override
					public void run()
					{
						if (LuceneHelper.luceneSearchOn())
						{
							LuceneHelper helper = new LuceneHelper(module);
							helper.update(type, id);
						}
					}
				});
			}
		});
	}

	// deletion part

	protected int deleteElement(ElementData element, String module) throws SQLException, IOException
	{
		deleteStoreFiles(element, module);
		deleteNotes(element, module);
		deleteMultiOptions(element, module);

		ElementData conditions = new ElementData();
		conditions.setId(element.getId());
		int result = getDataAccess().delete(getElementsTable(module), conditions);
		deleteLuceneIndex(EntityType.ELEMENT, module, element.getId());
		return result;
	}

	protected int deleteCategory(CategoryData category, String module) throws SQLException, IOException
	{

		deleteStoreFiles(category, module);
		deleteNotes(category, module);
		deleteMultiOptions(category, module);

		// delete all sub categories
		CategoryData condition1 = new CategoryData();
		condition1.setParentId(category.getId());
		deleteCategories(module, condition1);

		// delete all sub elements
		ElementData condition2 = new ElementData();
		condition2.setParentId(category.getId());
		deleteElements(module, condition2);

		// delete myself
		CategoryData conditions = new CategoryData();
		conditions.setId(category.getId());
		int result = getDataAccess().delete(getCategoriesTable(module), conditions);
		deleteLuceneIndex(EntityType.CATEGORY, module, category.getId());
		return result;
	}

	protected void deleteMultiOptions(ModuleData data, String module) throws SQLException
	{
		TransactionDataAccess da = txManager.getDataAccess();
		DataMap map = new DataMap();
		map.put("element_id", data.getId());
		map.put("module_name", module);
		map.put("module_type", data.getType());
		for (String field : data.keySet())
		{
			if (!field.startsWith("attrmulti_"))
			{
				continue;
			}
			map.put("attrmulti_name", field);
			da.delete(getMultiOptionsTable(), map);
		}
	}

	protected void deleteNotes(ModuleData data, String module) throws SQLException, IOException
	{
		TransactionDataAccess da = txManager.getDataAccess();
		StoreFileAccess fa = txManager.getFileAccess();
		for (String field : data.keySet())
		{
			if (!field.startsWith("attrnotes_"))
			{
				continue;
			}
			String[] args = new String[] { module, data.getId(), data.getClass().isAssignableFrom(ElementData.class) ? "Elements" : "Categories" };
			List<DataMap> toBeDeleted = da.selectMaps("select * from " + getNotesTable() + " where fieldName like '" + field + "%' and moduleName = ? and foriegnKey = ? and elementOrCategory = ? ", args);
			for (DataMap note : toBeDeleted)
			{
				File noteFile = ModuleHelper.getStoredFile(note.getString("note"));
				if (noteFile.exists() && noteFile.isFile())
				{
					fa.delete(noteFile);
				}
				da.update("delete from " + getNotesTable() + " where id = ?", new String[] { note.getString("id") });
			}
		}
	}

	protected void deleteStoreFiles(ModuleData data, String module) throws IOException
	{
		StoreFileAccess fa = txManager.getFileAccess();
		File idFolder = null;
		for (String field : data.keySet())
		{
			if (!field.startsWith("attrfile_") && !field.startsWith("attrlong_") && !field.startsWith("attrtext_"))
			{
				continue;
			}
			StoreFile file = data.getStoreFile(field);
			if (file == null || file.getStoredFile() == null || !file.getStoredFile().exists())
			{
				continue;
			}
			idFolder = file.getStoredFile().getParentFile().getParentFile();
			fa.delete(file.getStoredFile().getParentFile());
		}

		if (idFolder != null && idFolder.exists() && idFolder.isDirectory() && idFolder.listFiles() == null)
		{
			fa.delete(idFolder);
		}
	}

	protected void deleteLuceneIndex(final EntityType type, final String module, final String id)
	{
		if (!indexLucene)
		{
			return;
		}
		txManager.registerCommittingAction(new CommittingAction()
		{
			@Override
			public void doAction()
			{
				QueuedThreadService.getInstance(LuceneHelper.QUEUE_NAME).addToQueue(new Runnable()
				{
					@Override
					public void run()
					{
						if (LuceneHelper.luceneSearchOn())
						{
							LuceneHelper helper = new LuceneHelper(module);
							helper.delete(type, id);
						}
					}
				});
			}

		});
	}

	/*
	 * derived methods
	 */
	public int deleteElements(String module, ModuleData conditions) throws SQLException, IOException
	{
		List<ElementData> list = this.getElements(module, conditions);
		int result = 0;
		for (ElementData elm : list)
		{
			result += deleteElement(elm, module);
		}
		return result;
	}

	public int deleteCategories(String module, CategoryData conditions) throws SQLException, IOException
	{
		List<CategoryData> list = this.getCategories(module, conditions);
		int result = 0;
		for (CategoryData cate : list)
		{
			result += deleteCategory(cate, module);
		}
		return result;
	}

	public int deleteElementById(String module, String... ids) throws SQLException, IOException
	{
		int changes = 0;
		for (String id : ids)
		{
			ElementData conditions = new ElementData();
			conditions.setId(id);
			changes += deleteElements(module, conditions);
		}
		return changes;
	}

	public int deleteCategoryById(String module, String... ids) throws SQLException, IOException
	{
		int changes = 0;
		for (String id : ids)
		{
			CategoryData conditions = new CategoryData();
			conditions.setId(id);
			changes += deleteCategories(module, conditions);
		}
		return changes;
	}

	// add more...
}
