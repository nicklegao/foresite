package au.net.webdirector.common.datalayer.client;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import au.net.webdirector.common.datalayer.base.db.DataHelper;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.entity.Password;
import au.net.webdirector.common.datalayer.base.db.entity.SharedFile;
import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
import au.net.webdirector.common.datalayer.base.db.entity.TextFile;
import au.net.webdirector.common.datalayer.base.db.entity.version.VersionStoreFile;
import au.net.webdirector.common.datalayer.base.db.entity.version.VersionTextFile;
import au.net.webdirector.common.datalayer.base.db.entity.workflow.DraftStoreFile;
import au.net.webdirector.common.datalayer.base.db.entity.workflow.DraftTextFile;
import au.net.webdirector.common.datalayer.util.text.DataFormat;
import au.net.webdirector.common.datalayer.util.text.DefaultDateFormat;

import com.google.gson.Gson;

public abstract class ModuleData extends DataMap
{

	private static final long serialVersionUID = -7329209174081841240L;

	protected DataHelper getHelper()
	{
		return ModuleHelper.getInstance();
	}

	public SharedFile getShareFile(String key)
	{
		Object o = get(key);
		if (o == null)
		{
			return null;
		}
		if (o instanceof String)
		{
			String path = (String) o;
			if (StringUtils.isBlank(path))
			{
				return null;
			}
			return new SharedFile(path);
		}
		return (SharedFile) o;
	}

	public boolean getBoolean(String key)
	{
		Object o = get(key);
		if (o == null)
		{
			return false;
		}
		String value = o.toString();
		return StringUtils.equalsIgnoreCase("1", value) || StringUtils.equalsIgnoreCase("YES", value) || StringUtils.equalsIgnoreCase("TRUE", value) || StringUtils.equalsIgnoreCase("Y", value);
	}

	public TextFile getTextFile(String key)
	{
		Object o = get(key);
		if (o == null)
		{
			return null;
		}
		if (o instanceof String)
		{
			String path = (String) o;
			if (StringUtils.isBlank(path))
			{
				return null;
			}
			if (path.startsWith("/_DRAFT"))
			{
				return new DraftTextFile(path);
			}
			if (path.startsWith("/_VERSION_CONTROL"))
			{
				return new VersionTextFile(path);
			}
			return new TextFile(path);
		}
		return (TextFile) o;
	}

	public StoreFile getStoreFile(String key)
	{
		Object o = get(key);
		if (o == null)
		{
			return null;
		}
		if (o instanceof String)
		{
			String path = (String) o;
			if (StringUtils.isBlank(path))
			{
				return null;
			}
			if (path.startsWith("/_DRAFT"))
			{
				return new DraftStoreFile(path);
			}
			if (path.startsWith("/_VERSION_CONTROL"))
			{
				return new VersionStoreFile(path);
			}
			return new StoreFile(path);
		}
		return (StoreFile) o;
	}

	public String getStoreFileUrl(String key)
	{
		StoreFile file = getStoreFile(key);
		if (file == null)
		{
			return null;
		}
		return file.getUrl();
	}

	public Password getPassword(String key, DataFormat format)
	{
		Object o = get(key);
		if (o == null)
		{
			return null;
		}
		if (o instanceof String)
		{
			if (format == null)
			{
				format = getHelper().getDefaultDataFormat(new Password());
			}
			try
			{
				return (Password) format.parse((String) o);
			}
			catch (ParseException e)
			{
				throw new RuntimeException(e);
			}
		}
		return (Password) o;
	}

	public Password getPassword(String key)
	{
		return getPassword(key, null);
	}

	public int getDisplayOrder()
	{
		return getInt("display_order");
	}

	public Date getCreateDate()
	{
		return getDate("create_date");
	}

	public Date getLastUpdateDate()
	{
		return getDate("last_update_date");
	}

	public Date getLiveDate()
	{
		return getDate("live_date");
	}

	public Date getExpireDate()
	{
		return getDate("expire_date");
	}

	public Object getValueFromJsonData(String key, Gson parser, String attributes)
	{
		try
		{
			TextFile text = getTextFile(key);
			String dataStr = text.getContent();
			Map data = parser.fromJson(dataStr, Map.class);
			String[] attrChain = StringUtils.split(attributes, '.');
			Object value = data;
			for (String singleAttr : attrChain)
			{
				if (value instanceof Map)
				{
					value = ((Map) value).get(singleAttr);
				}
				else
				{
					return null;
				}
			}
			return value;
		}
		catch (Exception e)
		{
			return null;
		}
	}

	public void setDisplayOrder(int displayOrder)
	{
		put("display_order", displayOrder);
	}

	public void setLiveDate(Date liveDate)
	{
		put("live_date", liveDate);
	}

	public void setExpireDate(Date expireDate)
	{
		put("expire_date", expireDate);
	}

	public void setLiveDate(String liveDate)
	{
		put("live_date", DefaultDateFormat.parseDate(liveDate));
	}

	public void setExpireDate(String expireDate)
	{
		put("expire_date", DefaultDateFormat.parseDate(expireDate));
	}

	public abstract boolean isElement();

	public abstract boolean isCategory();

	public abstract String getId();

	public abstract String getName();

	public abstract String getParentId();

	public abstract boolean isLive();

	public abstract boolean logicalLive();

	public abstract void removeId();

	public abstract void setId(String id);

	public abstract void setName(String name);

	public abstract void setParentId(String parentId);

	public abstract void setLive(boolean live);

	public abstract ModuleData minimum();

	public abstract String getType();

}
