package au.net.webdirector.common.datalayer.client;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

public class ElementData extends ModuleData
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3194891801031857541L;

	public static final String TYPE = "element";

	@Override
	public boolean isElement()
	{
		return true;
	}

	@Override
	public boolean isCategory()
	{
		return false;
	}

	@Override
	public String getId()
	{
		return getString("element_id");
	}

	@Override
	public String getName()
	{
		return getString("attr_headline");
	}

	@Override
	public String getParentId()
	{
		return getString("category_id");
	}

	@Override
	public void removeId()
	{
		remove("element_id");
	}

	@Override
	public void setId(String id)
	{
		if (StringUtils.isBlank(id))
		{
			removeId();
			return;
		}
		put("element_id", id);
	}

	@Override
	public void setName(String name)
	{
		put("attr_headline", name);
	}

	@Override
	public void setParentId(String parentId)
	{
		put("category_id", parentId);
	}

	@Override
	public boolean logicalLive()
	{
		if (!getBoolean("live"))
		{
			return false;
		}
		Date liveDate = getLiveDate();
		if (liveDate != null && liveDate.after(new Date()))
		{
			return false;
		}
		Date expireDate = getExpireDate();
		if (expireDate != null && expireDate.before(new Date()))
		{
			return false;
		}
		return true;
	}

	@Override
	public boolean isLive()
	{
		return getBoolean("live");
	}

	@Override
	public void setLive(boolean live)
	{
		put("live", live ? new Integer(1) : new Integer(0));
	}

	@Override
	public ModuleData minimum()
	{
		ElementData data = new ElementData();
		ModuleHelper.putSystemColumn(data);
		data.setParentId(getParentId());
		data.setName(getName());
		data.setLive(isLive());
		data.put("create_date", new Date());
		return data;
	}

	@Override
	public String getType()
	{
		return TYPE;
	}

	public void setLogicalLive(boolean live)
	{
		setLive(live);
		String key = "(live_date is null or live_date <= now()) and (expire_date is null or expire_date >= now())";
		if (live)
		{
			put(key, "");
		}
		else
		{
			remove(key);
		}
	}
}
