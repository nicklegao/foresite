package au.net.webdirector.common.datalayer.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.codecs.Codec;
import org.owasp.esapi.codecs.MySQLCodec;

import au.net.webdirector.common.datalayer.admin.db.DataAccess;

public class DatabaseValidation
{

	private static Codec codec = new MySQLCodec(MySQLCodec.Mode.STANDARD);

	public static String encodeParam(String parameter)
	{
		String encoded = "";
		try
		{
			encoded = ESAPI.encoder().encodeForSQL(codec, parameter);
			encoded = StringUtils.replace(encoded, "\\_", "_");
			encoded = StringUtils.replace(encoded, "\\.", ".");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return encoded;
	}

	public static Object encodeParam(Object parameter)
	{
		String encoded = "";
		try
		{
			encoded = ESAPI.encoder().encodeForSQL(codec, parameter.toString());
			encoded = StringUtils.replace(encoded, "\\_", "_");
			encoded = StringUtils.replace(encoded, "\\.", ".");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return encoded;
	}

	public static String[] encodeParam(String[] parameters)
	{
		try
		{
			if (parameters != null)
			{
				for (int i = 0; i < parameters.length; i++)
				{
					parameters[i] = encodeParam(parameters[i]);
				}
				return parameters;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return new String[] {};
	}

	public static List<String> encodeParam(List<String> parameters)
	{
		try
		{
			if (parameters != null)
			{
				for (int i = 0; i < parameters.size(); i++)
				{
					parameters.set(i, encodeParam(parameters.get(i)));
				}
				return parameters;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return new ArrayList<String>();
	}

	private static int i = 4;

	private static void test()
	{
		String module = "INFORMATION";
		int categoryID = 2;
		String headline = "sql Injection test" + i++;
		String sql = "insert into elements_" + module + " (Language_id, Status_id, Lock_id, Version, display_order, Live, Category_id, ATTR_Headline) values (?,?,?,?,?,?,?,?)";
		int elementID = DataAccess.getInstance().updateData(sql, new Object[] { 1, 1, 1, 1, 0, 0, categoryID, headline });

//		String sql = "insert into elements_" + module + " (Language_id, Status_id, Lock_id, Version, display_order, Live, Category_id, ATTR_Headline) values (1, 1, 1, 1, 0, 0, " + categoryID + ", '"
//				+ headline + "')";
//		int elementID = DataAccess.getInstance().insertData(sql, new Object[] {});

	}


	public static void main(String args[])
	{
//    	String hello = StringUtils.join(encodeParam(new String[]{"abc@abc.com' or 1=1 ; #","hello world"}), "','");
//    	System.out.println(DatabaseValidation.encodeParam("Language_id,Status_id,Lock_id,Version,display_order,Create_date,Live_date,Expire_date "));

	}
}
