package au.net.webdirector.common.datalayer.util.text;

import java.text.ParseException;

import org.apache.commons.lang.StringUtils;


public class DefaultBooleanFormat implements DataFormat
{

	@Override
	public Object parse(String source) throws ParseException
	{
		if (StringUtils.equalsIgnoreCase("1", source) || StringUtils.equalsIgnoreCase("Y", source) || StringUtils.equalsIgnoreCase("Yes", source) || StringUtils.equalsIgnoreCase("True", source) || StringUtils.equalsIgnoreCase("T", source))
		{
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	@Override
	public String format(Object t)
	{
		Boolean b = (Boolean) t;
		if (b == null)
		{
			return "0";
		}
		return b.booleanValue() ? "1" : "0";
	}

}
