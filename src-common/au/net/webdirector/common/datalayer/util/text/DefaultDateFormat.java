package au.net.webdirector.common.datalayer.util.text;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang.StringUtils;


public class DefaultDateFormat implements DataFormat
{

	static au.net.webdirector.common.utils.SimpleDateFormat sdf = new au.net.webdirector.common.utils.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Override
	public Object parse(String source) throws ParseException
	{
		return sdf.parse(source);
	}

	@Override
	public String format(Object t)
	{
		if (t == null)
		{
			return "";
		}
		return sdf.format(t);
	}

	public static String formatDate(Object t, String pattern)
	{
		if (t == null)
		{
			return "";
		}
		return new au.net.webdirector.common.utils.SimpleDateFormat(pattern).format(t);
	}

	public static String formatDate(Object t)
	{
		if (t == null)
		{
			return "";
		}
		return sdf.format(t);
	}

	public static Date parseDate(String date)
	{
		try
		{
			if (StringUtils.isBlank(date))
			{
				return null;
			}
			if (date.length() == 16)
			{
				return (Date) new DefaultDateFormat().parse(date + ":00");
			}
			return (Date) new DefaultDateFormat().parse(date);
		}
		catch (Exception e)
		{
			return null;
		}
	}
}
