package au.net.webdirector.common.datalayer.util.text;


public class DefaultObjectFormat implements DataFormat
{

	@Override
	public String format(Object t)
	{
		return t.toString();
	}

	@Override
	public Object parse(String str)
	{
		return str;
	}

}
