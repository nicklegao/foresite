package au.net.webdirector.common.datalayer.util.text;

import java.text.ParseException;

public interface DataFormat
{

	public String format(Object obj);

	public Object parse(String source) throws ParseException;
}
