package au.net.webdirector.common.datalayer.util.text;

import java.text.ParseException;

import org.apache.commons.lang.StringUtils;

import au.net.webdirector.common.datalayer.base.db.entity.Password;


public class DefaultPasswordFormat implements DataFormat
{

	@Override
	public Object parse(String source) throws ParseException
	{
		Password password = new Password();
		password.setEncryptedText(source);
		if (!password.isEncrypted())
		{
			throw new ParseException("Invalid Encrypted Text: [" + source + "]", 0);
		}
		return password;
	}

	@Override
	public String format(Object o)
	{
		Password t = (Password) o;
		String encryptedText = t.getEncryptedText();
		return StringUtils.isNotBlank(encryptedText) ? encryptedText : "";
	}

}
