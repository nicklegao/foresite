package au.net.webdirector.common.datalayer.util;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class CaseInsensitiveMap implements Map<String, Object>, Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5350286854756865585L;
	protected final Map<String, Object> map = Collections.synchronizedMap(new LinkedHashMap<String, Object>());

	/*
	 * methods from Map interface
	 */

	@Override
	public int size()
	{
		return map.size();
	}

	@Override
	public boolean isEmpty()
	{
		return map.isEmpty();
	}

	@Override
	public boolean containsKey(Object key)
	{
		if (null == key)
		{
			return map.containsKey(key);
		}
		if (!(key instanceof String))
		{
			return false;
		}
		return map.containsKey(((String) key).toLowerCase());
	}

	@Override
	public boolean containsValue(Object value)
	{
		return map.containsValue(value);
	}

	@Override
	public Object get(Object key)
	{
		if (null == key)
		{
			return map.get(key);
		}
		if (!(key instanceof String))
		{
			return null;
		}
		return map.get(((String) key).toLowerCase());
	}

	@Override
	public Object put(String key, Object value)
	{
		if (null == key)
		{
			return map.put(key, value);
		}
		return map.put(key.toLowerCase(), value);
	}

	@Override
	public Object remove(Object key)
	{
		if (null == key)
		{
			return map.remove(key);
		}
		if (!(key instanceof String))
		{
			return null;
		}
		return map.remove(((String) key).toLowerCase());
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> m)
	{
		for (Entry<? extends String, ? extends Object> entry : m.entrySet())
		{
			String key = entry.getKey();
			if (key == null)
			{
				map.put(key, entry.getValue());
			}
			else
			{
				map.put(key.toLowerCase(), entry.getValue());
			}
		}
	}

	@Override
	public void clear()
	{
		map.clear();
	}

	@Override
	public Set<String> keySet()
	{
		return map.keySet();
	}

	@Override
	public Collection<Object> values()
	{
		return map.values();
	}

	@Override
	public Set<java.util.Map.Entry<String, Object>> entrySet()
	{
		return map.entrySet();
	}

	@Override
	public String toString()
	{
		return map.toString();
	}
}
