package au.net.webdirector.common.datalayer.util;


public class StringArrayUtils
{

	public static void fillArray(String[] array, String value)
	{
		for (int i = 0; i < array.length; i++)
		{
			array[i] = value;
		}
	}

	public static String[] fillArray(String value, int length)
	{
		String[] array = new String[length];
		fillArray(array, value);
		return array;
	}
}
