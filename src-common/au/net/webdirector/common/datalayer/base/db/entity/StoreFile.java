package au.net.webdirector.common.datalayer.base.db.entity;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import au.net.webdirector.common.datalayer.base.file.CommonStoreFileAccess;

public class StoreFile extends AbstractFile
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3718494376177235277L;

	protected File uploadedFile;
	protected String externalUrl;

	public StoreFile(String path)
	{
		super(path);
	}

	public StoreFile(File uploadedFile, String module, String field, boolean isCategory, String id)
	{
		this(uploadedFile, module, field, isCategory);
		this.id = id;
	}

	public StoreFile(File uploadedFile, String module, String field, boolean isCategory)
	{
		super(isCategory);
		this.uploadedFile = uploadedFile;
		this.module = module;
		this.field = field;
		this.fileName = uploadedFile == null ? "" : uploadedFile.getName();
	}

	public StoreFile(String externalUrl, File tempFolder, String module, String field, boolean isCategory, String id)
	{
		this(externalUrl, tempFolder, module, field, isCategory);
		this.id = id;
	}

	public StoreFile(String externalUrl, File tempFolder, String module, String field, boolean isCategory)
	{
		super(isCategory);
		File tempDir = new File(tempFolder, module + "/" + field);
		if (!tempDir.exists())
		{
			tempDir.mkdirs();
		}
		this.fileName = externalUrl.substring(externalUrl.lastIndexOf("/") + 1);
		this.uploadedFile = new File(tempDir, fileName);
		InputStream input = null;
		try
		{
			input = new URL(externalUrl).openStream();
			new CommonStoreFileAccess().copyInputStream(input, uploadedFile);
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
		finally
		{
			IOUtils.closeQuietly(input);
		}
		this.module = module;
		this.field = field;
	}

	public File getUploadedFile()
	{
		return uploadedFile;
	}

	public void setUploadedFile(File uploadedFile)
	{
		this.uploadedFile = uploadedFile;
	}

	public String getExternalUrl()
	{
		return externalUrl;
	}

	public String getThumbNailPath()
	{
		if (StringUtils.isBlank(fileName))
		{
			return "";
		}
		if (StringUtils.isBlank(id))
		{
			throw new RuntimeException("id not set yet");
		}
		if (isCategory)
		{
			return "/" + StringUtils.join(new String[] { module, "Categories", id, field, "_thumb", fileName }, "/");
		}
		return "/" + StringUtils.join(new String[] { module, id, field, "_thumb", fileName }, "/");
	}
}
