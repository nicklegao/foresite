package au.net.webdirector.common.datalayer.base.db.entity;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class Notes implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7603287345467877308L;
	String module;
	String id;
	String elementOrCategory;
	String field;
	List<Note> notes;

	public String getModule()
	{
		return module;
	}

	public void setModule(String module)
	{
		this.module = module;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getElementOrCategory()
	{
		return elementOrCategory;
	}

	public void setElementOrCategory(String elementOrCategory)
	{
		this.elementOrCategory = elementOrCategory;
	}

	public String getField()
	{
		return field;
	}

	public void setField(String field)
	{
		this.field = field;
	}

	public List<Note> getNotes()
	{
		return notes;
	}

	public void setNotes(List<Note> notes)
	{
		this.notes = notes;
	}

	public String toString()
	{
		return "";
	}

	public String asStr()
	{
		return StringUtils.join(notes, "|");
	}
}
