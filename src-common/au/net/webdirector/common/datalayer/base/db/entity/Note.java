package au.net.webdirector.common.datalayer.base.db.entity;

import java.io.Serializable;
import java.util.Date;

import au.net.webdirector.common.datalayer.util.text.DefaultDateFormat;

public class Note implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1646666010341919695L;

	private static final DefaultDateFormat ddf = new DefaultDateFormat();

	protected String id;
	protected String who;
	protected Date createDate;
	protected Date followUpDate;
	protected String content;

	boolean live;
	Date live_date;
	Date expire_date;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getWho()
	{
		return who;
	}

	public void setWho(String who)
	{
		this.who = who;
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

	public Date getFollowUpDate()
	{
		return followUpDate;
	}

	public void setFollowUpDate(Date followUpDate)
	{
		this.followUpDate = followUpDate;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getContent()
	{
		if (content == null)
		{
			return "";
		}
		return content;
	}

	public String getLive()
	{
		return live ? "1" : "0";
	}

	public boolean isLive()
	{
		return live;
	}

	public void setLive(boolean live)
	{
		this.live = live;
	}

	public Date getLive_date()
	{
		return live_date;
	}

	public void setLive_date(Date live_date)
	{
		this.live_date = live_date;
	}

	public Date getExpire_date()
	{
		return expire_date;
	}

	public void setExpire_date(Date expire_date)
	{
		this.expire_date = expire_date;
	}

	public String getLive_dateFormatted()
	{
		if (live_date == null)
			return null;

		return ddf.format(live_date);
	}

	public String getExpire_dateFormatted()
	{
		if (expire_date == null)
			return null;

		return ddf.format(expire_date);
	}

	public String toString()
	{
		return getContent();
	}
}
