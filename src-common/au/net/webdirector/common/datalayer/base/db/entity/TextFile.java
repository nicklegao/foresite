package au.net.webdirector.common.datalayer.base.db.entity;

import au.net.webdirector.common.datalayer.client.ModuleHelper;

public class TextFile extends AbstractFile
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5344441777340896860L;

	protected String content;

	public TextFile(String path)
	{
		super(path);
	}

	public TextFile(String content, String module, String field, boolean isCategory)
	{
		super(isCategory);
		this.content = content;
		this.module = module;
		this.field = field;
		this.fileName = field + ".txt";
	}

	public TextFile(String content, String module, String field, String id, boolean isCategory)
	{
		this(content, module, field, isCategory);
		this.id = id;
	}

	public String getContent()
	{
		if (content != null)
		{
			return content;
		}
		try
		{
			return ModuleHelper.readString(ModuleHelper.getStoredFile(getPath()));
		}
		catch (Exception e)
		{
			return "";
		}
	}

	public void setContent(String content)
	{
		this.content = content;
	}

}
