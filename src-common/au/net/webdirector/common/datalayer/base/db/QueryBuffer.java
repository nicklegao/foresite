/**
 * @author Sushant Verma
 * @date 8 Dec 2015
 */
package au.net.webdirector.common.datalayer.base.db;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.utils.SimpleDateFormat;

public class QueryBuffer<T extends QueryBuffer<T>> implements Cloneable
{
	private static Logger logger = Logger.getLogger(QueryBuffer.class);
	private static SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	protected StringBuffer query = new StringBuffer();
	protected LinkedList<String> params = new LinkedList<String>();

	public T append(String queryFragment, Object... args)
	{
		query.append(queryFragment);
		for (Object o : args)
		{
			if (o == null)
			{
				params.add(null);
			}
			else if (o instanceof Date)
			{
				Date aDate = (Date) o;
				params.add(dbDateFormat.format(aDate));
			}
			else
			{
				params.add(o.toString());
			}
		}

		return (T) this;
	}

	public T appendLike(String queryFragment, String list)
	{
		return append(queryFragment, "%" + list + "%");
	}

	public T appendLikeBegins(String queryFragment, String list)
	{
		return append(queryFragment, "%" + list);
	}

	public T appendLikeEnds(String queryFragment, String list)
	{
		return append(queryFragment, list + "%");
	}

	public T appendIn(String queryFragment, List<String> list)
	{
		return appendIn(queryFragment, list, "-1");
	}

	public T appendIn(String queryFragment, List<String> list, String emptyValue)
	{
		if (list.isEmpty())
		{
			list.add(emptyValue);
		}
		query.append(queryFragment);
		query.append("(");
		append(StringUtils.repeat("?", ",", list.size()), list.toArray());
		query.append(") ");

		return (T) this;
	}

	public void append(QueryBuffer anotherBuffer)
	{
		append(anotherBuffer.getSQL(), anotherBuffer.getParams());
	}

	public void addParam(String param)
	{
		params.add(param);
	}

	public void addParams(Set<String> set)
	{
		for (Object o : set)
		{
			params.add(o.toString());
		}
	}

	public String getSQL()
	{
		return query.toString();
	}

	public String[] getParams()
	{
		return params.toArray(new String[] {});
	}

	public static void main(String[] args)
	{
		try
		{
			new QueryBuffer().append("asdf", "asdf", 1, 1.2).clone();
		}
		catch (CloneNotSupportedException e)
		{
			logger.error("Error:", e);
		}
	}
}
