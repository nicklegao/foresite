package au.net.webdirector.common.datalayer.base.db.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

public class HashMapMapper implements RowMapper<Map<String, Object>>
{

	@Override
	public Map<String, Object> mapRow(ResultSet rs, int arg1) throws SQLException
	{
		HashMap<String, Object> row = new HashMap<String, Object>();
		for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++)
		{
			String colName = rs.getMetaData().getColumnLabel(i);
			row.put(colName, rs.getObject(i));
		}
		return row;
	}

}
