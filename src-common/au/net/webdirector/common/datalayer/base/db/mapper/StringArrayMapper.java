package au.net.webdirector.common.datalayer.base.db.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


public class StringArrayMapper implements RowMapper<String[]>
{

	public String[] mapRow(ResultSet results, int count) throws SQLException
	{
		String[] row = new String[results.getMetaData().getColumnCount()];
		for (int i = 0; i < results.getMetaData().getColumnCount(); i++)
		{
			row[i] = (results.getString(i + 1) == null ? "" : results.getString(i + 1));
		}
		return row;
	}

	private static StringArrayMapper instance;

	public synchronized static StringArrayMapper getInstance()
	{
		if (instance == null)
		{
			instance = new StringArrayMapper();
		}
		return instance;
	}
}
