package au.net.webdirector.common.datalayer.base.db.entity.workflow;

import java.io.File;

import org.apache.commons.lang.StringUtils;

import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;

public class DraftStoreFile extends StoreFile
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1156220027328051254L;

	public DraftStoreFile(String path)
	{
		super(path);
	}

	public DraftStoreFile(File uploadedFile, String module, String field, boolean isCategory)
	{
		super(uploadedFile, module, field, isCategory);
		// TODO Auto-generated constructor stub
	}

	protected void initFromPath(String path)
	{
		String[] array = StringUtils.split(path.substring(1), "/");
		this.module = array[1];
		this.id = array[2];
		this.field = array[3];
		this.fileName = array[4];
	}

	@Override
	protected String genRelativePath()
	{
		if (isCategory)
		{
			return "/" + StringUtils.join(new String[] { "_DRAFT", module, "Categories", id, field, fileName }, "/");
		}
		return "/" + StringUtils.join(new String[] { "_DRAFT", module, id, field, fileName }, "/");
	}
}
