/**
 * @author Patrick King
 * @date 2 Apr 2015
 */
package au.net.webdirector.common.datalayer.base.db.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class BooleanMapper implements RowMapper<Boolean>
{

	@Override
	public Boolean mapRow(ResultSet rs, int row) throws SQLException
	{
		return rs.getBoolean(1);
	}

}
