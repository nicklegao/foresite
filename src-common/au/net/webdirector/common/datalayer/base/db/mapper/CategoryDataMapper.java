/**
 * @author Sushant Verma
 * @date 23 Sep 2015
 */
package au.net.webdirector.common.datalayer.base.db.mapper;

import au.net.webdirector.common.datalayer.client.CategoryData;

public class CategoryDataMapper extends AbstractDataMapMapper<CategoryData>
{
	@Override
	public CategoryData createNewRow()
	{
		return new CategoryData();
	}
}
