package au.net.webdirector.common.datalayer.base.db.entity.workflow;

import org.apache.commons.lang.StringUtils;

import au.net.webdirector.common.datalayer.base.db.entity.TextFile;

public class DraftTextFile extends TextFile
{

	private static final long serialVersionUID = 7801507498696865609L;

	public DraftTextFile(String path)
	{
		super(path);
		// TODO Auto-generated constructor stub
	}

	public DraftTextFile(String content, String module, String field, String id, boolean isCategory)
	{
		super(content, module, field, id, isCategory);
		// TODO Auto-generated constructor stub
	}

	public DraftTextFile(String content, String module, String field, boolean isCategory)
	{
		super(content, module, field, isCategory);
		// TODO Auto-generated constructor stub
	}

	protected void initFromPath(String path)
	{
		String[] array = StringUtils.split(path.substring(1), "/");
		this.module = array[1];
		this.id = array[2];
		this.field = array[3];
		this.fileName = array[4];
	}

	@Override
	protected String genRelativePath()
	{
		if (isCategory)
		{
			return "/" + StringUtils.join(new String[] { "_DRAFT", module, "Categories", id, field, fileName }, "/");
		}
		return "/" + StringUtils.join(new String[] { "_DRAFT", module, id, field, fileName }, "/");
	}

}
