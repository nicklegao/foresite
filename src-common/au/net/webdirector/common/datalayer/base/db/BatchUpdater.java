/**
 * @author Nick Yiming Gao
 * @date 2016/12/14
 */
package au.net.webdirector.common.datalayer.base.db;

/**
 * 
 */

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.util.StringArrayUtils;

public class BatchUpdater implements AutoCloseable
{

	private static Logger logger = Logger.getLogger(BatchUpdater.class);

	protected List<DataMap> preparedDataMaps = new ArrayList<DataMap>();

	protected int threshold = 100;

	protected TransactionDataAccess da;

	public BatchUpdater(int threshold)
	{
		this(threshold, null);
	}

	public BatchUpdater(int threshold, TransactionDataAccess da)
	{
		super();
		this.threshold = threshold;
		this.da = da == null ? TransactionDataAccess.getInstance() : da;
	}

	public TransactionDataAccess getDataAccess()
	{
		return da;
	}

	@Override
	public void close() throws Exception
	{
		batchAction(null);
	}

	public void update(DataMap data) throws SQLException
	{
		update(data, data.getTable());
	}

	public void update(DataMap data, String table) throws SQLException
	{
		this.preparedDataMaps.add(data);
		if (preparedDataMaps.size() > threshold)
		{
			batchAction(table);
		}
	}

	private void batchAction(String table) throws SQLException
	{
		if (preparedDataMaps.size() == 0)
		{
			return;
		}
		try
		{
			if (StringUtils.isBlank(table))
			{
				table = preparedDataMaps.get(0).getTable();
				if (StringUtils.isBlank(table))
				{
					throw new IllegalArgumentException("Please provide table name");
				}
			}

			String[] fields = preparedDataMaps.get(0).getFields();
			String[] questionMarks = new String[fields.length];
			StringArrayUtils.fillArray(questionMarks, "?");

			QueryBuffer sql = new QueryBuffer();
			sql.append("insert into " + table + " (" + StringUtils.join(fields, ",") + ") values ");
			for (int i = 0; i < preparedDataMaps.size(); i++)
			{
				DataMap data = preparedDataMaps.get(i);
				String[] values = data.getValues(fields);
				sql.append("(" + StringUtils.join(questionMarks, ",") + ")", values);
				if (i < preparedDataMaps.size() - 1)
				{
					sql.append(",");
				}
			}
			sql.append(" ON DUPLICATE KEY UPDATE ");
			for (int i = 0; i < fields.length; i++)
			{
				String field = fields[i];
				sql.append(field + "=VALUES(" + field + ")");
				if (i < fields.length - 1)
				{
					sql.append(",");
				}
			}
			da.update(sql);
		}
		finally
		{
			preparedDataMaps.clear();
		}

	}
}
