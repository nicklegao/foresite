/**
 * @author Nick Yiming Gao
 * @date 26/11/2015
 */
package au.net.webdirector.common.datalayer.base.db.conf;

/**
 * 
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.base.db.DataMap;

public class Condition
{
	private static Logger logger = Logger.getLogger(Condition.class);

	protected String clause;

	protected List<String> values;

	public Condition(String clause)
	{
		this.clause = clause;
		this.values = new ArrayList<String>();
	}

	public Condition(String clause, String... values)
	{
		this.clause = clause;
		this.values = new ArrayList<String>(Arrays.asList(values));
	}

	public String getClause()
	{
		return clause;
	}

	public List<String> getValues()
	{
		return values;
	}

	public boolean isEmpty()
	{
		return StringUtils.isBlank(clause);
	}

	public Condition and(Condition... others)
	{
		if (others == null)
		{
			return this;
		}
		for (Condition another : others)
		{
			if (another == null || another.isEmpty())
			{
				continue;
			}
			if (this.isEmpty())
			{
				this.clause = another.getClause();
				this.values.addAll(another.getValues());
				continue;
			}
			this.clause += " and " + another.getClause();
			this.values.addAll(another.getValues());
		}
		return this;
	}

	public Condition or(Condition... others)
	{
		if (others == null)
		{
			return this;
		}
		for (Condition another : others)
		{
			if (another == null || another.isEmpty())
			{
				continue;
			}
			if (this.isEmpty())
			{
				this.clause = another.getClause();
				this.values.addAll(another.getValues());
				continue;
			}
			this.clause = "((" + this.clause + ") or (" + another.getClause() + "))";
			this.values.addAll(another.getValues());
		}
		return this;
	}

	public Condition and(String... others)
	{
		for (String another : others)
		{
			and(new Condition(another));
		}
		return this;
	}

	public Condition or(String... others)
	{
		for (String another : others)
		{
			or(new Condition(another));
		}
		return this;
	}


	public static Condition emptyInst()
	{
		return new Condition("");
	}

	public static Condition instance(DataMap data)
	{
		if (data == null || data.isEmpty())
		{
			return Condition.emptyInst();
		}
		Condition condition = null;
		for (String key : data.keySet())
		{
			Condition newOne = data.get(key) == null ? new Condition(key) : new Condition(key + "=?", data.getString(key));
			if (condition == null)
			{
				condition = newOne;
			}
			else
			{
				condition.and(newOne);
			}
		}
		return condition;
	}


}
