package au.net.webdirector.common.datalayer.base.db.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;

import org.springframework.jdbc.core.RowMapper;


public class HashtableMapper implements RowMapper<Hashtable<String, String>>
{

	public Hashtable<String, String> mapRow(ResultSet results, int count) throws SQLException
	{

		Hashtable<String, String> row = new Hashtable<String, String>();
		for (int i = 0; i < results.getMetaData().getColumnCount(); i++)
		{
			String colName = results.getMetaData().getColumnLabel(i + 1);
			row.put(colName, results.getString(i + 1) == null ? "" : results.getString(i + 1));
		}
		return row;
	}

	private static HashtableMapper _instance = null;

	public synchronized static HashtableMapper getInstance()
	{
		if (_instance == null)
		{
			_instance = new HashtableMapper();
		}
		return _instance;
	}

}