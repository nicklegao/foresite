package au.net.webdirector.common.datalayer.base.db.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import au.net.webdirector.common.datalayer.base.db.DataMap;

public abstract class AbstractDataMapMapper<T extends DataMap> implements RowMapper<T>
{


	@Override
	public T mapRow(ResultSet results, int idx) throws SQLException
	{
		// TODO Auto-generated method stub
		try
		{
			T row = createNewRow();
			for (int i = 0; i < results.getMetaData().getColumnCount(); i++)
			{
				String colName = results.getMetaData().getColumnLabel(i + 1);
				row.put(colName, results.getObject(i + 1));
			}
			return row;
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	public abstract T createNewRow();

}
