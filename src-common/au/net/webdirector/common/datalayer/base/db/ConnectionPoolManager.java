package au.net.webdirector.common.datalayer.base.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.context.support.AbstractApplicationContext;

public class ConnectionPoolManager
{
	private DataSource ds;
	private AbstractApplicationContext appContext;
	private static ConnectionPoolManager instance;
	private static Object singleton = new Object();

	private static Map<String, String> stub = new ConcurrentHashMap<>();


	public static ConnectionPoolManager getInstance()
	{
		if (instance == null)
		{
			synchronized (singleton)
			{
				if (instance == null)
				{
					instance = new ConnectionPoolManager();
				}
			}
		}
		return instance;
	}

	private String getCID(Connection conn)
	{
		if (true)
		{
			return "-1";
		}
		try
		{
			Statement stat = conn.createStatement();
			ResultSet rs = stat.executeQuery("SELECT CONNECTION_ID()");
			String cid = "-1";
			while (rs.next())
			{
				cid = rs.getString(1);
				break;
			}
			rs.close();
			stat.close();
			return cid;
		}
		catch (Exception e)
		{
			return "-1";
		}
	}

	public void releaseConnection(Connection conn) throws SQLException
	{
		if (conn != null)
		{
			stub.remove(getCID(conn));
			conn.close();
		}
	}

	public Connection getConnection() throws SQLException
	{
		Connection conn = ds.getConnection();
		stub.put(getCID(conn), new Date() + ":" + ExceptionUtils.getFullStackTrace(new Throwable()));
		return conn;
	}

	public void setAppContext(AbstractApplicationContext appContext)
	{
		this.appContext = appContext;

		this.ds = (DataSource) appContext.getBean("WebDirectorDataSource");
	}

	public interface ManagedConnection
	{
		void useConnection(Connection conn) throws SQLException;
	}

	public void managedConnection(ManagedConnection managedConnection) throws SQLException
	{
		Connection c = null;
		try
		{
			c = getConnection();
			managedConnection.useConnection(c);
		}
		finally
		{
			releaseConnection(c);
		}
	}

	public static void printConnections()
	{
		System.out.println(stub);
	}
}
