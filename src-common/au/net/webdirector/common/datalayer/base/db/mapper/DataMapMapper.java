/**
 * @author Sushant Verma
 * @date 2 Jul 2015
 */
package au.net.webdirector.common.datalayer.base.db.mapper;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.base.db.DataMap;

public class DataMapMapper extends AbstractDataMapMapper<DataMap>
{
	private static Logger logger = Logger.getLogger(DataMapMapper.class);

	/* (non-Javadoc)
	 * @see au.net.webdirector.common.datalayer.base.db.mapper.AbstractDataMapMapper#createNewRow()
	 */
	@Override
	public DataMap createNewRow()
	{
		return new DataMap();
	}

	private static DataMapMapper instance;
	private static Object _sharedLock_ = new Object();

	public static DataMapMapper getInstance()
	{
		if (instance == null)
		{
			synchronized (_sharedLock_)
			{
				if (instance == null)
				{
					instance = new DataMapMapper();
				}
			}
		}
		return instance;
	}

	private DataMapMapper()
	{
		logger.info("Creating singleton of class: " + getClass().getName());
	}
}
