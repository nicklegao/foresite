package au.net.webdirector.common.datalayer.base.db.entity;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import au.net.webdirector.common.utils.password.PasswordUtils;

public class Password implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3434272900981064121L;
	String encryptedText;
	String plainText;

	public Password()
	{

	}

	public Password(String plainText)
	{
		this.plainText = plainText;
	}

	public boolean isEncrypted()
	{
		return StringUtils.isNotBlank(encryptedText) && encryptedText.length() == 40;
	}

	public void setEncryptedText(String encryptedText)
	{
		this.encryptedText = encryptedText;
	}

	public String getEncryptedText()
	{
		return (StringUtils.isNotBlank(plainText) ? PasswordUtils.encrypt(plainText) : encryptedText);
	}

	public String getPlainText()
	{
		return plainText;
	}

	public void setPlainText(String plainText)
	{
		this.plainText = plainText;
	}

	public String toString()
	{
		return getEncryptedText();
	}


}
