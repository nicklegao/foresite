package au.net.webdirector.common.datalayer.base.db.entity;

import java.io.File;
import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import au.net.webdirector.common.datalayer.client.ModuleHelper;

public abstract class AbstractFile implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8932030274387741942L;

	protected String module;
	protected String field;
	protected String id;
	protected String fileName;
	protected boolean isCategory;

	public boolean isCategory()
	{
		return isCategory;
	}

	public void setCategory(boolean isCategory)
	{
		this.isCategory = isCategory;
	}

	protected String genRelativePath()
	{
		if (isCategory)
		{
			return "/" + StringUtils.join(new String[] { module, "Categories", id, field, fileName }, "/");
		}
		return "/" + StringUtils.join(new String[] { module, id, field, fileName }, "/");
	}

	protected AbstractFile(boolean isCategory)
	{
		this.isCategory = isCategory;
	}

	protected AbstractFile(String path)
	{
		if (StringUtils.isBlank(path))
		{
			return;
		}
		initFromPath(path);
	}

	protected void initFromPath(String path)
	{
		String[] array = StringUtils.split(path.substring(1), "/");
		if (array.length == 4)
		{
			this.isCategory = false;
			this.module = array[0];
			this.id = array[1];
			this.field = array[2];
			this.fileName = array[3];
		}
		else
		{
			this.isCategory = true;
			this.module = array[0];
			this.id = array[2];
			this.field = array[3];
			this.fileName = array[4];
		}
	}

	public String getPath()
	{
		if (StringUtils.isBlank(fileName))
		{
			return "";
		}
		if (StringUtils.isBlank(id))
		{
			throw new RuntimeException("id not set yet");
		}
		return genRelativePath();
	}


	public String getModule()
	{
		return module;
	}

	public void setModule(String module)
	{
		this.module = module;
	}

	public String getField()
	{
		return field;
	}

	public void setField(String field)
	{
		this.field = field;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getUrl()
	{
		return ModuleHelper.getStoreFileUrl(getPath());
	}

	public File getStoredFile()
	{
		return ModuleHelper.getStoredFile(getPath());
	}

	public String toString()
	{
		return getPath();
	}

	public String getFileName()
	{
		return fileName;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}
}
