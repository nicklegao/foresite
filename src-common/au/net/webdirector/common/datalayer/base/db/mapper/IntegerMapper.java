/**
 * @author Patrick King
 * @date 2 Apr 2015
 */
package au.net.webdirector.common.datalayer.base.db.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class IntegerMapper implements RowMapper<Integer>
{

	@Override
	public Integer mapRow(ResultSet rs, int row) throws SQLException
	{
		return rs.getInt(1);
	}

}
