package au.net.webdirector.common.datalayer.base.db.entity;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class MultiOptions implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4146005253041725969L;

	String id;
	String field;
	String module;
	String type;
	List<String> values;

	public MultiOptions()
	{
	}

	public MultiOptions(String id, String field, String module, String type, List<String> values)
	{
		this.id = id;
		this.field = field;
		this.module = module;
		this.type = type;
		this.values = values;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getField()
	{
		return field;
	}

	public void setField(String field)
	{
		this.field = field;
	}

	public String getModule()
	{
		return module;
	}

	public void setModule(String module)
	{
		this.module = module;
	}

	public List<String> getValues()
	{
		return values;
	}

	public void setValues(List<String> values)
	{
		this.values = values;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String toString()
	{
		return "";
	}

	public String asStr()
	{
		return StringUtils.join(values, "|");
	}

}
