package au.net.webdirector.common.datalayer.base.db.entity;

import java.io.File;
import java.io.Serializable;

import au.net.webdirector.common.Defaults;

public class SharedFile implements Serializable
{
	private static final long serialVersionUID = -5787243646205279756L;

	protected String path;

	public SharedFile(String path)
	{
		this.path = path;
	}

	public String getPath()
	{
		return path;
	}

	public void setPath(String path)
	{
		this.path = path;
	}

	public File getFile()
	{
		return new File(Defaults.getInstance().getStoreDir(), path);
	}

	public String getStorePath()
	{
		return "/" + Defaults.getInstance().getStoreContext() + path;
	}

	public String toString()
	{
		return path;
	}
}
