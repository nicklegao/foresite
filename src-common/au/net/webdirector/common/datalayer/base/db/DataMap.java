package au.net.webdirector.common.datalayer.base.db;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import au.net.webdirector.common.datalayer.util.CaseInsensitiveMap;
import au.net.webdirector.common.datalayer.util.text.DataFormat;

/*
 * 
 */
public class DataMap extends CaseInsensitiveMap
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4944340043242242147L;

	private String table;

	public String getTable()
	{
		return table;
	}

	public void setTable(String table)
	{
		this.table = table;
	}

	protected DataHelper getHelper()
	{
		return DataHelper.getInstance();
	}

	public String getString(String key, DataFormat format)
	{
		return format.format(get(key));
	}

	public String getString(String key)
	{
		return getHelper().toString(get(key));
	}

	public boolean getBoolean(String key)
	{
		return getInt(key) > 0;
	}

	public int getInt(String key)
	{
		BigDecimal dec = getDecimal(key);
		return dec == null ? 0 : dec.intValue();
	}

	public long getLong(String key)
	{
		BigDecimal dec = getDecimal(key);
		return dec == null ? 0 : dec.longValue();
	}

	public double getDouble(String key)
	{
		BigDecimal dec = getDecimal(key);
		return dec == null ? 0 : dec.doubleValue();
	}

	public BigDecimal getDecimal(String key)
	{
		Object o = get(key);
		if (o == null || StringUtils.isBlank(o.toString()))
		{
			return null;
		}
		try
		{
			return new BigDecimal(o.toString());
		}
		catch (NumberFormatException e)
		{
			throw new RuntimeException("Cannot parse " + o.toString() + " to Decimal", e);
		}
	}

	public Date getDate(String key, DataFormat format)
	{
		Object o = get(key);
		if (o == null)
		{
			return null;
		}
		if (o instanceof String)
		{
			if (format == null)
			{
				format = getHelper().getDefaultDataFormat(new Date());
			}
			try
			{
				return (Date) format.parse((String) o);
			}
			catch (ParseException e)
			{
				throw new RuntimeException(e);
			}
		}
		return (Date) o;
	}

	public Date getDate(String key)
	{
		return getDate(key, null);
	}

	public String[] getFields()
	{
		return keySet().toArray(new String[] {});
	}

	public String[] getValues(String[] fields)
	{
		String[] values = new String[fields.length];
		int i = 0;
		for (String field : fields)
		{
			if (get(field) == null)
			{
				values[i] = null;
				i++;
				continue;
			}
			values[i] = getString(field);
			i++;
		}
		return values;
	}


	@Override
	public DataMap clone()
	{
		try
		{
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			ObjectOutputStream out = new ObjectOutputStream(byteStream);
			out.writeObject(this);
			out.close();
			byteStream.close();
			ByteArrayInputStream inputStream = new ByteArrayInputStream(byteStream.toByteArray());
			ObjectInputStream in = new ObjectInputStream(inputStream);
			DataMap newObj = (DataMap) in.readObject();
			in.close();
			inputStream.close();
			return newObj;
		}
		catch (IOException i)
		{
			return null;
		}
		catch (ClassNotFoundException c)
		{
			return null;
		}
	}
}
