package au.net.webdirector.common.datalayer.base.db;

import java.util.Date;

import au.net.webdirector.common.datalayer.util.text.DataFormat;
import au.net.webdirector.common.datalayer.util.text.DefaultBooleanFormat;
import au.net.webdirector.common.datalayer.util.text.DefaultDateFormat;
import au.net.webdirector.common.datalayer.util.text.DefaultObjectFormat;

public class DataHelper
{

	public static DataHelper getInstance()
	{
		return new DataHelper();
	}

	public String toString(Object object)
	{
		if (object == null)
		{
			return "";
		}
		return getDefaultDataFormat(object).format(object);
	}

	protected boolean support(Object o, Class[] supportedClass)
	{
		if (o == null)
		{
			return true;
		}
		for (Class clazz : supportedClass)
		{
			if (o.getClass().isAssignableFrom(clazz))
			{
				return true;
			}
		}
		return false;
	}

	protected Class[] getSupportedClasses()
	{
		// TODO: supplement the rest
		return new Class[] { java.sql.Timestamp.class, java.lang.String.class, java.lang.Integer.class, java.math.BigDecimal.class, java.lang.Float.class };
	}

	public DataFormat getDefaultDataFormat(Object object)
	{
		if (object instanceof Date)
		{
			return new DefaultDateFormat();
		}
		if (object instanceof Boolean)
		{
			return new DefaultBooleanFormat();
		}
		return new DefaultObjectFormat();
	}

}
