package au.net.webdirector.common.datalayer.base.db.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class StringMapper implements RowMapper<String>
{

	public String mapRow(ResultSet results, int arg1) throws SQLException
	{
		String value = results.getString(1);
		return value != null ? value : "";
	}

	private static StringMapper _instance = null;

	public synchronized static StringMapper getInstance()
	{
		if (_instance == null)
		{
			_instance = new StringMapper();
		}
		return _instance;
	}
}
