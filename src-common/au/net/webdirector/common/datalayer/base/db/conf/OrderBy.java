/**
 * @author Nick Yiming Gao
 * @date 26/11/2015
 */
package au.net.webdirector.common.datalayer.base.db.conf;

/**
 * 
 */

import org.apache.log4j.Logger;

public class OrderBy
{

	public static String ASC = "ASC";
	public static String DESC = "DESC";

	private static Logger logger = Logger.getLogger(OrderBy.class);

	protected String fieldName;

	protected String option;


	protected OrderBy(String fieldName, String option)
	{
		super();
		this.fieldName = fieldName;
		this.option = option;
	}


	public static OrderBy asc(String fieldName)
	{
		return new OrderBy(fieldName, ASC);
	}

	public static OrderBy desc(String fieldName)
	{
		return new OrderBy(fieldName, DESC);
	}

	@Override
	public String toString()
	{
		return fieldName + " " + option;
	}
}
