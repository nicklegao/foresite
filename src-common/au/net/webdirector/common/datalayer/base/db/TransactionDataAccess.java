package au.net.webdirector.common.datalayer.base.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.springframework.jdbc.support.JdbcUtils;

import au.net.webdirector.common.datalayer.base.db.conf.Condition;
import au.net.webdirector.common.datalayer.base.db.conf.OrderBy;
import au.net.webdirector.common.datalayer.base.db.mapper.AbstractDataMapMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.StringArrayMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.datalayer.base.transaction.Transactionable;
import au.net.webdirector.common.datalayer.util.StringArrayUtils;

public class TransactionDataAccess implements Transactionable
{

	public static TransactionDataAccess getInstance()
	{
		return getInstance(true);
	}

	public static TransactionDataAccess getInstance(boolean autoCommit)
	{
		// TODO: implement this
		return new TransactionDataAccess(autoCommit);
	}

	public static TransactionDataAccess getInstance(TransactionManager tm)
	{
		if (tm == null)
		{
			return getInstance();
		}
		else
		{
			return tm.getDataAccess();
		}
	}

	protected boolean autoCommit = true;
	protected Connection conn;

	protected TransactionDataAccess(boolean autoCommit)
	{
		this.autoCommit = autoCommit;
	}

	protected <T> List<T> doSelect(String query, RowMapper<T> mapper, String... args) throws SQLException
	{
		RowMapperResultSetExtractor<T> rse = new RowMapperResultSetExtractor<T>(mapper);
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try
		{
			prepareConnection();
			stmt = conn.prepareStatement(query);
			for (int i = 0; i < args.length; i++)
			{
				stmt.setObject(i + 1, args[i]);
			}
			rs = stmt.executeQuery();
			return rse.extractData(rs);
		}
		finally
		{
			JdbcUtils.closeResultSet(rs);
			JdbcUtils.closeStatement(stmt);
			if (autoCommit)
			{
				releaseConnection();
			}
		}
	}

	/*
	 * basic method execute query return String
	 */
	public List<String> selectStrings(QueryBuffer queryBuffer) throws SQLException
	{
		return selectStrings(queryBuffer.getSQL(), queryBuffer.getParams());
	}

	public List<String> selectStrings(String query, String... args) throws SQLException
	{
		return doSelect(query, new StringMapper(), args);
	}

	public List<String> selectStrings(String query) throws SQLException
	{
		return selectStrings(query, new String[] {});
	}

	public String selectString(QueryBuffer queryBuffer) throws SQLException
	{
		return selectString(queryBuffer.getSQL(), queryBuffer.getParams());
	}

	public String selectString(String query, String... args) throws SQLException
	{
		List<String> list = selectStrings(query, args);
		return list.size() > 0 ? list.get(0) : null;
	}

	public String selectString(String query) throws SQLException
	{
		return selectString(query, new String[] {});
	}

	/*
	 * basic method execute query return String[]
	 */
	public List<String[]> selectStringArrays(QueryBuffer queryBuffer) throws SQLException
	{
		return selectStringArrays(queryBuffer.getSQL(), queryBuffer.getParams());
	}

	public List<String[]> selectStringArrays(String query, String... args) throws SQLException
	{
		return doSelect(query, new StringArrayMapper(), args);
	}

	public List<String[]> selectStringArrays(String query) throws SQLException
	{
		return selectStringArrays(query, new String[] {});
	}

	public String[] selectStringArray(String query, String... args) throws SQLException
	{
		List<String[]> list = selectStringArrays(query, args);
		return list.size() > 0 ? list.get(0) : null;
	}

	public String[] selectStringArray(String query) throws SQLException
	{
		return selectStringArray(query, new String[] {});
	}

	/*
	 * basic method execute query return Generic DataMap
	 */
	public <T extends DataMap> List<T> selectMaps(QueryBuffer queryBuffer, AbstractDataMapMapper<T> mapper) throws SQLException
	{
		return selectMaps(queryBuffer.getSQL(), mapper, queryBuffer.getParams());
	}

	public <T extends DataMap> List<T> selectMaps(String query, AbstractDataMapMapper<T> mapper, String... args) throws SQLException
	{
		return doSelect(query, mapper, args);
	}

	//--------------------------------------
	public <T extends DataMap> List<T> selectMaps(String[] columns, String tableName, DataMap conditions, AbstractDataMapMapper<T> mapper) throws SQLException
	{
		return selectMaps(columns, tableName, Condition.instance(conditions), mapper);
	}

	public <T extends DataMap> List<T> selectMaps(String[] columns, String tableName, Condition conditions, AbstractDataMapMapper<T> mapper) throws SQLException
	{
		return selectMaps(columns, tableName, conditions, new ArrayList<OrderBy>(), mapper);
	}

	//--------------------------------------
	public <T extends DataMap> List<T> selectMaps(String[] columns, String tableName, DataMap conditions, List<OrderBy> orders, AbstractDataMapMapper<T> mapper) throws SQLException
	{
		return selectMaps(columns, tableName, Condition.instance(conditions), orders, mapper);
	}

	public <T extends DataMap> List<T> selectMaps(String[] columns, String tableName, Condition conditions, List<OrderBy> orders, AbstractDataMapMapper<T> mapper) throws SQLException
	{

		List<String> values = new ArrayList<String>();
		String sql = "select " + StringUtils.join(columns, ",") + " from " + tableName + " ";
		if (conditions != null && !conditions.isEmpty())
		{
			sql += " where " + conditions.getClause();
			values.addAll(conditions.getValues());
		}
		if (orders != null && orders.size() > 0)
		{
			sql += " order by " + StringUtils.join(orders, " , ");
		}
		return selectMaps(sql, mapper, values.toArray(new String[] {}));
	}

	/*
	 * basic method execute query return DataMap
	 */
	public List<DataMap> selectMaps(QueryBuffer queryBuffer) throws SQLException
	{
		return selectMaps(queryBuffer.getSQL(), queryBuffer.getParams());
	}

	public List<DataMap> selectMaps(String query, String... args) throws SQLException
	{
		return selectMaps(query, new AbstractDataMapMapper<DataMap>()
		{
			@Override
			public DataMap createNewRow()
			{
				return new DataMap();
			}
		}, args);
	}

	public List<DataMap> selectMaps(String query) throws SQLException
	{
		return selectMaps(query, new String[] {});
	}

	//------------------------------
	public List<DataMap> selectMaps(String[] columns, String tableName, DataMap conditions) throws SQLException
	{
		return selectMaps(columns, tableName, Condition.instance(conditions));
	}

	public List<DataMap> selectMaps(String[] columns, String tableName, Condition conditions) throws SQLException
	{
		return selectMaps(columns, tableName, conditions, new ArrayList<OrderBy>());
	}

	//------------------------------
	public List<DataMap> selectMaps(String[] columns, String tableName, DataMap conditions, List<OrderBy> orders) throws SQLException
	{
		return selectMaps(columns, tableName, Condition.instance(conditions), orders);
	}

	public List<DataMap> selectMaps(String[] columns, String tableName, Condition conditions, List<OrderBy> orders) throws SQLException
	{
		return selectMaps(columns, tableName, conditions, orders, new AbstractDataMapMapper<DataMap>()
		{
			@Override
			public DataMap createNewRow()
			{
				return new DataMap();
			}
		});
	}

	public DataMap selectMap(QueryBuffer queryBuffer) throws SQLException
	{
		return selectMap(queryBuffer.getSQL(), queryBuffer.getParams());
	}

	public DataMap selectMap(String query, String... args) throws SQLException
	{
		List<DataMap> list = selectMaps(query, args);
		return list.size() > 0 ? list.get(0) : null;
	}

	public DataMap selectMap(String query) throws SQLException
	{
		return selectMap(query, new String[] {});
	}

	//------------------------------
	public DataMap selectMap(String[] columns, String tableName, DataMap conditions) throws SQLException
	{
		return selectMap(columns, tableName, Condition.instance(conditions));
	}

	public DataMap selectMap(String[] columns, String tableName, Condition conditions) throws SQLException
	{
		return selectMap(columns, tableName, conditions, new ArrayList<OrderBy>());
	}

	//------------------------------
	public DataMap selectMap(String[] columns, String tableName, DataMap conditions, List<OrderBy> orders) throws SQLException
	{
		return selectMap(columns, tableName, Condition.instance(conditions), orders);
	}

	public DataMap selectMap(String[] columns, String tableName, Condition conditions, List<OrderBy> orders) throws SQLException
	{
		List<DataMap> list = selectMaps(columns, tableName, conditions, orders);
		return list.size() > 0 ? list.get(0) : null;
	}

	protected int doUpdate(String query, boolean getGeneratedKey, Object... args) throws SQLException
	{
		PreparedStatement stmt = null;
		try
		{
			prepareConnection();
			if (!getGeneratedKey)
			{
				stmt = conn.prepareStatement(query);
			}
			else
			{
				stmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			}
			for (int i = 0; i < args.length; i++)
			{
				stmt.setObject(i + 1, args[i]);
			}
			int result = stmt.executeUpdate();
			if (!getGeneratedKey)
			{
				return result;
			}
			ResultSet generatedKeys = stmt.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next())
			{
				result = generatedKeys.getInt(1);
			}
			return result;
		}
		finally
		{
			JdbcUtils.closeStatement(stmt);
			if (autoCommit)
			{
				releaseConnection();
			}
		}
	}

	/*
	 * basic method execute update return number of effected rows
	 */
	public int update(QueryBuffer queryBuffer) throws SQLException
	{
		return update(queryBuffer.getSQL(), queryBuffer.getParams());
	}

	public int update(String query, Object... args) throws SQLException
	{
		return doUpdate(query, false, args);
	}

	public int update(String query) throws SQLException
	{
		return update(query, new String[] {});
	}

	public int insert(QueryBuffer queryBuffer) throws SQLException
	{
		return insert(queryBuffer.getSQL(), queryBuffer.getParams());
	}

	public int insert(String query, Object... args) throws SQLException
	{
		return doUpdate(query, true, args);
	}

	public int insert(String query) throws SQLException
	{
		return insert(query, new String[] {});
	}

	/*
	 * derived method
	 */
	public int insert(DataMap data, String tableName) throws SQLException
	{
		String[] fields = data.getFields();
		String[] questionMarks = new String[fields.length];
		StringArrayUtils.fillArray(questionMarks, "?");
		String[] values = data.getValues(fields);
		String sql = "insert into " + tableName + " (" + StringUtils.join(fields, ",") + ") values (" + StringUtils.join(questionMarks, ",") + ")";
		return insert(sql, values);
	}

	public int update(DataMap data, String tableName, DataMap conditions) throws SQLException
	{

		String[] fields = data.getFields();
		List<String> values = new ArrayList<String>();
		values.addAll(Arrays.asList(data.getValues(fields)));

		for (int i = 0; i < fields.length; i++)
		{
			fields[i] += "=?";
		}
		List<String> whereFields = new ArrayList<String>();
		for (String key : conditions.keySet())
		{
			String value = conditions.getString(key);
			if (StringUtils.isBlank(value))
			{
				// key is special condition needs to be added to where clause
				whereFields.add(key);
				continue;
			}
			whereFields.add(key + "=?");
			values.add(value);
		}

		String sql = "update " + tableName + " set " + StringUtils.join(fields, ",") + " ";
		if (whereFields.size() > 0)
		{
			sql += " where " + StringUtils.join(whereFields, " and ");
		}
		return update(sql, values.toArray(new String[] {}));
	}

	public int delete(String tableName, DataMap conditions) throws SQLException
	{
		List<String> values = new ArrayList<String>();
		List<String> whereFields = new ArrayList<String>();
		for (String key : conditions.keySet())
		{
			String value = conditions.getString(key);
			if (StringUtils.isBlank(value))
			{
				// key is special condition needs to be added to where clause
				whereFields.add(key);
				continue;
			}
			whereFields.add(key + "=?");
			values.add(value);
		}

		String sql = "delete from " + tableName + " ";
		if (whereFields.size() > 0)
		{
			sql += " where " + StringUtils.join(whereFields, " and ");
		}
		return update(sql, values.toArray(new String[] {}));
	}

	@Override
	public void commit()
	{
		if (!autoCommit)
		{
			try
			{
				if (conn != null)
				{
					conn.commit();
				}
			}
			catch (SQLException e)
			{
			}
			finally
			{
				releaseConnection();
			}
		}
	}

	@Override
	public void rollback()
	{
		if (!autoCommit)
		{
			try
			{
				if (conn != null)
				{
					conn.rollback();
				}
			}
			catch (SQLException e)
			{
			}
			finally
			{
				releaseConnection();
			}
		}
	}

	@Override
	public void finalize() throws Throwable
	{
		super.finalize();
	}

	protected void releaseConnection()
	{
		try
		{
			if (conn != null)
			{
				ConnectionPoolManager.getInstance().releaseConnection(conn);
				conn = null;
			}
		}
		catch (SQLException ex)
		{
			System.err.println("Could not close JDBC Connection");
			ex.printStackTrace();
		}
		catch (Throwable ex)
		{
			System.err.println("Unexpected exception on closing JDBC Connection");
			ex.printStackTrace();
		}
	}

	protected void prepareConnection()
	{
		try
		{
			if (conn == null)
			{
				conn = ConnectionPoolManager.getInstance().getConnection();
				conn.setAutoCommit(autoCommit);
			}
		}
		catch (SQLException ex)
		{
			throw new CannotGetJdbcConnectionException("Error in getting JDBC Connection", ex);
		}
	}
}
