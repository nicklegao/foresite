package au.net.webdirector.common.datalayer.base.transaction;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccess;
import au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory;

public class TransactionManager implements AutoCloseable
{
	private static Logger logger = Logger.getLogger(TransactionManager.class);

	TransactionDataAccess dataAccess;
	StoreFileAccess fileAccess;
	List<CommittingAction> committingActions = new ArrayList<CommittingAction>();
	boolean commitOrRollbackCalled = false;

	public static TransactionManager getInstance(TransactionDataAccess da, StoreFileAccess fa)
	{
		TransactionManager inst = new TransactionManager(da, fa);
		return inst;
	}

	public static TransactionManager getInstance(TransactionDataAccess da)
	{
		return getInstance(da, null);
	}

	public static TransactionManager getInstance(StoreFileAccess fa)
	{
		return getInstance(null, fa);
	}

	public static TransactionManager getInstance()
	{
		return getInstance(null, null);
	}

	TransactionManager(TransactionDataAccess da, StoreFileAccess fa)
	{
		super();
		this.dataAccess = da;
		this.fileAccess = fa;
	}

	public void commit()
	{
		commitOrRollbackCalled = true;
		if (dataAccess != null)
		{
			dataAccess.commit();
		}
		if (fileAccess != null)
		{
			fileAccess.commit();
		}
		while (!committingActions.isEmpty())
		{
			try
			{
				committingActions.remove(0).doAction();
			}
			catch (Exception e)
			{
				logger.warn("Error in ending action in commit.", e);
			}
		}
	}

	public void rollback()
	{
		commitOrRollbackCalled = true;
		if (dataAccess != null)
		{
			dataAccess.rollback();
		}
		if (fileAccess != null)
		{
			fileAccess.rollback();
		}
		if (!committingActions.isEmpty())
		{
			committingActions.clear();
		}
	}

	public TransactionDataAccess getDataAccess()
	{
		if (dataAccess == null)
		{
			synchronized (this)
			{
				if (dataAccess == null)
				{
					dataAccess = TransactionDataAccess.getInstance(false);
				}
			}
		}
		return dataAccess;
	}

	public StoreFileAccess getFileAccess()
	{
		if (fileAccess == null)
		{
			synchronized (this)
			{
				if (fileAccess == null)
				{
					fileAccess = StoreFileAccessFactory.getInstance(false);
				}
			}
		}
		return fileAccess;
	}

	public void registerCommittingAction(CommittingAction action)
	{
		this.committingActions.add(action);
	}

	@Override
	public void close() throws Exception
	{
		if (committingActions.size() > 0)
		{
			logger.fatal("Did you forget to commit? Pending transactions.size( " + committingActions.size());
		}
		else if (!commitOrRollbackCalled)
		{
			logger.fatal("Did you forget to commit? commitOrRollbackCalled = FALSE");
		}
		rollback();
	}
}
