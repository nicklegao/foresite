package au.net.webdirector.common.datalayer.base.transaction;

public interface Transactionable
{

	public void commit();

	public void rollback();
}
