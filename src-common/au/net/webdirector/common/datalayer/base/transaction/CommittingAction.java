/**
 * @author Nick Yiming Gao
 * @date 2016-2-18
 */
package au.net.webdirector.common.datalayer.base.transaction;

public interface CommittingAction
{
	public void doAction();
}
