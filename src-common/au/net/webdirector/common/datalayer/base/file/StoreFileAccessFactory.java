package au.net.webdirector.common.datalayer.base.file;

public class StoreFileAccessFactory
{

	public static StoreFileAccess getInstance()
	{
		return getInstance(true);
	}

	public static StoreFileAccess getInstance(boolean autoCommit)
	{
		if (autoCommit)
		{
			return new CommonStoreFileAccess();
		}
		if (!TransactionStoreFileAccess.isManagerRunning)
		{
			throw new RuntimeException("File Resource Manager not Running!");
		}
		return new TransactionStoreFileAccess();
	}
}
