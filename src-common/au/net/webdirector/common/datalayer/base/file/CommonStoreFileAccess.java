package au.net.webdirector.common.datalayer.base.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class CommonStoreFileAccess implements StoreFileAccess
{

	@Override
	public void commit()
	{

	}

	@Override
	public void rollback()
	{

	}

	@Override
	public void cleanDirectory(File directory) throws IOException
	{
		FileUtils.cleanDirectory(directory);
	}

	@Override
	public void copy(File src, File desc) throws IOException
	{
		if (src.isDirectory())
		{
			FileUtils.copyDirectory(src, desc);
			return;
		}
		FileUtils.copyFile(src, desc);
	}

	@Override
	public void copyTo(File src, File destDir) throws IOException
	{
		if (src.isDirectory())
		{
			FileUtils.copyDirectoryToDirectory(src, destDir);
			return;
		}
		FileUtils.copyFileToDirectory(src, destDir);
	}

	@Override
	public void copyInputStream(InputStream source, File destination) throws IOException
	{
		OutputStream output = null;
		try
		{
			if (!destination.getParentFile().exists())
			{
				destination.getParentFile().mkdirs();
			}
			output = new FileOutputStream(destination);
			IOUtils.copy(source, output);
		}
		finally
		{
			IOUtils.closeQuietly(output);
		}
	}

	@Override
	public void delete(File fileOrDir) throws IOException
	{
		if (fileOrDir.isDirectory())
		{
			FileUtils.deleteDirectory(fileOrDir);
			return;
		}
		FileUtils.forceDelete(fileOrDir);
	}

	@Override
	public void move(File src, File dest) throws IOException
	{
		if (src.isDirectory())
		{
			FileUtils.moveDirectory(src, dest);
			return;
		}
		FileUtils.moveFile(src, dest);
	}

	@Override
	public void moveTo(File src, File destDir) throws IOException
	{
		if (src.isDirectory())
		{
			FileUtils.moveDirectoryToDirectory(src, destDir, true);
			return;
		}
		FileUtils.moveFileToDirectory(src, destDir, true);
	}

	@Override
	public void writeStringToFile(File file, String data) throws IOException
	{
		FileUtils.writeStringToFile(file, data);
	}

	@Override
	public InputStream openInputStream(File src) throws IOException
	{
		return FileUtils.openInputStream(src);
	}

	@Override
	public boolean exists(File file) throws IOException
	{
		return file.exists();
	}
}
