package au.net.webdirector.common.datalayer.base.file;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.transaction.file.FileResourceManager;
import org.apache.commons.transaction.file.ResourceManagerException;
import org.springframework.transaction.TransactionException;

public class TransactionStoreFileAccess implements StoreFileAccess
{

	public static FileResourceManager manager;
	public static boolean isManagerRunning;

	protected String txId;

	protected TransactionStoreFileAccess()
	{
		try
		{
			txId = manager.generatedUniqueTxId();
			manager.startTransaction(txId);
		}
		catch (ResourceManagerException e)
		{
			throw new RuntimeException("Cannot get Transaction ID", e);
		}
	}

	private void checkFileLocation(File file)
	{
		if (file.getAbsolutePath().indexOf(new File(manager.getStoreDir()).getAbsolutePath()) == -1)
		{
			throw new InvalidParameterException("File[" + file.getAbsolutePath() + "] should be in store[" + new File(manager.getStoreDir()).getAbsolutePath() + "] ");
		}
	}

	private String getRelativePath(File file)
	{
		checkFileLocation(file);
		return file.getAbsolutePath().substring(new File(manager.getStoreDir()).getAbsolutePath().length());
	}

	@Override
	public void commit()
	{
		try
		{
			manager.prepareTransaction(txId);
			manager.commitTransaction(txId);
			txId = manager.generatedUniqueTxId();
			manager.startTransaction(txId);
		}
		catch (ResourceManagerException e)
		{
			throw new TransactionException("File Manager Commit : " + txId, e)
			{
				private static final long serialVersionUID = 8148651386045014203L;
			};
		}
	}

	@Override
	public void rollback()
	{
		try
		{
			manager.rollbackTransaction(txId);
			txId = manager.generatedUniqueTxId();
			manager.startTransaction(txId);
		}
		catch (ResourceManagerException e)
		{
			throw new TransactionException("File Manager Commit : " + txId, e)
			{
				private static final long serialVersionUID = -7624797788477663351L;
			};
		}
	}

	@Override
	public void cleanDirectory(File directory) throws IOException
	{
		File[] files = directory.listFiles();
		if (files == null)
		{
			return;
		}
		for (File file : files)
		{
			delete(file);
		}
	}

	@Override
	public void copy(File src, File dest) throws IOException
	{
		if (src.isDirectory())
		{
			copyDir(src, dest);
			return;
		}
		copyFile(src, dest);

	}

	private void copyFile(File src, File dest) throws IOException
	{
		try
		{
			manager.copyResource(txId, getRelativePath(src), getRelativePath(dest), true);
		}
		catch (ResourceManagerException e)
		{
			throw new IOException(e);
		}
		catch (InvalidParameterException e)
		{
			InputStream input = null;
			try
			{
				input = FileUtils.openInputStream(src);
				copyInputStream(input, dest);
			}
			finally
			{
				IOUtils.closeQuietly(input);
			}
		}
	}

	private void copyDir(File srcDir, File destDir) throws IOException
	{
		for (File f : srcDir.listFiles())
		{
			if (f.isDirectory())
			{
				copyDir(f, new File(destDir, f.getName()));
				continue;
			}
			copyFile(f, new File(destDir, f.getName()));
		}
	}

	@Override
	public void copyTo(File src, File destDir) throws IOException
	{
		File dest = new File(destDir, src.getName());
		copy(src, dest);
	}

	@Override
	public void copyInputStream(InputStream source, File destination) throws IOException
	{
		OutputStream output = null;
		try
		{
			output = manager.writeResource(txId, getRelativePath(destination), false);
			IOUtils.copy(source, output);
		}
		catch (ResourceManagerException e)
		{
			throw new IOException(e);
		}
		finally
		{
			IOUtils.closeQuietly(output);
		}
	}

	@Override
	public void delete(File fileOrDir) throws IOException
	{
		try
		{
			manager.deleteResource(txId, getRelativePath(fileOrDir), true);
		}
		catch (ResourceManagerException e)
		{
			throw new IOException(e);
		}
	}

	@Override
	public void move(File src, File dest) throws IOException
	{
		try
		{
			manager.moveResource(txId, getRelativePath(src), getRelativePath(dest), true);
		}
		catch (ResourceManagerException e)
		{
			throw new IOException(e);
		}
	}

	@Override
	public void moveTo(File src, File destDir) throws IOException
	{
		File dest = new File(destDir, src.getName());
		move(src, dest);
	}

	@Override
	public void writeStringToFile(File file, String data) throws IOException
	{
		OutputStream output = null;
		try
		{
			output = manager.writeResource(txId, getRelativePath(file), false);
			IOUtils.write(data, output);
			output.flush();
		}
		catch (ResourceManagerException e)
		{
			throw new IOException(e);
		}
		finally
		{
			IOUtils.closeQuietly(output);
		}
	}

	@Override
	public InputStream openInputStream(File src) throws IOException
	{
		try
		{
			return manager.readResource(txId, getRelativePath(src));
		}
		catch (ResourceManagerException e)
		{
			throw new IOException(e);
		}
		catch (InvalidParameterException e)
		{
			return FileUtils.openInputStream(src);
		}
	}

	@Override
	public boolean exists(File file) throws IOException
	{
		checkFileLocation(file);
		return file.exists();
	}
}