package au.net.webdirector.common.datalayer.base.file;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import au.net.webdirector.common.datalayer.base.transaction.Transactionable;

public interface StoreFileAccess extends Transactionable
{

	public void cleanDirectory(File directory) throws IOException;

	public void copy(File src, File desc) throws IOException;

	public void copyTo(File src, File destDir) throws IOException;

	public void copyInputStream(InputStream source, File destination) throws IOException;

	public void delete(File fileOrDir) throws IOException;

	public void move(File src, File dest) throws IOException;

	public void moveTo(File src, File destDir) throws IOException;

	public void writeStringToFile(File file, String data) throws IOException;

	public InputStream openInputStream(File src) throws IOException;

	public boolean exists(File file) throws IOException;
}
