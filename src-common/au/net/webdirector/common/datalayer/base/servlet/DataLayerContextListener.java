package au.net.webdirector.common.datalayer.base.servlet;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.io.FileUtils;
import org.apache.commons.transaction.file.FileResourceManager;
import org.apache.commons.transaction.file.ResourceManager;
import org.apache.commons.transaction.file.ResourceManagerSystemException;
import org.apache.commons.transaction.util.Log4jLogger;
import org.apache.log4j.Logger;
import org.springframework.web.context.support.XmlWebApplicationContext;

import au.net.webdirector.common.datalayer.base.db.ConnectionPoolManager;
import au.net.webdirector.common.datalayer.base.file.TransactionStoreFileAccess;

public class DataLayerContextListener implements ServletContextListener
{

	FileResourceManager frm;

	@Override
	public void contextDestroyed(ServletContextEvent contextEvent)
	{
		destroyFileTransaction(contextEvent);
	}

	@Override
	public void contextInitialized(ServletContextEvent contextEvent)
	{
		initDataTransaction(contextEvent);
		initFileTransaction(contextEvent);

	}

	private void initDataTransaction(ServletContextEvent contextEvent)
	{
		XmlWebApplicationContext appContext = new XmlWebApplicationContext();
		appContext.setServletContext(contextEvent.getServletContext());
		appContext.refresh();
		ConnectionPoolManager.getInstance().setAppContext(appContext);
	}

	private void initFileTransaction(ServletContextEvent contextEvent)
	{
		String storeDir = contextEvent.getServletContext().getInitParameter("storeDir");
		String workDir = contextEvent.getServletContext().getInitParameter("file-resource-manager-work");
		try
		{
			File f = new File(workDir);
			if (f.exists())
			{
				FileUtils.cleanDirectory(f);
			}
			else
			{
				f.mkdirs();
			}
		}
		catch (IOException e1)
		{
			e1.printStackTrace();
		}
		TransactionStoreFileAccess.manager = new FileResourceManager(storeDir, workDir, false, new Log4jLogger(Logger.getLogger(FileResourceManager.class)));
		try
		{
			TransactionStoreFileAccess.manager.start();
			TransactionStoreFileAccess.isManagerRunning = true;
		}
		catch (ResourceManagerSystemException e)
		{
			e.printStackTrace();
		}

	}

	private void destroyFileTransaction(ServletContextEvent contextEvent)
	{
		try
		{
			TransactionStoreFileAccess.manager.stop(ResourceManager.SHUTDOWN_MODE_ROLLBACK);
			TransactionStoreFileAccess.isManagerRunning = false;
		}
		catch (ResourceManagerSystemException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String workDir = contextEvent.getServletContext().getInitParameter("file-resource-manager-work");
		try
		{
			FileUtils.cleanDirectory(new File(workDir));
		}
		catch (IOException e1)
		{
			e1.printStackTrace();
		}
	}

}
