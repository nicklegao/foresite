package au.net.webdirector.common.datalayer.lucene;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.client.EntityType;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.ModuleData;
import au.net.webdirector.common.search.Search;

public class LuceneHelper
{

	private static Logger logger = Logger.getLogger(LuceneHelper.class);
	private static Defaults d = Defaults.getInstance();

	public static String QUEUE_NAME = "for_lucene_index";

	public static boolean luceneSearchOn()
	{
		return !d.getLuceneIndexDir().equals("");
	}

	ModuleAccess moduleAccess;
	String module;

	public File getIndexFolder()
	{
		File indexFolder = new File(d.getLuceneIndexDir(), module);
		if (!indexFolder.exists())
		{
			indexFolder.mkdirs();
		}
		return indexFolder;
	}

	/**
	 * @param isElement
	 *            - true if dealing with elements false if dealing with
	 *            categories
	 * @param module
	 *            - the name of the module NEWS, INFORMATIOn etc.
	 * @param id
	 *            - the element of category ID
	 */

	// Single item Vector containing an asset or a category represented by a
	// hashtable

	public LuceneHelper(String module)
	{
		this.moduleAccess = ModuleAccess.getInstance();
		this.module = module;
	}

	public List<String> searchElements(String query)
	{
		List<String> ids = new ArrayList<String>();
		Search search = new Search(module);
		ids.addAll(search.searchTree(query, "element"));
		return ids;
	}

	public List<String> searchCategories(String query)
	{
		List<String> ids = new ArrayList<String>();
		Search search = new Search(module);
		ids.addAll(search.searchTree(query, "category"));
		return ids;
	}

	public void insert(EntityType type, String id)
	{
		ModuleData record = null;
		try
		{
			record = EntityType.ELEMENT.equals(type) ? moduleAccess.getElementById(module, id) : moduleAccess.getCategoryById(module, id);
		}
		catch (Exception e)
		{
			return;
		}
		if (record == null)
		{
			return;
		}
		IndexWriter indexWriter = null;
		try
		{
			Directory directory = FSDirectory.open(getIndexFolder());
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_46, new WhitespaceAnalyzer(Version.LUCENE_46));
			indexWriter = new IndexWriter(directory, config);

			Document doc = createDocument(record);
			indexWriter.addDocument(doc);
		}
		catch (Exception e)
		{

		}
		finally
		{
			if (indexWriter != null)
			{
				try
				{
					indexWriter.close();
				}
				catch (IOException e)
				{
				}
			}
		}
	}

	public void update(EntityType type, String id)
	{
		ModuleData record = null;
		try
		{
			record = EntityType.ELEMENT.equals(type) ? moduleAccess.getElementById(module, id) : moduleAccess.getCategoryById(module, id);
		}
		catch (Exception e)
		{
			return;
		}
		if (record == null)
		{
			return;
		}
		IndexWriter indexWriter = null;
		try
		{
			// below will create a new index based on the moduleName if it does
			// not exist or append to an existing one.
			Directory directory = FSDirectory.open(getIndexFolder());
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_46, new WhitespaceAnalyzer(Version.LUCENE_46));
			indexWriter = new IndexWriter(directory, config);
			Document doc = createDocument(record);
			Term term = new Term("uid", record.getType() + id);
			indexWriter.updateDocument(term, doc);
		}
		catch (Exception e)
		{
		}
		finally
		{
			if (indexWriter != null)
			{
				try
				{
					indexWriter.close();
				}
				catch (IOException e)
				{
				}
			}
		}

	}

	public void delete(EntityType type, String id)
	{
		IndexWriter indexWriter = null;
		try
		{
			Directory directory = FSDirectory.open(getIndexFolder());
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_46, new WhitespaceAnalyzer(Version.LUCENE_46));
			indexWriter = new IndexWriter(directory, config);
			Term term = new Term("uid", (EntityType.ELEMENT.equals(type) ? "element" : "category") + id);
			indexWriter.deleteDocuments(term);
		}
		catch (Exception e)
		{
		}
		finally
		{
			if (indexWriter != null)
			{
				try
				{
					indexWriter.close();
				}
				catch (IOException e)
				{
				}
			}
		}
	}

	private Document createDocument(ModuleData record) throws Exception
	{

		Document doc = new Document();
		ModuleFieldRanker ranker = new ModuleFieldRanker(module, record.isElement(), moduleAccess);
		List<Field> fields = ranker.getLuceneFields(record);
		for (Field field : fields)
		{
			doc.add(field);
		}
		return doc;
	}
}
