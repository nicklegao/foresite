package au.net.webdirector.common.datalayer.lucene;

import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;


public class SearchNode
{

	public SearchNode(String id, String type)
	{
		this.id = id;
		this.type = type;
	}

	public SearchNode()
	{
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public ArrayList getChildren()
	{
		return children;
	}

	public SearchNode getLastChild()
	{
		if (children.size() == 0)
		{
			return null;
		}
		return children.get(children.size() - 1);
	}

	public void addChild(SearchNode child)
	{
		children.add(child);
	}

	String type = "";
	String name = "";
	String id = "";

	String cat = "";
	String page = "";
	int extra = 0;
	int perpage = 0;
	int privilege = 0;

	public int getPrivilege()
	{
		return privilege;
	}

	public void setPrivilege(int privilege)
	{
		this.privilege = privilege;
	}

	public int getPerpage()
	{
		return perpage;
	}

	public void setPerpage(int perpage)
	{
		this.perpage = perpage;
	}

	public int getExtra()
	{
		return extra;
	}

	public void addExtra()
	{
		this.extra++;
	}

	public String getPage()
	{
		return page;
	}

	public void setPage(String page)
	{
		this.page = page;
	}

	public String getCat()
	{
		return cat;
	}

	public void setCat(String cat)
	{
		this.cat = cat;
	}

	ArrayList<SearchNode> children = new ArrayList();

	public void setChildren(ArrayList<SearchNode> children)
	{
		this.children = children;
	}

	int rootSize = 0; // only used by the root node to keep track of size
	String folderLevel = "";

	public void removeChildren()
	{
		children = new ArrayList();
	}

	public String getFolderLevel()
	{
		return folderLevel;
	}

	public void setFolderLevel(String folderLevel)
	{
		this.folderLevel = folderLevel;
	}

	public int getRootSize()
	{
		return rootSize;
	}

	public void incrementRootSize()
	{
		rootSize++;
	}

	/**
	 * Returns object in JSON form
	 */
	public StringBuffer toString(int levels)
	{
		StringBuffer ret = new StringBuffer("");
		try
		{

			if (levels != 0)
			{
				if (!type.equals(""))
				{
					ret.append("{'type':'" + type + "', 'text':'" + name.replaceAll("'", "&#039;") + "', 'id':'"
							+ id + "', 'cat':'" + cat + "', 'privilege':'" + privilege + "'");
					if (StringUtils.isNotBlank(page))
					{
						ret.append(", 'page':'" + page + "', 'extra':'" + extra + "', 'perpage':'" + perpage + "'");
					}
					if (children.size() != 0 && levels != 1)
					{
						ret.append(", 'children':[");
						for (int i = 0; i < children.size(); i++)
						{
							SearchNode o = children.get(i);
							ret.append(o.toString(levels - 1));
							if (i != children.size() - 1)
							{
								ret.append(",");
							}
						}
						ret.append("]");
					}
					else if (children.size() != 0 && levels == 1)
					{
						ret.append(", 'children':'request'");
					}
					ret.append("}");
				}
				else
				{
					ret.append("[");
					for (int i = 0; i < children.size(); i++)
					{
						SearchNode o = children.get(i);
						ret.append(o.toString(levels - 1));
						if (i != children.size() - 1)
						{
							ret.append(",");
						}
					}
					ret.append("]");
				}
			}


		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return ret;
	}


	public String toString(String levelss)
	{
		return toString(Integer.parseInt(levelss)).toString();
	}

	public String printPage(int pageSize, int pageNum)
	{
		try
		{
			String ret = "[";
			for (int i = ((pageSize * pageNum) - pageSize); i < ((pageSize * pageNum)) && i < children.size(); i++)
			{
				if (children.get(i) != null)
				{
					ret += children.get(i).toString(1) + ",";
				}
			}
			if ((pageSize * pageNum) < children.size())
			{
				ret += "{'type':'more','page':" + pageNum + ",'cat':'" + id + "'}";
			}
			else
			{
				ret = ret.substring(0, ret.length() - 1);
			}

			ret += "]";
			return ret;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return e.getMessage();
		}
	}

}
