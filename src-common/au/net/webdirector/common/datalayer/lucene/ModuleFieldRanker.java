package au.net.webdirector.common.datalayer.lucene;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.pdfbox.io.RandomAccessBuffer;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.db.entity.StoreFile;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.ModuleData;
import au.net.webdirector.common.datalayer.client.ModuleHelper;

//import webdirector.db.DBaccess;

public class ModuleFieldRanker
{

	private String module;
	private boolean isAsset;
	ModuleAccess moduleAccess;
	private Map<String, RankedField> rankedFields;

	public ModuleFieldRanker(String module, boolean isAsset, ModuleAccess moduleAccess)
	{
		super();
		this.module = module;
		this.isAsset = isAsset;
		this.moduleAccess = moduleAccess;
		rankedFields = genRankedFields();
	}

	protected TransactionDataAccess getDataAccess()
	{
		return moduleAccess.getDataAccess();
	}


	private Map<String, RankedField> genRankedFields()
	{
		Map<String, RankedField> fields = new LinkedHashMap<String, RankedField>();
		try
		{
			String tableName = (isAsset ? "" : "category") + "labels_" + module;
			List<String[]> rs = getDataAccess().selectStringArrays("select label_internalname, label_tokenized, label_rank from " + tableName + " order by 2 desc, 3 desc");
			for (String[] r : rs)
			{
				if ("-1".equals(r[2]))
				{
					continue;
				}
				if ("0".equals(r[1]))
				{
					RankedField field = new RankedField(r[0], Integer.parseInt(r[2]), false);
					field.addFieldName(r[0]);
					fields.put(r[0], field);
					continue;
				}
				String fieldName = "RANK_" + r[2];
				RankedField field = null;
				if (fields.containsKey(fieldName))
				{
					field = fields.get(fieldName);
				}
				else
				{
					field = new RankedField(fieldName, Integer.parseInt(r[2]), true);
					fields.put(fieldName, field);
				}
				field.addFieldName(r[0]);
			}
		}
		catch (Exception e)
		{
		}
		return fields;
	}

	public List<String> getRankedFieldNames()
	{
		return new ArrayList<String>(rankedFields.keySet());
	}

	public Map<String, RankedField> getRankedFields()
	{
		return rankedFields;
	}

	public RankedField getRankedField(String fieldName)
	{
		return rankedFields.get(fieldName);
	}

	public List<Field> getLuceneFields(ModuleData data)
	{
		List<Field> fields = new ArrayList<Field>();
		// String id = data.getId();
		fields.add(createField("id", data.getId(), true));
		fields.add(createField("keyColumn", data.getType(), true));
		fields.add(createField("uid", data.getType() + data.getId(), true));
		fields.add(createField("name", data.getName(), true));
		fields.add(createField("module", module, true));
		StringBuffer all = new StringBuffer();
		for (String fieldName : rankedFields.keySet())
		{
			RankedField rankedField = rankedFields.get(fieldName);
			StringBuffer content = new StringBuffer();
			try
			{
				for (String name : rankedField.getFieldNames())
				{
					name = name.toLowerCase();
					String value = "";
					if (name.startsWith("attrlong_") || name.startsWith("attrtext_"))
					{
						value = " " + ModuleHelper.readStringFromFile(data, name);
					}
					else if (name.startsWith("attrfile_"))
					{
						value = " " + getFileContents(data.getStoreFile(name));
					}
					else
					{
						value = " " + data.getString(name);
					}
					content.append(value);
					all.append(value);
				}
			}
			catch (IOException e)
			{
			}
			if (content.length() == 0)
			{
				continue;
			}
			Field field = createField(fieldName, content.toString().toLowerCase(), true);
			field.setBoost(rankedField.getRank());
			fields.add(field);
		}

		// add live flag
		fields.add(createField("live", data.logicalLive() ? "1" : "0", true));
		// to make it compatible with old version
		fields.add(createField("content", all.toString().toLowerCase(), true));
		return fields;
	}

	private String getFileContents(StoreFile storeFile)
	{
		if (storeFile == null)
		{
			return "";
		}
		File file = storeFile.getStoredFile();
		InputStream input = null;
		try
		{
			input = ModuleHelper.openInputStream(file);
			String fileExt = getExtension(file);
			// deal with all plaintext or markup related text files
			if (fileExt.equalsIgnoreCase("txt") || fileExt.equalsIgnoreCase("html") || fileExt.equalsIgnoreCase("htm") || fileExt.equalsIgnoreCase("csv") || fileExt.equalsIgnoreCase("xml"))
			{
				return ModuleHelper.readString(input);
			}
			if (fileExt.equalsIgnoreCase("pdf"))
			{
				return getPDFtext(input);
			}
			if (fileExt.equalsIgnoreCase("doc"))
			{
				return getHWPFWordText(input);
			}
			if (fileExt.equalsIgnoreCase("docx"))
			{
				return getXWPFWordText(input);
			}
			// TODO excel, powerpoint, open office etc...

			// non-handled file types
			return "";
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return "";
		}
		finally
		{
			IOUtils.closeQuietly(input);
		}
	}

	private String getHWPFWordText(InputStream input)
	{
		try
		{
			WordExtractor extractText = new WordExtractor(new HWPFDocument(input));
			return extractText.getText();
		}
		catch (Exception e)
		{
			return "";
		}
	}

	private String getXWPFWordText(InputStream input)
	{
		try
		{
			XWPFWordExtractor extractText = new XWPFWordExtractor(new XWPFDocument(input));
			return extractText.getText();
		}
		catch (Exception e)
		{
			return "";
		}
	}

	private String getPDFtext(InputStream input)
	{
		PDDocument pdfDocument = null;
		try
		{
			PDFParser parser = new PDFParser(new RandomAccessBuffer(input));
			parser.parse();
			pdfDocument = parser.getPDDocument();
			PDFTextStripper stripper = new PDFTextStripper();
			String contents = stripper.getText(pdfDocument);

			return contents;
		}
		catch (Exception e)
		{
			return "";
		}
		finally
		{
			if (null != pdfDocument)
				try
				{
					pdfDocument.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}

		}
	}

	private String getExtension(File file)
	{
		String filename = file.getName();
		// where the last dot is. There may be more than one.
		int dotPlace = filename.lastIndexOf('.');
		if (dotPlace == -1)
		{
			// possibly empty
			return "";
		}
		return filename.substring(dotPlace + 1);
	}

	private Field createField(String name, String value, boolean stored)
	{
		FieldType type = new FieldType();
		type.setIndexed(true);
		type.setStored(stored);
		type.setStoreTermVectors(true);
		return new Field(name, StringEscapeUtils.unescapeHtml(value), type);
	}

	public class RankedField
	{
		String rankedName;
		int rank;
		boolean tokenized;
		List<String> fieldNames = new ArrayList<String>();

		private RankedField(String rankedName, int rank, boolean tokenized)
		{
			super();
			this.rankedName = rankedName;
			this.rank = rank;
			this.tokenized = tokenized;
		}

		private void addFieldName(String name)
		{
			this.fieldNames.add(name);
		}

		private List<String> getFieldNames()
		{
			return this.fieldNames;
		}

		public String getRankedName()
		{
			return rankedName;
		}

		public int getRank()
		{
			return rank;
		}

		public boolean isTokenized()
		{
			return tokenized;
		}

	}
}
