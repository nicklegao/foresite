/**
 * @author Nick Yiming Gao
 * @date 15/03/2017 4:16:33 pm
 */
package au.net.webdirector.dev.servlet;

/**
 * 
 */

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.utils.email.PendingEmail;
import au.net.webdirector.dev.annotation.DbColumn;
import au.net.webdirector.dev.annotation.DbTable;
import au.net.webdirector.dev.annotation.Module;

public class DbColumnCheckServlet extends HttpServlet
{

	private static Logger logger = Logger.getLogger(DbColumnCheckServlet.class);

	public void init(ServletConfig config)
	{
//		logger.info("=======DB Column Check Servlet Start==========");
//		try
//		{
//			String packagePath = config.getInitParameter("package-path");
//			if (StringUtils.isBlank(packagePath))
//			{
//				logger.error("No init param 'package-path' set in web.xml.");
//				return;
//			}
//
//			ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
//			provider.addIncludeFilter(new AnnotationTypeFilter(Module.class));
//			provider.addIncludeFilter(new AnnotationTypeFilter(DbTable.class));
//			logger.info("Scanning classes in package: " + packagePath);
//			Set<BeanDefinition> components = provider.findCandidateComponents(packagePath);
//			List<String> msgs = new ArrayList<>();
//			for (BeanDefinition component : components)
//			{
//				try
//				{
//					Class cls = Class.forName(component.getBeanClassName());
//					List<String> msg = checkClass(cls);
//					msgs.addAll(msg);
//				}
//				catch (ClassNotFoundException e)
//				{
//				}
//			}
//			if (msgs.size() == 0)
//			{
//				logger.info("No missing column found");
//				return;
//			}
//			logger.error("Missing column found.");
//			logger.info(formatMsgs(msgs, false));
//			String email = config.getInitParameter("notify-email");
//			if (!StringUtils.isBlank(email))
//			{
//				logger.info("Sending error to email:" + email);
//				sendEmails(msgs, StringUtils.split(email, ","));
//			}
//			else
//			{
//				logger.warn("No init param 'notify-email' set in web.xml. Do not send email.");
//			}
//			boolean forceStop = "true".equalsIgnoreCase(config.getInitParameter("force-stop"));
//			if (forceStop)
//			{
//				logger.info("Force stopping tomcat");
//				logger.info("=======DB Column Check Servlet Exit==========");
//				System.exit(-1);
//				return;
//			}
//			logger.info("No init param 'force-stop' set in web.xml. Do not stop tomcat.");
//		}
//		finally
//		{
//			logger.info("=======DB Column Check Servlet Exit==========");
//		}
	}

	/**
	 * @param msgs
	 * @return
	 */
	private String formatMsgs(List<String> msgs, boolean forHtml)
	{
		return StringUtils.join(msgs, forHtml ? "<br/>" : "\n");
	}

	/**
	 * @param msgs
	 * @param email
	 */
	private void sendEmails(List<String> msgs, String[] emails)
	{
		String host = Defaults.getInstance().getWebSiteURL().substring(Defaults.getInstance().getWebSiteURL().indexOf("://") + 3).trim();
		new PendingEmail("Db Column Check Error Found @" + host, formatMsgs(msgs, true), host)
				.addTo(emails)
				.doSend();
	}

	/**
	 * @param cls
	 * @return
	 */
	private List<String> checkClass(Class cls)
	{
		Module ma = (Module) cls.getAnnotation(Module.class);
		if (ma != null)
			return check(cls, ma.name(), true);
		DbTable da = (DbTable) cls.getAnnotation(DbTable.class);
		if (da != null)
			return check(cls, da.name(), false);
		return Arrays.asList(new String[] { "No 'Module/DbTable' annotation found in class:" + cls.getName() });

	}

	/**
	 * @param ma
	 * @param name
	 * @return
	 */
	private List<String> check(Class cls, String name, boolean forModule)
	{
		logger.info("Checking Class:" + cls.getName() + ", isModule:" + forModule);
		TransactionDataAccess da = TransactionDataAccess.getInstance();
		List<String> msgs = new ArrayList<>();
		for (Field f : cls.getFields())
		{
			DbColumn column = f.getAnnotation(DbColumn.class);
			if (column == null)
			{
				continue;
			}
			try
			{
				String type = forModule ? column.type() : DbColumn.TYPE_NONE;
				if (StringUtils.equalsIgnoreCase(DbColumn.TYPE_NONE, type) && forModule)
				{
					String field = f.getName().toLowerCase();
					type = field.startsWith("e_") ? DbColumn.TYPE_ELEMENT
							: field.matches("^c[0-9]*_") ? DbColumn.TYPE_CATEGORY
									: DbColumn.TYPE_NONE;
				}
				if (StringUtils.equalsIgnoreCase(DbColumn.TYPE_NONE, type) && forModule)
				{
					msgs.add("Type is not set and could not guess for field:" + f.getName());
					continue;
				}
				String dbFieldName = f.get(null).toString();
				String tableName = type + name;
				try
				{
					da.selectString("select " + dbFieldName + " from " + tableName + " where 1!=1 limit 1");
					logger.info("Checking field:" + f.getName() + " db:" + tableName + "." + dbFieldName + " exists.");
				}
				catch (SQLException e)
				{
					msgs.add("Error happened in checking field:" + f.getName() + ", db:" + tableName + "." + dbFieldName + " [" + e.getMessage() + "]");
				}

			}
			catch (Exception e)
			{
				msgs.add("Error:" + ExceptionUtils.getFullStackTrace(e));
			}
		}
		return msgs;
	}

}
