/**
 * @author Nick Yiming Gao
 * @date 15/03/2017 3:43:03 pm
 */
package au.net.webdirector.dev.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DbColumn
{
	public static final String TYPE_ELEMENT = "elements_";
	public static final String TYPE_CATEGORY = "categories_";
	public static final String TYPE_NONE = "";

	String type() default TYPE_NONE;
}
