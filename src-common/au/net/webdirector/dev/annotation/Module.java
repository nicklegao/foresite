/**
 * @author Nick Yiming Gao
 * @date 15/03/2017 3:42:17 pm
 */
package au.net.webdirector.dev.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Module
{

	String name();

}
