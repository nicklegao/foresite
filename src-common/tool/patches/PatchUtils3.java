package tool.patches;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

public class PatchUtils3
{

	public static void main(String[] args) throws IOException
	{
		System.out.print("Enter Project Name(Press enter to exit):");
		Scanner sc = new Scanner(System.in);
		while (sc.hasNextLine())
		{
			String projectName = sc.nextLine();
			if (StringUtils.isBlank(projectName))
			{
				System.out.print("Bye!");
				break;
			}
			patchFromSvn(projectName);
			System.out.print("Enter Project Name(Press enter to exit):");
		}
	}

	private static void patchFromSvn(String projectName) throws IOException
	{
		File svnPath = new File("C:\\Users\\NickTemp\\Desktop\\" + projectName);
		File classSrcPath = new File("C:\\eclipse-jee-kepler-SR1-win32-x86_64\\eclipse\\workspace\\" + projectName + "\\web\\WEB-INF\\classes");
		File classDestPath = new File("C:\\Users\\NickTemp\\Desktop\\" + projectName + "\\web\\WEB-INF\\classes");

		if (!svnPath.exists())
		{
			return;
		}

		copyResourcesFiles(svnPath, classDestPath, new String[] { "src-resources" });
		copySrcFiles(svnPath, classSrcPath, classDestPath);
		System.out.println("Finished.");
	}

	private static void copySrcFiles(File svnPath, File classSrcPath, File classDestPath) throws IOException
	{
		File[] srcFolders = svnPath.listFiles(new FilenameFilter()
		{
			@Override
			public boolean accept(File dir, String name)
			{

				return dir.isDirectory() && name.toLowerCase().startsWith("src") && !name.equals("src-resources");
			}

		});

		for (File src : srcFolders)
		{
			if (src.exists())
			{
				copyClassFiles(classSrcPath, classDestPath, src);
			}
		}
	}

	private static void copyResourcesFiles(File svnPath, File classDestPath, String[] resources) throws IOException
	{
		for (String name : resources)
		{
			File resource = new File(svnPath, name);
			if (resource.exists())
			{
				copyResourceFiles(classDestPath, resource);
			}
		}
	}

	private static void copyResourceFiles(File classDestPath, File resource) throws IOException
	{
		File[] files = resource.listFiles();
		for (File file : files)
		{
			if (file.isDirectory())
			{
				FileUtils.copyDirectoryToDirectory(file, classDestPath);
				continue;
			}
			FileUtils.copyFileToDirectory(file, classDestPath);
		}
	}

	private static void copyClassFiles(File classSrcPath, File classDestPath, File src) throws IOException
	{
		// TODO Auto-generated method stub
		File[] files = src.listFiles();
		for (File file : files)
		{
			if (file.isDirectory())
			{
				copyClassFiles(classSrcPath, classDestPath, file);
				continue;
			}
			String path = file.getAbsolutePath();
			String fullPath = path.substring(path.indexOf("\\", path.indexOf("\\src") + 1));
			fullPath = StringUtils.replace(fullPath, ".java", ".class");
			File srcFile = new File(classSrcPath, fullPath);
			File destDir = new File(classDestPath, fullPath).getParentFile();
			copyClassAndInnerClassesToDir(srcFile, destDir);
		}
	}

	private static void copyClassAndInnerClassesToDir(final File srcFile, File destDir) throws IOException
	{
		File dir = srcFile.getParentFile();
		File[] files = dir.listFiles(new FilenameFilter()
		{
			public boolean accept(File dir, String name)
			{
				if (srcFile.getName().indexOf(".class") == -1)
				{
					return name.equals(srcFile.getName());
				}
				String srcName = srcFile.getName().substring(0, srcFile.getName().indexOf(".class"));
				return name.indexOf(srcName) != -1;
			}
		});
		for (File file : files)
		{
			FileUtils.copyFileToDirectory(file, destDir);
			System.out.println(file + " -> " + destDir);
		}
	}
}
