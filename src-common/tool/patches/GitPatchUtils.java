package tool.patches;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class GitPatchUtils
{
	private static Logger logger = Logger.getLogger(GitPatchUtils.class);

	public static void main(String[] args) throws IOException, InterruptedException
	{
		start();
	}

	private static void start() throws IOException, InterruptedException
	{
		System.out.println("Preparing patch for: '" + getProjectName() + "'");
		System.out.print("Enter commit ids([start - end] Press enter to exit):");
		Scanner sc = new Scanner(System.in);
		while (sc.hasNextLine())
		{
			String version = sc.nextLine();
			if (StringUtils.isBlank(version))
			{
				System.out.print("Bye!");
				break;
			}
			patchFromGit(version);
			System.out.print("Enter commit ids([start - end] Press enter to exit):");
		}
	}

	private static String getProjectName() throws IOException
	{
		File projectFile = new File(getProjectFolder(), ".project");
		BufferedReader br = new BufferedReader(new FileReader(projectFile));
		try
		{
			String line = null;
			while ((line = br.readLine()) != null)
			{
				line = line.trim();
				if (line.startsWith("<name>"))
				{
					return line.substring(line.indexOf("<name>") + "<name>".length(), line.indexOf("</name>"));
				}
			}
			throw new RuntimeException("Cannot load .project file in [" + projectFile + "]");
		}
		finally
		{
			br.close();
		}
	}

	private static File getProjectFolder()
	{
		return new File(System.getProperty("user.dir"));
	}

	private static List<String> getGitDiff(String version) throws IOException, InterruptedException
	{
		//git diff-tree -r --no-commit-id --name-only --diff-filter=ACMRT d99526cf9193bd52a81aef1b616314ee485cc2fb d632a96d7e7784e1822dc4a500b842a6b245c5ba
		Runtime rt = Runtime.getRuntime();
		final List<String> result = new ArrayList<>();
		final Process p = rt.exec("git diff-tree -r --no-commit-id --name-only --diff-filter=ACMRT " + version);
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
				String line = null;
				try
				{
					while ((line = input.readLine()) != null)
						result.add(line);
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}).start();
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				BufferedReader input = new BufferedReader(new InputStreamReader(p.getErrorStream()));
				String line = null;
				try
				{
					while ((line = input.readLine()) != null)
						System.err.println(line);
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}).start();

		p.waitFor();
		return result;
	}

	private static void patchFromGit(String version) throws IOException, InterruptedException
	{
		File project = getProjectFolder();
		File destFolder = new File("C:\\Users\\" + System.getenv("USERNAME") + "\\Desktop\\" + getProjectName());
		File classSrcPath = new File(project, "web\\WEB-INF\\classes");
		File classDestPath = new File(destFolder, "web\\WEB-INF\\classes");

		if (!classSrcPath.exists())
		{
			classSrcPath = new File(project, "target\\classes");
		}
		if (!classSrcPath.exists())
		{
			return;
		}
		if (!classDestPath.exists())
		{
			classDestPath.mkdirs();
		}
		List<String> files = getGitDiff(version);
		for (String path : files)
		{
//			System.out.println(path);
			if (path.startsWith("src") && path.endsWith(".java"))
			{
				copyClassFile(path, classSrcPath, classDestPath);
			}
			else if (path.startsWith("src"))
			{
				copyResourceFile(path, classSrcPath, classDestPath);
			}
			else
			{
				copyFile(path, project, destFolder);
			}
		}
		System.out.println("Finished.");
	}


	private static void copyFileToDirectory(File srcFile, File destDir) throws IOException
	{
		System.out.println("Copying [" + srcFile + "] to [" + destDir + "]");
		FileUtils.copyFileToDirectory(srcFile, destDir);
	}

	private static void copyFile(String path, File project, File destFolder) throws IOException
	{
		String fullPath = path;
		File srcFile = new File(project, fullPath);
		File destDir = new File(destFolder, fullPath).getParentFile();

		copyFileToDirectory(srcFile, destDir);

	}

	private static void copyResourceFile(String path, File classSrcPath, File classDestPath) throws IOException
	{

		String fullPath = path.substring(path.indexOf("/") + 1);
		File srcFile = new File(classSrcPath, fullPath);
		File destDir = new File(classDestPath, fullPath).getParentFile();
		copyFileToDirectory(srcFile, destDir);
	}

	private static void copyClassFile(String path, File classSrcPath, File classDestPath) throws IOException
	{
		// file = "src-application/au/corporateinteractive/qcloud/proposalbuilder/control/LibraryController.java"
		String fullPath = path.substring(path.indexOf("/") + 1);
		fullPath = StringUtils.replace(fullPath, ".java", ".class");
		File srcFile = new File(classSrcPath, fullPath);
		File destDir = new File(classDestPath, fullPath).getParentFile();
		copyClassAndInnerClassesToDir(srcFile, destDir);
	}

	private static void copyClassAndInnerClassesToDir(final File srcFile, File destDir) throws IOException
	{
		File dir = srcFile.getParentFile();
		File[] files = dir.listFiles(new FilenameFilter()
		{
			@Override
			public boolean accept(File dir, String name)
			{
				if (srcFile.getName().indexOf(".class") == -1)
				{
					return name.equals(srcFile.getName());
				}
				String srcName = srcFile.getName().substring(0, srcFile.getName().indexOf(".class"));
				return name.indexOf(srcName) != -1;
			}
		});
		for (File file : files)
		{
			FileUtils.copyFileToDirectory(file, destDir);
			System.out.println(file + " -> " + destDir);
		}
	}
}
