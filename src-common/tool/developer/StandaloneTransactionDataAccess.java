package tool.developer;

import java.sql.Connection;

import org.apache.log4j.Logger;

public class StandaloneTransactionDataAccess extends au.net.webdirector.common.datalayer.base.db.TransactionDataAccess
{
	protected StandaloneTransactionDataAccess(boolean autoCommit, Connection conn)
	{
		super(autoCommit);
		this.conn = conn;
	}

	private static Logger logger = Logger.getLogger(StandaloneTransactionDataAccess.class);

	public static StandaloneTransactionDataAccess getInstance(Connection conn)
	{
		return getInstance(true, conn);
	}

	public static StandaloneTransactionDataAccess getInstance(boolean autoCommit, Connection conn)
	{
		return new StandaloneTransactionDataAccess(autoCommit, conn);
	}

	@Override
	protected void prepareConnection()
	{
		return;
	}

	@Override
	protected void releaseConnection()
	{
		return;
	}


}
