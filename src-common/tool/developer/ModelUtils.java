package tool.developer;

import java.io.File;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import model.Users;
import model.UsersCategory;
import tool.developer.anno.CIField;
import tool.developer.model.CICategory;
import tool.developer.model.CIElement;
import tool.developer.model.CIEntity;

public class ModelUtils
{
	private static Logger logger = Logger.getLogger(ModelUtils.class);

	private static String driverClassName = null;
	private static String password = null;
	private static String username = null;
	private static String url = null;

	public static void main(String[] args)
	{
//		generate("Module");
//		generate("Table");
		ElementData e = toElementData(new Users().withElementId("1"), true);
		System.out.println(e);
		CategoryData c = toCategoryData(new UsersCategory().withFolderLevel(2), true);
		System.out.println(c);
		Users u = toCIElement(e, Users.class);
		UsersCategory uc = toCICategory(c, UsersCategory.class);
		System.out.println(u.getElementId());
		System.out.println(uc.getFolderLevel());
	}


	private static void generate(String type)
	{
		loadContextXml();
		System.out.println("Package name:");
		Scanner sc = new Scanner(System.in);
		String packageName = sc.nextLine();
		System.out.print(type + " name(comma delimited, press enter to exit):");
		while (sc.hasNextLine())
		{
			String names = sc.nextLine();
			if (StringUtils.isBlank(names))
			{
				System.out.print("Bye!");
				break;
			}
			createModels(packageName, names, type);
			System.out.print(type + " name(comma delimited, press enter to exit):");
		}
		sc.close();
	}

	private static void createModels(String packageName, String namesStr, String type)
	{
		File folder = new File(new File("."), "/src-application/" + StringUtils.replace(packageName, ".", "/"));
		System.out.println("Creating models in folder:" + folder.getAbsolutePath());
		if (!folder.exists())
		{
			folder.mkdirs();
		}
		String[] names = StringUtils.split(namesStr, ",");
		Connection conn = getConnection();
		try
		{
			TransactionDataAccess da = StandaloneTransactionDataAccess.getInstance(conn);
			for (String name : names)
			{
				if (StringUtils.equalsIgnoreCase("Module", type))
				{
					createFile(true, folder, packageName, name, da, true);
					createFile(false, folder, packageName, name, da, true);
				}
				else
				{
					createFile(false, folder, packageName, name, da, false);
				}

			}
		}
		finally
		{
			try
			{
				conn.close();
			}
			catch (SQLException e)
			{
			}
		}
	}

	private static void createFile(boolean isCategory, File folder, String packageName, String name, TransactionDataAccess da, boolean isModule)
	{
		String className = getJavaClassName(name) + (isModule && isCategory ? "Category" : "");
		File file = new File(folder, className + ".java");

		InputStream in = ModelUtils.class.getResourceAsStream("./template/model_" + (isModule ? isCategory ? "c" : "e" : "t") + "_template.txt");
		try
		{
			String template = IOUtils.toString(in, "utf-8");
			template = StringUtils.replace(template, "$(packageName)", packageName);
			template = StringUtils.replace(template, "$(className)", className);
			String fragment = StringUtils.substringBetween(template, "//USER_DEFINED_START", "//USER_DEFINED_END");
			String newText = "";
			if (isModule)
			{
				newText = buildCodeFromModule(fragment, isCategory, name, da);
			}
			else
			{
				newText = buildCodeFromTable(fragment, name, da);
			}
			template = StringUtils.replace(template, fragment, newText);
			FileUtils.writeStringToFile(file, template, "utf-8");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			IOUtils.closeQuietly(in);
		}
	}

	private static String buildCodeFromTable(String fragment, String name, TransactionDataAccess da) throws SQLException
	{
		String newText = "";
		List<DataMap> fields = da.selectMaps("desc " + name);
		for (DataMap field : fields)
		{
			try
			{
				String fieldText = fragment;
				String dbName = field.getString("field");
				String fieldType = getFieldType(field.getString("type"));
				String fieldName = getJavaFieldName(dbName, false);
				fieldText = StringUtils.replace(fieldText, "$(dbName)", dbName);
				fieldText = StringUtils.replace(fieldText, "$(fieldType)", fieldType);
				fieldText = StringUtils.replace(fieldText, "$(fieldName)", fieldName);
				fieldText = StringUtils.replace(fieldText, "$(capitalizedFieldName)", WordUtils.capitalize(fieldName));
				newText += fieldText;
			}
			catch (Exception e)
			{
				System.err.println("Cannot add field:" + field);
				System.err.println(e);
			}
		}
		return newText;
	}


	private static String getFieldType(String dbType)
	{
		String type = dbType.indexOf("(") > -1 ? dbType.substring(0, dbType.indexOf("(")) : dbType;
		switch (type.toLowerCase())
		{
			case "char":
			case "varchar":
			case "text":
				return "String";
			case "int":
				return "Integer";
			case "decimal":
				return "Double";
			case "datetime":
			case "timestamp":
				return "Date";
		}
		throw new IllegalArgumentException("Unsupported type:" + type + " for " + dbType);
	}


	private static String buildCodeFromModule(String fragment, boolean isCategory, String moduleName, TransactionDataAccess da) throws SQLException
	{
		String newText = "";
		List<DataMap> fields = da.selectMaps("select * from " + (isCategory ? "category" : "") + "labels_" + moduleName);
		for (DataMap field : fields)
		{
			try
			{
				String fieldText = fragment;
				String dbName = field.getString("Label_InternalName");
				String fieldType = getFieldTypeByName(dbName);
				String fieldName = getJavaFieldName(dbName, true);
				fieldText = StringUtils.replace(fieldText, "$(dbName)", dbName);
				fieldText = StringUtils.replace(fieldText, "$(fieldType)", fieldType);
				fieldText = StringUtils.replace(fieldText, "$(fieldName)", fieldName);
				fieldText = StringUtils.replace(fieldText, "$(capitalizedFieldName)", WordUtils.capitalize(fieldName));
				newText += fieldText;
			}
			catch (Exception e)
			{
				System.err.println("Cannot add field:" + field);
				System.err.println(e);
			}
		}
		return newText;
	}


	private static String getJavaClassName(String dbName)
	{
		return WordUtils.capitalize(getCamelCase(dbName));
	}

	private static String getJavaFieldName(String dbName, boolean isModuleField)
	{
		String fieldName = isModuleField ? StringUtils.substring(dbName, StringUtils.indexOf(dbName, "_") + 1) : dbName;
		fieldName = getCamelCase(fieldName);
		return WordUtils.uncapitalize(fieldName);
	}

	private static String getCamelCase(String fieldName)
	{
		String[] arr = StringUtils.split(fieldName, "_");
		for (int i = 0; i < arr.length; i++)
		{
			arr[i] = WordUtils.capitalize(arr[i]);
		}
		return StringUtils.join(arr, "");
	}

	private static String getFieldTypeByName(String dbName)
	{
		String type = StringUtils.substring(dbName, 0, StringUtils.indexOf(dbName, "_"));
		switch (type.toLowerCase())
		{
			case "attr":
			case "attrfile":
			case "attrimage":
			case "attrdrop":
			case "attrshare":
				return "String";
			case "attrcheck":
				return "Boolean";
			case "attrinteger":
				return "Integer";
			case "attrcurrency":
				return "Double";
			case "attrdate":
				return "Date";
		}
		throw new IllegalArgumentException("Unsupported type:" + type + " for " + dbName);
	}

	private static void loadContextXml()
	{
		File xml = new File(new File("."), "/web/META-INF/context.xml");
		System.out.println("Loading xml:" + xml.getAbsolutePath());
		try
		{
			XMLConfiguration config = new XMLConfiguration(xml);
			driverClassName = config.getString("Resource[@driverClassName]");
			password = config.getString("Resource[@password]");
			username = config.getString("Resource[@username]");
			url = config.getString("Resource[@url]");
		}
		catch (ConfigurationException cex)
		{
			System.out.println("Cannot load context.xml, quit!");
			cex.printStackTrace();
			System.exit(0);
		}
	}

	private static Connection getConnection()
	{
		try
		{
			Class.forName(driverClassName);
			return DriverManager.getConnection(url, username, password);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public static DataMap toDataMap(CIEntity object, boolean ignoreNullValue)
	{
		DataMap map = new DataMap();
		setDataMap(map, object, ignoreNullValue);
		return map;
	}

	public static void setDataMap(DataMap map, CIEntity object, boolean ignoreNullValue)
	{
		Field[] fields = object.getClass().getDeclaredFields();
		if (fields == null || fields.length == 0)
		{
			return;
		}
		for (Field field : fields)
		{
			Annotation[] annos = field.getAnnotationsByType(CIField.class);
			if (annos == null || annos.length == 0)
			{
				continue;
			}
			CIField anno = (CIField) annos[0];
			field.setAccessible(true);
			try
			{
				if (field.get(object) != null || !ignoreNullValue)
				{
					map.put(anno.db(), field.get(object));
				}
			}
			catch (Exception e)
			{
				logger.error("Error:", e);
			}
		}
		return;
	}

	public static <T extends CIEntity> T toCIEntity(DataMap map, Class<T> clazz)
	{
		T object = null;
		try
		{
			object = clazz.newInstance();
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return object;
		}
		Field[] fields = clazz.getDeclaredFields();
		if (fields == null || fields.length == 0)
		{
			return object;
		}
		for (Field field : fields)
		{
			Annotation[] annos = field.getAnnotationsByType(CIField.class);
			if (annos == null || annos.length == 0)
			{
				continue;
			}
			CIField anno = (CIField) annos[0];
			field.setAccessible(true);
			try
			{
				Object value = getDataMapValue(map, field, anno.db());
				field.set(object, value);
			}
			catch (Exception e)
			{
				logger.error("Error:", e);
			}
		}
		return object;

	}

	public static <T extends CIElement> T toCIElement(ElementData map, Class<T> clazz)
	{
		return toCIEntity(map, clazz);
	}

	public static <T extends CICategory> T toCICategory(CategoryData map, Class<T> clazz)
	{
		return toCIEntity(map, clazz);
	}

	private static Object getDataMapValue(DataMap map, Field field, String dbField)
	{
		if (map.get(dbField) == null)
		{
			return null;
		}
		if (field.getType().isAssignableFrom(String.class))
		{
			return map.getString(dbField);
		}
		if (field.getType().isAssignableFrom(Integer.class))
		{
			return map.getInt(dbField);
		}
		if (field.getType().isAssignableFrom(Double.class))
		{
			return map.getDouble(dbField);
		}
		if (field.getType().isAssignableFrom(Boolean.class))
		{
			return map.getBoolean(dbField);
		}
		if (field.getType().isAssignableFrom(Date.class))
		{
			return map.getDate(dbField);
		}
		throw new IllegalArgumentException("Unsupported type:" + field.getType());
	}

	public static ElementData toElementData(CIElement object, boolean ignoreNullValue)
	{
		ElementData map = new ElementData();
		setDataMap(map, object, ignoreNullValue);
		return map;
	}

	public static CategoryData toCategoryData(CICategory object, boolean ignoreNullValue)
	{
		CategoryData map = new CategoryData();
		setDataMap(map, object, ignoreNullValue);
		return map;
	}
}
