package com.mandrill;

import java.util.TreeMap;

import com.google.gson.Gson;

public class MandrillMetadata extends TreeMap<String, String>
{
	private static final long serialVersionUID = 4674529321568853092L;

	@Override
	public String toString()
	{
		return new Gson().toJson(this);
	}
}
