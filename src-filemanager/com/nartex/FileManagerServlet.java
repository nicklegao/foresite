/**
 * @author Nick Yiming Gao
 * @date 2015-5-8
 */
package com.nartex;

/**
 * 
 */

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

public class FileManagerServlet extends HttpServlet
{

	private static final long serialVersionUID = 4577811656076711274L;

	private static Logger logger = Logger.getLogger(FileManagerServlet.class);

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		FileManager fm = new FileManager(getServletContext(), request);
		if (!auth(request, response))
		{
			respond(response, fm.error(fm.lang("AUTHORIZATION_REQUIRED")));
			return;
		}

		if (StringUtils.isBlank(request.getParameter("mode")))
		{
			return;
		}

		String mode = request.getParameter("mode");
		// fm.setGetVar("fType", request.getParameter("fType"));
		if (mode.equals("getinfo"))
		{
			if (fm.setGetVar("path", request.getParameter("path")))
			{
				respond(response, fm.getInfo());
			}
			return;
		}
		if (mode.equals("getfolder"))
		{
			if (fm.setGetVar("path", request.getParameter("path")))
			{
				if (request.getParameter("search").equals("true"))
				{
					if (fm.setGetVar("term", request.getParameter("term")))
					{
						respond(response, fm.getAll(request.getParameter("path")));
					}
				}
				else
				{
					respond(response, fm.getFolder());
				}
			}
			return;
		}
		if (mode.equals("getsearch"))
		{
			if (fm.setGetVar("term", request.getParameter("term")))
			{
				respond(response, fm.getAll(request.getParameter("path")));
			}
		}
		if (mode.equals("move"))
		{
			if (fm.setGetVar("old", request.getParameter("old")) &&
					fm.setGetVar("new", request.getParameter("new")))
			{
				respond(response, fm.move());
			}
			return;
		}
		if (mode.equals("rename"))
		{
			if (fm.setGetVar("old", request.getParameter("old")) &&
					fm.setGetVar("new", request.getParameter("new")))
			{
				respond(response, fm.rename());
			}
			return;
		}
		if (mode.equals("delete"))
		{
			if (fm.setGetVar("path", request.getParameter("path")))
			{
				respond(response, fm.delete());
			}
			return;
		}
		if (mode.equals("addfolder"))
		{
			if (fm.setGetVar("path", request.getParameter("path")) &&
					fm.setGetVar("name", request.getParameter("name")))
			{
				respond(response, fm.addFolder());
			}
			return;
		}
		if (mode.equals("download"))
		{
			if (fm.setGetVar("path", request.getParameter("path")))
			{
				fm.download(response);
			}
			return;
		}
		if (mode.equals("preview"))
		{
			if (fm.setGetVar("path", request.getParameter("path")))
			{
				fm.preview(response);
			}
			return;
		}
		if (mode.equals("usagecheck"))
		{
			if (fm.setGetVar("path", request.getParameter("path")))
			{
				respond(response, fm.usageCheck());
			}
			return;
		}
		respond(response, fm.error(fm.lang("MODE_ERROR")));
		return;
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		FileManager fm = new FileManager(getServletContext(), request);
		if (!auth(request, response))
		{
			respond(response, fm.error(fm.lang("AUTHORIZATION_REQUIRED")));
		}
		respond(response, fm.add(), true);
	}


	private boolean auth(HttpServletRequest request, HttpServletResponse response)
	{
		return true;
	}

	private void respond(HttpServletResponse response, JSONObject responseData, boolean putTextarea) throws IOException
	{
		PrintWriter pw = response.getWriter();
		String responseStr = responseData.toString();
		if (putTextarea)
			responseStr = "<textarea>" + responseStr + "</textarea>";
		//fm.log("c:\\logfilej.txt", "mode:" + mode + ",response:" + responseStr);
		pw.print(responseStr);
		pw.close();
	}

	private void respond(HttpServletResponse response, JSONObject responseData) throws IOException
	{
		respond(response, responseData, false);
	}
}
