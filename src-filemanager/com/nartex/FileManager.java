/*
 *	Filemanager.java utility class for for filemanager.jsp
 *
 *	@license	MIT License
 *	@author		Dick Toussaint <d.tricky@gmail.com>
 *	@copyright	Authors
 */
package com.nartex;

import java.awt.Dimension;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import au.net.webdirector.common.utils.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.ImageIcon;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;
import au.net.webdirector.common.datalayer.client.ModuleHelper;

public class FileManager
{

	protected static Properties config = null;
	protected static JSONObject language = null;
	protected Map<String, String> get = new HashMap<String, String>();
	protected Map<String, String> properties = new HashMap<String, String>();
	protected Map item = new HashMap();
	protected Map<String, String> params = new HashMap<String, String>();
	protected String documentRoot = "";
	protected String fileManagerRoot = "";
	protected String referer = "";

	protected JSONObject error = null;

	SimpleDateFormat dateFormat;
	List files = null;

	public FileManager(ServletContext servletContext, HttpServletRequest request)
	{
		// get document root like in php
		String contextPath = request.getContextPath();
		String documentRoot = servletContext.getRealPath("/").replaceAll("\\\\", "/");
		documentRoot = documentRoot.substring(0, documentRoot.indexOf(contextPath));

		this.referer = request.getHeader("referer") == null ? "" : request.getHeader("referer");
		//this.fileManagerRoot = Defaults.getInstance().getWebSiteURL() + "/adminExtra/filemanager/";
		this.fileManagerRoot = "http://localhost:9525/adminExtra/filemanager/";

		// get uploaded file list
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		if (ServletFileUpload.isMultipartContent(request))
			try
			{
				files = upload.parseRequest(request);
			}
			catch (Exception e)
			{ // no error handling}
			}

		this.properties.put("Date Created", null);
		this.properties.put("Date Modified", null);
		this.properties.put("Height", null);
		this.properties.put("Width", null);
		this.properties.put("Size", null);

		// load config file
		loadConfig();

		this.documentRoot = Defaults.getInstance().getStoreDir();
//		if (config.getProperty("doc_root") != null)
//			this.documentRoot = config.getProperty("doc_root");
//		else
//			this.documentRoot = documentRoot;

		dateFormat = new SimpleDateFormat(config.getProperty("date"));

		this.setParams();

		loadLanguageFile();
	}

	public JSONObject error(String msg)
	{
		JSONObject errorInfo = new JSONObject();
		try
		{
			errorInfo.put("Error", msg);
			errorInfo.put("Code", "-1");
			errorInfo.put("Properties", this.properties);
			this.error = errorInfo;
		}
		catch (JSONException e)
		{
			this.error("JSONObject error");
		}
		return error;
	}

	public JSONObject getError()
	{
		return error;
	}

	public String lang(String key)
	{
		String text = "";
		try
		{
			text = language.getString(key);
		}
		catch (Exception e)
		{
		}
		if (text == null || text == "")
			text = "Language string error on " + key;
		return text;
	}

	public boolean setGetVar(String var, String value)
	{
		boolean retval = false;
		if (value == null || value == "")
		{
			this.error(sprintf(lang("INVALID_VAR"), var));
		}
		else
		{
			this.get.put(var, sanitize(value));
			retval = true;
		}
		return retval;
	}

	public JSONObject getInfo()
	{
		this.item = new HashMap();
		this.item.put("properties", this.properties);
		this.getFileInfo("");

		try
		{
			JSONObject array = new JSONObject();
			array.put("Path", this.getPath());
			array.put("StoreContext", "/" + Defaults.getInstance().getStoreContext());
			array.put("Filename", this.item.get("filename"));
			array.put("File Type", this.item.get("filetype"));
			array.put("Preview", this.item.get("preview"));
			array.put("fa-icon", this.item.get("fa-icon"));
			array.put("Properties", this.item.get("properties"));
			array.put("Error", "");
			array.put("Code", 0);
			return array;
		}
		catch (JSONException e)
		{
			return this.error("JSONObject error");
		}
	}

	public JSONObject getAll(String directory)
	{
		JSONObject array = null;
		JSONObject subArray = null;
		Boolean allowedStores;
		File dir = new File(documentRoot + directory);

		File file = null;
		if (!dir.isDirectory())
		{
			return this.error(sprintf(lang("DIRECTORY_NOT_EXIST"), this.getPath()));
		}

		if (!dir.canRead())
		{
			return this.error(sprintf(lang("UNABLE_TO_OPEN_DIRECTORY"), this.getPath()));
		}
		array = new JSONObject();
		subArray = new JSONObject();
		String[] files = dir.list();
		List<String> dirs = new ArrayList<String>();
		JSONObject data = null;
		JSONObject props = null;
		for (int i = 0; i < files.length; i++)
		{
			allowedStores = true;
			if (directory.equals("/") && !contains(config.getProperty("allowed_stores_dirs"), files[i]))
			{
				allowedStores = false;
			}
			data = new JSONObject();
			props = new JSONObject();
			file = new File(documentRoot + directory + files[i]);
			if (file.isDirectory() &&
					!contains(config.getProperty("unallowed_dirs"), files[i]) && allowedStores)
			{
				try
				{
					if (files[i].toLowerCase().contains(this.get.get("term").toLowerCase()))
					{
						props.put("Date Created", (String) null);
						props.put("Date Modified", (String) null);
						props.put("Height", (String) null);
						props.put("Width", (String) null);
						props.put("Size", (String) null);
						data.put("Path", directory + files[i] + "/");
						data.put("StoreContext", "/" + Defaults.getInstance().getStoreContext());
						data.put("Filename", files[i]);
						data.put("File Type", "dir");
						//data.put("Preview", config.getProperty("icons-path") + config.getProperty("icons-directory"));
						data.put("fa-icon", config.getProperty("fa-icons-directory"));
						data.put("Error", "");
						data.put("Code", 0);
						data.put("Properties", props);

						array.put(directory + files[i] + "/", data);

					}
					subArray = getAll(directory + files[i] + "/");
					if (subArray.length() > 0)
					{
						try
						{
							Iterator i1 = subArray.keys();
							String tmp_key;
							while (i1.hasNext())
							{
								tmp_key = (String) i1.next();
								array.put(tmp_key, subArray.get(tmp_key));
							}
						}
						catch (Exception e)
						{

						}
					}
				}
				catch (JSONException e)
				{
					return this.error("JSONObject error");
				}

			}
			else if (!file.isDirectory() && !contains(config.getProperty("unallowed_files"), files[i]) && !this.getPath().equals("/"))
			{
				this.item = new HashMap();
				this.item.put("properties", this.properties);
				this.getFileInfo(directory + files[i]);

				if (this.params.get("type") == null || (this.params.get("type") != null && (!this.params.get("type").equals("Image") || this.params.get("type").equals("Image") &&
						contains(config.getProperty("images"), (String) this.item.get("filetype")))))
				{
					try
					{
						if (files[i].toLowerCase().contains(this.get.get("term").toLowerCase()))
						{
							data.put("Path", directory + files[i]);
							data.put("StoreContext", "/" + Defaults.getInstance().getStoreContext());
							data.put("Filename", this.item.get("filename"));
							data.put("File Type", this.item.get("filetype"));
							data.put("fa-icon", this.item.get("fa-icon"));
							data.put("Preview", this.item.get("preview"));
							data.put("Properties", this.item.get("properties"));
							data.put("Error", "");
							data.put("Code", 0);

							array.put(directory + files[i], data);
						}
					}
					catch (JSONException e)
					{
						return this.error("JSONObject error");
					}
				}
			}
		}

		return array;
	}

	public JSONObject getFolder()
	{
		JSONObject array = null;
		Boolean allowedStores;
		File dir = new File(documentRoot + this.getPath());

		if (!dir.isDirectory())
		{
			return this.error(sprintf(lang("DIRECTORY_NOT_EXIST"), this.getPath()));
		}

		if (!dir.canRead())
		{
			return this.error(sprintf(lang("UNABLE_TO_OPEN_DIRECTORY"), this.getPath()));
		}
		array = new JSONObject();
		File[] files = dir.listFiles();
		Arrays.sort(files, new Comparator<File>()
		{
			@Override
			public int compare(File o1, File o2)
			{
				if (o1.isDirectory() && !o2.isDirectory())
					return -1;
				if (!o1.isDirectory() && o2.isDirectory())
					return 1;
				return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
			}
		});
		JSONObject data = null;
		JSONObject props = null;
		for (File file : files)
		{
			allowedStores = true;
			if (this.getPath().equals("/") && !contains(config.getProperty("allowed_stores_dirs"), file.getName()))
			{
				allowedStores = false;
			}
			data = new JSONObject();
			props = new JSONObject();
			if (file.isDirectory() &&
					!contains(config.getProperty("unallowed_dirs"), file.getName()) && allowedStores)
			{
				try
				{
					props.put("Date Created", (String) null);
					props.put("Date Modified", (String) null);
					props.put("Height", (String) null);
					props.put("Width", (String) null);
					props.put("Size", (String) null);
					data.put("Path", this.getPath() + file.getName() + "/");
					data.put("StoreContext", "/" + Defaults.getInstance().getStoreContext());
					data.put("Filename", file.getName());
					data.put("File Type", "dir");
					//data.put("Preview", config.getProperty("icons-path") + config.getProperty("icons-directory"));
					data.put("fa-icon", config.getProperty("fa-icons-directory"));
					data.put("Error", "");
					data.put("Code", 0);
					data.put("Properties", props);

					array.put(this.getPath() + file.getName() + "/", data);
				}
				catch (JSONException e)
				{
					return this.error("JSONObject error");
				}

			}
			else if (!file.isDirectory() && !contains(config.getProperty("unallowed_files"), file.getName()) && !this.getPath().equals("/"))
			{
				this.item = new HashMap();
				this.item.put("properties", this.properties);
				this.getFileInfo(this.getPath() + file.getName());

				if (this.params.get("type") == null || (this.params.get("type") != null && (!this.params.get("type").equals("Image") || this.params.get("type").equals("Image") &&
						contains(config.getProperty("images"), (String) this.item.get("filetype")))))
				{
					try
					{
						data.put("Path", this.getPath() + file.getName());
						data.put("StoreContext", "/" + Defaults.getInstance().getStoreContext());
						data.put("Filename", this.item.get("filename"));
						data.put("File Type", this.item.get("filetype"));
						data.put("fa-icon", this.item.get("fa-icon"));
						data.put("Preview", this.item.get("preview"));
						data.put("Properties", this.item.get("properties"));
						data.put("Error", "");
						data.put("Code", 0);

						array.put(this.getPath() + file.getName(), data);
					}
					catch (JSONException e)
					{
						return this.error("JSONObject error");
					}
				}
			}
		}


		return array;
	}

	public JSONObject rename()
	{
		if ((this.get.get("old")).endsWith("/"))
		{
			this.get.put("old", (this.get.get("old")).substring(0, ((this.get.get("old")).length() - 1)));
		}
		boolean error = false;
		JSONObject array = null;
		String tmp[] = (this.get.get("old")).split("/");
		String filename = tmp[tmp.length - 1];
		int pos = this.get.get("old").lastIndexOf("/");
		String path = (this.get.get("old")).substring(0, pos + 1);
		File fileFrom = null;
		File fileTo = null;
		try
		{
			fileFrom = new File(this.documentRoot + this.get.get("old"));
			fileTo = new File(this.documentRoot + path + this.get.get("new"));
			if (fileTo.exists())
			{
				if (fileTo.isDirectory())
				{
					this.error(sprintf(lang("DIRECTORY_ALREADY_EXISTS"), this.get.get("new")));
					error = true;
				}
				else
				{ // fileTo.isFile
					this.error(sprintf(lang("FILE_ALREADY_EXISTS"), this.get.get("new")));
					error = true;
				}
			}
			else if (!fileFrom.renameTo(fileTo))
			{
				this.error(sprintf(lang("ERROR_RENAMING_DIRECTORY"), filename + "#" + this.get.get("new")));
				error = true;
			}
		}
		catch (Exception e)
		{
			if (fileFrom.isDirectory())
			{
				this.error(sprintf(lang("ERROR_RENAMING_DIRECTORY"), filename + "#" + this.get.get("new")));
			}
			else
			{
				this.error(sprintf(lang("ERROR_RENAMING_FILE"), filename + "#" + this.get.get("new")));
			}
			error = true;
		}
		if (!error)
		{
			array = new JSONObject();
			try
			{
				array.put("Error", "");
				array.put("Code", 0);
				array.put("Old Path", this.get.get("old"));
				array.put("Old Name", filename);
				array.put("New Path", path + this.get.get("new"));
				array.put("New Name", this.get.get("new"));
			}
			catch (JSONException e)
			{
				return this.error("JSONObject error");
			}
		}
		return array;
	}

	public JSONObject move()
	{
		if ((this.get.get("old")).endsWith("/"))
		{
			this.get.put("old", (this.get.get("old")).substring(0, ((this.get.get("old")).length() - 1)));
		}
		boolean error = false;
		JSONObject array = null;
		String tmp[] = (this.get.get("old")).split("/");
		String filename = tmp[tmp.length - 1];
		int pos = this.get.get("old").lastIndexOf("/");
		String path = (this.get.get("old")).substring(0, pos + 1);
		File fileFrom = null;
		File fileTo = null;
		try
		{
			fileFrom = new File(this.documentRoot + this.get.get("old"));
			fileTo = new File(this.documentRoot + this.get.get("new") + fileFrom.getName());
			if (fileTo.exists())
			{
				if (fileTo.isDirectory())
				{
					this.error(sprintf(lang("DIRECTORY_ALREADY_EXISTS"), this.get.get("new")));
					error = true;
				}
				else
				{ // fileTo.isFile
					this.error(sprintf(lang("FILE_ALREADY_EXISTS"), this.get.get("new")));
					error = true;
				}
			}
			else if (!fileFrom.renameTo(fileTo))
			{
				this.error(sprintf(lang("ERROR_RENAMING_DIRECTORY"), filename + "#" + this.get.get("new")));
				error = true;
			}
		}
		catch (Exception e)
		{
			if (fileFrom.isDirectory())
			{
				this.error(sprintf(lang("ERROR_RENAMING_DIRECTORY"), filename + "#" + this.get.get("new")));
			}
			else
			{
				this.error(sprintf(lang("ERROR_RENAMING_FILE"), filename + "#" + this.get.get("new")));
			}
			error = true;
		}
		if (!error)
		{
			array = new JSONObject();
			try
			{
				array.put("Error", "");
				array.put("Code", 0);
				array.put("Old Path", this.get.get("old"));
				array.put("Old Name", filename);
				array.put("New Path", this.get.get("new"));
				array.put("New Name", filename);
			}
			catch (JSONException e)
			{
				return this.error("JSONObject error");
			}
		}
		return array;
	}

	public JSONObject delete()
	{
		File file = new File(this.documentRoot + this.getPath());
		if (file.isDirectory())
		{
			this.unlinkRecursive(this.documentRoot + this.getPath(), true);
			try
			{
				JSONObject array = new JSONObject();
				array.put("Error", "");
				array.put("Code", 0);
				array.put("Path", this.getPath());
				array.put("StoreContext", "/" + Defaults.getInstance().getStoreContext());
				return array;
			}
			catch (Exception e)
			{
				return this.error("JSONObject error");
			}

		}
		if (file.exists())
		{
			if (!file.delete())
			{
				return this.error(sprintf(lang("ERROR_DELETING FILE"), this.getPath()));
			}
			try
			{
				JSONObject array = new JSONObject();
				array.put("Error", "");
				array.put("Code", 0);
				array.put("Path", this.getPath());
				array.put("StoreContext", "/" + Defaults.getInstance().getStoreContext());
				return array;
			}
			catch (JSONException e)
			{
				return this.error("JSONObject error");
			}
		}
		return this.error(lang("INVALID_DIRECTORY_OR_FILE"));
	}

	public JSONObject add()
	{
		JSONObject fileInfo = null;
		Iterator it = this.files.iterator();
		String mode = "";
		String currentPath = "";
		if (!it.hasNext())
		{
			return this.error(lang("INVALID_FILE_UPLOAD"));
		}

		String allowed[] = { ".", "-" };
		FileItem item = null;
		String fileName = "";
		try
		{
			while (it.hasNext())
			{
				item = (FileItem) it.next();
				if (item.isFormField())
				{
					if (item.getFieldName().equals("mode"))
					{
						mode = item.getString();
						if (!mode.equals("add"))
						{
							this.error(lang("INVALID_FILE_UPLOAD"));
						}
					}
					else if (item.getFieldName().equals("currentpath"))
					{
						currentPath = item.getString();
					}
				}
				else if (item.getFieldName().equals("newfile"))
				{
					fileName = item.getName();
					// strip possible directory (IE)
					int pos = fileName.lastIndexOf(File.separator);
					if (pos > 0)
						fileName = fileName.substring(pos + 1);
					boolean error = false;
					long maxSize = 0;
					if (config.getProperty("upload-size") != null)
					{
						maxSize = Integer.parseInt(config.getProperty("upload-size"));
						if (maxSize != 0 && item.getSize() > (maxSize * 1024 * 1024))
						{
							this.error(sprintf(lang("UPLOAD_FILES_SMALLER_THAN"), maxSize + "Mb"));
							error = true;
						}
					}
					if (!error)
					{
						if (!isImage(fileName) && (config.getProperty("upload-imagesonly") != null && config.getProperty("upload-imagesonly").equals("true")
								|| this.params.get("type") != null && this.params.get("type").equals("Image")))
						{
							this.error(lang("UPLOAD_IMAGES_ONLY"));
						}
						else
						{
							fileInfo = new JSONObject();
							LinkedHashMap<String, String> strList = new LinkedHashMap<String, String>();
							strList.put("fileName", fileName);
							fileName = (String) cleanString(strList, allowed).get("fileName");

							if (config.getProperty("upload-overwrite").equals("false"))
							{
								fileName = this.checkFilename(this.documentRoot + currentPath, fileName, 0);
							}

							File saveTo = new File(this.documentRoot + currentPath + fileName);
							item.write(saveTo);

							fileInfo.put("Path", currentPath);
							fileInfo.put("StoreContext", "/" + Defaults.getInstance().getStoreContext());
							fileInfo.put("Name", fileName);
							fileInfo.put("Error", "");
							fileInfo.put("Code", 0);
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			this.error(lang("INVALID_FILE_UPLOAD"));
		}

		return fileInfo;

	}

	public JSONObject addFolder()
	{
		String allowed[] = { "-", " " };
		LinkedHashMap<String, String> strList = new LinkedHashMap<String, String>();
		strList.put("fileName", this.get.get("name"));
		String filename = (String) cleanString(strList, allowed).get("fileName");
		if (filename.length() == 0) // the name existed of only special characters
			return this.error(sprintf(lang("UNABLE_TO_CREATE_DIRECTORY"), this.get.get("name")));

		File file = new File(this.documentRoot + this.getPath() + filename);
		if (file.isDirectory())
		{
			return this.error(sprintf(lang("DIRECTORY_ALREADY_EXISTS"), filename));
		}
		if (!file.mkdir())
		{
			return this.error(sprintf(lang("UNABLE_TO_CREATE_DIRECTORY"), filename));
		}

		try
		{
			JSONObject array = new JSONObject();
			array.put("Parent", this.getPath());
			array.put("Name", filename);
			array.put("Error", "");
			array.put("Code", 0);
			return array;
		}
		catch (JSONException e)
		{
			return this.error("JSONObject error");
		}


	}

	public void download(HttpServletResponse resp)
	{
		File file = new File(this.documentRoot + this.getPath());
		if (this.getPath() != null && file.exists())
		{
			resp.setHeader("Content-type", "application/force-download");
			resp.setHeader("Content-Disposition", "inline;filename=\"" + documentRoot + this.getPath() + "\"");
			resp.setHeader("Content-Transfer-Encoding", "Binary");
			resp.setHeader("Content-length", "" + file.length());
			resp.setHeader("Content-Type", "application/octet-stream");
			resp.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
			readFile(resp, file);
		}
		else
		{
			this.error(sprintf(lang("FILE_DOES_NOT_EXIST"), this.getPath()));
		}
	}

	private void readFile(HttpServletResponse resp, File file)
	{
		OutputStream os = null;
		FileInputStream fis = null;
		try
		{
			os = resp.getOutputStream();
			fis = new FileInputStream(file);
			byte fileContent[] = new byte[(int) file.length()];
			fis.read(fileContent);
			os.write(fileContent);
		}
		catch (Exception e)
		{
			this.error(sprintf(lang("INVALID_DIRECTORY_OR_FILE"), file.getName()));
		}
		finally
		{
			try
			{
				if (os != null)
					os.close();
			}
			catch (Exception e2)
			{
			}
			try
			{
				if (fis != null)
					fis.close();
			}
			catch (Exception e2)
			{
			}
		}
	}

	public void preview(HttpServletResponse resp)
	{
		File file = new File(this.documentRoot + this.get.get("path"));
		if (this.getPath() != null && file.exists())
		{
			resp.setHeader("Content-type", "image/" + getFileExtension(file.getName()));
			resp.setHeader("Content-Transfer-Encoding", "Binary");
			resp.setHeader("Content-length", "" + file.length());
			resp.setHeader("Content-Disposition", "inline; filename=\"" + getFileBaseName(file.getName()) + "\"");
			readFile(resp, file);
		}
		else
		{
			error(sprintf(lang("FILE_DOES_NOT_EXIST"), this.getPath()));
		}
	}

	public JSONObject usageCheck()
	{

		File file = new File(this.documentRoot + this.get.get("path"));
		DataAccess da = DataAccess.getInstance();
		int usageCount = 0;
		JSONObject usage = new JSONObject();
		List<Map<String, String>> fileUsage = new ArrayList<Map<String, String>>();
		try
		{
			List<Map<String, String>> modules = ModuleHelper.getInstance().getModules();
			for (int i = 0; i < modules.size(); i++)
			{
				String[] columns = null;
				String moduleName = modules.get(i).get("internal_module_name");
				String clientName = modules.get(i).get("client_module_name");
				columns = ModuleHelper.getInstance().getElementSharedFileColumns(moduleName);
				if (columns.length > 0)
				{
					for (int e = 0; e < columns.length; e++)
					{
						String sql = "select * from elements_" + moduleName + " where " + columns[e] + " = '" + this.get.get("path") + "'";
						List<Map<String, String>> matches = da.select(sql, new HashtableMapper());
						usageCount += matches.size();
						for (Map<String, String> item : matches)
						{
							Map<String, String> fileInfo = new HashMap<String, String>();
							fileInfo.put("type", "Element");
							fileInfo.put("module", clientName);
							fileInfo.put("headline", item.get("ATTR_Headline"));
							fileUsage.add(fileInfo);
						}
					}
				}
				columns = ModuleHelper.getInstance().getCategorySharedFileColumns(moduleName);
				if (columns.length > 0)
				{
					for (int c = 0; c < columns.length; c++)
					{
						String sql = "select * from categories_" + moduleName + " where " + columns[c] + " = '" + this.get.get("path") + "'";
						List<Map<String, String>> matches = da.select(sql, new HashtableMapper());
						usageCount += matches.size();
						for (Map<String, String> item : matches)
						{
							Map<String, String> fileInfo = new HashMap<String, String>();
							fileInfo.put("type", "Category");
							fileInfo.put("module", clientName);
							fileInfo.put("headline", item.get("ATTR_CategoryName"));
							fileUsage.add(fileInfo);
						}
					}
				}
			}
			usage.put("used", usageCount > 0 ? true : false);
			usage.put("times", usageCount);
			usage.put("usageInformation", fileUsage);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
		}

		return usage;
	}

	private String getFileBaseName(String filename)
	{
		String retval = filename;
		int pos = filename.lastIndexOf(".");
		if (pos > 0)
			retval = filename.substring(0, pos);
		return retval;
	}

	private String getFileExtension(String filename)
	{
		String retval = filename;
		int pos = filename.lastIndexOf(".");
		if (pos > 0)
			retval = filename.substring(pos + 1);
		return retval;
	}

	private void setParams()
	{
		String[] tmp = this.referer.split("\\?");
		String[] params_tmp = null;
		LinkedHashMap<String, String> params = new LinkedHashMap<String, String>();
		if (tmp.length > 1 && tmp[1] != "")
		{
			params_tmp = tmp[1].split("&");
			for (int i = 0; i < params_tmp.length; i++)
			{
				tmp = params_tmp[i].split("=");
				if (tmp.length > 1 && tmp[1] != "")
				{
					params.put(tmp[0], tmp[1]);
				}
			}
		}
		this.params = params;
	}

	public String getConfigString(String key)
	{
		return config.getProperty(key);
	}

	public String getDocumentRoot()
	{
		return this.documentRoot;
	}

	private void getFileInfo(String path)
	{
		String pathTmp = path;
		if (pathTmp == "")
		{
			pathTmp = this.getPath();
		}
		String[] tmp = pathTmp.split("/");
		File file = new File(this.documentRoot + pathTmp);
		this.item = new HashMap();
		String fileName = tmp[tmp.length - 1];
		this.item.put("filename", fileName);
		if (file.isFile())
			this.item.put("filetype", fileName.substring(fileName.lastIndexOf(".") + 1));
		else
			this.item.put("filetype", "dir");
		this.item.put("filemtime", "" + file.lastModified());
		this.item.put("filectime", "" + file.lastModified());

		//this.item.put("preview", config.getProperty("icons-path") + "/" + config.getProperty("icons-default")); // @simo
		this.item.put("fa-icon", config.getProperty("fa-icons-default"));

		HashMap<String, String> props = new HashMap();
		if (file.isDirectory())
		{

			//this.item.put("preview", config.getProperty("icons-path") + config.getProperty("icons-directory"));
			this.item.put("fa-icon", config.getProperty("fa-icons-directory"));

		}
		else if (isImage(pathTmp))
		{
			this.item.put("preview", "/webdirector/adminExtra/filemanager/connectors?mode=preview&path=" + pathTmp);
			Dimension imgData = getImageSize(documentRoot + pathTmp);
			props.put("Height", "" + imgData.height);
			props.put("Width", "" + imgData.width);
			props.put("Size", "" + file.length());
		}
		else
		{
			props.put("Size", "" + file.length());
			File icon = new File(fileManagerRoot + config.getProperty("icons-path") + ((String) this.item.get("filetype")).toLowerCase() + ".png");
			if (icon.exists())
			{
				this.item.put("preview", config.getProperty("icons-path") + ((String) this.item.get("filetype")).toLowerCase() + ".png");
			}
		}

		props.put("Date Modified", dateFormat.format(new Date(new Long((String) this.item.get("filemtime")))));
		this.item.put("properties", props);
	}

	private boolean isImage(String fileName)
	{
		boolean isImage = false;
		String ext = "";
		int pos = fileName.lastIndexOf(".");
		if (pos > 1 && pos != fileName.length())
		{
			ext = fileName.substring(pos + 1);
			isImage = contains(config.getProperty("images"), ext);
		}
		return isImage;
	}

	public boolean contains(String where, String what)
	{
		boolean retval = false;

		String[] tmp = where.split(",");
		for (int i = 0; i < tmp.length; i++)
		{
			if (what.equalsIgnoreCase(tmp[i]))
			{
				retval = true;
				break;
			}
		}
		return retval;
	}

	private Dimension getImageSize(String path)
	{
		Dimension imgData = new Dimension();
		Image img = new ImageIcon(path).getImage();
		imgData.height = img.getHeight(null);
		imgData.width = img.getWidth(null);
		return imgData;
	}

	private void unlinkRecursive(String dir, boolean deleteRootToo)
	{
		File dh = new File(dir);
		File fileOrDir = null;

		if (dh.exists())
		{
			String[] objects = dh.list();
			for (int i = 0; i < objects.length; i++)
			{
				fileOrDir = new File(dir + "/" + objects[i]);
				if (fileOrDir.isDirectory())
				{
					if (!objects[i].equals(".") && !objects[i].equals(".."))
					{
						unlinkRecursive(dir + "/" + objects[i], true);
					}
				}
				fileOrDir.delete();


			}
			if (deleteRootToo)
			{
				dh.delete();
			}
		}
	}

	private HashMap<String, String> cleanString(HashMap<String, String> strList, String[] allowed)
	{
		String allow = "";
		HashMap<String, String> cleaned = null;
		Iterator<String> it = null;
		String cleanStr = null;
		String key = null;
		for (int i = 0; i < allowed.length; i++)
		{
			allow += "\\" + allowed[i];
		}

		if (strList != null)
		{
			cleaned = new HashMap<String, String>();
			it = strList.keySet().iterator();
			while (it.hasNext())
			{
				key = it.next();
				cleanStr = strList.get(key).replaceAll("[^{" + allow + "}_a-zA-Z0-9]", "");
				cleaned.put(key, cleanStr);
			}
		}
		return cleaned;
	}

	private String sanitize(String var)
	{
		String sanitized = var.replaceAll("\\<.*?>", "");
		sanitized = sanitized.replaceAll("http://", "");
		sanitized = sanitized.replaceAll("https://", "");
		sanitized = sanitized.replaceAll("\\.\\./", "");
		return sanitized;
	}

	private String checkFilename(String path, String filename, int i)
	{
		File file = new File(path + filename);
		String i2 = "";
		String[] tmp = null;
		if (!file.exists())
		{
			return filename;
		}
		else
		{
			if (i != 0)
				i2 = "" + i;
			tmp = filename.split(i2 + "\\.");
			i++;
			filename = filename.replace(i2 + "." + tmp[tmp.length - 1], i + "." + tmp[tmp.length - 1]);
			return this.checkFilename(path, filename, i);
		}
	}

	private void loadConfig()
	{
		InputStream is = null;
		if (config == null)
		{
			try
			{
				is = getClass().getClassLoader().getResourceAsStream("com/nartex/config.properties");
				config = new Properties();
				config.load(is);
			}
			catch (Exception e)
			{
				error("Error loading config file");
			}
			finally
			{
				IOUtils.closeQuietly(is);
			}
		}
	}

	private void loadLanguageFile()
	{

		// we load langCode var passed into URL if present
		// else, we use default configuration var
		if (language == null)
		{
			String lang = "";
			if (params.get("langCode") != null)
				lang = this.params.get("langCode");
			else
				lang = config.getProperty("culture");
			BufferedReader br = null;
			InputStreamReader isr = null;
			String text;
			StringBuffer contents = new StringBuffer();
			try
			{
				isr = new InputStreamReader(new URL(this.fileManagerRoot + "/scripts/languages/" + lang + ".js").openStream());
				br = new BufferedReader(isr);
				while ((text = br.readLine()) != null)
					contents.append(text);
				language = new JSONObject(contents.toString());
			}
			catch (Exception e)
			{
				this.error("Fatal error: Language file not found.");
			}
			finally
			{
				try
				{
					if (br != null)
						br.close();
				}
				catch (Exception e2)
				{
				}
				try
				{
					if (isr != null)
						isr.close();
				}
				catch (Exception e2)
				{
				}
			}
		}
	}

	public String sprintf(String text, String params)
	{
		String retText = text;
		String[] repl = params.split("#");
		for (int i = 0; i < repl.length; i++)
		{
			retText = retText.replaceFirst("%s", repl[i]);
		}
		return retText;
	}

	public void log(String filename, String msg)
	{
		try
		{
			BufferedWriter out = new BufferedWriter(new FileWriter(filename, true));
			out.append(msg + "\r\n");
			out.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private String getPath()
	{
		return this.get.get("path");
//		return this.get.get("path");
	}
}
