/**
 * @author Nick Yiming Gao
 * @date 2016-3-9
 */
package au.net.webdirector.admin.extra.ckeditor;

/**
 * 
 */

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/adminExtra/ckeditor")
public class CKEditorController
{

	private static Logger logger = Logger.getLogger(CKEditorController.class);

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String edit() throws IOException
	{
		return "../adminExtra/FCK/editor";
	}

	@RequestMapping(value = "/templates", method = RequestMethod.GET)
	public String templates() throws IOException
	{
		return "../adminExtra/FCK/templates";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save() throws IOException
	{
		return "../adminExtra/FCK/editor_h";
	}

	@RequestMapping(value = "/delete-template", method = RequestMethod.GET)
	public String deleteTemplate() throws IOException
	{
		return "../adminExtra/FCK/delete-template";
	}

	@RequestMapping(value = "/config", method = RequestMethod.GET)
	public String config() throws IOException
	{
		return "../adminExtra/ckeditor/config";
	}
}
