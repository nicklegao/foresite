/**
 * @author Nick Yiming Gao
 * @date 2016-3-9
 */
package au.net.webdirector.admin.extra.filemanager;

/**
 * 
 */

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/adminExtra/filemanager")
public class FileManagerController
{

	private static Logger logger = Logger.getLogger(FileManagerController.class);

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index() throws IOException
	{
		return "../adminExtra/filemanager/index";
	}


}
