<%@page import="au.com.quotecloud.recaptcha.ReCaptchaV2"%>
<%@page import="au.com.quotecloud.recaptcha.ReCaptchaResponseV2"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html>
<!DOCTYPE html>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%
	String AssetType = "Dynamic Form Builder";
String AssetID = "0";
String categoryID = null;
String moduleName = null;
%>
<%@ include file="/client/IncludeFiles/i_init.jsp" %>

<jsp:useBean id="email" class="au.net.webdirector.common.utils.email.EmailUtils" scope="session" />
<jsp:setProperty name="email" property="*" />
</head>

<body>
<div style="position: fixed; top: 0; left: 0; width: 100%; height: 100%; background: #FFF; z-index: 1000;"></div>
<!--
  This is special Dynamic Form Builder JSP, This page will read all input field from request object, collect their values
  and than send all details to specified user.
  
  Note: Things need to be include in Form
    Form Submit Action:: Form must have an action using this JSP: c_dynamicFormEmailer.jsp so an example may look like:
    <form name="someFormName" action="/<%= warName %>/client/ContactUs/c_dynamicFormEmailer.jsp" method="POST"> 
  
  Few Hidden variables:
    fromEmailAddress: must provide hidden input variable name fromEmailAddress with proper value of emailAddress
    subject::  must provide subject variable for Subject of Email
    toEmailAddress: User must provide toEmailAdrress  for sending email to particular user.
    redirectURL: user must provide redirectURL to redirect after sending an Email.
    
  so for example the fromEmailAddress hidden tag would look like:
    <input type="hidden" name = "toEmailAddress" value="bhavin.mehta@traveldocs.cloud">
  
  Naming Convention:
    Names of the input variables fileds in form will be use as variable field in email so you must user friendly form names.
    You can use space in between names for more readability
 -->
<%
String remoteAddr = request.getRemoteAddr();
String gresponse = request.getParameter("g-recaptcha-response");
ReCaptchaResponseV2 reCaptchaResponse = null;
if(gresponse != null) {
	reCaptchaResponse = ReCaptchaV2.checkAnswerV2("6LfA9y0UAAAAAGqTKIBKaVRuetapjjT9b9hEPiP5", remoteAddr, gresponse);
}
String redirectUrl="";
String referer = request.getHeader("REFERER");
boolean retInfo = false;
boolean sendEmail = false;
if(reCaptchaResponse != null && reCaptchaResponse.isValid()) {
	
	System.out.println("EMAIL");
	sendEmail = true;
	Map map = request.getParameterMap();
	Vector v = new Vector(map.keySet());
	Collections.sort(v);
	String sessionKaptcha = (String)session.getAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);
	String fromEmailAddress = "contactus@traveldocs.cloud";
	String returnEmailAddress = "";
	String toEmailAddress="darren@traveldocs.cloud";
	String subject ="";
	String name = "";
	String phone = "";
	String message = "";
	redirectUrl = request.getParameter("redirectURL");
	
	int i=0;
	StringBuffer bodyText = new StringBuffer("<table cellpadding='5' cellspacing='1' border='0'>");
	
	for (Enumeration e = v.elements(); e.hasMoreElements();) {
		String key = (String)e.nextElement();
	  	String value = request.getParameter(key);
	
	  	value = (value != null && !"".equals(value) && !"null".equalsIgnoreCase(value)) ? value: "";
	
	  	if(key.equals("01_Name")){
		    name = value;
		    bodyText.append("<tr><td>Name:</td><td>"+name+"</td></tr>");
		    continue;
		}
		  
		if(key.equals("02_Email_Address")){
			
			returnEmailAddress = value;
		    bodyText.append("<tr><td>Email:</td><td>"+returnEmailAddress+"</td></tr>");
			continue;
		}
		
		if(key.equals("03_Phone_Number")){
			phone = value;
		    bodyText.append("<tr><td>Phone:</td><td>"+phone+"</td></tr>");
			continue;
		}
		
		if(key.equals("subject")){
			subject = value;
			continue;
		}
		
		if(key.equals("05_Comments")){
			message = value.replace("\n", "</br>");
		    bodyText.append("<tr><td>Message:</td><td>"+message+"</td></tr>");
			continue;
		}
	  
		if(key.equals("fromEmailAddress")){
	    	fromEmailAddress = value;
	    	continue;
	  	}
	  
		if(key.equals("toEmailAddress")){
		    toEmailAddress = value;
		    continue;
	  	}
		
		if (key.equalsIgnoreCase("redirectURL")){
		  	continue;
		}
		
		if (key.indexOf("_") != 2){
		  	continue;
		}
		
		String label = key.substring(3);
		
		if(label.lastIndexOf("-")!=-1){
		  	label = label.substring(0, label.lastIndexOf("-"));
		}
		
		try{
		  	Integer.parseInt(key.substring(0,2));
		}catch(Exception ex){
			continue;
		}
		
		if(i%2==0){
			if(value != null && !"".equals(value) && !"null".equalsIgnoreCase(value)){
		    	bodyText.append("<tr><td style='border-bottom:1px solid #E6EAD5; text-align:right; font-family :helvetica; font-size:11px;'>"+label+"</td><td width='500' style='border:5px #E6EAD5 solid; font-family :helvetica; font-size:11px;'>"+value+"</td></tr>");
			}
		}else{
		  	if(value != null && !"".equals(value) && !"null".equalsIgnoreCase(value)){
		    	bodyText.append("<tr><td style='border-bottom:1px solid #E6EAD5; text-align:right; font-family :helvetica; font-size:11px;'>"+label+"</td><td width='500' style='border:5px #E6EAD5 solid; font-family :helvetica; font-size:11px;'>"+value+"</td></tr>");
		  	}
		}
		i++;
	}
	//bodyText.append("<tr><td>&nbsp;</td><td></td></tr><tr><td>This online form response was from the Website URL:</td> <td>"+d.getWebSiteURL()+"</td></tr>");
	
	bodyText.append("</table>");
	
	if(!fromEmailAddress.equals("") && !fromEmailAddress.equals("null") && !toEmailAddress.equals("") && !toEmailAddress.equals("null")&& !subject.equals("")){
	  String[] emailAddress = {toEmailAddress, "andrew@traveldocs.cloud"};
	  String[] fromEmails = {fromEmailAddress, returnEmailAddress};
	  if(sendEmail){
		  System.out.println(emailAddress);
	    //retInfo = email.sendMail(emailAddress , null, null, fromEmailAddress, returnEmailAddress, name, phone, subject, message, null);
	    retInfo = email.sendMail(emailAddress , null, null, fromEmailAddress, subject, bodyText.toString(), null);
	  }
	}
}
%>
<link href="/quotecloud/js/plugins/SweetAlert2/sweetalert2.css" rel="stylesheet" media="screen">
<script src="/quotecloud/js/plugins/SweetAlert2/sweetalert2.js"></script>
<%if(reCaptchaResponse != null && !reCaptchaResponse.isValid()){%>
<script>
//  alert("Your reCaptcha token is invalid or has expired. Please try again.");
	$(document).ready(function() {
        $("#footer").empty();
        swal({
            text: "Your reCaptcha token is invalid or has expired. Please try again.",
            type: "error"
        }).then(function () {
            window.location.href = "<%=referer%>";
        })
    })
</script>
<%}else if(!sendEmail){%>
  <script>
//    alert("Oops, there was a problem with the information you typed in the form, please check and try again.");
	$(document).ready(function(){
        $("#footer").empty();
		swal({
			text: "Oops, there was a problem with the information you typed in the form, please check and try again.",
			type: "error"
		}).then(function(){
			window.location.href="<%=referer%>";
		})
    })
  </script>
<%}else if(retInfo){%>
  <script>
//    alert("Thank you, we have received your request.  We will reply shortly.");
	$(document).ready(function() {
        $("#footer").empty();
        swal({
            text: "Thank you, we have received your request.  We will reply shortly.",
            type: "success"
        }).then(function () {
            window.location.href = "<%=redirectUrl%>";
        })
    })
  </script>
<%}else{%>
  <script>
//    alert("That's embarrassing! There was an error while attempting to send your message, please try again later.");
	$(document).ready(function() {
        $("#footer").empty();
        swal({
            text: "That's embarrassing! There was an error while attempting to send your message, please try again later.",
            type: "error"
        }).then(function () {
            window.location.href = "<%=redirectUrl%>";
        })
    })
  </script>
<%}%>

</body>
</html>
</compress:html>