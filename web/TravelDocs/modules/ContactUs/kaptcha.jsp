<%@ page import="java.awt.*"%>
<%@ page import="java.awt.font.*"%>
<%@ page import="java.awt.geom.*"%>
<%@ page import="java.awt.image.*"%>
<%@ page import="java.io.*"%>
<%@ page import="java.util.*"%>
<%@ page import="javax.imageio.*"%>
<%  
response.setHeader("Cache-Control","no-store"); 
response.setDateHeader("Expires",0); 
response.setContentType("image/jpeg"); 
String[] QA = generateQA();
generateJPG(QA[0], response.getOutputStream());
session.setAttribute("com.corporateinteractive.kaptcha.answer",QA[1]);
%>
<%!
static Random r = new Random();

private static String[] generateQA() {
  int a = r.nextInt(9)+1;
  int b = r.nextInt(9)+1;
  int c = a+b;
  return new String[]{a+" + "+b+" = ?", String.valueOf(c)};
}

private static void generateJPG(String s, OutputStream output) throws IOException {
  int width = 200;
  int height = 50;
  Font font = new Font("Serif", Font.BOLD, 36);
  BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
  Graphics2D g2 = (Graphics2D) bi.getGraphics();
  g2.setBackground(Color.LIGHT_GRAY);
  g2.clearRect(0, 0, width, height);
  g2.setPaint(Color.BLACK);
  g2.setFont(font);
  FontRenderContext context = g2.getFontRenderContext();
  Rectangle2D bounds = font.getStringBounds(s, context);
  double x = (width - bounds.getWidth()) / 2;
  double y = (height - bounds.getHeight()) / 2;
  double ascent = -bounds.getY();
  double baseY = y + ascent;
  g2.drawString(s, (int) x, (int) baseY);
  ImageIO.write(bi, "jpg", output);
}
%>



