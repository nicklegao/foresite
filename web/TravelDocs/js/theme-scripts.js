function initialize() {
    geocoder = new google.maps.Geocoder;
    var a = new google.maps.LatLng(-34.397, 150.644),
        b = {
            center: a,
            zoom: zoom,
            scrollwheel: !1,
            disableDefaultUI: !0,
            styles: [{
                featureType: "all",
                elementType: "all",
                stylers: [{
                    color: "#ffffff"
                }, {
                    visibility: "off"
                }]
            }, {
                featureType: "all",
                elementType: "geometry",
                stylers: [{
                    visibility: "off"
                }]
            }, {
                featureType: "all",
                elementType: "geometry.fill",
                stylers: [{
                    color: "#ffffff"
                }, {
                    visibility: "on"
                }]
            }, {
                featureType: "all",
                elementType: "geometry.stroke",
                stylers: [{
                    visibility: "off"
                }]
            }, {
                featureType: "all",
                elementType: "labels.text",
                stylers: [{
                    color: "#6300ff"
                }, {
                    visibility: "off"
                }]
            }, {
                featureType: "administrative",
                elementType: "all",
                stylers: [{
                    weight: "0.01"
                }, {
                    visibility: "off"
                }]
            }, {
                featureType: "administrative.province",
                elementType: "all",
                stylers: [{
                    visibility: "off"
                }]
            }, {
                featureType: "administrative.province",
                elementType: "geometry.fill",
                stylers: [{
                    visibility: "on"
                }]
            }, {
                featureType: "administrative.province",
                elementType: "labels.text.fill",
                stylers: [{
                    visibility: "on"
                }, {
                    color: "#ffffff"
                }]
            }, {
                featureType: "administrative.locality",
                elementType: "all",
                stylers: [{
                    visibility: "simplified"
                }, {
                    color: "#aa7cff"
                }]
            }, {
                featureType: "landscape",
                elementType: "all",
                stylers: [{
                    color: "#3a3a3a"
                }, {
                    visibility: "on"
                }]
            }, {
                featureType: "landscape.natural.landcover",
                elementType: "all",
                stylers: [{
                    color: "#c96363"
                }, {
                    visibility: "off"
                }]
            }, {
                featureType: "landscape.natural.terrain",
                elementType: "all",
                stylers: [{
                    color: "#533434"
                }, {
                    visibility: "off"
                }]
            }, {
                featureType: "poi",
                elementType: "all",
                stylers: [{
                    visibility: "on"
                }, {
                    color: "#3a3a3a"
                }]
            }, {
                featureType: "poi",
                elementType: "labels.text",
                stylers: [{
                    visibility: "off"
                }]
            }, {
                featureType: "poi.park",
                elementType: "all",
                stylers: [{
                    visibility: "on"
                }]
            }, {
                featureType: "poi.park",
                elementType: "geometry",
                stylers: [{
                    color: "#96cb4b"
                }]
            }, {
                featureType: "poi.park",
                elementType: "labels.text",
                stylers: [{
                    visibility: "off"
                }]
            }, {
                featureType: "road",
                elementType: "all",
                stylers: [{
                    visibility: "simplified"
                }, {
                    weight: "0.5"
                }]
            }, {
                featureType: "road",
                elementType: "geometry.fill",
                stylers: [{
                    visibility: "on"
                }, {
                    color: "#464646"
                }]
            }, {
                featureType: "road",
                elementType: "geometry.stroke",
                stylers: [{
                    visibility: "off"
                }]
            }, {
                featureType: "road",
                elementType: "labels.text",
                stylers: [{
                    visibility: "off"
                }, {
                    weight: "0.01"
                }]
            }, {
                featureType: "road.highway",
                elementType: "all",
                stylers: [{
                    visibility: "on"
                }]
            }, {
                featureType: "road.highway",
                elementType: "geometry.fill",
                stylers: [{
                    weight: "2"
                }, {
                    color: "#ff9500"
                }, {
                    invert_lightness: !0
                }]
            }, {
                featureType: "road.highway",
                elementType: "geometry.stroke",
                stylers: [{
                    visibility: "off"
                }]
            }, {
                featureType: "road.highway",
                elementType: "labels.text",
                stylers: [{
                    visibility: "off"
                }]
            }, {
                featureType: "road.highway",
                elementType: "labels.text.fill",
                stylers: [{
                    color: "#ff0000"
                }, {
                    visibility: "off"
                }]
            }, {
                featureType: "road.highway",
                elementType: "labels.icon",
                stylers: [{
                    visibility: "off"
                }]
            }, {
                featureType: "water",
                elementType: "all",
                stylers: [{
                    visibility: "on"
                }, {
                    color: "#ffffff"
                }]
            }, {
                featureType: "water",
                elementType: "geometry.stroke",
                stylers: [{
                    color: "#652323"
                }, {
                    visibility: "off"
                }]
            }]
        };
    map = new google.maps.Map(document.getElementById("map-canvas"), b), codeAddress()
}

function codeAddress() {
    var a = query;
    geocoder.geocode({
        address: a
    }, function(a, b) {
        if (b == google.maps.GeocoderStatus.OK) {
            map.setCenter(a[0].geometry.location);
            new google.maps.Marker({
                map: map,
                position: a[0].geometry.location,
                icon: "img/map-marker.png",
                title: "My Office"
            })
        } else alert("Geocode was not successful for the following reason: " + b)
    })
}
jQuery(document).ready(function(a) {
    "use strict";
    var b = a(".intro"),
        c = a(".content-wrap"),
        d = a(".footer");
    a("a[href=#]").click(function(a) {
        a.preventDefault()
    }), Modernizr.touch ? (a(window).load(function() {
        a(".intro .column-wrap").css("height", a(window).height())
    }), a(window).resize(function() {
        a(".intro .column-wrap").css("height", a(window).height())
    }), a(window).on("load", function() {
        if (a(".navbar-sticky").length > 0) {
            new Waypoint.Sticky({
                element: a(".navbar-sticky")[0]
            })
        }
    })) : (a(window).load(function() {
        a(".intro .column-wrap").css("height", a(window).height()), c.css("margin-top", 1.5 * b.height()), a(".fixed-footer").length > 0 && c.css("margin-bottom", d.outerHeight()), a(".intro-features .icon-block").each(function() {
            var b = a(this).data("transition-delay");
            a(this).css({
                "transition-delay": b + "ms"
            })
        }), Detectizr.detect({
            detectScreen: !1
        })
    }), a(window).resize(function() {
        a(".intro .column-wrap").css("height", a(window).height()), c.css("margin-top", 1.5 * b.height()), a(".fixed-footer").length > 0 && c.css("margin-bottom", d.outerHeight())
    }), a(window).scroll(function() {
        a(window).scrollTop() > 15 ? b.addClass("transformed") : b.removeClass("transformed")
    }), a(window).on("load", function() {
        if (a(".navbar-sticky").length > 0) {
            new Waypoint.Sticky({
                element: a(".navbar-sticky")[0],
                handler: function(a) {
                    "down" == a ? (d.addClass("footer-fixed-bottom"), b.addClass("transparent")) : (d.removeClass("footer-fixed-bottom"), b.removeClass("transparent"))
                }
            })
        }
    })), Waves.displayEffect({
        duration: 600
    });
    var e = a(".form-control input, .form-control textarea");
    e.on("focus", function() {
        "" == this.value && a(this).parent().addClass("active")
    }), e.on("blur", function() {
        "" == this.value && a(this).parent().removeClass("active")
    });
    var e = a(".form-control input, .form-control textarea");
    e.on("focus", function() {
        "" == this.value && a(this).parent().addClass("active")
    }), e.on("blur", function() {
        "" == this.value && a(this).parent().removeClass("active")
    });
    var f = a('[data-offcanvas="open"]'),
        g = a('[data-offcanvas="close"]'),
        h = a(".offcanvas-nav");
    f.click(function() {
        h.addClass("open"), a("body").append('<div class="offcanvas-backdrop"></div>')
    }), g.click(function() {
        h.removeClass("open"), a(".offcanvas-backdrop").remove()
    }), a(document).on("click", ".offcanvas-backdrop", function() {
        h.removeClass("open"), a(".offcanvas-backdrop").remove()
    });
    var i = a(".search-box"),
        j = a(".search-box input");
    a("body").on("click", function() {
        "" == j.val() && i.removeClass("open")
    }), i.click(function(a) {
        a.stopPropagation()
    }), a(".search-box .search-toggle").click(function() {
        a(this).parent().toggleClass("open"), setTimeout(function() {
            a("#search-field").length > 0 && a("#search-field").focus()
        }, 500)
    }), a(".form-switch a").click(function(b) {
        var c = a(this).attr("href");
        b.preventDefault(), a(c + "> a").click()
    }), a('[data-modal-form="sign-in"]').click(function() {
        a("#form-2 a").click()
    }), a('[data-modal-form="sign-up"]').click(function() {
        a("#form-1 a").click()
    }), a(".feature-tabs .nav-tabs li a").on("click", function() {
        var b = a(this).data("phone"),
            c = a(this).data("tablet");
        a(".devices .phone .screens li").removeClass("active"), a(".devices .tablet .screens li").removeClass("active"), a(b).addClass("active"), a(c).addClass("active")
    });
    var k;
    if (a(".feature-tabs .tabs").on("mouseover", function() {
            clearInterval(k)
        }), a(".feature-tabs .tabs").on("mouseout", function() {
            if (a(".feature-tabs .nav-tabs[data-autoswitch]").length > 0) {
                var b = a(".feature-tabs .nav-tabs").data("interval");
                k = setInterval(function() {
                    var b = a(".feature-tabs .nav-tabs > li"),
                        c = b.filter(".active"),
                        d = c.next("li");
                    (d.length ? d.find("a") : b.eq(0).find("a")).trigger("click")
                }, b)
            }
        }), a(".feature-tabs .nav-tabs[data-autoswitch]").length > 0) {
        var l = a(".feature-tabs .nav-tabs").data("interval");
        k = setInterval(function() {
            var b = a(".feature-tabs .nav-tabs > li"),
                c = b.filter(".active"),
                d = c.next("li");
            (d.length ? d.find("a") : b.eq(0).find("a")).trigger("click")
        }, l)
    }
    a("#signin-form").length > 0 && a("#signin-form").validate(), a("#signup-form").length > 0 && a("#signup-form").validate(), a("#comment-form").length > 0 && a("#comment-form").validate(), a("#form-demo").length > 0 && a("#form-demo").validate(), a(window).load(function() {
        a(".scroller").mCustomScrollbar({
            axis: "x",
            theme: "dark",
            scrollInertia: 300,
            advanced: {
                autoExpandHorizontalScroll: !0
            }
        })
    });
    var m = a(".nav-body .overflow");
    a(window).load(function() {
        m.height(a(window).height() - a(".nav-head").height() - 80), m.mCustomScrollbar({
            theme: "dark",
            scrollInertia: 300,
            scrollbarPosition: "outside"
        })
    }), a(window).resize(function() {
        m.height(a(window).height() - a(".nav-head").height() - 80)
    }), a(".app-gallery a").length > 0 && a(".app-gallery a").magnificPopup({
        type: "image",
        gallery: {
            enabled: !0
        },
        removalDelay: 300,
        mainClass: "mfp-fade"
    }), a(".video-popup").length > 0 && a(".video-popup").magnificPopup({
        type: "iframe",
        removalDelay: 300,
        mainClass: "mfp-fade"
    }), a(".popup-image").length > 0 && a(".popup-image").magnificPopup({
        type: "image",
        gallery: {
            enabled: !0
        },
        removalDelay: 300,
        mainClass: "mfp-fade"
    }), a(".popup-video").length > 0 && a(".popup-video").magnificPopup({
        type: "iframe",
        removalDelay: 300,
        mainClass: "mfp-fade"
    }), a(".popup-img").length > 0 && a(".popup-img").magnificPopup({
        type: "image",
        gallery: {
            enabled: !0
        },
        removalDelay: 300,
        mainClass: "mfp-fade"
    }), a(window).load(function() {
        if (a(".masonry-grid").length > 0 && a(".masonry-grid").isotope({
                itemSelector: ".item",
                masonry: {
                    columnWidth: ".grid-sizer",
                    gutter: ".gutter-sizer"
                }
            }), a(".portfolio-grid").length > 0 && a(".portfolio-grid").isotope({
                itemSelector: ".grid-item",
                masonry: {
                    columnWidth: ".grid-sizer",
                    gutter: ".gutter-sizer"
                }
            }), a(".filter-grid").length > 0) {
            var b = a(".filter-grid");
            a(".nav-filters").on("click", "a", function(c) {
                c.preventDefault(), a(".nav-filters li").removeClass("active"), a(this).parent().addClass("active");
                var d = a(this).attr("data-filter");
                b.isotope({
                    filter: d
                })
            })
        }
    }), a(".page-slider input").length > 0 && a(".page-slider input").slider({
        formatter: function(a) {
            return a
        }
    }), a(window).load(function() {
        a(".bar-charts .chart").each(function() {
            var b = a(this).data("percentage");
            a(this).find(".bar").css("height", b + "%")
        })
    }), a(document).on("ifChecked", "[name=plan], [name=currency]", function(b) {
        var c = a('[name="plan"]:checked').data("term"),
            d = a('[name="currency"]:checked').data("term");
        a(".pricing-plan-title").each(function(b, e) {
            var f = a(e).find(".price"),
                planPrice = a(e).find(".planPrice"),
                userPrice = a(e).find(".userPrice"),
                storagePrice = a(e).find(".storagePrice"),
                g = a(e).find(".period"),
                h = a(e).find(".currency"),
                i = f.data(d + "-" + c);
            planPrice.text(d.toUpperCase() + " / " + c), 
            userPrice.text(d.toUpperCase() + " /user/ " + c), 
            storagePrice.text(d.toUpperCase() + " /5gb/ " + c),
            f.text(i)
        })
    }), a(".scroll").click(function(b) {
        var c = a(this).data("offset-top");
        a("html").velocity("scroll", {
            offset: a(this.hash).offset().top - c,
            duration: 1e3,
            easing: "easeOutExpo"
        }), a(window).width() < 768 && (h.removeClass("open"), a(".offcanvas-backdrop").remove()), b.preventDefault()
    }), a(".scrollup").click(function(b) {
        b.preventDefault(), a("html").velocity("scroll", {
            offset: 0,
            duration: 1400,
            mobileHA: !1
        })
    });
    var n, o = a("#scroll-nav"),
        p = o.outerHeight(),
        q = o.find("a"),
        r = q.map(function() {
            var b = a(a(this).attr("href"));
            if (b.length) return b
        });
    a(window).scroll(function() {
        var b = a(this).scrollTop() + p + 200,
            c = r.map(function() {
                if (a(this).offset().top < b) return this
            });
        c = c[c.length - 1];
        var d = c && c.length ? c[0].id : "";
        n !== d && (n = d, q.parent().removeClass("active").end().filter("[href=#" + d + "]").parent().addClass("active"))
    })
});
var geocoder, map, query = $(".google-map").data("location"),
    zoom = $(".google-map").data("zoom");
$("#map-canvas").length > 0 && google.maps.event.addDomListener(window, "load", initialize);