$("#sampleforms").validate({
    errorClass: "error-label",
    errorElement: "span",
    rules: {
        "01_Name": "required",
        "02_Email_Address": "required"
    },
    highlight: function(a) {
        $(a).addClass("error")
    },
    unhighlight: function(a) {
        $(a).removeClass("error")
    },
    submitHandler: function(a) {
        a.submit();
        if (typeof grecaptcha != 'undefined')
        {
            grecaptcha.reset(widget2);
        }
    },
    invalidHandler : function(event, validator)
    { // display error alert on form submit
        if (typeof grecaptcha != 'undefined')
        {
            grecaptcha.reset(widget2);
        }
    }
})

$("#contactforms").validate({
    errorClass: "error-label",
    errorElement: "span",
    rules: {
        "01_Name": "required",
        "02_Email_Address": "required",
        "03_Message": "required"
    },
    highlight: function(a) {
        $(a).addClass("error")
    },
    unhighlight: function(a) {
        $(a).removeClass("error")
    },
    submitHandler: function(a) {
        a.submit();
        if (typeof grecaptcha != 'undefined')
        {
            grecaptcha.reset(widget1);
        }
    },
    invalidHandler : function(event, validator)
    { // display error alert on form submit
        if (typeof grecaptcha != 'undefined')
        {
            grecaptcha.reset(widget1);
        }
    }
});