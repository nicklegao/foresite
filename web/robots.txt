# Default exclusions
User-agent: *
Disallow: /

# For sitemaps.xml autodiscovery. Uncomment if you have one:
# Sitemap: http://example.com/sitemap.xml