<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%
boolean underlineSection = false;
boolean boldSection = false;
boolean uppercaseSection = false;
boolean customSectionFontColor = false;

List<String> fonts = new ArrayList<>();
fonts.add("Noto Sans CJK TC Regular");
fonts.add("Lato");
fonts.add("Lato Hairline");
fonts.add("Open Sans");
fonts.add("Lato");
fonts.add("Roboto");
fonts.add("Noto Serif");
String fontFamilySection = "Lato";
String fontSizeSection = "30";
String fontColorSection = "black";

boolean underlineH1 = false;
boolean boldH1 = false;
boolean uppercaseH1 = false;
boolean customH1FontColor = false;

String fontFamilyH1 = "Lato";
String fontSizeH1 = "26";
String fontColorH1 = "black";

boolean underlineH2 = false;
boolean boldH2 = false;
boolean uppercaseH2 = false;
boolean customH2FontColor = false;

String fontFamilyH2 = "Lato";
String fontSizeH2 = "24";
String fontColorH2 = "black";

boolean underlineH3 = false;
boolean boldH3 = false;
boolean uppercaseH3 = false;
boolean customH3FontColor = false;

String fontFamilyH3 = "Lato";
String fontSizeH3 = "22";
String fontColorH3 = "black";

boolean boldDL = false;
boolean uppercaseDL = false;
boolean customDLFontColor = false;

String fontFamilyDL = "Lato";
String fontSizeDL = "22";
String fontColorDL = "black";

String fontFamilyBody = "Lato";
String fontSizeBody = "13";
String fontColorBody = "black";
%>

<%--BODY CARDS--%>
<div class="collapse-card">
	<div class="collapse-card__heading">
		<div class="collapse-card__title">
			<span class="styleHeadingTitles">Body</span>
		</div>
	</div>
	<div class="collapse-card__body">
		<div class="row heading">
			<div class="col-sm-6 f-field text-left">Font</div>
			<div class="col-sm-3 f-field text-left">Size</div>
			<div class="col-sm-3 f-field text-left" tag="custom-color">Colour</div>
		</div>
		<div class="row">
			<div class="col-sm-6 f-field">
				<select class="form-control" name="fontFamilyBody">
					<% for (String font : fonts){
                            String selectedClause = font.equals(fontFamilyBody)?"selected":"";
                        %>
					<option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>"
						<%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
					<% } %>
				</select>
			</div>
			<div class="col-sm-3 f-field">
				<input type="text" id="font-size-body" class="form-control"
					name="fontSizeBody" 
					value="<%=ESAPI.encoder().encodeForHTMLAttribute(fontSizeBody) %>">
			</div>
			<div class="col-sm-3 f-field text-center" tag="custom-color">
				<input type="text" id="color-body" class="form-control color-spectrum"
					name="fontColorBody"
					value="<%=ESAPI.encoder().encodeForHTMLAttribute(fontColorBody) %>">
			</div>
		</div>
	</div>
</div>
<%--BODY CARDS END--%>

<%--SECTION HEADINGS CARDS--%>
<div class="collapse-card">
	<div class="collapse-card__heading">
		<div class="collapse-card__title">
			<span
				class="styleHeadingTitles">Section Heading</span>
			<% if (underlineSection) { %>
			<i class="fa fa-underline fa-fw pull-right"></i>
			<% } %>
			<% if (boldSection) { %>
			<i class="fa fa-bold fa-fw pull-right"></i>
			<% } %>
			<% if (uppercaseSection) { %>
			<i class="fa fa-text-height fa-fw pull-right"></i>
			<% } %>
			<% if (customSectionFontColor) { %>
			<i class="fa fa-paint-brush fa-fw pull-right"></i>
			<% } %>
		</div>
	</div>
	<div class="collapse-card__body">
		<div class="row heading">
			<div class="col-sm-6 f-field text-left">Font</div>
			<div class="col-sm-3 f-field text-left">Size</div>
			<div class="col-sm-3 f-field text-left font-section-color-title" tag="custom-color">Colour</div>
		</div>
		<div class="row">
			<div class="col-sm-6 f-field">
				<select class="form-control" name="fontFamilySection">
					<%
                            for (String font : fonts){
                                String selectedClause = font.equals(fontFamilySection)?"selected":"";
                        %>
					<option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>"
						<%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
					<% } %>
				</select>
			</div>
			<div class="col-sm-3 f-field">
				<input type="text" id="font-size-h1" class="form-control"
					name="fontSizeSection" 
					value="<%=ESAPI.encoder().encodeForHTMLAttribute(fontSizeSection) %>">
			</div>
			<div class="col-sm-3 f-field text-center fontColorSectionTitleSection" tag="custom-color">
				<input type="text" id="color-header" class="form-control color-spectrum"
					name="fontColorSectionTitle"
					value="<%=ESAPI.encoder().encodeForHTMLAttribute(fontColorSection) %>">
			</div>
		</div>
		<div style="padding-top: .5em;" class="section-group">
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox toggle-custom-color" type="checkbox" data-icon="fa-paint-brush"
					name="customSectionColor" 
					<%=customSectionFontColor?"checked":"" %> /> Custom Font Color
			</section>
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox" type="checkbox" data-icon="fa-text-height"
					name="uppercaseSectionTitle" 
					<%=uppercaseSection?"checked":"" %> /> Uppercase Headings
			</section>
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox" type="checkbox" data-icon="fa-underline"
					name="underlineSectionTitle" 
					<%=underlineSection?"checked":"" %> /> Underline Headings
			</section>
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox" type="checkbox" data-icon="fa-bold"
					name="boldSectionTitle"  <%=boldSection?"checked":"" %> />
				Bold Headings
			</section>
		</div>
	</div>
</div>
<%--SECTION HEADINGS CARDS END--%>


<%--H1 CARDS--%>
<div class="collapse-card">
	<div class="collapse-card__heading">
		<div class="collapse-card__title">
			<span
				class="styleHeadingTitles">Heading 1</span>
			<% if (underlineH1) { %>
			<i class="fa fa-underline fa-fw pull-right"></i>
			<% } %>
			<% if (boldH1) { %>
			<i class="fa fa-bold fa-fw pull-right"></i>
			<% } %>
			<% if (uppercaseH1) { %>
			<i class="fa fa-text-height fa-fw pull-right"></i>
			<% } %>
			<% if (customH1FontColor) { %>
			<i class="fa fa-paint-brush fa-fw pull-right"></i>
			<% } %>
		</div>
	</div>
	<div class="collapse-card__body">
		<div class="row heading">
			<div class="col-sm-6 f-field text-left">Font</div>
			<div class="col-sm-3 f-field text-left">Size</div>
			<div class="col-sm-3 f-field text-left font-section-color-title-h1" tag="custom-color">Colour</div>
		</div>
		<div class="row">
			<div class="col-sm-6 f-field">
				<select class="form-control" name="fontFamilyH1">
					<%
                            for (String font : fonts){
                                String selectedClause = font.equals(fontFamilyH1)?"selected":"";
                        %>
					<option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>"
						<%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
					<% } %>
				</select>
			</div>
			<div class="col-sm-3 f-field">
				<input type="text" id="font-size-h1" class="form-control"
					name="fontSizeH1" 
					value="<%=ESAPI.encoder().encodeForHTMLAttribute(fontSizeH1) %>">
			</div>
			<div class="col-sm-3 f-field text-center fontColorHeading1" tag="custom-color">
				<input type="text" id="color-header1" class="form-control color-spectrum"
					name="fontColorHeading1"
					value="<%=ESAPI.encoder().encodeForHTMLAttribute(fontColorH1) %>">
			</div>
		</div>
		<div style="padding-top: .5em;" class="section-group">
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox toggle-custom-color" type="checkbox" data-icon="fa-paint-brush"
					name="customHeading1Color" 
					<%=customH1FontColor?"checked":"" %> /> Custom Font Color
			</section>
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox" type="checkbox" data-icon="fa-text-height"
					name="uppercaseHeading1" 
					<%=uppercaseH1?"checked":"" %> /> Uppercase Headings
			</section>
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox" type="checkbox" data-icon="fa-underline"
					name="underlineHeading1"  
					<%=underlineH1?"checked":"" %> /> Underline Headings
			</section>
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox" type="checkbox" data-icon="fa-bold" name="boldHeading1"
					value="on" <%=boldH1?"checked":"" %> /> Bold Headings
			</section>
		</div>
	</div>
</div>
<%--H1 CARDS END--%>


<%--H2 CARDS--%>
<div class="collapse-card">
	<div class="collapse-card__heading">
		<div class="collapse-card__title">
			<span
				class="styleHeadingTitles">Heading 2</span>
			<% if (underlineH2) { %>
			<i class="fa fa-underline fa-fw pull-right"></i>
			<% } %>
			<% if (boldH2) { %>
			<i class="fa fa-bold fa-fw pull-right"></i>
			<% } %>
			<% if (uppercaseH2) { %>
			<i class="fa fa-text-height fa-fw pull-right"></i>
			<% } %>
			<% if (customH2FontColor) { %>
			<i class="fa fa-paint-brush fa-fw pull-right"></i>
			<% } %>
		</div>
	</div>
	<div class="collapse-card__body">
		<div class="row heading">
			<div class="col-sm-6 f-field text-left">Font</div>
			<div class="col-sm-3 f-field text-left">Size</div>
			<div class="col-sm-3 f-field text-left font-section-color-title-h2" tag="custom-color">Colour</div>
		</div>
		<div class="row">
			<div class="col-sm-6 f-field">
				<select class="form-control" name="fontFamilyH2">
					<%
                            for (String font : fonts){
                                String selectedClause = font.equals(fontFamilyH2)?"selected":"";
                        %>
					<option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>"
						<%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
					<% } %>
				</select>
			</div>
			<div class="col-sm-3 f-field">
				<input type="text" id="font-size-h2" class="form-control"
					name="fontSizeH2" 
					value="<%=ESAPI.encoder().encodeForHTMLAttribute(fontSizeH2) %>">
			</div>
			<div class="col-sm-3 f-field text-center fontColorHeading2" tag="custom-color">
				<input type="text" id="color-header2" class="form-control color-spectrum"
					name="fontColorHeading2"
					value="<%=ESAPI.encoder().encodeForHTMLAttribute(fontColorH2) %>">
			</div>
		</div>
		<div style="padding-top: .5em;" class="section-group">
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox toggle-custom-color" type="checkbox" data-icon="fa-paint-brush"
					name="customHeading2Color" 
					<%=customH2FontColor?"checked":"" %> /> Custom Font Color
			</section>
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox" type="checkbox" data-icon="fa-text-height"
					name="uppercaseHeading2"  
					<%=uppercaseH2?"checked":"" %> /> Uppercase Headings
			</section>
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox" type="checkbox" data-icon="fa-underline"
					name="underlineHeading2" 
					<%=underlineH2?"checked":"" %> /> Underline Headings
			</section>
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox" type="checkbox" data-icon="fa-bold" name="boldHeading2"
					value="on" <%=boldH2?"checked":"" %> /> Bold Headings
			</section>
		</div>
	</div>
</div>
<%--H2 CARDS END--%>


<%--H3 CARDS--%>
<div class="collapse-card">
	<div class="collapse-card__heading">
		<div class="collapse-card__title">
			<span
				class="styleHeadingTitles">Heading 3</span>
			<% if (underlineH3) { %>
			<i class="fa fa-underline fa-fw pull-right"></i>
			<% } %>
			<% if (boldH3) { %>
			<i class="fa fa-bold fa-fw pull-right"></i>
			<% } %>
			<% if (uppercaseH3) { %>
			<i class="fa fa-text-height fa-fw pull-right"></i>
			<% } %>
			<% if (customH3FontColor) { %>
			<i class="fa fa-paint-brush fa-fw pull-right"></i>
			<% } %>
		</div>
	</div>
	<div class="collapse-card__body">
		<div class="row heading">
			<div class="col-sm-6 f-field text-left">Font</div>
			<div class="col-sm-3 f-field text-left">Size</div>
			<div class="col-sm-3 f-field text-left font-section-color-title-h3" tag="custom-color">Colour</div>
		</div>
		<div class="row">
			<div class="col-sm-6 f-field">
				<select class="form-control" name="fontFamilyH3">
					<%
                            for (String font : fonts){
                                String selectedClause = font.equals(fontFamilyH3)?"selected":"";
                        %>
					<option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>"
						<%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
					<% } %>
				</select>
			</div>
			<div class="col-sm-3 f-field">
				<input type="text" id="font-size-h3" class="form-control"
					name="fontSizeH3" 
					value="<%=ESAPI.encoder().encodeForHTMLAttribute(fontSizeH3) %>">
			</div>
			<div class="col-sm-3 f-field text-center fontColorHeading3" tag="custom-color">
				<input type="text" id="color-header3" class="form-control color-spectrum"
					name="fontColorHeading3"
					value="<%=ESAPI.encoder().encodeForHTMLAttribute(fontColorH3) %>">
			</div>
		</div>
		<div style="padding-top: .5em;" class="section-group">
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox toggle-custom-color" type="checkbox" data-icon="fa-paint-brush"
					name="customHeading3Color" 
					<%=customH3FontColor?"checked":"" %> /> Custom Font Color
			</section>
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox" type="checkbox" data-icon="fa-text-height"
					name="uppercaseHeading3" 
					<%=uppercaseH3?"checked":"" %> /> Uppercase Headings
			</section>
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox" type="checkbox" data-icon="fa-underline"
					name="underlineHeading3" 
					<%=underlineH3?"checked":"" %> /> Underline Headings
			</section>
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox" type="checkbox" data-icon="fa-bold" name="boldHeading3"
					value="on" <%=boldH3?"checked":"" %> /> Bold Headings
			</section>
		</div>
	</div>
</div>
<%--H3 CARDS END--%>

<%--Itinerary Data Labels CARDS--%>
<div class="collapse-card">
	<div class="collapse-card__heading">
		<div class="collapse-card__title">
			<span
				class="styleHeadingTitles">Data Labels</span>
			<% if (boldDL) { %>
			<i class="fa fa-bold fa-fw pull-right"></i>
			<% } %>
			<% if (uppercaseDL) { %>
			<i class="fa fa-text-height fa-fw pull-right"></i>
			<% } %>
			<% if (customDLFontColor) { %>
			<i class="fa fa-paint-brush fa-fw pull-right"></i>
			<% } %>
		</div>
	</div>
	<div class="collapse-card__body">
		<div class="row heading">
			<div class="col-sm-6 f-field text-left">Font</div>
			<div class="col-sm-3 f-field text-left">Size</div>
			<div class="col-sm-3 f-field text-left font-section-color-title-dl" tag="custom-color">Colour</div>
		</div>
		<div class="row">
			<div class="col-sm-6 f-field">
				<select class="form-control" name="fontFamilyDL">
					<%
                            for (String font : fonts){
                                String selectedClause = font.equals(fontFamilyDL)?"selected":"";
                        %>
					<option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>"
						<%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
					<% } %>
				</select>
			</div>
			<div class="col-sm-3 f-field">
				<input type="text" id="font-size-dl" class="form-control"
					name="fontSizeDL" 
					value="<%=ESAPI.encoder().encodeForHTMLAttribute(fontSizeDL) %>">
			</div>
			<div class="col-sm-3 f-field text-center fontColorDL" tag="custom-color">
				<input type="text" id="color-header3" class="form-control color-spectrum"
					name="fontColorDL"
					value="<%=ESAPI.encoder().encodeForHTMLAttribute(fontColorDL) %>">
			</div>
		</div>
		<div style="padding-top: .5em;" class="section-group">
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox toggle-custom-color" type="checkbox" data-icon="fa-paint-brush"
					name="customDLColor" 
					<%=customDLFontColor?"checked":"" %> /> Custom Font Color
			</section>
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox" type="checkbox" data-icon="fa-text-height"
					name="uppercaseDL" 
					<%=uppercaseDL?"checked":"" %> /> Uppercase Data Labels
			</section>
			<section style="padding-right: 0px; padding-left: 30px">
				<input class="table-checkbox" type="checkbox" data-icon="fa-bold" name="boldDL"
					value="on" <%=boldDL?"checked":"" %> /> Bold Data Labels
			</section>
		</div>
	</div>
</div>
<%--Itinerary Data Labels CARDS END--%>

<script type="text/javascript" src="/application/assets/new/js/editStyle.js"></script>