<%@page import="java.util.Hashtable"%>
<%@page import="java.util.ArrayList"%>
<%@page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="java.util.LinkedHashMap" %>
<%
	String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
	TUserRolesDataAccess urda = new TUserRolesDataAccess();
	UserPermissions up = urda.getPermissions(userId);
	Boolean viewAnalytics = true;
	Boolean manageSubscriptions = up.hasManageUsersPermission();
	
    ArrayList<Hashtable<String,String>> links = new ArrayList<Hashtable<String,String>>();
    
    Hashtable forecast = new Hashtable<String,String>();
    forecast.put("label","Sales Forecast");
    forecast.put("link","/salesForecast");
    forecast.put("icon", "fa-tachometer");
    forecast.put("permission","true");
    links.add(forecast);
    
    Hashtable dashboard = new Hashtable<String,String>();
    dashboard.put("label","Dashboard");
    dashboard.put("link","/dashboard");
    dashboard.put("icon", "fa-archive");
    dashboard.put("permission","true");
    links.add(dashboard);
    
    Hashtable analytics = new Hashtable<String,String>();
    analytics.put("label","Analytics");
    analytics.put("link","/analytics");
    analytics.put("icon", "fa-bar-chart");
    analytics.put("permission",viewAnalytics.toString());
    links.add(analytics);

    Hashtable extensions = new Hashtable<String,String>();
    extensions.put("label","Extensions");
    extensions.put("link","/plugins");
    extensions.put("icon", "fa-cart-plus");
    extensions.put("permission", manageSubscriptions.toString());
    links.add(extensions);

    String uri = request.getRequestURI();
%>
<ul class="nav navbar-nav" style="height: 49px">
    <%
        for (Hashtable<String,String> link : links)
        {
            String liClass = "";
            if (uri.equals("/application"+link.get("link")+".jsp"))
            	liClass = "active";
            if("true".equals(link.get("permission"))){
		    %>
		    <li class="<%=liClass%>"><a href="<%=link.get("link")%>"
		                                style="height: 49px"><i class="fa <%=link.get("icon")%>"></i><span>
						<%=link.get("label")%></span></a></li>
		    <%
            }
        }
    %>
</ul>
