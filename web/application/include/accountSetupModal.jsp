
<div class="modal-content" style="border-radius: 4px;">
    <div class="card-header" style="padding: 12px;">
        <div class="row align-items-center text-center">
            <div class="col" style="">
                <div class="form-bootstrapWizard">
                    <ul class="bootstrapWizard form-wizard three-steps">

                        <li class="active" data-target="#agent-link-tab">
                            <a href="#agent-link-tab" data-toggle="tab"> <span class="step">1</span> <span class="title">Agent Links</span> </a>
                        </li>

                        <li class="" data-target="#agent-info-tab" data-type="agent-info">
                            <a href="#agent-info-tab" data-type="agent-link" data-toggle="tab"> <span class="step">2</span> <span class="title">Inform Agent</span> </a>
                        </li>

                        <li class="" data-target="#zerorisk-tab" data-type="zerorisk">
                            <a href="#zerorisk-tab" data-type="agent-link" data-toggle="tab"> <span class="step">3</span> <span class="title">ZeroRisk</span> </a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
<%--            <a class="nav-link registration-wizard-menu-button ripple active fuse-ripple-ready active" data-type="agent-link" data-link="#agent-link-tab" href="#">--%>
<%--                <i class="fas fa-random mb-2" style="margin: auto;"></i>--%>
<%--                <span>Agent Link</span>--%>
<%--            </a>--%>
<%--            <div class="col-auto" style="padding: 0;">--%>
<%--                <i class="registration-wizard-menu-divider fal fa-ellipsis-h-alt"></i>--%>
<%--            </div>--%>
<%--            <div class="col-auto" style="width: 131px;">--%>
<%--                <a class="nav-link registration-wizard-menu-button ripple fuse-ripple-ready" data-type="agent-info" data-link="#agent-info-tab" href="#">--%>
<%--                    <i class="fas fa-envelope-open mb-2" style="margin: auto;"></i>--%>
<%--                    <span>Inform Agent</span>--%>
<%--                </a>--%>
<%--            </div>--%>
        </div>
    </div>
    <div class="modal-card card-body">
        <div class="tab-content"  id="agent-registion-modal">
            <div class="tab-pane fade show active p-3" id="agent-link-tab" role="tabpanel">
                <div class="row align-items-center mb-3">
                    <div class="col">
                        <h4>Link your Sabre Travel Agent</h4>
                    </div>
                    <div class="col-auto">
                        <button type="button" id="addNewAgent" class="btn btn-default-blue">Add New</button>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade p-3" id="agent-info-tab" role="tabpanel">
                <div class="row align-items-center mb-3">
                    <div class="col">
                        <h4>Information to share with your Agent</h4>
                    </div>
                    <div class="col-auto">
                        <button type="button" id="printAgentInfo" class="btn btn-default-blue">Email</button>
                    </div>
                </div>
                <div class="row align-items-center mb-3">
                    <div class="col">
                        <div class="agent-email-container" style="width: 100%; max-height: 300px; overflow: auto;background-color: rgb(239, 239, 239);border-radius: 4px;padding: 10px;border: 1px solid lightgray;">
                            <span>For the next phase of the implementation, please find below instructions.  This instructions will need to be given to your travel management company (TMC).
                                <br><br><u>SABRE GDS Opening Global Security</u>

                                <br><br>
<u style="color: red;">PLEASE NOTE: For questions related to Global Security please contact your Sabre Help Desk directly for assistance.</u><br><br>

                                Before any PNR's can be queued to TravelDocs, Global Security must be opened by your TMC utilising DK Number. DK number is a number or numbers that your TMC has assigned internally to your company, it could also be known as a customer number.

                                You TMC will need Keyword â€œAccessâ€ in their agency profile and duty code 9 to be eligible to open access. In most cases a supervisor or manager will need to perform the necessary functions to open access. If this cannot be done by your agency then the Sabre Help Desk will have to open global security. You will need to provide the DK numbers to Sabre so they can do the format correctly. Any questions regarding opening global security please contact the Sabre Help Desk for assistance.

                                Opening access will allow TravelDocs read-only access to the PNRâ€™s

                                <br><br><b>Your Customer Number(s)</b><br>

                                You need to add the customer numbers recorded on PNR's that identify your company.  These customer numbers will need to be added to this set-up wizard so that TravelDocs can identify your travel bookings.

                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade p-3" id="zerorisk-tab" role="tabpanel">
                <div class="row align-items-center mb-3">
                    <div class="col">
                        <h4>Link your ZeroRisk Account to TravelDocs</h4>
                    </div>
                </div>
                <div class="row custom-zerorisk-api-container" style="">
                    <div class="col">
                        <form>
                            <div class="form-group">
                                <label for="queueZRAPIKey">ZeroRisk API Key</label>
                                <input class="form-control" id="queueZRAPIKey" aria-describedby="queueZRAPIKey" >
                            </div>
                            <div class="form-group">
                                <label for="queueZRAPISecret">ZeroRisk API Secret</label>
                                <input type="password" class="form-control" id="queueZRAPISecret" aria-describedby="queueZRAPISecret">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="row align-items-center">
            <div class="col" >
                <button type="button" id="previousRegisterButton" style="display: none;" class="btn btn-default">Previous</button>
            </div>
            <div class="col text-right">
                <button type="button" id="nextRegisterButton" style=""  class="btn btn-default">Next</button>
            </div>
            <div class="col text-right" id="submitRegisterButton-container" style="display: none;">
                <button type="button" id="submitRegisterButton" style=""  class="btn btn-default">Submit</button>
            </div>
        </div>
    </div>
</div>