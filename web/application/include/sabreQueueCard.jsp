<div class="collapse-card card">
    <div class="collapse-card__heading" style="border-bottom: 1px solid lightgray;">
        <div class="collapse-card__title">
            <i class="fas fa-random fa-2x fa-fw" style="padding-top: 3px;"></i>
            <h6 style="padding-left: 39px;" class="queueNameLabel">New Agent Link</h6>
        </div>
    </div>
    <div class="collapse-card__body">
        <div class="row">
            <div class="col">
                <div class="row">
                    <div class="col">
                        <span class="h5">Agent Information</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <form>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="queueName">Agent Name</label>
                                        <input class="form-control" id="queueName" aria-describedby="queueName" value="">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="queueName">Agent Phone Number</label>
                                        <input class="form-control" id="queuePhoneNumber" aria-describedby="queuePhoneNumber" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="agentCode">Agent Code</label>
                                        <input class="form-control" id="agentCode" aria-describedby="agentCode" value="">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="customerCode">Customer Code</label>
                                        <input class="form-control" id="customerCode" aria-describedby="customerCode" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <select name="queueHomeCountry" multiple id="queueHomeCountry">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col">
                <div class="row">
                    <div class="col">
                        <span class="h5">TravelDocs Template</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Select a template to use for this queue</label>
                            <select class="form-control traveldocTemplateSelector">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="row">
                    <div class="col">
                        <span class="h5">Queue Settings</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Auto-Send Queue</label>
                            <input type="checkbox" class="autosendQueueSwitch" />
                            <small id="emailHelp" class="form-text text-muted">This will automatically send your TravelDoc as the itinerary is recieved from Sabre.</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="invalidCountrySelectorContainer" style="display:none;">
            <div class="col">
                <div class="form-group">
                    <select name="invalidCountrySelector" multiple id="invalidCountrySelector">
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <hr style="margin: 20px 0;">
            </div>
        </div>
        <div class="row">
            <div class="col">
                <button type="button" id="deleteQueueButton" class="btn btn-danger pull-left" style="margin-right: 20px;">
                    <span class="deleteQueueButton">Delete</span>
                </button>
            </div>
            <div class="col-auto">
                <button type="button" id="saveQueueButton" class="btn btn-secondary pull-left" style="margin-right: 20px;">
                    <span class="saveQueueButton">Save</span>
                </button>
            </div>
        </div>
    </div>
</div>