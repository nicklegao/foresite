<%@page import="com.stripe.model.Customer"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.StripeService"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.corporateinteractive.qcloud.subscription.SubscriptionPlan"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ page import="au.net.webdirector.common.datalayer.client.ModuleAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.base.db.entity.MultiOptions" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.EmailTemplateDataAccess" %>
<%@ page import="au.com.ci.sbe.util.UrlUtils" %>
<%
UserAccountDataAccess uada = new UserAccountDataAccess();
String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
CategoryData companyData = uada.getUserCompanyForUserId(userId);
String companyName = uada.getUserCompanyNameForUserId(userId);
%>
<div id="account-settings">
    <style>
    input.form-control {
      padding: 0 10px;
    }
    select.form-control {
      padding: 0 6px;
    }
    .smart-form *, .smart-form *:after, .smart-form *:before {
      box-sizing: border-box;
    }
    .smart-form .col-block {
        min-height: 1px;
        padding-right: 15px;
        padding-left: 15px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        clear: both;
    }
    
    #card-element-placeholder .card-number{
      color: #32325d;
      line-height: 24px;
      font-size: 16px;
      height: 24px;
    }
    p.note{
      font-size: 11px;
    }    
    img.stripe-seal{
      width: 130px;
      display: block;
      float: left;
      margin-right: 4px;
    }
    </style>
	<form class="form-email-template-settings" method="POST">
	<div class="modal-header">
		<h2 class="modal-title">Email Settings</h2>
	</div>
	<div class="modal-body smart-form">
		<div class="form-horizontal">
		
		<div class="company-details">
			<div class="row">
				<%
					EmailTemplateDataAccess etda = new EmailTemplateDataAccess();
					String content = etda.readContentWithUserId(companyData.getId(), "Itinerary Ready");
					if(StringUtils.isBlank(content)) {
                        content = etda.readDefaultTemplate("Itinerary Ready").get("content");
                    }
				%>
				<section class="col col-10 form-group">
					<label for="subject1" class="">Email Subject</label>
					<input value="<%=ESAPI.encoder().encodeForHTMLAttribute(etda.readSubjectWithUserId(companyData.getId(), "Itinerary Ready"))%>" name="subject1" type="text" class="form-control" id="subject1" placeholder="Write your subject here..." aria-required="true" aria-invalid="false">
				</section>
				<section class="col col-12  form-group">
					<label for="email-template-ready">TravelDoc Itinerary Send Email Template</label>
					<div class="panel-froala" id="email-template-ready"><%= content %></div>
				</section>
				<section class="col col-12  form-group">
					<button type="button" class="btn btn-warning loadDefaultEmailTemplate" style="padding: 6px 12px;" data-name="Itinerary Ready" data-editor="#email-template-ready">
						<i class="fa fa-sync" aria-hidden="true"></i> Load Default
					</button>
				</section>
			</div>
<%--			<div class="row">--%>
<%--				<%--%>
<%--					String content2 = etda.readContentWithUserId(userId, "Itinerary New Revision Ready");--%>
<%--                    if(StringUtils.isBlank(content)) {--%>
<%--                        content2 = etda.readDefaultTemplate("Itinerary New Revision Ready").get("content");--%>
<%--                    }--%>
<%--				%>--%>
<%--				<section class="col col-10 form-group">--%>
<%--					<label for="subject2" class="">Enter New Revision Ready Email Subject</label>--%>
<%--					<input value="<%=ESAPI.encoder().encodeForHTMLAttribute(etda.readSubjectWithUserId(userId, "Itinerary New Revision Ready"))%>" name="subject2" type="text" class="form-control" id="subject2" placeholder="Write subject here..." aria-required="true" aria-invalid="false">--%>
<%--				</section>--%>
<%--				<section class="col col-12 form-group">--%>
<%--					<label for="email-template-revision">New Revision Ready Template</label>--%>
<%--					<div class="panel-froala" id="email-template-revision"><%= content2 %></div>--%>
<%--				</section>--%>
<%--				<section class="col col-12 form-group">--%>
<%--					<button type="button" class="btn btn-warning loadDefaultEmailTemplate" style="padding: 6px 12px;" data-name="Itinerary New Revision Ready" data-editor="#email-template-revision">--%>
<%--						<i class="fa fa-sync" aria-hidden="true"></i> Load Default--%>
<%--					</button>--%>
<%--				</section>--%>
<%--			</div>--%>
		</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" id="closeForm" class="btn btn-danger"
			data-dismiss="modal">
			<i class="fa fa-times"></i> Close
		</button>
		<button type="submit" class="btn btn-success">
        	<i class="fa fa-save"></i> Save
      	</button>
	</div>
	</form>
</div>
<script>
	function initFroalaEditor()
	{

		// var uninitialisedFroalaEditors = $(".panel-froala");

		new FroalaEditor('.panel-froala', {
			toolbarButtons : [ "bold", "italic", "underline", "strikeThrough", "subscript", "superscript", "|", "fontSize", "color", "paragraphStyle", "fullscreen", "|", "specialCharacters", "insertHR", "clearFormatting", "|", "paragraphFormat", "align", "formatOL", "formatUL", "outdent", "indent", "quote", "insertLink", "insertTable", "|", "spellChecker", "|", "undo", "redo", "dataItems" ],
			enter : FroalaEditor.ENTER_P,
			key : '7F4D3F3H3cA5A4B3A1E4B2G2E3D1C6vDIG1QCYRWa1GPId1f1I1=='
		});
	}

	initFroalaEditor();
</script>
<script>
	$('.form-email-template-settings').validate({
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error, element)
		{
			// error.insertAfter(element);

		},
		ignore : '',
		rules : {
		},
		highlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-error');
		},
		success : function(label, element)
		{
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		submitHandler : function(form)
		{
			var $form = $(form);
			var data = $($form).serializeObject();
			var editor1 = new FroalaEditor('#email-template-ready', {}, function(){
				// data["readyEmailTemplate"] = editor1.html.get();
			});
			// var editor2 = new FroalaEditor('#email-template-revision', {}, function(){
			// 	// data["revisionEmailTemplate"] = editor2.html.get();
			// })
			data["readyEmailTemplate"] = editor1.html.get();
			// data["revisionEmailTemplate"] = editor2.html.get();
			// $().froalaEditor('html.get').replace(/\<br>/g, '<br/>');
			// data["revisionEmailTemplate"] = $('#email-template-revision').froalaEditor('html.get').replace(/\<br>/g, '<br/>');
			startLoadingForeground();
			$.ajax({
				url : '/api/updateEmailTemplates',
				type : 'POST',
				data : data,
				success : function(data)
				{
					stopLoadingForeground();
					if (data)
					{

					}
					else
					{
						swal({
							title : "Error",
							text : "It wasn't possible to change the settings. Contact your TravelDocs administrator and make sure you have permission to manage settings.",
							type : "error"
						});
					}
				},
				error : function(data)
				{
					stopLoadingForeground();

					alert("Unable to update your email template settings. Please try again later.");
				}
			});
		}
	});

	$('.loadDefaultEmailTemplate').on('click', function(){
		var name = $(this).attr('data-name');
		var editor = $(this).attr('data-editor');
		if(!confirm('You will lose your changes in this template. Are you sure?')){
			return;
		}
		$.ajax({
			url : '/api/readDefaultEmailTemplate',
			type : 'post',
			data : {
				name : name
			}
		}).done(function(data)
		{
			if(data.content){
				var froala = new FroalaEditor(editor, {}, function(){

				});

				froala.html.set(data.content);
				$('input#subject1').val(data.subject);
			}else{
				alert('Something went wrong, please try later!');
			}
		});

	});
</script>