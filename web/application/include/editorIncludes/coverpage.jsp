<style type="text/css" class="proposalCoverPageStyles">

    .coverpage-main-image {
        background: <:coverpageBGImage>;
    }
    .proposalTitle {
        background-color: <:coverpageTitleBG>;
        color:<:coverpageTitleColor>;
    }
    .coverData {
        background-color: <:coverpageTitleBG>;
    }
    .proposalInfo {
        background-color: <:coverpageInfoBG>;
        color: <:coverpageInfoColor>;
    }
    .authorInfo {
        background-color: <:coverpageAuthorBG>;
        color: <:coverpageAuthorColor>;
    }

    .cover.content-box-hidden .proposalTitle, .cover.content-box-hidden .coverData, .cover.content-box-hidden .proposalInfo, .cover.content-box-hidden .authorInfo {
        background-color: transparent;
    }
    
    #itineraryContent .segment .segment-heading, .content-holder[data-type=block-itinerary] .segment .segment-heading {
    	background-color: <:coverpageTitleBG>;
    	color: <:coverpageTitleColor>;
    }
    #itineraryContent .segment .segment-detail .segment-table .table-heading, body .iti-label, body .iti-segment .row .col-2, body .iti-segment .row .col-3, .iti-segment .segment-outer-row > div:first-child, #itineraryContent .desc .booking-desc .desc-text .desc-lbl {
    	color: <:coverpageTitleBG>;
    }

    .get-itinerary-directions-button {
        color: <:coverpageTitleBG> !important;
    }

    .zerorisk-container .accordion .card .card-header {
        background-color: <:coverpageTitleBG>;
        color: <:coverpageTitleColor>;
    }

    .zerorisk-container .accordion .card .card-header button {
        color: <:coverpageTitleColor>;
    }

    .zerorisk-container .accordion .card hr {
        background-color: <:coverpageTitleBG>;
    }

    .zerorisk-container .accordion .card .alert-section-title,
    .zerorisk-container .accordion .card .travel-section-title,
    .zerorisk-container .accordion .card .asset-section-title,
    .zerorisk-container .accordion .card .travel-rating{
        color: <:coverpageTitleBG>;
    }

</style>