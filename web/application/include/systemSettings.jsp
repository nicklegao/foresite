<%@page import="com.stripe.model.Customer"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.StripeService"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.corporateinteractive.qcloud.subscription.SubscriptionPlan"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ page import="au.net.webdirector.common.datalayer.client.ModuleAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.base.db.entity.MultiOptions" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.EmailTemplateDataAccess" %>
<%@ page import="au.com.ci.sbe.util.UrlUtils" %>
<%
    UserAccountDataAccess uada = new UserAccountDataAccess();
    String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
    CategoryData company = uada.getUserCompanyForUserId(userId);
    String companyName = uada.getUserCompanyNameForUserId(userId);
%>
<div id="account-settings">
    <style>
        input.form-control {
            padding: 0 10px;
        }
        select.form-control {
            padding: 0 6px;
        }
        .smart-form *, .smart-form *:after, .smart-form *:before {
            box-sizing: border-box;
        }
        .smart-form .col-block {
            min-height: 1px;
            padding-right: 15px;
            padding-left: 15px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            clear: both;
        }

        #card-element-placeholder .card-number{
            color: #32325d;
            line-height: 24px;
            font-size: 16px;
            height: 24px;
        }
        p.note{
            font-size: 11px;
        }
        img.stripe-seal{
            width: 130px;
            display: block;
            float: left;
            margin-right: 4px;
        }
    </style>
    <form class="form-email-template-settings" method="POST">
        <div class="modal-header">
            <h2 class="modal-title">System Settings</h2>
        </div>
        <div class="modal-body smart-form">
            <div class="form-horizontal">

                <div class="company-details">
                    <div class="row">
                        <div class="col">
                            <h4>Itinerary Expiry</h4>
                            <span>Set the amount of days past the 'End Travel' date you would like to store and itinerary before TravelDocs removes it from the system.</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <section class="col col-10 form-group">
                                <label for="nonReturnExpiry" class="">Non-Return Expiry (Days)</label>
                                <input value="<%=company.getInt(UserAccountDataAccess.C1_DELETE_ITINERARY_SINGLE)%>" name="nonReturnExpiry" type="number" class="form-control" id="nonReturnExpiry" min="1" aria-required="true" aria-invalid="false">
                            </section>
                        </div>
                        <div class="col">
                            <section class="col col-10 form-group">
                                <label for="returnExpiry" class="">Return Trip Expiry (Days)</label>
                                <input value="<%=company.getInt(UserAccountDataAccess.C1_DELETE_ITINERARY_RETURN)%>" name="returnExpiry" type="number" class="form-control" id="returnExpiry" min="1" aria-required="true" aria-invalid="false">
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="closeForm" class="btn btn-danger"
                    data-dismiss="modal">
                <i class="fa fa-times"></i> Close
            </button>
            <button type="submit" class="btn btn-success">
                <i class="fa fa-save"></i> Save
            </button>
        </div>
    </form>
</div>
<script>
    $('.form-email-template-settings').validate({
        errorElement : 'span',
        errorClass : 'help-block',
        errorPlacement : function(error, element)
        {
            // error.insertAfter(element);

        },
        ignore : '',
        rules : {
        },
        highlight : function(element)
        {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight : function(element)
        {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success : function(label, element)
        {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        submitHandler : function(form)
        {
            var $form = $(form);
            var data = $($form).serializeObject();


            startLoadingForeground();
            $.ajax({
                url : '/api/updateSystemSettings',
                data : data,
                success : function(data)
                {
                    stopLoadingForeground();
                    if (data)
                    {

                    }
                    else
                    {
                        swal({
                            title : "Error",
                            text : "It wasn't possible to change the settings. Contact your TravelDocs administrator and make sure you have permission to manage settings.",
                            type : "error"
                        });
                    }
                },
                error : function(data)
                {
                    stopLoadingForeground();

                    alert("Unable to update your email template settings. Please try again later.");
                }
            });
        }
    });

    $('.loadDefaultEmailTemplate').on('click', function(){
        var name = $(this).attr('data-name');
        var editor = $(this).attr('data-editor');
        if(!confirm('You will lose your changes in this template. Are you sure?')){
            return;
        }
        $.ajax({
            url : '/api/readDefaultEmailTemplate',
            type : 'post',
            data : {
                name : name
            }
        }).done(function(data)
        {
            if(data.content){
                var froala = new FroalaEditor(editor, {}, function(){

                });

                froala.html.set(data.content);
                $('input#subject1').val(data.subject);
            }else{
                alert('Something went wrong, please try later!');
            }
        });

    });
</script>