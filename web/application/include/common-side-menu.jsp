<%@page import="org.apache.commons.lang.StringUtils"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%@ page import="au.com.ci.system.classfinder.StringUtil" %>
<%@ page import="au.net.webdirector.common.datalayer.client.ElementData" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions" %>
<%

    String url = request.getRequestURI() + request.getQueryString();


    String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
    TUserRolesDataAccess urda = new TUserRolesDataAccess();
    UserAccountDataAccess uada = new UserAccountDataAccess();
    CategoryData company = uada.getUserCompanyForUserId(userId);
    String companyId = company.getId();
    UserPermissions up = urda.getPermissions(userId);
    %>

<script src="/webdirector/free/csrf-token/init"></script>
<aside id="aside" class="aside aside-left" data-fuse-bar="aside" data-fuse-bar-media-step="md" data-fuse-bar-position="left">
    <div class="aside-content-wrapper">

        <div class="aside-content bg-primary-700 text-auto">

            <div class="aside-toolbar">

                <div class="logo">
                    <span class="logo-icon">Td</span>
                </div>

            </div>

            <ul class="nav flex-column custom-scrollbar" id="sidenav" data-children=".nav-item">
                <li class="nav-item">
                    <a class="nav-link ripple <%= url.toLowerCase().contains("/dashboard") ? "active" : ""%>" href="/dashboard" data-url="index.html">

                        <i class="fas fa-th-large"></i>

                        <span>Dashboard</span>
                    </a>
                </li>
                <%-- <%
                    if (company != null && !StringUtil.isNullOrEmpty(company.getString(UserAccountDataAccess.C_ZERO_RISK_APIKEY))) {
                %>
                <li class="nav-item <%= request.getParameter("travelGuide") %>">
                    <a class="nav-link ripple  <%= url.toLowerCase().contains("/guide") ? "active" : ""%>" href="/travel-guide" data-url="index.html" style="width: 80px;">
                        <i class="fal fa-suitcase-rolling"></i>
                        <span style="">Travel Safety</span>
                        <span style="margin-top: -9px;">Guides</span>
                    </a>
                </li>
                <%
                    }
                    if (up.hasManageTemplatesPermission()) {
                %>
                <li class="nav-item <%= request.getParameter("libraryType") %>" >
                    <a class="nav-link ripple <%= url.toLowerCase().contains("/file-manager") && StringUtils.equals("template", request.getParameter("libraryType")) ? "active" : ""%>" href="/file-manager?libraryType=template" data-url="index.html">

                        <i class="fal fa-passport"></i>

                        <span>Template</span>
                    </a>
                </li>

                <%
                    }
                %> --%>
                <%    
                	if (up.hasManageProjectsPermission()) {
	            %>
	            <li class="nav-item <%= request.getParameter("libraryType") %>" >
	                <a class="nav-link ripple <%= url.toLowerCase().contains("/file-manager") && StringUtils.equals("project", request.getParameter("libraryType")) ? "active" : ""%>" href="/file-manager?libraryType=project" data-url="index.html">
	                    <i class="fal fa-passport"></i>
	                    <span>Project</span>
	                </a>
	            </li>
	            <%
	                }
                    if (up.hasManageUsersPermission()) {
                %>
                <li class="nav-item">
                    <a class="nav-link ripple <%= url.toLowerCase().contains("/users") ? "active" : ""%>" href="/users" data-url="users.html">

                        <i class="fal fa-user" style=""></i>

                        <span>Users</span>
                    </a>
                </li>

                <%
                    }
                %>
                <%
                    if (up.hasManageSettingsPermission()) {
                %>
                <li class="nav-item">
                    <a class="nav-link ripple <%= url.toLowerCase().contains("/sabre") ? "active" : ""%>" href="javascript:;" data-url="sabre.html">

                        <i class="fal fa-cogs" style=""></i>

                        <span>Settings</span>
                    </a>
                </li>
                <%
                    }
                %>
            </ul>
        </div>
    </div>
</aside>