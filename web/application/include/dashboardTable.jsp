<table class="table table-striped">
    <thead>
    <:dashboardMainHeader>
    </thead>
    <tbody>
    <:dashboardMainBody>
    </tbody>
</table>