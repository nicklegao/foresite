<%@ page import="au.net.webdirector.common.datalayer.client.ElementData" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>

<%
    UserAccountDataAccess uada = new UserAccountDataAccess();


    String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);

    ElementData userData = uada.getUserElementWithUserID(userId);
%>
<%--CONTENT AREA--%>
<div class="widget-header px-4 py-6 row no-gutters align-items-center justify-content-between">

    <div class="col">
        <span class="h5">Security</span>
    </div>

    <div>
        <%--<button type="button" id="exportUsers" class="btn btn-warning pull-left">--%>
            <%--<i class="fa fa-arrow-down"></i> <span class="exportUsersText">Export Users</span>--%>
        <%--</button>--%>
    </div>

</div>
<div class="tab-content"  id="security-management-modal">
    <div class="tab-pane fade show active p-3" id="heading-tab-pane" role="tabpanel" aria-labelledby="heading-tab">
        <div class="col-12 p-3 card">
            <h2>Account Security</h2>
            <p>Traveldocs provides two factor authentication as a method of verification to ensure no unauthorised parties are accessing your account</p>
			<div id="email-tfa" style="<%= UserAccountDataAccess.isUserUsingMobileTFA(userData) ? "display:none;" : "" %>">
            <p><b>Email Two Factor Authentication</b></p>
            <p>An email with a code will be sent the email address associated with your account</p>
            <p><button id="enableMobileVerification" class="btn btn-default-blue">Switch to mobile verification</button></p>
			</div>
			<div id="mobile-tfa" style="<%= UserAccountDataAccess.isUserUsingMobileTFA(userData) ? "" : "display:none;" %>">
            <p><b>Mobile Two Factor Authentication</b></p>
            <p>A code will be generated using an application</p>
            <p><button id="enableEmailVerification" class="btn btn-default-blue">Switch to email verification</button></p>
			</div>
        </div>
    </div>
</div>

<script>
    $('#enableEmailVerification').on('click', function(){
        updateEmailSecurity();
    })

    $('#enableMobileVerification').on('click', function(){
        enableMobileSecurity();
    });

    function updateEmailSecurity()
    {
        var params = {};

        $.ajax({
            url: '/api/generateEmailTwoFactor',
            type: 'post',
            success: function(response){
                console.log(response);
                if(response.success == true) {
                    $('body').modalmanager('loading');

                    var $modal = $('#auth-modal');

                    $modal.load('/application/dialog/emailTwoFactorSetupModal.jsp', params, function()
                    {
                        $modal.modal({
                            width : '760px',
                            backdrop : 'static',
                            keyboard : false
                        });
                    });
                } else {
                    swal({
                        title: 'Error',
                        type: 'error',
                        text: response.message
                    })
                }

            }
        })
    }

    function enableMobileSecurity()
    {
        var params = {};

        $.ajax({
            url: '/api/generateMobileTwoFactor',
            type: 'post',
            success: function(response){
                console.log(response);
                if(response.success == true) {
                    $('body').modalmanager('loading');

                    var $modal = $('#auth-modal');

                    $modal.load('/application/dialog/mobileTwoFactorSetupModal.jsp', params, function()
                    {
                        $modal.modal({
                            width : '760px',
                            backdrop : 'static',
                            keyboard : false
                        });
                    });
                } else {
                    swal({
                        title: 'Error',
                        type: 'error',
                        text: response.message
                    })
                }

            }
        })
    }
</script>
