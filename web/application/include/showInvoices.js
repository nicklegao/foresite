$(document).ready(function()
{
	var domHead = "<'navbar' <'navbar-form navbar-left'<'form-group pick-date'>> <'navbar-form navbar-right'<'form-group show-page' l>> >";
	var domBody = "t";
	var domFooter = "<'navbar' <'navbar-form navbar-left' i> <'navbar-form navbar-right'<'form-group' p>> <'navbar-form navbar-right' <'#actionsHolder'>> >";

	var dateRangePicker = undefined;// Setup later!
	var dashboardTable = $('#invoiceTable').DataTable({
		"aaSorting" : [ [ 0, "asc" ] ],
		"autoWidth" : true,
		"processing" : true,
		"serverSide" : true,
		"ajax" : {
			"url" : "/api/invoices/data",
			"method" : "POST",
			"data" : function(d)
			{
				if (dateRangePicker != undefined)
				{
					var picker = dateRangePicker.data('daterangepicker');
					d.startDate = picker.startDate.format('YYYY-MM-DD');
					d.endDate = picker.endDate.format('YYYY-MM-DD');
				}
				else
				{
					d.startDate = moment().subtract(3, 'month').format('YYYY-MM-DD');
					d.endDate = moment().format('YYYY-MM-DD');
				}
			}
		},
		"aoColumns" : [ {
			title : '#',
			data : 'attr_headline',
			render : function(data, type, row)
			{
				return 'INV-' + row.attr_headline;
			}
		}, {
			title : 'Date',
			data : 'attrdate_date',
			render : function(data, type, row)
			{
				if (type == 'display')
				{
					return formatDate(row.attrdate_date);
				}
				else
				{
					return row.attrdate_date;
				}
			}
		}, {
			title : 'Amount',
			data : 'attrcurrency_amount',
			render : function(data, type, row)
			{
				if (type == 'display')
				{
					return formatMoney(row.attr_currency, row.attrcurrency_amount)
				}
				else
				{
					return row.attrcurrency_amount;
				}
			}
		}, {
			title : 'Paid',
			data : 'attrcheck_paid',
			render : function(data, type, row)
			{
				if (type == 'display')
				{
					return row.attrcheck_paid == 1 ? 'Yes' : 'No';
				}
				else
				{
					return row.attrcheck_paid;
				}
			}
		}, {
			title : 'Invoice PDF',
			data : 'element_id',
			width : '200',
			render : function(data, type, row)
			{
				if (type == 'display')
				{
					var btn = $('<a class="btn btn-xs btn-success btn-download"><i class="fa fa-download"></i> Download</a> ').attr('href', '/api/invoices/download?inv=' + row.element_id).attr('target', '_blank');
					return $("<div>").append(btn).html();
				}
				else
				{
					return row.element_id;
				}
			}
		} ],
		"oLanguage" : {
			"sLengthMenu" : '<span>Show </span> <div class="per-page short-select-option"> _MENU_ </div> <span> invoices per page</span>',
			"oPaginate" : {
				"sPrevious" : '',
				"sNext" : ''
			},
			"sInfo" : 'Showing _START_ to _END_ of _TOTAL_ invoices',
			"sInfoEmpty" : "No invoices found",
		},
		// "aaSorting" : [ [ 15, 'desc' ] ],
		"aLengthMenu" : [ [ 10, 20, 50, 100 ], [ 10, 20, 50, 100 ] ],
		// // set the initial value
		"iDisplayLength" : 10,
		"sDom" : domHead + domBody + domFooter,
	}).on('preXhr.dt', function(e, settings, json, xhr)
	{
		$(".dashboardLoadingIndicator").addClass('fa-spin fa-cog').removeClass('fa-table');
	}).on('xhr.dt', function(e, settings, json, xhr)
	{
	}).on('draw.dt', function(e, settings)
	{
		$(".dashboardLoadingIndicator").removeClass('fa-spin fa-cog').addClass('fa-table');
	});


	initDatePicker();

	function initDatePicker()
	{
		/*
		 * Configure the dateRangePicker
		 */
		var start = moment().subtract(3, 'month');
		var end = moment();
		var defaultFormat = start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY');
		$(".pick-date").addClass("greentech-panel-group").append('<input type="text" class="form-control" name="daterange" value="' + defaultFormat + '" />');

		dateRangePicker = $(".pick-date input").daterangepicker({
			"locale" : {
				"format" : "DD/MM/YYYY"
			},
			"autoApply" : true,
			"autoUpdateInput" : true,
			"alwaysShowCalendars" : true,
			"ranges" : {
				'3 Months' : [ moment().subtract(3, 'month'), moment() ],
				'6 Months' : [ moment().subtract(6, 'month'), moment() ],
				'1 Year' : [ moment().subtract(12, 'month'), moment() ],
				'3 Years' : [ moment().subtract(36, 'month'), moment() ],
			}
		}).on('apply.daterangepicker', function(ev, picker)
		{
			dashboardTable.draw();
		});
	}

	function formatMoney(code, money)
	{
		return code + " " + money.toFixed(2);
	}

	function formatDate(date)
	{
		if (date == null || date == '')
		{
			return '';
		}
		var d = moment(date).format("DD/MMM/YYYY");
		if (d == '01/01/1900')
		{
			return '';
		}
		return d;
	}
});
