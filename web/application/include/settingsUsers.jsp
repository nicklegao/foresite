<%@ page import="au.net.webdirector.common.datalayer.client.ElementData" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%@ page import="java.util.List" %>

<%
    UserAccountDataAccess uada = new UserAccountDataAccess();

    TUserRolesDataAccess urda = new TUserRolesDataAccess();

    String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
    String companyId = uada.getUserCompanyForUserId(userId).getId();

    Hashtable<String, ElementData> companyUsers = uada.getCompanyUsers(companyId);
%>
<%--CONTENT AREA--%>
<div class="widget-header px-4 py-6 row no-gutters align-items-center justify-content-between">

    <div class="col">
        <span class="h5">Users</span>
    </div>

    <div>
        <button type="button" id="addItem" class="btn btn-default-blue pull-left" style="margin-right: 20px;">
            <i class="fa fa-plus"></i> <span class="addItemText">Add User</span>
        </button>
        <%--<button type="button" id="exportUsers" class="btn btn-warning pull-left">--%>
            <%--<i class="fa fa-arrow-down"></i> <span class="exportUsersText">Export Users</span>--%>
        <%--</button>--%>
    </div>

</div>
<div class="tab-content"  id="users-management-modal">
    <div class="tab-pane fade show active p-3" id="heading-tab-pane" role="tabpanel" aria-labelledby="heading-tab">
        <div class="col-12 p-3 card"  style="box-shadow: none">

            <table class="table table-bordered table-hover table-full-width" id="table-users" data-module="User" width="100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th hidden>Team</th>
                    <th>Roles</th>
                    <th width="140">Actions</th>
                </tr>
                </thead>
                <tbody>
                <%
                    for (ElementData user : companyUsers.values())
                    {
                %>
                <tr data-id="<%= user.getId()%>">
                    <td>
                        <div class="row align-items-center">
                            <div class="col">
                                <span><%=user.get(uada.E_NAME) + " " + user.get(uada.E_SURNAME)%> <%=(user.getId().equals(userId) ? "(You)" : "")%></span>
                            </div>
                        </div>
                    </td>
                    <td hidden><%=((CategoryData) user.get("team")).getName()%></td>
                    <td><%
                        List<ElementData> userRoles= urda.getRolesForUserId(user.getId());

                        for(int i=0;i<userRoles.size();i++){
                    %>
                        <%=userRoles.get(i).getName()+(i<userRoles.size()-1?",":"") %>
                        <% } %>
                    </td>
                    <td>
                        <div class="btn-group" data-id="<%= user.getId() %>">
                            <a class="btn btn-secondary edit-item tooltips" href="#">
                                Edit
                            </a>
                            <button class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" <%=(user.getId().equals(userId) ? "disabled" : "")%>>
<%--                                <span class="caret"></span>--%>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <% if(user.getString(uada.E_SURNAME).contains("(Pending Verification")) { %>
                                <a class="dropdown-item" id="resend-activation-item" href="#">Resend Activation Email</a>
                                <% } %>
                                <% if(!user.getId().equals(userId)) { %>
                                <a class="dropdown-item" id="delete-item" href="#">Delete User</a>
                                <% } %>
                            </div>
                            <ul class="dropdown-menu">
                                <%--<li>--%>
                                    <%--<a class="export-item">--%>
                                        <%--<i class="fa fa-arrow-down"></i>--%>
                                        <%--<span> Export</span>--%>
                                    <%--</a>--%>
                                <%--</li>--%>

                                <% if(!user.getId().equals(userId)) { %>
                                    <li style="text-align: center">
                                    <a class="delete-item">
                                        <i class="fa fa-times"></i>
                                        <span> Delete</span>
                                    </a>
                                </li>
                                <% } %>
                            </ul>
                        </div>
                    </td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>

        </div>
    </div>
</div>

<script>

    // var domHead = "<'navbar' <'navbar-form navbar-left'<'form-group' l>> <'navbar-form navbar-right'<'form-group' f>> >";
    var domBody = "t";
    var domFooter = "<'navbar' <'navbar-form navbar-left' i> <'navbar-form navbar-right'<'form-group' p>> <'navbar-form navbar-right' <'#actionsHolder'>> >";

    $("#table-users").DataTable({
        "oLanguage" : {
            "sLengthMenu" : "_MENU_",
            "sSearch" : "",
            "oPaginate" : {
                "sPrevious" : "",
                "sNext" : ""
            },
        },
        "columns" : [ null, null, null, {
            "orderable" : false
        } ],
        "sDom" : domBody + domFooter,
        "fnDrawCallback" : function(oSettings)
        {

            $('#users-management-modal .edit-item').click(editItemAction);
            $('#addItem').click(addItemAction);
            $('#users-management-modal #delete-item').click(deleteUserItemAction);
            $('[data-toggle="dropdown"]').dropdown();
        }
    });

    $('.nav-tabs li a').on('click', function()
    {
        var item = $(this).data('module');
        $(this).closest('.modal').find('.modal-title').text(item + 's');
        $(this).closest('.modal').find('.addItemText').text('Add ' + item);
        if (item === "User") {
            $('#exportUsers').show();
        } else {
            $('#exportUsers').hide();
        }
    });


    function migrateUsersAction(id)
    {
        var params = {};

        params.id = id;

        $('body').modalmanager('loading');

        var $modal = $('#migrations-modal');
        $modal.load('/application/dialog/migrationsUser.jsp', params, function()
        {
            $modal.modal({
                width : '660px',
                backdrop : 'static',
                keyboard : false
            });
        });
    }


    function addItemAction()
    {

        var $currentItem = $("#users-management-modal").find("table:visible");
        var module = $currentItem.attr('data-module');
        // if (module == 'User')
        // {
        //     $.ajax({
        //         url : '/api/reachMaxUserLimit',
        //         success : function(data)
        //         {
        //             if (!data.success)
        //             {
        //                 addItemAction1();
        //             }
        //             else
        //             {
        //                 swal({
        //                     title : 'Do you want to continue?',
        //                     text : data.message,
        //                     type : 'warning',
        //                     showCancelButton : true,
        //                 }, function(isConfirm)
        //                 {
        //                     if (isConfirm)
        //                     {
        //                         addItemAction1();
        //                     }
        //                 })
        //             }
        //         },
        //         error : function()
        //         {
        //             swal({
        //                 title: "Error",
        //                 type: "error",
        //                 text: "Unable to add user now. Please try again later."
        //             });
        //         }
        //     })
        // }
        // else
        // {
        //     addItemAction1();
        // }
        addItemAction1();
    }

    function addItemAction1()
    {

        var $currentItem = $("#users-management-modal").find("table:visible");
        var params = {};
        var module = $currentItem.attr('data-module');
        var action = "add";

        params.action = action;

        $('body').modalmanager('loading');

        var $modal = $('#edit-management-modal');
        $modal.load('/application/dialog/edit' + module + '.jsp', params, function()
        {
            $modal.modal({
                width : module == 'Role' ? '710px' : '760px',
                backdrop : 'static',
                keyboard : false
            });
        });
    }
    //myProfile

    function editMyProfile() {
        console.log("edit My Profile");
        var params = {};
        var id = $(this).closest(".btn-group").attr("data-id");
        var module = $(this).closest("table").attr('data-module');
        var action = "edit";

        params.id = <%=userId%>;
        params.action = action;

        $('body').modalmanager('loading');

        var $modal = $('#edit-management-modal');

        $modal.load('/application/dialog/editUser.jsp', params, function()
        {
            console.log("editing " + module);
            $modal.modal({
                width : '760px',
                backdrop : 'static',
                keyboard : false
            });
        });
    }

    function editItemAction()
    {
        console.log("edit item");
        var params = {};
        var id = $(this).closest(".btn-group").attr("data-id");
        var module = $(this).closest("table").attr('data-module');
        var action = "edit";

        params.id = id;
        params.action = action;

        $('body').modalmanager('loading');

        var $modal = $('#edit-management-modal');

        $modal.load('/application/dialog/editUser.jsp', params, function()
        {
            console.log("editing " + module);
            $modal.modal({
                width : '760px',
                backdrop : 'static',
                keyboard : false
            });
        });
    }


    function deleteUserItemAction()
    {
        console.log("delete User");
        var params = {};
        var id = $(this).closest(".btn-group").attr("data-id");
        var module = $(this).closest("table").attr('data-module');
        var action = "delete";

        params.id = id;

        swal({
            title : 'Are you sure?',
            text : 'Do you want to delete this User',
            type : 'warning',
            showCancelButton : true,
        }, function(isConfirm)
        {
            if (isConfirm)
            {
                $.ajax({
                    url : '/api/deleteUser',
                    data : {
                        id : id
                    },
                    success : function(data)
                    {
                        stopLoadingForeground();
                        if (data.success === false)
                        {
                            if (data.action === 'migrate')
                            {
                            	migrateUsersAction(id)
                            }
                            else
                            {
                                swal({
                                    title : 'Error',
                                    type : 'error',
                                    text : data.message
                                });
                            }
                        }
                        else
                        {
                            swal({
                                title : 'Deleted!',
                                text : 'The User has been deleted.',
                                type : 'success'
                            });
                            $('#settingsMenuUsers').click();
                        }
                    },
                    error : function()
                    {
                        stopLoadingForeground();

                        alert("Unable to delete your " + module + ". Please try again later.");
                    }
                })
            }
        })
    }

    $('#users-management-modal .dataTables_filter').prepend('<label class="search-label"><span class="search-icon fa fa-search"></span></label>');
    $('#users-management-modal .dataTables_filter input').addClass("search-box form-control").attr("placeholder", "Search...");
</script>

<script>

    $(document).ready(function() {
        if (get("myProfile") != null) {
            editMyProfile();
        }
    });

    function get(name){
        if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
            return decodeURIComponent(name[1]);
    }
</script>
