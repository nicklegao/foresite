
<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<meta name="format-detection" content="telephone=no">
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width">
<jsp:include page="/application/touch-icon.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" media="screen" href="/application/assets/plugins/bootstrap-qcloud/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="/application/assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/application/assets/plugins/bootstrap-daterangepicker/daterangepicker.css">

<link rel="stylesheet" href="/application/assets/plugins/qcloud-iconfont/style.css" />
<link rel="stylesheet" href="/application/assets/fonts/style.css" />
<link rel="stylesheet" href="/application/assets/plugins/iCheck/skins/all.css" />

<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/css/styles.css", request) %>" /><!-- Try get rid of this -->

<link rel="stylesheet" href="/application/assets/css/print.css" media="print" />
<!--[if IE 7]>
<link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
<![endif]-->

<!-- For this page only -->
<link rel="stylesheet" href="/application/assets/plugins/DataTables/media/css/DT_bootstrap.css" />
<link rel="stylesheet" href="/application/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="/application/assets/plugins/DataTables/buttons/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="/application/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" />
<link rel="stylesheet" href="/application/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" />
<link rel="stylesheet" href="/application/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css">
<link rel="stylesheet" href="/application/assets/plugins/jquery-fixedheadertable/css/defaultTheme.css" />
<link rel="stylesheet" href="/application/assets/plugins/sweetalert/sweetalert.css" />
<link rel="stylesheet" href="/application/assets/plugins/swalExtend/swalExtend.css" />
<link rel="stylesheet" href="/application/assets/plugins/hopscotch-0.2.5/css/hopscotch.css">
<link rel="stylesheet" href="/application/assets/plugins/dynatable.0.3.1/jquery.dynatable.css">
<link rel="stylesheet" href="/application/assets/plugins/dropzone/downloads/css/dropzone.css" />
<link rel="stylesheet" href="/application/assets/plugins/Jcrop/css/jquery.Jcrop.min.css" />
<link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/all.css">

<link rel="stylesheet" href="/application/css/modal.css" type="text/css"/>
<link rel="stylesheet" href="/stores/_fonts/stylesheet.css" type="text/css"/>
<link rel="stylesheet" href="/application/assets/plugins/ripple/ripple.min.css" type="text/css"/>
<link rel="stylesheet" href="/application/assets/fonts/sign-fonts.css" />