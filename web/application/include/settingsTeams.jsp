<%@ page import="au.net.webdirector.common.datalayer.client.ElementData" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%@ page import="java.util.List" %>

<%
    UserAccountDataAccess uada = new UserAccountDataAccess();


    String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
    String companyId = uada.getUserCompanyForUserId(userId).getId();

    List<CategoryData> teams = uada.getCompanyTeams(companyId);
%>
<%--CONTENT AREA--%>
<div class="widget-header px-4 py-6 row no-gutters align-items-center justify-content-between">

    <div class="col">
        <span class="h5">Teams</span>
    </div>

    <div>
        <button type="button" id="addItem" class="btn btn-info pull-left" style="margin-right: 20px;">
            <i class="fa fa-plus"></i> <span class="addItemText">Add Team</span>
        </button>
    </div>

</div>
<div class="tab-content"  id="users-management-modal">
    <div class="tab-pane fade show active p-3" id="heading-tab-pane" role="tabpanel" aria-labelledby="heading-tab">
        <div class="col-12 p-3 card" style="box-shadow: none">

            <table class="table table-bordered table-hover table-full-width" id="table-teams" data-module="Team" width="100%">
                <thead>
                <tr>
                    <th>Team</th>
                    <th width="140">Actions</th>
                </tr>
                </thead>
                <tbody>
                <%
                    for (CategoryData team : teams)
                    {
                %>
                <tr>
                    <td><%=team.getName()%></td>
                    <td>
                        <div class="btn-group" data-id="<%= team.getId() %>" >
                            <a class="btn btn-secondary edit-item tooltips" href="#">
                                Edit
                            </a>
                            <button class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<%--                                <span class="caret"></span>--%>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu">
                                <li style="text-align: center">
                                    <a class="delete-item">
                                        <i class="fa fa-times"></i>
                                        <span> Delete</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>

        </div>
    </div>
</div>

<script>

    var domBody = "t";
    var domFooter = "<'navbar' <'navbar-form navbar-left' i> <'navbar-form navbar-right'<'form-group' p>> <'navbar-form navbar-right' <'#actionsHolder'>> >";

    $("#table-teams").DataTable({
        "oLanguage" : {
            "sLengthMenu" : "_MENU_",
            "sSearch" : "",
            "oPaginate" : {
                "sPrevious" : "",
                "sNext" : ""
            },
        },
        "columns" : [ null, {
            "orderable" : false
        } ],
        "sDom" : domBody + domFooter,
        "fnDrawCallback" : function(oSettings)
        {

            $('#users-management-modal .edit-item').click(editItemAction);
            // $('#users-management-modal .delete-item').click(deleteItemAction);
        }
    });

    $('.nav-tabs li a').on('click', function()
    {
        var item = $(this).data('module');
        $(this).closest('.modal').find('.modal-title').text(item + 's');
        $(this).closest('.modal').find('.addItemText').text('Add ' + item)
        if (item === "User") {
            $('#exportUsers').show();
        } else {
            $('#exportUsers').hide();
        }
    });

    function editItemAction()
    {
        var params = {};
        var id = $(this).closest(".btn-group").attr("data-id");
        var module = $(this).closest("table").attr('data-module');
        var action = "edit";

        params.id = id;
        params.action = action;

        $('body').modalmanager('loading');

        var $modal = $('#edit-management-modal');

        $modal.load('/application/dialog/editTeam.jsp', params, function()
        {
            console.log("editing " + module);
            $modal.modal({
                width : '760px',
                backdrop : 'static',
                keyboard : false
            });
        });
    }

    $('#users-management-modal .dataTables_filter').prepend('<label class="search-label"><span class="search-icon fa fa-search"></span></label>');
    $('#users-management-modal .dataTables_filter input').addClass("search-box form-control").attr("placeholder", "Search...");
</script>
