<%@page import="com.stripe.model.Customer"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.corporateinteractive.qcloud.subscription.SubscriptionPlan"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ page import="au.net.webdirector.common.datalayer.client.ModuleAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.base.db.entity.MultiOptions" %>
<%
UserAccountDataAccess uada = new UserAccountDataAccess();
String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
CategoryData companyData = uada.getUserCompanyForUserId(userId);
String companyName = uada.getUserCompanyNameForUserId(userId);
%>

<link rel="stylesheet" href="/application/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css">
<div class="col-12 p-3">

    <div class="widget widget6 card">

        <div class="widget-content">
        
<div id="account-settings">
	<form class="form-account-settings" method="POST">
            <ul class="nav nav-tabs" id="accountSettings" role="tablist">

                <li class="nav-item">
                    <a class="nav-link btn active fuse-ripple-ready" style="color:black !important;" id="company-tab" data-toggle="tab" href="#company-tab-pane" role="tab" aria-controls="company-tab-pane" aria-expanded="true">Company Details</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link btn fuse-ripple-ready" style="color:black !important;" id="payment-tab" data-toggle="tab" href="#payment-tab-pane" role="tab" aria-controls="payment-tab-pane">Payment Details</a>
                </li>
            </ul>
            
            <div class="tab-content">
			  <div id="company-tab-pane" class="tab-pane in active" aria-expanded="true">
			    <div class="company-details">
					<div class="row">
						<section class="col col-6 form-group">
							<label for="companyName" class="label"> Company Name</label> 
						      <input value="<%=ESAPI.encoder().encodeForHTML(companyData.getString("attr_categoryname"))%>" 
						      	type="text" class="form-control" id="companyName" name="companyName" placeholder="Company Name">
						</section>
						<section class="col col-6 form-group">
							<label for="abnacn" class="label"> Company ID/Registration Number</label> 
		                      <input type="text" class="form-control" name="abnacn" id="abnacn" placeholder="Company ID/Registration Number" 
		                      value="<%=ESAPI.encoder().encodeForHTML(companyData.getString("attr_abnacn"))%>">
						</section>
						<section class="col col-xs-12 form-group">
							<label for="addressLine1" class="label"> Address Line 1</label> 
						      <input value="<%=ESAPI.encoder().encodeForHTML(companyData.getString("attr_addressLine1"))%>" 
						      	type="text" class="form-control" id="addressLine1" name="addressLine1" placeholder="Address Line 1">
						</section>
						<section class="col col-xs-12 form-group">
							<label for="addressLine2" class="label"> Address Line 2</label> 
		                      <input type="text" class="form-control" name="addressLine2" id="addressLine2" placeholder="Address Line 2" 
		                      value="<%=ESAPI.encoder().encodeForHTML(companyData.getString("attr_addressLine2"))%>">
						</section>
						<section class="col col-6 form-group">
							<label for="citySuburb" class="label"> City/Suburb</label> 
						      <input value="<%=ESAPI.encoder().encodeForHTML(companyData.getString("attr_citySuburb"))%>" 
						      	type="text" class="form-control" id="citySuburb" name="citySuburb" placeholder="City/Suburb">
						</section>
						<section class="col col-6 form-group">
							<label for="postcode" class="label"> Postcode</label> 
		                      <input type="text" class="form-control" name="postcode" id="postcode" placeholder="Postcode" 
		                      value="<%=ESAPI.encoder().encodeForHTML(companyData.getString("attr_postcode"))%>">
						</section>
						<section class="col col-6 form-group">
							<label for="state" class="label"> State</label> 
						      <input value="<%=ESAPI.encoder().encodeForHTML(companyData.getString("attr_state"))%>" 
						      	type="text" class="form-control" id="state" name="state" placeholder="State">
						</section>
						<section class="col col-6 form-group">
							<label for="country" class="label"> Country</label> 
		                    <select name="country" class="form-control" disabled>
		  						<% 
		                            for(String country : SubscriptionPlan.getSupportCountries()){
		                          	if(!StringUtils.equalsIgnoreCase(country, companyData.getString("attrdrop_country"))){
		                                continue;
		                              }
		                            %>
		                              <option value="<%= country %>"><%= country %></option>
		                            <% 
		                            } 
		                          %>
		  					</select>
						</section>
					</div>
				</div>
			  </div>
			  <div id="payment-tab-pane" class="tab-pane" aria-expanded="false">
			    <div class="payment-details">
					<div class="row">
						<%
							MultiOptions manages = ModuleAccess.getInstance().getMultiOptions(companyData, uada.C_BILLING_USERS,
									uada.USER_MODULE);
							String emailList = StringUtils.join(manages.getValues(), ",");
						%>
						<section class="col col-xs-12 form-group">
							<label for="payment-user-email-list" class="label"> Users who receive invoice related emails:</label>
							<input id="payment-user-email-list" type="text" class="tags" value="<%=emailList%>" name="payment-user-email-list">
						</section>
					</div>
				</div>
			  </div>
              <div class="row">
          		<div class="col col-12 form-group">
	          		<button class="btn btn-success"> Save </button>  
	            </div>
              </div>
			</div>
	</form>
</div>
        </div>

    </div>
</div>


  <script src="/application/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
<script src="/application/js/form-validation/abn.js"></script>
<script src="/application/js/form-validation/acn.js"></script>
<script src="/application/js/form-validation/modal-account-settings.js"></script>
			