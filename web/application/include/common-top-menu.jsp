<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions" %>
<%
    String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
    String userUsername = (String)session.getAttribute(Constants.PORTAL_USERNAME_ATTR);
    UserAccountDataAccess uada = new UserAccountDataAccess();
    Hashtable<String, String> userInfo = uada.getUserWithId(userId);
    TUserRolesDataAccess urda = new TUserRolesDataAccess();
    UserPermissions up = urda.getPermissions(userId);

%>
<nav id="toolbar" class="bg-white">

    <div class="row no-gutters align-items-center flex-nowrap">

        <div class="col">

            <div class="row no-gutters align-items-center flex-nowrap">

                <button type="button" class="toggle-aside-button btn btn-icon d-block d-lg-none" data-fuse-bar-toggle="aside">
                    <i class="icon icon-menu"></i>
                </button>

                <div class="toolbar-separator d-block d-lg-none"></div>

                <%--<div class="shortcuts-wrapper row no-gutters align-items-center px-0 px-sm-2">--%>

                    <%--<div class="shortcuts row no-gutters align-items-center d-none d-md-flex">--%>

                        <%--<a href="apps-chat.html" class="shortcut-button btn btn-icon mx-1">--%>
                            <%--<i class="icon icon-hangouts"></i>--%>
                        <%--</a>--%>

                        <%--<a href="apps-mail.html" class="shortcut-button btn btn-icon mx-1">--%>
                            <%--<i class="icon icon-email"></i>--%>
                        <%--</a>--%>

                    <%--</div>--%>
                <%--</div>--%>

                <%--<div class="toolbar-separator"></div>--%>

            </div>
        </div>

        <%-- <%
            if (up.hasManageTemplatesPermission()) {
        %>
        <div class="col-auto">

            <div class="row no-gutters align-items-center justify-content-end">

                <div class="toolbar-separator"></div>
                <div class="user-menu-button content-library-button dropdown">
                    <div class="dropdown-toggle ripple row align-items-center no-gutters px-2 px-sm-4" role="button" id="dropdownContentLibraries" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mx-3 d-none d-md-block">Content Libraries</span>
                    </div>
                    <div class="dropdown-menu" aria-labelledby="dropdownUserMenu">
                        <a class="dropdown-item my-2" href="/file-manager?libraryType=text">
                            <div class="row no-gutters align-items-center flex-nowrap">
                                <i class="fal fa-bars"></i>
                                <span class="px-3">Text</span>
                            </div>
                        </a>
                        <a class="dropdown-item my-2" href="/file-manager?libraryType=images">
                            <div class="row no-gutters align-items-center flex-nowrap">
                                <i class="fal fa-image"></i>
                                <span class="px-3">Images</span>
                            </div>
                        </a>
                        <a class="dropdown-item my-2" href="/file-manager?libraryType=videos">
                            <div class="row no-gutters align-items-center flex-nowrap">
                                <i class="fal fa-play"></i>
                                <span class="px-3">Videos</span>
                            </div>
                        </a>
                        <div class="dropdown-divider my-2"></div>
                        <a class="dropdown-item my-2" href="/file-manager?libraryType=cover">
                            <div class="row no-gutters align-items-center flex-nowrap">
                                <i class="fal fa-file-image"></i>
                                <span class="px-3">Cover Pages</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <%
            }
        %> --%>

        <div class="col-auto">

            <div class="row no-gutters align-items-center justify-content-end">

                <div class="toolbar-separator"></div>
                <div class="user-menu-button dropdown">
                    <div class="dropdown-toggle ripple row align-items-center no-gutters px-2 px-sm-4" role="button" id="dropdownUserMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<%--                        <div class="avatar-wrapper">--%>
<%--                            <img class="avatar" src="/application/assets/new/images/avatars/profile.jpg">--%>
<%--                        </div>--%>
                        <span class="username mx-3 d-none d-md-block"><%=userInfo.get(UserAccountDataAccess.E_NAME)%> <%=userInfo.get(UserAccountDataAccess.E_SURNAME)%></span>
                    </div>
                    <div class="dropdown-menu" aria-labelledby="dropdownUserMenu">
                        <a class="dropdown-item  my-2" href="/users?myProfile=true">
                            <div class="row no-gutters align-items-center flex-nowrap">
                                <i class="fas fa-user"></i>
                                <span class="px-3">My Profile</span>
                            </div>
                        </a>
                        <a class="dropdown-item  my-2" href="/users#settingsMenuSecurity" id="account-security">
                            <div class="row no-gutters align-items-center flex-nowrap">
                                <i class="fas fa-lock"></i>
                                <span class="px-3">Account Security</span>
                            </div>
                        </a>
                        <div class="dropdown-divider  my-2"></div>
                        <%--<a class="dropdown-item" href="/settings">--%>
                            <%--<div class="row no-gutters align-items-center flex-nowrap">--%>
                                <%--<i class="fas fa-bell fa-lg"></i>--%>
                                <%--<span class="px-3">Manage Notifications</span>--%>
                            <%--</div>--%>
                        <%--</a>--%>
                        <a class="dropdown-item  my-2" href="#email-support" id="emailBtn" data-toggle="modal">
                            <div class="row no-gutters align-items-center flex-nowrap">
                                <i class="fas fa-envelope "></i>
                                <span class="px-3">Email Support</span>
                            </div>
                        </a>
                        <%--<div class="dropdown-divider"></div>--%>
<%--                        <a class="dropdown-item  my-2" href="#">--%>
<%--                            <div class="row no-gutters align-items-center flex-nowrap">--%>
<%--                                <i class="fas fa-dollar-sign "></i>--%>
<%--                                <span class="px-3">Subscription Settings</span>--%>
<%--                            </div>--%>
<%--                        </a>--%>
                        <div class="dropdown-divider  my-2"></div>
                        <a class="dropdown-item  my-2" href="/api/free/logout">
                            <div class="row no-gutters align-items-center flex-nowrap">
                                <i class="fas fa-sign-out-alt"></i>
                                <span class="px-3">Logout</span>
                            </div>
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
 <div id="email-support" class="modal top-modal fade" tabindex="-1">
	    <form class="form-email-support" action="/api/emailSupport" method="POST">
	      <div class="modal-header">
	        <div class="titleBar">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	        </div>
	        <h2 class="modal-title">Email Support</h2>
	      </div>
	      <div class="modal-body">
	        <p>Please put down your questions:</p>
			  <div class="form-horizontal">
				  <div class="col-sm-12">
					  <textarea rows="5" id="message" class="form-control" name="message" placeholder="Questions"></textarea>
				  </div>
			  </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" id="closeForm" class="cancel-button" data-dismiss="modal">
	          Close
	        </button>
	        <button type="submit" class="success-button">
	          Submit
	        </button>
	      </div>
	    </form>
	  </div>
	  
	  
	  <jsp:include page="/application/dialog/backgroundLoading.jsp"></jsp:include>
	  
	  <script src="/application/assets/plugins/jquery.form/jquery.form.js"></script>
	  <script src="/application/js/dialogs.js"></script>
	  <script type="text/javascript" src="/application/js/form-validation/modal-email-support.js"></script>
	  <script type="text/javascript" src="/application/js/loading.js"></script>
	  
	  <!-- Bootstrap -->
		<link type="text/css" rel="stylesheet" href="/application/assets/plugins/bootstrap/css/bootstrap.min.css" />
		<link type="text/css" rel="stylesheet" href="/application/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" />
		<link type="text/css" rel="stylesheet" href="/application/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" />
		
		<script type="text/javascript" src="/application/assets/new/vendor/bootstrap/bootstrap.min.js"></script>
		