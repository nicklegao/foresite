<%@ page import="au.net.webdirector.common.datalayer.client.ElementData" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%@ page import="java.util.List" %>

<%
    UserAccountDataAccess uada = new UserAccountDataAccess();

    TUserRolesDataAccess urda = new TUserRolesDataAccess();

    String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
    String companyId = uada.getUserCompanyForUserId(userId).getId();

    List<ElementData> roles = urda.getCompanyRoles(companyId);
%>
<%--CONTENT AREA--%>
<div class="widget-header px-4 py-6 row no-gutters align-items-center justify-content-between">

    <div class="col">
        <span class="h5">Roles</span>
    </div>

    <div>
        <button type="button" id="addItem" class="btn btn-default-blue pull-left" style="margin-right: 20px;">
            <i class="fa fa-plus" style="font-size: 16px;padding-top: 0px;"></i> <span class="addItemText">Add Role</span>
        </button>
    </div>

</div>
<div class="tab-content" id="users-management-modal">
    <div class="tab-pane fade show active p-3" id="heading-tab-pane" role="tabpanel" aria-labelledby="heading-tab">
        <div class="col-12 p-3 card" style="box-shadow: none">

            <table class="table table-bordered table-hover table-full-width" id="table-roles" data-module="Role" width="100%">
                <thead>
                <tr>
                    <th>Role</th>
                    <th width="140">Actions</th>
                </tr>
                </thead>
                <tbody>
                <%
                    for (ElementData role : roles)
                    {
                %>
                <tr>
                    <td><%=role.getName()%></td>
                    <td>
                        <div class="btn-group" data-id="<%= role.getId() %>">
                            <a class="btn btn-secondary edit-item tooltips" href="#">
                                Edit
                            </a>
                            <button class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<%--                                <span class="caret"></span>--%>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" id="delete-item" href="#">Delete</a>
                            </div>
                            <ul class="dropdown-menu">
                                <li style="text-align: center">
                                    <a class="delete-item">
                                        <i class="fa fa-times"></i>
                                        <span> Delete</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>


</script>
