<div class="collapse-card card">
    <div class="collapse-card__heading" style="border-bottom: 1px solid lightgray;">
        <div class="collapse-card__title">
            <div class="row">
                <div class="col-auto">
                    <i class="fas fa-random fa-2x fa-fw" style="padding-top: 3px;"></i>
                </div>
                <div class="col">
                    <h6 style="" class="agentMainTitle">New Agent Link</h6>
                </div>
                <div class="col-auto agentTrashButton" style="display: none;">
                    <i class="fas fa-trash fa-2x fa-fw" style="padding-top: 3px;"></i>
                </div>
            </div>

        </div>
    </div>
    <div class="collapse-card__body">
        <div class="row">
            <div class="col">
                <div class="row">
                    <div class="col">
                        <span class="h4">Agent Information</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <form>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="agentName">Agent Name</label>
                                        <input class="form-control" id="agentName" aria-describedby="agentName" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="agentCode">Agent Code</label>
                                        <input class="form-control" id="agentCode" aria-describedby="agentCode" >
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="customerCode">Customer Code</label>
                                        <input class="form-control" id="customerCode" aria-describedby="customerCode">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>