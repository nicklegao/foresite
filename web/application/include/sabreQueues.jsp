<%@page import="org.owasp.esapi.ESAPI"%>
<%@ page import="au.net.webdirector.common.datalayer.client.ElementData" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.SabreAuthDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.AgentsDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess" %>
<%@ page import="au.com.ci.system.classfinder.StringUtil" %>
<%@ page import="java.util.*" %>
<%@ page import="au.net.webdirector.common.datalayer.client.ModuleAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.base.db.entity.MultiOptions" %>
<script>
    var templates = {};
    var countries = {};
</script>
<%
    UserAccountDataAccess uada = new UserAccountDataAccess();
    AgentsDataAccess ada = new AgentsDataAccess();

    TUserRolesDataAccess urda = new TUserRolesDataAccess();
    SabreAuthDataAccess sada = new SabreAuthDataAccess();

    String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
    String companyId = uada.getUserCompanyForUserId(userId).getId();
    CategoryData company = uada.getUserCompanyForUserId(userId);
    CategoryData queueInfo = sada.getQueueCompanyByCompanyID(companyId);

    List<ElementData> agents = new ArrayList<>();
    Hashtable<String, ElementData> companyUsers = uada.getCompanyUsers(companyId);

    if (company != null && company.getString(UserAccountDataAccess.C_SABRE_TYPE).equalsIgnoreCase("gds")) {
        agents = ada.getAgentsForCompanyID(companyId);
    }

    TravelDocsDataAccess tdda = new TravelDocsDataAccess();
    Vector<Hashtable<String, String>> templateData = tdda.getTemplates(companyId, company.getName());
    List<String> countryList = tdda.getListOfiATACountries();
    Hashtable<String, String> templates = new Hashtable<>();
    for (Hashtable<String, String> template : templateData) {
        %>
        <script>
            templates["<%=template.get("Element_id")%>"] = "<%=template.get("ATTR_Headline")%>";
        </script>

<%
        templates.put(template.get("Element_id"), template.get("ATTR_Headline"));
    }
    for (String country : countryList) {
        %>

<script>
    countries["<%=country.toLowerCase()%>"] = "<%=country%>";
</script>
<%

    }

%>
<%--CONTENT AREA--%>
<div class="widget-header px-4 py-6 row no-gutters align-items-center justify-content-between">

    <div class="col">
        <span class="h5">Integration Details</span>
    </div>

</div>
<div class="tab-content"  id="sabre-management-modal">
    <div class="tab-pane fade show active p-3" id="heading-tab-pane" role="tabpanel" aria-labelledby="heading-tab">
        <div class="row">
            <div class="col"></div>
            <div class="col-5 mr-5 p-3 card">
                <div class="row">
                    <div class="col">
                        <span class="h5">Sabre API</span>
                    </div>
                    <div class="col-auto">
                        <select class="form-control travelDocSabreChooser" style="width: 80px; display: none;">
                            <% if (company != null) { %>
                            <option value="GDS" <%=company.getString(UserAccountDataAccess.C_SABRE_TYPE).equalsIgnoreCase("gds") ? "selected" : ""%>>GDS</option>
                            <option value="SAM" <%=company.getString(UserAccountDataAccess.C_SABRE_TYPE).equalsIgnoreCase("sam") ? "selected" : ""%>>SAM</option>
                            <% } else { %>
                            <option value="GDS" select>GDS</option>
                            <option value="SAM">SAM</option>
                            <% } %>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="helper-text text-center mt-10 text-muted" style="<%=company.getString(UserAccountDataAccess.C_SABRE_TYPE).equalsIgnoreCase("sam") ? "display:none;" : ""%>">
                            <label>
                                Configure your Agent details below</label>
                        </div>
                        <form style="<%=company.getString(UserAccountDataAccess.C_SABRE_TYPE).equalsIgnoreCase("gds") ? "display:none;" : ""%>">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="sabreClientID">Client ID - Find in your Sabre Applications Console.</label>
                                        <input class="form-control" id="sabreClientID" aria-describedby="sabreClientID" placeholder="Enter your Sabre Client ID" value="<%=company != null ? company.getString(UserAccountDataAccess.C_SABRE_SAM_KEY) : ""%>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="sabreClientSecret">Client Secret - Find in your Sabre Applications Console.</label>
                                        <input type="password" class="form-control" id="sabreClientSecret" aria-describedby="sabreClientSecret" placeholder="Enter your Sabre Client Secret" value="<%=company != null ? company.getString(UserAccountDataAccess.C_SABRE_SAM_SECRET) : ""%>">
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-auto">
                                    <button type="submit" class="btn btn-secondary fuse-ripple-ready">Sync</button>
                                </div>
                                <div class="col" style="margin-top: 8px;">
                                    <span class="sync-status"></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-5 ml-5 p-3 card">
                <div class="row">
                    <div class="col">
                        <span class="h5">ZeroRisk API</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <form>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="ZRAPIKey">ZeroRisk API Key</label>
                                        <input class="form-control" id="ZRAPIKey" aria-describedby="ZRAPIKey" placeholder="Enter your ZeroRisk API Key" value="<%=company != null ? company.getString(UserAccountDataAccess.C_ZERO_RISK_APIKEY) : ""%>">
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-auto">
                                    <button id="saveSabreDetailsButton" class="btn btn-secondary fuse-ripple-ready">Sync</button>
                                </div>
                                <div class="col" style="margin-top: 8px;">
                                    <span class="sync-status"></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col"></div>
        </div>
    </div>
</div>
<div class="widget-header px-4 py-6 row no-gutters align-items-center justify-content-between sabre-queue-card-modal-header" style="<%=company.getString(UserAccountDataAccess.C_SABRE_TYPE).equalsIgnoreCase("sam") ? "display:none" : ""%>">

    <div class="col">
        <span class="h5">Sabre Agent List</span>
    </div>
    <div>
        <button type="button" id="addQueue" class="btn btn-default-blue pull-left" style="margin-right: 20px;">
            <i class="fa fa-plus"></i> <span class="addQueueButton">Add Agent</span>
        </button>
    </div>

</div>
<div class="tab-content"  id="sabre-queue-card-modal">
    <div class="tab-pane fade show active p-3" id="sabre-queue-card-container" role="tabpanel">

    <%
        for (ElementData agent : agents) {
    %>

        <div class="collapse-card card <%=AgentsDataAccess.isSecretKeyValid(agent) ? "" : "agent-pending"%>" data-sk="<%=ESAPI.encoder().encodeForHTMLAttribute(AgentsDataAccess.generateSecretKey(agent.getString(AgentsDataAccess.E_CUSTOMER_ID), agent.getString(AgentsDataAccess.E_AGENT_CODE), agent.getString(AgentsDataAccess.E_CUSTOMER_CODE)))%>">
            <div class="collapse-card__heading" style="border-bottom: 1px solid lightgray;">
                <div class="collapse-card__title">
                    <i class="fas fa-random fa-2x fa-fw" style="padding-top: 3px;"></i>
                    <h6 style="padding-left: 39px;" class="queueNameLabel"><%=agent.getName()%></h6>
                </div>
            </div>
            <div class="collapse-card__body">
                <input class="form-control" id="agentID" aria-describedby="agentID" value="<%=agent.getId()%>" style="display: none;">
                <div class="alert alert-danger pending-alert">
                    Please Call TravelDocs on <a href="tel:+611300662553">1300 662 553</a> or email <a href="mailto:hello@traveldocs.cloud?Subject=agent%20activation%20link" target="_top">hello@traveldocs.cloud</a> to activate this agent.
                    <br>
                    Reference No. <%= AgentsDataAccess.generateSecretKey(agent.getString(AgentsDataAccess.E_CUSTOMER_ID), agent.getString(AgentsDataAccess.E_AGENT_CODE), agent.getString(AgentsDataAccess.E_CUSTOMER_CODE)) %>
				</div>
                <div class="row">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <span class="h5">Agent Information</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <form>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="queueName">Agent Name</label>
                                                <input class="form-control" id="queueName" aria-describedby="queueName" value="<%=agent.getName()%>">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="queueName">Agent Phone Number</label>
                                                <input class="form-control" id="queuePhoneNumber" aria-describedby="queuePhoneNumber" value="<%=agent.getString(AgentsDataAccess.E_PHONE_NUMBER)%>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="agentCode">Agent Code</label>
                                                <input class="form-control" id="agentCode" aria-describedby="agentCode" value="<%=agent.getString(AgentsDataAccess.E_AGENT_CODE)%>">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="customerCode">Customer Code</label>
                                                <input class="form-control" id="customerCode" aria-describedby="customerCode" value="<%=agent.getString(AgentsDataAccess.E_CUSTOMER_CODE)%>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <select name="queueHomeCountry" multiple id="queueHomeCountry">
                                                    <%
                                                        MultiOptions savedHomeCountries = ModuleAccess.getInstance().getMultiOptions(agent, AgentsDataAccess.E_HOME_COUNTRY, AgentsDataAccess.MODULE);
                                                        List<String> homeCountriesList = savedHomeCountries.getValues();
                                                        for (String country : countryList) {
                                                    %>
                                                    <option value="<%=country%>" <%=homeCountriesList.contains(country) ? "selected" : ""%>><%=country%></option>
                                                    <%
                                                        }
                                                    %>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px;">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <span class="h5">TravelDocs Template</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Select a template to use for this queue</label>
                                    <select class="form-control traveldocTemplateSelector">
                                        <%
                                            Set<String> keys = templates.keySet();
                                            for (String key : keys) {
                                        %>
                                        <option value="<%=key%>" <%=key.equalsIgnoreCase(agent.getString(AgentsDataAccess.E_TEMPLATE_ID)) ? "selected" : ""%>><%=templates.get(key)%></option>
                                        <%
                                            }
                                        %>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <span class="h5">Queue Settings</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Auto-Send Queue</label>
                                    <input type="checkbox" class="autosendQueueSwitch" <%=agent.getBoolean(AgentsDataAccess.E_AUTO_SEND) ? "checked" : ""%> />
                                    <small id="emailHelp" class="form-text text-muted">This will automatically send your TravelDoc as the itinerary is recieved from Sabre.</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="invalidCountrySelectorContainer" style="<%=agent.getBoolean(AgentsDataAccess.E_AUTO_SEND) ? "" : "display:none;"%>">
                    <div class="col">
                        <div class="form-group">
                            <select name="invalidCountrySelector" multiple id="invalidCountrySelector">
                                <%
                                    MultiOptions savedAllowedCountries = ModuleAccess.getInstance().getMultiOptions(agent, AgentsDataAccess.E_ALLOWED_COUNTRIES, AgentsDataAccess.MODULE);
                                    List<String> allowedCountriesList = savedAllowedCountries.getValues();
                                    for (String country : countryList) {
                                %>
                                <option value="<%=country%>" <%= allowedCountriesList.contains(country) ? "selected" : ""%>><%=country%></option>
                                <%
                                    }
                                %>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <hr style="margin: 20px 0;">
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <button type="button" id="deleteQueueButton" class="btn btn-danger pull-left" style="margin-right: 20px;">
                            <span class="deleteQueueButton">Delete</span>
                        </button>
                    </div>
                    <div class="col-auto">
                        <button type="button" id="saveQueueButton" class="btn btn-secondary pull-left" style="margin-right: 20px;">
                            <span class="saveQueueButton">Save</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <%
            }
        %>


    </div>
</div>



<script>

    $('.collapse-card').each(function(e) {
        $(this).paperCollapse();
        var randomID = idGen.getId();
        var $elemsAutosend =  $(this).find('.autosendQueueSwitch');
        $elemsAutosend.attr('data-id', randomID);

        var switchAutosend = document.querySelector('.autosendQueueSwitch[data-id="'+randomID+'"]');

        var switcheryAutosend = new Switchery(switchAutosend);


        $('.autosendQueueSwitch[data-id="'+randomID+'"]').on('change', function(e) {

            var invalidCountrySelectorContainer = $($(this).closest('.collapse-card__body')).find('#invalidCountrySelectorContainer');

            if (this.checked) {
                $(this).addClass('active');
                $(invalidCountrySelectorContainer).show();
            } else {
                $(this).removeClass('active');
                $(invalidCountrySelectorContainer).hide();
            }
        });

        $elemsAutosend.data('switchery', switcheryAutosend);

        var queueHomeCountryMulti = $(this).find('#queueHomeCountry');
        var queueInvalidCountryMulti = $(this).find('#invalidCountrySelector');

        $(queueHomeCountryMulti).multiselect();
        $(queueInvalidCountryMulti).multiselect();

        $($(this).find('#invalidCountrySelectorContainer .selected-title')).html('Invalid Countries')
        $($(this).find('#invalidCountrySelectorContainer .unselected-title')).html('Valid Countries')

    });

</script>
