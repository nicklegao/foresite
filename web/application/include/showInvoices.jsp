
<link rel="stylesheet" type="text/css" href="/application/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
<style>
div.per-page.short-select-option{
  display: inline-block;
  padding: 0 5px;
}
.pick-date input
{
  min-width: 190px;
  text-align: center;
}
</style>

<div class="col-12 p-3">

    <div class="widget widget6 card">

        <div class="widget-content">
			<div class="col-12 p-3">
			  <table id="invoiceTable" class="table table-bordered table-hover table-full-width" cellspacing="0" width="100%">
			    <thead>
			      <tr>
			      </tr>
			    </thead>
			    <tbody>
			    </tbody>
			  </table>
	        </div>
        </div>

    </div>
</div>


<script src="/application/assets/plugins/moment/moment.js"></script>
<script src="/application/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script> 
<script src="/application/include/showInvoices.js"></script>
