<%@ page import="org.owasp.esapi.ESAPI" %>
<%@ page import="au.net.webdirector.common.Defaults" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsStylesDataAccess" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.OptionsDataAccess" %>
<%@ page import="java.util.List" %>

<link rel='stylesheet' href='/application/assets/plugins/paper-collapse/paper-collapse.min.css' />
<%
    UserAccountDataAccess uada = new UserAccountDataAccess();

    String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
    Hashtable<String, String> userDetails = uada.getUserWithId(userId);
    CategoryData company = uada.getUserCompanyForUserId(userId);
    TravelDocsStylesDataAccess tds = new TravelDocsStylesDataAccess();
    CategoryData companyStyles = tds.getCompanyStylesWithId(company.getId());

    OptionsDataAccess oda = new OptionsDataAccess();
    List<String> fonts = oda.getFonts();

    String topStyleInfo = companyStyles.getString(TravelDocsStylesDataAccess.C_HEADER_TOP_DATA);
    JSONObject topStyle = new JSONObject();
    JSONObject topStyleLeft = new JSONObject();
    JSONObject topStyleCenter = new JSONObject();
    JSONObject topStyleRight = new JSONObject();
    if (topStyleInfo != null && topStyleInfo.length() > 0) {
        topStyle = new JSONObject(topStyleInfo);
        topStyleLeft = topStyle.getJSONObject("left");
        topStyleCenter = topStyle.getJSONObject("center");
        topStyleRight = topStyle.getJSONObject("right");

    }

    Boolean topHeaderActive = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_HEADER_TOP_ACTIVE);
    String topHeaderFontColor = companyStyles.getString(TravelDocsStylesDataAccess.C_HEADER_TOP_FONT_COLOR);
    String topHeaderBackground = companyStyles.getString(TravelDocsStylesDataAccess.C_HEADER_TOP_BACKGROUND);

    String bottomStyleInfo = companyStyles.getString(TravelDocsStylesDataAccess.C_HEADER_BOTTOM_DATA);
    JSONObject bottomStyle = new JSONObject();
    JSONObject bottomStyleLeft = new JSONObject();
    JSONObject bottomStyleCenter = new JSONObject();
    JSONObject bottomStyleRight = new JSONObject();
    if (bottomStyleInfo != null && bottomStyleInfo.length() > 0) {
        bottomStyle = new JSONObject(bottomStyleInfo);
        bottomStyleLeft = bottomStyle.getJSONObject("left");
        bottomStyleCenter = bottomStyle.getJSONObject("center");
        bottomStyleRight = bottomStyle.getJSONObject("right");

    }

    Boolean bottomHeaderActive = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_HEADER_BOTTOM_ACTIVE);
    String bottomHeaderFontColor = companyStyles.getString(TravelDocsStylesDataAccess.C_HEADER_BOTTOM_FONT_COLOR);
    String bottomHeaderBackground = companyStyles.getString(TravelDocsStylesDataAccess.C_HEADER_BOTTOM_BACKGROUND);


    //Section Header Styles
    boolean uppercaseSection = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_SECTION_UPPERCASE);
    boolean underlineSection = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_SECTION_UNDERLINE);
    boolean boldSection = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_SECTION_BOLD);
    boolean customSectionFontColor = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_SECTION_CUSTOM_FONT_COLOR);

    //H1 styles
    boolean uppercaseH1 = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_H1_UPPERCASE);
    boolean underlineH1 = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_H1_UNDERLINE);
    boolean boldH1 = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_H1_BOLD);
    boolean customH1FontColor = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_H1_CUSTOM_FONT_COLOR);

    //H2 styles
    boolean uppercaseH2 = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_H2_UPPERCASE);
    boolean underlineH2 = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_H2_UNDERLINE);
    boolean boldH2 = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_H2_BOLD);
    boolean customH2FontColor = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_H2_CUSTOM_FONT_COLOR);

    //H3 styles
    boolean uppercaseH3 = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_H3_UPPERCASE);
    boolean underlineH3 = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_H3_UNDERLINE);
    boolean boldH3 = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_H3_BOLD);
    boolean customH3FontColor = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_H3_CUSTOM_FONT_COLOR);


    boolean showTabs = !companyStyles.getBoolean(TravelDocsStylesDataAccess.C_SHOW_TABS);
    boolean showCoverpage = !companyStyles.getBoolean(TravelDocsStylesDataAccess.C_HIDE_COVER_PAGE);
    boolean hideCoverProposalInfo = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_HIDE_COVER_PROPOSAL_INFO);
    boolean hideCoverSenderInfo = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_HIDE_COVER_SENDER_INFO);
    boolean hideCoverBoxBackground = companyStyles.getBoolean(TravelDocsStylesDataAccess.C_HIDE_COVER_BOX_BACKGROUND);

    String tabStyle = ESAPI.encoder().encodeForHTMLAttribute(companyStyles.getString(TravelDocsStylesDataAccess.C_TAB_STYLE));
    %>
<div class="col-12 p-3">

    <div class="widget widget6 card">

        <div class="widget-content">
            <ul class="nav nav-tabs" id="myTab" role="tablist">

                <li class="nav-item">
                    <a class="nav-link btn active fuse-ripple-ready" id="heading-tab" data-toggle="tab" href="#heading-tab-pane" role="tab" aria-controls="heading-tab-pane" aria-expanded="true">Heading and Fonts</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link btn fuse-ripple-ready" id="colours-tab" data-toggle="tab" href="#colours-tab-pane" role="tab" aria-controls="colours-tab-pane">Colours</a>
                </li>

            </ul>
        </div>

    </div>
</div>

<%--CONTENT AREA--%>
<div class="tab-content">
    <div class="tab-pane fade show active p-3" id="heading-tab-pane" role="tabpanel" aria-labelledby="heading-tab">
        <div class="col-12 p-3">

            <div class="widget widget6 card">

                <div class="widget-content p-4">
                    <h2 class="text-center">Proposal Font Options</h2>

                    <%--SECTION HEADINGS CARDS--%>
                    <div class="collapse-card">
                        <div class="collapse-card__heading">
                            <div class="collapse-card__title">
                                <i class="icon s-4 icon-format-font"></i>
                                <span class="styleHeadingTitles">Section Heading</span>
                                <% if (underlineSection) { %>
                                <i class="fa fa-underline fa-2x fa-fw pull-right"></i>
                                <% } %>
                                <% if (boldSection) { %>
                                <i class="fa fa-bold fa-2x fa-fw pull-right"></i>
                                <% } %>
                                <% if (uppercaseSection) { %>
                                <i class="fa fa-text-height fa-2x fa-fw pull-right"></i>
                                <% } %>
                                <% if (customSectionFontColor) { %>
                                <i class="fa fa-paint-brush fa-2x fa-fw pull-right"></i>
                                <% } %>
                            </div>
                        </div>
                        <div class="collapse-card__body" style="padding: 1rem 10rem;">
                            <div class="row heading">
                                <div class="col-sm-4 f-field text-left">Font</div>
                                <div class="col-sm-2 f-field text-left">Size</div>
                                <div class="col-sm-2 f-field text-left font-section-color-title">Font Colour</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 f-field">
                                    <select class="form-control" name="fontFamilySECTION">
                                        <%
                                            for (String font : fonts){
                                                String selectedClause = font.equals(company.getString(uada.C1_SECTION_FONT))?"selected":"";
                                        %>
                                        <option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>" <%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
                                        <% } %>
                                    </select>
                                </div>
                                <div class="col-sm-2 f-field">
                                    <input type="text" id="font-size-h1" class="form-control" name="fontSizeSECTION" placeholder="Font Size" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_SECTION_SIZE)) %>">
                                </div>
                                <div class="col-sm-2 f-field fontColorSectionTitleSection">
                                    <input type="text" id="color-header" class="form-control" name="fontColorSectionTitle" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_SECTION_HEADER_COLOR)) %>">
                                </div>
                            </div>
                            <div style="padding-top: .5em;">
                                <section style="padding: 5px 20px;">
                                    <input class="table-checkbox" type="checkbox" name="customSectionColor" value="show" <%=customSectionFontColor?"checked":"" %> /> Custom Font Color
                                </section>
                                <section style="padding: 5px 20px;">
                                    <input class="table-checkbox" type="checkbox" name="uppercaseSectionTitle" value="show" <%=uppercaseSection?"checked":"" %> /> Uppercase Headings
                                </section>
                                <section style="padding: 5px 20px;">
                                    <input class="table-checkbox" type="checkbox" name="underlineSectionTitle" value="show" <%=underlineSection?"checked":"" %> /> Underline Headings
                                </section>
                                <section style="padding: 5px 20px;">
                                    <input class="table-checkbox" type="checkbox" name="boldSectionTitle" value="show" <%=boldSection?"checked":"" %> /> Bold Headings
                                </section>
                            </div>
                        </div>
                    </div>
                    <%--SECTION HEADINGS CARDS END--%>


                    <%--H1 CARDS--%>
                    <div class="collapse-card">
                        <div class="collapse-card__heading">
                            <div class="collapse-card__title">
                                <i class="icon s-4 icon-format-header-1"></i>
                                <span class="styleHeadingTitles">Heading 1</span>
                                <% if (underlineH1) { %>
                                <i class="fa fa-underline fa-2x fa-fw pull-right"></i>
                                <% } %>
                                <% if (boldH1) { %>
                                <i class="fa fa-bold fa-2x fa-fw pull-right"></i>
                                <% } %>
                                <% if (uppercaseH1) { %>
                                <i class="fa fa-text-height fa-2x fa-fw pull-right"></i>
                                <% } %>
                                <% if (customH1FontColor) { %>
                                <i class="fa fa-paint-brush fa-2x fa-fw pull-right"></i>
                                <% } %>
                            </div>
                        </div>
                        <div class="collapse-card__body" style="padding: 1rem 10rem;">
                            <div class="row heading">
                                <div class="col-sm-4 f-field text-left">Font</div>
                                <div class="col-sm-2 f-field text-left">Size</div>
                                <div class="col-sm-2 f-field text-left font-section-color-title-h1">Font Colour</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 f-field">
                                    <select class="form-control" name="fontFamilyH1">
                                        <%
                                            for (String font : fonts){
                                                String selectedClause = font.equals(company.getString(uada.C1_H1_FONT))?"selected":"";
                                        %>
                                        <option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>" <%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
                                        <% } %>
                                    </select>
                                </div>
                                <div class="col-sm-2 f-field">
                                    <input type="text" id="font-size-h1" class="form-control" name="fontSizeH1" placeholder="Font Size" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_H1_SIZE)) %>">
                                </div>
                                <div class="col-sm-2 f-field fontColorHeading1">
                                    <input type="text" id="color-header1" class="form-control" name="fontColorHeading1" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_H1_HEADER_COLOR)) %>">
                                </div>
                            </div>
                            <div style="padding-top: .5em;">
                                <section style="padding: 5px 20px;">
                                    <input class="table-checkbox" type="checkbox" name="customHeading1Color" value="show" <%=customH1FontColor?"checked":"" %> /> Custom Font Color
                                </section>
                                <section style="padding: 5px 20px;">
                                    <input class="table-checkbox" type="checkbox" name="uppercaseHeading1" value="show" <%=uppercaseH1?"checked":"" %> /> Uppercase Headings
                                </section>
                                <section style="padding: 5px 20px;">
                                    <input class="table-checkbox" type="checkbox" name="underlineHeading1" value="show" <%=underlineH1?"checked":"" %> /> Underline Headings
                                </section>
                                <section style="padding: 5px 20px;">
                                    <input class="table-checkbox" type="checkbox" name="boldHeading1" value="show" <%=boldH1?"checked":"" %> /> Bold Headings
                                </section>
                            </div>
                        </div>
                    </div>
                    <%--H1 CARDS END--%>


                    <%--H2 CARDS--%>
                    <div class="collapse-card">
                        <div class="collapse-card__heading">
                            <div class="collapse-card__title">
                                <i class="icon s-4 icon-format-header-2"></i>
                                <span class="styleHeadingTitles">Heading 2</span>
                                <% if (underlineH2) { %>
                                <i class="fa fa-underline fa-2x fa-fw pull-right"></i>
                                <% } %>
                                <% if (boldH2) { %>
                                <i class="fa fa-bold fa-2x fa-fw pull-right"></i>
                                <% } %>
                                <% if (uppercaseH2) { %>
                                <i class="fa fa-text-height fa-2x fa-fw pull-right"></i>
                                <% } %>
                                <% if (customH2FontColor) { %>
                                <i class="fa fa-paint-brush fa-2x fa-fw pull-right"></i>
                                <% } %>
                            </div>
                        </div>
                        <div class="collapse-card__body" style="padding: 1rem 10rem;">
                            <div class="row heading">
                                <div class="col-sm-4 f-field text-left">Font</div>
                                <div class="col-sm-2 f-field text-left">Size</div>
                                <div class="col-sm-2 f-field text-left font-section-color-title-h2">Font Colour</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 f-field">
                                    <select class="form-control" name="fontFamilyH2">
                                        <%
                                            for (String font : fonts){
                                                String selectedClause = font.equals(company.getString(uada.C1_H2_FONT))?"selected":"";
                                        %>
                                        <option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>" <%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
                                        <% } %>
                                    </select>
                                </div>
                                <div class="col-sm-2 f-field">
                                    <input type="text" id="font-size-h2" class="form-control" name="fontSizeH2" placeholder="Font Size" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_H2_SIZE)) %>">
                                </div>
                                <div class="col-sm-2 f-field fontColorHeading2">
                                    <input type="text" id="color-header2" class="form-control" name="fontColorHeading2" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_H2_HEADER_COLOR)) %>">
                                </div>
                            </div>
                            <div style="padding-top: .5em;">
                                <section style="padding: 5px 20px;">
                                    <input class="table-checkbox" type="checkbox" name="customHeading2Color" value="show" <%=customH2FontColor?"checked":"" %> /> Custom Font Color
                                </section>
                                <section style="padding: 5px 20px;">
                                    <input class="table-checkbox" type="checkbox" name="uppercaseHeading2" value="show" <%=uppercaseH2?"checked":"" %> /> Uppercase Headings
                                </section>
                                <section style="padding: 5px 20px;">
                                    <input class="table-checkbox" type="checkbox" name="underlineHeading2" value="show" <%=underlineH2?"checked":"" %> /> Underline Headings
                                </section>
                                <section style="padding: 5px 20px;">
                                    <input class="table-checkbox" type="checkbox" name="boldHeading2" value="show" <%=boldH2?"checked":"" %> /> Bold Headings
                                </section>
                            </div>
                        </div>
                    </div>
                    <%--H2 CARDS END--%>


                    <%--H3 CARDS--%>
                    <div class="collapse-card">
                        <div class="collapse-card__heading">
                            <div class="collapse-card__title">
                                <i class="icon s-4 icon-format-header-3"></i>
                                <span class="styleHeadingTitles">Heading 3</span>
                                <% if (underlineH3) { %>
                                <i class="fa fa-underline fa-2x fa-fw pull-right"></i>
                                <% } %>
                                <% if (boldH3) { %>
                                <i class="fa fa-bold fa-2x fa-fw pull-right"></i>
                                <% } %>
                                <% if (uppercaseH3) { %>
                                <i class="fa fa-text-height fa-2x fa-fw pull-right"></i>
                                <% } %>
                                <% if (customH3FontColor) { %>
                                <i class="fa fa-paint-brush fa-2x fa-fw pull-right"></i>
                                <% } %>
                            </div>
                        </div>
                        <div class="collapse-card__body" style="padding: 1rem 10rem;">
                            <div class="row heading">
                                <div class="col-sm-4 f-field text-left">Font</div>
                                <div class="col-sm-2 f-field text-left">Size</div>
                                <div class="col-sm-2 f-field text-left font-section-color-title-h3">Font Colour</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 f-field">
                                    <select class="form-control" name="fontFamilyH3">
                                        <%
                                            for (String font : fonts){
                                                String selectedClause = font.equals(company.getString(uada.C1_H3_FONT))?"selected":"";
                                        %>
                                        <option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>" <%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
                                        <% } %>
                                    </select>
                                </div>
                                <div class="col-sm-2 f-field">
                                    <input type="text" id="font-size-h3" class="form-control" name="fontSizeH3" placeholder="Font Size" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_H3_SIZE)) %>">
                                </div>
                                <div class="col-sm-2 f-field fontColorHeading3">
                                    <input type="text" id="color-header3" class="form-control" name="fontColorHeading3" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_H3_HEADER_COLOR)) %>">
                                </div>
                            </div>
                            <div style="padding-top: .5em;">
                                <section style="padding: 5px 20px;">
                                    <input class="table-checkbox" type="checkbox" name="customHeading3Color" value="show" <%=customH3FontColor?"checked":"" %> /> Custom Font Color
                                </section>
                                <section style="padding: 5px 20px;">
                                    <input class="table-checkbox" type="checkbox" name="uppercaseHeading3" value="show" <%=uppercaseH3?"checked":"" %> /> Uppercase Headings
                                </section>
                                <section style="padding: 5px 20px;">
                                    <input class="table-checkbox" type="checkbox" name="underlineHeading3" value="show" <%=underlineH3?"checked":"" %> /> Underline Headings
                                </section>
                                <section style="padding: 5px 20px;">
                                    <input class="table-checkbox" type="checkbox" name="boldHeading3" value="show" <%=boldH3?"checked":"" %> /> Bold Headings
                                </section>
                            </div>
                        </div>
                    </div>
                    <%--H3 CARDS END--%>

                    <%--BODY CARDS--%>
                    <div class="collapse-card">
                        <div class="collapse-card__heading">
                            <div class="collapse-card__title">
                                <i class="icon s-4 icon-format-align-justify"></i>
                                <span class="styleHeadingTitles">Body</span>
                            </div>
                        </div>
                        <div class="collapse-card__body" style="padding: 1rem 10rem;">
                            <div class="row heading">
                                <div class="col-sm-4 f-field text-left">Font</div>
                                <div class="col-sm-2 f-field text-left">Size</div>
                                <div class="col-sm-2 f-field text-left">Font Colour</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 f-field">
                                    <select class="form-control" name="fontFamilyBody">
                                        <% for (String font : fonts){
                                            String selectedClause = font.equals(company.getString(uada.C1_BODY_FONT))?"selected":"";
                                        %>
                                        <option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>" <%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
                                        <% } %>
                                    </select>
                                </div>
                                <div class="col-sm-2 f-field">
                                    <input type="text" id="font-size-body" class="form-control" name="fontSizeBody" placeholder="Font Size"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_BODY_SIZE)) %>">
                                </div>
                                <div class="col-sm-2 f-field">
                                    <input type="text" id="color-body" class="form-control" name="fontColorBody" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_BODY_COLOR)) %>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--BODY CARDS END--%>
                    <h2 class="text-center">Proposal Page Options</h2>
                    <%--HEADER CARDS--%>
                    <div class="collapse-card">
                        <div class="collapse-card__heading">
                            <div class="collapse-card__title">
                                <i class="fa fa-align-justify fa-2x fa-fw"></i>
                                <span class="styleHeadingTitles">Page Header</span>
                            </div>
                        </div>
                        <div class="collapse-card__body" style="padding: 1rem 10rem;">
                            <div class="row">
                                <section style="padding-right:0px;padding-left: 60px">
                                    <input class="table-checkbox" type="checkbox" name="topHeaderActive" value="show" <%=topHeaderActive?"checked":"" %> /> Show top header all content pages
                                </section>
                            </div>
                            <h3 class="text-center">Header Colours</h3>
                            <div class="row heading">
                                <div class="col-sm-3 text-left"></div>
                                <div class="col-sm-3 f-field text-center">Font Colour</div>
                                <div class="col-sm-3 f-field text-center">Background Colour</div>
                                <div class="col-sm-3 f-field text-left"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 f-field"></div>
                                <div class="col-sm-3 f-field text-center">
                                    <input type="text" id="color-header-font" class="form-control" name="headerColorFont" value="<%=ESAPI.encoder().encodeForHTMLAttribute(topHeaderFontColor) %>">
                                </div>
                                <div class="col-sm-3 f-field text-center">
                                    <input type="text" id="color-header-background" class="form-control" name="headerColorBackground" value="<%=ESAPI.encoder().encodeForHTMLAttribute(topHeaderBackground) %>">
                                </div>
                                <div class="col-sm-3 f-field">
                                </div>
                            </div>
                            </br>
                            <h3 class="text-center">Header Sections</h3>
                            <div class="row heading">
                                <div class="col-sm-1 text-left"></div>
                                <div class="col-sm-3 text-left">Left</div>
                                <div class="col-sm-3 f-field text-left">Center</div>
                                <div class="col-sm-3 f-field text-left">Right</div>
                                <div class="col-sm-1 text-left"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1 f-field">
                                </div>
                                <div class="col-sm-3 f-field">
                                    <select class="form-control" name="topStyleLeft" positionId="Left" dataId="topLeftData" id="topStyleLeft">
                                        <% String topLeft = "";
                                            if (topStyleLeft != null && topStyleLeft.has("option")) {
                                                topLeft = topStyleLeft.getString("option");
                                            }%>
                                        <option value="none" <%=topLeft.equalsIgnoreCase("none") ?"selected":"" %>>Blank</option>
                                        <option value="image" <%=topLeft.equalsIgnoreCase("image") ?"selected":"" %>>Logo</option>
                                        <option value="text" <%=topLeft.equalsIgnoreCase("text") ?"selected":"" %>>Text</option>
                                        <option value="page" <%=topLeft.equalsIgnoreCase("page") ?"selected":"" %>>Page Counter</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 f-field">
                                    <select class="form-control" name="topStyleCenter" positionId="Center" dataId="topCenterData" id="topStyleCenter">
                                        <% String topCenter = "";
                                            if (topStyleCenter != null && topStyleCenter.has("option")) {
                                                topCenter = topStyleCenter.getString("option");
                                            }%>
                                        <option value="none" <%=topCenter.equalsIgnoreCase("none") ?"selected":"" %>>Blank</option>
                                        <option value="text" <%=topCenter.equalsIgnoreCase("text") ?"selected":"" %>>Text</option>
                                        <option value="page" <%=topCenter.equalsIgnoreCase("page") ?"selected":"" %>>Page Counter</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 f-field">
                                    <select class="form-control" name="topStyleRight" positionId="Right" dataId="topRightData" id="topStyleRight">
                                        <% String topRight = "";
                                            if (topStyleRight != null && topStyleRight.has("option")) {
                                                topRight = topStyleRight.getString("option");
                                            }%>
                                        <option value="none" <%=topRight.equalsIgnoreCase("none") ?"selected":"" %>>Blank</option>
                                        <option value="image" <%=topRight.equalsIgnoreCase("image") ?"selected":"" %>>Logo</option>
                                        <option value="text" <%=topRight.equalsIgnoreCase("text") ?"selected":"" %>>Text</option>
                                        <option value="page" <%=topRight.equalsIgnoreCase("page") ?"selected":"" %>>Page Counter</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 f-field">
                                </div>
                            </div>

                            <div class="row" id="topLeftData">
                                <% if (topStyleLeft != null && topStyleLeft.has("data")) {
                                    String topLeftData = topStyleLeft.getString("data");
                                    String topLeftType = topStyleLeft.getString("type");
                                    switch (topLeftType) {
                                        case "image":%>

                                <div class="row">
                                    <section class="logo-selector">
                                        <input class="" id="topLeftData-image-string" data-id="<%= ESAPI.encoder().encodeForHTMLAttribute(topLeftData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(topLeftData)%>" name="topLeftData-image-string" hidden/>
                                        <input class="header-upload" id="topLeftData-image" data-id="<%= Defaults.getInstance().getStoreContext() + ESAPI.encoder().encodeForHTMLAttribute(topLeftData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(topLeftData)%>" name="topLeftData-image" type="file" />
                                    </section>
                                    <input id="removeheaderImage" type="checkbox" name="removeheaderImage" class="hidden">
                                </div>
                                <%break;
                                    case "text": %>
                                <div class="col-sm-1 f-field"></div>
                                <div class="col-sm-10 f-field"><h5>Left Text</h5>
                                    <input type="text" id="topLeftData-text" class="form-control" name="topLeftData-text" placeholder="Content text"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(topLeftData)%>"></div>
                                <div class="col-sm-1 f-field"></div>
                                <% break;
                                    default:
                                        break;
                                }
                                }%>
                            </div>
                            <div class="row" id="topCenterData">
                                <% if (topStyleCenter != null && topStyleCenter.has("data")) {
                                    String topCenterData = topStyleCenter.getString("data");
                                    String topCenterType = topStyleCenter.getString("type");
                                    switch (topCenterType) {
                                        case "image":%>

                                <div class="row">
                                    <section class="logo-selector">
                                        <input class="" id="topCenterData-image-string" data-id="<%= ESAPI.encoder().encodeForHTMLAttribute(topCenterData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(topCenterData)%>" name="topCenterData-image-string" hidden/>
                                        <input class="header-upload" id="topCenterData-image" data-id="<%= Defaults.getInstance().getStoreContext() + ESAPI.encoder().encodeForHTMLAttribute(topCenterData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(topCenterData)%>" name="topCenterData-image" type="file" />
                                    </section>
                                    <input id="removeheaderImage" type="checkbox" name="removeheaderImage" class="hidden">
                                </div>
                                <%
                                        break;
                                    case "text": %>
                                <div class="col-sm-1 f-field"></div>
                                <div class="col-sm-10 f-field"><h5>Center Text</h5>
                                    <input type="text" id="topCenterData-text" class="form-control"  name="topCenterData-text" placeholder="Content text"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(topCenterData)%>"></div>
                                <div class="col-sm-1 f-field"></div>
                                <% break;
                                    default:
                                        break;
                                }
                                }%>
                            </div>
                            <div class="row" id="topRightData">
                                <% if (topStyleRight != null && topStyleRight.has("data")) {
                                    String topRightData = topStyleRight.getString("data");
                                    String topBottomType = topStyleRight.getString("type");
                                    switch (topBottomType) {
                                        case "image":%>

                                <div class="row">
                                    <section class="logo-selector">
                                        <input class="" id="topRightData-image-string" data-id="<%= ESAPI.encoder().encodeForHTMLAttribute(topRightData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(topRightData)%>" name="topRightData-image-string" hidden/>
                                        <input class="header-upload" id="topRightData-image" data-id="<%= Defaults.getInstance().getStoreContext() + ESAPI.encoder().encodeForHTMLAttribute(topRightData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(topRightData)%>" name="topRightData-image" type="file" />
                                    </section>
                                    <input id="removeheaderImage" type="checkbox" name="removeheaderImage" class="hidden">
                                </div>
                                <%
                                        break;
                                    case "text": %>
                                <div class="col-sm-1 f-field"></div>
                                <div class="col-sm-10 f-field"><h5>Right Text</h5>
                                    <input type="text" id="topRightData-text" class="form-control" name="topRightData-text" placeholder="Content text"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(topRightData)%>"></div>
                                <div class="col-sm-1 f-field"></div>
                                <% break;
                                    default:
                                        break;
                                }
                                }%>
                            </div>
                        </div>
                    </div>
                    <%--HEADER CARDS END--%>

                    <%--FOOTER CARDS--%>
                    <div class="collapse-card">
                        <div class="collapse-card__heading">
                            <div class="collapse-card__title">
                                <i class="fa fa-align-justify fa-2x fa-fw"></i>
                                <span class="styleHeadingTitles">Page Footer</span>
                            </div>
                        </div>
                        <div class="collapse-card__body" style="padding: 1rem 10rem;">
                            <div class="row">
                                <section style="padding-right:0px;padding-left: 60px">
                                    <input class="table-checkbox" type="checkbox" name="bottomHeaderActive" value="show" <%=bottomHeaderActive?"checked":"" %> /> Show bottom footer all content pages
                                </section>
                            </div>
                            <h3 class="text-center">Footer Colours</h3>
                            <div class="row heading">
                                <div class="col-sm-3 text-left"></div>
                                <div class="col-sm-3 f-field text-center">Font Colour</div>
                                <div class="col-sm-3 f-field text-center">Background Colour</div>
                                <div class="col-sm-3 f-field text-left"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 f-field"></div>
                                <div class="col-sm-3 f-field text-center">
                                    <input type="text" id="color-footer-font" class="form-control" name="footerColorFont" value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomHeaderFontColor) %>">
                                </div>
                                <div class="col-sm-3 f-field text-center">
                                    <input type="text" id="color-footer-background" class="form-control" name="footerColorBackground" value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomHeaderBackground) %>">
                                </div>
                                <div class="col-sm-3 f-field">
                                </div>
                            </div>
                            </br>
                            <h3 class="text-center">Footer Sections</h3>
                            <div class="row heading">
                                <div class="col-sm-1 text-left"></div>
                                <div class="col-sm-3 text-left">Left</div>
                                <div class="col-sm-3 f-field text-left">Center</div>
                                <div class="col-sm-3 f-field text-left">Right</div>
                                <div class="col-sm-1 text-left"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1 f-field">
                                </div>
                                <div class="col-sm-3 f-field">
                                    <select class="form-control" name="bottomStyleLeft" dataId="bottomLeftData" positionId="Left" id="bottomStyleLeft">
                                        <% String bottomLeft = "";
                                            if (bottomStyleLeft != null && bottomStyleLeft.has("option")) {
                                                bottomLeft = bottomStyleLeft.getString("option");
                                            }%>
                                        <option value="none" <%=bottomLeft.equalsIgnoreCase("none") ?"selected":"" %>>Blank</option>
                                        <option value="image" <%=bottomLeft.equalsIgnoreCase("image") ?"selected":"" %>>Logo</option>
                                        <option value="text" <%=bottomLeft.equalsIgnoreCase("text") ?"selected":"" %>>Text</option>
                                        <option value="page" <%=bottomLeft.equalsIgnoreCase("page") ?"selected":"" %>>Page Counter</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 f-field">
                                    <select class="form-control" name="bottomStyleCenter" dataId="bottomCenterData" positionId="Center" id="bottomStyleCenter">
                                        <% String bottomCenter = "";
                                            if (bottomStyleCenter != null && bottomStyleCenter.has("option")) {
                                                bottomCenter = bottomStyleCenter.getString("option");
                                            }%>
                                        <option value="none" <%=bottomCenter.equalsIgnoreCase("none") ?"selected":"" %>>Blank</option>
                                        <option value="text" <%=bottomCenter.equalsIgnoreCase("text") ?"selected":"" %>>Text</option>
                                        <option value="page" <%=bottomCenter.equalsIgnoreCase("page") ?"selected":"" %>>Page Counter</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 f-field">
                                    <select class="form-control" name="bottomStyleRight" dataId="bottomRightData" positionId="Right" id="bottomStyleRight">
                                        <% String bottomRight = "";
                                            if (bottomStyleRight != null && bottomStyleRight.has("option")) {
                                                bottomRight = bottomStyleRight.getString("option");
                                            }%>
                                        <option value="none" <%=bottomRight.equalsIgnoreCase("none") ?"selected":"" %>>Blank</option>
                                        <option value="image" <%=bottomRight.equalsIgnoreCase("image") ?"selected":"" %>>Logo</option>
                                        <option value="text" <%=bottomRight.equalsIgnoreCase("text") ?"selected":"" %>>Text</option>
                                        <option value="page" <%=bottomRight.equalsIgnoreCase("page") ?"selected":"" %>>Page Counter</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 f-field">
                                </div>
                            </div>

                            <div class="row" id="bottomLeftData">
                                <%  if (bottomStyleLeft != null && bottomStyleLeft.has("data")) {
                                    String bottomLeftData = bottomStyleLeft.getString("data");
                                    String bottomLeftType = bottomStyleLeft.getString("type");
                                    switch (bottomLeftType) {
                                        case "image":%>
                                <div class="row">
                                    <section class="logo-selector">
                                        <input class="" id="bottomLeftData-image-string" data-id="<%= ESAPI.encoder().encodeForHTMLAttribute(bottomLeftData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomLeftData)%>" name="bottomLeftData-image-string" hidden/>
                                        <input class="header-upload" id="bottomLeftData-image" data-id="<%= Defaults.getInstance().getStoreContext() + ESAPI.encoder().encodeForHTMLAttribute(bottomLeftData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomLeftData)%>"  name="bottomLeftData-image" type="file" />
                                    </section>
                                    <input id="removeheaderImage" type="checkbox" name="removeheaderImage" class="hidden">
                                </div>
                                <%
                                        break;
                                    case "text": %>
                                <div class="col-sm-1 f-field"></div>
                                <div class="col-sm-10 f-field"><h5>Left Text</h5>
                                    <input type="text" id="bottomLeftData-text" class="form-control" name="bottomLeftData-text" placeholder="Content text"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomLeftData)%>"></div>
                                <div class="col-sm-1 f-field"></div>
                                <% break;
                                    default:
                                        break;
                                }
                                }%>
                            </div>
                            <div class="row" id="bottomCenterData">
                                <% if (bottomStyleCenter != null && bottomStyleCenter.has("data")) {
                                    String bottomCenterData = bottomStyleCenter.getString("data");
                                    String bottomCenterType = bottomStyleCenter.getString("type");
                                    switch (bottomCenterType) {
                                        case "image":%>

                                <div class="row">
                                    <section class="logo-selector">
                                        <input class="" id="bottomCenterData-image-string" data-id="<%= ESAPI.encoder().encodeForHTMLAttribute(bottomCenterData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomCenterData)%>" name="bottomCenterData-image-string" hidden/>
                                        <input class="header-upload" id="bottomCenterData-image" data-id="<%= Defaults.getInstance().getStoreContext() + ESAPI.encoder().encodeForHTMLAttribute(bottomCenterData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomCenterData)%>" name="bottomCenterData-image" type="file" />
                                    </section>
                                    <input id="removeheaderImage" type="checkbox" name="removeheaderImage" class="hidden">
                                </div>
                                <%
                                        break;
                                    case "text": %>
                                <div class="col-sm-1 f-field"></div>
                                <div class="col-sm-10 f-field"><h5>Center Text</h5>
                                    <input type="text" id="bottomCenterData-text" class="form-control" name="bottomCenterData-text" placeholder="Content text"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomCenterData)%>"></div>
                                <div class="col-sm-1 f-field"></div>
                                <% break;
                                    default:
                                        break;
                                }
                                }%>
                            </div>
                            <div class="row" id="bottomRightData">
                                <% if (bottomStyleRight != null && bottomStyleRight.has("data")) {
                                    String bottomRightData = bottomStyleRight.getString("data");
                                    String bottomRightType = bottomStyleRight.getString("type");
                                    switch (bottomRightType) {
                                        case "image":%>

                                <div class="row">
                                    <section class="logo-selector">
                                        <input class="" id="bottomRightData-image-string" data-id="<%= ESAPI.encoder().encodeForHTMLAttribute(bottomRightData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomRightData)%>" name="bottomRightData-image-string" hidden/>
                                        <input class="header-upload" id="bottomRightData-image" data-id="<%= Defaults.getInstance().getStoreContext() + ESAPI.encoder().encodeForHTMLAttribute(bottomRightData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomRightData)%>" name="bottomRightData-image" type="file" />
                                    </section>
                                    <input id="removeheaderImage" type="checkbox" name="removeheaderImage" class="hidden">
                                </div>
                                <%
                                        break;
                                    case "text": %>
                                <div class="col-sm-1 f-field"></div>
                                <div class="col-sm-10 f-field"><h5>Right Text</h5>
                                    <input type="text" id="bottomRightData-text" class="form-control" name="bottomRightData-text" placeholder="Content text"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomRightData)%>"></div>
                                <div class="col-sm-1 f-field"></div>
                                <% break;
                                    default:
                                        break;
                                }
                                }%>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="tab-pane fade p-3" id="colours-tab-pane" role="tabpanel" aria-labelledby="colours-tab">
        <div class="col-12 p-3">

            <div class="widget widget6 card">

                <div class="widget-content p-4">
                    <div class="row">
                        <div class="col col-6">
                            <h2 class="text-center">Palettes</h2>
                            <div class="row" style="text-align: center;" id="edit-palette">
                                <section class="col col-3">
                                    <label class="input"> <input id="palette-1"
                                                                 class="form-control color {hash:true}" name="palette1"
                                                                 placeholder="Hex Colour"
                                                                 value="<%=ESAPI.encoder().encodeForHTMLAttribute(companyStyles.getString(tds.C_PALETTE_1))%>">
                                    </label>
                                </section>
                                <section class="col col-3">
                                    <label class="input"> <input id="palette-2"
                                                                 class="form-control color {hash:true}" name="palette2"
                                                                 placeholder="Hex Colour"
                                                                 value="<%=ESAPI.encoder().encodeForHTMLAttribute(companyStyles.getString(tds.C_PALETTE_2))%>">
                                    </label>
                                </section>
                                <section class="col col-3">
                                    <label class="input"> <input id="palette-3"
                                                                 class="form-control color {hash:true}" name="palette3"
                                                                 placeholder="Hex Colour"
                                                                 value="<%=ESAPI.encoder().encodeForHTMLAttribute(companyStyles.getString(tds.C_PALETTE_3))%>">
                                    </label>
                                </section>
                                <section class="col col-3">
                                    <label class="input"> <input id="palette-4"
                                                                 class="form-control color {hash:true}" name="palette4"
                                                                 placeholder="Hex Colour"
                                                                 value="<%=ESAPI.encoder().encodeForHTMLAttribute(companyStyles.getString(tds.C_PALETTE_4))%>">
                                    </label>
                                </section>
                            </div>
                        </div>
                        <div class="col col-6">
                            <h2 class="text-center">Background Colour</h2>
                            <div class="row" style="text-align: center;">
                                <section class="text-center background-selector" style="width: 100%;">
                                    <label class="input"> <input id="background-color"
                                                                 class="form-control color {hash:true}"
                                                                 name="backgroundColour" placeholder="Hex Colour"
                                                                 value="<%=ESAPI.encoder().encodeForHTMLAttribute(companyStyles.getString(tds.C_BACKGROUND_COLOUR))%>">
                                    </label>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div class="h3 hidden">Section Background</div>
                    <div class="row hidden">
                        <div class="col col-12">
                            <section class="background-selector">
                                <label class="input">
                                    <input id="background-color" class="form-control color {hash:true}" name="sectionBackgroundColour" placeholder="Hex Colour"
                                           value="<%=ESAPI.encoder().encodeForHTMLAttribute(companyStyles.getString(tds.C_SECTION_BACKGROUND_COLOUR)) %>">
                                </label>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="/application/assets/plugins/iCheck/jquery.icheck.min.js"></script>
<link rel='stylesheet' href='/application/assets/plugins/spectrum/spectrum.css' />
<script src='/application/assets/plugins/spectrum/spectrum.js'></script>
<script src='/application/assets/plugins/paper-collapse/paper-collapse.min.js'></script>
<script src="/application/js/form-validation/modal-edit-styles.js"></script>

<script>
	$(function () {
		$(".collapse-card").paperCollapse();
	});

</script>