<!DOCTYPE html>
<!--[if lt IE 9]><html class="bad-ie" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.OptionsDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.StatusReason"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TDiscountsDataAccess"%>
<%@page import="java.util.List"%>
<%@page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions"%>
<%@page import="java.util.Locale"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.DateFormatUtils"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.SecurityKeyService"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="java.util.Hashtable"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess" %>
<html lang="en">
<!--<![endif]-->
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
<head>

<%
UserAccountDataAccess uada = new UserAccountDataAccess();
TravelDocsDataAccess pda = new TravelDocsDataAccess();

String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
String userUsername = (String)session.getAttribute(Constants.PORTAL_USERNAME_ATTR);
CategoryData company = uada.getUserCompanyForUserId(userId);
String companyId = company.getId();
String corporateCSS = String.format("/%s/%s/Categories/%s/gen/css-document/dashboard-colours.css",
		Defaults.getInstance().getStoreContext(),
		UserAccountDataAccess.USER_MODULE,
		companyId);
Hashtable<String, String> userDetails = uada.getUserWithId(userId);

TUserRolesDataAccess urda = new TUserRolesDataAccess();
UserPermissions up = urda.getPermissions(userId);
OptionsDataAccess oda = new OptionsDataAccess();
List<String> allLostReasons = oda.getLostReasons();
StatusReason[] userReasons = new TDiscountsDataAccess().getStatusReasons(companyId);
for(StatusReason rs : userReasons){
	allLostReasons.add(rs.getReason());
}
boolean viewTeams = up.hasManageUsersPermission();
%>
<title>Analytics</title>

<jsp:include page="/application/include/common-head.jsp"></jsp:include>

<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
<link rel="stylesheet" type="text/css" media="screen" href="/proposals/css/smartadmin-production-plugins.css">
<link rel="stylesheet" type="text/css" media="screen" href="/proposals/css/smartadmin-production.css">
<link rel="stylesheet" type="text/css" media="screen" href="/proposals/css/smartadmin-skins.css">
<link rel="stylesheet" href="/application/assets/plugins/bootstrap-select/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="/application/assets/plugins/rangeslider/rangeslider.css" />
<link rel="stylesheet" href="/application/css/dashboard.css" type="text/css"/>
<link rel="stylesheet" href="/application/css/g9xne2ekdpp46n84m4.css" type="text/css"/>


	<link rel="shortcut icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="<%=corporateCSS%>" />

<jsp:include page="ie8StdHeaders.jsp"></jsp:include>

<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
		
<script>
var dateFormatPattern = "<%= DateFormatUtils.getInstance(company.getString(UserAccountDataAccess.C1_DATE_FORMAT), Locale.forLanguageTag(company.getString(UserAccountDataAccess.C1_LOCALE))).getJsPattern() %>";
var lostReasons = [
	<%
	for(String r : allLostReasons){
	%>
	'<%= r %>',
	<%
	}
	%>
];
</script>
</head>

<body class="">
  <div class="blurred">
      <!--  Notification Area -->
      <div class='notificationMessage' style='display:none'></div>
  <!-- HEADER -->
		<header id="header">
			<div id="logo-group">

				<span id="logo"> <img src="/application/images/qCloud.png" alt="QuoteCloud"> </span>
							
			</div>
			
			<jsp:include page="include/i_navbar.jsp" />
			
			<!-- pulled right: nav area -->
			<div class="pull-right">

				<!-- logout button -->
				 <div id="logout" class="btn-header transparent pull-right">
					<span> <a href="/api/free/logout" title="Sign Out" data-action="userLogout" data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->

				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a  data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->

			</div>
			<!-- end pulled right: nav area -->
			
		</header>
		<!-- END HEADER -->
		
		<div id="body">
			<div role="main">
				
				<div id="content">
					<div class="container contentContainer">
						<div class="row">
							<div class="col-sm-12">
								<p><i>*If proposals are deleted from Quotecloud, the data for those documents is not shown in these charts.</i></p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 col-lg-6">
								<div class="panel analytics-panel" id="proposalsBy" data-type="countProposals">
									<div class="panel-body panel-blue">
										<h1>Proposals</h1>
										
							  			<canvas></canvas>
							  			
									</div>
									<div class="panel-body">
										<ul class="nav nav-pills" role="tablist">
									    	<li role="presentation" class="ripple chart-filter" data-show="week"><a >Week</a></li>
									    	<li role="presentation" class="ripple chart-filter active" data-show="month"><a >Month</a></li>
									    	<li role="presentation" class="ripple chart-filter" data-show="quarter"><a >Quarter</a></li>
									    	<li role="presentation" class="chart-filter" data-show="day"><label class="ripple">Custom: <input type="text" id="dateRange" style="padding:4px;"/></label></li>
									  	</ul>
									  	<% if(viewTeams){ %>
									  	<select class="selectpicker" name="selectTeam" multiple title="Choose the teams">
										  <%
										  List<CategoryData> teams = uada.getCompanyTeams(companyId);
										  for(CategoryData team : teams){
											  %>
											  <option value="<%=team.getId() %>"><%=team.getName() %></option>
											  <%
										  }
										  %>
										</select>
										<%} %>
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-lg-6">
								<div class="panel analytics-panel" id="revenue" data-type="revenue">
									<div class="panel-body panel-green">
										<h1>Revenue <small>*accepted proposals</small></h1>
										
										<canvas></canvas>
							  			
									</div>
									<div class="panel-body">
										<ul class="nav nav-pills" role="tablist">
									    	<li role="presentation" class="ripple chart-filter" data-show="week"><a >Week</a></li>
									    	<li role="presentation" class="ripple chart-filter active" data-show="month"><a >Month</a></li>
									    	<li role="presentation" class="ripple chart-filter" data-show="quarter"><a >Quarter</a></li>
									    	<li role="presentation" class="chart-filter" data-show="day"><label class="ripple">Custom: <input type="text" id="dateRange" style="padding:4px;"/></label></li>
									  	</ul>
									  	<% if(viewTeams){ %>
									  	<select class="selectpicker" name="selectTeam" multiple title="Choose the teams">
										  <%
										  List<CategoryData> teams = uada.getCompanyTeams(companyId);
										  for(CategoryData team : teams){
											  %>
											  <option value="<%=team.getId() %>"><%=team.getName() %></option>
											  <%
										  }
										  %>
										</select>
										<%} %>
									</div>
									<table class="table">
									</table>
								</div>
							</div>
							<div class="col-sm-12 col-lg-6">
								<div class="panel analytics-panel" id="reasonsLost" data-type="reasonsLost">
									<div class="panel-body panel-red">
										<h1>Reasons lost </h1>
										
										<canvas></canvas>
							  			
									</div>
									<div class="panel-body">
										<ul class="nav nav-pills" role="tablist">
									    	<li role="presentation" class="ripple chart-filter" data-show="week"><a >Week</a></li>
									    	<li role="presentation" class="ripple chart-filter active" data-show="month"><a >Month</a></li>
									    	<li role="presentation" class="ripple chart-filter" data-show="quarter"><a >Quarter</a></li>
									    	<li role="presentation" class="chart-filter" data-show="day"><label class="ripple">Custom: <input type="text" id="dateRange" style="padding:4px;"/></label></li>
									  	</ul>
									  	<% if(viewTeams){ %>
									  	<select class="selectpicker" name="selectTeam" multiple title="Choose the teams">
										  <%
										  List<CategoryData> teams = uada.getCompanyTeams(companyId);
										  for(CategoryData team : teams){
											  %>
											  <option value="<%=team.getId() %>"><%=team.getName() %></option>
											  <%
										  }
										  %>
										</select>
										<%} %>
									</div>
									<table class="table">
									</table>
								</div>
							</div>
							<div class="col-sm-12 col-lg-6">
								<div class="panel analytics-panel" id="lostRevenue" data-type="lostRevenue">
									<div class="panel-body panel-red-light">
										<h1>Lost Revenue </h1>
										
										<canvas></canvas>
							  			
									</div>
									<div class="panel-body">
										<ul class="nav nav-pills" role="tablist">
									    	<li role="presentation" class="ripple chart-filter" data-show="week"><a >Week</a></li>
									    	<li role="presentation" class="ripple chart-filter active" data-show="month"><a >Month</a></li>
									    	<li role="presentation" class="ripple chart-filter" data-show="quarter"><a >Quarter</a></li>
									    	<li role="presentation" class="chart-filter" data-show="day"><label class="ripple">Custom: <input type="text" id="dateRange" style="padding:4px;"/></label></li>
									  	</ul>
									  	<% if(viewTeams){ %>
									  	<select class="selectpicker" name="selectTeam" multiple title="Choose the teams">
										  <%
										  List<CategoryData> teams = uada.getCompanyTeams(companyId);
										  for(CategoryData team : teams){
											  %>
											  <option value="<%=team.getId() %>"><%=team.getName() %></option>
											  <%
										  }
										  %>
										</select>
										<%} %>
									</div>
									<table class="table">
									</table>
								</div>
							</div>
							<div class="col-sm-12 col-lg-6">
								<div class="panel analytics-panel" id="reasonsLostPie" data-type="reasonsLostPie">
									<div class="panel-body">
										<h1>Reasons lost </h1>
										
										<canvas></canvas>
							  			
									</div>
									<div class="panel-body">
									  	<% if(viewTeams){ %>
									  	<select class="selectpicker" name="selectTeam" multiple title="Choose the teams">
										  <%
										  List<CategoryData> teams = uada.getCompanyTeams(companyId);
										  for(CategoryData team : teams){
											  %>
											  <option value="<%=team.getId() %>"><%=team.getName() %></option>
											  <%
										  }
										  %>
										</select>
										<%} %>
									</div>
									<table class="table">
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  <!-- start: BOOTSTRAP EXTENDED MODALS -->
  <div id="proposal-notes-modal" class="modal fade" tabindex="-1"></div>

  <jsp:include page="dialog/backgroundLoading.jsp"></jsp:include>
  
  <!-- end: BOOTSTRAP EXTENDED MODALS -->


  <!-- Bootstrap core JavaScript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="/application/js/log4javascript.js"></script>
  <script src="/application/js/logging.js"></script>
  <script src="/application/js/utils.js"></script>

  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="/application/assets/plugins/jquery-placeholder/js/jquery.placeholder.js"></script>
  <script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
  <script src="/application/assets/plugins/bootstrap-qcloud/js/bootstrap.min.js"></script>
  <script src="/application/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
  <script src="/application/assets/plugins/blockUI/jquery.blockUI.js"></script>
  <script src="/application/assets/plugins/iCheck/jquery.icheck.min.js"></script>
  <script src="/application/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
  <script src="/application/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
  <script src="/application/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
  <script src="/application/assets/plugins/jquery.fileDownload/jquery.fileDownload.js"></script>
  <script src="/application/assets/plugins/jquery-fixedheadertable/jquery.fixedheadertable.js"></script>
  <script src="/application/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script src="/application/assets/plugins/bootstrap-datepicker/js/date.format.js"></script>
  <script src="/application/assets/plugins/jquery.form/jquery.form.js"></script>
  <script src="/application/assets/plugins/moment/moment.js"></script>
  <script src="/application/assets/plugins/sweetalert/sweetalert.min.js"></script>
  <script src="/application/js/screenCheck.js"></script>

  <script src="/application/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script> <!-- min version may cause performance issue in Firefox, but working fine in IE and Chrome -->
  <script src="/application/assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
  <script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
  <script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
  <script src="/application/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"></script>
  <script src="/application/assets/js/ui-modals.js"></script>
  <script src="/application/assets/plugins/ckeditor/ckeditor.js"></script>
  <script src="/application/assets/plugins/ckeditor/adapters/jquery.js"></script>
  <script src="/application/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
  <script src="/application/assets/plugins/rangeslider/rangeslider.min.js"></script>
  <script src="/application/assets/plugins/animate-css/animateCss.js"></script>
  <script src="/application/assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
  <script src="/application/assets/plugins/Chart.js/Chart.min.js"></script>
  <script src="/application/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
  
  <script src="/application/js/dialogs.js"></script>
  <script src="/application/assets/plugins/randomColor/randomColor.js"></script>
  <script src="/application/js/g9xne2ekdpp46n84m4.js"></script>
  <script src="/application/js/admin.js"></script>
  <script src="/application/js/loading.js"></script>

  <jsp:include page="ga-tracking.jsp"></jsp:include>
</body>
</compress:html>
</html>