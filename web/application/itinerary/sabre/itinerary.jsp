<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@page import="au.corporateinteractive.qcloud.market.model.travel.TravellerStyleModel.FlightSegmentModel"%>
<%@page import="au.corporateinteractive.qcloud.market.model.travel.TravellerStyleModel"%>
<%@page
	import="au.corporateinteractive.qcloud.market.model.travel.core.FaresCI"%>
<%@page
	import="au.corporateinteractive.qcloud.market.model.travel.core.SegmentCI"%>
<%@page
	import="au.corporateinteractive.qcloud.market.model.travel.core.SegmentsCI"%>
<%@page
	import="au.corporateinteractive.qcloud.market.model.travel.core.ItineraryItemCI"%>
<%@page
	import="au.corporateinteractive.qcloud.market.model.travel.core.ItineraryCI"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"%>
<%@page
	import="au.corporateinteractive.qcloud.market.model.travel.core.BookingCI"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Section"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.db.ItineraryDataAccess"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@ page import="au.corporateinteractive.qcloud.market.utils.Constant" %>
<%@ page import="org.joda.time.Days" %>
<%@ page import="java.time.temporal.ChronoUnit" %>
<%@ page import="java.util.concurrent.TimeUnit" %>
<%@ page import="java.util.*" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess" %>

<%
	Hashtable<String, String> proposalHashtable = (Hashtable<String, String>) request.getAttribute("proposalHashtable");

	if (proposalHashtable == null)
	{
		String proposalId = (String) request.getAttribute("proposal");
		proposalHashtable = new TravelDocsDataAccess().getTravelDocById(proposalId);
	}
	TravelDocsDataAccess.addFormattedDates(proposalHashtable);
	String companyID = proposalHashtable.get(TravelDocsDataAccess.E_OWNER);
	UserAccountDataAccess uada = new UserAccountDataAccess();
	CategoryData companyData = uada.getCompanyForCompanyId(companyID);
	String companyDateFormat = companyData.getString("ATTR_DateFormat");
	SimpleDateFormat sdf = new SimpleDateFormat(companyDateFormat + " - HH:mm");
	SimpleDateFormat sdfDateOnly = new SimpleDateFormat(companyDateFormat);
	SimpleDateFormat sdfTimeOnly = new SimpleDateFormat("hh:mm");
	Proposal proposal = new TravelDocsDataAccess().loadTravelDoc(proposalHashtable.get(TravelDocsDataAccess.E_ID));
	
	String showRemarksAtt = (String) request.getAttribute("showRemarks");
	boolean showRemarks;
	if(showRemarksAtt == null)
	{
		Content conInfo = proposal.findGeneratedContentOfType("con-info");
		showRemarks = StringUtils.equalsIgnoreCase("on", conInfo.getMetadata().get("showRemarks"));
	}
	else {
		showRemarks = StringUtils.equalsIgnoreCase("true", showRemarksAtt);
	}
	
	BookingCI booking = proposal.getBooking();
	boolean pdfMode = "pdf".equals(request.getAttribute("proposalMode"));
	String itineraryStyleId = (String) request.getAttribute("itiStyle");

	ItineraryDataAccess ida = new ItineraryDataAccess();

	if (itineraryStyleId == null)
	{
		for (Section section : proposal.getSections())
		{
			if ("itinerary".equals(section.getType()))
			{
				for (Content content : section.getContent())
				{
					if ("con-iti".equals(content.getId()))
					{
						String style = content.getMetadata().get("style");
						itineraryStyleId = style;
						break;
					}
				}
				break;
			}
		}
	}
	if (pdfMode)
	{
		//THIS IS DANGEROUS -- FOR TESTING ONLY...
		itineraryStyleId = "1";
	}
	ElementData currentStyle = ida.getItineraryStyleElement(itineraryStyleId);

	Boolean preview = (Boolean) request.getAttribute("preview");
	if (preview == null)
	{
		preview = false;
	}
%>
<div class="panel" type="generated" dataId="con-iti" fixed="true">
	<%
		if (!pdfMode)
		{
	%>
	<div class="panel-heading">
		<i class="fa fa-ticket"></i> <span>Itinerary</span>
		<div class="panel-tools">
			<div class="iti-style-tool pageBreakPanel" style="">
				<div class="pageBreakPanel">
					<div class="iti-style-lbl">Itinerary Style</div>
				</div>
				<div class="pageBreakPanel">
					<div class="dropdown iti-select" style="max-width: 70px">
						<button class="btn btn-default dropdown-toggle small"
							type="button" data-toggle="dropdown" aria-haspopup="true"
							aria-expanded="true">
							<span style="font-size: 12px;" id="itiStyle"><%=currentStyle.getName()%></span>
							<span class="caret"></span>
						</button>
						<ul id="itiStyleSelect" class="dropdown-menu">
							<%
								List<ElementData> itineraryStyles = new ItineraryDataAccess().getItineraryStyles(companyData.getId());

									for (ElementData itiStyle : itineraryStyles)
									{
							%>
							<li
								class="<%=currentStyle.getId().equals(itiStyle.getId()) ? "selected" : ""%>"
								value="<%=itiStyle.getId()%>"><a><%=itiStyle.getName()%></a></li>
							<%
								}
							%>
						</ul>
					</div>
				</div>
			</div>
			<div class="pageBreakPanel newPDFLabel">
				<div class="pageBreakPanel-label">
					Start on a<span class="pageBreakPanel-label">new page (PDF)</span>
				</div>
			</div>
			<div class="pageBreakPanel sectionControls">
				<input type="checkbox" class="js-switch newPageSwitch" />
				<%
					if (!"false".equals(request.getParameter("movable")))
						{
				%>
				<a href="#" class="btn btn-xs btn-link move-up"> <i
					class="fa fa-chevron-up"></i></a> <a href="#"
					class="btn btn-xs btn-link move-down"> <i
					class="fa fa-chevron-down"></i></a>
				<%
					}
						else
						{
				%>
				<a href="#" class="btn btn-xs btn-link disabled"> <i class="fa"></i></a>
				<a href="#" class="btn btn-xs btn-link disabled"> <i class="fa"></i></a>
				<%
					}
				%>
				<a href="#" class="btn btn-xs btn-link lock"> <i
					class="fa fa-unlock"></i> <span></span></a> <a href="#"
					class="btn btn-xs btn-link disabled"> <i class="fa"></i></a>
			</div>
		</div>
	</div>
	<%
		}
	%>

	<div id="itinerarySection">
		<%
		ElementData data = ModuleAccess.getInstance().getElementById(ItineraryDataAccess.ITINERARY_MODULE, itineraryStyleId);
		if(data.getName().equals("Traveller"))
		{
		%>
			<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/css/market/itinerary/traveller.css", request) %>" type="text/css"/>
			<div class="noBreak">
				<div class="booking-detail"  >
					<div class="traveller" style="text-align: center;">
					<%
						
						TravellerStyleModel travellerModel = TravellerStyleModel.getModel(booking);
						Integer numberOfFlights = 0;
						Integer numberOfHotels = 0;
						Integer numberOfCarHire = 0;
						Integer numberOfCruises = 0;
						Integer numberOfTrains = 0;
						Integer numberOfDays = 0;
						Boolean hasPin = false;
						List<String> listOfCities = new ArrayList<String>();
						Integer numberOfPins = 0;

							for(FlightSegmentModel flightSegmentModel : travellerModel.getFlightSegmentModels()) {
								if(flightSegmentModel.getFlightSegment() != null) {
									numberOfFlights++;
									hasPin = true;
									if (!listOfCities.contains(flightSegmentModel.getFlightSegment().getFlight().getDestinationCity())) {
										listOfCities.add(flightSegmentModel.getFlightSegment().getFlight().getDestinationCity());

										if (flightSegmentModel.getFlightSegment().getFlight().getFlightDuration().getHours() > 12) {
											numberOfDays++;
											hasPin = true;
										}
									}
								}
								for(SegmentCI segments : flightSegmentModel.getSegments()) {
									switch (segments.getType()) {
										case Constant.SEGMENT_HOTEL:
											numberOfHotels++;
											hasPin = true;
											if (!listOfCities.contains(segments.getHotel().getCity().getCityName())) {
												listOfCities.add(segments.getHotel().getCity().getCityName());
												numberOfDays += segments.getHotel().getNumberOfNights();
											}
											break;
										case Constant.SEGMENT_CAR_HIRE:
											numberOfCarHire++;
											hasPin = true;
											break;
										case Constant.SEGMENT_CRUISE:
											numberOfCruises++;
											hasPin = true;
											if (!listOfCities.contains(segments.getCruise().getDestinationCity())) {
												listOfCities.add(segments.getCruise().getDestinationCity());
											}
											break;
										case Constant.SEGMENT_RAIL:
											numberOfTrains++;
											hasPin = true;
											if (!listOfCities.contains(segments.getRail().getDestinationCity())) {
												listOfCities.add(segments.getRail().getDestinationCity());
											}
											break;
										case Constant.SEGMENT_FERRY:
											numberOfTrains++;
											hasPin = true;
											if (!listOfCities.contains(segments.getFerry().getDestinationCity())) {
												listOfCities.add(segments.getFerry().getDestinationCity());
											}
											break;
										default:
											System.out.println("fail");
											break;
									}
								}
							}
							numberOfPins = (numberOfCarHire > 0 ? 1:0) + (numberOfCruises > 0 ? 1:0) + (numberOfHotels > 0 ? 1:0) + (numberOfDays > 0 ? 1:0) + (numberOfFlights > 0 ? 1:0) + (numberOfTrains > 0 ? 1:0) + (listOfCities.size() > 0 ? 1:0);
						%>

						<div class="travelStats" style="width:<%=numberOfPins * 82%>px;<%=hasPin ? "height: 80px;" : "height: 20px;"%>margin: 0px auto 18px;">
						<div class="travelContainer">
							<% if (listOfCities.size() > 0) { %>
							<div class="travelDash">
								<span class="travelDays"><%=listOfCities.size()%></span>
								<span class="travelLabels"><%=listOfCities.size() > 1 ? "Cities" : "City"%></span>
							</div>
							<% } %>
							<% if (numberOfDays > 0) { %>
							<div class="travelDash">
								<span class="travelDays"><%=numberOfDays%></span>
								<span class="travelLabels"><%=numberOfDays > 1 ? "Days" : "Day"%></span>
							</div>
							<% } %>
							<% if (numberOfFlights > 0) { %>
							<div class="travelDash">
								<span class="travelDays"><%=numberOfFlights%></span>
								<span class="travelLabels"><%=numberOfFlights > 1 ? "Flights" : "Flight"%></span>
							</div>
							<% } %>
							<% if (numberOfHotels > 0) { %>
							<div class="travelDash">
								<span class="travelDays"><%=numberOfHotels%></span>
								<span class="travelLabels"><%=numberOfHotels > 1 ? "Hotels" : "Hotel"%></span>
							</div>
							<% } %>
							<% if (numberOfCruises > 0) { %>
							<div class="travelDash">
								<span class="travelDays"><%=numberOfCruises%></span>
								<span class="travelLabels"><%=numberOfCruises > 1 ? "Cruises" : "cruise"%></span>
							</div>
							<% } %>
							<% if (numberOfTrains > 0) { %>
							<div class="travelDash">
								<span class="travelDays"><%=numberOfTrains%></span>
								<span class="travelLabels"><%=numberOfTrains > 1 ? "Trains" : "Train"%></span>
							</div>
							<% } %>
							<% if (numberOfCarHire > 0) { %>
							<div class="travelDash">
								<span class="travelDays"><%=numberOfCarHire%></span>
								<span class="travelLabels"><%=numberOfCarHire > 1 ? "Hired Cars" : "Hired Car"%></span>
							</div>
							<% } %>
						</div>
					</div>

						
					<%	for(FlightSegmentModel flightSegmentModel : travellerModel.getFlightSegmentModels()) {
							if (preview) {

					%>

						<div class="traveller-booking" flight=<%=flightSegmentModel.getFlightNum()%> style="position:relative;width:728px !important;">
						<% } else { %>
							<div class="traveller-booking" style="position:relative;"  flight=<%=flightSegmentModel.getFlightNum()%>>
								<% }
									SegmentCI segmentMain = flightSegmentModel.getFlightSegment();
									segmentMain.setActivityId(Integer.valueOf(booking.getBookingReference()));
								%>

						<%=new ItineraryDataAccess().getItinerarySegment(segmentMain, null, itineraryStyleId, companyDateFormat, preview, showRemarks, pdfMode) %>
							<%--<%--%>
								<%--if (flightSegmentModel.getSegments().size() == 0) {--%>
									<%--SegmentCI segment = new SegmentCI();--%>
									<%--segment.setType("Blank");--%>
							<%--%>--%>
								<%--<div class="tr-itinerary" style="padding: 1px 10px 10px 30px;">--%>

									<%--<%=	new ItineraryDataAccess().getItinerarySegment(segment, null, itineraryStyleId, companyDateFormat, preview, showRemarks, pdfMode)%>--%>
									<%--<div class="tools">--%>
										<%--<div>--%>

										<%--</div>--%>
									<%--</div>--%>
								<%--</div>--%>
								<%--<%}--%><%
								for(SegmentCI segment : flightSegmentModel.getSegments())
								{
							%>
								<div class="tr-itinerary">
									<%=new ItineraryDataAccess().getItinerarySegment(segment, null, itineraryStyleId, companyDateFormat, preview, showRemarks, pdfMode)%>
									<% if (segment.getType().equalsIgnoreCase(Constant.SEGMENT_HOTEL)) { %>
									<div class="tools <%=preview ? "tools-clickable" : ""%>">
										<div>
											<span class="fa fa-map-marker" />
										</div>
									</div>
									<% } %>
								</div>
							<%
								}
							%>
						</div>
					<%	
						}
					
					%>
					</div>
				</div>
			</div>
		<% if (preview) { %>
		<script src="<%=UrlUtils.autocachedUrl("/application/assets/plugins/jquery/1.10.2/jquery.min.js", request) %>"></script>
		<% } %>
		<script src="<%=UrlUtils.autocachedUrl("/application/js/market/itinerary/traveller.js", request) %>"></script>
		<%	
			}
		else
		{
		%>
		<div class="noBreak">
			<div class="booking-detail">
				<%=new ItineraryDataAccess().getBookingDetailSegment(booking, itineraryStyleId, companyDateFormat)%>
			</div>
		</div>
		<div id="itineraryContent">
			<%
				ItineraryCI itinerary = booking.getItinerary();
				List<SegmentCI> mainSegments = new ArrayList<SegmentCI>();


				for (ItineraryItemCI itineraryItem : itinerary.getItineraryItem())
				{
					SegmentsCI segments = itineraryItem.getSegments();
					FaresCI fares = itineraryItem.getFares();
					if (segments != null)
					{

						for (SegmentCI segment : segments.getSegment()) {
							mainSegments.add(segment);
						}
					}
				}

				mainSegments.sort(new Comparator<SegmentCI>()
				{
					public int compare(SegmentCI m1, SegmentCI m2)
					{

						Long m1Time = new ItineraryDataAccess().checkSort(m1);
						Long m2Time = new ItineraryDataAccess().checkSort(m2);
						return m1Time.compareTo(m2Time);
					}
				});

				for (SegmentCI segment : mainSegments)
				{

					String segmentContent = new ItineraryDataAccess().getItinerarySegment(segment, null, itineraryStyleId, companyDateFormat + " hh:mm", preview, showRemarks, pdfMode);
			%>

			<div class="noBreak">
				<div style="clear: both;"></div>
				<div class="segment">
					<%=segmentContent%>
				</div>
			</div>
			<%
				}
			%>
		</div>
		<%	
			}
		
		%>
	</div>
</div>