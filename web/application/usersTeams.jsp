<!DOCTYPE html>
<!--[if lt IE 9]><html class="bad-ie" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.control.StripeController"%>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.LinkedList" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Locale" %>
<%@page import="java.util.Map.Entry" %>
<%@page import="java.util.Set" %>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="com.google.gson.Gson" %>
<%@page import="au.com.ci.sbe.util.UrlUtils" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.StripeService" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.DateFormatUtils" %>
<%@ page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager" %>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%@ page import="au.net.webdirector.common.datalayer.client.ElementData" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.*" %>
<%@ page import="au.corporateinteractive.qcloud.market.enums.Market" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.control.UserController" %>
<html lang="en">
<!--<![endif]-->
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="false" compressJavaScript="false" compressCss="true">
    <head>

        <%


            TUserAccountDataAccess uada = new TUserAccountDataAccess();
            UserAccountDataAccess uda = new UserAccountDataAccess();

            String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
            String userUsername = (String)session.getAttribute(Constants.PORTAL_USERNAME_ATTR);
            CategoryData company = uada.getUserCompanyForUserId(userId);
//            uada.updateCompanySettings(company, request.getServletContext());


            ElementData userDetails = uada.getUserWithId(userId);

//This is an example notification that your proposal has been viewed
//	FirebaseService.getInstance().sendNotificationToUser(userId, "Proposal " + "29292" + " has been viewed", "Proposal " + "20202" + " has been opened and if currently being viewed by your customer.", false);

            TUserRolesDataAccess urda = new TUserRolesDataAccess();
            UserPermissions up = urda.getPermissions(userId);

            String[] accessRights = uada.getUserAccessRights(userId);
            List<ElementData> userAccess = uada.getUsers(accessRights);
            List<HashMap<String, String>> userAccessNames = new LinkedList<HashMap<String, String>>();
            for (ElementData u : userAccess)
            {
                HashMap<String, String> details = new HashMap();
                details.put("value", u.getId());
                details.put("label",String.format("%s %s", u.getString(UserAccountDataAccess.E_NAME), u.getString(UserAccountDataAccess.E_SURNAME)));
                userAccessNames.add(details);
            }

// Javascript flags
            boolean[] trialExpiredVars = UserController.isTrialPeriodOver(session);
            int expireInDays = new UserAccountDataAccess().trialPeriodOverInWeek(company);
            int expireInDaysCounter = new UserAccountDataAccess().trialPerioddayCounter(company);
            boolean trialExpired = trialExpiredVars[0];
            Set<Entry<String, ElementData>> uCount = uada.getCompanyUsers(company.getId()).entrySet();
            int userCount = 1;
            if (uCount != null) {
                userCount = uCount.size();
            }
            Boolean trialAlmostExpired = trialExpiredVars[1];
            boolean noPhoneNumber = false;
            if (StringUtils.isBlank(userDetails.getString(UserAccountDataAccess.E_MOBILE_NUMBER))) {
                noPhoneNumber = true;
            }
            boolean showFirstLogin = false; //(!company.getBoolean(TUserAccountDataAccess.TUTORIAL_FIRST_ACCESS) && up.hasManageSettingsPermission());
//			CustomFields customFields = cfda.getCompanyCustomFields(CustomFields.PROPOSALS, company.getId());
//			CustomFields productCustomFields = cfda.getCompanyCustomFields(CustomFields.PRODUCTS, company.getId());


//Proposal count for tip guide
//            int countTotal = pdda.countProposalsTotal(accessRights);
            boolean hasProposalsBoolean = false;
//            if (countTotal > 0) {
//                hasProposalsBoolean = true;
//            }

            boolean hasUserAccess = up.hasManageUsersPermission();

//            DiskUsageDataAccess duda = new DiskUsageDataAccess();
//            ElementData diskUsageData = duda.getDiskUsageForCompanyId(company.getString("Category_id"));
            boolean trialAccount = StringUtils.equalsIgnoreCase(company.getString("attrdrop_pricingplan"), "TRIAL");


            if (trialExpired) {
                new EmailBuilder().prepareAccountReaccessedEmail(userDetails).addTo(userDetails.getString("attr_headline")).doSend();
            }


            if (!up.hasManageSettingsPermission()) {
                response.sendRedirect("/dashboard");
                return;
            }
        %>

        <script>
			var expireInDaysCounter = <%=expireInDaysCounter %>;
			var hasUserAccess = <%=hasUserAccess %>;
			var trialExpired = <%=trialExpired %>;
			var trialAlmostExpired = <%=trialAlmostExpired %>;

			var needSetupPayment = false; <%--<%= StripeController.needSetupPayment(session) %>;--%>

			var expireInDays = <%=expireInDays %>;
			var noPhoneNumber = <%=noPhoneNumber %>;
			var showFirstLogin = <%=showFirstLogin %>;
			var userCount = <%=userCount %>;

			var userAccess = <%=new Gson().toJson(userAccessNames) %>;

			<%--var dateFormatPattern = "<%= DateFormatUtils.getInstance(company.getString(UserAccountDataAccess.C1_DATE_FORMAT), Locale.forLanguageTag(company.getString(UserAccountDataAccess.C1_LOCALE))).getJsPattern() %>";--%>

			var tutorialFirstLoad = "<%= uda.isUsersFirstAccess(userUsername)%>";
			var userEmail = "<%= userUsername %>";
			var tab4Shown;

			<%--var accountSuspended = <%= StripeService.isCompanySuspended(company.getId()) %>;--%>
			var hasManageSubscriptionsPermission = <%= up.hasManageUsersPermission() %>;

        </script>

        <title>TravelDocs - Dashboard</title>
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="shortcut icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon">

        <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700italic,700,900,900italic" rel="stylesheet">
        <link rel="stylesheet" href="/application/assets/plugins/bootstrap-modal/css/bootstrap-modal.css">
        <link rel="stylesheet" href="/application/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css">
        <link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/font-awesome.min.css" />

        <!-- STYLESHEETS -->
        <style type="text/css">
            .content-wrapper {
                height: 100%;
            }
            .content.custom-scrollbar {
                height: 100%;
            }
            [fuse-cloak],
            .fuse-cloak {
                display: none !important;
            }
            body {
                background-color: white !important;
            }
        </style>
        <!-- Icons.css -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/icons/fuse-icon-font/style.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/fontawesome-pro-5.8.1-web/css/all.css" />
        <!-- Animate.css -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/animate.css/animate.min.css">
        <!-- PNotify -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/pnotify/pnotify.custom.min.css">
        <!-- Nvd3 - D3 Charts -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/nvd3/build/nv.d3.min.css" />
        <!-- Fuse Html -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/fuse-html/fuse-html.min.css" />
        <!-- Swiper -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/swiper/css/swiper.min.css" />
        <!-- Datatable -->
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css" />
        <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.material.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.2/css/fixedHeader.dataTables.min.css">

        <link rel="stylesheet" href="/application/assets/plugins/sweetalert/sweetalert.css" />
        <!-- Main CSS -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/css/main.css">
        <link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/all.css">
        
        <link type="text/css" rel="stylesheet" href="/application/assets/new/css/dashboard.css">

            <%--<link type="text/css" rel="stylesheet" href="/application/assets/materialize/css/materialize.min.css"  media="screen,projection"/>--%>
        <!-- / STYLESHEETS -->

        <!-- JAVASCRIPT -->
        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
        <script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.3.min.js"></script>
        <!-- Mobile Detect -->
        <script type="text/javascript" src="/application/assets/new/vendor/mobile-detect/mobile-detect.min.js"></script>
        <!-- Popper.js -->
        <script type="text/javascript" src="/application/assets/new/vendor/popper.js/index.js"></script>
        <!-- Bootstrap -->
        <script type="text/javascript" src="/application/assets/new/vendor/bootstrap/bootstrap.min.js"></script>
        <!-- Nvd3 - D3 Charts -->
        <script type="text/javascript" src="/application/assets/new/vendor/d3/d3.min.js"></script>
        <script type="text/javascript" src="/application/assets/new/vendor/nvd3/build/nv.d3.min.js"></script>
        <!-- Data tables -->
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.material.min.js"></script>  <%--/application/assets/new/vendor/datatables.net/js/jquery.dataTables.min.js--%>
            <%--<script type="text/javascript" src="/application/assets/new/vendor/datatables-responsive/js/dataTables.responsive.js"></script>--%>
        <!-- PNotify -->
        <script type="text/javascript" src="/application/assets/new/vendor/pnotify/pnotify.custom.min.js"></script>
        <!-- Fuse Html -->
        <script type="text/javascript" src="/application/assets/new/vendor/fuse-html/fuse-html.min.js"></script>
        <!-- Main JS -->
        <script type="text/javascript" src="/application/assets/new/js/main.js"></script>

        <!-- / JAVASCRIPT -->

        <link rel="stylesheet" href="/application/assets/plugins/switchery/switchery.css" />
        <script src="/application/assets/plugins/switchery/switchery.js"></script>
        <script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
        v

    </head>

    <body class="layout layout-vertical layout-left-navigation layout-below-toolbar">
    <main>
        <div id="wrapper">

            <jsp:include page="/application/include/common-side-menu.jsp"></jsp:include>

            <div class="content-wrapper">

                <jsp:include page="/application/include/common-top-menu.jsp"></jsp:include>

                <div class="content-wrapper">
                    <div class="content custom-scrollbar">

                        <div id="settings" class="page-layout simple right-sidebar">
                            <aside class="page-sidebar custom-scrollbar" data-fuse-bar="file-manager-info-sidebar" data-fuse-bar-position="right" data-fuse-bar-media-step="lg">
                                <!-- SIDEBAR HEADER -->
                                <div class="header bg-secondary text-auto d-flex flex-column justify-content-between p-6">

                                    <!-- INFO -->
                                    <div>

                                        <div class="title"><%=company.getName()%> Settings</div>

                                        <!-- <div class="subtitle text-muted">
                                            <span>Users #</span>
                                            2
                                        </div> -->

                                    </div>
                                    <!-- / INFO-->

                                </div>
                                <!-- / SIDEBAR HEADER -->

                                <!-- SIDENAV CONTENT -->
                                <div class="content">

                                    <ul class="nav flex-column custom-scrollbar" id="sidenav" data-children=".nav-item">

                                        <li class="subheader">
                                            <span>Options</span>
                                        </li>

                                        <li class="nav-item">
                                            <a class="settingsMenu nav-link ripple active" href="#" id="settingsMenuUsers"  data-url="settingsUsers.jsp">

                                                <i class="icon s-4 icon-account-multiple"></i>

                                                <span>User</span>
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="settingsMenu nav-link ripple " href="#" id="settingsMenuRoles" data-url="settingsRoles.jsp">

                                                <i class="icon s-4 icon-account-network"></i>

                                                <span>Roles</span>
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="settingsMenu nav-link ripple " href="#" id="settingsMenuSecurity" data-url="settingsSecurity.jsp">

                                                <i class="icon s-4 icon-lock"></i>

                                                <span>Account Security</span>
                                            </a>
                                        </li>

<%--                                        <li class="nav-item">--%>
<%--                                            <a class="settingsMenu nav-link ripple " href="#" id="settingsMenuTeams" data-url="settingsTeams.jsp">--%>

<%--                                                <i class="icon s-4 icon-account-switch"></i>--%>

<%--                                                <span>Teams</span>--%>
<%--                                            </a>--%>
<%--                                        </li>--%>

                                    </ul>
                                </div>
                                <!-- / SIDENAV CONTENT -->
                            </aside>
                            <div class="page-content-wrapper custom-scrollbar" style="overflow-y: auto">

                                <!-- CONTENT -->
                                <div class="page-content custom-scrollbar" id="settingsContentArea">
                                        <%--SETTINGS CONTENT GOES HERE--%>
                                </div>
                                <!-- / CONTENT -->
                            </div>
                        </div>

                    </div>
                </div>
                <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
                    <div class="list-group" class="date">

                        <div class="list-group-item subheader">TODAY</div>

                        <div class="list-group-item two-line">

                            <div class="text-muted">

                                <div class="h1"> Friday</div>

                                <div class="h2 row no-gutters align-items-start">
                                    <span> 4</span>
                                    <span class="h6">th</span>
                                    <span> Apr</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="list-group">

                        <div class="list-group-item subheader">Events</div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Group Meeting</h3>
                                <p>In 32 Minutes, Room 1B</p>
                            </div>
                        </div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Public Beta Release</h3>
                                <p>11:00 PM</p>
                            </div>
                        </div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Dinner with David</h3>
                                <p>17:30 PM</p>
                            </div>
                        </div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Q&amp;A Session</h3>
                                <p>20:30 PM</p>
                            </div>
                        </div>

                    </div>

                    <div class="divider"></div>

                    <div class="list-group">

                        <div class="list-group-item subheader">Notes</div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Best songs to listen while working</h3>
                                <p>Last edit: May 8th, 2015</p>
                            </div>
                        </div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Useful subreddits</h3>
                                <p>Last edit: January 12th, 2015</p>
                            </div>
                        </div>

                    </div>

                    <div class="divider"></div>

                    <div class="list-group">

                        <div class="list-group-item subheader">Quick Settings</div>

                        <div class="list-group-item">

                            <div class="list-item-content">
                                <h3>Notifications</h3>
                            </div>

                            <div class="secondary-container">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" />
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </div>

                        </div>

                        <div class="list-group-item">

                            <div class="list-item-content">
                                <h3>Cloud Sync</h3>
                            </div>

                            <div class="secondary-container">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" />
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </div>

                        </div>

                        <div class="list-group-item">

                            <div class="list-item-content">
                                <h3>Retro Thrusters</h3>
                            </div>

                            <div class="secondary-container">

                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" />
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <div id="edit-management-modal" class="modal top-modal fade" tabindex="-1"></div>
    <div id="migrations-modal" class="modal top-modal fade" tabindex="-1"></div>
    <div id="auth-modal" class="modal top-modal fade" tabindex="-1"></div>


    <!-- Bootstrap core JavaScript
      ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<%=UrlUtils.autocachedUrl("/application/js/log4javascript.js", request) %>"></script>
        <%--<script src="<%=UrlUtils.autocachedUrl("/application/js/logging.js", request) %>"></script>--%>
    <script src="<%=UrlUtils.autocachedUrl("/application/js/utils.js", request) %>"></script>

    <script type="text/javascript" src="/application/assets/new/js/apps/dashboard/project.js"></script>
    <!-- Fuse Html -->
    <script src="/application/assets/plugins/jquery-placeholder/js/jquery.placeholder.js"></script>
    <script src="/application/assets/plugins/moment/moment.js"></script>
    <script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="/application/assets/plugins/moment-timezone/0.5.10/moment-timezone-with-data.min.js"></script>
    <script type="text/javascript" src="/application/assets/new/vendor/swiper/js/swiper.min.js"></script>
    <script src="/application/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="/application/assets/plugins/DataTables/buttons/js/dataTables.buttons.min.js"></script>
    <script src="/application/assets/plugins/DataTables/dataTables.colReorder.min.js"></script>
    <script src="/application/assets/plugins/DataTables/dataTables.colResize.js"></script>
    <script src="https://cdn.datatables.net/fixedcolumns/3.2.4/js/dataTables.fixedColumns.min.js"></script>
    <script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="/application/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"></script>
    <script src="/application/assets/plugins/timeago/jquery.timeago.js"></script>
    <script src="/application/assets/plugins/dynatable.0.3.1/jquery.dynatable.js"></script>
    <script src="/application/assets/plugins/dropzone/downloads/dropzone.min.js"></script>
    <script src="/application/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="/application/assets/plugins/bootstrap-datepicker/js/date.format.js"></script>
    <script src="/application/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="/application/assets/plugins/switchery/switchery.js"></script>
    <script src="/application/assets/plugins/nprogress/nprogress.js"></script>
    <script src="/application/assets/js/ui-modals.js"></script>
    <script src="/application/assets/plugins/Chart.js/Chart.min.js"></script>
    <script src="/application/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/application/assets/plugins/jquery.form/jquery.form.js"></script>


    <script src="<%=UrlUtils.autocachedUrl("/application/js/loading.js", request) %>"></script>

    <script src="/application/js/usersTeams.js"></script>


        <%--<script type="text/javascript" src="/application/assets/materialize/js/materialize.min.js"></script>--%>
        <%--MAIN DASH JS--%>
    <script>
    </script>
    </body>
</compress:html>
</html>