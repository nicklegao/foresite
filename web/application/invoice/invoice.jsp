<?xml version="1.0" encoding="UTF-8"?>
<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">   
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.bill.BookingSummary"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.bill.TDInvoice"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.List"%>
<%@page import="com.stripe.model.Plan"%>
<%@page import="com.stripe.model.InvoiceLineItemPeriod"%>
<%@page import="com.stripe.model.InvoiceLineItem"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.StripeService"%>
<%@page import="com.stripe.model.Invoice"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.PaymentDataAccess"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%
TDInvoice tdInvoice = (TDInvoice) request.getAttribute("invoice");
Invoice invoice = tdInvoice.getInvoice();
List<BookingSummary> summaries = tdInvoice.getSummary();
CategoryData company = (CategoryData)request.getAttribute("company");
ElementData invoiceCI = (ElementData)request.getAttribute("invoiceCI");
List<String> customers = (List<String>)request.getAttribute("customers");
SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
DecimalFormat invNof = new DecimalFormat("000000");
DecimalFormat nf = new DecimalFormat("#,##0");

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,IE=9,IE=8,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <style>
    body {
      font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
      font-size: 14px;
      line-height: 1.42857143;
      color: #333;
      background-color: #fff;
    }
    p {
      margin: auto;
    }
    .content{
      width:660px;
      padding: 30px;
    }
    .section{
      width: 100%;
      clear: both;
    }
    .column{
      width: 50%;
      float: left;
    }
    table.invoice {
      width: 100%;
      text-align: left;
      border-collapse: collapse;
      -fs-table-paginate: paginate;
    }
    table.invoice td, table.invoice th {
    padding: 4px;
    }
    table.invoice tr.header th, table.invoice tr.header td {
      text-align: left;
      border-bottom: 3px solid #888;
      border-top: 3px solid #888;
    }
    table.invoice tr{
	  page-break-inside:avoid;
	}
    tr.detail {
      text-align: left;
    }
    tr.detail.discount {
      color: red;
	}
    tr.detail:nth-child(even) {
	  background-color: #efefef;
	}
    tr.header {
      font-weight: bold;
    }
    tr.detail:last-child td{
      border-bottom: 2px solid #aaa;
    }
    td.qty, th.qty {
      text-align: right !important;
    }
    td.number, th.number {
      width: 150px;
      text-align: right !important;
    }
    tr.sub-total td {
      padding-top: 40px;
      font-weight: bold;
      border-bottom: 2px solid #999;
    }
    tr.sub-total td.empty{
      border-bottom-color: transparent;
    }
    tr.total {
      font-size: 1.2em;
      font-weight: bold;
    }
    tr.total td {
      border-top: 3px solid #333;
    }
    .box {
	    border: 1px solid black;
	    padding: 10px;
	    height: 230px;
	}
	.box.left {
		margin-right: 5px;
	}
	.box.right {
		margin-left: 5px;
	}
	.noBreak {
		page-break-inside: avoid;
	}
	.section:after {
	    display: table;
	    content: "";
	    clear: both;
	}
    </style>
</head>
<body>
  <div class="content">
    <div class="section">
      <h1>STATEMENT / TAX INVOICE</h1>
    </div>
    <div class="section">
      <div class="column">
        <p>To <strong><%= ESAPI.encoder().encodeForHTML(company.getName()) %></strong></p>
        <p><%= ESAPI.encoder().encodeForHTML(company.getString(UserAccountDataAccess.C_ADDRESS_LINE_1) + " " + company.getString(UserAccountDataAccess.C_ADDRESS_LINE_2)) %></p>
        <p><%= ESAPI.encoder().encodeForHTML(company.getString(UserAccountDataAccess.C_ADDRESS_CITY_SUBURB)) %></p>
        <p><%= ESAPI.encoder().encodeForHTML(company.getString(UserAccountDataAccess.C_ADDRESS_STATE)) %></p>
        <p><%= ESAPI.encoder().encodeForHTML(company.getString(UserAccountDataAccess.C_ADDRESS_POSTCODE)) %></p>
        <p><strong>Invoice Date</strong></p>
        <p><%= ESAPI.encoder().encodeForHTML(df.format(StripeService.parseDate(invoice.getDate()))) %></p>
        <p><strong>Invoice Number</strong></p>
        <p>TD-INV- <%= ESAPI.encoder().encodeForHTML(invoiceCI.getName()) %></p>
        <% if(!customers.isEmpty()){ %>
        <p><strong>Customer Code</strong></p>
        <p><%= ESAPI.encoder().encodeForHTML(StringUtils.join(customers, ", ")) %></p>
        <% } %>
      </div>
      <div class="column">
        <p><img class="logo" src=""/></p>
        <p><strong>TravelDocs</strong></p>
        <p>CORPORATE INTERACTIVE TECH PTY LTD</p>
        <p>Suite 1402, 109 Pitt Street</p>
        <p>Sydney</p>
        <p>NSW</p>
        <p>2000</p>
        <p><strong>ABN Number</strong></p>
        <p>45 099 052 454</p>
        <p><strong>All Billing Enquiries</strong></p>
        <p>accounts@corporateinteractive.com.au</p>
      </div>
    </div>
    <div class="section">
      <h2>Billing Period: <%=df.format(tdInvoice.getStart()) %> - <%=df.format(tdInvoice.getEnd()) %></h2>
      <table class="invoice">
        <thead>
          <tr class="header">
            <th>Description</th>
            <th class="qty">Quantity</th>
            <th class="number">Amount</th>
          </tr>
        </thead>  
        <tbody>
          <% for(InvoiceLineItem item : StripeService.getSortedItems(invoice)){
        	  InvoiceLineItemPeriod period = item.getPeriod();
              Plan plan = item.getPlan();
              String desc = StringUtils.isNotBlank(item.getDescription()) ? item.getDescription() : (plan.getName() + "("+plan.getCurrency().toUpperCase()+StripeService.formatMoney(plan.getAmount())+"/"+plan.getInterval()+")");
        	  //Subscription to QuoteCloud Staging - SaaS: STANDARD plan ($199.00/month) 
          %>
          <tr class="detail <%= item.getAmount()<0?"discount":"" %>">
            <td><%= ESAPI.encoder().encodeForHTML(desc) %></td>
            <td class="qty"><%= nf.format(item.getQuantity()) %></td>
            <td class="number"><%= item.getCurrency().toUpperCase() %> <%= StripeService.formatMoney(item.getAmount()) %></td>
          </tr>
          <% } %>
          <tr class="sub-total">
            <td width="50%" class="empty"></td>
            <td>Total</td>
            <td class="number"><%= invoice.getCurrency().toUpperCase() %> <%= StripeService.formatMoney(invoice.getTotal()) %></td>
          </tr>
          <tr>
            <td width="50%"></td>
            <td>Subtotal </td>
            <td class="number"><%= invoice.getCurrency().toUpperCase() %> <%= StripeService.formatMoney(invoice.getSubtotal()) %></td>
          </tr>
          <tr>
            <td width="50%"></td>
            <td>GST</td>
            <td class="number"><%= invoice.getCurrency().toUpperCase() %> <%= StripeService.formatMoney(invoice.getTax()) %></td>
          </tr>
          <tr class="total">
            <td width="50%"></td>
            <td><%= StringUtils.startsWith(invoice.getId(), "TD_") ? "Due" : "Paid" %></td>
            <td class="number"><%= invoice.getCurrency().toUpperCase() %> <%= StripeService.formatMoney(invoice.getAmountDue()) %></td>
          </tr>
        </tbody>
      </table>
    </div>
    
    <% if(StringUtils.startsWith(invoice.getId(), "TD_")){ %>
    <div class="section noBreak">
      <h3>How to pay</h3>
      <div class="column">
      	<div class="box left">
		<p>Please make electronic payment to:</p>      	
      	<p><strong style="font-size:1.5em;">Corporate Interactive</strong></p>
      	<p>BANK:<strong>St. George Bank</strong></p>
        <p>BSB: <strong>112 879</strong></p>
        <p>Account Number: <strong>414 497 416</strong></p>
      	<p>Please reference invoice number on the payment.</p>
      	<h4></h4>
      	<p>send remittance to: <strong>accounts@corporateinteractive.com.au</strong></p>
      	</div>
      </div>
      <div class="column">
      	<div class="box right">
      	  <p>NOTE: All bank fees for wire transfers shall be the responsibility of the sender! (including those of the beneficiary bank).</p>
      	</div>
      
      </div>
    </div>
    
    <% }else{ %>
    <div class="section noBreak">
      <h3>Automatic debit - no action required</h3>
      <p>The amount has been debited from your nominated credit card.</p>
    </div>
    <% } %>
    
    
    <h2>&nbsp;</h2>
    <div class="section">
      <h2>Transaction Summary</h2>
      <table class="invoice">
        <thead>
          <tr class="header">
            <th>Travel Management Company</th>
            <th>Travel Booking Agent</th>
            <th class="number">No. of Itineraries</th>
          </tr>
        </thead>  
        <tbody>
          <% 
          int total = 0;
          for(BookingSummary summary : summaries){ 
        	  total+=summary.getAmount();
          %>
          <tr class="detail">
            <td><%= ESAPI.encoder().encodeForHTML(summary.getAgent()) %></td>
            <td><%= ESAPI.encoder().encodeForHTML(summary.getConsultant()) %></td>
            <td class="number"><%= nf.format(summary.getAmount()) %></td>
          </tr>
          <% } %>
          <tr class="detail">
            <td class="empty"></td>
            <td>Total Bookings</td>
            <td class="number"><%= nf.format(total) %></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</body>
</html>