<!DOCTYPE html>
<!--[if lt IE 9]><html class="bad-ie" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<%@page import="java.util.Locale"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.DateFormatUtils"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.SecurityKeyService"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="java.util.Hashtable"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess" %>
<html lang="en">
<!--<![endif]-->
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
<head>

<%
UserAccountDataAccess uada = new UserAccountDataAccess();
TravelDocsDataAccess pda = new TravelDocsDataAccess();

String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
String userUsername = (String)session.getAttribute(Constants.PORTAL_USERNAME_ATTR);
CategoryData company = uada.getUserCompanyForUserId(userId);
String companyId = company.getId();
String corporateCSS = String.format("/%s/%s/Categories/%s/gen/css-document/dashboard-colours.css",
		Defaults.getInstance().getStoreContext(),
		UserAccountDataAccess.USER_MODULE,
		companyId);
Hashtable<String, String> userDetails = uada.getUserWithId(userId);

String[] accessRights = uada.getUserAccessRights(userId);

String bzresAutologin = "#";
%>
<script>
var dateFormatPattern = '<%= DateFormatUtils.getInstance(company.getString(UserAccountDataAccess.C1_DATE_FORMAT), Locale.forLanguageTag(company.getString(UserAccountDataAccess.C1_LOCALE))).getJsPattern() %>';
</script>
<title>Sales Forecast</title>

<jsp:include page="/application/include/common-head.jsp"></jsp:include>

<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
<link rel="stylesheet" type="text/css" media="screen" href="/proposals/css/smartadmin-production-plugins.css">
<link rel="stylesheet" type="text/css" media="screen" href="/proposals/css/smartadmin-production.css">
<link rel="stylesheet" type="text/css" media="screen" href="/proposals/css/smartadmin-skins.css">
<link rel="stylesheet" href="/application/assets/plugins/rangeslider/rangeslider.css" />
<link rel="stylesheet" href="/application/css/dashboard.css" type="text/css"/>

<link rel="stylesheet" href="<%=corporateCSS%>" />

<jsp:include page="ie8StdHeaders.jsp"></jsp:include>

<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
</head>

<body class="">
  <div class="blurred">
      <!--  Notification Area -->
      <div class='notificationMessage' style='display:none'></div>
  <!-- HEADER -->
		<header id="header">
			<div id="logo-group">

				<span id="logo"> <img src="/application/images/qCloud.png" alt="QuoteCloud"> </span>
							
			</div>
			
			<jsp:include page="include/i_navbar.jsp" />
			
			<!-- pulled right: nav area -->
			<div class="pull-right">

				<!-- logout button -->
				 <div id="logout" class="btn-header transparent pull-right">
					<span> <a href="/api/free/logout" title="Sign Out" data-action="userLogout" data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->

				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a  data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->

			</div>
			<!-- end pulled right: nav area -->
			
		</header>
		<!-- END HEADER -->
		
		<div id="body">
			<div role="main">
	
				<div id="ribbon">
					<span class="ribbon-text">Welcome back, <%=ESAPI.encoder().encodeForHTML(userDetails.get(UserAccountDataAccess.E_NAME)) %></span>
				</div>
				
				<div id="content">
					<div class="row" style="margin: 30px 15px">
						
						<ul id="sparks" class="buckets text-center">
							<li class="sparks-info status-bucket-creating statusCell" data-bucket="creating">
								<h5> Creating <span><i class="qc qc-creating"></i> <span class="totalSummary"><i class="fa fa-cog fa-spin"></i></span></span></h5>
							</li>
							<li class="sparks-info status-bucket-sent statusCell" data-bucket="sent">
								<h5> Sent <span><i class="qc qc-sent"></i> <span class="totalSummary"><i class="fa fa-cog fa-spin"></i></span></span></h5>
							</li>
							<li class="sparks-info status-bucket-needsAction statusCell" data-bucket="needsAction">
								<h5> Needs Action <span><i class="qc qc-needsAction"></i> <span class="totalSummary"><i class="fa fa-cog fa-spin"></i></span></span></h5>
							</li>
						</ul>
					</div>
					
					<div class="container contentContainer">
						<table class="table table-bordered table-hover table-full-width" id="forecastProposals" width="100%" data-is-admin=<%=accessRights.length > 1 %>>
						</table>
						<footer>
							<!-- <p>2015 &copy; QuoteCloud</p> -->
						</footer>
					</div>
				  <!-- /container -->
				</div>
			</div>
		</div>
	</div>
  <!-- start: BOOTSTRAP EXTENDED MODALS -->
  <div id="proposal-notes-modal" class="modal fade" tabindex="-1"></div>

  <jsp:include page="dialog/backgroundLoading.jsp"></jsp:include>

  <div id="createNoteModal" class="modal fade" tabindex="-1">
    <form class="form-create-note" action="/api/secure/discussion/createNote" method="POST">
      <div class="modal-header">
        <div class="titleBar">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        </div>
        <h2 class="modal-title">Create Note</h2>
      </div>
      
      <div class="modal-body">
        <input type="hidden" name="proposal"/>
        <input type="hidden" name="v-key"/>
        <div class="form-group no-margin">
          <textarea id="noteText" class="form-control" name="note" style="height: 212px;" placeholder="Please enter your note"></textarea>
        </div>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">
          <i class="fa fa-times"></i> Close
        </button>
        <button type="submit" class="btn btn-success">
          <i class="qc qc-edit"></i> Submit
        </button>
      </div>
    </form>
  </div>
  
  <!-- end: BOOTSTRAP EXTENDED MODALS -->


  <!-- Bootstrap core JavaScript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="/application/js/log4javascript.js"></script>
  <script src="/application/js/logging.js"></script>
  <script src="/application/js/utils.js"></script>

  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="/application/assets/plugins/jquery-placeholder/js/jquery.placeholder.js"></script>
  <script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
  <script src="/application/assets/plugins/bootstrap-qcloud/js/bootstrap.min.js"></script>
  <script src="/application/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
  <script src="/application/assets/plugins/blockUI/jquery.blockUI.js"></script>
  <script src="/application/assets/plugins/iCheck/jquery.icheck.min.js"></script>
  <script src="/application/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
  <script src="/application/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
  <script src="/application/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
  <script src="/application/assets/plugins/jquery.fileDownload/jquery.fileDownload.js"></script>
  <script src="/application/assets/plugins/jquery-fixedheadertable/jquery.fixedheadertable.js"></script>
  <script src="/application/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script src="/application/assets/plugins/bootstrap-datepicker/js/date.format.js"></script>
  <script src="/application/assets/plugins/jquery.form/jquery.form.js"></script>
  <script src="/application/assets/plugins/moment/moment.js"></script>
  <script src="/application/assets/plugins/sweetalert/sweetalert.min.js"></script>
  <script src="/application/js/screenCheck.js"></script>

  <script src="/application/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script> <!-- min version may cause performance issue in Firefox, but working fine in IE and Chrome -->
  <script src="/application/assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
  <script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
  <script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
  <script src="/application/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"></script>
  <script src="/application/assets/js/ui-modals.js"></script>
  <script src="/application/assets/plugins/ckeditor/ckeditor.js"></script>
  <script src="/application/assets/plugins/ckeditor/adapters/jquery.js"></script>
  <script src="/application/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
  <script src="/application/assets/plugins/rangeslider/rangeslider.min.js"></script>
  <script src="/application/assets/plugins/animate-css/animateCss.js"></script>
  
  <script src="/application/js/dialogs.js"></script>
  <script src="/application/js/salesForecast.js"></script>
  <script src="/application/js/admin.js"></script>
  <script src="/application/js/loading.js"></script>
  <script>
    jQuery(document).ready(function() {
      TableData.init();
      UIModals.init();
    });
  </script>

  <jsp:include page="ga-tracking.jsp"></jsp:include>
</body>
</compress:html>
</html>