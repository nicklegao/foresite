function pricingManagementAction(e)
{
	var $modal = $('#pricing-discounts-modal');
	$('body').modalmanager('loading');

	$modal.one('show.bs.modal', function()
	{
		$modal.find('input, textarea').placeholder();
	});

	$modal.load('/application/dialog/pricingDiscounts.jsp', {}, function()
	{
		$modal.modal({
			width : '700px',
			backdrop : 'static',
			keyboard : false
		});
	});

	return $modal;
}
