var imagePath;
var selectedImageUrl = "";
var selectedImage;
var cropped;
var ValidImageTypes = [ "image/gif", "image/jpeg", "image/png" ];

$(function()
{

	$(document).on('click', "#cropImage", function()
	{
		if (imagePath == undefined)
			imagePath = $("#thumbnailImage").attr('src');
		var blob = null;
		var xhr = new XMLHttpRequest();
		xhr.open("GET", imagePath);
		xhr.responseType = "blob";// force the HTTP response, response-type
		// header to be blob
		xhr.onload = function()
		{
			blob = xhr.response;// xhr.response is now a blob object
			var f = new File([ blob ], $('#fileName').text());
			var reader = new FileReader();
			reader.onload = function(e)
			{
				selectedImageUrl = e.target.result;
				$("#jCropModalImage #JcropPhotoFrame").attr('src', e.target.result).width("auto").height('auto');
				selectedImage = f;

				loadJcropModal();
			};
			showUploadingImage(true);
			reader.readAsDataURL(f);
		}
		xhr.send();
	});

	$("#addCover").on("change", function(evt)
	{
		$('#cropImage').hide();
		var file = evt.target.files;
//		selectedImage = file[0];
//		$('#add-edit-cover-form #save-changes').removeAttr('disabled');

		if (FileReader && file && file[0])
		{
			var reader = new FileReader();
			reader.onload = function(e)
			{
				selectedImageUrl = e.target.result;
				$("#jCropModalImage #JcropPhotoFrame").attr('src', e.target.result).width("auto").height('auto');
				selectedImage = file[0];

				if ($.inArray(selectedImage["type"], ValidImageTypes) < 0)
				{
					swal("Only image files are allowed.");
				}
				else
				{

					loadJcropModal();
				}
			};
			showUploadingImage(true);
			reader.readAsDataURL(file[0]);
		}
		else
		{
			alert("This browser do not support file upload.");
		}
	});

	$("#jCropModalImage").on('click', '.cancelImageUpload', function(event)
	{
		$('#cropImage').show();
		$("#jCropModalImage").modal('hide');
	});

	$("#imageDeleteModal").on('click', '.cancelDeleteButton', function(event)
	{
		$("#imageDeleteModal").modal('hide');
	});

	$("#imageMultiDeleteModal").on('click', '.cancelMultiDeleteButton', function(event)
	{
		$("#imageMultiDeleteModal").modal('hide');
	});

	$("#jCropModalImage").on('click', '.uploadFile', function()
    {
		$("#jCropModalImage .btn").addClass('disabled');
		showUploadingImage(true);
		uploadFileAction(selectedImage, function(ok)
		{
			if (ok)
			{
				console.log("Image uploaded");
				$("#jCropModalImage").modal('hide');
			}
			else
			{
				console.error("Error uploading image");
			}
			refreshContent();
			$("#jCropModalImage .btn").removeClass('disabled');
			showUploadingImage(false);
		});
		
    });

});

function uploadFileAction(file, callback)
{
	var formData = new FormData();
	formData.append("file", file);
	$.each(extra, function(key, value)
	{
		formData.append(key, Math.ceil(value));
	});
	$.ajax({
		type : 'POST',
		url : '/api/library/add-edit/cover',
		data : formData,
		cache : false,
		contentType : false,
		processData : false
	}).done(function(data)
	{
		callback(data);
	});
}


$(document).ready(function()
{
	console.log('cover.js')
	$("#add-cover-button").on('click', function(event)
	{
		console.log('addcover');
		$(event.currentTarget).siblings('#addCover').val('');
		$(event.currentTarget).siblings('#addCover').click();
	});
	
	$(document).on('click', ".delete-library-item-action-details", function()
	{
		var id = $(this).closest('form').find('#elID').val();
		var item = $('.sub-folders').find('[data-id="'+ id +'"][data-type="file"]');
		deleteItemPressed(item);
	});
});

function initialiseList()
{
	$('.library .sub-folders .center-cropped').tooltip();
}

function buildFileHtml(file)
{
	var $html = $($('.item-template').find('.item').clone());
	$html.addClass('item-file');
	$html.attr('data-type', 'file');
	$html.attr('data-id', file.element_id);
	$html.find('.item-icon').addClass('center-cropped').attr('style', 'background: url(\'/stores'+file.attrfile_image+'?image-size=156x156-fit\')').attr('data-placement', 'bottom').attr('data-toggle','tooltip').attr('title',file.attr_headline);
	$html.find('.item-modified').text(file.last_update_date ? moment(file.last_update_date).format('YYYY-MMM-DD HH:mm') : "");
	$html.find('.item-actions .fa-ellipsis-v').removeClass('fa-ellipsis-v').addClass('fa-ellipsis-h');
	$html.find('.item-actions .buttonEditTemplate').remove();
	return $html.prop('outerHTML');
}


function buildFolderHtml(folder, folderId)
{
	var $html = $($('.item-template').find('.item').clone());
	$html.addClass('item-folder');
	$html.attr('data-id', folder.category_id);
	if (folderId)
	{
		$html.attr('data-subfolder', folder.attr_categoryname);
	}
	else
	{
		$html.attr('data-folder', folder.category_id);
		$html.attr('data-cnt', folder.cnt);
	}
	$html.attr('data-type', 'folder');

	$html.find('.item-icon').html('<i class="icon-folder"></i>').addClass('center-cropped');
	$html.find('.item-name').text(folder.attr_categoryname);
	$html.find('.item-type').text('Folder');
	$html.find('.item-modified').text("");
	$html.find('.item-actions .fa-ellipsis-v').removeClass('fa-ellipsis-v').addClass('fa-ellipsis-h');
	return $html.prop('outerHTML');
}

function addEditItem(newItem)
{
	var $this = $(this);
	var $modal = $('#' + libraryType.toLowerCase() + '-library-add-edit-modal');

	if (newItem)
	{
		$('.modal-title', $modal).text('Add ' + libraryType.toLowerCase().substr(0, 1).toUpperCase() + libraryType.toLowerCase().substr(1));
		$('.delete-library-item-action-details', $modal).hide();
		$('[name="elementId"]', $modal).val('');
	}
	else
	{
		$('.delete-library-item-action-details', $modal).show();
		$('.modal-title', $modal).text('Edit ' + libraryType.toLowerCase().substr(0, 1).toUpperCase() + libraryType.toLowerCase().substr(1));
	}
	$('form', $modal).attr('action', '/api/library/add-edit/' + libraryType);
	$('form', $modal).clearForm();

	$.get('/api/library/add-edit/' + libraryType, function(response)
	{

		fetchFolderSelectOptions($modal, response);


		// remove ck
		// if (CKEDITOR.instances.imageLibDescTextArea)
		// {
		// CKEDITOR.instances.imageLibDescTextArea.destroy();
		// }
		// CKEDITOR.inline('imageLibDescTextArea');


		// FROALA EDITOR
		if ($('#add-edit-cover-form .panel-froala').data('froala.editor'))
		{
			$('#add-edit-cover-form .panel-froala').froalaEditor('destroy');
			$('#add-edit-cover-form .panel-froala').empty();
		}

		if (usingIEorEdge())
		{
			$("#image-alert-browser").show();
		}

		$('#thumbnailImage, [name="image"]', $modal).css({
			display : 'none'
		});

		$('[name="displayTitle"]').iCheck({
			checkboxClass : 'icheckbox_square-orange',
			radioClass : 'iradio_square-orange'
		});


		if (!newItem)
		{
			$('#chooseImage', $modal).html('<i class="fa fa-picture-o"></i> Change').removeClass("btn-primary").addClass("btn-warning");

			$('#cropImage', $modal).show();
			$.get('/api/library/add-edit/' + libraryType + '/' + $this.closest('.item').attr('data-id'), function(response)
			{
				$('[name="elementId"]', $modal).val(response.element_id);
				$('[name="folder"]', $modal).val(response.category_id);
				$.get('/api/library/add-edit/' + libraryType + '/subfolder/' + response.category_id, function(response1)
                {
                    fetchSubFolderSelectOptions($modal, response1);
                    if (response1 != "")
                    {
                        $('[name="subFolder"]', $modal).val(response.attr_subfolder);
                    }
                });
				$('[name="assetName"]', $modal).val(response.attr_headline);
				// remove ck
				// CKEDITOR.instances.imageLibDescTextArea.setData(response.attrlong_description);

				$('#add-edit-cover-form .panel-froala').froalaEditor('html.set', response.attrlong_description);

				$('[name="displayTitle"]', $modal).iCheck(response.attrcheck_displayname ? 'check' : 'uncheck');


				var imgComponents = response.attrfile_image.split('/');
				// imgComponents.splice(-1, 0, '_thumb');
				if (imgComponents[4] != undefined)
				{
					if (response.image_width > 768 || response.image_height > 768)
					{
						$('#thumbnailImage', $modal).attr('src', '/stores' + imgComponents.join('/') + '?image-size=768x768-fit').css({
							display : 'block'
						});
					}
					else
					{
						$('#thumbnailImage', $modal).attr('src', '/stores' + imgComponents.join('/')).css({
							display : 'block'
						});
					}


					$('#save-changes').prop("disabled", false);
					$("#image-container", $modal).show();
					$('#infoContainer', $modal).show();
					$('span#fileName', $modal).text(response.image_filename);
					$('span#fileType', $modal).text(response.image_type);
					$('span#uploadedOn', $modal).text(response.image_uploaded);
					$('span#fileSize', $modal).text(response.image_size);
					$('span#dimensions', $modal).text(response.image_width + " x " + response.image_height + " px");
				}
				else
				{
					$('#thumbnailImage', $modal).attr('src', '/application/assets/images/images_placeholder.png').css({
						display : 'block'
					});
					$('#infoContainer', $modal).hide();
				}
			});
		}
		else
		{
			// remove ck
			// CKEDITOR.instances.imageLibDescTextArea.setData("");
			$('[name="folder"]', $modal).val(currentRequest.folder);
			$.get('/api/library/add-edit/' + libraryType + '/subfolder/' + currentRequest.folder, function(response1)
			{
				fetchSubFolderSelectOptions($modal, response1);
				if (response1 != "" && currentRequest.subFolder)
				{
					$('[name="subFolder"]', $modal).val(currentRequest.subFolder);
				}
			});

			$('#add-edit-cover-form .panel-froala').froalaEditor('html.set', '');

			$('#chooseImage', $modal).html('<i class="fa fa-cloud-upload"></i> Upload').removeClass("btn-warning").addClass("btn-primary");

			$('#infoContainer', $modal).hide();
			$('[name="image"]', $modal).css({
				display : 'block'
			});
		}

		if (!($('form select[name="folder"]', $modal).find('option').length > 2))
		{
			$('form select[name="folder"]', $modal).val('0').trigger('change');
		}

		$modal.modal({
			width : '880px'
		});

		$('#add-edit-cover-form .panel-froala').on('froalaEditor.initialized', function(ev, editor)
		{
			customizeFroalaEditor(ev, editor)
		}).froalaEditor({
			iconsTemplate : 'font_awesome_5',
			enter : $.FroalaEditor.ENTER_P,
			key : '7F4D3F3H3cA5A4B3A1E4B2G2E3D1C6vDIG1QCYRWa1GPId1f1I1=='
		});

	});
}

function bulkAddImages()
{
	var myDropzone = new Dropzone("#uploadImages", {
		url : '/api/library/add/images/bulk',
		maxFilesize : maxSize, // MB
		paramName : "file",
		acceptedFiles : "image/jpeg,image/png,image/gif",
		createImageThumbnails : true,
		init : function()
		{

			this.on("sending", function(file)
			{

			});


			this.on("complete", function(file, response)
			{
				if (file.accepted)
				{
					myDropzone.removeFile(file);
				}
			});

			this.on("queuecomplete", function()
			{

				if (errorImages.length > 0)
				{
					var errorMessage = "Couldn't upload the following images <br/> Please try again.";
					errorMessage += "<ul id='errorImages'>"
					for (i = 0; i < errorImages.length; i++)
					{
						errorMessage += "<li>" + errorImages[i].name + "</li>";
					}
					errorMessage += "</ul>"
					swal({
						html : true,
						title : '<i>Warning</i>',
						text : errorMessage
					});
					errorImages = [];
				}
				else if (errorImagesSize.length > 0)
				{
					var errorMessage = "Couldn't upload the following images <br/> Max file size is " + maxSize + "MB.";
					errorMessage += "<ul id='errorImages'>"
					for (i = 0; i < errorImagesSize.length; i++)
					{
						errorMessage += "<li>" + errorImagesSize[i].name + "</li>";
					}
					errorMessage += "</ul>"
					swal({
						html : true,
						title : '<i>Warning</i>',
						text : errorMessage
					});
					errorImagesSize = [];
				}

				$('#content-library-modal').load('/api/library/manage', {
					libraryType : 'images'
				}, function()
				{
					reloadImages();
				});
			});

			this.on('error', function(file)
			{
				var fileType = file.type
				if ((file.size / 1048576) > maxSize)
				{
					errorImagesSize.push(file);
				}
				else if ((fileType.indexOf("image") == -1))
				{
					swal('File must be an image', 'Only images are allowed to be uploaded into the Image Library', 'error');
					this.removeFile(file);
				}
				console.log("ERROR");
			});
		},
		success : function(file, data)
		{
			if (data.success)
			{
				myDropzone.processQueue.bind(myDropzone);
			}
			else
			{
				errorImages.push(file);
				console.log("ERROR");
			}
		}
	});
}

function reloadImages()
{
	initDynaTable($('#images-list', $('#content-library-modal')));
	initTable($('#content-library-folder-table', $('#content-library-modal')));
	$('a[href="#folders"]').on('shown.bs.tab', function()
	{
		$('#content-library-folder-table', $('#content-library-modal')).DataTable().draw();
	});
	if (folder != null)
	{
		$('#search-folder').val(folder).trigger('change');
	}
}

var coverValidator = $('#add-edit-cover-form').validate({
	rules : {
		assetName : "required"
	},
	submitHandler : function(form)
	{
		var formData = new FormData(form);
		formData.append("file", selectedImage);
		if (cropped)
		{
			$.each(cropped, function(key, value)
			{
				formData.append(key, Math.ceil(value));
			});
		}
		$.ajax({
			type : 'POST',
			url : '/api/library/add-edit/cover',
			data : formData,
			cache : false,
			contentType : false,
			processData : false
		}).done(function(data)
		{
			if (data)
			{
				$("#jCropModalImage").modal('hide');
				$('#cover-library-add-edit-modal').modal('hide');
				refreshContent();
				selectedImage = null;
				$(form).clearForm();
			}
			else
			{
				alert("Error uploading image");
			}
			$("#jCropModalImage .btn").removeClass('disabled');
			showUploadingImage(false);
			$('#cropImage').show();
			imagePath = undefined;
		});

	}
});


function clearPreview()
{
	$("#thumbnailImage").removeClass('svg-clipped');
	$("#thumbnailImage").removeAttr('style');
	$("#image-container").removeAttr('style');
	$("#image-container").hide();
	$("#image-alert").hide();
}


function showErrorSize(show, msg)
{
	if (show)
	{
		$("#jCropModalImage .uploadErrorSize").text(msg);
		$("#jCropModalImage .uploadErrorSize").show();
	}
	else
	{
		$("#jCropModalImage .uploadErrorSize").hide();
	}
}


function showUploadingImage(show)
{
	if (show)
	{
		$(".uploadLoading").show();
	}
	else
	{
		$(".uploadLoading").hide();
	}
}

function loadJcropModal()
{
	var $jcropModal = $("#jCropModalImage");
	showErrorSize(false);
	var jcrop;

	$jcropModal.on('shown.bs.modal', function()
	{
		image = new Image();
		image.src = $("#JcropPhotoFrame", $jcropModal).attr('src');
		image.onload = function()
		{
			if ((image.naturalWidth >= 793 && image.naturalHeight >= 1122) && selectedImage.size <= 6000000)
			{
				$("#JcropPhotoFrame", $jcropModal).Jcrop({
					bgOpacity : 0.4,
					trueSize : [ image.naturalWidth, image.naturalHeight ],
					minSize : [ 793, 1122 ],
					aspectRatio : 793 / 1122,
					boxWidth: $($("#jCropModalImage .modal-header")[1]).width(),
					onSelect : function(c)
					{
						cropped = extra = c;
					}
				}, function()
				{
					jcrop = this;
					$(".uploadFile", $jcropModal).removeClass("disabled");
					showUploadingImage(false);
				});
				jcrop.setSelect([ 0, 0, 793, 1122 ]);
				jcrop.setOptions({
					allowSelect : false
				});
			}
			else
			{
				if(selectedImage.size > 6000000)
					showErrorSize(true, "The image is too big. (Max 6MB)");
				else {
					showErrorSize(true, "The image width must be bigger than 793px and the height bigger than 1122px");
				}
				$(".uploadFile", $jcropModal).addClass("disabled");
				showUploadingImage(false);
			}

		}
	}).on('hide.bs.modal', function()
	{
		$jcropModal.hide();
		if (jcrop)
		{
			jcrop.destroy();
		}
	});

	$jcropModal.modal({
		width : '800px',
		backdrop : 'static',
		keyboard : false
	});


}
