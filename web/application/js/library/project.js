$(document).ready(function()
{
	console.log('project.js')
	$(document).on('click', "#add-project-button", function()
	{
		addEditItem.call($(this), true);
	});

	$(document).on('click', ".delete-library-item-action-details", function()
	{
		var id = $(this).closest('form').find('#elID').val();
		var item = $('.sub-folders').find('[data-id="' + id + '"][data-type="file"]');
		deleteItemPressed(item);
	});

	// $('#project-library-add-edit-modal input[name="caption"]').keyup(function()
	// {
	// 	$('#warningLimit').text($('input[name="caption"]').val().length + "/255");
	// 	if ($('input[name="caption"]').val().length > 230)
	// 	{
	// 		$('#warningLimit').css('color', 'red');
	// 	}
	// 	else
	// 	{
	// 		$('#warningLimit').css('color', 'black');
	// 	}
	// });
	$('#add-edit-project-form').validate({
		rules : {
			projectName : "required"
		},
		errorElement : 'em',
		submitHandler : function(form)
		{
			startLoadingForeground();
			$(form).ajaxSubmit({
				success : function(response)
				{
					stopLoadingForeground();
					if (response)
					{
						$('#project-library-add-edit-modal').modal('hide');
						refreshContent();
					}
				}
			});
		}
	});

});

function buildFileHtml(file)
{
	var $html = $($('.item-template').find('.item').clone());
	$html.addClass('item-file');
	$html.attr('data-type', 'file');
	$html.attr('data-id', file.element_id);
	$html.find('.item-icon').html('<i class="file-icon"></i>');
	$html.find('.item-name').text(file.attr_headline);
	$html.find('.item-type').text('project');
	if (file.thumbnail)
	{
		$html.find('.item-thumbnail').append($('<a>').attr('href', file.attr_projectlink).attr('target','_blank').append($('<img>').attr('src', file.thumbnail)));
	}
	$html.find('.item-modified').text(file.last_update_date ? moment(file.last_update_date).format('YYYY-MMM-DD HH:mm') : "");
	$html.find('.item-actions .buttonDownloadItem').remove();
	$html.data('item', file);
	return $html;
}

function addEditItem(newItem)
{
	var $this = $(this);
	var $modal = $('#' + libraryType.toLowerCase() + '-library-add-edit-modal');

	if (newItem)
	{
		$('.modal-title', $modal).text('Add ' + libraryType.toLowerCase().substr(0, 1).toUpperCase() + libraryType.toLowerCase().substr(1));
		$('.delete-library-item-action-details', $modal).hide();
		$('[name="elementId"]', $modal).val('');
	}
	else
	{
		$('.delete-library-item-action-details', $modal).show();
		$('.modal-title', $modal).text('Edit ' + libraryType.toLowerCase().substr(0, 1).toUpperCase() + libraryType.toLowerCase().substr(1));
	}
	$('form', $modal).attr('action', '/api/library/add-edit/' + libraryType);
	$('form', $modal).clearForm();
	$('form .error', $modal).hide();

	$.get('/api/library/add-edit/' + libraryType, function(response)
	{

		fetchFolderSelectOptions($modal, response);

		if (!newItem)
		{
			$.get('/api/library/add-edit/' + libraryType + '/' + $this.closest('.item').attr('data-id'), function(response)
			{
				$('[name="elementId"]', $modal).val(response.element_id);
				$('[name="projectName"]', $modal).val(response.attr_headline);
			});
		}

		$modal.modal({
			width : '880px'
		});

	});
}
