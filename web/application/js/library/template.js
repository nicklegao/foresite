var libraryType = 'template';
var fileNumber = 0;
var errorPDF = [];
var errorPDFSize = [];

$(document).ready(function()
{
	console.log('template.js')
});

function addEditItem(newItem)
{
	var $this = $(this);

	if (newItem)
	{
		window.open("/template");
	}
	else
	{
		var data = $(this).closest('.item').data('item');
		window.open("/edit?traveldoc=" + data['element_id'] + "&e-key=" + data['e-key']);
	}

}

function copyItem()
{
	var data = $(this).closest('.item').data('item');
	var id = data['element_id'];
	swal({
		title : 'Enter new template name',
		content : "input",
		type : "input",
		showCancelButton : true,
		confirmButtonText : 'Copy template',
		showLoaderOnConfirm : true,
	}, function(value)
	{
		if (value)
		{
			$.ajax({
				url : '/api/traveldoc/copyTemplate',
				data : {
					travelDocId : id,
					templateName : value,
				},
				success: function (data) {
					loadContent(libraryType, $('[data-current-folder]').attr('data-current-folder'), $('[data-current-subfolder]').attr('data-current-subfolder'));
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
}

function buildFileHtml(file)
{
	var libraryType = currentRequest.libraryType;
	var $html = $($('.item-template').find('.item').clone());
	$html.addClass('item-file');
	$html.attr('data-type', 'file');
	$html.attr('data-id', file.element_id);
	$html.attr('data-libraryType', libraryType);
	$html.find('.item-icon').html('<i class="file-icon"></i>');
	$html.find('.item-name').text(file.attr_headline);
	$html.find('.item-type').text('File');
	$html.find('.item-modified').text(file.last_update_date ? moment(file.last_update_date).format('YYYY-MMM-DD HH:mm') : "");
	$html.data('item', file);
	$html.find('.item-actions .buttonDownloadItem').remove();
	return $html;
}
