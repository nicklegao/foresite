$(document).ready(function()
{
	console.log('text.js')
	$(document).on('click', "#add-text-button", function()
	{
		addEditItem.call($(this), true);
	});
});

function addEditItem(newItem)
{
	var $this = $(this);
	var $modal = $('#' + libraryType.toLowerCase() + '-library-add-edit-modal');

	if (newItem)
	{
		$('.modal-title', $modal).text('Add ' + libraryType.toLowerCase().substr(0, 1).toUpperCase() + libraryType.toLowerCase().substr(1));
		$('.delete-library-item-action-details', $modal).hide();
		$('[name="elementId"]', $modal).val('');
	}
	else
	{
		$('.delete-library-item-action-details', $modal).show();
		$('.modal-title', $modal).text('Edit ' + libraryType.toLowerCase().substr(0, 1).toUpperCase() + libraryType.toLowerCase().substr(1));
	}
	$('form', $modal).attr('action', '/api/library/add-edit/' + libraryType);
	$('form', $modal).clearForm();

	$.get('/api/library/add-edit/' + libraryType, function(response)
	{

		fetchFolderSelectOptions($modal, response);

		// remove ck
		// if (CKEDITOR.instances.textBlockTextarea)
		// {
		// CKEDITOR.instances.textBlockTextarea.destroy();
		// }
		// CKEDITOR.inline(textBlockTextarea, {
		// customConfig :
		// '/application/assets/plugins/ckeditor/custom/config-proposalFreeText.jsp'
		// });

		if ($('#add-edit-text-form .panel-froala').data('froala.editor'))
		{
			$('#add-edit-text-form .panel-froala').froalaEditor('destroy');
		}


		$('input[type="checkbox"]', $modal).iCheck({
			checkboxClass : 'icheckbox_square-orange'
		});

		if (!newItem)
		{
			$.get('/api/library/add-edit/' + libraryType + '/' + $this.closest('.item').attr('data-id'), function(response)
			{
				$('[name="elementId"]', $modal).val(response.element_id);
				$('[name="folder"]', $modal).val(response.category_id);
				$.get('/api/library/add-edit/' + libraryType + '/subfolder/' + response.category_id, function(response1)
				{
					fetchSubFolderSelectOptions($modal, response1);
					if (response1 != "")
					{
						$('[name="subFolder"]', $modal).val(response.attr_subfolder);
					}
				});
				$('[name="assetName"]', $modal).val(response.attr_headline);
				// remove ck
				// CKEDITOR.instances.textBlockTextarea.setData(response.attrlong_textblock);

				$('#add-edit-text-form .panel-froala').froalaEditor('html.set', response.attrlong_textblock);

				$('[name="lockContent"]', $modal).iCheck(response.attrcheck_lock_content ? 'check' : 'uncheck');
			});
		}
		else
		{
			// remove ck
			// CKEDITOR.instances.textBlockTextarea.setData("");
			$('[name="folder"]', $modal).val(currentRequest.folder);
			$.get('/api/library/add-edit/' + libraryType + '/subfolder/' + currentRequest.folder, function(response1)
			{
				fetchSubFolderSelectOptions($modal, response1);
				if (response1 != "" && currentRequest.subFolder)
				{
					$('[name="subFolder"]', $modal).val(currentRequest.subFolder);
				}
			});
			$('#add-edit-text-form .panel-froala').empty();
			$('#add-edit-text-form .panel-froala').froalaEditor('html.set', '');
		}

		if (!($('form select[name="folder"]', $modal).find('option').length > 2))
		{
			$('form select[name="folder"]', $modal).val('0').trigger('change');
		}

		$modal.modal({
			width : '860px'
		});

		$('#add-edit-text-form .panel-froala').on('froalaEditor.initialized', function(ev, editor)
		{
			// customizeFroalaEditor(ev, editor)
		}).froalaEditor({
			iconsTemplate : 'font_awesome_5',
			scrollableContainer : '#modal-popup',
			enter : $.FroalaEditor.ENTER_P,
			fontSize : [ '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '22', '24', '26', '28', '30', '60', '96' ],
			fontSizeDefaultSelection : '14',
			// Set the image upload parameter.
			imageUploadParam : 'attachment',
			// Set the image upload URL.
			imageUploadURL : '/api/library/text/image',
			// Additional upload params.
			imageUploadParams : {
				proposal : $('#param-proposal-id').val(),
				x : '0',
				y : '0',
				x2 : '900',
				y2 : '900',
				w : '100%',
				h : '100%'
			},
			// Set request type.
			imageUploadMethod : 'POST',
			// Set max image size to 5MB.
			imageMaxSize : 5 * 1024 * 1024,
			// Allow to upload PNG and JPG.
			imageAllowedTypes : [ 'jpeg', 'jpg', 'png' ],
			toolbarButtons : [ "bold", "italic", "underline", "strikeThrough", "subscript", "superscript", "|", "fontSize", "color", "paragraphStyle", "fullscreen", "|", "paragraphFormat", "align", "formatOL", "formatUL", "outdent", "indent", "quote", "insertLink", "insertTable", "insertImage", "|", "specialCharacters", "insertHR", "selectAll", "clearFormatting", "|", "spellChecker", "|", "undo", "redo", "dataItems" ],
			key : '7F4D3F3H3cA5A4B3A1E4B2G2E3D1C6vDIG1QCYRWa1GPId1f1I1=='
		}).on('froalaEditor.image.inserted', function(e, editor, $img, response)
		{
			$img.width($img.width());
		});
		;

	});
}

var textValidator = $('#add-edit-text-form').validate({
	rules : {
		folder : "required",
		assetName : "required",
		textBlock : "required"
	},
	submitHandler : function(form)
	{
		$(form).ajaxSubmit({
			data : {
				textBlock : $('#add-edit-text-form .panel-froala').froalaEditor('html.get').replace(/\<br>/g, '<br/>')
			},
			success : function(response)
			{
				if (response)
				{
					$('#text-library-add-edit-modal').modal('hide');
					$(form).clearForm();
				}
				refreshContent();
			}
		});
	}
});
