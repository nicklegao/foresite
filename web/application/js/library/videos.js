$(document).ready(function()
{
	console.log('images.js')
	$(document).on('click', "#add-video-button", function()
	{
		addEditItem.call($(this), true);
	});

	$(document).on('click', ".delete-library-item-action-details", function()
	{
		var id = $(this).closest('form').find('#elID').val();
		var item = $('.sub-folders').find('[data-id="' + id + '"][data-type="file"]');
		deleteItemPressed(item);
	});

	// $('#videos-library-add-edit-modal input[name="caption"]').keyup(function()
	// {
	// 	$('#warningLimit').text($('input[name="caption"]').val().length + "/255");
	// 	if ($('input[name="caption"]').val().length > 230)
	// 	{
	// 		$('#warningLimit').css('color', 'red');
	// 	}
	// 	else
	// 	{
	// 		$('#warningLimit').css('color', 'black');
	// 	}
	// });

	$("#videos-library-add-edit-modal .videoSubmit").click(function(event)
	{
		if ($("#add-edit-videos-form").valid())
		{
			var uploadValid = false;
			event.preventDefault();
			var videoLinkUrl = $('#videoLinkUrl').val()
			if (videoLinkUrl.indexOf("https://www.youtube.com/") >= 0 || videoLinkUrl.indexOf("https://vimeo.com/") >= 0 || videoLinkUrl.indexOf("https://youtu.be/") >= 0)
			{
				if (videoLinkUrl.indexOf('&') >= 0)
				{
					$('#videoLinkUrl').val(videoLinkUrl.substring(0, videoLinkUrl.indexOf('&')));
				}
				uploadValid = true;
			}
			else
			{
				uploadValid = false;
				$('#videoLinkUrl').val('');
				$('#videoLinkUrl').attr('style', 'border:1px solid #ff0000 !important;');
				$('#videoLinkUrl').attr('placeholder', "You didnt enter a valid Youtube or Vimeo Link, starting with https://");
			}

			// if ($('input[name="caption"]').val().length > 255)
			// {
			// 	uploadValid = false
			// 	$('label#warningText').text('WARNING: Your caption must be 255 Character or below.');
			// 	$('label#warningText').attr('style', 'color: red');
			//
			// }
			// else
			// {
			// 	uploadValid = true;
			// }

			if (uploadValid)
			{
				$(".videoSubmit").submit();
			}
		}
	});

	$('#add-edit-videos-form').validate({
		rules : {
			folder : "required",
			assetName : "required"
		},
		errorElement : 'em',
		submitHandler : function(form)
		{
			startLoadingForeground();
			$(form).ajaxSubmit({
				success : function(response)
				{
					stopLoadingForeground();
					if (response)
					{
						$('#videos-library-add-edit-modal').modal('hide');
						refreshContent();
					}
				}
			});
		}
	});

});

function buildFileHtml(file)
{
	var $html = $($('.item-template').find('.item').clone());
	$html.addClass('item-file');
	$html.attr('data-type', 'file');
	$html.attr('data-id', file.element_id);
	$html.find('.item-icon').html('<i class="file-icon"></i>');
	$html.find('.item-name').text(file.attr_headline);
	$html.find('.item-type').text('Video');
	if (file.thumbnail)
	{
		$html.find('.item-thumbnail').append($('<a>').attr('href', file.attr_videolink).attr('target','_blank').append($('<img>').attr('src', file.thumbnail)));
	}
	$html.find('.item-modified').text(file.last_update_date ? moment(file.last_update_date).format('YYYY-MMM-DD HH:mm') : "");
	$html.find('.item-actions .buttonDownloadItem').remove();
	$html.data('item', file);
	return $html;
}
function buildFolderHtml(folder, folderId)
{
	var $html = $($('.item-template').find('.item').clone());
	$html.addClass('item-folder')
	$html.attr('data-id', folder.category_id);
	if (folderId)
	{
		$html.attr('data-subfolder', folder.attr_categoryname);
	}
	else
	{
		$html.attr('data-folder', folder.category_id);
		$html.attr('data-cnt', folder.cnt);
	}
	$html.attr('data-type', 'folder');

	$html.find('.item-icon').html('<i class="icon-folder"></i>');
	$html.find('.item-name').text(folder.attr_categoryname);
	$html.find('.item-type').text('Folder');
	$html.find('.item-modified').text("");
	return $html;
}

function addEditItem(newItem)
{
	var $this = $(this);
	var $modal = $('#' + libraryType.toLowerCase() + '-library-add-edit-modal');

	if (newItem)
	{
		$('.modal-title', $modal).text('Add ' + libraryType.toLowerCase().substr(0, 1).toUpperCase() + libraryType.toLowerCase().substr(1));
		$('.delete-library-item-action-details', $modal).hide();
		$('[name="elementId"]', $modal).val('');
	}
	else
	{
		$('.delete-library-item-action-details', $modal).show();
		$('.modal-title', $modal).text('Edit ' + libraryType.toLowerCase().substr(0, 1).toUpperCase() + libraryType.toLowerCase().substr(1));
	}
	$('form', $modal).attr('action', '/api/library/add-edit/' + libraryType);
	$('form', $modal).clearForm();
	// $('#videos-library-add-edit-modal input[name="caption"]').keyup();
	$('form .error', $modal).hide();

	$.get('/api/library/add-edit/' + libraryType, function(response)
	{

		fetchFolderSelectOptions($modal, response);

		if (!newItem)
		{
			$.get('/api/library/add-edit/' + libraryType + '/' + $this.closest('.item').attr('data-id'), function(response)
			{
				$('[name="elementId"]', $modal).val(response.element_id);
				$('[name="folder"]', $modal).val(response.category_id);
				$.get('/api/library/add-edit/' + libraryType + '/subfolder/' + response.category_id, function(response1)
				{
					fetchSubFolderSelectOptions($modal, response1);
					if (response1 != "")
					{
						$('[name="subFolder"]', $modal).val(response.attr_subfolder);
					}
				});
				$('[name="assetName"]', $modal).val(response.attr_headline);
				// $('[name="caption"]', $modal).val(response.attr_caption);
				$('[name="videoLink"]', $modal).val(response.attr_videolink);

			});
		}
		else
		{
			$('[name="folder"]', $modal).val(currentRequest.folder);
			$.get('/api/library/add-edit/' + libraryType + '/subfolder/' + currentRequest.folder, function(response1)
			{
				fetchSubFolderSelectOptions($modal, response1);
				if (response1 != "" && currentRequest.subFolder)
				{
					$('[name="subFolder"]', $modal).val(currentRequest.subFolder);
				}
			});
		}

		if (!($('form select[name="folder"]', $modal).find('option').length > 2))
		{
			$('form select[name="folder"]', $modal).val('0').trigger('change');
		}

		$modal.modal({
			width : '880px'
		});

	});
}
