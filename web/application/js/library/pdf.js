var libraryType = 'pdf';
var fileNumber = 0;
var errorPDF = [];
var errorPDFSize = [];

$(document).ready(function()
{
	console.log('pdf.js')
	$(document).on('click', "#add-pdf-button", function()
	{
		addEditItem.call($(this), true);
	});

});

function addEditItem(newItem)
{
	var $this = $(this);
	var $modal = $('#' + libraryType.toLowerCase() + '-library-add-edit-modal');

	if (newItem)
	{
		$('.modal-title', $modal).text('Add ' + libraryType.toLowerCase().substr(0, 1).toUpperCase() + libraryType.toLowerCase().substr(1));
		$('.delete-library-item-action-details', $modal).hide();
		$('[name="elementId"]', $modal).val('');
	}
	else
	{
		$('.delete-library-item-action-details', $modal).show();
		$('.modal-title', $modal).text('Edit ' + libraryType.toLowerCase().substr(0, 1).toUpperCase() + libraryType.toLowerCase().substr(1));
	}
	$('form', $modal).attr('action', '/api/library/add-edit/' + libraryType);
	$('form', $modal).clearForm();

	$.get('/api/library/add-edit/' + libraryType, function(response)
	{

		fetchFolderSelectOptions($modal, response);


		if (!newItem)
		{
			if ($('.pdfDropzone').length > 0 && $('.pdfDropzone.dz-clickable').length == 0)
			{
				pdfdropZone.enable();
				pdfdropZone.options.autoProcessQueue = false;
			}
			$.get('/api/library/add-edit/' + libraryType + '/' + $this.closest('.item').attr('data-id'), function(response)
			{
				$('[name="elementId"]', $modal).val(response.element_id);
				$('[name="folder"]', $modal).val(response.category_id);
				$.get('/api/library/add-edit/' + libraryType + '/subfolder/' + response.category_id, function(response1)
				{
					fetchSubFolderSelectOptions($modal, response1);
					if (response1 != "")
					{
						$('[name="subFolder"]', $modal).val(response.attr_subfolder);
					}
				});
				$('[name="assetName"]', $modal).val(response.attr_headline);
				$('#uploadPDFDrop', $modal).hide();
				$('.pdfNameGroup', $modal).show();
				pdfEdit = true;
			});
		}
		else
		{
			$('[name="folder"]', $modal).val(currentRequest.folder);
			$.get('/api/library/add-edit/' + libraryType + '/subfolder/' + currentRequest.folder, function(response1)
			{
				fetchSubFolderSelectOptions($modal, response1);
				if (response1 != "" && currentRequest.subFolder)
				{
					$('[name="subFolder"]', $modal).val(currentRequest.subFolder);
				}
			});
			pdfEdit = false;
			if ($('.pdfDropzone').length > 0 && $('.pdfDropzone.dz-clickable').length == 0)
			{
				pdfdropZone.enable();
				pdfdropZone.options.autoProcessQueue = false;
			}
			$('.pdfNameGroup', $modal).hide();
			$('#uploadPDFDrop', $modal).show();
		}

		if (!($('form select[name="folder"]', $modal).find('option').length > 2))
		{
			$('form select[name="folder"]', $modal).val('0').trigger('change');
		}

		$modal.modal({
			width : '860px'
		});


	});
}

if ($('.pdfDropzone.dz-clickable').length == 0)
{
	if (!$('#uploadPDFDrop')[0].dropzone)
	{
		pdfdropZone = new Dropzone('#uploadPDFDrop', {
			url : '/api/library/add-edit/pdf',
			maxFilesize : 12,
			maxFiles : 6,
			paramName : 'file',
			forceFallback : false,// enable for testing
			acceptedFiles : 'application/pdf',
			autoProcessQueue : false,
			paramName : 'folderName',
			dictDefaultMessage : "Upload file please",
			createImageThumbnails : false,
			init : function()
			{

				this.on('addedfile', function(file)
				{
					fileNumber++;
				});

				this.on("maxfilesexceeded", function(file)
				{
					this.removeFile(file);
					fileNumber--;
					swal('Upload failed', 'Only 6 files can be uploaded at one time!', 'error');
				});

				this.on("processing", function()
				{
					this.options.autoProcessQueue = true;
				});

				this.on('sending', function(file, xhr, formData)
				{
					var $form = $('#add-edit-pdf-form');
					formData.append('folder', $form.find('[name="folder"]').val());
					formData.append('newFolder', $form.find('[name="newFolder"]').val());
					formData.append('subFolder', $form.find('[name="subFolder"]').val());
					formData.append('newSubFolder', $form.find('[name="newSubFolder"]').val());

					formData.append('file', file);
				});

				this.on('complete', function(file, response)
				{
					if (file.accepted)
					{
						pdfdropZone.removeFile(file);
					}
				});

				this.on('queuecomplete', function(file)
				{
					if (errorPDF.length > 0)
					{
						var errorMessage = "Couldn't upload the following images <br/> Please try again.";
						errorMessage += "<ul id='errorPDF'>"
						for (i = 0; i < errorPDF.length; i++)
						{
							errorMessage += "<li>" + errorPDF[i].name + "</li>";
						}
						errorMessage += "</ul>"
						swal({
							html : true,
							title : '<i>Warning</i>',
							text : errorMessage
						}, function()
						{
							pdfdropZone.removeAllFiles();
						});
						errorPDF = [];
					}
					else
					{
						$('#pdf-library-add-edit-modal').modal('hide');
						// $('#content-library-modal').load('/api/library/manage',
						// {
						// libraryType : 'pdf'
						// }, function()
						// {
						// initTable($('#content-library-table',
						// $('#content-library-modal')));
						// });
						pdfdropZone.removeAllFiles();
						// CLEAR THE DROPABLE AND FORM
						pdfdropZone.disable();
					}
					refreshContent();
				});


				this.on('error', function(file)
				{
					var fileType = file.type;
					if ((file.size / 1048576) > maxSize)
					{
						errorPDFSize.push(file);
					}
					else if (fileType.indexOf("pdf") == -1)
					{
						swal('File must be PDF', 'Only PDF files are allowed to be uploaded into the PDF Library', 'error');
						this.removeFile(file);
					}
					console.log("ERROR");
				});
			}
		});
	}
}
else
{
	if (pdfdropZone != null)
	{
		pdfdropZone.removeAllFiles();
	}
}

$(".pdfUploadButton").click(function(event)
{
	if ($("#add-edit-pdf-form").valid())
	{
		event.preventDefault();
		if (fileNumber > 0 && !pdfEdit)
		{
			pdfdropZone.processQueue();
		}
		else
		{
			$('#add-edit-pdf-form').submit();

		}
	}
});

var pdfValidator = $('#add-edit-pdf-form').validate({
	rules : {
		folder : "required",
		assetName : "required"
	},
	submitHandler : function(form)
	{
		var formData = new FormData(form);
		var select = $('select[name=folder]>option:selected').html()
		formData.append('assetRealName', select);
		$.ajax({
			type : 'POST',
			url : '/api/library/add-edit/pdf',
			data : formData,
			cache : false,
			contentType : false,
			processData : false
		}).done(function(data)
		{

			$('#folderNameId').addClass('hidden');
			$('#pdfNewFolderText').removeAttr('required');
			$('#pdf-library-add-edit-modal').modal('hide');
			// $('#content-library-modal').load('/api/library/manage', {
			// libraryType : 'pdf'
			// }, function()
			// {
			// });
			$(form).clearForm();
			refreshContent();
		});

	}
});
