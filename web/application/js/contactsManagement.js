var contactColumnDefs = undefined;
var currentContactDataRequest = undefined;
var contactsTable = undefined;
var contactSearchHidden = true;

$(document).ready(function()
{
	var contactsDatatable = function()
	{
		var domHead = "<'navbar' <'navbar-form navbar-left'<'form-group' l><'#columns-list.form-group'>> <'navbar-form navbar-right'<'form-group' f>> ><'proposalResizeBlur hidden'>";
		var domBody = 't'; // To add colResize place "Z" (capital) before t
		var domFooter = "<'navbar' <'navbar-form navbar-left' i> <'navbar-form navbar-right'<'form-group' p>> <'navbar-form navbar-right' <'#actionsHolder'>> >";


		if (isTouchSupported())
			var proposalsToShow = 5;
		else

			contactColumnDefs = [ {
				title : 'Contact Email',
				ciFilterType : 'text',
				data : 'headline',
				render : function(data, type, row)
				{
					if (type == 'sort')
						return ("<div style='max-height: 32px; overflow: hidden;'>" + row.email.htmlEscaped() + "</div>");
					return ("<div style='max-height: 32px; overflow: hidden;'>" + row.email.htmlEscaped() + "</div>");
				}
			}, {
				title : 'First Name',
				ciFilterType : 'text',
				data : 'firstName',
				render : function(data, type, row)
				{
					if (type == 'sort')
						return ("<div style='min-width: 70px !important; max-height: 32px; overflow: hidden; -ms-text-overflow: ellipsis;text-overflow: ellipsis;'>" + data.htmlEscaped() + "</div>");
					return ("<div style='min-width: 70px !important; max-height: 32px; overflow: hidden; -ms-text-overflow: ellipsis;text-overflow: ellipsis;'>" + data.htmlEscaped() + "</div>");
				},
				createdCell : function(td, cellData, rowData, row, col)
				{
				},
			}, {
				title : 'Last Name',
				ciFilterType : 'text',
				data : 'secondName',
				render : function(data, type, row)
				{
					if (type == 'sort')
						return ("<div style='max-height: 32px; overflow: hidden;'>" + data.htmlEscaped() + "</div>");
					return ("<div style='max-height: 32px; overflow: hidden;'>" + data.htmlEscaped() + "</div>");
				},
				createdCell : function(td, cellData, rowData, row, col)
				{
					$(td).attr('style', 'min-width: 60px;');
				},
			}, {
				title : 'Company',
				ciFilterType : 'text',
				data : 'companyName',
				render : function(data, type, row)
				{
					if (type == 'sort')
						return ("<div style='max-height: 32px; overflow: hidden;'>" + row.company.htmlEscaped() + "</div>");
					return ("<div style='max-height: 32px; overflow: hidden;'>" + row.company.htmlEscaped() + "</div>");
				},
			}, {
				title : 'Address',
				ciFilterType : 'text',
				data : 'address',
				render : function(data, type, row)
				{
					if (type == 'sort')
						return ("<div style='max-height: 32px; overflow: hidden;'>" + data.htmlEscaped() + "</div>");
					return ("<div style='max-height: 32px; overflow: hidden;'>" + data.htmlEscaped() + "</div>");
				},
			}, {
				title : 'Action',
				ciFilterType : 'clear',
				orderable : false,
				render : function(data, type, row)
				{
					if (type == "display")
					{
						return generateContactsButton(row);
					}
					return null;
				},
			} ];

		contactsTable = $('#contacts-table').DataTable({
			"autoWidth" : false,
			"scrollY" : false,
			"scrollX" : true,
			"scrollCollapse" : true,
			// "fixedColumns": {
			// "leftColumns": 1,
			// "rightColumns": 1,
			// "sHeightMatch": "none"
			// },
			"processing" : true,
			"serverSide" : true,
			"ajax" : {
				"url" : "/api/contacts/data",
				"method" : "POST",
				"data" : function(d)
				{
					// d.endDate = endDate.format('YYYY-MM-DD ZZ');
					// d.startDate = startDate.format('YYYY-MM-DD ZZ');

					// d.statusFilter = filter.join();
				}
			},
			"createdRow" : function(row, data, dataIndex)
			{
				$(row).attr("data-element-id", data.id);
				// showNotificationMessage(true, 'Working...');
				NProgress.start();
				showScrollButtons();
			},
			"aoColumns" : contactColumnDefs,
			"colReorder" : {
				"fixedColumnsLeft" : 1,
				"fixedColumnsRight" : 1,
				"realtime" : false,
				"fnReorderCallback" : function()
				{
					setupVisibility();
					var show = $('.dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow').size() != 0;
					rebuildContactsColumnFilters(show);
				}
			},
			"oLanguage" : {
				"sLengthMenu" : "_MENU_",
				"sSearch" : "",
				"oPaginate" : {
					"sPrevious" : "",
					"sNext" : ""
				},
				"sInfo" : '<i class="fa fa-spin fa-cog dashboardLoadingIndicator"></i> Showing _START_ to _END_ of _TOTAL_ contacts',
				"sInfoFiltered" : ' - filtered from _MAX_ contacts',
				"sInfoEmpty" : "No contacts found",
			},
			"aaSorting" : [ [ 0, 'asc' ] ],
			"aLengthMenu" : [ [ 10, 20, 40, 70, 100 ], [ 10, 20, 40, 70, 100 ] ],
			// set the initial value
			"iDisplayLength" : proposalsToShow,
			"sDom" : domHead + domBody + domFooter,
			"stateSave" : true,
			"stateDuration" : -1,
			"stateSaveCallback" : function(settings, data)
			{
				// Load saving to the database here (save state of users
				// datatable)
				var dashboardData = JSON.stringify(data);
				// $.ajax({
				// url: "/api/dashboard_state_save",
				// data: {
				// 'data': dashboardData,
				// },
				// dataType: "json",
				// type: "POST",
				// success: function () {
				//
				//
				// }
				// });
			},
			"stateLoadCallback" : function(settings)
			{
				var o;
				// $.ajax({
				// "url": "/api/dashboard_state_load",
				// "async": false,
				// "dataType": "json",
				// "success": function (json) {
				// o = json;
				// }
				// });
				return o;
			},
			"initComplete" : function()
			{
				// Check the width of the main table and reset the width of the
				// right and left tables
				var columnHeightTimer;
				clearTimeout(columnHeightTimer);
				columnHeightTimer = setTimeout(formatTablecolumns, 200);
			},
		});

		contactsTable.on('draw.dt', function()
		{
			blurTable(false, false)
			// showNotificationMessage(false);
			showLoadingCog(false);
			NProgress.done();
		});

		contactsTable.on('column-visibility.dt', function(e, settings, column, state)
		{
			rebuildContactsColumnFilters();
		});
		if ($('.dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow').size() == 0)
		{
			rebuildContactsColumnFilters();
		}

		// modify table search input
		// $('#contacts-table_wrapper .dataTables_filter').prepend('<label
		// class="search-label" for="search-proposals"><span class="search-icon
		// fa fa-search"></span></label>');

		var filterButton = $('<button class="ripple btn btn-default columnFilterCount" title="Filter"><i class="fa fa-filter"></i> <span id="filterLabel">Filter</span></button>').on('click', function()
		{
			$(".toggleColumnFilters").click();
		});
		$('#contacts-table_wrapper .dataTables_filter').prepend(filterButton);
		$('#contacts-table_wrapper .dataTables_filter input')
		// .attr('id', 'searchProposals').addClass("search-box
		// form-control").attr("placeholder", "Search...")
		.hide();

		// modify table paginate dropdown
		$('#contacts-table_wrapper .dataTables_length select').addClass("form-control");
		$('#contacts-table_wrapper .dataTables_length').closest(".form-group").before($("<span>").text('Show')).after($("<span>").text('proposals '));
		$('#contacts-table_wrapper .dataTables_length select').change(function(event)
		{
			var selected = $(':selected', this).val();
		});

		// visible columns
		setupVisibility();

		// Add the download data button
		$('#contacts-table_wrapper #actionsHolder').attr("class", "form-group").append($('<button>').attr("class", "btn btn-default downloadTableData guide-export-button").attr("title", "Download data").append($('<i>').attr("class", "qc qc-download")).append(" Export Proposal Data"));

		$("#contacts-table_wrapper .dataTables_scrollHead").on("change keyup", "tr.searchRow input, tr.searchRow select", function(e)
		{
			var searchCellIndex = $(this).closest("th").index();
			var headerCell = $('#contacts-table_wrapper .dataTables_scrollHeadInner > table > thead > tr').find("th")[searchCellIndex];

			contactsTable.column(headerCell).search($(this).val());
			contactsTable.page(0);

			var count = countContactsColumnFilters();
			if (count > 0)
			{
				var title = count + " Filters";
				$("#filterLabel").text(title)
				$(".columnFiltersEnabled").show();
			}
			else
			{
				$("#filterLabel").text("Filter");
				$(".columnFiltersEnabled").hide();
			}
			redrawContactsTable();
		});
		$("#contacts-table_filter").on("click", function(e)
		{
			contactsTable.columns().search('');
			var show = $('#contacts-table_wrapper .dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow').size() == 0;
			rebuildContactsColumnFilters(show);
		});

	};

	var generateContactsButton = function(data)
	{
		var test = '<td><div class="btn-group">' + '<button type="button" class="btn btn-default edit-contacts-action">' + '<i class="fa fa-pencil-square" aria-hidden="true"></i> Edit</button>' + '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + '<span class="caret"></span>' + '<span class="sr-only">Toggle Dropdown</span>' + '</button>' + '<ul class="dropdown-menu">' + '<li><a href="#" class="delete-contact-action">Delete</a></li></ul>' + '</div>' + '</td>'

		return test;
	}

	function rebuildContactsColumnFilters(force)
	{
		var $thead = $('#contacts-table_wrapper .dataTables_scrollHeadInner > table > thead');
		var $search = $('#contacts-table_wrapper .dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow');

		if ($search.size() > 0)
		{
			$('#contacts-table_wrapper .DTFC_ScrollWrapper').height("-=47");
			$('#contacts-table_wrapper .dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow').remove();
			$('#contacts-table_wrapper .DTFC_RightHeadWrapper > table > thead.searchHead > tr.searchRow').remove();
			$('#contacts-table_wrapper .DTFC_LeftHeadWrapper > table > thead.searchHead > tr.searchRow').remove();
			$search.remove();
			contactSearchHidden = true;

			// checkRowHeight();
			checkContactRowHeight();
		}
		//
		// var $filterCell1 = $('> tr > th:first-child', $thead).html('');
		// var $filterCell2 = $('> tr > th:last-child', $thead).html('');
		// $filterCell1.append($("<button>").css({
		// 'border-radius': '33px'
		// }).attr('class', 'btn btn-xs btn-block btn-default scroll-table-left
		// ripple').append($('<i class="fa fa-arrow-left">')));
		//
		// $filterCell2.append($('<span>').css({
		// 'position': 'absolute',
		// 'padding-top': '6px'
		// }));
		//
		// $filterCell2.append($("<button>").css({
		// 'border-radius': '33px',
		// 'width': '27px',
		// 'float': 'right'
		// }).attr('class', 'btn btn-xs btn-block btn-default scroll-table-right
		// ripple').append($('<i class="fa fa-arrow-right">')));

		if (force == true || countContactsColumnFilters() > 0)
		{
			var $searchHead = $('<thead>').attr('class', 'searchHead');
			var $searchRow = $("<tr>").attr('class', 'searchRow');
			contactsTable.columns().every(function(i, tableLoop, columnLoop)
			{
				var dtColumn = this;
				if (dtColumn.visible())
				{
					var $cell = $("<th>");
					if (i >= 0)
					{
						var colDef;
						for (var w = 0; w < contactColumnDefs.length; w++)
						{
							if (contactColumnDefs[w].title == $(dtColumn.header()).text())
							{
								colDef = contactColumnDefs[w];
							}
							else if (contactColumnDefs[w].title.indexOf($(dtColumn.header()).text()) !== -1)
							{
								colDef = contactColumnDefs[w];
							}
						}
						var $search = undefined;

						// $(dtColumn.header()).text()

						if (colDef.ciFilterType == 'text')
						{
							$search = $('<input type="text" class="form-control input-sm"/>');
							$search.val(dtColumn.search());
						}
						else if (colDef.ciFilterType == 'dropdown')
						{
							$search = $('<select class="form-control input-sm"/>');
							$search.append($("<option>").attr('value', '').attr('selected', 'selected').text('Filter...'));
							$(colDef.ciDataValues).each(function(i, e)
							{
								var $opt = $("<option>");
								$opt.text(e.label);
								$opt.attr('value', e.value);
								$search.append($opt);
							});

							$search.val(dtColumn.search());
						}
						else if (colDef.ciFilterType == 'clear')
						{
							$search = $("<button>").attr('class', 'btn btn-sm btn-danger clearColumnFilters columnFiltersEnabled').append($('<i class="fa fa-eraser">'), " Clear Filters");

							$search.val(dtColumn.search());
						}

						if ($search != undefined)
						{
							var label = $(dtColumn.header()).text();
							$search.attr('placeholder', 'Filter...');

							$cell.append($search);
						}
					}

					$searchRow.append($cell);
					$searchHead.append($searchRow);
				}
			});


			var $smallSearchHeadRight = $('<thead>').attr('class', 'searchHead');
			var $smallSearchRowRight = $("<tr>").attr('class', 'searchRow');
			var $cellRight = $("<th>").append($("<button>").attr('class', 'contacts-table_clearColumnFilters btn btn-sm btn-danger clearColumnFilters columnFiltersEnabled').append($('<i class="fa fa-eraser">'), " Clear Filters"));
			$smallSearchRowRight.append($cellRight)
			$smallSearchHeadRight.append($smallSearchRowRight);


			var $smallSearchHead = $('<thead>').attr('class', 'searchHead');
			var $smallSearchRow = $("<tr>").attr('class', 'searchRow');
			var $cell = $("<th>").attr('style', 'height:48px');
			$smallSearchRow.append($cell)
			$smallSearchHead.append($smallSearchRow);

			$('#contacts-table_wrapper .dataTables_scrollHeadInner > table ').append($searchHead);
			$('#contacts-table_wrapper .DTFC_RightHeadWrapper > table ').append($smallSearchHeadRight);
			$('#contacts-table_wrapper .DTFC_LeftHeadWrapper > table ').append($smallSearchHead);

			contactSearchHidden = false;

			$('.DTFC_ScrollWrapper').height("+=47");
			// checkRowHeight();
			checkContactRowHeight();


			$("#contacts-table_wrapper .searchRow").on("click", ".clearColumnFilters", function(e)
			{
				contactsTable.columns().search('');
				rebuildContactsColumnFilters(true);

				redrawContactsTable();
			});

			// $(".matchedProposals_clearColumnFilters").on("click", function(e)
			// {
			// proposalTable.columns().search('');
			// rebuildContactsColumnFilters(true);
			// redrawContactsTable();
			// });
		}

		var count = countContactsColumnFilters();
		if (count > 0)
		{
			var title = count + " Filters";
			$("#filterLabel").text(title)
			$(".columnFiltersEnabled").show();
		}
		else
		{
			$("#filterLabel").text("Filter");
			$(".columnFiltersEnabled").hide();
		}
	}

	var redrawContactsTableDelay = undefined;

	function redrawContactsTable()
	{
		// Display loading message to user
		// showNotificationMessage(true, "Working...");
		showLoadingCog(true);

		if (redrawContactsTableDelay != undefined)
		{
			clearTimeout(redrawContactsTableDelay);
		}
		redrawContactsTableDelay = setTimeout(function()
		{
			contactsTable && contactsTable.draw('page');
		}, 150);

	}

	function countContactsColumnFilters()
	{
		var count = 0;
		contactsTable.columns().every(function(i, tableLoop, columnLoop)
		{
			var dtColumn = this;
			if (dtColumn.visible() && dtColumn.search() != '')
			{
				++count;
			}
		});

		return count;
	}

	function checkContactRowHeight()
	{
		var $search = $('#contacts-table_wrapper .dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow');

		if (contactSearchHidden == true)
		{
			var rowHeight = $('#contacts-table_wrapper .dataTables_scrollHeadInner > table > thead > tr:first').height();
			$('#contacts-table_wrapper .DTFC_RightHeadWrapper').height(rowHeight - 1);
			$('#contacts-table_wrapper .DTFC_RightHeadWrapper > table > thead > tr:first').height((rowHeight));
			$('#contacts-table_wrapper .DTFC_LeftHeadWrapper').height(rowHeight);
			$('#contacts-table_wrapper .DTFC_LeftHeadWrapper > table > thead > tr:first').height((rowHeight));
			$('#contacts-table_wrapper .DTFC_RightBodyWrapper').attr('style', 'margin-top: 1px !important')
			$('#contacts-table_wrapper .DTFC_LeftBodyWrapper').attr('style', 'margin-top: 1px !important')
		}
		else
		{
			var rowHeight = $('#contacts-table_wrapper .dataTables_scrollHeadInner > table > thead > tr:first').height();
			var searchHeight = $('#contacts-table_wrapper .dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow').height();
			var headerheight = $('#contacts-table_wrapper .dataTables_scrollHead').height();
			$('#contacts-table_wrapper .DTFC_RightHeadWrapper').height(rowHeight - 1);
			$('#contacts-table_wrapper .DTFC_RightHeadWrapper > table > thead > tr:first').height((rowHeight));
			$('#contacts-table_wrapper .DTFC_LeftHeadWrapper').height(rowHeight);
			$('#contacts-table_wrapper .DTFC_LeftHeadWrapper > table > thead > tr:first').height((rowHeight));
			$('#contacts-table_wrapper .DTFC_RightBodyWrapper').attr('style', 'margin-top: ' + (searchHeight + 1) + 'px !important')
			$('#contacts-table_wrapper .DTFC_LeftBodyWrapper').attr('style', 'margin-top: ' + (searchHeight + 1) + 'px !important')
		}

		var rowWidth = $('#contacts-table_wrapper .dataTables_scrollHeadInner > table > thead > tr:first > th.first').width();
		$('#contacts-table_wrapper .DTFC_LeftHeadWrapper').width(rowWidth);
		$('#contacts-table_wrapper .DTFC_LeftBodyLiner > table').width(rowWidth);


		contactsTable.columns.adjust().draw();

	}

	contactsDatatable();
})
