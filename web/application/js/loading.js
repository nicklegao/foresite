var progressLoading = 0;
var progressLoaded = 0;
var progressClose = undefined;
var autoIncrement = undefined;
var saving_and_encrypting = "Encrypting and saving...";
var loading_and_decrypting = "Loading and decrypting...";

function startLoadingForeground(customizedText, counter)
{
	if (customizedText == undefined)
	{
		customizedText = 'Processing...';
	}
	if (counter == undefined)
	{
		counter = 1;
	}
	if (progressLoading == 0)
	{
		$(document).trigger("foregroundLoadingStarted");
		$('#foreground-loading').one('hidden.bs.modal', function(e)
		{
			setTimeout(function()
			{ // this set timeout is a workaround for some scenarios that the
				// main modal window loads after the foreground 2nd modal
				// window. Such as create revision complete scenario.

				$(document).trigger("foregroundLoadingComplete");
			}, 1000);
		});

		autoIncrement = setInterval(function()
		{
			// Initially make it visually look like 66%
			startLoadingForeground(customizedText, 1);
			stopLoadingForeground(1, false);// Increase the progress bar to make
			// it look like its loading.
		}, 500);
	}

	progressLoading += counter;
	$('#foreground-loading').find('#what-we-do').text(customizedText).end().modal('show');

	if (progressLoading == 1)
	{
		// Looks better this way
		setPercentageLoaded(5);
	}
	else
	{
		var percentageLoaded = ((progressLoaded + 1) / progressLoading) * 100;
		setPercentageLoaded(percentageLoaded);
	}
}

function stopLoadingForeground(counter, resetTimeout)
{
	if (counter == undefined)
	{
		counter = 1;
	}
	progressLoaded += counter;

	if (progressClose != undefined && (resetTimeout != undefined && resetTimeout == true))
	{
		clearTimeout(progressClose);
	}

	progressClose = setTimeout(function()
	{
		if (progressLoaded >= progressLoading)
		{
			$('#foreground-loading').modal('hide');
			progressLoading = 0;
			progressLoaded = 0;
			progressClose = undefined;
			setPercentageLoaded(0);

			clearInterval(autoIncrement);
		}
	}, 1000);

	var percentageLoaded = ((progressLoaded) / progressLoading) * 100;
	setPercentageLoaded(percentageLoaded);
}

function setPercentageLoaded(percentageLoaded)
{
	$('#foreground-loading .progress-bar').css('width', percentageLoaded + "%").attr('aria-valuenow', parseInt(percentageLoaded));
	$('#foreground-loading .progress-bar-text').text(parseInt(percentageLoaded) + "%");
}
