var $messages = $('.messages-content');
var d;
var h;
var m;
var i = 0;
var webChatToken;
var chatroom;
var firstName;
var lastName;
var clientName;
var textLength = 0;
var mapGlobal = [];
var firstMessageSent = false;
var messageTimer;


$(window).load(function()
{
});

$(document).ready(function()
{
	$(".acceptProposalAction").click(acceptProposal);
	$(".acceptReviewProposalAction").click(acceptReviewProposal);

	$("#documentSectionBrowser a, .toc-container a").click(autoscrollToSection);

	$(".back-to-top").click(backToTop);

	$('#signUpFName').on('keyup', function(e)
	{
		if (($('#signUpFName').val().length > 0) && ($('#signUpLName').val().length > 0))
		{
			$('.chatChat-submit')[0].disabled = false;
		}
		else
		{
			$('.chatChat-submit')[0].disabled = true;
		}
	});
	$('#signUpLName').on('keyup', function(e)
	{
		if (($('#signUpFName').val().length > 0) && ($('#signUpLName').val().length > 0))
		{
			$('.chatChat-submit')[0].disabled = false;
		}
		else
		{
			$('.chatChat-submit')[0].disabled = true;
		}
	});

	$('.view-option-button').click(function() {
		if($('.view-option-button').data('open')) {
			$('.view-option-button').data('open', false);

			console.log(viewHiddenURL);
			window.location.href = viewHiddenURL;
		} else
			$('.view-option-button').data('open', true);
	});

	$(document).click(function() {
		if($('.view-option-button').data('open')) {
			$('.view-option-button').data('open', false);

			console.log(viewHiddenURL);
			window.location.href = viewHiddenURL;
		}
	});

	$( '.view-option-dropdown a' ).on( 'click', function( event ) {

		var $target = $( event.currentTarget ),
			val = $target.attr( 'data-value' ),
			$inp = $target.find( 'input' ),
			idx;

		if ( ( idx = options.indexOf( val ) ) > -1 ) {
			options.splice( idx, 1 );
			viewHiddenURL = viewHiddenURL.replace(val, "");
			setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
		} else {
			options.push( val );
			if(viewHiddenURL.indexOf(val) == -1) {
				viewHiddenURL += val;
			}
			setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
		}

		$( event.target ).blur();
		console.log( options );
		return false;
	});

	$('#startLiveChat').on('click', function()
	{

		$.ajax({
			url : "/api/mobile/setupProposalChatroom",
			data : {
				'chatOwner' : ownerId,
				'proposalId' : proposalId,
				'clientName' : $('#signUpFName').val() + " " + $('#signUpLName').val()
			},
			type : "POST",
			success : function(data)
			{
				chatroom = data;
				$.ajax({
					url : "/api/mobile/getAllMessagesWeb",
					data : {
						'proposalID': proposalId
					},
					type : "GET",
					success : function(data)
					{
						$('.mCSB_container').html("");
						for (var m in data) {
							var messageOwner = data[m].attr_messageowner;
							var proposalOwner = "" + ownerId;
							var arrayLength = "" + (data.length - 1);
							if (messageOwner === proposalOwner && (m !== arrayLength)) {
								$('<div class="message message-recieved" id="' + data[m].attr_headline + '"><figure class="avatar"><img src="/application/images/user.png"></figure>' + data[m].attr_messagetext + '</div>').appendTo($('.mCSB_container')).addClass('new');
							} else if (messageOwner !== proposalOwner && (m !== arrayLength)) {
								$('<div class="message message-personal message-sent new" id="' + data[m].attr_headline + '">' + data[m].attr_messagetext + '</div>').appendTo($('.mCSB_container')).addClass('new');
							} else if (messageOwner === proposalOwner && (m === arrayLength)) {
								$('<div class="message message-recieved" id="' + data[m].attr_headline + '">' + data[m].attr_messagetext + '</div>').appendTo($('.mCSB_container')).addClass('new');
							} else {
								var message = "";
								if (data[m].attrcheck_delivered === 1 && data[m].attrcheck_read !== 1)
								{
									var deliveredDate = new Date(data[m].attrdate_deliveredtime);

									message = '<div class="timestamp message-sent delivered">' + deliveredDate.getHours() + ':' + deliveredDate.getMinutes() + ' Delivered - Not Seen</div>';

								} else if (data[m].attrcheck_read === 1)
								{
									var readDate = new Date(data[m].attrdate_messagedate);

									message = '<div class="timestamp message-sent delivered">' + readDate.getHours() + ':' + readDate.getMinutes() + ' Delivered - Read</div>';

								}
								$('<div class="message message-personal message-sent new" id="' + data[m].attr_headline + '">' + data[m].attr_messagetext + message + '</div>').appendTo($('.mCSB_container')).addClass('new');
							}
							updateScrollbar();
						}
						showHideDiv("avenue-messenger", true);
						showHideDiv("chat", false);
						showHideDiv("signUp", true);
						firbaseSync();
					}
				});
			}
		});
	});

	$(window).on('keydown', function(e)
	{
		if (e.which == 13)
		{
			msg = $('.message-input').val();
			if ($.trim(msg) == '')
			{
				return false;
			}
			$('.message-submit')[0].disabled = true;
			sendMessage();
			return false;
		}
	});

	$('.message-input').on('keydown', function(e)
	{
		if (firstMessageSent)
		{
			clearTimeout(messageTimer);
		}
	});

	$('.button').click(function()
	{
		$('.menu .items span').toggleClass('active');
		$('.menu .button').toggleClass('active');
	});

	$('.chatChat-submit').on('click', function()
	{
		clientName = $('#signUpFName').val() + " " + $('#signUpLName').val();
		showHideDiv("chat", true);
		showHideDiv("signUp", false);
	});

	$('.message-submit').click(function()
	{
		msg = $('.message-input').val();
		if ($.trim(msg) == '')
		{
			return false;
		}
		$('.message-submit')[0].disabled = true;
		sendMessage();
	});

	$('.buttonLiveChat').on('click', function()
	{
		// $('.avenue-messenger').fadeOut("swing");
		$('.avenue-messenger').animate({
			opacity : 0.0,
			height : "toggle"
		}, 500, function()
		{
			// Animation complete.
			$('.message-input').val(null);
			$('#liveChatInputField').height(14);
			showHideDiv("avenue-messenger", false);
		});
	});

	$messages.mCustomScrollbar({
		theme : "dark",
		scrollButtons : {
			enable : true
		}
	});
	loadDestinationSectionImage();
	if (paymentNeeded)
	{
		initStripe();
	}

	// initgooglemaps();

});
function loadDestinationSectionImage()
{
	console.log("loadDestinationSectionImage");
	$.each($('.destination-poi'), function(i, item)
	{
		if ($(item).attr('poi-id'))
		{
			var attachmentId = $(item).attr('poi-id');
			$.ajax('/api/proposal/attachment', {
				type : "POST",
				dataType : 'json',
				data : {
					attachmentId : attachmentId
				}
			}).done(function(data, textStatus, jqXHR)
			{
				if (undefined != data.attachmentPath)
				{
					var thumb = $('<img>').addClass('poi-thumb');
					thumb.attr('src', data.attachmentPath);
					thumb.insertBefore($(item));
				}
			});
		}
		else
		{
			if ($(item).attr('poi-image'))
			{
				var thumb = $('<img>').addClass('poi-thumb');
				thumb.attr('src', $(item).attr('poi-image'));
				thumb.insertBefore($(item));
			}
		}
	});
}
function textAreaAdjust(o)
{
	if (($('#liveChatInputField').val().length / 36) % 1 == 0)
	{
		if (textLength > $('#liveChatInputField').val().length)
		{
			o.style.height = ($('#liveChatInputField').height() - 25) + 'px';
		}
		else
		{
			if (($('#liveChatInputField').height() + 25) < 120)
			{
				o.style.height = ($('#liveChatInputField').height() + 25) + 'px';
			}
			else
			{
				o.style.height = '120px';
			}
		}
		textLength = ($('#liveChatInputField').val().length);
	}
}

function showHideDiv(divId, show)
{
	const
	div = document.querySelector('#' + divId);
	if (show)
	{
		div.style = "display: visible";
	}
	else
	{
		div.style = "display: none";
	}
}

function updateScrollbar()
{
	$messages.mCustomScrollbar("update").mCustomScrollbar('scrollTo', 'bottom', {
		scrollInertia : 10,
		timeout : 0
	});
}

function setDate()
{
	m = null;
	d = new Date()
	if (m != d.getMinutes())
	{
		m = d.getMinutes();

		$('<div class="timestamp message-sent">' + d.getHours() + ':' + m + ' Sent</div>').appendTo($('.message:last'));
		// $('<div
		// class="checkmark-read">&check;</div>').appendTo($('.message:last'));
	}
	else
	{
		$('<div class="timestamp message-sent">Sent</div>').appendTo($('.message:last'));
	}
}

function setDelivered()
{
	var items = $('.timestamp.message-sent');
	for (var i = 0; items.length; i++)
	{
		if (!($(items[i]).hasClass('delivered')))
		{
			m = null;
			d = new Date()
			if (m != d.getMinutes())
			{
				m = d.getMinutes();
				$(items[i]).replaceWith('<div class="timestamp message-sent delivered">' + d.getHours() + ':' + m + ' Delivered - Not Seen</div>');
			}
			else
			{
				$(items[i]).replaceWith('<div class="timestamp message-sent delivered">Delivered - Not Seen</div>');
			}
			return false;
		}
	}
}

function setRead()
{
	$('<div class="checkmark-read">Read</div>').appendTo($('.message:last'));

	var items = $('.timestamp.message-sent.delivered');
	for (var i = 0; items.length; i++)
	{
		if (!($(items[i]).hasClass('read')))
		{
			m = null;
			d = new Date()
			if (m != d.getMinutes())
			{
				m = d.getMinutes();
				$(items[i]).replaceWith('<div class="timestamp message-sent delivered read">' + d.getHours() + ':' + m + ' Delivered - Read</div>');
			}
			else
			{
				$(items[i]).replaceWith('<div class="timestamp message-sent delivered read">Delivered - Read</div>');
			}
			return false;
		}
	}
}

function insertMessage()
{
	msg = $('.message-input').val();
	if ($.trim(msg) == '')
	{
		return false;
	}
	$('<div class="message message-personal message-sent">' + msg + '</div>').appendTo($('.mCSB_container')).addClass('new');
	setDate();
	$('.message-input').val(null);
	$('#liveChatInputField').height(14);
	updateScrollbar();
	$('.message-submit')[0].disabled = false;
}

function sendMessage()
{
	var room = chatroom.chatRoomID;

	$.ajax({
		url : "/api/mobile/sendMessage",
		data : {
			'messageOwner' : clientName,
			'roomID' : room,
			'message' : $('.message-input').val()
		},
		type : "POST",
		success : function(data)
		{
			if (data)
			{
				setDelivered()
			}
			else
			{
				alert("MESSAGE DID NOT SEND (REPLACE ME HEHE)");
			}
		}
	});
	insertMessage();
	firstMessageSent = true;
	sendEmailNotificationOnTime();
}

function sendEmailNotificationOnTime()
{
	if (firstMessageSent)
	{
		clearTimeout(messageTimer);
		messageTimer = setTimeout(function () {
			sendEmailNotification();

		}, 10000);
	}
}


function sendEmailNotification()
{
	var room = chatroom.chatRoomID;
	$.ajax({
		url : "/api/mobile/sendEmailNotification",
		data : {
			'chatUserName' : clientName,
			'roomID' : room,
			'chatUserCompany': clientCompanyName
		},
		type : "POST",
		success : function(data)
		{
		}
	});
}

function backToTop(event)
{
	$("html, body").animate({
		scrollTop : 0
	}, "slow");
}

function autoscrollToSection(event)
{
	event.preventDefault();
	// 1. documentSectionBrowser tab has data-target
	// 2. table of contents has href
	// 3. headings in content has data-encoded.
	var targetSection = $(event.target).attr("data-target") || $(event.target).attr("href");
	var offset = $(event.target).attr("data-encoded") ? 50 : 10;
	$("html, body").animate({
		scrollTop : $(targetSection).offset().top - offset
	}, "slow");
}

function acceptReviewProposal()
{
	startLoadingForeground();

	$.ajax({
		type : "POST",
		data : {
			"proposal" : $("#param-proposal-id").val(),
			"email" : $("#param-email").val(),
			"v-key" : $("#param-v-key").val()
		},
		url : '/api/secure/proposal/acceptReview',
	}).done(function(data, textStatus, jqXHR)
	{
		stopLoadingForeground();
		if (data != "OK")
		{
			alert(data);
		}
		else
		{
			$(document).one('foregroundLoadingComplete', function()
			{
				swal("Your Proposal Approval Has Been Sent", "", "success");
				swal({
					title: "Your Proposal Approval Has Been Sent",
					text: "",
					type: "success"
				},
				function(isConfirm) {
					$("#proposalReviewAcceptModal").modal('hide');
					window.close();
				});
			});
		}
	});
}

function acceptProposal()
{
	startLoadingForeground();

	$.ajax({
		type : "POST",
		data : {
			"proposal" : $("#param-proposal-id").val(),
			"email" : $("#param-email").val(),
			"v-key" : $("#param-v-key").val()
		},
		url : '/api/secure/proposal/accept',
	}).done(function(data, textStatus, jqXHR)
	{
		stopLoadingForeground();
		if (data != "OK")
		{
			alert(data);
		}
		else
		{
			$(document).one('foregroundLoadingComplete', function()
			{
				var redirectLink = $("#accept-redirect-link").val();
				if (redirectLink != "null" && !!redirectLink.trim())
				{
					window.location.href = redirectLink;
				}
				else
				{
					showPaymentUI();
				}
			});
		}
	});
	$("#proposalAcceptModal").modal('hide');
}

function initStripe()
{
	var stripe = Stripe(stipePk);

	// Create an instance of Elements
	var elements = stripe.elements();

	// Custom styling can be passed to options when creating an Element.
	// (Note that this demo uses a wider set of styles than the guide below.)
	var style = {
		base : {
			color : '#32325d',
			lineHeight : '18px',
			fontFamily : '"Helvetica Neue", Helvetica, sans-serif',
			fontSmoothing : 'antialiased',
			fontSize : '16px',
			'::placeholder' : {
				color : '#aab7c4'
			}
		},
		invalid : {
			color : '#fa755a',
			iconColor : '#fa755a'
		}
	};

	// Create an instance of the card Element
	var card = elements.create('card', {
		style : style
	});

	// Add an instance of the card Element into the `card-element` <div>
	card.mount('#card-element');

	// Handle real-time validation errors from the card Element.
	card.addEventListener('change', function(event)
	{
		var displayError = document.getElementById('card-errors');
		if (event.error)
		{
			displayError.textContent = event.error.message;
		}
		else
		{
			displayError.textContent = '';
		}
	});

	// Handle form submission
	var form = document.getElementById('payment-form');
	form.addEventListener('submit', function(event)
	{
		event.preventDefault();

		stripe.createToken(card).then(function(result)
		{
			if (result.error)
			{
				// Inform the user if there was an error
				var errorElement = document.getElementById('card-errors');
				errorElement.textContent = result.error.message;
			}
			else
			{
				// Send the token to your server
				qcPayment(result.token);
			}
		});
	});

}

function showPaymentUI()
{
	if (!paymentNeeded)
	{
		swal("Your Order Approval Has Been Sent", "", "success");
		return;
	}
	$("#qcPaymentModal").modal('show');
}

function qcPayment(token)
{
	$.ajax({
		url : "/api/free/payment/charge",
		data : {
			'proposalId' : proposalId,
			'token' : token,
		},
		type : "POST",
		success : function(data)
		{
			$("#qcPaymentModal").modal('hide');
			swal({
				title : data.success ? "Success" : "Error",
				text : data.message,
				type : data.success ? "success" : "error",
			}, function()
			{
				window.location.reload();
			});
		}
	});
}
