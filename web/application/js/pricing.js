var pricingDetails = [];

var tableBody = $("[id='pricingTable']");

var columnSort = [];

var discountsByPercentage;
var discountsByThreshold;
var globalThresholdValueMultiplier = 0;

var ROW_COUNTER = 1;

$(function()
{
	initDiscounts();

	$(document).on('change', '#estimatedValue', function()
	{
		$('.customPricingRows tr[data-type="price"] .unitCostSelector').change();
	});

	$('.js-switch:visible').each(function()
	{
		var switchery = new Switchery(this, {
			size : 'small'
		});
	});

	$('.noTitle-switch:visible').each(function()
	{
		if (!($(this).hasClass('switchActive')))
		{
			var switcheryHide = new Switchery(this, {
				color : '#fc0d00',
				size : 'small'
			});

			$(this).siblings(".switchery").css("width", "60px").prepend("<span title='Your title will be shown within the PDF'>Show</span>").find("small").css("left", "0px");
			$(this).addClass('switchActive');

			$(this).change(function()
			{
				if (!($(this).is(':checked')) && $('#globalTitleHidden').is(':checked'))
				{
					$('#globalTitleHidden').click();
				}
				if ($(this).is(':checked'))
				{
					$(this).siblings(".switchery").find("span").text('Hide');
					$(this).siblings(".switchery").find("span").attr('title', 'Your title will be hidden inside the PDF');
					$(this).siblings(".switchery").find("span").addClass('hideLabel');
				}
				else
				{
					$(this).siblings(".switchery").find("span").text('Show');
					$(this).siblings(".switchery").find("span").attr('title', 'Your title will be shown within the PDF');
					$(this).siblings(".switchery").find("span").removeClass('hideLabel');
				}
			}).change();
		}
	});

	$(document).on('mouseover', '.hoverhint', function()
	{
		var targets = $(this).attr('data-hover').split(',');
		$.each(targets, function(i, e)
		{
			$(e).addClass('hoverTarget');
		})
	}).on('mouseout', '.hoverhint', function()
	{
		var targets = $(this).attr('data-hover').split(',');
		$.each(targets, function(i, e)
		{
			$(e).removeClass('hoverTarget');
		})
	});

	$(document).on('click', '.pricing-droppable', function()
	{
		$('.pricing-droppable:not(#htmlFragments .pricing-droppable)').removeClass('current');
		$(this).addClass('current');
	});

	$('.pricing-droppable:not(#htmlFragments .pricing-droppable)').each(function()
	{
		initPricingTable($(this));
	});
})

function initDiscounts()
{
	discountsByPercentage = discounts.slice(0).sort(function(a, b)
	{
		return a.percentage - b.percentage;
	});
	discountsByThreshold = discounts.slice(0).sort(function(a, b)
	{
		return b.threshold - a.threshold;
	});
}

function initMarginMenu($marginMenus)
{
	$marginMenus.each(function()
	{
		var margin_menu = $(this);
		$(".dropdown-menu", margin_menu).empty();
		var new_discount = $('<li>');
		new_discount.append($('<a>').text("New Discount or Margin"));
		new_discount.attr("data-percentage", null);
		new_discount.on("click", function()
		{
			pricingManagementAction();
		});

		var remove_discount = $('<li>');
		remove_discount.append($('<a>').text("Default Discount or Margin"));
		remove_discount.attr("data-percentage", null);
		remove_discount.on("click", function()
		{
			$(".discount-label", margin_menu).html("Default Discount or Margin");
			margin_menu.attr("data-percentage", null);
			recalculatePricingTables();
		});

		var no_discount = $('<li>');
		no_discount.append($('<a>').text("No Discount or Margin"));
		no_discount.attr("data-percentage", 0);
		no_discount.on("click", function()
		{
			$(".discount-label", margin_menu).html("No Discount or Margin");
			margin_menu.attr("data-percentage", 0);
			recalculatePricingTables();
		});

		$(".dropdown-menu", margin_menu).append(remove_discount);
		$(".dropdown-menu", margin_menu).append(no_discount);

		var discountCounter = 0;
		discountsByPercentage.map(function(item, index)
		{
			if (typeof item.threshold == 'undefined' && item.percentage)
			{
				discountCounter++;
				var li = $('<li>');
				var text = item.label + " (" + item.percentage + "%)";
				li.append($('<a>').text(text));
				li.attr("data-percentage", item.percentage);
				li.on("click", function()
				{
					$(".discount-label", margin_menu).html(text);
					margin_menu.attr("data-percentage", item.percentage);
					recalculatePricingTables();
				});
				$(".dropdown-menu", margin_menu).append(li);
			}
		});
		if (discountCounter > 0)
		{

		}
		$(".dropdown-menu", margin_menu).append(new_discount);
	})

}

function initPricingTable($panel)
{

	$("[id='pricingTable']", $panel).on('blur', '.qtyChooser', positiveIntOnly);
	$("[id='pricingTable']", $panel).on('blur', '.planTermSelector', positiveIntOnly);
	$("[id='pricingTable']", $panel).on('blur', '.unitCostSelector', floatOnly);


	$("[id='termDropDown']", $panel).on("click", "li", function(event)
	{
		$(this).siblings().removeClass("selected");
		$(this).addClass("selected");
		var term = $(this).text();
		$("[id='termType']", $panel).text(term);
		$("[id='termFooter']", $panel).text(terms[term]);
		$.cookie("pricing-term", $(this).text());
	});

	if (userPermissions.permissions.ATTRCHECK_applyPricingDicounts)
	{
		initMarginMenu($("[id='margin-discounts']", $panel));
	}

	$(".addCustomPricingAction", $panel).click(function()
	{
		addPricingRow($panel.find('[id="pricingTable"]'));
	});
	$(".addProductSectionAction", $panel).click(function()
	{
		addSectionRow($panel.find('[id="pricingTable"]'));
	});

	$("[id='pricingTable']", $panel).on('focus', '.rowDescription,.noteDescription,input', function()
	{
		$("[id='pricingTable']", $panel).find('.focus').removeClass('focus');
		$(this).closest('tr').addClass('focus');
	});

	clearSorting($panel);
	$("thead th[sortable]", $panel).on('click', function(e)
	{
		if ($(e.target).closest('div').attr('id') == 'termTypeDiv')
		{// to avoid table sorting when term drop down is clicked
			return;
		}

		var field = $(this).attr("sortable");

		if (event.ctrlKey || event.metaKey)
		{
			if ($(this).hasClass("sorted"))
			{
				$(this).removeClass("sorted");
				delete columnSort[field];
			}
			else
			{
				$(this).addClass("sorted");
				columnSort[field] = $(this).attr("order");
			}
		}
		else
		{
			if ($(this).hasClass("sorted"))
			{
				var order = parseInt($(this).attr("order"));
				order *= -1;
				$(this).attr("order", order);
				columnSort[field] = $(this).attr("order");
			}
			else
			{
				clearSorting($panel);
				$(this).addClass("sorted");
				columnSort[field] = $(this).attr("order");
			}
		}
		resortTable($panel);
	});

	$('#hidePriceColumn, #hideTermColumn, #hideQtyColumn, #hideTotalPriceColumn, #hideTaxColumn, #hideTotalCostColumn, #hideGrandTotalPriceRow, #hideOneOffColumn, #hideOverallTotalPriceRow, #hideOnOffCostRow, #hideDiscountFromPriceRow, #hideMinimumMonthlyCost, #hideFullTermCost, #hideAdditionColumn, #hideCustomColumn1, #hideCustomColumn2, #hideCustomColumn3, #hideCustomColumn4, #hideCustomColumn5', $panel).each(function()
	{
		new Switchery($(this)[0], {
			color : '#fc0d00',
			size : 'small'
		});

		$(this).siblings(".switchery").css("width", "60px").prepend("<span class=''>Show</span>").find("small");

		$(this).change(function(e)
		{
			if ($(this).is(':checked'))
			{
				$panel.addClass('hide-' + $(e.target).attr('data-target-class'));
				$(this).siblings(".switchery").find("span").text('Hide');
				$(this).siblings(".switchery").find("span").addClass('hideLabel');
			}
			else
			{
				$(this).siblings(".switchery").find("span").text('Show');
				$(this).siblings(".switchery").find("span").removeClass('hideLabel');
				$panel.removeClass('hide-' + $(e.target).attr('data-target-class'));
			}
			if ($(this).is(':checked') && $(this).attr("id") == "hideTermColumn")
			{
				if (!$('#hideMinimumMonthlyCost', $panel).is(':checked'))
				{
					$('#hideMinimumMonthlyCost', $panel).click();
				}
				if (!$('#hideFullTermCost', $panel).is(':checked'))
				{
					$('#hideFullTermCost', $panel).click();
				}
			}
		});
	});

	if (hideTermsAlways)
	{
		$('#hideTermColumn', $panel).each(function()
		{
			if (!($(this).is(':checked')))
			{
				$(this).click();
			}
		});
	}

	if (useTax)
	{
		new Switchery($('#removeTax', $panel)[0], {
			color : '#fc0d00',
			size : 'small'
		});
		$('#removeTax', $panel).siblings(".switchery").css("width", "60px").prepend("<span class=''>Show</span>").find("small");
		$('#removeTax', $panel).change(function(e)
		{
			var $panel = $(this).closest('.pricing-droppable');
			if ($(e.target).is(":checked"))
			{
				$panel.addClass('hide-tax');
				$(this).siblings(".switchery").find("span").text('Hide');
				$(this).siblings(".switchery").find("span").addClass('hideLabel');
			}
			else
			{
				$panel.removeClass('hide-tax');
				$(this).siblings(".switchery").find("span").text('Show');
				$(this).siblings(".switchery").find("span").removeClass('hideLabel');
			}

			if (!$('#hideTaxColumn', $panel).is(':checked'))
			{
				$('#hideTaxColumn', $panel).click();
			}
			recalculatePricingTables();
		});
	}

}

function positiveIntOnly(e)
{
	if (isNaN(parseInt($(e.target).val())) || parseFloat($(e.target).val()) < 0)
	{
		$(e.target).val(0).change();
	}
}

function floatOnly(e)
{
	if (isNaN(parseFloat($(e.target).val())))
	{
		$(e.target).val(formatMoney(0)).change();
	}
}

function resortTable($pricingTable)
{
	$.each($(".customPricingRows", $pricingTable), function(index, section)
	{
		var rows = $("[data-type='price']", $(section)).get();
		var sortLib = undefined;
		$.each(Object.keys(columnSort), function(index, column)
		{
			function genericSortingFunction(rowA, rowB)
			{
				var fieldA, fieldB, valA, valB, valA = valB = "";
				fieldA = $('.' + column, rowA);
				fieldB = $('.' + column, rowB);
				valA = fieldA.is('span') ? fieldA.text() : (fieldA.is('[type="checkbox"]') ? fieldA.prop('checked') : fieldA.val().toLowerCase());
				valB = fieldB.is('span') ? fieldB.text() : (fieldB.is('[type="checkbox"]') ? fieldB.prop('checked') : fieldB.val().toLowerCase());
				valA = $.isNumeric(valA) ? parseFloat(valA) : valA;
				valB = $.isNumeric(valB) ? parseFloat(valB) : valB;
				if (valA < valB)
					return -1;
				if (valA > valB)
					return 1;
				return 0;
			}

			if (sortLib == undefined)
				sortLib = firstBy(genericSortingFunction, {
					ignoreCase : true,
					direction : parseInt(columnSort[column])
				});
			else
				sortLib = sortLib.thenBy(genericSortingFunction, {
					ignoreCase : true,
					direction : parseInt(columnSort[column])
				});
		});

		if(sortLib != undefined) {
            rows.sort(sortLib);
			$.each(rows, function(index, tr)
			{
				$(section).append(tr);
			});
        }

		updateSummariseRow(this);
	});
}

function clearSorting($pricingTable)
{
	$("thead th[sortable]", $pricingTable).attr("order", 1);
	$('thead th.sorted', $pricingTable).removeClass("sorted");
	columnSort = [];
}

function resizeTextField(textField)
{
	textField.css("height", "34px");
	textField.css("height", (textField.prop('scrollHeight') + 2) + "px");
}

function delayResize(textField)
{
	setTimeout(function()
	{
		resizeTextField(textField);
	}, 10);
}

function plansChanged()
{
	$('.pricing-droppable:not(#htmlFragments .pricing-droppable)').each(function()
	{
		var numberOfPlans = $("[id='pricingTable'] tbody tr", $(this)).length;
		if (numberOfPlans == 0)
		{
			$('.requiresPlans', $(this)).hide();
			$('.missingPlans', $(this)).show();
		}
		else
		{
			$('.requiresPlans', $(this)).show();
			$('.missingPlans', $(this)).hide();
		}
	})
}

function addPricingByIds(pricingRow)
{
	return $.ajax({
		type : "POST",
		url : '/api/pricing/loadPricesAndDependencies',
		traditional : true,
		data : {
			pricingOptions : pricingRow
		}
	}).done(function(data, textStatus, jqXHR)
	{
		/*
		 * Sushant: Unlike addPricingForPlan(...) we must to find the associated
		 * data row and add it specifcally for each pricingRow that we are
		 * restoring. This allows for adding multiple rows of the same Plan.
		 */
		$.each(pricingRow, function(i, pricingItemId)
		{
			$.each(data, function(j, plan)
			{
				var found = (plan.terms[pricingItemId] != undefined);
				if (found)
				{
					addPlansToDisplay([ plan ]);
				}
				return !found; // Break early
			});
		});

		// reload the plan dependencies (F&B and T&C).
		updatePlanDependencies();
	});
}

function addPricingRow($pricingTable)
{
	clearSorting($pricingTable);
	addRowToDisplay($pricingTable, {
		type : "price",
		label : "",
		unitPrice : '1.00',
		term : 0,
		qty : '1.00',
		isOneOff : true,
		totalPrice : '1.00',
		autoTax : true,
	}, true);
}

function addSectionRow($pricingTable)
{
	clearSorting($pricingTable);
	addRowToDisplay($pricingTable, {
		type : "title",
		label : ""
	}, true);
}

function addRowToDisplay($pricingTable, rowElement, newElement)
{
	// rowElement.pricingId
	// rowElement.breaks
	rowElement.originPrice = rowElement.unitPrice ? rowElement.unitPrice : "0.00";
	rowElement.autoTax = (typeof rowElement.autoTax == 'undefined') ? true : rowElement.autoTax;
	rowElement.taxPercent = rowElement.autoTax ? taxPercentage : rowElement.taxPercent;
	rowElement.originTaxPercent = taxPercentage;
	rowElement.addition = rowElement.addition ? rowElement.addition : 0;
	rowElement.summarisedTax = rowElement.summarisedTax ? rowElement.summarisedTax : "0.00";
	rowElement.summarisedAmount = rowElement.summarisedAmount ? rowElement.summarisedAmount : "0.00";
	rowElement.summarisedAddition = rowElement.summarisedAddition ? rowElement.summarisedAddition : "0.00";

	var template = addRowToDisplay1($pricingTable, rowElement, newElement);

	if (rowElement.pricingId && typeof rowElement.breaks == 'undefined')
	{
		getProductBreaks(rowElement, function()
		{
			initPricingRow(template, $pricingTable, rowElement, newElement);
		})
	}
	else
	{
		initPricingRow(template, $pricingTable, rowElement, newElement);
	}
}

function getProductBreaks(rowElement, callback)
{
	$.ajax({
		url : '/api/pricing/content',
		data : {
			id : rowElement.pricingId,
		},
	}).done(function(data)
	{
		rowElement.breaks = data.breaks;
		rowElement.originPrice = data.unitPrice
		callback();
	})
}

function addRowToDisplay1($pricingTable, rowElement, newElement)
{
	var template;
	if (rowElement.type != "price")
	{
		template = attachPricingSection($pricingTable, rowElement, newElement);
		var summariseCheck = $('.js-switch', template)
		summariseCheck.prop('checked', rowElement.isSummarised);
		if (rowElement.isSummarised)
		{
			var summariseField = $('.summaryAmount', template);
			summariseField.text(rowElement.summariseAmount);
			template.find('.summaryAmount').addClass('active');
		}
		var switchery = new Switchery(template.find('.js-switch').get(0), {
			size : 'small'
		});
		template.find('.js-switch').on('change', function()
		{
			if (this.checked)
			{
				setUpSummariseRow(this);
			}
			else
			{
				cleanUpSummariseRow(this);
			}
		});
	}
	else
	{
		template = attachPricingRow($pricingTable, rowElement, newElement);
		template.attr("data-id", rowElement.pricingId);
	}

	template.attr("data-rowcounter", "row" + (ROW_COUNTER++))

	var descriptionField = $(".rowDescription", template);
	// descriptionField.froalaEditor('html.set', (rowElement.label));
	var desc = htmlDecode(rowElement.label)
	descriptionField.val(desc);

	// $(".rowDescription,.noteDescription", template).on("change", function()
	// {
	// // resizeTextField($(this));
	// }).on("paste cut keydown", function()
	// {
	// // resizeTextField($(this));
	// });

	$(".rowDescription", template).on("change", function()
	{
		resizeTextField($(this));
	}).on("paste cut keydown", function()
	{
		resizeTextField($(this));
	});

	$(".deletePricing", template).click(removeRow);

	$(".splitPricingTableAction", template).click(splitPricingTable);


	if (newElement)
	{
		resortTable($pricingTable);
	}

	return template;

}

function initPricingRow(template, $pricingTable, rowElement, newElement)
{
	if (rowElement.type == "price")
	{

		if (newElement)
		{
			rowElement.autoPrice = true;
		}
		rowElement.breaks = rowElement.breaks || [];
		var discountPermission = userPermissions.permissions.ATTRCHECK_applyPricingDicounts;
		var totalPrice = $(".productTotalCost", template);
		var unitPriceField = $(".unitCostSelector", template);
		var additionField = $(".additionSelector", template);
		var taxField = $(".taxSelector", template);
		var qtyField = $(".qtyChooser", template);

		// =================== init price input =====================
		unitPriceField.attr('data-auto', rowElement.autoPrice);
		unitPriceField.attr('data-origin', rowElement.originPrice);
		unitPriceField.val(rowElement.unitPrice);

		unitPriceField.on("change keyup", function(event)
		{
			if (event.originalEvent)
			{
				$(this).attr('data-auto', false).closest('td').find('.dropdown.price-selector li.selected').removeClass("selected");
			}
			if (unitPriceField.val() < 0)
			{
				if (!discountPermission)
				{
					unitPriceField.val(formatMoney(0));
					swal({
						title : "Oops",
						text : "Sorry, but you're not allowed to apply discounts",
						type : "error"
					});
				}
				else
				{
					totalPrice.removeAttr("data-auto-percentage");
					totalPrice.attr("data-manual-percentage", 0);
				}
				$('.discount-selector, .tax-selector', template).closest('td').find('[id="bar"]').empty().removeClass();
			}
			// execute only if the last value was negative
			else if (parseFloat2(unitPriceField.attr("data-val")) < 0)
			{
				$('.discount-selector .dropdown-menu li:first, .tax-selector .dropdown-menu li:first', template).click();
			}
			// update actual value
			unitPriceField.attr("data-val", unitPriceField.val());
		});


		// setup price
		var dropdownPrice = $(".dropdown.price-selector", template);
		if (rowElement.breaks.length == 0)
		{
			dropdownPrice.hide();
		}

		rowElement.breaks.unshift({
			label : 'No Discount on Quantities',
			qty : 0,
			price : rowElement.originPrice
		});
		$(rowElement.breaks).each(function(i, b)
		{
			var li = $('<li>');
			$(li).data("break", b);
			li.append($('<a>').text(b.label));
			if (!rowElement.autoPrice && rowElement.unitPrice == b.price)
				li.addClass('selected');
			$('.dropdown-menu', dropdownPrice).append(li);
		})

		$('.dropdown-menu', dropdownPrice).on("click", "li", function(event)
		{
			var autoPrice = $(this).data("break") ? false : true;
			unitPriceField.attr('data-auto', autoPrice);
			unitPriceField.val($(this).data("break") ? $(this).data("break").price : rowElement.originPrice);
			setPriceOnQty(rowElement.breaks, qtyField.val(), unitPriceField);
			$(".dropdown-menu .selected", dropdownPrice).removeClass("selected");
			$(this).addClass("selected");
		});

		// =================== init additions input =====================
		additionField.val(rowElement.addition);

		// =================== init tax input =====================
		taxField.attr('data-auto', rowElement.autoTax);
		taxField.attr('data-origin', rowElement.originTaxPercent);
		taxField.val(rowElement.taxPercent);

		// =================== init qty input =====================
		qtyField.val(typeof rowElement.qty == 'undefined' ? 1 : rowElement.qty);
		qtyField.on("change keyup", function(event)
		{
			setPriceOnQty(rowElement.breaks, $(this).val(), unitPriceField)
		});

		setPriceOnQty(rowElement.breaks, rowElement.qty, unitPriceField)

		if ($('.dropdown-menu li.selected', dropdownPrice).length)
			$('.dropdown-menu li.selected', dropdownPrice).click();
		else if (rowElement.autoPrice)
			$('.dropdown-menu li:first', dropdownPrice).click();

		taxField.on("change keyup", function(event)
		{
			if (event.originalEvent)
			{
				if ($(this).attr('data-origin') != $(this).val())
					$(this).attr('data-auto', false).closest('td').find('.dropdown.tax-selector li.selected').removeClass("selected");
			}
			taxField.attr("data-val", taxField.val());
			changeTaxStyle(taxField);
		})

		var dropdownTax = $(".dropdown.tax-selector", template);


		$('.dropdown-menu', dropdownTax).on("click", "li", function(event)
		{
			taxField.attr('data-auto', true);
			taxField.val(rowElement.originTaxPercent);
			$(".dropdown-menu .selected", dropdownTax).removeClass("selected");
			$(this).addClass("selected");
			changeTaxStyle(taxField)
			taxField.change();
		});
		if (rowElement.autoTax)
			$('.dropdown-menu li:first', dropdownTax).click();
		else
			taxField.change();
		// setup term

		var termField = $(".planTermSelector", template);
		termField.val(rowElement.isOneOff ? 1 : (rowElement.term || 1)).prop("disabled", rowElement.isOneOff);
		termField.change(pricingProductChanged);
		termField.keyup(pricingProductChanged);


		var dropdownDiscount = $(".dropdown.discount-selector", template);
		$('.dropdown-menu', dropdownDiscount).on("click", "li", function(event)
		{
			if (!discountPermission || unitPriceField.val() < 0 || qtyField.val() < 0)
			{
				return;
			}
			var obj = $(this).data("discount") ? $(this).data("discount").percentage : "";
			totalPrice.removeAttr("data-auto-percentage");
			totalPrice.attr("data-manual-percentage", obj);
			var bar = $('#bar', dropdownDiscount);
			bar.attr("class", "");
			$(".dropdown-menu .selected", dropdownDiscount).removeClass("selected");
			$(this).addClass("selected");
			if ($.isNumeric(obj))
			{
				setDiscountBar(template, obj, true);
			}
			pricingProductChanged(event);
		});

		var counter = 0;
		discountsByPercentage.forEach(function(discount, index)
		{
			if (!discount.threshold && discount.percentage)
			{
				counter++;
				var li = $('<li>');
				$(li).data("discount", discount);
				li.append($('<a>').text(discount.label + " (" + discount.percentage + "%)"));
				if (rowElement.manualPercentage === discount.percentage)
					li.addClass('selected');
				$('.dropdown-menu', dropdownDiscount).append(li);
			}
		});
		if (counter == 0)
		{
			$('.btn-link', dropdownDiscount).addClass('disabled');
		}
		else
		{
			var discount = {
				label : 'No Discount or Margin',
				percentage : 0.00000001,
				thresholdType : 'value'
			};
			var li = $('<li>');
			$(li).data("discount", discount);
			li.append($('<a>').text(discount.label + " (" + discount.percentage.toFixed(0) + "%)"));
			if (rowElement.manualPercentage === discount.percentage)
				li.addClass('selected');
			$('.dropdown-menu', dropdownDiscount).append(li);
		}
		if ($('.dropdown-menu li.selected', dropdownDiscount).length)
			$('.dropdown-menu li.selected', dropdownDiscount).click();
		else
			$('.dropdown-menu li:first', dropdownDiscount).click();

		if (!discountPermission)
		{
			$('.dropdown-toggle, li', dropdownDiscount).addClass("disabled");
		}

		$(".custom-field", template).each(function()
		{
			var key = $(this).attr('data-key');
			if (!rowElement.misc || !rowElement.misc[key])
			{
				if ($(this).hasClass('cust-fld-date'))
				{
					var _picker = $(this).datepicker({
						autoclose : true,
						format : {
							toDisplay : function(date, format, language)
							{
								return moment(date).format(dateFormatPattern);
							},
							toValue : function(date, format, language)
							{
								return moment(date).format(dateFormatPattern);
							}
						}
					});
				}
				else
				{
					$(this).val('');
				}
			}
			else if ($(this).hasClass('cust-fld-dropdown'))
			{
				var customVar = false;
				$(this).find('option').each(function()
				{
					if ($(this).text() == rowElement.misc[key])
					{
						customVar = true;
					}
				});
				if (customVar)
				{
					$(this).val(rowElement.misc[key])
				}
			}
			else if ($(this).hasClass('cust-fld-date'))
			{
				var _picker = $(this).datepicker({
					autoclose : true,
					format : {
						toDisplay : function(date, format, language)
						{
							return moment(date).format(dateFormatPattern);
						},
						toValue : function(date, format, language)
						{
							return moment(date).format(dateFormatPattern);
						}
					}
				});
				if (rowElement.misc && rowElement.misc[key])
				{
					var _d = moment(rowElement.misc[key], 'YYYY-MM-DD');
					if (_d.isValid())
					{
						_picker.datepicker('update', _d.toDate());
					}
				}
			}
			else
			{
				$(this).val(rowElement.misc[key])
			}


		});

		var noteField = $(".noteDescription", template);
		var note = rowElement.note;
		noteField.froalaEditor('html.set', note);

		var titleField = $(".noteTitle", template);
		titleField.val(rowElement.rawTitle);
		if (rowElement.note)
		{
			noteField.addClass("active");
		}
		$('.fr-view', noteField).on("change blur", function()
		{
			if (noteField.froalaEditor('html.get'))
			{
				noteField.closest("tr").find(".notePricing").hide();
			}
			else
			{
				noteField.closest("tr").find(".notePricing").show();
				noteField.removeClass("active");
			}
		})

		$(".notePricing", template).click(activateNotefield);

		$(".oneOffCheckbox", template).prop("disabled", false).iCheck({
			checkboxClass : 'icheckbox_square-orange',
			radioClass : 'iradio_square-orange'
		}).on('ifChanged', pricingProductChanged);
		if (rowElement.isOneOff)
		{
			$(".oneOffCheckbox", template).iCheck('check');
		}


		// =============================
		// general events binding : blur, change, keyup
		// important! this section has to be at the end.
		$('input.input-money', template).blur(function()
		{
			$(this).val(formatMoney($(this).val()));
		}).blur();

		$('input.input-integer', template).blur(function()
		{
			$(this).val(formatInt($(this).val()));
		}).blur();

		$('input.input-money, input.input-integer', template).on("change keyup", pricingProductChanged);

	}

	fixPricingRowsPlaceholder($pricingTable);
	$(".unitCostSelector", template).keyup();
	updatePlanDependencies();
	$(".rowDescription", template).change();
	$(".noteDescription", template).change();
	$(".noteTitle", template).change();
	$(".rowDescription", template).focus();

	updateSummariseRow(template);
}

function setPriceOnQty(breaks, qty, unitPriceField)
{
	var autoPrice = unitPriceField.attr('data-auto') == 'true';
	var defaultPrice = unitPriceField.attr('data-origin');
	var unitPrice = unitPriceField.val();
	if (autoPrice)
	{
		unitPrice = defaultPrice;
		$(breaks).each(function(i, b)
		{
			if (qty >= b.qty)
			{
				unitPrice = b.price;
			}
		})
	}
	unitPriceField.val(formatMoney(unitPrice)).change();
	changePriceStyle(unitPriceField);
}

function changePriceStyle(unitPriceField)
{
	var autoPrice = unitPriceField.attr('data-auto') == 'true';
	var defaultPrice = parseFloat(unitPriceField.attr('data-origin'));
	var unitPrice = parseFloat(unitPriceField.val());
	var $bar = unitPriceField.closest('td').find('#bar').attr('class', '').empty();
	var $tr = unitPriceField.closest('tr');

	if (unitPrice != defaultPrice && $tr.attr('data-id'))
	{
		$bar.html("was <span class='moneytaryValue'>" + formatMoney(defaultPrice) + "</span>");
		if (unitPrice > defaultPrice)
		{
			$bar.addClass('markup');
		}
		else
		{
			$bar.addClass('discount');
		}
		$bar.addClass(autoPrice ? 'auto' : 'manual');
	}
}

function changeTaxStyle(taxField)
{
	var autoTax = taxField.attr('data-auto') == 'true';
	var defaultTax = parseFloat(taxField.attr('data-origin'));
	var tax = parseFloat(taxField.val());
	var $bar = taxField.closest('td').find('#bar').attr('class', '').empty();
	var $tr = taxField.closest('tr');

	if (tax != defaultTax)
	{
		$bar.html("was <span class=''>" + defaultTax + "% </span>");
		if (tax > defaultTax)
		{
			$bar.addClass('markup');
		}
		else
		{
			$bar.addClass('discount');
		}
		$bar.addClass(autoTax ? 'auto' : 'manual');
	}
}


function setDiscountBar(row, value, manual)
{
	var bar = $('.discount-selector #bar', row);
	bar.attr("class", "");
	if (manual)
		bar.addClass('manual');

	if (value > 0)
	{
		bar.addClass('markup');
	}
}

function attachPricingSection($pricingTable, row, newSection)
{
	var template = $("#htmlFragments .pricingSection").clone();
	initFroalaEditorForPricing(template);

	var focus = getFocusedRow($pricingTable);

	startRowSortable($(template), $pricingTable);

	if (newSection && focus)
	{
		// add the section after the following section
		template.insertAfter(focus.closest('.customPricingRows'));
		// detach all following pricing rows
		var next = focus.nextAll('[data-type="price"]');
		if (focus.attr("data-type") === "price")
		{
			template.append(focus);
		}
		// attach the detached rows to the new section
		template.append(next);
	}
	else
	{
		$pricingTable.append(template[0]);
	}
	return $('[data-type="title"]', template);
}

function attachPricingRow($pricingTable, row, newRow)
{
	var template = $("#htmlFragments .productRow[data-type='price']").clone();
	initFroalaEditorForPricing(template);

	var focus = getFocusedRow($pricingTable);

	if (newRow && focus)
	{
		focus.after(template);
	}
	else
	{
		$(".customPricingRows:last-child", $pricingTable).append(template);
	}
	return template;
}

function getFocusedRow($pricingTable)
{
	var focus = $pricingTable.find('.focus').eq(0);
	if (focus[0])
	{
		return $(focus[0]);
	}
}

function getSelectedPricingItem(row)
{
	if (row instanceof jQuery == false)
	{
		row = $(row);
	}

	var title = row.prevAll("[data-type='title']").length > 0 ? row.prevAll("[data-type='title']:first").find('.rowDescription').val().trim() || row.prevAll("[data-type='title']:first").find('.rowDescription').attr("placeholder") : "";
	// var title = row.prevAll("[data-type='title']").length > 0 ?
	// row.prevAll("[data-type='title']:first").find('.rowDescription').froalaEditor("html.get").replace(/\<br>/g,
	// '<br/>') ||
	// row.prevAll("[data-type='title']:first").find('.rowDescription').data('froala.editor').$placeholder.text()
	// : "";
	// var label = row.find(".rowDescription").val() ?
	// row.find(".rowDescription").val().trim() :
	// row.find(".rowDescription").attr("placeholder");

	// var rowDesc = row.find(".rowDescription");
	//
	// var label = (rowDesc.data("froala.editor") &&
	// rowDesc.froalaEditor("html.get")) ?
	// rowDesc.froalaEditor("html.get").replace(/\<br>/g, '<br/>') :
	// rowDesc.attr('placeholder');
	var label = row.find(".rowDescription").val() ? row.find(".rowDescription").val().trim() : row.find(".rowDescription").attr("placeholder");
	// label = strip(label);

	var object = {

		type : row.attr("data-type"),
		label : label
	};

	if (row.attr("data-type") == "price")
	{
		var pricingId = row.attr("data-id");
		var term = row.find(".planTermSelector").length > 0 ? parseInt(row.find(".planTermSelector").val()) : 1;
		var unitCost = parseFloat2(row.find(".unitCostSelector").val());
		var autoPrice = eval(row.find(".unitCostSelector").attr('data-auto'));
		var selectedQuantity = parseFloat2(row.find(".qtyChooser").val());

		var oneOff = row.find(".oneOffCheckbox").length > 0 ? row.find(".oneOffCheckbox").prop("checked") : true;

		var manual_percentage = parseFloat2(row.find(".productTotalCost").attr("data-manual-percentage"));
		var auto_percentage = parseFloat2(row.find(".productTotalCost").attr("data-auto-percentage"));
		var totalPrice = parseFloat2(row.find(".productTotalCost").text());
		
		var noteDesc = $('.noteDescription', row);
		var note = noteDesc.hasClass('active') ? noteDesc.froalaEditor('html.get').replace(/\<br>/g, '<br/>') : "";

		//Code placed in for legacy issue with conversion of CKeditor br style code in froala editor
		var strings = note.split('<br style');
		for (var string in strings) {
			if (string > 0) {
				var log = strings[string].split('>');
				note = note.replace('<br style' + log[0] + '>', '<br style' + log[0] + '/>')
			}

		}

		var productTitle = $('.noteTitle', row).val();

		var misc = {};
		$.each($('.custom-field', row), function(index, dom)
		{
			var id = $(dom).attr("data-key");
			if (productsCustomFields[id] && productsCustomFields[id].type == 'date')
			{
				var _d = moment($(dom).val(), dateFormatPattern)
				if (_d.isValid())
				{
					misc[id] = _d.format('YYYY-MM-DD');
				}
				else
				{
					misc[id] = '';
				}
			}
			else
			{
				misc[id] = $(dom).val();
			}
		});

		var autoTax = row.find(".taxSelector").length > 0 ? eval(row.find(".taxSelector").attr('data-auto')) : true;
		var taxPercent = row.find(".taxSelector").length > 0 ? parseFloat2(row.find(".taxSelector").val()) : 0;
		var addition = row.find(".additionSelector").length > 0 ? parseFloat2(row.find(".additionSelector").val()) : 0.0;

		$.extend(object, {
			pricingId : pricingId,
			unitPrice : formatMoney(unitCost),
			term : term,
			qty : formatMoney(selectedQuantity),
			isOneOff : oneOff,
			totalPrice : totalPrice,
			manualPercentage : manual_percentage,
			autoPercentage : auto_percentage,
			note : note,
			misc : misc,
			autoPrice : autoPrice,
			rawTitle : productTitle,
			autoTax : autoTax,
			taxPercent : taxPercent,
			addition : addition,
		});
	}

	if (row.attr("data-type") === "title")
	{
		var summarised = row.find(".js-switch").prop("checked");
		var summarisedTotal = parseFloat2(row.find('.summaryAmount.summaryTotal').text());
		var summarisedTax = parseFloat2(row.find('.summaryAmount.summaryTax').text());
		var summarisedAddition = parseFloat2(row.find('.summaryAmount.summaryAddition').text());
		var summarisedCost = parseFloat2(row.find('.summaryAmount.summaryCost').text());
		$.extend(object, {
			isSummarised : summarised,
			summarisedTotal : summarisedTotal,
			summarisedTax : summarisedTax ? summarisedTax : "0.00",
			summarisedAddition : summarisedAddition ? summarisedAddition : "0.00",
			summarisedAmount : summarisedCost ? summarisedCost : "0.00"
		});
	}

	if (row.attr("data-type") != "title")
	{
		$.extend(object, {
			title : title
		});
	}
	return object;
}

function activateNotefield(event)
{
	var itemRow = $(event.target).closest('tr').find('.noteDescription');
	itemRow.addClass('active');
	itemRow.change();
	itemRow.focus();
	$(event.target).closest("tr").find(".notePricing").hide();
}

function removeRow(event)
{
	var itemRow = $(event.target).closest('tr');
	var $panel = itemRow.closest('.panel');
	if (itemRow.attr("data-type") === "title")
	{
		var section = $(itemRow).closest('.customPricingRows');
		var merge = section.prev();
		var prices = itemRow.nextAll('[data-type="price"]');
		merge.append(prices);
		section.remove();
	}
	else
	{
		itemRow.remove();

		recalculatePricingTables();

		// reload the plan dependencies (F&B and T&C).
		updatePlanDependencies();
	}
}

function splitPricingTable(event)
{
	var $tr = $(event.target).closest('tr');
	var $panel = $tr.closest('.panel');
	var rowIndex = $panel.find('.pricingTable .customPricingRows tr.productRow').index($tr);
	var proposal = getCurrentProposal();
	var $group = $tr.closest('.group-panel');
	var $panel = $tr.closest('.panel');
	var $currentSection = $tr.closest('.contentContainer');
	var $tabPane = $(this).closest('.tab-pane');
	var $a = $('#documentSectionBrowser a[href="#' + $tabPane.attr('id') + '"]');

	var sectionIndex = $('#documentSectionBrowser a').index($a);
	var contentIndex = $panel.attr('dataOrder');
	var section = proposal.sections[sectionIndex];
	var content = section.content[contentIndex];
	restoreContent(section.content.length, content, $currentSection);
	var $newGroup = $currentSection.find('.group-panel').last();
	$newGroup.insertAfter($group);
	updateDataOrder($currentSection);
	var pricingIndex = $('.panel.pricing-droppable').index($panel);
	var pricing = proposal.pricings[pricingIndex]
	for (var i = 0; i < rowIndex; i++)
	{
		pricing.rows.shift();
	}
	restoreProposalPricing(pricing, $newGroup.find('.panel.pricing-droppable'));
	if (rowIndex > 0)
	{
		$panel.find('.deletePricing:gt(' + (rowIndex - 1) + ')').click();
	}
	else
	{
		$panel.find('.deletePricing').click();
	}
}

function pricingRemoveProduct(event)
{
	var itemRow = $(event.target).closest('tr');
	itemRow.remove();

	updateTotalPricing();

	// reload the plan dependencies (F&B and T&C).
	updatePlanDependencies();
}

var delayRecalculatePricingTables = undefined;

function recalculatePricingTables()
{
	if (delayRecalculatePricingTables != undefined)
	{
		clearTimeout(delayRecalculatePricingTables);
	}
	delayRecalculatePricingTables = setTimeout(function()
	{
		backgroundProcessing++;
		recalculateThresholdMultiplier();
		$('.pricing-droppable:not(#htmlFragments .pricing-droppable)').each(function()
		{
			recalculatePricingTable($(this));
		})
		updateOverallTotalPricing();
		backgroundProcessing--;
	}, 100);
}

function recalculateThresholdMultiplier()
{
	globalThresholdValueMultiplier = 0;
	var totalValueBeforePercentage = 0;
	var estimatedField = parseFloat2($("#estimatedValue").val());
	if (estimatedValueOn && estimatedField)
	{
		totalValueBeforePercentage = estimatedField;
	}
	else
	{
		$.each($("[id='pricingTable'] tbody tr"), function(i, e)
		{
			if ($(this).data('pricing'))
				totalValueBeforePercentage += parseFloat2($(this).data('pricing')['regular']);
		});
	}
	// check threshold with total
	discountsByThreshold.some(function(discount, index)
	{
		if (typeof discount.threshold != 'undefined' && discount.thresholdType == 'value' && totalValueBeforePercentage >= discount.threshold)
		{
			globalThresholdValueMultiplier = parseFloat2(discount.percentage);
			return true;
		}
	});
}

function updateOverallTotalPricing()
{
	var summary = {
		total : 0.0,
		discount : 0.0,
		margin : 0.0,
		tax : 0.0,
		addition : 0.0,
		totalIncTax : 0.0,
	};
	var tables = $('.pricing-droppable:not(#htmlFragments .pricing-droppable)').length;
	$('.pricing-droppable:not(#htmlFragments .pricing-droppable)').each(function()
	{
		summary.total += parseFloat2($('.grandTotalRow .price', $(this)).text());
		summary.discount += parseFloat2($('.grandTotalRow .discount', $(this)).text());
		summary.margin += parseFloat2($('.grandTotalRow .margin', $(this)).text());
		summary.tax += parseFloat2($('.grandTotalRow .tax', $(this)).text());
		summary.addition += parseFloat2($('.grandTotalRow .addition', $(this)).text());
		summary.totalIncTax += parseFloat2($('.grandTotalRow .cost', $(this)).text());
	})

	$('.pricing-droppable:not(#htmlFragments .pricing-droppable)').addClass('not-last').each(function(i)
	{
		$('.overallTotalRow .price', $(this)).text(formatMoney(summary.total));
		$('.overallTotalRow .discount', $(this)).text(formatMoney(summary.discount));
		$('.overallTotalRow .margin', $(this)).text(formatMoney(summary.margin));
		$('.overallTotalRow .tax', $(this)).text(formatMoney(summary.tax));
		$('.overallTotalRow .addition', $(this)).text(formatMoney(summary.addition));
		$('.overallTotalRow .cost', $(this)).text(formatMoney(summary.totalIncTax));
		if (i == tables - 1 && i > 0)
		{
			$(this).removeClass('not-last');
		}
	})
}

function recalculatePricingTable($panel)
{

	logger.info("recalculatePricingTable called after delay");
	var currentThresholdMultiplier = globalThresholdValueMultiplier;
	var discountSet = false;
	var margin_discount = parseFloat($("#margin-discounts", $panel).attr('data-percentage'));
	if (!isNaN(margin_discount))
	{
		currentThresholdMultiplier = margin_discount;
		discountSet = true;
	}
	updateTotalPricing(currentThresholdMultiplier, $panel, discountSet);
}

function updateTotalPricing(thresholdMultiplier, $panel, discountSet)
{

	var summary = {
		monthly : {
			total : 0.0,
			discount : 0.0,
			margin : 0.0,
			tax : 0.0,
			addition : 0.0,
			totalIncTax : 0.0,
		},
		fullTerm : {
			total : 0.0,
			discount : 0.0,
			margin : 0.0,
			tax : 0.0,
			addition : 0.0,
			totalIncTax : 0.0,
		},
		oneOff : {
			total : 0.0,
			discount : 0.0,
			margin : 0.0,
			tax : 0.0,
			addition : 0.0,
			totalIncTax : 0.0,
		},
		total : {
			total : 0.0,
			discount : 0.0,
			margin : 0.0,
			tax : 0.0,
			addition : 0.0,
			totalIncTax : 0.0,
		},
	}
	$.each($("#pricingTable tbody .customPricing[data-type='price']", $panel), function(i, e)
	{
		var tempThresholdMultiplier = thresholdMultiplier;
		var $e = $(e);
		var pricing = $e.data('pricing');
		if (pricing && !pricing['isManualMultiple'])
		{
			$(".productTotalCost", $e).attr("data-auto-percentage", tempThresholdMultiplier);
		}
		theOnlyLineItemPriceCalculator(e, true);
		var pricing = $e.data('pricing');
		if (!pricing)
		{
			return;
		}

		if (pricing.oneOff)
		{
			if (pricing.regular < 0)
			{
				summary.oneOff.discount += -1 * pricing.regular;
			}
			else
			{
				summary.oneOff.total += pricing.regular;
				summary.oneOff.discount += Math.max(pricing.regular - pricing.total, 0);
				summary.oneOff.margin += Math.max(pricing.total - pricing.regular, 0);
			}
			summary.oneOff.addition += pricing.addition;
			summary.oneOff.tax += pricing.tax;
			summary.oneOff.totalIncTax += pricing.totalIncTax;
		}
		else
		{
			if (pricing.regular < 0)
			{
				summary.fullTerm.discount += -1 * pricing.regular;
				summary.monthly.discount += -1 * pricing.regular / pricing.term;
			}
			else
			{
				summary.fullTerm.total += pricing.regular;
				summary.fullTerm.discount += Math.max(pricing.regular - pricing.total, 0);
				summary.fullTerm.margin += Math.max(pricing.total - pricing.regular, 0);

				summary.monthly.total += pricing.regular / pricing.term;
				summary.monthly.discount += Math.max(pricing.regular - pricing.total, 0) / pricing.term;
				summary.monthly.margin += Math.max(pricing.total - pricing.regular, 0) / pricing.term;
			}
			summary.fullTerm.addition += pricing.addition;
			summary.fullTerm.tax += pricing.tax;
			summary.fullTerm.totalIncTax += pricing.totalIncTax;

			summary.monthly.addition += pricing.addition / pricing.term;
			summary.monthly.tax += pricing.tax / pricing.term;
			summary.monthly.totalIncTax += pricing.totalIncTax / pricing.term;
		}

		if (pricing.regular < 0)
		{
			summary.total.discount += -1 * pricing.regular;
		}
		else
		{
			summary.total.total += pricing.regular;
			summary.total.discount += Math.max(pricing.regular - pricing.total, 0);
			summary.total.margin += Math.max(pricing.total - pricing.regular, 0);
		}
		summary.total.addition += pricing.addition;
		summary.total.tax += pricing.tax;
		summary.total.totalIncTax += pricing.totalIncTax;

	});
	$.each($("#pricingTable tbody .customPricing[data-type='title']", $panel), function(i, e)
	{
		updateSummariseRow(e);
	});


	console.log(summary);
	$('[id="pricingFooter"] .minimumMothlyCostRow .price', $panel).text(formatMoney(summary.monthly.total));
	$('[id="pricingFooter"] .minimumMothlyCostRow .discount', $panel).text(formatMoney(summary.monthly.discount));
	$('[id="pricingFooter"] .minimumMothlyCostRow .margin', $panel).text(formatMoney(summary.monthly.margin));
	$('[id="pricingFooter"] .minimumMothlyCostRow .tax', $panel).text(formatMoney(summary.monthly.tax));
	$('[id="pricingFooter"] .minimumMothlyCostRow .addition', $panel).text(formatMoney(summary.monthly.addition));
	$('[id="pricingFooter"] .minimumMothlyCostRow .cost', $panel).text(formatMoney(summary.monthly.totalIncTax));

	$('[id="pricingFooter"] .fullTermCostRow .price', $panel).text(formatMoney(summary.fullTerm.total));
	$('[id="pricingFooter"] .fullTermCostRow .discount', $panel).text(formatMoney(summary.fullTerm.discount));
	$('[id="pricingFooter"] .fullTermCostRow .margin', $panel).text(formatMoney(summary.fullTerm.margin));
	$('[id="pricingFooter"] .fullTermCostRow .tax', $panel).text(formatMoney(summary.fullTerm.tax));
	$('[id="pricingFooter"] .fullTermCostRow .addition', $panel).text(formatMoney(summary.fullTerm.addition));
	$('[id="pricingFooter"] .fullTermCostRow .cost', $panel).text(formatMoney(summary.fullTerm.totalIncTax));

	$('[id="pricingFooter"] .onOffCostRow .price', $panel).text(formatMoney(summary.oneOff.total));
	$('[id="pricingFooter"] .onOffCostRow .discount', $panel).text(formatMoney(summary.oneOff.discount));
	$('[id="pricingFooter"] .onOffCostRow .margin', $panel).text(formatMoney(summary.oneOff.margin));
	$('[id="pricingFooter"] .onOffCostRow .tax', $panel).text(formatMoney(summary.oneOff.tax));
	$('[id="pricingFooter"] .onOffCostRow .addition', $panel).text(formatMoney(summary.oneOff.addition));
	$('[id="pricingFooter"] .onOffCostRow .cost', $panel).text(formatMoney(summary.oneOff.totalIncTax));

	$('[id="pricingFooter"] .grandTotalRow .price', $panel).text(formatMoney(summary.total.total));
	$('[id="pricingFooter"] .grandTotalRow .discount', $panel).text(formatMoney(summary.total.discount));
	$('[id="pricingFooter"] .grandTotalRow .margin', $panel).text(formatMoney(summary.total.margin));
	$('[id="pricingFooter"] .grandTotalRow .tax', $panel).text(formatMoney(summary.total.tax));
	$('[id="pricingFooter"] .grandTotalRow .addition', $panel).text(formatMoney(summary.total.addition));
	$('[id="pricingFooter"] .grandTotalRow .cost', $panel).text(formatMoney(summary.total.totalIncTax));

}

function refreshTextareas()
{
	$("#pricingTable textarea").change();
}

var pricingProductChangedDelay = {};

function pricingProductChanged(event)
{
	var itemRow = $(event.target).closest('tr');
	var $panel = itemRow.closest('.panel');
	var rowId = itemRow.attr("data-rowcounter");
	var sectionBody = itemRow.closest('tbody');
	var pricingFooter = itemRow.closest('.panel-body').find('[id="pricingFooter"]');
	if (sectionBody.find('.summarise').length > 0)
	{
		if (sectionBody.find('.oneOffCheckbox:checked').length > 0)
		{
			sectionBody.find('.summarise').show();
		}
		else
		{
			sectionBody.find('.summarise').hide();
		}
	}

	if (pricingProductChangedDelay[rowId] != undefined)
	{
		clearTimeout(pricingProductChangedDelay[rowId]);
		delete pricingProductChangedDelay[rowId];
	}
	pricingProductChangedDelay[rowId] = setTimeout(function()
	{
		// logger.info("pricingProductChanged called after delay");
		if (itemRow.hasClass('customPricing'))
		{
			theOnlyLineItemPriceCalculator(itemRow, false);
		}

		recalculatePricingTables();
	}, 100);

}

function theOnlyLineItemPriceCalculator(row, calculateDiscount)
{
	var itemRow = $(row);
	var $priceTotal = $(".productTotalCost", itemRow);
	var $productTotalCostIncTax = $(".productTotalCostIncTax", itemRow);
	var $qtyChooser = $(".qtyChooser", itemRow);
	var $additionSelector = $(".additionSelector", itemRow);
	var $unitCostSelector = $(".unitCostSelector", itemRow);
	var $taxSelector = $(".taxSelector", itemRow);

	var isOneOff = $(".oneOffCheckbox", itemRow).length == 0 ? true : $(".oneOffCheckbox", itemRow).prop("checked");
	var $termSelector = $(".planTermSelector", itemRow).prop('disabled', isOneOff);
	var isManualMultiple = !isNaN(parseFloat($priceTotal.attr("data-manual-percentage")));

	if (isOneOff)
	{
		$termSelector.val(1);
	}
	var unitCost = parseFloat2($unitCostSelector.val());
	if (unitCost < 0)
	{
		itemRow.find('.discount-selector .dropdown-toggle, .discount-selector .dropdown-toggle li, .price-selector .dropdown-toggle, .price-selector .dropdown-toggle li').addClass('disabled');
	}
	else
	{
		itemRow.find('.discount-selector .dropdown-toggle, .discount-selector .dropdown-toggle li, .price-selector .dropdown-toggle, .price-selector .dropdown-toggle li').removeClass('disabled');
	}
	if (!isOneOff || unitCost < 0)
	{
		$additionSelector.val("0").prop('disabled', true).blur();
	}
	else
	{
		$additionSelector.prop('disabled', false);
	}
	var term = $termSelector.val() ? parseInt($termSelector.val()) : 1;


	var qty = parseFloat2($qtyChooser.val());
	var multiplier = 1;
	if (calculateDiscount && unitCost > 0)
	{
		multiplier = parseInt(parseFloat($priceTotal.attr("data-auto-percentage") ? $priceTotal.attr("data-auto-percentage") : $priceTotal.attr("data-manual-percentage") ? $priceTotal.attr("data-manual-percentage") : 0).toFixed(0));
		multiplier = Math.max(-100, multiplier);
		var percentage = multiplier;
		multiplier = Math.abs(100 + multiplier) / 100;

		$('.discount-selector #bar', itemRow).removeClass().empty();
		if (percentage != 0)
		{
			$('.discount-selector #bar', itemRow).html("was <span class='moneytaryValue'>" + formatMoney(unitCost * term * qty) + "</span> (" + percentage + "%)").addClass(percentage >= 0 ? 'markup' : 'discount').addClass(isManualMultiple ? "manual" : "auto");
		}
	}


	var regular = formatMoney(term * unitCost * qty);
	var total = formatMoney(term * unitCost * qty * multiplier);
	var addition = parseFloat2($additionSelector.val()) * term;
	var tax = formatMoney((parseFloat2(total) + addition) * parseFloat2($taxSelector.val()) / 100.0);
	var totalIncTax = formatMoney(parseFloat2(total) + addition + parseFloat2(tax));

	$priceTotal.text(total);
	$productTotalCostIncTax.text(totalIncTax);
	itemRow.data('pricing', {
		regular : parseFloat2(regular),
		total : parseFloat2(total),
		addition : parseFloat2(addition),
		tax : parseFloat2(tax),
		totalIncTax : parseFloat2(totalIncTax),
		oneOff : isOneOff,
		isManualMultiple : isManualMultiple,
		term : parseInt(term),
	});
}

function setUpSummariseRow(row)
{
	$tbody = $(row).closest('.customPricingRows');
	$tbody.find('.summaryAmount').addClass('active');
	updateSummariseRow(row)
}

function updateSummariseRow(item)
{
	$body = $(item).closest('.customPricingRows');
	var summary = {
		amount : 0.0,
		tax : 0.0,
		addition : 0.0,
		cost : 0.0,
	};
	$body.find('tr').each(function()
	{
		var pricing = $(this).data('pricing');
		if (pricing)
		{
			summary.amount += pricing.total;
			summary.tax += pricing.tax;
			summary.addition += pricing.addition;
			summary.cost += pricing.totalIncTax;
		}
	});
	if ($body.find('.summarise').length > 0)
	{
		if ($body.find('.oneOffCheckbox:not(:checked)').length > 0)
		{
			$body.find('.summarise').hide();
		}
		else
		{
			$body.find('.summarise').show();
		}
	}
	$body.find('.summaryTotal').text(formatMoney(summary.amount));
	$body.find('.summaryTax').text(formatMoney(summary.tax));
	$body.find('.summaryAddition').text(formatMoney(summary.addition));
	$body.find('.summaryCost').text(formatMoney(summary.cost));
}

function cleanUpSummariseRow(row)
{
	$tbody = $(row).closest('.customPricingRows');
	$tbody.find('.summaryAmount').removeClass('active');
}

function rebuildDiscounts()
{
	$(".dropdown.discount-selector").each(function()
	{
		var dropdownDiscount = this;
		var rowElement = $(dropdownDiscount).closest('.productRow');
		var manualPercentage = rowElement.find('[data-manual-percentage]').attr('data-manual-percentage');

		$('.dropdown-menu', dropdownDiscount).empty();

		var auto = $('<li>');
		auto.append($('<a>').text('Auto'));
		$('.dropdown-menu', dropdownDiscount).append(auto);

		var counter = 0;
		discountsByPercentage.forEach(function(discount, index)
		{
			if (!discount.threshold && discount.percentage)
			{
				counter++;
				var li = $('<li>');
				$(li).data("discount", discount);
				li.append($('<a>').text(discount.label + " (" + discount.percentage + "%)"));
				if (manualPercentage == discount.percentage)
					li.addClass('selected');
				$('.dropdown-menu', dropdownDiscount).append(li);
			}
		});
		if (counter == 0)
		{
			$('.btn-link', dropdownDiscount).addClass('disabled');
		}
		else
		{
			var discount = {
				label : 'No Discount or Margin',
				percentage : 0.00000001,
				thresholdType : 'value'
			};
			var li = $('<li>');
			$(li).data("discount", discount);
			li.append($('<a>').text(discount.label + " (" + discount.percentage.toFixed(0) + "%)"));
			if (manualPercentage == discount.percentage)
				li.addClass('selected');
			$('.dropdown-menu', dropdownDiscount).append(li);
		}
		if ($('.dropdown-menu li.selected', dropdownDiscount).length)
			$('.dropdown-menu li.selected', dropdownDiscount).click();
		else
			$('.dropdown-menu li:first', dropdownDiscount).click();
	});
}


function formatMoney(val)
{
	var decimals = roundingOn ? 0 : 2;
	return parseFloat2(val).toFixed(decimals);
}

function formatInt(val)
{
	return parseFloat2(val).toFixed(0);
}

function round(val)
{
	var decimals = roundingOn ? 0 : 2;
	return parseFloat2(parseFloat2(val).toFixed(decimals));
}

function fixHTML()
{
	$.each($("#pricingTable textarea"), function(i, o)
	{
		var d = $("<div>");
		d.html($(this).val());
		$(this).val(d.text());
	});
};

function getCurrentPricingTable()
{
	var $panel = getCurrentPricingPanel();
	return $('[id="pricingTable"]', $panel);
}

function getCurrentPricingPanel()
{
	var $panel = $('.pricing-droppable.current:eq(0)');
	if ($panel.length > 0)
	{
		return $panel;
	}
	return $('.pricing-droppable:eq(0)').addClass('current');
}

function parseFloat2(val)
{
	var f = parseFloat(val);
	if (isNaN(f))
	{
		return 0.0;
	}
	return f * 1;
}

function parseInt2(val)
{
	var f = parseInt(val);
	if (isNaN(f))
	{
		return 0;
	}
	return f * 1;
}

function initFroalaEditorForPricing(template)
{
	// $('.rowDescription', template).froalaEditor({
	// key : 'DLAHYKAJOEc1HQDUH==',
	// toolbarInline : true,
	// charCounterCount : false,
	// toolbarButtons : [ 'bold', 'italic', 'underline', 'strikeThrough',
	// 'fontSize', 'color', '-', 'align', 'formatOL', 'indent', 'outdent',
	// 'insertLink', '-', 'undo', 'redo' ],
	// htmlAllowedAttrs : [ 'style', 'class', 'colspan', 'href', 'target' ],
	// toolbarVisibleWithoutSelection : true,
	// heightMin : 0,
	// scrollableContainer : '#editorContent'
	// });

	$('.noteDescription', template).on('froalaEditor.initialized', function(ev, editor)
	{

		customizeFroalaEditor(ev, editor)
	}).froalaEditor({
		enter: $.FroalaEditor.ENTER_P,
		key : 'DLAHYKAJOEc1HQDUH==',
		charCounterCount : false,
		toolbarBottom: true,
		toolbarButtons : [ 'bold', 'italic', 'underline', 'strikeThrough', 'fontSize', 'color', 'align', 'formatOL', "formatUL", 'indent', 'outdent', 'insertLink', 'undo', 'redo', 'clearFormatting' ],
		htmlAllowedAttrs : [ 'style', 'class', 'colspan', 'href', 'target' ],
		pluginsEnabled : ["align", "charCounter", "codeBeautifier", "codeView", "colors", "draggable", "embedly", "emoticons", "entities", "file", "fontFamily", "fontSize", "fullscreen", "image", "imageManager", "inlineStyle", "lineBreaker", "link", "lists", "paragraphFormat", "paragraphStyle", "quote", "save", "table", "url", "wordPaste"]
	});

}


function htmlDecode(value)
{
	return $('<div/>').html(value).text();
}
