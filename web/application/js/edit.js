//var metadata = {};
//var pricing = {};
var lastSave = "";
var isTemplate = false;
var radialArray = [ '#fa-position-1', '#fa-position-2', '#fa-position-3', '#fa-position-4', '#fa-size-1', '#fa-size-2', '#fa-size-3', '#fa-size-4', '#fa-aspect-1', '#fa-aspect-2', '#fa-aspect-3', '#fa-width-1', '#fa-width-2', '#fa-width-3', '#fa-width-4', '#fa-width-5' ];


$(document).ready(function()
{

	checkScrollTable();

	// ctrl-s saves
	$(window).bind('keydown', function(event)
	{
		if (event.ctrlKey || event.metaKey)
		{
			if (String.fromCharCode(event.which).toLowerCase() == 's')
			{
				doSaveProposal();
				event.preventDefault();
			}
		}
	});

	$(document).ajaxComplete(function(event, xhr, ajaxOptions)
	{
		if (xhr.status == 401)
		{
			swal({
				title : "You've been logged out!",
				text : "Due to inactivity, you have been logged out. We autosaved your latest work so you can recover it after login.",
				type : "warning"
			}, function()
			{
				window.location.reload();
			});
			logger.info("HTTP 401(Unauthorised) recieved... autosaving before closing.");
			saveToLocalStorage();
		}
		if (xhr.status == 500)
		{
			swal({
				title : "Oops, that's embarrassing",
				text : "Something went wrong on the server. Please try again later.",
				type : "warning"
			}, function()
			{

			});
			logger.info("HTTP 500(Internal Server Error) recieved.");
			saveToLocalStorage();
		}

		checkScrollTable();
	});

	setupEditorInteractions();
	detectOrientation();
	window.onorientationchange = detectOrientation;

	// For Coverpage
	$("#cc-emails").tagsInput({
		defaultText : 'Add an email',
		width : 'auto',
		minInputWidth : '184px',
		placeholderColor : '#858585'
	});

	$("#colorPicker .btn").click(function(e)
	{
		setTimeout(function()
		{
			var spectrumColor = $("input:radio[name=spectrumColor]:checked").val();
			$("body").attr("spectrum", spectrumColor);
		}, 100);
	});

	$(".coverPageOption").click(function(e)
	{
		$(e.currentTarget).siblings().removeClass("active");
		$(e.currentTarget).addClass("active");

		var $scrollTo = $(e.currentTarget);
		var $myContainer = $("#coverPicker");
		var newScroll = $scrollTo.offset().left - $myContainer.offset().left + $myContainer.scrollLeft();
		var adjust = ($scrollTo.width() - $myContainer.width()) / 2
		$myContainer.animate({
			scrollLeft : newScroll + adjust
		}, 500);

		$("#coverPageHiddenField").val($(e.currentTarget).attr("pageId"));
	});

	// var fieldsDiv = $(".custom-fields");
	// var count = 0;
	// $.each(customFields, function(key, value)
	// {
	// if (value.on)
	// {
	// count++;
	// fieldsDiv.append(getCustomField(value, key));
	// }
	// });
	// if (!count)
	// {
	// fieldsDiv.append($("<div>").addClass("text-center").css("padding",
	// "20px").text("No custom fields enabled"));
	// }

	$(".fixed-dropdown-placeholder").each(function()
	{
		$(this).append(getInsertTextAttachmentButton($(this).closest('.contentContainer')).removeClass('dropup'));
		$(this).append($("<div>").attr('class', 'dropArea pulse-droppable-help').append($('<div>').attr('class', 'msg')));
	});


	// $("#displayPricingSummary").iCheck({
	// checkboxClass: 'icheckbox_square-purple',
	// radioClass: 'iradio_square-purple'
	// });

	$('#documentSectionBrowser a[data-toggle="tab"]').on('shown.bs.tab', onSectionTabActivated);

	$("#documentSectionBrowser").on('click', '.deleteSection', deleteCurrentSection);
	// $("#documentSectionBrowser").on('click', ".enabledSection",
	// doDisablePricingSection);
	// $("#documentSectionBrowser").on('click', ".disabledSection",
	// enablePricingSection);


	$('#pricing-tab-check').each(function()
	{
		var name = "";
		if ($(this).attr("id") == "pricing-tab-check")
		{
			name = 'pricing-switcher';
		}
		new Switchery($(this)[0], {
			size : 'small',
			className : 'switchery switchery-small tab-switchery ' + name
		});
		$(this).change(function(e)
		{
			if ($(this).is(':checked') && $(this).attr("id") == "pricing-tab-check")
			{
				enablePricingSection();
			}
			else if ($(this).attr("id") == "pricing-tab-check")
			{
				doDisablePricingSection();
			}
		});
	});


	$('div.fr-element.fr-view').on('click', function()
	{


	});


	$(".saveAndPreviewAction").click(doSaveAndPreviewProposal);
	$(".saveProposalAction").click(doSaveProposal);

	$('.payment-option-fields .payment-value-btn').next().find('a').click(function()
	{
		$(this).closest('ul').prev().attr('data-type', $(this).attr('data-type')).find('span.type-content').text($(this).text());
	});

	updatePlanDependencies();

	if (addTemplate == false)
	{
		restoreProposal();
	}
	else
	{
		var tableOfContentSwitchery = new Switchery($('#tableOfContent').get(0), {
			size : 'small'
		});

		var landscapeModeSwitchery = new Switchery($('#landscapeMode').get(0), {
			size : 'small'
		});
		var globalTitleHiddenSwitchery = new Switchery($('#globalTitleHidden').get(0), {
			color : '#fc0d00',
			size : 'small'
		});
	}

	$(document).on('scroll', '.scroll-holder', function()
	{
		$(this).closest('.panel-body').find('#scrollH').hide()
	});

	$('.pageBreakSection').css('visibility', 'initial');

	logger.info("Activating library");
	$('#documentSectionBrowser a[href="#doc-pricing"]').click();
	onSectionTabActivated();// HACK but seems to work

	$(".documentTagSrc").keyup(function(e)
	{
		var $target = $(e.target);
		var tag = $target.attr("tag");
		$('.documentTag[tag="' + tag + '"]').text($target.val());
	});

	$(".documentTagSrc").change(function(e)
	{
		var $target = $(e.target);
		var tag = $target.attr("tag");
		$('.documentTag[tag="' + tag + '"]').text($target.val());
	});

	$(".editorContent").scroll(function(e)
	{
		var editor = CKEDITOR.currentInstance;
		if (editor != undefined)
		{
			editor.fire('focus');
		}
	});

	window.onbeforeunload = confirmExit;

	var $sidebar = $("#sidebar");
	var $mainPanel = $(".main-content");
	var $actions = $(".libraryActions");

	$("#sidebar").resizable({
		minWidth : 225,
		maxWidth : 450,
		handles : "w",
		start : function()
		{
			$(".main-container").addClass("no-transition");
		},
		resize : function(event, ui)
		{
			$mainPanel.css("right", ui.size.width + "px");
			$actions.css("right", (ui.size.width + 30) + "px");
		},
		stop : function()
		{
			$(".main-container").removeClass("no-transition");
		}
	});

	$(document).on('content-library-changed', function()
	{
		DocumentLibrary.rebuildTree();
	});

	maestranoOrganizationValidation();

	$('.documentTagSrc.cust-fld-date').datepicker({
		autoclose : true,
		format : {
			toDisplay : function(date, format, language)
			{
				var d = new Date(date);
				d.setMinutes(d.getMinutes() + d.getTimezoneOffset());
				return moment(d).format(dateFormatPattern);
			},
			toValue : function(date, format, language)
			{
				var d = new Date(date);
				d.setMinutes(d.getMinutes() + d.getTimezoneOffset());
				return moment(d).format(dateFormatPattern);
			}
		}
	});

	var chbx = $("#displayOrderButton");
	var chk = $("#showOrderChk");
	chk.hover(function()
	{
		chk.toggleClass("hover");
	});
	chk.click(function()
	{
		chk.attr("class").indexOf("checked") < 0 ? check(chk) : unCheck(chk);
	});

	function check(chk)
	{
		chk.addClass("checked")
		chbx.prop("checked", true);
	}

	function unCheck()
	{
		chk.removeClass("checked");
		chbx.prop("checked", false);
	}

	$("#showRemarks").change(function()
	{
		showHideRemarks();
	});

	showHideRemarks();

	$('#form-add-template').validate({
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error, element)
		{

		},
		ignore : '',
		rules : {
			templateTitle : {
				minlength : 2,
				maxlength : 50,
				required : true
			},
			templateDescription : {
				minlength : 2,
				maxlength : 255,
				required : true
			},
		},
		highlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-error');
		},
		success : function(label, element)
		{
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		submitHandler : function(form)
		{
			processSaveProposal();
		}
	});
});

$(window).resize(function()
{
	checkScrollTable();
});

function showHideRemarks()
{
	if ($('#showRemarks').is(':checked'))
	{
		$('#itinerarySection .remark').show();
	}
	else
	{
		$('#itinerarySection .remark').hide();
	}
}
function checkScrollTable()
{
	$('.pricing-droppable:not(#htmlFragments .pricing-droppable)').each(function()
	{
		if ($('.scroll-holder', $(this)).width() < $('[id="pricingTable"]', $(this)).width())
		{
			$('[id="scrollH"]', $(this)).css('display', 'flex');
		}
		else
		{
			$('[id="scrollH"]', $(this)).hide();
		}
	})
}


function getCustomField(object, key)
{
	var $formGroup = $("<div>").addClass("form-group");
	$formGroup.append($("<label>").addClass("col-sm-3 control-label").attr("for", key).text(object.label + ":"));
	var field = $("<div>").addClass("col-sm-8");
	if (object.type === "text")
	{
		field.append($("<input>").attr("type", "text").attr("id", key).attr("tag", key).attr("name", key).addClass("form-control documentTagSrc"));
	}
	else if (object.type === "dropdown")
	{
		var select = $("<select>").attr("id", key).attr("tag", key).attr("name", key).addClass("form-control documentTagSrc");
		var options = object.values.split("|");
		select.append($("<option>").attr("value", "").text(""));
		$.each(options, function(index, value)
		{
			select.append($("<option>").attr("value", value).text(value.trim()));
		});
		field.append(select);
	}
	$formGroup.append(field);
	return $formGroup;
}

function highlightMovingItem($target)
{
	var originalBackgroundColor = $target.css("background-color");

	$target.animate({
		backgroundColor : "#FDE8C1"
	}, {
		duration : 500,
		queue : true
	});

	$target.animate({
		backgroundColor : originalBackgroundColor
	}, {
		duration : 500,
		queue : true
	});
}

function scrollToTarget(targetSelector, topElementSelector, scrollContainerSelector)
{
	var $target = $(targetSelector);
	var $scrollContainer = $(scrollContainerSelector);
	var offset = $target.offset().top;
	if (offset < 0 || offset > $scrollContainer.height())
	{
		var $topElement = $target.closest(topElementSelector);
		var topOffset = $topElement.offset().top;
		$scrollContainer.scrollTop(offset - topOffset);
	}
	highlightMovingItem($target);
}

function setupEditorInteractions()
{
	logger.info('setupEditorInteractions(...)');
	isTemplate = $("#proposal-template").val() === "true";

	// UP ARROW
	$(document).on('click', '.move-up', function()
	{
		var $current = $(this).closest('.group-panel');
		var $currentPanel = $current.find('.panel').eq(0);

		if ($current.prevAll('.group-panel:not(.fixed)').length == 0)
		{
			return false;
		}
		var $previous = $current.prev('.group-panel');
		var $previousPanel = $previous.find('.panel').eq(0);
		$currentPanel.attr("dataOrder", parseInt($currentPanel.attr("dataOrder")) - 1);
		$previousPanel.attr("dataOrder", parseInt($previousPanel.attr("dataOrder")) + 1);

		$previous.fadeOut('fast');
		$current.fadeOut('fast', function()
		{
			$current.insertBefore($previous).fadeIn('fast', function()
			{
				scrollToTarget($current, ".tab-pane", ".editorContent");
			});
			$previous.fadeIn('fast');
			var $section = $current.closest('.tab-pane');
			if ($section.attr('id') == 'doc-pricing')
			{
				setTimeout(updateOverallTotalPricing, 500);
			}
		});
	});

	// DOWN ARROW
	$(document).on('click', '.move-down', function()
	{
		var $current = $(this).closest('.group-panel');
		var $currentPanel = $current.find('.panel').eq(0);

		if ($current.nextAll('.group-panel:not(.fixed)').length == 0)
		{
			return false;
		}
		var $next = $current.next();
		var $nextPanel = $next.find('.panel').eq(0);
		$currentPanel.attr("dataOrder", parseInt($currentPanel.attr("dataOrder")) + 1);
		$nextPanel.attr("dataOrder", parseInt($nextPanel.attr("dataOrder")) - 1);

		$next.fadeOut('fast');
		$current.fadeOut('fast', function()
		{
			$current.insertAfter($next).fadeIn('fast', function()
			{
				scrollToTarget($current, ".tab-pane", ".editorContent");
			});
			$next.fadeIn('fast');

			var $section = $current.closest('.tab-pane');
			if ($section.attr('id') == 'doc-pricing')
			{
				setTimeout(updateOverallTotalPricing, 500);
			}
		});
	});

	$(document).on('click', '.minimum', function()
	{
		$(this).removeClass('minimum').addClass('maximum').find('.fa').removeClass('fa-window-minimize').addClass('fa-window-restore');
		var $current = $(this).closest('.group-panel');
		var $currentPanel = $current.find('.panel').eq(0);
		$currentPanel.children(':not(.panel-heading)').addClass('collapse');
	});

	$(document).on('click', '.maximum', function()
	{
		$(this).removeClass('maximum').addClass('minimum').find('.fa').removeClass('fa-window-restore').addClass('fa-window-minimize');
		var $current = $(this).closest('.group-panel');
		var $currentPanel = $current.find('.panel').eq(0);
		$currentPanel.children(':not(.panel-heading)').removeClass('collapse');
	});

	$(document).on('click', '.clone', function()
	{
		var proposal = getCurrentProposal();
		var $group = $(this).closest('.group-panel');
		var $panel = $(this).closest('.panel');
		var $currentSection = $(this).closest('.contentContainer');
		var $tabPane = $(this).closest('.tab-pane');
		var $a = $('#documentSectionBrowser a[href="#' + $tabPane.attr('id') + '"]');

		var sectionIndex = $('#documentSectionBrowser a').index($a);
		var contentIndex = $panel.attr('dataOrder');
		var section = proposal.sections[sectionIndex];
		var content = section.content[contentIndex];
		var callback;
		if (($panel.attr('type') == 'generated' && $panel.attr('dataid') == 'con-plans') || $panel.attr('type') == 'editor' || $panel.attr('type') == 'text' || $panel.attr('type') == 'imageAttachment' || $panel.attr('type') == 'spreadsheet')
		{
			callback = restoreContent(section.content.length, content, $currentSection);
		}
		else
		{
			alert('This content cannot be duplicated! Please contact support team.');
			return;
		}
		if (callback)
		{
			callback.done(function()
			{
				var $newGroup = $currentSection.find('.group-panel').last();
				$newGroup.insertAfter($group);
				updateDataOrder($currentSection);
				if ($panel.attr('type') == 'editor')
				{
					initFroalaEditor();
				}
				else if ($panel.attr('type') == 'spreadsheet')
				{
					var spreadsheetIndex = $('.spreadsheet').index($panel.find('.spreadsheet'));
					initKendoSpreadsheet($newGroup, proposal.spreadsheets[spreadsheetIndex]);
				}
			})
		}
		else
		{
			var $newGroup = $currentSection.find('.group-panel').last();
			$newGroup.insertAfter($group);
			updateDataOrder($currentSection);
			if ($panel.attr('type') == 'generated' && $panel.attr('dataid') == 'con-plans')
			{
				// pricing table
				var pricingIndex = $('.panel.pricing-droppable').index($panel);
				restoreProposalPricing(proposal.pricings[pricingIndex], $newGroup.find('.panel.pricing-droppable'));
			}
		}

	});

	$(document).on('click', '.lock', function()
	{
		var $panel = $(this).closest('.panel');
		var fixed = $panel.attr('fixed') === "true";
		$panel.attr('fixed', !fixed);
		var lockIcon = $(this).find('> i');
		var infoText = $(this).find('> span');
		if (!fixed)
		{
			lockIcon.attr('class', 'fa fa-lock');
			infoText.text('Delete Not Allowed');
			$panel.find('.panel-body').css('background', "rgb(253, 221, 230)");
			$panel.find('.panel-froala .fr-wrapper').css('background', "rgb(253, 221, 230)");
			$panel.find('.panel-tools').width(338);
		}
		else
		{
			lockIcon.attr('class', 'fa fa-unlock');
			infoText.text('Delete Allowed');
			$panel.find('.panel-body').css('background', "rgb(255, 255, 255)");
			$panel.find('.panel-froala .fr-wrapper').css('background', "rgb(255, 255, 255)");
			$panel.find('.panel-tools').width(314);
		}
	});

	$(document).on('click', '.remove', function()
	{
		var group = $(this).closest('.group-panel');
		updateDataOrderNext(group, false);
		removeContentPanel(group);

		var $section = $(this).closest('.tab-pane');
		if ($section.attr('id') == 'doc-pricing')
		{
			setTimeout(updateOverallTotalPricing, 500);
		}
	});

	$('.tab-content').on('keyup', '.sectionTitle', function()
	{
		if ($("#documentSectionBrowser").hasClass('collapsed'))
		{
			var tabId = $(this).closest('.tab-pane').attr('id');
			$('[href="#' + tabId + '"] div.tab-label').attr('title', $(this).val()).text($(this).val());
		}
		else
		{
			var tabId = $(this).closest('.tab-pane').attr('id');
			if (tabId != 'doc-pricing')
			{
				$('[href="#' + tabId + '"] div.tab-label').text($(this).val());
			}
		}
	});

	$('.add-new-tab').click(function()
	{
		createSection('Type section name here', "off", $('#globalTitleHidden').is(':checked') ? "on" : "off", true);
	});

	$(".collapse-side").click(function()
	{
		var left = $('.editorContent').position().left;
		if ($("#documentSectionBrowser").hasClass('collapsed'))
		{
			uncollapseMenu();
		}
		else
		{
			collapseMenu();
		}
	})

	$('.nav-tabs').sortable({
		axis : 'y',
		distance : 10,
		placeholder : "ui-state-highlight",
		items : "li:not(.ui-sort-disabled)",
		update : function()
		{
			if ($("#documentSectionBrowser").hasClass('collapsed'))
			{
				var x = 1;
				$(this).find('li div.tab-numbered').each(function()
				{
					$(this).text(x + ".");
					x++;
				})
			}
		}
	});

	$(document).on('click', '.addText', function()
	{
		var $currentSection = $(".editorContent").find(".tab-pane.active .contentContainer");

		var addTxtDiv = $(this).parent();
		var parent = $(addTxtDiv).parent();
		addTxtDiv.css('display', 'none');
		var txtDiv = addTxtDiv.siblings('.panel-body');
		txtDiv.insertAfter(addTxtDiv);
		txtDiv.show();
		initSpecificFroalaEditor($currentSection, parent.attr('dataorder'));
		var imageDiv = addTxtDiv.siblings('.freetext-inline-image');
		$('.radial > .radialButtons', imageDiv).show();
		$('.radial > .radialButtons', imageDiv).not('.align-pos button').removeClass('active');
		$('.radial > .radialButtons button[data-alignment=top]', imageDiv).addClass('active');
		txtDiv.focus();
	});

	$(document).on('change', '.imageAlignmentEditor select, .imageAlignmentEditor input', function()
	{
		var pos;
		var size;
		var width = '100%';

		var $alignment = $(this).closest('.imageAlignmentEditor').find('[name="itemAlignment"]');
		var $width = $(this).closest('.imageAlignmentEditor').find('[name="itemWidth"]');
		var $size = $(this).closest('.imageAlignmentEditor').find('[name="itemSize"]');

		pos = $alignment.val();
		size = $size.val();
		width = $width.val();

		var imgDiv = $(this).closest('.panel').find('.freetext-inline-image');
		var txtDiv = imgDiv.siblings('.panel-froala');

		if ($(this).closest('.imageAlignmentEditor').find('[name="type"]').val() == "video")
		{
			alignVideo(pos, imgDiv, txtDiv, size);
		}
		else
		{
			alignImage(pos, imgDiv, txtDiv, width);
		}
	});

	$(document).on('change', '[name="itemWidth"]', function()
	{
		var min = parseInt($(this).attr('min'));
		var max = parseInt($(this).attr('max'));
		if (parseInt($(this).val()) > max)
		{
			$(this).val(max);
		}
		if (parseInt($(this).val()) < min)
		{
			$(this).val(min);
		}
	})

	$(document).on('click', '#radialImageAlignmentButtons .dropdown-menu li', function()
	{
		var $alignmentPanel = $(this).closest('#radialImageAlignmentButtons');
		if ($(this).attr('data-alignment') != null)
		{
			$alignmentPanel.find('[name="itemAlignment"]').val($(this).attr('data-alignment')).trigger('change');
		}
		if ($(this).attr('data-size') != null)
		{
			$alignmentPanel.find('[name="itemSize"]').val($(this).attr('data-size')).trigger('change');
		}
	})

	$(document).on('click', '.radial > .radialButtons', function()
	{
		var pos;
		var size;
		var width = '100%';

		if ($(this).attr('data-alignment') != null)
		{
			pos = $(this).attr('data-alignment');
		}
		if ($(this).attr('data-size') != null)
		{
			size = $(this).attr('data-size');
		}
		if ($(this).attr('data-width') != null)
		{
			width = $(this).attr('data-width');
		}

		if ($(this).hasClass('position'))
		{
			if ($(this).siblings('#fa-size-3').hasClass('active') || $(this).siblings('#fa-size-4').hasClass('active'))
			{
				if ($(this).siblings('#fa-size-3').hasClass('active'))
				{
					$(this).siblings('#fa-size-3').removeClass('active');
				}
				if ($(this).siblings('#fa-size-4').hasClass('active'))
				{
					$(this).siblings('#fa-size-4').removeClass('active');
				}
				$(this).siblings('#fa-size-2').addClass('active');
				$(this).addClass("active").siblings().not('#size').removeClass("active");

				var cloneSizeNode = $('#fa-size-2').find('.holder').clone();
				var mainSizeNode = $('.group-button.size');
				mainSizeNode.find('.holder').replaceWith(cloneSizeNode);

				var mainNode = $(this).siblings('.group-button.open');
				var cloneNode = $(this).find('.holder').clone();
				mainNode.find('.holder').replaceWith(cloneNode);

			}
			else
			{
				$(this).siblings('.position').removeClass('active');
				$(this).addClass('active');
				var mainNode = $(this).siblings('.group-button.open');
				var cloneNode = $(this).find('.holder').clone();
				mainNode.find('.holder').replaceWith(cloneNode);

				if ($('#fa-size-1').hasClass('active'))
				{
					pos += 'top';
				}
			}

			$(this).siblings('.width').each(function()
			{
				if ($(this).hasClass('active'))
				{
					width = $(this).attr('data-width') + "%";
				}
			});

		}
		else if ($(this).is('#fa-size-1') || $(this).is('#fa-size-2'))
		{
			if ($(this).siblings('#fa-size-3').hasClass('active') || $(this).siblings('#fa-size-4').hasClass('active') || $(this).siblings('#fa-size-1').hasClass('active') || $(this).siblings('#fa-size-2').hasClass('active'))
			{

				$(this).siblings('.radialButtons.size').each(function()
				{
					if ($(this).hasClass("active") && !$(this).hasClass("image-size-button"))
					{
						$(this).removeClass('active');
					}
				});
				$(this).addClass('active');
				if (!($(this).siblings('#fa-size-3').hasClass('active')) || !($(this).siblings('#fa-size-4').hasClass('active')))
				{
					$(this).siblings('#fa-position-1').addClass('active');
				}

				var clonePositionNode = $('#fa-position-1').find('.holder').clone();
				var mainPositionNode = $('.group-button.position');
				mainPositionNode.find('.holder').replaceWith(clonePositionNode);

				var mainNode = $(this).siblings('.group-button.open');
				var cloneNode = $(this).find('.holder').clone();
				mainNode.find('.holder').replaceWith(cloneNode);
			}

			$(this).siblings('.width').each(function()
			{
				if ($(this).hasClass('active'))
				{
					width = $(this).attr('data-width') + "%";
				}
			});
		}
		else if ($(this).is('#fa-size-3') || $(this).is('#fa-size-4'))
		{
			$(this).siblings('.radialButtons.size').each(function()
			{
				if ($(this).hasClass("active") && !$(this).hasClass("image-size-button"))
				{
					$(this).removeClass('active');
				}
			});
			$(this).siblings('.position').removeClass('active');
			$(this).addClass('active');
			var mainNode = $(this).siblings('.group-button.open');
			var cloneNode = $(this).find('.holder').clone();
			mainNode.find('.holder').replaceWith(cloneNode);

			$(this).siblings('.width').each(function()
			{
				if ($(this).hasClass('active'))
				{
					width = $(this).attr('data-width') + "%";
				}
			});
		}
		else if ($(this).hasClass('aspect'))
		{
			$(this).siblings('.aspect').removeClass('active');
			$(this).addClass('active');
			var mainNode = $(this).siblings('.group-button.open');
			var cloneNode = $(this).find('.holder').clone();
			mainNode.find('.holder').replaceWith(cloneNode);

			$(this).siblings('.position').each(function()
			{
				if ($(this).hasClass('active'))
				{
					pos += $(this).attr('data-alignment');
				}
			});

			$(this).siblings('.size').each(function()
			{
				if ($(this).hasClass('active'))
				{
					pos += $(this).attr('data-alignment');
				}
			});

		}
		else if ($(this).hasClass('width'))
		{
			$(this).siblings('.width').removeClass('active');
			$(this).addClass('active');
			var mainNode = $(this).siblings('.group-button.open');
			var cloneNode = $(this).find('.holder').clone();
			mainNode.find('.holder').replaceWith(cloneNode);


			$(this).siblings('.position').each(function()
			{
				if ($(this).hasClass('active'))
				{
					pos += $(this).attr('data-alignment');
				}
			});

			$(this).siblings('.size').each(function()
			{
				if ($(this).hasClass('active'))
				{
					pos += $(this).attr('data-alignment');
				}
			});

			$(this).siblings('.width').each(function()
			{
				if ($(this).hasClass('active'))
				{
					width = $(this).attr('data-width') + "%";
				}
			});

		}
		else
		{
			$(this).siblings('.position').removeClass('active');
			$(this).addClass('active');
			var mainNode = $(this).siblings('.group-button.open');
			var cloneNode = $(this).find('.holder').clone();
			mainNode.find('.holder').replaceWith(cloneNode);
		}


		var imgDiv = $(this).closest('.freetext-inline-image');
		var txtDiv = imgDiv.siblings('.panel-froala');

		if ($(this).attr('data-type') == "video")
		{
			alignVideo(pos, imgDiv, txtDiv, size);
		}
		else
		{
			alignImage(pos, imgDiv, txtDiv, width);
		}

	});

	// Clicking the PDF embed button will trigger new page button
	$(document).on('change', '.embedPDFSwitch', function()
	{
		var panel = $(this).parents('.panel.viewable');
		var controls = $(panel).find('.newPageSwitch');
		if (!$(this).hasClass('active') && (!controls.hasClass('active')))
		{
			controls.click();
			controls.addClass('active');
		}
		$(this).toggleClass('active');

	});


	// Toggle active for pdf button features
	$(document).on('change', '.newPageSwitch', function()
	{
		if ($(this).hasClass('active'))
		{
			$(this).removeClass('active');
		}
		else
		{
			$(this).addClass('active');
		}

	});

	// Display and retract image settings menu (radial FAB)
	$(document).on('click', '.radial .fab', function()
	{

		// CHECK AND CHANGE ICONS
		var sizeFab = $(this).siblings('.radialButtons.size');
		if (!($(this).hasClass('open')))
		{
			$(sizeFab).each(function()
			{
				if ($(this).hasClass('active'))
				{
					var node = $(this);
					var cloneNode = $(node).find('.holder').clone();
					var mainNode = $(this).siblings('.group-button.size');
					mainNode.find('.holder').replaceWith(cloneNode);
				}
			});
		}

		var positionFab = $(this).siblings('.radialButtons.position');
		if (!($(this).hasClass('open')))
		{
			$(positionFab).each(function()
			{
				if ($(this).hasClass('active'))
				{
					var node = $(this);
					var cloneNode = $(node).find('.holder').clone();
					var mainNode = $(this).siblings('.group-button.position');
					mainNode.find('.holder').replaceWith(cloneNode);
				}
			});
		}

		var aspectFab = $(this).siblings('.radialButtons.aspect');
		if (!($(this).hasClass('open')))
		{
			$(aspectFab).each(function()
			{
				if ($(this).hasClass('active'))
				{
					var node = $(this);
					var cloneNode = $(node).find('.holder').clone();
					var mainNode = $(this).siblings('.group-button.aspect');
					mainNode.find('.holder').replaceWith(cloneNode);
				}
			});
		}

		var widthFab = $(this).siblings('.radialButtons.width');
		if (!($(this).hasClass('open')))
		{
			$(widthFab).each(function()
			{
				if ($(this).hasClass('active'))
				{
					var node = $(this);
					var cloneNode = $(node).find('.holder').clone();
					var mainNode = $(this).siblings('.group-button.width');
					mainNode.find('.holder').replaceWith(cloneNode);
				}
			});
		}

		displayMainButtons($(this));
		displayMainButtons($(this).parent());

		if ($(this).hasClass('open'))
		{
			displaySubButtons($(this), true);
		}
		else
		{
			displaySubButtons($(this), false);
		}
		if ($(this).siblings('.image-position-button').hasClass('open'))
		{
			$(this).siblings('.image-position-button').click();
		}
		if ($(this).siblings('.image-size-button').hasClass('open'))
		{
			$(this).siblings('.image-size-button').click();
		}
		if ($(this).siblings('.image-aspect-button').hasClass('open'))
		{
			$(this).siblings('.image-aspect-button').click();
		}
		if ($(this).siblings('.image-width-button').hasClass('open'))
		{
			$(this).siblings('.image-width-button').click();
		}
	});
	$(document).on('click', '.image-position-button', function()
	{
		if ($(this).siblings('.image-size-button').hasClass('open'))
		{
			$(this).siblings('.image-size-button').click();
		}
		if ($(this).siblings('.image-aspect-button').hasClass('open'))
		{
			$(this).siblings('.image-aspect-button').click();
		}
		if ($(this).siblings('.image-width-button').hasClass('open'))
		{
			$(this).siblings('.image-width-button').click();
		}

		displayMainButtons($(this));
		displayMainButtons($(this).siblings('#fa-position-1'));
		displayMainButtons($(this).siblings('#fa-position-2'));
		displayMainButtons($(this).siblings('#fa-position-3'));
		displayMainButtons($(this).siblings('#fa-position-4'));

	});
	$(document).on('click', '.image-size-button', function()
	{
		if ($(this).siblings('.image-position-button').hasClass('open'))
		{
			$(this).siblings('.image-position-button').click();
		}
		if ($(this).siblings('.image-aspect-button').hasClass('open'))
		{
			$(this).siblings('.image-aspect-button').click();
		}
		if ($(this).siblings('.image-width-button').hasClass('open'))
		{
			$(this).siblings('.image-width-button').click();
		}
		displayMainButtons($(this));
		displayMainButtons($(this).siblings('#fa-size-1'));
		displayMainButtons($(this).siblings('#fa-size-2'));
		displayMainButtons($(this).siblings('#fa-size-3'));
		displayMainButtons($(this).siblings('#fa-size-4'));
	});
	$(document).on('click', '.image-width-button', function()
	{
		if ($(this).siblings('.image-position-button').hasClass('open'))
		{
			$(this).siblings('.image-position-button').click();
		}
		if ($(this).siblings('.image-aspect-button').hasClass('open'))
		{
			$(this).siblings('.image-aspect-button').click();
		}
		if ($(this).siblings('.image-size-button').hasClass('open'))
		{
			$(this).siblings('.image-size-button').click();
		}
		displayMainButtons($(this));
		displayMainButtons($(this).siblings('#fa-width-1'));
		displayMainButtons($(this).siblings('#fa-width-2'));
		displayMainButtons($(this).siblings('#fa-width-3'));
		displayMainButtons($(this).siblings('#fa-width-4'));
		displayMainButtons($(this).siblings('#fa-width-5'));
	});
	$(document).on('click', '.image-aspect-button', function()
	{
		if ($(this).siblings('.image-position-button').hasClass('open'))
		{
			$(this).siblings('.image-position-button').click();
		}
		if ($(this).siblings('.image-size-button').hasClass('open'))
		{
			$(this).siblings('.image-size-button').click();
		}
		displayMainButtons($(this));
		displayMainButtons($(this).siblings('#fa-aspect-1'));
		displayMainButtons($(this).siblings('#fa-aspect-2'));
		displayMainButtons($(this).siblings('#fa-aspect-3'));
	});

	function displayMainButtons($element)
	{
		$element.toggleClass('open');
		if ($element.children('.hamburger--slider'))
		{
			$element.children('.hamburger--slider').toggleClass('is-active');
		}
	}

	function displaySubButtons($element, display)
	{
		if (display)
		{
			setTimeout(function()
			{
				for ( var radial in radialArray)
				{
					var $elem = $element.siblings(radialArray[radial]);
					$elem.show();
				}
			}, 500);
		}
		else
		{
			for ( var radial in radialArray)
			{
				var $elem = $element.siblings(radialArray[radial]);
				$elem.hide();
			}
		}
	}


	// startSortable($('[id="pricingTable"]'));

	// Fix for JQueryUI sortable not working on Microsoft Surface.
	if (window.navigator.msPointerEnabled)
	{
		logger.info("Enabling drag and drop... On IE");
		$('.nav-tabs').css('-ms-touch-action', 'none').css('-ms-touch-select', 'none');
	}

	$(document).on('click', '#itiStyleSelect li', function()
	{
		$(this).siblings().removeClass("selected");
		$(this).addClass("selected");
		var style = $(this).val();
		$('#itiStyle').text($(this).text());
		var showRemarks = $('#showRemarks').is(':checked')
		redrawItinerary(style, showRemarks);
	});
}

function redrawItinerary(style, showRemarks)
{
	if (!$('#foreground-loading').is(':visible'))
	{
		startLoadingForeground();
	}
	var data = {};
	data.type = 'itinerary';
	data.proposalId = $("#param-proposal-id").val();
	data.style = style;
	data.showRemarks = showRemarks;

	$.ajax({
		type : "POST",
		url : '/api/editor/content',
		data : data
	}).done(function(data, textStatus, jqXHR)
	{
		var itinerarySection = $('#itinerarySection');
		var newContent = $('#itinerarySection', $(data));

		itinerarySection.html(newContent.html());
		showHideRemarks();
		stopLoadingForeground();
	});


}
function displayLearningCenterAction()
{
	logger.info("Displaying Learning Center Modal");

	var $modal = $('#learning-center-modal');
	$('body').modalmanager('loading');

	$modal.load('/application/dialog/learningCenter.jsp', {}, function()
	{
		$modal.modal({
			width : '760px',
			backdrop : 'static',
			keyboard : false
		});
		setupModalsBackgroundColor();
	});

	return $modal;
}

function startSortable(pricingTables)
{
	$(pricingTables).each(function()
	{
		$(this).sortable({
			axis : 'y',
			distance : 10,
			placeholder : "ui-section-state-highlight",
			items : "tbody.customPricingRows:not(.standard)",
			handle : "span.section-sortable-handle",
			connectWith : '[id="pricingTable"]',
			start : function(event, ui)
			{
				ui.placeholder.css("display", "inline-block");
				ui.placeholder.css("height", ui.helper.height());
			},
			update : function()
			{
				fixPricingRowsPlaceholder($(this));
				updateSummariseRow(this);
			}
		});
		startRowSortable($('.customPricingRows.standard', $(this)), $(this));
	});
}


function startRowSortable(section, $pricingTable)
{
	var scrollInterval;
	$(section).sortable({
		axis : 'y',
		distance : 10,
		placeholder : "ui-state-highlight",
		dropOnEmpty : true,
		cursor : 'pointer',
		cursorAt : {
			cursor : "move",
			left : 5
		},
		tolerance : "pointer",
		// connectWith : $pricingTable.find(".customPricingRows"),
		connectWith : '[id="pricingTable"] .customPricingRows',
		items : "tr.customPricing:not([data-type='title'])",
		handle : "span.sortable-handle",
		start : function()
		{
			clearSorting();
			$(editorContent).mouseover(function(e)
			{
				e = e || window.event;
				var cursor = {
					y : 0
				};
				cursor.y = e.pageY; // Cursor YPos
				clearInterval(scrollInterval);
				var papaWindow = $(editorContent);
				var $pxFromTop = $(papaWindow).scrollTop();
				var $userScreenHeight = $(papaWindow).height();
				var $contentHeight = $('.tab-pane:visible').height();
				if (cursor.y < $contentHeight)
				{
					if ((cursor.y) > (($(window).height()) - 250))
					{
						scrollInterval = setInterval(function()
						{
							$(".editorContent").scrollTop($(".editorContent").scrollTop() + 40)
						}, 50);
					}
					else if (cursor.y < 250)
					{
						scrollInterval = setInterval(function()
						{
							$(".editorContent").scrollTop($(".editorContent").scrollTop() - 40)
						}, 50);
					}
				}
			});
		},
		sort : function()
		{
			var updateSortablePosition;
			if (updateSortablePosition != undefined)
			{
				clearTimeout(updateSortablePosition);
			}
			updateSortablePosition = setTimeout(function()
			{
				$(section).sortable("refreshPositions");
			}, 500);
		},
		stop : function()
		{
			$(editorContent).unbind('mouseover');
			clearInterval(scrollInterval);
		},
		update : function()
		{
			fixPricingRowsPlaceholder($pricingTable);
			updateSummariseRow(this);
		}
	});
}

function fixPricingRowsPlaceholder($pricingTable)
{
	var title = 1;
	var price = 1;
	$.each($("tbody .customPricing", $pricingTable), function(i, e)
	{
		switch ($(e).attr("data-type"))
		{
			case "title":
				$('.rowDescription', e).attr("placeholder", "Section " + title);
				title++;
				break;
			case "price":
				$('.rowDescription', e).attr("placeholder", "Price Item " + price);
				price++;
				break;
		}
	});
}

function detectOrientation()
{

	checkScrollTable()

	switch (window.orientation)
	{
		case 0:
			collapseMenu();
			break;
		case 90:
			uncollapseMenu();
			break;
		case -90:
			uncollapseMenu();
			break;
		case 180:
			collapseMenu();
			break;
		default:
			uncollapseMenu();
			break;
	}
}

function removeContentPanel($contentPanel)
{
	$section = $contentPanel.closest('.contentContainer');

	$contentPanel.slideUp(function()
	{
		$(this).remove();
		updateDataOrder($section);
	});

}

function updateDataOrder($section)
{
	$section.find('.group-panel:not(.fixed)').each(function(i, e)
	{
		$(e).find('.panel').attr("dataOrder", i);
	});
}

function collapseMenu()
{
	var x = 1;
	$('#documentSectionBrowser').addClass('collapsed').find('li').outerWidth(60);
	$('.add-new-tab').outerWidth(60).find("span").text("");
	$('.collapse-side').find('i').removeClass('fa-arrow-circle-left').addClass('fa-arrow-circle-right')
	$('.tabbable').outerWidth(60);
	$('.editorContent').css('left', 66);
	$('#documentSectionBrowser div.tab-label').hide();
	$('#documentSectionBrowser div.tab-numbered').each(function()
	{
		$(this).attr('title', $(this).text());
		$(this).text(x + ".");
		x++;
	}).show();
	setTimeout(function()
	{
		refreshTextareas();
	}, 300);
}

function uncollapseMenu()
{
	$('#documentSectionBrowser').removeClass('collapsed').find('li').outerWidth(150);
	$('.add-new-tab').outerWidth(150).find('span').text(" Add Section ");
	$('.collapse-side').find('i').removeClass('fa-arrow-circle-right').addClass('fa-arrow-circle-left');
	$('.tabbable').outerWidth(150)
	$('.editorContent').css('left', 156);
	$('#documentSectionBrowser div.tab-label').show();
	$('#documentSectionBrowser div.tab-numbered').each(function()
	{
		$(this).text($(this).attr('title'));
		$(this).attr('title', '');
	}).hide();
	setTimeout(function()
	{
		refreshTextareas();
	}, 300);
}

function proposalChanged()
{
	var currentHash = MD5(JSON.stringify(getCurrentProposal()));

	return (lastSave != "" && lastSave != currentHash);
}

function confirmExit()
{
	if (proposalChanged())
	{
		return "You have made changes to this proposal. Are you sure you would like to navigate away from this page without saving?";
	}
}

function dismissCK()
{
	if (CKEDITOR.currentInstance != undefined)
	{
		new CKEDITOR.focusManager(CKEDITOR.currentInstance).blur(true);
	}
}

function insertContentIntoEditor(targetSection, dataIndex, itemType, itemId, autoscroll, fixedPanel, top)
{
	var sendData = {};
	var proposalId = $("#param-proposal-id").val();
	sendData.type = itemType;
	sendData.proposalId = proposalId;
	if (itemId != undefined)
	{
		sendData.id = itemId;
	}

	return $.ajax({
		type : "POST",
		url : '/api/editor/content',
		data : sendData
	}).done(function(data, textStatus, jqXHR)
	{
		if (!top)
		{
			insertContentIntoPanel(targetSection, dataIndex, autoscroll, fixedPanel, data);
			if (itemType == 'Spreadsheet')
			{
				var $panel = $('.panel[dataOrder="' + dataIndex + '"]', targetSection);
				if (itemId)
				{
					$.ajax({
						type : "POST",
						url : '/api/editor/spreadsheet',
						data : {
							itemId : itemId
						}
					}).done(function(data, textStatus, jqXHR)
					{
						console.log(data);
						initKendoSpreadsheet($panel, data);
					});
				}
				else
				{
					initKendoSpreadsheet($panel);
				}
			}
		}
		if (itemType == "itinerary")
		{
			showHideRemarks();
		}
	});
}

function insertContentIntoPanel(targetSection, dataIndex, autoscroll, fixedPanel, content, dropId)
{
	var $newPane = $(content);
	var wrapped = wrapPanel($newPane, fixedPanel, targetSection);
	var switchery = new Switchery(wrapped.find('.js-switch').get(0), {
		size : 'small'
	});
	if (wrapped.find('.embedPDFSwitch').length > 0)
	{
		var pdfSwitchery = new Switchery(wrapped.find('.embedPDFSwitch').get(0), {
			size : 'small'
		});
	}
	$newPane.attr("dataOrder", dataIndex);
	if (!isTemplate)
	{
		$newPane.find('.lock').addClass('disabled').find('> i').attr('class', 'fa');
	}

	var $existingPanels = $("> .group-panel > .panel", targetSection);
	var inserted = false;
	$.each($existingPanels, function(i, e)
	{
		var $targetPanel = $(e);
		var eDataOrder = parseInt($targetPanel.attr("dataOrder"));
		if (eDataOrder > dataIndex)
		{
			var group = $targetPanel.closest('.group-panel');
			wrapped.insertBefore(group);
			inserted = true;
			return false;
		}

	});
	if (!inserted)
	{
		targetSection.append(wrapped);
	}

	$newPane.find('input, textarea').placeholder();

	// Force the document tags to reload.
	$(".documentTagSrc").keyup();

	targetSection.ready(function()
	{
		if (autoscroll)
		{
			$(".editorContent").animate({
				scrollTop : $(".editorContent")[0].scrollHeight
			}, "slow");
		}
	});


	if (dropId != null)
	{
		var beforeIndex = '.dropAreaNumber' + dropId;
		var beforeDropArea = $(beforeIndex, targetSection).detach();
		targetSection.append(beforeDropArea);
	}

	if (~content.toString().indexOf("textblock"))
	{
		setupImageUploadInteractions($newPane);
	}

}

function onSectionTabActivated(event)
{
	var $activePane = $('.editorContent > .tab-pane.active');
	var paneId = $activePane.attr("id");
	logger.info("section changed to: " + paneId);

	dismissCK();

	/*
	 * This is used to update the data in the spreadsheets when the tab changes. /*
	 * Forces the spreadsheet to update the data because the user changes tabs /*
	 * while editing it will not save what they are currently changing. It also
	 * forces a refresh to /* update the spreadsheet view so it will not go
	 * blank.
	 */

	getProposalSpreadsheets();

	var tabLibraryType = $('#documentSectionBrowser a[href="#' + paneId + '"]').attr('libraryType');

	if (tabLibraryType == 'pricing')
	{
		DocumentLibrary.enterPricingMode();
	}
	else if (tabLibraryType == 'general')
	{
		DocumentLibrary.enterGeneralMode();
	}
	else if (tabLibraryType == 'cover')
	{
		DocumentLibrary.enterCoverMode();
	}
	else
	{
		DocumentLibrary.enterDormantMode();
	}

	initFroalaEditor($activePane);

	/*
	 * This is used to update the spreadsheet view for initialisation. /* This
	 * prevents issues with spreadsheets in different tabs as the width and
	 * height are calculated based on clientWidth/clientHeight /* which requires
	 * the element to be visible.
	 */
	$('.spreadsheet', $activePane).each(function()
	{
		$(this).data("kendoSpreadsheet").refresh()
	});
}

function initFroalaEditor($activePane)
{
	if ($activePane == undefined)
	{
		$activePane = $('.editorContent .tab-pane.active');
	}

	var uninitialisedFroalaEditors = $(".editable .panel-froala", $activePane);
	$.each(uninitialisedFroalaEditors, function(i, e)
	{
		$(e).on('froalaEditor.initialized', function(ev, editor)
		{
			customizeFroalaEditor(ev, editor)
		}).froalaEditor({
			enter : $.FroalaEditor.ENTER_P,
			fontSize : [ '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '22', '24', '26', '28', '30', '60', '96' ],
			fontSizeDefaultSelection : '14',
			toolbarSticky : true,
			// Set the image upload parameter.
			imageUploadParam : 'image',
			// Set the image upload URL.
			imageUploadURL : '/api/proposal/attach/image',
			// Additional upload params.
			imageUploadParams : {
				proposal : $('#param-proposal-id').val(),
				x : '0',
				y : '0',
				x2 : '900',
				y2 : '900',
				w : '100%',
				h : '100%'
			},
			// Set request type.
			imageUploadMethod : 'POST',
			// Set max image size to 5MB.
			imageMaxSize : 5 * 1024 * 1024,
			// Allow to upload PNG and JPG.
			imageAllowedTypes : [ 'jpeg', 'jpg', 'png' ],
			quickInsertButtons : [ 'image', 'table', 'ul', 'ol', 'hr' ],
			key : 'vYA6mE5C5G4A3jC1QUd1Xd1OZJ1ABVJRDRNGGUE1ITrE1D4A3C11B1B6E6B1F4I3=='
		});
	})
}

function updatePlanDependencies()
{
	plansChanged();

	// reloadUC();
	// reloadFeaturesAndBenefits();
	// reloadTermsAndConditions();
}

function doSaveProposal(event, callback)
{
	saveAndPreview = false;
	if (addTemplate == true)
	{
		$('#saveTemplateModal').modal();
	}
	else
	{
		processSaveProposal(event, callback);
	}

}

function processSaveProposal(event, callback)
{
	logger.info("Saving proposal");
	var result = checkSaveRequirements();
	if (result.ok)
	{
		var proposal = getCurrentProposal();
		saveProposal(proposal, callback);
	}
	else
	{
		swal({
			title : "Sorry",
			text : "Something have to be done before we can save: " + result.message,
			type : "error",
			showCancelButton : false,
			confirmButtonColor : "#BA131A",
			confirmButtonText : "Ok",
			closeOnConfirm : true
		}, function()
		{
			if (result.action)
				result.action();
		});
	}
}

var saveAndPreview = false;
function doSaveAndPreviewProposal(event)
{
	logger.info("Saving proposal");
	// We may need to save fist... so create a callback function.
	var preview = function()
	{
		var proposalId = $("#param-proposal-id").val();
		var vKey = $("#doc-v-key").val();
		// sendProposal(proposalId);
		startLoadingForeground(saving_and_encrypting);
		window.location = "/view?proposal=" + proposalId + "&v-key=" + vKey;
	};

	saveAndPreview = true;

	if (addTemplate == true)
	{
		$('#saveTemplateModal').modal();
	}
	else
	{
		processSaveProposal(event, preview);
	}

	// if (proposalChanged())
	// {
	// // $(document).one('foregroundLoadingComplete', preview);
	// doSaveProposal(event, preview);
	// }
	// else
	// {
	// preview();
	// }
}

// This will flip the switch on any id switchery
function changeSwitch(switch_id, active)
{
	var check = document.querySelector(switch_id);
	// console.log("check",switch_id,check.checked,"to",active);
	if (!check.checked && active)
	{
		var c = $(check).next('span').attr("class").replace("switchery ", "");
		var l = {
			"switchery-large" : "26px",
			"switchery-small" : "13px",
			"switchery-default" : "20px"
		};
		$(check).prop("checked", true).next('span').attr("style", "box-shadow: rgb(100, 189, 99) 0px 0px 0px 11px inset; border-color: rgb(100, 189, 99); background-color: rgb(100, 189, 99); transition: border 0.4s, box-shadow 0.4s, background-color 1.2s;").find('small').attr("style", "right: 0px; transition: background-color 0.4s, left 0.2s; background-color: rgb(255, 255, 255);");
	}
	else if (check.checked && !active)
	{
		$(check).prop("checked", false).next('span').attr("style", "box-shadow: rgb(223, 223, 223) 0px 0px 0px 0px inset; border-color: rgb(223, 223, 223); background-color: rgb(255, 255, 255); transition: border 0.4s, box-shadow 0.4s;").find('small').attr("style", "left: 0px; transition: background-color 0.4s, left 0.2s;");
	}
}

function createSection(title, breakPageBeforeInPDF, titleHide, autoclick)
{
	var genId = "dyn-" + unique_id();
	var tab = $('#htmlFragments .dynamicTab').clone();
	tab.find('> a').attr('href', '#' + genId).find("div.tab-label").text(title);
	tab.find('div.tab-numbered').hide();
	if ($('#documentSectionBrowser').hasClass('collapsed'))
	{
		tab.width($('#documentSectionBrowser li').width());
		tab.find('div.tab-numbered').text(($('#documentSectionBrowser li').length + 1) + ".").attr('title', title).show();
		tab.find('div.tab-label').hide();
	}
	tab.appendTo('#documentSectionBrowser.nav-tabs');


	var content = $('#htmlFragments .dynamicTabContent').clone();
	var hideTitleBool = titleHide == 'on' ? true : false;
	content.attr('id', genId);
	content.find(".sectionTitle").val(title);
	content.appendTo('.editorContent.tab-content');
	content.find('.js-switch').prop('checked', breakPageBeforeInPDF == 'on' ? true : false);
	content.find('.noTitle-switch').prop('checked', hideTitleBool);
	var switchery = new Switchery(content.find('.js-switch').get(0), {
		size : 'small'
	});


	var $titleHidden = content.find('.noTitle-switch').change(function()
	{
		if (!($(this).is(':checked')) && $('#globalTitleHidden').is(':checked'))
		{
			$('#globalTitleHidden').click();
		}
		if ($(this).is(':checked'))
		{
			$(this).siblings(".switchery").find("span").text('Hide');
			$(this).siblings(".switchery").find("span").attr('title', 'Your title will be hidden inside the PDF');
			$(this).siblings(".switchery").find("span").addClass('hideLabel');
		}
		else
		{
			$(this).siblings(".switchery").find("span").text('Show');
			$(this).siblings(".switchery").find("span").attr('title', 'Your title will be shown within the PDF');
			$(this).siblings(".switchery").find("span").removeClass('hideLabel');
		}
	}).change();

	var switcheryHide = new Switchery($titleHidden.get(0), {
		color : '#fc0d00',
		size : 'small'
	});

	if (hideTitleBool)
	{
		content.find('.noTitle-switch').siblings(".switchery").css("width", "60px").prepend("<span class='hideLabel' title='Your title will be hidden inside the PDF'>Hide</span>").find("small").css("left", "40px");
	}
	else
	{
		content.find('.noTitle-switch').siblings(".switchery").css("width", "60px").prepend("<span title='Your title will be shown within the PDF'>Show</span>").find("small").css("left", "0px");
	}

	tab.on('shown.bs.tab', onSectionTabActivated);

	if (autoclick)
	{
		$('[href="#' + genId + '"]').click();
	}
	logger.info("Created section with ID: " + genId);
	return genId;
}

function deleteCurrentSection()
{
	var $tab = $("#documentSectionBrowser li.active");
	var editorToDelete = $(">a", $tab).attr('href');
	if (!$(editorToDelete).find('.remove').hasClass('disabled'))
	{
		swal({
			title : "Warning!",
			text : "Are you sure you would like to remove the current section?",
			type : "warning",
			showCancelButton : true,
			cancelButtonText : "No",
			confirmButtonColor : "#BA131A",
			confirmButtonText : "Yes, delete it",
			closeOnConfirm : true
		}, function(confirm)
		{
			if (confirm)
			{

				var $switchTo = $tab.next();
				if ($switchTo.length == 0)
				{
					$switchTo = $tab.prev();
				}
				$("a", $switchTo).click();


				$('.editorContent ' + editorToDelete).remove();

				$tab.slideUp(function()
				{
					$(this).remove();
				});
			}
		});
	}
	else
	{
		swal({
			title : "Warning!",
			text : "You cannot delete this section as it contains locked content.",
			type : "warning",
			closeOnConfirm : true
		});
	}
}

function doDisablePricingSection()
{
	swal({
		title : "Warning!",
		text : "Disabling the pricing section, any information in this area will be hidden in the proposal.",
		type : "warning",
		showCancelButton : true,
		cancelButtonText : "No",
		confirmButtonColor : "#FF8700",
		confirmButtonText : "Yes, disable it",
		closeOnConfirm : true
	}, function(confirm)
	{
		if (confirm)
		{
			var $tab = $("#documentSectionBrowser li.active");
			logger.info("Disabling Pricing Section");
			$tab.addClass('notenabled');
			// $(".disabledSection", $tab).css('display', 'inline-block');
			// $(".enabledSection", $tab).css('display', 'none');
			var id = $("a", $tab).attr("href");
			$(".editorContent " + id).attr("data-enabled", false);
			$('.pricing-droppable:not(#htmlFragments .pricing-droppable)').droppable("disable");
		}
		else
		{
			changeSwitch("#pricing-tab-check", true);
		}
	});
}

function enablePricingSection()
{
	var $tab = $("#documentSectionBrowser li.active");
	logger.info("Enabling Pricing Section");
	$tab.removeClass('notenabled');
	// $(".disabledSection", $tab).css('display', 'none');
	// $(".enabledSection", $tab).css('display', 'inline-block');
	var id = $("a", $tab).attr("href");
	$(".editorContent " + id).attr("data-enabled", true);
	$('.pricing-droppable:not(#htmlFragments .pricing-droppable)').droppable("enable")
}

function reorderSectionToIndex(sectionId, index)
{
	logger.info("Reordering section with ID: " + sectionId + " to index " + index);
	var target = $('#documentSectionBrowser li')[index];
	$('a[href="#' + sectionId + '"]').closest("li").insertBefore(target);
}

function unique_id()
{
	return new Date().getTime();
}

$("#mnoOrganizationId").change(function()
{
	maestranoOrganizationValidation();
});

function maestranoOrganizationValidation()
{
	var sel = $("#mnoOrganizationId option:selected");
	var data = sel.data();
	var val = sel.val();
	if (val)
	{
		logger.debug("maestrano company select info: ", data);
		logger.debug("maestrano company id: ", val);
		// #companyName
		if (data.name)
		{
			$("#companyName").val(data.name);
			$("#companyName").prop("readonly", true);
		}
		// #contactEmail
		if (data.email)
		{
			$("#contactEmail").val(data.email);
			$("#contactEmail").prop("readonly", true);
		}
		// #cc-emails
		$("#cc-emails").addTag(data.email2);
	}
	else
	{
		// #companyName
		$("#companyName").val("");
		$("#companyName").prop("readonly", false);
		// #contactEmail
		$("#contactEmail").val("");
		$("#contactEmail").prop("readonly", false);
		// #cc-emails
		$("#cc-emails").importTags('');
	}
}

function alignImage(pos, imgDiv, txtDiv, size)
{
	var float;
	var finalSize;
	imgDiv.find('.bar').removeClass('bar-left');
	if (pos.indexOf('intext') >= 0)
	{
		if (pos.indexOf('left') >= 0)
		{
			float = 'left';
			imgDiv.find('.bar').addClass('bar-left');
		}
		else if (pos.indexOf('right') >= 0)
		{
			float = 'right';
		}
		$('#image', imgDiv).css('width', '100%').css('height', 'auto');
		if (size == null)
		{
			finalSize = '407';
		}
		else
		{
			var step1 = size.replace('%', '');
			finalSize = (step1 / 100) * 407;
		}
		imgDiv.css('width', finalSize).css('height', 'auto').css('float', float);
		// .css('z-index', 5)
	}
	else
	{
		if (size == null)
		{
			finalSize = '100%';
		}
		else
		{
			var step1 = size.replace('%', '');
			finalSize = step1 + "%";
		}
		imgDiv.css('width', '100%').css('height', 'auto').css('float', 'none');
		if (pos.indexOf('wide') >= 0)
		{
			$('#image', imgDiv).css('width', finalSize).css('height', 'auto');
			if ($('#JcropPhotoFrame', imgDiv).attr('src') != '')
			{
				$('#JcropPhotoFrame', imgDiv).css('width', '100%');
			}
		}
		else
		{
			var width = '';
			if (pos.indexOf('left') >= 0)
			{
				imgDiv.css('text-align', 'left');
			}
			else if (pos.indexOf('right') >= 0)
			{
				imgDiv.css('text-align', 'right');
			}
			else if (pos.indexOf('center') >= 0)
			{
				imgDiv.css('text-align', 'center');
			}
			$('#image', imgDiv).css('width', finalSize);
			if ($('#JcropPhotoFrame', imgDiv).attr('src') != '')
			{
				$('#JcropPhotoFrame', imgDiv).css('width', width);
			}
		}
	}

	pos.indexOf('top') >= 0 ? imgDiv.insertBefore(txtDiv) : function()
	{
		imgDiv.insertAfter(txtDiv);
	}();
}

function alignVideo(pos, imgDiv, txtDiv, size)
{
	var float;

	if (pos != null)
	{
		if (pos.indexOf('intext') >= 0)
		{
			if (pos.indexOf('left') >= 0)
			{
				float = 'left';
				imgDiv.attr('style', 'width:100%;height:100%');
				// $('iframe.videoId', imgDiv).attr('style',
				// 'width:100%;height:100%');
			}
			else if (pos.indexOf('right') >= 0)
			{
				float = 'right';
				imgDiv.attr('style', 'width:100%;height:100%');
				// $('iframe.videoId', imgDiv).attr('style',
				// 'width:100%;height:100%');
			}
			$('#video', imgDiv).css('width', '100%');
			$('iframe.videoId', imgDiv).css('float', float).css('z-index', 1);

			if (((pos.indexOf('left') >= 0) || (pos.indexOf('right') >= 0)) && (size == null))
			{
				$('iframe.videoId', imgDiv).css('height', '240');
			}
			else
			{
				if (!(size.indexOf('small') >= 0) || !(size.indexOf('medium') >= 0) || !(size.indexOf('wide') >= 0))
				{
					$('iframe.videoId', imgDiv).css('height', '240');
				}
			}

		}
		else
		{
			imgDiv.css('width', '100%').css('float', 'none');
			if (pos.indexOf('wide') >= 0)
			{
				$('#video', imgDiv).css('width', '100%');
				if ($('#JcropPhotoFrame', imgDiv).attr('src') != '')
				{
					$('#JcropPhotoFrame', imgDiv).css('width', '100%');
				}
			}
			else
			{
				var width = '';
				if (pos.indexOf('left') >= 0)
				{
					imgDiv.css('text-align', 'left');
				}
				else if (pos.indexOf('right') >= 0)
				{
					imgDiv.css('text-align', 'right');
				}
				else if (pos.indexOf('center') >= 0)
				{
					imgDiv.css('text-align', 'center');
				}
				$('video', imgDiv).css('width', width);
				if ($('#JcropPhotoFrame', imgDiv).attr('src') != '')
				{
					$('#JcropPhotoFrame', imgDiv).css('width', width);
					if (size != null)
					{
						if (!(size.indexOf('wide') >= 0))
						{
							$('iframe.videoId', imgDiv).removeAttr('style');
						}
					}
				}
			}
		}
		pos.indexOf('top') >= 0 ? imgDiv.insertBefore(txtDiv) : function()
		{
			imgDiv.insertAfter(txtDiv);
		}();
	}

	// Widths for size of video
	if (size != null)
	{
		if (size.indexOf('small') >= 0)
		{
			$('iframe.videoId', imgDiv).css('width', '342px');
			$('iframe.videoId', imgDiv).css('height', '230px');
			imgDiv.css('width', '100%');
			imgDiv.css('height', '230px');
		}
		else if (size.indexOf('medium') >= 0)
		{
			$('iframe.videoId', imgDiv).css('width', '457px');
			$('iframe.videoId', imgDiv).css('height', '270px');
			imgDiv.css('width', '100%');
			imgDiv.css('height', '270px');
		}
		else if (size.indexOf('wide') >= 0)
		{
			$('iframe.videoId', imgDiv).css('width', '80%');
			$('iframe.videoId', imgDiv).css('height', '440px');
			imgDiv.css('width', '100%');
			imgDiv.css('height', '440px');
			if ((pos.indexOf('left') >= 0) || (pos.indexOf('right') >= 0))
			{
				imgDiv.css('width', '100%').css('float', 'none');
				imgDiv.css('text-align', 'center');
				if (pos != null)
				{
					if (pos.indexOf('top') >= 0)
					{
						imgDiv.insertAfter(txtDiv);
					}
					else
					{
						imgDiv.insertBefore(txtDiv);
					}
				}
				else
				{
					imgDiv.insertBefore(txtDiv);
				}
			}
		}
		// if (((pos.indexOf('left')>=0)||(pos.indexOf('right')>=0)) &&
		// !(size.indexOf('wide')>=0)) {
		// imgDiv.css('width', $('iframe.videoId', imgDiv).width())
		// }
	}
}


// Execute a validation and return an object with information about the
// validation The object has the following properties:
// - ok (true/false) : define if the validation
// - messase (string) : message to be prompted to the user when the validation
// fails
// - action (function) : function to be executed when the user close the prompt
function checkSaveRequirements()
{
	var result = {
		ok : true
	};

	// check mandatory proposal custom fields
	logger.info("checking proposal custom fields requirements");
	var hasEmptyRequiredProposalField = false;
	var firstEmptyField = null;
	$(".custom-fields .custom-field").each(function(index, field)
	{
		var key = $(field).attr("id");
		var fieldInfo = customFields[key];
		if (fieldInfo.required && !$(field).val())
		{
			$(field).addClass("error");
			hasEmptyRequiredProposalField = true;
			if (firstEmptyField == null)
				firstEmptyField = $(field);
			logger.error("Empty required custom field found: ", fieldInfo);
		}
		else
		{
			$(field).removeClass("error");
		}
	});
	if (hasEmptyRequiredProposalField)
	{
		result.ok = false;
		result.message = "The proposal has some empty required custom fields";
		result.action = function()
		{
			$('#documentSectionBrowser [librarytype="cover"]').click();
			setTimeout(function()
			{
				$('#editorContent').animate({
					scrollTop : firstEmptyField.offset().top
				}, 500);
			}, 500);
		}
		return result;
	}

	// check mandatory products custom fields
	logger.info("checking products custom fields requirements");
	var hasEmptyRequiredProductField = false;
	firstEmptyField = null;
	$(".pricingTable .custom-field").each(function(index, field)
	{
		var key = $(field).attr("data-key");
		var fieldInfo = productsCustomFields[key];
		if (fieldInfo.required && !$(field).val())
		{
			$(field).addClass("error");
			hasEmptyRequiredProductField = true;
			if (firstEmptyField == null)
				firstEmptyField = $(field);
			logger.error("Empty required product custom field found: ", fieldInfo);
		}
		else
		{
			$(field).removeClass("error");
		}
	});
	if (hasEmptyRequiredProductField)
	{
		result.ok = false;
		result.message = "The proposal has some empty required custom fields";
		result.action = function()
		{
			$('#documentSectionBrowser [librarytype="pricing"]').click();
			setTimeout(function()
			{
				$('#editorContent').animate({
					scrollTop : firstEmptyField.offset().top
				}, 500);
			}, 500);
		}
		return result;
	}

	return result;
}
