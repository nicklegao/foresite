
$(document).ready(function() {

	$("#settingsContentArea").load("/application/include/settingsStyles.jsp", function(responseTxt, statusTxt, xhr){
		if(statusTxt == "error")
			alert("Error: " + xhr.status + ": " + xhr.statusText);
	});

	$("a.settingsMenu").click(function(){

		var url = $(this).data("url");
		var element = $(this);
		$("#settingsContentArea").html('');
		$("#settingsContentArea").load("/application/include/"+url, function(responseTxt, statusTxt, xhr){
			if(statusTxt == "success") {
				$(".settingsMenu").removeClass('active');
				$(element).addClass('active');
			}


			if(statusTxt == "error")
				alert("Error: " + xhr.status + ": " + xhr.statusText);
		});
	});
});

