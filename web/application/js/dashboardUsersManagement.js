$(document).ready(function()
{

	$('#users-management-modal #addItem').click(addItemAction);
	$('#users-management-modal #exportUsers').click(function() {

		var id = $(this).closest(".btn-group").attr("data-id");
		exportUsersAction(id);
	});


	$('#users-management-modal .export-item').click(function() {

		var id = $(this).closest(".btn-group").attr("data-id");
		exportUsersAction(id);
	});

	$('#users-management-modal #resend-activation-item').click(function(){
		var id = $(this).closest(".btn-group").attr("data-id");
        resendActivationEmail(id)
	})

	var $modal = $("#users-management-modal");

	var domHead = "<'navbar' <'navbar-form navbar-left'<'form-group' l>> <'navbar-form navbar-right'<'form-group' f>> >";
	var domBody = "t";
	var domFooter = "<'navbar' <'navbar-form navbar-left' i> <'navbar-form navbar-right'<'form-group' p>> <'navbar-form navbar-right' <'#actionsHolder'>> >";

	$("#table-users", $modal).DataTable({
		"oLanguage" : {
			"sLengthMenu" : "_MENU_",
			"sSearch" : "",
			"oPaginate" : {
				"sPrevious" : "",
				"sNext" : ""
			},
		},
		"columns" : [ null, null, null, {
			"orderable" : false
		} ],
		"sDom" : domHead + domBody + domFooter,
		"fnDrawCallback" : function(oSsettings)
		{

			$('#users-management-modal .edit-item').click(editItemAction);
			$('#users-management-modal .delete-item').click(deleteItemAction);
			$('[data-toggle="dropdown"]').dropdown();
		}
	});

	$("#table-teams", $modal).DataTable({
		"oLanguage" : {
			"sLengthMenu" : "_MENU_",
			"sSearch" : "",
			"oPaginate" : {
				"sPrevious" : "",
				"sNext" : ""
			},
		},
		"columns" : [ null, {
			"orderable" : false
		} ],
		"sDom" : domHead + domBody + domFooter,
		"fnDrawCallback" : function(oSettings)
		{

			$('#users-management-modal .edit-item').click(editItemAction);
			$('#users-management-modal .delete-item').click(deleteItemAction);
			$('[data-toggle="dropdown"]').dropdown();
		}
	});

	$("#table-roles", $modal).DataTable({
		"oLanguage" : {
			"sLengthMenu" : "_MENU_",
			"sSearch" : "",
			"oPaginate" : {
				"sPrevious" : "",
				"sNext" : ""
			},
		},
		"columns" : [ null, {
			"orderable" : false
		} ],
		"sDom" : domHead + domBody + domFooter,
		"fnDrawCallback" : function(oSettings)
		{

			$('#users-management-modal .edit-item').click(editItemAction);
			$('#users-management-modal .delete-item').click(deleteItemAction);
			$('[data-toggle="dropdown"]').dropdown();
		}
	});

	$('.nav-tabs li a').on('click', function()
	{
		var item = $(this).data('module');
		$(this).closest('.modal').find('.modal-title').text(item + 's');
		$(this).closest('.modal').find('.addItemText').text('Add ' + item)
		if (item === "User") {
			$('#exportUsers').show();
		} else {
			$('#exportUsers').hide();
		}
	});

	$('#users-management-modal .dataTables_filter').prepend('<label class="search-label"><span class="search-icon fa fa-search"></span></label>');
	$('#users-management-modal .dataTables_filter input').addClass("search-box form-control").attr("placeholder", "Search...");

	$('#users-management-modal .dataTables_length select').addClass("form-control");
	$('#users-management-modal .dataTables_length').closest(".form-group").before($("<span>").text('Show')).after($("<span>").text('proposals '));

});

function exportUsersAction(userButton)
{
		startLoadingForeground();
		$.fileDownload('/api/downloadUser', {
			data : {
				userSelected : userButton
			},
			successCallback : function(url)
			{
				stopLoadingForeground();
			},
			failCallback : function(html, url)
			{

				stopLoadingForeground();
			}
		});
}

function resendActivationEmail(userButton)
{
    startLoadingForeground();
    $.ajax({
		url: '/api/resendActivation',
        data : {
            userSelected : userButton
        },
        success : function(data)
        {
            stopLoadingForeground();
            if (data.success)
            {
                swal({
                    title : 'Success',
                    type : 'success',
                    text : data.message,
                });
            }
            else
            {
                swal({
                    title : 'Error',
                    type : 'error',
                    text : data.message,
                });
            }
        },
        error : function()
        {
            swal({
				title: "Error",
				type: "error",
				text: "Could not send an activation email. Please try again later."
            });
        }
    });
}

function addItemAction()
{
	var $currentItem = $("#users-management-modal").find("table:visible");
	var module = $currentItem.attr('data-module');
	if (module == 'User')
	{
		$.ajax({
			url : '/api/reachMaxUserLimit',
			success : function(data)
			{
				if (!data.success)
				{
					addItemAction1();
				}
				else
				{
					swal({
						title : 'Do you want to continue?',
						text : data.message,
						type : 'warning',
						showCancelButton : true,
					}, function(isConfirm)
					{
						if (isConfirm)
						{
							addItemAction1();
						}
					})
				}
			},
			error : function()
			{
				swal({
					title: "Error",
					type: "error",
					text: "Unable to add user now. Please try again later."
                });
			}
		})
	}
	else
	{
		addItemAction1();
	}
}

function addItemAction1()
{
	var $currentItem = $("#users-management-modal").find("table:visible");
	var params = {};
	var module = $currentItem.attr('data-module');
	var action = "add";

	params.action = action;

	$('body').modalmanager('loading');

	var $modal = $('#edit-management-modal');
	$modal.load('/application/dialog/edit' + module + '.jsp', params, function()
	{
		$modal.modal({
			width : module == 'Role' ? '710px' : '760px',
			backdrop : 'static',
			keyboard : false
		});
	});
}

function editItemAction()
{
	
	var params = {};
	var id = $(this).closest(".btn-group").attr("data-id");
	var module = $(this).closest("table").attr('data-module');
	var action = "edit";

	params.id = id;
	params.action = action;

	$('body').modalmanager('loading');

	var $modal = $('#edit-management-modal');
	$modal.load('/application/dialog/edit' + module + '.jsp', params, function()
	{
		console.log("editing " + module);
		$modal.modal({
			width : module == 'Role' ? '772px' : '760px',
			backdrop : 'static',
			keyboard : false
		});
	});
}

function deleteItemAction()
{
	var params = {};
	var id = $(this).closest(".btn-group").attr("data-id");
	var module = $(this).closest("table").attr('data-module');
	var action = "delete";

	params.id = id;

	swal({
		title : 'Are you sure?',
		text : 'Do you want to delete this ' + module,
		type : 'warning',
		showCancelButton : true,
	}, function(isConfirm)
	{
		if (isConfirm)
		{
			$.ajax({
				url : '/api/delete' + module,
				data : {
					id : id
				},
				success : function(data)
				{
					stopLoadingForeground();
					if (data.success === false)
					{
						if (data.action === 'migrate')
						{
							migrateItemsAction(module, id)
						}
						else
						{
							swal({
								title : 'Error',
								type : 'error',
								text : data.message
							});
						}
					}
					else
					{
						swal({
							title : 'Deleted!',
							text : 'The ' + module + ' has been deleted.',
							type : 'success'
						});
						$('#users-management-modal').load('/application/dialog/managementUsers.jsp?tabModule=' + module);
					}
				},
				error : function()
				{
					stopLoadingForeground();

					alert("Unable to delete your " + module + ". Please try again later.");
				}
			})
		}
	})
}


function migrateItemsAction(module, id)
{
	var params = {};

	params.id = id;

	$('body').modalmanager('loading');

	var $modal = $('#migrations-modal');
	$modal.load('/application/dialog/migrations' + module + '.jsp', params, function()
	{
		$modal.modal({
			width : '660px',
			backdrop : 'static',
			keyboard : false
		});
	});
}
