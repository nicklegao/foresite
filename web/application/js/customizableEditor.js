$(document).ready(function()
{
	colorPicker();

	$(document).on("click", ".customFieldsAction", customFieldsAction);
	$(".collapse-card").paperCollapse();


	/* color selector for table */
	function colorPicker()
	{
		$('.color').spectrum({
			preferredFormat : "hex",
			replacerClassName : 'round-replacer',
			showInput : true,
			hide : function(tinycolor)
			{
				{
					color = tinycolor.toHexString();
				}
			}
		});

	}

	// $('.form-control[name="borderStyleColor"]').spectrum({
	// preferredFormat : "hex",
	// replacerClassName : 'round-replacer-small margin',
	// showInput : true,
	// hide : function(tinycolor)
	// {
	// $('.tableStyle .editorArea').css('color', tinycolor.toHexString());
	// }
	// });

	$("#palette-1").change(function()
	{
		$(".table th").css("background-color", $("#palette-1").val());
	});
	$("#palette-2").change(function()
	{
		$(".table tr:even").css("background-color", $("#palette-2").val());
	});
	$("#palette-3").change(function()
	{
		$(".table tr:odd").css("background-color", $("#palette-3").val());
	});
	$("#palette-4").change(function()
	{
		$("#dragableTbl").css("background-color", $("#palette-4").val());
	});

	$('.form-control[name="borderStyleType"]').on('change', function(e)
	{
		setupTableStyle();
	});

	$('.form-control[name="borderStyleSpacing"]').on('change', function(e)
	{
		setupTableStyle();
	});

	$('.form-control[name="borderStyleColor"]').on('change', function(e)
	{
		setupTableStyle();
	});

	$('#notesStyleType, #notesStyleSize, #notesStyleColor').on('change', function(e)
	{
		setupNotesStyle();
	});

	$('#iconsStyleSize').on('change', function(e)
	{
		setupIconsStyle();
	});

	$('#subHeadingStyleType, #subHeadingStyleSize, #subHeadingStyleWeight').on('change', function(e)
	{
		setupSubHeadingStyle();
	});

	setupSubHeadingStyle();

	setupNotesStyle();

	setupTableStyle();

	/*
	 * For selecting each columns /*$('.sp-choose').bind( "click", function() {
	 * colorPicker(); $('body').on('click', '#dragableTbl th', function ( event ) { //
	 * Get index of parent TD among its siblings (add one for nth-child) var ndx =
	 * $(this).index() + 1; // Find all TD elements with the same index $('table >
	 * tbody >tr > td:nth-child(' + ndx + ')').css("background-color",color);
	 * 
	 * });
	 */


	/*
	 * function displayDiskUsageAction() { alert("dasd") logger.info("Displaying
	 * disk usage popup");
	 * 
	 * var $modal = $('#disk-usage-modal'); $('body').modalmanager('loading');
	 * 
	 * $modal.load('/application/dialog/diskUsage.jsp', {}, function() {
	 * $modal.modal({ backdrop : 'static', keyboard : false });
	 * setupModalsBackgroundColor(); });
	 * 
	 * return $modal; }
	 */

	// saving the styles of the pricing table
	$(".saveProposalStyles").click(doSaveProposal);

	function doSaveProposal(event, callback)
	{
		/* logger.info("Saving proposal styles"); */
		var proposal = saveProposalStyles();
	}

	function getPricingStyles()
	{
		$.ajax('/api/styles/get', {
			type : 'POST'
		}).done(function(data)
		{

			alert("data");

		});
	}


	function saveProposalStyles()
	{
		var table = buildTableJson();
		$.ajax('/api/styles/save', {
			type : 'POST',
			data : {
				proposalStyle : JSON.stringify(table)
			}
		}).done(function(data)
		{
			if (data)
			{
				if ($('#static-want-more-detail').data('froala.editor'))
				{
					saveBottomText();
				}
				else
				{
					swal("Success", "Successfully saved", "success");
				}
			}
			else
			{
				swal("Opps, there was an issue...", "We werent able to save your pricing table but were working on fixing it.", "error");
			}
		});
	}


});

function saveBottomText()
{
	$.ajax('/api/updateProposalBottomText', {
		type : 'POST',
		data : {
			// pricing-title
			'wantMoreDetail' : $('#static-want-more-detail').froalaEditor('html.get').replace(/\<br>/g, '<br/>')
		}
	}).done(function(data)
	{
		if (data)
		{
			saveTableStyles();
		}
		else
		{
			swal("Opps, there was an issue...", "We werent able to save your pricing table but were working on fixing it.", "error");
		}
	});
}

function saveTableStyles()
{
	$.ajax('/api/updatePricingTableStyles', {
		type : 'POST',
		data : {
			'pricingTitle' : $('#pricing-title').val(),
			'borderStyleType' : $('#borderStyleType').val(),
			'borderStyleSpacing' : $('#borderStyleSpacing').val(),
			'borderStyleColor' : $('#borderStyleColor').val()
		}
	}).done(function(data)
	{
		if (data)
		{
			swal("Successfully Saved", "Please note changes saved do not affect existing proposals until the proposal is saved.", "success");
		}
		else
		{
			swal("Opps, there was an issue...", "We werent able to save your pricing table but were working on fixing it.", "error");
		}
	});
}

function setupTableStyle()
{
	var size = $('#borderStyleSpacing').val();
	var style = $('.form-control[name="borderStyleType"]').val();
	var color = $('.form-control[name="borderStyleColor"]').val();

	if (style == null)
	{
		style = "solid";
	}
	if (size == null || size < 1)
	{
		size = "1";
	}
	else if (color == null || color.indexOf("#") == -1)
	{
		color = "#000";
	}

	$('th').each(function()
	{
		// $(this).css("border-right", size + "px " + style + " " + color);
		// $(this).css("border-left", size + "px " + style + " " + color);
		$(this).css("border-bottom", size + "px " + style + " " + color);
		$(this).css("border-top", 'none');
	});

	$('td').each(function()
	{
		$('table').each(function()
		{
			if (style == "dashed")
			{
				$(this).css("border-spacing", '2px');
				$(this).css("border-collapse", 'separate');
			}
			else if (style == "dotted")
			{
				$(this).css("border-spacing", '1px');
				$(this).css("border-collapse", 'separate');
			}
			else
			{
				$(this).css("border-spacing", '0px');
				$(this).css("border-collapse", 'inherit');
			}
		});
		$(this).css("border-top", 'none');
		$(this).css("border-bottom", size + "px " + style + " " + color);
	});

}

function setupNotesStyle()
{
	var size = $('#notesStyleSize').val();
	var style = $('#notesStyleType').val();
	var color = $('#notesStyleColor').val();

	if (style == null)
	{
		style = "solid";
	}
	if (size == null || size < 1)
	{
		size = "10";
	}
	if (color == 'theme')
	{
		color = '#5676f1';
	}
	if (color == null || color.indexOf("#") == -1)
	{
		color = "#ebebeb";
	}

	$('.notes').each(function()
	{
		// $(this).css("border-right", size + "px " + style + " " + color);
		// $(this).css("border-left", size + "px " + style + " " + color);
		$(this).css("border-left", size + "px " + style + " " + color);
	});
}

function setupIconsStyle()
{
	var size = $('#iconsStyleSize').val();
	if (size == null || size < 1)
	{
		size = "25";
	}
	$('#dragableTbl').find('thead th').children('i').each(function()
	{
		$(this).css("font-size", size + "px");
	})
}

function setupSubHeadingStyle()
{
	var size = $('#subHeadingStyleSize').val();
	var style = $('#subHeadingStyleType').val();
	var weight = $('#subHeadingStyleWeight').val();

	if (style == null)
	{
		style = "normal";
	}
	if (size == null || size < 1)
	{
		size = "14";
	}
	if (weight == null)
	{
		weight = "bold";
	}

	$('.sectionRow > td').each(function()
	{
		// $(this).css("border-right", size + "px " + style + " " + color);
		// $(this).css("border-left", size + "px " + style + " " + color);
		$(this).css("font-size", size + "px ");
		$(this).css("font-style", style);
		$(this).css("font-weight", weight);
	});
}

function buildTableJson()
{
	var columns = buildTableColumnJson();
	var rows = buildTableRowJson();
	var footer = buildTableFooterJson();
	var styling = buildTableStylingJson();
	// var dat = JSON.stringify();
	var table = {};
	table.version = $('#dragableTbl').data('version');
	table.columns = columns;
	table.rows = rows;
	table.footer = footer;
	table.userId = userId;
	table.styling = styling;
	table.footerContent = $('#footerContent').html();
	return table;
}

function buildTableColumnJson()
{
	var $table = $("#dragableTbl");
	var jsonList = [];

	$table.find('thead tr th').each(function(position)
	{
		var json = {};
		var classList = $(this).find('.picker-target').get(0).className.split(/\s+/);
		var className = "";
		for (var i = 0; i < classList.length; i++)
		{
			if ((classList[i].includes('fa-') && classList[i] != "fa-3x") || classList[i] == "moneytaryValue")
			{
				className = classList[i];
			}
		}
		// className = $(this).find('.picker-target').attr('data-pricingicon');
		json.icon = className;
		json.label = $(this).children('.input').val();
		json.customfieldType = $(this).data('customfieldtype');
		json.type = $(this).data('type');
		json.displayOrder = position;
		json.alignment = $(this).attr('data-alignment');
		jsonList.push(json);
	});
	return jsonList;
}

function buildTableRowJson()
{
	var $table = $("#dragableTbl");
	var json = {};
	var customfields = [];

	$table.find('tbody tr:first [data-customfieldvalue]').each(function(position)
	{
		customfields.push($(this).data('customfieldvalue'));
	});
	json.customFields = customfields;

	json.headings = buildSubHeadingsStyling();
	json.notes = buildNotesStyling();

	var labels = {};
	$table.find('[data-mapping]').each(function()
	{
		var key = $(this).data('mapping');
		var value = $(this).val();

		labels[key] = value;
	});
	json.labels = labels;
	return json;
}

function buildIconStyling()
{
	var icon = {};
	var size = $('#iconsStyleSize').val();

	icon.size = size;
	return icon;
}

function buildSubHeadingsStyling()
{
	var headings = {};
	var size = $('#subHeadingStyleSize').val();
	var style = $('#subHeadingStyleType').val();
	var weight = $('#subHeadingStyleWeight').val();
	headings.size = size;
	headings.style = style;
	headings.weight = weight;
	return headings;
}

function buildNotesStyling()
{
	var note = {};
	var size = $('#notesStyleSize').val();
	var style = $('#notesStyleType').val();
	var color = $('#notesStyleColor').val();
	var fontStyle = $('#notesFontStyleType').val();
	note.size = size;
	note.style = style;
	note.color = color;
	note.fontStyle = fontStyle;
	return note;
}

function buildTableFooterJson()
{
	var $table = $("#subTotalTbl");
	var json = {};
	var labels = {};

	$table.find('[data-mapping]').each(function()
	{
		var key = $(this).data('mapping');
		var value = $(this).val();

		labels[key] = value;
	});
	json.labels = labels;
	return json;
}

function buildTableStylingJson()
{
	var styling = {};
	styling.icons = buildIconStyling();
	return styling;
}

function customFieldsAction(e)
{
	var $modal = $('#custom-fields-modal');

	$modal.one('show.bs.modal', function()
	{
		$modal.find('input, textarea').placeholder();
		$('.nav-tabs a[href="#products"]').tab('show');
	});

	$modal.load('/application/dialog/customFields.jsp', {}, function()
	{
		$modal.modal({
			width : '660px',
			backdrop : 'static',
			keyboard : false
		});
	});

	return $modal;
}


// Replacement for dashboard reload table
function redrawTable()
{
	getCustomFields().done(function(customFields, textStatus, jqXHR)
	{
		var panel = $('#collapsableCustomFields');
		panel.find('.panel-body').empty();
		var amount = 0;
		$.each(customFields, function(key, value)
		{
			if (value.on)
			{
				var html = "<div class=\"customField ui-draggable\" sortable=\"" + key + "\" style=\"position: relative;\"> <span class=\"sort\">" + value.label + "</span> <span style=\"display:none\" class=\"type\">" + value.type + "</span><i class=\"fa fa-bars\" style=\"position: absolute;right: 12px;top: 12px;\"></i> </div>";
				panel.find('.panel-body').append(html);
				amount++;
			}
		});
		if (amount == 0)
		{
			panel.find('.panel-body').append("<p>You do not have any custom fields enabled. You can edit custom fields by clicking on the manage button.</p>")
		}
		$(".customField").draggable({
			revert : 'invalid',
			drag : function()
			{
				$('.product-item').addClass('droppable-highlight');
				$('#dragableTbl thead').addClass('droppable-highlight');
			},
			stop : function()
			{
				$('.product-item').removeClass('droppable-highlight');
				$('#dragableTbl thead').removeClass('droppable-highlight');
			},
		});
	});
}

function getCustomFields()
{
	return $.ajax({
		url : "/api/loadCustomFields?type=products",
		dataType : "json"
	});
}
