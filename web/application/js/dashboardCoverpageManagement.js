var extra;
var selectedUploadCover = null;
var jcrop_api;
var uploadImage;
var jcModal;
$(document).ready(function()
{
	console.log('test');
	showUploading(false);

	refreshCovers();

	$("#coverpage-management").on('click', ".choseFile", function()
	{
		$("#coverUploadButton").val("");
		$("#coverUploadButton").click();
	});

	$("#coverUploadButton").on("change", function(evt)
	{
		var file = evt.target.files;

		if (FileReader && file && file[0])
		{
			var reader = new FileReader();
			reader.onload = function(e)
			{
				$("#jCropModal #JcropPhotoFrame").attr('src', e.target.result).width("auto").height('auto');
				selectedUploadCover = file[0];

				loadJcropModal();
			};
			showUploading(true);
			reader.readAsDataURL(file[0]);
		}
		else
		{
			alert("This browser do not support file upload.");
		}
	});

	$("#jCropModal").on('click', '.uploadFile', function()
	{
		$("#jCropModal .btn").addClass('disabled');
		showUploading(true);
		uploadFileAction(selectedUploadCover, function(ok)
		{
			if (ok)
			{
				logger.info("Image uploaded");
				$("#jCropModal").modal('hide');
			}
			else
			{
				logger.error("Error uploading image");
			}
			refreshCovers();
			$("#jCropModal .btn").removeClass('disabled');
			showUploading(false);
		});
	})


	$("#coverpage-management").on("click", ".coverpageDeleteAction", coverpageDeleteAction);

	$(document).on("click", ".coverpageDeleteConfirmed", coverpageDeleteConfirmed);

	new Switchery($('#landscapeMode').get(0), {
		size : 'small'
	});

	$("#landscapeMode").change(function()
	{
		console.log('change orientation');
		var landscape = $('#landscapeMode').is(':checked');
		var minWidth = landscape ? 1122 : 793;
		var minHeight = landscape ? 793 : 1122;


		if (uploadImage.naturalWidth >= minWidth && uploadImage.naturalHeight >= minHeight)
		{
			var minSize;
			var aspectRation;
			var select;
			if (landscape)
			{
				minSize = [ 1122, 793 ];
				aspectRatio = 1122 / 793;
				select = [ 0, 0, 1122, 793 ];
			}
			else
			{
				minSize = [ 793, 1122 ];
				aspectRatio = 793 / 1122;
				select = [ 0, 0, 793, 1122 ];
			}
			if (jcrop_api)
			{
				jcrop_api.setOptions({
					minSize : minSize,
					aspectRatio : aspectRatio
				});
				jcrop_api.setSelect(select);
			}
			else
			{
				$("#JcropPhotoFrame", jcModal).Jcrop({
					bgOpacity : 0.4,
					trueSize : [ uploadImage.naturalWidth, uploadImage.naturalHeight ],
					minSize : minSize,
					aspectRatio : aspectRatio,
					onSelect : function(c)
					{
						extra = c;
					}
				}, function()
				{
					jcrop = this;
					jcrop_api = this;
					$(".uploadFile", jcModal).removeClass("disabled");
					showUploading(false);
				});
				jcrop.setSelect(select);
				jcrop.setOptions({
					allowSelect : false
				});
			}

			showError(false);
			$(".uploadFile", jcModal).removeClass("disabled");
		}
		else
		{
			var errMsg = 'The image width must be bigger than ' + minWidth + 'px and the height bigger than ' + minHeight + 'px';

			if (jcrop_api)
			{
				jcrop_api.destroy();
			}

			jcrop_api = undefined;
			updateErrorMsg(errMsg);
			$(".uploadFile", jcModal).addClass("disabled");
		}
	});
});

function loadJcropModal()
{
	var $jcropModal = $("#jCropModal");
	jcModal = $jcropModal;
	showError(false);
	var jcrop;

	$jcropModal.on('shown.bs.modal', function()
	{
		var image = new Image();
		image.src = $("#JcropPhotoFrame", $jcropModal).attr('src');
		image.onload = function()
		{
			if (image.naturalWidth >= 793 && image.naturalHeight >= 1122)
			{
				$("#JcropPhotoFrame", $jcropModal).Jcrop({
					bgOpacity : 0.4,
					trueSize : [ image.naturalWidth, image.naturalHeight ],
					minSize : [ 793, 1122 ],
					aspectRatio : 793 / 1122,
					onSelect : function(c)
					{
						extra = c;
					}
				}, function()
				{
					jcrop = this;
					jcrop_api = this;
					$(".uploadFile", $jcropModal).removeClass("disabled");
					showUploading(false);
				});
				jcrop.setSelect([ 0, 0, 793, 1122 ]);
				jcrop.setOptions({
					allowSelect : false
				});
			}
			else
			{
				showError(true);
				$(".uploadFile", $jcropModal).addClass("disabled");
				showUploading(false);
			}

		}
		uploadImage = image;
	}).on('hide.bs.modal', function()
	{
		if ($('#landscapeMode').is(':checked'))
		{
			$('#landscapeMode').siblings('.switchery').trigger('click');
		}
		if (jcrop)
		{
			jcrop.destroy();
		}
		if (jcrop_api)
		{
			jcrop_api.destroy();
		}
		jcrop_api = undefined;
	});

	$jcropModal.modal({
		width : '675px',
		backdrop : 'static',
		keyboard : false
	});
}

function refreshCovers()
{
	showUploading(true);
	$.ajax({
		url : '/api/loadCoverPages'
	}).done(function(data, textStatus, jqXHR)
	{
		$("#coverpage-management #coverPicker").empty();
		if (data.pages.length > 0)
		{
			$.each(data.pages, function(index, page)
			{
				var wrapper = $('<div class="image-wrapper">');
				wrapper.attr('data-id', page.id);
				var overlay = $('<div class="overlay">');
				overlay.append($('<a class="coverpageDeleteAction" href="#coverpageDeleteModal" data-placement="top" data-toggle="modal">').append($('<i class="fal fa-times-circle">')).attr('data-id', page.id));
				var img = $('<img>').attr('src', page.thumbnail);
				wrapper.append(overlay);
				wrapper.append(img);
				$("#coverpage-management #coverPicker").append(wrapper);
			});
		}
		else
		{
			$("#coverpage-management #coverPicker").append($("<p>").text("No cover pages uploaded."));
		}
		showUploading(false);
	});
}

function showUploading(show)
{
	if (show)
	{
		$(".uploadLoading").show();
	}
	else
	{
		$(".uploadLoading").hide();
	}
}

function showError(show)
{
	if (show)
	{
		$("#jCropModal .uploadError").show();
	}
	else
	{
		$("#jCropModal .uploadError").hide();
	}
}

function updateErrorMsg(msg)
{
	$("#jCropModal .uploadError").show();
	$("#jCropModal .uploadError").text(msg);
}

function uploadFileAction(file, callback)
{
	var formData = new FormData();
	formData.append("cover", file);
	$.each(extra, function(key, value)
	{
		formData.append(key, Math.ceil(value));
	});
	$.ajax({
		type : 'POST',
		url : '/api/uploadCoverPage',
		data : formData,
		cache : false,
		contentType : false,
		processData : false
	}).done(function(data)
	{
		callback(data);
	});
}

function coverpageDeleteAction(event)
{
	var coverpageId = $(this).attr("data-id");
	$("#coverpageDeleteModal .coverpageDeleteConfirmed").attr("data-id", coverpageId);
}

function coverpageDeleteConfirmed(event)
{
	var coverpageId = $("#coverpageDeleteModal .coverpageDeleteConfirmed").attr("data-id");
	$("#coverpageDeleteModal").modal("hide");
	startLoadingForeground();
	$.ajax({
		url : '/api/deleteCoverPage',
		data : {
			coverpageId : coverpageId
		}
	}).done(function(data, textStatus, jqXHR)
	{
		$('.image-wrapper[data-id="' + coverpageId + '"]').fadeOut(function()
		{
			refreshCovers();
		});
		stopLoadingForeground();
	});
}
