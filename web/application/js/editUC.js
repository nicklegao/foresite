var ucAjax = null;

function reloadUC()
{
  var selectedPlans = {};
  $.each(getProposalPricing().plans, function (i, elem){
    selectedPlans[elem.pricingId] = true;
  });
  
  if (ucAjax != null)
  {
    ucAjax.abort();
  }
  ucAjax = $.ajax({
    type: "POST",
    url: '/api/ucForPlans',
    traditional: true,
    data: {
      selectedPlans : Object.keys(selectedPlans)
    }
  }).done(function( data, textStatus, jqXHR ) {

    //This function is 'borrowed' from features and benefits.
    addUCs("#ucHolder", "#htmlFragments .ucCheckbox", data.matches);
    
    $(document).trigger( "ucUpdated" );
  });
}

function addUCs(holderSelector, templateSelector, data)
{
  var domHolder = $(holderSelector);
  
  var selected = {};
  $.each($(holderSelector+" input[type='checkbox']:checked"), function(i,obj){
    selected[$(obj).attr("dataId")] = true;
  });
  
  domHolder.html('');
  
  if (Object.keys(data).length == 0)
  {
    domHolder.append($('<strong>').text('Please select a plan with Upsell-sell / Cross-sell data.'));
  }
  $.each(data, function (groupName, items){
    var wrapper = $('<div>');

    wrapper.append($('<h4>').text(groupName));
    $.each(items, function (i, obj){
      var checkbox = $(templateSelector).clone();
      
      checkbox.find("input")
        .prop('checked', selected[obj.id] != undefined)
        .attr("dataId", obj.id);
      checkbox.find("strong").text(obj.title);
      checkbox.find("span.checkboxCost").text(parseFloat(obj.unitPrice).toFixed(2));
      checkbox.find("span.checkboxTerm").text(obj.isOneOff?'':'/mth');
      checkbox.find("span.checkboxBody").text(obj.contents);
      
      wrapper.append(checkbox);

    });
    
    domHolder.append(wrapper);
  });
  initCheckboxes(domHolder, requireUCSelection);

}

function requireUCSelection(domHolder){
  domHolder.on('ifChanged', function(){
    var checkboxes = $("input[type='checkbox']", domHolder); 
    var checked = $("input[type='checkbox']:checked", domHolder);
    var unchecked = $("input[type='checkbox']:not(:checked)", domHolder);
    
    if (checked.size() >= 3)
    {
      $(checked).iCheck('enable');
      $(unchecked).iCheck('disable');
    }
    else
    {
      $(checkboxes).iCheck('enable');
    }
  }).trigger('ifChanged');
}

