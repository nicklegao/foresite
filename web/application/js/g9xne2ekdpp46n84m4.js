colors = {
	accepted : '#41b84a',
	closed : '#d9534f',
	creating : '#a9a9a9',
	needsaction : '#f09427',
	sent : '#d8d001',
	revenue : '#000',
	blue : '#009bcc'
};

colorHelper = Chart.helpers.color;

$(document).ready(function()
{
	var proposalsByTerm = new ProposalsByChart($('#proposalsBy'), {
		type : 'line',
		data : {
			labels : [],
			datasets : [ {
				label : 'In Progress',
				status : 'sent',
				data : [],
				backgroundColor : "rgba(85, 150, 158, 0.5)",
				pointBackgroundColor : "#00bcd4",
				borderColor : "#55969e",
				borderWidth : 3,
				pointBorderWidth : 2,
				pointRadius : 5
			}, {
				label : 'Accepted',
				status : 'accepted',
				data : [],
				backgroundColor : "rgba(255, 255, 255, 0.5)",
				pointBackgroundColor : "#00bcd4",
				borderColor : "#ffffff",
				borderWidth : 3,
				pointBorderWidth : 2,
				pointRadius : 5
			}, {
				label : 'Lost',
				status : 'closed',
				data : [],
				backgroundColor : "rgba(128, 222, 234, 0.5)",
				pointBackgroundColor : "#00bcd4",
				borderColor : "#80deea",
				borderWidth : 3,
				pointBorderWidth : 2,
				pointRadius : 5
			}, {
				label : 'Total',
				status : 'total',
				data : [],
				backgroundColor : "rgba(52, 138, 171, 0.5)",
				pointBackgroundColor : "#00bcd4",
				borderColor : "#348aab",
				borderWidth : 3,
				pointBorderWidth : 2,
				pointRadius : 5
			} ]
		},
		options : {
			tooltips : {
				mode : 'index',
				intersect : false,
				callbacks : {
					labelColor : function(tooltipItem, chart)
					{
						var color = chart.data.datasets[tooltipItem.datasetIndex].backgroundColor;
						color = "rgb(" + color.substring(5, color.lastIndexOf(',')) + ")";
						return {
							backgroundColor : color
						}
					}
				}
			},
			scales : {
				yAxes : [ {
					ticks : {
						fontColor : '#fff',
						callback : function(value, index, values)
						{
							if (Math.floor(value) === value)
							{
								return value;
							}
						}
					}
				} ],
				xAxes : [ {
					gridLines : {
						color : 'rgba(255,255,255,0)'
					},
					ticks : {
						fontColor : '#fff'
					}
				} ]
			},
			legend : {
				labels : {
					fontColor : '#fff'
				},
				position : 'bottom'
			}
		}
	}, function(data)
	{
		var range = data.range;
		var chart = this.chart;
		this.cleanData();
		var access = range[0] + 1;
		do
		{
			if (access >= range[1])
				access = access % range[1];

			chart.data.labels.push(data.data[access].label);
			chart.data.datasets[2].data.push(data.data[access].closed);
			chart.data.datasets[1].data.push(data.data[access].accepted);
			chart.data.datasets[3].data.push(data.data[access].sent + data.data[access].closed + data.data[access].accepted);
			chart.data.datasets[0].data.push(data.data[access].sent);

			access++;
		}
		while (access != (range[0] + 1))
		this.chart.update();
	});

	var revenue = new ProposalsByChart($('#revenue'), {
		type : 'line',
		data : {
			labels : [],
			datasets : [ {
				label : 'In Progress Revenue',
				status : 'sentRevenue',
				data : [],
				backgroundColor : "rgba(71,162,154,0.5)",
				borderColor : "#47a29a",
				pointBackgroundColor : "#00796B",
				borderWidth : 3,
				pointBorderWidth : 2,
				pointRadius : 5
			}, {
				label : 'Accepted Revenue',
				status : 'acceptedRevenue',
				data : [],
				backgroundColor : "rgba(149,195,188,0.5)",
				borderColor : "#ffffff",
				pointBackgroundColor : "#00796B",
				borderWidth : 3,
				pointBorderWidth : 2,
				pointRadius : 5
			}, {
				label : 'Lost Revenue',
				status : 'lostRevenue',
				data : [],
				backgroundColor : "rgba(136,112,68,0.5)",
				borderColor : "#887044",
				pointBackgroundColor : "#00796B",
				borderWidth : 3,
				pointBorderWidth : 2,
				pointRadius : 5,
				hidden : true
			} ]
		},
		options : {
			tooltips : {
				mode : 'index',
				intersect : false,
				callbacks : {
					label : function(tooltipItem, data)
					{
						// if (tooltipItem.datasetIndex === 0)
						return data.datasets[tooltipItem.datasetIndex].label + ': ' + '$' + tooltipItem.yLabel.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
						// return data.datasets[tooltipItem.datasetIndex].label
						// + ': ' + tooltipItem.yLabel;
					},
					labelColor : function(tooltipItem, chart)
					{
						var color = chart.data.datasets[tooltipItem.datasetIndex].backgroundColor;
						color = "rgb(" + color.substring(5, color.lastIndexOf(',')) + ")";
						return {
							backgroundColor : color
						}
					}
				}
			},
			scales : {
				yAxes : [ {
					ticks : {
						beginAtZero : true,
						callback : function(value, index, values)
						{
							return '$' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
						},
						fontColor : '#fff'
					}
				} ],
				xAxes : [ {
					gridLines : {
						color : 'rgba(255,255,255,0)'
					},
					ticks : {
						fontColor : '#fff'
					},
					stacked : true
				} ]
			},
			legend : {
				labels : {
					fontColor : '#fff'
				},
				// display : true,
				position : 'bottom'
			}
		}
	}, function(data)
	{
		var range = data.range;
		var chart = this.chart;
		this.cleanData();
		var access = range[0] + 1;
		do
		{
			// turn array int a circular list
			if (access >= range[1])
				access = access % range[1];

			chart.data.labels.push(data.data[access].label);
			chart.data.datasets[0].data.push(data.data[access].sentRevenue);
			chart.data.datasets[1].data.push(data.data[access].acceptedRevenue);
			chart.data.datasets[2].data.push(data.data[access].closedRevenue);

			access++;
		}
		while (access != (range[0] + 1))
		this.chart.update();
	});

	var lostReasonsDataset = [];
	$.each(lostReasons, function(index, object)
	{
		var reason = {};
		reason.label = object;
		reason.data = [];
		reason.backgroundColor = randomColor({
			hue : '#963f3f',
			luminosity : 'light',
			alpha : 0.5,
			format : 'rgba',
			seed : index
		});
		reason.borderColor = randomColor({
			hue : '#963f3f',
			luminosity : 'light',
			format : 'hex',
			seed : index
		});
		reason.pointBackgroundColor = "#a90329";
		reason.borderWidth = 3;
		reason.pointBorderWidth = 2;
		reason.pointRadius = 5;
		lostReasonsDataset.push(reason);
	});

	var reasonsLost = new ProposalsByChart($('#reasonsLost'), {
		type : 'line',
		data : {
			labels : [],
			datasets : lostReasonsDataset
		},
		options : {
			tooltips : {
				mode : 'index',
				intersect : false,
				callbacks : {
					labelColor : function(tooltipItem, chart)
					{
						var color = chart.data.datasets[tooltipItem.datasetIndex].backgroundColor;
						color = "rgb(" + color.substring(5, color.lastIndexOf(',')) + ")";
						return {
							backgroundColor : color
						}
					}
				}
			},
			scales : {
				yAxes : [ {
					ticks : {
						beginAtZero : true,
						fontColor : '#fff',
						callback : function(value, index, values)
						{
							if (Math.floor(value) === value)
							{
								return value;
							}
						}
					}
				} ],
				xAxes : [ {
					gridLines : {
						color : 'rgba(255,255,255,0)'
					},
					ticks : {
						fontColor : '#fff'
					},
					stacked : true
				} ]
			},
			legend : {
				labels : {
					fontColor : '#fff'
				},
				position : 'bottom'
			}
		}
	}, function(data)
	{
		var range = data.range;
		var chart = this.chart;
		this.cleanData();

		var access = range[0] + 1;
		do
		{
			// turn array int a circular list
			if (access >= range[1])
				access = access % range[1];

			chart.data.labels.push(data.data[access].label);
			$.each(chart.data.datasets, function(index, dataset)
			{
				dataset.data.push(data.data[access][dataset.label]);
			});

			access++;
		}
		while (access != (range[0] + 1))
		this.chart.update();
	});


	var lostRevenue = new ProposalsByChart($('#lostRevenue'), {
		type : 'line',
		data : {
			labels : [],
			datasets : lostReasonsDataset
		},
		options : {
			tooltips : {
				mode : 'index',
				intersect : false,
				callbacks : {
					label : function(tooltipItem, data)
					{
						console.log(tooltipItem, data);
						// if (tooltipItem.datasetIndex === 0)
						return data.datasets[tooltipItem.datasetIndex].label + ': ' + '$' + tooltipItem.yLabel.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
						// return data.datasets[tooltipItem.datasetIndex].label
						// + ': ' + tooltipItem.yLabel;
					},
					labelColor : function(tooltipItem, chart)
					{
						var color = chart.data.datasets[tooltipItem.datasetIndex].backgroundColor;
						color = "rgb(" + color.substring(5, color.lastIndexOf(',')) + ")";
						return {
							backgroundColor : color
						}
					}
				}
			},
			scales : {
				yAxes : [ {
					ticks : {
						beginAtZero : true,
						callback : function(value, index, values)
						{
							return '$' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
						},
						fontColor : '#fff'
					}
				} ],
				xAxes : [ {
					gridLines : {
						color : 'rgba(255,255,255,0)'
					},
					ticks : {
						fontColor : '#fff'
					},
					stacked : true
				} ]
			},
			legend : {
				labels : {
					fontColor : '#fff'
				},
				position : 'bottom'
			}
		}
	}, function(data)
	{
		var range = data.range;
		var chart = this.chart;
		this.cleanData();

		var access = range[0] + 1;
		do
		{
			// turn array int a circular list
			if (access >= range[1])
				access = access % range[1];

			chart.data.labels.push(data.data[access].label);
			$.each(chart.data.datasets, function(index, dataset)
			{
				dataset.data.push(data.data[access]["revenue" + dataset.label]);
			});

			access++;
		}
		while (access != (range[0] + 1))
		this.chart.update();
	});

	var lostReasonPie = new ReasonsLostPie($('#reasonsLostPie'), {
		type : 'doughnut',
		data : {
			labels : [],
			datasets : [ {
				data : [],
				backgroundColor : []
			} ]
		},
		options : {
			responsive : true,
			legend : {
				position : 'left'
			}
		}
	}, function(data)
	{
		var chart = this.chart;
		this.cleanData();

		$.each(data.data, function(label, value)
		{
			chart.data.labels.push(label);
			chart.data.datasets[0].data.push(value);
			chart.data.datasets[0].backgroundColor.push(randomColor({
				hue : '#963f3f',
				luminosity : 'bright',
				format : 'hex'
			}));
		});
		this.chart.update();
	});
});

// ******************************************************************************************************************************************************************************************************

function AnalyticsChart(element, chartOptions, changeDataFunc)
{

	this.element = element;

	this.data = {};

	this.chart = new Chart(this.element.find('canvas'), chartOptions);

	this.type = this.element.attr("data-type");

	this.changeData = changeDataFunc;

}
AnalyticsChart.prototype.setData = function(data)
{
	if (data)
		this.data = data;
}
AnalyticsChart.prototype.refresh = function()
{
	this.changeData(this.data.proposals[this.showing]);
}
AnalyticsChart.prototype.show = function(type)
{
	if (this.data.proposals[type])
	{
		this.showing = type;
		this.refresh();
	}
}
AnalyticsChart.prototype.cleanData = function()
{
	$.each(this.chart.data.datasets, function(index, value)
	{
		value.data = [];
	});
	this.chart.data.labels = [];
}

// ******************************************************************************************************************************************************************************************************

function MultipleChart(element, chartOptions, changeDataFunc)
{
	this.showing = 'month';

	AnalyticsChart.call(this, element, chartOptions, changeDataFunc);

	var context = this;
	$(this.element).on('click', '.nav li', function(event)
	{
		context.show($(this).data('show'));
	});
}
MultipleChart.prototype = Object.create(AnalyticsChart.prototype);
MultipleChart.prototype.show = function(type)
{
	if (this.data.proposals[type])
	{
		this.showing = type;
		$(this.element).find('.nav li.active').removeClass('active');
		$(this.element).find('.nav [data-show="' + type + '"]').addClass('active');
		this.refresh();
	}
}
MultipleChart.prototype.setData = function(data)
{
	if (data)
		this.data = data;
}

// ******************************************************************************************************************************************************************************************************

function ProposalsByChart(element, chartOptions, changeDataFunc)
{
	MultipleChart.call(this, element, chartOptions, changeDataFunc);

	this.start = null;
	this.end = null;
	this.teams = null;

	var context = this;

	$('.selectpicker', element).on('change.bs.select', function(e)
	{
		context.teams = $(this).val();
	}).on('hidden.bs.select', function(e)
	{
		context.update().done(function()
		{
			if (context.start)
				context.updateByDate().done(function()
				{
					context.refresh();
				});
			else
				context.refresh();
		});
	});

	$('#dateRange', element).daterangepicker({
		autoApply : true,
		alwaysShowCalendars : true,
		showCustomRangeLabel : false,
		maxDate : moment(),
		opens : "center",
		drops : "up",
		ranges : {
			'Today' : [ moment(), moment() ],
			'Yesterday' : [ moment().subtract(1, 'days'), moment().subtract(1, 'days') ],
			'Last 7 Days' : [ moment().subtract(6, 'days'), moment() ],
			'Last 30 Days' : [ moment().subtract(29, 'days'), moment() ],
			'This Month' : [ moment().startOf('month'), moment().endOf('month') ],
			'Last Month' : [ moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month') ]
		},
		locale : {
			format : dateFormatPattern,
			separator : " - "
		}
	}).on('apply.daterangepicker', function(ev, picker)
	{
		$(context.element).find('.nav li.active').removeClass('active');
		$(this).closest('li').addClass('active');
		context.start = picker.startDate.format('YYYY-MM-DD');
		context.end = picker.endDate.format('YYYY-MM-DD');

		context.updateByDate(function()
		{
			context.show('day');
		});

	}).on('change', function()
	{
		var inputWidth = $(this).textWidth();
		$(this).css({
			width : (inputWidth + 30)
		})
	}).trigger('change');

	this.update().done(function()
	{
		context.refresh();
	});
}
ProposalsByChart.prototype = Object.create(MultipleChart.prototype);
ProposalsByChart.prototype.update = function(callback)
{
	var context = this;
	return $.ajax("/api/analytics/" + this.type, {
		method : "POST",
		data : {
			teams : context.teams ? context.teams.join() : null
		}
	}).done(function(data, textStatus, jqXHR)
	{
		context.setData(data);
		if (callback)
			callback(data);
	});
}
ProposalsByChart.prototype.updateByDate = function(callback)
{
	var context = this;
	return $.ajax("/api/analytics/" + this.type, {
		method : "POST",
		data : {
			start : context.start,
			end : context.end,
			teams : context.teams ? context.teams.join() : null
		}
	}).done(function(data, textStatus, jqXHR)
	{
		data.proposals.day.data = Object.values(data.proposals.day.data).sort(function(a, b)
		{
			if (a.label <= b.label)
				return -1;
			if (a.label == b.label)
				return 0;
			if (a.label >= b.label)
				return 1;
		});

		context.data.proposals.day = data.proposals.day;

		if (callback)
			callback(data);
	});
}

// ******************************************************************************************************************************************************************************************************

function ReasonsLostPie(element, chartOptions, changeDataFunc)
{
	this.showing = 'total';

	AnalyticsChart.call(this, element, chartOptions, changeDataFunc);

	this.teams = null;

	var context = this;

	$('.selectpicker', element).on('change.bs.select', function(e)
	{
		context.teams = $(this).val();
	}).on('hidden.bs.select', function(e)
	{
		context.update().done(function()
		{
			context.refresh();
		});
	});

	this.update().done(function()
	{
		context.refresh();
	});
}
ReasonsLostPie.prototype = Object.create(AnalyticsChart.prototype);
ReasonsLostPie.prototype.update = function(callback)
{
	var context = this;
	return $.ajax("/api/analytics/" + this.type, {
		method : "POST",
		data : {
			teams : context.teams ? context.teams.join() : null
		}
	}).done(function(data, textStatus, jqXHR)
	{
		context.setData(data);
		if (callback)
			callback(data);
	});
}
ReasonsLostPie.prototype.cleanData = function()
{
	$.each(this.chart.data.datasets, function(index, value)
	{
		value.data = [];
		value.backgroundColor = [];
	});
	this.chart.data.labels = [];
}

// ******************************************************************************************************************************************************************************************************

function ActiveUsers(element)
{

	AnalyticsChart.call(this, element, {
		type : 'bar',
		data : {
			labels : [],
			datasets : [ {
				label : 'Proposals created',
				data : [],
				backgroundColor : this.colorHelper(this.colors.blue).alpha(0.8).rgbString(),
				borderColor : this.colors.blue,
				borderWidth : 1
			} ]
		},
		options : {
			tooltips : {
				mode : 'index',
				intersect : false
			},
			scales : {
				yAxes : [ {
					ticks : {
						beginAtZero : true
					}
				} ]
			}
		}
	});
}
ActiveUsers.prototype = Object.create(AnalyticsChart.prototype);
ActiveUsers.prototype.changeData = function(data)
{
	var chart = this.chart;
	this.cleanData();
	$.each(data, function(index, value)
	{
		chart.data.labels.push(value.channel);
		chart.data.datasets[0].data.push(value.userscount);
	});
	this.chart.update();
}

$.fn.textWidth = function(text, font)
{

	if (!$.fn.textWidth.fakeEl)
		$.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);

	$.fn.textWidth.fakeEl.text(text || this.val() || this.text() || this.attr('placeholder')).css('font', font || this.css('font'));

	return $.fn.textWidth.fakeEl.width();
};
