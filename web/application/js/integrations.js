


$(document).ready(function() {

    $(document).on('click', '#saveQueueButton', function(e) {
        var agent = $(this).closest('.collapse-card__body');
        if (agentIsValid(agent)) {
            saveSabreAgent(agent);
        }
    });

    $(document).on('click', '.deleteQueueButton', function(e) {
        var agent = $(this).closest('.collapse-card__body');
        deleteAgent(agent);
    });

});

function deleteAgent(element) {
    var agentID = $(element).find("#agentID");
    if (agentID === null || agentID === undefined) {
        var card = $(element).closest('.collapse-card.card');
        $(card).remove();
    } else {
        $.ajax({
            type: 'POST',
            url: '/api/removeSabreAgents',
            data: {
                agentID: $(agentID).val().trim()
            },
            success: function (data) {
                console.log(data);
                var card = $(element).closest('.collapse-card.card');
                $(card).remove();
            },
            error: function(error) {
                console.log(error);
            }
        });
    }
}

function saveSabreAgent(element) {
    var agentID = $(element).find("#agentID");
    var queueName = $(element).find("#queueName");
    var queuePhoneNumber = $(element).find("#queuePhoneNumber");
    var queueHomeCountry = $(element).find("#queueHomeCountry");
    var invalidCountrySelector = $(element).find("#invalidCountrySelector");
    var agentCode = $(element).find("#agentCode");
    var customerCode = $(element).find("#customerCode");
    var traveldocTemplateSelector = $(element).find('select.traveldocTemplateSelector');
    var autosendQueueSwitch = $(element).find('.autosendQueueSwitch');

    var activew = $(autosendQueueSwitch).hasClass('active');

    var agent = "";
    if (agentID.length > 0) {
        agent = $(agentID).val().trim();
    }

    $.ajax({
        type: 'POST',
        url: '/api/saveUpdateSabreAgents',
        data: {
            agentID: agent,
            queueName: $(queueName).val().trim(),
            queuePhoneNumber: $(queuePhoneNumber).val().trim(),
            agentCode: $(agentCode).val().trim(),
            "queueHomeCountry[]": $(queueHomeCountry).val(),
            "invalidCountrySelector[]": $(invalidCountrySelector).val(),
            customerCode : $(customerCode).val().trim(),
            traveldocTemplateSelector : $(traveldocTemplateSelector).val(),
            autosendQueueSwitch : activew
        },
        success: function (data) {
            console.log(data);
            if (data) {
                location.reload();
            } else {
                swal({
                    title : "Error",
                    text : "It wasn't possible to save this agent. Contact your TravelDocs administrator and make sure you have permission to do this.",
                    type : "error"
                });
            }
        },
        error: function(error) {
            swal({
                title : "Error",
                text : "It wasn't possible to save this agent. Contact your TravelDocs administrator and make sure you have permission to do this.",
                type : "error"
            });
        }
    });

}

function agentIsValid(element) {
    var queueName = $(element).find("#queueName");
    var agentCode = $(element).find("#agentCode");
    var customerCode = $(element).find("#customerCode");

    if ($(queueName).val().trim() !== "" && $(agentCode).val().trim() !== "" && $(customerCode).val().trim() !== "")  {
        return true;
    }
    return false;
}