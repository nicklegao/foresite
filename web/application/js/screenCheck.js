$(document).ready(function(){  
  if($(window).width() < 768) {
	  swal({
			title: "Warning!",
			text: "The size of your window is not supported. Please make it larger or switch to another device for the optimal viewing experience.",
			type: "warning",
			showConfirmButton: false,
	  })
  }
  $(window).resize(function(){
	  if($(window).width() < 768) {
		swal({
			title: "Warning!",
			text: "The size of your window is not supported. Please make it larger or switch to another device for the optimal viewing experience.",
			type: "warning",
			showConfirmButton: false,
		})
	  }	else {
		  swal.close();
	  }
  })
})