
//Set width for resizing window
var windowWidthTimer;
var mobileTimer;

$(window).on('resize', function(){
    $('.animation-image-wrapper').css('min-height', $('.animation-image-4').height())
});

$(document).ready(function(){
    $(window).trigger('resize');
    clearTimeout(mobileTimer);
    mobileTimer = setTimeout(triggerClick, 2500);
});

function triggerClick() {
    if ($(window).width() < 992) {
        $(window).trigger('resize');
        if(animationStep < maxStep) {
            animationStep += 1;
            $('.animation-image-' + animationStep).addClass('animated');
            $('.animation-text').hide();
            $('.animation-text-step-' + animationStep).show();
            if(animationStep === maxStep)
            {
                $('#restartAnimation').css('display', 'inline-block');
                $('#playAnimation').hide();
            }
        }
    }
}

var animationStep = 0;
var maxStep = 4;

$('#playAnimation').on('click', function(){
    event.preventDefault()
    $(window).trigger('resize');
    if(animationStep < maxStep) {
        animationStep += 1;
        $('.animation-image-' + animationStep).addClass('animated');
        $('.animation-text').hide();
        $('.animation-text-step-' + animationStep).show();
        if(animationStep === maxStep)
        {
            $('#restartAnimation').css('display', 'inline-block');
            $('#playAnimation').hide();
        }
    }
})

$('#restartAnimation').on('click', function(){
    event.preventDefault()
    animationStep = 0;
    $('#restartAnimation').hide();
    $('#playAnimation').css('display', 'inline-block');
    $('.animation-text').hide();
    for(var i = 0; i <= maxStep; i++)
    {
        $('.animation-image-' + i).removeClass('animated');
    }
})