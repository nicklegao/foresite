var fnbAjax = null;

function reloadFeaturesAndBenefits()
{
	var selectedPlans = {};
	$.each(getProposalPricing().plans, function (i, elem){
		selectedPlans[elem.pricingId] = true;
	});
	
	if (fnbAjax != null)
	{
		fnbAjax.abort();
	}
	fnbAjax = $.ajax({
		type: "POST",
		url: '/api/fnbForPlans',
		traditional: true,
		data: {
			selectedPlans : Object.keys(selectedPlans)
		}
	}).done(function( data, textStatus, jqXHR ) {
		
		//Find out what fNb are currently (NOT?) selected
		var groupedHolder = $(".groupedFnbHolder");
		var unselected = {};
		$.each($("input[type='checkbox']:not(:checked)", groupedHolder), function(i,obj){
			unselected[$(obj).attr("dataId")] = true;
		});
		groupedHolder.html('');

		var activeProductCategories = {};
		$.each(data.categories, function(i, category){
			var categoryName = category.categoryTitle;
			var categoryId = category.productCategoryId;
			activeProductCategories[categoryId] = categoryId;
			
			var $fnbPanel = $('.con-fnb[dataId="con-fnb-'+categoryId+'"]');
			if ($fnbPanel.size() == 0)
			{
				var newSectionIndex = $("#doc-solution > .panel").length;
				logger.info('Creating new fNb category at index:'+newSectionIndex+', with ID:'+categoryId+', Name:'+categoryName);
				
				$fnbPanel = $("#htmlFragments .con-fnb").clone();
				$fnbPanel.attr('dataId', 'con-fnb-'+categoryId);
				$fnbPanel.attr("dataOrder", newSectionIndex);

				$("#doc-solution .contentContainer").append($fnbPanel);
			}
			
			var cat = $("#htmlFragments .categoryBenefits").clone();
			$("h3", cat).text(categoryName);
			
			addBenefits(
					$(".benefitsHolder", cat), 
					category.benefits,
					unselected);

			addFeatures(
					$(".planFeaturesHolder", cat), 
					category.features,
					unselected);

			
			$(".groupedFnbHolder", $fnbPanel).append(cat);
		});
		
		//remove unused fNb sections
		$.each($("#doc-solution .con-fnb"), function(i, panel){
			var $panel = $(panel);
			var panelProductCategory = $panel.attr("dataId").substring(8);
			if (activeProductCategories[panelProductCategory] == undefined)
			{
				removeContentPanel($panel);
			}
		});
		
		$(document).trigger( "fnbUpdated" );
	});
}

function addBenefits(holderSelector, data, unselected)
{
	var domHolder = $(holderSelector).html('');
	
	if (Object.keys(data).length == 0)
	{
		domHolder.append($('<strong>').text('No benefits to display.'));
	}
	$.each(data, function (i, obj){
		var checkbox = $("#htmlFragments .benefitCheckbox").clone();
		
		checkbox.find("input").prop('checked', unselected[obj.id] == undefined)
		.attr("dataId", obj.id);
		checkbox.find("strong").text(obj.title);
		checkbox.find("span").text(obj.contents);
		
		domHolder.append(checkbox);
	});
	
	initCheckboxes(domHolder, requireBenefitSelection);
}

function requireBenefitSelection(domHolder){
	domHolder.on('ifChanged', function(){
		var checkboxes = $("input[type='checkbox']", domHolder); 
		var checked = $("input[type='checkbox']:checked", domHolder);
		var unchecked = $("input[type='checkbox']:not(:checked)", domHolder);
		
		if (checked.size() <= 3)
		{
			$(unchecked).iCheck('enable');
			$(checked).iCheck('disable');
		}
		else
		{
			$(checkboxes).iCheck('enable');
		}
	}).trigger('ifChanged');
}

function addFeatures(holderSelector, data, unselected)
{
	var domHolder = $(holderSelector).html('');
	
	if (Object.keys(data).length == 0)
	{
		domHolder.append($('<strong>').text('No features to display.'));
	}
	$.each(data, function (groupName, items){
		var plan = $("#htmlFragments .planFeatures").clone();
		$("h4", plan).text(groupName);

		$.each(items, function (i, obj){
			var checkbox = $("#htmlFragments .featureCheckbox").clone();
			
			checkbox.find("input").prop('checked', unselected[obj.id] == undefined)
			.attr("dataId", obj.id);
			checkbox.find("strong").text(obj.title);
			checkbox.find("span").text(obj.contents);
			
			plan.append(checkbox);
		});
		
		domHolder.append(plan);
		
		initCheckboxes(plan, requireFeatureSelection);
	});
}

function requireFeatureSelection(domHolder){
	domHolder.on('ifChanged', function(){
		var checkboxes = $("input[type='checkbox']", domHolder); 
		var checked = $("input[type='checkbox']:checked", domHolder);
		var unchecked = $("input[type='checkbox']:not(:checked)", domHolder);
		
		if (checked.size() <= 1)
		{
			$(unchecked).iCheck('enable');
			$(checked).iCheck('disable');
		}
		else
		{
			$(checkboxes).iCheck('enable');
		}
	}).trigger('ifChanged');
}

function initCheckboxes(domHolder, callback)
{
	setTimeout(function(){
		$(domHolder).iCheck({
			checkboxClass: 'icheckbox_square-green',
			radioClass: 'iradio_square-green'
		});
		
		if (callback != undefined)
		{
			callback(domHolder);
		}
	},100);

}
