var itemTable = undefined;
var cropped;
var image;
var errorImages = [];
var errorImagesSize = [];
var errorPDF = [];
var errorPDFSize = [];
var maxSize = 6;
var selectedImageUrl = "";
var selectedImage;
var fileNumber = 0;
var pdfEdit = false;
var ValidImageTypes = [ "image/gif", "image/jpeg", "image/png" ];
var imagePath;
var folder;
var pdfdropZone;
var libraryTypeGlobal;
var selectButtons = 0;
var selectButtonsList = new Array();
Dropzone.autoDiscover = false;

$(document).ready(function()
{
	$(document).on('click', ".export-product-folder-action", function()
	{
		exportProductsAction.call($(this), $(this), false);
	});
	$(document).on('click', ".export-all-products-action", function()
	{
		exportProductsAction.call($(this), null, false);
	});
	$(document).on('click', ".download-sample-action", function()
	{
		exportProductsAction.call($(this), null, true);
	});
	$(document).on('click', ".import-product-folder-action", function()
	{
		importProductsAction.call($(this), $(this));
	});
	$(document).on('click', ".import-all-products-action", function()
	{
		importProductsAction.call($(this), null);
	});
	$(document).on('click', ".manage-content-library-btn", manageContentLibraryAction);
	$(document).on('click', ".delete-library-item-action", deleteLibraryItemAction);
	$(document).on('click', ".delete-library-item-action-details", deleteLibraryItemActionDetails);
	$(document).on('click', ".add-library-item-action", function()
	{
		$('#cropImage').hide();
		addEditLibraryItemAction.call($(this), true);

	});
	$(document).on('click', ".edit-library-item-action", function()
	{

		addEditLibraryItemAction.call($(this), false);
	});
	$(document).on('click', ".edit-library-folder-action", editLibraryFolderAction);
	$(document).on('click', ".add-library-folder-action", addLibraryFolderAction);
	$(document).on('click', ".select-library-item-action", selectLibraryItemsAction);
	$(document).on('click', ".delete-multiple-library-item-action", deleteMultipleLibraryItemsAction);
	$(document).on('click', ".select-library-item-button", selectLibraryItemsButtonAction);
	$(document).on('click', ".delete-library-folder-action", deleteLibraryFolderAction);
	$(document).on('change', 'select[name="folder"]', newFolderPrompt);
	$(document).on('change', 'select[name="subFolder"]', newSubFolderPrompt);
	$(document).on('click', '.imageDeleteConfimed', processImageDelete);
	$(document).on('click', '.imageMultiDeleteConfimed', processimageMultiDelete);
	$(document).on('click', "#chooseImage", function()
	{
		$("#imageUploadButton").val("");
		$("#imageUploadButton").click();
	});


	$(document).on('click', ".clearColumnFilters", function(e)
	{
		if ($('div.tab-pane#folders').hasClass('active'))
		{
			var contentLibrary = $('div.tab-pane#folders').find('.content-library-table');
			contentLibrary.DataTable().columns().search('');
			contentLibrary.DataTable().draw();

			$('tr.folder-filters').find('input').each(function()
			{
				$(this).val('');
			});
			$('tr.folder-filters').find('select').each(function()
			{
				$(this).val('');
			});

		}
		if ($('div.tab-pane#items').hasClass('active'))
		{
			var contentLibrary = $('div.tab-pane#items').find('#content-library-table');
			contentLibrary.DataTable().columns().search('');
			contentLibrary.DataTable().draw();

			$('tr.item-filters').find('input').each(function()
			{
				$(this).val('');
			});
			$('tr.item-filters').find('select').each(function()
			{
				$(this).val('');
			});
		}
	});

	$(document).on('click', "#cropImage", function()
	{
		if (imagePath == undefined)
			imagePath = $("#thumbnailImage").attr('src');
		var blob = null;
		var xhr = new XMLHttpRequest();
		xhr.open("GET", imagePath);
		xhr.responseType = "blob";// force the HTTP response, response-type
		// header to be blob
		xhr.onload = function()
		{
			blob = xhr.response;// xhr.response is now a blob object
			var f = new File([ blob ], $('#fileName').text());
			var reader = new FileReader();
			reader.onload = function(e)
			{
				selectedImageUrl = e.target.result;
				$("#jCropModalImage #JcropPhotoFrame").attr('src', e.target.result).width("auto").height('auto');
				selectedImage = f;

				loadJcropModal();
			};
			showUploadingImage(true);
			reader.readAsDataURL(f);
		}
		xhr.send();
	});

	$("#imageUploadButton").on("change", function(evt)
	{
		$('#cropImage').hide();
		var file = evt.target.files;

		if (FileReader && file && file[0])
		{
			var reader = new FileReader();
			reader.onload = function(e)
			{
				selectedImageUrl = e.target.result;
				$("#jCropModalImage #JcropPhotoFrame").attr('src', e.target.result).width("auto").height('auto');
				selectedImage = file[0];

				if ($.inArray(selectedImage["type"], ValidImageTypes) < 0)
				{
					swal("Only image files are allowed.");
				}
				else
				{

					loadJcropModal();
				}
			};
			showUploadingImage(true);
			reader.readAsDataURL(file[0]);
		}
		else
		{
			alert("This browser do not support file upload.");
		}
	});

	$("#jCropModalImage").on('click', '.cancelImageUpload', function(event)
	{
		$('#cropImage').show();
		$("#jCropModalImage").modal('hide');
	});

	$("#imageDeleteModal").on('click', '.cancelDeleteButton', function(event)
	{
		$("#imageDeleteModal").modal('hide');
	});

	$("#imageMultiDeleteModal").on('click', '.cancelMultiDeleteButton', function(event)
	{
		$("#imageMultiDeleteModal").modal('hide');
	});

	// This is to stop a bug that was override existing folders when the user
	// would create after editing and then cancelling existing folder
	$("#content-library-folder-edit-modal").on('click', 'button#editFolderClose', function(event)
	{
		event.preventDefault();
		$('input[name=folderId]').removeAttr('value');
		$("#content-library-folder-edit-modal").modal('hide');
	});

	$("#jCropModalImage").on('click', '.uploadFile', function()
	{
		var ratio = 1;

		if (image.naturalWidth > 768)
		{
			ratio = image.naturalWidth / 768;
		}

		var x = Math.floor(cropped.x / ratio);
		var y = Math.floor(cropped.y / ratio);
		var x2 = Math.floor(cropped.x2 / ratio);
		var y2 = Math.floor(cropped.y2 / ratio);

		$("#image-container").show();
		$("#img-width").text(Math.floor(cropped.w));
		$("#img-height").text(Math.floor(cropped.h));
		$("#image-alert").show();
		$("#thumbnailImage").attr('src', selectedImageUrl).show();
		$("#thumbnailImage").addClass('svg-clipped');
		// $("#thumbnailImage").attr('style', 'position:absolute; clip-path:
		// polygon('+cropped.x/ratio+'px '+ cropped.y/ratio+'px ,'+
		// cropped.x2/ratio+'px '+cropped.y/ratio+'px ,'+cropped.x2/ratio+'px
		// '+cropped.y2/ratio+'px ,'+cropped.x/ratio+'px
		// '+cropped.y2/ratio+'px); -webkit-clip-path:
		// polygon('+cropped.x/ratio+'px '+ cropped.y/ratio+'px ,'+
		// cropped.x2/ratio+'px '+cropped.y/ratio+'px ,'+cropped.x2/ratio+'px
		// '+cropped.y2/ratio+'px ,'+cropped.x/ratio+'px
		// '+cropped.y2/ratio+'px);');
		$("#image-container").attr('style', 'background-color: rgba(0,0,0,0.63) !important; background: url(' + selectedImageUrl + ')').width($("#thumbnailImage").width()).height($("#thumbnailImage").height());
		$('#svgPath').html('<rect x="' + x + '" y="' + y + '" width="' + cropped.w / ratio + '" height="' + cropped.h / ratio + '" opacity="1" />');

		$('#save-changes').prop("disabled", false);

		$("#jCropModalImage").modal('hide');
	});


	$("body").tooltip({
		selector : '[data-toggle=tooltip]'
	});

	$.fn.dataTable.ext.errMode = function(event, settings, techNote, tn)
	{
		console.log('An error has been reported by DataTables: ', techNote, tn);
	};

});

function clearPreview()
{
	$("#thumbnailImage").removeClass('svg-clipped');
	$("#thumbnailImage").removeAttr('style');
	$("#image-container").removeAttr('style');
	$("#image-container").hide();
	$("#image-alert").hide();
}


function showErrorSize(show)
{
	if (show)
	{
		$("#jCropModalImage .uploadErrorSize").show();
	}
	else
	{
		$("#jCropModalImage .uploadErrorSize").hide();
	}
}


function showUploadingImage(show)
{
	if (show)
	{
		$(".uploadLoading").show();
	}
	else
	{
		$(".uploadLoading").hide();
	}
}

function loadJcropModal()
{
	var $jcropModal = $("#jCropModalImage");
	showErrorSize(false);
	var jcrop;

	$jcropModal.on('shown.bs.modal', function()
	{
		image = new Image();
		image.src = $("#JcropPhotoFrame", $jcropModal).attr('src');
		image.onload = function()
		{
			if (selectedImage.size <= 6000000)
			{
				$("#JcropPhotoFrame", $jcropModal).Jcrop({
					bgOpacity : 0.4,
					trueSize : [ image.naturalWidth, image.naturalHeight ],
					onSelect : function(c)
					{
						cropped = extra = c;
					}
				}, function()
				{
					jcrop = this;
					$(".uploadFile", $jcropModal).removeClass("disabled");
					showUploadingImage(false);
				});
				jcrop.setSelect([ 0, 0, 500, 500 ]);
				jcrop.setOptions({
					allowSelect : false
				});
			}
			else
			{
				showErrorSize(true);
				$(".uploadFile", $jcropModal).addClass("disabled");
				showUploadingImage(false);
			}

		}
	}).on('hide.bs.modal', function()
	{
		$jcropModal.hide();
		if (jcrop)
		{
			jcrop.destroy();
		}
	});

	$jcropModal.modal({
		width : '800px',
		backdrop : 'static',
		keyboard : false
	});


}

function manageContentLibraryAction(e)
{

	$('body').modalmanager('loading');
	var $btn = $(this);

	var $modal = $('#content-library-modal');
	$modal.load('/api/library/manage', {
		libraryType : $btn.data('module')
	}, function()
	{
		$modal.modal({
			width : '90%',
			backdrop : 'static',
			keyboard : false
		});
	});

	$modal.one('show.bs.modal', function()
	{
		initTable($('#content-library-table', $modal));
		initDynaTable($('#images-list', $modal));
		initTable($('#content-library-folder-table', $modal));
		$('.modal-title', $modal).text(modalTitle($btn.data('module')));

	});

	$modal.one('shown.bs.modal', function()
	{
		$('#content-library-table', $modal).DataTable().draw();
		$('#content-library-table-gen_images', $modal).dynatable();
		$('a[href="#folders"]').on('shown.bs.tab', function()
		{
			$('#content-library-folder-table', $modal).DataTable().draw();
		});
		$(window).resize(function()
		{
			$('#content-library-table', $modal).DataTable().draw();
			$('#content-library-folder-table', $modal).DataTable().draw();
		});
	});

	$modal.one('hide.bs.modal', function()
	{
		$(document).trigger('content-library-changed');
	});
}

function modalTitle(btn)
{
	if (btn.toLowerCase() == "images")
	{
		return "Manage Image Library"
	}
	if (btn.toLowerCase() == "products")
	{
		return "Manage Product Library"
	}
	if (btn.toLowerCase() == "text")
	{
		return "Manage Text Library"
	}
	if (btn.toLowerCase() == "videos")
	{
		return "Manage Video Library"
	}
	// To be implemented in the future
	// if (btn.toLowerCase() == "cover pages") {
	// return "Manage Image Library"
	// }
	// if (btn.toLowerCase() == "templates") {
	// return "Manage Image Library"
	// }

}

function deleteLibraryItemAction(e)
{
	var $item = $(this).closest('tr');

	$.ajax({
		type : 'post',
		url : '/api/library/delete',
		data : {
			'element-id' : $item.data('element-id'),
			'library-type' : $item.data('library-type')
		}
	}).done(function(response)
	{
		if (response.success)
		{
			$('#content-library-table').DataTable().row($item).remove().draw();
			swal({
				title : 'Success!',
				type : 'success',
				text : response.message,
				html : true
			});
		}
		else
		{
			swal({
				title : 'Error',
				type : 'error',
				text : response.message,
				html : true
			})
		}
	});
}

function deleteLibraryItemActionDetails(e)
{
	$('#imageDeleteModal').modal('show');

}

function processImageDelete()
{
	$('#imageDeleteModal').modal('hide');
	var $item = $('#elID');
	var library = 'images';
	$.ajax({
		type : 'post',
		url : '/api/library/delete',
		data : {
			'element-id' : $item.val(),
			'library-type' : library
		}
	}).done(function(response)
	{
		if (response.success)
		{
			folder = $('#search-folder').val();
			$('#images-library-add-edit-modal').modal('hide');
			$('#content-library-modal').load('/api/library/manage', {
				libraryType : library
			}, function()
			{
				reloadImages();
			});
			swal({
				title : 'Success!',
				type : 'success',
				text : response.message,
				html : true
			});
		}
		else
		{
			swal({
				title : 'Error',
				type : 'error',
				text : response.message,
				html : true
			})
		}
	});
}

function newFolderPrompt(e)
{
	var $select = $(this);
	if ($select.val() == "0")
	{
		$select.siblings('#folderNameId').removeClass('hidden');
		$('#pdfNewFolderText').attr('required', true);
		if ($('.subFolderLabel').length > 0 && $('.subFolderLabel').val() != "")
		{
			$select.siblings('.subFolderLabel').addClass('hidden');
			$select.siblings('.subFolderSelect').addClass('hidden');
			$select.siblings('#subFolderNameId').removeClass('hidden');
		}
	}
	else
	{
		$select.siblings('#folderNameId').addClass('hidden');
		$('#pdfNewFolderText').removeAttr('required');

		if ($('.subFolderLabel').length > 0 && ($select.attr('id') == "productNameSelector" || $select.attr('id') == "spreadsheetNameSelector"))
		{
			$.get('/api/library/add-edit/' + libraryTypeGlobal + '/subfolder/' + $select.val(), function(response)
			{
				var $modal = $('#' + libraryTypeGlobal.toLowerCase() + '-library-add-edit-modal');
				fetchSubFolderSelectOptions($modal, response);
				$select.siblings('.subFolderLabel').removeClass('hidden');
				$select.siblings('.subFolderSelect').removeClass('hidden');
				$select.siblings('#subFolderNameId').addClass('hidden');
			});
		}
	}
}

function newSubFolderPrompt(e)
{
	var $select = $(this);
	if ($select.val() == "0" && this.options[this.selectedIndex].text != "No SubFolder")
	{
		$select.siblings('#subFolderNameId').removeClass('hidden');

	}
	else
	{
		$select.siblings('#subFolderNameId').addClass('hidden');
	}
}

function requireCheckGroup(element)
{
	var name = element.name;
	var idx = name.substring(name.length - 1, name.length);
	var form = $(element).closest('form');
	return $('[name="attr_label' + idx + '"]', form).val() != '' || $('[name="attrfloat_qty' + idx + '"]', form).val() != '' || $('[name="attrcurrency_price' + idx + '"]', form).val() != ''
}

var products_add_edit_validator = $('#add-edit-products-form').validate({
	ignore : [],
	rules : {
		folder : "required",
		newFolder : {
			required : function(element)
			{
				return $(element).closest('form').find('select[name="folder"]').val() == '0'
			},
		},
		productName : "required",
		description : "required",
		modelNumber : "required",
		price : {
			required : true,
			number : true
		},
		attrfloat_qty1 : {
			required : requireCheckGroup,
			number : true
		},
		attrfloat_qty2 : {
			required : requireCheckGroup,
			number : true
		},
		attrfloat_qty3 : {
			required : requireCheckGroup,
			number : true
		},
		attrfloat_qty4 : {
			required : requireCheckGroup,
			number : true
		},
		attrfloat_qty5 : {
			required : requireCheckGroup,
			number : true
		},
		attrcurrency_price1 : {
			required : requireCheckGroup,
			number : true
		},
		attrcurrency_price2 : {
			required : requireCheckGroup,
			number : true
		},
		attrcurrency_price3 : {
			required : requireCheckGroup,
			number : true
		},
		attrcurrency_price4 : {
			required : requireCheckGroup,
			number : true
		},
		attrcurrency_price5 : {
			required : requireCheckGroup,
			number : true
		},
		attr_label1 : {
			required : requireCheckGroup,
			maxlength : 100
		},
		attr_label2 : {
			required : requireCheckGroup,
			maxlength : 100
		},
		attr_label3 : {
			required : requireCheckGroup,
			maxlength : 100
		},
		attr_label4 : {
			required : requireCheckGroup,
			maxlength : 100
		},
		attr_label5 : {
			required : requireCheckGroup,
			maxlength : 100
		},
	},
	submitHandler : function(form)
	{
		custFieldData = {}
		for (var i = 1; i < 6; i++)
		{
			var key = "attr_customfield" + i;
			if (productsCustomFields[key] && productsCustomFields[key].type == 'date')
			{
				var _d = moment($('.' + key, $(form)).val(), dateFormatPattern);
				if (_d.isValid())
				{
					custFieldData[key] = _d.format('YYYY-MM-DD');
				}
				else
				{
					custFieldData[key] = '';
				}
			}
		}
		$(form).ajaxSubmit({
			data : {
				custFieldData : custFieldData,
				note : $('#add-edit-products-form .panel-froala').froalaEditor('html.get').replace(/\<br>/g, '<br/>')
			},
			success : function(response)
			{
				if (response)
				{
					$('#products-library-add-edit-modal').modal('hide');
					$('#content-library-modal').load('/api/library/manage', {
						libraryType : 'products'
					}, function()
					{
						// initTable($('#content-library-table',
						// $('#content-library-modal')));
						$('#content-library-table', $('#content-library-modal')).DataTable().draw();
					});
					$(form).clearForm();
				}
			}
		});
	},
	showErrors : function(errorMap, errorList)
	{
		if (errorList.length > 0)
		{
			console.log("Your form contains " + this.numberOfInvalids() + " errors, see details below.");
			console.log(errorMap, errorList);

			var id = $(errorList[0].element).closest('.tab-pane').attr('id');
			$(errorList[0].element).closest('.modal-body').find('.nav.nav-tabs a[href="#' + id + '"]').click();
		}
		this.defaultShowErrors();
	}
});

var spreadsheet_add_edit_validator = $('#add-edit-spreadsheet-form').validate({
	ignore : [],
	rules : {
		folder : "required",
		newFolder : {
			required : function(element)
			{
				return $(element).closest('form').find('select[name="folder"]').val() == '0'
			},
		},
		spreadsheetName : "required",
	},
	submitHandler : function(form)
	{
		var spreadsheet = $('.spreadsheet', form).data('kendoSpreadsheet');
		
		$('.spreadsheet', form).siblings('input.spreadsheet-val').val(JSON.stringify(removeSpreadsheetSelection(spreadsheet.toJSON())));
		$(form).ajaxSubmit({
			success : function(response)
			{
				if (response)
				{
					$('#spreadsheet-library-add-edit-modal').modal('hide');
					$('#content-library-modal').load('/api/library/manage', {
						libraryType : 'spreadsheet'
					}, function()
					{
						$('#content-library-table', $('#content-library-modal')).DataTable().draw();
					});
					$(form).clearForm();
					$('.spreadsheet', form).empty();
				}
			}
		});
	},
	showErrors : function(errorMap, errorList)
	{
		this.defaultShowErrors();
	}
});


if ($('.pdfDropzone.dz-clickable').length == 0)
{
	pdfdropZone = new Dropzone('#uploadPDFDrop', {
		url : '/api/library/add-edit/pdf',
		maxFilesize : 12,
		maxFiles : 6,
		paramName : 'file',
		forceFallback : false,// enable for testing
		acceptedFiles : 'application/pdf',
		autoProcessQueue : false,
		paramName : 'folderName',
		dictDefaultMessage : "Upload file please",
		createImageThumbnails : false,
		init : function()
		{

			this.on('addedfile', function(file)
			{
				fileNumber++;
			});

			this.on("maxfilesexceeded", function(file)
			{
				this.removeFile(file);
				fileNumber--;
				swal('Upload failed', 'Only 6 files can be uploaded at one time!', 'error');
			});

			this.on("processing", function()
			{
				this.options.autoProcessQueue = true;
			});

			this.on('sending', function(file, xhr, formData)
			{
				formData.append('newFolder', $('#add-edit-pdf-form').find('input[name="newFolder"]').val());
				if ($('#add-edit-pdf-form').find(':selected').text() == "")
				{
					formData.append('folder', 'New Bulk Folder');
				}
				else
				{
					formData.append('folder', $('#add-edit-pdf-form').find(':selected').text());
				}

				formData.append('file', file);
			});

			this.on('complete', function(file, response)
			{
				if (file.accepted)
				{
					pdfdropZone.removeFile(file);
				}
			});

			this.on('queuecomplete', function(file)
			{
				if (errorPDF.length > 0)
				{
					var errorMessage = "Couldn't upload the following images <br/> Please try again.";
					errorMessage += "<ul id='errorPDF'>"
					for (i = 0; i < errorPDF.length; i++)
					{
						errorMessage += "<li>" + errorPDF[i].name + "</li>";
					}
					errorMessage += "</ul>"
					swal({
						html : true,
						title : '<i>Warning</i>',
						text : errorMessage
					}, function()
					{
						pdfdropZone.removeAllFiles();
					});
					errorPDF = [];
				}
				else
				{
					$('#pdf-library-add-edit-modal').modal('hide');
					$('#content-library-modal').load('/api/library/manage', {
						libraryType : 'pdf'
					}, function()
					{
						initTable($('#content-library-table', $('#content-library-modal')));
					});
					pdfdropZone.removeAllFiles();
					// CLEAR THE DROPABLE AND FORM
					pdfdropZone.disable();
				}

			});


			this.on('error', function(file)
			{
				var fileType = file.type;
				if ((file.size / 1048576) > maxSize)
				{
					errorPDFSize.push(file);
				}
				else if (fileType.indexOf("pdf") == -1)
				{
					swal('File must be PDF', 'Only PDF files are allowed to be uploaded into the PDF Library', 'error');
					this.removeFile(file);
				}
				console.log("ERROR");
			});
		}
	});
}
else
{
	if (pdfdropZone != null)
	{
		pdfdropZone.removeAllFiles();
	}
}

$(".pdfUploadButton").click(function(event)
{
	if ($("#add-edit-pdf-form").valid())
	{
		event.preventDefault();
		if (fileNumber > 0 && !pdfEdit)
		{
			pdfdropZone.processQueue();
		}
		else
		{
			$('#add-edit-pdf-form').submit();

		}
	}
});


$('#add-edit-pdf-form').validate({
	rules : {
		folder : "required",
		assetName : "required"
	},
	submitHandler : function(form)
	{
		var formData = new FormData(form);
		var select = $('select[name=folder]>option:selected').html()
		formData.append('assetRealName', select);
		$.ajax({
			type : 'POST',
			url : '/api/library/add-edit/pdf',
			data : formData,
			cache : false,
			contentType : false,
			processData : false
		}).done(function(data)
		{

			$('#folderNameId').addClass('hidden');
			$('#pdfNewFolderText').removeAttr('required');
			$('#pdf-library-add-edit-modal').modal('hide');
			$('#content-library-modal').load('/api/library/manage', {
				libraryType : 'pdf'
			}, function()
			{
			});
			$(form).clearForm();
		});

	}
});
$('#add-edit-text-form').validate({
	rules : {
		folder : "required",
		assetName : "required",
		textBlock : "required"
	},
	submitHandler : function(form)
	{
		$(form).ajaxSubmit({
			data : {
				textBlock : $('#add-edit-text-form .panel-froala').froalaEditor('html.get').replace(/\<br>/g, '<br/>')
			},
			success : function(response)
			{
				if (response)
				{
					$('#text-library-add-edit-modal').modal('hide');
					$('#content-library-modal').load('/api/library/manage', {
						libraryType : 'text'
					}, function()
					{
						initTable($('#content-library-table', $('#content-library-modal')));
					});
					$(form).clearForm();
				}
			}
		});
	}
});


$('#add-edit-images-form').validate({
	rules : {
		folder : "required",
		assetName : "required"
	},
	submitHandler : function(form)
	{
		// remove ck
		// $('#imageLibDescTextArea').val(CKEDITOR.instances.imageLibDescTextArea.getData());
		var formData = new FormData(form);
		formData.append("image", selectedImage);
		formData.append("description", $('#add-edit-images-form .panel-froala').froalaEditor('html.get').replace(/\<br>/g, '<br/>'));
		if (cropped)
		{
			$.each(cropped, function(key, value)
			{
				formData.append(key, Math.ceil(value));
			});
		}
		$.ajax({
			type : 'POST',
			url : '/api/library/add-edit/images',
			data : formData,
			cache : false,
			contentType : false,
			processData : false
		}).done(function(data)
		{
			if (data)
			{
				folder = $('#search-folder').val();
				logger.info("Image uploaded");
				$("#jCropModalImage").modal('hide');
				$('#images-library-add-edit-modal').modal('hide');
				$('#content-library-modal').load('/api/library/manage', {
					libraryType : 'images'
				}, function()
				{
					reloadImages();
				});
				selectedImage = null;
				$(form).clearForm();
			}
			else
			{
				logger.error("Error uploading image");
			}
			$("#jCropModalImage .btn").removeClass('disabled');
			showUploadingImage(false);
			$('#cropImage').show();
			imagePath = undefined;
		});

	}
});

$('input[name="caption"]').keyup(function()
{
	$('label#warningLimit').text($('input[name="caption"]').val().length + "/255");
	if ($('input[name="caption"]').val().length > 230)
	{
		$('label#warningLimit').attr('style', 'position: absolute; top: 33px; right: 7px;color: red');
	}
	else
	{
		$('label#warningLimit').attr('style', 'position: absolute; top: 33px; right: 7px;color: black');
	}
});

$(".videoSubmit").click(function(event)
{
	if ($("#add-edit-videos-form").valid())
	{
		var uploadValid = false;
		event.preventDefault();
		var videoLinkUrl = $('#videoLinkUrl').val()
		if (videoLinkUrl.indexOf("https://www.youtube.com/") >= 0 || videoLinkUrl.indexOf("https://vimeo.com/") >= 0 || videoLinkUrl.indexOf("https://youtu.be/") >= 0)
		{
			if (videoLinkUrl.indexOf('&') >= 0)
			{
				$('#videoLinkUrl').val(videoLinkUrl.substring(0, videoLinkUrl.indexOf('&')));
			}
			uploadValid = true;
		}
		else
		{
			uploadValid = false;
			$('#videoLinkUrl').val('');
			$('#videoLinkUrl').attr('style', 'border:1px solid #ff0000 !important;');
			$('#videoLinkUrl').attr('placeholder', "You didnt enter a valid Youtube or Vimeo Link, starting with https://");
		}

		if ($('input[name="caption"]').val().length > 255)
		{
			uploadValid = false
			$('label#warningText').text('WARNING: Your caption must be 255 Character or below.');
			$('label#warningText').attr('style', 'color: red');

		}
		else
		{
			uploadValid = true;
		}

		if (uploadValid)
		{
			$(".videoSubmit").submit();
		}
	}
});

$('#add-edit-videos-form').validate({
	rules : {
		folder : "required",
		assetName : "required"
	},
	submitHandler : function(form)
	{
		$(form).ajaxSubmit({
			success : function(response)
			{
				if (response)
				{
					$('#videos-library-add-edit-modal').modal('hide');
					$('#content-library-modal').load('/api/library/manage', {
						libraryType : 'videos'
					}, function()
					{
						initTable($('#content-library-table', $('#content-library-modal')));
					});
					$(form).clearForm();
				}


			}
		});
	}
});

$('#folder-edit-form').validate({
	rules : {
		folderName : "required"
	},
	submitHandler : function(form)
	{
		var libraryType = $('[name="libraryType"]', form).val();
		$.post('/api/library/folder/edit', $(form).serialize(), function(response)
		{
			if (response)
			{
				$('#content-library-folder-edit-modal').modal('hide');
				$('#content-library-modal').load('/api/library/manage', {
					libraryType : libraryType
				}, function()
				{
					initTable($('#content-library-table', $('#content-library-modal')));
					initTable($('#content-library-folder-table', $('#content-library-modal')));
					$('a[href="#folders"]').tab('show');
					$('#content-library-folder-table').DataTable().draw();
					reloadImages();
				});
			}
		});
	}
});

$('#folder-delete-form').validate({
	rules : {
		transferTo : "required"
	},
	submitHandler : function(form)
	{
		var libraryType = $('[name="libraryType"]', form).val();
		$.post('/api/library/folder/delete', $(form).serialize(), function(response)
		{
			if (response)
			{
				$('#content-library-folder-delete-modal').modal('hide');
				$('#content-library-modal').load('/api/library/manage', {
					libraryType : libraryType
				}, function()
				{
					initTable($('#content-library-table', $('#content-library-modal')));
					initTable($('#content-library-folder-table', $('#content-library-modal')));
					$('a[href="#folders"]').tab('show');
					$('#content-library-folder-table').DataTable().draw();
					console.log(libraryType);
					if (libraryType == "images")
					{
						reloadImages()
					}
				});
			}
		});
	}
});

$('#import-products-form').validate({
	ignore : '',
	rules : {
		csvUpload : "required",
	},
	submitHandler : function(form)
	{
		startLoadingForeground();
		$(form).ajaxSubmit({
			success : function(response)
			{
				stopLoadingForeground();
				if (response.success)
				{
					startProductImportStatusCheck();
				}
				else
				{
					swal({
						title : "Error",
						type : "warning",
						text : response.message,
						closeOnConfirm : true,
						html : true,
					});
				}
			}
		});
	}
});

function addEditLibraryItemAction(newItem)
{
	clearPreview();
	var libraryType;
	var $item = $(this).closest('tr');
	if (newItem)
	{
		libraryType = $(this).data('library-type').toLowerCase();
	}
	else
	{
		libraryType = $item.data('library-type');
		if (libraryType == null)
		{
			$item = $(this).closest('li');
			libraryType = $item.data('library-type');
		}
	}
	var $modal = $('#' + libraryType.toLowerCase() + '-library-add-edit-modal');
	if (newItem)
	{
		$('.modal-title', $modal).text('Add ' + libraryType.toLowerCase().substr(0,1).toUpperCase()+libraryType.toLowerCase().substr(1));
		$('.delete-library-item-action-details', $modal).hide();
		$('[name="elementId"]', $modal).val('');
	}
	else
	{
		$('.delete-library-item-action-details', $modal).show();
		$('.modal-title', $modal).text('Edit ' + libraryType.toLowerCase().substr(0,1).toUpperCase()+libraryType.toLowerCase().substr(1));
	}
	$('form', $modal).attr('action', '/api/library/add-edit/' + libraryType);
	$('form', $modal).clearForm();

	if (libraryType == 'images')
	{
		$('#thumbnailImage', $modal).attr('src', '');
	}


	$.get('/api/library/add-edit/' + libraryType, function(response)
	{
		fetchFolderSelectOptions($modal, response);

		if (libraryType == 'products')
		{
			libraryTypeGlobal = libraryType;
			products_add_edit_validator.resetForm();
			// select first option in folder selector, as reset form will ignore
			// the disabled option


			if ($('#add-edit-products-form .panel-froala').data('froala.editor'))
			{
				$('#add-edit-products-form .panel-froala').froalaEditor('destroy');
			}
			$('#add-edit-products-form .panel-froala').on('froalaEditor.initialized', function(ev, editor)
			{
				customizeFroalaEditor(ev, editor)
			}).froalaEditor({
				enter: $.FroalaEditor.ENTER_P,
				fontSize: ['8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '22', '24', '26', '28', '30', '60', '96'],
				fontSizeDefaultSelection: '14',
				toolbarSticky: true,
				key : 'DLAHYKAJOEc1HQDUH=='
			});

			$('form select[name="folder"] option:eq(0)', $modal).prop("selected", true);
			$('#thumbnail-buttons button', $modal).unbind('click');
			$('#imageUpload', $modal).unbind("change");
			$("#thumbnail", $modal).html("");
			$("#thumbnail-buttons button", $modal).remove();
			$('[name="term"]', $modal).prop("readonly", false);
			$('.subFolderLabel', $modal).addClass('hidden');
			$('.subFolderSelect', $modal).addClass('hidden');
			$('#subFolderNameId', $modal).addClass('hidden');


			$('#imageUpload', $modal).on("change", function(evt)
			{
				$("#thumbnail", $modal).html("");
				$('[name="deleteImage"]', $modal).prop("checked", false);
				var file = evt.target.files;

				if (FileReader && file && file[0])
				{
					var reader = new FileReader();
					reader.onload = function(e)
					{
						var img = new Image();
						img.onload = function()
						{
							var canvas = document.createElement("canvas");
							canvas.width = 100;
							canvas.height = (img.height / img.width) * canvas.width;
							var context = canvas.getContext("2d");
							context.imageSmoothingEnabled = true;
							context.imageSmoothingQuality = "high";
							context.drawImage(img, 0, 0, canvas.width, canvas.height);
							$("#thumbnail", $modal).append(canvas);
							$('[name="image"]', $modal).val(canvas.toDataURL("image/png"));
						}
						img.src = reader.result;
					};
					reader.readAsDataURL(file[0]);
				}
				else
				{
					alert("This browser do not support file upload.");
				}
			});

			$('[name=oneOff]').iCheck({
				checkboxClass : 'icheckbox_square-orange',
				radioClass : 'iradio_square-orange'
			});

			$('[name=oneOff]', $modal).on('ifChecked', function()
			{
				$('[name="term"]', $modal).val(1);
				$('[name="term"]', $modal).prop("readonly", true);
			});
			$('[name=oneOff]', $modal).on('ifUnchecked', function()
			{
				$('[name="term"]', $modal).prop("readonly", false);
			});


			if (!newItem)
			{

				$.get('/api/library/add-edit/' + libraryType + '/' + $item.data('element-id'), function(response)
				{
					$('[name="elementId"]', $modal).val(response.element_id);

					$('[name="folder"]', $modal).val(response.category_id);

					$.get('/api/library/add-edit/' + libraryTypeGlobal + '/subfolder/' + response.category_id, function(response1)
					{
						fetchSubFolderSelectOptions($modal, response1);
						$('.subFolderLabel', $modal).removeClass('hidden');
						$('.subFolderSelect', $modal).removeClass('hidden');
						if (response1 != "")
						{
							$('[name="subFolder"]', $modal).val(response.attr_subfolder);
						}
						$('[name="productName"]', $modal).val(response.attr_headline);
						$('[name="description"]', $modal).val(response.attr_description);
						$('[name="price"]', $modal).val(response.attrcurrency_price || 0);
						$('[name="term"]', $modal).val(response.attr_term || 1);
						$('[name="oneOff"]', $modal).iCheck(response.attrcheck_oneoff ? 'check' : 'uncheck');
						$('[name="note"]', $modal).val(response.attr_note);
						$('#add-edit-products-form .panel-froala').froalaEditor('html.set', response.attr_note);
						if (response.attrfile_image)
						{
							$("#thumbnail", $modal).append($("<img>").attr("src", "/stores" + response.attrfile_image));
							$("#thumbnail-buttons", $modal).append(getButton("Replace"));
							$("#thumbnail-buttons", $modal).append(getButton("Delete"));
						}
						else
						{
							$("#thumbnail-buttons", $modal).append(getButton("Add"));
						}

						$('.choseFile', $modal).on("click", function()
						{
							$('#imageUpload', $modal).val("");
							$('#imageUpload', $modal).click();
						});

						$('.deleteFile', $modal).on("click", function()
						{
							$('[name="deleteImage"]', $modal).prop("checked", true);
							$("#thumbnail", $modal).html("");
						});
						for (var i = 1; i < 6; i++)
						{
							var key = "attr_customfield" + i;
							if (productsCustomFields[key] && productsCustomFields[key].type == 'date')
							{
								var _d = moment(response[key], 'YYYY-MM-DD');
								if (_d.isValid())
								{
									$('.' + key, $modal).datepicker('update', _d.toDate());
								}
								else
								{
									$('.' + key, $modal).datepicker('update', '');
								}
							}
							else
							{
								$('.' + key, $modal).val(response[key]);
							}
						}

						for (var i = 1; i < 6; i++)
						{
							var keyLabel = "attr_label" + i;
							var keyQty = "attrfloat_qty" + i;
							var keyPrice = "attrcurrency_price" + i;
							$('.' + keyLabel, $modal).val(response[keyLabel]);
							if (response[keyLabel] == '')
							{
								$('.' + keyQty, $modal).val('');
								$('.' + keyPrice, $modal).val('');
							}
							else
							{
								$('.' + keyQty, $modal).val(response[keyQty]);
								$('.' + keyPrice, $modal).val(response[keyPrice]);
							}
						}
					});

				});
			}
			else
			{
				$("#thumbnail-buttons", $modal).append(getButton("Add"));
				$('#add-edit-products-form .panel-froala').froalaEditor('html.set', "");

				$('.choseFile', $modal).on("click", function()
				{
					$('#imageUpload', $modal).val("");
					$('#imageUpload', $modal).click();
				});
			}

			$('.cust-fld-date', $modal).datepicker({
				autoclose : true,
				format : {
					toDisplay : function(date, format, language)
					{
						return moment(date).format(dateFormatPattern);
					},
					toValue : function(date, format, language)
					{
						return moment(date).format(dateFormatPattern);
					}
				}
			});
			$modal.modal({
				width : '660px'
			});
		}
		else if (libraryType == 'text')
		{
			// remove ck
			// if (CKEDITOR.instances.textBlockTextarea)
			// {
			// CKEDITOR.instances.textBlockTextarea.destroy();
			// }
			// CKEDITOR.inline(textBlockTextarea, {
			// customConfig :
			// '/application/assets/plugins/ckeditor/custom/config-proposalFreeText.jsp'
			// });

			if ($('#add-edit-text-form .panel-froala').data('froala.editor'))
			{
				$('#add-edit-text-form .panel-froala').froalaEditor('destroy');
			}
			$('#add-edit-text-form .panel-froala').on('froalaEditor.initialized', function(ev, editor)
			{
				customizeFroalaEditor(ev, editor)
			}).froalaEditor({
				enter: $.FroalaEditor.ENTER_P,
				fontSize: ['8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '22', '24', '26', '28', '30', '60', '96'],
				fontSizeDefaultSelection: '14',
				toolbarSticky: true,
				key : 'DLAHYKAJOEc1HQDUH=='
			});

			$('input[type="checkbox"]', $modal).iCheck({
				checkboxClass : 'icheckbox_square-orange'
			});

			if (!newItem)
			{
				$.get('/api/library/add-edit/' + libraryType + '/' + $item.data('element-id'), function(response)
				{
					$('[name="elementId"]', $modal).val(response.element_id);
					$('[name="folder"]', $modal).val(response.category_id);
					$('[name="assetName"]', $modal).val(response.attr_headline);
					// remove ck
					// CKEDITOR.instances.textBlockTextarea.setData(response.attrlong_textblock);

					$('#add-edit-text-form .panel-froala').froalaEditor('html.set', response.attrlong_textblock);

					$('[name="lockContent"]', $modal).iCheck(response.attrcheck_lock_content ? 'check' : 'uncheck');
				});
			}
			else
			{
				// remove ck
				// CKEDITOR.instances.textBlockTextarea.setData("");
				$('#add-edit-text-form .panel-froala').froalaEditor('html.set', '');
			}

			$modal.modal({
				width : '860px'
			});
		}
		else if (libraryType == 'pdf')
		{
			if (!newItem)
			{
				if ($('.pdfDropzone').length > 0 && $('.pdfDropzone.dz-clickable').length == 0)
				{
					pdfdropZone.enable();
					pdfdropZone.options.autoProcessQueue = false;
				}
				$.get('/api/library/add-edit/' + libraryType + '/' + $item.data('element-id'), function(response)
				{
					$('[name="elementId"]', $modal).val(response.element_id);
					$('[name="folder"]', $modal).val(response.category_id);
					$('[name="assetName"]', $modal).val(response.attr_headline);
					$('#uploadPDFDrop', $modal).hide();
					$('.pdfNameGroup', $modal).show();
					pdfEdit = true;
				});
			}
			else
			{
				pdfEdit = false;
				if ($('.pdfDropzone').length > 0 && $('.pdfDropzone.dz-clickable').length == 0)
				{
					pdfdropZone.enable();
					pdfdropZone.options.autoProcessQueue = false;
				}
				$('.pdfNameGroup', $modal).hide();
				$('#uploadPDFDrop', $modal).show();
			}

			$modal.modal({
				width : '860px'
			});
		}
		else if (libraryType == 'images')
		{
			// remove ck
			// if (CKEDITOR.instances.imageLibDescTextArea)
			// {
			// CKEDITOR.instances.imageLibDescTextArea.destroy();
			// }
			// CKEDITOR.inline('imageLibDescTextArea');


			// FROALA EDITOR
			if ($('#add-edit-images-form .panel-froala').data('froala.editor'))
			{
				$('#add-edit-images-form .panel-froala').froalaEditor('destroy');
			}
			$('#add-edit-images-form .panel-froala').on('froalaEditor.initialized', function(ev, editor)
			{
				customizeFroalaEditor(ev, editor)
			}).froalaEditor({
				enter: $.FroalaEditor.ENTER_P,
				key : 'DLAHYKAJOEc1HQDUH=='
			});

			if (usingIEorEdge())
			{
				$("#image-alert-browser").show();
			}

			$('#thumbnailImage, [name="image"]', $modal).css({
				display : 'none'
			});

			$('[name="displayTitle"]').iCheck({
				checkboxClass : 'icheckbox_square-orange',
				radioClass : 'iradio_square-orange'
			});


			if (!newItem)
			{
				$('#chooseImage', $modal).html('<i class="fa fa-picture-o"></i> Change').removeClass("btn-primary").addClass("btn-warning");

				$('#cropImage', $modal).show();
				$.get('/api/library/add-edit/' + libraryType + '/' + $item.data('element-id'), function(response)
				{
					$('[name="elementId"]', $modal).val(response.element_id);
					$('[name="folder"]', $modal).val(response.category_id);
					$('[name="assetName"]', $modal).val(response.attr_headline);
					// remove ck
					// CKEDITOR.instances.imageLibDescTextArea.setData(response.attrlong_description);

					$('#add-edit-images-form .panel-froala').froalaEditor('html.set', response.attrlong_description);

					$('[name="displayTitle"]', $modal).iCheck(response.attrcheck_displayname ? 'check' : 'uncheck');


					var imgComponents = response.attrfile_image.split('/');
					// imgComponents.splice(-1, 0, '_thumb');
					if (imgComponents[4] != undefined)
					{
						if (response.image_width > 768 || response.image_height > 768)
						{
							$('#thumbnailImage', $modal).attr('src', '/stores' + imgComponents.join('/') + '?image-size=768x768-fit').css({
								display : 'block'
							});
						}
						else
						{
							$('#thumbnailImage', $modal).attr('src', '/stores' + imgComponents.join('/')).css({
								display : 'block'
							});
						}


						$('#save-changes').prop("disabled", false);
						$("#image-container", $modal).show();
						$('#infoContainer', $modal).show();
						$('span#fileName', $modal).text(response.image_filename);
						$('span#fileType', $modal).text(response.image_type);
						$('span#uploadedOn', $modal).text(response.image_uploaded);
						$('span#fileSize', $modal).text(response.image_size);
						$('span#dimensions', $modal).text(response.image_width + " x " + response.image_height + " px");
					}
					else
					{
						$('#thumbnailImage', $modal).attr('src', '/application/assets/images/images_placeholder.png').css({
							display : 'block'
						});

						$('#infoContainer', $modal).hide();
					}


				});
			}
			else
			{
				// remove ck
				// CKEDITOR.instances.imageLibDescTextArea.setData("");

				$('#add-edit-images-form .panel-froala').froalaEditor('html.set', '');

				$('#chooseImage', $modal).html('<i class="fa fa-cloud-upload"></i> Upload').removeClass("btn-warning").addClass("btn-primary");

				$('#infoContainer', $modal).hide();
				$('[name="image"]', $modal).css({
					display : 'block'
				});
			}


			$modal.modal({
				width : '880px'
			});
		}
		else if (libraryType == 'videos')
		{
			if (!newItem)
			{
				$.get('/api/library/add-edit/' + libraryType + '/' + $item.data('element-id'), function(response)
				{
					$('[name="elementId"]', $modal).val(response.element_id);
					$('[name="folder"]', $modal).val(response.category_id);
					$('[name="assetName"]', $modal).val(response.attr_headline);
					$('[name="videoLink"]', $modal).val(response.attr_videolink);
					$('[name="caption"]', $modal).val(response.attr_caption);
				});
			}

			$modal.modal({
				width : '660px'
			});
		}
		else if (libraryType == 'spreadsheet')
		{
			spreadsheet_add_edit_validator.resetForm();
			libraryTypeGlobal = libraryType;
			$('form select[name="folder"] option:eq(0)', $modal).prop("selected", true);
			$('.subFolderLabel', $modal).addClass('hidden');
			$('.subFolderSelect', $modal).addClass('hidden');
			$('#subFolderNameId', $modal).addClass('hidden');
			if (!newItem)
			{
				$.get('/api/library/add-edit/' + libraryType + '/' + $item.data('element-id'), function(response)
				{
					$('[name="elementId"]', $modal).val(response.element_id);

					$('[name="folder"]', $modal).val(response.category_id);

					$.get('/api/library/add-edit/' + libraryTypeGlobal + '/subfolder/' + response.category_id, function(response1)
					{
						fetchSubFolderSelectOptions($modal, response1);
						$('.subFolderLabel', $modal).removeClass('hidden');
						$('.subFolderSelect', $modal).removeClass('hidden');
						if (response1 != "")
						{
							$('[name="subFolder"]', $modal).val(response.attr_subfolder);
						}
						$('[name="spreadsheetName"]', $modal).val(response.attr_headline);
					});
					initKendoSpreadsheet($modal, response.attrtext_json);
				});
			}
			else
			{
				initKendoSpreadsheet($modal);
			}
			
			$modal.modal({
				width : '80%',
			});
			$modal.one('shown.bs.modal', function()
			{
				$('.spreadsheet', $modal).data('kendoSpreadsheet').resize();
			});
		}
		else
		{
			alert("unexpected libraryType : " + libraryType);
		}


		$modal.modal('show');
		setTimeout(function()
		{
			preCheckUpdate($modal);
			bindCloseButtonAction($modal);
		}, 1000);
	});

	imagePath = undefined; // reset the image crop path
}

function editLibraryFolderAction(e)
{
	var $tr = $(this).closest('tr');
	var libraryType = $tr.data('library-type').toLowerCase();
	var $modal = $('#content-library-folder-edit-modal');
	$('[name="libraryType"]', $modal).val(libraryType);
	$('[name="folderId"]', $modal).val($tr.data('element-id'));
	$('[name="folderName"]', $modal).val($tr.data('folder-name'));

	$modal.modal({
		width : '50%'
	});
	$modal.modal('show');
}

function deleteMultipleLibraryItemsAction(e) {
	if (selectButtonsList.length > 0) {
		$('#imageMultiDeleteModal').modal('show');
	}
}

function processimageMultiDelete() {
	$('#imageMultiDeleteModal').modal('hide');
	var library = 'images';
	$.ajax({
		type : 'post',
		url : '/api/library/deleteMulti',
		data : {
			'element-id-list' : selectButtonsList,
			'library-type' : library
		}
	}).done(function(response)
	{
		if (response.success)
		{
			folder = $('#search-folder').val();
			$('#images-library-add-edit-modal').modal('hide');
			$('#content-library-modal').load('/api/library/manage', {
				libraryType : library
			}, function()
			{
				$('.select-library-item-action').html('<i class="fa fa-plus-square-o" aria-hidden="true"></i>\n' +
					'\t\t\t&nbsp;Select Multiple');
				$('.select-library-item-action').removeClass('active');
				$('.select-library-item-action').addClass('btn-primary').removeClass('btn-warning');
				$('.delete-multiple-library-item-action').html('<i class="fa fa-plus-square-o" aria-hidden="true"></i>\n' +
					'\t\t\t&nbsp;Add Folder');
				$('.delete-multiple-library-item-action').addClass('btn-primary').removeClass('btn-danger');
				$('.delete-multiple-library-item-action').addClass('add-library-folder-action').removeClass('delete-multiple-library-item-action');
				selectButtonsList = new Array();
				reloadImages();
			});
			swal({
				title : 'Success!',
				type : 'success',
				text : response.message,
				html : true
			});
		}
		else
		{
			swal({
				title : 'Error',
				type : 'error',
				text : response.message,
				html : true
			})
		}
	});
}

function selectLibraryItemsButtonAction(e) {


	var $item = $(this).closest('li');
	var libraryType = $item.data('library-type');
	var elementID = $item.data('element-id');

	if ($(this).hasClass('active-delete-item')) {
		$(this).removeClass('active-delete-item');
		$(this).find("div").css("box-shadow", "");
		selectButtons--;
		selectButtonsList = jQuery.grep(selectButtonsList, function(value) {
			return value != elementID;
		});
	} else {
		$(this).addClass('active-delete-item');
		selectButtons++;
		$(this).find("div").css("box-shadow", "inset 0 0 0 4px rgba(255, 7, 7, 0.31)");
		selectButtonsList.push(elementID);
	}

	if (selectButtons > 0) {
		$('.delete-multiple-library-item-action').html('<i class="fa fa-trash" aria-hidden="true"></i>\n' +
			'\t\t\t&nbsp;Delete ' + selectButtons);
	} else {
		$('.delete-multiple-library-item-action').html('<i class="fa fa-trash" aria-hidden="true"></i>\n' +
			'\t\t\t&nbsp;Delete');
	}
}

function selectLibraryItemsAction(e) {
	var libraryType = $(this).data('library-type').toLowerCase();
	selectButtons = 0;

	$('.library-item-cell').each(function(i, obj) {
		if ($(obj).hasClass("edit-library-item-action")) {
			$(obj).addClass('select-library-item-button').removeClass('edit-library-item-action');
		} else {
			$(obj).addClass('edit-library-item-action').removeClass('select-library-item-button');
		}
	});

	if ($(this).hasClass("active")) {
		$(this).html('<i class="fa fa-plus-square-o" aria-hidden="true"></i>\n' +
			'\t\t\t&nbsp;Select Multiple');
		$(this).removeClass('active');
		$(this).addClass('btn-primary').removeClass('btn-warning');
		$('.delete-multiple-library-item-action').html('<i class="fa fa-plus-square-o" aria-hidden="true"></i>\n' +
			'\t\t\t&nbsp;Add Folder');
		$('.delete-multiple-library-item-action').addClass('btn-primary').removeClass('btn-danger');
		$('.delete-multiple-library-item-action').addClass('add-library-folder-action').removeClass('delete-multiple-library-item-action');
		selectButtonsList = new Array();
		$('.library-item-cell').each(function(i, obj) {
			if ($(obj).hasClass('active-delete-item')) {
				$(obj).removeClass('active-delete-item');
				$(obj).find("div").css("box-shadow", "");
			}
		});
	} else {
		$(this).html('<i class="fa fa-plus-square-o" aria-hidden="true"></i>\n' +
			'\t\t\t&nbsp;Cancel');
		$(this).addClass('active');
		$(this).addClass('btn-warning').removeClass('btn-primary');
		$('.add-library-folder-action').html('<i class="fa fa-trash" aria-hidden="true"></i>\n' +
			'\t\t\t&nbsp;Delete');
		$('.add-library-folder-action').addClass('btn-danger').removeClass('btn-primary');
		$('.add-library-folder-action').addClass('delete-multiple-library-item-action').removeClass('add-library-folder-action');
	}


}

function addLibraryFolderAction(e)
{
	var libraryType = $(this).data('library-type').toLowerCase();
	var $modal = $('#content-library-folder-edit-modal');
	$('[name="libraryType"]', $modal).val(libraryType);
	$('.modal-title', $modal).text("Add Folder");
	$('[name="folderName"]', $modal).val("");

	$modal.modal({
		width : '50%'
	});
	$modal.modal('show');
}

function deleteLibraryFolderAction(e)
{
	var $tr = $(this).closest('tr');
	var libraryType = $tr.data('library-type').toLowerCase();
	var $modal = $('#content-library-folder-delete-modal');
	var folderId = $tr.data('element-id');
	$('[name="libraryType"]', $modal).val(libraryType);
	$('[name="folderId"]', $modal).val(folderId);
	$('#folderToBeDeleted', $modal).val($tr.data('folder-name'));

	$.get('/api/library/add-edit/' + libraryType, function(response)
	{
		var $select = $('form select[name="transferTo"]', $modal);
		$select.empty();
		for (var i = 0; i < response.length; i++)
		{
			if (response[i].category_id != folderId)
			{
				$select.append($('<option>').val(response[i].category_id).text(response[i].attr_categoryname));
			}
		}

		$modal.modal({
			width : '50%'
		});
		$modal.modal('show');
	});
}

function fetchFolderSelectOptions($modal, response)
{
	var $select = $('form select[name="folder"]', $modal);

	$select.empty();
	$select.append($('<option>').prop('disabled', true).prop('selected', true).prop('value', null));
	$select.append($('<option>').val(0).text("Add new folder..."));
	for (var i = 0; i < response.length; i++)
	{
		$select.append($('<option>').val(response[i].category_id).text(response[i].attr_categoryname));
	}
}

function fetchSubFolderSelectOptions($modal, response)
{
	var $select = $('form select[name="subFolder"]', $modal);

	$select.empty();
	$select.append($('<option>').prop('selected', true).prop('value', 0).text("No Sub Folder"));
	$select.append($('<option>').val(0).text("Add new Sub Folder..."));
	for (var i = 0; i < response.length; i++)
	{
		if (response[i] != "")
		{
			$select.append($('<option>').val(response[i]).text(response[i]));
		}
	}
}

function initTable($table)
{
	var table = $table.DataTable({
		"orderCellsTop" : true,
		"searching" : true,
		"dom" : '<"top">rt<"bottom"<"row"<"col-sm-6"i><"col-sm-6" p l>>><"clear">',
		"lengthMenu" : [ [ 10, 25, 50, 100, -1 ], [ 10, 25, 50, 100, "All" ] ],
		"stateSave" : true, // same session state shared by product, image and
		// videos
		"scrollY" : $(window).height() * 0.5,
		columnDefs : [ {
			"width" : "150px",
			"targets" : -1
		} ]
	});

	table.columns().search('');
	table.draw();

	$('#content-library-table_length select').addClass("form-control input-sm");
	$('#content-library-folder-table_length select').addClass("form-control input-sm");

	table.columns().every(function()
	{
		var that = this;

		var colIndex = $(this.header()).index();
		var $th = $(this.header()).closest('tr').next().children().eq(colIndex);


		$('input', $th).on('keyup change', function()
		{
			if (that.search() !== this.value)
			{
				that.search(this.value).draw();

			}
		});

		$('select', $th).on('keyup change', function()
		{
			var selectedVal = $(":selected", this).val();
			if (that.search() !== selectedVal)
			{
				that.search(selectedVal).draw();
			}
		});
	});
}

function initDynaTable($list)
{

	if ($list.length > 0)
	{
		bulkAddImages();
	}

	$('#content-library-table_length select').addClass("form-control input-sm");
	$('#content-library-folder-table_length select').addClass("form-control input-sm");

	// Function that renders the list items from our records
	function ulWriter(rowIndex, record, columns, cellWriter)
	{
		if ($('.select-library-item-action.active').is(":visible")) {
			if(jQuery.inArray(record.elementID, selectButtonsList) !== -1) {
				var thumb = record.thumbnail;
					thumb = thumb.replace('<a href="javascript:;" class="library-item-cell edit-library-item-action">', '<a href="javascript:;" class="library-item-cell select-library-item-button active-delete-item">');
					thumb = thumb.replace('?image-size=156x156-fit\')"></div>','?image-size=156x156-fit\'); box-shadow: rgba(255, 7, 7, 0.31) 0px 0px 0px 4px inset;"></div>');
				return '<li data-element-id="' + record.elementID + '" data-library-type="' + record.libraryType + '"><div class="image-thumbnail" data-placement="bottom" data-toggle="tooltip" title="' + record.label + '">' + thumb + '</div><div class="caption">' + record.label + '</div></li>';
			} else {
				var thumb = record.thumbnail;
				thumb = thumb.replace('<a href="javascript:;" class="library-item-cell edit-library-item-action">', '<a href="javascript:;" class="library-item-cell select-library-item-button">');
				thumb = thumb.replace('?image-size=156x156-fit\')"></div>','?image-size=156x156-fit\')"></div>');
				return '<li data-element-id="' + record.elementID + '" data-library-type="' + record.libraryType + '"><div class="image-thumbnail" data-placement="bottom" data-toggle="tooltip" title="' + record.label + '">' + thumb + '</div><div class="caption">' + record.label + '</div></li>';
			}
		} else {
			return '<li data-element-id="' + record.elementID + '" data-library-type="' + record.libraryType + '"><div class="image-thumbnail" data-placement="bottom" data-toggle="tooltip" title="' + record.label + '">' + record.thumbnail + '</div><div class="caption">' + record.label + '</div></li>';
		}

	}

	// Function that creates our records from the DOM when the page is loaded
	function ulReader(index, li, record)
	{
		var $li = $(li), $caption = $li.find('.caption');
		record.thumbnail = $li.find('.image-thumbnail').html();
		record.elementID = $li.data("element-id");
		record.libraryType = $li.data("library-type");
		record.imageURL = $li.find('img').prop('src');
		record.caption = $caption.html();
		record.label = $caption.find('h3').text();
		record.folder = $li.find('.image-container').data("category-name");
	}


	var dyna = $list

	.bind('dynatable:init', function(e, dynatable)
	{
		dynatable.queries.functions['folder'] = function(record, queryValue)
		{

			if (record.folder == queryValue)
			{
				return true;
			}
			else
			{
				return false;
			}

		};
	})

	.dynatable({
		table : {
			bodyRowSelector : 'li'
		},
		dataset : {
			perPageDefault : 25,
			perPageOptions : [ 10, 25, 50, 100 ]
		},
		writers : {
			_rowWriter : ulWriter
		},
		readers : {
			_rowReader : ulReader
		},
		params : {
			records : 'images'
		},
		inputs : {
			queryEvent : 'keyup',
			perPagePlacement : 'after',
			searchTarget : $('#search-row')
		}
	}).data('dynatable');

	$('#search-folder').change(function()
	{
		var value = $(this).val();
		if (value === "All folders")
		{
			dyna.queries.remove("folder");
		}
		else
		{
			dyna.queries.add("folder", value);
		}
		dyna.process();
	});


}

function getButton(button)
{
	var cls = "";
	var icon = "";
	var type = "primary";
	switch (button)
	{
		case "Add":
			type = "success";
		case "Replace":
			cls = "choseFile";
			icon = "cloud-upload";
			break;
		case "Delete":
			cls = "deleteFile";
			icon = "trash"
			type = "danger";
	}
	var btn = $("<button type='button' class='btn btn-" + type + " " + cls + "'>").append($("<i class='fa fa-" + icon + "'>"), " ", $("<span>").text(button));
	return btn;
}

function bulkAddImages()
{
	var myDropzone = new Dropzone("#uploadImages", {
		url : '/api/library/add/images/bulk',
		maxFilesize : maxSize, // MB
		paramName : "file",
		acceptedFiles : "image/jpeg,image/png,image/gif",
		createImageThumbnails : true,
		init : function()
		{

			this.on("sending", function(file)
			{

			});


			this.on("complete", function(file, response)
			{
				if (file.accepted)
				{
					myDropzone.removeFile(file);
				}
			});

			this.on("queuecomplete", function()
			{

				if (errorImages.length > 0)
				{
					var errorMessage = "Couldn't upload the following images <br/> Please try again.";
					errorMessage += "<ul id='errorImages'>"
					for (i = 0; i < errorImages.length; i++)
					{
						errorMessage += "<li>" + errorImages[i].name + "</li>";
					}
					errorMessage += "</ul>"
					swal({
						html : true,
						title : '<i>Warning</i>',
						text : errorMessage
					});
					errorImages = [];
				}
				else if (errorImagesSize.length > 0)
				{
					var errorMessage = "Couldn't upload the following images <br/> Max file size is " + maxSize + "MB.";
					errorMessage += "<ul id='errorImages'>"
					for (i = 0; i < errorImagesSize.length; i++)
					{
						errorMessage += "<li>" + errorImagesSize[i].name + "</li>";
					}
					errorMessage += "</ul>"
					swal({
						html : true,
						title : '<i>Warning</i>',
						text : errorMessage
					});
					errorImagesSize = [];
				}

				$('#content-library-modal').load('/api/library/manage', {
					libraryType : 'images'
				}, function()
				{
					reloadImages();
				});
			});

			this.on('error', function(file)
			{
				var fileType = file.type
				if ((file.size / 1048576) > maxSize)
				{
					errorImagesSize.push(file);
				}
				else if ((fileType.indexOf("image") == -1))
				{
					swal('File must be an image', 'Only images are allowed to be uploaded into the Image Library', 'error');
					this.removeFile(file);
				}
				console.log("ERROR");
			});
		},
		success : function(file, data)
		{
			if (data.success)
			{
				myDropzone.processQueue.bind(myDropzone);
			}
			else
			{
				errorImages.push(file);
				console.log("ERROR");
			}
		}
	});
}

function reloadImages()
{
	initDynaTable($('#images-list', $('#content-library-modal')));
	initTable($('#content-library-folder-table', $('#content-library-modal')));
	$('a[href="#folders"]').on('shown.bs.tab', function()
	{
		$('#content-library-folder-table', $('#content-library-modal')).DataTable().draw();
	});
	if (folder != null)
	{
		$('#search-folder').val(folder).trigger('change');
	}
}

function usingIEorEdge()
{
	var ua = window.navigator.userAgent;

	var msie = ua.indexOf('MSIE ');
	if (msie > 0)
	{
		// IE 10 or older => return version number
		return true;
	}

	var trident = ua.indexOf('Trident/');
	if (trident > 0)
	{
		// IE 11 => return version number
		var rv = ua.indexOf('rv:');
		return true;
	}

	var edge = ua.indexOf('Edge/');
	if (edge > 0)
	{
		// Edge (IE 12+) => return version number
		return true;
	}

	// other browser
	return false;
}

function importProductsAction(folder)
{
	var $modal = $('#products-library-import-modal');
	$('#csvUpload-btn', $modal).off('click').on("click", function()
	{
		$('#csvUpload', $modal).val("");
		$('#csvUpload', $modal).click();
	});
	$('#csvUpload', $modal).off('change').on('change', function()
	{
		var name = $(this).val();
		if (name == '')
		{
			$('#csvUpload-filename').text('');
			return;
		}
		var idx = name.lastIndexOf('\\');
		if (idx == -1)
		{
			idx = name.lastIndexOf('/');
		}
		$('#csvUpload-filename').text(idx == -1 ? '' : name.substring(idx + 1));
	})
	$('form', $modal).clearForm().find('input').change();
	$('[name="categoryId"]', $modal).val(folder ? $(folder).closest('tr').attr('data-element-id') : '');
	$('[name=deleteAll]', $modal).iCheck({
		checkboxClass : 'icheckbox_square-orange',
		radioClass : 'iradio_square-orange'
	});
	$.ajax({
		url : '/api/process/status/get',
		type : 'post',
		data : {
			processor : "au.corporateinteractive.qcloud.proposalbuilder.service.MasterProductsImporter"
		}
	}).done(function(data)
	{
		if (data == '')
		{
			$modal.removeClass("importing")
		}
		else
		{
			$modal.addClass("importing");
			startProductImportStatusCheck();
		}
		$modal.modal({
			width : '660px'
		});
	})
}

function exportProductsAction(folder, isSample)
{
	var endpoint = '/api/library/export/products';
	startLoadingForeground();
	$.fileDownload(endpoint, {
		data : {
			id : folder ? $(folder).closest('tr').attr('data-element-id') : '',
			row : isSample ? "1" : ""
		},
		successCallback : function(url)
		{
			stopLoadingForeground();
		},
		failCallback : function(html, url)
		{
			stopLoadingForeground();
			alert("Oops! Something wrong in exporting");
		}
	});
}

function startProductImportStatusCheck()
{
	var $modal = $('#products-library-import-modal');
	var check = function()
	{
		if (!$modal.is(':visible'))
		{
			return;
		}
		$.ajax({
			url : '/api/process/status/get',
			type : 'post',
			data : {
				processor : "au.corporateinteractive.qcloud.proposalbuilder.service.MasterProductsImporter"
			}
		}).done(function(data)
		{
			if (data == '')
			{
				$('#products-library-import-modal').modal('hide');
				$('#content-library-modal').load('/api/library/manage', {
					libraryType : 'products'
				}, function()
				{
					initTable($('#content-library-table', $('#content-library-modal')));
				});
			}
			else if (!data.ended)
			{
				$('.progress-bar', $modal).css('width', (100.0 * data.processed / data.total) + '%');
				$('.progress-bar-text', $modal).text(data.processed + " / " + data.total);
				$('.stage-text', $modal).text(data.stage);

				$modal.addClass("importing");
				setTimeout(check, 1000);
			}
			else
			{
				$('.progress-bar', $modal).css('width', (100.0 * data.processed / data.total) + '%');
				$('.progress-bar-text', $modal).text(data.processed + " / " + data.total);
				swal({
					title : data.success ? "Success" : "Error",
					type : data.success ? "success" : "warning",
					text : data.message,
					closeOnConfirm : true,
					html : true,
				}, function()
				{
					$.ajax({
						url : '/api/process/status/remove',
						type : 'post',
						data : {
							processor : "au.corporateinteractive.qcloud.proposalbuilder.service.MasterProductsImporter"
						}
					});
					if (data.success)
					{
						$modal.modal('hide');
						$('#content-library-modal').load('/api/library/manage', {
							libraryType : 'products'
						}, function()
						{
							initTable($('#content-library-table', $('#content-library-modal')));
						});
					}
					else
					{
						$modal.removeClass("importing");
					}
				});
			}
		})
	};
	setTimeout(check, 1000);
}
