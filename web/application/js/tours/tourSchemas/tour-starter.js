var tourStarter = {
		id: "tourStarter",
		i18n: {
			nextBtn: "Ok"
		},
		steps: [
			{
				title: "Welcome to Quote Cloud",
				content: "If you have any questions or are in need of a quick introduction click Guide Me",
				target: ".guideMeStarter",
				placement: "bottom",
	        	nextOnTargetClick: true,
				showCloseButton: false,
				yOffset: 5
			}
		]
}