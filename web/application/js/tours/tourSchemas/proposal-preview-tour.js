var templatePreviewTour = {
		id: "templatePreviewTour",
		steps: [
	        {
	        	title: "Previewing your proposal",
	        	content: "Click the arrow next to the edit button on the proposal you would like to preview, then click the Preview button",
	        	target: ".DTFC_RightBodyWrapper",
	        	placement: "left",
	        	showNextButton: false,
	        	xOffset: 12,
	        	yOffset: -12,
	        	nextOnTargetClick: true,
                multipage: true,
                onNext: function() {
                    waitForModalElement('.guide-template-modal');
                }
	        }
		]
}