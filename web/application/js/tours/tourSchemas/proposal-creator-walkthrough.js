
var creatorStart = introJs();
var creatorStep2;
var creatorStep2Basic;
var creatorStep3;
var pageNumber = 1;
var tutorialActive = false;


var waitForElementVisible = function(element, callback) {
    var checkExist = setInterval(function() {
        $element = $(element);
        if ($element.is(':visible')) {
            clearInterval(checkExist);
            if (typeof callback == "function") {
                callback();
            }
        }
    }, 100);
};

$(document).ready(function(){

    $(document).on('click', '.next.finish', function() {
        if (tutorialActive) {
            if (tab4Shown) {
                tutorialActive = false;
                creatorStep2.exit();
            } else {
                tutorialActive = false;
                creatorStep2Basic.exit();
            }
        }
    });

    $(document).on('click', '#endTutorial', function() {
        if (tutorialActive) {
            if (creatorStep2Basic != null) {
                tutorialActive = false;
                creatorStep2Basic.exit();
            } else if (creatorStep2 != null) {
                tutorialActive = false;
                creatorStep2.exit();
            } else if (creatorStart != null) {
                tutorialActive = false;
                creatorStart.exit();
            } else {
                tutorialActive = false;
                creatorStep3.exit();
            }
            $.ajax({
                type : 'post',
                url : '/api/tutorial/endfirst',
            }).done(function()
            {
                location.reload();
            });
        }

    });

    $(document).on('click', '#openNewProposal', function() {

        if (tutorialFirstLoad == "true") {
            location.reload();
        }
    });

    $(document).on('click', '#closeBtn', function() {

        if (tutorialFirstLoad == "true") {
            $.ajax({
                type : 'post',
                url : '/api/tutorial/endfirst',
            }).done(function(success)
            {
                if (success) {
                    //show sweet alert
                }
                location.reload();
            });
        }
    });

    $(document).on('click', '.createProposalAction', function() {
        if (tutorialActive) {
            if (tutorialFirstLoad == "true") {
                creatorStart.exit();
                loadTutorialWhenReady();
            }
        }
    });

    $(document).on('click', '.nextButton', function() {

        if (tutorialActive) {
        if (tutorialFirstLoad == "true") {
            if (pageNumber == 1) {
                setTimeout(function () {
                    if (!tab4Shown) {
                        tutorialActive = false;
                        creatorStep2Basic.exit();
                        pageNumber++
                        creatorStep2Basic.goToStep(5).start();
                        tutorialActive = true;
                        initStopButton();
                    } else {
                        tutorialActive = false;
                        creatorStep2.exit();
                        pageNumber++
                        creatorStep2.goToStep(5).start();
                        tutorialActive = true;
                        initStopButton();
                    }
                }, 500);
            } else if (pageNumber == 2) {
                setTimeout(function () {
                    if (!tab4Shown) {
                        tutorialActive = false;
                        creatorStep2Basic.exit();
                        pageNumber++
                        creatorStep2Basic.goToStep(12).start();
                        tutorialActive = true;
                        initStopButton();
                    } else {
                        tutorialActive = false;
                        creatorStep2.exit();
                        pageNumber++
                        creatorStep2.goToStep(12).start();
                        tutorialActive = true;
                        initStopButton();
                    }
                }, 500);
            } else if (pageNumber == 3) {
                setTimeout(function () {
                    if (!tab4Shown) {
                        tutorialActive = false;
                        creatorStep2Basic.exit();
                        pageNumber++
                        creatorStep2Basic.goToStep(14).start();
                        tutorialActive = true;
                        initStopButton();
                    } else {
                        tutorialActive = false;
                        creatorStep2.exit();
                        pageNumber++
                        creatorStep2.goToStep(14).start();
                        tutorialActive = true;
                        initStopButton();
                    }
                }, 500);
            } else if (pageNumber == 4) {
                setTimeout(function () {
                    if (tab4Shown) {
                        tutorialActive = false;
                        creatorStep2.exit();
                    }
                }, 500);
            }
        }
        }
    });


    $(document).on('keyup', '.step1 #proposalTitle', function(e) {
        if (tutorialActive) {
            if (tutorialFirstLoad == "true") {
                if ($('.step1 #proposalTitle').val().length > 3) {
                    $('.introjs-tooltipbuttons').show();
                    $('.introjs-prevbutton').hide();
                }
            }
        }
    });


    $(document).on('keyup', '.step4 #contactEmail', function(e) {
        if (tutorialActive) {
            if (tutorialFirstLoad == "true") {
                if ($('.step4 #contactEmail').val().toUpperCase() == userEmail.toUpperCase()) {
                    if (creatorStep2Basic != null) {
                        tutorialActive = false;
                        $('.introjs-nextbutton').click();
                        tutorialActive = true;
                        initStopButton();
                    } else {
                        tutorialActive = false;
                        $('.introjs-nextbutton').click();
                        tutorialActive = true;
                        initStopButton();
                    }
                }
            }
        }
    });


    $(document).on('keyup', '#contactName', function(e) {
        if (tutorialActive) {
            if (tutorialFirstLoad == "true") {
                if ($('#contactName').val().length > 0) {
                    $('.introjs-tooltipbuttons').show();
                    $('.introjs-prevbutton').hide();
                }
            }
        }
    });


    $(document).on('keyup', '#contactLastName', function(e) {
        if (tutorialActive) {
            if (tutorialFirstLoad == "true") {
                if ($('#contactLastName').val().length > 0) {
                    $('.introjs-tooltipbuttons').show();
                    $('.introjs-prevbutton').hide();
                }
            }
        }
    });


    $(document).on('keyup', '.step7 #companyName', function(e) {
        if (tutorialActive) {
            if (tutorialFirstLoad == "true") {
                if ($('.step7 #companyName').val().length > 0) {
                    $('.introjs-tooltipbuttons').show();
                    $('.introjs-prevbutton').hide();
                }
            }
        }
    });


});
function loadTutorialWhenReady() {
    if (tab4Shown != null) {
        setTimeout(function () {
            $('.modal-backdrop').hide();
            if (tab4Shown) {
                tutorialActive = true;
                waitForElementVisible('.step1', step2CreatorWalkthrough());
            } else {
                tutorialActive = true;
                waitForElementVisible('.step1', step2CreatorWalkthroughBasic());
            }
        }, 1000);
    } else {
        setTimeout(loadTutorialWhenReady, 250);
    }
}

function startCreatorWalkthrough(){
    creatorStart.setOptions({
        showBullets: false,
        showProgress: false,
        keyboardNavigation: false,
        showStepNumbers:true,
        exitOnEsc:false,
        exitOnOverlayClick:false,
        hidePrev:true,
        steps: [
            {
                intro: "<h1>Welcome to the QuoteCloud User Dashboard</h1><br>This welcome tutorial will guide you through creating your first proposal." +
                "<br><br>Follow the steps and type the example information where prompted." +
                    "<br><br>At the end of this tutorial there are more guided tours in the 'Guide Me' menu on this User Dashboard."
            },
            //1
            {
                element: '.createProposalAction',
                intro: "<h1>Create your first proposal</h1><br>Lets start by clicking the 'Create Proposal' button to load the creator.",
                position: 'bottom'
            }
        ]
    });


    creatorStart.onchange(function () {
        console.log(this._currentStep);
        if ((this._currentStep == 1)) {
            $('.introjs-tooltipbuttons').hide();
        }
    });
    tutorialActive = true;
    creatorStart.start();
    initStopButton();
}

function step2CreatorWalkthrough(){
    creatorStep2 = introJs('.modal-scrollable');

    creatorStep2.setOptions({
        showBullets: false,
        showProgress: true,
        keyboardNavigation: false,
        showStepNumbers:true,
        exitOnEsc:false,
        exitOnOverlayClick:false,
        hidePrev:true,
        steps: [
            {
                element: '.form-create-proposal',
                intro: "<h1>Create Proposal Wizard</h1><br>This wizard will appear every-time you need to make a new proposal, its job is to guide you through the steps to get essential information for a new sales proposal.",
                position: 'right'
            },
            {
                element: '.step1',
                intro: "<h1>Proposal Title</h1><br>This is where we name our proposal. Please type <b style='font-family: Courier New'>Simple Proposal</b> in this field.",
                position: 'right'
            },
            //1
            {
                element: '.step2',
                intro: "<h1>Colour Scheme</h1><br>Here we choose the colour scheme for our new sales proposal, this selection can be changed later but for now choose one you like.",
                position: 'right'
            },
            //2
            {
                element: '.step3',
                intro: "<h1>Cover Page</h1><br>Click on a cover page from the available list.<br><br>You can change the available selection of cover pages from the User Dashboard menu, but for now please choose from one of our samples.",
                position: 'right'
            },
            //3
            {
                element: '.nextButton',
                intro: "Click the <b>Next</b> button to continue.",
                position: 'top'
            },
            //4
            {
                element: '.step4',
                intro: "<h1>Customer Email Address</h1><br>Ok now lets type in the email address for your customer in this case type your own: " + userEmail,
                position: 'right'
            },
            //5
            {
                element: '.step5',
                intro: "<h1>Customer Name</h1><br>Here you type the customers first name, For example type: <b style='font-family: Courier New'>Mark</b>",
                position: 'right'
            },
            //6
            {
                element: '.step6',
                intro: "<h1>Customer Name</h1><br>Now lets type Marks last name: <b style='font-family: Courier New'>Smith</b>",
                position: 'right'
            },
            //7
            {
                element: '.step7',
                intro: "<h1>Company Name</h1><br>Lets enter the company that Mark represents, Type: <b style='font-family: Courier New'>NewCo Limited</b>",
                position: 'right'
            },
            //8
            {
                element: '.step8',
                intro: "<h1>Customer Address</h1><br>Here we type in the address for our customer, type <b style='font-family: Courier New'>224 George Street, Sydney, NSW, 2000, Australia</b>. You will notice QuoteCloud will lookup the address you type to suggest an accurate street address.",
                position: 'right'
            },
            //9
            {
                element: '.step9',
                intro: "<h1>Additional Recipient Email Addresses</h1><br>Here you can add the email addresses of anyone else that should receive a copy of the sales proposal.",
                position: 'right'
            },
            //10
            {
                element: '.nextButton',
                intro: "Click the <b>Next</b> button to continue.",
                position: 'top'
            },
            //11
            {
                element: '.templates-item.active[data-id="0"]',
                intro: "<h1>Proposal Templates</h1><br>Templates allow you to quickly create proposals using pre-made content.  If you do not have any templates at the moment you can either download some from the QuoteCloud sample template library, or save one of your own proposals as a template on the User Dashboard.",
                position: 'right'
            },
            //12
            {
                element: '.nextButton',
                intro: "Click the <b>Next</b> button to continue.",
                position: 'top'
            },
            //13
            {
                element: '#tab4',
                intro: "<h1>Custom Data</h1><br>As you have created custom data fields, you can now add your special information.",
                position: 'right'
            },
            //14
            {
                element: '.modal-footer .btn-success.nextButton',
                intro: "Click the <b>Create</b> button to generate your sales proposal and move to the Proposal Editor.",
                position: 'top'
            }
        ]
    });


    creatorStep2.onchange(function () {
        console.log(this._currentStep);
        console.log("-1-1-1-1-1-");
        $('.introjs-tooltipbuttons').hide();
        $('.introjs-prevbutton').hide();

        if ((this._currentStep == 2) || (this._currentStep == 3) || (this._currentStep == 9)
            || (this._currentStep == 10) || (this._currentStep == 12) || (this._currentStep == 14)) {
            $('.introjs-tooltipbuttons').show();
        }
        if (this._currentStep == 1) {
            $('input#proposalTitle').focus();
        }
        if (this._currentStep == 5) {
            $('input#contactEmail').focus();
        }
        if (this._currentStep == 6) {
            $('input#contactName').focus();
        }
        if (this._currentStep == 7) {
            $('input#contactLastName').focus();
        }
        if (this._currentStep == 8) {
            $('input#companyName').focus();
        }
        if (this._currentStep == 9) {
            $('input#contactAddress').focus();
        }
    });

    tutorialActive = true;
    creatorStep2.start();
    initStopButton();

}

function step2CreatorWalkthroughBasic(){
    creatorStep2Basic = introJs('.modal-scrollable');

    creatorStep2Basic.setOptions({
        showBullets: false,
        showProgress: true,
        keyboardNavigation: false,
        showStepNumbers:true,
        exitOnEsc:false,
        exitOnOverlayClick:false,
        hidePrev:true,
        steps: [
            {
                element: '.form-create-proposal',
                intro: "<h1>Create Proposal Wizard</h1><br>This wizard will appear every-time you need to make a new proposal, its job is to guide you through the steps to get essential information for a new sales proposal.",
                position: 'right'
            },
            {
                element: '.step1',
                intro: "<h1>Proposal Title</h1><br>This is where we name our proposal. Please type <b style='font-family: Courier New'>Simple Proposal</b> in this field.",
                position: 'right'
            },
            //1
            {
                element: '.step2',
                intro: "<h1>Colour Scheme</h1><br>Here we choose the colour scheme for our new sales proposal, this selection can be changed later but for now choose one you like.",
                position: 'right'
            },
            //2
            {
                element: '.step3',
                intro: "<h1>Cover Page</h1><br>Click on a cover page from the available list.<br><br>You can change the available selection of cover pages from the User Dashboard menu, but for now please choose from one of our samples.",
                position: 'right'
            },
            //3
            {
                element: '.nextButton',
                intro: "Click the <b>Next</b> button to continue.",
                position: 'top'
            },
            //4
            {
                element: '.step4',
                intro: "<h1>Customer Email Address</h1><br>Ok now lets type in the email address for your customer in this case type your own: " + userEmail,
                position: 'right'
            },
            //5
            {
                element: '.step5',
                intro: "<h1>Customer Name</h1><br>Here you type the customers first name, please type: <b style='font-family: Courier New'>Mark</b>",
                position: 'right'
            },
            //6
            {
                element: '.step6',
                intro: "<h1>Customer Name</h1><br>Now lets type Marks last name: <b style='font-family: Courier New'>Smith</b>",
                position: 'right'
            },
            //7
            {
                element: '.step7',
                intro: "<h1>Company Name</h1><br>Lets enter the company that Mark represents, Type: <b style='font-family: Courier New'>NewCo Limited</b>",
                position: 'right'
            },
            //8
            {
                element: '.step8',
                intro: "<h1>Customer Address</h1><br>Here we type in the address for our customer, type <b style='font-family: Courier New'>224 George Street, Sydney, NSW, 2000, Australia</b>. You will notice QuoteCloud will lookup the address you type to suggest an accurate street address.",
                position: 'right'
            },
            //9
            {
                element: '.step9',
                intro: "<h1>Additional Recipient Email Addresses</h1><br>Here you can add the email addresses of anyone else that should receive a copy of the sales proposal.",
                position: 'right'
            },
            //10
            {
                element: '.nextButton',
                intro: "Click the <b>Next</b> button to continue.",
                position: 'top'
            },
            //11
            {
                element: '.templates-item.active[data-id="0"]',
                intro: "<h1>Proposal Templates</h1><br>Templates allow you to quickly create proposals using pre-made content.  If you do not have any templates at the moment you can either download some from the QuoteCloud sample template library, or save one of your own proposals as a template on the User Dashboard.",
                position: 'right'
            },
            //12
            {
                element: '.modal-footer .btn-success.nextButton',
				intro: "Click the <b>Create</b> button to generate your sales proposal and move to the Proposal Editor.",
                position: 'top'
            }
        ]
    });


    creatorStep2Basic.onchange(function () {
        console.log(this._currentStep);
        $('.introjs-tooltipbuttons').hide();
        $('.introjs-prevbutton').hide();

        if ((this._currentStep == 2) || (this._currentStep == 3) || (this._currentStep == 9)
            || (this._currentStep == 10) || (this._currentStep == 12)) {
            $('.introjs-tooltipbuttons').show();
        }
        if (this._currentStep == 1) {
            $('input#proposalTitle').focus();
        }
        if (this._currentStep == 5) {
            $('input#contactEmail').focus();
        }
        if (this._currentStep == 6) {
            $('input#contactName').focus();
        }
        if (this._currentStep == 7) {
            $('input#contactLastName').focus();
        }
        if (this._currentStep == 8) {
            $('input#companyName').focus();
        }
        if (this._currentStep == 9) {
            $('input#contactAddress').focus();
        }
    });

    tutorialActive = true;
    creatorStep2Basic.start();
    initStopButton();
}

function finalCreatorWalkthrough(){
    creatorStep3 = introJs();

    creatorStep3.setOptions({
        showBullets: false,
        showProgress: true,
        keyboardNavigation: false,
        showStepNumbers:true,
        exitOnEsc:false,
        exitOnOverlayClick:false,
        steps: [
            {
                intro: "Now lets edit the proposal and add our first content."
            }
        ]
    });

    $('.introjs-skipbutton').show();
    $('.introjs-tooltipbuttons').show();

    tutorialActive = true;
    creatorStep3.start();
    initStopButton();
}

function initStopButton() {
    var overlay = $('.introjs-overlay');
    if (overlay != null) {
        $(overlay).append("<a id='endTutorial' href='#'>End Tutorial</a>");
    }
}