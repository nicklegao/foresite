var discountMargintour  = {
		id: "discountMargintour",
		steps: [
	        {
	        	title: "Configuration drop down",
	        	content: "Click to open the configuration drop down",
	        	target: ".guide-DM-config",
	        	placement: "bottom",
	        	multipage: true,
	        	nextOnTargetClick: true,
	        	onNext: function() {
	        		waitForElementVisible('.guide-DM-config-button', hopscotch.getCurrStepNum(), nextCallback);
	        	}
	        },
	        {
	        	title: "Discount & Margins",
	        	content: "Click to open the Pricing Discounts and Thresholds window",
	        	target: ".guide-DM-config-button",
	        	placement: "bottom",
	        	nextOnTargetClick: true,
	        	delay: 500,
	        	onNext: function() {
	        		waitForElementVisible('.form-pricing-discounts .addDiscount', hopscotch.getCurrStepNum(), nextCallback);
	        	}
	        },
	        {
	        	title: "Add Rule",
	        	content: "Click the add button to create your discounts and thresholds rule",
	        	target: ".form-pricing-discounts .addDiscount",
	        	placement: "bottom",
	        	nextOnTargetClick: true,
	        	delay: 500,
	        	onNext: function() {
	        		waitForElementVisible('.form-pricing-discounts .pricing-label', hopscotch.getCurrStepNum(), nextCallback);
	        	}
	        },
	        {
	        	title: "Name your rule",
	        	content: "Create a unquie name for your rule",
	        	target: ".form-pricing-discounts .pricing-label",
	        	placement: "bottom",
	        	nextOnTargetClick: true,
	        	delay: 500,
	        	onNext: function() {
	        		waitForElementVisible('.form-pricing-discounts .input-group-btn', hopscotch.getCurrStepNum(), nextCallback);
	        	}
	        },
	        {
	        	title: "Percentage & Threshold",
	        	content: "Place the percentage of discount or price markup and a threshold limited if you want to target a specific price",
	        	target: ".form-pricing-discounts .input-group-btn",
	        	placement: "bottom",
	        	nextOnTargetClick: true,
	        	delay: 500
	        },
	        {
	        	title: "Add more or Save your rule",
	        	content: "Click the add button to add additional rules or click the save button to apply your new rules",
	        	target: ".form-pricing-discounts",
	        	placement: "bottom",
	        	nextOnTargetClick: true,
	        	delay: 500
	        }
		]
}