var templateSaveTour = {
		id: "templateSaveTour",
		steps: [
	        {
	        	title: "Saving a template",
	        	content: "Click the arrow next to the edit button on the proposal you want to save as a template, then click save as template",
	        	target: ".DTFC_RightBodyWrapper",
	        	placement: "left",
	        	showNextButton: false,
	        	xOffset: 12,
	        	yOffset: -12,
	        	nextOnTargetClick: true
	        }
		]
}