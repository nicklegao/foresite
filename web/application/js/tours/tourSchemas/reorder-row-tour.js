/**
 * Created by coreybaines on 22/6/17.
 */
var reorderRowTour = {
    id: "reorderRowTour",
    steps: [
        {
            title: "Reorder your table columns",
            content: "Click and drag the titles to reorder the columns in your proposal table. The blue indicator shows you where they will be placed",
            target: ".DTFC_ScrollWrapper",
            placement: "top",
            xOffset: 200,
        }
    ]
}