
var intro = introJs();
var intro2 = introJs();
var buttonHidden = false;
var tutorialActive = false;
var tutorial2Active = false;
var dropDownOpen = false;
var editorOpen = false;
var currentStep = 0;
var menuButton;

$(document).ready(function(){
    $(document).on('click', '#tutorialButton', function() {
        var currentSection = $(".editorContent").find(".tab-pane.active");
        if ($(currentSection).attr('id') != "doc-pricing") {
            $("a[librarytype='pricing']").click();
        }

        startIntro();

    });

    $(document).on('click', '#endTutorial', function() {
        if (tutorialActive) {
            tutorialActive = false;
            intro.exit();
        } else {
            tutorial2Active = false;
            intro2.exit();
        }
    });

    $(document).on('click', 'button.choseFile', function() {
        if (tutorial2Active) {
            $('.introjs-nextbutton').click();
        }
    });

    $(document).on('click', 'button.cropImage', function() {
        if (tutorial2Active) {
            $('.introjs-nextbutton').click();
        }
    });

    $(document).on('click', '.saveAndPreviewClicked', function() {
        if (tutorial2Active) {
            $('.saveAndPreviewAction').click();
        }
    });



    $(document).on('click', '.fixed-dropdown-placeholder .dropdown-toggle', function() {
        if (tutorialActive && !dropDownOpen) {
            if (currentStep > 3) {
                intro.exit();
				setTimeout(function() {
					intro.goToStep(18).start();
				}, 1000);
                initStopButton();
            } else {
                intro.exit();
				setTimeout(function() {
					intro.goToStep(2).start();
				}, 1000);
                initStopButton();
            }
        } else if (tutorial2Active && !dropDownOpen) {
            intro2.exit();
			setTimeout(function() {
				intro2.goToStep(2).start();
			}, 1000);
            initStopButton();
        }
    });

    $(document).on('click', '.add-new-tab', function() {
        if (tutorialActive) {
            tutorialActive = false;
            intro.exit();
            setTimeout(function() {
                startIntro2();
            }, 2000);
        }
    });

    $(document).on('click', '.addPricingTableAction', function() {
        if (tutorialActive) {
            intro.exit();
            setTimeout(function() {
                if (currentStep > 17) {
                    intro.goToStep(19).start();
                    initStopButton();
                } else {
                    intro.goToStep(3).start();
                    initStopButton();
                }
            }, 500);
        }
    });

    $(document).on('click', '.addFreeTextLibraryAction', function() {
        if (tutorial2Active) {
            intro2.exit();
            setTimeout(function() {
                intro2.goToStep(3).start();
                initStopButton();
            }, 500);
        }
    });


    $(document).on('click', '.addFreeTextLibraryAction', function() {
        if (tutorialActive) {
            intro.exit();
            setTimeout(function () {
                intro.goToStep(28).start();
                initStopButton();
            }, 500);
        }
    });

    $(document).on('click', '.addCustomPricingAction', function() {
        if (tutorialActive) {
            intro.exit();
            if (($('.productRow').length > 3) && ($('.productRow').length < 5)) {
                setTimeout(function () {
                    intro.goToStep(13).start();
                    initStopButton();
                }, 500);
            } else if ($('.productRow').length > 4) {
                setTimeout(function () {
                    intro.goToStep(21).start();
                    initStopButton();
                }, 500);
            } else {
                setTimeout(function () {
                    intro.goToStep(5).start();
                    initStopButton();
                }, 500);
            }
        }
    });

    $(document).on('keyup', '.productRow[data-rowcounter="row1"] .rowDescription', function(e) {

        if (tutorialActive) {
            if ($('.productRow[data-rowcounter="row1"] .rowDescription').froalaEditor('html.get').length > 0) {
                $('.introjs-tooltipbuttons').show();
                $('.introjs-prevbutton').hide();
            }
        }
    });

    $(document).on('keyup', '.productRow[data-rowcounter="row2"] .rowDescription', function(e) {
        if (tutorialActive) {
            if ($('.productRow[data-rowcounter="row2"] .rowDescription').froalaEditor('html.get').length > 0) {
                $('.introjs-tooltipbuttons').show();
                $('.introjs-prevbutton').hide();
            }
        }
    });

    $(document).on('keyup', '.pricing-droppable[dataorder="0"] .rowDescription', function(e) {
        if (tutorialActive) {
            if ($('.pricing-droppable[dataorder="0"] .rowDescription').froalaEditor('html.get').length > 0) {
                $('.introjs-tooltipbuttons').show();
                $('.introjs-prevbutton').hide();
            }
        }
    });

    $(document).on('keyup', 'input#pricing-title', function(e) {
        if (tutorialActive) {
            if ($('input#pricing-title').val().length() > 0) {
                $('.introjs-tooltipbuttons').show();
                $('.introjs-prevbutton').hide();
            }
        }
    });

    $(document).on('keyup', '.productRow[data-rowcounter="row1"] .unitCostSelector', function(e) {
        if (tutorialActive) {
            var val = $('.productRow[data-rowcounter="row1"] .unitCostSelector').val();
            if (val == "99.00" || val == "99") {
                $('.introjs-tooltipbuttons').show();
                $('.introjs-prevbutton').hide();
            }
        }
    });

    $(document).on('keyup', '.productRow[data-rowcounter="row2"] .unitCostSelector', function(e) {
        if (tutorialActive) {
            var val = $('.productRow[data-rowcounter="row2"] .unitCostSelector').val();
            if (val == "1200.00" || val == "1200") {
                $('.introjs-tooltipbuttons').show();
                $('.introjs-prevbutton').hide();
            }
        }
    });

    $(document).on('keyup', '.pricing-droppable[dataorder="0"] .unitCostSelector', function(e) {
        if (tutorialActive) {
            var val = $('.pricing-droppable[dataorder="0"] .unitCostSelector').val();
            if (val == "100.00" || val == "100") {
                $('.introjs-tooltipbuttons').show();
                $('.introjs-prevbutton').hide();
            }
        }
    });

    $(document).on('keyup', '.productRow[data-rowcounter="row1"] .planTermSelector', function(e) {
        if (tutorialActive) {
            if ($('.productRow[data-rowcounter="row1"] .planTermSelector').val() == "24") {
                $('.introjs-tooltipbuttons').show();
                $('.introjs-prevbutton').hide();
            }
        }
    });


});

function startIntro(){
    intro.setOptions({
        showBullets: false,
        showProgress: true,
        keyboardNavigation: false,
        showStepNumbers:true,
        exitOnEsc:false,
        exitOnOverlayClick:false,
        hidePrev:true,
        steps: [
            {
                intro: "<h1>Welcome to the Proposal Editor</h1><br>You are now in the Proposal Editor, when you first start editing you will always start on the pricing table. Lets begin by adding some line items to your sales proposal."
            },
            //1
            {
                element: '.tab-pane.active .dropdown-toggle',
                intro: "<h1>Add a Price Line Item</h1><br>Firstly click <b>+ Insert</b> to display widgets you can add to your proposal.",
                position: 'right'
            },
            //2
            {
                element: '.dropdown-menu',
                intro: "<h1>Add a Price Line Item</h1><br>Now click <b>Add Pricing Table</b> .  This will add a new Pricing Table to your sales proposal.",
                position: 'right'
            },
            //3
            {
                element: '.pricing-droppable.current',
                intro: "<h1>Add a Price Line Item</h1><br>Now we have a price table you need to add some price line items.<br><br>Line items can be either custom/generic items (free format), or standard products added from a product catalogue managed within QuoteCloud.",
                position: 'bottom'
            },
            //4
            {
                element: '.addCustomPricingAction',
                intro: "<h1>Add a Custom Price Line Item</h1><br>Lets add a custom price line item, click on the <b>+ Add Custom Price</b> button.",
                position: 'bottom'
            },
            //5
            {
                element: '.productRow[data-rowcounter="row1"]',
                intro: "<h1>Add a Custom Price Line Item</h1><br>Now we have a new line item we need to add what we are selling, how many items and what the cost is.",
                position: 'bottom'
            },
            //6
            {
                element: '.productRow[data-rowcounter="row1"] .rowDescription',
                intro: "<h1>Add a Custom Price Line Item</h1><br>Lets add a description of the product we are selling, type <b style='font-family: Courier New'>Mobile Phone Plan</b>",
                position: 'bottom'
            },
            //7
            {
                element: '.productRow[data-rowcounter="row1"] .unitCostSelector',
                intro: "<h1>Add a Custom Price Line Item</h1><br>The unit price is where you add the price of your product or service,  type <b style='font-family: Courier New'>99.00</b>.",
                position: 'bottom'
            },
            //8
            {
                element: '.productRow[data-rowcounter="row1"] .icheckbox_square-orange',
                intro: "<h1>Add a Custom Price Line Item</h1><br>QuoteCloud is capable of setting recurring costs and one-off costs.  The One-Off check box indicates if this item has a one-off price or will be recurring charge." +
                "<br><br><img src='/application/images/checkAnimation.gif' alt='Checkbox example' style='width:99px;height:69px;'>" +
                "<br><br>Lets untick this check box, and set the Mobile Phone Plan as a recurring charge",
                position: 'bottom'
            },
            //9
            {
                element: '.productRow[data-rowcounter="row1"] .planTermSelector',
                intro: "<h1>Add a Custom Price Line Item</h1><br>The Term field allows you to set how many times the price would be charged. So lets set our mobile phone plan to be for 24 months.  Type <b style='font-family: Courier New'>24</b>.",
                position: 'bottom'
            },
            //10
            {
                element: '.termTypeDiv',
                intro: "<h1>Add a Custom Price Line Item</h1><br>The <b>Term</b> drop down indicates how often a recurring charge will occur, it will default to <b>Months</b>, you can change this to other settings by clicking on this drop down and selecting another option.<br><br>  Lets leave this set to <b>Monthly</b>",
                position: 'bottom'
            },
            //11
            {
                element: '.productRow[data-rowcounter="row1"] .qtyChooser',
                intro: "<h1>Add a Custom Price Line Item</h1><br>As this line item is a recurring charge, if you wanted to sell more than one of these products to your customer you could increase this quantity. However, leave the quantity as 1 for now.",
                position: 'bottom'
            },
            //12
            {
                element: '.addCustomPricingAction',
                intro: "<h1>Add a Custom Price Line Item</h1><br>Now lets add another line item - click on the <b>+ Add Custom Price</b> button again.",
                position: 'bottom'
            },
            //13
            {
                element: '.productRow[data-rowcounter="row2"]',
                intro: "<h1>Add a Custom Price Line Item</h1><br>You will see a new line item appear where we can enter a new product or service.",
                position: 'bottom'
            },
            //14
            {
                element: '.productRow[data-rowcounter="row2"] .descriptionColum',
                intro: "<h1>Add a Custom Price Line Item</h1><br>Type in the Description field <b style='font-family: Courier New'>iPhone Handset</b>.",
                position: 'bottom'
            },
            //15
            {
                element: '.productRow[data-rowcounter="row2"] .unitCostSelector',
                intro: "<h1>Add a Custom Price Line Item</h1><br>Type the Unit Price <b style='font-family: Courier New'>1200.00</b>",
                position: 'bottom'
            },
            //16
            {
                element: '.productRow[data-rowcounter="row2"] .icheckbox_square-orange',
                intro: "<h1>Add a Custom Price Line Item</h1><br>We leave the One-off check box ticked, as this will be a one-off charge.",
                position: 'bottom'
            },
            //17
            {
                element: '.dropdown-toggle',
                intro: "<h1>Adding Another Price Table</h1><br>QuoteCloud will allow you to create multiple Price Tables, so lets add anther price table.<br><br>Click on the Insert Button, and select <b>Add Price Table</b>.",
                position: 'right'
            },
            //18
            {
                element: '.dropdown-menu',
				intro: "<h1>Adding Another Price Table</h1><br>QuoteCloud will allow you to create multiple Price Tables, so lets add anther price table.<br><br>Click on the Insert Button, and select <b>Add Price Table</b>.",
                position: 'right'
            },
            //19
            {
                element: '.pricing-droppable[dataorder="0"]',
                intro: "<h1>Adding Another Price Table</h1><br>In this price table lets add a service charge of 4 hours, lets say our hourly rate is $100.00",
                position: 'bottom'
            },
            //20
            {
                element: '.pricing-droppable[dataorder="0"] .addCustomPricingAction',
                intro: "<h1>Adding Another Price Table</h1><br>Click the <b>+ Add Custom Price</b> button to add a new line item.",
                position: 'bottom'
            },
            //21
            {
                element: '.pricing-droppable[dataorder="0"] .rowDescription',
                intro: "<h1>Adding Another Price Table</h1><br>Type a line item description: <b style='font-family: Courier New'>Service Charge</b>.",
                position: 'bottom'
            },
            //22
            {
                element: '.pricing-droppable[dataorder="0"] .unitCostSelector',
                intro: "<h1>Adding Another Price Table</h1><br>Lets set the hourly rate to <i>100.00</i> in the Unit Price field. Type <b style='font-family: Courier New'>100.00</b>",
                position: 'bottom'
            },
            //22
            {
                element: '.pricing-droppable[dataorder="0"] .pricingTableFormatSwitches',
                intro: "<h1>Price Table Controls</h1><br>Below each price table are a set of switches, these are used to show or hide certain parts of the price table when your customer views your sales proposal.<br><br>Click on the switch next to <b>Term</b>, this will hide the 'term' column as this price table has no recurring line items in it. We will see this happen when we view the sales proposal later.",
                position: 'top'
            },
            //23
            // {
            //     element: '.pricing-droppable[dataorder="0"] .hideTerm',
            //     intro: "In our 'Service Charge' price table lets hide the 'Term' column as we do not need it.  Click on the button here to hide it.",
            //     position: 'top'
            // },
            //24
            {
                intro: "<h1>Adding Content Blocks</h1><br>Now you have successfully created a pricing section, lets take a look at adding some general content such as text and images to your proposal content."
            },
            //26
            {
                element: '.add-new-tab',
                intro: "<h1>New Section</h1><br>Now click the <b>+ Add Section</b>",
                position: 'right'
            }
        ]
    });

    intro.onchange(function (element) {

        currentStep = this._currentStep;

        if ((this._currentStep == 1) || (this._currentStep == 2) || (this._currentStep == 4) || (this._currentStep == 7) || (this._currentStep == 12)
            || (this._currentStep == 15) || (this._currentStep == 17) || (this._currentStep == 18)
            || (this._currentStep == 20) || (this._currentStep == 22) || (this._currentStep == 25)
            || (this._currentStep == 26) || (this._currentStep == 27) || (this._currentStep == 33) || (this._currentStep == 8)) {
            // intro.setOption("showButtons", false);
            $('.introjs-tooltipbuttons').hide();
        }

		if (this._currentStep == 8) {
			var checkExist = setInterval(function() {
				if (!(currentStep == 8)) {
					clearInterval(checkExist);
				}
				if ((currentStep == 8) && ($('.upfrontColumn .icheckbox_square-orange').hasClass('checked')))
				{
					$('.introjs-tooltipbuttons').hide();
				} else {
					$('.introjs-tooltipbuttons').show();
					$('.introjs-nextbutton').show();
                }
			}, 500);
		}


        if ((this._currentStep == 1)) {
            menuButton = element;
        }

        if ((this._currentStep == 2)) {
            var checkExist = setInterval(function() {
                dropDownOpen = true;
                if (currentStep != 2) {
                    dropDownOpen = false;
                    clearInterval(checkExist);
                    menuButton = null;
                }
                if ($('.dropdown.open').length < 1) {
                    $(menuButton).click();
                }
            }, 500);
        }

        $('.introjs-prevbutton').hide();

        if ((this._currentStep == 9) && ($('.upfrontColumn .icheckbox_square-orange').hasClass('checked'))) {
            $('.introjs-nextbutton').hide();
            $('.introjs-prevbutton').show();
        } else if ((this._currentStep == 9) && (!($('.upfrontColumn .icheckbox_square-orange').hasClass('checked')))) {
            $('.introjs-tooltipbuttons').hide();
        } else if ((this._currentStep == 9) || ($('.upfrontColumn .icheckbox_square-orange').hasClass('checked'))) {
            $('.introjs-nextbutton').show();
        }



        if ((this._currentStep == 22) || (this._currentStep == 23)) {
            intro.setOption("scrollToElement", true);
        }
    });
    tutorialActive = true;
    intro.start();

    initStopButton();
}

function startIntro2(){
    tutorial2Active = true;
    tutorialActive = false;
    intro2.setOptions({
        showBullets: false,
        showProgress: true,
        keyboardNavigation: false,
        showStepNumbers:true,
        exitOnEsc:false,
        exitOnOverlayClick:false,
        hidePrev:true,
        steps: [
            //26
            {
                element: '.tab-pane.dynamicTabContent.active .sectionTitle',
                intro: "<h1>Add a New Section</h1><br>Now click into the title field, delete the current text and type the title of your new section.<br><br> After deleting the existing text, type <b style='font-family: Courier New'>My New Section</b>",
                position: 'right'
            },
            //27
            {
                element: '.tab-pane.dynamicTabContent.active .dropdown-toggle',
                intro: "<h1>Add a Text Block</h1><br>Now we have a new section, lets add a text block. <br> Click on the <b>+ Insert</b> button.",
                position: 'right'
            },
            //28
            {
                element: '.tab-pane.dynamicTabContent.active .dropdown-menu',
                intro: "<h1>Add a Text Block</h1><br>Click on the option <b>Add Free Text<b>",
                position: 'right'
            },
            //29
            {
                element: '.panel.editable[type="editor"]',
                intro: "<h1>Add a Text Block</h1><br>Now type some text into your new text block.  You can try some of the formatting tools also if you like.",
                position: 'right'
            },
            {
                element: '.panel.editable[type="editor"] .panel-heading .panel-tools',
                intro: "<h1>Reordering Content Blocks</h1><br> You can change the order of content blocks, you can use these arrows to change the order of content blocks.",
                position: 'bottom'
            },
            //32
            {
                element: 'ul#documentSectionBrowser',
                intro: "<h1>Re-ordering Sections</h1><br>You can decide on the order of your sections by Click & Drag on the section name. </br><hr></br> <b>Click & Drag</b> your new section up to the top, below the Proposal Summary Section.",
                position: 'left'
            },
            //33
            {
                intro: "<h1>Congratulations you've completed the tutorial</h1> <br>Ok, lets take a look at your new sales proposal as the customer will see it.<br><br> Click on the <b>Save & Preview</b> button.",
            }
        ]
    });


    intro2.onchange(function (element) {
        console.log(this._currentStep);
        currentStep = this._currentStep;

        $('.introjs-prevbutton').hide();
        if ((this._currentStep == 1) || (this._currentStep == 2)) {
            // intro.setOption("showButtons", false);
            $('.introjs-tooltipbuttons').hide();
        } else if (this._currentStep == 6) {
            $('.introjs-nextbutton').hide();
            $('.introjs-tooltipbuttons').append('<a role="button" class="introjs-button saveAndPreviewClicked" tabindex="-1">Save & Preview</a>')
        } else {
            $('.introjs-tooltipbuttons').show();
        }

        if (this._currentStep == 3) {
            $('.cke_editor_editor1').attr('style', 'z-index:999999999 !important;');
        }


        if ((this._currentStep == 1)) {
            menuButton = element;
        }

        if ((this._currentStep == 2)) {
            var checkExist = setInterval(function() {
                dropDownOpen = true;
                if (currentStep != 2) {
                    dropDownOpen = false;
                    clearInterval(checkExist);
                    menuButton = null;
                }
                if ($('.dropdown.open').length < 1) {
                    $(menuButton).click();
                }
            }, 500);
        }

		if ((this._currentStep == 2)) {
			var checkExist = setInterval(function() {
				dropDownOpen = true;
				if (currentStep != 2) {
					dropDownOpen = false;
					clearInterval(checkExist);
				}
				if ($('.dropdown.open').length < 1) {
					$('button.dropdown-toggle')[1].click();
				}
			}, 500);
		}

		if ((this._currentStep == 3)) {
			var checkExist = setInterval(function() {
				dropDownOpen = true;


				if (($('div.panel.editable[type="editor"]').length > 0) && !editorOpen) {
				    editorOpen = true;
					intro2.goToStep(3).start();
					clearInterval(checkExist);
				} else if (($('div.panel.editable[type="editor"]').length < 1) && !editorOpen) {
					intro2.exit();
                }

			}, 500);
		}

    });


    intro2.start();

    initStopButton();
}

function initStopButton() {
    var overlay = $('.introjs-overlay');
    if (overlay != null) {
        $(overlay).append("<a id='endTutorial' href='#'>End Tutorial</a>");
    }
}