var exportProposalsTour  = {
		id: "exportProposalsTour",
		steps: [
	        {
	        	title: "Export Proposals",
	        	content: "Click to export all your proposals as a .csv formatted file and store then locally on your computer",
	        	target: ".guide-export-button",
	        	placement: "left",
	        	nextOnTargetClick: true
	        }
		]
}