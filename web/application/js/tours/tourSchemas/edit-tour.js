var tour  = {
		id: "edit-tour",
		steps: [
	        {
	        	title: "Editing your proposal",
	        	content: "This is a list of sections in your proposal.",
	        	target: "#documentSectionBrowser",
	        	delay: 1500,
	        	placement: "right",
	        	nextOnTargetClick: true,
	        },
	        {
	        	title: "Adding sections",
	        	content: "You can add sections by clicking here.",
	        	target: ".add-new-tab",
	        	placement: "right",
	        	nextOnTargetClick: true,
	        },
	        {
	        	title: "Editing Content",
	        	content: "You can drag and drop content from the sidebar into the proposal",
	        	target: "#sidebar",
	        	placement: "left",
	        },
	        {
	        	title: "Editing Sections",
	        	content: "You can also insert custom content manually by clicking here",
	        	target: ".contentContainer .fixed-dropdown-placeholder .btn",
	        	placement: "right",
	        	delay: 1000,
	        },
	        {
	        	title: "Pricing",
	        	content: "Add a custom pricing section now",
	        	target: "#doc-pricing .libraryActions",
	        	placement: "bottom",
	        	showNextButton: false,
	        	nextOnTargetClick: true,
	        	onNext: function() {
	        		waitForElementVisible("#customPricingRows .customPricing", hopscotch.getCurrStepNum(), nextCallback);
	        	}
	        },
	        {
	        	title: "Pricing",
	        	content: "You can edit pricing details which will show up in your proposal",
	        	target: "#customPricingRows .customPricing",
	        	placement: "top",
	        },
	        {
	        	title: "Preview",
	        	content: "You can preview how the client will see the proposal here. This will also allow you to send the proposal as well.",
	        	target: ".saveAndPreviewAction",
	        	placement: "left",
	        },
		]
}