console.log('sabre tour');
var sabreTour = {
	id : "sabreTour",
	steps : [ {
		title : "Account drop down",
		content : "Click to open the extensions and plugins page",
		target : ".guide-account",
		placement : "left",
		multipage : true,
		nextOnTargetClick : true,
		yOffset : -2,
		arrowOffset : 6,
		onNext : function()
		{
			waitForElementVisible('.guide-extensions-button', hopscotch.getCurrStepNum(), sabreTourNextCallback);
		}
	}, {
		title : "Extensions &amp; Plugins",
		content : "Click to open the Extensions &amp; Plugins window",
		target : ".guide-extensions-button",
		placement : "left",
		multipage : true,
		nextOnTargetClick : true,
		yOffset : -9,
		arrowOffset : 3,
		showNextButton : false

	}, {
		title : "Select plugin",
		content : "Find the Sabre plugin and click on it",
		target : document.querySelector("#app-nav-categories"),
		placement : "left",
		multipage : true,
		onNext : function()
		{
			waitForElementVisible('#settingsBtn[app-name="qcsabre"]', hopscotch.getCurrStepNum(), sabreTourNextCallback);
		}
	}, {
		title : "Sabre settings",
		content : "Click to open Sabre settings window",
		target : "#settingsBtn",
		placement : "bottom",
		multipage : true,
		nextOnTargetClick : true,
		onNext : function()
		{
			waitForElementVisible("#usersTable .edit-item", hopscotch.getCurrStepNum(), sabreTourNextCallback);
		}
	}, {
		title : "User Settings",
		content : "Click to edit the Sabre consultant ID for the user",
		target : "#usersTable .edit-item",
		placement : "bottom",
		multipage : true,
		nextOnTargetClick : true,
		onNext : function()
		{
			waitForElementVisible("#saveUserbutton", hopscotch.getCurrStepNum(), sabreTourNextCallback);
		}
	}, {
		title : "Save",
		content : "Save the consultant ID",
		target : "#saveUserbutton",
		placement : "bottom",
		nextOnTargetClick : true
	}, {
		title : "Teams",
		content : "Select Teams tab to edit the team details for Sabre",
		target : 'a[aria-controls="teamsSettings"]',
		placement : "bottom",
		multipage : true,
		nextOnTargetClick : true,
		onNext : function()
		{
			waitForElementVisible("#teamsTable .edit-item", hopscotch.getCurrStepNum(), sabreTourNextCallback);
		}
	}, {
		title : "Team Settings",
		content : "Click to edit team setting for Sabre",
		target : "#teamsTable .edit-item",
		placement : "bottom",
		multipage : true,
		nextOnTargetClick : true,
		onNext : function()
		{
			waitForElementVisible("#saveTeambutton", hopscotch.getCurrStepNum(), sabreTourNextCallback);
		}
	}, {
		title : "Save",
		content : "Save the team settings",
		target : "#saveTeambutton",
		placement : "bottom",
		nextOnTargetClick : true
	} ]
}
