var templateDeleteTour = {
		id: "templateDeleteTour",
		steps: [
	        {
	        	title: "Deleting a proposal",
	        	content: "Click the arrow next to the edit button on the proposal you want to delete, then click the Delete button",
	        	target: ".DTFC_RightBodyWrapper",
	        	placement: "left",
	        	showNextButton: false,
	        	xOffset: 12,
	        	yOffset: -12,
	        	nextOnTargetClick: true
	        }
		]
}