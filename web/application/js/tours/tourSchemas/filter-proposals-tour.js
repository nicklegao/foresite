var filterProposalTour  = {
		id: "filterProposalTour",
		steps: [
	        {
	        	title: "Filters Types",
	        	content: "You can filter your proposal by status",
	        	target: "#sparks",
	        	placement: "bottom",
	        	xOffset: 500
	        },
	        {
	        	title: "Filter proposals",
	        	content: "You can also filter the number of proposals displayed on one page",
	        	target: ".form-control",
	        	placement: "top",
	        	nextOnTargetClick: true,
	        	xOffset: 10,
	        	yOffset: -12
	        },
	        {
	        	title: "Date filter",
	        	content: "Here you can filter your proposals based on a date period",
	        	target: "#dateRangePicker",
	        	placement: "top",
	        	nextOnTargetClick: true,
	        	xOffset: 50,
	        	yOffset: -12
	        },
	        {
	        	title: "Visible Columns",
	        	content: "In this drop down you can select which columns you see in the table below",
	        	target: "#columns-list",
	        	placement: "top",
	        	nextOnTargetClick: true,
	        	yOffset: -12
	        },
	        {
	        	title: "Column based filter",
	        	content: "Click here to see the filters you can placed on the table below based on the columns",
	        	target: ".dataTables_filter",
	        	placement: "left",
	        	xOffset: -12,
	        	yOffset: -12,
	        	multipage: true,
	        	nextOnTargetClick: true,
	        	onNext: function() {
	        		waitForElementVisible('.searchRow', hopscotch.getCurrStepNum(), nextCallback);
	        	}
	        },
	        {
	        	title: "Test the company filter",
	        	content: "Enter a company's name and try filtering your proposals to show only that company",
	        	target: ".searchRow",
	        	placement: "left",
	        	xOffset: 510,
	        	multipage: true,
	        	nextOnTargetClick: true,
	        	onNext: function() {
	        		waitForElementVisible('.columnFiltersEnabled', hopscotch.getCurrStepNum(), nextCallback);
	        	}
	        },
	        {
	        	title: "Clear your filter",
	        	content: "Click here to clear your column filters and reset the proposal table to show everything",
	        	target: ".columnFiltersEnabled",
	        	placement: "left",
	        	xOffset: -16,
	        	yOffset: -12,
	        	nextOnTargetClick: true
	        }
//	        {
//	        	title: "Editing Proposals",
//	        	content: "You can edit your proposal by click this button or you can manage them by expanding the list of options.",
//	        	target: "#matchedProposals tbody tr:first-child .dashboard-action-button",
//	        	placement: "left",
//	        }
		]
}