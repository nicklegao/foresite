var proposalTour  = {
		id: "proposalTour",
		steps: [
	        {
	        	title: "Open the Proposal Creator",
	        	content: "You can get started by clicking here to create your proposal",
	        	target: ".createProposalAction",
	        	placement: "right",
	        	multipage: true,
	        	nextOnTargetClick: true,
	        	onNext: function() {
	        		waitForElementVisible('.form-create-proposal #proposalTitle', hopscotch.getCurrStepNum(), nextCallback);
	        	}
	        },
	        {
	        	title: "Creating a proposal",
	        	content: "You can fill follow the steps to create your first proposal",
	        	target: ".form-create-proposal #proposalTitle",
	        	placement: "bottom",
	        	nextOnTargetClick: true,
	        	delay: 500,
	        },
	        {
	        	title: "Template Preview",
	        	content: "Here you can see a list of covers and a preview",
	        	target: ".form-create-proposal #coverPicker",
	        	placement: "right",
	        	nextOnTargetClick: true,
	        	multipage: true,
	        	onNext: function() {
	        		waitForElementVisible('#tab2', hopscotch.getCurrStepNum(), nextCallback)
	        	}
	        },
	        {
	        	title: "Customer Information",
	        	content: "Here you can fill in the customer information for your proposal",
	        	target: "#tab2",
	        	placement: "right",
	        	nextOnTargetClick: true,
	        	multipage: true,
	        	onNext: function() {
	        		waitForElementVisible('.next.finish', hopscotch.getCurrStepNum(), nextCallback)
	        	}
	        	
	        },
	        {
	        	title: "Create!",
	        	content: "When you are done click here to create your proposal!",
	        	target: ".form-create-proposal .next.finish",
	        	placement: "bottom",
	        	nextOnTargetClick: true,
	        	onNext: function() {
	        		waitForElementVisible('#matchedProposals tbody tr:first-child', hopscotch.getCurrStepNum(), nextCallback)
	        	}
	        }
		]
}