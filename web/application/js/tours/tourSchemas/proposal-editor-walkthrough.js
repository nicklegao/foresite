/**
 * Created by coreybaines on 22s/6/17.
 */
var proposalEditorWalkthrough = {
    id: "proposalEditorWalkthrough",
    steps: [
        {
            title: "The Content Editor",
            content: "Content is place here to simulate your proposal pages. Here is where you can edit Text, Pricing, Images, Videos and more content comming soon.",
            target: ".input-icon",
            placement: "bottom",
            xOffset: 450,
            yOffset: 300,
        },
        {
            title: "Sections",
            content: "This is where you are able to switch between the sections that make part of your proposal. If you hover over the sections you will also see a (x) appear allowing you to delete an entire section.",
            target: "#documentSectionBrowser",
            placement: "right",
            xOffset: 0,
            yOffset: 5,
        },
        {
            title: "Add a new Section",
            content: "Click this button to add a new section to your proposal.",
            target: ".add-new-tab",
            placement: "right",
            xOffset: 0,
            yOffset: -15,
        },
        {
            title: "Library Content",
            content: "Here you can view and use your library content inside your proposal.",
            target: "#sidebar",
            placement: "left",
            xOffset: 0,
            yOffset: 100,
        },
        {
            title: "Manage Library Content",
            content: "Clicking 'Manage' will give you quick access to its content library where you can edit and upload new content.",
            target: ".manage-content-library-btn",
            placement: "left",
            xOffset: 0,
            yOffset: -20,
        },
        {
            title: "Search Library Content",
            content: "Here you can search the entire content libraries giving you quick access to your content.",
            target: ".documentLibrarySearch",
            placement: "left",
            xOffset: 0,
            yOffset: -20,
        },
        {
            title: "Section Title",
            content: "This is where you can place a title at the top of each section.",
            target: "#pricing-title",
            placement: "bottom",
            xOffset: 100,
            yOffset: 0,
        },
        {
            title: "Hide Title",
            content: "Using this switch you can hide or show the titles in each section.",
            target: ".noTitleSection",
            placement: "bottom",
            xOffset: 0,
            yOffset: 0,
        },
        {
            title: "New Page Switch",
            content: "This switch allows you to force each section to start on its own page.",
            target: ".pageBreakPanel",
            placement: "bottom",
            xOffset: 0,
            yOffset: 0,
        },
        {
            title: "Select your first Content",
            content: "Lets place your first Image onto the content editor. Click the images section of the content library, content is stored in folders to help you sort through different images. Lets select the first one.",
            target: "#sidebar",
            placement: "left",
            xOffset: 0,
            yOffset: 100,
        },
        {
            title: "Drag and Place",
            content: "Drag the content to the Content Area. You will see a tap appear at the top, drop the image ontop of that tab and you will have added your first piece of content.",
            target: "#sidebar",
            placement: "left",
            xOffset: -15,
            yOffset: 150,
        }
    ]
}