var storedTourStep;

var waitForElementVisible = function(element, tourStep, callback)
{
	var checkExist = setInterval(function()
	{
		$element = $(element);
		if ($element.is(':visible'))
		{
			clearInterval(checkExist);
			if (typeof callback == "function")
			{
				callback(tourStep);
			}
		}
	}, 100);
};

function waitForStartupHidden(element)
{
	var checkExist = setInterval(function()
	{
		$element = $(element);
		if (($element == null) || (!($element.is(':visible'))))
		{
			clearInterval(checkExist);
			if (navigator.userAgent.match(/iPad/i) == null)
			{
				startCreatorWalkthrough();
			}
		}
	}, 100);
};


function waitForModalElement(element)
{
	var checkExist = setInterval(function()
	{
		$element = $(element);
		if (($element.is(':visible')))
		{
			clearInterval(checkExist);
			endTour();
		}
	}, 100);
};

var nextCallback = function(tourStep)
{
	hopscotch.startTour(proposalTour, tourStep);
}

var sabreTourNextCallback = function(tourStep)
{
	hopscotch.startTour(sabreTour, tourStep);
}

hopscotch.listen("next", function()
{
	// Push hopscotch to the next step

});

hopscotch.listen("prev", function()
{
	// Push hopscotch to the next step

});

hopscotch.listen("close", function()
{
})

hopscotch.listen("start", function()
{
	// Push commands to sessionStorage about starting
})

hopscotch.listen("end", function()
{
})

// $('html').click(function(e) {
// $element = $('.tour-tourStarter');
// if (!($element.is(':visible'))) {
// if(!$(e.target).hasClass('tour-tourStarter') )
// {
// $('.menu').remove();
// }
// }
//	
//	
// if(!$(e.target).hasClass('tour-tourStarter') )
// {
// $('.menu').remove();
// }
// });

function isStorageWorking()
{
	try
	{
		sessionStorage.setItem('sessionStorageTest', 'true');
		return true
	}
	catch (e)
	{
		// In private browsing...
		return false;
	}
}

function welcomeTour()
{
	setTimeout(function()
	{
		if (isStorageWorking())
		{
			var welcomeTourFinished = localStorage.getItem('dashboard.welcome.tour.finished');

			if (!welcomeTourFinished)
			{
				localStorage.setItem('dashboard.welcome.tour.finished', 'true');
				hopscotch.startTour(tourStarter, 0);
			}
		}
		else
		{
			if (!readCookie('dashboard.welcome.tour.finished'))
			{
				createCookie('dashboard.welcome.tour.finished', 'true', 3000);
				hopscotch.startTour(tourStarter, 0);
			}
		}
	}, 1000);
}

function isStarted()
{
	if (sessionStorage.getItem('dashboard.welcome.tour.started') || sessionStorage.getItem('hopscotch.tour.state'))
	{
		return true;
	}
	return false;
}

function endTour()
{
	if (isStarted)
	{
		hopscotch.endTour(true);
	}
}

//
//
//
// DASHBOARD TOURS BEGIN HERE
//
//
//

function guideCreatingProposal(e)
{
	endTour();
	hopscotch.startTour(proposalTour, 0);
}

function guideSavingTemplate(e)
{
	endTour();
	hopscotch.startTour(templateSaveTour, 0);
}

function guideFilterProposal(e)
{
	endTour();
	hopscotch.startTour(filterProposalTour, 0);
}

function guideExportproposals(e)
{
	endTour();
	hopscotch.startTour(exportProposalsTour, 0);
}

function guideDeleteproposals(e)
{
	endTour();
	hopscotch.startTour(templateDeleteTour, 0);
}

function guidePreviewproposals(e)
{
	endTour();
	hopscotch.startTour(templatePreviewTour, 0);
}

function guideReorderColumns(e)
{
	endTour();
	hopscotch.startTour(reorderRowTour, 0);
}

function guideSebreSetup(e)
{
	endTour();
	hopscotch.startTour(sabreTour, 0);
}

function guideProposalEditor(e)
{
	endTour();
	hopscotch.startTour(proposalEditorWalkthrough, 0);
}

function guideDMPricing(e)
{
	endTour();
	hopscotch.startTour(discountMargintour, 0);
}


//
//
//
// EDITOR TOURS BEGIN HERE
//
//
//


//
//
//
// TOUR SUPPORT FUNCTIONS BEGIN HERE
//
//
//

function restartTour()
{
	storedTourStep = 0;
	waitForElementVisible(tour.steps[storedTourStep].target, storedTourStep, nextCallback);
}

function createCookie(name, value, days)
{
	var expires = "";
	if (days)
	{
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		expires = "; expires=" + date.toUTCString();
	}
	document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name)
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++)
	{
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0)
			return c.substring(nameEQ.length, c.length);
	}
	return null;
}

function eraseCookie(name)
{
	createCookie(name, "", -1);
}


// if(tourState != null) {
// storedTourStep = tourState.substring(tourState.indexOf(":") + 1);
// waitForElementVisible(tour.steps[storedTourStep].target, storedTourStep,
// nextCallback);
// } else {
// hopscotch.startTour(tourStarter, 0);
// }
