
var travelDocsTable = undefined;
var tableColumnDefs = undefined;
var travelDocsTable = undefined;
var tableColumnOrder = [];
var searchHidden = true;
var autoLogoutWarning = undefined;
var refreshTimer;
var refreshTimeout;
var redrawTableDelay = undefined;
var runDataTable;
var typingTimer; // timer identifier
var doneTypingInterval = 500; // time in ms, 5 second for example
var endDate;
var startDate;
var currentDashboardDataRequest;
var columnFilters = [];

var datatablecolumnOrder = [];
var datatablecolumnOrderRaw = [];

$(document).ready(function()
{
	$('#toggle-fold-aside-button').click();
	generateFilterVisibilitySetting();

	function buildUrl(path, data)
	{
		var ret = [];
		for ( var d in data)
			ret.push(encodeURIComponent(d) + "=" + encodeURIComponent(data[d]));
		return path + "?" + ret.join("&");
	}

	$(window).scroll(function(e)
	{

		var $el = $('.dataTables_scrollHead');
		var isPositionFixed = ($el.css('position') == 'fixed');
		if ($(this).scrollTop() > 200 && !isPositionFixed)
		{
			$('.dataTables_scrollHead').css({
				'position' : 'fixed',
				'top' : '0px'
			});
		}
		if ($(this).scrollTop() < 200 && isPositionFixed)
		{
			$('.dataTables_scrollHead').css({
				'position' : 'static',
				'top' : '0px'
			});
		}
	});

	$(document).on('click', '.dashboard-table-header', function(e)
	{
		var type = $(this).attr('data-direction');
		if (type == undefined)
		{
			var arrow = '<span class="dashboard-filter-direction-arrow" style="float: right;height: 14px;position:absolute;right: 0;"><i class="fas fa-chevron-up" style="font-size: 12px;color: white;"></i></span>';
			$(this).attr('data-direction', "asc");
			$(this).append(arrow);
		}
		else
		{
			var span = $(this).find('.dashboard-filter-direction-arrow');
			if (type === "asc")
			{
				$(span).html('<i class="fas fa-chevron-down" style="font-size: 12px;color: white;"></i>');
				$(this).attr('data-direction', "desc");
			}
			else
			{
				$(span).remove();
				$(this).attr('data-direction', "");
			}
		}
		datatablecolumnOrder = [];
		datatablecolumnOrderRaw = [];
		var pendingFilters = $('.dashboard-table-header .dashboard-filter-direction-arrow');
		$(pendingFilters).each(function(e)
		{
			var th = $(this).closest('.dashboard-table-header');
			var direction = $(th).attr('data-direction');
			var index = $(th).index();
			var myObject = {};
			myObject.column = index;
			myObject.dir = direction + "";
			datatablecolumnOrder.push(JSON.stringify(myObject));

			var myObject1 = {};
			myObject1.column = index;
			myObject1.name = $(th).attr('id');
			myObject1.dir = direction + "";
			datatablecolumnOrderRaw.push(myObject1);
		});
		runDataTable();
	});

	$(document).on('click', '.dt-pagination-btn', function()
	{
		$('.dt-pagination-btn.selected').removeClass('selected mdl-button--raised mdl-button--colored');
		$(this).addClass('selected mdl-button--raised mdl-button--colored');
		runDataTable();
	});

	$(document).on('click', '.dt-pagination-prev-btn', function()
	{
		$('.dt-pagination-btn.selected').prev().click();
	});

	$(document).on('click', '.dt-pagination-next-btn', function()
	{
		$('.dt-pagination-btn.selected').next().click();
	});


	$(document).on('click', '#travelDocs_filter', function()
	{
		// if ($('#travelDocs_filter_advance').is(':visible')) {
		// $('#travelDocs_filter_advance').parent().hide();
		// } else {
		// $('#travelDocs_filter_advance').parent().show();
		// }

		showSearchRow();
	});


	$(document).on('click', '#travelDocs_filter_advance', function()
	{
		$('#travelDocsSearchClear').click();
		if ($('#traveldocFilterWindow').is(':visible'))
		{
			$('#traveldocFilterWindow').hide();
		}
		else
		{
			$('#traveldocFilterWindow').show();
		}
	});


	$(document).on('click', '#travelDocs_filter_cancel', function()
	{
		$('.filter-container').empty();
		$('#travelDocsSearchClear').click();
		$('#traveldocFilterWindow').hide();

	});


	$(document).on('click', '#travelDocs_filter_add', function()
	{
		newAdvanceFilter();
	});

	$(document).on('click', '.delete-advance-filter', function()
	{
		removeAdvanceFilter($(this).closest('.row'));
	});

	$(document).on("click", ".doResendProposal", function(e)
	{


		$('body').modalmanager('loading');
		var $modal = $('#send-proposal-modal');
		var traveldocID = $(this).attr('data-id');

		$modal.load('/application/dialog/sendProposal.jsp?proposal=' + traveldocID, '', function()
		{
			console.log('MODAL LOADED');
			$modal.modal({
				backdrop : 'static',
				keyboard : false
			});
			// $modal.css('left', '42%');
			// $modal.css('top', '30%');
			$modal.css('width', '650px');
		});
	});
	$(document).on("click", ".doSendTraveldocsConfirmedAction", sendProposalConfirmed);

	$(document).on('click', '.filter-container .dropdown a', function()
	{
		var dropdown = $(this).closest('.dropdown');
		var row = $(dropdown).closest('.row');
		var deleteButton = $(row).find('.delete-advance-filter');
		var button = $(dropdown).find('button');
		$(button).attr('data-filterValue', $(this).attr('value')).html($(this).attr('value'));

		if ($(dropdown).hasClass('filter-drop'))
		{
			$(button).attr('data-filterType', $(this).attr('data-filterType')).attr('data-filterValue', $(this).attr('data-fullname')).html($(this).html());

			if ($(this).attr('data-filterType') === "date")
			{
				$(appendFilterCols("date", row)).insertBefore($(deleteButton).parent());
			}
			else
			{
				$(appendFilterCols("text", row)).insertBefore($(deleteButton).parent());
			}
		}
		else if ($(dropdown).hasClass('filter-seperator-drop'))
		{
			$(button).attr('data-seperatorType', $(this).attr('data-seperatorType')).attr('data-filterValue', $(this).attr('data-fullname')).html($(this).html());

			if ($(this).attr('data-seperatorType') === "equals")
			{
				$(appendFilterCols("equals", row)).insertBefore($(deleteButton).parent());
				if ($(row).find('.advance-form-datepicker').length > 0)
				{
					$(row).find('.advance-form-datepicker').daterangepicker({
						autoApply : true,
						showDropdowns : true,
						singleDatePicker : true,
						locale : {
							format : 'YYYY-MM-DD'
						}
					});
				}
			}
			else if ($(this).attr('data-seperatorType') === "between")
			{
				$(appendFilterCols("between", row)).insertBefore($(deleteButton).parent());
				$(row).find('.advance-form-datepicker').daterangepicker({
					autoApply : true,
					showDropdowns : true,
					locale : {
						format : 'YYYY-MM-DD',
						separator : " -- "
					}
				});
			}
			else
			{
				$(appendFilterCols("like", row)).insertBefore($(deleteButton).parent());
			}
		}

	});


	// $(document).on('input', '.travelDocsSearchInput', function()
	// {
	// showLoader(true, null);
	// runDataTable();
	// });

	$(document).on('input', '.travelDocsSearchInput:not(select)', function()
	{
		showLoader(true, null);
		clearTimeout(typingTimer);
		typingTimer = setTimeout(doneTyping, doneTypingInterval);
	});

	$(document).on('change', 'select.travelDocsSearchInput', function()
	{
		runDataTable();
	});

	$(document).on('keydown', '.travelDocsSearchInput', function()
	{
		clearTimeout(typingTimer);
	});

	$(document).on('click', 'select.travelDocsSearchInput', function(e)
	{
		e.stopPropagation();
	});

	$(document).on('click', '#travelDocsSearchClear', function()
	{

		showLoader(true, null);
		$('.travelDocSearchRow').remove();
		showSearchRow();
		runDataTable();
	});

	$(document).on('click', '.travelDocStatusButton', function()
	{
		$(this).toggleClass('active');
		showLoader(true, null);
		runDataTable();
	});

	$(document).on('click', '.btn-complete', function(e)
	{
		e.preventDefaults;
		var btn = $(this);
		Swal.fire({
			title : "Confirm",
			type : "warning",
			html : '<p>Are you sure to mark this task as completed?</p>',
			showCancelButton: true,
		}).then(function(result)
		{
			$.ajax({
				url: '/api/tasks/update-status',
				data: {
					id : btn.closest('tr').attr('data-eleid'),
					status : 'Complete',
				},
				type : 'POST'
			}).done(function(response)
			{
				if (response.success)
				{
					redrawTable();
				}
				else
				{
					Swal.fire({
						title : 'Error',
						text : 'Unable to update status.Please try again.',
						type : 'error'
					});
				}
			});
		});
	});
	
	$(document).on('click', '.btn-incomplete', function(e)
	{
		e.preventDefaults;
		var btn = $(this);
		Swal.fire({
			title : "Confirm",
			type : "warning",
			html : '<p>Are you sure to mark this task as incompleted?</p>',
			input: 'textarea',
			inputPlaceholder: 'Please enter the reason...',
			inputAttributes: {
			  'aria-label': 'Please enter the reason'
			},
			showCancelButton: true,
		}).then(function(result)
		{
			if(!result.value){
				alert("Please enter the reason of incomplete.")
			}
			$.ajax({
				url: '/api/tasks/update-status',
				data: {
					id : btn.closest('tr').attr('data-eleid'),
					status : 'Incomplete',
					reason : result.value,
				},
				type : 'POST'
			}).done(function(response)
			{
				if (response.success)
				{
					redrawTable();
				}
				else
				{
					Swal.fire({
						title : 'Error',
						text : 'Unable to update status.Please try again.',
						type : 'error'
					});
				}
			});
		});
	});

	$(document).on('click', '#travelDocs_filter_advance_go', function()
	{

		runDataTable();
	});

	$(document).on('click', '#matchedTravelDocs tr td', function(e)
	{
		var tr = $(this).parent();
		var trID = $(tr).attr('data-id');
		if (trID == null || trID == undefined)
		{
			return;
		}
		var rowIndex = $(tr).index(this);
		var tdCount = $(tr).children('td').length;
		var sibling = $(tr).next();

		var descriptionRow = '<tr><td class="leftHolder" style="display: table-cell;"></td><td class="td-row-description" colspan="' + (tdCount - 2) + '" style=""></td><td class="rightHolder" style=""></td></tr>';

		console.log(tr);

		if ($(sibling).find('.td-row-description').length < 1)
		{

			if ($('.td-row-description').length > 0)
			{
				$('.td-row-description').parent().remove();
			}
			$(descriptionRow).insertAfter($(tr));
			loadTaskDescription($('.td-row-description'), $(tr).attr('data-id'), $(tr).attr('data-eleid'));
		}
		else
		{
			$('.td-row-description').parent().remove();
		}


	});

	$(document).on('mouseenter', '#matchedTravelDocs tr td', function(e)
	{
		var tr = $(this).parent();
		var button = $(tr).find('.quick-view-button');
		$(button).addClass('active');

	});

	$(document).on('mouseleave', '#matchedTravelDocs tr td', function(e)
	{
		var tr = $(this).parent();
		var button = $(tr).find('.quick-view-button');
		$(button).removeClass('active');

	});

	$("#travelDocsDisplaySelector").on('change', function()
	{
		showLoader(true, null);
		runDataTable();
	});

	$('#project-dashboard').on('click', '.tableColumnHolder .dropdown-item', function()
	{

		var $this = $(this);
		var visible = !$(this).data('visible');
		$this.data('visible', visible);
		var columnId = $this.data('id');
		if (visible)
		{
			$this.addClass('toggle-vis');
			$('#matchedTravelDocs').find('#' + columnId).show();
			$('#matchedTravelDocs').find('[data-id="' + columnId + '"]').show();
		}
		else
		{
			$this.removeClass('toggle-vis');
			$('#matchedTravelDocs').find('#' + columnId).hide();
			$('#matchedTravelDocs').find('[data-id="' + columnId + '"]').hide();
		}
		buildFilterSettingsJson();
		if ($('tr.travelDocSearchRow').is(':visible'))
		{
			$('button#travelDocs_filter').click();
		}

	});
	$(document).on("click", ".displayDeleteTaskAction", displayDeleteTaskAction);
	$(document).on("click", ".taskDeleteConfimed", taskDeleteConfimed);
	$(document).on("click", ".displayTravelDocNotesAction", displayTravelDocNotesAction);

	$(document).on('click', "button#addNewAgent", function()
	{
		addNewAgentCard();
	});

	$(document).on('click', '#clearAllAgentsButton', function(e)
	{
		$('#agent-link-tab').empty();
		addNewAgentCard();
	});

	$(document).on('click', '.agentTrashButton i', function(e)
	{
		e.preventDefaults;
		var card = $(this).closest('.collapse-card.card');
		$(card).remove();
		var cards = $('#agent-link-tab .collapse-card.card');
		if (cards.length < 2)
		{
			$('.agentTrashButton').hide();
		}
	});

	$(document).on('click', '.form-bootstrapWizard a', function(e)
	{
		e.preventDefaults;
		var type = $(this).attr('data-type');
		var link = $(this).attr('data-link');

		if (type === 'agent-info' && validateSabreRegistrationFields())
		{
			$('.form-bootstrapWizard li').addClass('active');
			$('#account-setup-modal .tab-pane').removeClass('active').removeClass('show');
			$('#account-setup-modal #agent-info-tab').addClass('active').addClass('show');
			$('#previousRegisterButton').show();
			$('#nextRegisterButton').show();
			$('#submitRegisterButton-container').hide();
		}
		else if (type === 'zerorisk' && validateSabreRegistrationFields())
		{
			$('.form-bootstrapWizard li').addClass('active');
			$('#account-setup-modal .tab-pane').removeClass('active').removeClass('show');
			$('#account-setup-modal #zerorisk-tab').addClass('active').addClass('show');
			$('#previousRegisterButton').show();
			$('#nextRegisterButton').hide();
			$('#submitRegisterButton-container').show();
		}
		else if (type !== 'agent-link')
		{
			$('.form-bootstrapWizard li').removeClass('active');
			$('#account-setup-modal .tab-pane').removeClass('active').removeClass('show');
			$('#account-setup-modal #agent-link-tab').addClass('active').addClass('show');
			$(this).closest('li').addClass('active');
			$('#previousRegisterButton').hide();
			$('#nextRegisterButton').show();
			$('#submitRegisterButton-container').hide();
		}
	});

	$(document).on('click', '#account-setup-modal #printAgentInfo', function(e)
	{
		e.preventDefaults;
		var html = "";
		$('.agent-email-container p').each(function(e)
		{
			html += $(this).text();
		});
		window.open('mailto:test@example.com?subject=subject&body=' + encodeURIComponent(html));
	});

	$(document).on('click', '#account-setup-modal #nextRegisterButton', function(e)
	{
		e.preventDefaults;
		if (validateSabreRegistrationFields())
		{
			if ($('#agent-registion-modal #agent-info-tab').hasClass('active'))
			{
				$('.form-bootstrapWizard li').addClass('active');
				$('#agent-registion-modal .tab-pane').removeClass('active').removeClass('show');
				$('#agent-registion-modal #zerorisk-tab').addClass('active').addClass('show');
				$('#previousRegisterButton').show();
				$('#nextRegisterButton').hide();
				$('#submitRegisterButton-container').show();
			}
			else
			{
				$('.form-bootstrapWizard li').addClass('active');
				$('#agent-registion-modal .tab-pane').removeClass('active').removeClass('show');
				$('#agent-registion-modal #agent-info-tab').addClass('active').addClass('show');
				$('#previousRegisterButton').show();
				$('#nextRegisterButton').show();
				$('#submitRegisterButton-container').hide();
			}

		}
	});

	$(document).on('click', '#account-setup-modal #previousRegisterButton', function(e)
	{
		e.preventDefaults;
		if ($('#agent-registion-modal #zerorisk-tab').hasClass('active'))
		{
			$('.form-bootstrapWizard li').removeClass('active');
			$($('.form-bootstrapWizard li')[0]).addClass('active');
			$($('.form-bootstrapWizard li')[1]).addClass('active');
			$('#account-setup-modal .tab-pane').removeClass('active').removeClass('show');
			$('#account-setup-modal #agent-info-tab').addClass('active').addClass('show');
			$('#previousRegisterButton').show();
			$('#nextRegisterButton').show();
			$('#submitRegisterButton-container').hide();
		}
		else if ($('#agent-registion-modal #agent-info-tab').hasClass('active'))
		{
			$('.form-bootstrapWizard li').removeClass('active');
			$($('.form-bootstrapWizard li')[0]).addClass('active');
			$('#account-setup-modal .tab-pane').removeClass('active').removeClass('show');
			$('#account-setup-modal #agent-link-tab').addClass('active').addClass('show');
			$('#previousRegisterButton').hide();
			$('#nextRegisterButton').show();
			$('#submitRegisterButton-container').hide();
		}
	});

	$(document).on('click', '#account-setup-modal #submitRegisterButton', function(e)
	{
		e.preventDefaults;
		submitAccountSetupModal();
	});
	
	$("#projectSelector").select2().change(function(){
		redrawTable(true);
	});
});

function sendProposalConfirmed(event)
{
	var proposalId = $(this).attr('traveldocID');

	var emailCc = $("#proposalSendEmail #emailCc").val();
	var emailSubject = $("#proposalSendEmail #emailSubject").val();

	var editor1 = new FroalaEditor('#emailBody', {}, function()
	{
	});

	var emailBody = editor1.html.get().replace(/\<br>/g, '<br/>').replace(/\%24/g, '$');

	var sendProposalDialog = $("#send-proposal-modal"); // .closest(".modal");
	sendProposalDialog.modal('hide');

	startLoadingForeground();

	setTimeout(function()
	{

		$.ajax({
			type : "POST",
			url : '/api/secure/proposal/send',
			data : {
				traveldoc : proposalId,
				'e-key' : $("#travelDoc-user-e-key").val(),

				emailCc : emailCc,
				emailSubject : emailSubject,
				emailBody : emailBody
			}
		}).done(function(response)
		{
			stopLoadingForeground();

			if (response == "OK")
			{
				$(document).one('foregroundLoadingComplete', function()
				{

					Swal.fire({
						title : 'TravelDoc Sent!',
						text : 'The TravelDoc has been sent to the traveller',
						type : 'success'
					});
				});
			}
			else
			{
				Swal.fire({
					title : 'Error Sending',
					text : 'Unable to send proposal to the client. Server message:' + response + 'Please try again.',
					type : 'error'
				});
			}

		});
	}, 1000);
}

function validateEmail(email)
{
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}


function submitAccountSetupModal()
{

	$('#account-setup-modal #submitRegisterButton').html('<div class="spinner-border" role="status">\n' + '  <span class="sr-only">Loading...</span>\n' + '</div>');
	$('#account-setup-modal #submitRegisterButton').prop('disabled', true);
	var accountAgentInfo = getAccountAgentInfo();
	var json = LZString.compressToBase64(JSON.stringify(accountAgentInfo));
	var formData = new FormData();
	formData.append('json', json);
	$.ajax('/api/registerAccountAgents', {
		type : "POST",
		data : formData,
		processData : false,
		contentType : false
	}).done(function(data, textStatus, jqXHR)
	{
		if (!data.success)
		{
			$('#account-setup-modal #submitRegisterButton').html('Submit');
			$('#account-setup-modal #submitRegisterButton').prop('disabled', false);
		}
		else
		{
			var $modal = $('#account-setup-modal');
			$modal.modal('hide');
		}

	});
}

function getAccountAgentInfo()
{

	var accountInfo = {};
	accountInfo.agents = [];
	var queueZRAPIKey = $('#agent-registion-modal #queueZRAPIKey').val();
	var queueZRAPISecret = $('#agent-registion-modal #queueZRAPISecret').val();
	accountInfo.queueZRAPIKey = queueZRAPIKey;
	accountInfo.queueZRAPISecret = queueZRAPISecret;

	var agents = $('#agent-registion-modal .collapse-card');

	$(agents).each(function(e)
	{
		var agentInfo = {};
		var agentName = $(this).find('#agentName').val();
		var agentCode = $(this).find('#agentCode').val();
		var customerCode = $(this).find('#customerCode').val();
		agentInfo.agentName = agentName;
		agentInfo.agentCode = agentCode;
		agentInfo.customerCode = customerCode;
		accountInfo.agents.push(agentInfo);
	});
	return accountInfo;
}

function validateSabreRegistrationFields()
{
	var isValid = true;
	$('#agent-registion-modal .collapse-card').each(function(e)
	{
		var agentName = $(this).find('#agentName');
		var agentCode = $(this).find('#agentCode');
		var customerCode = $(this).find('#customerCode');

		var header = $(this).find('.collapse-card__heading');

		if (agentName.val().trim().length < 1)
		{
			isValid = false;
			$(agentName).addClass('is-invalid');
			$(header).css('background-color', 'rgb(255, 136, 136)');
		}
		else
		{
			$(agentName).removeClass('is-invalid');
			$(header).css('background-color', '');
		}
		if (agentCode.val().trim().length < 1)
		{
			isValid = false;
			$(agentCode).addClass('is-invalid');
			$(header).css('background-color', 'rgb(255, 136, 136)');
		}
		else
		{
			$(agentCode).removeClass('is-invalid');
			$(header).css('background-color', '');
		}
		if (customerCode.val().trim().length < 1)
		{
			isValid = false;
			$(customerCode).addClass('is-invalid');
			$(header).css('background-color', 'rgb(255, 136, 136)');
		}
		else
		{
			$(customerCode).removeClass('is-invalid');
			$(header).css('background-color', '');
		}
	});
	return isValid;
}

function addNewAgentCard()
{
	$.ajax({
		type : "GET",
		url : "/application/include/sabreAgentInitSetupCard.jsp",
		success : function(text)
		{
			var $card = $(text);
			var randomID = idGen.getId();

			$card = $card.attr('data-id', randomID);

			$('#agent-link-tab').append($card);

			var cards = $('#agent-link-tab .collapse-card.card');
			if (cards.length > 1)
			{
				$('.agentTrashButton').show();
			}
			$('.collapse-card.card[data-id="' + randomID + '"]').paperCollapse();
		}
	});
}

function loadTaskDescription(element, traveldocID, elementID)
{

	$(element).html('<span class="text-muted">Loading Task Information...</span>');
	$(element).html('<span class="text-muted">There was no Task Information.</span>');
}

function displayTravelDocNotesAction(e)
{
	var travelDocId = $(e.target).attr("data-id");

	displayTaskNotesSideBar(travelDocId);
}

function displayTaskNotesSideBar(travelDocId)
{

	$('.cd-panel').toggleClass('is-visible');
	$('div.sideBar-backDrop').show();

	$('.cd-panel-content').empty()

	var $modal = $('.cd-panel-content');

	$modal.load('/application/dialog/proposalNotes.jsp', {
		travelDocId : travelDocId
	}, function()
	{
		$modal.find('.closeManagementDialog').click(function()
		{

			$('.cd-panel').toggleClass('is-visible');
			$('div.sideBar-backDrop').hide();
			redrawTable();
			updateCommentNotificationCounter();
		});
	});

	return $modal;
}

function displayDeleteTaskAction(event)
{
	var travelDocId = $(event.target).attr("data-id");
	$("#travelDocDeleteModal .taskDeleteConfimed").attr("data-id", travelDocId);
}

function taskDeleteConfimed(event)
{
	$("#travelDocDeleteModal").modal("hide");
	var travelDocId = $("#travelDocDeleteModal .taskDeleteConfimed").attr("data-id");
	var row = $('tr[data-id="' + travelDocId + '"]');

	startLoadingForeground();
	$.ajax({
		url : '/api/traveldoc/delete',
		data : {
			travelDocId : travelDocId
		}
	}).done(function(data, textStatus, jqXHR)
	{
		redrawTable();
		stopLoadingForeground();
	});
}

function generateAdvanceFilterString()
{
	var filterString = "";
	if ($('.filter-container').is(':visible'))
	{
		var rows = $('.filter-container .row');
		$(rows).each(function()
		{
			if ($(this).find('.col-auto').length !== 4)
			{
				if ($(this).hasClass('filter-row-join'))
				{
					var sepRow = $(this).find('.dropdown button').attr('data-filtervalue');
					if (sepRow === '')
					{
						filterString = '';
					}
					else
					{
						filterString += ' ' + sepRow + ' ';
					}
				}
				else
				{
					filterString = '';
				}
			}
			else
			{
				var colName = $(this).find('.filter-col-name button').attr('data-filtervalue');
				var seperatorType = $(this).find('.filter-col-seperator button').attr('data-seperatortype');
				var valueText = $(this).find('.filter-col-value input').val();
				if (colName === '' || seperatorType === '' || valueText === '')
				{
					filterString = '';
				}
				else
				{
					if (seperatorType === "like")
					{
						filterString += "sp." + colName + ' ' + seperatorType + ' "%' + valueText + '%"';
					}
					else if (colName.includes("ATTRDATE_"))
					{
						if (valueText.includes("--"))
						{
							valueText = valueText.replace(' -- ', '" and "');
						}
						if (seperatorType === "equals")
						{
							filterString += "date(sp." + colName + ') = "' + valueText + '"';
						}
						else
						{
							filterString += "sp." + colName + ' ' + seperatorType + ' "' + valueText + '"';
						}
					}
					else
					{
						filterString += "sp." + colName + ' ' + seperatorType + ' "' + valueText + '"';
					}
				}
			}
		});
	}
	return filterString;
}

function getProjectId(){
	return $('#projectSelector').val();
}

function appendFilterCols(type, ele)
{
	var filterCol = $(ele).find('.filter-col-seperator');
	var filterVal = $(ele).find('.filter-col-value');
	if (type === "date")
	{
		var col = $('<div class="col-auto filter-col-seperator">');
		if (filterCol != null)
		{
			$(filterCol).remove();
		}
		if (filterVal != null)
		{
			$(filterVal).remove();
		}
		var dropdown = "<div class=\"dropdown filter-seperator-drop\">\n" + "    <button class=\"btn btn-default dropdown-toggle\" type=\"button\" data-seperatorType=\"\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Separator</button>\n" + "    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">" + "<a class=\"dropdown-item\" data-seperatorType=\"between\" href=\"#\">Between</a>" + "<a class=\"dropdown-item\" data-seperatorType=\"equals\" href=\"#\">Equals</a>" + "</div></div>";
		col = $(col).append(dropdown);
		return col;
	}
	else if (type === "text")
	{
		var col = $('<div class="col-auto filter-col-seperator">');
		if (filterCol != null)
		{
			$(filterCol).remove();
		}
		if (filterVal != null)
		{
			$(filterVal).remove();
		}
		var dropdown = "<div class=\"dropdown filter-seperator-drop\">\n" + "    <button class=\"btn btn-default dropdown-toggle\" type=\"button\" data-seperatorType=\"\" data-filterValue=\"\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Separator</button>\n" + "    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">" + "<a class=\"dropdown-item\" data-seperatorType=\"equals\" href=\"#\">Equals</a>" + "<a class=\"dropdown-item\" data-seperatorType=\"like\" href=\"#\">Like</a>" + "</div></div>";
		col = $(col).append(dropdown);
		return col;
	}
	else if (type === "equals")
	{
		var col = $('<div class="col-auto filter-col-value">');
		if (filterVal != null)
		{
			$(filterVal).remove();
		}
		var filterType = $(filterCol).parent().find('.filter-drop button').attr('data-filtertype');
		if (filterType === "text")
		{
			var input = $('<input type="text" class="form-control" aria-label="Enter value..." placeholder="Enter value...">');
			col = $(col).append(input);
		}
		else
		{
			var input = $('<input type="text" class="form-control advance-form-datepicker">');
			col = $(col).append(input);
		}
		return col;
	}
	else if (type === "between")
	{
		var col = $('<div class="col-auto filter-col-value">');
		if (filterVal != null)
		{
			$(filterVal).remove();
		}

		var input = $('<input type="text" class="form-control advance-form-datepicker" style="width: 12em;">');
		col = $(col).append(input);
		return col;
	}
	else if (type === "like")
	{
		var col = $('<div class="col-auto filter-col-value">');
		if (filterVal != null)
		{
			$(filterVal).remove();
		}

		var input = $('<input type="text" class="form-control" aria-label="Enter value..." placeholder="Enter value...">');
		col = $(col).append(input);
		return col;
	}
}

function newAdvanceFilter()
{

	if (columnFilters.length < 1)
	{
		$.ajax({
			url : '/api/load_dashboard_filter_columns',
			method : 'GET'
		}).done(function(data, textStatus, jqXHR)
		{
			columnFilters = data;
			setupAdvanceFilter();
		});
	}
	else
	{
		setupAdvanceFilter();
	}

}

function setupAdvanceFilter()
{
	$('#travelDocs_filter_save').show();
	var containerString = $('.filter-container').html();
	if (containerString.trim().length != 0)
	{
		var dividerSelect = "<div class=\"dropdown\">\n" + "    <button class=\"btn btn-default dropdown-toggle\" type=\"button\"\n" + "            data-toggle=\"dropdown\" aria-haspopup=\"true\" data-filterValue=\"and\" aria-expanded=\"false\">\n" + "        And\n" + "    </button>\n" + "    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">\n" + "        <a class=\"dropdown-item\" value=\"and\" href=\"#\">And</a>\n" + "        <a class=\"dropdown-item\" value=\"or\" href=\"#\">Or</a>\n" + "    </div>\n" + "</div>";

		var row1 = $('<div class="row align-items-center mb-3 filter-row-join">');
		var col1 = $('<div class="col-auto">');
		col1 = $(col1).append(dividerSelect);
		row1 = $(row1).append(col1);
		$('.filter-container').append(row1);
	}

	var row = $('<div class="row align-items-center mb-3 filter-row">');
	var col = $('<div class="col-auto filter-col-name">');
	var deleteButton = $('<div class="col-auto"><button type="button" style="min-width: 38px;" class="btn btn-danger delete-advance-filter"><i class="fas fa-times"></i></button></div>');

	var dropdown = "<div class=\"dropdown filter-drop\">\n" + "    <button class=\"btn btn-default dropdown-toggle\" type=\"button\" data-filterType=\"\" data-filterValue=\"\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Filter Row</button>\n" + "    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">";

	for (var i = 0; i < columnFilters.length; i++)
	{
		var filter = columnFilters[i];
		if (!filter.includes("Headline"))
		{
			dropdown += "<a class=\"dropdown-item\" data-filterType=\"" + generateFilterInfo(filter)["type"] + "\" data-fullname=\"" + filter + "\" href=\"#\">" + generateFilterInfo(filter)["name"] + "</a>";
		}
	}

	dropdown += "</div>\n" + "</div>";

	col = $(col).append(dropdown);
	row = $(row).append(col);
	row = $(row).append(deleteButton);
	$('.filter-container').append(row);
}

function generateFilterInfo(filter)
{

	if (filter.includes("ATTR_"))
	{
		return {
			"name" : filter.replace("ATTR_", ""),
			"type" : "text"
		};
	}
	else if (filter.includes("ATTRDATE_"))
	{
		return {
			"name" : filter.replace("ATTRDATE_", ""),
			"type" : "date"
		};
	}
	else if (filter.includes("ATTRDROP_"))
	{
		return {
			"name" : filter.replace("ATTRDROP_", ""),
			"type" : "text"
		};
	}
	else
	{
		return {
			"name" : filter,
			"type" : "unknown"
		};
	}

}

function removeAdvanceFilter(row)
{
	if ($(row).prev().hasClass('filter-row-join'))
	{
		$(row).prev().remove();
	}
	$(row).remove();

	if ($('.filter-container').is(':empty'))
	{
		$('#travelDocs_filter_save').hide();
	}
}

function filterSettingVisibility(id, visible)
{
	var columnId = item.columnId;
	var columnVisible = item.visible;
	var columnSearch = item.search;
	var $setting = $('.tableColumnHolder').find('[data-id="' + id + '"]');
	var $thead = $('#matchedTravelDocs').find('#' + id);
	var $tbody = $('#matchedTravelDocs').find('[data-id="' + id + '"]')
	$thead.find('input').val(columnSearch);
	$setting.data("visible", visible);
	if (visible)
	{
		$setting.addClass('toggle-vis');
		$thead.show();
		$tbody.show();
	}
	else
	{
		$setting.removeClass('toggle-vis');
		$thead.hide();
		$tbody.hide();
	}
}

function generateFilterVisibilitySetting()
{
	if (localStorageTest())
	{
		try
		{
			var columnJson = localStorage.getItem("project-column-setting");
			if (columnJson != null)
			{
				var columnSetting = JSON.parse(columnJson);
				columnSetting.forEach(function(item)
				{
					filterSettingVisibility(item);
				})
			}
		}
		catch (e)
		{
			localStorage.removeItem("project-column-setting")
		}
	}

}

function buildFilterSettingsJson()
{
	var columnsJson = [];
	$('#project-dashboard').find('.tableColumnHolder a').each(function()
	{
		var columnJson = {};
		var columnId = $(this).data('id');
		var columnVisible = $(this).data('visible');
		columnJson.columnId = columnId;
		columnJson.visible = columnVisible;
		columnJson.search = $(this).val();
		columnsJson.push(columnJson);
	});
	console.log(columnsJson);
	if (localStorageTest())
	{
		localStorage.setItem("project-column-setting", JSON.stringify(columnsJson));
	}
}

function localStorageTest()
{
	var test = "testing-asdfasdf";
	try
	{
		localStorage.setItem(test, test);
		localStorage.getItem(test);
		localStorage.removeItem(test);
		return true;
	}
	catch (e)
	{
		return false;
	}
}

function doneTyping()
{
	runDataTable()
}

// For the table
var TableData = function()
{
	$.fn.dataTable.ext.errMode = function(event, settings, techNote, tn)
	{
		console.log('An error has been reported by DataTables: ', techNote, tn);
	};

	var tableLength = 100; // localStorage['tableLenght'] ?
	// localStorage['tableLenght'] : 100;
	var currentDashboardDataRequest = undefined;
	var filter = localStorage['filters'] ? JSON.parse(localStorage["filters"]) : [];
	var proposalsToShow = tableLength;
	$.each(filter, function(i, e)
	{
		$('.status-bucket-' + e).addClass('active');
	});
	endDate = moment();
	startDate = moment().subtract(12, 'months');

	var runSetupPlaceholders = function()
	{
		$('input, textarea').placeholder();
	};

	// var customFields = JSON.parse($('#custom-fields-data').text());

	runDataTable = function()
	{
		var pageIndex = 0;
		if ($('.dt-pagination-btn.selected').length > 0)
		{
			pageIndex = $('.dt-pagination-btn.selected').attr('data-table-idx');
		}
		var itemsPerPage = $('#travelDocsDisplaySelector').val();
		var start = (pageIndex * itemsPerPage);

		$.ajax({
			url : '/api/dashboard',
			type : "POST",
			data : {
				"columns" : getTableColumns(),
				"order" : getOrderedColumn(),
				"start" : start,
				"length" : itemsPerPage,
				"endDate" : endDate.format('YYYY-MM-DD ZZ'),
				"startDate" : startDate.format('YYYY-MM-DD ZZ'),
				"statusFilter" : getStatusParam(),
				"advanceFilter" : generateAdvanceFilterString(),
				"datetime" : new Date().getTime(),
				"project" : getProjectId(),
			},
			error : function(jq, st, err)
			{
			},
			success : function(result)
			{

				if (result.length == 0)
				{
					// temp there was an issue
				}
				else
				{
					if (result.data.length < 1 && !($('tr.travelDocSearchRow').is(':visible')))
					{
						showLoader(false, "No task found");
						$('.matchedTravelDocs').html('');
					}
					else if (result.data.length < 1 && $('tr.travelDocSearchRow').is(':visible'))
					{
						$('.table.table-striped tbody').empty();
						showLoader(false, "No task found");
					}
					else
					{
						// There are row to put in
						var searchRow;
						if ($('tr.travelDocSearchRow').is(':visible'))
						{
							searchRow = $('tr.travelDocSearchRow').clone();
						}
						$.get('/application/include/dashboardTable.jsp', function(table)
						{
							var contentInto = table;

							var columnsContent = generateActiveColumnHeads();
							contentInto = contentInto.replace(new RegExp('<:dashboardMainHeader>', 'g'), columnsContent.html);

							var tableBodyContent = "";
							var columns = columnsContent.columns;
							for (var i = 0; i < result.data.length; i++)
							{
								var data = result.data[i];

								tableBodyContent += '<tr data-id="' + data.attr_externalid + '" data-eleid="' + data.element_id + '" ">';
								for (var c = 0; c < columns.length; c++)
								{
									var columnName = columns[c];
									var visible = $('.tableColumnHolder').find('[data-id="' + columnName + '"]').data('visible');
									if (columnName === "attr_externalid")
									{
										tableBodyContent += "<td class=\"leftHolder\" data-id=\"" + columnName + "\" style=\"text-align: center; display: " + (visible ? "table-cell" : "none") + ";\">" + data[columnName] + "<span class=\"quick-view-button\">Quick View</span></td>";
									}
									else
									{
										var info = "";
										if (data[columnName] && data[columnName].length > 0)
										{
											info = data[columnName];
										}
										else
										{
											// if (columnName === "clientName")
											// {
											// info = "John";
											// } else if (columnName ===
											// "clientLastName") {
											// info = "smith";
											// } else if (columnName ===
											// "clientEmail") {
											// info =
											// "john.smith@covermore.com";
											// }
											info = "Not Available";
										}
										tableBodyContent += "<td data-id=\"" + columnName + "\" style=\"display: " + (visible ? "table-cell" : "none") + ";\">" + info + "</td>";
									}

								}
								tableBodyContent += "<td class=\"rightHolder\">" + getOptionsButton(data) + "</td></tr>";

							}
							var activeElement = document.activeElement;
							contentInto = contentInto.replace(new RegExp('<:dashboardMainBody>', 'g'), tableBodyContent);

							$('.matchedTravelDocs').html(contentInto);

							$("tr:odd").find('td.leftHolder').css("background-color", "rgba(0, 0, 0, 0.05)");
							$("tr:odd").find('td.rightHolder').css("background-color", "rgba(0, 0, 0, 0.05)");

							if (searchRow != null)
							{
								showSearchRow(searchRow, activeElement);
							}


							showLoader(false, result.data.length + (result.data.length > 1 ? " Tasks displayed " : " Task displayed"));

							updatePagination(result, pageIndex, itemsPerPage);

						});
					}

				}
			}
		});

	};
	return {
		// main function to initiate template pages
		init : function()
		{
			if (trialExpired)
			{
				return;
			}
			loadTravelDocCounters();
			runDataTable();
			runSetupPlaceholders();
		}
	};
}();

function updatePagination(result, page, itemsPerPage)
{
	var listLength = 3;
	page = parseInt(page);
	var maxPages = Math.ceil(result.recordsFiltered / itemsPerPage);
	var $paginationHtml = $('<div class="pagination">');

	var paginationButtonsLength = Math.min(maxPages, listLength)

	var listStart = 0;
	if (page > (Math.floor(listLength / 2)))
	{
		listStart = page - (Math.floor(listLength / 2));
	}
	if ((page + Math.floor(listLength / 2)) > (maxPages - 1))
	{
		if ((maxPages - listLength) > 0)
		{
			listStart = (maxPages - listLength);
		}
	}

	for (var i = 0; i < paginationButtonsLength; i++)
	{
		var index = (listStart + i)
		index = parseInt(index);
		var $button = $('<button class="mdl-button dt-pagination-btn ' + (index == page ? "selected mdl-button--raised mdl-button--colored" : "") + '" data-table-idx="' + index + '">' + (index + 1) + '</button>')
		$paginationHtml.append($button);
	}

	if (page > 0)
	{
		$paginationHtml.prepend('<button class="mdl-button dt-pagination-prev-btn"><</button>')
	}

	if (page < (maxPages - 1))
	{
		$paginationHtml.append('<button class="mdl-button dt-pagination-next-btn">></button>')
	}

	$('#itineraryTableFooterText').append($paginationHtml);
}

function showLoader(show, text)
{
	if (show)
	{
		$('#itineraryTableFooterText').html('<span class="dt-records-count"> <i class="fa fa-spinner fa-spin "></i> Loading... </span>');
	}
	else
	{
		$('#itineraryTableFooterText').html('<span class="dt-records-count">' + text + '</span>');
	}
}

function checkRowHeight()
{
	var $search = $('.dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow');

	if (searchHidden == true)
	{
		var rowHeight = $('.dataTables_scrollHeadInner > table > thead > tr:first').height();
		$('.DTFC_RightHeadWrapper').height(rowHeight - 1);
		$('.DTFC_RightHeadWrapper > table > thead > tr:first').height((rowHeight));
		$('.DTFC_LeftHeadWrapper').height(rowHeight);
		$('.DTFC_LeftHeadWrapper > table > thead > tr:first').height((rowHeight));
		$('.DTFC_RightBodyWrapper').attr('style', 'margin-top: 1px !important')
		$('.DTFC_LeftBodyWrapper').attr('style', 'margin-top: 1px !important')
	}
	else
	{
		var rowHeight = $('.dataTables_scrollHeadInner > table > thead > tr:first').height();
		var searchHeight = $('.dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow').height();
		var headerheight = $('.dataTables_scrollHead').height();
		$('.DTFC_RightHeadWrapper').height(rowHeight - 1);
		$('.DTFC_RightHeadWrapper > table > thead > tr:first').height((rowHeight));
		$('.DTFC_LeftHeadWrapper').height(rowHeight);
		$('.DTFC_LeftHeadWrapper > table > thead > tr:first').height((rowHeight));
		$('.DTFC_RightBodyWrapper').attr('style', 'margin-top: ' + (searchHeight + 1) + 'px !important')
		$('.DTFC_LeftBodyWrapper').attr('style', 'margin-top: ' + (searchHeight + 1) + 'px !important')
	}

	var rowWidth = $('.dataTables_scrollHeadInner > table > thead > tr:first > th.first').width();
	$('.DTFC_LeftHeadWrapper').width(rowWidth);
	$('.DTFC_LeftBodyLiner > table').width(rowWidth);


	travelDocsTable.columns.adjust().draw();

}

function loadTravelDocCounters()
{
	if (currentDashboardDataRequest != undefined)
	{
		currentDashboardDataRequest.abort();
	}

	currentDashboardDataRequest = $.ajax({
		url : '/api/dashboard/summary',
		method : 'post',
		data : {
			"datetime" : new Date().getTime()
		}
	}).done(function(data, textStatus, jqXHR)
	{
		currentDashboardDataRequest = undefined;


		var statusButtons = "<div class=\"btn-group\" role=\"group\" aria-label=\"Basic example\" style=\"margin: auto;height: auto\">";
		$.each(data, function(status, val)
		{
			if (val > 0)
			{
				var icon;
				var name;
				console.log(status.toLowerCase());
				switch (status.toLowerCase())
				{
					case "pending":
						icon = "fal fa-exclamation-triangle";
						name = "TravelDoc Not Sent";
						break;
					case "traveldoc-not-sent":
						icon = "fal fa-exclamation-triangle";
						name = "TravelDoc Not Sent";
						break;
					case "duetotravel":
						icon = "far fa-plane-departure";
						name = "Due to Travel";
						break;
					case "traveling":
						icon = "fal fa-plane";
						name = "Traveling";
						break;
					case "complete":
						icon = "far fa-plane-arrival";
						name = "Complete";
						break;
					case "needsAction":
						icon = "fal fa-reply";
						name = "Attention";
						break;
					default:
						icon = "fal fa-question-circle";
						name = "Unknown";
						break;
				}
				if (status.toLowerCase() != "total" && status.toLowerCase() != "template")
				{
					statusButtons += "<button type=\"button\" class=\"btn btn-light travelDocStatusButton\" data-status=\"" + status + "\" style=\"display: block;height: 66px !important;\"><i class=\"" + icon + " fa-lg\" style=\"font-size: 24px;\"></i><span class=\"nav-icon-title\" style=\"display: block;line-height: 1;margin: 0;padding-top: 6px;text-transform: capitalize;letter-spacing: 0;\">" + val + " " + name + "</span></button>";
				}
			}
		});
		statusButtons += "</div>";
		$('#mobileStats').html(statusButtons);
	});
}

function countColumnFilters()
{
	var count = 0;
	travelDocsTable.columns().every(function(i, tableLoop, columnLoop)
	{
		var dtColumn = this;
		if (dtColumn.visible() && dtColumn.search() != '')
		{
			++count;
		}
	});
	return count;
}

function rebuildColumnFilters(force)
{
	var $thead = $('.dataTables_scrollHeadInner > table > thead');
	var $search = $('.dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow');

	if ($search.size() > 0)
	{
		$('.DTFC_ScrollWrapper').height("-=47");
		$('.dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow').remove();
		$('.DTFC_RightHeadWrapper > table > thead.searchHead > tr.searchRow').remove();
		$('.DTFC_LeftHeadWrapper > table > thead.searchHead > tr.searchRow').remove();
		$search.remove();
		searchHidden = true;

		checkRowHeight();
	}

	if (force == true || countColumnFilters() > 0)
	{
		var $searchHead = $('<thead>').attr('class', 'searchHead');
		var $searchRow = $("<tr>").attr('class', 'searchRow');
		travelDocsTable.columns().every(function(i, tableLoop, columnLoop)
		{
			var dtColumn = this;
			if (dtColumn.visible())
			{
				var $cell = $("<th>");
				if (i > 0)
				{
					var colDef;
					for (var w = 0; w < tableColumnDefs.length; w++)
					{
						if (tableColumnDefs[w].title == $(dtColumn.header()).text())
						{
							colDef = tableColumnDefs[w];
						}
						else if (tableColumnDefs[w].title.indexOf($(dtColumn.header()).text().htmlEscaped()) !== -1)
						{
							colDef = tableColumnDefs[w];
						}
					}
					var $search = undefined;

					// $(dtColumn.header()).text()

					if (colDef.ciFilterType == 'text')
					{
						$search = $('<input type="text" class="form-control input-sm"/>');
						$search.val(dtColumn.search());
					}
					else if (colDef.ciFilterType == 'dropdown')
					{
						$search = $('<select class="form-control input-sm"/>');
						$search.append($("<option>").attr('value', '').attr('selected', 'selected').text('Filter...'));
						$(colDef.ciDataValues).each(function(i, e)
						{
							var $opt = $("<option>");
							$opt.text(e.label);
							$opt.attr('value', e.value);
							$search.append($opt);
						});

						$search.val(dtColumn.search());
					}
					else if (colDef.ciFilterType == 'clear')
					{
						$search = $("<button>").attr('class', 'btn btn-sm btn-danger clearColumnFilters columnFiltersEnabled').append($('<i class="fa fa-eraser">'), " Clear Filters");

						$search.val(dtColumn.search());
					}

					if ($search != undefined)
					{
						var label = $(dtColumn.header()).text();
						$search.attr('placeholder', 'Filter...');

						$cell.append($search);
					}
				}

				$searchRow.append($cell);
				$searchHead.append($searchRow);
			}
		});


		var $smallSearchHeadRight = $('<thead>').attr('class', 'searchHead');
		var $smallSearchRowRight = $("<tr>").attr('class', 'searchRow');
		var $cellRight = $("<th>").append($("<button>").attr('class', 'matchedProposals_clearColumnFilters btn btn-sm btn-danger clearColumnFilters columnFiltersEnabled').append($('<i class="fa fa-eraser">'), " Clear Filters"));
		$smallSearchRowRight.append($cellRight);
		$smallSearchHeadRight.append($smallSearchRowRight);


		var $smallSearchHead = $('<thead>').attr('class', 'searchHead');
		var $smallSearchRow = $("<tr>").attr('class', 'searchRow');
		var $cell = $("<th>").attr('style', 'height:48px');
		$smallSearchRow.append($cell);
		$smallSearchHead.append($smallSearchRow);

		$('.dataTables_scrollHeadInner > table ').append($searchHead);
		$('.DTFC_RightHeadWrapper > table ').append($smallSearchHeadRight);
		$('.DTFC_LeftHeadWrapper > table ').append($smallSearchHead);

		searchHidden = false;

		$('.DTFC_ScrollWrapper').height("+=47");
		checkRowHeight();


		$(".searchRow").on("click", ".clearColumnFilters", function(e)
		{
			travelDocsTable.columns().search('');
			rebuildColumnFilters(true);

			redrawTable(true);
		});

		// $(".matchedProposals_clearColumnFilters").on("click", function(e)
		// {
		// travelDocsTable.columns().search('');
		// rebuildColumnFilters(true);
		// redrawTable();
		// });
	}

	var count = countColumnFilters();
	if (count > 0)
	{
		var title = count + " Filters";
		$("#filterLabel").text(title);
		$(".columnFiltersEnabled").show();
	}
	else
	{
		$("#filterLabel").text("Filter");
		$(".columnFiltersEnabled").hide();
	}
}

function redrawTable(resetPageNum)
{

	// Display loading message to user
	// showNotificationMessage(true, "Working...");
	// showLoadingCog(true);
	// NProgress.start();
	showScrollButtons();

	if (redrawTableDelay != undefined)
	{
		clearTimeout(redrawTableDelay);
	}
	redrawTableDelay = setTimeout(function()
	{
		runDataTable();
	}, 150);

}

function showScrollButtons()
{
	if ($('.dataTables_scrollHead').width() >= $('.table-full-width.dataTable').width())
	{
		$('.scroll-table-left').hide()
		$('.scroll-table-right').hide();
	}
	else
	{
		$('.scroll-table-left').show();
		$('.scroll-table-right').show();
	}
}

function setupVisibility()
{
	if ($('.column-visible-button').length > 0)
	{
		$('.column-visible-button').remove();
	}

	// <a class="dropdown-item toggle-vis" data-column="0" href="#"><i class="fa
	// check" ></i>Asset ID</a>

	var columnDiv = $('#columns-list');

	var columnButton = $('<button class="btn btn-default dropdown-toggle column-visible-button" type="button" id="dropdownMenuButton"' + 'data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">');
	columnButton.append("Visible columns");
	columnDiv.append(columnButton);

	var columnFloatMenu = $('<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">');
	var menuColumn = $('<div>')
	columnFloatMenu.append(menuColumn);
	columnDiv.append(columnFloatMenu);
	if (travelDocsTable.columns().length > 0)
	{
		for (var i = 1; i < travelDocsTable.columns()[0].length - 1; i++)
		{
			if (($(travelDocsTable.column(i).header()).html()).match("undefined"))
			{
				var invalidColumn = travelDocsTable.column(i);
				invalidColumn.visible(false)
			}
			var columnName = $(travelDocsTable.column(i).header()).html();
			var tagged = $('<a>').addClass('column-tag dropdown-item toggle-vis');
			var column = $(tagged).attr("data-column", i);
			var key = $('span.custom-field', travelDocsTable.column(i).header()).data("key");

			if (key && !customFields[key].on)
			{
				continue;
			}
			if (travelDocsTable.column(i).visible())
			{
				column.html('<i class="fa check" ></i>' + columnName);
			}
			else if (!travelDocsTable.column(i).visible())
			{
				column.html(columnName);
			}
			if (i == 8)
			{
				menuColumn = $('<div>');
				columnFloatMenu.append(menuColumn);
			}
			menuColumn.append(column);
			column.on("click", function()
			{
				var thisColumn = travelDocsTable.column($(this).attr('data-column'));
				thisColumn.visible(!thisColumn.visible());
				$(this).toggleClass("active", thisColumn.visible());

				checkRowHeight();
				rebuildColumnFilters();
			});
		}
	}


	columnFloatMenu.on('click', function(event)
	{
		event.stopPropagation();
	});

	columnButton.on("click", function()
	{
		columnFloatMenu.toggle();

		setTimeout(function()
		{
			$(window).one('click', function()
			{
				columnFloatMenu.toggle();
			});
		}, 100);
	});
}

$.fn.textWidth = function(text, font)
{

	if (!$.fn.textWidth.fakeEl)
		$.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);

	$.fn.textWidth.fakeEl.text(text || this.val() || this.text() || this.attr('placeholder')).css('font', font || this.css('font'));

	return $.fn.textWidth.fakeEl.width();
};

function isTouchSupported()
{
	// return ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera
	// Mini/i.test(navigator.userAgent) );
	return 'ontouchstart' in document.documentElement;
};

function escapeHtml(str)
{
	if (str != undefined)
	{
		return str.htmlEscaped();
	}
	else
	{
		"";
	}
}

function formatCustomField(data, type, forSort)
{
	if (data == '')
	{
		return data;
	}
	if (forSort)
		return data;
	if (type == 'date')
		try
		{
			var _d = moment(data);
			if (_d.isValid())
				return _d.format(dateFormatPattern);
			return "";
		}
		catch (e)
		{
			return "";
		}
	if (type == 'currency')
	{
		try
		{
			return '<span class="moneytaryValue"> ' + parseFloat(data).formatMoney() + '</span>';
		}
		catch (e)
		{
			return "";
		}
	}
	return data.htmlEscaped();
}

function getTableColumns()
{
	var columnHolder = $('.tableColumnHolder');
	var columns = $(columnHolder).find('a');
	var jsonObj = [];

	$(columns).each(function(i)
	{
		var fieldName = '.travelDocsSearchInput.' + $(this).data('id');
		var searchField = $(fieldName);
		if (searchField.length < 1)
		{
			searchField = null;
		}
		else if ($(searchField).is('select'))
		{
			$(searchField).attr('data-value', $(searchField).val());
		}
		var myObject = {};
		var subObject = {};
		myObject.data = $(this).data('id');
		myObject.name = "";
		subObject.value = searchField != null ? $(searchField).val() : "";
		subObject.regex = "false";
		myObject.search = subObject;
		jsonObj.push(JSON.stringify(myObject));
	});
	return JSON.stringify(jsonObj);

}

function generateActiveColumnHeads()
{
	var columnHolder = $('.tableColumnHolder');
	var columns = $(columnHolder).find('a');
	var myObject = {};
	var columnArr = [];
	var tableHeader = "";
	tableHeader += "<tr>";
	$(columns).each(function(i)
	{
		var direction = "";
		var name = $(this).data('id');
		if (datatablecolumnOrderRaw.filter(function(e)
		{
			return e.name === name;
		}).length > 0)
		{
			/* vendors contains the element we're looking for */
			var result = datatablecolumnOrderRaw.filter(function(e)
			{
				return e.name === name;
			});
			direction = result[0].dir;
		}
		if ($(this).data('id') === "attr_externalid")
		{
			tableHeader += "<th class=\"leftHolder\" " + (direction !== "" ? "data-direction=\"" + direction + "\"" : "") + " id=\"" + $(this).data('id') + "\" style=\"cursor: pointer; display: " + ($(this).data('visible') ? "table-cell" : "none") + ";\">" + "<span>" + $(this).text() + "</span>" + (direction !== "" ? (direction === "asc" ? "<span class=\"dashboard-filter-direction-arrow\" style=\"float: right;height: 14px; position: absolute;\"><i class=\"fas fa-chevron-up\" style=\"font-size: 12px;color: white; right: 0;\"></i></span>" : "<span class=\"dashboard-filter-direction-arrow\" style=\"float: right;height: 14px; position: absolute; right: 0;\"><i class=\"fas fa-chevron-down\" style=\"font-size: 12px;color: white;\"></i></span>") : "") + "</th>";
		}
		else
		{
			tableHeader += "<th class=\"dashboard-table-header\" " + (direction !== "" ? "data-direction=\"" + direction + "\"" : "") + " id=\"" + $(this).data('id') + "\" style=\"margin-right: 20px; cursor: pointer; position: relative; display: " + ($(this).data('visible') ? "table-cell" : "none") + ";\"><span>" + $(this).text() + "</span>" + (direction !== "" ? (direction === "asc" ? "<span class=\"dashboard-filter-direction-arrow\" style=\"float: right;height: 14px;position: absolute;right: 0;\"><i class=\"fas fa-chevron-up\" style=\"font-size: 12px;color: white; right: 0;\"></i></span>" : "<span class=\"dashboard-filter-direction-arrow\" style=\"float: right;height: 14px; position: absolute; right: 0;\"><i class=\"fas fa-chevron-down\" style=\"font-size: 12px;color: white;\"></i></span>") : "") + "</th>";
		}

		columnArr.push($(this).data('id'));
	});
	tableHeader += "<th class=\"rightHolder\"></th></tr>";
	myObject.columns = columnArr;
	myObject.html = tableHeader;
	return myObject;
}

function getOrderedColumn()
{
	// var columnHolder = $('.tableColumnHolder');
	// var columns = $(columnHolder).find('a');
	var columnHolder = $('.tableColumnHolder');
	var columns = $(columnHolder).find('a');
	var jsonObj = [];
	if (datatablecolumnOrder.length < 1)
	{
		var myObject = {};
		myObject.column = 0;
		myObject.dir = "asc";
		jsonObj.push(JSON.stringify(myObject));
	}
	else
	{
		jsonObj = datatablecolumnOrder;
	}
	return JSON.stringify(jsonObj);

}

function getStatusParam()
{

	var statusButtons = $('.travelDocStatusButton.active');
	var status = "";
	$(statusButtons).each(function(e)
	{
		status += $(this).data('status') + ",";
	});
	return status;
}

function getSearchedColumn()
{
	// var columnHolder = $('.tableColumnHolder');
	// var columns = $(columnHolder).find('a');
	var jsonObj = [];
	var myObject = {};
	myObject.column = 27;
	myObject.dir = "desc";
	jsonObj.push(JSON.stringify(myObject));
	return JSON.stringify(jsonObj);

}

function showSearchRow(data, activeElement)
{
	var table = $('.matchedTravelDocs').find('table');
	if ($(table).find('.travelDocSearchRow').length > 0)
	{
		$('.travelDocSearchRow').remove();
		runDataTable();
	}
	else
	{
		var thead = $(table).find('thead');
		var tr = $(thead).find('tr');

		var ele = $(tr).clone();
		$(ele).addClass('travelDocSearchRow');
		var oldSearch = data != null ? $(data).find("th") : null;

		$(ele).find("th").each(function(cell)
		{
			$(this).empty();
			if ($(this).hasClass('rightHolder'))
			{
				$(this).html('<btn class="btn btn-default fuse-ripple-ready" id="travelDocsSearchClear" style="background: white;width: 100%; border-right: 1px solid lightgray;"><i class="qc qc-close"></i><span style="text-transform: capitalize"> Clear</span></btn>')
			}
			else
			{
				$(this).css('padding', '1.6rem 1.2rem');

				// <input type="date" data-date-inline-picker="false"
				// data-date-open-on-focus="true" />
				var rowID = $(this).attr('id').toLowerCase();
				if (rowID.indexOf("date") >= 0 || rowID.indexOf("status") >= 0)
				{
					var row;
					if (oldSearch != null)
					{
						row = oldSearch[cell];
					}
				}
				else
				{
					var row;
					if (oldSearch != null)
					{
						row = oldSearch[cell];
					}
					if (row != null)
					{
						if (rowID === "bookingdestinationtype")
						{
							var value = $(row).find('select').attr('data-value');
							var select = $('<select style="min-width: 100px;" class="form-control travelDocsSearchInput ' + $(this).attr('id') + '"><option value=""></option><option value="1" ' + (value === "1" ? "selected" : "") + '>Domestic</option><option value="0" ' + (value === "0" ? "selected" : "") + '>International</option></select>');
							$(this).html(select);
						}
						else
						{
							$(this).html('<input type="text" class="form-control travelDocsSearchInput ' + $(this).attr('id') + '" data-type="' + $(this).attr('id') + '" placeholder="search" aria-label="Search" value="' + $(row).find('input').val() + '">');
						}
					}
					else
					{
						if (rowID === "bookingdestinationtype")
						{
							var select = $('<select style="min-width: 100px;" class="form-control travelDocsSearchInput ' + $(this).attr('id') + '"><option value=""></option><option value="1">Domestic</option><option value="0">International</option></select>');
							$(this).html(select);
						}
						else
						{
							$(this).html('<input type="text" class="form-control travelDocsSearchInput ' + $(this).attr('id') + '" data-type="' + $(this).attr('id') + '" placeholder="search" aria-label="Search">');
						}
					}
				}
			}
		});
		$(ele).insertAfter(tr);
		if (activeElement)
		{
			var cursorPosition = $(activeElement).getCursorPosition();
			var $newActive = $(ele).find('[data-type="' + $(activeElement).data('type') + '"]');
			if (cursorPosition != undefined && $newActive != undefined)
			{
				setCaretPosition($newActive.get(0), cursorPosition);
			}

		}
	}
}

function setCaretPosition(ctrl, pos)
{
	// Modern browsers
	if (ctrl.setSelectionRange)
	{
		ctrl.focus();
		ctrl.setSelectionRange(pos, pos);

		// IE8 and below
	}
	else if (ctrl.createTextRange)
	{
		var range = ctrl.createTextRange();
		range.collapse(true);
		range.moveEnd('character', pos);
		range.moveStart('character', pos);
		range.select();
	}
}

function displayTasksImportModal()
{
	var $modal = $('#import-tasks-modal');
	$modal.load('/application/dialog/tasks/import.jsp', '', function()
	{
		console.log('MODAL LOADED');
		$modal.modal({
			backdrop : 'static',
			keyboard : false
		});
	});
}

function exportContactsAction(isSample)
{
	var endpoint = '/api/tasks/export';
	startLoadingForeground();
	$.fileDownload(endpoint, {
		data : {
			row : isSample ? "1" : ""
		},
		successCallback : function(url)
		{
			stopLoadingForeground();
		},
		failCallback : function(html, url)
		{
			stopLoadingForeground();
			alert("Oops! Something wrong in exporting");
		}
	});
}

(function($)
{
	$.fn.getCursorPosition = function()
	{
		var input = this.get(0);
		if (!input)
			return; // No (input) element found
		if ('selectionStart' in input)
		{
			// Standard-compliant browsers
			return input.selectionStart;
		}
		else if (document.selection)
		{
			// IE
			input.focus();
			var sel = document.selection.createRange();
			var selLen = document.selection.createRange().text.length;
			sel.moveStart('character', -input.value.length);
			return sel.text.length - selLen;
		}
	}
})(jQuery);
