
const tokenDivId = 'token_div';
const permissionDivId = 'permission_div';
var chatDBRef;
var config = {
	apiKey: "AIzaSyCjGIC8Jg0d1rEececEMEJW0QMHi0N5P2o",
	authDomain: "quotecloud-55f9a.firebaseapp.com",
	databaseURL: "https://quotecloud-55f9a.firebaseio.com",
	projectId: "quotecloud-55f9a",
	storageBucket: "quotecloud-55f9a.appspot.com",
	messagingSenderId: "250598880955"
};

firebase.initializeApp(config);
// var messaging = firebase.messaging();
var database = firebase.database();

function firbaseSync() {
	chatDBRef = firebase.database().ref('Chat/' + chatroom.companyID + '/' + chatroom.chatRoomID + '/Messages');

	chatDBRef.on('value', function(snapshot) {
		console.log(snapshot.val());
		if (snapshot.val() != null) {
			var messageDict = snapshot.val();
			var message = messageDict[Object.keys(messageDict)[Object.keys(messageDict).length- 1]];
			if (message != null && (message.from != clientName)) {
				$.ajax({
					url : "/api/mobile/getMessage",
					data : {
						'messageId': message.messageID
					},
					type : "POST",
					success : function(data)
					{
						if ($("#" + data.attr_headline).length < 1) {
							$('<div class="message message-recieved new" id="' + data.attr_headline + '"><figure class="avatar"><img src="/application/images/user.png"></figure>' + data.attr_messagetext + '</div>').appendTo($('.mCSB_container')).addClass('new');
							updateScrollbar();
						}
					}
				});
			}
		}
	});
}





// function writeUserData(userId, name, email) {
// 	firebase.database().ref('users/' + userId).set({
// 		username: name,
// 		email: email
// 	});
// }
//
// // [START refresh_token]
// // Callback fired if Instance ID token is updated.
// messaging.onTokenRefresh(function() {
// 	messaging.getToken()
// 		.then(function(refreshedToken) {
// 			console.log('Token refreshed.');
// 			// Indicate that the new Instance ID token has not yet been sent to the
// 			// app server.
// 			setTokenSentToServer(false);
// 			// Send Instance ID token to app server.
// 			sendTokenToServer(refreshedToken);
// 			// [START_EXCLUDE]
// 			// Display new Instance ID token and clear UI of all previous messages.
// 			resetUI();
// 			// [END_EXCLUDE]
// 		})
// 		.catch(function(err) {
// 			console.log('Unable to retrieve refreshed token ', err);
// 			showToken('Unable to retrieve refreshed token ', err);
// 		});
// });
// // [END refresh_token]
//
// // [START receive_message]
// // Handle incoming messages. Called when:
// // - a message is received while the app has focus
// // - the user clicks on an app notification created by a sevice worker
// //   `messaging.setBackgroundMessageHandler` handler.
// messaging.onMessage(function(payload) {
// 	console.log("Message received. ", payload);
// 	// [START_EXCLUDE]
// 	// Update the UI to include the received message.
// 	appendMessage(payload);
// 	// [END_EXCLUDE]
// });
// // [END receive_message]
//
// $(document).ready(function()
// {
//
// 	resetUI();
// });
//
// function resetUI() {
// 	showToken('loading...');
// 	// [START get_token]
// 	// Get Instance ID token. Initially this makes a network call, once retrieved
// 	// subsequent calls to getToken will return from cache.
// 	messaging.getToken()
// 		.then(function(currentToken) {
// 			console.log(currentToken);
// 			if (currentToken) {
// 				sendTokenToServer(currentToken);
// 				updateUIForPushEnabled(currentToken);
// 			} else {
// 				// Show permission request.
// 				console.log('No Instance ID token available. Request permission to generate one.');
// 				// Show permission UI.
// 				requestPermission();
// 				setTokenSentToServer(false);
// 			}
// 		})
// 		.catch(function(err) {
// 			console.log('An error occurred while retrieving token. ', err);
// 			showToken('Error retrieving Instance ID token. ', err);
// 			setTokenSentToServer(false);
//
// 			//init long polling here...
// 			poll();
// 		});
// }
//
// function poll(){
// 	console.log("polling");
// 	setTimeout(function(){
// 		$.ajax({ url: "server", success: function(data){
// 			//Update your dashboard gauge
// 			// salesGauge.setValue(data.value);
// 			console.log("polling");
// 			//Setup the next poll recursively
// 			poll();
// 		}, dataType: "json"});
// 	}, 30000);
// }
//
// // [END get_token]
//
// // Send the Instance ID token your application server, so that it can:
// // - send messages back to this app
// // - subscribe/unsubscribe the token from topics
// function sendTokenToServer(currentToken) {
// 	if (!isTokenSentToServer()) {
// 		console.log('Saving current token variable and unlocking chat button');
// 		$('#startLiveChat')[0].disabled = false;
// 		webChatToken = currentToken;
// 		setTokenSentToServer(true);
// 	} else {
// 		console.log('Token already sent to server so won\'t send it again ' +
// 			'unless it changes');
// 	}
//
// }
//
// function isTokenSentToServer() {
// 	return window.localStorage.getItem('sentToServer') == 1;
// }
//
// function setTokenSentToServer(sent) {
// 	window.localStorage.setItem('sentToServer', sent ? 1 : 0);
// }
//
// function requestPermission() {
// 	console.log('Requesting permission...');
// 	// [START request_permission]
// 	messaging.requestPermission()
// 		.then(function() {
// 			console.log('Notification permission granted.');
// 			// TODO(developer): Retrieve an Instance ID token for use with FCM.
// 			// [START_EXCLUDE]
// 			// In many cases once an app has been granted notification permission, it
// 			// should update its UI reflecting this.
// 			resetUI();
// 			// [END_EXCLUDE]
// 		})
// 		.catch(function(err) {
// 			console.log('Unable to get permission to notify.', err);
// 		});
// 	// [END request_permission]
// }
//
// // Add a message to the messages element.
// function appendMessage(payload) {
// 	const messagesElement = document.querySelector('#messages');
// 	const dataHeaderELement = document.createElement('h5');
// 	const dataElement = document.createElement('pre');
// 	dataElement.style = 'overflow-x:hidden;'
// 	dataHeaderELement.textContent = 'Received message:';
// 	dataElement.textContent = JSON.stringify(payload, null, 2);
// 	messagesElement.appendChild(dataHeaderELement);
// 	messagesElement.appendChild(dataElement);
// }
//
// function updateUIForPushEnabled(currentToken) {
// 	showHideDiv(tokenDivId, true);
// 	showHideDiv(permissionDivId, false);
// 	showToken(currentToken);
// }
//
// function updateUIForPushPermissionRequired() {
// 	showHideDiv(tokenDivId, false);
// 	showHideDiv(permissionDivId, true);
// }
//
// function showToken(currentToken) {
// 	// Show token in console and UI.
// 	console.log(currentToken);
// }