function initKendoSpreadsheet($panel, data)
{
	$('.spreadsheet', $panel).empty().kendoSpreadsheet({
		sheetsbar : false,
		toolbar : {
			home : [ "open", "exportAs", [ "cut", "copy", "paste" ], [ "bold", "italic", "underline" ], "backgroundColor", "textColor", "borders", "fontSize",
			// "fontFamily",
			"alignment", "textWrap", [ "formatDecreaseDecimal", "formatIncreaseDecimal" ], "format", "merge", "freeze", "filter" ],
			insert : [ [ "addColumnLeft", "addColumnRight", "addRowBelow", "addRowAbove" ], [ "deleteColumn", "deleteRow" ] ],
			data : [ "sort", "filter", "validation" ]
		}
	});

	var spreadsheet = $('.spreadsheet', $panel).data("kendoSpreadsheet");
	if (data)
	{
		spreadsheet.fromJSON(removeSpreadsheetSelection(data));
	}

	var dataItems = {
		text : "Data Items",
		items : [],
	}
	$.ajax({
		type : 'GET',
		url : '/api/library/dataitems',
		data : {
			module : 'SPREADSHEET'
		}
	}).done(function(data, textStatus, jqXHR)
	{
		$.each(data, function(key, value)
		{
			var subItem = {
				text : key,
				items : []
			}
			$.each(value, function(key, value)
			{
				subItem.items.push({
					text : '<span data-val="' + value + '">' + key + '</span>',
					encoded : false
				})
			});
			dataItems.items.push(subItem);
		});

	// 	$.ajax({
	// 		type : 'GET',
	// 		url : '/api/library/customfields'
	// 	}).done(function(data, textStatus, jqXHR)
	// 	{
	// 		var subItem = {
	// 			text : 'Custom Fields',
	// 			items : []
	// 		}
	// 		$.each(data, function(key, value)
	// 		{
	// 			subItem.items.push({
	// 				text : '<span data-val="' + value + '">' + key + '</span>',
	// 				encoded : false
	// 			})
	// 		});
	// 		if (subItem.items.length > 0)
	// 		{
	// 			dataItems.items.push(subItem);
	// 		}
	//
	// 		if (dataItems.items.length > 0)
	// 		{
	// 			var cellContextMenu = spreadsheet.cellContextMenu();
	// 			cellContextMenu.append([ dataItems ]);
	// 			cellContextMenu.bind("select", function(e)
	// 			{
	// 				var sheet = spreadsheet.activeSheet(), selection = sheet.selection();
	// 				selection.leftColumn().value($(e.item).find('span[data-val]').attr('data-val'));
	// 			});
	// 		}
	// 	});
	});
}

function removeSpreadsheetSelection(spreadsheet)
{
	delete spreadsheet.sheets["0"].activeCell;
	delete spreadsheet.sheets["0"].selection;
	return spreadsheet;
}
