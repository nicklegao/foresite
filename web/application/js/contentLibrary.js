var backgroundProcessing = 0;

$(document).ready(function()
{
	DocumentLibrary.init();
	DocumentLibrary.enterDormantMode();

	$(document).on('click', ".addFreeTextLibraryAction", function()
	{
		var index = getNextIndex($(this));
		addFreeTextLibraryAction(index);
	});
	$(document).on('click', ".addPricingTableAction", function()
	{
		var index = getNextIndex($(this));
		addPricingTableAction(index);
	});
	$(document).on('click', ".attachFileAction", function()
	{
		var index = getNextIndex($(this));
		attachFileAction(index);
	});
	$(document).on('click', ".attachImageAction", function()
	{
		var index = getNextIndex($(this));
		attachImageAction(index);
	});
	$(document).on('click', ".attachSpreadsheetAction", function()
	{
		var index = getNextIndex($(this));
		attachSpreadsheetAction(index);
	});

	$(document).on('click', ".addSignAction", function()
	{
		console.log("add action clicked");
		var index = getNextIndex($(this));
		addSignTableAction(index);
	});


	// Image upload buttons
	$(document).on('click', ".choseFile", function()
	{
		var $panel = $(this).closest('.panel');
		imageUploadChooseAction($panel);
	});
	$(document).on('click', ".removeFreetextImageAttachment", function()
	{
		var $panel = $(this).closest('.panel');
		imageUploadRemoveAction($panel);
	});
	$(document).on('click', "li.documentLibrarySearch a.trigger", function(e)
	{
		e.preventDefault();
		if ($("body").hasClass("minified"))
			unminifyContentLibrary();
		$("li.documentLibrarySearch a.trigger input").focus();
	});

	$("body").toggleClass('minified', sessionStorage['contentLibraryMinified'] === "true");

	$(".minifyme").click(function()
	{
		if (!$("body").hasClass("minified"))
			minifyContentLibrary();
		saveMinifiedState();
	});


});

function showCoords(panel)
{
	var jcrop_holder = $('.jcrop-holder', panel);

	if ($('#crop-details', panel).length == 0)
	{
		var details = '<span id="crop-details">x:<span id="x" class="cr-val"></span> y:<span id="y"class="cr-val"></span> height:<span id="h"class="cr-val"></span> width:<span id="w"class="cr-val"></span></span>';
		jcrop_holder.append(details);
	}
	$('#x', panel).text(Math.round(panel.extra.x) + 'px');
	$('#y', panel).text(Math.round(panel.extra.y) + 'px');
	$('#h', panel).text(Math.round(panel.extra.h) + 'px');
	$('#w', panel).text(Math.round(panel.extra.w) + 'px');

}
function minifyContentLibrary()
{
	$.each($("#sidebar nav > ul > li a"), function()
	{
		closeContentTree($(this));
	});
}

function unminifyContentLibrary()
{
	$("body").removeClass("minified");
	saveMinifiedState();
}

function saveMinifiedState()
{
	setTimeout(function()
	{
		sessionStorage.setItem('contentLibraryMinified', $("body").hasClass("minified"));
		refreshTextareas();
	}, 300);
}

function addFreeTextLibraryAction(index)
{
	var $currentSection = $(".editorContent").find(".tab-pane.active .contentContainer");
	var newSectionIndex = $(".group-panel", $currentSection).length;

	insertContentIntoEditor($currentSection, index, 'freeText', undefined, false).done(function(data, textStatus, jqXHR)
	{
		initSpecificFroalaEditor($currentSection, index);

		var $panel = $('.panel[dataOrder="' + index + '"]', $currentSection);
		setupImageUploadInteractions($panel);
		$('[data-toggle="tooltip"]', $panel).tooltip();
	});
}

function addItineraryTableAction()
{
	var $currentSection = $("#itineraryContainer");
	if ($currentSection.length == 1)
	{
		insertContentIntoEditor($currentSection, 0, "itinerary", undefined, false);
	}
}

function addPricingTableAction(index)
{
	var $currentSection = $(".editorContent").find(".tab-pane.active .contentContainer");
	var template = $('#htmlFragments .pricing-droppable').clone();
	insertContentIntoPanel($currentSection, index, false, false, template);
	// init pricing table
	var $panel = $('.panel[dataOrder="' + index + '"]', $currentSection);
	$('[data-toggle="tooltip"]', $panel).tooltip();
	initPricingTable($panel);
	DocumentLibrary.initiatePricingDropzone();

	$('.pricing-droppable:not(#htmlFragments .pricing-droppable)').removeClass('current');
	$panel.addClass('current');
	startSortable($panel.find('[id="pricingTable"]'));
	setUpPricingTermType("", $panel);
	updateOverallTotalPricing();

	$('#editorContent').on('scroll', function()
	{
		var $tableBody = $('.scroll-holder', $panel);
		if (!$tableBody.is(':visible'))
		{
			return;
		}
		var top = $tableBody.offset().top;
		if (top <= 101 && $tableBody.height() + top >= 108)
		{
			$tableBody.closest('.panel-body').addClass("fixed-buttons");
		}
		else
		{
			$tableBody.closest('.panel-body').removeClass("fixed-buttons");
		}

	})
}

function removeSignatureRow(btn)
{
	var $panel = $(btn).closest('.panel');
	var rowCount = $('#signTable >tbody >tr', $panel).length;
	if (rowCount > 1)
	{
		$(btn).closest("tr").remove();
	}
}

function getEmailsInProposal()
{
	var emails = [];
	var proposal = getCurrentProposal();
	$(proposal.sections[0].content).each(function()
	{
		if (this.id != 'con-info')
		{
			return;
		}
		if (this.metadata.contactEmail && this.metadata.contactEmail != '')
		{
			emails.push(this.metadata.contactEmail);
		}
		if (this.metadata['cc-emails'] && this.metadata['cc-emails'] != '')
		{
			emails.push.apply(emails, this.metadata['cc-emails'].split(','));
		}
	});
	return emails;
}

$(document).on('keyup', '#contactEmail', function()
{
	cleanupSignTables();
	initPayerList();
});

$(document).on('change', '#cc-emails', function()
{
	cleanupSignTables();
	initPayerList();
});

function cleanupSignTables()
{
	if ($('[type="sign"]').length > 0)
	{
		$('[type="sign"]').each(function()
		{
			var $panel = $(this);
			var signaturesLength = $('.signTbl', $panel).find('tbody tr').length;
			var emails = getEmailsInProposal();
			if (signaturesLength >= emails.length)
			{
				$('.addNewSignAction', $panel).hide();
			}
			else
			{
				$('.addNewSignAction', $panel).show();
			}
			for (var x = 0; x < $('select[name="email"]', $panel).length; x++)
			{
				var $select = $($('select[name="email"]', $panel).get(x));
				var selectedEmail = $select.val();
				$select.empty();
				$select.append('<option value="">Any authorised recipient</option>');
				$(getEmailsInProposal()).each(function()
				{
					var selected = "";
					if (this == selectedEmail)
					{
						selected = "selected";
					}
					$select.append('<option value="' + this + '" ' + selected + '>' + this + '</option>');
				});
			}
		});
	}
}

function initSignTable(index, $currentSection)
{
	var $panel = $('.panel[dataOrder="' + index + '"]', $currentSection);
	$('[data-toggle="tooltip"]', $panel).tooltip();

	$('select[name="email"]', $panel).empty();
	$('select[name="email"]', $panel).append('<option value="">Any authorised recipient</option>');
	$(getEmailsInProposal()).each(function()
	{
		$('select[name="email"]', $panel).append('<option value="' + this + '">' + this + '</option>');
	})
	var tr = $('#signTable > tbody', $panel).find('tr:last');
	tr.attr('data-uuid', uuid());

	$("#addNewSign", $panel).click(function()
	{
		tr.parent().append(tr.clone().find('select[name="email"]').val('').end().find('input[name="dateNeeded"]').prop('checked', false).end().attr('data-uuid', uuid()).show());
	});

	// var elems = document.querySelectorAll('.dateNeeded');
	//
	// for (var i = 0; i < elems.length; i++)
	// {
	// new Switchery(elems[i], {
	// color : '#64BD63',
	// secondaryColor : '#aaa',
	// size : 'small'
	// });
	// }


	$('#hideDrawSign, #hideTypeSign, #hideUploadSign', $panel).each(function()
	{
		new Switchery($(this)[0], {
			color : '#fc0d00',
			size : 'small'
		});
		$(this).siblings(".switchery").css("width", "60px").prepend("<span class=''>Show</span>").find("small");
		$(this).change(function(e)
		{
			if ($(this).is(':checked'))
			{
				// $panel.addClass('hide-' +
				// $(e.target).attr('data-target-class'));
				$(this).siblings(".switchery").find("span").text('Hide');
				$(this).siblings(".switchery").find("span").addClass('hideLabel');
			}
			else
			{
				$(this).siblings(".switchery").find("span").text('Show');
				$(this).siblings(".switchery").find("span").removeClass('hideLabel');
				// $panel.removeClass('hide-' +
				// $(e.target).attr('data-target-class'));
			}
			if ($('#hideDrawSign, #hideTypeSign, #hideUploadSign', $panel).filter(':checked').length == 3)
			{
				$(this).click();
				swal({
					title : "Error",
					text : "You need to show at least one signature type in a signature section.",
					type : "warning",
					closeOnConfirm : true
				})
			}
			// else if ($('#hideDrawSign, #hideTypeSign,
			// #hideUploadSign').filter(':checked').length < 2) {
			// $('#hideDrawSign, #hideTypeSign,
			// #hideUploadSign').not(':checked').attr('disabled', false);
			// }
		});
	});

	$('.dropdown.theme-chooser li', $panel).click(function()
	{
		$(this).closest('.theme-chooser').find('button > span.theme-name').text($(this).text());
	})
}

function addSignTableAction(index)
{
	var $currentSection = $(".editorContent").find(".tab-pane.active .contentContainer");
	var newSectionIndex = $(".group-panel", $currentSection).length;

	insertContentIntoEditor($currentSection, index, 'signature', undefined, false).done(function(data, textStatus, jqXHR)
	{
		initSignTable(index, $currentSection)

	});
}

function initSpecificFroalaEditor($currentSection, newSectionIndex)
{

	// logger.debug('initSpecificFroalaEditor [dataOrder="' + newSectionIndex +
	// '"]
	// .panel-body[contenteditable="true"]');
	logger.debug('initSpecificFroalaeditor [dataOrder="' + newSectionIndex + '"] .panel-body[contenteditable="true"]');
	var editableSections = $('.panel[dataOrder="' + newSectionIndex + '"] .panel-body[contenteditable="true"]', $currentSection);
	var editableFroalaSections = $('.panel[dataOrder="' + newSectionIndex + '"] .panel-froala', $currentSection);

	if (editableSections.length > 0)
	{
		CKEDITOR.inline(editableSections[0], {
			customConfig : '/application/assets/plugins/ckeditor/custom/config-proposalFreeText.jsp'
		});
	}

	if (editableFroalaSections.length > 0)
	{
		$(editableFroalaSections[0]).on('froalaEditor.initialized', function(ev, editor)
		{
			customizeFroalaEditor(ev, editor)
		}).froalaEditor({
			enter : $.FroalaEditor.ENTER_P,
			// Set the image upload parameter.
			imageUploadParam : 'image',
			// Set the image upload URL.
			imageUploadURL : '/api/proposal/attach/image',
			// Additional upload params.
			imageUploadParams : {
				proposal : $('#param-proposal-id').val(),
				x : '0',
				y : '0',
				x2 : '900',
				y2 : '900',
				w : '100%',
				h : '100%'
			},
			// Set request type.
			imageUploadMethod : 'POST',
			// Set max image size to 5MB.
			imageMaxSize : 5 * 1024 * 1024,
			// Allow to upload PNG and JPG.
			imageAllowedTypes : [ 'jpeg', 'jpg', 'png' ],
			quickInsertButtons : [ 'image', 'table', 'ul', 'ol', 'hr' ],
			key : 'DLAHYKAJOEc1HQDUH=='
		});
	}
}

function attachFileAction(index)
{
	var $currentSection = $(".editorContent").find(".tab-pane.active .contentContainer");
	var newSectionIndex = $("> .panel", $currentSection).length;

	insertContentIntoEditor($currentSection, index, 'fileUpload', undefined, false).done(function(data, textStatus, jqXHR)
	{
		var proposalId = $("#param-proposal-id").val();

		var newPanel = $('.panel[dataorder="' + index + '"]', $currentSection);
		var dropzone = $(".dropzone", newPanel).dropzone({
			url : '/api/proposal/attach',
			acceptedFiles : "application/pdf",
			maxFilesize : 5, // MB
			paramName : 'attachment',
			maxFiles : 1,
			forceFallback : false,// enable
			// for
			// testing
			dictDefaultMessage : "Upload file please",
			createImageThumbnails : false,
			params : {
				proposal : proposalId
			},
			init : function()
			{
				this.on("maxfilesexceeded", function(file)
				{
					this.removeFile(file);
					swal('Upload failed', 'Only one file can be uploaded at a time!', 'error');
				});

				this.on('addedfile', function()
				{
					backgroundProcessing += 1;
				});
				this.on('complete', function()
				{
					backgroundProcessing -= 1;
				});

				this.on('error', function(file)
				{
					if (file.size > this.options.maxFilesize * 1024 * 1024)
					{
						swal('Upload failed', 'The file you selected is too large. Attachments can be a maximum of ' + this.options.maxFilesize + 'MB.', 'error');
						this.removeAllFiles();
					}
				});
			},
			success : function(file, data)
			{
				$(".dz-preview", dropzone).removeClass("dz-processing");
				if (data.success)
				{
					$(".dz-preview", dropzone).addClass("dz-success");

					$("#attachmentId", newPanel).val(data.fileId);
					$("#fileName", newPanel).val(file.name);
				}
				else
				{
					$(".dz-preview", dropzone).addClass("dz-error");
				}
			}
		});

	});
}

function attachImageAction(index)
{
	var $currentSection = $(".editorContent").find(".tab-pane.active .contentContainer");
	var newSectionIndex = $(".group-panel", $currentSection).length;

	insertContentIntoEditor($currentSection, index, 'freeText', undefined, false).done(function(data, textStatus, jqXHR)
	{

		var $panel = $('.panel[dataOrder="' + index + '"]', $currentSection);
		setupImageUploadInteractions($panel);
		$('[data-toggle="tooltip"]', $panel).tooltip();
		attachImageLayout($panel, false, false, "attachImageAction");
	});
}

function attachImageLayout($panel, hasImg, hasTxt, action)
{
	$($panel).attr('action', action);
	$('.panel-heading .fa-keyboard-o', $panel).removeClass("fa-keyboard-o").addClass("fa-image");
	$('.panel-heading .fa-image', $panel).siblings('span').text('Add Image');
	if (hasImg)
	{
		$('.choseFile', $panel).hide();
		if (!hasTxt)
		{
			$(".inlineImage-addText", $panel).show();
		}
	}
	if (!hasTxt)
	{
		$('.panel-body', $panel).css('display', 'none');
	}
	if (!hasImg && hasTxt)
	{
		$('.panel-body', $panel).insertAfter($('.freetext-inline-image', $panel));
	}
}

function setupImageUploadInteractions($newPanel)
{
	$('.cropImage', $newPanel).hide();
	$(".crop-loading", $newPanel).hide();
	if ($("#attachmentId", $newPanel).val() == '')
	{
		$('#radialImageAlignmentButtons', $newPanel).hide();
		$(".removeFreetextImageAttachment", $newPanel).hide();
	}

	$("#imageUpload", $newPanel).on("change", function(evt)
	{
		var file = evt.target.files;

		if (FileReader && file && file[0])
		{
			var reader = new FileReader();
			reader.onload = function(e)
			{
				switch (file[0].type)
				{
					case "image/gif":
					case "image/jpeg":
					case "image/png":
						if (file[0].size <= 5242880)
						{
							$newPanel.image = file[0];
							$("#JcropPhotoFrame", $newPanel).attr('src', e.target.result).width('auto').height('auto');
							var image = new Image();
							image.src = e.target.result;
							image.onload = function()
							{
								$("#JcropPhotoFrame", $newPanel).Jcrop({
									bgOpacity : 0.4,
									trueSize : [ image.naturalWidth, image.naturalHeight ],
									bgColor : '',
									setSelect : [ 0, 0, 500, 350 ],
									allowSelect : false,
									onSelect : function(c)
									{
										$newPanel.extra = c;
										showCoords($newPanel);
									},
									onChange : function(c)
									{
										$newPanel.extra = c;
										showCoords($newPanel)
									}
								}, function()
								{
									$newPanel.jcrop = this;
									$newPanel.data('jcrop', $newPanel.jcrop);
								});
								$('.cropImage', $newPanel).show();
								$('.cropImage', $newPanel).addClass('pulse-button');
							}
						}
						else
						{
							swal("Upload failed", "The file you selected is too large. Attachments can be a maximum of 5MB.", "error");
						}
						break;
					default:
						swal("File type not supported!", "Only gif, jpeg and png are supported.", "error");
				}
			};
			reader.readAsDataURL(file[0]);
		}
		else
		{
			alert("This browser do not support file upload.");
		}

	});

	$newPanel.on('click', ".cropImage", function()
	{
		$('.choseFile', $newPanel).hide();
		var formData = new FormData();
		formData.append("image", $newPanel.image);
		$.each($newPanel.extra, function(key, value)
		{
			formData.append(key, value);
		});
		var proposalId = $("#param-proposal-id").val();
		formData.append('proposal', proposalId);
		$(".crop-loading", $newPanel).show();
		$(".cropImage", $newPanel).addClass("disabled");
		if ($newPanel.data('jcrop'))
		{
			$newPanel.jcrop = $newPanel.data('jcrop');
			$newPanel.jcrop.disable();
		}

		$.ajax({
			type : 'POST',
			url : '/api/proposal/attach/image',
			data : formData,
			cache : false,
			contentType : false,
			processData : false
		}).done(function(data)
		{
			if (data.success)
			{
				$('.cropImage', $newPanel).hide();
				$('.choseFile', $newPanel).hide();
				if ($newPanel.data('jcrop'))
				{
					$newPanel.jcrop = $newPanel.data('jcrop');
					$newPanel.jcrop.destroy();
				}
				var url = "/stores/PROPOSAL_ATTACHMENTS/" + data.fileId + "/ATTRFILE_attachment/" + data.name;
				$("#JcropPhotoFrame", $newPanel).attr('src', url);
				$("#JcropPhotoFrame", $newPanel).css('width', 'auto').css('height', 'auto');
				$("#attachmentId", $newPanel).val(data.fileId);
				$("#image", $newPanel).attr("src", url);
				$("#image", $newPanel).attr("align", "Middle");
				$(".crop-loading", $newPanel).hide();

				$(".removeFreetextImageAttachment", $newPanel).show();
				$('#radialImageAlignmentButtons', $newPanel).show();
				$(".freetext-inline-image", $newPanel).addClass("hasImage");
				var action = $newPanel.attr('action');

				if (action == 'attachImageAction' || action == 'attachLibraryImageAction')
				{
					if (!$('.panel-body', $newPanel).is(':visible'))
					{
						$(".inlineImage-addText", $newPanel).show();
						$('.inlineImageAlignmentButtons button', $newPanel).not('.align-pos button').hide();
					}
					$('.inlineImageAlignmentButtons button', $newPanel).not('.align-pos button').removeClass('active');
					$('.inlineImageAlignmentButtons button[data-alignment=top]', $newPanel).addClass('active');

					if (action == 'attachLibraryImageAction')
					{
						$newPanel.attr('action', 'attachImageAction');
					}
				}
			}
			else
			{
				$('.choseFile', $newPanel).show();
			}
		});
	});

}

function imageUploadChooseAction($newPanel)
{
	if ($newPanel.data('jcrop'))
	{
		$newPanel.jcrop = $newPanel.data('jcrop')
		$newPanel.jcrop.destroy();
	}
	$("#JcropPhotoFrame", $newPanel).attr("src", "").css("width", "auto").css("height", "auto");
	$("#imageUpload", $newPanel).val("");
	$('.cropImage', $newPanel).hide();
	$("#imageUpload", $newPanel).click();
}

function imageUploadRemoveAction($newPanel)
{
	$('#content-removed', $newPanel).hide();
	$newPanel.find("#attachmentId").val("");

	$('.choseFile', $newPanel).show();
	$(".cropImage", $newPanel).removeClass("disabled");

	$(".freetext-inline-image", $newPanel).removeClass("hasImage").css('text-align', 'left');
	$("#image", $newPanel).hide();
	$("#JcropPhotoFrame", $newPanel).hide();
	$("#radialImageAlignmentButtons", $newPanel).hide();
	$(".removeFreetextImageAttachment", $newPanel).hide();
	$(".freetext-inline-image", $newPanel).css('width', '100%');
	if ($newPanel.attr('action') == 'attachImageAction' || $newPanel.attr('action') == 'attachLibraryImageAction')
	{
		$(".freetext-inline-image", $newPanel).css('float', 'none').insertBefore($(".panel-body", $newPanel));
		$(".inlineImage-addText", $newPanel).hide();
		if ($newPanel.attr('action') == 'attachLibraryImageAction')
		{
			$newPanel.attr('action', 'attachImageAction');
		}
	}
	else
	{
		$(".freetext-inline-image", $newPanel).insertAfter($(".panel-body", $newPanel));
	}
	$(".freetext-inline-image", $newPanel).find('button').removeClass('active');
	$(".freetext-inline-image", $newPanel).find('button[data-alignment=bottom]').addClass('active');
	$(".freetext-inline-image", $newPanel).find('button[data-alignment=left]').addClass('active');
}

function attachSpreadsheetAction(index)
{
	var $currentSection = $(".editorContent").find(".tab-pane.active .contentContainer");
	var newSectionIndex = $(".group-panel", $currentSection).length;

	insertContentIntoEditor($currentSection, index, 'Spreadsheet', undefined, false).done(function(data, textStatus, jqXHR)
	{
		var $panel = $('.panel[dataOrder="' + index + '"]', $currentSection);
		$('[data-toggle="tooltip"]', $panel).tooltip();
	});
}
/*
 * ======================================================================================= #
 * ### ###### ###### # ###### # # ###### ###### ### ##### ### # # ##### # # # # # # # # # # # # # # # # # # # # ## # # # # # # # # # # # # # # # # # # # # # # # # # # # #
 * ###### ###### # # ###### # ###### ###### # # # # # # # #### # # # # # #
 * ####### # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ## # #
 * ####### ### ###### # # # # # # # # # # ### ##### ### # # #####
 * 
 * =========================================================================================
 */
var LibraryPricingMode = {
	name : 'pricing',

	load : function(parentId)
	{
		var dataOut = {
			mode : this.name,
			leaf : false,
			search : $(".documentLibrarySearch input").val()
		};

		if (parentId != undefined)
		{
			dataOut.parentId = parentId;
		}

		var currentNode = DocumentLibrary.getNodeByIndex(DocumentLibrary.currentLibraryLevels - 1);
		if (currentNode != undefined)
		{
			dataOut.leaf = currentNode.leaf;
		}

		return $.ajax({
			type : "POST",
			url : '/api/library/content',
			dataType : 'json',
			data : dataOut
		}).done(function(data, textStatus, jqXHR)
		{
			DocumentLibrary.libraryDataLoaded(data, textStatus, jqXHR)
		}).done(function(data, textStatus, jqXHR)
		{
			data.mode = dataOut.mode;
			DocumentLibrary.loadDone(data, textStatus, jqXHR);
		});
	},

	selectItem : function(itemId)
	{
		addPricingForPlan(itemId);
	}
}

/*
 * ======================================================================================= #
 * ### ###### ###### # ###### # # ##### ####### # # ####### ###### # # # # # # # # # # # # # # # # # ## # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
 * ###### ###### # # ###### # # #### ##### # # # ##### ###### # # # # # # # # #
 * ####### # # # # # # # # # # # # ####### # # # # # # # # # # # # # # # # ## # # # # # #
 * ####### ### ###### # # # # # # # ##### ####### # # ####### # # # # #######
 * 
 * =========================================================================================
 */
var LibraryGeneralMode = {
	name : 'general',

	load : function(parentId)
	{
		var dataOut = {
			mode : this.name,
			leaf : false,
			search : $(".documentLibrarySearch input").val()
		};

		var rootNode = DocumentLibrary.getNodeByIndex(0);
		if (rootNode != undefined)
		{
			dataOut.type = rootNode.label;
		}

		var currentNode = DocumentLibrary.getNodeByIndex(DocumentLibrary.currentLibraryLevels - 1);
		if (currentNode != undefined)
		{
			dataOut.leaf = currentNode.leaf;
		}

		if (parentId != undefined)
		{
			dataOut.parentId = parentId;
		}

		return $.ajax({
			type : "POST",
			url : '/api/library/content',
			dataType : 'json',
			data : dataOut
		}).done(function(data, textStatus, jqXHR)
		{
			DocumentLibrary.libraryDataLoaded(data, textStatus, jqXHR)
		}).done(function(data, textStatus, jqXHR)
		{
			data.mode = dataOut.mode;
			DocumentLibrary.loadDone(data, textStatus, jqXHR);
		});
	},

	selectItem : function(itemId)
	{
		var $currentSection = $(".editorContent").find(".tab-pane.active .contentContainer");
		var newSectionIndex = $("> .panel", $currentSection).length;

		var rootNode = DocumentLibrary.getNodeByIndex(0);
		var itemType = rootNode.id;

		insertContentIntoEditor($currentSection, newSectionIndex, itemType, itemId, true).done(function(data, textStatus, jqXHR)
		{
			initSpecificFroalaEditor($currentSection, newSectionIndex);
		});
	}
}

/*
 * ======================================================================================= #
 * ### ###### ###### # ###### # # ##### ####### # # ####### ###### # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
 * ###### ###### # # ###### # # # # # # ##### ###### # # # # # # ####### # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
 * ####### ### ###### # # # # # # # ##### ####### # ####### # #
 * 
 * =========================================================================================
 */
var LibraryCoverMode = {
	name : 'cover',

	load : function(parentId)
	{
		var dataOut = {
			mode : this.name,
			search : $(".documentLibrarySearch input").val()
		};

		var rootNode = DocumentLibrary.getNodeByIndex(0);
		if (rootNode != undefined)
		{
			dataOut.type = rootNode.id;
		}

		if (parentId != undefined)
		{
			dataOut.parentId = parentId;
		}

		dataOut.leaf = false;
		return $.ajax({
			type : "POST",
			url : '/api/library/content',
			dataType : 'json',
			data : dataOut
		}).done(function(data, textStatus, jqXHR)
		{
			DocumentLibrary.libraryDataLoaded(data, textStatus, jqXHR)
		}).done(function(data, textStatus, jqXHR)
		{
			data.mode = dataOut.mode;
			DocumentLibrary.loadDone(data, textStatus, jqXHR);
		});
	},

	selectItem : function(itemId)
	{
		var $currentSection = $(".editorContent").find(".tab-pane.active .contentContainer");
		var newSectionIndex = $("> .group-panel", $currentSection).length;

		var rootNode = DocumentLibrary.getNodeByIndex(0);
		var itemType = rootNode.id;

		insertContentIntoEditor($currentSection, newSectionIndex, itemType, itemId, true).done(function(data, textStatus, jqXHR)
		{
			initSpecificFroalaEditor($currentSection, newSectionIndex);
		});
	}
}

var LibraryDormantMode = {
	name : 'dormant',

	load : function(parentId)
	{
		return new Object(); // Is this a safe fallback?
	},

	selectItem : function(itemId)
	{
		// Should never be called in dormant mode
	}
}

/*
 * =======================================================================================
 * 
 * ###### ####### ##### # # # # ####### # # ####### # ### ###### ###### # ###### # # # # # # # # # # ## ## # ## # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
 * ##### # # # # # # ###### ###### # # ###### # # # # # # # # # # # # # # # # # # # # #
 * ####### # # # # # # # # # # # # # # # ## # # # # # # # # # # # # ######
 * ####### ##### ##### # # ####### # # # ####### ### ###### # # # # # # #
 * 
 * =========================================================================================
 */
var DocumentLibrary = {
	holder : "#sidebar nav > ul",
	mode : undefined,

	currentRequest : undefined,
	currentRequestDelay : undefined,

	// internal state variables
	currentLibraryLevels : undefined,
	libraryData : undefined,
	selectedLibraryNodes : undefined,

	init : function()
	{
		this.currentLibraryLevels = 0;
		this.libraryData = [];
		this.selectedLibraryNodes = [];

		this.setLoaded(false);
	},

	// Resets the tree
	clear : function(modeChange)
	{
		if (modeChange == true)
		{
			$(this.holder).html('');
		}
		else
		{
			$("> li.documentLibraryContent", this.holder).remove();
		}

		if (DocumentLibrary.currentRequest != undefined)
		{
			DocumentLibrary.currentRequest.abort();
			DocumentLibrary.currentRequest = undefined;
		}

		this.init();

		// $(".libraryActions").find("> button").hide();
		// if (mode == LibraryPricingMode)
		// {
		// $(".libraryActions").find(".addCustomPricingAction").show();
		// $(".libraryActions").find(".addProductSectionAction").show();
		// $(".libraryActions").find(".addFreeTextLibraryAction").show();
		// $(".libraryActions").find(".attachFileAction").show();
		// }
		// else if (mode == LibraryGeneralMode)
		// {
		// $(".libraryActions").find(".addFreeTextLibraryAction").show();
		// $(".libraryActions").find(".attachFileAction").show();
		// }
	},

	enterDormantMode : function()
	{
		mode = LibraryDormantMode;

		this.clear(true);
	},

	enterCoverMode : function()
	{
		if (mode != LibraryCoverMode)
		{
			mode = LibraryCoverMode;

			this.rebuildTree();
		}
	},

	enterPricingMode : function()
	{
		mode = LibraryPricingMode;
		this.rebuildTree();
		this.initiatePricingDropzone();
		refreshTextareas();
		$('.tab-pane.active .contentContainer .dropArea').each(function(i, obj)
		{
			initDropArea($(this));
		});
	},

	enterGeneralMode : function()
	{
		if (mode != LibraryGeneralMode)
		{
			mode = LibraryGeneralMode;

			this.rebuildTree();

		}
		$('.tab-pane.active .contentContainer .dropArea').each(function(i, obj)
		{
			initDropArea($(this));
		});

	},

	rebuildTree : function()
	{
		this.clear(true);
		this.loadData();
	},

	initiatePricingDropzone : function()
	{
		$('.pricing-droppable:not(#htmlFragments .pricing-droppable)').droppable({
			scope : mode.name,
			activeClass : "active",
			hoverClass : "hover",
			tolerance : "pointer",
			accept : function(elm)
			{
				return $(elm).closest('.documentLibraryContent').hasClass('fancyProducts');
			},
			drop : function(event, ui)
			{
				var sourceNode = $(ui.helper).data("ftSourceNode");
				if (sourceNode.data.type === "Products")
				{
					loadPricingItem($(this).find('[id="pricingTable"]'), sourceNode.key);
				}
			}
		});
	},

	loadData : function(callback)
	{
		var currentNode = 0;
		if (this.selectedLibraryNodes.length > 0)
		{
			currentNode = this.selectedLibraryNodes[this.selectedLibraryNodes.length - 1].id;
		}

		// cancel whatever requests are currently active.
		if (DocumentLibrary.currentRequest != undefined)
		{
			DocumentLibrary.currentRequest.abort();
			DocumentLibrary.currentRequest = undefined;
		}

		// cancel whatever requests are waiting to go out.
		if (this.currentRequestDelay != undefined)
		{
			clearTimeout(this.currentRequestDelay);
		}

		// add a request to the queue.
		this.currentRequestDelay = setTimeout(function()
		{
			logger.debug("Sending delayed library request");
			DocumentLibrary.currentRequest = mode.load(currentNode);

			DocumentLibrary.currentRequest.done(function(data, textStatus, jqXHR)
			{
				if (callback != undefined)
				{
					callback();
				}
				DocumentLibrary.currentRequest = undefined;
			});
		}, this.currentLibraryLevels == 0 ? 500 : 0);

	},

	loadDone : function(data, textStatus, jqXHR)
	{
		var parentNode = this.getNodeByIndex(-1);
		var mode = data.mode;

		// Circular list with the possible colors for the items in the sidebar
		var colorClass = function(index)
		{
			var colors = [ 'blueNeon', 'greenLight', 'orange', 'orangeDark', 'blueLight', 'blueDark', 'green', 'greenDark', 'red', 'yellow', 'pink', 'pinkDark', 'purple', 'darken', 'lighten', 'white', 'grayDark', 'magenta', 'teal', 'redLight', 'blue' ];

			// Select the next item, return to the first
			if (index >= colors.length)
			{
				index = (index++) % colors.length;
			}
			return colors[index];
		}

		// Add the main options
		if (data.fields.length > 0)
		{
			if ($(DocumentLibrary.holder).find("li.documentLibrarySearch").size() == 0)
			{
				var searchSection = $('<li>').attr('class', 'documentLibrarySearch');
				var searchBox = $("<input>").attr("placeholder", "Search");
				var optionNode = $("<a>").attr('class', 'trigger').attr("href", "javascript:;").attr("title", "Search").append($('<i>').attr('class', 'fa fa-lg fa-fw fa-search')).append(searchBox);
				var section = $("<section>").css('display', 'none');
				var sub = $("<div>").attr('class', 'lazytree');
				searchSection.on('keyup', "input", function()
				{
					DocumentLibrary.clear();
					DocumentLibrary.loadData(function()
					{
						$("#sidebar li.documentLibraryContent a").each(function(i, e)
						{
							openContentTree($(e));
						});
					});
				});
				searchSection.append(optionNode);
			}

			$(DocumentLibrary.holder).append(searchSection);

			$.each(data.fields, function(i, e)
			{
				var scrollInterval;
				// Create the navigation item and append it to
				// the sidebar
				var pricingSection = $('<li>').attr('class', 'slideInLeft animated bg-color-' + colorClass(i) + ' txt-color-white documentLibraryContent fancy' + e.label);

				var optionNode = $("<a>").attr('class', 'trigger bg-color-' + colorClass(i) + ' txt-color-white fancy' + e.label).attr("href", "#").attr("title", e.label);
				optionNode.append($('<i>').attr('class', 'fa fa-lg fa-fw fa-' + e.icon));
				optionNode.append($("<span>").attr('class', 'title').text(e.label));
				if (canManageContentLibrary[e.type] == true)
				{
					optionNode.append($("<button>").addClass('btn btn-primary btn-xs manage-content-library-btn pull-right').text('Manage').data('module', e.type));
				}
				optionNode.append($('<i>').attr('class', 'fa fa-fw fa-caret-down pull-right'));

				var section = $("<section>").css('display', 'none');
				var sub = $("<div>").attr('class', 'lazytree lazytree' + e.label);
				pricingSection.append(optionNode);
				section.append(sub);
				pricingSection.append(section);

				// navigation click event
				pricingSection.on("click", "a.trigger", function(e)
				{
					e.preventDefault();
					unminifyContentLibrary();

					toggleContentTree($(this));
				});

				// create the fancytree for navigation item
				$(DocumentLibrary.holder).append(pricingSection);

				sub.fancytree({
					debugLevel : 0,

					// load the first level (category)
					// of items
					source : {
						method : "POST",
						url : '/api/library/content',
						data : {
							mode : mode,
							type : e.type,
							leaf : e.leaf,
							parentId : "" + e.id + "",
							search : $(".documentLibrarySearch input").val()
						},
					},

					// lazy load the childs of one level
					// when it's expanded
					lazyLoad : function(event, data)
					{
						var node = data.node;
						data.result = {
							method : "POST",
							url : '/api/library/content',
							data : {
								mode : mode,
								type : node.data.type,
								parentId : node.key,
								leaf : node.data.nextLeaf,
								search : $(".documentLibrarySearch input").val()
							},
							cache : false
						};
					},

					// create the node model with the
					// response
					postProcess : function(event, data)
					{
						data.result = [];
						$.each(data.response.fields, function(i, e)
						{
							var node = {
								title : escapeHtml(e.label),
								key : e.id,
								folder : e.hasChild,
								lazy : e.hasChild,
								extraClasses : (!e.hasChild ? "fancytree-leaf" : ''),
								type : e.type,
								nextLeaf : e.leaf,
								isLeaf : !e.hasChild
							};
							if (e.type == 'Images' || e.type == "Videos")
							{
								node.thumbnail = e.thumbnail;
								if (isIpad())
								{
									node.extraClasses += ' fancytree-thumbnail-ipad';
								}
								else
								{
									node.extraClasses += ' fancytree-thumbnail';
								}
							}
							if (e.type == 'Products')
							{
								node.price = e.price;
								node.extraClasses += ' fancytree-pricing';
							}
							data.result.push(node);
						});

						if (data.response.fields.length == 0)
						{
							data.result.push({
								title : "No items",
								key : 0,
								folder : false,
								lazy : false,
								extraClasses : "fancytree-leaf",
								isLeaf : false
							});
						}
					},

					loadError : function(event, data)
					{
						logger.info("error loading the tree view:", event, data);
					},

					// creates the DOM element for the
					// node
					createNode : function(event, data)
					{
						var node = data.node;
						var span = $(node.span);
						if (node.statusNodeType == null && node.data.isLeaf)
						{
							if (node.data.type == "Images" || node.data.type == "Videos")
							{
								span.find('.fancytree-expander').remove();
								span.attr('display', 'block');

								var image = $('<img>').addClass('fancytree-list-thumb').attr("title", "Click and drag over to the middle content view").attr('src', node.data.thumbnail);
								var imageDiv = $('<div>').addClass('fancytree-static-image-item').attr("style", "background-image: url('" + node.data.thumbnail + "')");
								var overlayContainer = $('<div>').addClass('fancytree-static-title-overlay');
								var gradientOverlay = $('<div>').addClass('fancytree-static-title-gradient');
								var imageTitleContainer = $('<div>').addClass('fancytree-static-title-container');
								var imageTitleHeader = $('<div>').addClass('fancytree-static-title-container-header');

								var container = $('<a>').addClass('fancytree-static-anchor');
								var controller = $('<div>').addClass('fancytree-static-controller').attr("style", "opacity: 1; width: 156px; height: 126px; margin: 2.5px 0;");

								imageTitleHeader.append($('<h2>').addClass('fancytree-static-title').text(node.title));
								imageTitleContainer.append(imageTitleHeader);
								imageDiv.append(image);
								container.append(imageDiv);
								container.append(gradientOverlay);
								container.append(overlayContainer);
								container.append(imageTitleContainer);
								controller.append(container);

								span.find('.fancytree-icon').replaceWith(controller);
								span.find('.fancytree-title').remove();
								span.parent().attr('type', 'fancy-tree-li');

								fancyTreeRescale();

							}
							span.attr('title', "Click and drag over to the middle content view");
						}
					},
					extensions : [ "glyph", "dnd" ],
					glyph : {
						map : {
							doc : "fa fa-file",
							docOpen : "fa fa-file",
							checkbox : "fa fa-square-o",
							checkboxSelected : "fa fa-check-square-o",
							checkboxUnknown : "fa fa-share-square-o",
							error : "fa fa-exclamation-triangle",
							expanderClosed : "fa fa-plus",
							expanderLazy : "fa fa-plus",
							expanderOpen : "fa fa-minus",
							folder : "fa fa-folder",
							folderOpen : "fa fa-folder-open",
							loading : "fa fa-spinner fa-pulse"
						}
					},

					dblclick : function(event, data)
					{
						var node = data.node;
						if (node.data.isLeaf)
							if (node.data.type == "Products")
							{
								loadPricingItem(getCurrentPricingTable(), node.key);
							}
							else
							{
								// if element is double
								// clicked perform drop
								// to the bottom of the
								// list
								var $currentSection = $(".editorContent").find(".tab-pane.active .contentContainer");
								var index = $(".group-panel", $currentSection).length - 1;
								doDrop(node, index, $currentSection);
							}
					},

					// drag and drop extension for the
					// treeview
					dnd : {
						draggable : {
							distance : 200,

							// creates the temporary
							// draggable item
							helper : function(event)
							{
								var $helper, sourceNode = $.ui.fancytree.getNode(event.target), $nodeTag = $(sourceNode.span);
								$helper = $("<div class='fancytree-drag-helper'></div>");
								if (sourceNode.data.type == "Images" || sourceNode.data.type == "Videos")
								{
									$helper.append($(sourceNode.li).find('.fancytree-list-thumb').clone());
								}
								else
								{
									$helper.append($('<span>').text($nodeTag.find("span.fancytree-title").text()));
								}

								$helper.data("ftSourceNode", sourceNode);
								return $helper;
							},

							// items can only be dragged
							// to dropzones with the
							// same scope
							cancel : ".fancytree-has-children",
							scope : "" + mode,
							appendTo : "body",
							opacity : 0.7,
							cursorAt : {
								top : 56,
								left : 56
							},
							scroll : true
						},
						dragStart : function(node, data)
						{
							// Start a scroll effect to
							// move the editorContent up
							// and down (on desktops
							// only)
							var drop = 0;
							if (mode != "pricing")
							{
								drop = $(".tab-pane.active .contentContainer").height();
							}
							else
							{
								drop = getCurrentPricingPanel().offset().top - $('#doc-pricing .disabledOverlay').offset().top;
							}
							if (data.node.data.isLeaf && drop != undefined)
							{
								$('.group-panel > .dropdown').hide(200);
								$(editorContent).mouseover(function(e)
								{
									e = e || window.event;
									var cursor = {
										y : 0
									};
									cursor.y = e.pageY; // Cursor
									// YPos
									clearInterval(scrollInterval);
									var papaWindow = $(editorContent);
									var $pxFromTop = $(papaWindow).scrollTop();
									var $userScreenHeight = $(papaWindow).height();
									var $contentHeight = $('.tab-pane:visible').height();
									if (cursor.y < $contentHeight)
									{
										if (cursor.y < $contentHeight)
										{
											if ((cursor.y) > (($(window).height()) - 250))
											{
												scrollInterval = setInterval(function()
												{
													$(".editorContent").scrollTop($(".editorContent").scrollTop() + 40)
												}, 50);
											}
											else if (cursor.y < 250)
											{
												scrollInterval = setInterval(function()
												{
													$(".editorContent").scrollTop($(".editorContent").scrollTop() - 40)
												}, 50);
											}
										}
									}
								});
								return true;
							}
							else
							{
								return false;
							}
						},
						dragStop : function(node, data)
						{
							$('.group-panel > .dropdown').show(200);
							$(editorContent).unbind('mouseover');
							clearInterval(scrollInterval);
						}
					}
				});
			});

		}
		// Sushant: Dont think we need a else condition. Possible reasons?...
		// fire events?
	},

	libraryDataLoaded : function(data, textStatus, jqXHR)
	{
		this.libraryData[DocumentLibrary.currentLibraryLevels] = data;
	},

	setLoaded : function(done)
	{
		var addButton = $(".libraryActions .addFromLibraryAction");
		addButton.prop("disabled", true);
		if (done)
		{
			addButton.show();
		}
		else
		{
			addButton.hide();
		}
	},

	addLevel : function(selectElement)
	{

		// selectElement.attr('class', "liblevel"+this.currentLibraryLevels);
		//
		// $(this.holder).append(selectElement);
		// ++this.currentLibraryLevels;
		//    
		//    
		// if (selectElement.attr("size") == undefined) {
		// selectElement.click(function(event) {
		// DocumentLibrary.loadSubItems(event);
		// });
		// // selectElement.change(); // trigger
		// }
	},

	addSelected : function()
	{
		var selected = $(DocumentLibrary.holder).find('select[liblevel=2]').val();

		if (selected != undefined)
		{
			mode.selectItem(selected);
		}
	},

	loadSubItems : function(event)
	{
		// Find the selected nodes ID
		var target = $(event.target);
		var selectedId = target.attr('value');
		if (selectedId != undefined)
		{
			var selectedNode = undefined;

			// Find what level the parent resides on.
			var selectedLevel = parseInt(target.attr('libLevel'));

			// Find the node for the parent
			var levelData = this.libraryData[selectedLevel];
			$.each(levelData.fields, function(i, e)
			{
				if (e.id == selectedId)
				{
					selectedNode = e;
					return false;// break
				}
			});
			this.selectedLibraryNodes[selectedLevel] = selectedNode;

			// Remove the rest of the levels 'above' the selected level.
			$.each($(DocumentLibrary.holder + " li"), function(i, e)
			{
				var level = parseInt($(e).attr('libLevel'));
				if (level > selectedLevel)
				{
					$(e).closest(".form-group").remove();
				}
			});
			this.setLoaded(false);

			this.selectedLibraryNodes = this.selectedLibraryNodes.slice(0, selectedLevel + 1);
			this.currentLibraryLevels = selectedLevel + 1;

			this.loadData();
		}
	},

	getNodeByIndex : function(index)
	{
		// Supports negative indexing (ie -1 = last element)
		if (this.selectedLibraryNodes.length == 0)
		{
			return undefined;
		}
		if (index < 0)
		{
			index += this.selectedLibraryNodes.length;
		}
		return this.selectedLibraryNodes[index];
	}
};

$(window).load(function()
{
	$('.ui-resizable').resize(function()
	{
		fancyTreeRescale();
	});
});

function fancyTreeRescale()
{

	var viewWidth = $('.ui-resizable').width();
	var ul = $("li[type='fancy-tree-li']").parent();
	if (!isIpad())
	{
		if (viewWidth > 364)
		{
			ul.css("column-count", "2");
		}
		if (viewWidth < 364 && viewWidth > 260)
		{
			ul.css("column-count", "1");
		}
		if (viewWidth < 260)
		{
			ul.css("column-count", "1");
		}
	}

}

function loadPricingItem($pricingTable, id)
{
	$.ajax({
		type : "POST",
		url : '/api/pricing/content',
		data : {
			id : id
		}
	}).done(function(data, textStatus, jqXHR)
	{
		addRowToDisplay($pricingTable, data, true);
	});
}

function toggleContentTree(value)
{
	var li = $(value).parent();

	if (li.hasClass("open"))
	{
		closeContentTree(value);
	}
	else
	{
		openContentTree(value);
	}
}

function openContentTree(value)
{
	var li = $(value).parent();
	li.closest("ul").children("li").each(function(index, item)
	{
		if (li.hasClass("open"))
		{
			li.removeClass("open");
			li.find("section").slideUp();
		}
	});
	li.addClass("open");
	li.find("section").slideDown();
}

function closeContentTree(value)
{
	var li = $(value).parent();
	li.removeClass("open");
	li.find("section").slideUp();
}
