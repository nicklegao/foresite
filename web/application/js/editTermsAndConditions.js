var tncAjax = null;

function reloadTermsAndConditions()
{
  var selectedPlans = {};
  $.each(getProposalPricing().plans, function (i, elem){
    selectedPlans[elem.pricingId] = true;
  });
  
  if (tncAjax != null)
  {
    tncAjax.abort();
  }
  tncAjax = $.ajax({
    type: "POST",
    url: '/api/tncForPlans',
    traditional: true,
    data: {
      selectedPlans : Object.keys(selectedPlans)
    }
  }).done(function( data, textStatus, jqXHR ) {
    tncAjax = null;
    
    var keyCount = Object.keys(data).length;
    logger.info("T&C found: "+keyCount);
    
    var tableBody = $("#termsAndConditionsTbl tbody");
    tableBody.html('');
    if (keyCount == 0)
    {
      tableBody.append($("<tr>").append($("<td>")
                                    .attr("colspan",2)
                                    .text("No applicable terms and conditions")
      ));
    }
    else
    {
      $.each(data, function(key, value){
        tableBody.append($("<tr>").append($("<td>").text(key)
                                  ).append($("<td>").append($("<a>").attr("href", value).text(value))
        ));
      })
    }
  });
}
