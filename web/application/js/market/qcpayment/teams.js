$(document).ready(function()
{
	$(document).on("click", "#saveTeambutton", function(e)
	{

		var $modal = $("#settings-qcpayment-teams").closest(".modal");

		var stripePK = $('#stripePK').val();
		var stripeSK = $('#stripeSK').val();
		var teamId = $('#settings-qcpayment-teams').attr("data-team");
		var params = {
			"id" : teamId,
			"stripePK" : stripePK,
			"stripeSK" : stripeSK,
		};

		if (params.length < 1)
		{
			$modal.modal('hide');
			startLoadingForeground();
		}
		else
		{
			$.ajax({
				url : '/api/market/qcpayment/updateTeam',
				data : params,
				success : function(data)
				{
					stopLoadingForeground();
					if (data.success === false)
					{
						swal({
							title : 'Error',
							type : 'error',
							text : data.message
						})
					}
					$modal.modal('hide');
					startLoadingForeground();

				},
				error : function()
				{
					stopLoadingForeground();

					alert("Unable to update your payment settings. Please try again later.");
				}
			});
		}

	});


});
