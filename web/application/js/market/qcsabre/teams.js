$(document).ready(function()
{
	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

	elems.forEach(function(html)
	{
		var switchery = new Switchery(html, {
			size : 'small'
		});
	});


	$('#proposalTemplate').select2();
	$('#itiStyles').select2();

	$(document).on("click", "#saveTeambutton", function(e)
	{

		var $modal = $("#settings-sabre-teams").closest(".modal");

		var sabrePassword = $('#sabrePassword').val();
		var sabreAgencyCode = $('#sabreAgencyCode').val();
		var autoSendItinerary = $('#autoSendItinerary').is(":checked");
		var itiStyle = $('#itiStyles').val();
		var proposalTemplate = $('#proposalTemplate').val();
		var params = {
			"proposalTemplate" : proposalTemplate,
			"id" : teamId,
			"sabrePassword" : sabrePassword,
			"sabreAgencyCode" : sabreAgencyCode,
			"autoSendItinerary" : autoSendItinerary,
			"itiStyle" : itiStyle
		};

		if (params.length < 1)
		{
			$modal.modal('hide');
			startLoadingForeground();
		}
		else
		{
			$.ajax({
				url : '/api/market/sabre/updateTeam',
				data : params,
				success : function(data)
				{
					stopLoadingForeground();
					if (data.success === false)
					{
						swal({
							title : 'Error',
							type : 'error',
							text : data.message
						})
					}
					$modal.modal('hide');
					startLoadingForeground();

				},
				error : function()
				{
					stopLoadingForeground();

					alert("Unable to update your Sabre team settings. Please try again later.");
				}
			});
		}

	});


});
