$(document).ready(function()
{


	$(document).on("click", "#saveUserbutton", function(e) {


		var $modal = $("#settings-sabre-users").closest(".modal");


		var sabreConsultantId = $('#sabreConsultantId').val();
		var params = {"sabreConsultantId": sabreConsultantId, "id": userID};

		if (params.length < 1) {
			$modal.modal('hide');
			startLoadingForeground();
		} else {
			$.ajax({
				url : '/api/market/sabre/updateUser',
				data : params,
				success : function(data)
				{
					stopLoadingForeground();
					if (data.success === false)
					{
						swal({
							title : 'Error',
							type : 'error',
							text : data.message
						})
					}
					$modal.modal('hide');
					startLoadingForeground();

				},
				error : function()
				{
					stopLoadingForeground();

					alert("Unable to update your Sabre user settings. Please try again later.");
				}
			});
		}

	});




});

