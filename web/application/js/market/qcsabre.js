$(document).ready(function()
{

	$('#app-qcsabre-modal .edit-item').click(function()
	{
		var params = {};
		var id = $(this).closest(".btn-group").attr("data-id");
		var module = $(this).closest("table").attr('data-module');

		params.id = id;

		$('body').modalmanager('loading');

		var $modal = $('#edit-management-modal');
		$modal.load('/application/dialog/market/qcsabre/' + module + '.jsp', params, function()
		{
			console.log("editing " + module);
			$modal.modal({
				width : module == 'Role' ? '772px' : '760px',
				backdrop : 'static',
				keyboard : false
			});
		});
	});


});
