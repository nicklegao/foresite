$(document).ready(function()
{
	$('.traveller-booking[flight="1"]').addClass('active');

	$('.prev-word').click(function(e)
	{
		console.log('prev');
		var segment = $(e.target).closest('.traveller-booking');
		segment.removeClass('active');
		var flight = parseInt(segment.attr('flight'));
		flight = (flight - 1) == 0 ? flight : flight - 1;
		$('.traveller-booking[flight=' + flight + ']').addClass('active');
		for (var maps in mapGlobal) {
			google.maps.event.trigger(maps, "resize");
		}
	});

	$('.next-word').click(function(e)
	{
		console.log('next');
		var segment = $(e.target).closest('.traveller-booking');
		segment.removeClass('active');
		var flight = parseInt(segment.attr('flight'));
		var totalFlights = $('.traveller-booking').length;
		flight = (flight + 1) > totalFlights ? flight : flight + 1;
		$('.traveller-booking[flight=' + flight + ']').addClass('active');
		for (var maps in mapGlobal) {
			google.maps.event.trigger(maps, "resize");
		}

	});

	$('div.tools-clickable').click(function(e)
	{
		console.log('tool');
		var elem = $(this).siblings('.hotel-map');
		console.log(elem);
		if ($(elem).is(":visible")) {
			$(elem).first().hide();
		} else {
			initgooglemaps(elem);
		}
	});

	$('div#intineraryCalendar').click(function(e)
	{
		console.log('tool');
	});

	$('div#currencyLink').click(function(e)
	{
		console.log('tool');
	});

	$('div#intineraryEmail').click(function(e)
	{
		console.log('tool');
	});

});


function initgooglemaps(elem)
{

	var theme = [
		{
			"featureType": "administrative",
			"elementType": "labels.text.fill",
			"stylers": [
				{
					"color": "#444444"
				}
			]
		},
		{
			"featureType": "landscape",
			"elementType": "all",
			"stylers": [
				{
					"color": "#f2f2f2"
				}
			]
		},
		{
			"featureType": "poi",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "all",
			"stylers": [
				{
					"saturation": -100
				},
				{
					"lightness": 45
				}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "simplified"
				}
			]
		},
		{
			"featureType": "road.arterial",
			"elementType": "labels.icon",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "transit",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "water",
			"elementType": "all",
			"stylers": [
				{
					"color": "#46bcec"
				},
				{
					"visibility": "on"
				}
			]
		}
	];

	$(elem).first().show();
	$.each($(elem), function(i, el)
	{
		var myOptions = {
			zoom : 16,
			mapTypeId : google.maps.MapTypeId.ROADMAP,
			styles: theme
		};

		var map = new google.maps.Map(el, myOptions);

		mapGlobal.push(map);

		var geocoder = new google.maps.Geocoder();
		var address = el.getAttribute('data-a');
		var markerTitle = el.getAttribute('data-i')
		geocoder.geocode({
			'address' : address
		}, function(results, status)
		{
			if (status == google.maps.GeocoderStatus.OK)
			{
				if (status != google.maps.GeocoderStatus.ZERO_RESULTS)
				{
					map.setCenter(results[0].geometry.location);

					var infowindow = new google.maps.InfoWindow({
						content : '<b>' + address + '</b>',
						size : new google.maps.Size(150, 50)
					});

					var marker = new google.maps.Marker({
						position : results[0].geometry.location,
						map : map,
						title : markerTitle
					});
					google.maps.event.addListener(marker, 'click', function()
					{
						infowindow.open(map, marker);
					});

				}
				else
				{
					alert("No results found");
				}
			}
			else
			{
				alert("Geocode was not successful for the following reason: " + status);
			}
		});
	});

}
