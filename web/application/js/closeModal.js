function checkUpdate($modal)
{
	var $form = $('form', $modal);
	if ($form.length == 0)
	{
		return false;
	}
	$('textarea', $form).each(function()
	{
		this.id && CKEDITOR.instances[this.id] && CKEDITOR.instances[this.id].updateElement();
	});

	$('.spreadsheet', $form).each(function()
	{
		$(this).siblings('input.spreadsheet-val').val(JSON.stringify(removeSpreadsheetSelection($(this).data('kendoSpreadsheet').toJSON())));
	});

	if (!$form.data('serialize'))
	{
		return false;
	}
	if ($form.serialize() == $form.data('serialize'))
	{
		return false;
	}
	return true;
}

function closeModalAfterCheckUpdate($modal)
{
	if (!checkUpdate($modal))
	{

		if ($('.pdfDropzone.dz-clickable').length > 0)
		{
			pdfdropZone.removeAllFiles();
		}
		$modal.modal('hide');
		return;
	}
	swal({
		title : "Want to leave?",
		text : "All your unsaved changes will be lost. Do you want to continue?",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		confirmButtonText : "Yes",
		cancelButtonText : "No",
		closeOnConfirm : true
	}, function()
	{
		$modal.modal('hide');
	});
}

function preCheckUpdate($modal)
{
	var $form = $('form', $modal);
	if ($form.length == 0)
	{
		return;
	}
	$('textarea', $form).each(function()
	{
		this.id && CKEDITOR.instances[this.id] && CKEDITOR.instances[this.id].updateElement();
	});

	$('.spreadsheet', $form).each(function()
	{
		$(this).siblings('input.spreadsheet-val').val(JSON.stringify(removeSpreadsheetSelection($(this).data('kendoSpreadsheet').toJSON())));
	});

	$form.data('serialize', $form.serialize());
}

function bindCloseButtonAction($modal)
{
	$('[data-dismiss="modal"]', $modal).off().click(function(e)
	{
		e.preventDefault();
		e.stopPropagation();
		closeModalAfterCheckUpdate($modal);
	})
}
