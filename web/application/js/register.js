$(function()
{
	var form = $("#register-form").show();
	var sabreVerified = false;
	var emailVerified = false;
	form.steps({
		headerTag : "h3",
		bodyTag : "section",
		transitionEffect : "fade",
		// transitionEffectSpeed : 0,
		onStepChanging : function(event, currentIndex, newIndex)
		{
			// Allways allow previous action even if the current form is not
			// valid!
			if (currentIndex > newIndex)
			{
				return true;
			}
			// Needed in some cases if the user went back (clean up)
			if (currentIndex < newIndex)
			{
				// To remove error styles
				form.find(".body:eq(" + newIndex + ") label.error").remove();
				form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
			}
			if (!form.valid())
			{
				return false;
			}
			if (currentIndex == 0)
			{
				if (sabreVerified)
					return true;
				verifySabreClient();
				return sabreVerified;
			}
			if (currentIndex == 1)
			{
				if (emailVerified)
					return true;
				verifyEmail();
				return emailVerified;
			}
			if (currentIndex == 2)
			{
				return true;
			}
			if (currentIndex == 3)
			{
				return true;
			}
			console.log(event, currentIndex, newIndex);
			return false;
		},
		onStepChanged : function(event, currentIndex, priorIndex)
		{
			if (currentIndex == 0)
			{
				sabreVerified = false;
			}
			if (currentIndex == 1)
			{
				emailVerified = false;
			}
		},
		onFinishing : function(event, currentIndex)
		{
			form.validate().settings.ignore = ":disabled";
			return form.valid();
		},
		onFinished : function(event, currentIndex)
		{
			alert("Submitted!");
		},
		onInit : function(event, currentIndex)
		{
			$("input.form-control, textarea.form-control", form).fuseMdInput()
		},
	}).validate({
		errorPlacement : function errorPlacement(error, element)
		{
			element.parent().append(error);
		},
		rules : {
			email : {
				email : true,
			},
			repeatPassword : {
				equalTo : '#password',
			},
			verificationCode : {
				remote : {
					url : "/api/free/email/verify2",
					type : "post",
					data : {
						verificationCode : function()
						{
							return $("#verificationCode").val();
						}
					}
				}
			}
		}
	});

	function verifySabreClient()
	{
		$.ajax('/api/free/sabre/verify', {
			type : "POST",
			data : {
				sabreClientId : form.find('[name="sabreClientId"]').val(),
				sabreClientSecret : form.find('[name="sabreClientSecret"]').val(),
			}
		}).done(function(data, textStatus, jqXHR)
		{
			if (data && data.success)
			{
				sabreVerified = true;
				form.steps('next');
			}
			else
			{
				swal("Error", "Your Sabre account couldn't be verified.", "error");
			}
		});
	}

	function verifyEmail()
	{
		var email = form.find('[name="email"]').val();
		$.ajax('/api/free/email/verify1', {
			type : "POST",
			data : {
				firstname : form.find('[name="firstname"]').val(),
				lastname : form.find('[name="lastname"]').val(),
				email : email,
			}
		}).done(function(data, textStatus, jqXHR)
		{
			if (data && data.success)
			{
				emailVerified = true;
				swal({
					title : "Email sent",
					text : "An email with the verification code has been sent to your email address. Please check your email and put it in the form.",
					type : "info"
				}, function()
				{
					form.find('[name="verificationCode"]').val(data.data);
					form.steps('next');
				});
			}
			else
			{
				swal("Error", data.data, "error");
			}
		});
	}
})
