(function( $ ) {
	$.fn.bsAlert = function(options) {
		var settings = $.extend({
			// These are the defaults.
			id: "bsAlert"+new Date().getTime(),
			dataBackdrop: "static",
			dataKeyboard: "false",
			alertClass: "alert-info",
			icon: $("<i>").attr("class", 'fa fa-info-circle'),
			title: "title",
			message: "message",
			buttons: [{
				text:"Close"
				}]
		}, options );
		
		var displayButtons = [];
		$.each(settings.buttons, function(i,e){
			var buttonSettings = $.extend({
				buttonClass:"btn btn-default",
				icon:"",
				attrs:[{"data-dismiss":"modal"}]
			}, e);
			var button = $("<button>")
				.attr("class", buttonSettings.buttonClass)
				.append(buttonSettings.icon).append(" "+buttonSettings.text);
			$.each(buttonSettings.attrs, function(i, setAttr){
				$.each(setAttr, function(attr, attrVal){
					button.attr(attr, attrVal);
				});
			});
			displayButtons.push(button);
		});
		
		var heading = $("<h4>").addClass('alert-heading')
			.append(settings.icon).append(" "+settings.title);
		
		var alert = $("<div>")
			.attr("class", "alert alert-block fade in "+settings.alertClass)
			.css({
				'margin':0
			})
			.append(heading)
			.append($("<p>").text(settings.message))
			.append($("<p>").append(displayButtons));
		
		var dialog = $("<div>")
				.attr("id", settings.id)
				.attr("class", "modal fade")
				.attr("data-backdrop", settings.dataBackdrop)
				.attr("data-keyboard", settings.dataKeyboard)
				.append(alert);
		
		this.append(dialog);
		dialog.modal('show');
		
		return this;
	};

}( jQuery ));
