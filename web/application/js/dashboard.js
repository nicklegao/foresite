var proposalTable = undefined;
var tableColumnDefs = undefined;
var tableColumnOrder = [];
var searchHidden = true;
var autoLogoutWarning = undefined;
var refreshTimer;
var refreshTimeout;

$(document).ready(function()
{

	refreshTimer = setInterval(function()
	{
		// code goes here that will be run every 5 seconds.
		console.log("IN");
		redrawTable();
	}, (1000 * 60) * 5);
	clearTimeout(refreshTimeout);
	refreshTimeout = setTimeout(function()
	{
		// code goes here that will be run every 5 seconds.
		console.log("Out");
		clearInterval(refreshTimer);

	}, (1000 * 60) * 60);

	$(document).on("click", ".helpAction", function(event)
	{
		helpAction($(event.target).attr("data-content"));
	});

	$(document).ajaxComplete(function(event, xhr, ajaxOptions)
	{
		if (xhr.status == 401)
		{
			// logger.info("HTTP 401(Unauthorised) recieved... Logging out due
			// to inactivity.");
			// alert("Unfortunately you have been logged out due to inactivity.
			// Please login again.");

			setTimeout(function()
			{
				window.location = "/";
			}, 600);
		}

		// THIS IS FOR THE SESSION TIME AND USERNAME:

		var sessionLife = xhr.getResponseHeader("session-lifetime");
		if (sessionLife != undefined)
		{
			var seconds = parseInt(sessionLife);
			var logoutAt = moment().add(seconds, 'seconds');

			var warningText = $(".autologoutTime").text(logoutAt.format('h:mm:ss a')).css("color", "inherit");
			$('.tv-contact-services').hide();
			$('i.fa-clock-o.fa-spin').hide();
			if (autoLogoutWarning != undefined)
			{
				clearTimeout(autoLogoutWarning);
			}
			autoLogoutWarning = setTimeout(function()
			{
				warningText.css("color", "orange");
				$('.tv-contact-services').show();

				autoLogoutWarning = setTimeout(function()
				{
					warningText.css("color", "red");
					$('.tv-pulse-btn').css("background", "red");
					$('.tv-pulse-btn').css("box-shadow", "0 0 0 0 #f0f0f0, 0 0 0 0 rgba(255,0,0,0.7)");

				}, (2000 * 60));
			}, (seconds * 1000) - (2000 * 60));
		}
	});
	$(document).on("click", ".createProposalAction", createProposalAction);
	$(document).on("click", ".displayProposalManagementAction", displayProposalManagementAction);
	$(document).on("click", ".displayProposalVersionAction", displayProposalVersionAction);
	$(document).on("click", ".displayProposalNotesAction", displayProposalNotesAction);
	$(document).on("click", ".displayProfileManagementAction", displayProfileManagementAction);
	$(document).on("click", ".displayNotificationManagementAction", displayNotificationManagementAction);
	$(document).on("click", ".displayCommentsFeedAction", displayCommentsFeedAction); //
	$(document).on("click", ".accountSettingsAction", displayAccountSettingsAction);
	$(document).on("click", ".diskUsage", displayDiskUsageAction);
	$(document).on("click", ".learningCenter", displayLearningCenterAction);
	$(document).on("click", ".changeSubscriptionAction", displayChangeSubscriptionAction);
	$(document).on("click", ".trialSubscriptionAction", displayChangeSubscriptionAlertAction);
	$(document).on("click", ".subscriptionCancellationAction", subscriptionCancellationAction);
	$(document).on("click", ".showInvoices", showInvoices);

	$(document).on("click", ".editStylesAction", editStylesAction);
	$(document).on("click", ".coverPageManagementAction", coverPageManagementAction);
	$(document).on("click", ".templateManagementAction", templateManagementAction);
	$(document).on("click", ".usersManagementAction", usersManagementAction);
	$(document).on("click", ".rolesManagementAction", rolesManagementAction);
	$(document).on("click", ".teamsManagementAction", teamsManagementAction);
	$(document).on("click", ".settingsManagementAction", settingsManagementAction);
	$(document).on("click", ".customFieldsAction", customFieldsAction);
	$(document).on("click", ".editLabelsAction", editLabelsAction);
	$(document).on("click", ".pricingManagementAction", pricingManagementAction);
	$(document).on("click", ".contactManagementAction", contactManagementAction);
	$(document).on("click", ".doResendProposal", function(button)
	{
		$('body').modalmanager('loading');
		var proposalId = $(this).attr('proposalid');

		var $modal = $('#send-proposal-modal');
		$modal.load('/application/dialog/sendProposal.jsp?proposal=' + proposalId, '', function()
		{
			$modal.modal('show');
			$modal.modal({
				width : "650px"
			});
		});
	});

	$(document).on("click", ".displaySaveTemplateAction", displaySaveTemplateAction);
	$(document).on("click", ".displayDeleteProposalAction", displayDeleteProposalAction);

	$(document).on("click", ".createProposalRevisionAction", createProposalRevisionAction);
	$(document).on("click", ".proposalDeleteConfimed", proposalDeleteConfimed);
	$(document).on("click", ".regenerateApiCredentialsAction", regenerateApiCredentialsAction);
	$(document).on("click", ".copyProposalAction", copyProposalAction);

	$(document).on('click', ".add-contacts-action", function()
	{
		addEditContacts.call($(this), true);
	});

	$(document).on('click', ".edit-contacts-action", function()
	{
		addEditContacts.call($(this), false);
	});

	$(document).on('click', ".delete-contact-action", function(e)
	{
		deleteContactAction.call($(this), e);
	})

	$(document).on('click', ".export-all-contacts-action", function()
	{
		exportContactsAction.call($(this), false);
	});
	$(document).on('click', ".download-sample-contacts-action", function()
	{
		exportContactsAction.call($(this), true);
	});
	$(document).on('click', ".import-all-contacts-action", function()
	{
		importContactsAction.call($(this));
	});

	if (userCount > 1)
	{
		$(document).on("click", ".handoverProposalDashboardAction", handoverProposalDashboardAction);
	}
	else
	{
		$(document).on("click", ".handoverProposalDashboardAction", handoverProposalDashboardActionError);
	}
	$(document).on("click", ".downloadTableData", downloadTableData);

	$('.modal').on('shown.bs.modal', function()
	{
		setupModalsBackgroundColor();
	});

	// $(".accountAccessHelper").iCheck({
	// checkboxClass : 'icheckbox_square-orange col-sm-1',
	// radioClass : 'iradio_square-orange'
	// });

	$(".noteEmailHelper, .accountAccessHelper").each(function()
	{
		if (!($(this).hasClass('switchActive')))
		{
			var switcheryHide = new Switchery(this, {
				color : '#009b46',
				size : 'small'
			});

			$(this).siblings(".switchery").css("width", "60px").prepend("<span title=''>Yes</span>").find("small").css("left", "0px");
			$(this).addClass('switchActive');

			$(this).change(function()
			{
				if (!($(this).is(':checked')) && $('#globalTitleHidden').is(':checked'))
				{
					$('#globalTitleHidden').click();
				}
				if ($(this).is(':checked'))
				{
					$(this).siblings(".switchery").find("span").text('Yes');
					$(this).siblings(".switchery").find("span").addClass('hideLabel');
				}
				else
				{
					$(this).siblings(".switchery").find("span").text('No');
					$(this).siblings(".switchery").find("span").removeClass('hideLabel');
				}
			}).change();
		}
	});

	// $(".noteEmailHelper").iCheck({
	// checkboxClass : 'icheckbox_square-orange col-sm-1',
	// radioClass : 'iradio_square-orange'
	// });


	$('.cd-panel').on('click', function(event)
	{
		if ($(event.target).is('.cd-panel') || $(event.target).is('.cd-panel-close'))
		{
			event.preventDefault();
			$('.cd-panel').toggleClass('is-visible');
			$('.cd-panel-container').removeAttr('style');
			$('div.sideBar-backDrop').hide();
		}
	});


	$(document).on("click", ".scroll-table-left", function()
	{
		event.preventDefault();

		$('.dataTables_scrollBody').animate({
			scrollLeft : "-=200px"
		}, "slow");
	})

	$(document).on("click", ".scroll-table-right", function()
	{
		event.preventDefault();
		$('.dataTables_scrollBody').animate({
			scrollLeft : "+=200px"
		}, "slow");
	})

	$(window).on('resize', function()
	{
		var scrollMore = $('.scrollmore-wrapper:visible');
		if (scrollMore.length > 0)
		{
			updateScrollModalItem(scrollMore);
		}
	});

	$(document).on('shown.bs.modal', '.modal', function()
	{
		var childModal = $(this).hasClass('child-modal');
		if (!childModal)
		{
			// setupModalScrollMoreIcon();
		}
	});

	$('#create-proposal-modal').keydown(function(event)
	{
		if (event.keyCode == 13)
		{
			event.preventDefault();
			return false;
		}
	});

	NProgress.configure({
		showSpinner : false,
		parent : '.contentContainer',
		trickleSpeed : 50
	});

	if (trialExpired)
	{
		displayChangeSubscriptionAction();
	}
	else if (trialAlmostExpired)
	{
		displayChangeSubscriptionAlertAction();
	}
	else if (noPhoneNumber)
	{
		displayProfileManagementAction();
	}
	else if (showFirstLogin)
	{
		displayFirstLoginAction();
	}
	else if (accountSuspended)
	{
		displayAccountSuspendedAction();
	}

	updateCommentNotificationCounter();

	// Set width for resizing window
	var windowWidthTimer;

	// On windows focus (moving from other app or tab) reload the website data
	$(window).on('focus', function()
	{
		redrawTable();
		updateCommentNotificationCounter();
	});


	// On windows resize (greater) reload datatable
	$(window).on('resize', function()
	{
		blurTable(true, false);
		clearTimeout(windowWidthTimer);
		windowWidthTimer = setTimeout(resizeFinished, 500);
	});

	$(document).on('click', '[data-action="launchFullscreen"]', function(e)
	{
		if (!$('body').hasClass("full-screen"))
		{

			$('body').addClass("full-screen");

			if (document.documentElement.requestFullscreen)
			{
				document.documentElement.requestFullscreen();
			}
			else if (document.documentElement.mozRequestFullScreen)
			{
				$(document.documentElement).mozRequestFullScreen();
			}
			else if (document.documentElement.webkitRequestFullscreen)
			{
				document.documentElement.webkitRequestFullscreen();
			}
			else if (document.documentElement.msRequestFullscreen)
			{
				document.documentElement.msRequestFullscreen();
			}
		}
		else
		{

			$('body').removeClass("full-screen");

			if (document.exitFullscreen)
			{
				document.exitFullscreen();
			}
			else if (document.mozCancelFullScreen)
			{
				document.mozCancelFullScreen();
			}
			else if (document.webkitExitFullscreen)
			{
				document.webkitExitFullscreen();
			}

		}
		e.preventDefault();
	});

	// disable line break(enter key) in save template form
	$(document).on('keypress', '.form-save-as-template', function(event)
	{
		if (event.which == 13)
		{
			event.preventDefault();
		}
	});

	if (tutorialFirstLoad == "true")
	{
		setTimeout(function()
		{
			waitForStartupHidden("#first-login-modal");
		}, 4000);
	}
	$(document).on('click', '.tourStart', welcomeTour);
	$(document).on('click', '.guide-creating-proposal', guideCreatingProposal);
	$(document).on('click', '.guide-save-template', guideSavingTemplate);
	$(document).on('click', '.guide-filter-proposals', guideFilterProposal);
	$(document).on('click', '.guide-export-proposals', guideExportproposals);
	$(document).on('click', '.guide-delete-proposals', guideDeleteproposals);
	$(document).on('click', '.guide-DM-pricing', guideDMPricing);
	$(document).on('click', '.guide-preview-proposals', guidePreviewproposals);
	$(document).on('click', '.guide-reorder-columns', guideReorderColumns);
	$(document).on('click', '.guide-sabre-setup', guideSebreSetup);
	// $(document).on('click', '#tutorialButton', guideProposalEditor);

	$('#add-edit-contacts-form').validate({
		rules : {
			assetName : {
				required : true,
				email : true
			},
		},
		submitHandler : function(form)
		{
			$(form).ajaxSubmit({
				success : function(response)
				{
					if (response.success)
					{
						swal({
							title : 'Success!',
							type : 'success',
							text : response.message,
							html : true
						});
						$('#contacts-add-edit-modal').modal('hide');
						// $('#contacts-management-modal').load('/api/contacts/manage',
						// {}, function()
						// {
						// initTable($('#contacts-table',
						// $('#contacts-management-modal')));
						// });
						contactsTable.draw('page');
						$(form).clearForm();
					}
					else
					{
						swal({
							title : 'Error',
							type : 'error',
							text : response.message,
							html : true
						})
					}
				}
			});
		}
	});

	$('#import-contacts-form').validate({
		ignore : '',
		rules : {
			csvUpload : "required",
		},
		submitHandler : function(form)
		{
			startLoadingForeground();
			$(form).ajaxSubmit({
				success : function(response)
				{
					stopLoadingForeground();
					if (response.success)
					{
						startContactImportStatusCheck();
					}
					else
					{
						swal({
							title : "Error",
							type : "warning",
							text : response.message,
							closeOnConfirm : true,
							html : true,
						});
					}
				}
			});
		}
	});
});

function resizeFinished()
{
	proposalTable.columns.adjust().draw();
	checkRowHeight();
	blurTable(false, false);
	var parentWidth = $('.DTFC_RightBodyWrapper').width();
	$('.DTFC_RightBodyLiner').attr('style', 'width:' + parentWidth + 'px !important;');
}

// Function to blur the proposal table/control notification loading and ignore
// when searching using filters
function blurTable(on, search)
{

	$('.proposalResizeBlur').height($('.DTFC_ScrollWrapper').height() + 1);
	$('.proposalResizeBlur').width($('.DTFC_ScrollWrapper').width() + 1);

	if (on && !search)
	{
		if (!($('.proposalResizeBlur').is(':visible')))
		{
			// showNotificationMessage(true);
			showLoadingCog(true);
			NProgress.start();
			$('.proposalResizeBlur').removeClass('hidden');
			// $('.DTFC_ScrollWrapper').addClass('scroll-fade-in'); //removed
			// blur effect in preference for opaque white
		}
	}
	else if (!on && !search)
	{
		if ($('.proposalResizeBlur').is(':visible'))
		{
			// showNotificationMessage(false);
			showLoadingCog(false);

			NProgress.done();
			$('.proposalResizeBlur').addClass('hidden');
			// $('.DTFC_ScrollWrapper').removeClass('scroll-fade-in'); //removed
			// blur effect in preference for opaque white
		}
	}
}


function sendProposal()
{

}

function setupModalsBackgroundColor()
{
	var visible = $('.modal:visible');
	visible.removeClass('child-modal').slice(1).addClass('child-modal');
}

function setupModalScrollMoreIcon()
{
	var currentOverflow = $('.modal-scrollable:visible').css('overflow');
	var scrollableHeight = $('.modal-scrollable:visible').height();
	var modalHeight = $('.modal:visible').height();
	if (modalHeight > scrollableHeight)
	{
		var scrollmore = $('<div class="scrollmore-wrapper"><div class="mouse"></div><p>Scroll for more</p></div>')
		$('.modal-scrollable:visible').find('.modal').first().append(scrollmore);
		updateScrollModalItem(scrollmore);
		var initalScrollTop = $('.modal-scrollable').scrollTop();
		$('.modal-scrollable:visible').last().on('scroll', function()
		{
			if ($('.scrollmore-wrapper:visible').length > 0 && $('.modal-scrollable').scrollTop() > initalScrollTop)
			{
				$('.scrollmore-wrapper').hide()
				$('.modal-scrollable:visible').unbind('scroll');
			}
		});
		$(window).on('resize', function()
		{
			if ($('.scrollmore-wrapper:visible').length > 0 && $('.modal-scrollable').scrollTop() == 0)
			{
			}
		});

		$("li[role='presentation']").on("click", function()
		{
			var ele = $('.modal-scrollable:visible').last();
			var child = $(ele).children("div.top-modal");
			if ($(child).height() < $(ele).height())
			{
				$('.scrollmore-wrapper').hide()
				$('.modal-scrollable:visible').unbind('scroll');
			}
			else
			{
				if (!($('.scrollmore-wrapper:visible')))
				{
					var scrollmore = $('<div class="scrollmore-wrapper"><div class="mouse"></div><p>Scroll for more</p></div>')
					$('.modal-scrollable:visible').find('.modal').last().append(scrollmore);
					updateScrollModalItem(scrollmore);
					var initalScrollTop = $('.modal-scrollable').scrollTop();
				}
			}
		});
	}
}

function updateScrollModalItem(element)
{
	var $modal = $(element).closest('.modal-scrollable').find('.modal');
	$(element).css('bottom', -($modal.closest('.modal-scrollable').scrollTop())).css('width', $modal.outerWidth());
	return element;
}

function isTouchSupported()
{
	// return ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera
	// Mini/i.test(navigator.userAgent) );
	return 'ontouchstart' in document.documentElement;
};

// For the table
var TableData = function()
{
	$.fn.dataTable.ext.errMode = function(event, settings, techNote, tn)
	{
		console.log('An error has been reported by DataTables: ', techNote, tn);
	};

	var tableLenght = localStorage['tableLenght'] ? localStorage['tableLenght'] : 10;
	var currentDashboardDataRequest = undefined;
	var filter = localStorage['filters'] ? JSON.parse(localStorage["filters"]) : [];
	$.each(filter, function(i, e)
	{
		$('.status-bucket-' + e).addClass('active');
	});
	var endDate = moment();
	var startDate = moment().subtract(12, 'months');

	var domHead = "<'navbar' <'navbar-form navbar-left'<'form-group' l><'#dateRangePicker'><'#columns-list.form-group'>> <'navbar-form navbar-right'<'form-group' f>> ><'proposalResizeBlur hidden'>";
	var domBody = 't'; // To add colResize place "Z" (capital) before t
	var domFooter = "<'navbar' <'navbar-form navbar-left' i> <'navbar-form navbar-right'<'form-group' p>> <'navbar-form navbar-right' <'#actionsHolder'>> >";

	var runSetupPlaceholders = function()
	{
		$('input, textarea').placeholder();
	};

	// var customFields = JSON.parse($('#custom-fields-data').text());

	var runDataTable = function()
	{
		if (isTouchSupported())
			var proposalsToShow = 5;
		else
			var proposalsToShow = tableLenght;

		tableColumnDefs = [ {
			title : '',
			width : '50',
			orderable : false,
			data : 'attr_status',
			render : function(data, type, row, rowData)
			{
				if (type == "display")
				{

					// if (rowData.notifications > 0)
					// {
					// $(td).append($('<div>').attr('class', 'qc qc-chat
					// notificationCtr').append($('<span>').text(rowData.notifications)));
					// }
					return ("<div style='overflow: hidden;max-height: 32px;'><div style='max-height: 32px; overflow: hidden;'><i class='qc qc-" + row.statusBucket + "'></i></div></div>")
				}
				else if (type == "filter")
				{
					return row.statusBucket;
				}
				return null;
			},
			createdCell : function(td, cellData, rowData, row, col)
			{
				if (rowData.notifications > 0)
				{
					setInterval(TogglePanels, 1000);
				}
				$(td).attr('class', 'statusCell text-center');
			}
		}, {
			title : 'Project Name',
			data : 'projectName',
			ciFilterType : 'dropdown',
			ciDataValues : userAccess,
			width : '70',
			visible : false,
			render : function(data, type, row)
			{
				if (type == 'sort')
					return ("<div style='max-height: 32px; overflow: hidden;'>" + row.ownerName.htmlEscaped() + " " + row.ownerSurname.htmlEscaped() + "</div>");
				return ("<div style='max-height: 32px; overflow: hidden;'>" + row.ownerName.htmlEscaped() + " " + row.ownerSurname.htmlEscaped() + "</div>");
			}
		}, {
			title : 'ID',
			name : function()
			{
				return 'raf';
			},
			data : 'element_id',
			ciFilterType : 'text',
			width : '70',
			createdCell : function(td, cellData, rowData, row, col)
			{
			}
		}, {
			title : 'Task',
			ciFilterType : 'text',
			width : '100',
			data : 'attr_headline',
			render : function(data, type, row)
			{
				if (type == 'sort')
					return ("<div style='min-width: 70px !important; max-height: 32px; overflow: hidden; -ms-text-overflow: ellipsis;text-overflow: ellipsis;'>" + data.htmlEscaped() + "</div>");
				return ("<div style='min-width: 70px !important; max-height: 32px; overflow: hidden; -ms-text-overflow: ellipsis;text-overflow: ellipsis;'>" + data.htmlEscaped() + "</div>");
			},
			createdCell : function(td, cellData, rowData, row, col)
			{
			},
		}, {
			title : 'ID',
			data : 'id',
			ciFilterType : 'text',
			width : '70',
			data : 'attr_externalid',
			render : function(data, type, row)
			{
				if (type == 'sort')
					return data;
				return data;
			},
			createdCell : function(td, cellData, rowData, row, col)
			{
			},
		}, {
			title : 'Start Date',
			data : 'attrdate_startdate',
			width : '180',
			render : function(data, type, row)
			{
				if (type == 'sort')
					return ("<div style='max-height: 32px; overflow: hidden;'>" + data + "</div>");
				return ("<div style='max-height: 32px; overflow: hidden;'>" + formatDateForDisplay(data) + "</div>");
			}
		}, {
			title : 'End Date',
			data : 'attrdate_enddate',
			width : '180',
			render : function(data, type, row)
			{
				if (type == 'sort')
					return ("<div style='max-height: 32px; overflow: hidden;'>" + data + "</div>");
				return ("<div style='max-height: 32px; overflow: hidden;'>" + formatDateForDisplay(data) + "</div>");
			}
		}, {
			data : 'status',
			ciFilterType : 'dropdown',
			ciDataValues : [ {
				'value' : 'creating',
				'label' : 'Creating'
			}, {
				'value' : 'sent',
				'label' : 'Sent'
			}, {
				'value' : 'read',
				'label' : 'Read'
			}, {
				'value' : 'comments',
				'label' : 'Comments'
			}, {
				'value' : 'revising',
				'label' : 'Revising'
			}, {
				'value' : 'bounced',
				'label' : 'Bounced'
			}, {
				'value' : 'accepted',
				'label' : 'Accepted'
			}, {
				'value' : 'closed',
				'label' : 'Closed'
			}, {
				'value' : 'lost',
				'label' : 'Lost'
			} ],
			title : 'Status',
			width : '80',
			render : function(data, type, row)
			{
				// if (type=='filter') return row.statusBucket;
				return data;
			},
			createdCell : function(td, cellData, rowData, row, col)
			{
			}
		}, {
			title : 'Action',
			ciFilterType : 'clear',
			width : '122px',
			orderable : false,
			render : function(data, type, row)
			{
				if (type == "display")
				{
					return getOptionsButton(row);
				}
				return null;
			},
		} ];

		proposalTable = $('#matchedProposals').DataTable({
			"autoWidth" : false,
			"scrollY" : false,
			"scrollX" : true,
			"scrollCollapse" : true,
			"fixedColumns" : {
				"leftColumns" : 1,
				"rightColumns" : 1,
				"sHeightMatch" : "none"
			},
			"processing" : true,
			"serverSide" : true,
			"ajax" : {
				"url" : "/api/dashboard",
				"method" : "POST",
				"data" : function(d)
				{
					d.endDate = endDate.format('YYYY-MM-DD ZZ');
					d.startDate = startDate.format('YYYY-MM-DD ZZ');

					d.statusFilter = filter.join();
				}
			},
			"createdRow" : function(row, data, dataIndex)
			{
				$(row).attr("data-id", data.id).attr("data-vkey", data.vKey).addClass("status-bucket-" + data.statusBucket);
				// showNotificationMessage(true, 'Working...');
				NProgress.start();
				showScrollButtons();
			},
			"aoColumns" : tableColumnDefs,
			"colReorder" : {
				"fixedColumnsLeft" : 1,
				"fixedColumnsRight" : 1,
				"realtime" : false,
				"fnReorderCallback" : function()
				{
					setupVisibility();
					var show = $('.dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow').size() != 0;
					rebuildColumnFilters(show);
				}
			},
			"oLanguage" : {
				"sLengthMenu" : "_MENU_",
				"sSearch" : "",
				"oPaginate" : {
					"sPrevious" : "",
					"sNext" : ""
				},
				"sInfo" : '<i class="fa fa-spin fa-cog dashboardLoadingIndicator"></i> Showing _START_ to _END_ of _TOTAL_ proposals',
				"sInfoFiltered" : ' - filtered from _MAX_ proposals',
				"sInfoEmpty" : "No TravelDocs found",
			},
			"aaSorting" : [ [ 12, 'desc' ] ],
			"aLengthMenu" : [ [ 10, 20, 40, 70, 100 ], [ 10, 20, 40, 70, 100 ] ],
			// set the initial value
			"iDisplayLength" : proposalsToShow,
			"sDom" : domHead + domBody + domFooter,
			"stateSave" : true,
			"stateDuration" : -1,
			"stateSaveCallback" : function(settings, data)
			{
				// Load saving to the database here (save state of users
				// datatable)
				var dashboardData = JSON.stringify(data);
				$.ajax({
					url : "/api/dashboard_state_save",
					data : {
						'data' : dashboardData,
					},
					dataType : "json",
					type : "POST",
					success : function()
					{


					}
				});
			},
			"stateLoadCallback" : function(settings)
			{
				var o;
				$.ajax({
					"url" : "/api/dashboard_state_load",
					"async" : false,
					"dataType" : "json",
					"success" : function(json)
					{
						o = json;
					}
				});
				return o;
			},
			"initComplete" : function()
			{
				// Check the width of the main table and reset the width of the
				// right and left tables
				var columnHeightTimer;
				clearTimeout(columnHeightTimer);
				columnHeightTimer = setTimeout(formatTablecolumns, 200);
			},
		});

		proposalTable.on('draw.dt', function()
		{

			blurTable(false, false)
			// showNotificationMessage(false);
			showLoadingCog(false);
			NProgress.done();

			if (currentDashboardDataRequest != undefined)
			{
				currentDashboardDataRequest.abort();
			}
			currentDashboardDataRequest = $.ajax({
				url : '/api/dashboard/summary',
				method : 'post'
			}).done(function(data, textStatus, jqXHR)
			{
				logger.info("Start rendering after posting to /api/dashboard/summary and start rendering.");
				currentDashboardDataRequest = undefined;

				$statusSummary = $(".buckets");
				dataMap = {};

				$.each(data, function(status, val)
				{
					var totalSummary = $('.status-bucket-' + status + " .totalSummary", $statusSummary);
					var currentText = totalSummary.text();
					var currentVal = parseInt(currentText);
					if (currentVal != val)
					{
						if (isNaN(currentVal))
							currentVal = 0;

						var change = Math.max(1, Math.floor(Math.abs(val - currentVal) / 10));
						var loop = setInterval(function()
						{
							if (currentVal < val)
							{
								currentVal = Math.min(currentVal + change, val);
							}
							else if (currentVal > val)
							{
								currentVal = Math.max(currentVal - change, val);
							}
							else
							{
								clearInterval(loop);
								totalSummary.animateCss("rubberBand");
							}
							totalSummary.text(currentVal);
						}, 50);
						// special case for the needsAction bucket
						var li = $('.status-bucket-' + status, $statusSummary);
						if (status == "needsAction" && val > 0)
						{
							li.addClass('flashes');
						}
						else
						{
							li.removeClass('flashes');
						}
					}


				});
				logger.info("Finish rendering after posting to /api/dashboard/summary");
				var parentWidth = $('.DTFC_RightBodyWrapper').width();
				$('.DTFC_RightBodyLiner').attr('style', 'width:' + parentWidth + 'px !important;');

				showScrollButtons();
			});
		});

		proposalTable.on('column-visibility.dt', function(e, settings, column, state)
		{
			rebuildColumnFilters();
		});
		if ($('.dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow').size() == 0)
		{
			rebuildColumnFilters();
		}

		// modify table search input
		// $('#matchedProposals_wrapper .dataTables_filter').prepend('<label
		// class="search-label" for="search-proposals"><span class="search-icon
		// fa fa-search"></span></label>');

		var filterButton = $('<button class="ripple btn btn-default columnFilterCount" title="Filter"><i class="fa fa-filter"></i> <span id="filterLabel">Filter</span></button>').on('click', function()
		{
			$(".toggleColumnFilters").click();
		});
		$('#matchedProposals_wrapper .dataTables_filter').prepend(filterButton);
		$('#matchedProposals_wrapper .dataTables_filter input')
		// .attr('id', 'searchProposals').addClass("search-box
		// form-control").attr("placeholder", "Search...")
		.hide();

		// modify table paginate dropdown
		$('#matchedProposals_wrapper .dataTables_length select').addClass("form-control");
		$('#matchedProposals_wrapper .dataTables_length').closest(".form-group").before($("<span>").text('Show')).after($("<span>").text('proposals '));
		$('#matchedProposals_wrapper .dataTables_length select').change(function(event)
		{
			var selected = $(':selected', this).val();
			localStorage.setItem('tableLenght', selected);
		});

		// Attach events
		$(".buckets > li").click(function(e)
		{
			var bucket = $(e.currentTarget).attr('data-bucket');
			$(e.currentTarget).toggleClass("active");
			var selected = $('#sparks li.active');
			filter = [];
			$.each(selected, function(i, e)
			{
				filter.push($(e).attr('data-bucket'));
			});
			localStorage.setItem('filters', JSON.stringify(filter));
			redrawTable(true);
		});

		// Create the date range picking dropdown...
		var $input = $("<input>").attr("type", "text").attr("class", "form-control between").attr("name", "daterange").attr("data-last", startDate.valueOf());

		$("#dateRangePicker").attr("class", "input-daterange input-group").append($("<span>").text("between ")).append($("<div>").attr('class', 'form-group').append($input));

		if (isTouchSupported())
		{
			$(".startDate, .endDate").attr("readonly", "true");
		}
		$input.daterangepicker({
			autoApply : true,
			showDropdowns : true,
			startDate : startDate,
			endDate : endDate,
			ranges : {
				'Last 30 Days' : [ moment().subtract(29, 'days'), moment() ],
				'Last 60 Days' : [ moment().subtract(59, 'days'), moment() ],
				'Last 90 Days' : [ moment().subtract(89, 'days'), moment() ],
				'Last 12 Months' : [ moment().subtract(12, 'months'), moment() ],
				'Last 24 Months' : [ moment().subtract(24, 'months'), moment() ]
			},
			locale : {
				format : dateFormatPattern,
				separator : " - "
			}
		}, function(start, end, label)
		{
			startDate = start;
			endDate = end;
			redrawTable(true);
		});

		$input.on('change', function()
		{
			var inputWidth = $(this).textWidth();
			$(this).css({
				width : (inputWidth + 30)
			})
		}).trigger('change');


		// visible columns
		setupVisibility();

		// Add the download data button
		$('#matchedProposals_wrapper #actionsHolder').attr("class", "form-group").append($('<button>').attr("class", "btn btn-default downloadTableData guide-export-button").attr("title", "Download data").append($('<i>').attr("class", "qc qc-download")).append(" Export Proposal Data"));

		$(".dataTables_scrollHead").on("change keyup", "tr.searchRow input, tr.searchRow select", function(e)
		{

			var searchCellIndex = $(this).closest("th").index();
			var headerCell = $('.dataTables_scrollHeadInner > table > thead > tr').find("th")[searchCellIndex];

			proposalTable.column(headerCell).search($(this).val());

			var count = countColumnFilters();
			if (count > 0)
			{
				var title = count + " Filters";
				$("#filterLabel").text(title)
				$(".columnFiltersEnabled").show();
			}
			else
			{
				$("#filterLabel").text("Filter");
				$(".columnFiltersEnabled").hide();
			}
			redrawTable(true);
		});

	};
	return {
		// main function to initiate template pages
		init : function()
		{

			if (trialExpired)
			{
				return;
			}
			runDataTable();
			runSetupPlaceholders();
		}
	};
}();

function TogglePanels()
{
	if ($(".switch_up").is(":visible"))
	{
		$('.switch_up').fadeToggle("slow");
		$('.switch_up2').fadeToggle("slow");
	}
	else
	{
		$('.switch_up').fadeToggle("slow");
		$('.switch_up2').fadeToggle("slow");
	}
}

function setupVisibility()
{
	if ($('.column-visible-button').length > 0)
	{
		$('.column-visible-button').remove();
	}

	// checkRowHeight();

	var columnDiv = $('#columns-list');
	var columnButton = $('<button class="ripple btn btn-default column-visible-button" title="Visible columns">');
	columnButton.append("Visible columns");
	columnDiv.append(columnButton);
	var columnList = $('<ul>');
	for (var i = 1; i < proposalTable.columns()[0].length - 1; i++)
	{
		if (($(proposalTable.column(i).header()).html()).match("undefined"))
		{
			var invalidColumn = proposalTable.column(i);
			invalidColumn.visible(false)
		}
		var columnName = $(proposalTable.column(i).header()).html()
		var tagged = $('<li>').addClass('column-tag');
		var column = $(tagged).attr("data-column", i).html(columnName);
		var key = $('span.custom-field', proposalTable.column(i).header()).data("key");
		//
		// var columnOrder;
		// if (columnName.toLowerCase() === "consultant".toLowerCase() ||
		// columnName.toLowerCase() === "status".toLowerCase()) {
		// columnOrder = {"name": columnName, "ciFilterType": "dropdown"};
		// } else if (columnName.toLowerCase() === "Action".toLowerCase()) {
		// columnOrder = {"name": columnName, "ciFilterType": "dropdown"};
		// } else {
		// columnOrder = {"name": columnName, "ciFilterType": "text"};
		// }
		// tableColumnOrder.push(columnOrder);

		if (key && !customFields[key].on)
		{
			continue;
		}
		if ((columnName.toLowerCase() == "travel reference" || columnName.toLowerCase() == "itinerary destinations" || columnName.toLowerCase() == "travel reason" || columnName.toLowerCase() == "travel start date" || columnName.toLowerCase() == "travel end date") && !itineraryAccess)
		{
			continue;
		}
		if (proposalTable.column(i).visible())
		{
			column.addClass("active");
		}
		columnList.append(column);
		column.on("click", function()
		{
			var thisColumn = proposalTable.column($(this).attr('data-column'));
			thisColumn.visible(!thisColumn.visible());
			$(this).toggleClass("active", thisColumn.visible());

			checkRowHeight();
			rebuildColumnFilters();
		});
	}


	// Keep the daterangepicker class to use the same css
	var columnFloatMenu = $('<div class="daterangepicker dropdown-menu opensright">').append($('<div class="ranges">').append(columnList));
	columnDiv.append(columnFloatMenu);
	columnFloatMenu.css("top", columnButton.outerHeight());
	columnFloatMenu.css("left", 0);

	columnFloatMenu.on('click', function(event)
	{
		event.stopPropagation();
	});

	columnButton.on("click", function()
	{
		columnFloatMenu.toggle();

		setTimeout(function()
		{
			$(window).one('click', function()
			{
				columnFloatMenu.toggle();
			});
		}, 100);
	});
}

function checkRowHeight()
{
	var $search = $('.dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow');

	if (searchHidden == true)
	{
		var rowHeight = $('.dataTables_scrollHeadInner > table > thead > tr:first').height();
		$('.DTFC_RightHeadWrapper').height(rowHeight - 1);
		$('.DTFC_RightHeadWrapper > table > thead > tr:first').height((rowHeight));
		$('.DTFC_LeftHeadWrapper').height(rowHeight);
		$('.DTFC_LeftHeadWrapper > table > thead > tr:first').height((rowHeight));
		$('.DTFC_RightBodyWrapper').attr('style', 'margin-top: 1px !important')
		$('.DTFC_LeftBodyWrapper').attr('style', 'margin-top: 1px !important')
	}
	else
	{
		var rowHeight = $('.dataTables_scrollHeadInner > table > thead > tr:first').height();
		var searchHeight = $('.dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow').height();
		var headerheight = $('.dataTables_scrollHead').height();
		$('.DTFC_RightHeadWrapper').height(rowHeight - 1);
		$('.DTFC_RightHeadWrapper > table > thead > tr:first').height((rowHeight));
		$('.DTFC_LeftHeadWrapper').height(rowHeight);
		$('.DTFC_LeftHeadWrapper > table > thead > tr:first').height((rowHeight));
		$('.DTFC_RightBodyWrapper').attr('style', 'margin-top: ' + (searchHeight + 1) + 'px !important')
		$('.DTFC_LeftBodyWrapper').attr('style', 'margin-top: ' + (searchHeight + 1) + 'px !important')
	}

	var rowWidth = $('.dataTables_scrollHeadInner > table > thead > tr:first > th.first').width();
	$('.DTFC_LeftHeadWrapper').width(rowWidth);
	$('.DTFC_LeftBodyLiner > table').width(rowWidth);


	proposalTable.columns.adjust().draw();

}
// Format all columns to display correctly and remove padding appearing above
// left and right heading
function formatTablecolumns()
{
	checkRowHeight();
}

function formatCustomField(data, type, forSort)
{
	if (data == '')
	{
		return data;
	}
	if (forSort)
		return data;
	if (type == 'date')
		try
		{
			var _d = moment(data);
			if (_d.isValid())
				return _d.format(dateFormatPattern);
			return "";
		}
		catch (e)
		{
			return "";
		}
	if (type == 'currency')
	{
		try
		{
			return '<span class="moneytaryValue"> ' + parseFloat(data).formatMoney() + '</span>';
		}
		catch (e)
		{
			return "";
		}
	}
	return data.htmlEscaped();
}

function rebuildColumnFilters(force)
{
	var $thead = $('.dataTables_scrollHeadInner > table > thead');
	var $search = $('.dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow');

	if ($search.size() > 0)
	{
		$('.DTFC_ScrollWrapper').height("-=47");
		$('.dataTables_scrollHeadInner > table > thead.searchHead > tr.searchRow').remove();
		$('.DTFC_RightHeadWrapper > table > thead.searchHead > tr.searchRow').remove();
		$('.DTFC_LeftHeadWrapper > table > thead.searchHead > tr.searchRow').remove();
		$search.remove();
		searchHidden = true;

		checkRowHeight();
	}

	var $filterCell1 = $('> tr > th:first-child', $thead).html('');
	var $filterCell2 = $('> tr > th:last-child', $thead).html('');
	$filterCell1.append($("<button>").css({
		'border-radius' : '33px'
	}).attr('class', 'btn btn-xs btn-block btn-default scroll-table-left ripple').append($('<i class="fa fa-arrow-left">')));

	$filterCell2.append($('<span>').css({
		'position' : 'absolute',
		'padding-top' : '6px'
	}));

	$filterCell2.append($("<button>").css({
		'border-radius' : '33px',
		'width' : '27px',
		'float' : 'right'
	}).attr('class', 'btn btn-xs btn-block btn-default scroll-table-right ripple').append($('<i class="fa fa-arrow-right">')));

	if (force == true || countColumnFilters() > 0)
	{
		var $searchHead = $('<thead>').attr('class', 'searchHead');
		var $searchRow = $("<tr>").attr('class', 'searchRow');
		proposalTable.columns().every(function(i, tableLoop, columnLoop)
		{
			var dtColumn = this;
			if (dtColumn.visible())
			{
				var $cell = $("<th>");
				if (i > 0)
				{
					var colDef;
					for (var w = 0; w < tableColumnDefs.length; w++)
					{
						if (tableColumnDefs[w].title == $(dtColumn.header()).text())
						{
							colDef = tableColumnDefs[w];
						}
						else if (tableColumnDefs[w].title.indexOf($(dtColumn.header()).text().htmlEscaped()) !== -1)
						{
							colDef = tableColumnDefs[w];
						}
					}
					var $search = undefined;

					// $(dtColumn.header()).text()

					if (colDef.ciFilterType == 'text')
					{
						$search = $('<input type="text" class="form-control input-sm"/>');
						$search.val(dtColumn.search());
					}
					else if (colDef.ciFilterType == 'dropdown')
					{
						$search = $('<select class="form-control input-sm"/>');
						$search.append($("<option>").attr('value', '').attr('selected', 'selected').text('Filter...'));
						$(colDef.ciDataValues).each(function(i, e)
						{
							var $opt = $("<option>");
							$opt.text(e.label);
							$opt.attr('value', e.value);
							$search.append($opt);
						});

						$search.val(dtColumn.search());
					}
					else if (colDef.ciFilterType == 'clear')
					{
						$search = $("<button>").attr('class', 'btn btn-sm btn-danger clearColumnFilters columnFiltersEnabled').append($('<i class="fa fa-eraser">'), " Clear Filters");

						$search.val(dtColumn.search());
					}

					if ($search != undefined)
					{
						var label = $(dtColumn.header()).text();
						$search.attr('placeholder', 'Filter...');

						$cell.append($search);
					}
				}

				$searchRow.append($cell);
				$searchHead.append($searchRow);
			}
		});


		var $smallSearchHeadRight = $('<thead>').attr('class', 'searchHead');
		var $smallSearchRowRight = $("<tr>").attr('class', 'searchRow');
		var $cellRight = $("<th>").append($("<button>").attr('class', 'matchedProposals_clearColumnFilters btn btn-sm btn-danger clearColumnFilters columnFiltersEnabled').append($('<i class="fa fa-eraser">'), " Clear Filters"));
		$smallSearchRowRight.append($cellRight);
		$smallSearchHeadRight.append($smallSearchRowRight);


		var $smallSearchHead = $('<thead>').attr('class', 'searchHead');
		var $smallSearchRow = $("<tr>").attr('class', 'searchRow');
		var $cell = $("<th>").attr('style', 'height:48px');
		$smallSearchRow.append($cell);
		$smallSearchHead.append($smallSearchRow);

		$('.dataTables_scrollHeadInner > table ').append($searchHead);
		$('.DTFC_RightHeadWrapper > table ').append($smallSearchHeadRight);
		$('.DTFC_LeftHeadWrapper > table ').append($smallSearchHead);

		searchHidden = false;

		$('.DTFC_ScrollWrapper').height("+=47");
		checkRowHeight();


		$(".searchRow").on("click", ".clearColumnFilters", function(e)
		{
			proposalTable.columns().search('');
			rebuildColumnFilters(true);

			redrawTable(true);
		});

		// $(".matchedProposals_clearColumnFilters").on("click", function(e)
		// {
		// proposalTable.columns().search('');
		// rebuildColumnFilters(true);
		// redrawTable();
		// });
	}

	var count = countColumnFilters();
	if (count > 0)
	{
		var title = count + " Filters";
		$("#filterLabel").text(title);
		$(".columnFiltersEnabled").show();
	}
	else
	{
		$("#filterLabel").text("Filter");
		$(".columnFiltersEnabled").hide();
	}
}

function showSearchBar()
{
}

function countColumnFilters()
{
	var count = 0;
	proposalTable.columns().every(function(i, tableLoop, columnLoop)
	{
		var dtColumn = this;
		if (dtColumn.visible() && dtColumn.search() != '')
		{
			++count;
		}
	});
	return count;
}

function escapeHtml(str)
{
	if (str != undefined)
	{
		return str.htmlEscaped();
	}
	else
	{
		"";
	}
}

function createProposalAction(event)
{
	createProposal();
}

function createProposal(copyProposal)
{
	var params = {};
	if (copyProposal != undefined)
	{
		params.copyProposal = copyProposal;
	}
	$('body').modalmanager('loading');

	var $modal = $('#create-proposal-modal');
	$modal.load('/application/dialog/createProposal.jsp', params, function()
	{
		$('.cust-fld-date', $modal).datepicker({
			autoclose : true,
			format : {
				toDisplay : function(date, format, language)
				{
					return moment(date).format(dateFormatPattern);
				},
				toValue : function(date, format, language)
				{
					return moment(date).format(dateFormatPattern);
				}
			}
		});
		$modal.modal({
			width : '660px',
			backdrop : 'static',
			keyboard : false
		});
	});

	$modal.one('show.bs.modal', function()
	{
		// setup ui
		$("#cc-emails", $modal).tagsInput({
			defaultText : 'Add a email',
			width : 'auto',
			minInputWidth : '390px',
			placeholderColor : '#858585'
		});

		$("#colorPicker > label input", $modal).on('change', function(e)
		{
			var selected = $('input[name="spectrumColor"]:checked', $modal).val();
			$modal.attr("spectrum", selected);
		}).change();

		$("#proposalTitle", $modal).on('keyup', function(e)
		{
			$('[tag="companyName"]', $modal).text($(e.currentTarget).val());
		});

		$(".coverPageOption", $modal).click(function(e)
		{
			$(e.currentTarget).siblings().removeClass("active");
			$(e.currentTarget).addClass("active");

			var $scrollTo = $(e.currentTarget);
			var $myContainer = $("#coverPicker");
			var newScroll = $scrollTo.offset().left - $myContainer.offset().left + $myContainer.scrollLeft();
			var adjust = ($scrollTo.width() - $myContainer.width()) / 2
			$myContainer.animate({
				scrollLeft : newScroll + adjust
			}, 500);

			$("#coverPageHiddenField", $modal).val($(e.currentTarget).attr("pageId"));
		});

		$modal.find('input, textarea').placeholder();

		$.validator.addMethod("emailList", function(value, element)
		{
			// Any code that will return TRUE or FALSE
			var valid = true;
			var tagsStr = $(element).val();
			if (tagsStr.length > 0)
			{
				var tags = tagsStr.split(",");
				$.each(tags, function(i, email)
				{
					valid = valid && validateEmail(email);
				});
			}
			return valid;
		}, "Error message");

		$('.templates-list .templates-item', $modal).click(function()
		{
			$(this).addClass('active starred').siblings().removeClass('active starred');
			$("#template-chooser", $modal).val($(this).attr('data-id'));
			$(this).siblings().find('input').removeProp("checked");
			$(this).find('input').prop('checked', true);
			// manually remove error highlighing
			$(this).closest('.form-group').removeClass('has-error');
		});

		// set validation rules - custom fields can be set on/off as required
		var proposalValidationRules = {
			custProposalTitle : {
				minlength : 2,
				maxlength : 100,
				required : true
			},
			custCompanyName : {
				minlength : 2,
				maxlength : 100,
				required : false
			},
			custContactName : {
				minlength : 2,
				maxlength : 100,
				required : true
			},
			custContactLastName : {
				minlength : 2,
				maxlength : 100,
				required : false
			},
			custContactAddress : {
				minlength : 2,
				maxlength : 500,
				required : false
			},
			custContactEmail : {
				minlength : 2,
				maxlength : 100,
				email : true,
				required : true
			},
			custCcEmails : {
				emailList : true
			},
			chosenTemplate : {
				required : true
			}
		};


		$.each(customFields, function(index, field)
		{
			if (typeof (field) == "object" && field.on)
			{
				proposalValidationRules[index] = {
					required : field.required
				}
			}
		});

		// setup validation
		var $validator = $('.form-create-proposal', $modal).validate({
			errorElement : 'span',
			errorClass : 'help-block',
			errorPlacement : function(error, element)
			{

			},
			submitHandler : function(form)
			{
				startLoadingForeground();
				$(form).ajaxSubmit({
					success : function(data)
					{
						$modal.modal('hide');

						$("#openNewProposal").attr("href", data);
						$(document).one('foregroundLoadingComplete', function()
						{
							var d = $("#proposalCreatedModal").modal('show');

							if (data === "#")
							{
								$("#openNewProposal", d).remove();
							}
							$("#openNewProposal, #closeBtn", d).one("click", function()
							{
								d.modal('hide');
								redrawTable();
							});
						});

						stopLoadingForeground();
					}
				});
			},
			rules : proposalValidationRules,
			highlight : function(element)
			{
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			unhighlight : function(element)
			{
				$(element).closest('.form-group').removeClass('has-error');
			},
			success : function(label, element)
			{
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			}
		});

		$('.form-steps', $modal).bootstrapWizard({
			'tabClass' : 'form-wizard',
			'onNext' : function(tab, navigation, index)
			{
				var $valid = $(".form-create-proposal").valid();
				if (!$valid)
				{
					$validator.focusInvalid();
					return false;
				}
				else
				{
					$('.form-steps', $modal).find('.form-wizard').children('li').eq(index - 1).addClass('complete');
					$('.form-steps', $modal).find('.form-wizard').children('li').eq(index - 1).find('.step').html('<i class="fa fa-check"></i>');
				}
			},
			'onTabClick' : function(tab, navigation, index)
			{
				return false;
			},
			'onTabShow' : function(tab, navigation, index)
			{
				var $total = navigation.find('li').length;
				var $current = index + 1;
				if ($current >= $total)
				{
					$('.form-steps', $modal).find('.modal-footer .next').hide();
					$('.form-steps', $modal).find('.modal-footer .finish').show();
					$('.form-steps', $modal).find('.modal-footer .finish').removeClass('disabled');
				}
				else
				{
					$('.form-steps', $modal).find('.modal-footer .next').show();
					$('.form-steps', $modal).find('.modal-footer .finish').hide();
				}
			}
		});

	});
}

function handoverError()
{
	swal({
		title : "Create a new user",
		text : "Oops, there appears to be no other sales consultant. To handover a proposal to another consultant you will need to create more users.",
		type : "info",
		showCancelButton : true,
		confirmButtonText : "Create user",
		closeOnConfirm : false
	}, function()
	{
		if ($('.closeManagementDialog').length > 0)
		{
			$('.closeManagementDialog').click();
		}
		$('.usersManagementAction').click();
		swal("Create your User", "Click the + Add User button a create a new user", "success");
	});
}

function getCustomField(object, key)
{
	var $formGroup = $("<div>").addClass("form-group");
	$formGroup.append($("<label>").addClass("col-sm-3 control-label").attr("for", key).text(object.label + ":"));
	var field = $("<div>").addClass("col-sm-8");
	if (object.type === "text")
	{
		field.append($("<input>").attr("type", "text").attr("id", key).attr("name", key).addClass("form-control"));
	}
	else
	{
		var select = $("<select>").attr("id", key).attr("name", key).addClass("form-control");
		var options = object.values.split("|");
		select.append($("<option>").attr("value", "").text(""));
		$.each(options, function(index, value)
		{
			select.append($("<option>").attr("value", value).text(value.trim()));
		});
		field.append(select);
	}
	$formGroup.append(field);
	return $formGroup;
}

function handoverProposalDashboardAction(e)
{
	var proposalId = $(e.target).closest("tr").attr("data-id");

	displayProposalManagementDialog(proposalId, true);
}

function handoverProposalDashboardActionError(e)
{
	handoverError();
}

function displayProposalManagementAction(e)
{
	var proposalId = $(e.target).closest("tr").attr("data-id");

	displayProposalManagementSideBar(proposalId);
}

function displayProposalVersionAction(e)
{
	var proposalId = $(e.target).closest("tr").attr("data-id");

	displayProposalVersionSideBar(proposalId);
}

function displayProposalManagementSideBar(proposalId, handover)
{
	logger.info("Displaying proposal management popup proposalId=" + proposalId);


	$('.cd-panel').toggleClass('is-visible');
	$('div.sideBar-backDrop').show();

	$('.cd-panel-content').empty()

	var $modal = $('.cd-panel-content');


	$modal.load('/application/dialog/proposalManagement.jsp', {
		proposalId : proposalId
	}, function()
	{

		if (handover)
		{
			var action = $modal.find('.handoverProposalAction');
			if (action.length > 0)
			{
				$modal.find('.handoverProposalAction').click();
			}
			else
			{
				$modal.find('.handoverProposalActionError').click();
			}
		}

		$modal.find('.closeManagementDialog').click(function()
		{
			$('.cd-panel').toggleClass('is-visible');
			$('div.sideBar-backDrop').hide();
			redrawTable();
			updateCommentNotificationCounter();
		});

		$modal.find('#submitButton').click(function()
		{
			// $('.cd-panel').toggleClass('is-visible');
			$('div.sideBar-backDrop').hide();
			redrawTable();
			updateCommentNotificationCounter();
		});
	});

	return $modal;
}


function displayProposalVersionSideBar(proposalId, handover)
{
	logger.info("Displaying proposal management popup proposalId=" + proposalId);


	$('.cd-panel').toggleClass('is-visible');
	$('div.sideBar-backDrop').show();
	var width = $('.cd-panel-container').width();
	$('.cd-panel-container').width(width + 60);

	$('.cd-panel-content').empty()

	var $modal = $('.cd-panel-content');


	$modal.load('/application/dialog/proposalVersions.jsp', {
		proposalId : proposalId
	}, function()
	{

		if (handover)
		{
			var action = $modal.find('.handoverProposalAction');
			if (action.length > 0)
			{
				$modal.find('.handoverProposalAction').click();
			}
			else
			{
				$modal.find('.handoverProposalActionError').click();
			}
		}

		$modal.find('.closeManagementDialog').click(function()
		{
			$('.cd-panel').toggleClass('is-visible');
			$('.cd-panel-container').removeAttr('style');
			$('div.sideBar-backDrop').hide();
			redrawTable();
			updateCommentNotificationCounter();
		});
	});

	return $modal;
}

function displayProposalManagementDialog(proposalId, handover)
{
	logger.info("Displaying proposal management popup proposalId=" + proposalId);

	var $modal = $('#proposal-management-modal');
	$('body').modalmanager('loading');

	$modal.one('show.bs.modal', function()
	{
		$modal.find('input, textarea').placeholder();
	});

	$modal.load('/application/dialog/proposalManagement.jsp', {
		proposalId : proposalId
	}, function()
	{
		$modal.modal({
			width : '660px',
			backdrop : 'static',
			keyboard : false
		});

		if (handover)
		{
			var action = $modal.find('.handoverProposalAction');
			if (action.length > 0)
			{
				$modal.find('.handoverProposalAction').click();
			}
			else
			{
				$modal.find('.handoverProposalActionError').click();
			}
		}

		$modal.find('.closeManagementDialog').click(function()
		{
			redrawTable();
			updateCommentNotificationCounter();
		});
	});

	return $modal;
}

function displayProposalNotesAction(e)
{
	var proposalId = $(e.target).closest("tr").attr("data-id");

	displayProposalNotesSideBar(proposalId);
}

function displayProposalNotesSideBar(proposalId)
{


	logger.info("Displaying proposal notes popup proposalId=" + proposalId);


	$('.cd-panel').toggleClass('is-visible');
	$('div.sideBar-backDrop').show();

	$('.cd-panel-content').empty()

	var $modal = $('.cd-panel-content');

	$modal.load('/application/dialog/proposalNotes.jsp', {
		proposalId : proposalId
	}, function()
	{
		$modal.find('.closeManagementDialog').click(function()
		{

			$('.cd-panel').toggleClass('is-visible');
			$('div.sideBar-backDrop').hide();
			redrawTable();
			updateCommentNotificationCounter();
		});
	});

	return $modal;
}

function displayProposalNotesDialog(proposalId)
{
	logger.info("Displaying proposal notes popup proposalId=" + proposalId);

	var $modal = $('#proposal-notes-modal');
	$('body').modalmanager('loading');

	$modal.one('show.bs.modal', function()
	{
		$modal.find('input, textarea').placeholder();
	});

	$modal.load('/application/dialog/proposalNotes.jsp', {
		proposalId : proposalId
	}, function()
	{
		$modal.modal({
			width : '550px',
			backdrop : 'static',
			keyboard : false
		});

		$modal.find('.closeManagementDialog').click(function()
		{
			redrawTable();
			updateCommentNotificationCounter();
		});
	});

	return $modal;
}

function displayProfileManagementAction(e)
{
	displayProfileManagementDialog();
	return false;// remove hash
}

function displayProfileManagementDialog()
{
	logger.info("Displaying profile management popup");

	var $modal = $('#profile-management-modal');
	$('body').modalmanager('loading');

	$modal.one('show.bs.modal', function()
	{
		$modal.find('input, textarea').placeholder();
	});

	$modal.load('/application/dialog/profileManagement.jsp', {}, function()
	{
		$modal.modal({
			width : '700px',
			backdrop : 'static',
			keyboard : false
		});

		$modal.find('.closeManagementDialog').click(function()
		{
			redrawTable();
		});
	});

	return $modal;
}

function displayNotificationManagementAction(e)
{
	displayNotificationManagementDialog();
	return false;// remove hash
}

function displayNotificationManagementDialog()
{
	logger.info("Displaying profile management popup");

	var $modal = $('#notification-management-modal');
	$('body').modalmanager('loading');

	$modal.one('show.bs.modal', function()
	{
		$modal.find('input, textarea').placeholder();
	});

	$modal.load('/application/dialog/notificationManagement.jsp', {}, function()
	{
		$modal.modal({
			backdrop : 'static',
			keyboard : false
		});

		$modal.find('.closeManagementDialog').click(function()
		{
			redrawTable();
		});
	});

	return $modal;
}

function displayCommentsFeedAction(e)
{
	logger.info("Displaying comments feed popup");

	if ($("span#unreadCommentsCounter").text() == 0)
	{
		return;
	}

	var $modal = $('#comment-feed-modal');
	$('body').modalmanager('loading');

	$modal.one('show.bs.modal', function()
	{
		$modal.find('input, textarea').placeholder();
	});

	$modal.load('/application/dialog/commentsFeed.jsp', {}, function()
	{
		$modal.modal({
			width : '700px',
			backdrop : 'static',
			keyboard : false
		});

		$modal.find('.closeManagementDialog').click(function()
		{
			redrawTable();
			updateCommentNotificationCounter();
		});
	});

	return $modal;
}

function showInvoices(e)
{
	var $modal = $('#show-transactions-modal');
	$('body').modalmanager('loading');

	$modal.load('/application/dialog/showInvoices.jsp', {}, function()
	{
		$modal.modal({
			width : '700px',
			backdrop : 'static',
			keyboard : false
		});
	});

	return $modal;
}

function displayAccountSettingsAction()
{
	logger.info("Displaying AccountSettings popup");

	var $modal = $('#account-settings-modal');
	$('body').modalmanager('loading');

	$modal.load('/application/dialog/accountSettings.jsp', {}, function()
	{
		if (accountSuspended)
		{
			$modal.find('#closeForm, .close, .company-details').hide();
		}

		$modal.modal({
			backdrop : 'static',
			keyboard : false
		});
		setupModalsBackgroundColor();
	});

	return $modal;
}

function displayDiskUsageAction()
{
	logger.info("Displaying disk usage popup");

	var $modal = $('#disk-usage-modal');
	$('body').modalmanager('loading');

	$modal.load('/application/dialog/diskUsage.jsp', {}, function()
	{
		$modal.modal({
			backdrop : 'static',
			keyboard : false
		});
		setupModalsBackgroundColor();
	});

	return $modal;
}

function displayLearningCenterAction()
{
	logger.info("Displaying Learning Center Modal");

	var $modal = $('#learning-center-modal');
	$('body').modalmanager('loading');

	$modal.load('/application/dialog/learningCenter.jsp', {}, function()
	{
		$modal.modal({
			backdrop : 'static',
			keyboard : false
		});
		setupModalsBackgroundColor();
	});

	return $modal;
}

function showMarket()
{
	var $modal = $('#add-ons-modal');
	$('body').modalmanager('loading');

	$modal.load('/application/dialog/plugins.jsp', {}, function()
	{
		$modal.modal({
			width : '700px',
			backdrop : 'static',
			keyboard : false
		});
		setupModalsBackgroundColor();
	});

	return $modal;
}

function displayChangeSubscriptionAlertAction()
{
	if ($.cookie('donotshowexpirealert') == 'true')
	{
		return;
	}
	var swalFunction = function()
	{
		swal({
			title : "Your trial account will end in " + expireInDaysCounter + " days",
			text : 'Hi, your free trial will end soon, we hope you are loving QuoteCloud as much as we do. <br/> To avoid any downtime, would you like to subscribe now?',
			type : "warning",
			html : true,
			showCancelButton : true,
			cancelButtonText : "Later",
			confirmButtonText : "Yes",
			closeOnConfirm : true
		}, function(yes)
		{
			if (yes)
			{
				displayChangeSubscriptionAction();
			}
		});
	}
	swalExtend({
		swalFunction : swalFunction,
		hasCancelButton : true,
		buttonNum : 1,
		buttonColor : [ "#DD6B55" ],
		buttonNames : [ 'Not now' ],
		clickFunctionList : [ function()
		{
			$.cookie('donotshowexpirealert', 'true', {
				expires : 365
			});
		} ]
	});
}

function displayAccountSuspendedAction()
{
	var text = '<p>Unfortunately, we were unable to process your subscription fee payment after trying for more than 14 days. We are very sorry, but QuoteCloud is now in a suspended state.  </p>';
	text += '<p>However, all is not lost.... simply contact the person in your company responsible for the subscription payments to review the subscription fee payment details. Once we can process the subscription fee, access will automatically switch on again!</p>';

	if (hasManageSubscriptionsPermission)
	{
		text = '<p>Unfortunately, we were unable to process your payment in the last 14 days. Please click OK button to review and setup your payment details </p>'
	}

	swal({
		title : "Oops!",
		text : text,
		type : "warning",
		html : true,
		closeOnConfirm : true,
	}, function()
	{
		if (hasManageSubscriptionsPermission)
		{
			displayAccountSettingsAction(true);
		}
		else
		{
			window.location.href = '/api/free/logout';
		}
	});
}
function displayChangeSubscriptionAction()
{
	logger.info("Displaying change subscription popup");

	var $modal = $('#change-subscription-modal');
	$('body').modalmanager('loading');

	$modal.one('show.bs.modal', function()
	{
		$modal.find('input, textarea').placeholder();
	});

	$modal.load('/application/dialog/changeSubscription.jsp', {}, function()
	{
		$modal.modal({
			backdrop : 'static',
			keyboard : false
		});
	});

	return $modal;
}

function displayFirstLoginAction()
{
	logger.info("Displaying first login popup");

	var $modal = $('#first-login-modal');
	$('body').modalmanager('loading');

	$modal.one('show.bs.modal', function()
	{
		$modal.find('input, textarea').placeholder();
		$('.form-steps', $modal).bootstrapWizard({
			'tabClass' : 'form-wizard',
			'onNext' : function(tab, navigation, index)
			{
				var method = eval('submitFirstLoginForm' + index);
				if (method)
				{
					method.call();
				}
				return false;
			},
			'onTabClick' : function(tab, navigation, index)
			{
				return true;
			},
			'onTabShow' : function(tab, navigation, index)
			{
				navigation.find('li').removeClass('complete');
				navigation.find('li:lt(' + index + ')').addClass('complete');

				var $total = navigation.find('li').length;
				var $current = index + 1;
				if ($current >= $total)
				{
					$('.form-steps', $modal).find('.modal-footer .next').hide();
					$('.form-steps', $modal).find('.modal-footer .finish').show();
					$('.form-steps', $modal).find('.modal-footer .finish').removeClass('disabled');
				}
				else
				{
					$('.form-steps', $modal).find('.modal-footer .next').show();
					$('.form-steps', $modal).find('.modal-footer .finish').hide();
				}
			}
		});
	});


	$modal.load('/application/dialog/firstLogin.jsp', {}, function()
	{
		$modal.modal({
			width : '760px',
			backdrop : 'static',
			keyboard : false
		});
	});

	return $modal;
}

var redrawTableDelay = undefined;
function redrawTable(resetPageNum)
{

	// Display loading message to user
	// showNotificationMessage(true, "Working...");
	showLoadingCog(true);
	NProgress.start();
	showScrollButtons();

	if (redrawTableDelay != undefined)
	{
		clearTimeout(redrawTableDelay);
	}
	redrawTableDelay = setTimeout(function()
	{
		resetPageNum && proposalTable && proposalTable.page(0);
		proposalTable && proposalTable.draw('page');
	}, 150);

}

function displaySaveTemplateAction(event)
{
	var proposalId = $(event.target).closest("li").attr("data-id");
	$("#saveAsTemplateModal #save-template-proposal-id").val(proposalId);
}

function displayDeleteProposalAction(event)
{
	var proposalId = $(event.target).closest("li").attr("data-id");
	$("#proposalDeleteModal .proposalDeleteConfimed").attr("data-id", proposalId);
}

function copyProposalAction(e)
{
	var proposalId = $(e.target).closest("tr").attr("data-id");
	if ($('#proposal-management-modal').is(':visible'))
	{
		$('#proposal-management-modal').one('hidden.bs.modal', function()
		{
			createProposal(proposalId);
		}).modal('hide');
	}
	else
	{
		createProposal(proposalId);
	}
}

function createProposalRevisionAction(e)
{
	swal({
		title : "Create a new revision?",
		text : "With a new revision, the previous sent proposal wont be accesible anymore.",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		confirmButtonText : "Yes, create it!",
		closeOnConfirm : true
	}, function()
	{

		var proposalId = $(e.target).closest("tr").attr("data-id");
		var vKey = $(e.target).closest("tr").attr("data-vkey");

		logger.info("Creating proposal revision for proposalId=" + proposalId);
		$("#proposalRevisionModal").modal('hide');

		startLoadingForeground();
		$.ajax({
			type : "POST",
			url : '/api/secure/proposal/createRevision',
			data : {
				proposal : proposalId,
				'v-key' : vKey
			}
		}).done(function(data)
		{

			$("#openNewRevision").attr("href", data);
			if ($('#proposal-management-modal').is(':visible'))
			{
				closeProposalManagementDialog(function()
				{

					openRevisionModal();

				});
			}
			else
			{
				openRevisionModal();
			}
			stopLoadingForeground();
		});
	});
}

function openRevisionModal()
{
	var $revisionCreatedDialog = $("#revisionCreatedModal").modal('show');
	$("#openNewRevision, #closeBtn", $revisionCreatedDialog).one("click", function()
	{
		$revisionCreatedDialog.modal('hide');
	});
}
function reloadProposalManagementDialog(callback)
{

	var proposalId = $("#proposal-management").attr("data-originalId");
	$('#proposal-management-modal').one('hidden.bs.modal', function(e)
	{
		setTimeout(function()
		{ // this setTimeout works around Chrome cannot reload the management
			// modal window because the hide event is not complete in another
			// 'thread'
			displayProposalManagementDialog(proposalId);
			redrawTable();

			$(document).one('foregroundLoadingComplete', function()
			{
				if (callback != undefined)
				{
					callback();
				}
			});
		}, 500);


	}).modal('hide');
}

function closeProposalManagementDialog(callback)
{
	$('#proposal-management-modal').one('hidden.bs.modal', function(e)
	{
		redrawTable();
		if (callback != undefined)
		{
			callback();
		}
	}).modal("hide");
}

function proposalDeleteConfimed(event)
{
	$("#proposalDeleteModal").modal("hide");
	var proposalId = $("#proposalDeleteModal .proposalDeleteConfimed").attr("data-id");
	var row = $('tr[data-id="' + proposalId + '"]');

	startLoadingForeground();
	$.ajax({
		url : '/api/proposal/delete',
		data : {
			proposalId : proposalId
		}
	}).done(function(data, textStatus, jqXHR)
	{
		redrawTable()
		$('#proposal-management-modal').modal("hide");
		stopLoadingForeground();
	});
}

function getActiveTableRows()
{
	return proposalTable.$('tr', {
		"filter" : "applied"
	});
}

function downloadTableData()
{
	startLoadingForeground();
	$.fileDownload('/api/dashboard/download', {
		data : proposalTable.ajax.params(),
		successCallback : function(url)
		{
			stopLoadingForeground();
		},
		failCallback : function(html, url)
		{

			stopLoadingForeground();
		}
	});
}

var commentLoop;

function updateCommentNotificationCounter()
{
	$.ajax({
		url : '/api/dashboard/unreadComments'
	}).done(function(data, textStatus, jqXHR)
	{
		var $iconObject = $(".displayCommentsFeedAction");
		var total = data;

		if (isNaN(parseInt(data)))
			total = 0;

		if (total == 0)
		{
			$("span#unreadCommentsCounter", $iconObject).text("0");
		}
		else
		{
			var count = $("span#unreadCommentsCounter", $iconObject).text();
			if (commentLoop != null)
			{
				clearInterval(commentLoop);
			}
			commentLoop = setInterval(function()
			{
				$("span#unreadCommentsCounter", $iconObject).text(count);

				if (count == total)
				{
					clearInterval(commentLoop);
					$iconObject.animateCss("rubberBand");
				}
				else if (count > total)
				{
					count = 0;
				}

				count++;

			}, 150);

		}

	});
}

function editStylesAction(e)
{
	var $modal = $('#edit-styles-modal');
	$('body').modalmanager('loading');

	$modal.one('show.bs.modal', function()
	{
		$modal.find('input, textarea').placeholder();
	});

	$modal.load('/application/dialog/editStyles.jsp', {}, function()
	{
		$modal.modal({
			width : '800px',
			backdrop : 'static',
			keyboard : false
		});
	});

	return $modal;
}

function coverPageManagementAction(e)
{
	var $modal = $('#cover-page-management-modal');
	$('body').modalmanager('loading');

	$modal.one('show.bs.modal', function()
	{
		$modal.find('input, textarea').placeholder();
	});

	$modal.load('/application/dialog/coverpageManagement.jsp', {}, function()
	{
		$modal.modal({
			width : '675px',
			backdrop : 'static',
			keyboard : false
		});
	});

	return $modal;
}

function templateManagementAction(e)
{
	var $modal = $('#template-management-modal');
	$('body').modalmanager('loading');

	$modal.one('show.bs.modal', function()
	{
		$modal.find('input, textarea').placeholder();
	});

	$modal.load('/application/dialog/templateManagement.jsp', {}, function()
	{
		$modal.modal({
			width : '660px',
			backdrop : 'static',
			keyboard : false
		});
	});

	return $modal;
}

function usersManagementAction(e)
{
	var $modal = $('#users-management-modal');
	$('body').modalmanager('loading');

	$modal.load('/application/dialog/managementUsers.jsp', {}, function()
	{
		$modal.modal({
			width : '1024px',
			backdrop : 'static',
			keyboard : false
		});
	});

	return $modal;
}

function rolesManagementAction(e)
{
	var $modal = $('#roles-management-modal');
	$('body').modalmanager('loading');

	$modal.load('/application/dialog/managementRoles.jsp', {}, function()
	{
		$modal.modal({
			width : '660px',
			backdrop : 'static',
			keyboard : false
		});
	});

	return $modal;
}

function teamsManagementAction(e)
{
	var $modal = $('#teams-management-modal');
	$('body').modalmanager('loading');

	$modal.load('/application/dialog/managementTeams.jsp', {}, function()
	{
		$modal.modal({
			width : '660px',
			backdrop : 'static',
			keyboard : false
		});
	});

	return $modal;
}


function settingsManagementAction(e)
{
	var $modal = $('#settings-management-modal');
	$('body').modalmanager('loading');

	$modal.one('show.bs.modal', function()
	{
		$modal.find('input, textarea').placeholder();
	});

	$modal.load('/application/dialog/settingsManagement.jsp', {}, function()
	{
		$modal.modal({
			width : '820px',
			backdrop : 'static',
			keyboard : false
		});
	});

	return $modal;
}

function customFieldsAction(e)
{
	var $modal = $('#custom-fields-modal');
	$('body').modalmanager('loading');

	$modal.one('show.bs.modal', function()
	{
		$modal.find('input, textarea').placeholder();
	});

	$modal.load('/application/dialog/customFields.jsp', {}, function()
	{
		$modal.modal({
			width : '660px',
			backdrop : 'static',
			keyboard : false
		});
	});

	return $modal;
}

function contactManagementAction(e)
{
	var $modal = $('#contacts-management-modal');
	$('body').modalmanager('loading');

	$modal.load('/api/contacts/manage', {}, function()
	{
		$modal.modal({
			width : '90%',
			backdrop : 'static',
			keyboard : false
		});
		// initTable($('#contacts-table', $('#contacts-management-modal')));
	});

	// $modal.one('show.bs.modal', function()
	// {
	// $modal.find('input, textarea').placeholder();
	// initTable($('#content-library-table', $modal));
	//
	// });
	//
	// $modal.one('shown.bs.modal', function()
	// {
	// $('#content-library-table', $modal).DataTable().draw();
	// $(window).resize(function()
	// {
	// $('#content-library-table', $modal).DataTable().draw();
	// });
	// });
	//
	$modal.one('hide.bs.modal', function()
	{
		// $(document).trigger('content-library-changed');
		// TODO: Rewrite code so we don't use modal.empty() as a hack to fix
		// filter button interactions.
		$modal.empty();
	});

	return $modal;
}

function editLabelsAction(e)
{
	var $modal = $('#edit-labels-modal');
	$('body').modalmanager('loading');

	$modal.one('show.bs.modal', function()
	{
		$modal.find('input, textarea').placeholder();
	});

	$modal.load('/application/dialog/editLabels.jsp', {}, function()
	{
		$modal.modal({
			width : '90%',
			backdrop : 'static',
			keyboard : false
		});
	});

	return $modal;
}

function subscriptionCancellationAction(e)
{
	swal({
		title : "Cancel your account?",
		text : "With a cancellation, your account won't be accessable anymore.",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		confirmButtonText : "Yes, cancel it!",
		cancelButtonText : "No",
		closeOnConfirm : true
	}, function()
	{
		setTimeout(function()
		{
			$.ajax({
				url : '/api/cancelSubscription'
			}).done(function(data, textStatus, jqXHR)
			{
				var modalTitle = "Failed";
				var modalMessage = "We couldn't process your cancellation request. Please try again later.";
				var link = "/dashboard";

				if (data == true)
				{
					modalTitle = "Success";
					modalMessage = "Your subscription has been cancelled and you are now logged out.";
					link = "/api/free/logout";
				}

				swal({
					title : modalTitle,
					text : modalMessage,
					type : data == true ? "success" : "warning",
					closeOnConfirm : true
				}, function()
				{
					window.location.href = link;
				});
			});
		}, 500)
	})

}

function addEditContacts(newItem)
{
	var $item = $(this).closest('tr');
	var $modal = $('#contacts-add-edit-modal');
	if (newItem)
	{
		$('.modal-title', $modal).text('Add Contact');
		$('.delete-contact-action-details', $modal).hide();
		$('[name="elementId"]', $modal).val('');
	}
	else
	{
		$('.delete-contact-action-details', $modal).show();
		$('.modal-title', $modal).text('Edit Contact');
	}
	$('form', $modal).attr('action', '/api/contacts/add-edit');
	$('form', $modal).clearForm();


	// var addressBook;
	// addressBook = new google.maps.places.Autocomplete(
	// /** @type {!HTMLInputElement} */(document.getElementById('addressBook')),
	// {types: ['address']}
	// );

	$.get('/api/contacts/add-edit', function(response)
	{

		$('input[type="checkbox"]', $modal).iCheck({
			checkboxClass : 'icheckbox_square-orange'
		});

		if (!newItem)
		{
			$.get('/api/contacts/add-edit/' + $item.data('element-id'), function(response)
			{
				$('[name="elementId"]', $modal).val(response.element_id);
				$('[name="assetName"]', $modal).val(response.attr_headline);
				$('[name="firstName"]', $modal).val(response.attr_firstname);
				$('[name="lastName"]', $modal).val(response.attr_secondname);
				$('[name="companyName"]', $modal).val(response.attr_companyname);
				$('[name="address"]', $modal).val(response.attr_address);
			});
		}

		$modal.modal({
			width : '860px'
		});

		$modal.modal('show');
		setTimeout(function()
		{
			preCheckUpdate($modal);
			$('[data-dismiss="modal"]', $modal).off().click(function(e)
			{
				e.preventDefault();
				e.stopPropagation();
				closeModalAfterCheckUpdate($modal);
			})
		}, 1000);
	});
}

function deleteContactAction(e)
{
	var $item = $(this).closest('tr');

	$.ajax({
		type : 'post',
		url : '/api/contacts/delete',
		data : {
			'elementId' : $item.data('element-id'),
		}
	}).done(function(response)
	{
		if (response.success)
		{
			contactsTable.draw('page');
			swal({
				title : 'Success!',
				type : 'success',
				text : response.message,
				html : true
			});
		}
		else
		{
			swal({
				title : 'Error',
				type : 'error',
				text : response.message,
				html : true
			})
		}
	});
}

function regenerateApiCredentialsAction(e)
{
	$.ajax({
		url : '/api/regenerateApiCredentials'
	}).done(function(data, textStatus, jqXHR)
	{

		var modalTitle = "Failed";
		var modalMessage = "Your API Credentials were not properly regenerated. Try again later.";

		if (data == true)
		{
			modalTitle = "Success";
			modalMessage = "Your API Credentials have been successfully regenerated!";
		}

		$('body').bsAlert({
			title : modalTitle,
			message : modalMessage,
			icon : $("<i>").attr("class", 'fa fa-check'),
			buttons : [ {
				text : "Close",
				icon : $("<i>").attr("class", 'fa fa-times'),
				buttonClass : "btn btn-danger"
			} ]
		});
	});
}

$.fn.textWidth = function(text, font)
{

	if (!$.fn.textWidth.fakeEl)
		$.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);

	$.fn.textWidth.fakeEl.text(text || this.val() || this.text() || this.attr('placeholder')).css('font', font || this.css('font'));

	return $.fn.textWidth.fakeEl.width();
};

function showScrollButtons()
{
	if ($('.dataTables_scrollHead').width() >= $('.table-full-width.dataTable').width())
	{
		$('.scroll-table-left').hide()
		$('.scroll-table-right').hide();
	}
	else
	{
		$('.scroll-table-left').show();
		$('.scroll-table-right').show();
	}
}

function test()
{
	console.log('test');
}


function importContactsAction()
{
	var $modal = $('#contacts-import-modal');
	$('#csvUpload-btn', $modal).off('click').on("click", function()
	{
		$('#csvUpload', $modal).val("");
		$('#csvUpload', $modal).click();
	});
	$('#csvUpload', $modal).off('change').on('change', function()
	{
		var name = $(this).val();
		if (name == '')
		{
			$('#csvUpload-filename').text('');
			return;
		}
		var idx = name.lastIndexOf('\\');
		if (idx == -1)
		{
			idx = name.lastIndexOf('/');
		}
		$('#csvUpload-filename').text(idx == -1 ? '' : name.substring(idx + 1));
	})
	$('form', $modal).clearForm().find('input').change();
	// $('[name="categoryId"]', $modal).val(folder ?
	// $(folder).closest('tr').attr('data-element-id') : '');
	$('[name=deleteAll]', $modal).iCheck({
		checkboxClass : 'icheckbox_square-orange',
		radioClass : 'iradio_square-orange'
	});
	$('[name="overWrite"]', $modal).iCheck({
		checkboxClass : 'icheckbox_square-orange',
		radioClass : 'iradio_square-orange'
	}).iCheck('check');
	$.ajax({
		url : '/api/process/status/get',
		type : 'post',
		data : {
			processor : "au.corporateinteractive.qcloud.proposalbuilder.service.MasterContactsImporter"
		}
	}).done(function(data)
	{
		if (data == '')
		{
			$modal.removeClass("importing")
		}
		else
		{
			$modal.addClass("importing");
			startContactImportStatusCheck();
		}
		$modal.modal({
			width : '660px'
		});
	})
}

function exportContactsAction(isSample)
{
	var endpoint = '/api/contacts/export';
	startLoadingForeground();
	$.fileDownload(endpoint, {
		data : {
			row : isSample ? "1" : ""
		},
		successCallback : function(url)
		{
			stopLoadingForeground();
		},
		failCallback : function(html, url)
		{
			stopLoadingForeground();
			alert("Oops! Something wrong in exporting");
		}
	});
}

function startContactImportStatusCheck()
{
	var $modal = $('#contacts-import-modal');
	var check = function()
	{
		if (!$modal.is(':visible'))
		{
			return;
		}
		$.ajax({
			url : '/api/process/status/get',
			type : 'post',
			data : {
				processor : "au.corporateinteractive.qcloud.proposalbuilder.service.MasterContactsImporter"
			}
		}).done(function(data)
		{
			if (data == '')
			{
				$('#contacts-import-modal').modal('hide');
				// $('#contacts-management-modal').load('/api/contacts/manage',
				// {}, function()
				// {
				// initTable($('#contacts-table',
				// $('#contacts-management-modal')));
				// });
				contactsTable.draw('page');
			}
			else if (!data.ended)
			{
				$('.progress-bar', $modal).css('width', (100.0 * data.processed / data.total) + '%');
				$('.progress-bar-text', $modal).text(data.processed + " / " + data.total);
				$('.stage-text', $modal).text(data.stage);

				$modal.addClass("importing");
				setTimeout(check, 1000);
			}
			else
			{
				$('.progress-bar', $modal).css('width', (100.0 * data.processed / data.total) + '%');
				$('.progress-bar-text', $modal).text(data.processed + " / " + data.total);
				swal({
					title : data.success ? "Success" : "Error",
					type : data.success ? "success" : "warning",
					text : data.message,
					closeOnConfirm : true,
					html : true,
				}, function()
				{
					$.ajax({
						url : '/api/process/status/remove',
						type : 'post',
						data : {
							processor : "au.corporateinteractive.qcloud.proposalbuilder.service.MasterContactsImporter"
						}
					});
					if (data.success)
					{
						$modal.modal('hide');
						// $('#contacts-management-modal').load('/api/contacts/manage',
						// {}, function()
						// {
						// initTable($('#contacts-table',
						// $('#contacts-management-modal')));
						// });
						contactsTable.draw('page');
					}
					else
					{
						$modal.removeClass("importing");
					}
				});
			}
		})
	};
	setTimeout(check, 1000);
}
