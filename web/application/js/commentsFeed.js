$(document).ready(function(){
  $(document).on("click", ".markAsReadAction", markAsReadAction);
});

function markAsReadAction(event)
{
	$a = $(this);
	var $block = $(this).closest('.timeline-block');
	
	var proposalId = $a.attr("data-id");
	$.ajax({
		url:'/api/discussion/resetNotifications',
		method:'post',
		data:{
			proposalId: proposalId
		}
	}).done(function(data, textStatus, jqXHR){
		if (data > 0) {
			$block.slideUp();
			
			setTimeout(function() {
				if ($("section#timeline > div.timeline-block:visible").length == 0) {
					$('#comment-feed-modal').modal('hide');
				}
			}, 1000);
		}
		else
			alert("Something bad happen");
	});
	
}

var controller = new ScrollMagic.Controller({ container: "#comments-feed-scroll"});
var items = document.getElementsByClassName("timeline-block").length;
for (x = 0; x < items; x++) {
	var elementHeight = document.getElementsByClassName("timeline-block")[x].offsetHeight;
	var triggerElement = "#fadein-trigger" + x;
	var animateElementLeft = ".timeline-content-left" + x;
	var animateElementRight = ".timeline-content-right" + x;
	var animateElementFadein = ".timeline-icon" + x;
	new ScrollMagic.Scene({
	  triggerElement: triggerElement,
	  reverse: true,
	  duration: elementHeight,
	  triggerHook: 0.8,
	})
	.setTween(animateElementLeft, 1, { left: 0, opacity: 1 })
	.addTo(controller);
	new ScrollMagic.Scene({
	  triggerElement: triggerElement,
	  reverse: true,
	  duration: elementHeight,
	  triggerHook: 0.8,
	})
	.setTween(animateElementRight, 1, { left: 0, opacity: 1})
	.addTo(controller);
	new ScrollMagic.Scene({
		  triggerElement: triggerElement,
		  reverse: true,
		  duration: elementHeight,
		  triggerHook: 0.8,
	})
	.setTween(animateElementFadein, 1, {opacity: 1})
	.addTo(controller);
}