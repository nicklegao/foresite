var currentHistoryTableDataRequest = undefined;

$(document).ready(function()
{

	// $('#proposal-notes-modal').one('shown.bs.modal', managementModalShown);


	// $(".historyFilters").on("click", "span", function()
	// {
	// var $span = $(this);
	// $span.toggleClass("uncheck");
	// $("section#cd-timeline div.cd-timeline-block[data-type='" +
	// $span.data('filter') + "']").toggle(!$span.hasClass("uncheck"));
	// });

	$('.historyFilters .filter-switch').each(function()
	{
		var switchery = new Switchery(this, {
			size : 'small'
		});

		$(this).change(function(e)
		{
			var $span = $(this).parent();
			$("section#cd-timeline div.cd-timeline-block[data-type='" + $span.data('filter') + "']").toggle($(this).is(':checked'));
		});
	});

	$("#proposal-notes").on("click", ".addNoteAction", addNoteAction).on("click", ".smart-timeline-list li", viewLogAction);

	var timelineBlocks = $('.cd-timeline-block'), offset = 0.8;

	// hide timeline blocks which are outside the viewport
	hideBlocks(timelineBlocks, offset);

	// on scolling, show/animate timeline blocks when enter the viewport
	$(window).on('scroll', function()
	{
		(!window.requestAnimationFrame) ? setTimeout(function()
		{
			showBlocks(timelineBlocks, offset);
		}, 100) : window.requestAnimationFrame(function()
		{
			showBlocks(timelineBlocks, offset);
		});
	});

	function hideBlocks(blocks, offset)
	{
		blocks.each(function()
		{
			($(this).offset().top > $(window).scrollTop() + $(window).height() * offset) && $(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
		});
	}

	function showBlocks(blocks, offset)
	{
		blocks.each(function()
		{
			($(this).offset().top <= $(window).scrollTop() + $(window).height() * offset && $(this).find('.cd-timeline-img').hasClass('is-hidden')) && $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
		});
	}

});

function addNoteAction(event)
{
	var vKey = $(event.target).closest("#proposal-notes").attr("data-vkey");
	var travelDocId = $(event.target).closest("#proposal-notes").attr("data-id");
	var isOwner = $(event.target).closest("#proposal-notes").attr("data-isOwner") === "false" ? false : true;
	var owner = $(event.target).closest("#proposal-notes").attr("data-owner");

	$('input[name="traveldoc"]').val(travelDocId);
	$('input[name="v-key"]').val(vKey);
	$('select#noteEmails').empty();

	$.ajax({
		type: 'POST',
		url: '/api/secure/discussion/noteOwners',
		data: {
			"v-key": vKey,
			traveldoc:travelDocId

		},
		success: function (data) {
			if (data.length > 0) {
				$('#ownerEmailChooser').show();
				for (var i = 0; i < data.length; i++) {
					var emailAddress = data[i];
					var option = "";
					if (emailAddress === owner) {
						option += "<option selected value=\""+ emailAddress +"\">" + emailAddress + "</option>";
					} else {
						option += "<option value=\""+ emailAddress +"\">" + emailAddress + "</option>";
					}
					$('select#noteEmails').append(option);
				}
				if(jQuery.inArray(userEmail, data) === -1) {
					var optionEmail = "<option value=\""+ userEmail +"\">" + userEmail + "</option>";
					$('select#noteEmails').append(optionEmail);
				}
				$('select#noteEmails').select2();
			} else {
				$('#ownerEmailChooser').hide();
			}
		},
		error: function(error) {
			console.log(error);
			$('#ownerEmailChooser').hide();
		}
	});

	$('.form-create-note').validate({
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error, element)
		{

		},
		submitHandler : function(form)
		{
			startLoadingForeground();

			$(form).ajaxSubmit({
				success : function(data)
				{

					stopLoadingForeground();
					if (data.success)
					{
						var $li = $("<div class='cd-timeline-block'>").attr("title", "Consultant").attr("data-id", data.dataId).append($("<div>").addClass("cd-timeline-img cd-picture consultant").html($('<i>').attr('class', 'fa fa-user'))).append($("<div>").addClass("cd-timeline-content").attr("data-type", "consultant").append($('<h2>').text('Consultant Note')).append($('<p>').text($('#noteText').val())).append($("<span>").addClass("cd-date").text(formatDateForDisplay(Date.now(), 'dd/mm/yyyy hh:mm A'))))
						$("section.cd-container").prepend($li);

						$("#createNoteModal").modal("hide");
						$('#noteText').val('');
					}
					else
					{
						alert("Unable to create your note. " + data.message);
					}
				},
				error : function()
				{
					stopLoadingForeground();

					alert("Encountered internal server error registering your note. Please try again later.");
				}
			});
		},
		ignore : '',
		rules : {
			note : {
				required : true,
				minlength : 1,
				maxlength : 10000
			},
		},
		highlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-error');
		},
		success : function(label, element)
		{
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		}
	});
}

function viewLogAction(event)
{
	var $li = $(this);
	var $modal = $("#proposal-notes");
	$li.toggleClass("open");

	if ($(".content", $li).length == 0)
	{
		var vKey = $modal.data("vkey");
		var proposalId = $modal.data("id");
		if (currentHistoryTableDataRequest != undefined)
			currentHistoryTableDataRequest.abort();

		currentHistoryTableDataRequest = $.ajax({
			url : '/api/secure/discussion/getDetailedHistory',
			method : 'post',
			data : {
				proposal : proposalId,
				'v-key' : vKey,
				historyItem : $li.data("id")
			}
		}).done(function(data, textStatus, jqXHR)
		{
			data = data.replace(/\n/g, "<br/>");
			currentHistoryTableDataRequest = undefined;
			var $p = $("<p>").addClass("content");
			$p.html(data);
			$(".smart-timeline-content", $li).append($p);
			markHistoryAsRead($li);
		});
	}
}

function managementModalShown()
{
	var $modal = $('.cd-panel-content');

	$(".historyFilters").on("click", "span", function()
	{
		var $span = $(this);
		$span.toggleClass("uncheck");
		$(".smart-timeline-list li[data-type='" + $span.data('filter') + "']").toggle(!$span.hasClass("uncheck"));
	});

	$modal.on('hidden.bs.modal', function(event)
	{
		$modal.off();
	});

}

function markHistoryAsRead($li)
{
	if (currentHistoryTableDataRequest != undefined)
		currentHistoryTableDataRequest.abort();

	currentHistoryTableDataRequest = $.ajax({
		url : '/api/discussion/markAsRead',
		method : 'post',
		data : {
			historyId : $li.data("id")
		}
	}).done(function(data, textStatus, jqXHR)
	{
		currentHistoryTableDataRequest = undefined;
		if (data)
		{
			$li.removeClass("unread");
		}
	});
}
