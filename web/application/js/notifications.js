var typeMap = new Object();
typeMap["info"] = "<i alt='Information' title='Information' class='fa fa-info-circle light-blue' aria-hidden='true'></i>";
typeMap["alert"] = "<i alt='Alert' title='Alert' class='fa fa-exclamation-triangle red' aria-hidden='true'></i>";
typeMap["help"] = "<i alt='Help' title='Help' class='fa fa-thumbs-o-up green' aria-hidden='true'></i>";

var NotificationsList = {
	initDropdown : function()
	{

		$(document).mouseup(function(e)
		{
			var container = $('#activity');

			if (!container.is(e.target)&& container.has(e.target).length === 0)
			{
				container.next('.ajax-dropdown').hide();
				container.removeClass('active');
			}
		});

		// ACTIVITY
		// ajax drop
		$('#activity').click(function(e)
		{
			var $this = $(this);

			if (!$this.next('.ajax-dropdown').is(':visible'))
			{
				$this.next('.ajax-dropdown').show();
				$this.addClass('active');

				NotificationsList.notificationsViewed();
			}
			else
			{
				$this.next('.ajax-dropdown').hide();
				$this.removeClass('active');
			}

			var theUrlVal = $this.next('.ajax-dropdown').find('.btn-group > .active > input').attr('id');

			// clear memory reference
			$this = null;
			theUrlVal = null;

			e.preventDefault();
		});

		/*
		 * AJAX BUTTON LOADING TEXT
		 * Usage: <button type="button" data-loading-text="Loading..." class="btn btn-xs btn-default ajax-refresh"> .. </button>
		 */
		$('button[data-loading-text]').on('click', function()
		{
			NotificationsList.loadData();
		});

		// NOTIFICATION IS PRESENT
		// Change color of lable once notification button is clicked

		$this = $('#activity > .badge');

		if (parseInt($this.text()) > 0)
		{
			$this.addClass("bg-color-red bounceIn animated");

			// clear memory reference
			$this = null;
		}

		NotificationsList.loadData();
	},

	loadData : function()
	{
		var currentActivityBadge = $("#activity .badge").text();
		currentActivityBadge = isNaN(parseInt(currentActivityBadge)) ? 0 : parseInt(currentActivityBadge);

		// reload button
		var reloadDataBtn = $(".ajax-dropdown button[data-loading-text]");
		reloadDataBtn.button('loading');

		$("#activity .badge").empty().append($("<i>").attr("class", "fa fa-refresh fa-spin"));

		$.ajax({
			url : '/api/dashboard/notifications',
			type : 'GET'
		}).done(function(data, textStatus, jqXHR)
		{
			$(".ajax-dropdown ul#notifications").empty();
			$.each(data, function(i, e)
			{
				NotificationsList.addNotification(e);
			});

			// Update badge
			var notificationsSize = $(".ajax-dropdown ul#notifications").children().length;
			var total = notificationsSize;
			$(".ajax-dropdown .notificationsCount").text(notificationsSize);

			var animateChange = setInterval(function()
			{
				$("#activity .badge").text(currentActivityBadge);

				if (currentActivityBadge > total)
					--currentActivityBadge;
				else if (currentActivityBadge < total)
					++currentActivityBadge;
				else
				{
					clearInterval(animateChange);

					$("#activity .badge").animateCss("rubberBand");
				}
				;
			}, Math.floor(750 / Math.abs(currentActivityBadge - total)));

			if (notificationsSize > 0)
			{
				$("#activity .badge").addClass("bg-color-red");
			}
			else
			{
				$("#activity .badge").removeClass("bg-color-red");
			}

			$('.ajax-dropdown [data-toggle="tooltip"]').tooltip();
			$(".ajax-dropdown #statusText").text(moment().format('LLL'));
			reloadDataBtn.button('reset');
		});
	},

	addNotification : function(notification)
	{
		var $actionButton = $("<button>").attr("type", "button").attr("class", "btn bg-color-lightGrey txt-color-white btn-xs").attr("title", notification.type).attr("data-toggle", "tooltip").attr("data-placement", "right").append(retrieveNotificationType(notification.type));

		var $row = NotificationsList.createNotificationRow(notification);
		$(".handle", $row).append($actionButton);
		$(".ajax-dropdown ul#notifications").append($row);

		$actionButton.click(function()
		{
			NotificationsList.openNotification(notification);
		});

		$row.click(function()
		{
			NotificationsList.openNotification(notification);
		});

	},

	notificationsViewed : function(notification)
	{

		$.ajax({
			url : '/api/dashboard/notifications/viewed',
		}).done(function(data, textStatus, jqXHR)
		{
			$("#activity .badge").html($('<i class="fa fa-check">'));
			$("#activity .badge").animateCss("rubberBand");
		});
	},

	createNotificationRow : function(notification)
	{
		var $handle = $("<span>").attr("class", "handle");
		var $title = $("<p>").append($("<strong>").text(notification.title));
		// .append($("<p>").html((notification.content).length >
		// 100?notification.content.substring(0,100)+"...":notification.content))
		var $time = $("<p>").attr("class", "notification-time").text($.timeago(new Date(notification.liveDate)));
		return $("<li>").append($handle, $title, $time);
	},
	
	openNotification : function(notification)
	{
		logger.info("Displaying platform notification");

		var $modal = $('#platform-notification-modal');
		$('body').modalmanager('loading');

		$modal.load('/application/dialog/notifications.jsp?notificationId='+notification.id, {}, function()
		{
			$modal.modal({
				backdrop : 'static',
				keyboard : false,
				width : '860px'
			});
		});
		
		return $modal;
	}
	
};


function retrieveNotificationType(k)
{
	return typeMap[k];
}


NotificationsList.initDropdown();
