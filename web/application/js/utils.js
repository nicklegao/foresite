Number.prototype.formatMoney = function(decPlaces, thouSeparator, decSeparator)
{

	var n = this, decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces, decSeparator = decSeparator == undefined ? "." : decSeparator, thouSeparator = thouSeparator == undefined ? "," : thouSeparator, sign = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
	return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

$.fn.serializeObject = function()
{
	var json = {};
	var array = this.serializeArray();
	$.each(array, function()
	{
		if (json[this.name] !== undefined)
		{
			if (!json[this.name].push)
			{
				json[this.name] = [ json[this.name] ];
			}
			json[this.name].push(this.value || '');
		}
		else
		{
			json[this.name] = this.value || '';
		}
	});
	return json;
}

if (typeof String.prototype.startsWith != 'function')
{
	// see below for better implementation!
	String.prototype.startsWith = function(str)
	{
		return this.indexOf(str) == 0;
	};
}

if (typeof String.prototype.htmlEscaped != 'function')
{
	// see below for better implementation!
	String.prototype.htmlEscaped = function()
	{
		var div = document.createElement('div');
		div.appendChild(document.createTextNode(this));
		return div.innerHTML;
	};
}

function buildUrl(path, data)
{
	var ret = [];
	for ( var d in data)
		ret.push(encodeURIComponent(d) + "=" + encodeURIComponent(data[d]));
	return path + "?" + ret.join("&");
}

// Toast notifiction at top of screen
function showNotificationMessage(show, text)
{
	if (show)
	{
		if ($('.notificationMessage').children().length == 0)
		{
			$('.notificationMessage').append($('<i>').attr("class", "fa fa-spin fa-cog dashboardLoadingIndicatorNM")).append(($('<span>').attr("style", "padding-left:6px")).append(text)).fadeIn(400);
			$(".dashboardLoadingIndicatorNM").stop().css({
				opacity : 1
			});
		}
		else
		{
			$('.notificationMessage').fadeIn(400);
		}
	}
	else
	{
		$('.notificationMessage').delay(1000).fadeOut(400);
	}
}

function showLoadingCog(show)
{
	if (show)
	{
		$(".dashboardLoadingIndicator").stop().css({
			opacity : 1
		});
	}
	else
	{
		$(".dashboardLoadingIndicator").animate({
			opacity : 0
		}, 300);
	}
}

function getOptionsButton(data)
{
	var dropWrapper = '<div class="btn-group">\n' +
	    '<button type="button" class="btn btn-secondary btn-complete">Complete</button>\n' +
	    '<button type="button" class="btn btn-secondary btn-incomplete">\n' +
	        'Incomplete\n' +
	   '</button>\n' +
	'</div>';
	return dropWrapper;
}

function getSalesforecastButtons(data)
{
	var type = "view";
	var obj = {};
	obj['proposal'] = data.id;
	obj["v-key"] = data.vKey;

	var wrapper = $('<div>');
	var group = $("<div>").addClass('btn-group');

	var action = $("<a>").addClass('btn btn-default tooltips dashboard-action-button').append($('<i>').attr("class", "qc qc-search")).append($('<span style="text-transform: capitalize">').append(" " + type));
	action.attr("href", buildUrl("/" + type, obj));

	var split = $('<button>').addClass('btn btn-default dropdown-toggle').attr('data-toggle', 'dropdown').attr('aria-haspopup', 'true').attr('aria-expanded', 'false');
	split.append($('<span class="caret"></span>')).append($('<span class="sr-only">Toggle Dropdown</span>'));

	var list = $("<ul>").addClass("dropdown-menu");
	list.append($("<li>").append($('<a>').attr("href", buildUrl("/pdf/" + data.id + ".pdf", {
		'proposal' : data.id,
		'v-key' : data.vKey
	})).attr("target", "pdf-" + data.id).append($('<i>').attr("class", "qc qc-document")).append($('<span>').append(" PDF"))));
	list.append($("<li>").append($('<a href="#">').attr("class", "displayProposalNotesAction").append($('<i>').attr("class", "fa fa-sticky-note")).append($('<span>').append(" Notes"))));

	wrapper.append(group);
	group.append(action);
	group.append(split);
	group.append(list);

	return wrapper.html();
}

function getInsertTextAttachmentButton($currentSection)
{
	var drop = $('<div>').attr('class', 'dropdown dropup');
	var button = $('<button>').attr('class', "btn btn-default dropdown-toggle").attr('data-toggle', 'dropdown').attr('aria-expanded', 'false').html("<i class='fa fa-plus'></i> Insert... <span class='caret'></span>");
	drop.append(button);
	var list = $('<ul>').attr('class', 'dropdown-menu');
	if (isPricingSectionActive($currentSection) && multiPricingEnabled)
	{
		list.append($('<li>').attr('class', 'addPricingTableAction').html("<a href='#'><i class='fa fa-table'></i> Add Pricing Table </a>"));
	}
	list.append($('<li>').attr('class', 'addFreeTextLibraryAction').html("<a href='#'><i class='fa fa-keyboard-o'></i> Add Free Text </a>"));
	list.append($('<li>').attr('class', 'attachFileAction').html("<a href='#'><i class='fa fa-paperclip'></i> Attach PDF File </a>"));
	list.append($('<li>').attr('class', 'attachImageAction').html("<a href='#'><i class='fa fa-image'></i> Add Image </a>"));
	list.append($('<li>').attr('class', 'attachSpreadsheetAction').html("<a href='#'><i class='fa fa-table'></i> Add Spreadsheet </a>"));
	if (eSignEnabled)
	{
		list.append($('<li>').attr('class', 'addSignAction').html("<a href='#'><i class='fa fa-pencil-square-o'  aria-hidden='true'></i> Add Signature </a>"));
	}
	drop.append(list);

	return drop;
}

// PLACE the drop method in a location
function generateDropArea($currentSection)
{
	var area = $("<div>").attr('class', 'dropArea pulse-droppable-help');
	return area;
}

// INIT the drop method (modular) and get index
function initDropArea(dropArea)
{

	dropArea.droppable({
		scope : mode.name,
		activeClass : "active",
		tolerance : "touch",
		accept : function(elm)
		{
			return !$(elm).closest('.documentLibraryContent').hasClass('fancyProducts');
		},
		drop : function(event, ui)
		{
			var node = $(ui.helper).data("ftSourceNode");
			if (node.data.type != 'Products')
			{
				var index = getNextIndex($(this));
				var $currentSection = $(".editorContent").find(".tab-pane.active .contentContainer");
				doDrop(node, index, $currentSection);
			}
		}
	});

	// Mouse based hover as the above is not sensitive enough
	dropArea.hover(function()
	{
		$(this).toggleClass("hover");
	});


}

function isIpad()
{
	return navigator.userAgent.match(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i) != null;
}

// Perform drop of content from side panel to selected dropzone
function doDrop(node, index, $currentSection)
{

	var itemType = node.data.type == 'Images' ? 'images-library' : node.data.type;
	var callback = insertContentIntoEditor($currentSection, index, itemType, node.key, false).done(function(data, textStatus, jqXHR)
	{
		initSpecificFroalaEditor($currentSection, index);
	});
	callback.done(function(data, textStatus, jqXHR)
	{
		var $panel = $('.panel[dataOrder="' + index + '"]', $currentSection);
		restorePanelFixedState(false, $panel);
		if (node.data.type == 'Images')
		{
			setupImageUploadInteractions($panel);
			attachImageLayout($panel, true, true, "attachLibraryImageAction");
			$('#image', $panel).show();
			$('.radial .radialButtons', $panel).not('.position').removeClass('active');
			$('.radial .radialButtons[data-alignment=top]', $panel).addClass('active');
			$('.panel-body', $panel).insertAfter($('.freetext-inline-image', $panel))
		}
	});
}

function isPricingSectionActive($currentSection)
{
	return $currentSection ? $currentSection.closest('.tab-pane').attr('id') == 'doc-pricing' : false;
}

function wrapPanel(panel, fixed, $currentSection)
{
	var wrap = $('<div>').attr('class', 'group-panel droppable-append');
	if (typeof fixed == 'boolean')
		fixed && wrap.addClass('fixed');
	else if (panel.hasClass('fixed-panel'))
		wrap.addClass('fixed');
	wrap.append(panel);
	wrap.append(getInsertTextAttachmentButton($currentSection));
	var drop = generateDropArea($currentSection);
	wrap.append(drop);
	// If panel is general ONLY then activate drop area
	// Activate drop area because we use it in pricing as well ONLY after
	// proposal has been loaded. Otherwise everything breaks.
	if (mode.name == 'general' || (mode.name == 'pricing' && lastSave))
	{
		initDropArea(drop);
	}
	return wrap;
}

function updateDataOrderNext(group, plus)
{
	var $currentSection = $(".editorContent").find(".tab-pane.active .contentContainer");
	var nextItems = group.nextAll('.group-panel:not(.fixed)');
	$.each(nextItems, function(i, e)
	{
		var order = parseInt($(e).find('.panel').eq(0).attr('dataOrder'));
		// If true, increase all the dataorder. If false, decreases
		if (plus)
			order++;
		else
			order--;
		$(e).find('.panel').eq(0).attr('dataOrder', order);
	});
}

function getNextIndex(button)
{
	var group = button.closest('.group-panel');
	var index = parseInt(group.find('.panel').eq(0).attr('dataOrder'));
	index = isNaN(index) ? -1 : index;
	updateDataOrderNext(group, true);
	return index + 1;
}

function formatDateForDisplay(data, smartFormat)
{
	smartFormat = smartFormat == undefined ? true : false;

	if (data == 0 || data === "1900-01-01 00:00:00.0")
	{
		return "";
	}
	var date = moment(data);
	var today = moment();
	var deltaMs = today.diff(date, 'seconds');

	if (smartFormat)
	{
		if (deltaMs < -60)
		{
			// future date... format as date.
			return moment(data).format(dateFormatPattern);
		}
		if (deltaMs < 60)
		{
			return "Just now";
		}

		deltaMs = Math.floor(deltaMs / 60);
		if (deltaMs < 60)
		{
			return deltaMs + " minutes ago";
		}

		if (date.date() == today.date() && date.month() == today.month() && date.year() == today.year())
		{
			return moment(data).format('hh:mm A');
		}
	}
	return moment(data).format(dateFormatPattern);
}

function repositionModal()
{
	$(this).css('display', 'block');

	$(this).css("margin-top", 0 - Math.max(0, $(this).height() / 2));
}

function escapeHtml(str)
{
	var div = document.createElement('div');
	div.appendChild(document.createTextNode(str));
	return div.innerHTML;
};

function validateEmail(email)
{
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

function helpAction(content)
{
	var $modal = $('#help-modal');
	$('body').modalmanager('loading');

	$modal.one('show.bs.modal', function()
	{
		$modal.find('input, textarea').placeholder();
	});

	$modal.load('/application/dialog/help/' + content + '.jsp', {}, function()
	{
		$modal.modal({
			width : '660px',
			backdrop : 'static',
			keyboard : false
		});
	});
}

function uuid()
{
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c)
	{
		var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
}
