$(document).ready(function()
{
	$(document).on("click", ".doSendProposalAction", sendProposal);
	$(document).on("click", ".doRequestApprovalAction", requestApproval);
	$(document).on("click", ".doSendProposalConfirmedAction", sendProposalConfirmed);
	$(document).on("click", ".doRequestApprovalConfirmedAction", requestApprovalConfirmed);
});

function sendProposal()
{
	$('body').modalmanager('loading');
	var proposalId = $("#param-proposal-id").val();

	var $modal = $('#send-proposal-modal');
	$modal.load('/application/dialog/sendProposal.jsp?proposal=' + proposalId, '', function()
	{
		$modal.modal({
			width : "650px"
		});
	});
}

function requestApproval()
{
	$('body').modalmanager('loading');
	var proposalId = $("#param-proposal-id").val();

	var $modal = $('#send-proposal-modal');
	$modal.load('/application/dialog/requestApproval.jsp?proposal=' + proposalId, '', function()
	{
		$modal.modal({
			width : "650px"
		});
	});
}

function requestApprovalConfirmed(event)
{
	var proposalId = $(this).attr('proposalId');
	var sendProposalDialog = $("#sendProposalDialog").closest(".modal");
	var emailBody = $('#emailBody').froalaEditor('html.get').replace(/\<br>/g, '<br/>').replace(/\%24/g, '$');
	sendProposalDialog.modal('hide');

	startLoadingForeground();

	setTimeout(function()
	{

		$.ajax({
			type : "POST",
			url : '/api/secure/proposal/requestApproval',
			data : {
				proposal : proposalId,
				emailBody : emailBody,
				'e-key' : $("#user-e-key").val()
			}
		}).done(function(response)
		{
			stopLoadingForeground();

			if (response == "OK")
			{
				$(document).one('foregroundLoadingComplete', function()
				{
					$("#proposalSentModal").modal('show');
				});
			}
			else
			{
				alert("Unable to send proposal for approval. Server message:\n" + response + "\nPlease try again.")
			}

		});
	}, 1000);
}

function sendProposalConfirmed(event)
{
	var proposalId = $(this).attr('proposalId');

	var emailCc = $("#proposalSendEmail #emailCc").val();
	var emailSubject = $("#proposalSendEmail #emailSubject").val();
	var emailBody = $('#emailBody').froalaEditor('html.get').replace(/\<br>/g, '<br/>').replace(/\%24/g, '$');
	var attachPdf = $("#proposalSendEmail #attachPdf").prop("checked");

	var sendProposalDialog = $("#sendProposalDialog").closest(".modal");
	sendProposalDialog.modal('hide');

	startLoadingForeground();

	setTimeout(function()
	{

		$.ajax({
			type : "POST",
			url : '/api/secure/proposal/send',
			data : {
				proposal : proposalId,
				'e-key' : $("#user-e-key").val(),

				emailCc : emailCc,
				emailSubject : emailSubject,
				emailBody : emailBody,
				attachPdf : attachPdf
			}
		}).done(function(response)
		{
			stopLoadingForeground();

			if (response == "OK")
			{
				$(document).one('foregroundLoadingComplete', function()
				{
					$("#proposalSentModal").modal('show');
				});
			}
			else
			{
				alert("Unable to send proposal to the client. Server message:\n" + response + "\nPlease try again.")
			}

		});
	}, 1000);
}
