$(document).ready(function(){
  $(document).ajaxComplete(function(event, xhr, ajaxOptions) {
    
    if (xhr.status > 0 && xhr.status != 200 && xhr.status != 401)
    {
      logger.fatal("HTTP "+xhr.status+" recieved for url..." + ajaxOptions.url + " with data..."+ajaxOptions.data);
      setTimeout(function(){
        emailLogs("HTTP Error on AJAX... Code:"+xhr.status);
      }, 300);
    }
  });
});
