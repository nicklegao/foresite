var restoredProposal = undefined;
var pricingTerm = undefined;

function saveToLocalStorage()
{
	if (proposalChanged())
	{
		var proposalId = $("#param-proposal-id").val();
		var proposal = getCurrentProposal();
		logger.info("Saving proposal " + proposalId + " to localStorage");
		localStorage.setItem('unsavedProposal-' + proposalId, JSON.stringify(proposal));
		lastSave = MD5(JSON.stringify(proposal));
	}
}

function saveProposal(proposal_unused, callback)
{
	startLoadingForeground(saving_and_encrypting);

	var go = function()
	{
		var eKey = $("#param-e-key").val();
		var proposalId = $("#param-proposal-id").val();
		logger.info("start saving");
		var proposal = getCurrentProposal();
		var proposalJson = LZString.compressToBase64(JSON.stringify(proposal));
		// var proposalJson = JSON.stringify(proposal);
		var data = {
			proposalJson : proposalJson,
			'e-key' : eKey,
			proposal : proposalId
		};
		if (addTemplate == true)
		{
			data.templateTitle = $('#template-title').val();
			data.templateDescription = $('#template-description').val();
			data.preview = saveAndPreview;
		}
		$.ajax('/api/secure/proposal/save', {
			type : "POST",
			data : data
		}).done(function(data, textStatus, jqXHR)
		{
			stopLoadingForeground();
			logger.info("Proposal " + proposalId + " saved:" + data.success);
			if (!data.success)
			{
				swal({
					title : "Oops, that's embarrassing",
					text : data.message,
					type : "warning"
				});
				return;
			}

			logger.info("Cleaning the any unsaved proposal in localStorage");
			localStorage.removeItem('unsavedProposal-' + proposalId);
			lastSave = MD5(JSON.stringify(proposal));
			if (data.message)
			{
				swal({
					title : "Oops, that's embarrassing",
					text : data.message,
					type : "warning"
				}, function()
				{
					callback && callback();
				});
			}
			else if (data.data)
			{
				if (data.redirect)
				{
					window.location.replace(data.redirect);
				}
				else
				{
					window.location.replace('');
				}
			}
			else
			{
				callback && callback();
			}
		});
	};

	// Wait for any background tasks to complete.
	var poller = setInterval(function()
	{
		// TODO stop polling.. use events
		if (backgroundProcessing == 0)
		{
			clearInterval(poller);
			go();
		}
	}, 500);

}

function restoreProposal()
{
	var proposalId = $("#param-proposal-id").val();
	var eKey = $("#param-e-key").val();

	if (localStorage['unsavedProposal-' + proposalId])
	{
		swal({
			title : "Unsaved Proposal!",
			text : "Your last proposal wasn't saved properly, would you like to restore it?",
			type : "warning",
			showCancelButton : true,
			cancelButtonText : "No, ignore",
			confirmButtonText : "Yes, restore it!",
			closeOnConfirm : true
		}, function(restore)
		{
			if (restore)
			{
				restoreStorageProposal(proposalId, eKey);
			}
			else
			{
				restoreServerProposal(proposalId, eKey);
			}
		});
	}
	else
	{
		restoreServerProposal(proposalId, eKey);
	}
}

function restoreStorageProposal(proposalId, eKey)
{

	unsavedProposal = JSON.parse(localStorage['unsavedProposal-' + proposalId]);

	logger.info("Loading proposal from local storage.");
	startLoadingForeground(loading_and_decrypting);

	// don't make the request straight away...
	// let the browser finish loading and the loading dialog to display
	setTimeout(function()
	{

		restoredProposal = unsavedProposal;
		logger.info("Raw data recieved");

		restoreSections(unsavedProposal.sections);
		if (unsavedProposal.version > 0)
		{
			// Make ajax requests
			restoreProposalPricings(unsavedProposal.pricings);
		}

		if (unsavedProposal.paymentOption)
		{
			restorePaymentOption(unsavedProposal.paymentOption);
		}

		logger.info("Done firing events for rebuilding document.");

		stopLoadingForeground();

		$(document).one('foregroundLoadingComplete', function()
		{
			lastSave = MD5(JSON.stringify(getCurrentProposal()));

			initFroalaEditor();
		});

	}, 750);
}

function restoreServerProposal(proposalId, eKey)
{
	logger.info("Loading proposal from server.");
	startLoadingForeground(loading_and_decrypting);

	// don't make the request straight away...
	// let the browser finish loading and the loading dialog to display
	setTimeout(function()
	{

		$.ajax('/api/proposal/loadData', {
			type : "POST",
			dataType : 'json',
			data : {
				proposalId : proposalId,
				eKey : eKey
			}
		}).done(function(data, textStatus, jqXHR)
		{
			restoredProposal = data;
			logger.info("Raw data recieved");

			restoreSections(data.sections);

			if (data.version > 0)
			{
				// Make ajax requests
				restoreProposalPricings(data.pricings);
			}

			if (data.paymentOption)
			{
				restorePaymentOption(data.paymentOption);
			}

			logger.info("Done firing events for rebuilding document.");

			stopLoadingForeground();

			$(document).one('foregroundLoadingComplete', function()
			{
				$('.documentTagSrc.cust-fld-date').each(function()
				{
					$(this).datepicker('update', moment($(this).val(), dateFormatPattern).toDate())
				});
				lastSave = MD5(JSON.stringify(getCurrentProposal()));
				initFroalaEditor();
				// Initialise drop area when finished loading proposal
				$('.tab-pane.active .contentContainer .dropArea').each(function(i, obj)
				{
					initDropArea($(this));
				});
			});
		});

	}, 750);
}

function papercollapse()
{
	$.each($('.collapse-card'), function(i, collapsecard)
	{
		var content = $('.droppable-append', collapsecard);

		var title = $('h1', content).text();

		var collapsecard__heading = $('<div>');
		collapsecard__heading.attr('class', 'collapse-card__heading');
		var collapsecard__title = $('<div>');
		collapsecard__title.attr('class', 'collapse-card__title');
		collapsecard__title.append($('<h1>').text(title));
		var collapsecard__body = $('<div>');
		collapsecard__body.attr('class', 'collapse-card__body');

		collapsecard__heading.append(collapsecard__title);
		$(collapsecard).append(collapsecard__heading);


		$(collapsecard).append(collapsecard__body);


		content.appendTo($('.collapse-card__body', collapsecard));


	});
	$('.collapse-card').paperCollapse();
}

var terms = {
	'Months' : 'Monthly',
	'Days' : 'Daily',
	'Weeks' : 'Weekly',
	'Quarters' : 'Quarterly',
	'Years' : 'Yearly'
};

function setUpPricingTermType(pricingTerm, $panel)
{
	if (!$panel)
	{
		return;
	}
	if (typeof pricingTerm == 'undefined' || pricingTerm == '')
	{
		pricingTerm = ($.cookie("pricing-term") == undefined) ? 'Months' : $.cookie("pricing-term");
	}
	var term = $('[id="termDropDown"].dropdown-menu.dropdown-menu-right li', $panel).removeClass('selected').filter(':contains("' + pricingTerm + '")');
	term.addClass("selected");
	$('[id="termType"]', $panel).text(term.text());
	$('[id="termFooter"]', $panel).text(terms[term.text()]);
}

function getCurrentProposal()
{
	return {
		version : 1,
		booking : restoredProposal ? restoredProposal.booking : null,
		pricings : getProposalPricings(),
		sections : getProposalSections(),
		signSettings : getProposalSignSettings(),
		paymentOption : getProposalPaymentOption(),
		spreadsheets : getProposalSpreadsheets(),
	}
}

function restoreProposalPricings(pricings)
{
	logger.info("Start restoring plans");
	startLoadingForeground(loading_and_decrypting);
	for ( var i in pricings)
	{
		restoreProposalPricing(pricings[i], $('.pricing-droppable:eq(' + i + ')'));
	}
	logger.info("Pricing options restored");
	stopLoadingForeground();
}

function restoreProposalPricing(pricing, $panel)
{

	setUpPricingTermType(pricing.pricingTermType, $panel);

	var custom = pricing.custom;
	var rows = pricing.rows;

	// left for old proposals
	var titles = pricing.titles;

	if (typeof pricing.marginDiscount != 'undefined')
	{
		$("#margin-discounts ul.dropdown-menu li", $panel).each(function(i, el)
		{
			if (parseFloat($(el).attr('data-percentage')) === pricing.marginDiscount)
				$(el).click();
		});
	}
	if (pricing.tag)
		$('[name="pricingTag"]', $panel).val(pricing.tag);
	if (pricing.hidePriceColumn)
		!$("#hidePriceColumn", $panel).is(':checked') && $("#hidePriceColumn", $panel).click();
	if ((pricing.hideTerm || pricing.hideTermColumn) && !hideTermsAlways)
	{
		!$("#hideTermColumn", $panel).is(':checked') && $("#hideTermColumn", $panel).click();
	}
	else if (!pricing.hideTerm && !pricing.hideTermColumn && hideTermsAlways)
	{
		$("#hideTermColumn", $panel).click();
	}
	if (pricing.hideQtyColumn)
		!$("#hideQtyColumn", $panel).is(':checked') && $("#hideQtyColumn", $panel).click();
	if (pricing.hideAdditionColumn)
		!$("#hideAdditionColumn", $panel).is(':checked') && $("#hideAdditionColumn", $panel).click();
	if (pricing.hideTotalPriceColumn)
		!$("#hideTotalPriceColumn", $panel).is(':checked') && $("#hideTotalPriceColumn", $panel).click();
	if (pricing.hideTotalCostColumn)
		!$("#hideTotalCostColumn", $panel).is(':checked') && $("#hideTotalCostColumn", $panel).click();
	if (pricing.hideTaxColumn)
		!$("#hideTaxColumn", $panel).is(':checked') && $("#hideTaxColumn", $panel).click();
	if (pricing.hideGrandTotalPriceRow)
		!$("#hideGrandTotalPriceRow", $panel).is(':checked') && $("#hideGrandTotalPriceRow", $panel).click();
	if (pricing.hideOnOffCostRow)
		!$("#hideOnOffCostRow", $panel).is(':checked') && $("#hideOnOffCostRow", $panel).click();
	if (pricing.hideOverallTotalPriceRow)
		!$("#hideOverallTotalPriceRow", $panel).is(':checked') && $("#hideOverallTotalPriceRow", $panel).click();
	if (pricing.hideDiscountFromPriceRow)
		!$("#hideDiscountFromPriceRow", $panel).is(':checked') && $("#hideDiscountFromPriceRow", $panel).click();
	if (pricing.hideMinimumMonthlyCost && !hideTermsAlways && !pricing.hideTerm && !pricing.hideTermColumn)
	{
		!$("#hideMinimumMonthlyCost", $panel).is(':checked') && $("#hideMinimumMonthlyCost", $panel).click();
	}
	else if (!pricing.hideMinimumMonthlyCost && hideTermsAlways)
	{
		$("#hideMinimumMonthlyCost", $panel).click();
	}
	else if (!pricing.hideMinimumMonthlyCost && !hideTermsAlways && (pricing.hideTerm || pricing.hideTermColumn))
	{
		$("#hideMinimumMonthlyCost", $panel).click();
	}
	if (pricing.hideFullTermCost && !hideTermsAlways && !pricing.hideTerm && !pricing.hideTermColumn)
	{
		!$("#hideFullTermCost", $panel).is(':checked') && $("#hideFullTermCost", $panel).click();
	}
	else if (!pricing.hideFullTermCost && hideTermsAlways)
	{
		$("#hideFullTermCost", $panel).click();
	}
	else if (!pricing.hideFullTermCost && !hideTermsAlways && (pricing.hideTerm || pricing.hideTermColumn))
	{
		$("#hideFullTermCost", $panel).click();
	}
	if (pricing.removeTax)
		!$("#removeTax", $panel).is(':checked') && $("#removeTax", $panel).click();
	if (pricing.hideCustom1)
		!$("#hideCustomColumn1", $panel).is(':checked') && $("#hideCustomColumn1", $panel).click();
	if (pricing.hideCustom2)
		!$("#hideCustomColumn2", $panel).is(':checked') && $("#hideCustomColumn2", $panel).click();
	if (pricing.hideCustom3)
		!$("#hideCustomColumn3", $panel).is(':checked') && $("#hideCustomColumn3", $panel).click();
	if (pricing.hideCustom4)
		!$("#hideCustomColumn4", $panel).is(':checked') && $("#hideCustomColumn4", $panel).click();
	if (pricing.hideCustom5)
		!$("#hideCustomColumn5", $panel).is(':checked') && $("#hideCustomColumn5", $panel).click();


	// suport for old proposals
	if (pricing.hidePriceQty)
	{
		!$("#hidePriceColumn", $panel).is(':checked') && $("#hidePriceColumn", $panel).click();
		!$("#hideQtyColumn", $panel).is(':checked') && $("#hideQtyColumn", $panel).click();
	}

	var pricingIds = [];
	addPricingByIds(pricingIds).done(function(data, textStatus, jqXHR)
	{
		var inputs = [];
		logger.info("Latest plans loaded... Restoring selected pricing options");

		try
		{
			// left for old proposals
			$.each(titles, function(i, e)
			{
				addProductSectionToDisplay($('[id="pricingTable"]', $panel), e.label, false);
			});
			$.each(custom, function(i, e)
			{
				inputs.push(addCustomPriceToDisplay($('[id="pricingTable"]', $panel), e.label, e.unitPrice, e.term, e.qty, e.isOneOff, e.title, false));
			});
		}
		catch (error)
		{
			logger.warn(error);
		}

		var numRows = rows.length;
		$.each(rows, function(i, e)
		{
			var itemNum = i + 1;
			if ((i % 10) == 0 || itemNum == numRows)
			{
				logger.info("Restoring row " + itemNum + " of " + numRows + " (" + Math.round(100 * (itemNum / numRows)) + "%)");
			}
			inputs.push(addRowToDisplay($('[id="pricingTable"]', $panel), e, false));
		});

		$('#pricingTable', $panel).find('.pricingSection').each(function()
		{
			var sectionBody = $(this);
			if (sectionBody.find('.summarise').length > 0)
			{
				if (sectionBody.find('.oneOffCheckbox:checked').length > 0)
				{
					sectionBody.find('.summarise').show();
				}
				else
				{
					sectionBody.find('.summarise').hide();
				}
			}
		})
	});
}

function getProposalPricings()
{
	var pricings = [];
	$('div[data-type="block-pricing-table"]').each(function()
	{
		pricings.push($(this));
	});
	return pricings;
}


function getProposalPricing($panel)
{

	var rows = [];
	$.each($("#pricingTable .customPricingRows tr.productRow", $panel), function(i, row)
	{
		var productOption = getSelectedPricingItem(row);

		rows.push(productOption);
	});

	var pricingFooter = $('#pricingFooter', $panel);
	var totalProposalValue = $(".grandTotalRow .cost", $panel).first().text();
	var overallTotalProposalValue = $(".overallTotalRow .cost", $panel).first().text();

	var pricing = {
		'tag' : $('[name="pricingTag"]', $panel).val(),
		'pricingTitle' : $('#pricing-title', $panel).val(),
		'pricingTermType' : $(".selected", $("[id='termDropDown']", $panel)).text(),
		'marginDiscount' : $('#margin-discounts', $panel).attr("data-percentage"),
		'hidePriceColumn' : $('#hidePriceColumn', $panel).is(':checked'),
		'hideTermColumn' : $('#hideTermColumn', $panel).is(':checked'),
		'hideQtyColumn' : $('#hideQtyColumn', $panel).is(':checked'),
		'hideTotalPriceColumn' : $('#hideTotalPriceColumn', $panel).is(':checked'),
		'hideTotalCostColumn' : $('#hideTotalCostColumn', $panel).is(':checked'),
		'hideTaxColumn' : $('#hideTaxColumn', $panel).is(':checked'),
		'hideAdditionColumn' : $('#hideAdditionColumn', $panel).is(':checked'),
		'hideGrandTotalPriceRow' : $('#hideGrandTotalPriceRow', $panel).is(':checked'),
		'hideOnOffCostRow' : $('#hideOnOffCostRow', $panel).is(':checked'),
		'hideOverallTotalPriceRow' : $('#hideOverallTotalPriceRow', $panel).is(':checked'),
		'hideDiscountFromPriceRow' : $('#hideDiscountFromPriceRow', $panel).is(':checked'),
		'hideMinimumMonthlyCost' : $('#hideMinimumMonthlyCost', $panel).is(':checked'),
		'hideFullTermCost' : $('#hideFullTermCost', $panel).is(':checked'),
		'removeTax' : $('#removeTax', $panel).is(':checked'),
		'taxLabel' : $('.grandTotalRow .tax', pricingFooter).attr('data-label'),
		'rows' : rows,
		'totalProposalValue' : totalProposalValue,
		'overallTotalProposalValue' : overallTotalProposalValue,
		'hideCustom1' : $('#hideCustomColumn1', $panel).is(':checked'),
		'hideCustom2' : $('#hideCustomColumn2', $panel).is(':checked'),
		'hideCustom3' : $('#hideCustomColumn3', $panel).is(':checked'),
		'hideCustom4' : $('#hideCustomColumn4', $panel).is(':checked'),
		'hideCustom5' : $('#hideCustomColumn5', $panel).is(':checked')
	};

	var taxPercentage = parseFloat($('.grandTotalRow .tax', pricingFooter).attr('data-percentage'));
	var tax = $('.grandTotalRow .tax', pricingFooter).text();
	if (taxPercentage)
		pricing['taxPercentage'] = taxPercentage;
	if (tax)
		pricing['tax'] = tax;

	return pricing;
}

function getProposalFeaturesWhitelist()
{
	return getCheckboxWhitelist(".groupedFnbHolder .planFeatures");
}

function getProposalBenefitsWhitelist()
{
	return getCheckboxWhitelist(".groupedFnbHolder .benefitsHolder");
}

function getProposalUCWhitelist()
{
	return getCheckboxWhitelist("#ucHolder");
}

function getCheckboxWhitelist(rootSelector)
{
	var selected = {};
	$.each($(rootSelector + " input[type='checkbox']:checked"), function(i, obj)
	{
		selected[$(obj).attr("dataId")] = true;
	});

	return Object.keys(selected);
}

function restoreCheckboxWhitelist(rootSelector, whitelist)
{
	logger.info("Restoring Whitelist for: " + rootSelector);
	var whitelistMap = {};
	$.each(whitelist, function(i, e)
	{
		whitelistMap[e] = true;
	});

	var root = $(rootSelector);
	$.each($('input[type="checkbox"]', root), function(i, e)
	{
		var dataId = $(e).attr("dataid");

		$(e).prop('checked', whitelistMap[dataId] != undefined);
		// $('input[dataid="'+e+'"][type="checkbox"]', root).prop('checked',
		// true);
	});
}

function restorePanelFixedState(content, panel)
{
	var fixed = content.fixed;
	panel.attr('fixed', fixed);

	if (isTemplate)
	{
		// don't do anything wit the close button
		if (fixed)
		{
			panel.find('.panel-heading .lock i').attr('class', 'fa fa-lock');
			panel.find('.panel-heading .lock span').text('Delete Not Allowed');
			panel.find('.panel-froala').css('background', 'rgb(253, 221, 230)')
			panel.find('.panel-tools').width(390);
		}
		else
		{
			panel.find('.panel-heading .lock i').attr('class', 'fa fa-unlock');
			panel.find('.panel-heading .lock span').text('Delete Allowed');
			panel.find('.panel-tools').width(366);
		}
	}
	else
	{
		panel.find('.panel-heading .lock').switchClass('lock', 'disabled').find('> i').attr('class', 'fa');
		if (fixed)
		{
			panel.find('.panel-heading .remove').addClass('disabled');
			panel.find('.panel-heading .clone').find('.fa-clone').removeClass('fa-clone').end().switchClass('clone', 'disabled');
			panel.find('.panel-heading .remove .fa-times').switchClass('fa-times', 'fa-ban');
			panel.find('.panel-froala').removeAttr('contenteditable');
			panel.find('.panel-froala').css('background', 'rgb(253, 221, 230)')
			panel.switchClass('editable', 'viewable');
		}
		else
		{
			// don't do anything with the close button
		}
	}
}

function restorePaymentOption(option)
{
	initPayerList();
	var $options = $('.payment-option-fields');
	$options.find('[name="paymentTitle"]').val(option.title);
	$options.find('[name="paymentDescription"]').val(option.description);
	$options.find('[name="paymentPayee"]').val(option.payee);
	$options.find('[name="paymentPayer"]').val(option.payer);
	$options.find('.payment-value-btn').next().find('a[data-type="' + option.type + '"]').click();
	$options.find('[name="paymentValue"]').val(option.value).blur();
}

function initPayerList()
{
	var $select = $('.payment-option-fields [name="paymentPayer"]');
	var val = $select.val();
	$select.empty().append('<option value="">Any authorised recipient</option>');
	$(getEmailsInProposal()).each(function()
	{
		$select.append('<option value="' + this + '">' + this + '</option>');
	})
	$select.val(val);
}

function restoreSections(sections)
{
	var pricingTableAdded = false;
	var itineraryTableAdded = false;
	$.each(sections, function(i, section)
	{
		var currentSectionId = 0;
		// Create and/or move the section into the correct index.
		if (section.type == 'content')
		{
			logger.info("Restoring dynamic section with title: " + section.title);
			currentSectionId = createSection(section.title, section.breakPageBeforeInPDF, section.titleHidden, false);
		}
		else if (section.type == 'cover')
		{
			var coverHeading = $("#cover-title");
			var tabHeading = $('a[librarytype="cover"] div.tab-label');
			logger.info("Restoring coverpage");
			currentSectionId = "doc-cover";
			if (section.title == null || section.title == '')
			{
				coverHeading.val('About');
				tabHeading.text('Proposal Summary');
			}
			else
			{
				coverHeading.val(section.title);
				tabHeading.text(section.title);
			}
		}
		else if (section.type == 'pricing' || section.type == 'itinerary')
		{
			if (section.type == 'itinerary')
			{
				logger.info("Restoring coverpage");
				currentSectionId = "doc-itinerary";

				var itineraryHeading = $("#itinerary-title");
				itineraryHeading.val(section.title);
			}
			else if (section.type == 'pricing')
			{
				logger.info("Restoring pricing");
				currentSectionId = "doc-pricing";

				var pricingHeading = $("#pricing-title");

				if (section.title == '')
				{
					pricingHeading.val(pricingTitle);
				}
				else
				{
					pricingHeading.val(section.title);
				}
			}

			section.enabled = section.enabled != undefined ? section.enabled : true;

			var breakPageSwitch = $(".editorContent #" + currentSectionId).find(".pageBreakSection").find(".js-switch");
			if (breakPageSwitch.length > 0 && !breakPageSwitch.data('switchery'))
			{
				breakPageSwitch.prop('checked', section.breakPageBeforeInPDF === "on" ? true : false);
				var switchery = new Switchery(breakPageSwitch.get(0), {
					size : 'small'
				});
			}
			else if (section.breakPageBeforeInPDF === "off")
			{
				breakPageSwitch.trigger('click');
			}

			var hideTitleBool = section.titleHidden === "on" ? true : false;

			var hideTitleSwitch = $(".editorContent #" + currentSectionId).find(".noTitleSection").find(".noTitle-switch");

			$(hideTitleSwitch).change(function()
			{
				if (!($(this).is(':checked')) && $('#globalTitleHidden').is(':checked'))
				{
					$('#globalTitleHidden').click();
				}
				if ($(this).is(':checked'))
				{
					$(this).siblings(".switchery").find("span").text('Hide');
					$(this).siblings(".switchery").find("span").attr('title', 'Your title will be hidden inside the PDF');
					$(this).siblings(".switchery").find("span").addClass('hideLabel');
				}
				else
				{
					$(this).siblings(".switchery").find("span").text('Show');
					$(this).siblings(".switchery").find("span").attr('title', 'Your title will be shown within the PDF');
					$(this).siblings(".switchery").find("span").removeClass('hideLabel');
				}
			}).change();

			if (!hideTitleSwitch.data('switcheryHide') && !hideTitleSwitch.hasClass('switchActive'))
			{
				hideTitleSwitch.prop('checked', hideTitleBool);
				var switcheryHide = new Switchery(hideTitleSwitch.get(0), {
					color : '#fc0d00',
					size : 'small'
				});

				if (hideTitleBool)
				{
					$(hideTitleSwitch).siblings(".switchery").css("width", "60px").prepend("<span class='hideLabel' title='Your title will be hidden inside the PDF'>Hide</span>").find("small").css("left", "40px");
				}
				else
				{
					$(hideTitleSwitch).siblings(".switchery").css("width", "60px").prepend("<span title='Your title will be shown within the PDF'>Show</span>").find("small").css("left", "0px");
				}
			}

			$(".editorContent #" + currentSectionId).attr("data-enabled", section.enabled);
			if (!section.enabled)
			{
				var li = $("#documentSectionBrowser a[href='#doc-pricing']").closest('li');
				li.addClass('notenabled');
				// $(".disabledSection", li).css('display', 'inline-block');
				// $(".enabledSection", li).css('display', 'none');
				changeSwitch("#pricing-tab-check", false);
				$('.pricing-droppable:not(#htmlFragments .pricing-droppable)').droppable("disable")
			}
		}
		else
		{
			logger.error("Unknown section type: " + section.type);
			currentSectionId = createSection(section.title || section.type, section.breakPageBeforeInPDF, section.titleHidden, false);
		}
		reorderSectionToIndex(currentSectionId, i);

		var $currentSection = $("#" + currentSectionId + " .contentContainer");

		$currentSection.attr('sectiontype', section.type);

		if (currentSectionId == 'doc-itinerary')
		{
			addItineraryTableAction();
			itineraryTableAdded = true;
		}

		if (section.content.length == 0 && currentSectionId == 'doc-pricing')
		{
			addPricingTableAction(0);
			pricingTableAdded = true;
		}

		// Populate the content
		$.each(section.content, function(j, content)
		{
			restoreContent(j, content, $currentSection);
		});

	});

	if (!itineraryTableAdded)
	{
		addItineraryTableAction();
	}

	if (!pricingTableAdded)
	{
		// addPricingTableAction(0);
	}


}

function restoreContent(j, content, $currentSection)
{

	if (content.type == 'generated')
	{
		if (content.id == 'con-info')
		{
			var $docCover = $("#doc-cover");
			$.each(content.metadata, function(key, value)
			{
				var input = $("#" + key, $docCover);
				if (input.is(':checkbox'))
				{
					input.prop('checked', value == 'on');
				}
				else
				{
					input.val(value);
				}
			});


			var $tableOfContent = $('#tableOfContent').change(function()
			{
				if ($('#tableOfContent').is(':checked'))
				{
					$('#tableOfContentLevelGroup').show();
				}
				else
				{
					$('#tableOfContentLevelGroup').hide();
				}
			}).change();

			var $globalTitleHidden = $('#globalTitleHidden').change(function()
			{
				if ($('#globalTitleHidden').is(':checked'))
				{
					hideHeadings(true);
				}
				else
				{
					var all = true;
					$('.noTitle-switch').each(function()
					{
						if (!($(this).is(':checked')))
						{
							all = false;
						}
					});
					if (all)
					{
						hideHeadings(false);
					}
				}
			}).change();

			var tableOfContentSwitchery = new Switchery($tableOfContent.get(0), {
				size : 'small'
			});

			var landscapeModeSwitchery = new Switchery($('#landscapeMode').get(0), {
				size : 'small'
			});
			var globalTitleHiddenSwitchery = new Switchery($globalTitleHidden.get(0), {
				color : '#fc0d00',
				size : 'small'
			});

			if ($('#showRemarks').length > 0)
			{
				var remarksSwitchery = new Switchery($('#showRemarks').get(0), {
					size : 'small'
				});
			}

			// if (content.metadata['globalTitleHidden'] === "on")
			// {
			// $('#globalTitleHidden').click();
			// }


			$(".documentTagSrc", $docCover).keyup();// special
			if (content.metadata['cc-emails'] != undefined)
			{
				$("#cc-emails", $docCover).importTags(content.metadata['cc-emails']);
			}

			$("#displayOrderButton").prop('checked', content.metadata['displayOrderButton'] === "true");
			$("#showOrderChk").addClass(content.metadata['displayOrderButton'] == "true" ? "checked" : "");
			// Restore coverpage and color if possible...
			if (content.metadata['spectrumColor'] != undefined)
			{
				var spectrumColor = content.metadata['spectrumColor'];
				logger.info("Restoring doc color:" + spectrumColor);
				$('#colorPicker .btn-qcloud-design-' + spectrumColor).click();

				var coverPageImage = content.metadata['coverPageImage'];
				logger.info("Restoring coverPageImage ID:" + coverPageImage);
				$('#coverPicker .coverPageOption[pageId="' + coverPageImage + '"]').click();
			}
			else
			{
				$('#colorPicker .btn').first().click();
			}
		}
		else if (content.id == 'con-custom-fields')
		{
			var $docCover = $("#doc-cover");
			$.each(content.metadata, function(key, value)
			{
				if (customFields[key] && customFields[key].type == 'date')
				{
					var _d = moment(value, 'YYYY-MM-DD');
					if (_d.isValid())
					{
						$("#" + key, $docCover).datepicker('update', _d.toDate());
					}
					else
					{
						$("#" + key, $docCover).datepicker('update', '');
					}
				}
				else
				{
					$("#" + key, $docCover).val(value);
				}
			});
			// $(".documentTagSrc", $docCover).keyup();// special
		}
		else if (content.id.startsWith('con-fnb-'))
		{
			var categoryId = content.id.substring(8);
			var newSectionIndex = $("#doc-solution .contentContainer > .panel").length;
			logger.info('Restoring fNb category at index:' + newSectionIndex + ', with ID:' + categoryId);

			var $fnbPanel = $("#htmlFragments .con-fnb").clone();
			$fnbPanel.attr('dataId', 'con-fnb-' + categoryId);
			$fnbPanel.attr("dataOrder", newSectionIndex);

			$("#doc-solution .contentContainer").append($fnbPanel);
		}
		else if (content.id == 'con-plans')
		{
			addPricingTableAction(j);
			var $panel = $('.panel[dataOrder="' + j + '"]', $currentSection);
			if (content.breakPageBeforeInPDF === "on")
			{
				$panel.find('.panel-heading').find('.js-switch').trigger('click');
			}
			pricingTableAdded = true;
			return;
		}
		else if (content.id == 'con-iti')
		{
			return;
		}
		var $targetContentPane = $('.panel[type="generated"][dataid="' + content.id + '"]');
		$targetContentPane.attr("dataOrder", j);

		var $siblingContent = $targetContentPane.siblings(".panel");
		$.each($siblingContent, function(k, sibling)
		{
			var siblingOrder = parseInt($(sibling).attr('dataOrder'));
			if (siblingOrder < j)
			{
				$targetContentPane.insertAfter($(sibling));
			}
		});
	}
	else
	{
		var dynType = undefined;
		if (content.type == 'image')
		{
			dynType = 'Images';
		}
		else if (content.type == 'video')
		{
			dynType = 'Videos';
		}
		else if (content.type == 'text')
		{
			dynType = 'Text';
		}
		else if (content.type == 'editor')
		{
			if (content.action == 'attachLibraryImageAction')
			{
				dynType = 'images-library';
			}
			else
			{
				dynType = 'freeText';
			}
		}
		else if (content.type == 'destination')
		{
			dynType = 'destination';
		}
		else if (content.type == 'attachment')
		{
			dynType = 'fileHandle';
		}
		else if (content.type == 'pdf')
		{
			dynType = 'pdf';
		}
		else if (content.type == 'imageAttachment')
		{
			dynType = 'imageHandle';
		}
		else if (content.type == 'itinerary')
		{
			dynType = 'itinerary';
		}
		else if (content.type == 'sign')
		{
			dynType = 'signature';
		}
		else if (content.type == 'spreadsheet')
		{
			dynType = 'spreadsheet';
		}
		else
		{
			logger.error("Unknown content type: " + content.type);
		}

		if (dynType != undefined)
		{

			// have determined the type so load it.
			var callback = insertContentIntoEditor($currentSection, j, dynType, content.id, false);

			// do some post-initilisation
			if (content.type == 'editor')
			{
				callback.done(function(data, textStatus, jqXHR)
				{
					var $panel = $('.panel[dataOrder="' + j + '"]', $currentSection);
					$('.panel-body', $panel).html(content.data);

					// froala-editor
					$('.panel-froala', $panel).html(content.data);

					if (content.action == 'attachImageAction' || content.action == 'attachLibraryImageAction')
					{
						attachImageLayout($panel, content.id != undefined, content.data != '<p><br /></p>', content.action);
					}
					if (content.id == undefined) // no image
					// attachment found
					// (hopefully)
					{
					}
					else
					{
						$("#attachmentId", $panel).val(content.id);
						$("#image", $panel).attr("src", content.metadata['src']);
						$("#image", $panel).show();
						$(".choseFile", $panel).hide();
						var alignment = content.metadata['alignment'];
						var size = content.metadata['size'];
						$('.radial .radialButtons', $panel).removeClass('active');
						if (alignment == undefined)
						{
							alignment = "bottomwide"
						}
						if (alignment == 'left' || alignment == 'right' || alignment == 'center')
						{ // for older proposals
							alignment = alignment == 'left' ? 'topleft-intext' : alignment == 'right' ? 'topright-intext' : 'bottomwide';
						}
						var pos = alignment.indexOf('top') >= 0 ? 'top' : 'bottom';
						var align = alignment.indexOf('left') >= 0 ? 'left' : alignment.indexOf('right') >= 0 ? 'right' : alignment.indexOf('center') >= 0 ? 'center' : 'wide';
						if ((content.action == 'attachImageAction' || content.action == 'attachLibraryImageAction') && content.data == '<p><br /></p>')
						{
							alignment = 'top' + align;
							$(".inlineImage-addText", $panel).show();
							$('.radial .radialButtons', $panel).not('.position').removeClass('active');
							$('.radial .radialButtons[data-alignment=top]', $panel).addClass('active');
							$('.radial .radialButtons', $panel).not('.position').hide();
						}
						if (content.action == 'attachLibraryImageAction')
						{
							if ($('#content-removed', $panel).css('display') == 'block')
							{
								$('#radialImageAlignmentButtons', $panel).hide();
								$('#image', $panel).hide();
								$('#image', $panel).attr('src', '');
							}
						}
						if (alignment.indexOf('intext') >= 0)
						{
							// $('.radial .radialButtons[data-alignment=' +
							// alignment + ']', $panel).addClass('active');
							$('[name="itemAlignment"]', $panel).val(alignment);
						}
						else
						{
							// $('.radial .radialButtons[data-alignment=' +
							// align + ']',
							// $panel).addClass('active').siblings();
							$('[name="itemAlignment"]', $panel).val(alignment);
							// $('.radial .radialButtons[data-alignment=' + pos
							// + ']', $panel).addClass('active').siblings();
						}
						if (size != null)
						{
							// $('.radial .radialButtons[data-width=' + size +
							// ']', $panel).addClass('active').siblings();
							$('[name="itemSize"]', $panel).val(size);
						}
						// alignImage(alignment,
						// $panel.children('.freetext-inline-image'),
						// $panel.children('.panel-body'));

						// froala-editor
						alignImage(alignment, $panel.children('.freetext-inline-image'), $panel.children('.panel-froala'), size);
						$(".freetext-inline-image", $panel).addClass("hasImage");
					}

					setupImageUploadInteractions($panel);
				});
			}
			if (content.type == 'destination')
			{
				callback.done(function(data, textStatus, jqXHR)
				{
					var $panel = $('.panel[dataOrder="' + j + '"]', $currentSection);
					$('.panel-body', $panel).html(content.data);

					// froala-editor
					$('.panel-froala', $panel).html(content.data);

					if (content.action == 'attachImageAction' || content.action == 'attachLibraryImageAction')
					{
						attachImageLayout($panel, content.id != undefined, content.data != '<p><br /></p>', content.action);
					}
					if (content.id == undefined) // no image
					// attachment found
					// (hopefully)
					{
					}
					else
					{
						$("#attachmentId", $panel).val(content.id);
						$("#image", $panel).attr("src", content.metadata['src']);
						$("#image", $panel).show();
						$(".choseFile", $panel).hide();
						var alignment = content.metadata['alignment'];
						$('.radial .radialButtons', $panel).removeClass('active');
						if (alignment == undefined)
						{
							alignment = "bottomwide"
						}
						if (alignment == 'left' || alignment == 'right' || alignment == 'center')
						{ // for older proposals
							alignment = alignment == 'left' ? 'topleft-intext' : alignment == 'right' ? 'topright-intext' : 'bottomwide';
						}
						var pos = alignment.indexOf('top') >= 0 ? 'top' : 'bottom';
						var align = alignment.indexOf('left') >= 0 ? 'left' : alignment.indexOf('right') >= 0 ? 'right' : alignment.indexOf('center') >= 0 ? 'center' : 'wide';
						if ((content.action == 'attachImageAction' || content.action == 'attachLibraryImageAction') && content.data == '<p><br /></p>')
						{
							alignment = 'top' + align;
							$(".inlineImage-addText", $panel).show();
							$('.radial .radialButtons', $panel).not('.position').removeClass('active');
							$('.radial .radialButtons[data-alignment=top]', $panel).addClass('active');
							$('.radial .radialButtons', $panel).not('.position').hide();
						}
						if (content.action == 'attachLibraryImageAction')
						{
							if ($('#content-removed', $panel).css('display') == 'block')
							{
								$('#radialImageAlignmentButtons', $panel).hide();
								$('#image', $panel).hide();
								$('#image', $panel).attr('src', '');
							}
						}
						if (alignment.indexOf('intext') >= 0)
						{
							// $('.radial .radialButtons[data-alignment=' +
							// alignment + ']', $panel).addClass('active');
							$('[name="itemAlignment"]', $panel).val(alignment);
						}
						else
						{
							// $('.radial .radialButtons[data-alignment=' +
							// align + ']',
							// $panel).addClass('active').siblings();
							$('[name="itemAlignment"]', $panel).val(alignment);
							// $('.radial .radialButtons[data-alignment=' + pos
							// + ']', $panel).addClass('active').siblings();
						}
						// alignImage(alignment,
						// $panel.children('.freetext-inline-image'),
						// $panel.children('.panel-body'));
						alignImage(alignment, $panel.children('.freetext-inline-image'), $panel.children('.panel-froala'));
						$(".freetext-inline-image", $panel).addClass("hasImage");
					}

					setupImageUploadInteractions($panel);
				});
			}
			else if (content.type == 'imageAttachment')
			{
				callback.done(function(data, textStatus, jqXHR)
				{
					var $panel = $('.panel[dataOrder="' + j + '"]', $currentSection);
					var body = $('.panel-body', $panel);
					$("#attachmentId", body).val(content.id);
					$("#fileTitle", body).val(content.metadata['title']);
					$("#fileDescription", body).val(content.metadata['description']);
					$("#image", body).attr("src", content.metadata['src']);
					// $('.radial .radialButtons[data-alignment=' +
					// content.metadata['alignment'] + ']',
					// body).addClass('active');
					$('[name="itemAlignment"]', body).val(content.metadata['alignment']);
				});
			}
			else if (content.type == 'image')
			{
				callback.done(function(data, textStatus, jqXHR)
				{
					var $panel = $('.panel[dataOrder="' + j + '"]', $currentSection);
					var body = $('.panel-body', $panel);
					if (content.metadata != undefined) // check because
					// we can't do
					// data
					// migration
					{
						// $('.radial .radialButtons[data-alignment=' +
						// content.metadata['alignment'] + ']',
						// body).addClass('active');
						$('[name="itemAlignment"]', body).val(content.metadata['alignment']);
					}
				});
			}
			else if (content.type == 'attachment')
			{
				callback.done(function(data, textStatus, jqXHR)
				{
					var $panel = $('.panel[dataOrder="' + j + '"]', $currentSection);
					var body = $('.panel-body', $panel);
					$("#attachmentId", body).val(content.id);
					$("#fileTitle", body).val(content.metadata['title']);
					$("#fileDescription", body).val(content.metadata['description']);
					$("#fileName", body).val(content.metadata['fileName']);

					if (content.metadata['embedPDFSwitch'] === "true" ? true : false)
					{
						$(".embedPDFSwitch", body).trigger('click')
					}
				});
			}
			else if (content.type == 'pdf')
			{
				callback.done(function(data, textStatus, jqXHR)
				{
					var $panel = $('.panel[dataOrder="' + j + '"]', $currentSection);
					var body = $('.panel-body', $panel);
					$("#attachmentId", body).val(content.id);
					$("#fileTitle", body).val(content.metadata['title']);
					$("#fileDescription", body).val(content.metadata['description']);
					$("#fileName", body).val(content.metadata['fileName']);

					if (content.metadata['embedPDFSwitch'] === "true" ? true : false)
					{
						$(".embedPDFSwitch", body).trigger('click')
					}
				});
			}
			else if (content.type == 'text')
			{
				callback.done(function(data, textStatus, jqXHR)
				{
					var $panel = $('.panel[dataOrder="' + j + '"]', $currentSection);
					var body = $('.panel-body', $panel);
					$.each(content.metadata, function(key, val)
					{
						$('input[tag="' + key + '"]', body).val(val);
					});
				});
			}
			else if (content.type == 'video')
			{
				callback.done(function(data, textStatus, jqXHR)
				{
					var $panel = $('.panel[dataOrder="' + j + '"]', $currentSection);

					if (content.data != null && content.data != "<p><br /></p>")
					{
						$('.panel-body', $panel).html(content.data);
						// froala-editor
						$('.panel-froala', $panel).html(content.data);
					}

					if (content.id == undefined || content.metadata == undefined) // no
					// video
					// attachment found
					// (hopefully)
					{
					}
					else
					{
						$("#attachmentId", $panel).val(content.id);
						$(".videoId", $panel).attr("src", content.metadata['src']);
						$(".videoId", $panel).show();
						var alignment = content.metadata['alignment'];
						var size = content.metadata['size'];
						$('.radial .radialButtons', $panel).removeClass('active');
						if (alignment == undefined)
						{
							alignment = "bottomwide"
						}
						if (alignment == 'left' || alignment == 'right' || alignment == 'center')
						{ // for older proposals
							alignment = alignment == 'left' ? 'topleft-intext' : alignment == 'right' ? 'topright-intext' : 'bottomwide';
						}
						var pos = alignment.indexOf('top') >= 0 ? 'top' : 'bottom';
						var align = alignment.indexOf('left') >= 0 ? 'left' : alignment.indexOf('right') >= 0 ? 'right' : alignment.indexOf('center') >= 0 ? 'center' : 'wide';

						if (alignment.indexOf('intext') >= 0)
						{
							// $('.radial .radialButtons[data-alignment=' +
							// alignment + ']', $panel).addClass('active');
							$('[name="itemAlignment"]', $panel).val(alignment);
						}
						else
						{
							// $('.radial .radialButtons[data-alignment=' +
							// align + ']',
							// $panel).addClass('active').siblings();
							// $('.radial .radialButtons[data-alignment=' + pos
							// + ']', $panel).addClass('active').siblings();
							$('[name="itemAlignment"]', $panel).val(alignment);
						}
						alignVideo(alignment, $panel.children('.freetext-inline-image'), $panel.children('.panel-body'), size);
						$(".freetext-inline-image", $panel).addClass("hasImage");
					}
				});
			}
			else if (content.type == 'sign')
			{
				callback.done(function(data, textStatus, jqXHR)
				{
					var $panel = $('.panel[dataOrder="' + j + '"]', $currentSection);
					var body = $('.panel-body', $panel);
					var footer = $('.panel-footer', $panel);
					initSignTable(j, $currentSection);
					// if all signature types are disabled, enable all of them
					// let user choose again.
					if (content.metadata.hideDrawSign == 'true' && content.metadata.hideTypeSign == 'true' && content.metadata.hideUploadSign == 'true')
					{
						content.metadata.hideDrawSign = 'false';
						content.metadata.hideTypeSign = 'false';
						content.metadata.hideUploadSign = 'false';
					}
					$.each(content.metadata, function(key, val)
					{
						if (key == 'theme')
						{
							$('span.theme-name', footer).text(val);
							return;
						}
						if (val == 'true')
						{
							$('#' + key, footer).click();
						}
					});
					$.each(restoredProposal.signSettings, function(index)
					{
						var signSetting = restoredProposal.signSettings[index];
						var signTable = $('[id="signTable"]:eq(' + index + ')');
						var tr = $('tbody', signTable).find('tr:last');
						if (signSetting.length == 0)
						{
							tr.attr('data-uuid', uuid());
							return;
						}
						$.each(signSetting, function(i)
						{
							if (i > 0)
							{
								tr.parent().append(tr.clone().find('select[name="email"]').val(signSetting[i]['email']).end().find('input[name="dateNeeded"]').prop('checked', signSetting[i]['dateNeeded']).end().attr('data-uuid', signSetting[i]['uuid']).show());
								return;
							}
							tr.find('select[name="email"]').val(signSetting[i]['email']).end().find('input[name="dateNeeded"]').prop('checked', signSetting[i]['dateNeeded']).end().attr('data-uuid', signSetting[i]['uuid']);
						})


						console.log(signSetting);
					})
				});
			}
			else if (content.type == 'spreadsheet')
			{
				callback.done(function(data, textStatus, jqXHR)
				{
					var $panel = $('.panel[dataOrder="' + j + '"]', $currentSection);

					var spreadsheetData = undefined;
					$.each(restoredProposal.spreadsheets, function(index)
					{
						if (restoredProposal.spreadsheets[index].order == $currentSection.closest('.tab-pane').index() + '.' + j)
							spreadsheetData = restoredProposal.spreadsheets[index];
					})
					initKendoSpreadsheet($panel, spreadsheetData);
				});
			}

			callback.done(function(data, textStatus, jqXHR)
			{
				var p = $('.panel[dataOrder="' + j + '"]', $currentSection);
				if (content.breakPageBeforeInPDF === "on")
				{
					p.find('.panel-heading').find('.js-switch').trigger('click');
				}
				restorePanelFixedState(content, p);
			});
			return callback;
		}

	}

}

function hideHeadings(active)
{
	if (active)
	{
		$('.noTitle-switch').each(function()
		{
			if (!($(this).is(':checked')))
			{
				$(this).click();
				$(this).attr('checked', true);
			}
		});
	}
	else
	{
		$('.noTitle-switch').each(function()
		{
			if (($(this).is(':checked')))
			{
				$(this).click();
				$(this).attr('checked', true);
			}
		});
	}
}

function getProposalSignSettings()
{
	var signSettings = [];
	$('[id="signTable"]').each(function()
	{
		signSettings.push(getProposalSignSetting($(this)));
	})
	return signSettings;
}

function getProposalPaymentOption()
{
	var $options = $('.payment-option-fields');
	if ($options.length == 0)
		return null;
	return {
		title : $options.find('[name="paymentTitle"]').val(),
		description : $options.find('[name="paymentDescription"]').val(),
		payee : $options.find('[name="paymentPayee"]').val(),
		payer : $options.find('[name="paymentPayer"]').val(),
		type : $options.find('.payment-value-btn').attr('data-type'),
		value : isNaN(parseFloat($options.find('[name="paymentValue"]').val())) ? 0.0 : parseFloat($options.find('[name="paymentValue"]').val()),
	};
}

function getProposalSpreadsheets()
{
	return $('.spreadsheet').map(function()
	{
		if ($(this).data('kendoSpreadsheet'))
		{
			var data = $(this).data('kendoSpreadsheet').toJSON();
			data.order = $(this).closest('.tab-pane').index() + '.' + $(this).closest('.panel').attr('dataorder');
			return data;
		}
		return null;
	}).get();
}

function getProposalSignSetting(signTable)
{
	var signSetting = [];
	$('[id="personOne"]', signTable).each(function()
	{
		signSetting.push({
			email : $('select[name="email"]', $(this)).val(),
			dateNeeded : $('input[name="dateNeeded"]', $(this)).is(':checked'),
			uuid : $(this).attr('data-uuid'),
		});
	})
	return signSetting;
}

function getProposalSections()
{
	dismissCK();

	var orderedSections = [];
	var domSections = $("#documentSectionBrowser a");

	$.each(domSections, function(i, e)
	{
		var section = {};
		var sectionId = $(e).attr('href');
		var contentSection = $(".editorContent > " + sectionId);

		section.breakPageBeforeInPDF = contentSection.find(".pageBreakSection").find(".js-switch").prop("checked") ? "on" : "off";
		section.titleHidden = contentSection.find(".noTitle-switch").prop("checked") ? "on" : "off";

		if (sectionId == "#doc-cover")
		{
			section.type = 'cover';
			section.breakPageBeforeInPDF = "off";
			var sectionTitle = contentSection.find('#cover-title').val();
			section.title = sectionTitle;
		}
		else if (sectionId == "#doc-pricing")
		{
			section.type = 'pricing';
			section.title = $('#pricing-title').val();
			if ($(".editorContent " + sectionId).attr("data-enabled") == undefined)
			{
				section.enabled = true;
			}
			else
			{
				section.enabled = $(".editorContent " + sectionId).attr("data-enabled") == "true";
			}
		}
		else if (sectionId == "#doc-itinerary")
		{
			section.type = 'itinerary';
			section.title = $('#itinerary-title').val();
			if ($(".editorContent " + sectionId).attr("data-enabled") == undefined)
			{
				section.enabled = true;
			}
			else
			{
				section.enabled = $(".editorContent " + sectionId).attr("data-enabled") == "true";
			}
		}
		else
		{
			section.type = contentSection.find('.contentContainer[sectiontype]').length > 0 ? contentSection.find('.contentContainer[sectiontype]').attr('sectiontype') : 'dynamic';

			var sectionTitle = contentSection.find('.sectionTitle').val();
			section.title = sectionTitle;
		}

		var domContents = $(".editorContent " + sectionId + " .contentContainer > .group-panel > .panel");
		section.content = [];
		$.each(domContents, function(i, panel)
		{
			var content = getProposalContent(panel);
			if (typeof content == 'object')
			{
				section.content.push(content);
			}
		});

		orderedSections.push(section);
	});

	return orderedSections;
}

function getProposalContent(panel)
{

	var $domPanel = $(panel);
	var content = {};
	content.type = $domPanel.attr("type");
	content.breakPageBeforeInPDF = $domPanel.find('.pageBreakPanel').find('.js-switch').prop('checked') ? "on" : "off";
	if (content.type == undefined)
	{
		return true;
	}

	if (content.type != 'editor' && content.type != 'attachment' && content.type != 'imageAttachment')
	{
		// only the editor and attachments dont have dataId
		content.id = $domPanel.attr("dataId");
	}

	// Load type specifc fields...
	if (content.type == 'editor')
	{
		// ck remove
		// content.data = $domPanel.find(".panel-body").html();
		content.data = getFroalaEditorData($domPanel.find(".panel-froala"));
		content.action = $domPanel.attr('action');

		var id = $domPanel.find("#attachmentId").val();
		if (id == '')
		{
			// logger.warn("Not adding attachment due to failed upload
			// or missing attachment.")
		}
		else
		{
			content.id = $domPanel.find("#attachmentId").val();
			var align;
			align = $('[name="itemAlignment"]', $domPanel).val();
			if (align == null)
			{
				align = "bottomcenter"; // set default
			}
			var width = $('[name="itemWidth"]', $domPanel).val();
			if (width == '')
			{
				width = "100";
			}

			content.metadata = {
				source : "image",
				src : $domPanel.find("#image").attr("src"),
				alignment : align,
				size : width + ""
			}
		}
	}
	if (content.type == 'destination')
	{
		content.data = getFroalaEditorData($domPanel.find(".panel-froala"));
		content.action = $domPanel.attr('action');

		var id = $domPanel.find("#attachmentId").val();
		if (id == '')
		{
			// logger.warn("Not adding attachment due to failed upload
			// or missing attachment.")
		}
		else
		{
			content.id = $domPanel.find("#attachmentId").val();
			var align;
			align = $('[name="itemAlignment"]', $domPanel).val();
			if (align == null)
			{
				align = "bottomcenter"; // set default
			}

			// var attachments = '';
			// $.each($('.section-head', $domPanel), function(i, item)
			// {
			// if ($(item).attr('thumb-id'))
			// {
			// attachments += $(item).attr('thumb-id') + ', ';
			// }
			//
			// });
			// $.each($('.destination-poi', $domPanel), function(i,
			// item)
			// {
			// if ($(item).attr('poi-id'))
			// {
			// attachments += $(item).attr('poi-id') + ', ';
			// }
			//
			// });
			content.metadata = {
				source : "image",
				src : $domPanel.find("#image").attr("src"),
				alignment : align
			// attachments : attachments
			}
		}
	}
	else if (content.type == 'attachment')
	{
		var id = $domPanel.find("#attachmentId").val();
		if (id == '')
		{
			logger.warn("Not adding attachment due to failed upload or missing attachment.");
			return true;
		}

		content.id = $domPanel.find("#attachmentId").val();
		content.metadata = {
			title : $domPanel.find("#fileTitle").val(),
			description : $domPanel.find("#fileDescription").val(),
			fileName : $domPanel.find("#fileName").val(),
			embedPDFSwitch : $domPanel.find("#embedPDFSwitch").prop("checked") ? "true" : "false",
			attached : true
		};
	}
	else if (content.type == 'pdf')
	{
		var id = $domPanel.find("#attachmentId").val();
		if (id == '')
		{
			logger.warn("Not adding attachment due to failed upload or missing attachment.");
			return true;
		}

		content.id = $domPanel.find("#attachmentId").val();
		content.metadata = {
			source : "pdf",
			title : $domPanel.find("#fileTitle").val(),
			description : $domPanel.find("#fileDescription").val(),
			fileName : $domPanel.find("#fileName").val(),
			embedPDFSwitch : $domPanel.find("#embedPDFSwitch").prop("checked") ? "true" : "false"
		};
	}
	else if (content.type == 'imageAttachment')
	{
		var id = $domPanel.find("#attachmentId").val();
		if (id == '')
		{
			logger.warn("Not adding attachment due to failed upload or missing attachment.");
			return true;
		}

		content.id = $domPanel.find("#attachmentId").val();
		content.metadata = {
			title : $domPanel.find("#fileTitle").val(),
			description : $domPanel.find("#fileDescription").val(),
			src : $domPanel.find("#image").attr("src"),
			alignment : $(".radial > .radialButtons.active", $domPanel).data("alignment")
		};
	}
	else if (content.type == 'text')
	{
		content.data = $domPanel.find(".originalTemplate").html();
		content.metadata = {};
		$.each($domPanel.find('input[type="text"]'), function(i, field)
		{
			content.metadata[$(field).attr("tag")] = $(field).val();
		});
	}
	else if (content.type == 'generated')
	{
		if (content.id == 'con-info')
		{
			content.metadata = {};
			$.each($domPanel.find('.col-sm-8 input.form-control, .col-sm-8 select.form-control'), function(i, field)
			{
				content.metadata[$(field).attr("id")] = $(field).val();
			});
			content.metadata['tableOfContent'] = $('#tableOfContent', $domPanel).is(':checked') ? 'on' : 'off';
			content.metadata['globalTitleHidden'] = $('#globalTitleHidden', $domPanel).is(':checked') ? 'on' : 'off';
			content.metadata['displayOrderButton'] = $('#displayOrderButton').prop('checked');
			content.metadata['spectrumColor'] = $('input:radio[name=spectrumColor]:checked', $domPanel).val();
			content.metadata['coverPageImage'] = $("#coverPageHiddenField", $domPanel).val();
			content.metadata['mnoOrganizationId'] = $("#mnoOrganizationId", $domPanel).val();
			content.metadata['landscapeMode'] = $('#landscapeMode', $domPanel).is(':checked') ? 'on' : 'off';
			content.metadata['showRemarks'] = $('#showRemarks', $domPanel).is(':checked') ? 'on' : 'off';
			$.each($domPanel.find('.col-sm-8 .documentTagSrc'), function(i, field)
			{
				if (customFields[$(field).attr("id")] && customFields[$(field).attr("id")].type == 'date')
				{
					var _d = moment($(field).val(), dateFormatPattern);
					if (_d.isValid())
					{
						content.metadata[$(field).attr("id")] = _d.format('YYYY-MM-DD');
					}
					else
					{
						content.metadata[$(field).attr("id")] = '';
					}
				}
				else
				{
					content.metadata[$(field).attr("id")] = $(field).val();
				}
			});
		}
		else if (content.id == 'con-custom-fields')
		{
			content.metadata = {};
			$.each($domPanel.find('.col-sm-8 .documentTagSrc'), function(i, field)
			{
				if (customFields[$(field).attr("id")] && customFields[$(field).attr("id")].type == 'date')
				{
					var _d = moment($(field).val(), dateFormatPattern);
					if (_d.isValid())
					{
						content.metadata[$(field).attr("id")] = _d.format('YYYY-MM-DD');
					}
					else
					{
						content.metadata[$(field).attr("id")] = '';
					}
				}
				else
				{
					content.metadata[$(field).attr("id")] = $(field).val();
				}
			});
		}
		else if (content.id == 'con-iti')
		{
			content.metadata = {
				style : $('#itiStyleSelect li.selected').val()
			};
		}
	}
	else if (content.type == 'image')
	{
		// $(".radial > .radialButtons.active", $domPanel).data("alignment")
		content.metadata = {
			alignment : $('[name="itemAlignment"]', $domPanel).val()
		};
	}
	else if (content.type == 'video')
	{

		content.data = getFroalaEditorData($domPanel.find('.panel-froala'));
		content.action = $domPanel.attr('action');

		var id = $domPanel.find("#attachmentId").val();
		var src = $domPanel.find(".videoId").attr("src");
		if (id == '' || src == '')
		{
			// logger.warn("Not adding attachment due to failed upload
			// or missing attachment.")
		}
		else
		{
			content.id = $domPanel.find("#attachmentId").val();
			var align = $('[name="itemAlignment"]', $domPanel).val();
			if (align == null || align == '')
			{
				align = "bottomcenter"; // set default
			}
			var size = $('[name="itemSize"]', $domPanel).val();
			if (size == null || size == '')
			{
				size = "default";
			}

			content.metadata = {
				source : "video",
				src : $domPanel.find(".videoId").attr("src"),
				alignment : align,
				size : size,
				width : $('iframe.videoId', $domPanel).width(),
				height : $('iframe.videoId', $domPanel).height(),
			}
		}
	}
	else if (content.type == 'sign')
	{
		content.data = '';
		content.metadata = {
			hideDrawSign : $('#hideDrawSign', $domPanel).prop('checked'),
			hideTypeSign : $('#hideTypeSign', $domPanel).prop('checked'),
			hideUploadSign : $('#hideUploadSign', $domPanel).prop('checked'),
			theme : $('span.theme-name', $domPanel).text(),
		};
	}
	else if (content.type == 'spreadsheet')
	{

	}

	content.fixed = $domPanel.attr('fixed') == "true";
	return content;
}

function getFroalaEditorData(editor)
{
	if (editor.data('froala.editor'))
	{
		return editor.froalaEditor('html.get').replace(/\<br>/g, '<br/>');
	}
	else
	{
		return editor.html();
	}
}

function strip(html)
{
	var tmp = document.createElement("DIV");
	tmp.innerHTML = html;
	return htmlEncode(tmp.textContent) || htmlEncode(tmp.innerText) || "";
}

function htmlEncode(value)
{
	return $('<div/>').text(value).html();
}

