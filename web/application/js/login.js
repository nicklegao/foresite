var MIN_PASSWORD_LENGTH = 8;

jQuery.validator.addMethod("passwordStrength", function(value, element)
{
	var digit = new RegExp('[0-9]');
	if (!digit.test(value))
	{
		return false;
	}
	var lowercase = new RegExp('[a-z]');
	if (!lowercase.test(value))
	{
		return false;
	}
	var uppercase = new RegExp('[A-Z]');
	if (!uppercase.test(value))
	{
		return false;
	}

	return true;
}, "Password must be atleast " + MIN_PASSWORD_LENGTH + " characters, contain uppercase, lowercase and a number");

var Login = function()
{
	var runLoginButtons = function()
	{
		$('.go-back').click(function()
		{
			window.location = '/';
		});
	};

	var runSetupPlaceholders = function()
	{
		$('input, textarea').placeholder();
	};

	var runSetupValidation = function()
	{
		$.validator.setDefaults({
			errorElement : "span", // contain the error msg in a small tag
			errorClass : 'help-block',
			errorPlacement : function(error, element)
			{ // render error placement for each input type
				if (element.attr("type") == "radio" || element.attr("type") == "checkbox")
				{ // for chosen elements, need to insert the error after the
					// chosen container
					error.insertAfter($(element).closest('.form-group').children('div').children().last());
				}
				else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy")
				{
					error.appendTo($(element).closest('.form-group').children('div'));
				}
				else
				{
					error.insertAfter(element);
					// for other inputs, just perform default behavior
				}
			},
			ignore : ':hidden',
			highlight : function(element)
			{
				$(element).closest('.help-block').removeClass('valid');
				// display OK icon
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
				// add the Bootstrap error class to the control group
			},
			unhighlight : function(element)
			{ // revert the change done by hightlight
				$(element).closest('.form-group').removeClass('has-error');
				// set error class to the control group
			},
			success : function(label, element)
			{
				label.addClass('help-block valid');
				// mark the current input as valid and display OK icon
				$(element).closest('.form-group').removeClass('has-error');
			},
			highlight : function(element)
			{
				$(element).closest('.help-block').removeClass('valid');
				// display OK icon
				$(element).closest('.form-group').addClass('has-error');
				// add the Bootstrap error class to the control group
			},
			unhighlight : function(element)
			{ // revert the change done by hightlight
				$(element).closest('.form-group').removeClass('has-error');
				// set error class to the control group
			}
		});
	};
	var runLoginValidator = function()
	{
		var form = $('.form-login');
		var errorHandler = $('.errorHandler', form);
		form.validate({
			rules : {
				username : {
					minlength : 2,
					required : true
				},
				password : {
					minlength : MIN_PASSWORD_LENGTH,
					required : function()
					{
						return $('[name="username"]').val() != '';
					}
				}
			},
			highlight : function(element)
			{
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			unhighlight : function(element)
			{
				$(element).closest('.form-group').removeClass('has-error');
			},
			success : function(label, element)
			{
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			},
			submitHandler : function(form)
			{
				errorHandler.hide();
				form.submit();
			},
			invalidHandler : function(event, validator)
			{ // display error alert on form submit
				errorHandler.show();
			}
		});
	};
	var runChangePasswordValidator = function()
	{
		var form = $('.form-change-password');
		var errorHandler = $('.errorHandler', form);
		form.validate({
			rules : {
				currentPassword : {
					minlength : MIN_PASSWORD_LENGTH,
					required : true
				},
				newPassword : {
					minlength : MIN_PASSWORD_LENGTH,
					passwordStrength : true,
					required : true
				},
				repeatPassword : {
					equalTo : $('#newPassword', form),
					required : true
				}
			},
			highlight : function(element)
			{
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			unhighlight : function(element)
			{
				$(element).closest('.form-group').removeClass('has-error');
			},
			success : function(label, element)
			{
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			},
			submitHandler : function(form)
			{
				errorHandler.hide();
				form.submit();
			},
			invalidHandler : function(event, validator)
			{ // display error alert on form submit
				errorHandler.show();
			}
		});
	};
	var runForgotPasswordValidator = function()
	{
		var form = $('.form-forgot-password');
		var errorHandler = $('.errorHandler', form);
		form.validate({
			rules : {
				email : {
					email : true,
					maxlength : 100,
					required : true
				}
			},
			highlight : function(element)
			{
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			unhighlight : function(element)
			{
				$(element).closest('.form-group').removeClass('has-error');
			},
			success : function(label, element)
			{
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			},
			submitHandler : function(form)
			{
				errorHandler.hide();
				form.submit();
			},
			invalidHandler : function(event, validator)
			{ // display error alert on form submit
				errorHandler.show();
			}
		});
	};
	var runRegisterValidator = function()
	{
		var form3 = $('.form-register');
		var errorHandler3 = $('.errorHandler', form3);
		form3.validate({
			rules : {
				username : {
					minlength : 2,
					maxlength : 50,
					required : true
				},
				name : {
					minlength : 2,
					maxlength : 50,
					required : true
				},
				surname : {
					minlength : 2,
					maxlength : 50,
					required : true
				},
				email : {
					email : true,
					maxlength : 100,
					required : true
				},
				mobileNumber : {
					minlength : 2,
					maxlength : 50,
					required : false
				},
				password : {
					minlength : MIN_PASSWORD_LENGTH,
					passwordStrength : true,
					required : true
				},
				repeatPassword : {
					equalTo : $('[name="password"]', form3),
					required : true
				},
				company : {
					required : true,
					maxlength : 100
				}
			},
			submitHandler : function(form)
			{
				if ($(form).find(":selected").text() == "Choose your country")
				{
					errorHandler3.show();
					if ($('span[for="company"].help-block.valid') == null) {
						$('#country').parent().append('<span style="color: red;" for="country" class="help-block">This field is required.</span>');
					} else {
						$('span[for="country"].help-block.valid').text('This field is required.');
					}
					$('.countrySelecter').addClass('has-error');
					// $('input.select-dropdown').addClass('countryError');
					// $('input.select-dropdown').val("Enter a valid country");
				} else if (($(form).find("#company").val().replace(/\s/g, '')).toUpperCase() == "DEFAULT") {
					if ($('span[for="company"].help-block.valid') == null) {
						$('#company').parent().append('<span style="color: red;" for="company" class="help-block">This company name cannot be used, please try another name.</span>');
					} else {
						$('span[for="company"].help-block.valid').text('This company name cannot be used, please try another name.');
					}
					$('#company').addClass('has-error');
					$('#company').val('');
				}
				else
				{
					errorHandler3.hide();
					form.submit();
				}


				console.log("submit register form");
				if (typeof grecaptcha != 'undefined')
				{
					grecaptcha.reset();
				}
			},
			invalidHandler : function(event, validator)
			{ // display error alert on form submit
				if (typeof grecaptcha != 'undefined')
				{
					grecaptcha.reset();
				}
				errorHandler3.show();
			}
		});
	};

	var loadRegisterData = function()
	{
	}

	return {
		// main function to initiate template pages
		_init : function()
		{
			runLoginButtons();
			runSetupValidation();
			runSetupPlaceholders();
		},
		initLogin : function()
		{
			this._init();
			runLoginValidator();
		},
		initChangePassword : function()
		{
			this._init();
			runChangePasswordValidator();
		},
		initForgotPassword : function()
		{
			this._init();
			runForgotPasswordValidator();
		},
		initRegister : function()
		{
			this._init();
			runRegisterValidator();
			loadRegisterData();
		}
	};
}();
