var proposalTable = undefined;
var tableColumnDefs = undefined;

$(document).ready(function()
{

	$(document).on("click", ".displayProposalNotesAction", displayProposalNotesAction);

	$(window).on('focus', function()
	{
		redrawTable();
	});

	$(document).on('click', '[data-action="launchFullscreen"]', function(e)
	{
		if (!$('body').hasClass("full-screen"))
		{

			$('body').addClass("full-screen");

			if (document.documentElement.requestFullscreen)
			{
				document.documentElement.requestFullscreen();
			}
			else if (document.documentElement.mozRequestFullScreen)
			{
				$(document.documentElement).mozRequestFullScreen();
			}
			else if (document.documentElement.webkitRequestFullscreen)
			{
				document.documentElement.webkitRequestFullscreen();
			}
			else if (document.documentElement.msRequestFullscreen)
			{
				document.documentElement.msRequestFullscreen();
			}

		}
		else
		{

			$('body').removeClass("full-screen");

			if (document.exitFullscreen)
			{
				document.exitFullscreen();
			}
			else if (document.mozCancelFullScreen)
			{
				document.mozCancelFullScreen();
			}
			else if (document.webkitExitFullscreen)
			{
				document.webkitExitFullscreen();
			}

		}
		e.preventDefault();
	});

});

function isTouchSupported()
{
	// return ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera
	// Mini/i.test(navigator.userAgent) );
	return 'ontouchstart' in document.documentElement;
};

// For the table
var TableData = function()
{

	$.fn.dataTable.ext.errMode = function(event, settings, techNote, tn)
	{
		console.log('An error has been reported by DataTables: ', techNote, tn);
	};

	var currentDashboardDataRequest = undefined;
	var salesFilter = localStorage['salesFilters'] ? JSON.parse(localStorage["salesFilters"]) : [];
	$.each(salesFilter, function(i, e)
	{
		$('.status-bucket-' + e).addClass('active');
	});
	// var endDate = moment();
	// var startDate = moment(0);

	var domHead = "<'navbar' <'navbar-form navbar-left'> <'navbar-form navbar-right'> ><'proposalResizeBlur hidden'>";
	var domBody = "t";
	var domFooter = "<'navbar' <'navbar-form navbar-left' i> <'navbar-form navbar-right'<'form-group' p>> <'navbar-form navbar-right' <'#actionsHolder'>> >";

	var runSetupPlaceholders = function()
	{
		$('input, textarea').placeholder();
	};

	var forecastMonthClasses = getCssClassesForMonths();


	var runDataTable = function()
	{

		tableColumnDefs = [ {
			title : '',
			width : '50',
			orderable : false,
			data : 'statusBucket',
			render : function(data, type, row)
			{
				if (type == "display")
				{
					return $("<div>").append($("<i>").attr('class', 'qc qc-' + row.statusBucket)).html();
				}
				else if (type == "filter")
				{
					return row.statusBucket;
				}
				return null;
			},
			createdCell : function(td, cellData, rowData, row, col)
			{
				$(td).attr('class', 'statusCell text-center');
			}
		}, {
			title : 'Title',
			orderable : false,
			data : 'proposalTitle',
			ciFilterType : 'text',
			createdCell : function(td, cellData, rowData, row, col)
			{
				$(td).html($('<a>').attr('href', "/pdf/" + rowData.id + ".pdf?proposal=" + rowData.id + "&v-key=" + rowData.vKey).attr("target", "pdf-" + rowData.id).text(rowData.proposalTitle.htmlEscaped()));
			}
		}, {
			title : 'Client Company',
			orderable : false,
			data : 'clientCompany',
			ciFilterType : 'text',
			render : function(data, type, row)
			{
				if (type == 'sort')
					return data;
				return data.htmlEscaped();
			}
		}, {
			title : 'First Name',
			orderable : false,
			data : 'clientName',
			ciFilterType : 'text',
			createdCell : function(td, cellData, rowData, row, col)
			{
				if (rowData.notifications > 0)
				{
					$(td).append($('<div>').attr('class', 'qc qc-chat notificationCtr').append($('<span>').html(rowData.notifications)));
				}
			},
			render : function(data, type, row)
			{
				if (type == 'sort')
					return data;
				return data.htmlEscaped();
			}
		}, {
			title : 'Last Name',
			orderable : false,
			data : 'clientLastName',
			ciFilterType : 'text',
			createdCell : function(td, cellData, rowData, row, col)
			{
				if (rowData.notifications > 0)
				{
					$(td).append($('<div>').attr('class', 'qc qc-chat notificationCtr').append($('<span>').html(rowData.notifications)));
				}
			},
			render : function(data, type, row)
			{
				if (type == 'sort')
					return data;
				return data.htmlEscaped();
			}
		}, {
			title : 'Value',
			orderable : false,
			width : '120',
			data : 'totalMinimumCost',
			render : function(data, type, row)
			{
				if (type == 'sort')
					return data;
				return '<span class="moneytaryValue"> ' + data.formatMoney() + '</span>';
			},
			createdCell : function(td, cellData, rowData, row, col)
			{
				$(td).addClass('text-right');
			}
		}, {
			title : 'Confidence',
			orderable : false,
			data : 'forecastConfidence',
			width : '600',
			render : function(data, type, row)
			{
				if (type == "display")
					return '<div class="row">' + '<div class="col-lg-2 text-center"><output class="output"></output></div>' + '<div class="col-lg-10"><input type="range" min="0" max="100" step="10" value="' + data + '"></div>' + '</div>';
				else
					return data;
			}
		}, {
			title : 'Forecast Month',
			orderable : false,
			data : 'forecastMonth',
			width : '100',
			render : function(data, type, row)
			{
				if (type == 'sort')
					return data;

				var date = new Date(data);
				var today = moment();
				today.set({
					'millisecond' : 0,
					'second' : 0,
					'minute' : 0,
					'hour' : 0,
					'date' : 1
				});
				var select = '<select class="forecastMonthDropdown form-control">';

				if (moment(date).valueOf() < today.valueOf())
					select += '<option value="0">Unassigned</option>';
				else
					select += '<option value="0" selected="selected">Unassigned</option>';

				for (var i = 0; i < 6; i++)
				{
					var selected = "";
					if (moment(date).valueOf() == today.valueOf())
					{
						selected = ' selected="selected"';
					}
					select += "<option value=" + today.valueOf() + selected + ">" + today.format("MMMM") + "</option>";
					today.add(1, 'months');
				}
				select += "</select>";
				return select;
			}
		}, {
			title : 'Actions',
			width : '200',
			orderable : false,
			render : function(data, type, row)
			{
				if (type == "display")
				{
					return getSalesforecastButtons(row);
				}
				return null;
			}
		} ];
		proposalTable = $('table#forecastProposals').DataTable({
			"autoWidth" : false,
			"processing" : true,
			"serverSide" : true,
			"ajax" : {
				"url" : "/api/salesForecast",
				"method" : "POST",
				"data" : function(d)
				{
					// d.endDate = endDate.format('YYYY-MM-DD');
					// d.startDate = startDate.format('YYYY-MM-DD');
					d.statusFilter = salesFilter.join();
				}
			},
			"createdRow" : function(row, data, dataIndex)
			{
				$(row).attr("data-id", data.id).addClass("status-bucket-" + data.statusBucket).addClass(forecastMonthClasses.get(data.forecastMonth));

			},
			"aoColumns" : tableColumnDefs,
			"oLanguage" : {
				"sLengthMenu" : "_MENU_",
				"sSearch" : "",
				"oPaginate" : {
					"sPrevious" : "",
					"sNext" : ""
				},
				"sInfo" : '<i class="fa fa-spin fa-cog dashboardLoadingIndicator"></i> Showing _START_ to _END_ of _TOTAL_ proposals',
				"sInfoEmpty" : "No TravelDocs found",
			},
			"aaSorting" : [ [ 7, 'asc' ], [ 6, 'asc' ], [ 5, 'desc' ] ],
			"aLengthMenu" : [ [ 5, 10, 15, 20, -1 ], [ 5, 10, 15, 20, "All" ] // change
			// per
			// page
			// values
			// here
			],
			// set the initial value
			"iDisplayLength" : -1,
			"sDom" : domHead + domBody + domFooter,
            "initComplete": function() {
            },
		});

		proposalTable.on('draw.dt', function()
		{
			$(".dashboardLoadingIndicator").animate({
				opacity : 0
			}, 1000);

			if (currentDashboardDataRequest != undefined)
			{
				currentDashboardDataRequest.abort();
			}
			currentDashboardDataRequest = $.ajax({
				url : '/api/dashboard/summary',
				method : 'post'
			}).done(function(data, textStatus, jqXHR)
			{

				logger.info("Start rendering after posting to /api/dashboard/summary and start rendering.");
				currentDashboardDataRequest = undefined;

				$statusSummary = $(".buckets");
				dataMap = {};

				$.each(data, function(status, val)
				{
					var totalSummary = $('.status-bucket-' + status + " .totalSummary", $statusSummary);
					var currentText = totalSummary.text();
					var currentVal = parseInt(currentText);
					if (currentVal != val)
					{
						if (isNaN(currentVal))
							currentVal = 0;

						var change = Math.max(1, Math.floor(Math.abs(val - currentVal) / 10));
						var loop = setInterval(function()
						{
							if (currentVal < val)
							{
								currentVal = Math.min(currentVal + change, val);
							}
							else if (currentVal > val)
							{
								currentVal = Math.max(currentVal - change, val);
							}
							else
							{
								clearInterval(loop);
								totalSummary.animateCss("rubberBand");
							}
							totalSummary.text(currentVal);
						}, 50);
					}

				});

				logger.info("Finish rendering after posting to /api/dashboard/summary");

                blurTable(false, false)
			});

			// Render the range slider plugin after table draw event
			$inputRange = $('input[type="range"]', this);

			// Example functionality to demonstrate a value feedback
			function valueOutput(element)
			{
				var value = element.value, output = $(element).closest('td').find('output').eq(0)[0];
				output.innerHTML = value + "%";
			}
			for (var i = $inputRange.length - 1; i >= 0; i--)
			{
				valueOutput($inputRange[i]);
			}
			;
			$(document).on('input', 'input[type="range"]', function(e)
			{
				valueOutput(e.target);
			});
			// end

			var $slider = $('input[type="range"]');
			$slider.rangeslider({
				polyfill : false
			});

			// add in divisions for the months
			var class2month = getMonthsFromClasses();
			for (var i = 0; i <= class2month.size; i++)
			{
				$("tbody > tr.forecast-month-" + i + ":first", this).before('<tr><td colspan="8"><h2>' + class2month.get(i) + '</h2></td></tr>');
			}
		});

		proposalTable.on('column-visibility.dt', function(e, settings, column, state)
		{
			rebuildColumnFilters();
		});
		rebuildColumnFilters();

		// modify table search input
		$('#forecastProposals_wrapper .dataTables_filter input').addClass("form-control").attr("placeholder", "Search...");

		// modify table paginate dropdown
		$('#forecastProposals_wrapper .dataTables_length select').addClass("form-control");
		$('#forecastProposals_wrapper .dataTables_length').closest(".form-group").before($("<span>").text('Show')).after($("<span>").text('proposals '));

		// Attach events
		$(".buckets > li").click(function(e)
		{
			var bucket = $(e.currentTarget).attr('data-bucket');
			$(e.currentTarget).toggleClass("active");
			var selected = $('#sparks li.active');
			salesFilter = [];
			$.each(selected, function(i, e)
			{
				salesFilter.push($(e).attr('data-bucket'));
			});
			localStorage.setItem('salesFilters', JSON.stringify(salesFilter));
			redrawTable();
		});

		// update forecast
		$("#forecastProposals").on("change", 'select.forecastMonthDropdown', updateForecastMonth);
		$("#forecastProposals").on("change", 'input[type="range"]', updateForecastConfidence);

		$("#forecastProposals").on("change keyup", "tr.searchRow input, tr.searchRow select", function(e)
		{
			var searchCellIndex = $(this).closest("th").index();
			var headerCell = $("#forecastProposals thead tr").not('.searchRow').find("th")[searchCellIndex];
			proposalTable.column(headerCell).search($(this).val());

			var count = countColumnFilters();
			if (count > 0)
			{
				$(".columnFilterCount").text(count);
				$(".columnFiltersEnabled").show();
			}
			else
			{
				$(".columnFilterCount").text('');
				$(".columnFiltersEnabled").hide();
			}
			redrawTable();
		});

		$("#forecastProposals").on("click", ".clearColumnFilters", function(e)
		{
			proposalTable.columns().search('');
			rebuildColumnFilters(true);

			redrawTable();
		});
		$("#forecastProposals").on("click", ".toggleColumnFilters", function(e)
		{
			proposalTable.columns().search('');
			var show = $('#forecastProposals > thead > tr.searchRow').size() == 0;

			rebuildColumnFilters(show);
			if (!show) {
                redrawTable()
			}
		});
	};
	return {
		// main function to initiate template pages
		init : function()
		{
			runDataTable();
			runSetupPlaceholders();
		}
	};
}();

//Function to blur the proposal table/control notification loading and ignore when searching using filters
function blurTable(on, search) {

    $('.proposalResizeBlur').height($('table#forecastProposals').height() + 2);
    $('.proposalResizeBlur').width($('table#forecastProposals').width() + 2);

    if (on && !search) {
        if (!($('.proposalResizeBlur').is(':visible'))) {
            showNotificationMessage(true);
            showLoadingCog(true);
            $('.proposalResizeBlur').removeClass('hidden');
            //$('.DTFC_ScrollWrapper').addClass('scroll-fade-in'); //removed blur effect in preference for opaque white
        }
    } else if (!on && !search) {
        if ($('.proposalResizeBlur').is(':visible')) {
            showNotificationMessage(false);
            showLoadingCog(false);
            $('.proposalResizeBlur').addClass('hidden');
            //$('.DTFC_ScrollWrapper').removeClass('scroll-fade-in'); //removed blur effect in preference for opaque white
        }
    }
}

function rebuildColumnFilters(force)
{
	var $thead = $('#forecastProposals > thead');
	$('.searchRow', $thead).remove();

	var $filterCell = $('> tr > th:first-child', $thead).html('');
	$filterCell.append($("<button>").css({
		'border-radius' : '33px'
	}).attr('class', 'btn btn-xs btn-block btn-default toggleColumnFilters').append($('<i class="fa fa-filter">'), $('<span class="columnFilterCount">')));

	if (force == true || countColumnFilters() > 0)
	{
		var $searchRow = $("<tr>").attr('class', 'searchRow');
		proposalTable.columns().every(function(i, tableLoop, columnLoop)
		{
			var dtColumn = this;
			if (dtColumn.visible())
			{
				var $cell = $("<th>");
				if (i > 0)
				{
					var colDef = tableColumnDefs[i];
					var $search = undefined;
					if (colDef.ciFilterType == 'text')
					{
						$search = $('<input type="text" class="form-control input-sm"/>');
						$search.val(dtColumn.search());
					}
					else if (colDef.ciFilterType == 'dropdown')
					{
						$search = $('<select class="form-control input-sm"/>');
						$search.append($("<option>").attr('value', '').attr('selected', 'selected').text('Filter...'));
						$.each(colDef.ciDataValues, function(i, e)
						{
							var $opt = $("<option>");
							$opt.text(e.label);
							$opt.attr('value', e.value);
							$search.append($opt);
						});

						$search.val(dtColumn.search());
					}
					else if (colDef.ciFilterType == 'clear')
					{
						$search = $("<button>").attr('class', 'btn btn-sm btn-danger clearColumnFilters columnFiltersEnabled').append($('<i class="fa fa-eraser">'), " Clear Filters");

						$search.val(dtColumn.search());
					}

					if ($search != undefined)
					{
						var label = $(dtColumn.header()).text();
						$search.attr('placeholder', 'Filter...');

						$cell.append($search);
					}
				}

				$searchRow.append($cell);
			}
		});
		$thead.append($searchRow);
	}

	var count = countColumnFilters();
	if (count > 0)
	{
		$(".columnFilterCount").text(count);
		$(".columnFiltersEnabled").show();
	}
	else
	{
		$(".columnFilterCount").text('');
		$(".columnFiltersEnabled").hide();
	}
}

function countColumnFilters()
{
	var count = 0;
	proposalTable.columns().every(function(i, tableLoop, columnLoop)
	{
		var dtColumn = this;
		if (dtColumn.visible() && dtColumn.search() != '')
		{
			++count;
		}
	});

	return count;
}

var redrawTableDelay = undefined;
function redrawTable()
{

	if (redrawTableDelay != undefined)
	{
		clearTimeout(redrawTableDelay);
	}
	redrawTableDelay = setTimeout(function()
	{
        proposalTable && proposalTable.draw('page');
	}, 300);

	$(".dashboardLoadingIndicator").stop().css({
		opacity : 1
	});
}

function getActiveTableRows()
{
	return proposalTable.$('tr', {
		"filter" : "applied"
	});
}

function updateForecastMonth(e)
{
	var $tr = $(this).closest("tr");
	var monthMs = $("option:selected", this).val();

	$.ajax({
		url : '/api/salesForecast/updateMonth',
		method : 'post',
		data : {
			proposalId : $tr.attr("data-id"),
			month : monthMs
		}
	}).done(function(data, textStatus, jqXHR)
	{
		if (data == 1)
			redrawTable();
	});
}

function updateForecastConfidence(e)
{
	var $tr = $(e.currentTarget).closest("tr");
	var confidence = e.currentTarget.value;

	$.ajax({
		url : '/api/salesForecast/updateConfidence',
		method : 'post',
		data : {
			proposalId : $tr.attr("data-id"),
			confidence : confidence
		}
	}).done(function(data, textStatus, jqXHR)
	{
		if (data == 1)
			redrawTable();
	});
}

// Return a map mapping millisecond keys to an int between 0-6
function getCssClassesForMonths()
{
	var time2css = new Map();
	var today = moment();
	today.set({
		'millisecond' : 0,
		'second' : 0,
		'minute' : 0,
		'hour' : 0,
		'date' : 1
	});

	time2css.set(0, "forecast-month-0");

	for (var i = 1; i <= 6; i++)
	{
		time2css.set(today.valueOf(), "forecast-month-" + i);
		today.add(1, 'months');
	}

	return time2css;
}

// Return a map mapping an int between 0-6 to the name of the month
function getMonthsFromClasses()
{
	var css2month = new Map();
	var today = moment();
	today.set({
		'millisecond' : 0,
		'second' : 0,
		'minute' : 0,
		'hour' : 0,
		'date' : 1
	});

	css2month.set(0, "Unassigned");

	for (var i = 1; i <= 6; i++)
	{
		css2month.set(i, today.format("MMMM"));
		today.add(1, 'months');
	}

	return css2month;
}

function displayProposalNotesAction(e)
{
	var proposalId = $(e.target).closest("tr").data("id");

	displayProposalNotesDialog(proposalId);
}

function displayProposalNotesDialog(proposalId)
{
	logger.info("Displaying proposal notes popup proposalId=" + proposalId);

	var $modal = $('#proposal-notes-modal');
	$('body').modalmanager('loading');

	$modal.load('/application/dialog/proposalNotes.jsp', {
		proposalId : proposalId
	}, function()
	{
		$modal.modal({
			width : '550px',
			backdrop : 'static',
			keyboard : false
		});
	});

	return $modal;
}
