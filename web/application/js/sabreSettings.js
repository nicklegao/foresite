$(document).ready(function() {

    $("#settingsContentArea").load("/application/include/sabreQueues.jsp", function(responseTxt, statusTxt, xhr){
        if(statusTxt == "error")
            alert("Error: " + xhr.status + ": " + xhr.statusText);
    });

    $("a.settingsMenu").click(function(){

        var url = $(this).data("url");
        var element = $(this);
        $("#settingsContentArea").html('');
        $("#settingsContentArea").load("/application/include/"+url, function(responseTxt, statusTxt, xhr){
            if(statusTxt == "success") {
                $(".settingsMenu").removeClass('active');
                $(element).addClass('active');
            }


            if(statusTxt == "error")
                alert("Error: " + xhr.status + ": " + xhr.statusText);
        });
    });

    $(document).on('click', "button#addQueue", function()
    {
        $.ajax({ type: "GET",
            url: "/application/include/sabreQueueCard.jsp",
            success : function(text)
            {
                var $card = $(text);
                var randomID = idGen.getId();

                $card = $card.attr('data-id', randomID);

                $('#sabre-queue-card-container').append($card);


                $('.collapse-card.card[data-id="'+randomID+'"]').paperCollapse();
                var $elems =  $('.collapse-card.card[data-id="'+randomID+'"]').find('.customZeroRiskAPI');
                $elems.attr('data-id', randomID);

                if (templates != null) {
                    for (var t in templates) {
                        if (templates.hasOwnProperty(t)) {
                            $('.collapse-card.card[data-id="'+randomID+'"] .traveldocTemplateSelector').append('<option value="' + t + '"> ' + templates[t] + '</option>');
                        }
                    }
                }

                if ( countries != null ) {

                    for (var t in countries) {
                        if (countries.hasOwnProperty(t)) {
                            $('.collapse-card.card[data-id="'+randomID+'"] #invalidCountrySelector').append('<option value="' + t + '"> ' + templates[t] + '</option>');
                            $('.collapse-card.card[data-id="'+randomID+'"] #queueHomeCountry').append('<option value="' + t + '"> ' + countries[t] + '</option>');
                        }
                    }
                }
                $('.collapse-card.card[data-id="'+randomID+'"] #queueHomeCountry').multiselect();
                $('.collapse-card.card[data-id="'+randomID+'"] #invalidCountrySelector').multiselect();

                var $elemsAutosend =  $('.collapse-card.card[data-id="'+randomID+'"]').find('.autosendQueueSwitch');
                $elemsAutosend.attr('data-id', randomID);
                var switchAutosend = document.querySelector('.autosendQueueSwitch[data-id="'+randomID+'"]');

                var switcheryAutosend = new Switchery(switchAutosend);


                $('.autosendQueueSwitch[data-id="'+randomID+'"]').on('change', function(e) {

                    var invalidCountrySelectorContainer = $($(this).closest('.collapse-card__body')).find('#invalidCountrySelectorContainer');
                    if (this.checked) {
                        $(this).addClass('active');
                        $(invalidCountrySelectorContainer).show();
                    } else {
                        $(this).removeClass('active');
                        $(invalidCountrySelectorContainer).hide();
                    }
                });
                $elemsAutosend.data('switchery', switcheryAutosend);


            }
        });

    });

    $(document).on('change', ".customZeroRiskAPI", function()
    {
        var body = $(this).closest('.collapse-card__body');
        var customInput = $(body).find('.custom-zerorisk-api-container');
        if ($(customInput).is(':visible')) {
            $(customInput).hide();
            $(customInput).find('input').val('');
        } else {
            $(customInput).show();
        }

    });

    $(document).on('click', "#saveSabreDetailsButton", function()
    {
        var valid = true;
        saveAuthForm();

    });


    $(document).on('change', "#queueName", function()
    {
        var val = $(this).val();
        var body = $(this).closest('.collapse-card__body');
        var header = $(body).prev();

        $(header).find('.queueNameLabel').html(val);

    });

    $(document).on('change', ".travelDocSabreChooser", function()
    {
        var val = $(this).val();
        if (val === "SAM") {
            $('#sabreClientID').val('');
            $('#sabreClientSecret').val('');
            $('.sabreSyncCol .sync-status').empty().hide();
            $('#sabre-queue-card-container').empty();
            $('.sabre-queue-card-modal-header').hide();
            $('#sabre-queue-card-modal').hide();
        } else {
            $('#sabreClientID').val('');
            $('#sabreClientSecret').val('');
            $('.sabre-queue-card-modal-header').show();
            $('#sabre-queue-card-modal').show();
        }

    });

    $(document).on('click', ".deleteQueueButton", function()
    {
        var card = $(this).closest('.collapse-card.card');
        $(card).remove();

    });
});

function saveAuthForm() {
    $.ajax({
        url : '/api/saveSabreAuthDetails',
        type : 'POST',
        data : generateAuthFormData(),
        beforeSend: function( xhr ) {
            console.log('BEFORE');
            // $('#linkVideoButton').html('<div class="spinner-border spinner-border-sm" role="status"><span class="sr-only">Loading...</span></div>');
        },
        success: function (data) {
            // your callback here
            console.log('DONE');
            alert("DONE");
        },
        error: function (error) {
            // handle error
            console.log('ERROR');
            alert("ERROR");
        }
    });
}

function getSabreQueues() {
    // if ()
    $.ajax({
        url : '/api/saveSabreAuthDetails',
        type : 'POST',
        data : generateAuthFormData(),
        beforeSend: function( xhr ) {
            console.log('BEFORE');
            // $('#linkVideoButton').html('<div class="spinner-border spinner-border-sm" role="status"><span class="sr-only">Loading...</span></div>');
        },
        success: function (data) {
            // your callback here
            console.log('DONE');
            alert("DONE");
        },
        error: function (error) {
            // handle error
            console.log('ERROR');
            alert("ERROR");
        }
    });
}

function generateAuthFormData() {
    var formData = {};
    formData.sabreApiType = $('.travelDocSabreChooser').val();

    if ($('#sabreClientID').val().trim() !== "") {
        formData.sabreApiKey = $('#sabreClientID').val().trim();
    }
    if ($('#sabreClientSecret').val().trim() !== "") {
        formData.sabreApisecret = $('#sabreClientSecret').val().trim();
    }
    if ($('#ZRAPIKey').val().trim() !== "") {
        formData.zeroRiskApiKey = $('#ZRAPIKey').val().trim();
    }

    return formData;
}

