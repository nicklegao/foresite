$(function(){
  $('.form-comments').validate({
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
      error.insertAfter(element);
    },
    ignore: '',
    rules: {
      comments: {
        required: true,
        minlength: 1,
        maxlength: 10000
      }
    },
    messages: {
      comments: {
        required: 'Comments cannot be empty!'
      }
    },
    highlight: function (element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    success: function (label, element) {
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    },
    submitHandler: function(form){
      startLoadingForeground();
      
      var $form = $(form);
      $.ajax({
        type: "POST",
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize()
      }).done(function(response){
        stopLoadingForeground();
        //alert(response);
        if (response != 'OK')
        {
          alert("Unable to send. Server message:\n  "+response+"\nPlease try again.");
        }
        
        $form.find('textarea').val('');
        $form.find('.has-success').removeClass('has-success');
        $form.find('.has-error').removeClass('has-error');
      });
      
      $("#comments.modal").modal('hide');
    }
  });
});