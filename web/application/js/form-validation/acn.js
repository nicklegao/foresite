function CheckACN(ACN)
{
	var acn_weights = new Array(8, 7, 6, 5, 4, 3, 2, 1);

	ACN = ACN.trim().split(' ').join('');

	if (ACN.length != 9)
	{
		return false;
	}

	var total = 0;

	for (var i = 0; i < acn_weights.length; i++)
	{
		var value = ACN.charAt(i);
		total += acn_weights[i] * value;
	}
	var remainder = total % 10;
	var complement = 10 - remainder;
	if(complement === 10) {
		complement = 0;
	}

	// console.log(complement);
	console.log("ACN" + (complement == ACN.charAt((ACN.length - 1))))
	return (complement == ACN.charAt((ACN.length - 1)));
}

$(document).ready(function()
{
	if ($.validator)
	{
		$.validator.addMethod("validACN", function(value, element)
		{
			return this.optional(element) || CheckACN(value);
		}, "Please enter a valid ACN.");
		
		$.validator.addMethod("validACN4AU", function(value, element)
		{
		  return this.optional(element) || ($('[name="country"]').val() != 'AU' && $('[name="country"]').val().toLowerCase() != 'australia') || CheckACN(value);
		}, "Please enter a valid ACN.");
	}
})
