$(function () {
    var $modal = $("#edit-labels").closest(".modal");

    $("a[href='#labels_tab']").click();

    $('a[data-toggle="tab"]', $modal).on('shown.bs.tab', function (e) {
        console.log("activated", e.target);
        console.log("previous activated", e.relatedTarget);
    });

    function serialize() {
        var data = {};
        data.type = "labels";
        // var $id = $("#" + tab + ".tab-pane", $modal);
        // data.type = tab;
        $(".tab-pane", $modal).each(function () {
            var inputs = $(".custom-label", $(this));
            inputs.each(function () {
                var key = $(this).data("key");
                data[key] = $(this).val();
            });
        });
        return JSON.stringify(data);
    }

    $('.form-edit-labels').validate({
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        ignore: '',
        rules: {},
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label, element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        submitHandler: function (form) {
            startLoadingForeground();
            var $form = $(form);
            $(form).ajaxSubmit({
                url: '/api/updateLabels',
                data: {
                    'labels': serialize()
                },
                success: function (data) {
                    if (data) {
                        $modal.modal('hide');
                        redrawTable();
                    }
                    else {
                        swal({
                            title: "Error",
                            text: "It wasn't possible to change the custom labels. Contact your Qcloud administrator and make sure you have permission to manage custom fields.",
                            type: "error"
                        });
                    }
                    stopLoadingForeground();
                },
                error: function () {
                    stopLoadingForeground();
                    alert("Unable to update your company custom labels. Please try again later.");
                }
            });
        }
    });
});
