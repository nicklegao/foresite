$(function()
{
	var $modal = $("#edit-team").closest(".modal");
	$('.form-edit-team').validate({
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error, element)
		{
			error.insertAfter(element);
		},
		ignore : '',
		rules : {
			teamName : {
				required : true,
			},
			travelportBranchCode : {
				remote : '/api/travelportBranchCodeValid?id=' + $('[name="id"]').val()
			}
		},
		messages : {
			travelportBranchCode : {
				remote : 'This branch code already exists.'
			}
		},
		highlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-error');
		},
		success : function(label, element)
		{
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		submitHandler : function(form)
		{
			$modal.modal('hide');
			startLoadingForeground();
			var $form = $(form);
			var data = buildFormData();
			$.ajax({
				url : '/api/updateTeam',
				data : data,
				success : function(data)
				{
					stopLoadingForeground();
					if (data.success === false)
					{
						swal({
							title : 'Error',
							type : 'error',
							text : data.message
						})
					}

                    $('#settingsMenuTeams').click();

				},
				error : function()
				{
					stopLoadingForeground();

					alert("Unable to update your team settings. Please try again later.");
				}
			});
		}
	});

	$('#proposalTemplate', $modal).select2();
	$('#itiStyles', $modal).select2();

});


var initMultiSelector = function(selector)
{
	selector.show();
	selector.multiselect().next().find('input.search').width('70%');
};


$(".input-multidrop").each(function()
{
	if ($(this).children("option").length > 20)
	{
		var selector = $(this).hide();
		initMultiSelector(selector);
		// $(this).hide()
	}
	else
	{
		initMultiSelector($(this));
	}
});

$("select#coverPageFiles").imagepicker({
	hide_select : true,
	show_label : false
});

var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

elems.forEach(function(html)
{
	var switchery = new Switchery(html, {
		size : 'small'
	});
	teamActiveSwitchLabel(html);
	html.onchange = function()
	{
		teamActiveSwitchLabel(html, $(this));
	};
});

function teamActiveSwitchLabel(html)
{
	var parent = $(html).closest('.tab-pane');
	if (html.checked)
	{
		$(parent).find('#multi-container').fadeIn("fast");
		$(parent).find('#folderHelperText').fadeIn("fast");
		$(parent).find('#multi-container-header').fadeIn("fast");
		$(html).siblings('.activeSwitchLabel').text("Restrictions Active: Place visible folders on the Left");
		$(html).siblings('.activeSwitchLabelCoverImages').text("Restrictions Active: Visible cover images are highlighted blue");
	}
	else
	{
		$(parent).find('#multi-container').fadeOut("fast");
		$(parent).find('#folderHelperText').fadeOut("fast");
		$(parent).find('#multi-container-header').fadeOut("fast");
		$(html).siblings('.activeSwitchLabel').text("There are no restrictions - all folders are visible");
		$(html).siblings('.activeSwitchLabelCoverImages').text("There are no restrictions - all folders are visible");
	}
}


function buildFormData()
{
	var params = {};
	$('#edit-team form').find('input, select').each(function()
	{
		var value = $(this).val();
		if ($(this).prop('type') == 'checkbox')
		{
			value = $(this).prop('checked');
		}
		params[$(this).prop('name')] = value;
	});
	return params;
}
