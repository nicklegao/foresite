function CheckABN(ABN)
{


	var abn_weights = new Array(10, 1, 3, 5, 7, 9, 11, 13, 15, 17, 19);

	ABN = ABN.trim().split(' ').join('');

	if (ABN.length != 11)
	{
		return false;
	}

	var total = 0;

	for (var i = 0; i < 11; i++)
	{
		var value = ABN.charAt(i);
		if (i === 0)
		{
			value = value - 1;
		}
		total += abn_weights[i] * value;
	}
	if (total == 0 || total % 89 != 0)
	{
		return false;
	}
	else
	{
		return true;
		console.log("valid abn");
	}
}

$(document).ready(function()
{
	if ($.validator)
	{
		$.validator.addMethod("validABN", function(value, element)
		{
			return this.optional(element) || CheckABN(value);
		}, "Please enter a valid ABN.");
		
		$.validator.addMethod("validABN4AU", function(value, element)
    {
      return this.optional(element) || ($('[name="country"]').val() != 'AU' && $('[name="country"]').val().toLowerCase() != 'australia') || CheckABN(value);
    }, "Please enter a valid ABN.");
	}
})
