$(function()
{
	var $modal = $("#account-settings");

	var yourRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/; // Email
	// address
	$("#payment-user-email-list", $modal).tagsInput({
		defaultText : 'Add a email',
		width : 'auto',
		minInputWidth : '390px',
		placeholderColor : '#858585',
		pattern : yourRegex
	});

	$('.form-account-settings').validate({
		rules : {
			companyName : {
				required : true
			},
			abnacn : {
				required : false,
				validABNorACN : true
			},
			addressLine1 : {
				required : true,
				maxlength : 100
			},
			addressLine2 : {
				maxlength : 100
			},
			citySuburb : {
				required : true,
				maxlength : 50
			},
			state : {
				required : true,
				maxlength : 50
			},
			postcode : {
				required : false,
				maxlength : 20
			}
		},
		submitHandler : function(form)
		{
			var $form = $(form);
			submitForm($form);
		}
	});

	function submitForm($form)
	{
		var data = retrieveSettingsObject($form);
		startLoadingForeground();
		$.ajax({
			url : '/api/updateAccountSettings',
			data : data,
			success : function(data)
			{
				stopLoadingForeground();
				if (data.success)
				{
					swal({
						title : "Success",
						text : "Your settings has been updated successfully.",
						type : "success"
					});
				}
				else
				{
					swal({
						title : "Error",
						text : data.message,
						type : "error"
					});
				}
			},
			error : function(data)
			{
				stopLoadingForeground();
				alert("Unable to update your company settings. Please try again later.");
			}
		});
	}

	function retrieveSettingsObject(element)
	{
		var settings = $(element).serializeObject();
		return settings;
	}

	$.fn.serializeObject = function()
	{
		var json = {};
		var array = this.serializeArray();
		$.each(array, function()
		{
			if (json[this.name] !== undefined)
			{
				if (!json[this.name].push)
				{
					json[this.name] = [ json[this.name] ];
				}
				json[this.name].push(this.value || '');
			}
			else
			{
				json[this.name] = this.value || '';
			}
		});
		return json;
	}

	if ($.validator)
	{
		$.validator.addMethod("validABNorACN", function(value, element)
		{
			console.log("test")
			return this.optional(element) || checkABNorACN(value);
		}, "Please enter a valid ABN/ACN.");
	}

	function checkABNorACN(value)
	{
		var value = value.toUpperCase();
		var formattedValue;
		if(value.indexOf("ACN") != -1)
		{
			formattedValue = value.replace("ACN", "");
			return CheckACN(formattedValue);
		}
		else if(value.indexOf("ABN") != -1)
		{
			formattedValue = value.replace("ABN", "");
			return CheckABN(formattedValue);
		}
		else
		{
			formattedValue = value.trim().split(' ').join('');
			if(formattedValue.length === 9) {
				return CheckACN(formattedValue);
			} else if(formattedValue.length === 11) {
				return CheckABN(formattedValue);
			}
		}
		return false;
	}

});
