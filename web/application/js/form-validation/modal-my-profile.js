$(function(){
  var $modal = $("#profile-management").closest(".modal");
  $modal.click();
// Shhh... Secret feature. Developers dont leak secrets ;)
//  $modal
//  .one('shown.bs.modal', function (e) {
//    CKEDITOR.replace($("#profile-signature")[0],{
//      height: 100
//    });
//  })
//  .one('hidden.bs.modal', function (e) {
//    CKEDITOR.instances["profile-signature"].destroy();
//  });

  $('.form-my-profile').validate({
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
      
    },
    submitHandler: function(form) {
      $modal.modal('hide');
      
      for(var instanceName in CKEDITOR.instances)
        CKEDITOR.instances[instanceName].updateElement();
      
      startLoadingForeground();
      $(form).ajaxSubmit({
        success : function(data){
          $(".firstNameDisplay").text($("#profile-name").val());
          $(".surnameDisplay").text($("#profile-surname").val());
          
          stopLoadingForeground();
        },
        error : function(){
          stopLoadingForeground();
          
          alert("Unable to update your profile. Please try again later.");
        }
      });
    },
    ignore: '',
    rules: {
      myName: {
        minlength: 2,
        maxlength: 50,
        required: true
      },
      mySurname: {
        minlength: 2,
        maxlength: 50,
        required: true
      },
      myMobile: {
        minlength: 2,
        maxlength: 50,
        required: true
      },
    },
    highlight: function (element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    success: function (label, element) {
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    }
  });

  $('#changeEmailButton').on('click', function() {
  	changePassword();
  })

});

function changePassword() {
	var $modal1 = $('#change-email-modal');
	$modal1.load('/application/dialog/profileEmailChange.jsp', function()
	{

		$modal1.modal({
			width : '660px',
			backdrop : 'static',
			keyboard : false
		});
	});
}