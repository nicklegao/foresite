$(function(){

	// var $modal = $("#profile-email-change").closest(".modal");


	$('.form-change-email').validate({
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function (error, element) {

		},
		submitHandler: function(form) {
			// $modal.modal('hide');


			startLoadingForeground();
			$(form).ajaxSubmit({
				url: '/api/updateAccountEmail',
				success : function(data){
					if (data === "true") {
						swal({
								title: "Successfully Saved",
								text: "Your windows will now be refresh remember to check your email to confirm the new password.",
								type: "success"
							},
							function(){
								location.reload();
							});
					} else if (data === "emailUsed") {
						swal("Email Change Failed", "That email address is already in use, please choice another", "error");
					} else {
						swal("Email Change Failed", "Looks like your password was incorrect...", "error");
					}
					stopLoadingForeground();
				},
				error : function(){
					stopLoadingForeground();

					swal("Email Change Failed", "Looks like your password was incorrect...", "error");
				}
			});
		},
		ignore: '',
		rules: {
			accountPassword: {
				minlength: 7,
				maxlength: 50,
				required: true
			},
			newEmail: {
				minlength: 7,
				maxlength: 50,
				email: true,
				required: true
			}
		},
		highlight: function (element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function (element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		success: function (label, element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		}
	});


});
