$(function()
{
	var $modal = $("#first-login").closest(".modal");
	
	var $tz = $('#timeZone', $modal);
	$(moment.tz.names()).each(function(){
		$tz.append($('<option>').attr('value', this).text(this));
	});
	$tz.val(moment.tz.guess());

	$('.form-settings-management', $modal).validate({
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error, element)
		{
			error.insertAfter(element);
		},
		ignore : '',
		rules : {
			locale : {
				required : true
			}
		},
		highlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-error');
		},
		success : function(label, element)
		{
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		submitHandler : function(form)
		{
			startLoadingForeground();
			var $form = $(form);
			var data = $form.serializeObject();
			console.log(data);
			$.ajax({
				url : '/api/updateFirstLoginSettings',
				data : data,
				success : function(data)
				{
					if (data)
					{
					  $('.form-steps', $modal).bootstrapWizard('show', 1);
					}
					else
					{
						swal({
							title : "Error",
							text : "It wasn't possible to change the settings. Contact your Qcloud administrator and make sure you have permission to manage settings.",
							type : "error"
						});
					}
					stopLoadingForeground();
				},
				error : function(data)
				{
					stopLoadingForeground();
					alert("Unable to update your company settings. Please try again later.");
				}
			});
		}
	});
	
});

$.fn.serializeObject = function()
{
  var json = {};
  var array = this.serializeArray();
  $.each(array, function()
  {
    if (json[this.name] !== undefined)
    {
      if (!json[this.name].push)
      {
        json[this.name] = [ json[this.name] ];
      }
      json[this.name].push(this.value || '');
    }
    else
    {
      json[this.name] = this.value || '';
    }
  });
  return json;
}

function submitFirstLoginForm1(){
  $('#first-login .form-settings-management').submit();
}

function submitFirstLoginForm2(){
  var $modal = $("#first-login").closest(".modal");
  var $form = $('#first-login .form-choose-sample-templates');
  if($form.length > 0){
    chooseSampleTemplates($form, function(){
      // $('.form-steps', $modal).bootstrapWizard('show', 2);
      $modal.modal('hide');
    });
    return;
  }
  // $('.form-steps', $modal).bootstrapWizard('show', 2);
  $modal.modal('hide');
  
}
