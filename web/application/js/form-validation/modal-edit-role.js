$(function()
{
	var $modal = $("#edit-role").closest(".modal");
	$('.form-edit-role', $modal).validate({
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error, element)
		{
			error.insertAfter(element);
		},
		ignore : '',
		rules : {
			roleName : {
				required : true,
			},
		},
		highlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-error');
		},
		success : function(label, element)
		{
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		submitHandler : function(form)
		{
			$modal.modal('hide');
			startLoadingForeground();
			var $form = $(form);
			var data = buildFormData();
			$.ajax({
				url : '/api/updateRole',
				data : data,
				success : function(data)
				{
					stopLoadingForeground();
					if (data.success === false)
					{
						swal({
							title : 'Error',
							type : 'error',
							text : data.message
						})
					}

                    $('#settingsMenuRoles').click();

				},
				error : function()
				{
					stopLoadingForeground();

					alert("Unable to update your role settings. Please try again later.");
				}
			});
		}
	});


	// $("#edit-role", $modal).iCheck({
	// 	checkboxClass : 'icheckbox_square-red',
	// 	radioClass : 'iradio_square-red'
	// })

	$('input[type="checkbox"]', $modal).each(function()
	{
		var check = $(this)[0];
		var attr = $(check).attr("data-switchery");
		if (typeof attr == typeof undefined || attr == false) {
			new Switchery($(this)[0], {
				color : '#64bd63',
				size : 'small'
			});
		}

	});

});

function buildFormData()
{
	var params = {};
	$('#edit-role form').find('input').each(function()
	{
		var value = $(this).val();
		if ($(this).prop('type') == 'checkbox')
		{
			value = $(this).prop('checked');
		}
		params[$(this).prop('name')] = value;
	})
	return params;
}
