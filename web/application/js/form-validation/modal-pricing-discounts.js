$(function()
{
	var $modal = $("#pricing-discounts").closest(".modal");

	$("[data-toggle='popover']", $modal).popover({
		container : 'body',
		content : 'Automatically apply this discount when the value of the proposal reaches this threshold.  For example, if the threshold is set to ' + currencySymbol + '1000, the value of a proposal must be greater than ' + currencySymbol + '1000 before the discount is applied.<br><br>The threshold discount is not applied to price line items that already have a discount applied to them.',
		html : true
	});

	var fieldCount = 0;

	$('#pricing-discounts', $modal).on('click', '.addDiscount', function(event)
	{
		insertNewDiscount();
	});
	$('#fields', $modal).on('click', '.delete', function(event)
	{
		$(event.target).closest('.row').remove();
	});
	$('#fields', $modal).on('click', '.dropdown-menu li a', function(event)
	{
		var group = $(this).closest('.input-group');
		group.find('.button-label').text($(this).text());
		group.find('input').attr('data-value', $(this).data('value'));
		if ($(this).data('value') === 'discount')
			group.find('input').attr("max", 100);
		else
			group.find('input').removeAttr("max");
	});

	$.ajax({
		url : "/api/loadDiscounts",
		dataType : "json"
	}).done(function(data, textStatus, jqXHR)
	{
		$.each(data, function(key, value)
		{
			insertNewDiscount(value);
		});
	});

	function insertNewDiscount(data)
	{
		var $clone = $('#fragments .row', $modal).clone();
		$('#fields', $modal).append($clone);
		$('.pricing-label', $clone).attr("name", "pricing-label-" + fieldCount);
		if (data)
		{
			$('.pricing-label', $clone).val(data.label);
			$('.pricing-percentage', $clone).val(Math.abs(data.percentage));
			$('.pricing-threshold', $clone).val(data.threshold);
			if (data.percentage <= 0)
				$('.dropdown-menu li a[data-value="discount"]', $clone).click();
			else
				$('.dropdown-menu li a[data-value="markup"]', $clone).click();
			$('.dropdown-menu li a[data-value="' + data.thresholdType + '"]', $clone).click();
		}
		fieldCount++;
		$('.pricing-label', $clone).focus();
	}

	function serialize()
	{
		var data = [];
		$('#fields .row', $modal).each(function()
		{
			var discount = {};
			discount.label = $('.pricing-label', $(this)).val();
			discount.percentage = parseFloat($('.pricing-percentage', $(this)).val());
			if ($('.pricing-percentage', $(this)).data('value') === 'discount')
				discount.percentage *= -1;
			discount.threshold = parseFloat($('.pricing-threshold', $(this)).val());
			discount.thresholdType = 'value';
			data.push(discount);
		});
		return JSON.stringify(data);
	}

	$('.form-pricing-discounts').validate({
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error, element)
		{
			error.appendTo($(element).closest('.form-group'));
		},
		ignore : '',
		rules : {
			'[name*="pricing-label-"]' : {
				required : true
			}
		},
		highlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-error');
		},
		success : function(label, element)
		{
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		submitHandler : function(form)
		{
			startLoadingForeground();
			var $form = $(form);
			$(form).ajaxSubmit({
				url : '/api/updateDiscounts',
				data : {
					'data' : serialize()
				},
				success : function(data)
				{
					if (data)
					{
						$modal.modal('hide');
						if (typeof redrawTable != 'undefined')
						{
							// happening in dashboard.
							redrawTable();
						}
						else if (typeof discounts != 'undefined' && typeof initDiscounts != 'undefined')
						{
							// happening in editor.
							$.ajax({
								url : "/api/loadDiscounts",
								dataType : "json"
							}).done(function(data, textStatus, jqXHR)
							{
								discounts = data;
								initDiscounts();
								rebuildDiscounts();
								initMarginMenu($("[id='margin-discounts']"));
								recalculatePricingTables();
							});
						}
					}
					else
					{
						swal({
							title : "Error",
							text : "It wasn't possible to change the discounts. Contact your Qcloud administrator and make sure you have permission to manage pricing discounts.",
							type : "error"
						});
					}
					stopLoadingForeground();
				},
				error : function()
				{
					stopLoadingForeground();

					alert("Unable to update your company pricing discounts. Please try again later.");
				}
			});
		}
	});

});
