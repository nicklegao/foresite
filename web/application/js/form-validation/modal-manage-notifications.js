$(function(){
  var $modal = $("#notification-management").closest(".modal");
  $modal.click();
  $modal
  .one('shown.bs.modal', function (e) {
  })
  .one('hidden.bs.modal', function (e) {
  });

	$modal.find('input[type="checkbox"]').each(function()
	{
		new Switchery($(this)[0], {
			color : '#64bd63',
			size : 'small'
		});

		$(this).on('click', function() {
			console.log("hello");
		})
	});
  
  $('.form-notification-management').ajaxForm({
    beforeSubmit : function(formData, jqForm, options){
      $modal.modal('hide');
      startLoadingForeground();
    },
    success : function(data){
      stopLoadingForeground();
    },
    error : function(){
      stopLoadingForeground();
      
      alert("Unable to update your notification preferences. Please try again later.");
    }
  });

});