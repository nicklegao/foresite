$(function()
{
	$(document).on("click", ".reloadRefreshAction", reloadRefreshAction);

	$('.plan-selector:not(.disabled)').click(function()
	{
		$(this).closest('.form-horizontal').find('.plan-selector').removeClass('active');
		$(this).addClass('active');
		$('[name="pricingPlan"]').val($(this).data('plan'));
	});

	$('.form-change-subscription').validate({
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error, element)
		{

		},
		submitHandler : function(form)
		{
			if ($('[name="pricingPlan"]').val() == $('[name="currentPlan"]').val())
			{
				// $('#change-subscription-modal.modal').modal('hide');
				return false;
			}
			$.ajax({
				type : "POST",
				url : '/api/stripe/need-setup-payment',
			}).done(function(data, textStatus, jqXHR)
			{
				if (data)
				{
					swal({
						title : "Please update your payment settings.",
						text : "We currently do not have a valid credit card to process your payments, either your credit card information is <u>missing</u> or your credit card details are <u>incorrect</u	>. <br/> Do you want to update your credit card information now?",
						type : "warning",
						html : true,
						showCancelButton : true,
						cancelButtonText : "No",
						confirmButtonText : "Yes",
						closeOnConfirm : true
					}, function(goSetup)
					{
						if (goSetup)
						{
							displayAccountSettingsAction();
						}
					});
				}
				else
				{
					changePlan(form);
				}
			});
		},
		ignore : '',
		rules : {
			pricingPlan : {
				required : true
			}
		},
		highlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-error');
		},
		success : function(label, element)
		{
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		}
	});

	$('#logoutBtn').click(function()
	{
		document.location.href = "/api/free/logout";
	})

	if ($('.plan-selector.current').length == 0)
	{
		$('.plan-selector:not(.disabled)').first().click();
	}
});

function changePlan1()
{
	changePlan($('.form-change-subscription'));
}

function changePlan(form)
{
	$('#change-subscription-modal.modal').modal('hide');
	startLoadingForeground();
	$(form).ajaxSubmit({
		success : function(data)
		{
			console.log(data);
			var message = data == 'success' ? "Your subscription plan has been successfully updated." : "You are almost there, we are processing your payment. As soon as your payment is finalised with Paypal your plan will automatically update.  Sometimes verification of payment approval is slightly delayed, so please be patient with us while we wait to hear from Paypal.";
			stopLoadingForeground();
			swal({
				title : "Thank you!",
				text : message,
				type : "success",
				html : true,
				closeOnConfirm : true
			}, function()
			{
				window.location.href = "/dashboard";
			});

			$(".form-change-subscription textarea").each(function(i, e)
			{
				$(e).val('');
			});
		},
		error : function()
		{
			stopLoadingForeground();
			alert("Unable to change your subscription. Please try again later.");
		}
	});
}

function reloadRefreshAction()
{
	location.reload(true);
}
