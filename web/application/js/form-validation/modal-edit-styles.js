$(document).ready(function()
{
	$("select").each(function()
	{
		$(this).css("font-family", $(this).val());
	});
	$("select").change(function()
	{
		$(this).css("font-family", $(this).val());
	});

	$('.color').spectrum({
		preferredFormat : "hex",
		replacerClassName : 'round-replacer',
		showInput : true,
		hide : function(tinycolor)
		{
			if ($(this).attr('id') == "palette-1")
			{
				$('.tableStyle .editorArea .titleRow td').css('color', tinycolor.toHexString());
			}

		}
	});
	$("#color-body").spectrum({
		preferredFormat : "hex",
		replacerClassName : 'round-replacer-small margin',
		showInput : true,
		hide : function(tinycolor)
		{
			$('.tableStyle .editorArea').css('color', tinycolor.toHexString());
		}
	});
	$("#color-header, #color-header1, #color-header2, #color-header3, #color-header-font, #color-header-background, #color-footer-font, #color-footer-background").spectrum({
		preferredFormat : "hex",
		replacerClassName : 'round-replacer-small margin',
		showInput : true,
		hide : function(tinycolor)
		{
			$('.tableStyle .editorArea').css('color', tinycolor.toHexString());
		}
	});

	$("#bottomStyleLeft, #bottomStyleCenter, #bottomStyleRight, #topStyleLeft, #topStyleCenter, #topStyleRight").on('change', function() {

		var elementID = $(this).attr('id');
		var elementVal = $(this).val();
		var dataID = '#' + $(this).attr("dataId");
		var nameID = $(this).attr("dataId");
		var positionId = $(this).attr("positionId");
		console.log(dataID);

		switch (elementVal) {
			case "none":
				blankHeaderSwitcher(elementID);
				$(dataID).empty();
				break;
			case "image":
				imageHeaderSwitcher(elementID);

				$(dataID).empty();
				$(dataID).append('<div class="row"><section class="logo-selector"><input class="header-upload" id="' + nameID + '-image"  name="' + nameID + '-image" type="file" /></section><input id="removeheaderImage" type="checkbox" name="removeheaderImage" class="hidden">');
				$('.header-upload').fileinput({
				showZoom: false,
					overwriteInitial: true,
				browseClass: "btn btn-primary",
				theme:"fa",
				showClose: false,
				showCaption: false,
				showUpload: false

		}).on('change', function(event) {
			$("#removeheaderImage").prop('checked',false);
		}).on('filecleared', function(event) {
			$("#removeheaderImage").prop('checked',true);
		});
				break;
			case "text":
				otherHeaderSwitcher(elementID);
				$(dataID).empty();
				$(dataID).append('<div class="col-sm-1 f-field"></div><div class="col-sm-10 f-field"><h5>' + positionId + ' Text</h5><input type="text" id="' + nameID + '-text" class="form-control" name="' + nameID + '-text" placeholder="Content text"  value=""></div><div class="col-sm-1 f-field"></div>');
				break;
			case "page":
				otherHeaderSwitcher(elementID);
				$(dataID).empty();
				break;
			default:
				break;
		}

	});

	$('.header-upload').each(function(e) {
		$(this).fileinput({
		initialPreview: [
		'<img src="' + $(this).attr("data-id") + '" class="file-preview-image" alt="Logo" title="Logo">'
		],
		showZoom: false,
			overwriteInitial: true,
			browseClass: "btn btn-primary",
			theme:"fa",
			showClose: false,
			showCaption: false,
			showUpload: false

	}).on('change', function(event) {
			$("#removeheaderImage").prop('checked',false);
		}).on('filecleared', function(event) {
			$("#removeheaderImage").prop('checked',true);
		});
	});



	$("#cover-color").spectrum({
		preferredFormat : "hex",
		replacerClassName : 'round-replacer-small',
		showInput : true,
		hide : function(tinycolor)
		{
			$('.tableStyle .editorArea').css('color', tinycolor.toHexString());
		}
	});

	$('.table-radio').iCheck({
		checkboxClass : 'icheckbox_square-orange',
		radioClass : 'iradio_square-orange'
	});

    $('.padding-wrapper input').on('keyup change', function () {
        var leftPadding = $('[name="contentLeftPadding"]').val();
        var topPadding = $('[name="contentTopPadding"]').val();
        var rightPadding = $('[name="contentRightPadding"]').val();
        var bottomPadding = $('[name="contentBottomPadding"]').val();

        $('.padding-demo').css('padding-left', Number(leftPadding)).css('padding-right', Number(rightPadding)).css('padding-top', Number(topPadding)).css('padding-bottom', Number(bottomPadding));
    }).trigger('keyup');

	$('.table-checkbox[name="coverPageProposalInfo"], .table-checkbox[name="coverPageSenderInfo"], .table-checkbox[name="uppercaseSectionTitle"], .table-checkbox[name="underlineSectionTitle"], .table-checkbox[name="boldSectionTitle"], ' +
		'.table-checkbox[name="uppercaseHeading1"], .table-checkbox[name="underlineHeading1"], .table-checkbox[name="boldHeading1"], ' +
		'.table-checkbox[name="uppercaseHeading2"], .table-checkbox[name="underlineHeading2"], .table-checkbox[name="boldHeading2"],' +
		'.table-checkbox[name="uppercaseHeading3"], .table-checkbox[name="underlineHeading3"], .table-checkbox[name="boldHeading3"], .table-checkbox[name="topHeaderActive"], .table-checkbox[name="bottomHeaderActive"]').each(function()
	{
		new Switchery($(this)[0], {
			color : '#64bd63',
			size : 'small'
		});
		// $(this).change(function(e)
		// {
		// });
	});

	if ($('.table-checkbox[name="coverPageBoxBackground"]').prop('checked')) {
		$('#coverpage .cover-field').removeClass('hidden');
	} else {
		$('#coverpage .cover-field').addClass('hidden');
	}

	$('.table-checkbox[name="coverPageBoxBackground"]').each(function()
	{
		new Switchery($(this)[0], {
			color : '#64bd63',
			size : 'small'
		});
		$(this).change(function(e)
		{
			if ($(this).is(':checked'))
			{
				$('#coverpage .cover-field').removeClass('hidden');
			} else {
				$('#coverpage .cover-field').addClass('hidden');
			}
		});
	});

	if ($('.table-checkbox[name="customSectionColor"]').prop('checked')) {
		$('.font-section-color-title').removeAttr('style');
		$('.fontColorSectionTitleSection').removeAttr('style');
	} else {
		$('.font-section-color-title').attr('style', 'display:none;');
		$('.fontColorSectionTitleSection').attr('style', 'display:none;');
	}

	if ($('.table-checkbox[name="customHeading1Color"]').prop('checked')) {
		$('.font-section-color-title-h1').removeAttr('style');
		$('.fontColorHeading1').removeAttr('style');
	} else {
		$('.font-section-color-title-h1').attr('style', 'display:none;');
		$('.fontColorHeading1').attr('style', 'display:none;');
	}

	if ($('.table-checkbox[name="customHeading2Color"]').prop('checked')) {
		$('.font-section-color-title-h2').removeAttr('style');
		$('.fontColorHeading2').removeAttr('style');
	} else {
		$('.font-section-color-title-h2').attr('style', 'display:none;');
		$('.fontColorHeading2').attr('style', 'display:none;');
	}

	if ($('.table-checkbox[name="customHeading3Color"]').prop('checked')) {
		$('.font-section-color-title-h3').removeAttr('style');
		$('.fontColorHeading3').removeAttr('style');
	} else {
		$('.font-section-color-title-h3').attr('style', 'display:none;');
		$('.fontColorHeading3').attr('style', 'display:none;');
	}

	$('.table-checkbox[name="customSectionColor"]').each(function()
	{
		new Switchery($(this)[0], {
			color : '#64bd63',
			size : 'small'
		});
		$(this).change(function(e)
		{
			if (!($(this).is(':checked')))
			{
				$('.font-section-color-title').attr('style', 'display:none;');
				$('.fontColorSectionTitleSection').attr('style', 'display:none;');
			} else {
				$('.font-section-color-title').removeAttr('style');
				$('.fontColorSectionTitleSection').removeAttr('style');
			}
		});
	});

	$('.table-checkbox[name="customHeading1Color"]').each(function()
	{
		new Switchery($(this)[0], {
			color : '#64bd63',
			size : 'small'
		});
		$(this).change(function(e)
		{
			if (!($(this).is(':checked')))
			{
				$('.font-section-color-title-h1').attr('style', 'display:none;');
				$('.fontColorHeading1').attr('style', 'display:none;');
			} else {
				$('.font-section-color-title-h1').removeAttr('style');
				$('.fontColorHeading1').removeAttr('style');
			}
		});
	});

	$('.table-checkbox[name="customHeading2Color"]').each(function()
	{
		new Switchery($(this)[0], {
			color : '#64bd63',
			size : 'small'
		});
		$(this).change(function(e)
		{
			if (!($(this).is(':checked')))
			{
				$('.font-section-color-title-h2').attr('style', 'display:none;');
				$('.fontColorHeading2').attr('style', 'display:none;');
			} else {
				$('.font-section-color-title-h2').removeAttr('style');
				$('.fontColorHeading2').removeAttr('style');
			}
		});
	});

	$('.table-checkbox[name="customHeading3Color"]').each(function()
	{
		new Switchery($(this)[0], {
			color : '#64bd63',
			size : 'small'
		});
		$(this).change(function(e)
		{
			if (!($(this).is(':checked')))
			{
				$('.font-section-color-title-h3').attr('style', 'display:none;');
				$('.fontColorHeading3').attr('style', 'display:none;');
			} else {
				$('.font-section-color-title-h3').removeAttr('style');
				$('.fontColorHeading3').removeAttr('style');
			}
		});
	});


	// $('.table-checkbox[name="coverPageBoxBackground"]').iCheck({
	// 	checkboxClass : 'icheckbox_square-orange',
	// 	radioClass : 'iradio_square-orange'
	// }).on('ifChecked', function()
	// {
	// 	$('#coverpage .cover-field').removeClass('hidden');
	// }).on('ifUnchecked', function()
	// {
	// 	$('#coverpage .cover-field').addClass('hidden');
	// });
	//

	var tabs = Array.prototype.slice.call(document.querySelectorAll('.tab-switch'));
	tabs.forEach(function(html)
	{
		var switchery = new Switchery(html, {
			size : 'small'
		});
		html.onchange = function()
		{
			var div = $(this).attr('data-id');
			$(div).toggleClass('tab-content-disabled', !this.checked);
		};
	});


});

//$("#bottomStyleLeft, #bottomStyleCenter, #bottomStyleRight, #topStyleLeft, #topStyleCenter, #topStyleRight")

function imageHeaderSwitcher(elementID) {
	switch (elementID) {
		case "topStyleLeft":
			$('#topStyleCenter').prop('disabled', 'disabled');
			$("#topStyleRight > option").each(function() {
				if (this.value !== "image" && this.value !== "none") {
					$(this).prop('disabled', 'disabled');
				}
			});
			if ($('#topStyleRight').val() !== "image" && $('#topStyleRight').val() !== "none") {
				$("#topStyleRight").val("none");
				$("#topRightData").empty();
			}
			$("#topStyleCenter").val("none");
			$("#topCenterData").empty();
			break;
		case "topStyleRight":
			$('#topStyleCenter').prop('disabled', 'disabled');
			$("#topStyleLeft > option").each(function() {
				if (this.value !== "image" && this.value !== "none") {
					$(this).prop('disabled', 'disabled');
				}
			});
			if ($('#topStyleLeft').val() !== "image" && $('#topStyleLeft').val() !== "none") {
				$("#topStyleLeft").val("none");
				$("#topLeftData").empty();
			}
			$("#topStyleCenter").val("none");
			$("#topCenterData").empty();
			break;
		case "bottomStyleLeft":
			$('#bottomStyleCenter').prop('disabled', 'disabled');
			$("#bottomStyleRight > option").each(function() {
				if (this.value !== "image" && this.value !== "none") {
					$(this).prop('disabled', 'disabled');
				}
			});
			if ($('#bottomStyleRight').val() !== "image" && $('#bottomStyleRight').val() !== "none") {
				$("#bottomStyleRight").val("none");
				$("#bottomRightData").empty();
			}
			$("#bottomStyleCenter").val("none");
			$("#bottomCenterData").empty();
			break;
		case "bottomStyleRight":
			$('#bottomStyleCenter').prop('disabled', 'disabled');
			$("#bottomStyleLeft > option").each(function() {
				if (this.value !== "image" && this.value !== "none") {
					$(this).prop('disabled', 'disabled');
				}
			});
			if ($('#bottomStyleLeft').val() !== "image" && $('#bottomStyleLeft').val() !== "none") {
				$("#bottomStyleLeft").val("none");
				$("#bottomLeftData").empty();
			}
			$("#bottomStyleCenter").val("none");
			$("#bottomCenterData").empty();
			break;
		default:
			console.log("ISSUE HERE");
			break;
	}
}

function blankHeaderSwitcher(elementID) {
	switch (elementID) {
		case "topStyleLeft":
			if ($("#topStyleRight").val() !== "image") {
				$('#topStyleCenter').prop('disabled', false);
			}
			$("#topStyleRight > option").each(function() {
				$(this).prop('disabled', false);
			});
			break;
		case "topStyleRight":
			if ($("#topStyleLeft").val() !== "image") {
				$('#topStyleCenter').prop('disabled', false);
				$("#topStyleLeft > option").each(function() {
					$(this).prop('disabled', false);
				});
			}
			$("#topStyleLeft > option").each(function() {
				$(this).prop('disabled', false);
			});
			break;
		case "bottomStyleLeft":
			if ($("#bottomStyleRight").val() !== "image") {
				$('#topStyleCenter').prop('disabled', false);
			}
			$("#bottomStyleRight > option").each(function() {
				$(this).prop('disabled', false);
			});
			break;
		case "bottomStyleRight":
			if ($("#bottomStyleLeft").val() !== "image") {
				$('#topStyleCenter').prop('disabled', false);
			}
			$("#bottomStyleLeft > option").each(function() {
				$(this).prop('disabled', false);
			});
			break;
		default:
			console.log("ISSUE HERE");
			break;
	}
}

function otherHeaderSwitcher(elementID) {
	switch (elementID) {
		case "topStyleLeft":
			$('#topStyleCenter').prop('disabled', false);
			$("#topStyleRight > option").each(function() {
				$(this).prop('disabled', false);
			});
			break;
		case "topStyleRight":
			$('#topStyleCenter').prop('disabled', false);
			$("#topStyleLeft > option").each(function() {
				$(this).prop('disabled', false);
			});
			break;
		case "bottomStyleLeft":
			$('#bottomStyleCenter').prop('disabled', false);
			$("#bottomStyleRight > option").each(function() {
				$(this).prop('disabled', false);
			});
			break;
		case "bottomStyleRight":
			$('#bottomStyleCenter').prop('disabled', false);
			$("#bottomStyleLeft > option").each(function() {
				$(this).prop('disabled', false);
			});
			break;
		default:
			console.log("ISSUE HERE");
			break;
	}
}


$(function()
{
	var $modal = $("#edit-styles").closest(".modal");
	$('.form-company-styles').validate({
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error, element)
		{
			error.insertAfter(element);
		},
		ignore : '',
		rules : {
			fontFamilyBody : {
				required : true,
			},
			fontSizeBody : {
				required : true,
			},
			fontFamilyH1 : {
				required : true,
			},
			fontSizeH1 : {
				required : true,
			},
			fontFamilyH2 : {
				required : true,
			},
			fontSizeH2 : {
				required : true,
			},
			fontFamilyH3 : {
				required : true,
			},
			fontSizeH3 : {
				required : true,
			},
			palette1 : {
				required : true,
			},
			palette2 : {
				required : true,
			},
			palette3 : {
				required : true,
			},
			palette4 : {
				required : true,
			},
		},
		highlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-error');
		},
		success : function(label, element)
		{
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		submitHandler : function(form)
		{
			$modal.modal('hide');
			startLoadingForeground();
			var $form = $(form);
			$(form).ajaxSubmit({
				url : '/api/updateStyles',
				success : function(data)
				{
					stopLoadingForeground();
					location.reload();
				},
				error : function()
				{
					stopLoadingForeground();

					alert("Unable to update your style settings. Please try again later.");
				}
			});
		}
	});
});
