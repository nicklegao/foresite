$(function(){
  $('.form-handover-proposal').validate({
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
      
    },
    submitHandler: function(form) {
      startLoadingForeground();
      
      $(form).ajaxSubmit({
        success : function(success){
          stopLoadingForeground(); 

          if (success)
          {
            $('#handover-email-address').val('');
            $(document).one('foregroundLoadingComplete', function(){
            	$('#proposalHandoverModal').one('hidden.bs.modal', function(){
            		closeProposalManagementDialog();
            	}).modal('hide');
            });
          }
          else
          {
           alert("Unable to transfer ownership. Please check the email and try again.");
          }
        },
        error : function(){
          stopLoadingForeground();
          
          alert("Encountered internal server transfering ownership. Please try again later.");
        }
      });
    },
    ignore: '',
    rules: {
      newUserId: {
        required: true
      },
      message:{
    	  required: false
      }
    },
    highlight: function (element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    success: function (label, element) {
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    }
  });
});