var MIN_PASSWORD_LENGTH = 8;

jQuery.validator.addMethod( "passwordStrength",
  function(value, element){
    var digit=new RegExp('[0-9]');
    if (!digit.test(value))
    {
      return false;
    }
    var lowercase=new RegExp('[a-z]');
    if (!lowercase.test(value))
    {
      return false;
    }
    var uppercase=new RegExp('[A-Z]');
    if (!uppercase.test(value))
    {
      return false;
    }
    
    return true;
  }, "Password must be atleast "+MIN_PASSWORD_LENGTH+" characters, contain uppercase, lowercase and a number" );

$(function(){
  $('.form-change-password').validate({
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
      
    },
    ignore: '',
    rules: {
      currentPassword: {
        required: true
      },
      newPassword: {
        passwordStrength: true,
        required: true
      },
      repeatPassword: {
        equalTo: '#newPassword',
        required: true
      }
    },
    highlight: function (element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    success: function (label, element) {
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    }
  });
});