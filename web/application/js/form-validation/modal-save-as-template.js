$(function(){
  $('.form-save-as-template').validate({
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
      
    },
    submitHandler: function(form) {
      startLoadingForeground();
      
      $(form).ajaxSubmit({
        success : function(success){
          stopLoadingForeground(); 

          if (success)
          {
            $('#template-title').val('');
            $('#template-description').val('');
            $(document).one('foregroundLoadingComplete', function(){
            	$('#saveAsTemplateModal').one('hidden.bs.modal', function(){
            		reloadProposalManagementDialog();
            	}).modal('hide');
            });
          }
          else
          {
           alert("Unable to create the template. Please try again.");
          }
        },
        error : function(){
          stopLoadingForeground();
          
          alert("Encountered internal error saving the template. Please try again later.");
        }
      });
    },
    ignore: '',
    rules: {
      templateTitle: {
    	  minlength: 2,
          maxlength: 50,
    	  required: true
      },
      templateDescription:{
    	  minlength: 2,
          maxlength: 255,
          required: true
      },
    },
    highlight: function (element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    success: function (label, element) {
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    }
  });
});