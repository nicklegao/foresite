$(function()
{
	var $modal = $("#settings-management").closest(".modal");

	// $("input[type='checkbox']").iCheck({
	// checkboxClass : 'icheckbox_square-orange',
	// radioClass : 'iradio_square-orange'
	// });

	var fieldCount = 0;

	var $tz = $('#timeZone', $modal);
	$(moment.tz.names()).each(function()
	{
		$tz.append($('<option>').attr('value', this).text(this));
	});
	$tz.val($('#timeZoneInput', $modal).val() ? $('#timeZoneInput', $modal).val() : moment.tz.guess());


	$modal.one('shown.bs.modal', function()
	{

		var uninitialisedFroalaEditors = $modal.find(".panel-froala");
		logger.debug('initFroalaEditor... Found ' + uninitialisedFroalaEditors.length + " editors");
		$.each(uninitialisedFroalaEditors, function(i, e)
		{
			if (!$(e).data('froala.editor'))
			{
				$(e).on('froalaEditor.initialized', function(ev, editor)
				{
					// customizeFroalaEditor(ev, editor)
				}).froalaEditor({
					enter : $.FroalaEditor.ENTER_P,
					key : 'vYA6mE5C5G4A3jC1QUd1Xd1OZJ1ABVJRDRNGGUE1ITrE1D4A3C11B1B6E6B1F4I3=='
				});
			}
		})

	})


	$('.form-settings-management', $modal).on('click', '.addStatusReason', function(event)
	{
		insertNewReason();
	});


	$('#fields', $modal).on('click', '.delete', function(event)
	{
		$(event.target).closest('.row').remove();
	});

	function insertNewReason(data)
	{
		var $clone = $('#fragments .row', $modal).clone();
		$('#fields', $modal).append($clone);
		$('.reason-label', $clone).attr("name", "reason-label-" + fieldCount);
		if (data)
		{
			$('.reason-label', $clone).val(data.reason);
		}
		fieldCount++;
		$('.reason-label', $clone).focus();
	}


	$('#subjectLine').on('keyup change', function()
	{
		if ($(this).val() == "other")
		{
			$('#otherSubjectText').show();
		}
		else
		{
			$('#otherSubjectText').hide();
		}
	});

	if (!($('#enableTax').prop('checked')))
	{
		$('#taxLabelSection').attr('style', 'display:none;');
		$('#taxPercentageSection').attr('style', 'display:none;');
	}
	else
	{
		$('#taxLabelSection').removeAttr('style');
		$('#taxPercentageSection').removeAttr('style');
	}

	$('#estimatedValue, #roundingValue, #enableTax, #hideTermsAlways, #enableAddition, #enableTeamEditing, #encryptClientData, #enableEncryptedDataSync').each(function()
	{
		var color = '#64bd63';
		if ($(this).attr("id") == "hideTermsAlways")
		{
			color = '#fc0d00';
		}
		if ($(this).attr("id") == "enableTax")
		{
			$(this).change(function(e)
			{
				if (!($(this).is(':checked')))
				{
					$('#taxLabelSection').attr('style', 'display:none;');
					$('#taxPercentageSection').attr('style', 'display:none;');
				}
				else
				{
					$('#taxLabelSection').removeAttr('style');
					$('#taxPercentageSection').removeAttr('style');
				}
			});
		}
		new Switchery($(this)[0], {
			color : color,
			size : 'small'
		});

	});


	$.ajax({
		url : "/api/loadStatusReasons",
		dataType : "json"
	}).done(function(data, textStatus, jqXHR)
	{
		$.each(data, function(key, value)
		{
			insertNewReason(value);
		});
	});

	$('button#submitButton').on('click', function(e)
	{
		e.preventDefault();

		if (($('#enableTax').prop('checked')))
		{
			var taxLabel = $('input#taxLabel').val();
			var taxPercentage = $('input#taxPercentage').val();

			if (taxLabel == "" && !($("#taxLabel-help-block").length))
			{
				$('input#taxLabel').addClass("pulse-button-help");
				$('input#taxLabel').after("<span for=\"taxLabel\" id=\"taxLabel-help-block\" class=\"help-block\">This field is required.</span>");
			}
			if (taxPercentage == "" && !($("#taxPercentage-help-block").length))
			{
				$('input#taxPercentage').addClass("pulse-button-help");
				$('input#taxPercentage').after("<span for=\"taxPercentage\" id=\"taxPercentage-help-block\" class=\"help-block\">This field is required.</span>");
			}
			if (taxPercentage == "" || taxLabel == "")
			{
				return;
			}
		}

		$('.form-settings-management').submit();
	});

	$('input#taxPercentage').on('change', function(e)
	{
		if ($(this).hasClass("pulse-button-help"))
		{
			$(this).removeClass("pulse-button-help");
			$('span#taxPercentage-help-block').remove();
		}
	});

	$('input#taxLabel').on('change', function(e)
	{
		if ($(this).hasClass("pulse-button-help"))
		{
			$(this).removeClass("pulse-button-help");
			$('span#taxLabel-help-block').remove();
		}
	});

	$('.form-settings-management').validate({
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error, element)
		{
			// error.insertAfter(element);

		},
		ignore : '',
		rules : {
			taxLabel : {
				required : "#enableTax:checked"
			},
			taxPercentage : {
				required : "#enableTax:checked",
				number : true
			}
		},
		highlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-error');
		},
		success : function(label, element)
		{
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		submitHandler : function(form)
		{
			if (validateEmailTemplates())
			{
				startLoadingForeground();
				var $form = $(form);
				var data = retrieveSettingsObject($form);
				var passExpMonths = data.passwordExpiryDate;
				$.ajax({
					url : '/api/updateSettings',
					data : data,
					success : function(data)
					{
						if (data)
						{
							$modal.modal('hide');
							redrawTable();
							$('#changePassMonths').text(passExpMonths);
							location.reload();
						}
						else
						{
							swal({
								title : "Error",
								text : "It wasn't possible to change the settings. Contact your Qcloud administrator and make sure you have permission to manage settings.",
								type : "error"
							});
						}
						stopLoadingForeground();
					},
					error : function(data)
					{
						stopLoadingForeground();

						alert("Unable to update your company settings. Please try again later.");
					}
				});
			}
		}
	});
});

var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

elems.forEach(function(html)
{
	var switchery = new Switchery(html, {
		size : 'small'
	});
	maestranoSyncActiveSwitchLabel(html);
	html.onchange = function()
	{
		maestranoSyncActiveSwitchLabel(html, $(this));
	};
});

function maestranoSyncActiveSwitchLabel(html)
{
	var parent = $(html).closest('.tab-pane');
	if (html.checked)
	{
		$(html).siblings('.activeSwitchLabel').text("Restrictions Active: Place visible folders on the Left");
	}
	else
	{
		$(html).siblings('.activeSwitchLabel').text("There are no restrictions - all folders are visible");
	}
}

function retrieveSettingsObject(element)
{
	var settings = $(element).serializeObject();

	// remove ck
	// settings["readyEmailTemplate"] =
	// CKEDITOR.instances['email-template-ready'].getData();
	// settings["revisionEmailTemplate"] =
	// CKEDITOR.instances['email-template-revision'].getData();

	settings["readyEmailTemplate"] = $('#email-template-ready').froalaEditor('html.get').replace(/\<br>/g, '<br/>');
	settings["revisionEmailTemplate"] = $('#email-template-revision').froalaEditor('html.get').replace(/\<br>/g, '<br/>');
	// settings["pricingInfoText"] =
	// CKEDITOR.instances['static-pricing-info-text'].getData();
	// settings["wantMoreDetail"] =
	// CKEDITOR.instances['static-want-more-detail'].getData();

	var data = [];
	$('#fields .customStatus').each(function()
	{
		var reason = {};
		reason.reason = $('.reason-label', $(this)).val();
		data.push(reason);
	});
	settings["statusReasonJSON"] = JSON.stringify(data);
	return settings;
}

$.fn.serializeObject = function()
{
	var json = {};
	var array = this.serializeArray();
	$.each(array, function()
	{
		if (json[this.name] !== undefined)
		{
			if (!json[this.name].push)
			{
				json[this.name] = [ json[this.name] ];
			}
			json[this.name].push(this.value || '');
		}
		else
		{
			json[this.name] = this.value || '';
		}
	});
	return json;
}

function validateEmailTemplates()
{
	var valid = true;
	$('.email-template-content').each(function(i, e)
	{
		var html = $(e).html();
		if (!html.indexOf('${proposalLink}') && !html.indexOf('${serverBase}/view?${vParams}'))
		{
			valid = false;
			var errorElement = '<span class="help-block">Please include a link to your proposal in your email template</span>';
			$(e).parent().append(errorElement);
			$(e).parent().addClass('has-error');
		}
		else
		{
			$(e).parent().remove('.help-block');
			$(e).parent().removeClass('has-error');
		}
	});
	return valid;
}
