$(function(){
  $('.form-email-support #closeForm').click(function(event){
    $(".form-email-support textarea").each(function(i, e){
      $(e).val('');
    });
  });
  
  $('.form-email-support').validate({
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
      
    },
    submitHandler: function(form) {
      $('#email-support.modal').modal('hide');
      
      startLoadingForeground();
      $(form).ajaxSubmit({
        success : function(data){
          
          
          $(document).one('foregroundLoadingComplete', function(){
            $('body').bsAlert({
              title:"Success",
              message:"Your message has been sent to our support team.",
              icon:$("<i>").attr("class", 'fa fa-check'),
              buttons: [{
                text:"Close",
                icon:$("<i>").attr("class", 'fa fa-times'),
                buttonClass:"btn btn-danger btn-secondary"
              }]
            });
            
            $(".form-email-support textarea").each(function(i, e){
            	$(e).val('');
            });
          });

          stopLoadingForeground();
        },
        error : function(){
          stopLoadingForeground();
          
          alert("Unable to send your message to our support. Please try again later.");
        }
      });
    },
    ignore: '',
    rules: {
      message: {
        required: true
      }
    },
    highlight: function (element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    success: function (label, element) {
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    }
  });
});