$(function()
{
	var $modal = $("#custom-fields").closest(".modal");

	$(".custom-value").tagsInput({
		defaultText : 'Type values here',
		width : '100%',
		height : '34px',
		delimiter : '|',
		placeholderColor : '#858585'
	});

	$("a[href='#proposals_tab']").click();

	$('a[data-toggle="tab"]', $modal).on('shown.bs.tab', function(e)
	{
		console.log("activated", e.target);
		console.log("previous activated", e.relatedTarget);
	});

	loadCustomFields("proposals").done(function(data, textStatus, jqXHR)
	{
		createCustomFields(data);
		loadCustomFields("products").done(function(productsData, textStatus, jqXHR)
		{
			createCustomFields(productsData);
			$("#saveForm").removeClass("disabled");
		});
	});

	function loadCustomFields(type)
	{
		var param = "";
		if (type)
		{
			param = "?type=" + type;
		}

		return $.ajax({
			url : "/api/loadCustomFields" + param,
			dataType : "json"
		});
	}

	function createCustomFields(data)
	{
		var tab = "#" + data.type;
		$.each(data, function(key, value)
		{

			if (!value.on)
			{
				disableFields(value.id, tab);
			}
			else
			{
				$(tab + " .custom-label." + value.id, $modal).val(value.label);
				$(tab + " .custom-type." + value.id, $modal).val(value.type);
				$(tab + " .custom-value." + value.id, $modal).importTags(value.values || "");
				$(tab + " .custom-required." + value.id, $modal).prop("checked", value.required);
				enableFields(value.id, tab);
			}
			$(tab + " .custom-on." + value.id, $modal).iCheck({
				checkboxClass : 'icheckbox_square-orange',
				radioClass : 'iradio_square-orange'
			}).on('ifChanged', function()
			{
				if (this.checked)
				{
					enableFields(value.id, tab);
				}
				else
				{
					disableFields(value.id, tab);
				}
			});
			$(tab + " .custom-required." + value.id, $modal).iCheck({
				checkboxClass : 'icheckbox_square-orange',
				radioClass : 'iradio_square-orange'
			});
			$(tab + " select." + value.id, $modal).change(function()
			{
				enableValues(value.id, tab);
			});
		});
	}

	function disableFields(key, tab)
	{
		$(tab + " .custom-label." + key, $modal).prop("disabled", true);
		$(tab + " .custom-type." + key, $modal).prop("disabled", true);
		enableValues(key, tab);
		$(tab + " .custom-on." + key, $modal).prop("checked", false);
	}

	function enableFields(key, tab)
	{
		$(tab + " .custom-label." + key, $modal).prop("disabled", false);
		$(tab + " .custom-type." + key, $modal).prop("disabled", false);
		enableValues(key, tab);
		$(tab + " .custom-on." + key, $modal).prop("checked", true);
	}

	function enableValues(key, tab)
	{
		var select = $(tab + " .custom-type." + key, $modal);
		var field = $(tab + " .custom-value." + key, $modal);
		if (select.val() === "dropdown")
		{
			field.next('.tagsinput').show();
			field.prop("disabled", false);
		}
		else
		{
			field.next('.tagsinput').hide();
			field.prop("disabled", true);
		}
	}

	function serialize(tab)
	{
		var data = {};
		var $id = $("#" + tab + ".tab-pane", $modal);
		data.type = tab;
		var inputs = $(".custom-on", $id);
		inputs.each(function()
		{
			var key = $(this).data("key");
			data[key] = {};
			data[key].on = $(".custom-on." + key, $id).prop("checked");
			if (data[key].on)
			{
				data[key].label = $(".custom-label." + key, $id).val();
				data[key].type = $(".custom-type." + key, $id).val();
				if (data[key].type == "dropdown")
					data[key].values = $(".custom-value." + key, $id).val();
				data[key].required = $(".custom-required." + key, $id).prop("checked");
			}
		});
		return JSON.stringify(data);

	}

	$('.form-custom-fields').validate({
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error, element)
		{
			if ($(element).hasClass('custom-value'))
			{
				error.insertAfter($(element).next('.tagsinput'));
				return;
			}
			error.insertAfter(element);
		},
		ignore : '',
		rules : {

		},
		highlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-error');
		},
		success : function(label, element)
		{
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		submitHandler : function(form)
		{
			startLoadingForeground();
			var $form = $(form);
			$(form).ajaxSubmit({
				url : '/api/updateCustomFields',
				data : {
					'proposals' : serialize('proposals'),
					'products' : serialize('products')
				},
				success : function(data)
				{
					if (data)
					{
						$modal.modal('hide');
						location.reload();

					}
					else
					{
						swal({
							title : "Error",
							text : "It wasn't possible to change the custom fields. Contact your Qcloud administrator and make sure you have permission to manage custom fields.",
							type : "error"
						});
					}
					stopLoadingForeground();
				},
				error : function()
				{
					stopLoadingForeground();

					alert("Unable to update your company custom fields. Please try again later.");
				}
			});
		}
	});

	$('[name*="label-"]', $modal).each(function()
	{
		$(this).rules('add', {
			required : function(element)
			{
				var key = $(element).data("key");
				return $("#on-" + key).prop("checked");
			}
		});
	});

	$('[name*="values-"]', $modal).each(function()
	{
		$(this).rules('add', {
			required : function(element)
			{
				var key = $(element).data("key");
				var checked = $("#on-" + key).prop("checked");
				var val = $("#type-" + key).val() === "dropdown";
				return checked && val;
			}
		});
	});

});
