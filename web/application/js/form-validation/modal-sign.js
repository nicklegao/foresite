$(function()
{
	$('.form-sign').validate({
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error, element)
		{
			error.insertAfter(element);
		},
		rules : {
			firstname : {
				required : true,
				minlength : 1,
				maxlength : 50
			},
			lastname : {
				required : true,
				minlength : 1,
				maxlength : 50
			},
			token : {
				required : true,
			},
			content : {
				required : true,
				minlength : 1,
				maxlength : 50
			},
			file : {
				required : true,
			},
		},
		submitHandler : function(form)
		{
			startLoadingForeground();
			$('#sign-panel').find('input[name="imageData"]').val($("#wPaint", '#sign-panel').wPaint("image"));
			$(form).ajaxSubmit({
				success : function(response, statusText, xhr, $form)
				{
					stopLoadingForeground();
					// alert(response);
					if (!response.success)
					{
						swal("Error", response.message, "error");
					}
					else if ($('.sign-button').length == 1 && $('.sign-button-area').length == 1)
					{
						$('#sign-panel').modal('hide');
						$('.sign-button-area').remove();
						$('.inlineAction-accept').click();
					}
					else
					{
						window.location.reload();
					}
				}
			});
		}
	});

	$('.form-verify-sign').validate({
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error, element)
		{
			error.insertAfter(element);
		},
		rules : {
			email : {
				required : true,
				minlength : 1,
				maxlength : 50
			},
		},
		submitHandler : function(form)
		{
			startLoadingForeground();
			$(form).ajaxSubmit({
				success : function(response, statusText, xhr, $form)
				{
					// alert(response);
					if (response != 'OK')
					{
						swal("Error", "Unable to verify that you are a valid signatory.", "error");
					}
					else
					{
						swal("Verification required", "You have been sent an email to authenticate yourself as a signatory.", "info");
					}
					stopLoadingForeground();
					$('#sign-verify-panel').modal('hide');
				},
				error : function(response)
				{
					stopLoadingForeground();
					$('#sign-verify-panel').modal('hide');
					swal("Error", "Unable to verify that you are a valid signatory.", "error");
				}
			});
		}
	});
});
