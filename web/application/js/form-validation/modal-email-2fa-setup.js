$(function()
{
	var $modal = $("#auth-modal").closest(".modal");
	$('.form-email-auth').validate({
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error, element)
		{
			error.insertAfter(element);
		},
		ignore : '',
		rules : {
			verifyotp : {
				required : true,
				minlength : 6,
				maxlength : 6,
			},
		},
		highlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-error');
		},
		success : function(label, element)
		{
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		submitHandler : function(form)
		{
			startLoadingForeground();
			var $form = $(form);
			$(form).ajaxSubmit({
				url : '/api/updateEmailTwoFactor',
				success : function(data)
				{
					stopLoadingForeground();
					if (data.success === false)
					{
						swal({
							title : 'Error',
							type : 'error',
							text : data.message
						})
					}
					else
					{
						$modal.modal('hide');
						swal({
							title : 'Success',
							type : 'success',
							text : data.message
						})
						$('#mobile-tfa').hide();
						$('#email-tfa').show();
					}
				},
				error : function()
				{
					stopLoadingForeground();

					alert("Unable to update your user settings. Please try again later.");
				}
			});
		}
	});
});
