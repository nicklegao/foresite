$(function(){
  var $modal = $("#migration-team").closest(".modal");
  $('.form-migration-team').validate({
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
      error.insertAfter(element);
    },
    ignore: '',
    rules: {
      newId: {
        required: true,
      },
    },
    highlight: function (element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    success: function (label, element) {
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    },
	submitHandler: function(form){
      $modal.modal('hide');
      startLoadingForeground();
      var $form = $(form);
      $(form).ajaxSubmit({
    	  url: '/api/deleteTeam',
		  success : function(data){         
	          stopLoadingForeground();
	          if(data.success === false)
        	  {
	        	  swal({
	        		  title: 'Error',
	        		  type: 'error',
	        		  text: data.message
	        	  })
        	  }
			  $('#users-management-modal').load('/application/dialog/managementUsers.jsp', function () {

				  $('#users-management-modal').find('.teamsButton').click()

			  });
	        },
	        error : function(){
	          stopLoadingForeground();
	          
	          alert("Unable to update your team settings. Please try again later.");
	      }
	  });
    }
  });
});