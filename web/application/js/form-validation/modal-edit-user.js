var MIN_PASSWORD_LENGTH = 8;

jQuery.validator.addMethod("passwordStrength", function(value, element)
{
	if (value.length < 8)
	{
		return false;
	}
	var digit = new RegExp('[0-9]');
	if (!digit.test(value))
	{
		return false;
	}
	var lowercase = new RegExp('[a-z]');
	if (!lowercase.test(value))
	{
		return false;
	}
	var uppercase = new RegExp('[A-Z]');
	if (!uppercase.test(value))
	{
		return false;
	}

	return true;
}, "Password must be at least " + MIN_PASSWORD_LENGTH + " characters, contain uppercase, lowercase and a number");

$.validator.addMethod("needsSelection", function(value, element)
{
	return $(element).find('option:selected').length > 0;
});

function isAddAction()
{
	return $("#action").val() === "add";
}

function isPasswordPresent()
{
	return $('#resetPassword').val().length > 0;
}

$(function()
{
	var $modal = $("#edit-user").closest(".modal");


	$modal.find('[data-toggle="tooltip"]').tooltip();
	$('.form-edit-user').validate({
		errorElement : 'span',
		errorClass : 'has-error help-block',
		errorPlacement : function(error, element)
		{
			error.insertAfter(element);


		},
		ignore : '',
		rules : {
			username : {
				required : true,
				remote : '/api/userNotExists?id=' + $('[name="id"]').val(),
			},
			emailAddress : {
				required : true,
				email : true,
			},
			name : {
				required : true,
			},
			surname : {
				required : true,
			},
			phone : {
				required : true,
			},
			resetPassword : {
				required : {
					depends : isAddAction
				},
				passwordStrength : {
					depends : isPasswordPresent
				},
			},
			confirmPassword : {
				required : isPasswordPresent,
				equalTo : {
					depends : isPasswordPresent,
					param : "#resetPassword"
				}
			},
			// teams : {
			// required : true
			// },
			roles : {
				needsSelection : true
			},
			sabreConsultantId : {
				remote : '/api/sabreIdValid?id=' + $('[name="id"]').val()
			},
			travelportConsultantId : {
				remote : '/api/travelportIdValid?id=' + $('[name="id"]').val()
			}
		},
		messages : {
			emailAddress : {
				remote : "User already exists."
			},
			confirmPassword : {
				equalTo : "Please enter the same password to confirm its spelt correctly."
			},
			roles : {
				needsSelection : "Please assign at least one role to the user in the Enrollment tab."
			},
			sabreConsultantId : {
				remote : "User with this consultant id already exists."
			},
			travelportConsultantId : {
				remote : "User with this consultant id already exists."
			}
		},
		highlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			displayError(element, true);
		},
		unhighlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-error');
			displayError(element, false);
		},
		success : function(label, element)
		{
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		submitHandler : function(form)
		{
			$modal.modal('hide');
			startLoadingForeground();
			var $form = $(form);
			var data = buildFormData();
			$.ajax({
				url : '/api/updateUser',
				data : data,
				success : function(response)
				{
					stopLoadingForeground();
					if (response.success === false)
					{

						swal({
							title : 'Error',
							type : 'error',
							text : response.message
						})
					}
					else
					{
						swal({
							title : "Success",
							type : "success",
							text : response.message
						})
					}
					$('#settingsMenuUsers').click();
				},
				error : function()
				{
					stopLoadingForeground();

					alert("Unable to update your user settings. Please try again later.");
				}
			});
		}
	});

	$('#edit-user', $modal).find('select').select2();

	// $("#edit-user", $modal).iCheck({
	// checkboxClass : 'icheckbox_square-red',
	// radioClass : 'iradio_square-red'
	// })
	$('input[type="checkbox"]', $modal).each(function()
	{
		new Switchery($(this)[0], {
			color : '#64bd63',
			size : 'small'
		});
		// $(this).change(function(e)
		// {
		// });
	});

	$('#resendActivationEmail').click(function()
	{
		var id = $('.form-edit-user').find('input#id').val();
		resendActivationEmail(id);
	});
});

function displayError(element, show)
{
	var id = $(element).attr('id')
	if (id == 'roles')
	{
		if (show)
		{
			$('#enrollment_tab_button_a').attr('style', 'background:#ffb3b1;');
		}
		else
		{
			$('#enrollment_tab_button_a').removeAttr('style');
		}
	}
	if ((id == 'name') || (id == 'surname') || (id == 'phone'))
	{
		var sessionId = sessionStorage.getItem('error' + id);
		if (show)
		{
			sessionStorage.setItem(('error' + id), 'true')
			$('#personal_tab_button_a').attr('style', 'background:#ffb3b1;');
		}
		else if (sessionId != 'false' && sessionId != null)
		{
			sessionStorage.setItem(('error' + id), 'false')
			$('#personal_tab_button_a').removeAttr('style');
		}
	}
}


function buildFormData()
{
	var params = {};
	$('#edit-user form').find('input, select').each(function()
	{
		var value = $(this).val();
		if ($(this).prop('type') == 'checkbox')
		{
			value = $(this).prop('checked');
		}
		params[$(this).prop('name')] = value;
	});
	return params;
}
