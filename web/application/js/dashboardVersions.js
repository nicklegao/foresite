var versionTable = undefined;

$(document).ready(function() {
    var domHead = "<'navbar versionNav' <'navbar-form versionNav-form navbar-left'<'#dateRangePicker'><'#version-columns-list.form-group'>>>";
    var domBody = 't';


    versionTable = $('#revisionsTable').DataTable( {
        "paging": false,
        "columnDefs": [
            {
                "targets": [ 2, 3, 4 ],
                "visible": false,
                "searchable": false
            },
        ],
        "sDom" : domHead + domBody,
    } );

    $('a.toggle-vis').on( 'click', function (e) {
        e.preventDefault();

        // Get the column API object
        var column = versionTable.column( $(this).attr('data-column') );

        // Toggle the visibility
        column.visible( ! column.visible() );
    } );

    setupVisibility();
} );



function setupVisibility() {
    if ($('.version-column-visible-button').length > 0) {
        $('.version-column-visible-button').remove();
    }

    var columnDiv = $('#version-columns-list');
    var columnButton = $('<button class="ripple btn btn-default version-column-visible-button" title="Visible columns">');
    columnButton.append("Visible columns");
    columnDiv.append(columnButton);
    var columnList = $('<ul>');
    for (var i = 1; i < versionTable.columns()[0].length - 1; i++)
    {
        if (($(versionTable.column(i).header()).html()).match("undefined")) {
            var invalidColumn = versionTable.column(i);
            invalidColumn.visible(false)
        }
        var tagged = $('<li>').addClass('column-tag');
        var column = $(tagged).attr("data-column", i).html($(versionTable.column(i).header()).html());
        if ((i == 1)|| (i == 5) || (i == 6)|| (i == 7))
        {
            column.addClass("active");
        }
        columnList.append(column);
        column.on("click", function()
        {
            var width = $('.cd-panel-container').width();
            if ($(this).hasClass('active')) {
                $('.cd-panel-container').width(width - 30);
            } else {
                $('.cd-panel-container').width(width + 30);
            }
            var thisColumn = versionTable.column($(this).attr('data-column'));
            thisColumn.visible(!thisColumn.visible());
            $(this).toggleClass("active", thisColumn.visible());
            versionTable.draw();
        });
    }



    // Keep the daterangepicker class to use the same css
    var columnFloatMenu = $('<div class="daterangepicker dropdown-menu opensright">').append($('<div class="ranges">').append(columnList));
    columnDiv.append(columnFloatMenu);
    columnFloatMenu.css("top", columnButton.outerHeight());
    columnFloatMenu.css("left", 0);

    columnFloatMenu.on('click', function(event)
    {
        event.stopPropagation();
    });

    columnButton.on("click", function()
    {
        columnFloatMenu.toggle();

        setTimeout(function()
        {
            $(window).one('click', function()
            {
                columnFloatMenu.toggle();
            });
        }, 100);
    });
}