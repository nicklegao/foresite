var appData, imageCounter = 1;
$(document).ready(function()
{

	$(document).ajaxComplete(function(event, xhr, ajaxOptions)
	{
		if (xhr.status == 401)
		{
			swal({
				title : "You've been logged out!",
				text : "Due to inactivity, you have been logged out.",
				type : "warning"
			}, function()
			{
				window.location.reload();
			});
			logger.info("HTTP 401(Unauthorised) recieved... ");
		}

	});

	$('.demo-video').magnificPopup({
		disableOn : 700,
		type : 'iframe',
		mainClass : 'mfp-fade',
		removalDelay : 160,
		preloader : false,

		fixedContentPos : false
	});
	loadApps();

	$(document).on('click', '#apps-list li', function(evt)
	{
		var appId = $(evt.target).closest('li').attr('dataId');
		loadApp(appId);
	});

	$('#app-nav-categories li').on('click', function(evt)
	{
		$(evt.target).closest('li').addClass('active').siblings().removeClass('active');
		loadApps();
	});

	$('#backBtn').on('click', function()
	{
		$('#market-apps').show();
		$('#app-details').hide();
	});

	console.log('market');
	$('.apps-list .app-item').on('click', function(evt)
	{
		var appId = $(evt.target).closest('li').attr('dataId');
		loadApp(appId);
	});

	$('.action #btnSubscribe').on('click', function(evt)
	{

		var appId = $(evt.target).closest('.action').attr('dataId');
		$.ajax({
			type : "POST",
			url : '/api/market/need-setup-payment',
		}).done(function(data, textStatus, jqXHR)
		{
			if (data)
			{
				swal({
					title : "Please update your payment settings.",
					text : "We currently do not have a valid credit card to process your payments, either your credit card information is <u>missing</u> or your credit card details are <u>incorrect</u	>. <br/> Do you want to update your credit card information now?",
					type : "warning",
					html : true,
					showCancelButton : true,
					cancelButtonText : "No thank you",
					confirmButtonText : "Yes please",
					closeOnConfirm : true
				}, function(goSetup)
				{
					if (goSetup)
					{
						displayAccountSettingsAction();
					}
				});
			}
			else
			{
				var text = "Note: you can cancel the subscription at<br/>any time if you decide to stop using this service.";
				var iconUrl = $('.app-thumbnail img').attr('src');
				var message;
				if ($('#btnSubscribe').text() == "FREE TRIAL")
				{
					message = '<div class="swalTitle">Thank you for claiming your <br/>14 days free trial of the ' + $("#appTitle").text() + ' plugin. <br/><br/>After completion of your free trial period <br/>subscription to this service costs:<br/>' + $('#appCost').text() + ' <span id="terms">' + $('#terms').text() + '</span></div>';
				}
				else
				{
					message = '<div class="swalTitle">Thank you for choosing <br/>the ' + $("#appTitle").text() + ' plugin. <br/><br/>Subscription to this service costs:<br/>' + $('#appCost').text() + ' <span id="terms">' + $('#terms').text() + '</span></div>';
				}
				swal({
					html : true,
					title : message,
					text : text,
					imageUrl : iconUrl,
					showCancelButton : true,
					confirmButtonText : "Yes please",
					cancelButtonText : "No thank you",
					closeOnConfirm : true
				}, function()
				{
					setTimeout(function()
					{
						displayTermsAndConditions();
					}, 500)
				});

			}
		});
	});

	$('.action #btnUnsubscribe').on('click', function(evt)
	{
		var iconUrl = $('.app-thumbnail img').attr('src');

		swal({
			title : '<div class="swalTitle">After you cancel your subscription to this feature, you can continue to use it until your paid period expires, or if you were just trying out this feature in a free trial it will cancel immediately.</div>',
			text : "Please note, there is only one free trial allowed per QuoteCloud account for each plug-in/extension.",
			html : true,
			imageUrl : iconUrl,
			showCancelButton : true,
			confirmButtonColor : "#DD6B55",
			confirmButtonText : "Yes, unsubscribe",
			cancelButtonText : "No",
			closeOnConfirm : true
		}, function()
		{
			setTimeout(function()
			{
				var appId = $(evt.target).closest('.action').attr('dataId');
				startLoadingForeground();
				$.ajax({
					url : '/api/market/unsubscribe',
					type : "POST",
					data : {
						appId : appId
					},
					success : function(data)
					{
						console.log(data);
						var message = data == 'success' ? "Plugin unsubscribed." : "Something went wrong.";
						stopLoadingForeground();
						swal({
							title : "Thank you!",
							text : message,
							type : "success",
							html : true,
							closeOnConfirm : true
						}, function()
						{
							window.location.href = "/dashboard";
						});

					},
					error : function(data)
					{
						stopLoadingForeground();
						alert("Unable to process. Please try again later.");
					}
				});
			}, 500)
		});

	});

	$('.action #btnReactivate').on('click', function(evt)
	{

		var iconUrl = $('.app-thumbnail img').attr('src');
		swal({
			html : true,
			title : '<div class="swalTitle">Thank you for choosing to continue using the ' + $("#appTitle").text() + ' plugin.</div>',
			text : "Note: you will only be charged after the end of current paid period.",
			imageUrl : iconUrl,
			showCancelButton : true,
			confirmButtonText : "Yes please",
			cancelButtonText : "No thank you",
			closeOnConfirm : true
		}, function()
		{
			setTimeout(function()
			{
				var appId = $(evt.target).closest('.action').attr('dataId');
				startLoadingForeground();
				$.ajax({
					url : '/api/market/reactivate',
					type : "POST",
					data : {
						appId : appId
					},
					success : function(data)
					{
						console.log(data);
						var message = data == 'success' ? "Plugin reactivated." : "Something went wrong.";
						stopLoadingForeground();
						swal({
							title : "Thank you!",
							text : message,
							type : "success",
							html : true,
							closeOnConfirm : true
						}, function()
						{
							window.location.href = "/dashboard";
						});

					},
					error : function(data)
					{
						stopLoadingForeground();
						alert("Unable to process. Please try again later.");
					}
				});
			}, 500)
		});

	});

	$(document).on('click', '#agreementContinue', function()
	{
		var appId = $('#market .action').attr('dataId');
		$('#terms-modal').hide();
		subscribe(appId);
	});

	$("input[type='checkbox']").iCheck({
		checkboxClass : 'icheckbox_square-orange',
		radioClass : 'iradio_square-orange'
	});

	$('.terms-check').on('ifChanged', function(evt)
	{
		if ($(evt.target).is(':checked'))
		{
			$('#agreementContinue').removeAttr('disabled');
		}
		else
		{
			$('#agreementContinue').attr('disabled', '');
		}
	});


	$(document).on("click", "#settingsBtn", appSettingsAction);

	if (hopscotch.getState() === "sabreTour:2")
	{
		hopscotch.startTour(sabreTour);
	}

});

function loadApps()
{
	console.log('load apps');
	var catId = $('#app-nav-categories li[class="active"]').attr('dataId');
	var list = $('#apps-list');
	$('li', list).remove();
	$.ajax('/api/market/loadApps', {
		type : "POST",
		dataType : 'json',
		data : {
			catId : catId
		}
	}).done(function(data)
	{
		$.each(data, function(key, value)
		{
			var li = $('<li>', {
				dataId : value.element_id
			});
			var div = $('<div class="img-wrap">');
			div.append($('<div class="wrap-desc">', {

			}).text(value.attr_label));
			div.append($('<img>', {
				alt : value.attr_headline,
				src : value.attrfile_appicon
			}));
			li.append(div);
			li.append($('<h2>').text(value.attr_headline));
			list.append(li);
		});
	});

}

function loadApp(appId)
{
	imageCounter = 1;
	$.ajax('/api/market/loadApp', {
		type : "POST",
		dataType : 'json',
		data : {
			appId : appId
		}
	}).done(function(data)
	{
		appData = data;

		$('.updateDate').text('Last updated on: ' + data.attrdate_updatedon)
		$('#settingsBtn').attr('app-name', data.attr_appcode.toLowerCase());
		$('.sub-msg').text("");
		$('.app-thumbnail img').attr('src', data.attrfile_appicon);
		$('#appTitle').text(data.attr_headline);
		$('#appLabel').text(data.attr_label);
		// $('#lblVersion').text(data.attr_version);
		// $('#lblSubscribers').text(data.attrinteger_subscribers);
		// $('#lblUpdate').text(data.attrdate_updatedon);
		// $('#lblCost').text(data.currencysymbol + data.price.toFixed(2));
		$('#descContent').html(data.attrlong_appdescription ? data.attrlong_appdescription.content : '');
		$('#descBodyContent').html(data.attrlong_appdetail ? data.attrlong_appdetail.content : '');
		$('.app-image').attr('src', data.attrfile_appimage1).attr('alt', data.attr_headline);
		$('#app-details .action').attr('dataId', appId);
		$('#termsContent').html(data.attrlong_terms ? data.attrlong_terms.content : '');

		if (data.attrcheck_hassettings == 1)
		{
			$('#settingsBtn').show();
		}
		else
		{
			$('#settingsBtn').hide();
		}
		if (data.price == 0)
		{
			$('#terms').hide();
			$('.app-image-box').css('margin-top', '30px');
		}
		else
		{
			var gst = "";
			if (data.countrycode == "AU")
			{
				gst = " (EXCL GST)";
			}
			$('#terms').text(data.attrcheck_perusermonth ? "PER USER/MONTH" + gst : "PER MONTH" + gst).show();
			$('.app-image-box').css('margin-top', '50px');
		}

		$('#appCost').text(data.price == 0 ? 'FREE' : (data.currencysymbol + data.price.toFixed(2)));

		if (data.attr_video_link)
		{
			$('.demo-video').attr('href', data.attr_video_link);
			$('.demo-video').show();
		}
		else
		{
			$('.demo-video').attr('href', '');
			$('.demo-video').hide();
		}

		$('#btnComingSoon').hide();
		$('#termsOfService').show();
		$('.app-pricing').show();
		if (data.attrcheck_release != 1)
		{
			// $('.sub-msg').text("Coming Soon");
			$('#btnSubscribe').hide();
			$('#btnUnsubscribe').hide();
			$('#settingsBtn').hide();
			$('#btnReactivate').hide();
			$('#btnComingSoon').show();
			$('#termsOfService').hide();
			$('.app-pricing').hide();
		}
		else if (data.subscribed)
		{
			$('#btnSubscribe').text('SUBSCRIBE');
			var sub_status = data.subscription_status.toLowerCase();
			if (sub_status == 'cancelled')
			{
				$('#btnUnsubscribe').hide();
				$('#settingsBtn').hide();
				$('#btnReactivate').hide();
				$('#btnSubscribe').show();

			}
			if (sub_status == 'unsubscribed' || sub_status == "subscribed" || sub_status == "trial")
			{
				$('#btnSubscribe').hide();
				if (sub_status == 'unsubscribed')
				{
					$('#btnReactivate').show();
					$('#settingsBtn').hide();
					$('#btnUnsubscribe').hide();
					$('.sub-msg').text("Your subscription ends on " + data.subscription_end);
				}
				else
				{
					var id = '#app-' + $('#settingsBtn').attr('app-name') + '-modal';
					$('#btnUnsubscribe').show();
					$('#btnReactivate').hide();
				}
			}
		}
		else
		{
			$('#btnSubscribe').text('FREE TRIAL');
			$('#btnSubscribe').show();
			$('#settingsBtn').hide();
			$('#btnUnsubscribe').hide();
			$('#btnReactivate').hide();
		}

		var carousel_indicators = $('.carousel-indicators');
		carousel_indicators.html('');
		var carousel_inner = $('.carousel-inner');
		carousel_inner.html('');
		var image_desc = $('.image-desc');
		image_desc.html('');

		var iteration = 1;
		var image = 'attrfile_appimage' + iteration;
		var desc = 'attrlong_image' + iteration + '_desc_html_string';
		var imageCount = 0;
		while (iteration < 6)
		{
			if (data[image])
			{
				imageCounter = imageCounter < 1 ? 5 : imageCounter;

				carousel_indicators.append($('<li>', {
					'data-target' : '#myCarousel',
					'data-slide-to' : iteration - 1
				}));
				console.log(data[desc]);

				carousel_inner.append($('<div>', {
					class : 'item'
				}).append($('<img>', {
					src : data[image]
				})).append($(data[desc], {
					class : 'image-desc'
				})));
				imageCount++;
			}
			iteration++;
			image = 'attrfile_appimage' + iteration;
			desc = 'attrlong_image' + iteration + '_desc_html_string';
		}

		if (imageCount < 2)
		{
			$('.slide-show').hide();
			$('.carousel-control').hide();
		}
		else
		{
			$('.slide-show').show();
			$('.carousel-control').show();
		}

		$(carousel_indicators.children('li')[0]).addClass('active');
		$(carousel_inner.children('div')[0]).addClass('active');

		$('#market-apps').hide();
		$('#app-details').show();
	});

}
function subscribe(appId)
{
	startLoadingForeground();
	$.ajax({
		url : '/api/market/subscribe',
		type : "POST",
		data : {
			appId : appId
		},
		success : function(data)
		{
			console.log(data);
			var message = data == 'success' ? "The " + $("#appTitle").text() + " plugin has been subscribed successfully." : "Something went wrong.";
			stopLoadingForeground();
			swal({
				title : "Thank you!",
				text : message,
				type : data,
				html : true,
				closeOnConfirm : true
			}, function()
			{
				window.location.href = "/dashboard";
			});

		},
		error : function(data)
		{
			stopLoadingForeground();
			alert("Unable to subscribe. Please try again later.");
			window.location.href = "/dashboard";
		}
	});
}

function flipToNext()
{
	if (scroll)
	{
		scroll = false;
		var past = $('.flip-items  .past');
		var previous = $('.flip-items  .previous');
		var current = $('.flip-items  .current');
		var next = $('.flip-items  .next');
		var future = $('.flip-items  .future');
		past.addClass('future').removeClass('past');
		previous.addClass('past').removeClass('previous');
		current.addClass('previous').removeClass('current');
		next.addClass('current').removeClass('next');
		future.addClass('next').removeClass('future');
		scroll = true;
	}
}
function flipToPrevious()
{
	if (scroll)
	{
		scroll = false;
		var past = $('.flip-items  .past');
		var previous = $('.flip-items  .previous');
		var current = $('.flip-items  .current');
		var next = $('.flip-items  .next');
		var future = $('.flip-items  .future');
		past.addClass('previous').removeClass('past');
		previous.addClass('current').removeClass('previous');
		current.addClass('next').removeClass('current');
		next.addClass('future').removeClass('next');
		future.addClass('past').removeClass('future');
		scroll = true;
	}
}
function displayAccountSettingsAction()
{
	logger.info("Displaying AccountSettings popup");

	var $modal = $('#account-settings-modal');
	$('body').modalmanager('loading');

	$modal.load('/application/dialog/accountSettings.jsp', {}, function()
	{
		$modal.modal({
			backdrop : 'static',
			keyboard : false
		});
		setupModalsBackgroundColor();
	});

	return $modal;
}

function displayTermsAndConditions()
{
	var $modal = $('#terms-modal');
	$('body').modalmanager('loading');
	$('.terms-check').attr('checked', false);
	$('#agreementContinue').attr('disabled', '');
	$modal.modal({
		width : '50%',
		backdrop : 'static',
		keyboard : false
	});
	setupModalsBackgroundColor();
	return $modal;
}

function displayTermsAndConditionsButton()
{
	var $modal = $('#terms-modal');
	$('body').modalmanager('loading');
	$('#termsFooter').hide();
	$modal.modal({
		width : '50%',
		backdrop : 'static',
		keyboard : false
	});
	setupModalsBackgroundColor();
	return $modal;
}

function setupModalsBackgroundColor()
{
	var visible = $('.modal:visible');
	visible.removeClass('child-modal').slice(1).addClass('child-modal');
}

// var redrawTableDelay = undefined;
function redrawTable()
{
	// Display loading message to user
	// showNotificationMessage(true, "Working...");
	showLoadingCog(true);
	NProgress.start();
	// showScrollButtons();

	// if (redrawTableDelay != undefined)
	// {
	// clearTimeout(redrawTableDelay);
	// }
	// redrawTableDelay = setTimeout(function()
	// {
	// proposalTable && proposalTable.draw('page');
	// }, 150);

}

function appSettingsAction(e)
{
	var id = '#app-' + $('#settingsBtn').attr('app-name') + '-modal';
	var modal = '/application/dialog/market/' + $('#settingsBtn').attr('app-name') + '.jsp';

	var $modal = $(id);
	$modal.empty();
	$('body').modalmanager('loading');

	$modal.load(modal, {}, function()
	{
		$modal.modal({
			width : '1024px',
			backdrop : 'static',
			keyboard : false
		});
	});

	return $modal;
}
