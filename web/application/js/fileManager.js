var previousFolder = 0;
var preventDScallback = false;

$(document).ready(function()
{

	// $(document).on("click", "#add-template-button", createTemplateAction);
	$(document).on("click", ".file-add-container .btn1", closeAddFileButton);
	$(document).on("click", "#add-folder-button", createFolder);
	$(document).on("click", '[data-type="folder"] .buttonEditTemplate', function()
	{
		editFolder($(this));
	});

	$(document).on("click", '[data-type="file"] .buttonDownloadItem', function()
	{
		downloadItem($(this));
	});

	$(document).on('click', '[data-type="file"] .buttonEditTemplate', function()
	{
		addEditItem.call($(this), false);
	});

	$(document).on('click', '[data-type="file"] .buttonCopyTemplate', function(){
		copyItem.call($(this));
	});

	$(document).on("click", ".back-button", backButtonPressed);
	$(document).on("click", "#templateDeleteAction", deleteButtonPressed);
	$(document).on("click", ".templateDeleteConfirmed", templateDeleteConfirmed);
	$(document).on("click", ".deleteItemConfirm", templateDeleteConfirmed);
	$(document).on('change', 'select[name="folder"]', newFolderPrompt);
	$(document).on('change', 'select[name="subFolder"]', newSubFolderPrompt);
	$(document).on("click", ".buttonDeleteItem", function(event)
	{
		deleteItemPressed($(this).closest('[data-id]'));
	});


	$(document).on("click mousedown touchstart", ".item-actions button", function(event)
	{
		event.stopPropagation();
	});

	$(document).on("click", ".mainFileBody tr, .page-content .content .root-library .item", function(ele)
	{

		var folderType = $(this).attr('data-type');

		switch (folderType)
		{
			case "template":
				loadContent("template");
				break;
			case "images":
				loadContent("images");
				break;
			case "text":
				loadContent("text");
				break;
			// case "products":
			// 	loadContent("products");
			// 	break;
			case "videos":
				loadContent("videos");
				break;
			case "coverpage":
				// loadCoverPages();
				console.log("Need to be implemented");
				break;
			default:
				console.log("default");
				loadContent(libraryType);
				f
				break;
		}
	});

	$(document).on("click", "#buttonDeleteLibraryContent", function()
	{
		var id = $(this).attr('data-id');
		var libraryType = $(this).attr('data-type');
		startLoadingForeground();
		$.ajax({
			url : '/api/library/delete',
			data : {
				libraryType : libraryType,
				elementId : id
			}
		}).done(function(data, textStatus, jqXHR)
		{
			if (data.success)
			{
				console.log("Item Deleted");
				refreshContent();
			}
			else
			{
				swal({
					title : "Error",
					text : "It wasn't possible to delete the item. Contact your TravelDocs administrator and make sure you have permission to do this.",
					type : "error"
				});
			}
			stopLoadingForeground();
		});

	});

	$(document).on("click", ".page-content .content .sub-folders .item", function(ele)
	{
		// var fileFolderType = $('.SubFileBody').attr('data-type');
		// var level = $('.SubFileBody').attr('data-level');
		// var folder = $('.SubFileBody').attr('data-folder');
		var libraryType = $(this).closest('[data-library]').attr('data-library');
		var level = $(this).closest('[data-level]').attr('data-level');
		var folder = $(this).closest('[data-folder]').attr('data-folder');
		var subfolder = $(this).closest('[data-subfolder]').attr('data-subfolder');
		var sidebar = $('.page-sidebar');
		var item = $(this);
		var id = $(item).attr('data-id');

		if (folder == null && subfolder != null)
		{
			folder = previousFolder;
		}


		if ($(this).closest('[data-type]').attr('data-type') === "file")
		{
			if (libraryType === "template")
			{
				$(sidebar).find('#downloadFile').hide();
				$(sidebar).find('#editFile').show();
				$(sidebar).find('#sidebarIcon').html('<i class="icon-file-image s-12"></i>');
				$(sidebar).find('#statType').html('Template Stats');
				$(sidebar).find('.type td').html('Template file');
				$(sidebar).find('.location td').html("Templates" + (level > 1 ? (" > " + folder) : ""));
				$($(this).find('.buttonEditTemplate')).click();
			}
			else
			{
				$(sidebar).find('#downloadFile').show();
				$(sidebar).find('#editFile').hide();
				$($(this).find('.buttonEditTemplate')).click();
			}
			$(sidebar).find('.mainTitle').html($(item).attr('data-name'));
			$(sidebar).find('.subtitle').html("Updated: " + $(item).attr('data-date'));

		}
		else
		{
			switch (libraryType)
			{
				case "template":
					loadContent("template", folder, subfolder);
					break;
				case "images":
					loadContent("images", folder, subfolder);
					break;
				case "text":
					loadContent("text", folder, subfolder);
					break;
				// case "products":
				// 	loadContent("products", folder, subfolder);
				// 	break;
				case "videos":
					loadContent("videos", folder, subfolder);
					break;
				default:
					loadContent(libraryType, folder, subfolder);
					console.log("default");
					break;
			}

		}

	});

	switch (libraryType)
	{
		case "template":
			loadContent("template");
			break;
		case "images":
			loadContent("images");
			break;
		case "text":
			loadContent("text");
			break;
		// case "products":
		// 	loadContent("products");
		// 	break;
		case "videos":
			loadContent("videos");
			break;
		case "coverpage":
			// loadCoverPages();
			console.log("Need to be implemented");
			break;
		default:
			loadContent(libraryType);
			console.log("default");
			break;
	}


	$('#folder-delete-form').validate({
		rules : {
			transferTo : "required"
		},
		submitHandler : function(form)
		{
			executeDeleteAjax(currentRequest.libraryType, $('[name="transferTo"]', form).val());
		}
	});
});

function deleteButtonPressed()
{

	var travelDocId = $(this).attr("data-id");

	$("#templateDeleteModal .templateDeleteConfirmed").attr("data-id", travelDocId);

}

function deleteItem()
{
	swal({
		title : "Error"
	})
}

function templateDeleteConfirmed()
{
	var travelDocId = $("#templateDeleteModal .templateDeleteConfirmed").attr("data-id");
	$("#templateDeleteModal").modal("hide");
	startLoadingForeground();
	$.ajax({
		url : '/api/traveldoc/delete',
		data : {
			travelDocId : travelDocId
		}
	}).done(function(data, textStatus, jqXHR)
	{
		if (data.success)
		{
			console.log("deleted");
		}
		else
		{
			swal({
				title : "Error",
				text : "It wasn't possible to delete the template. Contact your TravelDocs administrator and make sure you have permission to do this.",
				type : "error"
			});
		}
		stopLoadingForeground();
	});
}

function item($item)
{
	var isSubFolder = $item.filter('[data-subfolder]').length > 0;
	var elementId = $item.attr('data-id');
	var type = isSubFolder ? 'subfolder' : $item.attr('data-type');
	var name = isSubFolder ? $item.attr('data-subfolder') : '';
	var cnt = $item.attr('data-cnt');
	return {
		subfolder : isSubFolder,
		elementId : elementId,
		type : type,
		name : name,
		cnt : cnt,
	}
}

function deleteItemPressed($item)
{
	var libraryType = $('[data-library]').attr('data-library');
	toBeDeletedElements = [];
	var transferto = '';
	var i = item($item);
	toBeDeletedElements.push(i);
	/*
	 * $('.item.ds-selected').not($item).each(function(index, e) {
	 * toBeDeletedElements.push(item($(e))); })
	 */
	
	if (i.type == 'folder' && i.cnt && i.cnt != "0")
	{
		newDeleteLibraryFolderAction(i);
		return;
	}
	swal({
		title : 'Are you sure you want to delete this item?',
		type : "warning",
		showCancelButton : true,
		confirmButtonText : 'Delete',
		showLoaderOnConfirm : true,
	}, function(result)
	{
		if (result)
		{
			executeDeleteAjax(libraryType, transferto);
		}
	});
}

function executeDeleteAjax(libraryType, transferto)
{
	startLoadingForeground();
	$.ajax({
		url : '/api/library/delete',
		data : JSON.stringify({
			elements : toBeDeletedElements,
			libraryType : libraryType,
			transferto : transferto,
		}),
		type : 'POST',
		dataType : 'json',
		contentType : 'application/json; charset=utf-8',
	}).done(function(data, textStatus, jqXHR)
	{
		stopLoadingForeground();
		if (data.success)
		{
			$('#content-library-folder-delete-modal').modal('hide');
			console.log("Item Deleted");
			$('.modal:visible').modal('hide');
			refreshContent();
		}
		else
		{
			swal({
				title : "Error",
				html : true,
				text : data.message,
				type : "error"
			});

		}
	});
}

function backButtonPressed()
{
	if ($('.library .sub-folders').attr("data-level") > 0)
	{
		var level = $('.library .sub-folders').attr("data-level");
		var type = $('.library .sub-folders').attr("data-library");

		if (level == 3)
		{
			switch (type)
			{
				case "template":
					loadContent("template", previousFolder);
					break;
				case "images":
					loadContent("images", previousFolder);
					break;
				default:
					console.log("Running default loader for " + type);
					loadContent(type, previousFolder);
					break;
			}
		}
		else if (level < 2)
		{
			// $('.SubFileBody').attr("data-level","0");
			// $('.SubFileBody').empty();
			// $('.mainFileBody').show();

			$('.library .sub-folders').attr("data-level", "0");
			$('.library .sub-folders').hide();
			$('.library .root-library').show();

			$('.back-button-icon').hide();
		}
		else
		{
			// $('.SubFileBody').attr("data-level","1");
			$('.library .sub-folders').attr("data-level", "1");
			switch (type)
			{
				case "template":
					loadContent("template");
					break;
				case "images":
					loadContent("images");
					break;
				default:
					console.log("Running default loader for " + type);
					loadContent(type);
					break;
			}
		}
	}
	else
	{
		// $('.SubFileBody').attr("data-level","0");
		$('.library .sub-folders').attr("data-level", "0");

		$('.back-button-icon').hide();
	}
}

function loadTemplates(cat)
{
	var url = "/api/library/templates" + (cat != null ? "/" + cat : "");
	$.ajax(url, {
		success : function(data)
		{

			var fileHTML = "";

			var files = "<div class=\"files\">";
			for (var i = 0; i < data.length; i++)
			{
				var libraryType = currentRequest.libraryType;
				var file = data[i];
				var $html = $($('.item-template').find('.item').clone());
				$html.addClass('item-file');
				$html.attr('data-type', 'file');
				$html.attr('data-libraryType', libraryType);
				$html.attr('data-id', file.element_id);
				$html.find('.item-icon').html('<i class="file-icon"></i>');
				$html.find('.item-name').text(file.attr_headline);
				$html.find('.item-type').text('File');
				$html.find('.item-modified').text(file.last_update_date ? moment(file.last_update_date).format('YYYY-MMM-DD HH:mm') : "");
				files += $html.prop('outerHTML');
			}
			files += "</div>";

			$('.library .root-library').hide();
			$('.library .sub-folders').show();
			$('.library .sub-folders').html(files);
			$('.library .sub-folders').removeData("level");
			$('.library .sub-folders').attr("data-library", libraryType);
			$('.library .sub-folders').attr("data-current-folder", folder ? folder : null);
			$('.library .sub-folders').attr("data-current-subfolder", subFolder);
			var level = (subFolder != null ? "3" : (folder != null ? "2" : "1"));
			$('#file-manager').removeClass('level-1 level-2 level-3').addClass('level-' + level);
			$('.library .sub-folders').attr("data-level", level);

			$('.back-button-icon').hide();

			$('#notification-bar').text('The page has been successfully loaded');

			$(".item-actions button").on("click mousedown touchstart", function(event)
			{
				preventDScallback = true;
			});

			// $('div[data-type="file"][data-librarytype="template"] .buttonCopyTemplate').remove();
			initialiseDragSelect();

		},
		error : function()
		{
			$('#notification-bar').text('An error occurred');
		}
	});
}

function loadContent(libraryType, folder, subFolder)
{
	currentRequest = {
		"libraryType" : libraryType,
		"folder" : folder,
		"subFolder" : subFolder
	};
	refreshContent();
}

function refreshContent()
{
	closeAddFileButton();
	var folder = currentRequest.folder;
	var libraryType = currentRequest.libraryType;
	var subFolder = currentRequest.subFolder;
	$.ajax({
		url : "/api/library/contentmanagement?" + (+new Date),
		data : currentRequest,
		success : function(data)
		{

			destroyList();
			if (folder != null && folder != "")
			{
				previousFolder = folder;
			}

			var lib = data.libraryItems;
			var fold = data.libraryFolders;

			var folderHTML = "";
			var fileHTML = "";

			// $.get('/application/dialog/contentLibrary/contentLibraryList.jsp',
			// function(contentLibrary){
			// var libraryHtml = contentLibrary;
			//
			// });

			if (lib == null)
			{
				lib = [];
			}
			if (fold == null)
			{
				fold = [];
			}


			var dropzoneHtml = $("<div class=\"dropzone-placeholder\"><div class=\"placeholder-information\"><p>Drop here to upload file</p></div></div>");

			var foldersHTML = $("<div class=\"folders\">");
			for (var i = 0; i < fold.length; i++)
			{
				foldersHTML.append(buildFolderHtml(fold[i], folder));
			}

			var files = $("<div class=\"files\">");
			for (var i = 0; i < lib.length; i++)
			{
				files.append(buildFileHtml(lib[i]));
			}


			$('.library .root-library').hide();
			$('.library .sub-folders').show();
			$('.library .sub-folders').empty().append(dropzoneHtml).append(foldersHTML).append(files);
			$('.library .sub-folders').removeData("level");
			$('.library .sub-folders').attr("data-library", libraryType);
			$('.library .sub-folders').attr("data-current-folder", folder ? folder : null);
			$('.library .sub-folders').attr("data-current-subfolder", subFolder ? subFolder : null);
			var level = (subFolder != null ? "3" : (folder != null ? "2" : "1"));
			$('#file-manager').removeClass('level-1 level-2 level-3').addClass('level-' + level);
			$('.library .sub-folders').attr("data-level", level);

			if (subFolder != null || folder != null)
			{
				$('.back-button-icon').show();
			}
			else
			{
				$('.back-button-icon').hide();
			}

			$('#notification-bar').text('The page has been successfully loaded');

			$(".item-actions button").on("click mousedown touchstart", function(event)
			{
				preventDScallback = true;
			});
			$('div[data-type="folder"]:not([data-librarytype="template"]) .buttonCopyTemplate').remove();
			$('div[data-type="file"]:not([data-librarytype="template"]) .buttonCopyTemplate').remove();
			$('div.sub-folders[data-level="1"] div[data-type="folder"][data-librarytype="template"] .item-actions div[data-toggle="dropdown"]').remove();
			$('div[data-type="folder"][data-librarytype="template"] .buttonCopyTemplate').remove();
			initialiseList();
			initialiseDragSelect();
			initialiseDropzone();
			initialiseDropdown();
		},
		error : function()
		{
			$('#notification-bar').text('An error occurred');
		}
	});
}

function initialiseList()
{
	// to be overwrote in xx.js (library.js)
}
function destroyList()
{
	// to be overwrote in xx.js (library.js)
}
function buildFileHtml(file)
{
	
	var libraryType = currentRequest.libraryType;
	// to be overwrote in xx.js (library.js)
	var $html = $($('.item-template').find('.item').clone());
	$html.addClass('item-file');
	$html.attr('data-type', 'file');
	$html.attr('data-id', file.element_id);
	$html.attr('data-libraryType', libraryType);
	$html.find('.item-icon').html('<i class="file-icon"></i>');
	$html.find('.item-name').text(file.attr_headline);
	$html.find('.item-type').text('File');
	$html.find('.item-modified').text(file.last_update_date ? moment(file.last_update_date).format('YYYY-MMM-DD HH:mm') : "");
	$html.data('item', file);
	return $html;
}

function buildFolderHtml(folder, folderId)
{
	var libraryType = currentRequest.libraryType;
	var $html = $($('.item-template').find('.item').clone());
	$html.addClass('item-folder');
	$html.attr('data-id', folder.category_id);
	if (folderId)
	{
		$html.attr('data-subfolder', folder.attr_categoryname);
	}
	else
	{
		$html.attr('data-folder', folder.category_id);
		$html.attr('data-cnt', folder.cnt);
	}
	$html.attr('data-type', 'folder');
	$html.attr('data-libraryType', libraryType);

	$html.find('.item-icon').html('<i class="icon-folder"></i>');
	$html.find('.item-name').text(folder.attr_categoryname);
	$html.find('.item-type').text('Folder');
	$html.find('.item-modified').text("");
	return $html;
}

function initialiseDropdown() {
	$('[data-toggle="dropdown"]').dropdown();
}

function folderClickedAction(ele)
{
	var tableBody = $('.mainFileBody');
}

function createTemplateAction(event)
{
	createTemplate();
}

function createFolder()
{
	swal({
		title : 'Enter folder name',
		content : "input",
		type : "input",
		showCancelButton : true,
		confirmButtonText : 'Create folder',
		showLoaderOnConfirm : true,
	}, function(value)
	{
		if (value)
		{
			console.log(value)
			var type = $('[data-type]').attr('data-type');
			var library = $('[data-library]').attr('data-library');
			var level = $('[data-level]').attr('data-level');
			var parentId = 0;
			if (level === "2")
			{
				parentId = previousFolder;
			}
			var folderName = value;
			$.ajax({
				url : '/api/library/createFolder/' + library,
				data : {
					folderName : folderName,
					parentId : parentId,
					levels : level
				},
				type : 'POST',
			}).done(function(response)
			{
				loadContent(libraryType, $('[data-current-folder]').attr('data-current-folder'), $('[data-current-subfolder]').attr('data-current-subfolder'));
			})
		}
	});
}


function editFolder(element)
{
	var $item = $(element).closest('.item');
	var currentFolderName = $item.find('.item-name').text();
	swal({
		title : 'Enter folder name',
		content : {
			element : "input",
			attributes : {
				value : currentFolderName,
				type : "text",
			},
		},
		type : "input",
		showCancelButton : true,
		confirmButtonText : 'Update folder',
		showLoaderOnConfirm : true,
	}, function(value)
	{
		if (value)
		{
			console.log(value)
			var subFolder = $item.attr('data-subfolder');
			var folderId = $item.attr('data-folder');
			var library = $('[data-library]').attr('data-library');
			var level = $('[data-level]').attr('data-level');
			var parentId = 0;
			if (subFolder != null)
			{
				parentId = $('[data-current-folder]').attr('data-current-folder');
			}
			var folderName = value;
			$.ajax({
				url : '/api/library/updateFolder/' + library,
				data : {
					oldFolderName : currentFolderName,
					folderName : folderName,
					parentId : parentId,
					folderId : folderId,
					levels : level
				},
				type : 'POST',
			}).done(function(response)
			{
				swal({
					title : "Success",
					type : "success",
					text : "Folder updated!"
				}, function()
				{
					refreshContent();
				})
			})
		}
	});
}

function downloadItem(element)
{
	var $item = $(element).closest('.item');
	var itemId = $item.attr('data-id');
	var libraryType = $('[data-library]').attr('data-library');

	window.location = '/api/library/download/' + libraryType + "?item=" + itemId
}

function createTemplate(copyTravelDoc)
{

	var params = {};
	if (copyTravelDoc != undefined)
	{
		params.copyTravelDoc = copyTravelDoc;
	}

	$('body').modalmanager('loading');

	var $modal = $('#create-template-modal');

	$modal.load('/application/dialog/createTravelDoc.jsp', params, function()
	{
		$('.cust-fld-date', $modal).datepicker({
			autoclose : true,
			format : {
				toDisplay : function(date, format, language)
				{
					return moment(date).format(dateFormatPattern);
				},
				toValue : function(date, format, language)
				{
					return moment(date).format(dateFormatPattern);
				}
			}
		});
		$modal.modal({
			width : '660px',
			backdrop : 'static',
			keyboard : false
		});
	});

	$modal.css('opacity', '1.0');

	$modal.one('show.bs.modal', function()
	{

		$("#colorPicker > label input", $modal).on('change', function(e)
		{
			var selected = $('input[name="spectrumColor"]:checked', $modal).val();
			$modal.attr("spectrum", selected);
		}).change();

		$(".coverPageOption", $modal).click(function(e)
		{
			$(e.currentTarget).siblings().removeClass("active");
			$(e.currentTarget).addClass("active");

			var $scrollTo = $(e.currentTarget);
			var $myContainer = $("#coverPicker");
			var newScroll = $scrollTo.offset().left - $myContainer.offset().left + $myContainer.scrollLeft();
			var adjust = ($scrollTo.width() - $myContainer.width()) / 2;
			$myContainer.animate({
				scrollLeft : newScroll + adjust
			}, 500);

			$("#coverPageHiddenField", $modal).val($(e.currentTarget).attr("pageId"));
		});

		$modal.find('input, textarea').placeholder();


		// set validation rules - custom fields can be set on/off as required
		var proposalValidationRules = {
			custProposalTitle : {
				minlength : 2,
				maxlength : 100,
				required : true
			}
		};

		// setup validation
		var $validator = $('.form-create-traveldoc', $modal).validate({
			errorElement : 'span',
			errorClass : 'help-block',
			errorPlacement : function(error, element)
			{

			},
			submitHandler : function(form)
			{
				startLoadingForeground();
				$(form).ajaxSubmit({
					success : function(data)
					{
						$modal.modal('hide');

						$("#openNewProposal").attr("href", data);
						$(document).one('foregroundLoadingComplete', function()
						{
							var d = $("#proposalCreatedModal").modal('show');

							if (data === "#")
							{
								$("#openNewTravelDoc", d).remove();
							}
							$("#openNewTravelDoc, #closeBtn", d).one("click", function()
							{
								d.modal('hide');
								redrawTable();
							});
						});

						stopLoadingForeground();
					}
				});
			},
			rules : proposalValidationRules,
			highlight : function(element)
			{
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			unhighlight : function(element)
			{
				$(element).closest('.form-group').removeClass('has-error');
			},
			success : function(label, element)
			{
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			}
		});

		$('.form-steps', $modal).bootstrapWizard({
			'tabClass' : 'form-wizard',
			'onNext' : function(tab, navigation, index)
			{
				var $valid = $(".form-create-traveldoc").valid();
				if (!$valid)
				{
					$validator.focusInvalid();
					return false;
				}
				else
				{
					$('.form-steps', $modal).find('.form-wizard').children('li').eq(index - 1).addClass('complete');
					$('.form-steps', $modal).find('.form-wizard').children('li').eq(index - 1).find('.step').html('<i class="fa fa-check"></i>');
				}
			},
			'onTabClick' : function(tab, navigation, index)
			{
				return false;
			},
			'onTabShow' : function(tab, navigation, index)
			{
				var $total = navigation.find('li').length;
				var $current = index + 1;
				if ($current >= $total)
				{
					$('.form-steps', $modal).find('.modal-footer .next').hide();
					$('.form-steps', $modal).find('.modal-footer .finish').show();
					$('.form-steps', $modal).find('.modal-footer .finish').removeClass('disabled');
				}
				else
				{
					$('.form-steps', $modal).find('.modal-footer .next').show();
					$('.form-steps', $modal).find('.modal-footer .finish').hide();
				}
			}
		});

	});
}

var dropzone = null;

function initialiseDropzone()
{
	if (($('#file-manager').hasClass('level-2') || $('#file-manager').hasClass('level-3')) && (libraryType === "images" || libraryType === "pdf") && dropzone === null)
	{
		dropzone = new Dropzone(".page-content .library .content", {
			url : "/api/library/add-edit/" + libraryType,
			previewTemplate : "<div class=\"item dz-preview dz-file-preview\">\n" + "\t<div class=\"item-icon\"><img data-dz-thumbnail /></div>\n" + "\t<div class=\"item-name\" data-dz-name></div>\n" + "\t<div class=\"item-type\">File</div>\n" + "\t<div class=\"item-modified\">Now</div>\n" + "\t<div class=\"item-upload dz-progress\">\n" + "\t\t<span class=\"dz-upload library-upload-progress\"></span>\n" + "\t\t<span data-dz-errormessage></span>\n" + "\t</div>\n" + "</div>",
			previewsContainer : ".files",
			init : function()
			{
				this.on('sending', function(file, xhr, formData)
				{
					var currentFolder = $(this.element).find('[data-current-folder]').attr('data-current-folder');
					if (currentFolder == null || currentFolder == "")
					{
						currentFolder = previousFolder;
					}
					if ($(this.element).find('[data-level]').attr('data-level') === 3)
					{
						currentFolder = null;
					}
					formData.append('folder', currentFolder);
					if ($(this.element).find('[data-current-subfolder]').attr('data-current-subfolder') != null)
					{
						formData.append('subFolder', $(this.element).find('[data-current-subfolder]').attr('data-current-subfolder'));
					}
					formData.append(csrfTokenName, csrfTokenValue);
				});
				this.on('complete', function(file, response)
				{
					swal({
						title : "Success",
						text : "The file has been uploaded.",
						type : "info"
					}, function()
					{
						refreshContent();
					});
				})
			}
		});

		dropzone.on('totaluploadprogress', function(progress)
		{
			$(".library-upload-progress").width(progress + '%');
			// $(".library-upload-progress").text(progress + '%');
		})

		// dropzone.on('dragstart', function(){
		// if($('.files .item').length > 0) {
		// $('.dropzone').addClass('dropzone-visible');
		// }
		// });
		//
		dropzone.on('addedfile', function(file)
		{
			var scroll = $('.files').prepend($(file.previewElement)).scrollTop();
			$('.page-content').scrollTop(scroll);
		});
		dropzone.on('dragenter', function(event)
		{
			if ($('[data-level]').attr('data-level') > 1)
			{
				if (containsFiles(event))
				{
					$('.dropzone').addClass('dropzone-visible');
					dragEventTarget = event.target;
				}
				else
				{
					$('.dropzone').removeClass('dropzone-visible')
				}
			}
		});

		dropzone.on("drop", function(event)
		{
			$('.dropzone').removeClass('dropzone-visible')
		});
		//
		// dropzone.on('dragend', function(){
		// if($('.files .item').length > 0) {
		// $('.dropzone').removeClass('dropzone-visible');
		// }
		// })
		//
		// dropzone.on('dragleave', function(){
		// if($('.files .item').length > 0) {
		// $('.dropzone').removeClass('dropzone-visible');
		// }
		// })

		// $('body').on('dragenter', function (e) {
		// if(containsFiles(e))
		// {
		// $('.dropzone').addClass('dropzone-visible')
		// }
		// else
		// {
		//
		// }
		// });
		//
		// $('body').on('dragleave', function (e) {
		// e.preventDefault();
		// $('.dropzone').removeClass('dropzone-visible')
		// });
	}
}

var dragEventTarget;

$(document).ready(function()
{
	document.addEventListener("dragover", function(event)
	{
		if ($('[data-level]').attr('data-level') > 1)
		{
			if (containsFiles(event))
			{
				$('.dropzone').addClass('dropzone-visible');
				dragEventTarget = event.target;
			}
			else
			{
				$('.dropzone').removeClass('dropzone-visible')
			}
		}
	});

	document.addEventListener("dragleave", function(event)
	{
		if (document === event.fromElement)
		{
			$('.dropzone').removeClass('dropzone-visible')
		}
	});

	document.addEventListener("mouseenter", function(event)
	{
		$('.dropzone').removeClass('dropzone-visible')
	});

	document.addEventListener("drop", function(event)
	{
		$('.dropzone').removeClass('dropzone-visible')
	});

})

function containsFiles(event)
{
	if (event.dataTransfer.types)
	{
		for (var i = 0; i < event.dataTransfer.types.length; i++)
		{
			if (event.dataTransfer.types[i] == "Files")
			{
				return true;
			}
		}
	}
}

function fetchFolderSelectOptions($modal, response)
{
	var $select = $('form select[name="folder"]', $modal);

	$select.empty();
	$select.append($('<option>').prop('disabled', true).prop('selected', true).prop('value', null));
	$select.append($('<option>').val(0).text("Add new folder..."));
	for (var i = 0; i < response.length; i++)
	{
		$select.append($('<option>').val(response[i].category_id).text(response[i].attr_categoryname));
	}
}

function fetchSubFolderSelectOptions($modal, response)
{
	var $select = $('form select[name="subFolder"]', $modal);

	$select.empty();
	$select.append($('<option>').prop('selected', true).prop('value', 0).text("No Sub Folder"));
	$select.append($('<option>').val(0).text("Add new Sub Folder..."));
	for (var i = 0; i < response.length; i++)
	{
		if (response[i] != "")
		{
			$select.append($('<option>').val(response[i]).text(response[i]));
		}
	}
}


function usingIEorEdge()
{
	var ua = window.navigator.userAgent;

	var msie = ua.indexOf('MSIE ');
	if (msie > 0)
	{
		// IE 10 or older => return version number
		return true;
	}

	var trident = ua.indexOf('Trident/');
	if (trident > 0)
	{
		// IE 11 => return version number
		var rv = ua.indexOf('rv:');
		return true;
	}

	var edge = ua.indexOf('Edge/');
	if (edge > 0)
	{
		// Edge (IE 12+) => return version number
		return true;
	}

	// other browser
	return false;
}

function customizeFroalaEditor(e, editor)
{
	editor.toolbar.hide();

	editor.events.on('focus', function(e)
	{
		var $toolbar = $(event.target).parent().siblings('.fr-toolbar');
		if ($(event.target).closest('.panel-froala').offset().top > 0)
		{
			$toolbar.removeClass('fr-sticky-on');
			$toolbar.addClass('fr-sticky-off');
		}
		else
		{
			$toolbar.removeClass('fr-sticky-off');
			$toolbar.addClass('fr-sticky-on');
		}

		$(event.target).closest('.modal').removeClass('shake');
		this.toolbar.show();
	});
	editor.events.on('blur', function(e)
	{
		this.toolbar.hide();
		// $(e.target).parent('.fr-wrapper').siblings('.fr-toolbar').removeClass('fr-sticky-on').addClass('fr-sticky-off').css({
		// top : 0
		// });
	});

	// editor.events.on('drop', function(dropEvent)
	// {
	// event.preventDefault();
	// dropEvent.stopPropagation();
	// return false;
	// }, true);
}

function newFolderPrompt(e)
{
	var $select = $(this);
	if ($select.val() == "0")
	{
		$select.closest('form').find('#folderNameId').removeClass('hidden').find('#newFolderText').prop('required', true);
	}
	else
	{
		$select.closest('form').find('#folderNameId').addClass('hidden').find('#newFolderText').prop('required', false);
		$.get('/api/library/add-edit/' + libraryType + '/subfolder/' + $select.val(), function(response)
		{
			var $modal = $('#' + libraryType.toLowerCase() + '-library-add-edit-modal');
			fetchSubFolderSelectOptions($modal, response);
			$modal.find('#subFolderNameId').addClass('hidden');
		});
	}
}

function newSubFolderPrompt(e)
{
	var $select = $(this);
	if ($select.val() == "0" && this.options[this.selectedIndex].text != "No Sub Folder")
	{
		$select.closest('form').find('#subFolderNameId').removeClass('hidden').find('#newSubFolderText').prop('required', true);

	}
	else
	{
		$select.closest('form').find('#subFolderNameId').addClass('hidden').find('#newSubFolderText').prop('required', false);
	}
}

function closeAddFileButton()
{
	$('#add-file-button .icon-close').click();
}


function newDeleteLibraryFolderAction(e)
{
	var folderId = e.elementId;
	var folderName = e.name;
	var libraryType = currentRequest.libraryType;
	var $modal = $('#content-library-folder-delete-modal');
	$('[name="libraryType"]', $modal).val(libraryType);
	$('[name="folderId"]', $modal).val(folderId);
	$('#folderToBeDeleted', $modal).val(folderName);

	$.get('/api/library/add-edit/' + libraryType, function(response)
	{
		var $select = $('form select[name="transferTo"]', $modal);
		$select.empty();
		for (var i = 0; i < response.length; i++)
		{
			if (response[i].category_id != folderId)
			{
				$select.append($('<option>').val(response[i].category_id).text(response[i].attr_categoryname));
			}
			else
			{
				$('#folderToBeDeleted', $modal).val(response[i].attr_categoryname);
			}
		}

		$modal.modal({
			width : '50%'
		});
		$modal.modal('show');
	});
}
