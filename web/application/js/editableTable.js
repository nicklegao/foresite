$(document).ready(function()
{
	sortTables();

	/*
	 * var uninitilisedEditors = $('.editable-content');
	 * $.each(uninitilisedEditors, function(i, e) { //
	 * $(e).attr('contenteditable', true); CKEDITOR.replace(e, { customConfig :
	 * '/application/assets/plugins/ckeditor/custom/config-proposalEmailText.jsp'
	 * }); });
	 */

	setTimeout(function()
	{
		var uninitialisedEditors = $('.panel-froala');
		$.each(uninitialisedEditors, function(i, e)
		{
			// $(e).attr('contenteditable', true);
			$(e).on('froalaEditor.initialized', function(ev, editor)
			{
				customizeFroalaEditor(ev, editor)
			}).froalaEditor({
				enter: $.FroalaEditor.ENTER_P,
				scrollableContainer : '.mainPanel',
				key : 'DLAHYKAJOEc1HQDUH=='
			});
		});
	}, 200);


	// adding and removing tables components
	// http://jsfiddle.net/learner73/9dh5Lvb0/3/
	// add row

	$('body').on('click', '.add-row', function()
	{
		var tr = $(this).parents('.table-content').find('.table tbody tr:last');
		if (tr.length > 0)
		{
			var clone = tr.clone();
			clone.find(':text').val('');
			tr.after(clone);
		}
		else
		{
			$(this).parents('.table-content').find('.table tbody').append('<tr> <td></span><span class="remove remove-row">x</span></td><td> <input type="text" class="form-control"> </td></tr>');
		}

	});

	// delete row
	$('body').on('click', '.remove-row', function()
	{
		$(this).parents('tr').remove();
	});

	// add column
	$('body').on('click', '.add-col', function()
	{
		$(this).parent().find('.table thead tr').append('<th>Headings<span class="pull-left remove remove-col">x</span></th>');
		$(this).parent().find('.table tbody tr').append('<td><input type="text" class="form-control pull-left" value=""></td>');
		sortTables();
	});

	// remove column
	$('body').on('click', '.remove-col', function(event)
	{
		// Get index of parent TD among its siblings (add one for nth-child)
		var ndx = $(this).closest('th').index() + 1;
		// Find all TD elements with the same index
		$('th', '#dragableTbl').remove(':nth-child(' + ndx + ')');
		$('td', '#dragableTbl').remove(':nth-child(' + ndx + ')');
		sortTables();
	});

	// Hide Icon
	$('body').on('click', '.hide-icon', function(event)
	{
		// Get index of parent TD among its siblings (add one for nth-child)
		var icon = $(this).siblings('.picker-target');
		// Find all TD elements with the same index
		if ($(icon).hasClass('fa-none'))
		{
			$(this).text('Hide');
			$(icon).removeClass().addClass('picker-target fa-3x ifont fa fa-500px');
		}
		else
		{
			$(this).text('Show');
			$(icon).removeClass().addClass('picker-target fa-3x ifont fa fa-none');
		}

	});

	$('body').on('click', '.remove-customfield', function(event)
	{
		$(this).closest("p").remove();
	})

	// resizing table columns
	function resizeTable()
	{
		var thHeight = $("table#dragableTbl th:first").height();
		$("table#dragableTbl th").resizable({
			handles : "e",
			minHeight : thHeight,
			maxHeight : thHeight,
			minWidth : 40,
			resize : function(event, ui)
			{
				var sizerID = "#" + $(event.target).attr("id") + "-sizer";
				$(sizerID).width(ui.size.width);
			}
		});
	}
	;

	// sorting tables columns
	function sortTables()
	{
		console.log("event triggered");
		var $table1 = $('#dragableTbl');
		var $table1Tbody = $table1.find('tbody');
		var $table1Thead = $table1.find('thead');
		var startPos;
		var oldPos;
		// The sorting
		$table1Thead.sortable({
			axis : "x",
			items : 'th',
			containment : 'parent',
			cursor : 'move',
			helper : 'clone',
			distance : 5,
			opacity : 0.5,
			placeholder : 'ui-state-highlight',
			start : function(event, ui)
			{
				startPos = $table1Thead.find('th').index(ui.item);
				oldPos = startPos;
			},
			change : function(event, ui)
			{
				// Get position of the placeholder
				var newPos = $table1Thead.find('th').index($table1Thead.find('th.ui-state-highlight'));

				// If the position is right of the original position, substract
				// it by one in cause of the hidden th
				if (newPos > startPos)
					newPos--;

				// move all the row elements
				// console.log('Move: 'oldPos+' -> '+newPos);
				$table1Tbody.find('tr').find('td:eq(' + oldPos + ')').each(function()
				{
					var tdElement = $(this);
					var tdElementParent = tdElement.parent();
					if (newPos > oldPos)// Move it the right
						tdElementParent.find('td:eq(' + newPos + ')').after(tdElement);
					else
						// Move it the left
						tdElementParent.find('td:eq(' + newPos + ')').before(tdElement);
				});
				oldPos = newPos;
			}
		});
	}

	// drag and drop products on table
	$(".customField").draggable({
		revert : 'invalid',
		drag : function()
		{
			$('.product-item').addClass('droppable-highlight');
			$('#dragableTbl thead').addClass('droppable-highlight');
		},
		stop : function()
		{
			$('.product-item').removeClass('droppable-highlight');
			$('#dragableTbl thead').removeClass('droppable-highlight');
		},
	});

	$("#dragableTbl tbody .product-item").find("[data-customfieldvalue]").each(function()
	{
		var customField = $(this).data("customfieldvalue");
		var fieldName = $('[sortable="' + customField + '"]').find('.sort').text() + " Custom Field";
		$(this).find('.customfield-label').text(fieldName);
	});

	$("#dragableTbl tbody .product-item").droppable({
		accept : '.customField',
		drop : function(e, ui)
		{
			var drag_element = ui.draggable[0];

			$(drag_element).css({
				'position' : ''
			});

			var customField = $(drag_element).attr('sortable');

			$('.droppable-highlight').removeClass('droppable-highlight');

			$(drag_element.helper).remove(); // destroy clone
			$(drag_element.draggable).remove(); // remove from list

			var dragText = $(drag_element).find('.sort').text();
			var dragType = $(drag_element).find('.type').text();

			var tdHtml = '<p data-customfieldvalue="' + customField + '"><span class="customfield-label">' + $(drag_element).find('.sort').text() + ' Custom Field</span><span class="remove remove-customfield">X</span></p>'

			var tmpTdHtml = (tdHtml);

			var sz = $('#dragableTbl > tbody > tr').length;

			for (var i = 0; i < sz; i++)
			{
				var indx = i + 1;
                $('#dragableTbl tbody tr:nth-child(' + indx + ') .product-item .notes').before(tmpTdHtml);

			}
			sortTables();
			$(drag_element).remove();
			redrawTable();
		}
	});

	$("#dragableTbl thead tr").droppable({
		accept : '.customField',
		drop : function(e, ui)
		{

			var drag_element = ui.draggable[0];

			$(drag_element).css({
				'position' : ''
			});


			$(drag_element.helper).remove(); // destroy clone
			$(drag_element.draggable).remove(); // remove from list

			var dragText = $(drag_element).find('.sort').text();
			var dragType = $(drag_element).find('.type').text();

			$('.droppable-highlight').removeClass('droppable-highlight');

			var customField = $(drag_element).attr('sortable');
            var fieldIcon = 'fa-none';
            if (dragType.toLowerCase() === 'currency') {
                fieldIcon = 'moneytaryValue';
            }

            var thHtml = '<th style="position: relative;" class="" data-type="' + customField + '" data-customfieldtype="' + dragType + '" data-alignment="left">';
            thHtml += '<div>';
            thHtml += '<div class="dropdown-group">';
            thHtml += '<span data-toggle="dropdown"class="options options-column"><i class="fa fa-bars"></i></span>';
            thHtml += '<ul class="dropdown-menu">';
            thHtml += '<li><a href="#" class="alignColumnLeft">Left Align</a></li>';
            thHtml += '<li><a href="#" class="alignColumnCenter">Center Align</a></li>';
            thHtml += '<li><a href="#" class="alignColumnRight">Right Align</a></li>';
            thHtml += '</ul>';
            thHtml += '</div>';
            thHtml += '<span class=" remove remove-col">X</span>';
            thHtml += '</div>'
            thHtml += '<i class="fa picker-target ifont ' + fieldIcon + '" data-pricingicon="' + fieldIcon + '"></i><input class="form-control input" value="' + dragText + '"></th>';

			if (dragType == "dropdown")
			{
				var tdHtml = '<td><p>Dropdown</p></td>'
			}
			else if (dragType == "currency")
			{
				var tdHtml = '<td><p>$99</p></td>'
			}
			else if (dragType == "date")
			{
				var tdHtml = '<td><p>01/01/2017</p></td>'
			}
			else if (dragType == "text")
			{
				var tdHtml = '<td><p>Text</p></td>'
			}

			var tmpThHTML = (thHtml);
			var tmpTdHtml = (tdHtml);
            var tdHtml = '<td>';
			$("#dragableTbl thead tr th:first").after(tmpThHTML);

			var sz = $('#dragableTbl > tbody > tr').length;

			for (var i = 0; i < sz; i++)
			{
				var indx = i + 1;
                if (indx == 1 || indx == 2) {
                    $('#dragableTbl tbody:first tr:nth-child(' + indx + ') td:first').after(tmpTdHtml);
                } else {
                    $('#dragableTbl tbody:first tr:nth-child(' + indx + ') td:first').after(tdHtml);
                }
			}

            $('#subTotalTbl tr').each(function () {
                $(this).find('td:first').after('<td>')
            });

			$('#dragableTbl thead tr th > i').on('iconpickerSelected', function (e) {
                $(this).closest('.picker-target').get(0).className = 'picker-target fa-3x ifont ' +
                    // e.iconpickerInstance.options.iconBaseClass + ' ' +
                    e.iconpickerInstance.options.fullClassFormatter(e.iconpickerValue);
                $(this).closest('.picker-target').attr('data-pricingicon', e.iconpickerInstance.options.fullClassFormatter(e.iconpickerValue));
            }).iconpicker({
                icons: $.merge([
                        'fa-none',
                        'moneytaryValue'],
                    $.iconpicker.defaultOptions.icons),
                placement: 'rightBottom'
            });

			sortTables();
			$(drag_element).remove();
			redrawTable();
		}
	});


	// font awesome icon picker
	// https://github.com/farbelous/fontawesome-iconpicker

	$('#dragableTbl thead tr th > i.fa').iconpicker({
        icons: $.merge(['fa-none', 'moneytaryValue'], $.iconpicker.defaultOptions.icons),
        placement: 'rightBottom'
	});

	$('.ifont').on('iconpickerSelected', function(e)
	{
		$(this).closest('.picker-target').get(0).className = 'picker-target fa-3x ifont ' +
		// e.iconpickerInstance.options.iconBaseClass + ' ' +
		e.iconpickerInstance.options.fullClassFormatter(e.iconpickerValue);
		$(this).closest('.picker-target').attr('data-pricingicon', e.iconpickerInstance.options.fullClassFormatter(e.iconpickerValue));
	});

	$('body').on('click', '.tableStyle', function()
	{
		var styleId = $(this).data('styleId');
		$.ajax({
			url : "/editor/loadStyle",
			data : {
				styleId : styleId
			}
		}).success(function()
		{

		});
	});

	$('body').on('keyup', '[data-mapping]', function()
	{
		var mapping = $(this).data('mapping');
		$('[data-mapping="' + mapping + '"]').not(this).val($(this).val());
	});

	$('body').on('click', '.alignColumnLeft', function()
	{
		$(this).closest('[data-type]').attr('data-alignment', 'left');
		updateSelected($(this).closest('[data-type]'));
		var ndx = $(this).closest('th').index() + 1;
		$('th', '#dragableTbl').filter(':nth-child(' + ndx + ')').css('text-align', 'left');
		$('td', '#dragableTbl').filter(':nth-child(' + ndx + ')').css('text-align', 'left');
	});

	$('body').on('click', '.alignColumnCenter', function()
	{
		$(this).closest('[data-type]').attr('data-alignment', 'center');
		updateSelected($(this).closest('[data-type]'));
		var ndx = $(this).closest('th').index() + 1;
		$('th', '#dragableTbl').filter(':nth-child(' + ndx + ')').css('text-align', 'center');
		$('td', '#dragableTbl').filter(':nth-child(' + ndx + ')').css('text-align', 'center');
	});

	$('body').on('click', '.alignColumnRight', function()
	{
		$(this).closest('[data-type]').attr('data-alignment', 'right');
		updateSelected($(this).closest('[data-type]'));
		var ndx = $(this).closest('th').index() + 1;
		$('th', '#dragableTbl').filter(':nth-child(' + ndx + ')').css('text-align', 'right');
		$('td', '#dragableTbl').filter(':nth-child(' + ndx + ')').css('text-align', 'right');
	});

	$('[data-alignment]').each(function()
	{
		updateSelected(this);
	})

    $('#dragableTbl').on('iconpickerShown', 'thead tr th:last > i', function () {
        $('.table-content').scrollLeft($('.table-content').width());
    });

    // $(document).on('scroll', '.table-content', function () {
    //     $('.table-content').find('#scrollH').hide()
    // });
    //
    // $(window).resize(function () {
    //     checkScrollTable();
    // });
    //
    // function checkScrollTable() {
    //     if ($('.table-content').width() < $('#dragableTbl').width()) {
    //         $('[id="scrollH"]', $(this)).css('display', 'flex');
    //     }
    //     else {
    //         $('[id="scrollH"]', $(this)).hide();
    //     }
    // }

});

function updateSelected(element)
{
	$(element).find('.selected').removeClass('selected');
	if ($(element).attr('data-alignment') != null)
	{
		$(element).find(".alignColumn" + $(element).attr('data-alignment').capitalize()).addClass('selected');
	}
}

String.prototype.capitalize = function()
{
	return this.charAt(0).toUpperCase() + this.slice(1);
}