
var v = function(){console.log('testing ie ve load');return 'test'}
var test = v();
icon["row"] = 'far fa-plus-square';
validAddLevel["row"] = 'both';
componentName["row"] = 'Row';
componentDescription["row"] = 'Place content elements inside the row';
validTabs["row"] = ["basic-elements", "other-elements"];
validProperties["newrow"] = {
		"general" : [  "columnContentPosition"],
		"layout" : [ "*" ],
		"design" : [ "layout", "borderColor", "borderStyle", "borderRadius" ],
	};

// the widget definition, where "custom" is the namespace,
// "newrow" the widget name
$.widget( "custom.newrow", {
    // default options
    options: {
    	properties: {
    		layout : '1/1',
    		column_gutter: 'gtr-0',
    		textcolor_style: 'inherit',
    		row_height: 'auto',
    		column_content_position: 'top'
    	},
        inner: false
    },

    // The constructor
    _create: function() {
    	console.log('row.js');
        var $target = $(this.element);
        $target.data('ve_type','newrow');
        $target.addClass('ve_element ve_row ' + (this.options.inner?'ve_row_inner':''));
         var $container = $('<div>');
         $container.addClass('ve_section-container container');
        this.element.container = $container;
        var row_container = $('<div>').addClass('row ve_element_container ve_row_container row-col-content-flex ' + (this.options.inner?' ve_row_inner_container':''));

        if (this.options.children && this.options.children.length>0) {
        	var viewer = this.options.viewer;
            this.options.children.forEach(function(element, index){
                console.log('element: '+element.type);
                if(element.type=="colElement") {
                    var newCol = $('<div>').addClass('ve_element ve_col');
                    if(!element.options){
                    	element.options = [];
                    }
                    element.options.children = element.children;
                    element.options.properties = element.properties;
                    element.options.inner = element.inner;
                    element.options.viewer = viewer;
                    row_container.append(newCol);
                    newCol.colElement(element.options);
                }
            });
        }
        else {
            var newCol = $('<div>').addClass('ve_element ve_col '+(this.options.inner?'ve_col_inner':''));
            newCol.colElement({inner:this.options.inner});
            row_container.append(newCol);
        }

         $container.append(row_container);
         $target.append($container);
// $target.append(row_container);
        // Bind click events on the changer button to the random method
        this._on( this.changer, {
            // _on won't call random when widget is disabled
            click: "random"
        });
        this._refresh();
    },

    // Called when created, and later when changing options
    _refresh: function() {
    	 var ve_element = $(this.element);

    	 var properties = this.options.properties;
         
         if(properties){
        	 if(properties.full_width) {
        	 	this.element.container[(properties.full_width.toString()=="true"?'remove':'add')+'Class']('container');
			 }
        	 else {
        		 this.element.container.addClass('container');
        	 }
        	 if(getLayout(ve_element) != properties.layout){
        		 updateLayout(ve_element);
        	 }
        	 if(properties.column_content_position){
        		 this.element.container.find('.ve_row_container').removeClass('col-content-top').removeClass('col-content-middle').removeClass('col-content-bottom').addClass('col-content-'+properties.column_content_position);
        	 }
        	 setProperties(ve_element, properties);
         }
        // Trigger a callback/event
        rowSortable(this.element.closest('.ve_edit-mode'));

		if(this.options.inner) {
			innerRowSortable(this.element.closest('.ve_col_container'));
		}
        this._trigger( "change" );
    },

    // A public method to change the color to a random value
    // can be called directly via .newrow( "random" )
// random: function( event ) {
// var colors = {
// red: Math.floor( Math.random() * 256 ),
// green: Math.floor( Math.random() * 256 ),
// blue: Math.floor( Math.random() * 256 )
// };
//
// // Trigger an event, check if it's canceled
// if ( this._trigger( "random", event, colors ) !== false ) {
// this.option( colors );
// }
// },

    // Events bound via _on are removed automatically
    // revert other modifications here
    _destroy: function() {
        // remove generated elements
        // this.changer.remove();

        this.element
            .removeClass( "custom-newrow" )
            .enableSelection()
            .css( "background-color", "transparent" );
    },

    // _setOptions is called with a hash of all options that are changing
    // always refresh when changing options
    _setOptions: function() {
        // _super and _superApply handle keeping the right this-context
        this._superApply( arguments );
        this._refresh();
    },

    // _setOption is called for each individual option that is changing
    _setOption: function( key, value ) {
        this._super( key, value );
    },
    
    getOptions: function() {
    	return this.options;
    }
});

function getLayout ($row) {
	var rowLayout = [];
	$row.find('.ve_row_container').eq(0).children('.ve_col').each(function(){
		rowLayout.push(toFraction($(this).data('custom-colElement').options.properties.column_width));
	});
	return rowLayout.join(' + ')
}

function setLayout($row) {
	$row.newrow('instance').options.properties.layout = getLayout($row);
}

function updateLayout($ve_row){
	var elements = buildElements($ve_row);
	var layout = $ve_row.data('custom-newrow').options.properties.layout.split(' + ');
	
	var lastColumn;
	var orphans = [];
	$.each($ve_row.find('.ve_element.ve_col').eq(0).siblings('.ve_element.ve_col').addBack(), function(index, column){
		var columnLayout = layout[index]; 
		var $column = $(column);
		if(columnLayout){
			$column.data('custom-colElement').options.properties.column_width = (columnLayout.split('/')[0]*12)/columnLayout.split('/')[1];
			$column.colElement();
			lastColumn = $column;
		}
		else {
			$.merge(orphans,buildElements($column));
			$column.colElement('deleteColumnElement');
		}
	});
	
	var diff = layout.length - elements.length;
	if(diff>0) {
		var container = lastColumn.closest('.ve_row_container');
		var i;
		for(i=0;i<diff;i++) {
			var columnLayout = layout[elements.length+i];
			var ve_element = $('<div>');
			container.append(ve_element);
			ve_element.colElement({
				properties : {
					column_width : (columnLayout.split('/')[0]*12)/columnLayout.split('/')[1]
				},
				inner : lastColumn.data('custom-colElement').options.inner
			})
		}
	}
	else if(diff<0) {
		var container = lastColumn.find('>.ve_col_container');
		if(orphans.length>0){
			lastColumn.removeClass('ve_col_empty');
		}
		orphans.forEach(function(element, index){
			var ve_element = $('<div>');
			container.append(ve_element);
			ve_element[element.type](element);
		});
	}
}

function createLayoutTable($ve_row, layout) 
{
	var select_lg = [];
	var select_md = [];
	var select_sm = [];
	var select_xs = [];
	if($ve_row) {
		convertToJSON($ve_row).elements.forEach(function(col, index){
			select_lg.push(getColumnSelect(getColumnWidth_lg(col.properties)).attr('index',index).attr('size','lg'));
			select_md.push(getColumnSelect(getColumnWidth_md(col.properties), true).attr('index',index).attr('size','md'));
			select_sm.push(getColumnSelect(getColumnWidth_sm(col.properties)).attr('index',index).attr('size','sm'));
			select_xs.push(getColumnSelect(getColumnWidth_xs(col.properties)).attr('index',index).attr('size','xs'));
		});
	}
	else {
		var prop = {};
		var cols = layout.split(' + ');
		cols.forEach(function(col, index){
			prop.column_width = (col.split('/')[0]*12)/col.split('/')[1];
			select_lg.push(getColumnSelect(getColumnWidth_lg(prop), false, true).attr('index',index).attr('size','lg'));
			select_md.push(getColumnSelect(getColumnWidth_md(prop), true, true).attr('index',index).attr('size','md'));
			select_sm.push(getColumnSelect(getColumnWidth_sm(prop), false, true).attr('index',index).attr('size','sm'));
			select_xs.push(getColumnSelect(getColumnWidth_xs(prop), false, true).attr('index',index).attr('size','xs'));
		});
	}
	
	$('.ve_size-responsive-lg').children().remove();
	$('.ve_size-responsive-md').children().remove();
	$('.ve_size-responsive-sm').children().remove();
	$('.ve_size-responsive-xs').children().remove();
	select_lg.forEach(function(select, index){
		$('.ve_size-responsive-lg').append(select);
	});
	select_md.forEach(function(select, index){
		$('.ve_size-responsive-md').append(select);
	});
	select_sm.forEach(function(select, index){
		$('.ve_size-responsive-sm').append(select);
	});
	select_xs.forEach(function(select, index){
		$('.ve_size-responsive-xs').append(select);
	});
}

function getColumnSelect(width, isDefault, updated){
	var input;
	if(isDefault){
		input = $('<input disabled>');
		input.attr('title', 'Default value from row layout')
		input.val(toFraction(width));
	}
	else {
		input = $('<select class="col-layout-responsive" '+(updated?'updated="true"':'')+'>');
		var i;
		for(i=0;i<13;i++) {
			var option = $('<option>').val(i==0?'inherit':i).text(i==0?'Inherit from smaller':toFraction(i));
			input.append(option);
		}
		input.val(width);
	}
	
	
	var wrapper = $('<div>');
	wrapper.addClass('select-wrapper').append(input);
	return wrapper;
}

function getColumnWidth_lg(properties){
	if(properties.column_col_lg){
		return properties.column_col_lg;
	}
	return getColumnWidth_md(properties);
}

function getColumnWidth_md(properties){
	return properties.column_width;
}

function getColumnWidth_sm(properties) {
	if(properties.column_col_sm){
		return properties.column_col_sm;
	}
	return 12;
}

function getColumnWidth_xs(properties) {
	if(properties.column_col_xs){
		return properties.column_col_xs;
	}
	return 12;
}













