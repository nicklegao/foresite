//Temporary
icon["column"] = '';
validAddLevel["column"] = 'none';
componentName["column"] = 'Column';
validTabs["column"] = [""];
validProperties["colElement"] = {
		"design":["*"]
};

$.widget( "custom.colElement", {

    options: {
    	properties: {
    		column_width : 12,
    		textcolor_style : 'inherit',
    	}
    },

    _create: function() {
        var $target = $(this.element);
        $target.addClass('ve_element ve_col ')
        $target.addClass(this.options.inner?'ve_col_inner':'');
        $target.data('ve_type','colElement');
        var viewer = this.options.viewer; 
        if(!viewer) {
	        var veControl = $('<div>').addClass('vecontrol-container');
	        var veControl_in = $('<div>').addClass('vecontrol-in');
	        var veControl_out = $('<div>').addClass('vecontrol-out');
	        var veControl_append = $('<div>').addClass('vecontrol-append');
	        var rowControl = $('<div>').addClass('ve-control ve-row-control');
	        var rowControl_reposition = $('<span>').addClass('ve_row-' + (this.options.inner?'inner-':'')  + 'handle').prepend($('<a>').text((this.options.inner?' Inner':'')+' Row').prepend($('<i>').addClass('fa fa-arrows-alt')));
	
	        var editRow = $('<a>').attr('title', 'Edit'+(this.options.inner?' Inner':'')+' Row').append($('<i>').addClass('fa fa-edit'));
	        this.editRow = editRow.button();
	        this._on(this.editRow, {
	        	click : "editRowElement"
	        });
	        
	        var changeLayout = $('<a>').attr('title', 'Change Layout').append($('<i>').addClass('fa fa-bars'));
	        this.changeLayout = changeLayout.button();
	        this._on(this.changeLayout, {
	        	click : "changeRowLayout"
	        });
	        
	        var cloneRow = $('<a>').attr('title', 'Clone'+(this.options.inner?' Inner':'')+' Row').append($('<i>').addClass('fa fa-clone'));
	        this.cloneRow = cloneRow.button();
	        this._on(this.cloneRow, {
	            click: "cloneRowElement"
	        });
	        
//	        if(!this.options.inner) {
//	        	var copyRow = $('<a>').attr('title', 'Copy Row').append($('<i>').addClass('fa fa-copy'));
//	            this.copyRow = copyRow.button();
//	            this._on(this.copyRow, {
//	                click: "copyRowElement"
//	            });
//	            
//	            var pasteRow = $('<a>').attr('title', 'Paste Row').append($('<i>').addClass('fa fa-paste'));
//	            this.pasteRow = pasteRow.button();
//	            this._on(this.pasteRow, {
//	                click: "pasteRowElement"
//	            });
//	        }
	        
	        var deleteRow = $('<a>').attr('title', 'Delete'+(this.options.inner?' Inner':'')+' Row').append($('<i>').addClass('fa fa-times'));
	        this.deleteRow = deleteRow.button();
	        this._on(this.deleteRow, {
	            click: "deleteRowElement"
	        });
	
	        var rowAdvancedCtrl = $('<a>').attr('title', 'Show Row Controls').append($('<i>').addClass('fa fa-caret-right'));
	
	        this.showHideRowAdvanced = rowAdvancedCtrl.button();
	        this._on(this.showHideRowAdvanced, {
	            click: "showHideAdvanced"
	        });
	
	        var showRowControls = $('<span>').addClass('show-hide-ctrl').append(rowAdvancedCtrl);
	
	        var addColumn = $('<a>').attr('title', 'Add new'+(this.options.inner?' Inner':'')+' Column').append($('<i>').addClass('fa fa-plus'));
	        this.addCol = addColumn.button();
	        this._on( this.addCol, {
	            click: "columnAdd"
	        });
	
	        var rowControl_option = $('<span>').addClass('ve-advanced').append(editRow).append(changeLayout).append(addColumn).append(cloneRow).append(deleteRow);//.append(copyRow).append(pasteRow);
	        rowControl.append(rowControl_reposition).append(rowControl_option).append(showRowControls);
	
	        var colControl = $('<div>').addClass('ve-control ve-col-control active');
	        var colControl_reposition = $('<span>').addClass('ve_col-' + (this.options.inner?'inner-':'')  + 'handle').prepend($('<a>').text((this.options.inner?' Inner':'')+' Column').prepend($('<i>').addClass('fa fa-arrows-alt')));
	
	        var editColumn = $('<a>').attr('title', 'Edit'+(this.options.inner?' Inner':'')+' Column').append($('<i>').addClass('fa fa-edit'));
	        this.editCol = editColumn.button();
	        this._on( this.editCol, {
	            click: "columnEdit"
	        });
	        
//	        var prependToColumn = $('<a>').attr('title', 'Prepend to'+(this.options.inner?' Inner':'')+' Column').append($('<i>').addClass('fa fa-plus'));
//	        this.prependToColumn = prependToColumn.button();
//	        this._on(this.prependToColumn, {
//	            click: "prepend_ve_element"
//	        });
	        
	        var deleteColumn = $('<a>').attr('title', 'Delete'+(this.options.inner?' Inner':'')+' Column').append($('<i>').addClass('fa fa-times'));
	        this.deleteColumn = deleteColumn.button();
	        this._on(this.deleteColumn, {
	            click: "deleteColumnElement"
	        });
	
	        var colAdvancedCtrl = $('<a>').attr('title', 'Show Column Controls').append($('<i>').addClass('fa fa-caret-right'));
	        this.showHideColAdvanced = colAdvancedCtrl.button();
	        this._on(this.showHideColAdvanced, {
	            click: "showHideAdvanced"
	        });
	
	        var showColumnControls = $('<span>').addClass('show-hide-ctrl').append(colAdvancedCtrl);
	//     	  addColumn.on('click', function(){
	//     		 var newCol = $('<div>').addClass('col col-'+this.options.col);
	//     		 newCol.colElement();
	//     		 $(this).closest('.ve_row_container').append(newCol);
	// 		  });
	        var colControl_option = $('<span>').addClass('ve-advanced').append(editColumn).append(deleteColumn);//.append(prependToColumn);
	        colControl.append(colControl_reposition).append(colControl_option).append(showColumnControls);
	
	        veControl_out.append(rowControl).append(colControl);
	
	        var appendElement = getInsertBlockBtn();//$('<i>',{'class':'fa fa-plus addElement'});
//	        this.appendElement = appendElement.button();
//	        this._on(this.appendElement, {
//	            click: "append_ve_element"
//	        });
	        this._on(appendElement.find('.dropdown-menu li'), {
	        	click: "insert_content_block"
	        });
	        veControl_in.append(appendElement);
	
	        var appendToColumn = getInsertBlockBtn(true);
//	        this.appendToColumn = appendToColumn.button();
//	        this._on(this.appendToColumn, {
//	            click: "append_ve_element"
//	        });
	        this._on(appendToColumn.find('.dropdown-menu li'), {
	        	click: "insert_content_block"
	        });
	        veControl_append.append(appendToColumn);
	
	        veControl.append(veControl_out).append(veControl_in).append(veControl_append);
	        $target.append(veControl);
        }


        var col_container = $('<div>').addClass('ve_element_container ve_col_container');
        
        if(!viewer) {
        	col_container.append($('<div class="droppable-holder"><p>Drop your block here</p></div>'));
        }

        if (this.options.children && this.options.children.length > 0)
        {
            this.options.children.forEach(function(element, index)
            {
                if (element.type == "newrow")
                {
                    var ve_row = $('<div>');
                    if(!element.options){
                    	element.options = [];
                    }
                    element.options.children = element.children;
                    element.options.properties = element.properties;
                    element.options.inner = element.inner;
                    ve_row.newrow(element.options);
                    col_container.append(ve_row);
                }
                else if (element.type !== null && element.type !== '')
                {
                	  var elementBody = $("<div class=\"content-holder sortable\" data-type=\"" + element.type + "\" style=\"width: 100%; height: auto;\">");
                	  
                	  var colContent = element.colContent;
                	  if (colContent != null) {
                        	if(colContent.elements) {
        	                	colContent.elements.forEach(function(component, index){
        	                		var $component = $('<div>');
        	                		elementBody.append($component);
        	                		$component[component.type]({
        	                            children : component.children,
        	                            properties : component.properties,
        	                            inner : component.inner,
        	                            viewer : viewer
        	                        });
        	                	});
                        	}
        				}
                	  else {
                		  var elementContainer;
                    	  if (element.type == "block-video" || element.type == "content-library-videos") {
                        	  if(!viewer || (element.data != null && element.data != 'null')) {
                        		  if(element.data != null && element.data != 'null') {
                        			  elementContainer = $("<div class=\"videoContainer\" style=\"vertical-align: top;display: inline-block;text-align: center;width: 100%;\"><div class=\"iframe-wrapper\"><iframe src=\""+element.data+"\" ></iframe></div></div>");
    	                    	  } else {
    	                    		  elementContainer = $('<div class="videoContainer" style="vertical-align: top;display: inline-block;text-align: center;width: 100%;"><i class="fab fa-youtube fa-5x"></i><div class="button-row"><input id="videoCode" style="border: 1px solid lightgray;margin: 20 0 0 0;text-align:  center;border-radius:  6px;" type="text" placeholder="Video URL" minlength="11" maxlength="110"/></div><div class="button-row"><button style="display:none;">Library</button><button id="videoSaveButton" class="btn btn-default" >Save</button></div></div>');
    	                    	  }
                        	  }
                          } else if(!viewer || (element.data != null && element.data.indexOf('uploadImageButton')<0)) {
                        	  elementContainer = $(element.data);
                          }
                    	  
                    	  elementBody.append(elementContainer);
                          
                          $target.removeClass('ve_col_empty');
                          
                          if(element.properties) {
                        	  elementBody.data('properties', {properties: element.properties});
                        	  setProperties(elementContainer, element.properties);
                          }
                          
                	  }
                	  if(!viewer) {
                    	  elementBody.insertBefore(col_container.find('.droppable-holder'));
                      }
                      else{
                    	  col_container.append(elementBody);
                      }
                }
            });
        }
        else
        {
            $target.addClass('ve_col_empty');
        }

        $target.append(col_container);


        this._refresh();
    },

    _refresh: function() {
    	var ve_element = $(this.element);
    	var properties = this.options.properties;
    	if(properties){
    		setProperties(ve_element, properties);
    	}

    	columnSortable(this.element.closest('.ve_row .row:not(.ve_row_inner_container)'));

        if(this.options.inner) {
            innerColumnSortable(this.element.closest('.ve_row_inner_container'));
        }

        elementSortable(this.element.find('.ve_col_container'));

        this._trigger( "change" );
    },

    editRowElement: function( event ) {
    	 var target_row = $(this.element).closest('.ve_element.ve_row');
    	showEditPanel(target_row);
    },
    
    changeRowLayout: function( event ) {
    	var target_row = $(this.element).closest('.ve_element.ve_row');
    	showEditPanel(target_row, undefined, true);
    },
   
    cloneRowElement : function( event) {
        var target_row = $(this.element).closest('.ve_element.ve_row');
        var contentHolder = target_row.closest('.content-holder');
        var cloneContentHolder = $("<div class=\"content-holder sortable\" data-type=\"block-columns\" style=\"width: 100%; height: auto;\">");
        cloneContentHolder.insertAfter(contentHolder);
        var data = convertToJSON(contentHolder);
        data.elements.forEach(function(component, index){
    		var $component = $('<div>');
    		cloneContentHolder.append($component);
    		$component[component.type]({
                children : component.children,
                properties : component.properties,
                inner : component.inner
            });
    	});
        initFroalaEditor();
        $.each($('.content-holder:not([data-type=block-columns])', cloneContentHolder), function(j, content)
        {
            if ($(content).attr('data-type')) {
            	var type = $(content).attr('data-type');
            	var editable = type=='block-video' || type=='content-library-videos';
                var toolbar = $('<div class="touch-container">');
            	toolbar.append($('<span class="block-handle"><a><i class="fas fa-arrows-alt"></i> '+ve_elements[type]+'</a></span>'));
                var contentHolder = $('<span class="content-holder-toolbar">');
                var toolbarHolder = $('<div class="toolbar-button-holder">');
                var save = $('<div type="button" data-toggle="modal" data-target="#saveContentModal" class="cht-btn cht-button-save">').append($('<i class="fas fa-save"></i>'));
                var view = $('<div type="button" class="cht-btn cht-button-hide">').append($('<i class="fas fa-eye-slash"></i>'));
                var move = $('<div class="cht-btn cht-button-move">').append($('<i class="fas fa-arrows-alt"></i>'));
                var edit = $('<div type="button" id="edit-content-block" data-ve_type="'+ve_widgets[type]+'" class="cht-btn cht-button-edit'+(editable?"":"hidden")+'">').append($('<i class="fa fa-edit"></i>'));
                
                var trash = $('<div type="button" id="delete-content-block" class="cht-btn cht-button-delete">').append($('<i class="fas fa-trash-alt"></i>'));
                
                toolbarHolder.append(save).append(view).append(move).append(edit).append(trash);
                contentHolder.append(toolbarHolder);
                toolbar.append(contentHolder);
                $(content).append(toolbar); 
                
                if(editable) {
                	var properties = $(content).data('properties');
                	edit[ve_widgets[type]](properties?properties : DEFAULTS[ve_widgets[type]]);
                }
                
            }

        });
    },
    
    copyRowElement : function( event) {
        copyRow();
    },
    
    pasteRowElement : function( event) {
//    	navigator.clipboard.readText().then(text => pasteRow(text));
    	navigator.clipboard.readText().then(function (text) {
    		  return pasteRow(text);
    		});
//        var target_row = $(this.element).closest('.ve_element.ve_row');
//        var target_widget = target_row.newrow('instance');
//        var ve_row_clone = $('<div>');
//        ve_row_clone.insertAfter(target_row);
//        var children = buildElements(target_row);
//        ve_row_clone.newrow({children: children, properties: target_widget.options.properties, inner: target_widget.options.inner});
    },

    deleteRowElement : function( event) {
        var delete_row = $(this.element).closest('.ve_element.ve_row');
        if(delete_row.hasClass('ve_row_inner') && delete_row.siblings('.ve_element').length==0) {
            delete_row.closest('.ve_element.ve_col').addClass('ve_col_empty');
        }
        delete_row.parent().remove();
    },

    columnAdd: function( event ) {
        var newCol = $('<div>');
        newCol.colElement({inner:this.options.inner});
        $(this.element).closest('.ve_row_container').append(newCol);
        var rowProperties = $(this.element).closest('.ve_element.ve_row').data('custom-newrow').options.properties;
        rowProperties.layout = rowProperties.layout + ' + 1/1'
    },

    columnEdit: function( event ) {
    	showEditPanel(this.element);
//        $('#column-edit-modal').data('target',this.element).modal();
    },

    deleteColumnElement : function( event) {
        if($(this.element).siblings('.ve_element').length>0){
           var row = $(this.element).closest('.ve_element.ve_row');
        	$(this.element).remove();
            setLayout(row);
        }
        else {
            this.deleteRowElement(event);
        }

    },

    insert_content_block : function(event){
    	var $target = $(event.currentTarget);
    	var blockType =$target.data('type');
    	var blockId =$target.data('id');
    	var $column = $target.closest('.ve_col');
    	$column.removeClass('ve_col_empty');
    	var contentItem = $("<div class=\"content-new-holder\" data-type=\""+ blockType +"\" data-id=\""+ blockId +"\"><div class=\"content-new-loader\"></div></div>");
    	contentItem.insertBefore($column.find(' > .ve_element_container > .droppable-holder:last-child'));
    	contentItem.animate({height:'50px', width:'100%'},
                400, function() {
            $('.content-new-holder').height('auto');
            setTimeout(function() {
                loadContent($('.content-new-holder'));
            }, 500);
        });
	},
    
    append_ve_element: function(event) {
        var col_container = $(this.element).find('>.ve_col_container');
        $('#element-picker-modal').data('target', col_container).data('add-mode', 'append').modal();
    },
    
    prepend_ve_element: function(event) {
        var col_container = $(this.element).find('>.ve_col_container');
        $('#element-picker-modal').data('target', col_container).data('add-mode', 'prepend').modal();
    },
    
    showHideAdvanced: function(event) {
        var ctrl = $(event.target.closest('.ve-control'));
        ctrl.addClass('active');
        ctrl.siblings('.ve-control').removeClass('active');
    },

    _destroy: function() {
        //this.changer.remove();

        this.element
            .removeClass( "custom-newrow" )
            .enableSelection()
            .css( "background-color", "transparent" );
    },

    _setOptions: function() {
        this._superApply( arguments );
        this._refresh();
    },

    _setOption: function( key, value ) {
        this._super( key, value );
    }
});

function getColumn_property_lg(properties, name)
{
	if(properties['column_'+name+'_lg']) {
		if(properties['column_'+name+'_lg'] != 'inherit') {
			return properties['column_'+name+'_lg']==0 ? '' : name + "-lg-" + properties['column_'+name+'_lg'];
		}
		return getColumn_property_md(properties, name).replace('md','lg');
	}
	return '';
}

function getColumn_property_md(properties, name)
{
	if(name=='col') {
		return 'col-md-' + properties['column_width'];
	}
	if(properties['column_'+name+'_md']){
		if(properties['column_'+name+'_md'] != 'inherit') {
			return properties['column_'+name+'_md']==0 ? '' : name + "-md-" + properties['column_'+name+'_md'];
		}
		return getColumn_property_sm(properties, name).replace('sm','md');
	}
	return '';
}

function getColumn_property_sm(properties, name)
{
	if(properties['column_'+name+'_sm']) {
		if(properties['column_'+name+'_sm'] != 'inherit') {
			return properties['column_'+name+'_sm']==0 ? '' : name + "-sm-" + properties['column_'+name+'_sm'];
		}
		var property_xs = getColumn_property_xs(properties, name);
		return property_xs.length == 0 ? '' : property_xs.slice(0, property_xs.indexOf('-')) + '-sm' + property_xs.slice(property_xs.indexOf('-'))
	}
	return '';
}

function getColumn_property_xs(properties, name)
{
	if(properties['column_'+name+'_xs']) {
		return properties['column_'+name+'_xs']==0 ? '' : name + "-" + properties['column_'+name+'_xs'];
	}
	return '';
}


function getInsertBlockBtn(append)
{
	var drop = $('<div>').attr('class', 'dropdown dropup');
	var button;
	if(append)
		button = $('<a>').attr('class', "dropdown-toggle").attr('data-toggle', 'dropdown').attr('aria-expanded', 'false').attr('title', 'Append to Column').append($('<i>').addClass('fa fa-plus'));
	else
		button = $('<button>').attr('class', "btn btn-secondary dropdown-toggle").attr('data-toggle', 'dropdown').attr('aria-expanded', 'false').html("<i class='fa fa-plus'></i> <span style=\"margin-left: 10px;\">Insert...</span> <span class='caret'></span>");
	
	drop.append(button);
	var list = $('<ul>').attr('class', 'dropdown-menu');
	var insertBlocks = ['block-text', 'block-image', 'block-video'];
	var block_icons = {'block-text': 'fa-align-justify', 'block-image': 'fa-image', 'block-video':'fa-video'};
	contentBlocks.forEach(function(block,i){
		if(insertBlocks.includes(block.attr_blockid)) {
			list.append($('<li>').attr('class', 'add'+block.attr_headline).data('type',block.attr_blockid).data('id', block.element_id).html("<a href='#'><i class='fa "+block_icons[block.attr_blockid]+"'></i> Add "+block.attr_headline+" </a>"));
		}
	})
//	list.append($('<li>').attr('class', 'addText').data('type','block-text').html("<a href='#'><i class='fa fa-align-justify'></i> Add Text </a>"));
//	list.append($('<li>').attr('class', 'addImage').data('type','block-image').html("<a href='#'><i class='fa fa-image'></i> Add Image </a>"));
//	list.append($('<li>').attr('class', 'addVideo').data('type','block-video').html("<a href='#'><i class='fa fa-video'></i> Add Video </a>"));
	drop.append(list);

	return drop;
}