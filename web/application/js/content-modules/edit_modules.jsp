<div class="ve_ui-panel ve_ui-panel-edit" id="ve_ui-panel-edit">
	<div class="ve_ui-panel-inner">
		<div class="ve_ui-panel-header-container">
			<div class="ve_ui-panel-header-content">
				<div class="ve_ui-panel-header-title">
					<span id="ve_element-name"></span>
				</div>
				<div class="ve_ui-panel-header-controls"></div>
			</div>		
		</div>
		<div class="ve_ui-panel-content-container">
			<div class="ve_ui-panel-content">
				<ul class="nav nav-tabs" role="tablist">
					  <li class="nav-item" data-setting="general">
					    <a class="nav-link" id="basic-settings-tab" data-toggle="tab" href="#basic-settings" role="tab" aria-controls="basic-settings" aria-selected="true">General</a>
					  </li>
					  <li class="nav-item" data-setting="layout">
					    <a class="nav-link" id="layout-settings-tab" data-toggle="tab" href="#layout-settings" role="tab" aria-controls="layout-settings" aria-selected="false">Layout</a>
					  </li>
					  <li class="nav-item" data-setting="design">
					    <a class="nav-link" id="design-settings-tab" data-toggle="tab" href="#design-settings" role="tab" aria-controls="design-settings" aria-selected="false">Design Options</a>
					  </li>
					  <li class="nav-item" data-setting="background">
					    <a class="nav-link" id="background-settings-tab" data-toggle="tab" href="#background-settings" role="tab" aria-controls="background-settings" aria-selected="false">Background</a>
					  </li>
					<li class="nav-item" data-setting="effects">
					    <a class="nav-link" id="effects-settings-tab" data-toggle="tab" href="#effects-settings" role="tab" aria-controls="effects-settings" aria-selected="false">Effects</a>
					  </li>
					  <li class="nav-item" data-setting="responsive">
					    <a class="nav-link" id="responsive-settings-tab" data-toggle="tab" href="#responsive-settings" role="tab" aria-controls="responsive-settings" aria-selected="false">Responsive Options</a>
					  </li>
					<li class="nav-item" data-setting="map">
						<a class="nav-link" id="map-settings-tab" data-toggle="tab" href="#map-settings" role="tab" aria-controls="map-settings" aria-selected="false">Map Settings</a>
					</li>
				</ul>
				<div class="tab-content">
				  	<div class="tab-pane fade show" id="basic-settings" role="tabpanel" aria-labelledby="basic-settings">
				  		<div class="col-12">
                            <div class="row">
				  			<div class="col-12" data-element-property="text">
					  			<label>Text</label>
								<div id="ve_textblock_editor"></div>
							</div>
							<div class="col-6" data-element-property="lineSpacing">
								<label>Line Spacing</label>
								<div><input  name="line_spacing"></div>
							</div>
							<div class="col-6" data-element-property="paragraphSpacing">
								<label>Paragraph Spacing</label>
								<div>
									<select name="paragraph_spacing">
										<option value="">Default</option>
										<option value="0">None</option>
										<option value="1">1px</option>
										<option value="2">2px</option>
										<option value="3">3px</option>
										<option value="4">4px</option>
										<option value="5">5px</option>
										<option value="6">6px</option>
										<option value="7">7px</option>
										<option value="8">8px</option>
										<option value="9">9px</option>
										<option value="10">10px</option>
									</select>
								</div>
							</div>
							<div class="col-6" data-element-property="letterSpacing">
								<label>Letter Spacing</label>
								<div>
									<select name="letter_spacing">
										<option value="">Default</option>
										<option value="0">None</option>
										<option value="1">1px</option>
										<option value="2">2px</option>
										<option value="3">3px</option>
										<option value="4">4px</option>
										<option value="5">5px</option>
										<option value="6">6px</option>
										<option value="7">7px</option>
										<option value="8">8px</option>
										<option value="9">9px</option>
										<option value="10">10px</option>
									</select>
								</div>
							</div>
							<div class="col-6" data-element-property="lineHeight">
								<label>Line Height</label>
								<div><input  name="line_height"></div>
							</div>
							<div class="col-6" data-element-property="columnContentPosition">
								<label>Column Content Position</label>
								<div class="ve_column-content-position">
									<select name="column_content_position">
										<option value="top">Top</option>
										<option value="middle">Middle</option>
										<option value="bottom">Bottom</option>
									</select>
								</div>
							</div>
							<div class="col-6" data-element-property="columnsLayout">
								<label>Columns Layout</label>
								<div class="ve_columns-layout">
									<select name="columns_layout">
										<option value="default">Default</option>
										<option value="boxes">Boxes</option>
									</select>
								</div>
							</div>
							<div class="col-6" data-element-property="rowHeight">
								<label>Row Height</label>
								<div class="ve_row-height">
									<select name="row_height">
										<option class="auto" value="auto">Equals the content height</option>
										<option value="small">Small</option>
										<option value="medium">Medium</option>
										<option value="large">Large</option>
										<option value="huge">Huge</option>
										<option value="full">Full Screen</option>
									</select>
								</div>
							</div>
							<div class="col-6" data-element-property="gutter">
								<label>Columns Gap</label>
								<div class="ve_column-gutter">
									<select name="column_gutter" class="ve_column-gutter">
										<option value="gtr-0">None</option>
										<option value="gtr-1">1px</option>
										<option value="gtr-2">2px</option>
										<option value="gtr-3">3px</option>
										<option value="gtr-4">4px</option>
										<option value="gtr-5">5px</option>
										<option value="gtr-6">6px</option>
										<option value="gtr-7">7px</option>
										<option value="gtr-8">8px</option>
										<option value="gtr-9">9px</option>
										<option value="gtr-10">10px</option>
									</select>
								</div>
							</div>
							<div class="col-12" data-element-property="fullWidth">
								<label>Full Width Content</label>
								<div><input type="checkbox" name="full_width" value="true">
									<div class="checkbox-desc">Stretch content of this row to the screen width</div>
								</div>
							</div>
							<div class="col-6" data-element-property="textColorStyle">
								<label>Text Color Style</label>
								<div class="ve_textcolor_style">
									<select name="textcolor_style" class="ve_textcolor_style">
										<option value="inherit" selected>Inherit</option>
										<option value="custom">Custom</option>
									</select>
								</div>
							</div>
							<div class="col-6" data-element-property="textColor">
								<label>Text Colour</label>
								<div class="ve_text-color">
									<button class="ve_color-picker btn">
										<input type="hidden" name="text_color">
										<span>Select Color</span>
									</button>
								</div>
							</div>
							<div class="col-12" data-element-property="animation">
								<div class="row">
									<div class="col-6" >
										<label>Animation</label>
										<div class="">
											<select name="animation">
												<option	value="">None</option>
												<option value="fade">Fade</option>
												<option value="afc">Appear From Center</option>
												<option value="afl">Appear From Left</option>
												<option value="afr">Appear From Right</option>
												<option value="afb">Appear From Bottom</option>
												<option value="aft">Appear From Top</option>
												<option value="hfc">Height From Center</option>
												<option value="wfc">Width From Center</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<label>Animation Duration</label>
										<div class="">
											<select name="animation_duration">
												<option value="">None</option>
												<option value="duration1">0.5 second</option>
												<option value="duration2">0.7 second</option>
												<option value="duration3">1 second</option>
												<option value="duration4">1.5 second</option>
												<option value="duration5">2 second</option>
											</select>
										</div>
									</div>
									<div class="col-6">
										<label>Animation Delay</label>
										<div class="">
											<select name="animation_delay">
												<option value="">None</option>
												<option value="delay1">0.2 second</option>
												<option value="delay2">0.4 second</option>
												<option value="delay3">0.6 second</option>
												<option value="delay4">0.8 second</option>
												<option value="delay5">1 second</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-6" data-element-property="align">
								<label>Items Text Alignment</label>
								<div class="">
									<select name="align">
										<option class="left" value="left">Left</option>
										<option class="center" value="center">Center</option>
										<option class="right" value="right">Right</option>
									</select>
								</div>
							</div>
							<div class="col-12" data-element-property="backgroundVideo">
								<label>Background Video</label>
								<div><input name="background_video"></div>
							</div>
							<div class="col-6" data-element-property="backgroundOverlay">
								<label>Background Overlay</label>
								<div class="ve_background-overlay">
									<button class="ve_color-picker btn">
										<input type="hidden" name="background_overlay">
										<span>Select Color</span>
									</button>
								</div>
							</div>
							<div class="col-12" data-element-property="fullWidth">
								<label>Sticky Row</label>
								<div><input type="checkbox" name="sticky_row" value="true">
									<div class="checkbox-desc">Fix this row at the top of a page during scroll</div>
								</div>
							</div>
							<div class="col-12" data-element-property="disable">
								<label>Disable Row</label>
								<div><input type="checkbox" name="disable" value="true">
									<div class="checkbox-desc">If checked the row won't be visible on the public side of your website. You can switch it back any time.</div>
								</div>
							</div>
							<div class="col-12" data-element-property="progressbar">
								<div class="row">
									<div class="col-6">
										<label>Title</label>
										<div class="ve_progressbar-title">
											<input name="title"  />
										</div>
									</div>
									<div class="col-6">
										<label>Progress Value (%)</label>
										<div class="ve_progressbar-progress">
											<input name="progress"  />
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<label>Style</label>
										<div class="ve_progressbar-style">
											<select name="style" class="ve_progressbar-style">
												<option value="style1">Style 1</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<label>Progress Bar Color</label>
										<div class="ve_progressbar-color">
											<button class="ve_color-picker btn">
												<input type="hidden" name="bar_color">
												<span>Select Color</span>
											</button>
										</div>
									</div>
									<div class="col-6">
										<label>Progress Bar Height</label>
										<div class="ve_progressbar-height">
											<input name="bar_height"  />
										</div>
									</div>
								</div>
							</div>
							<div class="col-6" data-element-property="alignment">
					  			<label>Alignment</label>
								<div class="">
									<select name="alignment">
										<option value="left">Left</option>
										<option value="center">Center</option>
										<option value="right">Right</option>
									</select>
								</div>
							</div>
							<div class="col-6" data-element-property="height">
								<label>Height</label>
								<div><input name="height"></div>
							</div>
							<div class="col-12" data-element-property="label">
								<label>Button Label</label>
								<div><input name="label"></div>
							</div>
							<div class="col-6" data-element-property="color">
								<label>Color</label>
								<div class="color">
									<button class="ve_color-picker btn">
										<input type="hidden" name="color">
										<span>Select Color</span>
									</button>
								</div>
							</div>
							<div class="col-6" data-element-property="btnColor">
								<label>Button Color</label>
								<div class="btn_color">
									<button class="ve_color-picker btn">
										<input type="hidden" name="button_color">
										<span>Select Color</span>
									</button>
								</div>
							</div>
							<div class="col-12" data-element-property="title">
								<label>Title</label>
								<div class="ve_title">
									<input name="title" class="ve_title"  />
								</div>
							</div>
							<div class="col-6" data-element-property="titleColorStyle">
								<label>Title Color Style</label>
								<div class="ve_titlecolor_style">
									<select name="titlecolor_style" class="ve_titlecolor_style">
										<option value="inherit" selected>Inherit</option>
										<option value="custom">Custom</option>
									</select>
								</div>
							</div>
							<div class="col-6" data-element-property="titleColor">
								<label>Title Colour</label>
								<div class="ve_title-color">
									<button class="ve_color-picker btn">
										<input type="hidden" name="title_color">
										<span>Select Color</span>
									</button>
								</div>
							</div>
							<div class="col-6" data-element-property="titleSize">
								<label>Title Size</label>
								<div class="ve_title-size">
									<input name="title_size" class="ve_title-size"  />
								</div>
							</div>
							<div class="col-6" data-element-property="accordionStyle">
								<label>Collapse Style</label>
								<div class="ve_accordion_style">
									<select name="accordion_style" class="ve_accordion_style">
										<option value="style1" selected>Style 1</option>
										<option value="style2">Style 2</option>
									</select>
								</div>
							</div>
							<div class="col-6" data-element-property="fontSize">
								<label>Font size</label>
								<div><input name="fontSize"></div>
							</div>
							<div class="col-12" data-element-property="image">
								<div class="row">
									<div class="col-12">
										<label>Image</label>
										<div class="ve_image">
											<div class="thumbnail">
												<img>
												<a class="remove-image"><i class="fa fa-times"></i></a>
											</div>
											<input type="hidden" name="image">
											<!-- <input id="imageUpload" type="file" accept="image/*" name="background_image"> -->
											<a class="filemanager-add-image filemanager">
												<i class="fa fa-plus"></i>
											</a>
											<!-- <button class="filemanager">Browse</button> -->
										</div>
									</div>
									<div class="col-6">
										<label>Image Size</label>
										<div class="ve_image-size">
											<select name="image_size" class="ve_image-size">
												<option value="full">Full</option>
												<option value="large">1024 x 1024</option>
												<option value="medium_large">768 x any</option>
												<option value="medium">300 x 300</option>
												<option value="thumbnail">150 x 150</option>
											</select>
										</div>
									</div>
                                    <div class="col-6">
                                        <label>Image Alignment</label>
                                        <div class="ve_image-alignment">
                                            <select name="image_alignment" class="ve_image-alignment">
                                                <option value="left">Left</option>
                                                <option value="center">Center</option>
                                                <option value="right">Right</option>
                                            </select>
                                        </div>
                                    </div>
								</div>
							</div>
							<div class="col-12" data-element-property="icon">
								<div class="row">
									<div class="col-6">
										<label>Icon</label>
										<div class="ve_iconbox-icon">
											<input name="icon" class="ve_icon"  />
										</div>
									</div>
									<div class="col-6">
										<label>Icon Style</label>
										<div class="ve_iconbox-icon-style">
											<select name="icon_style" class="ve_icon-style">
												<option value="simple">Simple</option>
												<option value="solid">Inside the solid circle</option>
											</select>
										</div>
									</div>
								</div>
								<!-- <div class="row">
									<div class="col-6">
										<label>Image</label>
										<div class="ve_iconbox-icon">
											<input name="icon_image" class="ve_icon-image"  />
										</div>
									</div>
									<div class="col-6">
										<label>Icon Type</label>
										<div class="ve_iconbox-icon-type">
											<select name="icon_type" class="ve_icon-type">
												<option value="icon">Icon</option>
												<option value="image">Image</option>
											</select>
										</div>
									</div>
								</div> -->
								<div class="row">
									<div class="col-6">
										<label>Icon Colour</label>
										<div class="ve_iconbox-icon-color">
											<button class="ve_color-picker btn">
												<input type="hidden" name="icon_color">
												<span>Select Color</span>
											</button>
										</div>
									</div>
									<div class="col-6">
										<label>Icon Size</label>
										<div class="ve_iconbox-icon-size">
											<div class="ve_iconbox-icon-size">
												<input name="icon_size" class="ve_size"  />
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<label>Icon Position</label>
										<div class="ve_iconbox-icon-position">
											<select name="icon_position" class="ve_position">
												<option value="top">Top</option>
												<option value="left">Left</option>
												<option value="right">Right</option>
											</select>
										</div>
									</div>
									<div class="col-6 icon_alignment">
										<label>Alignment</label>
										<div class="ve_iconbox-icon-alignment">
											<select name="icon_alignment" class="icon_alignment">
												<option value="left">Left</option>
												<option value="center">Center</option>
												<option value="right">Right</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
										<label>Title Tag Name</label>
										<div class="ve_iconbox-title-tag-name">
											<select name="title_tag_name">
												<option class="h1" value="h1">h1</option>
												<option class="h2" value="h2">h2</option>
												<option class="h3" value="h3">h3</option>
												<option class="h4" value="h4">h4</option>
												<option class="h5" value="h5">h5</option>
												<option class="h6" value="h6">h6</option>
												<option class="p" value="p">p</option>
												<option class="div" value="div">div</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
										<label>Description</label>
										<div class="ve_iconbox-description">
											<input name="description" class="ve_description"  />
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<label>Description Colour</label>
										<div class="ve_iconbox-description-color">
											<button class="ve_color-picker btn">
												<input type="hidden" name="description_color">
												<span>Select Color</span>
											</button>
										</div>
									</div>
									<div class="col-6">
										<label>Description Size</label>
										<div class="ve_iconbox-description-size">
											<input name="description_size" class="ve_description-size"  />
										</div>
									</div>
								</div>
							</div>
							<div class="col-12" data-element-property="link">
								<div class="row">
									<div class="col-12">
										<label>On Click Action</label>
										<div class="ve_click-action">
											<select name="click_action" class="ve_image-action">
												<option value="none">None</option>
												<option value="link">Open custom link</option>
												<option value="file">Open file</option>
											</select>
										</div>
										<div class="click-action">
											<input hidden  class="" name="linkURL" >
											<input hidden  class="" name="linkText" >
											<div class="click-action-link">
												<button id="selectUrl" class="btn btn-sm">Select URL</button>
												<span>Title: </span>
												<span class="link-title"></span>
												<span>URL: </span>
												<span class="link-url"></span>
											</div>
											<div class="click-action-file">
												<button id="selectFile" class="btn btn-sm">Select File</button>
												<span>File: </span>
												<span class="file-name"></span>
												<div><label><span></span><input type="checkbox" class="" name="newtab" value="true"> Open link in a new tab</label></div>
												<div><label><span></span><input type="checkbox" class="" name="nofollow" value="true"> Add nofollow option to link</label></div>
											</div>
										</div>
									</div>
								</div>
							</div>
								<div class="col-12" data-element-property="video">
									<div class="row">
										<div class="col-12">
											<label>Video link</label>
											<div class="ve_video-link">
												<input name="url" class="ve_video-link"  />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-6">
											<label>Aspect Ratio</label>
											<div class="ve_video-aspect-ratio">
												<select name="aspect_ratio">
													<option value="21x9">21:9</option>
													<option value="16x9" selected="selected">16:9</option>
													<option value="4x3">4:3</option>
													<option value="3x2">3:2</option>
													<option value="1x1">1:1</option>
												</select>
											</div>
										</div>
										<div class="col-6">
											<label>Max Width</label>
											<div class="ve_iconbox-icon-size">
												<div class="ve_video-width">
													<input name="max_width"  />
												</div>
											</div>
										</div>
										<div class="col-6">
											<label>Video Height</label>
											<div class="ve_iconbox-icon-size">
												<div class="ve_video-height">
													<input name="height"  />
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-12" data-element-property="slideshow">
									<div class="row">
										<div class="col-12">
											<label>Images</label>
											<div class="slide-images">
												<input type="hidden" name="slide_count">
												<a class="filemanager-add-image filemanager">
													<i class="fa fa-plus"></i>
												</a>
											</div>
										</div>
									</div>
									<div class="slide-detail-container">
										<div class="slide-pagination"></div>
									</div>
									<div class="slide-detail-template">
										<div class="slide-detail">
											<div class="row">
												<div class="col-12">
													<label>Title</label>
													<div>
														<input class="slide-title" name="slide_title">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-12">
													<label>Sub - Title</label>
													<div>
														<input class="slide-sub-title" name="slide_sub_title">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-6" data-element-property="slideAnimation">
										<label>Slide Animation</label>
										<div class="ve_slide-animation">
											<select name="slide_animation">
												<option value="slide">Slide</option>
												<option value="fade">Fade</option>
											</select>
										</div>
									</div>
									<div class="col-6" data-element-property="slideTitleAlignment">
										<label>Slide Title Alignment</label>
										<div class="ve_slide-title-align">
											<select name="slide_title_align">
												<option value="left">Left</option>
												<option value="center">Center</option>
												<option value="right">Right</option>
											</select>
										</div>
									</div>
									<div class="col-12" data-element-property="slideTitlePosition">
										<label>Slide Title Position</label>
										<div class="ve_slide-title-position">

											<span>Top</span><input name="slide_title_top" value=""><span>Bottom</span><input
												name="slide_title_bottom" value=""><span>Left</span><input
												name="slide_title_left" value=""><span>Right</span><input
												name="slide_title_right" value="">
										</div>
									</div>
								<!-- <div class="col-6" data-element-property="elementId">
									<label>Element ID</label>
									<div><input name="id"></div>
								</div>
								<div class="col-6" data-element-property="className">
									<label>Extra Class Name</label>
									<div><input type="hidden" name="old_className"><input name="className"></div>
								</div> -->
                            </div>
						</div>
					</div>
					<div class="tab-pane fade show" id="layout-settings" role="tabpanel" aria-labelledby="layout-settings">
				  		<div  class="col-12" data-element-property="layout">
				  			<div class="form-group">
							    <label for="rowLayout" class="active">Row layout</label>
							    <div id="layout-options">
							    	<div class="layout" data-layout="1/1" title="1/1">
										<div class="row">
											<div class="col col-12">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-12">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-12">
												<div class="bar"></div>
											</div>
										</div>
									</div>
									<div class="layout" data-layout="1/2 + 1/2" title="1/2 + 1/2">
										<div class="row">
											<div class="col col-6">
												<div class="bar"></div>
											</div>
											<div class="col col-6">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-6">
												<div class="bar"></div>
											</div>
											<div class="col col-6">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-6">
												<div class="bar"></div>
											</div>
											<div class="col col-6">
												<div class="bar"></div>
											</div>
										</div>
									</div>
									<div class="layout" data-layout="2/3 + 1/3" title="2/3 + 1/3">
										<div class="row">
											<div class="col col-8">
												<div class="bar"></div>
											</div>
											<div class="col col-4">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-8">
												<div class="bar"></div>
											</div>
											<div class="col col-4">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-8">
												<div class="bar"></div>
											</div>
											<div class="col col-4">
												<div class="bar"></div>
											</div>
										</div>
									</div>
									<div class="layout" data-layout="1/3 + 1/3 + 1/3" title="1/3 + 1/3 + 1/3">
										<div class="row">
											<div class="col col-4">
												<div class="bar"></div>
											</div>
											<div class="col col-4">
												<div class="bar"></div>
											</div>
											<div class="col col-4">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-4">
												<div class="bar"></div>
											</div>
											<div class="col col-4">
												<div class="bar"></div>
											</div>
											<div class="col col-4">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-4">
												<div class="bar"></div>
											</div>
											<div class="col col-4">
												<div class="bar"></div>
											</div>
											<div class="col col-4">
												<div class="bar"></div>
											</div>
										</div>
									</div>
									<div class="layout" data-layout="1/4 + 1/4 + 1/4 + 1/4" title="1/4 + 1/4 + 1/4 + 1/4">
										<div class="row">
											<div class="col col-3">
												<div class="bar"></div>
											</div>
											<div class="col col-3">
												<div class="bar"></div>
											</div>
											<div class="col col-3">
												<div class="bar"></div>
											</div>
											<div class="col col-3">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-3">
												<div class="bar"></div>
											</div>
											<div class="col col-3">
												<div class="bar"></div>
											</div>
											<div class="col col-3">
												<div class="bar"></div>
											</div>
											<div class="col col-3">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-3">
												<div class="bar"></div>
											</div>
											<div class="col col-3">
												<div class="bar"></div>
											</div>
											<div class="col col-3">
												<div class="bar"></div>
											</div>
											<div class="col col-3">
												<div class="bar"></div>
											</div>
										</div>
									</div>
									<div class="layout" data-layout="1/4 + 3/4" title="1/4 + 3/4">
										<div class="row">
											<div class="col col-3">
												<div class="bar"></div>
											</div>
											<div class="col col-9">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-3">
												<div class="bar"></div>
											</div>
											<div class="col col-9">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-3">
												<div class="bar"></div>
											</div>
											<div class="col col-9">
												<div class="bar"></div>
											</div>
										</div>
									</div>
									<div class="layout" data-layout="1/4 + 1/2 + 1/4" title="1/4 + 1/2 + 1/4">
										<div class="row">
											<div class="col col-3">
												<div class="bar"></div>
											</div>
											<div class="col col-6">
												<div class="bar"></div>
											</div>
											<div class="col col-3">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-3">
												<div class="bar"></div>
											</div>
											<div class="col col-6">
												<div class="bar"></div>
											</div>
											<div class="col col-3">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-3">
												<div class="bar"></div>
											</div>
											<div class="col col-6">
												<div class="bar"></div>
											</div>
											<div class="col col-3">
												<div class="bar"></div>
											</div>
										</div>
									</div>
									<div class="layout" data-layout="5/6 + 1/6" title="5/6 + 1/6">
										<div class="row">
											<div class="col col-10">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-10">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-10">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
										</div>
									</div>
									<div class="layout" data-layout="1/6 + 1/6 + 1/6 + 1/6 + 1/6" title="1/6 + 1/6 + 1/6 + 1/6 + 1/6">
										<div class="row">
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
										</div>
									</div>
									<div class="layout" data-layout="1/6 + 4/6 + 1/6" title="1/6 + 4/6 + 1/6">
										<div class="row">
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-8">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-8">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-8">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
										</div>
									</div>
									<div class="layout" data-layout="1/6 + 1/6 + 1/6 + 1/2" title="1/6 + 1/6 + 1/6 + 1/2">
										<div class="row">
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-6">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-6">
												<div class="bar"></div>
											</div>
										</div>
										<div class="row">
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-2">
												<div class="bar"></div>
											</div>
											<div class="col col-6">
												<div class="bar"></div>
											</div>
										</div>
									</div>
								</div>
							    <small class="form-text text-muted">Select row layout from predefined options.</small>
							</div>
							<div class="form-group">
							    <label>Enter custom layout for your row</label>
							    	<input  class="form-control" name="layout">
							    	<div class="help-block">Wrong layout</div>
							    <small class="form-text text-muted">Change particular row layout manually by specifying number of columns and their size value.</small>
							</div>
							<div class="form-group" style="display: none !important;">
							    <label for="rowLayout" class="active">Responsive layout</label>
							    <table class="ve_table table-responsive-layout">
							    	<thead>
							    		<tr>
							    			<th style="width:70px;">Device</th>
							    			<th>Responsive width</th>
							    		</tr>
							    	</thead>
							    	<tbody>
							    		<tr>
							    			<td>
							    				<span title="Large">
													<i class="fas fa-desktop"></i>
												</span>
							    			</td>
							    			<td class="ve_size-responsive-lg">
							    			</td>
							    		</tr>
							    		<tr>
							    			<td>
							    				<span title="Medium">
													<i class="fa fa-tablet-alt"></i>
												</span>
							    			</td>
							    			<td class="ve_size-responsive-md">
							    			</td>
							    		</tr>
							    		<tr>
							    			<td>
							    				<span title="Small">
													<i class="fa fa-tablet-alt" style="transform:rotate(-90deg);"></i>
												</span>
							    			</td>
							    			<td class="ve_size-responsive-sm">
							    			</td>
							    		</tr>
							    		<tr>
							    			<td>
							    				<span title="Extra Small">
													<i class="fa fa-mobile-alt"></i>
												</span>
							    			</td>
							    			<td class="ve_size-responsive-xs">
							    			</td>
							    		</tr>
							    	</tbody>
							    </table>
						    </div>
				  		</div>
			  		</div>
					<div class="tab-pane fade show" id="background-settings" role="tabpanel" aria-labelledby="background-settings">
				  		<div  class="col-12">
				  			<div data-element-property="backgroundStyle">
					  			<label>Background Style</label>
								<div class="ve_background-style">
									<select name="background_style" class="ve_background-style">
										<option value="default">Defaults</option>
										<option value="single-color">Single Color</option>
										<option value="gradient">Gradient Color</option>
										<option value="image">Image</option>
										<option value="dynamic-color">Dynamic Color</option>
									</select>
								</div>
							</div>
							<div data-element-property="backgroundColor" class="show-if-color">
								<label>Background Color</label>
								<div class="background-color">
									<button class="ve_color-picker btn">
										<input type="hidden" name="background_single_color">
										<span>Select Color</span>
									</button>
								</div>
							</div>
							<div data-element-property="dynamic_single_color" class="show-if-dynamic">
								<label>Background Color:</label>
								<div class="background-color">
									<button class="ve_color-picker btn">
										<input type="hidden" name="dynamic_single_color">
										<span>Select Color</span>
									</button>
								</div>
							</div>
							<div data-element-property="dynamic-color-position" class="show-if-dynamic">
								<label>Position:</label>
								<div class="background-color">
									<select name="dynamic-color-position" class="ve-dynamic-color-position">
										<option value="top">Top</option>
										<option value="bottom">Bottom</option>
									</select>
								</div>
							</div>
							<div class="row  show-if-dynamic">
								<div class="col-6 show-if-dynamic" data-element-property="leftPositionDynamic">
									<label>Left Position from top:</label>
									<div class="dynamic-position-left">
										<input type="number" min="0" max="100" name="leftPositionDynamic">
									</div>
								</div>
								<div class="col-6 show-if-dynamic" data-element-property="rightPositionDynamic">
									<label>Right Position from top:</label>
									<div class="dynamic-position-right">
										<input type="number" min="0" max="100" name="rightPositionDynamic">
									</div>
								</div>
							</div>
							<div class="row show-if-dynamic">
								<div class="col-12 show-if-dynamic">
									<label>Preview:</label>
									<div id="dynamic-color-svg-preview" style="width: 200px; height: 100px; margin: auto;border: 1px dotted lightgray;">
										<svg width='100%' height='100%' viewBox="0 0 100 100" preserveAspectRatio="none" style='background-color: white'>
											<polygon style="stroke-width:1" />
										</svg>
									</div>
								</div>
							</div>
							<div data-element-property="backgroundImage" class="show-if-image">
					  			<label>Background Image</label>
								<div class="background-image">
									<div class="thumbnail">
										<img>
										<a class="remove-image"><i class="fa fa-times"></i></a>
									</div>
									<input type="hidden" name="background_image">
									<!-- <input id="imageUpload" type="file" accept="image/*" name="background_image"> -->
									<a class="filemanager-add-image filemanager">
										<i class="fa fa-plus"></i>
									</a>
								</div>
							</div>
				  			<div data-element-property="parallax-effect" class="show-if-image">
					  			<label>Parallax Effect</label>
								<div class="ve_parallax-effect">
									<select name="parallax-effect" class="ve_parallax-effect">
										<option value="parallax-none">None</option>
										<option value="parallax-ver">Vertical</option>
										<option value="parallax-fix">Fixed</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade show" id="effects-settings" role="tabpanel" aria-labelledby="effects-settings">
						<div class="col-12">
							<div data-element-property="seperatorPosition" class="">
								<label>Seperator Position</label>
								<div class="ve_seperator-position">
									<select name="seperator_position" class="ve_seperator-position">
										<option value="top">Top</option>
										<option value="bottom">Bottom</option>
									</select>
								</div>
							</div>
							<div data-element-property="seperatorStyle">
								<label>Seperator Style</label>
								<div class="ve_seperator-style">
									<select name="seperator_style" class="ve_seperator-style">
										<option value="">None</option>
										<option value="triangle">Triangle</option>
										<option value="semicircle">Half Circle</option>
										<option value="image">Image</option>
									</select>
								</div>
							</div>
							<div data-element-property="seperatorColor" class="show-if-seperator-color" style="display: none;">
								<label>Seperator Color</label>
								<div class="seperator-color">
									<button class="ve_color-picker btn">
										<input type="hidden" name="seperator_color">
										<span>Select Color</span>
									</button>
								</div>
							</div>
							<div data-element-property="seperatorImage" class="show-if-seperator-image" style="display: none;">
								<label>Seperator Image</label>
								<div class="seperator-image">
									<div class="thumbnail">
										<img>
										<a class="remove-image"><i class="fa fa-times"></i></a>
									</div>
									<input type="hidden" name="seperator_image">
									<!-- <input id="imageUpload" type="file" accept="image/*" name="background_image"> -->
									<a class="filemanager-add-image filemanager">
										<i class="fa fa-plus"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
			  		<div class="tab-pane fade" id="design-settings" role="tabpanel" aria-labelledby="design-settings">
			  			<div  class="col-12">
				  			<div data-element-property="width">
					  			<label>Width</label>
								<div>
									<select name="column_width" class="ve_column-width">
										<option class="1/12" value="1">1 column - 1/12</option>
										<option class="1/6" value="2">2 columns - 1/6</option>
										<option class="1/4" value="3">3 columns - 1/4</option>
										<option class="1/3" value="4" selected="selected">4 columns - 1/3</option>
										<option class="5/12" value="5">5 columns - 5/12</option>
										<option class="1/2" value="6">6 columns - 1/2</option>
										<option class="7/12" value="7">7 columns - 7/12</option>
										<option class="2/3" value="8">8 columns - 2/3</option>
										<option class="3/4" value="9">9 columns - 3/4</option>
										<option class="5/6" value="10">10 columns - 5/6</option>
										<option class="11/12" value="11">11 columns - 11/12</option>
										<option class="1/1" value="12">12 columns - 1/1</option>
									</select>
								</div>
							</div>
						</div>
			  			<div class="col-12">
							<div>
								CSS box
							</div>
							<div class="ve_css-editor row">
								<div class="ve-layout_onion col-7" data-element-property="layout">
									<div class="ve_margin">
										<label>margin</label>
										<input  name="margin_top" data-name="margin-top" class="ve_top" placeholder="-" data-attribute="margin" value="">
										<input  name="margin_bottom" data-name="margin-bottom" class="ve_bottom" placeholder="-" data-attribute="margin" value="">
										<input  name="margin_left" data-name="margin-left" class="ve_left" placeholder="-" data-attribute="margin" value="">
										<input  name="margin_right" data-name="margin-right" class="ve_right" placeholder="-" data-attribute="margin" value="">
										<div class="ve_border">
											<label>border</label>
											<input  name="border_top" data-name="border-top" class="ve_top" placeholder="-" data-attribute="border" value="">
											<input  name="border_bottom" data-name="border-bottom" class="ve_bottom" placeholder="-" data-attribute="border" value="">
											<input  name="border_left" data-name="border-left" class="ve_left" placeholder="-" data-attribute="border" value="">
											<input  name="border_right" data-name="border-right" class="ve_right" placeholder="-" data-attribute="border" value="">
											<div class="ve_padding">
												<input  name="padding_top" data-name="padding-top" class="ve_top" placeholder="-" data-attribute="padding" value="">
												<input  name="padding_bottom" data-name="padding-bottom" class="ve_bottom" placeholder="-" data-attribute="padding" value="">
												<input  name="padding_left" data-name="padding-left" class="ve_left" placeholder="-" data-attribute="padding" value="">
												<input  name="padding_right" data-name="padding-right" class="ve_right" placeholder="-" data-attribute="padding" value="">
												<label>padding</label>
												<div class="ve_content">
								
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-5 ve_settings">
									<div data-element-property="borderColor">
										<label>Border Color</label>
										<div class="ve_textblock-border-color">
											<button class="ve_color-picker btn">
												<input type="hidden" name="border_color">
												<span>Select Color</span>
											</button>
										</div>
									</div>
									<div data-element-property="borderStyle">
										<label>Border Style</label>
										<div class="ve_textblock-border-style">
											<select name="border_style" class="ve_border-style">
												<option value="">Theme defaults</option>
												<option value="solid">Solid</option>
												<option value="dotted">Dotted</option>
												<option value="dashed">Dashed</option>
												<option value="none">None</option>
												<option value="hidden">Hidden</option>
												<option value="double">Double</option>
												<option value="groove">Groove</option>
												<option value="ridge">Ridge</option>
												<option value="inset">Inset</option>
												<option value="outset">Outset</option>
												<option value="initial">Initial</option>
												<option value="inherit">Inherit</option></select>
										</div>
									</div>
									<div data-element-property="borderRadius">
										<label>Border Radius</label>
										<div class="ve_textblock-border-radius">
											<select name="border_radius" class="ve_border-radius">
												<option value="">None</option>
												<option value="1px">1px</option>
												<option value="2px">2px</option>
												<option value="3px">3px</option>
												<option value="4px">4px</option>
												<option value="5px">5px</option>
												<option value="10px">10px</option>
												<option value="15px">15px</option>
												<option value="20px">20px</option>
												<option value="25px">25px</option>
												<option value="30px">30px</option>
												<option value="35px">35px</option>
											</select>
										</div>
									</div>
									<div data-element-property="background">
										<label>Background</label>
										<div class="ve_textblock-background">
											<button class="ve_color-picker btn">
												<input type="hidden" name="background_color" value="transparent">
												<span>Select Color</span>
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
			  		</div>
			  		<div class="tab-pane fade" id="responsive-settings" role="tabpanel" aria-labelledby="responsive-settings">
				  		<div  class="col-12">
				  			<div data-element-property="width">
					  			<label>Width</label>
								<div>
									<select name="column_width" class="ve_column-width">
										<option class="1/12" value="1">1 column - 1/12</option>
										<option class="1/6" value="2">2 columns - 1/6</option>
										<option class="1/4" value="3">3 columns - 1/4</option>
										<option class="1/3" value="4" selected="selected">4 columns - 1/3</option>
										<option class="5/12" value="5">5 columns - 5/12</option>
										<option class="1/2" value="6">6 columns - 1/2</option>
										<option class="7/12" value="7">7 columns - 7/12</option>
										<option class="2/3" value="8">8 columns - 2/3</option>
										<option class="3/4" value="9">9 columns - 3/4</option>
										<option class="5/6" value="10">10 columns - 5/6</option>
										<option class="11/12" value="11">11 columns - 11/12</option>
										<option class="1/1" value="12">12 columns - 1/1</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-12">
							<div data-element-property="responsive">
								<label>Responsiveness</label>
								<div class="">
									<table class="ve_table">
										<tbody>
											<tr>
												<th>Device</th>
												<th>Offset</th>
												<th>Width</th>
												<th>Hide on device?</th>
											</tr>
											<tr class="ve_size-lg">
												<td>
													<span title="Large">
														<i class="fas fa-desktop"></i>
													</span>
												</td>
												<td>
													<select name="column_offset_lg" class="ve_column_offset_field" data-type="offset-lg">
														<option value="inherit" style="color: #ccc;">Inherit from smaller</option>
														<option value="0" style="color: #ccc;">No offset</option>
														<option value="1">1 column - 1/12</option>
														<option value="2">2 columns - 1/6</option>
														<option value="3">3 columns - 1/4</option>
														<option value="4">4 columns - 1/3</option>
														<option value="5">5 columns - 5/12</option>
														<option value="6">6 columns - 1/2</option>
														<option value="7">7 columns - 7/12</option>
														<option value="8">8 columns - 2/3</option>
														<option value="9">9 columns - 3/4</option>
														<option value="10">10 columns - 5/6</option>
														<option value="11">11 columns - 11/12</option>
														<option value="12">12 columns - 1/1</option>
													</select>
												</td>
												<td>
													<select name="column_col_lg" class="ve_column_col_field" data-type="width-lg">
														<option value="inherit" style="color: #ccc;">Inherit from smaller</option>
														<option value="1">1 column - 1/12</option>
														<option value="2">2 columns - 1/6</option>
														<option value="3">3 columns - 1/4</option>
														<option value="4">4 columns - 1/3</option>
														<option value="5">5 columns - 5/12</option>
														<option value="6">6 columns - 1/2</option>
														<option value="7">7 columns - 7/12</option>
														<option value="8">8 columns - 2/3</option>
														<option value="9">9 columns - 3/4</option>
														<option value="10">10 columns - 5/6</option>
														<option value="11">11 columns - 11/12</option>
														<option value="12">12 columns - 1/1</option>
													</select>
												</td>
												<td>
													<label>
														<input type="checkbox" name="column_hidden_lg" value="yes" class="">
													</label>
												</td>
											</tr>
											<tr class="ve_size-md">
												<td>
													<span title="Medium">
														<i class="fa fa-tablet-alt"></i>
													</span>
												</td>
												<td>
													<select name="column_offset_md" class="ve_column_offset_field" data-type="offset-md">
														<option value="inherit" style="color: #ccc;">Inherit from smaller</option>
														<option value="0" style="color: #ccc;">No offset</option>
														<option value="1">1 column - 1/12</option>
														<option value="2">2 columns - 1/6</option>
														<option value="3">3 columns - 1/4</option>
														<option value="4">4 columns - 1/3</option>
														<option value="5">5 columns - 5/12</option>
														<option value="6">6 columns - 1/2</option>
														<option value="7">7 columns - 7/12</option>
														<option value="8">8 columns - 2/3</option>
														<option value="9">9 columns - 3/4</option>
														<option value="10">10 columns - 5/6</option>
														<option value="11">11 columns - 11/12</option>
														<option value="12">12 columns - 1/1</option>
													</select>
												</td>
												<td>
													<span class="ve_description">Default value from width attribute</span>
												</td>
												<td>
													<label> 
														<input type="checkbox" name="column_hidden_md" value="yes" class="ve_column_offset_field">
													</label>
												</td>
											</tr>
											<tr class="ve_size-sm">
												<td>
													<span title="Small">
														<i class="fa fa-tablet-alt" style="transform:rotate(-90deg);"></i>
													</span>
												</td>
												<td>
													<select name="column_offset_sm" class="ve_column_offset_field" data-type="offset-sm">
														<option value="inherit" style="color: #ccc;">Inherit from smaller</option>
														<option value="0" style="color: #ccc;">No offset</option>
														<option value="1">1 column - 1/12</option>
														<option value="2">2 columns - 1/6</option>
														<option value="3">3 columns - 1/4</option>
														<option value="4">4 columns - 1/3</option>
														<option value="5">5 columns - 5/12</option>
														<option value="6">6 columns - 1/2</option>
														<option value="7">7 columns - 7/12</option>
														<option value="8">8 columns - 2/3</option>
														<option value="9">9 columns - 3/4</option>
														<option value="10">10 columns - 5/6</option>
														<option value="11">11 columns - 11/12</option>
														<option value="12">12 columns - 1/1</option>
													</select>
												</td>
												<td>
													<select name="column_col_sm" class="ve_column_col_field" data-type="width-sm">
														<option value="inherit" style="color: #ccc;">Inherit from smaller</option>
														<option value="1">1 column - 1/12</option>
														<option value="2">2 columns - 1/6</option>
														<option value="3">3 columns - 1/4</option>
														<option value="4">4 columns - 1/3</option>
														<option value="5">5 columns - 5/12</option>
														<option value="6">6 columns - 1/2</option>
														<option value="7">7 columns - 7/12</option>
														<option value="8">8 columns - 2/3</option>
														<option value="9">9 columns - 3/4</option>
														<option value="10">10 columns - 5/6</option>
														<option value="11">11 columns - 11/12</option>
														<option value="12">12 columns - 1/1</option>
													</select>
												</td>
												<td>
													<label>
														<input type="checkbox" name="column_hidden_sm" value="yes" class="ve_column_offset_field">
													</label>
												</td>
											</tr>
											<tr class="ve_size-xs">
												<td>
													<span title="Extra Small">
														<i class="fa fa-mobile-alt"></i>
													</span>
												</td>
												<td>
													<select name="column_offset_xs" class="ve_column_offset_field" data-type="offset-xs">
														<option value="0" style="color: #ccc;">No offset</option>
														<option value="1">1 column - 1/12</option>
														<option value="2">2 columns - 1/6</option>
														<option value="3">3 columns - 1/4</option>
														<option value="4">4 columns - 1/3</option>
														<option value="5">5 columns - 5/12</option>
														<option value="6">6 columns - 1/2</option>
														<option value="7">7 columns - 7/12</option>
														<option value="8">8 columns - 2/3</option>
														<option value="9">9 columns - 3/4</option>
														<option value="10">10 columns - 5/6</option>
														<option value="11">11 columns - 11/12</option>
														<option value="12">12 columns - 1/1</option>
													</select>
												</td>
												<td>
													<select name="column_col_xs" class="ve_column_col_field" data-type="width-xs">
														<option value="" style="color: #ccc;"></option>
														<option value="1">1 column - 1/12</option>
														<option value="2">2 columns - 1/6</option>
														<option value="3">3 columns - 1/4</option>
														<option value="4">4 columns - 1/3</option>
														<option value="5">5 columns - 5/12</option>
														<option value="6">6 columns - 1/2</option>
														<option value="7">7 columns - 7/12</option>
														<option value="8">8 columns - 2/3</option>
														<option value="9">9 columns - 3/4</option>
														<option value="10">10 columns - 5/6</option>
														<option value="11">11 columns - 11/12</option>
														<option value="12">12 columns - 1/1</option>
													</select>
												</td>
												<td>
													<label>
														<input type="checkbox" name="column_hidden_xs" value="yes" class="ve_column_offset_field">
													</label>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- start of map tab -->
					<div class="tab-pane fade" id="map-settings" role="tabpanel" aria-labelledby="map-settings">
						<div class="col-12">
							<%--<div>--%>
								<%--<button type="button" class="btn btn-default" id="clonePath">Add new path</button>--%>
							<%--</div>--%>
							<div id="mapPaths">
								<div>
									Map Directions <span class="pathNumber">#1</span>
								</div>
								<div class="form-group">
									<div class="row">
                                        <div data-element-property="pathColor">
                                            <label>Path Color</label>
                                            <div class="color">
                                                <button class="ve_color-picker btn">
                                                    <input type="hidden" name="pathColor[]">
                                                    <span>Select Color</span>
                                                </button>
                                            </div>
                                        </div>
                                        <div data-element-property="pathCoords">
                                            <label>Paths </label>
                                            <div class="color">
                                                <textarea name="path[]" style="width: 100%;"></textarea>
                                            </div>
                                        </div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- end of map tab -->
				</div>
			</div>
		</div>
		<div class="ve_ui-panel-footer-container">
			<div class="ve_ui-panel-footer-content">
	        	<button type="button" class="btn btn-secondary closeSettings">Close</button>
	        	<button type="button" class="btn btn-secondary saveSettings">Save</button>
			</div>
	    </div>
	</div>
</div>