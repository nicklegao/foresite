<%@page import="java.util.Collections"%>
<%@page import="java.util.Arrays"%>
<%@ page import="java.io.File" %>
<%@ page import="java.io.FilenameFilter" %>
<%@ page import="org.apache.commons.io.FileUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%
    String currentPath = request.getRealPath(request.getServletPath());
    File directory = new File(currentPath).getParentFile();
    File[] components = directory.listFiles(new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
            return name.endsWith(".js");
        }
    });
    List<File> jsFiles = Arrays.asList(components);
    int cfi = 0;
    for(File file : jsFiles) {
    	if(file.getName().equals("common.js")){
    		cfi = jsFiles.indexOf(file);
    		break;
    	}
    }
    if(cfi != 0) {
    	Collections.swap(jsFiles, cfi, 0);
    }
    response.setContentType("text/javascript");
%>
var icon = {};
var validAddLevel = {};
var componentName = {};
var componentDescription = {};
var validTabs = {};
var components = [];
var v = function(){console.log('testing');return 'test'}
var test = v();
var validProperties = {};

<%
    for (File component: jsFiles) {
        String contents =  FileUtils.readFileToString(component);
%>
components.push('<%= component.getName().substring(0, component.getName().indexOf(".")) %>');
<%= contents %>
<%
    }
%>