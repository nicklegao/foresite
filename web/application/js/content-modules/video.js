DEFAULTS.videoEdit = {properties:{aspect_ratio:"16x9"}};
validTabs["videoEdit"] = ["basic-elements"];
validProperties["videoEdit"] = {"general":["elementId", "className", "video"]};

$.widget('custom.videoEdit', {
	options : {
		properties : {
			aspect_ratio : '16x9'
		},
		inner : false
	},

	_create : function() {
		this.element.video = $(this.element).closest('.touch-container').siblings('.videoContainer').find('iframe');
		this._refresh();
	},

	_refresh : function() {
		 var element = this.element;
		 var properties = this.options.properties;
		 if(element.video.length>0) {
			 var ve_element = element.video.closest('.videoContainer');
			 
			 if(properties){
				 if(properties.url) {
		        	var videoCode;
		        	if(properties.url.indexOf('vimeo.com')>-1){
		        		videoCode = properties.url.substring(properties.url.lastIndexOf('/')+1, properties.url.length)
		        		videoEmbed = "//player.vimeo.com/video/"+ videoCode +"?title=0&amp;byline=0&amp;portrait=0";
		        	}
		        	else {
		        		videoCode = getUrlParam(properties.url, 'v');
		        		videoEmbed = "//www.youtube.com/embed/"+ videoCode +"?rel=0&amp;showinfo=0";
		        	}
		        	element.video.attr('src', videoEmbed);
		        	element.video.attr('height', '420px');
		        	element.video.attr('width', '350px');
				 }
				 
		        setProperties(ve_element, properties)
	        }
		 
		 }
	},

	_setOptions : function() {
		this._superApply(arguments);
		this._refresh();
	},

	_setOption : function(key, value) {
		this._super(key, value);
	}
});