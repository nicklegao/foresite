var ve_elements = {
	"newrow" : "Row",
	"colElement" : "Column",
	"block-text" : "Text",
	"content-library-text" : "Text",
	"block-image" : "Image",
	"content-library-images" : "Image",
	"block-zerorisk" : "Zerorisk",
	"block-itinerary" : "Itinerary",
	"block-video" : "Video",
	"content-library-videos" : "Video",
	"videoEdit" : "Video",
	"block-newpage" : "Page Break"
};

var ve_widgets = {
		"newrow" : "newrow",
		"colElement" : "colElement",
		"block-text" : "Text",
		"content-library-text" : "Text",
		"block-image" : "Image",
		"content-library-images" : "Image",
		"block-zerorisk" : "Zerorisk",
		"block-itinerary" : "Itinerary",
		"block-video" : "videoEdit",
		"content-library-videos" : "videoEdit",
		"block-newpage" : "Page Break"
}
var DEFAULTS = {};

$(function(){
	
	$('.ve_ui-panel').draggable({
		handle : '.ve_ui-panel-header-container',
		containment : "body",
		drag : function(e,ui){            
            // Do not permit to be more close than 80 px of the left border
            var headerHeight = $('.editor-header').outerHeight();
            if(ui.position.top 	 < headerHeight) { 
                ui.position.top = headerHeight;
			}
            if(ui.position.top > (screen.height - $(this).outerHeight())){
            	ui.position.top = screen.height - $(this).outerHeight() - headerHeight;
            }
        },
        stop : function(e, ui){
        	adjustPosition(ui.helper);
        }
	});
	$('.ve_ui-panel-inner').resizable({
		minHeight : 500,
		minWidth : 500,
		containment : "body",
        stop : function(e, ui){
        	adjustPosition(ui.element.closest('.ve_ui-panel'));
        }
	});

	$('.ve_color-picker').spectrum({
		preferredFormat : "hex",
		showInput : true,
		showPalette : true,
		showAlpha : true,
		showInitial : true,
		change : function(color)
		{
			$(this).css('backgroundColor', color.toString());
			$(this).find('input').val(color.toString());

			if ($('div[data-element-property="dynamicColor"]').is(':visible')) {
				$('#dynamic-color-svg-preview polygon').css('fill', color.toString());
			}
		}
	});
	
	$('.ve_ui-panel').each(function(){
		adjustPosition($(this));
	});
	$('.ve_ui-panel-edit .layout').on('click',function(){
		$(this).addClass('active').siblings().removeClass('active');
		$('.ve_ui-panel-edit input[name=layout]').val($(this).data('layout'));
		createLayoutTable(null, $(this).data('layout'));
	});
	
	$('.ve_ui-panel-edit .saveSettings').on('click', function()
	{
		var target = $(this).closest('.ve_ui-panel-edit').data('target');
		var type = $(this).closest('.ve_ui-panel-edit').data('ve_type');
		var editPanel = $(this).closest('.ve_ui-panel');
		// $(target).textblock('option', {text: $('#ve_textblock_editor',
		// '.ve_ui-panel-edit').froalaEditor('html.get')});


		var option = {};
		/*
		 * for(var key in CKEDITOR.instances) { var $input =
		 * $(editPanel).find('#' + key); if($input.length > 0) { option.text =
		 * editors[key].getData(); } }
		 */

		//option.text = CKEDITOR.instances.ve_textblock_editor.getData();

		// option.text = editors.ve_textblock_editor.getData();
		var properties = {};

		$.each(Object.keys(validProperties[type]), function()
		{
			var tabPane = $('#' + $('.nav-item[data-setting=' + this + '] a.nav-link', editPanel).attr('aria-controls'), editPanel);
			var saveAll = $.inArray('*', validProperties[type][this]) > -1;
			if (saveAll)
			{
				tabPane.find('input[name], select[name], textarea[name]').each(function()
				{
					if ($(this).attr('name') == 'SYS_CSRF_TOKEN')
					{
						return;
					}
					if($(this).attr('type')=='checkbox'){
						if($(this).is(':checked')){
							appendProperty(properties, $(this).attr('name'), $(this).val());
						}
					}
					else {
						if($(this).val())
							properties[$(this).attr('name')] = $(this).val();
					}

				});
			}
			else
			{
				validProperties[type][this].forEach(function(value, index)
				{
					$('[data-element-property=' + value + ']', editPanel).find('input[name], select[name], textarea[name]').each(function()
					{
						if ($(this).attr('name') == 'SYS_CSRF_TOKEN')
						{
							return;
						}
						if($(this).attr('type')=='checkbox'){
							if($(this).is(':checked')){
								appendProperty(properties, $(this).attr('name'), $(this).val());
							}
						}
						else {
							properties[$(this).attr('name')] = $(this).val();
						}
					});
				});
			}
		});

		option.properties = properties;

		$(target)[type]('option', option);
		
		if(type=='newrow') {
			var rowColumns = target.find('.ve_row_container').eq(0).find('.ve_element.ve_col').eq(0).siblings().addBack();
			if($('.col-layout-responsive[updated=true]', editPanel).length > 0){
				var updateCols = [];
				$.each($('.col-layout-responsive[updated=true]', editPanel), function(index, select){
					var wrapper = $(this).parent('.select-wrapper');
					var $col = $(rowColumns[wrapper.attr('index')]); 
					var colOptions = $col.colElement('instance').options;
					if(!colOptions.properties){
						colOptions.properties = {};
					}
					colOptions.properties['column_col_'+wrapper.attr('size')]=$(select).val()
					$col.colElement('option', colOptions);
				});
			}
		}
		
		$('.ve_ui-panel-edit').hide();
		
		if(type=='colElement') {
			setLayout($(target).closest('.ve_element.ve_row'));
		}
	});
	
	
	
	$.fn.extend({
        donetyping: function(callback,timeout){
            timeout = timeout || 1000;
            var timeoutReference,
                doneTyping = function(el){
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
            return this.each(function(i,el){
                var $el = $(el);
                $el.is(':input') && $el.on('keyup keypress paste',function(e){
                    if (e.type=='keyup' && e.keyCode!=8) return;
                    
                    if (timeoutReference) clearTimeout(timeoutReference);
                    timeoutReference = setTimeout(function(){
                        doneTyping(el);
                    }, timeout);
                }).on('blur',function(){
                    doneTyping(el);
                });
            });
        }
    });
	$('#layout-settings input[name=layout]').donetyping(function(){
		console.log('changed');
		if(rowLayoutValid($(this).val()))
		{
			$(this).closest('.form-group').removeClass('has-error');
			createLayoutTable(null, $(this).val());
		}
		else {
			$(this).closest('.form-group').addClass('has-error');
		}
	});
	
	$(document).on('mouseover', '.ve_col-handle', function()
	{
		var element = $(this).closest('.ve_row .row:not(.ve_row_inner_container)');
		columnSortable(element);
	});
	
	$('.ve_row .row:not(.ve_row_inner_container)').each(function(){
		columnSortable(this);
	});
	
	$('.ve_ui-panel-edit .closeSettings').on('click', function()
	{
		$(this).closest('.ve_ui-panel-edit').hide();
	});
});

function rowLayoutValid (layout) {
	var valid = true;
	layout.split('+').forEach(function(value, i){
		if(!value.match(/\b((1\/12)|(1\/6)|(1\/4)|(1\/3)|(5\/12)|(1\/2)|(7\/12)|(2\/3)|(3\/4)|(5\/6)|(11\/12)|(1\/1))\b/g))
		{
			valid = false;
		}
	});
	return valid;
}


function adjustPosition(ui){
    var headerHeight = $('#veadminbar').outerHeight();
	var e = ui.find('.ve_ui-panel-inner');
	var width = parseInt(e.css('width'));
	var height = parseInt(e.css('height'));
	
	if(width > window.innerWidth){
		e.css("width", window.innerWidth + "px");
	}
	if(height > window.innerHeight - headerHeight){
		e.css("height", (window.innerHeight - headerHeight) + "px");
	}

	var top = parseInt(ui.css('top'));
	var left = parseInt(ui.css('left'));

	if(top < headerHeight){
		ui.css('top', headerHeight+'px');
	}
	
	if(left <0){
		ui.css('left','0px');
	}
	
	var width = parseInt(e.css('width'));
	var height = parseInt(e.css('height'));
	var top = parseInt(ui.css('top'));
	var left = parseInt(ui.css('left'));
	
	if(top + height > window.innerHeight){
		ui.css('top', (window.innerHeight - height)+'px');
	}
	
	if(left + width > window.innerWidth){
		ui.css('left',(window.innerWidth - width)+'px');
	}
	
}
function setProperties(element, properties)
{
	var target = element;
	var type = target.data('ve_type'); 
	if(type=='colElement')
		target = target.find('.ve_col_container').eq(0);
	if(type=='colElement' || type=='newrow') {
		target.css('margin','');
		target.css('padding','');
		target.css('border','');
		target.css('border-radius','');
	}
	$.each(Object.keys(properties), function(index, key)
	{
		if (properties[key])
		{
			if(key == 'aspect_ratio' && properties.aspect_ratio) {
				element.removeClass((element.attr('class').match(/ratio_(\d{1,2})x(\d{1,2})/g) || []).join(' '));
				element.addClass('ratio_'+properties.aspect_ratio);
			 }
			if(key == 'column_gutter') {
				element.removeClass(element.attr('class').match(/gtr-(\d{1,2})/g));
				element.addClass(properties.column_gutter);
			}
			if(key == 'parallax-effect') {
				element.removeClass(element.attr('class').match(/parallax-[a-z]*/));
				element.addClass(properties['parallax-effect']);
			}
			if(key == 'row_height') {
				element.removeClass(element.attr('class').match(/height-[a-z][a-z]*/));
				element.addClass('height-'+properties.row_height);
			}
			if (key == 'column_width' && element.data('ve_type')=='colElement') {
				var matches = element.attr('class').match(/(col|offset)(-(lg|md|sm|xs))?-(\d{1,2})/g);
				if(matches) {
					matches.forEach(function(match, index){
						element.removeClass(match);
					});
				}
				
				element.addClass('col-' + properties.column_width);
//				element.addClass(getColumn_property_lg(properties, 'col')).addClass(getColumn_property_sm(properties, 'col')).addClass(getColumn_property_xs(properties, 'col'));
//				element.addClass(getColumn_property_lg(properties, 'offset')).addClass(getColumn_property_md(properties, 'offset')).addClass(getColumn_property_sm(properties, 'offset')).addClass(getColumn_property_xs(properties, 'offset'));
			}
			if (key == 'textcolor_style'){
				target.css('color',  properties.textcolor_style == 'custom' ? properties.text_color : 'inherit');
			}
			
			if (key == 'id')
				target.attr('id', properties.id);
			if (key == 'className')
			{
				target.removeClass(properties.old_className);
				target.addClass(properties.className);
				properties.old_className = properties.className;
			}
			if(key == 'alignment')
				target.css('text-align', properties.alignment)
			
			if (key == 'height')
				target.css('height', cssLength(properties.height));
			if (key == 'margin_top')
				target.css('margin-top', cssLength(properties.margin_top));
			if (key == 'margin_bottom')
				target.css('margin-bottom', cssLength(properties.margin_bottom));
			if (key == 'margin_left')
				target.css('margin-left', cssLength(properties.margin_left));
			if (key == 'margin_right')
				target.css('margin-right', cssLength(properties.margin_right));
			if (key == 'padding_top')
				target.css('padding-top', cssLength(properties.padding_top));
			if (key == 'padding_bottom')
				target.css('padding-bottom', cssLength(properties.padding_bottom));
			if (key == 'padding_left')
				target.css('padding-left', cssLength(properties.padding_left));
			if (key == 'padding_right')
				target.css('padding-right', cssLength(properties.padding_right));
			if (key == 'border_style')
			{
				if (properties.border_top)
				{
					target.css('border-top', cssLength(properties.border_top) + ' ' + properties.border_style + ' ' + properties.border_color);
				}
				if (properties.border_bottom)
				{
					target.css('border-bottom', cssLength(properties.border_bottom) + ' ' + properties.border_style + ' ' + properties.border_color);
				}
				if (properties.border_left)
				{
					target.css('border-left', cssLength(properties.border_left) + ' ' + properties.border_style + ' ' + properties.border_color);
				}
				if (properties.border_right)
				{
					target.css('border-right', cssLength(properties.border_right) + ' ' + properties.border_style + ' ' + properties.border_color);
				}
			}
			if (key == 'border_radius')
				target.css('border-radius', cssLength(properties.border_radius));
//			if (key == 'background_color')
//				target.css('background-color', properties.background_color);
			if (key == 'background_style')
			{
				target.css('background-color', '');
				target.css('background-image', '');
				target.css('background-size', '');
				target.css('background-position', '');
				if (properties.background_style == 'single-color')
				{
					target.css('background-color', properties.background_single_color);
				}
				if (properties.background_style == 'image')
				{
					target.css('background-image', 'url("' + properties.background_image + '")');
					target.css('background-size', 'cover');
					target.css('background-position', 'center');
				}
			}

			if (key == 'color')
			{
				target.find('.ve_element_container').css('color', properties.color);
			}
			if (key == 'fontSize')
			{
				target.find('.ve_element_container').css('font-size', properties.fontSize);
			}
			if(key == 'max_width')
			{
				target.css('max-width', cssLength(properties.max_width));
			}
			
		}
	});
}

function rowSortable(element)
{
	// Row
	// var element = $($handle).closest('.ve_edit-mode');
	if(!$(element).data('ui-sortable')) {
		$(element).sortable({
			containerSelector: 'div',
			itemSelector: '>.ve_element.ve_row',
			handle: '.ve_row-handle',
			pullPlaceholder: false,
		});
	}
}

function columnSortable(element)
{
	// Column
	// var element = $($handle).closest('.ve_row
	// .row:not(.ve_row_inner_container)');
	if(!$(element).data('ui-sortable')) {
		$(element).sortable({
			containerSelector: 'div',
			connectWith: '.ve_row_container',
			handle: '.ve_col-handle',
			itemSelector: '.ve_col',
			pullPlaceholder: false,
			// Create a blank column element so empty elements after drag aren't
			// broken
			receive: function (event, ui) {
				// var $container = ui.item.closest('.ve_row');
				var $sender = ui.sender;
				if ($sender.children().length == 0) {
					var newCol = $('<div>').addClass('ve_element ve_col');
					newCol.colElement({
						inner: false,
						colWidth: 12
					});
					$sender.closest('.ve_row_container').append(newCol);
				}
			},
			update: function (event, ui) {
				var row = ui.item.closest('.ve_element.ve_row');
				row.newrow('instance').options.properties.layout = getLayout(row);
			}
			// out : function(event, ui)
			// {
			// var $container = ui.sender;
			// if ($container.children().length == 0)
			// {
			// $container.find('.testAdd').click();
			// }
			// }
		});
	}
}

function elementSortable(element)
{
	// Element
	// var element =
	// $($handle).closest('.ve_element_container.ve_col_container');
	if(!$(element).data('ui-sortable')) {
		$(element).sortable({
			containerSelector: 'div',
			connectWith: '.ve_col_container',
			handle: '.ve_element-handle, .ve_row-inner-handle',
			itemSelector: '.ve_element:not(.ve_col, .ve_row), .ve_row_inner',
			pullPlaceholder: false,
			receive: function (event, ui) {
				var $container = ui.item.closest('.ve_element_container');
				// $container.closest('.ve_col').removeClass('ve_col_empty');
				if ($container.children().length > 0) {
					$container.removeClass('ve_element-container-empty');
				}
			},
			out: function (event, ui) {
				var $container = ui.sender.closest('.ve_element_container');
				if ($container.children().length == 0) {
					$container.addClass('ve_element-container-empty');
				}
			}
		});
	}
}

function setupSortables()
{
	$('.ve_row .row:not(.ve_row_inner_container)').each(function(){
		columnSortable(this);
	});
}

function toFraction(width)
{
	var total = 12;
	var hcf = function hcf(a, b)
	{
		return b ? hcf(b, a % b) : a;
	};
	hcf = hcf(width, total);
	return width / hcf + '/' + total / hcf;
}

function showEditPanel(target, panel, changeLayout)
{
	var editWidgetPanel = panel ? $(panel) : $('#ve_ui-panel-edit');
	var ve_type = target.data('ve_type');
	var widget = target.data('custom-' + ve_type);
	
	$.each(editWidgetPanel.find('.nav-item'), function()
	{
		var nav_item = $(this);
		var aria_controls = nav_item.find('.nav-link').attr('aria-controls');
		var properties = validProperties[ve_type];
		if ($.inArray(nav_item.data("setting"), Object.keys(properties)) > -1)
		{
			nav_item.show();
			var showAll = $.inArray('*', properties[nav_item.data("setting")]) > -1;
			$.each(editWidgetPanel.find('.tab-pane#' + aria_controls).find('[data-element-property]'), function()
			{
				var property = $(this);
				if (showAll || $.inArray(property.data("element-property"), properties[nav_item.data("setting")]) > -1)
				{
					if(property.data("element-property")=='slideshow'){
						var widgetProperties = widget.options;
						var slideContainer = $('.slide-images', property);
						slideContainer.find('.slide').remove();
						var slideDetailContainer = $('.slide-detail-container', property);
						slideDetailContainer.find('.slide-detail').remove();
						var slidePagination = $('.slide-pagination', property);
						slidePagination.children().remove();
						$('[name=slide_count]', slideContainer).val(widgetProperties.slide_count);
						for(var i = 1; i <= widgetProperties.slide_count; i++) {
							var slide = $('<div class="slide">');
							slide.attr('slide', i);
							var thumbnail = $('<div class="thumbnail">');
							var remove = $('<a class="remove-slide"><i class="fa fa-times"></i></a>');
							var image = $('<img>').attr('src', widgetProperties['image_'+i]);
							var input = $('<input type="hidden">').attr('name', 'image_'+i).val(widgetProperties['image_'+i]);
							
							thumbnail.append(image).append(remove);
							slide.append(thumbnail);
							slide.append(input);
							
							slide.insertBefore($(".filemanager-add-image", slideContainer));
							
							var slideDetail = $('.slide-detail-template .slide-detail', property).clone();
							slideDetail.attr('slide-detail', i);
							var slideTitle = slideDetail.find('[name=slide_title]');
							var slideSubTitle = slideDetail.find('[name=slide_sub_title]');
							
							slideTitle.attr('name', 'slide_title_'+i).val(widgetProperties['slide_title_'+i]);
							slideSubTitle.attr('name', 'slide_sub_title_'+i).val(widgetProperties['slide_sub_title_'+i]);
							slideDetailContainer.append(slideDetail);
							
							slide.data('slide-detail', slideDetail);
							
							var slidePage = $('<span class="badge">');
							slidePage.attr('page', i);
							slidePage.text(i);
							$('.slide-pagination', property).append(slidePage);
							
							if(i==1){
								slide.addClass('active');
								slideDetail.addClass('active');
								slidePage.addClass('active');
							}
						}
						slideContainer.sortable({
							items: '.slide',
							update: function( event, ui ) {
								resetSlideOrder($(this));
							}
						});
					}
					else {
						property.find('[name]:not([type=file])').each(function()
						{
							var input = $(this);
							console.log('name: '+input.attr('name'));
							
							if(input.attr('type')=='checkbox'){
								if(widget.options.properties[input.attr('name')]){
									var array = widget.options.properties[input.attr('name')].toString().split(",");
									input.prop('checked', array.indexOf(input.val()) > -1);
								}else{
									input.prop('checked', false);
								}
							}
							else {
								input.val(widget.options.properties[input.attr('name')]);
							}
							
							if(input.siblings('.thumbnail').length > 0) {
								var thumbnail = input.siblings('.thumbnail'); 
								thumbnail.find('img').attr('src', input.val());
								if(input.val()=="/images/test_image.jpg" || input.val()=="")
									thumbnail.hide();
								else 
									thumbnail.show();
							}
							else if(input.parent('.ve_color-picker').length > 0){
								input.parent('.ve_color-picker').spectrum('set',input.val()?input.val():"rgba(255, 255, 255, 0)");
							}
							
							if(input.attr('name')=='click_action'){
								editWidgetPanel.find('.link-title').text('');
								editWidgetPanel.find('.link-url').text('');
								editWidgetPanel.find('.file-name').text('');
								if(input.val()=='link') {
									editWidgetPanel.find('.link-title').text(widget.options.properties['linkText']);
									editWidgetPanel.find('.link-url').text(widget.options.properties['linkURL']);
								}
								else if(input.val()=='file'){
									editWidgetPanel.find('.file-name').text(widget.options.properties['linkURL'].substring(widget.options.properties['linkURL'].lastIndexOf('/')+1));
								}
							}
							
							if(input.attr('name')=='layout'){
								$('#layout-options .layout.active', editWidgetPanel).removeClass('active');
								$('#layout-options .layout[data-layout="'+widget.options.properties.layout+'"]', editWidgetPanel).addClass('active');
							}

						});
					}
					
					property.show();
				}
				else
				{
					property.hide();
				}
			});
		}
		else
		{
			nav_item.hide();
		}
	});

//	toggleBackgroundSetting($('select[name=background_style]', editWidgetPanel));
//	toggleSeperatorSetting($('select[name=seperator_style]', editWidgetPanel));
//	toggleClickActionSetting($('select[name=click_action]', editWidgetPanel));
//	toggleIconAlignmentSetting($('select[name=icon_position]', editWidgetPanel));
//	togglePortfolioOrderSetting($('select[name=orderby]', editWidgetPanel));
//	toggleTextColorSetting($('select[name=textcolor_style]', editWidgetPanel));
//	toggleTitleColorSetting($('select[name=titlecolor_style]', editWidgetPanel));

	$('a.nav-link.active', editWidgetPanel).removeClass('active').attr('aria-selected', false);
	$('.tab-pane.active', editWidgetPanel).removeClass('active');


	$("#ve_element-name", editWidgetPanel).text(changeLayout?'Row Layout':(ve_elements[ve_type] + ' Settings'));
	editWidgetPanel.show().data('target', target).data('ve_type', ve_type);
	
	if(changeLayout) {

		createLayoutTable(target);
		
		$('a.nav-link#layout-settings-tab', editWidgetPanel).addClass('active').attr('aria-selected', true);
		$('.tab-pane#layout-settings', editWidgetPanel).addClass('active show');

		$('.nav.nav-tabs', editWidgetPanel).hide();
	}
	else {

		$('.nav.nav-tabs', editWidgetPanel).show();
		$('.nav-item[data-setting=layout]').hide();
		
		var activeTab = $('a.nav-link:visible', editWidgetPanel).eq(0);
		activeTab.addClass('active').attr('aria-selected', true);
		
		$('.tab-pane#'+activeTab.attr('aria-controls'), editWidgetPanel).addClass('active show');
	}
	
}

function convertToJSON($container)
{
	var children = {};
	children.elements = buildElements($container);
	return children;
}

function buildElements($parentContainer)
{
	var elements = [];
	if ($parentContainer.find('.ve_element').eq(0).siblings('.ve_element').length > 0) {
		$parentContainer.find('.ve_element').eq(0).siblings('.ve_element').addBack().each(function()
		{
			var elementType = $(this).data('ve_type');
			var widget = $(this).data('custom-' + elementType);
			var element = {};
			element.type = elementType;
			if (widget != null)
			{
				element.properties = widget.options.properties;
				element.text = widget.options.text;
				element.inner = widget.options.inner || false;
			}
			if ($(this).find('.ve_element_container').eq(0).siblings('.ve_element_container').length > 0) {
				$(this).find('.ve_element_container').eq(0).siblings('.ve_element_container').addBack().each(function()
				{
					if($(this).hasClass('ve_col_container')) {
						var children = [];
						$.each($(this).children('.content-holder'), function(i, panel)
						{
							var content = getTravelDocContent(panel);
							if (typeof content == 'object')
							{
								children.push(content);
							}
						});
						element.children = children;
					}
					else {
						var children = buildElements($(this));
						if (children.length > 0)
						{
							element.children = children;
						}
					}
				});
			} else {
				$(this).find('.ve_element_container').eq(0).each(function()
				{
					if($(this).hasClass('ve_col_container')) {
						var children = [];
						$.each($(this).children('.content-holder'), function(i, panel)
						{
							var content = getTravelDocContent(panel);
							if (typeof content == 'object')
							{
								children.push(content);
							}
						});
						element.children = children;
					}
					else {
						var children = buildElements($(this));
						if (children.length > 0)
						{
							element.children = children;
						}
					}
				});
			}

			elements.push(element);
		});
	} else {
		$parentContainer.find('.ve_element').eq(0).each(function()
		{
			var elementType = $(this).data('ve_type');
			var widget = $(this).data('custom-' + elementType);
			var element = {};
			element.type = elementType;
			if (widget != null)
			{
				element.properties = widget.options.properties;
				element.text = widget.options.text;
				element.inner = widget.options.inner || false;
			}

			if ($(this).find('.ve_element_container').eq(0).siblings('.ve_element_container').length > 0) {
				$(this).find('.ve_element_container').eq(0).siblings('.ve_element_container').addBack().each(function()
				{
					if($(this).hasClass('ve_col_container')) {
						var children = [];
						$.each($(this).children('.content-holder'), function(i, panel)
						{
							var content = getTravelDocContent(panel);
							if (typeof content == 'object')
							{
								children.push(content);
							}
						});
						element.children = children;
					}
					else {
						var children = buildElements($(this));
						if (children.length > 0)
						{
							element.children = children;
						}
					}
				});
			} else {
				$(this).find('.ve_element_container').eq(0).each(function()
				{
					if($(this).hasClass('ve_col_container')) {
						var children = [];
						$.each($(this).children('.content-holder'), function(i, panel)
						{
							var content = getTravelDocContent(panel);
							if (typeof content == 'object')
							{
								children.push(content);
							}
						});
						element.children = children;
					}
					else {
						var children = buildElements($(this));
						if (children.length > 0)
						{
							element.children = children;
						}
					}
				});
			}
			elements.push(element);
		});
	}
	return elements;
}

function columnSortable(element)
{
	if(!$(element).data('ui-sortable')) {
		$(element).sortable({
			containerSelector: 'div',
			connectWith: '.ve_row_container',
			handle: '.ve_col-handle',
			itemSelector: '.ve_col',
			pullPlaceholder: false,
			// Create a blank column element so empty elements after drag aren't
			// broken
			receive: function (event, ui) {
				var $sender = ui.sender;
				if ($sender.children().length == 0) {
					var newCol = $('<div>').addClass('ve_element ve_col');
					newCol.colElement({
						inner: false,
						colWidth: 12
					});
					$sender.closest('.ve_row_container').append(newCol);
				}
			},
			update: function (event, ui) {
				var row = ui.item.closest('.ve_element.ve_row');
				row.newrow('instance').options.properties.layout = getLayout(row);
			}
		});
	}
}

function cssLength(value) {
	return $.isNumeric(value) ? (value+'px') : value;
}

function getUrlParam(url, name)
{
	var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(url);
	if (results)
		return results[1];
	else
		return '';
};