$(document).ready(function()
{
	$("a.settingsMenu").click(function()
	{

		var url = $(this).data("url");
		var element = $(this);
		$("#settingsContentArea").html('');
		$("#settingsContentArea").load("/application/include/" + url, function(responseTxt, statusTxt, xhr)
		{
			if (statusTxt == "success")
			{
				$(".settingsMenu").removeClass('active');
				$(element).addClass('active');
				$('[data-toggle="dropdown"]').dropdown();
			}

			if (url === "settingsRoles.jsp")
			{
				initRolesDataTable();
			}


			if (statusTxt == "error")
				alert("Error: " + xhr.status + ": " + xhr.statusText);
		});
	});

	$('#account-security').click(function()
	{
		$('#settingsMenuSecurity').click();
	});

	loadSettings();

	// $(document).on('click', '#addItem', function() {
	// if ($(this).find('addItemText').html() === "Add Role") {
	// addItemAction();
	// }
	// });

	$(document).on('click', '#resend-activation-item', function()
	{
		var id = $(this).closest(".btn-group").attr("data-id");
		resendActivationEmail(id)
	});
});


function loadSettings()
{
	if (window.location.href.indexOf('#') != -1)
	{
		var menu = location.href.substr(location.href.indexOf("#"));
		window.location.hash = '';

		$(menu).click();
	}
	else
	{
		$("#settingsContentArea").load("/application/include/settingsUsers.jsp", function(responseTxt, statusTxt, xhr)
		{
			if (statusTxt == "error")
				alert("Error: " + xhr.status + ": " + xhr.statusText);
		});
		$('[data-toggle="dropdown"]').dropdown();
	}
}

function initRolesDataTable()
{

	var domBody = "t";
	var domFooter = "<'navbar' <'navbar-form navbar-left' i> <'navbar-form navbar-right'<'form-group' p>> <'navbar-form navbar-right' <'#actionsHolder'>> >";

	$("#table-roles").DataTable({
		"oLanguage" : {
			"sLengthMenu" : "_MENU_",
			"sSearch" : "",
			"oPaginate" : {
				"sPrevious" : "",
				"sNext" : ""
			},
		},
		"columns" : [ null, {
			"orderable" : false
		} ],
		"sDom" : domBody + domFooter,
		"fnDrawCallback" : function(oSettings)
		{
			$('#users-management-modal .edit-item').click(editRowItemAction);
			$('#addItem').click(addRowItemAction)
			$('#users-management-modal #delete-item').click(deleteRowItemAction);
			$('[data-toggle="dropdown"]').dropdown();
		}
	});


	// $('#users-management-modal .dataTables_filter').prepend('<label
	// class="search-label"><span class="search-icon fa
	// fa-search"></span></label>');
	// $('#users-management-modal .dataTables_filter
	// input').addClass("search-box form-control").attr("placeholder",
	// "Search...");
}

function migrateRolesAction(id)
{
	var params = {};

	params.id = id;

	$('body').modalmanager('loading');

	var $modal = $('#migrations-modal');
	$modal.load('/application/dialog/migrationsRole.jsp', params, function()
	{
		$modal.modal({
			width : '660px',
			backdrop : 'static',
			keyboard : false
		});
	});
}

function deleteRowItemAction()
{
	var params = {};
	var id = $(this).closest(".btn-group").attr("data-id");
	var module = $(this).closest("table").attr('data-module');
	var action = "delete";

	params.id = id;

	swal({
		title : 'Are you sure?',
		text : 'Do you want to delete this Role?',
		type : 'warning',
		showCancelButton : true,
	}, function(isConfirm)
	{
		if (isConfirm)
		{
			$.ajax({
				url : '/api/deleteRole',
				data : {
					id : id
				},
				success : function(data)
				{
					stopLoadingForeground();
					if (data.success === false)
					{
						if (data.action === 'migrate')
						{
							migrateRolesAction(id);
						}
						else
						{
							swal({
								title : 'Error',
								type : 'error',
								text : data.message
							});
						}
					}
					else
					{
						swal({
							title : 'Deleted!',
							text : 'The Role has been deleted.',
							type : 'success'
						});
						$('#settingsMenuRoles').click();
					}
				},
				error : function()
				{
					stopLoadingForeground();

					alert("Unable to delete your " + module + ". Please try again later.");
				}
			})
		}
	})
}


function addRowItemAction()
{
	var params = {};
	var action = "add";

	params.action = action;

	$('body').modalmanager('loading');

	var $modal = $('#edit-management-modal');
	$modal.load('/application/dialog/editRole.jsp', params, function()
	{
		$modal.modal({
			width : '772px',
			backdrop : 'static',
			keyboard : false
		});
	});
}


function editRowItemAction()
{
	var params = {};
	var id = $(this).closest(".btn-group").attr("data-id");
	var module = $(this).closest("table").attr('data-module');
	var action = "edit";

	params.id = id;
	params.action = action;

	$('body').modalmanager('loading');

	var $modal = $('#edit-management-modal');

	$modal.load('/application/dialog/editRole.jsp', params, function()
	{
		console.log("editing " + module);
		$modal.modal({
			width : '772px',
			backdrop : 'static',
			keyboard : false
		});
	});
}

function resendActivationEmail(userButton)
{
	startLoadingForeground();
	$.ajax({
		url : '/api/resendActivation',
		data : {
			userSelected : userButton
		},
		success : function(data)
		{
			stopLoadingForeground();
			if (data.success)
			{
				swal({
					title : 'Success',
					type : 'success',
					text : data.message,
				});
			}
			else
			{
				swal({
					title : 'Error',
					type : 'error',
					text : data.message,
				});
			}
		},
		error : function()
		{
			swal({
				title : "Error",
				type : "error",
				text : "Could not send an activation email. Please try again later."
			});
		}
	});
}
