var currentHistoryTableDataRequest = undefined;

$(document).ready(function()
{

	if ($('#proposal-management').is(':visible'))
	{
		managementModalShown();
	}

	$("#proposal-management").on("click", ".proposalDeleteAction", proposalDeleteAction)

	.on("click", ".handoverProposalAction", handoverProposalAction)

	.on("click", ".handoverProposalActionError", handoverProposalActionError)

	.on("click", ".saveTemplateAction", saveAsTemplateAction)

	.on("click", ".copyProposalAction", copyProposalAction);

	if (isTouchSupported())
	{
		$("#expireDateSelector").attr("readonly", "true");
	}

	// Run fixedHeaderTable plugin only on tabpanel shown
	$('#proposal-management a[data-toggle="tab"]').one('shown.bs.tab', function(e)
	{
		var tabId = $(e.target).attr("aria-controls");
		var tabPanelDiv = "div#" + tabId;
		var $tabPanel = $(tabPanelDiv);

		$(".fixedHeaderTable", $tabPanel).fixedHeaderTable({
			footer : false,
			cloneHeadToFoot : false,
			fixedColumn : false,
			height : '205'
		});
	});
});

function handoverProposalAction(event)
{
	var proposalId = $("#proposal-management").attr("data-id");
	$("#proposalHandoverModal #handover-proposal-id").val(proposalId);
}

function handoverProposalActionError(event)
{
	handoverError();
}


function saveAsTemplateAction(event)
{
	var proposalId = $("#proposal-management").attr("data-id");

	$("#saveAsTemplateModal #save-template-proposal-id").val(proposalId);
}

function proposalDeleteAction(event)
{
	var proposalId = $("#proposal-management").attr("data-id");

	$("#proposalDeleteModal .proposalDeleteConfimed").attr("data-id", proposalId);
}

function managementModalShown()
{
	var $modal = $('#proposal-management');

	$(".displayOderNow", $modal).iCheck({
		checkboxClass : 'icheckbox_square-red',
		radioClass : 'iradio_square-red'
	}).on('ifClicked', function(event)
	{
		$("#submitButton", $modal).prop('disabled', false);
	}).on('ifChecked', function(event)
	{
		$("#showDisplayOrder", $modal).val('1');
	}).on('ifUnchecked', function(event)
	{
		$("#showDisplayOrder", $modal).val('0');
	});

	$("#outcomeChanger", $modal).change(function(event)
	{
		var $select = $(event.currentTarget);
		var $option = $("option:selected", $select);

		if ($option.attr('data-accepted') == undefined && $option.val() != '')
		{
			$("#reasonChanger", $modal).show();
		}
		else
		{
			$("#reasonChanger", $modal).hide();
			$("#reasonChanger option", $modal).first().prop('selected', true);
		}

		$("#submitButton", $modal).prop('disabled', false);
	}).change();

	$("#reasonChanger", $modal).change(function(event)
	{
		$("#submitButton", $modal).prop('disabled', false);
	}).change();

	$("#expireDateSelector", $modal).daterangepicker({
		singleDatePicker : true,
		autoApply : true,
		showDropdowns : true,
		drops : 'up',
		locale : {
			format : "DD/MM/YYYY"
		}
	}, function(start, end, label)
	{
		$("#submitButton", $modal).prop('disabled', false);
	});

	$("#submitButton", $modal).prop('disabled', true);// hack because of calls
	// to .change() above
	$modal.resize();

	$modal.on('click', ".fixedHeaderTable tr", function(event)
	{
		var $tr = $(event.currentTarget);

		var proposalId = $tr.attr("data-id");
		var vKey = $tr.attr("data-vkey");

		$(".viewRevisionAction").attr("href", "/view?traveldoc=" + proposalId + "&v-key=" + vKey);
		$(".viewRevisionAction").attr("target", "view-" + proposalId);

	});


	$('#proposalSummaryForm', $modal).validate({
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error, element)
		{
			error.insertAfter(element);
		},
		rules : {
			setExpiry : {
				rangelength : [ 10, 10 ],
				required : true
			},
			setOutcome : {
				required : false
			},
			setReason : {
				required : function(element)
				{
					return $(element).is(':visible');
				}
			}
		},
		submitHandler : function(form)
		{

			$("#submitButton", $modal).prop('disabled', true);

			startLoadingForeground();
			$(document).one('foregroundLoadingComplete', function()
			{
				$('.cd-panel').toggleClass('is-visible');
				redrawTable();
			});

			$(form).ajaxSubmit({
				success : function(success)
				{
					stopLoadingForeground();
				},
				error : function()
				{
					$("#submitButton", $modal).prop('disabled', false);
					stopLoadingForeground();

					alert("Unable your changes. Please try again later.");
				}
			});
		},
		ignore : '#displayOderNowOption',

		//
		// CUSTOM DO NOT REUSE
		//
		highlight : function(element)
		{
			var cell = $(element).closest('td').removeClass('has-success').addClass('has-error');
			cell.prevUntil('th').add(cell.prevAll('th').first()).removeClass('has-success').addClass('has-error');
		},
		unhighlight : function(element)
		{
			var cell = $(element).closest('td').removeClass('has-error');
			cell.prevUntil('th').add(cell.prevAll('th').first()).removeClass('has-error');
		},
		success : function(label, element)
		{
			var cell = $(element).closest('td').removeClass('has-error').addClass('has-success');
			cell.prevUntil('th').add(cell.prevAll('th').first()).removeClass('has-error').addClass('has-success');
		}
	});

	$("#submitButton", $modal).click(function(event)
	{
		$("#proposalSummaryForm", $modal).submit();
	});
}
