<!DOCTYPE html>
<!--[if lt IE 9]><html class="bad-ie" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.control.StripeController"%>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.LinkedList" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Locale" %>
<%@page import="java.util.Map.Entry" %>
<%@page import="java.util.Set" %>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="com.google.gson.Gson" %>
<%@page import="au.com.ci.sbe.util.UrlUtils" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.StripeService" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.DateFormatUtils" %>
<%@ page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager" %>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%@ page import="au.net.webdirector.common.datalayer.client.ElementData" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.*" %>
<%@ page import="au.corporateinteractive.qcloud.market.enums.Market" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.control.UserController" %>
<html lang="en">
<!--<![endif]-->
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="false" compressJavaScript="false" compressCss="true">
	<head>

		<%


			TUserAccountDataAccess uada = new TUserAccountDataAccess();
			UserAccountDataAccess uda = new UserAccountDataAccess();

			String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			String userUsername = (String)session.getAttribute(Constants.PORTAL_USERNAME_ATTR);
			CategoryData company = uada.getUserCompanyForUserId(userId);
//            uada.updateCompanySettings(company, request.getServletContext());


			ElementData userDetails = uada.getUserWithId(userId);

//This is an example notification that your proposal has been viewed
//	FirebaseService.getInstance().sendNotificationToUser(userId, "Proposal " + "29292" + " has been viewed", "Proposal " + "20202" + " has been opened and if currently being viewed by your customer.", false);

			TUserRolesDataAccess urda = new TUserRolesDataAccess();
			UserPermissions up = urda.getPermissions(userId);


			String[] accessRights = uada.getUserAccessRights(userId);
			List<ElementData> userAccess = uada.getUsers(accessRights);
			List<HashMap<String, String>> userAccessNames = new LinkedList<HashMap<String, String>>();
			for (ElementData u : userAccess)
			{
				HashMap<String, String> details = new HashMap();
				details.put("value", u.getId());
				details.put("label",String.format("%s %s", u.getString(UserAccountDataAccess.E_NAME), u.getString(UserAccountDataAccess.E_SURNAME)));
				userAccessNames.add(details);
			}

// Javascript flags
			boolean[] trialExpiredVars = UserController.isTrialPeriodOver(session);
			int expireInDays = new UserAccountDataAccess().trialPeriodOverInWeek(company);
			int expireInDaysCounter = new UserAccountDataAccess().trialPerioddayCounter(company);
			boolean trialExpired = trialExpiredVars[0];
			Set<Entry<String, ElementData>> uCount = uada.getCompanyUsers(company.getId()).entrySet();
			int userCount = 1;
			if (uCount != null) {
				userCount = uCount.size();
			}
			Boolean trialAlmostExpired = trialExpiredVars[1];
			boolean noPhoneNumber = false;
			if (StringUtils.isBlank(userDetails.getString(UserAccountDataAccess.E_MOBILE_NUMBER))) {
				noPhoneNumber = true;
			}
			boolean showFirstLogin = false; //(!company.getBoolean(TUserAccountDataAccess.TUTORIAL_FIRST_ACCESS) && up.hasManageSettingsPermission());
//			CustomFields customFields = cfda.getCompanyCustomFields(CustomFields.PROPOSALS, company.getId());
//			CustomFields productCustomFields = cfda.getCompanyCustomFields(CustomFields.PRODUCTS, company.getId());


//Proposal count for tip guide
//            int countTotal = pdda.countProposalsTotal(accessRights);
			boolean hasProposalsBoolean = false;
//            if (countTotal > 0) {
//                hasProposalsBoolean = true;
//            }

			boolean hasUserAccess = up.hasManageUsersPermission();

//			DiskUsageDataAccess duda = new DiskUsageDataAccess();
//			ElementData diskUsageData = duda.getDiskUsageForCompanyId(company.getString("Category_id"));
			boolean trialAccount = StringUtils.equalsIgnoreCase(company.getString("attrdrop_pricingplan"), "TRIAL");


			if (trialExpired) {
				new EmailBuilder().prepareAccountReaccessedEmail(userDetails).addTo(userDetails.getString("attr_headline")).doSend();
			}

		%>

		<script>
			var dateFormatPattern = 'DD/MMM/YYYY';

			var expireInDaysCounter = <%=expireInDaysCounter %>;
			var hasUserAccess = <%=hasUserAccess %>;
			var trialExpired = <%=trialExpired %>;
			var trialAlmostExpired = <%=trialAlmostExpired %>;

			var needSetupPayment = false; <%--<%= StripeController.needSetupPayment(session) %>;--%>

			var expireInDays = <%=expireInDays %>;
			var noPhoneNumber = <%=noPhoneNumber %>;
			var showFirstLogin = <%=showFirstLogin %>;
			var userCount = <%=userCount %>;

			var userAccess = <%=new Gson().toJson(userAccessNames) %>;

			var tutorialFirstLoad = false; <%--"<%= uda.isUsersFirstAccess(userUsername)%>";--%>
			var userEmail = "<%= userUsername %>";
			var tab4Shown;

			<%--var accountSuspended = <%= StripeService.isCompanySuspended(company.getId()) %>;--%>
			var hasManageSubscriptionsPermission = <%= up.hasManageUsersPermission() %>;
			var companyAccountActive = true; 

		</script>

		<title>Foresite - Dashboard</title>
		<meta charset="UTF-8">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="manifest" href="/application/assets/manifest.json">

		<link rel="shortcut icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon">

		<link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700italic,700,900,900italic" rel="stylesheet">

		<!-- STYLESHEETS -->
		<style type="text/css">
			[fuse-cloak],
			.fuse-cloak {
				display: none !important;
			}

		</style>
		<!-- Icons.css -->
		<link type="text/css" rel="stylesheet" href="/application/assets/new/icons/fuse-icon-font/style.css">
		<link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/all.css">
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/fontawesome-pro-5.8.1-web/css/all.css" />
		<!-- Animate.css -->
		<link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/animate.css/animate.min.css">
		<!-- PNotify -->
		<link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/pnotify/pnotify.custom.min.css">
		<!-- Nvd3 - D3 Charts -->
		<link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/nvd3/build/nv.d3.min.css" />
		<!-- Fuse Html -->
		<link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/fuse-html/fuse-html.min.css" />
		<!-- Swiper -->
		<link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/swiper/css/swiper.min.css" />
		<!-- Datatable -->
		<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css" />
		<link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.material.min.css" />
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.2/css/fixedHeader.dataTables.min.css">
		<!-- Main CSS -->
		<link type="text/css" rel="stylesheet" href="/application/assets/new/css/main.css">
		
		<link type="text/css" rel="stylesheet" href="/application/assets/new/css/dashboard.css">
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
		
		<link type="text/css" rel="stylesheet" href="/application/assets/plugins/switchery/switchery.css" />
		<link type="text/css" rel="stylesheet" href="/application/assets/plugins/select2/select2.css" />
		<link rel="stylesheet" href="/application/assets/plugins/paper-collapse/paper-collapse.min.css" />

		<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/plugins/table.css",request) %>" />
		<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/froala_style.min.css",request) %>" />
		<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/froala_editor.pkgd.min.css",request) %>" />
		
		<!-- / STYLESHEETS -->

		<!-- JAVASCRIPT -->
		<!-- jQuery -->
		<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
		<script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.3.min.js"></script>
		<!-- Mobile Detect -->
		<script type="text/javascript" src="/application/assets/new/vendor/mobile-detect/mobile-detect.min.js"></script>
		<!-- Popper.js -->
		<script type="text/javascript" src="/application/assets/new/vendor/popper.js/index.js"></script>
		
		<!-- Nvd3 - D3 Charts -->
		<script type="text/javascript" src="/application/assets/new/vendor/d3/d3.min.js"></script>
		<script type="text/javascript" src="/application/assets/new/vendor/nvd3/build/nv.d3.min.js"></script>
		<!-- Data tables -->
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.material.min.js"></script>  <%--/application/assets/new/vendor/datatables.net/js/jquery.dataTables.min.js--%>
			<%--<script type="text/javascript" src="/application/assets/new/vendor/datatables-responsive/js/dataTables.responsive.js"></script>--%>
		<!-- PNotify -->
		<script type="text/javascript" src="/application/assets/new/vendor/pnotify/pnotify.custom.min.js"></script>
		<!-- Fuse Html -->
		<script type="text/javascript" src="/application/assets/new/vendor/fuse-html/fuse-html.min.js"></script>
		<!-- Main JS -->
		<script type="text/javascript" src="/application/assets/new/js/main.js"></script>
		<!-- / JAVASCRIPT -->

		<style>
			.swiper-container {
				width: 100%;
				height: 100%;
			}
			.swiper-slide {
				text-align: center;
				font-size: 18px;
				background: #fff;
				/* Center slide text vertically */
				display: -webkit-box;
				display: -ms-flexbox;
				display: -webkit-flex;
				display: flex;
				-webkit-box-pack: center;
				-ms-flex-pack: center;
				-webkit-justify-content: center;
				justify-content: center;
				-webkit-box-align: center;
				-ms-flex-align: center;
				-webkit-align-items: center;
				align-items: center;
			}
			.tableColumnHolder > .dropdown-item {
				line-height: 2.8rem !important;
				height: 2.8rem !important;
			}
			.tableColumnHolder > .dropdown-item > i {
				margin-right: 6px;
				visibility: hidden;
			}

			.tableColumnHolder > .dropdown-item.toggle-vis > i {
				visibility: visible;
			}
		</style>
	</head>

	<body class="layout layout-vertical layout-left-navigation layout-below-toolbar">
	<main>
		<div id="wrapper">

			<jsp:include page="/application/include/common-side-menu.jsp"></jsp:include>

			<div class="content-wrapper">

				<jsp:include page="/application/include/common-top-menu.jsp"></jsp:include>

				<div class="content custom-scrollbar">

					<div id="project-dashboard" class="page-layout simple right-sidebar">

						<div class="page-content-wrapper custom-scrollbar" style="overflow-y: scroll">
							<!-- HEADER -->

							<!-- / HEADER -->

							<!-- CONTENT -->
							<div class="page-content" >

								<div class="tab-content" >
									<div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab" >

										<!-- WIDGET GROUP -->

										<div class="widget-group row no-gutters mb-3" id="mobileStats">

										</div>
											<%--Widget Group for Advance Filter--%>
										<div class="widget-group row no-gutters pb-4" style="display: none" id="traveldocFilterWindow">

											<!-- WIDGET 1 -->
											<div class="col-12">

												<div class="card">

													<div class="card-body">
														<div class="filter-container">

														</div>
														<div class="row align-items-center">
															<div class="col-auto">
																<button class="btn btn-default pull-left" id="travelDocs_filter_add">Add Filter</button>
															</div>
															<div class="col-auto">
																<button class="btn btn-success pull-left fuse-ripple-ready" id="travelDocs_filter_save" style="display: none">Save Filter</button>
															</div>
														</div>
													</div>
													<div class="card-footer text-muted">
														<div class="row align-items-center" style="width: 100%;margin: 0;">
															<div class="col ">
															</div>
															<div class="col-auto">
																<button class="btn btn-default pull-left" id="travelDocs_filter_cancel">Cancel</button>
															</div>
<%--															<div class="col-auto">--%>
<%--																<button class="btn btn-default pull-left" id="travelDocs_filter_advance_go">Filter</button>--%>
<%--															</div>--%>
														</div>
													</div>
												</div>
											</div>
											<!-- WIDGET 1 -->
										</div>
										<!-- WIDGET GROUP -->
										<div class="widget-group row no-gutters">

											<!-- WIDGET 1 -->
											<div class="col-12">

												<div class="card" style="border-radius: 4px;">
													<div class="card-header px-4 py-2 row no-gutters align-items-center justify-content-between">
														<div class="row align-items-center" style="width: 100%;margin: 0;">
															<div class="col" style="display: flex;padding-left: 0;">
																<span style="padding-right: 6px;padding-top: 5px;" id="shownStatusText">Choose a project to see the task list:</span>
																<select name="projectSelector" id="projectSelector" style="width:200px">
																	<% for(ElementData project : new ProjectsDataAccess().list(company)){ %>
																	<option value="<%= project.getId() %>"><%= ESAPI.encoder().encodeForHTML(project.getName()) %></option>
																	<% } %>
																</select>
															</div>
															<div class="col-auto">
																<span>Show</span>
																<select class="form-control" id="travelDocsDisplaySelector" >
																	<option value="10">10</option>
																	<option value="50" selected>50</option>
																	<option value="100">100</option>
																	<option value="200">200</option>
																</select>
															</div>
															<div class="col-auto">
																<button class="btn btn-secondary pull-left" id="travelDocs_filter">Filter</button>
															</div>
<%--															<div class="col-auto" style="display: none" >--%>
<%--																<button class="btn btn-default pull-left" id="travelDocs_filter_advance">Advanced</button>--%>
<%--															</div>--%>
															<div class="col-auto">
																<div class="dropdown columns-list">
																	<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
																	data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Visible Columns
																	</button>
																	<div class="dropdown-menu tableColumnHolder" aria-labelledby="dropdownMenuButton">
																		<div>
																			<a class="dropdown-item toggle-vis hidden" data-column="1" href="#" data-id="attr_externalid" data-visible="true">Task ID</a>
																			<a class="dropdown-item toggle-vis" data-column="2" href="#" data-id="attr_headline" data-visible="true">Task</a>
																			<a class="dropdown-item toggle-vis" data-column="3" href="#" data-id="attr_status" data-visible="true">Status</a>
																			<a class="dropdown-item toggle-vis" data-column="4" href="#" data-id="attr_resourcenames" data-visible="true">Resources</a>
																		</div>
																		<div>
																			<a class="dropdown-item toggle-vis" data-column="5" href="#" data-id="attrdate_startdate" data-visible="true">Start Date</a>
																			<a class="dropdown-item toggle-vis" data-column="6" href="#" data-id="attrdate_enddate" data-visible="true">End Date</a>
																			<a class="dropdown-item toggle-vis" data-column="7" href="#" data-id="attr_duration" data-visible="true">Duration</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="card-body" style="padding-top: 0;padding-bottom: 0;">
														<div class="table-responsive" >
															<% if (!trialExpired) { %>
															<script>
                                                                <%--var customFields = JSON.parse("<%=ESAPI.encoder().encodeForJavaScript(new Gson().toJson(customFields))%>");--%>
                                                                <%--var productsCustomFields = JSON.parse("<%=ESAPI.encoder().encodeForJavaScript(new Gson().toJson(productCustomFields))%>");--%>
                                                                var itineraryAccess = true;
															</script>
																<%--<table class="matchedTravelDocs mdl-data-table" id="matchedTravelDocs"></table>--%>
															<div class="matchedTravelDocs" id="matchedTravelDocs"></div>
															<% } %>
														</div>
													</div>
													<div class="card-footer text-muted">
														<span id="itineraryTableFooterText" style="margin-left: 20px;">
															Loading Traveldocs
														</span>
														<button class="btn btn-success pull-left" onclick="displayTasksImportModal()"> 
															<i class="fa fa-upload"></i> Import Tasks 
														</button>
													</div>
												</div>
											</div>
											<!-- WIDGET 1 -->
										</div>
									</div>
								</div>

							</div>
							<!-- / CONTENT -->

						</div>

					</div>


				</div>
			</div>
		</div>
		<%--<div id="setup-credentials-modal" class="modal top-modal fade" tabindex="-1">--%>
			<%--<jsp:include page="/application/dialog/setupCredentials.jsp"></jsp:include>--%>
		<%--</div>--%>


		<%--<% if(true) { %>--%>
		<%--<script>--%>
			<%--$(document).ready(function(){--%>
				<%--$('#setup-credentials-modal').modal('show');--%>
			<%--});--%>
		<%--</script>--%>
		<%--<% } %>--%>
		<div id="travelDocDeleteModal" 
        class="modal fade" 
        tabindex="-1" 
        data-backdrop="static" 
        data-keyboard="false">
  
	    <div class="alert alert-block alert-danger fade in" style="margin: 0">
	      <h4 class="alert-heading"><i class="fa fa-exclamation-circle"></i> Confirmation</h4>
	      <p>
	        Are you sure you would like to delete this TravelDoc?
	      </p>
	      <p>
	        <a class="btn btn-success btn-default travelDocDeleteConfimed" href="#">
	          <i class="fa fa-check"></i>
	          &nbsp;Yes
	        </a>
	        <button type="button" class="btn btn-danger btn-default" data-dismiss="modal">
	          <i class="fa fa-times"></i>
	          &nbsp;No
	        </button>
	      </p>
	    </div>
	  </div>
	  
	   <div id=createNoteModal class="modal top-modal fade" tabindex="-1">
	    <form class="form-create-note" action="/api/secure/discussion/createNote" method="POST">
	      <div class="modal-header">
	        <div class="titleBar">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	        </div>
	        <h2 class="modal-title">Create Note</h2>
	      </div>
	      
	      <div class="modal-body">
	        <input type="hidden" name="traveldoc" id="traveldocStuff"/>
	        <input type="hidden" name="v-key"/>
	        <div class="no-margin">
	          <textarea id="noteText" class="form-control" name="note" style="height: 212px;" placeholder="Please enter your note"></textarea>
	        </div>
			  <div style="padding: 10px 0px 0;" id="ownerEmailChooser">
				  <label>Send a receipt of this note to the following people:</label>
				  <select style="width: 100%;" multiple id="noteEmails" name="noteEmails"></select>
	
			  </div>
	      </div>
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-danger btn-default" data-dismiss="modal">
	          <i class="fa fa-times"></i> Close
	        </button>
	        <button type="submit" class="btn btn-success btn-default">
	          <i class="qc qc-edit"></i> Submit
	        </button>
	      </div>
	    </form>
	  </div>
	  
	  <%--Slide over menu from the right hand side--%>
		<div class="sideBar-backDrop" ></div>
		<div class="cd-panel from-right">
			<div class="cd-panel-container">
				<div class="cd-panel-content">
					<!-- place side content here -->
				</div>
			</div>
		</div>
		<%--End Slide over menu from the right hand side--%>
	</main>
	<div class="modal fade" id="account-setup-modal" tabindex="-1" role="dialog" style="display: none;width: auto" aria-hidden="true">
		<div class="modal-dialog modal-mainContent modal-dialog-centered" role="document" style="margin: 0px;"></div>
	</div>
	<div class="modal fade" id="send-proposal-modal" tabindex="-1" role="dialog" style="display: none;width: 650px" aria-hidden="true"></div>
	<div class="modal fade import-modal" id="import-tasks-modal" tabindex="-1" role="dialog" style="display: none;width: 650px" aria-hidden="true"></div>



	<!-- Bootstrap core JavaScript
      ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<%=UrlUtils.autocachedUrl("/application/js/log4javascript.js", request) %>"></script>
		<%--<script src="<%=UrlUtils.autocachedUrl("/application/js/logging.js", request) %>"></script>--%>
	<script src="<%=UrlUtils.autocachedUrl("/application/js/utils.js", request) %>"></script>

	<script type="text/javascript" src="/application/assets/new/js/apps/dashboard/project.js"></script>
	<!-- Fuse Html -->
	<script src="/application/assets/plugins/jquery-placeholder/js/jquery.placeholder.js"></script>
	<script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="/application/assets/plugins/moment/moment.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/application/assets/plugins/moment-timezone/0.5.10/moment-timezone-with-data.min.js"></script>
	<script type="text/javascript" src="/application/assets/new/vendor/swiper/js/swiper.min.js"></script>
	<script src="/application/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
	<script src="/application/assets/plugins/DataTables/buttons/js/dataTables.buttons.min.js"></script>
	<script src="/application/assets/plugins/DataTables/dataTables.colReorder.min.js"></script>
	<script src="/application/assets/plugins/DataTables/dataTables.colResize.js"></script>
	<script src="https://cdn.datatables.net/fixedcolumns/3.2.4/js/dataTables.fixedColumns.min.js"></script>
	<script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
	<script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
	<script src="/application/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"></script>
	<script src="/application/assets/plugins/timeago/jquery.timeago.js"></script>
	<script src="/application/assets/plugins/dynatable.0.3.1/jquery.dynatable.js"></script>
	<script src="/application/assets/plugins/dropzone/downloads/dropzone.min.js"></script>
	<script src="/application/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="/application/assets/plugins/bootstrap-datepicker/js/date.format.js"></script>
	<script src="/application/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script src="/application/assets/plugins/switchery/switchery.js"></script>
	<script src="/application/assets/plugins/nprogress/nprogress.js"></script>
	<script src="/application/assets/js/ui-modals.js"></script>
	<script src="/application/assets/plugins/Chart.js/Chart.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js"></script>
	<script src="/application/assets/plugins/paper-collapse/paper-collapse.min.js"></script>
	<script type="text/javascript" src="/application/js/randomIDGenerator.js"></script>
	<script src="/application/assets/plugins/select2/select2.js"></script>
	<script src="/application/assets/plugins/lz-string/lz-string.js"></script>


	<!-- Froala Editor -->
	<!-- <script src="/application/assets/plugins/froala-editor/js/froala_editor.pkgd.min.js"></script> -->
	<script src="/application/assets/plugins/froala_editor_3.0.0/js/froala_editor.pkgd.min.js"></script>

		<%--MAIN DASH JS--%>

	<script type="text/javascript" src="/application/assets/plugins/DateTime/dateTime.js"></script>
	<script type="text/javascript" src="/application/js/dash.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			TableData.init();
			UIModals.init();
		});
		
		var checkExist = setInterval(function() {
			if ($('.DTFC_RightBodyLiner').length) {
				console.log("Exists!");
				$('[data-toggle="tooltip-options"]').tooltip();
				$('.DTFC_RightBodyLiner').
				on('mouseover', 'tr', function() {
					var wrapperWidth = $(".DTFC_RightWrapper").width();
					var linerWidth = $(".DTFC_RightBodyLiner").width();
					$(".DTFC_RightWrapper").width(wrapperWidth + 100);
					$(".DTFC_RightBodyLiner").width(linerWidth + 100);
					$('.editButton').show();
					$('.sendButton').show();
					$('.viewButton').show();
				}).
				on('mouseout', 'tr', function() {
					var wrapperWidth = $(".DTFC_RightWrapper").width();
					var linerWidth = $(".DTFC_RightBodyLiner").width();
					$(".DTFC_RightWrapper").width(wrapperWidth - 100);
					$(".DTFC_RightBodyLiner").width(linerWidth - 100);
					$('.editButton').hide();
					$('.sendButton').hide();
					$('.viewButton').hide();
				});
				clearInterval(checkExist);
			}
		}, 100);
	</script>

	</body>
</compress:html>
</html>