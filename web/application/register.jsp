<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.ClientApplicationProperties"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ page import="au.corporateinteractive.qcloud.subscription.SubscriptionPlan" %>
<%@ page import="au.corporateinteractive.service.RecaptchaService" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
	String redirect = request.getParameter(Constants.PORTAL_LOGIN_REDIRECT_PARAM);

	String loginStatus = (String) session.getAttribute(Constants.LOGIN_STATUS);
	session.removeAttribute(Constants.LOGIN_STATUS);
	String loginError = (String) session.getAttribute(Constants.LOGIN_ERROR);
	session.removeAttribute(Constants.LOGIN_ERROR);


	if (userId != null && userId.length() > 0)
	{
		UserAccountDataAccess ada = new UserAccountDataAccess();
		ElementData user = ada.getUserElementWithUserID(userId);
		if (redirect == null)
		{
			redirect = "/dashboard";
		}

		response.sendRedirect(redirect);
		return;
	}
	String loginErrorUsername = (String)session.getAttribute(Constants.LOGIN_ERROR_USERNAME);
	if (loginErrorUsername == null)
	{
		loginErrorUsername = "";
	}
	java.util.Calendar calendar = java.util.Calendar.getInstance();
	int currentYear = calendar.get(java.util.Calendar.YEAR);
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>


	<!--[if lt IE 9]>
	<script src="/application/assets/plugins/respond.js"></script>
	<![endif]-->
	<script src="/application/assets/plugins/jquery-placeholder/js/jquery.placeholder.js"></script>
	<script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="/application/assets/plugins/select2/select2.js"></script>
	<script src="/application/js/login.js"></script>
	<script src="/application/assets/plugins/sweetalert/sweetalert.min.js"></script>

	<style>
		.wrapper {
			overflow: hidden;
			margin: 0 auto;
		}
		.login-conatiner {
			background-image: url('/application/assets/new/images/backgrounds/travel-docs-pricing-bg.png'); /* ../application/assets/images/login/quote-cloud-pricing3.png*/
			background-size: cover;
			background-position: bottom;
			background-repeat: no-repeat;
			position: relative;
			color: inherit;
			border-color: rgba(255, 255, 255, 0);
			padding-top: 4rem!important;
			padding-bottom: 4rem!important;
			min-height: 800px;
			height: 100vh !important;
		}
		.login-card-container {
			margin: auto;

			/* margin: 0;
            position: absolute;
            top: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
            left: 50%;
            -ms-transform: translateX(-50%);
            transform: translateX(-50%); */

		}
		.card {
			max-width: 850px;
			border: none !important;
			box-shadow: 29px 23px 63px -34px #9fa1a299 !important;
			-webkit-box-shadow: 29px 23px 63px -34px #9fa1a299 !important;
			-moz-box-shadow: 29px 23px 63px -34px #9fa1a299 !important;
			box-shadow: 29px 23px 63px -34px #9fa1a299 !important;
		}
		.login-card-container > .card {
			margin-top: -150px;
		}
		.card-image {
			width: 100%;
			height: auto;
			border-color: rgba(255, 255, 255, 0);
			background-color: rgba(255, 255, 255, 0);
			margin-left: -20px;
			margin-top: -54px;
			margin-right: 0px;
			margin-bottom: -20px;
		}
		.md-form {
			margin-top: 1rem;
			margin-bottom: 1rem;
			text-align: center !important;
		}
		.md-form input[type=date], .md-form input[type=datetime-local], .md-form input[type=email], .md-form input[type=number], .md-form input[type=password], .md-form input[type=search-md], .md-form input[type=search], .md-form input[type=tel], .md-form input[type=text], .md-form input[type=time], .md-form input[type=url], .md-form textarea.md-textarea {
			-webkit-transition: border-color .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
			-o-transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
			transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
			transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
			outline: 0;
			-webkit-box-shadow: none;
			box-shadow: none;
			border: none;
			border-bottom: 1px solid #ced4da;
			-webkit-border-radius: 0;
			border-radius: 0;
			-webkit-box-sizing: content-box;
			box-sizing: content-box;
			background-color: transparent;
		}
		.md-form .form-control {
			margin: 0 0 .5rem;
			margin-left: -10px !important;
			-webkit-border-radius: 0;
			border-radius: 0;
			padding: .3rem 0 .55rem;
			background-image: none;
			background-color: transparent;
			height: auto;
		}
		input, input[type=text], input[type=password], input[type=email], input[type=number], input[type=search], input[type=tel], input[type=url], input[type=reset], textarea, select {
			transition: background 0.3s;
			-moz-transition: background 0.3s;
			-webkit-transition: background 0.3s;
			-o-transition: background 0.3s;
			margin: 0;
			display: inline-block;
			vertical-align: middle;
			border: 1px solid #aaa;
			border-radius: 3px;
			color: #000;
			text-decoration: inherit;
		}
		.form-control {
			display: block;
			width: 100%;
			height: calc(2.25rem + 2px);
			padding: 0.375rem 0.75rem;
			font-size: 1rem;
			line-height: 1.5;
			color: #495057;
			background-color: #fff;
			background-clip: padding-box;
			border: 1px solid #ced4da;
			border-radius: 0.25rem;
			transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
		}
		.md-form input[type]:focus:not([readonly]) {
			-webkit-box-shadow: 0 1px 0 0 #f44d00;
			box-shadow: 0 1px 0 0 #f44d00;
			border-bottom: 1px solid #f44d00;
		}
		.md-form input[type]:focus:not([readonly])+label {
			color: #f44d00;
		}
		.md-form label {
			font-size: 1rem;
		}
		label {
			display: inline-block;
			margin-bottom: 0.5rem;
		}
		.md-form label {
			position: absolute;
			top: 1.65rem;
			left: 0;
			-webkit-transition: .2s ease-out;
			-o-transition: .2s ease-out;
			transition: .2s ease-out;
			cursor: text;
			color: #757575;
		}

		.md-form label.active {
			font-size: .8rem;
			-webkit-transform: translateY(-140%);
			-ms-transform: translateY(-140%);
			transform: translateY(-140%);
		}
		#registerButton {
			background: #f44d00 !important;
			font-size: 20px !important;
			width: 180px;
			-webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
			box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
			padding: .84rem 2.14rem;
			font-size: .81rem;
			-webkit-transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
			-o-transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
			transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
			transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
			margin: .375rem;
			border: 0;
			-webkit-border-radius: .125rem;
			border-radius: .125rem;
			cursor: pointer;
			text-transform: uppercase;
			white-space: normal;
			word-wrap: break-word;
			color: #fff;

			font-weight: 400;
			text-align: center;
		}
		.login-form-container input {
			min-width: 260px;
		}
		.edge-flex-container {
			flex-basis: 850px;
		}
		.edge-flex-col {
			flex-basis: 270px;
		}
		@media all and (max-width: 750px) {
			.edge-flex-container {
				padding-right: 0 !important;
			}
			.login-card-container > .card {
				margin-top: 0px !important;
			}
			.wrapper {
				overflow: auto !important;
			}
			.helper-person {
				display: none !important;
			}
		}
	</style>
</head>
<body>
<div class="wrapper">
	<div class="login-conatiner flex-center row ">
		<div class="col-auto login-card-container edge-flex-container">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col text-center">
							<h1>TravelDocs Registration</h1>
						</div>
					</div>
					<div class="row">
						<div class="col text-center text-muted">
							<h4>Professional Interactive Itineraries</h4>
						</div>
					</div>
					<form action="/api/free/register" method="post" class="text-center form-register" style="color: #757575;" novalidate="novalidate">
						<div class="row">
							<div class="col">
								<div class="errorHandler alert alert-danger" style="display: none">
									<i class="fa fa-remove-sign"></i> We're missing some information. Please check below.
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col edge-flex-col">
								<div class="login-form-container">
									<div class="form-row">
										<div class="col">
											<!-- Last name -->
											<div class="md-form">
												<input type="text" data-input="3" name="username" id="username" class="form-control">
												<label for="username" class="">Username</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col edge-flex-col">
								<div class="login-form-container">
									<div class="form-row">
										<div class="col">
											<!-- First name -->
											<div class="md-form">
												<input type="text" data-input="1" name="name" id="first_name" class="form-control">
												<label for="first_name" class="">First Name</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col edge-flex-col">
								<div class="login-form-container">
									<div class="form-row">
										<div class="col">
											<!-- First name -->
											<div class="md-form">
												<input type="text"data-input="2" name="surname" id="last_name" class="form-control">
												<label for="last_name" class="">Last Name</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col edge-flex-col">
								<div class="login-form-container">
									<div class="form-row">
										<div class="col">
											<!-- Last name -->
											<div class="md-form">
												<input type="email" data-input="3" name="email" id="email" class="form-control">
												<label for="email" class="">Work Email Address</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col edge-flex-col">
								<div class="login-form-container">
									<div class="form-row">
										<div class="col">
											<!-- First name -->
											<div class="md-form">
												<input type="tel" data-input="4" name="mobileNumber" id="phone" class="form-control">
												<label for="phone" class="">Mobile Number</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col edge-flex-col">
								<div class="login-form-container">
									<div class="form-row">
										<div class="col">
											<!-- First name -->
											<div class="md-form">
												<input type="text" data-input="5" name="company" id="company" class="form-control">
												<label for="company" class="">Company Name</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col edge-flex-col">
								<div class="login-form-container">
									<div class="form-row">
										<div class="col" style="border-bottom: 1px solid rgb(206, 212, 218);padding: 0;">
											<!-- First name -->
											<select name="country" data-input="6" class="form-control validate countryDropDown" id="country" style="border: none;margin-top: 16px;">
												<option value="" disabled selected>Choose your country</option>
												<% for(String country : SubscriptionPlan.getSupportCountries()){ %>
												<option value="<%= country %>"><%= country %></option>
												<% } %>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col edge-flex-col">
								<div class="login-form-container">
									<div class="form-row">
										<div class="col">
											<!-- First name -->
											<div class="md-form">
												<input type="password" data-input="7" name="password" id="password" class="form-control">
												<label for="password" class="">Password</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col edge-flex-col">
								<div class="login-form-container">
									<div class="form-row">
										<div class="col">
											<!-- First name -->
											<div class="md-form" >
												<input type="password" data-input="8" name="repeatPassword" id="repeatPassword" class="form-control">
												<label for="repeatPassword" class="">Confirm Password</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row mb-3">
							<div class="col">
								<input type="hidden" name="pricingPlan" value="TRIAL">
								<input type="hidden" name="g-recaptcha-response">
								<span class="text-muted text-left" style="font-size: 12px;">Enter your details above to begin your free trial of TravelDocs. By doing so you are agreeing to our privacy policy available below.</span>
							</div>
						</div>
						<div class="row">
							<div class="col"></div>
							<div class="col-auto text-right">
								<button data-sitekey="<%= RecaptchaService.sharedInstance().getRecaptchaPublicKey() %>" class="submit g-recaptcha btn btn-primary btn-float btn-block btn-sm waves-effect submitButton" id="registerButton">Register</button>
							</div>
						</div>
					</form>
				</div>
				<div class="helper-person">
					<img src="../application/assets/new/images/backgrounds/dcperson1.png" style="
                                        position: absolute;
                                    top: 50%;
                                    right: 0;
                                    transform: translateX(65%);
                                ">
				</div>
			</div>
			<!-- start: COPYRIGHT -->
			<div class="copyright" style="display: none">
				QuoteCloud version <%=ESAPI.encoder().encodeForHTML(ClientApplicationProperties.sharedInstance().getApplicationVersion()) %> &copy; Copyright QuoteCloud 2015-<%= currentYear %>
			</div>
		</div>
	</div>
</div>




<script src="/webdirector/free/csrf-token/init"></script>
<script src="" async defer></script>

<script>
	function activateSubmit(value)
	{
		//$("button[type='submit']").removeAttr("disabled");
		$('[name="g-recaptcha-response"]').closest('form').submit();
	}
</script>
<script>
	$(document).ready(function() {

		checkInputOnLoad();

		$(document).on('focusin', '.md-form input', function(e) {
			$(this).siblings('label').addClass('active');
		});
		$(document).on('focusout', '.md-form input', function(e) {
			if ($(this).val().trim() === "") {
				$(this).siblings('label').removeClass('active');
			}
		});


		$(document).on('keydown', '.form-control', function(e) {
			var code = e.keyCode || e.which;
			if(code === 9) {
				e.preventDefault();
				var dataID = $(this).attr('data-input');
				if (dataID !== '') {
					var newID = (Number(dataID)+1);
					$('.form-control[data-input="'+newID+'"]').focus();
				}
			}
		});
	});

	function checkInputOnLoad() {
		$('.md-form input').each(function() {
			if ($(this).val().trim() !== "") {
				$(this).next().addClass('active');
			}
		});
	}
</script>
<script>

	jQuery(document).ready(function() {

		$(document).on("click", ".submitButton", function() {
			event.preventDefault();
			$(this).html('<div class="spinner-border spinner-border-sm" role="status">\n' +
					'  <span class="sr-only">Loading...</span>\n' +
					'</div>');
			activateSubmit();
		});

		Login.initRegister();
	});
</script>
<%--<jsp:include page="ga-tracking.jsp"></jsp:include>--%>
<%--<script src="https://apis.google.com/js/platform.js" async defer></script>--%>

<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<%--<script type="text/javascript">--%>
<%--	var google_tag_params = {--%>
<%--		dynx_pagetype: 'other'--%>
<%--	};--%>
<%--</script>--%>
<%--<script type="text/javascript">--%>
<%--	/* <![CDATA[ */--%>
<%--	var google_conversion_id = 872108986;--%>
<%--	var google_custom_params = window.google_tag_params;--%>
<%--	var google_remarketing_only = true;--%>
<%--	var google_conversion_language = "en";--%>
<%--	var google_conversion_format = "3";--%>
<%--	var google_conversion_color = "ffffff";--%>
<%--	var google_conversion_label = "mF9cCLX4lm8QuqftnwM";--%>
<%--	/* ]]> */--%>
<%--</script>--%>
<%--<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">--%>
<%--// </script>--%>
<%--// <noscript>--%>
<%--// 	<div style="display:inline;">--%>
<%--// 		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/872108986/?guid=ON&amp;script=0"/>--%>
<%--// 	</div>--%>
<%--// </noscript>--%>
</body>
</html>