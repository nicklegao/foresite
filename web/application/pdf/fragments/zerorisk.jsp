<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.control.TravelWatchController" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.service.TravelWatchService" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.GregorianCalendar" %>

<%

    if (!StringUtil.isNullOrEmpty(proposalId))  {
        TravelWatchService tws = new TravelWatchService();
        List<Map<String, Object>> travelSegments = tws.gettravelWatchSegments(proposalId);
        for (Object seg: travelSegments) {
            HashMap<String, Object> segment = (HashMap<String, Object>) seg;
            HashMap<String, Object> countryInfo = (HashMap<String, Object>) segment.get("country");
            HashMap<String, List<HashMap<String, Object>>> cityAlertArray = (HashMap<String, List<HashMap<String, Object>>>) segment.get("cityAlerts");
            List<HashMap<String, Object>> travelArray = (List<HashMap<String, Object>>) segment.get("travel");
            List<HashMap<String, Object>> terrorismArray = (List<HashMap<String, Object>>) segment.get("terrorism");
            List<HashMap<String, Object>> crimeArray = (List<HashMap<String, Object>>) segment.get("crime");
            List<HashMap<String, Object>> dateArray = (List<HashMap<String, Object>>) segment.get("date");
            List<HashMap<String, Object>> weatherArray = (List<HashMap<String, Object>>) segment.get("weather");
            List<HashMap<String, Object>> demonstrationsArray = (List<HashMap<String, Object>>) segment.get("demonstrations");
            List<HashMap<String, Object>> healthArray = (List<HashMap<String, Object>>) segment.get("health");
            List<HashMap<String, Object>> emergencyProtocolArray = (List<HashMap<String, Object>>) segment.get("emergencyProtocol");
            List<String> emergancyContactArray = (List<String>) segment.get("emergancyContact");
            if (countryInfo != null) {
            %>
<div class="zerorisk-country-container" style="display: inline;">
<div class="title-container">
    <div class="row">
        <div class="col-12" style="text-transform: uppercase;text-align: center;">
            <h1 style="margin: 0px;"><%=countryInfo.get("attr_categoryname")%></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="sub-title" style="text-align: center;">
                <span>ZeroRisk International Advice</span>
            </div>
        </div>
    </div>
</div>
<%
    if (!StringUtil.isNullOrEmpty((String) countryInfo.get("attr_assessmentdescription"))) {
%>
<div class="country-description-container" style="border: 1px solid;border-radius: 4px;margin: 10px 10px;">
    <div class="country-description" style="padding: 10px 20px;">
        <div class="row">
            <div class="col-1" style="text-align: center;">
                <div class="status-icon">
                    <i class="fas fa-3x fa-exclamation-triangle"></i>
                    <%
                      String levelCode = (String) countryInfo.get("attrdrop_assessmentalertlevel");
                      String level = "";
                      switch (levelCode.toLowerCase()) {
                          case "l":
                              level = "Low";
                              break;
                          case "l-m":
                              level = "Low to Medium";
                              break;
                          case "m":
                              level = "Medium";
                              break;
                          case "m-h":
                              level = "Medium to High";
                              break;
                          case "h":
                              level = "High";
                              break;
                          default:
                              level = "Low";
                              break;
                      }
                    %>
                    <span class="status-icon-text" style="display: block; line-height: 1; margin: 0; padding-top: 3px; text-transform: capitalize; letter-spacing: 0;"><%=level%></span>
                </div>
            </div>
            <div class="col-10">
                <span style="font-size: 12px;"><%=countryInfo.get("attr_assessmentdescription")%></span>
            </div>
        </div>
    </div>
</div>

<%
        }
    %>

    <div class="alerts-container" style="margin: 20px 10px;">
<%

    if (cityAlertArray != null) {
        for ( String key : cityAlertArray.keySet() ) {
            List<HashMap<String, Object>> alerts =  cityAlertArray.get(key);
            %>

        <div class="row">
            <div class="col-12" style="padding-right: 0px;">
                <div class="alert-section-title">
                    <h3>Recent <%=key%> Alerts</h3>
                </div>
            </div>
        </div>
    <%
        if (alerts != null && alerts.size() > 0) {
            for (HashMap<String, Object> alertInfo : alerts) {
                Calendar date = new GregorianCalendar();
                date.setTime(new Date((Long) alertInfo.get("attrdate_start_date")));
    %>
    <div class="alert-info-container" style="border: 1px solid;border-radius: 4px;margin: 10px 0px;">
        <div class="country-description" style="padding: 10px 20px;">
            <div class="row">
                <div class="col-10">
                    <div class="row">
                        <div class="col-12">
                            <div class="alert-text">
                                <span style="font-size: 12px;"><%=StringUtils.abbreviate((String) alertInfo.get("attrtext_content"), 400)%></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <span style="font-size: 12px;"><%=alertInfo.get("attr_address")%> -- <%=date.get(Calendar.DAY_OF_MONTH)%>/<%=date.get(Calendar.MONTH)%>/<%=date.get(Calendar.YEAR)%></span>
                        </div>
                        <div class="col-5" style="text-align: right">
                            <span style="font-size: 12px;">Read More in your ZeroRisk Secapp</span>
                        </div>
                    </div>
                </div>
                <div class="col-1" style="text-align: center;">
                    <div class="status-icon">
                        <i class="fas fa-3x fa-exclamation-triangle" style="color: <%=tws.getAlertColour((String) alertInfo.get("attr_rating"))%>;"></i>
                        <span class="status-icon-text" style="display: block; line-height: 1; margin: 0; padding-top: 3px; text-transform: capitalize; letter-spacing: 0;"><%=alertInfo.get("attr_rating")%></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%
                }
            } else {
            %>
<div class="alert-info-container" style="border: 1px solid;border-radius: 4px;margin: 20px 0px;">
        <div class="country-description" style="padding: 10px 20px;">
            <div class="row">
                <div class="col text-center">
                    <span>No ZeroRisk alerts reported in this area at present.</span>
                </div>
            </div>
        </div>
    </div>
        <%
            }
        }
    } else {
%>
            <div class="row">
                <div class="col-12" style="padding-right: 0px;">
                    <div class="alert-section-title">
                        <h3>Recent <%=countryInfo.get("attr_categoryname")%> Alerts</h3>
                    </div>
                </div>
            </div>
    <div class="alert-info-container" style="border: 1px solid;border-radius: 4px;margin: 20px 0px;">
        <div class="country-description" style="padding: 10px 20px;">
            <div class="row">
                <div class="col text-center">
                    <span>No ZeroRisk alerts reported in this area at present.</span>
                </div>
            </div>
        </div>
    </div>


    <%
        }
    %>
</div>
<%
    if (travelArray != null && travelArray.size() > 0) {
        for (Integer t = 0; t < travelArray.size(); t++) {
            HashMap<String, Object> element = terrorismArray.get(t);
%>
<div class="travel-container" style="margin: 10px 10px;">
    <div class="row">
        <div class="col-8" style="padding-right: 0px;">
            <div class="travel-section-title">
                <h3>Local Travel Information</h3>
            </div>
        </div>
        <div class="col-3" style="padding-right: 0px;text-align: right;margin: 16px 0;">
            <%
                String levelCode = (String) element.get("attrdrop_assessmentalertlevel");
                String level = "";
                switch (levelCode.toLowerCase()) {
                    case "l":
                        level = "Low";
                        break;
                    case "l-m":
                        level = "Low to Medium";
                        break;
                    case "m":
                        level = "Medium";
                        break;
                    case "m-h":
                        level = "Medium to High";
                        break;
                    case "h":
                        level = "High";
                        break;
                    default:
                        level = "Low";
                        break;
                }
            %>
            <%=level%>
        </div>
    </div>
    <div class="travel-info-container" style="margin: 20px 0px;">
        <div class="row">
            <div class="col">
                <div class="travel-info-text"><%=((String) element.get("attrlong_description")).replaceAll("<br>", "<br></br>")%></div>
            </div>
        </div>
    </div>
</div>
<%
        }
    }
%>
<%
    if (terrorismArray != null && terrorismArray.size() > 0) {
        for (Integer t = 0; t < terrorismArray.size(); t++) {
            HashMap<String, Object> element = terrorismArray.get(t);
%>
<div class="terrorism-container" style="margin: 10px 10px;">
    <div class="row">
        <div class="col-8" style="padding-right: 0px;">
            <div class="travel-section-title">
                <h3>Terrorism Threat</h3>
            </div>
        </div>
        <div class="col-3" style="padding-right: 0px;text-align: right;margin: 16px 0;">
            <%
                String levelCode = (String) element.get("attrdrop_assessmentalertlevel");
                String level = "";
                switch (levelCode.toLowerCase()) {
                    case "l":
                        level = "Low";
                        break;
                    case "l-m":
                        level = "Low to Medium";
                        break;
                    case "m":
                        level = "Medium";
                        break;
                    case "m-h":
                        level = "Medium to High";
                        break;
                    case "h":
                        level = "High";
                        break;
                    default:
                        level = "Low";
                        break;
                }
            %>
            <%=level%>
        </div>
    </div>
    <div class="terrorism-info-container" style="margin: 0px 0px;">
        <div class="row">
            <div class="col">
                <div class="terrorism-info-text"><%=((String) element.get("attrlong_description")).replaceAll("<br>", "<br></br>")%></div>
            </div>
        </div>
    </div>
</div>
<%
        }
    }
%>
<%
    if (crimeArray != null && crimeArray.size() > 0) {
        for (Integer t = 0; t < crimeArray.size(); t++) {
            HashMap<String, Object> element = crimeArray.get(t);
%>
<div class="crime-container" style="margin: 10px 10px;">
    <div class="row">
        <div class="col-8" style="padding-right: 0px;">
            <div class="travel-section-title">
                <h3>Local Crime Information</h3>
            </div>
        </div>
        <div class="col-3" style="padding-right: 0px;text-align: right;margin: 16px 0;">
            <%
                String levelCode = (String) element.get("attrdrop_assessmentalertlevel");
                String level = "";
                switch (levelCode.toLowerCase()) {
                    case "l":
                        level = "Low";
                        break;
                    case "l-m":
                        level = "Low to Medium";
                        break;
                    case "m":
                        level = "Medium";
                        break;
                    case "m-h":
                        level = "Medium to High";
                        break;
                    case "h":
                        level = "High";
                        break;
                    default:
                        level = "Low";
                        break;
                }
            %>
            <%=level%>
        </div>
    </div>
    <div class="crime-info-container" style="margin: 0px 0px;">
        <div class="row">
            <div class="col">
                <div class="crime-info-text"><%=((String) element.get("attrlong_description")).replaceAll("<br>", "<br></br>")%></div>
            </div>
        </div>
    </div>
</div>
<%
        }
    }
%>
<%
    if ((dateArray != null && dateArray.size() > 0) || (demonstrationsArray != null && demonstrationsArray.size() > 0)) {
%>
<div class="date-container" style="margin: 10px 10px;">
    <div class="row">
        <div class="col-12" style="padding-right: 0px;">
            <div class="travel-section-title">
                <h3>Anniversaries &amp; Demonstrations</h3>
            </div>
        </div>
    </div>
    <%
        for (Integer t = 0; t < dateArray.size(); t++) {
            HashMap<String, Object> element = dateArray.get(t);
    %>
    <div class="date-info-container" style="margin: 10px 0px;">
        <div class="row">
            <div class="col">
                <div class="date-info-text"><%=((String) element.get("attrlong_description")).replaceAll("<br>", "<br></br>")%></div>
            </div>
        </div>
    </div>
    <%
        }
    %>
    <%
        for (Integer t = 0; t < demonstrationsArray.size(); t++) {
            HashMap<String, Object> element = demonstrationsArray.get(t);
    %>
    <div class="demonstrations-info-container" style="margin: 10px 0px;">
        <div class="row">
            <div class="col">
                <div class="demonstrations-info-text"><%=((String) element.get("attrlong_description")).replaceAll("<br>", "<br></br>")%></div>
            </div>
        </div>
    </div>
    <%
        }
    %>
</div>
<%
    }
%>
<%
    if (weatherArray != null && weatherArray.size() > 0) {
        for (Integer t = 0; t < weatherArray.size(); t++) {
            HashMap<String, Object> element = weatherArray.get(t);
%>
<div class="weather-container" style="margin: 10px 10px;">
    <div class="row">
        <div class="col-8" style="padding-right: 0px;">
            <div class="weather-section-title">
                <h3>Weather &amp; Disaster Information</h3>
            </div>
        </div>
        <div class="col-3" style="padding-right: 0px;text-align: right;margin: 16px 0;">
            <%
                String levelCode = (String) element.get("attrdrop_assessmentalertlevel");
                String level = "";
                switch (levelCode.toLowerCase()) {
                    case "l":
                        level = "Low";
                        break;
                    case "l-m":
                        level = "Low to Medium";
                        break;
                    case "m":
                        level = "Medium";
                        break;
                    case "m-h":
                        level = "Medium to High";
                        break;
                    case "h":
                        level = "High";
                        break;
                    default:
                        level = "Low";
                        break;
                }
            %>
            <%=level%>
        </div>
    </div>
    <div class="weather-info-container" style="margin: 20px 0px;">
        <div class="row">
            <div class="col">
                <div class="weather-info-text"><%=((String) element.get("attrlong_description")).replaceAll("<br>", "<br></br>")%></div>
            </div>
        </div>
    </div>
</div>
<%
        }
    }
%>
<%
    if (healthArray != null && healthArray.size() > 0) {
        for (Integer t = 0; t < healthArray.size(); t++) {
            HashMap<String, Object> element = healthArray.get(t);
%>
<div class="health-container" style="margin: 10px 10px;">
    <div class="row">
        <div class="col-8" style="padding-right: 0px;">
            <div class="weather-section-title">
                <h3>Health &amp; Vaccine Information</h3>
            </div>
        </div>
        <div class="col-3" style="padding-right: 0px;text-align: right;margin: 16px 0;">
            <%
                String levelCode = (String) element.get("attrdrop_assessmentalertlevel");
                String level = "";
                switch (levelCode.toLowerCase()) {
                    case "l":
                        level = "Low";
                        break;
                    case "l-m":
                        level = "Low to Medium";
                        break;
                    case "m":
                        level = "Medium";
                        break;
                    case "m-h":
                        level = "Medium to High";
                        break;
                    case "h":
                        level = "High";
                        break;
                    default:
                        level = "Low";
                        break;
                }
            %>
            <%=level%>
        </div>
    </div>
    <div class="health-info-container" style="margin: 20px 0px;">
        <div class="row">
            <div class="col">
                <div class="health-info-text"><%=((String) element.get("attrlong_description")).replaceAll("<br>", "<br></br>")%></div>
            </div>
        </div>
    </div>
</div>
<%
        }
    }
%>
<%
    if (emergencyProtocolArray != null && emergencyProtocolArray.size() > 0) {
        for (Integer t = 0; t < emergencyProtocolArray.size(); t++) {
            HashMap<String, Object> element = emergencyProtocolArray.get(t);
%>
    <div style="page-break-after: always;"></div>
<div class="emergencyProtocol-container" style="margin: 20px 10px;">
    <div class="row">
        <div class="col-8" style="padding-right: 0px;">
            <div class="weather-section-title">
                <h3>Critical Emergency Protocol</h3>
            </div>
        </div>
        <div class="col-3" style="padding-right: 0px;text-align: right">
            <%
                String levelCode = (String) element.get("attrdrop_assessmentalertlevel");
                String level = "";
                switch (levelCode.toLowerCase()) {
                    case "l":
                        level = "Low";
                        break;
                    case "l-m":
                        level = "Low to Medium";
                        break;
                    case "m":
                        level = "Medium";
                        break;
                    case "m-h":
                        level = "Medium to High";
                        break;
                    case "h":
                        level = "High";
                        break;
                    default:
                        level = "Low";
                        break;
                }
            %>
            <%=level%>
        </div>
    </div>
    <div class="emergencyProtocol-info-container" style="margin: 0px 0px;">
        <div class="row">
            <div class="col">
                <div class="emergencyProtocol-info-text"><%=((String) element.get("attrlong_description")).replaceAll("<br>", "<br></br>")%></div>
            </div>
        </div>
    </div>
</div>
<%
        }
    }
%>
<%
    if (emergancyContactArray != null && emergancyContactArray.size() > 0) {
        for (Integer t = 0; t < emergancyContactArray.size(); t++) {
            String element = emergancyContactArray.get(t);
            if (StringUtils.isNotBlank(element)) {
%>
<div class="emergancyContact-container" style="margin: 20px 10px;">
    <div class="row">
        <div class="col-12" style="padding-right: 0px;">
            <div class="weather-section-title">
                <h3>Emergency Contact Information</h3>
            </div>
        </div>
    </div>
    <div class="emergancyContact-info-container" style="margin: 20px 0px;">
        <div class="row">
            <div class="col">
                <div class="emergancyContact-info-text"><%=(element.replaceAll("<br>", "<br></br>"))%></div>
            </div>
        </div>
    </div>
</div>
<%
            }
        }
    }
%>
</div>
<div style="page-break-after: always;"></div>
            <%
                        }
        }
    }
%>