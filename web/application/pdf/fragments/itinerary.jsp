<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.service.TemplateBuilder" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ page import="au.corporateinteractive.qcloud.market.model.travel.core.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.Comparator" %>
<%@ page import="au.corporateinteractive.qcloud.market.utils.Constant" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.SabreDataAccess" %>
<div class="content-holder sortable" data-type="<%=content.getType()%>" style="width: 100%; height: auto;">


<% if (proposalHashtable.containsKey(TravelDocsDataAccess.E_ITINERARY_DATA) && proposalHashtable.get(TravelDocsDataAccess.E_ITINERARY_DATA) != null) {
StoreUtils storeUtils = new StoreUtils();
String itineraryFile = storeUtils.readFieldFromStores(proposalHashtable.get(TravelDocsDataAccess.E_ITINERARY_DATA));
    Gson gson = new Gson();
if (itineraryFile.length() > 100) {
JSONObject itin = new JSONObject(itineraryFile);
    String itinerary = itin.getString("data");
    BookingCI booking = gson.fromJson(itinerary, BookingCI.class);

    if (booking == null) { %>
    <div class="item" style="vertical-align: top;display: inline-block;text-align: center;width: 100%;padding: 15px;border: 1px dotted;"><i class="fas fa-error fa-7x"></i><span class="caption" style="display: block; padding-top: 10px;">There was an error downloading your Itinerary</span></div>
   <% } else {
       List<ItineraryItemCI> items = booking.getItinerary().getItineraryItem();
       List<ItineraryItemCI> insuranceInfo = new ArrayList<>();
       List<ItineraryItemCI> visaInfo = new ArrayList<>();
       List<ItineraryItemCI> segmentInfoGroups = new ArrayList<>();
       List<SegmentCI> segmentInfo = new ArrayList<>();
       SabreDataAccess sda = new SabreDataAccess();
       FrequentFlyersCI frequentFlyerInfo = booking.getItinerary().getFrequentFlyers();
       for (ItineraryItemCI item : items) {
           if (item.getNonTravelService() != null && item.getNonTravelService().getInsurance() != null) {
               insuranceInfo.add(item);
           } else if (item.getNonTravelService() != null && item.getNonTravelService().getVisa() != null) {
               visaInfo.add(item);
           } else if (item.getSegments() != null && item.getSegments().getSegment().size() > 0) {
               segmentInfoGroups.add(item);
           }
       }

       for (ItineraryItemCI segmentInfoGrup : segmentInfoGroups) {
           if (segmentInfoGrup.getSegments().getSegment().size() > 0) {
                segmentInfo.addAll(segmentInfoGrup.getSegments().getSegment());
           }
       }

       segmentInfo = sda.reorderSegmentsBasedOnDate(segmentInfo);

   %>

    <%
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM/yyyy HH:mm a");
    %>
    <%@include file="/application/pdf/fragments/itinerary/header.jsp" %>

    <%@include file="/application/pdf/fragments/itinerary/travellers.jsp" %>

    <%
        for (SegmentCI segment : segmentInfo) {
            if (segment.getType() != null && segment.isShowOnItinerary()) {
    %>
    <div class="iti-segment">
    <%
        switch (segment.getType()) {
            case Constant.SEGMENT_FLIGHT:
    %>
    <%@include file="/application/pdf/fragments/itinerary/flight.jsp" %>
    <%
            break;
        case Constant.SEGMENT_HOTEL:
    %>
    <%@include file="/application/pdf/fragments/itinerary/hotel.jsp" %>
    <%
            break;
        case Constant.SEGMENT_TOUR:
    %>
    <%@include file="/application/pdf/fragments/itinerary/tour.jsp" %>
    <%
                        break;
        case Constant.SEGMENT_BUS:
    %>
    <%@include file="/application/pdf/fragments/itinerary/bus.jsp" %>
    <%
                        break;
        case Constant.SEGMENT_CRUISE:
    %>
    <%@include file="/application/pdf/fragments/itinerary/cruise.jsp" %>
    <%
                        break;
        case Constant.SEGMENT_OWN_ARRANGEMENT:
    %>
    <%@include file="/application/pdf/fragments/itinerary/own.jsp" %>
    <%
                        break;
        case Constant.SEGMENT_FERRY:
    %>
    <%@include file="/application/pdf/fragments/itinerary/ferry.jsp" %>
    <%
                        break;
        case Constant.SEGMENT_CAR_HIRE:
    %>
    <%@include file="/application/pdf/fragments/itinerary/car.jsp" %>
    <%
                        break;
        case Constant.SEGMENT_RAIL:
    %>
    <%@include file="/application/pdf/fragments/itinerary/rail.jsp" %>
    <%
                        break;
        case Constant.SEGMENT_GENERAL:
    %>
        <%@include file="/application/pdf/fragments/itinerary/general.jsp" %>
        <%
                    break;
                    default:
                        System.out.println("UNKNOWN SEGMENT");
                        break;
                }
    %>
    </div>
    <%
            }
        }
    %>
    <% } %>
<%--    TemplateBuilder tb = new TemplateBuilder();--%>
<%--    String itineraryString = tb.buildItinerary(company, booking);%>--%>
<%} else {%>
    <div class="item" style="vertical-align: top;display: inline-block;text-align: center;width: 100%;padding: 15px;border: 1px dotted;"><i class="fas fa-plane fa-7x"></i><span class="caption" style="display: block; padding-top: 10px;">This is where your itinerary will be loaded</span></div>
<%}
}%>
</div>

<%

%>