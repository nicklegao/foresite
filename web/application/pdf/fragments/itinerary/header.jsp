<div class="row">
    <div class="col-12" style="text-align: center;">
        <h1 style="margin: 0;">Your Travel Itinerary</h1>
    </div>
</div>
<div class="row" style="">
    <div class="col-12" style="text-align: center;margin-top: -8px;">
        <%
            if (StringUtil.isNullOrEmpty(booking.getAgency().getPhoneNumber())) {
        %>
        <span>For further information on the itinerary below please contact <%=booking.getAgency().getAgency().getName()%></span>
        <%
            } else {
        %>
        <span>For further information on the itinerary below please contact <%=booking.getAgency().getAgency().getName()%> on <%=booking.getAgency().getPhoneNumber()%></span>
        <%
            }
        %>
    </div>
</div>
