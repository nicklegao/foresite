<%@ page import="au.corporateinteractive.qcloud.market.model.travel.core.SegmentTourCI" %>
<%
    SegmentTourCI tourInfo = segment.getTour();
    SimpleDateFormat tourFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");
    SimpleDateFormat tourArrivalDepartFormat = new SimpleDateFormat("hh:mm aa");

    String tourArrivalHeaderString = "";
    String tourDepartsHeaderString = "";

    if (tourInfo.getSupplier() != null && StringUtils.isNotBlank(tourInfo.getSupplier().getDescription())) {
        tourArrivalHeaderString += tourInfo.getSupplier().getDescription();
    }
    if (tourInfo.getSupplier() != null && tourInfo.getSupplier().getSupplier().getCity() != null && StringUtils.isNotBlank(tourInfo.getSupplier().getSupplier().getCity().getCityName())) {
        if (StringUtils.isNotBlank(tourArrivalHeaderString.trim())) {
            tourArrivalHeaderString += " in ";
        }
        tourArrivalHeaderString += tourInfo.getSupplier().getSupplier().getCity().getCityName();
    }
    if (tourArrivalHeaderString.trim().length() < 1) {
        if (tourInfo.getSupplier() != null && tourInfo.getSupplier().getSupplier() != null && tourInfo.getSupplier().getSupplier().getSupplierCode() != null && StringUtils.isNotBlank(tourInfo.getSupplier().getSupplier().getSupplierCode().getName())) {
            tourArrivalHeaderString += tourInfo.getSupplier().getSupplier().getSupplierCode().getName();
        }
    }
    if (StringUtil.isNullOrEmpty(tourArrivalHeaderString.trim())) {
        tourArrivalHeaderString += "Pick-up Location Pending";
    }

    if (tourInfo.getSupplier() != null && StringUtils.isNotBlank(tourInfo.getSupplier().getDescription())) {
        tourDepartsHeaderString += tourInfo.getSupplier().getDescription() + " on " + tourFormat.format(tourInfo.getDepartDate());
    }
    if (StringUtil.isNullOrEmpty(tourDepartsHeaderString.trim())) {
        tourDepartsHeaderString += "Drop-off Location Pending";
    }

%>

<div class="row segment">
    <div class="col-12">
    	<div class="segment-heading">
	      <div class="icon">
	          <span class="fa fa-globe"></span>
	      </div>
	      <div class="segment-heading-text">
	          <h1>TOUR DETAILS</h1>
	      </div>
	  </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <hr></hr>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <h2 class="iti-segment-date"><%=tourFormat.format(tourInfo.getDepartDate())%></h2>
    </div>
</div>
<div class="row">
    <div class="col-1">
        <span><%=tourArrivalDepartFormat.format(tourInfo.getDepartDate())%></span>
    </div>
    <div class="col-2" style="text-align: center;">
        <span style="">Pick-Up:</span>
    </div>
    <div class="col-8">
        <span style=""><%=tourArrivalHeaderString%></span>
    </div>
</div>


<div class="row">
    <div class="col-12">
        <%
            if (StringUtils.isNotBlank(tourInfo.getBookingReference())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Booking Reference:</span>
            </div>
            <div class="col-7">
                <span style=""><%=tourInfo.getBookingReference()%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (tourInfo.getSupplier() != null && StringUtils.isNotBlank(tourInfo.getSupplier().getDescription())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Supplier Description:</span>
            </div>
            <div class="col-7">
                <span style=""><%=tourInfo.getSupplier().getDescription()%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (tourInfo.getSupplier() != null && StringUtils.isNotBlank(tourInfo.getSupplier().getPhoneNumber())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Contact:</span>
            </div>
            <div class="col-7">
                <span style=""><%=tourInfo.getSupplier().getPhoneNumber()%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (tourInfo.getSupplier() != null && tourInfo.getSupplier().getAddress() != null && StringUtils.isNotBlank(tourInfo.getSupplier().getAddress().getLine1())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Address:</span>
            </div>
            <div class="col-7">
                <span style=""><%=tourInfo.getSupplier().getAddress().getLine1()%> <%=tourInfo.getSupplier().getAddress().getSuburb()%> <%=tourInfo.getSupplier().getAddress().getState()%> <%=tourInfo.getSupplier().getAddress().getCountry()%> <%=tourInfo.getSupplier().getAddress().getPostcode()%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (tourInfo.getSupplier() != null && tourInfo.getSupplier().getHours() != null) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Hours:</span>
            </div>
            <div class="col-7">
                <span style=""><%=tourArrivalDepartFormat.format(tourInfo.getSupplier().getHours().getOpeningTime())%> - <%=tourArrivalDepartFormat.format(tourInfo.getSupplier().getHours().getClosingTime())%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (tourInfo.getSupplier() != null && StringUtils.isNotBlank(tourInfo.getSupplier().getRating())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Tour Rating:</span>
            </div>
            <div class="col-7">
                <span style=""><%=tourInfo.getSupplier().getRating()%></span>
            </div>
        </div>
        <%
            }
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Number of Days:</span>
            </div>
            <div class="col-7">
                <span style=""><%=(tourInfo.getNumberOfDays() != null && tourInfo.getNumberOfDays() > 0) ? tourInfo.getNumberOfDays() : "Pending"%></span>
            </div>
        </div>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Number of Seats:</span>
            </div>
            <div class="col-7">
                <span style=""><%=(tourInfo.getNumberOfSeats() != null && tourInfo.getNumberOfSeats() > 0) ? tourInfo.getNumberOfSeats() : "Pending"%></span>
            </div>
        </div>
        <%
            if (StringUtils.isNotBlank(tourInfo.getBookingReference())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Booking Reference:</span>
            </div>
            <div class="col-7">
                <span style=""><%=tourInfo.getBookingReference()%></span>
            </div>
        </div>
        <%
            }
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Booking Status:</span>
            </div>
            <div class="col-7">
                <span style=""><%=StringUtils.isNotBlank(tourInfo.getStatus()) ? tourInfo.getStatus() : "Pending Confirmation"%></span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-1">
        <span><%=tourArrivalDepartFormat.format(tourInfo.getDepartDate())%></span>
    </div>
    <div class="col-2" style="text-align: center;">
        <span style="">Drop-off:</span>
    </div>
    <div class="col-8">
        <span style=""><%=tourDepartsHeaderString%></span>
    </div>
</div>
<%
    if (tourInfo.getSupplier() != null && StringUtils.isNotBlank(tourInfo.getSupplier().getCancellationPolicy())) {
%>
<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Cancelation Policy:</span>
            </div>
            <div class="col-7">

                <span style=""><%=tourInfo.getSupplier().getCancellationPolicy()%></span>
            </div>
        </div>
    </div>
</div>
<%
    }
%>