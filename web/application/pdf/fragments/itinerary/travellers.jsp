<%@ page import="au.corporateinteractive.qcloud.market.model.travel.core.PassengerCI" %>
<%@ page import="au.corporateinteractive.qcloud.market.model.travel.core.EmailAddressesCI" %>
<%@ page import="au.corporateinteractive.qcloud.market.model.travel.core.PassportCI" %>
<%@ page import="au.corporateinteractive.qcloud.market.model.travel.core.BookingStatusCI" %>
<div class="row" style="height: 14px;"></div>

<div class="row">
    <div class="col-6">
        <div class="row">
            <div class="col-12">
                <h2 style="margin: 0 0 -8px 0;">Passenger Information</h2>
            </div>
        </div>
        <%
            List<PassengerCI> passengers = booking.getPassengers().getPassenger();
        %>
        <%
            for (PassengerCI passenger : passengers) {
                String gender =  passenger.getGender() != null ? passenger.getGender().value() : "";
                String passengerType = passenger.getPassengerType() != null ? passenger.getPassengerType().value() : "";
        %>
        <div class="row">
            <div class="col-12">
                <span><%=passenger.getLastname()%> / <%=passenger.getFirstname()%> <%=(StringUtils.isNotBlank(gender) || StringUtils.isNotBlank(passengerType)) ? ("(" + (StringUtils.capitalize(passengerType.toLowerCase()) + " " + StringUtils.capitalize(gender.toLowerCase())).trim() + ")").trim() : ""%></span>
            </div>
        </div>
        <%
            String phone = passenger.getPhoneNumber();
            List<String> emails = new ArrayList<>();
            if (passenger.getEmails() != null) {
                emails.addAll(passenger.getEmails().getEmail());
            }

            List<PassportCI> passports = new ArrayList<>();
            if (passenger.getPassports() != null) {
                passports.addAll(passenger.getPassports().getPassport());
            }
        %>
        <%
            for (PassportCI passport : passports) {
        %>
        <div class="row">
            <div class="col-12">
                <span style="padding-left: 10px;"><%=passport.getPassportNumber()%> <%=StringUtils.isNotBlank(passport.getCountry().getName()) ? " - " + passport.getCountry().getName() : ""%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (StringUtils.isNotBlank(phone)) {
        %>
        <div class="row">
            <div class="col-12">
                <span style="padding-left: 10px;"><%=phone%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            for (String email : emails) {
        %>
        <div class="row">
            <div class="col-12">
                <span style="padding-left: 10px;"><%=email%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            }
        %>
    </div>
    <div class="col-5">
        <div class="row">
            <div class="col-12">
                <h2 style="margin: 0 0 -8px 0;">Booking Information</h2>
            </div>
        </div>
        <%

            BookingStatusCI status = booking.getBookingStatus();
        %>
        <div class="row">
            <div class="col-12">
                <table>
                    <tbody>
                    <tr>
                        <td class="iti-label">Booking Reference:</td>
                        <td><%=booking.getBookingReference()%></td>
                    </tr>
                    <tr style="margin-top: -14px;">
                        <td class="iti-label">Booking Status:</td>
                        <td><%=StringUtils.isNotBlank(status.getBookingStatus().value()) ? StringUtils.capitalize(status.getBookingStatus().value().toLowerCase()) : "Pending"%></td>
                    </tr>
                    <tr style="margin-top: -14px;">
                        <td class="iti-label">Order Date:</td>
                        <td><%=formatter.format(status.getBookDate())%></td>
                    </tr>
                    </tbody>
                </table>
            </div>
<%--            <div class="col-4">--%>
<%--                <div class="row">--%>
<%--                    <div class="col-12">--%>
<%--                        <span class="iti-label">Booking Reference:</span>--%>
<%--                    </div>--%>
<%--                    <div class="col-12">--%>
<%--                        <span class="iti-label">Booking Status:</span>--%>
<%--                    </div>--%>
<%--                    <div class="col-12">--%>
<%--                        <span class="iti-label">Order Date:</span>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--            <div class="col-6">--%>
<%--                <div class="row">--%>
<%--                    <div class="col-12">--%>
<%--                        <span class="iti-label"><%=booking.getBookingReference()%></span>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--                <div class="row">--%>
<%--                    <div class="col-12">--%>
<%--                        <span class="iti-label"><%=StringUtils.isNotBlank(status.getBookingStatus().value()) ? StringUtils.capitalize(status.getBookingStatus().value().toLowerCase()) : "Pending"%></span>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--                <div class="row">--%>
<%--                    <div class="col-12">--%>
<%--                        <span class="iti-label"><%=formatter.format(status.getBookDate())%></span>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
        </div>

    </div>
</div>

<%
   if ((frequentFlyerInfo != null && frequentFlyerInfo.getFrequentFlyer().size() > 0) || insuranceInfo.size() > 0) {
%>
<div class="row" style="height: 20px;"></div>
<div class="row">
    <% if (frequentFlyerInfo != null && frequentFlyerInfo.getFrequentFlyer().size() > 0) { %>
    <div class="<%=insuranceInfo.size() > 0 ? "col-6" : "col-12"%>">
        <div class="row">
            <div class="col-12">
                <h2 style="margin: 0;">Frequent Flyer Information</h2>
            </div>
        </div>
        <%
            for (FrequentFlyerCI ffInfo : frequentFlyerInfo.getFrequentFlyer()) {
        %>
        <div class="row">
            <div class="col-12">
                <span style=""><%=ffInfo.getPassengerName()%></span>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <span style="padding-left: 10px;"><%=ffInfo.getMembershipNumber()%> <%=StringUtils.isNotBlank(ffInfo.getAdvisedAirline()) ? " - " + ffInfo.getAdvisedAirline() : ""%></span>
            </div>
        </div>
        <%
            }
        %>
    </div>
    <% } %>
    <%
        if (insuranceInfo.size() > 0) {
    %>
    <div class="<%=(frequentFlyerInfo != null && frequentFlyerInfo.getFrequentFlyer().size() > 0) ? "col-6" : "col-12"%>">
        <div class="row">
            <div class="col-12">
                <h2 style="margin: 0;">Insurance Information</h2>
            </div>
        </div>
        <%
            for (ItineraryItemCI info : insuranceInfo) {
        %>
        <div class="row">
            <div class="col-4">
                <div class="row">
                    <div class="col-12">
                        <span class="iti-label">Policy Number:</span>
                    </div>
                    <div class="col-12">
                        <span class="iti-label">Order Date:</span>
                    </div>
                    <div class="col-12">
                        <span class="iti-label">Duration:</span>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <span style=""><%=info.getNonTravelService().getInsurance().getPolicyNumber()%></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <span style=""><%=formatter.format(info.getNonTravelService().getInsurance().getStartDate())%></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <span style=""><%=info.getNonTravelService().getInsurance().getDuration()%></span>
                    </div>
                </div>
            </div>
        </div>
        <%
            }
        %>
    </div>
    <%
        }
    %>


</div>
<%
    }
%>