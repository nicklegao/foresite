<%@ page import="au.corporateinteractive.qcloud.market.model.travel.core.SegmentCarHireCI" %>
<%
    SegmentCarHireCI carInfo = segment.getCar();
    SimpleDateFormat carFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");
    SimpleDateFormat carArrivalDepartFormat = new SimpleDateFormat("hh:mm aa");

    String carArrivalHeaderString = "";
    String carDepartsHeaderString = "";

    if (carInfo.getPickup() != null && carInfo.getPickup().getVehicleLocationDateTime() != null && carInfo.getPickup().getVehicleLocationDateTime().getLocationCityDateTime() != null && carInfo.getPickup().getVehicleLocationDateTime().getLocationCityDateTime().getCity() != null) {
        carArrivalHeaderString += "<strong>" + carInfo.getPickup().getVehicleLocationDateTime().getLocationCityDateTime().getCity().getCityName() + "</strong>";
    }
    if (carInfo.getSupplier() != null && StringUtils.isNotBlank(carInfo.getSupplier().getDescription())) {
        if (StringUtils.isNotBlank(carArrivalHeaderString.trim())) {
            carArrivalHeaderString += " from ";
        }
        carArrivalHeaderString += "<strong>" + carInfo.getSupplier().getDescription() + "</strong>";
    }
    if (StringUtil.isNullOrEmpty(carArrivalHeaderString.trim())) {
        carArrivalHeaderString += "Pick-up Location Pending";
    }

    if (carInfo.getDropoff() != null && carInfo.getDropoff().getVehicleLocationDateTime() != null && carInfo.getDropoff().getVehicleLocationDateTime().getLocationCityDateTime() != null && carInfo.getDropoff().getVehicleLocationDateTime().getLocationCityDateTime().getCity() != null) {
        carDepartsHeaderString += "<strong>" + carInfo.getDropoff().getVehicleLocationDateTime().getLocationCityDateTime().getCity().getCityName() + "</strong>";
    }
    if (carInfo.getDepartDate() != null) {
        if (StringUtils.isNotBlank(carDepartsHeaderString.trim())) {
            carDepartsHeaderString += " on ";
        }
        carDepartsHeaderString += "<strong>" + carFormat.format(carInfo.getDepartDate()) + "</strong>";
    }
    if (StringUtil.isNullOrEmpty(carDepartsHeaderString.trim())) {
        carDepartsHeaderString += "Drop-off Location Pending";
    }

%>

<div class="row segment">
    <div class="col-12">
    	<div class="segment-heading">
	      <div class="icon">
	          <span class="fa fa-car"></span>
	      </div>
	      <div class="segment-heading-text">
	          <h1>CAR DETAILS</h1>
	      </div>
	  </div>
    </div>
</div>
<div class="row">
    <div class="col-12" style="margin-left: 10px;">
        <div class="row">
            <div class="col-12">
                <h2 class="iti-segment-date"><%=carFormat.format(carInfo.getDepartDate())%></h2>
            </div>
        </div>
        <div class="row segment-outer-row">
            <div class="col-1" style="margin-right: 10px">
                <span><%=carArrivalDepartFormat.format(carInfo.getDepartDate())%></span>
            </div>
            <div class="col-3" style="text-align: left;color: black !important;">
                <span style="">Pick-Up:</span>
            </div>
            <div class="col-7">
                <span style=""><%=carArrivalHeaderString%></span>
            </div>
        </div>


        <div class="row segment-inner-rows">
            <div class="col-12">
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Booking Status:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=StringUtils.isNotBlank(carInfo.getStatus()) ? carInfo.getStatus() : "Pending Confirmation"%></span>
                    </div>
                </div>
                <%
                    if (carInfo.getNumberOfCars() > 0) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Number of Cars:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=carInfo.getNumberOfCars()%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Duration:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=StringUtil.isNullOrEmpty(carInfo.getDurationOfHire()) ? "Pending" : carInfo.getDurationOfHire()%></span>
                    </div>
                </div>
                <%
                    if (carInfo.getSupplier() != null && StringUtils.isNotBlank(carInfo.getSupplier().getDescription())) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Supplier Description:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=carInfo.getSupplier().getDescription()%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <%
                    if (carInfo.getSupplier() != null && StringUtils.isNotBlank(carInfo.getSupplier().getPhoneNumber())) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Contact:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=carInfo.getSupplier().getPhoneNumber()%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <%
                    if (carInfo.getSupplier() != null && carInfo.getSupplier().getAddress() != null && StringUtils.isNotBlank(carInfo.getSupplier().getAddress().getLine1())) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Address:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=carInfo.getSupplier().getAddress().getLine1()%> <%=carInfo.getSupplier().getAddress().getSuburb()%> <%=carInfo.getSupplier().getAddress().getState()%> <%=carInfo.getSupplier().getAddress().getCountry()%> <%=carInfo.getSupplier().getAddress().getPostcode()%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <%
                    if (carInfo.getSupplier() != null && carInfo.getSupplier().getHours() != null) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Hours:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=carArrivalDepartFormat.format(carInfo.getSupplier().getHours().getOpeningTime())%> - <%=carArrivalDepartFormat.format(carInfo.getSupplier().getHours().getClosingTime())%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <%
                    if (carInfo.getSupplier() != null && StringUtils.isNotBlank(carInfo.getSupplier().getRating())) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Car Rating:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=carInfo.getSupplier().getRating()%></span>
                    </div>
                </div>
                <%
                    }
                %>
            </div>
        </div>

        <div class="row">
            <div class="col-1" style="margin-right: 10px">
                <span><%=carArrivalDepartFormat.format(carInfo.getDepartDate())%></span>
            </div>
            <div class="col-3" style="text-align: left;color: black !important;">
                <span style="">Drop-off:</span>
            </div>
            <div class="col-7">
                <span style=""><%=carDepartsHeaderString%></span>
            </div>
        </div>
        <%
            if (carInfo.getSupplier() != null && StringUtils.isNotBlank(carInfo.getSupplier().getCancellationPolicy())) {
        %>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Cancelation Policy:</span>
                    </div>
                    <div class="col-7">

                        <span style=""><%=carInfo.getSupplier().getCancellationPolicy()%></span>
                    </div>
                </div>
            </div>
        </div>
        <%
            }
        %>
    </div>
</div>