<%@ page import="au.corporateinteractive.qcloud.market.model.travel.core.SegmentBusCoachCI" %>
<%
    SegmentBusCoachCI busInfo = segment.getBus();
    SimpleDateFormat busFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");
    SimpleDateFormat busArrivalDepartFormat = new SimpleDateFormat("hh:mm aa");

    String busArrivalHeaderString = "";
    String busDepartsHeaderString = "";

    if (busInfo.getSupplier() != null && StringUtils.isNotBlank(busInfo.getSupplier().getDescription())) {
        busArrivalHeaderString += busInfo.getSupplier().getDescription();
    }
    if (busInfo.getSupplier() != null && busInfo.getSupplier().getSupplier().getCity() != null && StringUtils.isNotBlank(busInfo.getSupplier().getSupplier().getCity().getCityName())) {
        if (StringUtils.isNotBlank(busArrivalHeaderString.trim())) {
            busArrivalHeaderString += " in ";
        }
        busArrivalHeaderString += busInfo.getSupplier().getSupplier().getCity().getCityName();
    }
    if (busArrivalHeaderString.trim().length() < 1) {
        if (busInfo.getSupplier() != null && busInfo.getSupplier().getSupplier() != null && busInfo.getSupplier().getSupplier().getSupplierCode() != null && StringUtils.isNotBlank(busInfo.getSupplier().getSupplier().getSupplierCode().getName())) {
            busArrivalHeaderString += busInfo.getSupplier().getSupplier().getSupplierCode().getName();
        }
    }
    if (StringUtil.isNullOrEmpty(busArrivalHeaderString.trim())) {
        busArrivalHeaderString += "Pick-up Location Pending";
    }

    if (busInfo.getSupplier() != null && StringUtils.isNotBlank(busInfo.getSupplier().getDescription())) {
        busDepartsHeaderString += busInfo.getSupplier().getDescription() + " on " + busFormat.format(busInfo.getDepartDate());
    }
    if (StringUtil.isNullOrEmpty(busDepartsHeaderString.trim())) {
        busDepartsHeaderString += "Drop-off Location Pending";
    }

%>

<div class="row segment">
    <div class="col-12">
    	<div class="segment-heading">
	      <div class="icon">
	          <span class="fa fa-globe"></span>
	      </div>
	      <div class="segment-heading-text">
	          <h1>BUS DETAILS</h1>
	      </div>
	  </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <h2 class="iti-segment-date"><%=busFormat.format(busInfo.getDepartDate())%></h2>
    </div>
</div>
<div class="row">
    <div class="col-1">
        <span><%=busArrivalDepartFormat.format(busInfo.getDepartDate())%></span>
    </div>
    <div class="col-2" style="text-align: center;">
        <span style="">Pick-Up:</span>
    </div>
    <div class="col-8">
        <span style=""><%=busArrivalHeaderString%></span>
    </div>
</div>


<div class="row">
    <div class="col-12">
        <%
            if (StringUtils.isNotBlank(busInfo.getBookingReference())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Booking Reference:</span>
            </div>
            <div class="col-7">
                <span style=""><%=busInfo.getBookingReference()%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (busInfo.getSupplier() != null && StringUtils.isNotBlank(busInfo.getSupplier().getDescription())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Supplier Description:</span>
            </div>
            <div class="col-7">
                <span style=""><%=busInfo.getSupplier().getDescription()%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (busInfo.getSupplier() != null && StringUtils.isNotBlank(busInfo.getSupplier().getPhoneNumber())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Contact:</span>
            </div>
            <div class="col-7">
                <span style=""><%=busInfo.getSupplier().getPhoneNumber()%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (busInfo.getSupplier() != null && busInfo.getSupplier().getAddress() != null && StringUtils.isNotBlank(busInfo.getSupplier().getAddress().getLine1())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Address:</span>
            </div>
            <div class="col-7">
                <span style=""><%=busInfo.getSupplier().getAddress().getLine1()%> <%=busInfo.getSupplier().getAddress().getSuburb()%> <%=busInfo.getSupplier().getAddress().getState()%> <%=busInfo.getSupplier().getAddress().getCountry()%> <%=busInfo.getSupplier().getAddress().getPostcode()%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (busInfo.getSupplier() != null && busInfo.getSupplier().getHours() != null) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Hours:</span>
            </div>
            <div class="col-7">
                <span style=""><%=busArrivalDepartFormat.format(busInfo.getSupplier().getHours().getOpeningTime())%> - <%=busArrivalDepartFormat.format(busInfo.getSupplier().getHours().getClosingTime())%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (busInfo.getSupplier() != null && StringUtils.isNotBlank(busInfo.getSupplier().getRating())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Bus Rating:</span>
            </div>
            <div class="col-7">
                <span style=""><%=busInfo.getSupplier().getRating()%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (StringUtils.isNotBlank(busInfo.getBookingReference())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Booking Reference:</span>
            </div>
            <div class="col-7">
                <span style=""><%=busInfo.getBookingReference()%></span>
            </div>
        </div>
        <%
            }
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Booking Status:</span>
            </div>
            <div class="col-7">
                <span style=""><%=StringUtils.isNotBlank(busInfo.getStatus()) ? busInfo.getStatus() : "Pending Confirmation"%></span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-1">
        <span><%=busArrivalDepartFormat.format(busInfo.getDepartDate())%></span>
    </div>
    <div class="col-2" style="text-align: center;">
        <span style="">Drop-off:</span>
    </div>
    <div class="col-8">
        <span style=""><%=busDepartsHeaderString%></span>
    </div>
</div>
<%
    if (busInfo.getSupplier() != null && StringUtils.isNotBlank(busInfo.getSupplier().getCancellationPolicy())) {
%>
<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Cancelation Policy:</span>
            </div>
            <div class="col-7">

                <span style=""><%=busInfo.getSupplier().getCancellationPolicy()%></span>
            </div>
        </div>
    </div>
</div>
<%
    }
%>