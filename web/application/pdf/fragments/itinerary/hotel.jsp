<%
    SegmentHotelStayCI hotelInfo = segment.getHotel();
    SimpleDateFormat hotelFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");
    SimpleDateFormat hotelArrivalDepartFormat = new SimpleDateFormat("hh:mm aa");

    String hotelArrivalHeaderString = "";
    String hotelDepartsHeaderString = "";

    if (hotelInfo.getSupplier() != null && StringUtils.isNotBlank(hotelInfo.getSupplier().getDescription())) {
        hotelArrivalHeaderString += "<strong>" + hotelInfo.getSupplier().getDescription() + "</strong>";
    }
    if (hotelInfo.getSupplier() != null && hotelInfo.getSupplier().getSupplier().getCity() != null && StringUtils.isNotBlank(hotelInfo.getSupplier().getSupplier().getCity().getCityName())) {
        if (StringUtils.isNotBlank(hotelArrivalHeaderString.trim())) {
            hotelArrivalHeaderString += " in ";
        }
        hotelArrivalHeaderString += "<strong>" + hotelInfo.getSupplier().getSupplier().getCity().getCityName() + "</strong>";
    }

    if (hotelInfo.getSupplier() != null && StringUtils.isNotBlank(hotelInfo.getSupplier().getDescription())) {
        hotelDepartsHeaderString += "<strong>" + hotelInfo.getSupplier().getDescription() + " on " + hotelFormat.format(hotelInfo.getDepartDate()) + "</strong>";
    }

    %>

<div class="row segment">
    <div class="col-12">
        <div class="segment-heading">
	      <div class="icon">
	          <span class="fa fa-bed"></span>
	      </div>
	      <div class="segment-heading-text">
	          <h1>ACCOMODATION DETAILS</h1>
	      </div>
	  </div>
    </div>
</div>
<div class="row">
    <div class="col-12" style="margin-left: 10px;">
        <div class="row">
            <div class="col-12">
                <h2 class="iti-segment-date"><%=hotelFormat.format(hotelInfo.getArriveDate())%></h2>
            </div>
        </div>
        <div class="row segment-outer-row">
            <div class="col-1" style="margin-right: 10px">
                <span><%=hotelArrivalDepartFormat.format(hotelInfo.getArriveDate())%></span>
            </div>
            <div class="col-3" style="text-align: left;color: black !important;">
                <span style="">Check-In:</span>
            </div>
            <div class="col-7">
                <span style=""><%=hotelArrivalHeaderString%></span>
            </div>
        </div>
        <div class="row segment-inner-rows">
            <div class="col-12">
                <%
                    if (hotelInfo.getSupplier() != null && StringUtils.isNotBlank(hotelInfo.getSupplier().getPhoneNumber())) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Contact:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=hotelInfo.getSupplier().getPhoneNumber()%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <%
                    if (hotelInfo.getSupplier() != null && hotelInfo.getSupplier().getAddress() != null && StringUtils.isNotBlank(hotelInfo.getSupplier().getAddress().getLine1())) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Address:</span>
                    </div>
                    <div class="col-7">
                        <% if (StringUtil.isNullOrEmpty(hotelInfo.getSupplier().getAddress().getLine2())) { %>
                        <span style=""><%=hotelInfo.getSupplier().getAddress().getLine1()%> <%=hotelInfo.getSupplier().getAddress().getSuburb()%> <%=hotelInfo.getSupplier().getAddress().getState()%> <%=hotelInfo.getSupplier().getAddress().getCountry()%> <%=hotelInfo.getSupplier().getAddress().getPostcode()%></span>
                        <% } else {%>
                        <span style=""><%=hotelInfo.getSupplier().getAddress().getLine1()%> <%=hotelInfo.getSupplier().getAddress().getLine2()%></span>
                        <%  } %>
                    </div>
                </div>
                <%
                    }
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Room Type:</span>
                    </div>
                    <div class="col-7">
                        <span style="">Pending</span>
                    </div>
                </div>
                <%
                    if (hotelInfo.getSupplier() != null && hotelInfo.getSupplier().getHours() != null) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Hours:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=hotelArrivalDepartFormat.format(hotelInfo.getSupplier().getHours().getOpeningTime())%> - <%=hotelArrivalDepartFormat.format(hotelInfo.getSupplier().getHours().getClosingTime())%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <%
                    if (hotelInfo.getSupplier() != null && StringUtils.isNotBlank(hotelInfo.getSupplier().getRating())) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Hotel Rating:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=hotelInfo.getSupplier().getRating()%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Number of Rooms:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=(hotelInfo.getNumberOfRooms() != null && hotelInfo.getNumberOfRooms() > 0) ? hotelInfo.getNumberOfRooms() : "Pending"%></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Number of Nights:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=(hotelInfo.getNumberOfNights() != null && hotelInfo.getNumberOfNights() > 0) ? hotelInfo.getNumberOfNights() : "Pending"%></span>
                    </div>
                </div>
                <%
                    if (StringUtils.isNotBlank(hotelInfo.getBookingReference())) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Booking Reference:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=hotelInfo.getBookingReference()%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Booking Status:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=StringUtils.isNotBlank(hotelInfo.getStatus()) ? hotelInfo.getStatus() : "Pending Confirmation"%></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row segment-outer-row">
            <div class="col-1" style="margin-right: 10px">
                <span><%=hotelArrivalDepartFormat.format(hotelInfo.getDepartDate())%></span>
            </div>
            <div class="col-3" style="text-align: left;color: black !important;">
                <span style="">Check-out:</span>
            </div>
            <div class="col-7">
                <span style=""><%=hotelDepartsHeaderString%></span>
            </div>
        </div>
        <%
            if (StringUtils.isNotBlank(hotelInfo.getCancellationPolicy())) {
        %>
        <div class="row segment-inner-rows">
            <div class="col-12">
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Cancelation Policy:</span>
                    </div>
                    <div class="col-7">

                        <span style=""><%=hotelInfo.getCancellationPolicy()%></span>
                    </div>
                </div>
            </div>
        </div>
        <%
            }
        %>
    </div>
</div>