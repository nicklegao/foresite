<%@page import="org.apache.commons.lang.StringUtils"%>
<%
    SegmentFlightCI flightInfo = segment.getFlight();
    SimpleDateFormat flightFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");
    SimpleDateFormat arrivalDepartFormat = new SimpleDateFormat("hh:mm aa");
    String departLocationString = "";
    String departHeaderString = "";
    if (StringUtils.isNotBlank(flightInfo.getDeparts().getAirport().getAirportName())) {
        departHeaderString += "<strong>" + flightInfo.getDeparts().getAirport().getAirportName() + "</strong>";
    }
    if (flightInfo.getMarketingCarrier() != null && StringUtils.isNotBlank(flightInfo.getMarketingCarrier().getName())) {
        if (StringUtils.isNotBlank(departHeaderString.trim())) {
            departHeaderString += " on ";
        }
        departHeaderString += "<strong>" + flightInfo.getMarketingCarrier().getName() + "</strong>";
    }
    if (StringUtils.isNotBlank(flightInfo.getFlightNumber())) {
        if (StringUtils.isNotBlank(departHeaderString.trim())) {
            departHeaderString += " flight ";
        }
        departHeaderString += "<strong>" + flightInfo.getMarketingCarrier().getCode() + flightInfo.getFlightNumber() + "</strong>";
    }
    String arrivalHeaderString = "";
    if (StringUtils.isNotBlank(flightInfo.getArrives().getAirport().getAirportName())) {
        arrivalHeaderString += "<strong>" + flightInfo.getArrives().getAirport().getAirportName() + "</strong>";
    }
    if (StringUtils.isNotBlank(arrivalHeaderString.trim())) {
        arrivalHeaderString += " on ";
    }
    arrivalHeaderString += "<strong>" + flightFormat.format(flightInfo.getArrives().getDate()) + "</strong>";
    String seating = "";
    if (flightInfo.getSeating() != null && flightInfo.getSeating().getSeat().size() > 0) {
        for (Integer s=0;s<flightInfo.getSeating().getSeat().size();s++) {
            FlightSeatCI seat = flightInfo.getSeating().getSeat().get(s);

            if ((s+1) == flightInfo.getSeating().getSeat().size()){
                seating += seat.getSeatNumber();
            } else {
                seating += seat.getSeatNumber() + ", ";
            }
        }
    } 
%>
<div class="row segment">
    <div class="col-12">
        <div class="segment-heading">
	      <div class="icon">
	          <span class="fa fa-plane"></span>
	      </div>
	      <div class="segment-heading-text">
	          <h1>FLIGHT DETAILS</h1>
	      </div>
	      <div class="segment-heading-subtext">
	          <strong>
	              <em>*PLEASE CHECK DIRECTLY WITH THE AIRLINE FOR FLIGHT CHANGES 24 HOURS BEFORE DEPARTURE</em>
	          </strong>
	      </div>
	  </div>
    </div>
</div>
<div class="row">
    <div class="col-12" style="margin-left: 10px;">
        <div class="row">
            <div class="col-12">
                <h2 class="iti-segment-date"><%=flightFormat.format(flightInfo.getDeparts().getDate())%></h2>
            </div>
        </div>
        <div class="row segment-outer-row">
            <div class="col-1" style="margin-right: 10px">
                <span class=""><%=arrivalDepartFormat.format(flightInfo.getDepartDate())%></span>
            </div>
            <div class="col-3" style="text-align: left;color: black !important;">
                <span style="">Departs:</span>
            </div>
            <div class="col-7">
                <span style="font-size:13px;font-weight:normal;"><%=departHeaderString%></span>
            </div>
        </div>

        <div class="row segment-inner-rows">
            <div class="col-12">
                <%
                    if (StringUtils.isNotBlank(flightInfo.getDeparts().getTerminal())) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Departure Terminal:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=flightInfo.getDeparts().getTerminal()%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <%
                    if (flightInfo.getClassOfService() != null && StringUtils.isNotBlank(flightInfo.getClassOfService().getName())) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Class of Travel:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=flightInfo.getClassOfService().getName()%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <%
                    if (flightInfo.getEquipment() != null && StringUtils.isNotBlank(flightInfo.getEquipment().getName())) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Aircraft:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=flightInfo.getEquipment().getName()%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <%
                    if (flightInfo.getMarketingCarrier() != null && StringUtils.isNotBlank(flightInfo.getMarketingCarrier().getName())) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Marketing Carrier:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=flightInfo.getMarketingCarrier().getName()%> <%=StringUtils.isNotBlank(flightInfo.getMarketingCarrier().getCode()) ? "(" + flightInfo.getMarketingCarrier().getCode() + ")":""%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <%
                    if (flightInfo.getOperatingCarrier() != null && StringUtils.isNotBlank(flightInfo.getOperatingCarrier().getName())) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Operating Carrier:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=flightInfo.getOperatingCarrier().getName()%> <%=StringUtils.isNotBlank(flightInfo.getOperatingCarrier().getCode()) ? "(" + flightInfo.getOperatingCarrier().getCode() + ")":""%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <%
                    if (flightInfo.getFlightDuration() != null) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Duration:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=flightInfo.getFlightDuration().getHours() > 0 ? flightInfo.getFlightDuration().getHours() + " Hours" : ""%> <%=flightInfo.getFlightDuration().getMinutes() > 0 ? " " + flightInfo.getFlightDuration().getMinutes() + " Minutes" : ""%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <%
                    if (flightInfo.getMiles() > 0) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Distance:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=flightInfo.getMiles()%> Miles</span>
                    </div>
                </div>
                <%
                    }
                %>
                <%
                    if (StringUtils.isNotBlank(flightInfo.getAirlineReference())) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Airline Reference:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=flightInfo.getAirlineReference()%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <%
                    if (StringUtils.isNotBlank(seating.trim())) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Seating:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=seating.trim()%></span>
                    </div>
                </div>
                <%
                    }
                %>
                <%
                    if (flightInfo.getNumberOfStops() != null) {
                %>
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Number of Stops:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=flightInfo.getNumberOfStops() > 0 ? flightInfo.getNumberOfStops() : "Non-Stop"%></span>
                    </div>
                </div>
                <%
                    }
                %>
            </div>
        </div>

        <div class="row segment-outer-row">
            <div class="col-1" style="margin-right: 10px">
                <span class=""><%=arrivalDepartFormat.format(flightInfo.getArriveDate())%></span>
            </div>
            <div class="col-3" style="text-align: left;color: black !important;">
                <span style="">Arrives:</span>
            </div>
            <div class="col-7">
                <span style=""><%=arrivalHeaderString%></span>
            </div>
        </div>
        <%
            if (StringUtils.isNotBlank(flightInfo.getArrives().getTerminal())) {
        %>
        <div class="row segment-inner-rows">
            <div class="col-12">
                <div class="row">
                    <div class="col-1" style="margin-right: 10px"></div>
                    <div class="col-3">
                        <span class="iti-label">Arrival Terminal:</span>
                    </div>
                    <div class="col-7">
                        <span style=""><%=flightInfo.getArrives().getTerminal()%></span>
                    </div>
                </div>
            </div>
        </div>
        <%
            }
        %>
    </div>
</div>
