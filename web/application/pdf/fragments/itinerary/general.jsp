
<%
    SegmentGeneralCI generalInfo = segment.getGeneral();
    SimpleDateFormat generalFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");
    SimpleDateFormat generalArrivalDepartFormat = new SimpleDateFormat("hh:mm aa");


%>

<div class="row segment">
    <div class="col-12">
        <div class="segment-heading">
            <div class="icon">
                <span class="fa fa-globe"></span>
            </div>
            <div class="segment-heading-text">
                <h1>OTHER DETAILS</h1>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12" style="margin-left: 10px;">
        <div class="row">
            <div class="col-12">
                <h2 class="iti-segment-date"><%=generalFormat.format(generalInfo.getCityDateTime().getDate())%></h2>
            </div>
        </div>
        <div class="row segment-outer-row">
            <div class="col-1">
                <span><%=generalArrivalDepartFormat.format(generalInfo.getCityDateTime().getDate())%></span>
            </div>
            <div class="col-3" style="text-align: left;color: black !important;">
                <span style="">General Booking Information:</span>
            </div>
            <div class="col-7">
                <%
                    for (String text : generalInfo.getText()) {
                %>
                <div class="row">
                    <div class="col-12">
                        <span style=""><%=text.replaceAll("& ", "&amp; ")%></span>
                    </div>
                </div>
                <%
                    }
                %>

            </div>
        </div>
    </div>
</div>