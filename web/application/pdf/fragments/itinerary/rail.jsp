<%@ page import="au.corporateinteractive.qcloud.market.model.travel.core.SegmentRailCI" %>
<%
    SegmentRailCI railInfo = segment.getRail();
    SimpleDateFormat railFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");
    SimpleDateFormat railArrivalDepartFormat = new SimpleDateFormat("hh:mm aa");

    String railArrivalHeaderString = "";
    String railDepartsHeaderString = "";

    if (railInfo.getSupplier() != null && StringUtils.isNotBlank(railInfo.getSupplier().getDescription())) {
        railArrivalHeaderString += railInfo.getSupplier().getDescription();
    }
    if (railInfo.getSupplier() != null && railInfo.getSupplier().getSupplier().getCity() != null && StringUtils.isNotBlank(railInfo.getSupplier().getSupplier().getCity().getCityName())) {
        if (StringUtils.isNotBlank(railArrivalHeaderString.trim())) {
            railArrivalHeaderString += " in ";
        }
        railArrivalHeaderString += railInfo.getSupplier().getSupplier().getCity().getCityName();
    }
    if (railArrivalHeaderString.trim().length() < 1) {
        if (railInfo.getSupplier() != null && railInfo.getSupplier().getSupplier() != null && railInfo.getSupplier().getSupplier().getSupplierCode() != null && StringUtils.isNotBlank(railInfo.getSupplier().getSupplier().getSupplierCode().getName())) {
            railArrivalHeaderString += railInfo.getSupplier().getSupplier().getSupplierCode().getName();
        }
    }
    if (StringUtil.isNullOrEmpty(railArrivalHeaderString.trim())) {
        railArrivalHeaderString += "Pick-up Location Pending";
    }

    if (railInfo.getSupplier() != null && StringUtils.isNotBlank(railInfo.getSupplier().getDescription())) {
        railDepartsHeaderString += railInfo.getSupplier().getDescription() + " on " + railFormat.format(railInfo.getDepartDate());
    }
    if (StringUtil.isNullOrEmpty(railDepartsHeaderString.trim())) {
        railDepartsHeaderString += "Drop-off Location Pending";
    }

%>

<div class="row segment">
    <div class="col-12">
    	<div class="segment-heading">
	      <div class="icon">
	          <span class="fa fa-train"></span>
	      </div>
	      <div class="segment-heading-text">
	          <h1>RAIL DETAILS</h1>
	      </div>
	  </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <h2 class="iti-segment-date"><%=railFormat.format(railInfo.getDepartDate())%></h2>
    </div>
</div>
<div class="row">
    <div class="col-1">
        <span><%=railArrivalDepartFormat.format(railInfo.getDepartDate())%></span>
    </div>
    <div class="col-2" style="text-align: center;">
        <span style="">Pick-Up:</span>
    </div>
    <div class="col-8">
        <span style=""><%=railArrivalHeaderString%></span>
    </div>
</div>


<div class="row">
    <div class="col-12">
        <%
            if (railInfo.getSupplier() != null && StringUtils.isNotBlank(railInfo.getSupplier().getDescription())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Supplier Description:</span>
            </div>
            <div class="col-7">
                <span style=""><%=railInfo.getSupplier().getDescription()%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (railInfo.getSupplier() != null && StringUtils.isNotBlank(railInfo.getSupplier().getPhoneNumber())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Contact:</span>
            </div>
            <div class="col-7">
                <span style=""><%=railInfo.getSupplier().getPhoneNumber()%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (railInfo.getSupplier() != null && railInfo.getSupplier().getAddress() != null && StringUtils.isNotBlank(railInfo.getSupplier().getAddress().getLine1())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Address:</span>
            </div>
            <div class="col-7">
                <span style=""><%=railInfo.getSupplier().getAddress().getLine1()%> <%=railInfo.getSupplier().getAddress().getSuburb()%> <%=railInfo.getSupplier().getAddress().getState()%> <%=railInfo.getSupplier().getAddress().getCountry()%> <%=railInfo.getSupplier().getAddress().getPostcode()%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (railInfo.getSupplier() != null && railInfo.getSupplier().getHours() != null) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Hours:</span>
            </div>
            <div class="col-7">
                <span style=""><%=railArrivalDepartFormat.format(railInfo.getSupplier().getHours().getOpeningTime())%> - <%=railArrivalDepartFormat.format(railInfo.getSupplier().getHours().getClosingTime())%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (railInfo.getSupplier() != null && StringUtils.isNotBlank(railInfo.getSupplier().getRating())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Rail Rating:</span>
            </div>
            <div class="col-7">
                <span style=""><%=railInfo.getSupplier().getRating()%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (StringUtils.isNotBlank(railInfo.getRailCabin())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Cabin Information:</span>
            </div>
            <div class="col-7">
                <span style=""><%=railInfo.getRailCabin()%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (StringUtils.isNotBlank(railInfo.getSeatingInfo())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Seating Information:</span>
            </div>
            <div class="col-7">
                <span style=""><%=railInfo.getSeatingInfo()%></span>
            </div>
        </div>
        <%
            }
        %>
        <%
            if (StringUtils.isNotBlank(railInfo.getClassOfService())) {
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Class of Service:</span>
            </div>
            <div class="col-7">
                <span style=""><%=railInfo.getClassOfService()%></span>
            </div>
        </div>
        <%
            }
        %>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Booking Status:</span>
            </div>
            <div class="col-7">
                <span style=""><%=StringUtils.isNotBlank(railInfo.getStatus()) ? railInfo.getStatus() : "Pending Confirmation"%></span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-1">
        <span><%=railArrivalDepartFormat.format(railInfo.getDepartDate())%></span>
    </div>
    <div class="col-2" style="text-align: center;">
        <span style="">Drop-off:</span>
    </div>
    <div class="col-8">
        <span style=""><%=railDepartsHeaderString%></span>
    </div>
</div>
<%
    if (railInfo.getSupplier() != null && StringUtils.isNotBlank(railInfo.getSupplier().getCancellationPolicy())) {
%>
<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="col-1"></div>
            <div class="col-3">
                <span class="iti-label">Cancelation Policy:</span>
            </div>
            <div class="col-7">

                <span style=""><%=railInfo.getSupplier().getCancellationPolicy()%></span>
            </div>
        </div>
    </div>
</div>
<%
    }
%>