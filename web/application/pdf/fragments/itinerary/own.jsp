<%@ page import="au.corporateinteractive.qcloud.market.model.travel.core.SegmentOwnArrangementsCI" %>
<%
    SegmentOwnArrangementsCI ownInfo = segment.getOwnArrangement();
    SimpleDateFormat ownFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");
    SimpleDateFormat ownArrivalDepartFormat = new SimpleDateFormat("hh:mm aa");


%>

<div class="row segment">
    <div class="col-12">
        <div class="segment-heading">
	      <div class="icon">
	          <span class="fa fa-globe"></span>
	      </div>
	      <div class="segment-heading-text">
	          <h1>OWN ARRANGEMENT DETAILS</h1>
	      </div>
	  </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <h2 class="iti-segment-date"><%=ownFormat.format(ownInfo.getArriveDate())%></h2>
    </div>
</div>
<div class="row">
    <div class="col-1">
        <span><%=ownArrivalDepartFormat.format(ownInfo.getArriveDate())%></span>
    </div>
    <div class="col-3" style="text-align: center;">
        <span style="">Own Arrangements:</span>
    </div>
    <div class="col-7">
        <span style=""><%=ownInfo.getOriginCity().replaceAll("& ", "&amp; ")%></span>
    </div>
</div>


<div class="row">
    <div class="col-1">
        <span><%=ownArrivalDepartFormat.format(ownInfo.getDepartDate())%></span>
    </div>
    <div class="col-3" style="text-align: center;">
        <span style="">Own Arrangements:</span>
    </div>
    <div class="col-7">
        <span style=""><%=ownInfo.getDestinationCity().replaceAll("& ", "&amp; ")%></span>
    </div>
</div>