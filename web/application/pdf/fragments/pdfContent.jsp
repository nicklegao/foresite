<%@page import="java.util.List"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.*" %>
<%@ page import="java.util.stream.Collectors" %>

    <%
    Section coverpageSection = null;

        for (Section section : sections) {
            if (section.getType().equalsIgnoreCase("cover"))
                coverpageSection = section;
        }
        List<Section> dynamicSections = new ArrayList<>();
        for (Section section : sections) {
            if (!section.getType().equalsIgnoreCase("cover"))
                dynamicSections.add(section);
        }

    if (coverpageSection != null) {
        List<Content> coverItems = coverpageSection.getContent();
        for (Content content:coverItems) {
                if (content.getType().equalsIgnoreCase("cover")) {%>

<%@include file="/application/pdf/fragments/coverpage.jsp" %>

                <%}
             }
        } %>
<%--<div style="page-break-after: always;"></div>--%>

<%
    if (dynamicSections != null && dynamicSections.size() > 0) {
        for(Section section:dynamicSections) {
            sectionCount++;
            List<Content> dynamicItems = section.getContent();%>
<div class="doc-page-area-sheet dynaContent" id="content-section">
    <div class="section-head"><%=section.getTitle()%></div>
    <div class="page-content-area">
        <%
            for (Content content:dynamicItems) {
                if (content.getType().equalsIgnoreCase("block-itinerary")) {
        %>
                    <%@include file="/application/pdf/fragments/itinerary.jsp" %>

        <%      } else if (content.getType().equalsIgnoreCase("block-newpage")) {%>

                    <%@include file="/application/pdf/fragments/newPage.jsp" %>

        <%      } else if (content.getType().equalsIgnoreCase("block-zerorisk")) {%>

                    <%@include file="/application/pdf/fragments/zerorisk.jsp" %>

        <%      } else {
                    ColContent colContent = content.getColContent(); %>
        <div class="content-holder sortable" data-type="<%=content.getType()%>" style="width: 100%; height: auto;">
            <%@include file="/application/pdf/fragments/columnContent.jsp" %>
        </div>
        <%
            }
        %>
        <% } %>

    </div>
</div>

<%
    if (sectionCount < sections.size()) { %>
<div style="page-break-after: always;"></div>
        <%  }
        }
    }%>
