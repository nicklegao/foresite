<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.utils.DateFormatUtils" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsStylesDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.AgentsDataAccess" %><%
    String htmlContent = content.getCoverData().getHtml();

    String name = proposalHashtable.getOrDefault(TravelDocsDataAccess.E_CLIENT_NAME, "");
    String surname = proposalHashtable.getOrDefault(TravelDocsDataAccess.E_CLIENT_LAST_NAME, "");
    TravelDocsStylesDataAccess tds = new TravelDocsStylesDataAccess();
    CategoryData companyStyles = tds.getCompanyStylesWithId(company.getId());


    if (proposalHashtable.containsKey(TravelDocsDataAccess.E_ITINERARY_DATA) && proposalHashtable.get(TravelDocsDataAccess.E_ITINERARY_DATA) != null) {
        StoreUtils storeUtils = new StoreUtils();
        AgentsDataAccess agda = new AgentsDataAccess();
        String itineraryFile = storeUtils.readFieldFromStores(proposalHashtable.get(TravelDocsDataAccess.E_ITINERARY_DATA));
        Gson gson = new Gson();
        if (itineraryFile.length() > 100) {
            JSONObject itin = new JSONObject(itineraryFile);
            String itinerary = itin.getString("data");
            BookingCI booking = gson.fromJson(itinerary, BookingCI.class);
            String bookingCompany = "";
            String agentNumber = "";
            if (booking != null && booking.getCompany() != null) {
                ElementData agent = agda.getAgentbyCompanyIDAgentID(company.getId(), booking.getCompany().getName(), booking.getAgency().getAgency().getCode());
                if (agent != null) {
                    bookingCompany = agent.getName();
                    agentNumber = agent.getString(AgentsDataAccess.E_PHONE_NUMBER);
                }
            }

            DateFormatUtils dateFormater = DateFormatUtils.getInstance(companyStyles.getString(TravelDocsStylesDataAccess.C_DATE_FORMAT), Locale.forLanguageTag(company.getString(UserAccountDataAccess.C1_LOCALE)));

            String bookingDate = "";
            if (booking.getBookingStatus() != null && booking.getBookingStatus().getLastUpdated() != null) {
                bookingDate = dateFormater.format(booking.getBookingStatus().getLastUpdated());
            } else {
                bookingDate = proposalMetadata.get("proposalCreate");
            }

            String bookingAgentName = "";
            String bookingAgentEmail = "";
            if (booking.getBookingEmails() != null && booking.getBookingEmails().getEmail().size() > 0) {
                bookingAgentName = (StringUtils.isNotBlank(booking.getBookingEmails().getEmail().get(0).getName()) ? booking.getBookingEmails().getEmail().get(0).getName() : booking.getBookingEmails().getEmail().get(0).getAddress());
                if (bookingAgentName.contains("@")) {
                    bookingAgentName = bookingAgentName.split("@")[0];
                }
                bookingAgentEmail = booking.getBookingEmails().getEmail().get(0).getAddress();
            } else {
                bookingAgentName = (owner.getString(UserAccountDataAccess.E_NAME) + " " + owner.getString(UserAccountDataAccess.E_SURNAME));
                bookingAgentEmail = owner.getString(UserAccountDataAccess.E_EMAIL_ADDRESS);
            }
            if (booking.getCompany() != null && StringUtils.isNotBlank(booking.getCompany().getName())) {
                if (StringUtil.isNullOrEmpty(bookingCompany)){
                    bookingCompany = booking.getCompany().getName();
                    while(bookingCompany.endsWith("Z")) {
                        bookingCompany = bookingCompany.substring(0, bookingCompany.length() - 1);
                    }
                }
            } else {
                if (StringUtil.isNullOrEmpty(bookingCompany)) {
                    bookingCompany = company.getName();
                }
            }

            if (StringUtil.isNullOrEmpty(agentNumber)) {
                agentNumber = "Unknown Number";
            }

            htmlContent = htmlContent.replaceAll("\\Q${\\EreferenceNumber}", booking.getBookingReference());
            htmlContent = htmlContent.replaceAll("\\Q${\\Edate}", bookingDate);
            htmlContent = htmlContent.replaceAll("\\Q${\\EbookingAgent}", bookingAgentName);
            htmlContent = htmlContent.replaceAll("\\Q${\\EbookingCompany}", bookingCompany);
            htmlContent = htmlContent.replaceAll("\\Q${\\EagentPhone}", agentNumber);
            htmlContent = htmlContent.replaceAll("\\Q${\\EagentEmail}", bookingAgentEmail);
        }
    }

    htmlContent = htmlContent.replaceAll("\\Q${\\EcustomerName}", (surname + "/" + name));

%>

<div class="doc-page-area-sheet coverPageContent" id="content-section-coverpage" data-title="Cover Page">
    <%= htmlContent %>
</div>