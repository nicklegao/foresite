<%@page import="org.apache.commons.lang.StringUtils"%>
<%
    String bodyArea = "";

    if (colContent != null) {
        bodyArea += colContent.getContentHTMLForPDF();
    } else {
        if (content.getType().equalsIgnoreCase("block-video")) {
            if (!StringUtil.isNullOrEmpty(content.getData()) && content.getData().toLowerCase().contains("vimeo")) {
                String id = content.getData().substring(content.getData().lastIndexOf("o/") + 2);
                bodyArea += "<div class=\"videoContainer\" style=\"vertical-align: top;display: inline-block;text-align: center;width: 100%;padding: 15px;margin-left:10px;margin-right:10px;\"><a href=\""+content.getData()+"\"><img src=\""+new TravelDocsDataAccess().getVimeoPreviewImage(id)+"\" style=\"width: 95%;height: auto;\"></img></a></div>";
            } else if (!StringUtil.isNullOrEmpty(content.getData()) && content.getData().toLowerCase().contains("youtube")) {
                String id = content.getData().substring(content.getData().lastIndexOf("d/") + 2);
                bodyArea += "<div class=\"videoContainer\" style=\"vertical-align: top;display: inline-block;text-align: center;width: 100%;padding: 15px;margin-left:10px;margin-right:10px;\"><a href=\""+content.getData()+"\"><img src=\"https://img.youtube.com/vi/"+id+"/maxresdefault.jpg\" style=\"width: 95%;height: auto;\"></img></a></div>";
            }
        } else {
            if(content.getData() != null)
                bodyArea += content.getData().replace("1px dotted", "");
        }

    }
%>
<%=bodyArea%>