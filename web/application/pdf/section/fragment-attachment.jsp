<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.AttachmentDetails"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.ProposalAttachmentDataAccess"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal" %>
<jsp:useBean id="proposal" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"></jsp:useBean>
<jsp:useBean id="content" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"></jsp:useBean>

<%
AttachmentDetails attachmentData =  new ProposalAttachmentDataAccess().loadAttachmentById(content.getId());
if(attachmentData != null){
request.setAttribute("attachmentData",attachmentData);

  request.setAttribute("page-break-before-always", true);
%>
<div class="noBreak">
  
  <h3><c:out value="${attachmentData.title}" /></h3>
  <div class="">
  	<table style="width: 100%;">
  	<tbody>
  	<tr>
  	<td style="width: 35%;">
    <div class="downloadCell">
      <a target="_blank" class="attachment" 
         href="<%=request.isSecure()?"https://":"http://"%><%=request.getServerName()%>:<%=request.getServerPort()%>/<c:out value="${initParam.storeContext}" /><c:out value="${attachmentData.downloadPath}" />">
        <div class="downloadDiv">
        </div>  
        <div class="lower"> 
          <span>${SYSTEM_MESSAGE_ATTACHMENT_CLICK_HERE}</span>
        </div>
      </a>
    </div>
    </td>
    <td>
    <div class="col-sm-8">
      <div class="downloadDesc">
        <%
        String description = ((AttachmentDetails)request.getAttribute("attachmentData")).getDescription();
        description = description.replaceAll("<br/>","\n");
        description = ESAPI.encoder().encodeForHTML(description);
        description = description.replaceAll("&#xa;","<br/>");
        %>
        <%= description %>
      </div>
    </div>
    </td>
    </tr>
    </tbody>
    </table>
    <div class="clearfix"></div>
  </div>
</div>
<%}%>