<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.CustomFieldsDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.CustomFields" %>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.PricingViewMerger"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.StaticTextDataAccess"%>
<%@page import="java.util.Hashtable"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.MoneyUtils.DisplayableMoney"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.MoneyUtils"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.PricingTableSection" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.PricingTableSectionName" %>
<%@page import="java.util.Map"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Pricing" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal" %>
<%@ page import="java.util.Enumeration" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="proposal" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"></jsp:useBean>
<jsp:useBean id="pricingIndex" scope="request" type="java.lang.Integer"></jsp:useBean>
<%
Hashtable<String, String> proposalHt = (Hashtable<String, String>) request.getAttribute("proposalHashtable");
Pricing pricing = proposal.getPricings().get(pricingIndex);
boolean isSummarised = pricing.getSummarisedPricing();
List<Map<String, String>> titles = pricing.sectionsTitles();
titles.addAll(pricing.getTitles());
Map<String, HashMap<String, Object>> summarisedSections = pricing.sectionsSummarised();

	System.out.println(request.getParameter("viewallhidden"));
	boolean isHideMargin = true;
	pricing.setHideMarginFromPriceRow(true);
if (request.getParameter("viewallhidden") != null && request.getParameter("viewallhidden").equalsIgnoreCase("true")) {
	if (request.getParameterValues("hiddenOptions") != null) {
		String[] hiddenOptions = request.getParameterValues("hiddenOptions");
		for (String option : hiddenOptions) {
			switch (option.toLowerCase()) {
				case "summaryrows":
					summarisedSections = new HashMap<>();
					for (PlanClass row : pricing.getRows()) {
						row.setIsSummarised(false);
					}
					break;
				case "customfields":
					pricing.setHideCustom1(false);
					pricing.setHideCustom2(false);
					pricing.setHideCustom3(false);
					pricing.setHideCustom4(false);
					pricing.setHideCustom5(false);
					break;
				case "discounts":
					pricing.setHideDiscountFromPriceRow(false);
					isHideMargin = false;
					pricing.setHideMarginFromPriceRow(false);
					break;
				default:
					System.out.println("Opps");
			}
		}
	}

}

//summarisedSections.addAll(pricing.getTitles());
String companyID = (String)request.getAttribute("companyID");
UserAccountDataAccess uada = new UserAccountDataAccess();
CategoryData companySettings = uada.getCompanyForCompanyId(companyID);
Pricing.TermTypeI18N pricingTermType = StringUtils.isNotBlank(pricing.getPricingTermType()) ? Pricing.TermTypeI18N.valueOf(pricing.getPricingTermType()) : Pricing.TermTypeI18N.Months;

boolean isTermHideAlways = companySettings.getBoolean(uada.C1_HIDE_TERMS_ALWAYS);
int hidden = 0;
boolean isHidePriceQty = pricing.isHidePriceQty();
boolean isHideTerm = pricing.isHideTerm();
boolean isHidePriceColumn = pricing.isHidePriceColumn();
boolean isHideTermColumn = pricing.isHideTermColumn();
boolean isHideQtyColumn = pricing.isHideQtyColumn();
boolean isHideTotalPriceColumn = pricing.isHideTotalPriceColumn();
boolean isHideTotalCostColumn = pricing.isHideTotalCostColumn();
boolean isHideTaxColumn = pricing.isHideTaxColumn();
boolean hideCustom1 = pricing.isHideCustom1();
boolean hideCustom2 = pricing.isHideCustom2();
boolean hideCustom3 = pricing.isHideCustom3();
boolean hideCustom4 = pricing.isHideCustom4();
	boolean hideCustom5 = pricing.isHideCustom5();
	boolean isHideGrandTotalPriceRow = pricing.isHideGrandTotalPriceRow();
	boolean isHideOnOffCostRow = pricing.isHideOnOffCostRow();
boolean isHideOverallTotalPriceRow = (pricingIndex == proposal.getPricings().size() - 1 && pricingIndex > 0) ? pricing.isHideOverallTotalPriceRow() : true;
boolean isHideDiscountFromPriceRow = pricing.isHideDiscountFromPriceRow();
boolean isHideMinimumMonthlyCost = pricing.isHideMinimumMonthlyCost();
boolean isHideFullTermCost = pricing.isHideFullTermCost();
boolean removeTax = pricing.isRemoveTax();
boolean isHideAdditionColumn = pricing.isHideAdditionColumn();

if (companySettings.getBoolean(uada.C1_HIDE_TERMS_ALWAYS)) {
	isHideTerm = true;
	isHideTermColumn = true;
}
if (!companySettings.getBoolean(uada.C1_ENABLE_ADDITION)) {
	isHideAdditionColumn = true;
}
if (!companySettings.getBoolean(uada.C1_ENABLE_TAX)) {
	removeTax = true;
}

Map<String, String> initialTitle = new HashMap();

initialTitle.put("label", "");

titles.add(0, initialTitle);

Map<PricingTableSectionName, PricingTableSection> planSectionMap = pricing.getPlanSections();
PricingTableSection ongoingCost = planSectionMap.get(PricingTableSectionName.ONGOING_COST);
PricingTableSection ongoingDiscount = planSectionMap.get(PricingTableSectionName.ONGOING_DISCOUNT);
DisplayableMoney monthlyTotal = MoneyUtils.toDisplayableMoney(ongoingCost.getTotalUnitPrice());
DisplayableMoney fullTermTotal = MoneyUtils.toDisplayableMoney(ongoingCost.getTotalPrice() + ongoingDiscount.getTotalPrice());
PricingTableSection oneOffCost = planSectionMap.get(PricingTableSectionName.ONE_OFF_COST);
PricingTableSection oneOffDiscount = planSectionMap.get(PricingTableSectionName.ONE_OFF_DISCOUNT);
PricingTableSection totalSection = planSectionMap.get(PricingTableSectionName.TOTAL);

pageContext.setAttribute("oneOffCost", oneOffCost);
pageContext.setAttribute("oneOffDiscount", oneOffDiscount);

pageContext.setAttribute("ongoingCost", ongoingCost);
pageContext.setAttribute("ongoingDiscount", ongoingDiscount);

pageContext.setAttribute("totalSection", totalSection);

pageContext.setAttribute("monthlyTotal", monthlyTotal);
pageContext.setAttribute("fullTermTotal", fullTermTotal);


pageContext.setAttribute("isSummarised", false);
pageContext.setAttribute("isHidePriceQty", isHidePriceQty);
pageContext.setAttribute("isHideTerm", isHideTerm);
pageContext.setAttribute("isHidePriceColumn",isHidePriceColumn);
pageContext.setAttribute("isHideTermColumn",isHideTermColumn);
pageContext.setAttribute("isHideQtyColumn",isHideQtyColumn);
pageContext.setAttribute("isHideTotalPriceColumn",isHideTotalPriceColumn);
pageContext.setAttribute("isHideTotalCostColumn",isHideTotalCostColumn);
pageContext.setAttribute("isHideTaxColumn",isHideTaxColumn);
	pageContext.setAttribute("isHideGrandTotalPriceRow",isHideGrandTotalPriceRow);
	pageContext.setAttribute("isHideOnOffCostRow",isHideOnOffCostRow);
	pageContext.setAttribute("isHideDiscountFromPriceRow",isHideDiscountFromPriceRow);
	pageContext.setAttribute("isHideMarginFromPriceRow",isHideMargin);
pageContext.setAttribute("isHideMinimumMonthlyCost",isHideMinimumMonthlyCost);
pageContext.setAttribute("isHideFullTermCost",isHideFullTermCost);
pageContext.setAttribute("removeTax",removeTax);
pageContext.setAttribute("monthlyTotal", monthlyTotal);
pageContext.setAttribute("fullTermTotal", fullTermTotal);
pageContext.setAttribute("titles", titles);
pageContext.setAttribute("summarisedSections", summarisedSections);
pageContext.setAttribute("pricingTermType", pricingTermType);
pageContext.setAttribute("hideCustom1", hideCustom1);
pageContext.setAttribute("hideCustom2", hideCustom2);
pageContext.setAttribute("hideCustom3", hideCustom3);
pageContext.setAttribute("hideCustom4", hideCustom4);
pageContext.setAttribute("hideCustom5", hideCustom5);
pageContext.setAttribute("isHideAdditionColumn",isHideAdditionColumn);
String locale = (String) request.getAttribute("locale");

Map<String,Object> extraFields = new TreeMap<String,Object>();
extraFields.put("oneOffCost", oneOffCost);
extraFields.put("oneOffDiscount", oneOffDiscount);
extraFields.put("ongoingCost", ongoingCost);
extraFields.put("ongoingDiscount", ongoingDiscount);
extraFields.put("monthlyTotal", Double.parseDouble(monthlyTotal.toString()));
extraFields.put("fullTermTotal", Double.parseDouble(fullTermTotal.toString()));
extraFields.put("isHidePriceColumn",isHidePriceColumn);
extraFields.put("isHideTermColumn",isHideTermColumn);
extraFields.put("isHideQtyColumn",isHideQtyColumn);
extraFields.put("isHideTotalPriceColumn",isHideTotalPriceColumn);
extraFields.put("isHideTotalCostColumn",isHideTotalCostColumn);
extraFields.put("isHideTaxColumn",isHideTaxColumn);
	extraFields.put("isHideGrandTotalPriceRow",isHideGrandTotalPriceRow);
	extraFields.put("isHideOnOffCostRow",isHideOnOffCostRow);
extraFields.put("isHideOverallTotalPriceRow",isHideOverallTotalPriceRow);
	extraFields.put("isHideDiscountFromPriceRow",isHideDiscountFromPriceRow);
	extraFields.put("isHideMarginFromPriceRow",isHideMargin);
extraFields.put("isHideMinimumMonthlyCost",isHideMinimumMonthlyCost);
extraFields.put("isHideFullTermCost",isHideFullTermCost);
extraFields.put("hideCustom1", hideCustom1);
extraFields.put("hideCustom2", hideCustom2);
extraFields.put("hideCustom3", hideCustom3);
extraFields.put("hideCustom4", hideCustom4);
extraFields.put("hideCustom5", hideCustom5);
extraFields.put("removeTax",removeTax);
extraFields.put("isHideAdditionColumn",isHideAdditionColumn);
extraFields.put("spectrumColor", request.getAttribute("spectrumColor"));
extraFields.put("locale",locale);
extraFields.put("pricingTermType",pricingTermType);
if(pricing.getTax()!=null){
	extraFields.put("tax",pricing.getTax());
}
if(pricing.getTaxLabel()!=null){
	extraFields.put("taxLabel",pricing.getTaxLabel());
}
if(pricing.getTaxPercentage()!=null){
	extraFields.put("taxPercentage",pricing.getTaxPercentage());
}

CustomFieldsDataAccess cfda=new CustomFieldsDataAccess();
CustomFields cf = cfda.getCompanyCustomFields(CustomFields.PRODUCTS, companyID);

extraFields.put("customfields",cf);

extraFields.put("rounding",companySettings.getBoolean(uada.C1_ROUNDING));

PricingViewMerger vm = new PricingViewMerger(companyID, proposalHt.get(TravelDocsDataAccess.E_ID), extraFields);
%>
<c:set var="totalIndex" value="${0}" />
<fmt:setLocale value="${locale}"/>
	<c:set var="titles" value="${titles}" />
	<c:set var="summarisedSections" value="${summarisedSections}" />
	<table id="pricingTable" class="pricingTable" cellpadding="0" cellspacing="0">
		<thead>
			<%=vm.mergeTableHeader() %>
		</thead>
	    
	      <c:if test="${fn:length(oneOffCost.sortedPlans) > 0}">
	         <c:set var="plans" value="${oneOffCost.sortedPlans}" />
	         	<c:if test="${fn:length(ongoingCost.sortedPlans) > 0}">
		         <tr class="groupTitleRow">
				  	<td colspan="5">${TEXT_CON_PLANS_ONE_OFF_COSTS}</td>
				 </tr>
				</c:if>
	         <%@include file="/application/pdf/section/plan/one_off_row.jsp" %>
	      </c:if>
	      <c:if test="${fn:length(oneOffDiscount.sortedPlans) > 0}">
	         <c:set var="plans" value="${oneOffDiscount.sortedPlans}" />
	         <tr class="groupTitleRow">
			 	<td colspan="4">${TEXT_CON_PLANS_LESS_DISCOUNTS}</td>
			 </tr>
	         <%@include file="/application/pdf/section/plan/one_off_row.jsp" %>
	      </c:if>
	    
	    <c:if test="${fn:length(ongoingCost.sortedPlans) > 0}">
		    <c:set var="plans" value="${ongoingCost.sortedPlans}" />
		    <c:if test="${fn:length(oneOffCost.sortedPlans) > 0}">
			    <tr class="groupTitleRow">
					<td colspan="4">${pricingTermType.getTermOngoingLabel()}</td>
				</tr>
			</c:if>
		    <%@include file="/application/pdf/section/plan/ongoing_row.jsp" %>
	    </c:if>
	    
	    <c:if test="${fn:length(ongoingDiscount.sortedPlans) > 0}">
			<c:set var="plans" value="${ongoingDiscount.sortedPlans}" />
			<tr class="groupTitleRow">
				<td colspan="5">${pricingTermType.getTermDiscountsLabel()}</td>
			</tr>
	        <%@include file="/application/pdf/section/plan/ongoing_row.jsp" %>
	    </c:if>
	    
	    <c:if test="${(fn:length(oneOffCost.sortedPlans) + fn:length(oneOffDiscount.sortedPlans) + fn:length(ongoingCost.sortedPlans) + fn:length(ongoingDiscount.sortedPlans))  > 1}">
		    <c:set var="plans" value="${totalSection.sortedPlans}" />
	        <%@include file="/application/pdf/section/plan/total_row.jsp" %>
        </c:if>
	</table>

	<table class="pricingTableFooter noBreak">
		<!-- <tr>
			<td></td>
			<td width="10%"></td>
			<td width="22%"></td>
			<td width="120"></td>
		</tr> -->
		<%=vm.mergeTablefooter(pricing) %>		
	</table>


