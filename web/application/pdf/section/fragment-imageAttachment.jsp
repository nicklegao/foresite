<%@page import="java.awt.image.BufferedImage" %>
<%@page import="java.io.File"%>
<%@page import="javax.imageio.ImageIO"%>
<%@page import="org.apache.commons.lang.StringUtils" %>
<%@page import="org.owasp.esapi.ESAPI" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.ProposalAttachmentDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.AttachmentDetails" %>
<%@page import="au.net.webdirector.common.Defaults" %>
<jsp:useBean id="proposal" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"></jsp:useBean>
<jsp:useBean id="content" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"></jsp:useBean>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
	final int MAX_IMAGE_WIDTH_FOR_PDF = 729;
	final int HALF_PAGE_WIDTH = MAX_IMAGE_WIDTH_FOR_PDF / 2;

	request.setAttribute("page-break-before-always", true);
	
	AttachmentDetails attachment =  new ProposalAttachmentDataAccess().loadAttachmentById(content.getId());%>

<%
	if(attachment != null){
		request.setAttribute("attachment", attachment);
		
		BufferedImage bimg = ImageIO.read(new File(Defaults.getInstance().getStoreDir() + attachment.getDownloadPath()));
		int width = bimg.getWidth();
		if (width > MAX_IMAGE_WIDTH_FOR_PDF) 
		{
			width = MAX_IMAGE_WIDTH_FOR_PDF;
		}
		if (StringUtils.equalsIgnoreCase(content.getMetadata().get("alignment"), "left")
				|| StringUtils.equalsIgnoreCase(content.getMetadata().get("alignment"), "right")) 
		{
			if (width > HALF_PAGE_WIDTH) 
			{
				width = HALF_PAGE_WIDTH;
			}
		}
%>

	<div>
	  <h3 style="text-align: <c:out value="${content.metadata.alignment}" />"><c:out value="${attachment.title}" /></h3>
	  
	  <c:if test="${content.metadata.alignment == 'center'}">
	  	<p style="text-align: <c:out value="${content.metadata.alignment}" />">
	  		<img width="<%= width %>"
		       src="/<c:out value="${initParam.storeContext}" /><c:out value="${attachment.encodedPath}" />"
		    />
	  	</p>
	  </c:if>
	  <c:if test="${content.metadata.alignment != 'center'}">
	  	<img width="<%= width %>"
	       src="/<c:out value="${initParam.storeContext}" /><c:out value="${attachment.encodedPath}" />"
	       style="float: <c:out value="${content.metadata.alignment}" />; margin-right: 15px;"
	    />
	  </c:if>
	  
	  <%
		String description = attachment.getDescription();
		description = description.replaceAll("<br/>","\n");
		description = ESAPI.encoder().encodeForHTML(description);
		description = description.replaceAll("&#xa;","<br/>");
	  %>

	  <p><%=description %></p>
	  <div style="clear: both;"></div>
	</div>
<% } %>