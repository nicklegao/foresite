<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.TextLibraryService"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"%>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<jsp:useBean id="proposal" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"></jsp:useBean>
<jsp:useBean id="content" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"></jsp:useBean>
<jsp:useBean id="userMetadata" scope="request" type="java.util.Map"></jsp:useBean>
<jsp:useBean id="company" scope="request" type="au.net.webdirector.common.datalayer.client.CategoryData"></jsp:useBean>
<%
Content conInfo = proposal.findGeneratedContentOfType("con-info");
Content conCustomFields = proposal.findGeneratedContentOfType("con-custom-fields");

    request.setAttribute("page-break-before-always", true);

Map<String,String> mp = new TreeMap<String, String>();
mp.putAll(conInfo.getMetadata());
if(conCustomFields != null)
	mp.putAll(TextLibraryService.formatCustomFields(conCustomFields.getMetadata(), company));

    System.out.println(proposal);
//    CategoryData companyData = new UserAccountDataAccess().getUserCompanyForUserId(proposal);

// String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
// Map<String, String> userFields = new UserAccountDataAccess().getUserMetadata(userId);

request.setAttribute("textData", new TextLibraryService().generateTextForView(
    content.getData(),
    userMetadata,
    mp,
    content.getMetadata() ));
%>
<div class="textData">
  <p><c:out value="${textData.dataForPdf}" escapeXml="false"/></p>
</div>
