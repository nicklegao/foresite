<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.TextLibraryService"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.*"%>
<%@page import="java.util.*"%>
<jsp:useBean id="proposal" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"></jsp:useBean>
<jsp:useBean id="section" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Section"></jsp:useBean>

<%
 Integer index = (Integer)request.getAttribute("sectionIndex");
 String title = section.getGeneratedTitle();
	String showTitle = section.getTitleHidden();
%>
<div class="sectionDiv">
	<div class="sectionContent">
		<%
		List<Content> sectionContent = section.getContent();
		for (int j = 0 ; j < sectionContent.size() ; ++j)
		{
			Content content = sectionContent.get(j);
			request.setAttribute("content", content);
			request.setAttribute("sectionTitle", title);
			request.setAttribute("showTitle", showTitle);
			request.setAttribute("contentIndex", j);
			%>
			<jsp:include page="/application/pdf/section/content.jsp"></jsp:include>
			<%
		}
		%>
	</div>
</div>




