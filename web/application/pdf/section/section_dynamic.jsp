<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.TextLibraryService"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content" %>
<%@page import="java.util.*"%>
<jsp:useBean id="proposal" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"></jsp:useBean>
<jsp:useBean id="section" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Section"></jsp:useBean>

<%
 Integer index = (Integer)request.getAttribute("sectionIndex");
 String title = section.getGeneratedTitle();
 String showTitle = section.getTitleHidden();
%>
<div class="sectionDiv">
	<div class="sectionContent">
		<%
		List<Content> sectionContent = section.getContent();
		for (int j = 0 ; j < sectionContent.size() ; ++j)
		{
			Content content = sectionContent.get(j);
			request.setAttribute("content", content);
			request.setAttribute("sectionTitle", title);
			request.setAttribute("showTitle", showTitle);
			request.setAttribute("contentIndex", j);
			if ((j+1) == sectionContent.size() && request.getAttribute("last-page-nobreak-section") != null) {
				request.setAttribute("last-page-nobreak", true);
			}
			%>
			<jsp:include page="/application/pdf/section/content.jsp"></jsp:include>
			<%
		}
		%>
		
		<%
		if ("true".equals(request.getParameter("isIntroduction"))) {
		%>
		<!-- <div class="introductionLegal greyBlock smallerText noBreak">
		  <p>For each suggested plan below, click on the link to the <a class="inPageLink" href="#yourSolutionInDetail">Critical Information Summary</a> for important details.</p>
		</div> -->
		<%
		}
		%>
		
	</div>
</div>
