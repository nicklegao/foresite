<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.PricingViewMerger"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="java.io.File"%>
<%@page import="java.util.Map"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.PlanClass"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:forEach items="${plans}" var="p" varStatus="status">
  <%=vm.mergeTotalRow((PlanClass)pageContext.getAttribute("p")) %>
</c:forEach>
