<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.PricingViewMerger"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="java.io.File"%>
<%@page import="java.util.Map"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.PlanClass"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:forEach items="${plans}" var="p" varStatus="status">
  <c:choose>
	<c:when test="${p.type == 'title' && p.isSummarised}">
	  	<%=vm.mergeSummarisedRow((PlanClass)pageContext.getAttribute("p")) %>
	</c:when>
	<c:when test="${p.type == 'title' && !p.isSummarised}">
	  	<%=vm.mergeSectionTitle((PlanClass)pageContext.getAttribute("p")) %>
	</c:when>
	<c:otherwise>
	    <%=vm.mergeOneOffRow((PlanClass)pageContext.getAttribute("p")) %>
	</c:otherwise>
  </c:choose>
</c:forEach>
