<%@page import="java.util.TreeMap"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:forEach items="${plans}" var="p" varStatus="status">
  <c:choose>
	<c:when test="${p.type == 'title' && p.isSummarised}">
	  	<%=vm.mergeSummarisedRow((PlanClass)pageContext.getAttribute("p")) %>
	</c:when>
	<c:when test="${p.type == 'title' && !p.isSummarised}">
	  	<%=vm.mergeSectionTitle((PlanClass)pageContext.getAttribute("p")) %>
	</c:when>
	<c:otherwise>
	    <%=vm.mergeOngoingRow((PlanClass)pageContext.getAttribute("p")) %>
	</c:otherwise>
  </c:choose>
</c:forEach>
