<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.StaticTextDataAccess"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.TextLibraryService"%>
<%@page import="java.util.*"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.AttachmentDetails" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.TextDetails" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<jsp:useBean id="proposal" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"></jsp:useBean>
<jsp:useBean id="section" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Section"></jsp:useBean>
<jsp:useBean id="company" scope="request" type="au.net.webdirector.common.datalayer.client.CategoryData"></jsp:useBean>

<%
String title = section.getGeneratedTitle();
String showTitle = section.getTitleHidden();
%>
<div class="sectionDiv">
	<div class="sectionContent">
		<%
		List<Content> sectionContent = section.getContent();
        int pricingIndex = 0;
		for (int j = 0 ; j < sectionContent.size(); ++j)
		{
			Content content = sectionContent.get(j);
            if(content.getType().equals("generated") && content.getId().equals("con-plans")){
    			request.setAttribute("pricingIndex", pricingIndex++);
            }
			request.setAttribute("content", content);
			request.setAttribute("sectionTitle", title);
			request.setAttribute("showTitle", showTitle);
			request.setAttribute("contentIndex", j);
			%>
			<jsp:include page="/application/pdf/section/content.jsp"></jsp:include>
			<%
		}
		%>

        <%
			Content conInfo = proposal.findGeneratedContentOfType("con-info");
			Content conCustomFields = proposal.findGeneratedContentOfType("con-custom-fields");
			Map<String,String> mp = new TreeMap<String, String>();
			mp.putAll(conInfo.getMetadata());

			UserAccountDataAccess uada = new UserAccountDataAccess();

			request.setAttribute("page-break-before-always", true);
			if(conCustomFields != null)
				mp.putAll(TextLibraryService.formatCustomFields(conCustomFields.getMetadata(), company));


			Hashtable<String, String> proposalHashtable = (Hashtable<String, String>)request.getAttribute("proposalHashtable");

//			CategoryData company = uada.getCompanyForCompanyId(proposal.get(TravelDocsDataAccess.E_OWNER));
			List<Hashtable<String, String>> users = uada.getUsersByCompanyId(company.getId());
			Hashtable<String, String> consultantHT = users.get(0);
			Map<String, String> metadata = uada.getUserMetadata(consultantHT.get(UserAccountDataAccess.E_ID));
       	 	String contentString = new StaticTextDataAccess().readContentWithUserId(consultantHT.get(UserAccountDataAccess.E_ID), StaticTextDataAccess.EHEADLINE_PRICING_WANT_MORE_DETAIL);

			TextDetails contentBlock = new TextLibraryService().generateTextForView(contentString, metadata, mp, conInfo.getMetadata());
			contentString = contentBlock.getDataForPdf();


        %>
      <div class="contentDiv content2"> 
        <div class="contentHolder">
          <div class="greyBlock moreDetail noBreak">
            <%= contentString %>
          </div>
        </div> 
      </div>
	</div>
</div>
