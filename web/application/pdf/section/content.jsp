<%@page import="java.util.Map"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="content" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"></jsp:useBean>
<jsp:useBean id="contentIndex" scope="request" type="java.lang.Integer"></jsp:useBean>
<jsp:useBean id="sectionTitle" scope="request" type="java.lang.String"></jsp:useBean>
<jsp:useBean id="showTitle" scope="request" type="java.lang.String"></jsp:useBean>
<%
if (contentIndex == 0)
{
    if (showTitle.equalsIgnoreCase("off")) { %>
<div class="sectionTitle"><c:out value="<%=sectionTitle %>" escapeXml="true"/></div>

	<% } else { %>
	<div class="sectionTitle" style="padding-bottom: 10px;"></div>
<% }
}
%>
<%
	boolean pageBroken = request.getAttribute("page-break-after-always") != null;
	if(pageBroken){
		request.removeAttribute("page-break-after-always");
	}
	boolean topPageBroken = request.getAttribute("page-break-before-always") == null;
	if(topPageBroken){
		%>
	<div class="contentDiv content<%=contentIndex %> page-break-before-off">
	<%
	} else {
		%>
	<div class="contentDiv content<%=contentIndex %> page-break-before-<%= pageBroken ? "off" : content.getBreakPageBeforeInPDF() %>">
	<%
	}
%>

<div class="contentHolder">
	<%
	if (content.getType().equals("generated"))
	{
		if (content.getId().equals("con-plans"))
		{
		%>
			<jsp:include page="/application/pdf/section/con-plans.jsp"></jsp:include>
		<%
		}
		else if (content.getId().equals("con-uscs"))
		{
		%>
			<jsp:include page="/application/pdf/section/con-uscs.jsp"></jsp:include>
		<%
		}
		else if (content.getId().equals("con-should-know"))
		{
		%>
			<jsp:include page="/application/pdf/section/con-should-know.jsp"></jsp:include>
		<%
		}
		else if (content.getId().equals("con-terms"))
		{
		%>
			<jsp:include page="/application/pdf/section/con-terms.jsp"></jsp:include>
		<%
		}
		else if (content.getId().equals("con-iti"))
		{
		%>
			<jsp:include page="/application/itinerary/sabre/itinerary.jsp"></jsp:include>
		<%
		}
		else if (content.getId().startsWith("con-fnb-"))
		{
		%>
			<jsp:include page="/application/pdf/section/con-fnb.jsp"></jsp:include>
		<%
		}
	}
	else if (content.getType().equals("image"))
	{
	%>
		<jsp:include page="/application/pdf/section/fragment-image.jsp"></jsp:include>
	<%
	}
	else if (content.getType().equals("video"))
	{
	%>
		<jsp:include page="/application/pdf/section/fragment-video.jsp"></jsp:include>
	<%
	}
	else if (content.getType().equals("text"))
	{
	%>
		<jsp:include page="/application/pdf/section/fragment-text.jsp"></jsp:include>
	<%
	}
	else if (content.getType().equals("sign"))
	{
	%>
		<jsp:include page="/application/pdf/section/fragment-sign.jsp"></jsp:include>
	<%
	}
	else if (content.getType().equals("spreadsheet"))
	{
	%>
		<jsp:include page="/application/pdf/section/fragment-spreadsheet.jsp"></jsp:include>
	<%
	}
	else if (content.getType().equals("editor"))
	{
	%>
	<jsp:include page="/application/pdf/section/fragment-editor.jsp"></jsp:include>
	<%
	}else if (content.getType().equals("destination"))
	{
	%>
	<jsp:include page="/application/pdf/section/fragment-destination.jsp"></jsp:include>
	<%
	}
	else if (content.getType().equals("attachment"))
	{
		Map<String, String> metadata = content.getMetadata();
		if (Boolean.valueOf(metadata.get("embedPDFSwitch"))) {
		%>
		<jsp:include page="/application/pdf/section/fragment-pdf-inline.jsp"></jsp:include>
		<%
		} else { %>
		<jsp:include page="/application/pdf/section/fragment-attachment.jsp"></jsp:include>
	<%	}
	}
	else if (content.getType().equals("pdf"))
	{
		Map<String, String> metadata = content.getMetadata();
		if (Boolean.valueOf(metadata.get("embedPDFSwitch"))) {
		%>
		<jsp:include page="/application/pdf/section/fragment-pdf-inline.jsp"></jsp:include>
		<%
		} else { %>
		<jsp:include page="/application/pdf/section/fragment-pdf-link.jsp"></jsp:include>
	<%	}
	}
	else if (content.getType().equals("imageAttachment"))
	{
	%>
		<jsp:include page="/application/pdf/section/fragment-imageAttachment.jsp"></jsp:include>
	<%
	}
	%>
</div>
</div>