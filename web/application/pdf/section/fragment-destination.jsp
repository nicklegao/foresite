<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.exception.NoLongerAvaliableException"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.ProposalAttachmentDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.AttachmentDetails"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.ImageDetails"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.TextLibraryService"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.TextDetails" %>
<%@ page import="org.springframework.security.access.method.P" %>
<%@ page import="java.util.Arrays" %>
<jsp:useBean id="proposal" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"></jsp:useBean>
<jsp:useBean id="content" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"></jsp:useBean>
<jsp:useBean id="userMetadata" scope="request" type="java.util.Map"></jsp:useBean>
<jsp:useBean id="company" scope="request" type="au.net.webdirector.common.datalayer.client.CategoryData"></jsp:useBean>

<%
    Content conInfo = proposal.findGeneratedContentOfType("con-info");
    Content conCustomFields = proposal.findGeneratedContentOfType("con-custom-fields");
    AttachmentDetails attachment = null;
    Map<String,String> mp = new TreeMap<String, String>();
    mp.putAll(conInfo.getMetadata());

    request.setAttribute("page-break-before-always", true);
    if(conCustomFields != null)
        mp.putAll(TextLibraryService.formatCustomFields(conCustomFields.getMetadata(), company));

// String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
// Map<String, String> userFields = new UserAccountDataAccess().getUserMetadata(userId);

    TextDetails destinationText = new TextLibraryService().generateTextForView(
            content.getData(),
            userMetadata,
            mp,
            content.getMetadata() );

    String h2Split[] = destinationText.getData().split("<h2 class=\"section-head\"");
    String mainTitle[] = h2Split[0].split("\\r\\n|\\n|\\r");
    //String mainText[] = destinationText.getData().split("<h2 class=\"section-head\" thumb-id|</div>");
    String mainTextOld[] = destinationText.getData().split("<h2 class=\"section-head\" thumb-image|</div>");
    String[] mainText = Arrays.copyOfRange(mainTextOld, 1, mainTextOld.length);
    if (!mainText[0].contains("section-head")) {
    	String temp[] = new String[mainText.length-1];
    	for(int i = 0; i< mainText.length-1; i++)
    	{
    		temp[i] = mainText[i+1];
    	}
    	mainText = temp;
    }



	// inline image
	Boolean preview = (Boolean) request.getAttribute("preview");

    if (content.getId() != null && content.getId().equals("con-info") == false) {
        if(content.getAction() != null && content.getAction().equalsIgnoreCase("attachLibraryImageAction")) { //for image from the library
            try {
                ImageDetails imageDetails = new ImageLibraryService().loadImageById(content.getId(), request.getParameter("proposal"));
                Hashtable<String, String> ht = new Hashtable<String, String>();
                ht.put("Element_id", imageDetails.getId());
                ht.put("ATTRFILE_attachment", imageDetails.getPath());
                attachment = new AttachmentDetails(ht);
            } catch (NoLongerAvaliableException e) {
                // do nothing
            }
        } else {
            attachment = new ProposalAttachmentDataAccess().loadAttachmentById(content.getId());
        }

        int width;
        if(attachment==null){
            width = 0;
        }else if(preview != null) {
            width = ImageLibraryService.getImageWidthForPreview(content, attachment.getDownloadPath());
        } else {
            width = ImageLibraryService.getImageWidthForPdf(content, attachment.getDownloadPath());
        }
        String alignment = content.getMetadata().get("alignment");
        if(alignment == null || "".equals(alignment)) {
            alignment = "bottomwide";
        }
        if(alignment.equals("left") || alignment.equals("right") || alignment.equals("center")) { // for older proposals
            alignment = alignment.equals("left") ? "topleft-intext" : alignment.equals("right") ? "topright-intext" : "bottomwide";
        }
        String align = alignment.contains("left") ? "left" : alignment.contains("right") ?"right": "center";
        String pos = alignment.contains("top") ? "top":"bottom";
        String intext = String.valueOf(alignment.contains("intext"));

        content.getMetadata().put("align", align);
        content.getMetadata().put("pos", pos);
        content.getMetadata().put("intext", intext);

        content.getMetadata().put("width", Integer.toString(width));
        request.setAttribute("attachment", attachment);
    }
    
    int serverPort = request.getServerPort();
	String portString = "";
	if (serverPort != 80 && serverPort != 443)
	{
		portString = ":" + serverPort;
	}
	String server = "http://" + request.getServerName() + portString + request.getContextPath();
	
%>

<div class="editorArea" style="display: table;">
    <%=mainTitle[0]%>

    <% if (content.getId() != null && attachment != null) { %>
    <c:if test="${content.metadata.intext != 'true'}">
        <%-- <div style="width:100%; text-align: <c:out value="${content.metadata.align}" />"> --%>
        <p style="text-align: <c:out value="${content.metadata.align}" />">
            <img width="<c:out value="${content.metadata.width}" />"
                 src="/<c:out value="${initParam.storeContext}" /><c:out value="${attachment.encodedPath}" />"
            />
        </p>
        <!--  </div> -->
    </c:if>
    <c:if test="${content.metadata.intext== 'true'}">
        <img width="<c:out value="${content.metadata.width}" />"
             src="/<c:out value="${initParam.storeContext}" /><c:out value="${attachment.encodedPath}" />"
             style="float: <c:out value="${content.metadata.align}" />;
             <c:if test="${content.metadata.align == 'right'}">
                     margin-left: 10px;
             </c:if>
                     margin-right: 10px;"
        />
    </c:if>
    <% } %>
    <%String[] mainDescArray = new String[0];
     if (mainText.length > 1) {
         mainDescArray = mainText[0].split("</h2>");//\n
         /* if(mainDescArray.length > 2) {
         	mainDescArray = mainDescArray[2].split("<h3");
         } else {
         	mainDescArray = mainDescArray[1].split("</h2>|<h3");
         }

     } else {
         mainDescArray = mainText[0].split("class=\"section-head\">|</h2>\\n");
         if(mainDescArray.length > 2) {
             mainDescArray = mainDescArray[2].split("<h3");
         } else {
             mainDescArray = mainDescArray[1].split("</h2>");
         }*/
     } 
    if (!mainDescArray[0].contains("<p>")) {%>
     <%=mainDescArray[1]%>
    <% } else { %>
    <%=mainDescArray[0]%>
    <% } %>
    <%
    
    	int i = 0;
    
    if(preview==null) {
    	String title = mainTitle[0];
    	int beginIndex = title.indexOf(">") + 1;
    	title = title.substring(beginIndex, title.indexOf("<", beginIndex));
    	%><div class="page-break-before-on">
    		<h1><%=title + " Arrival Guide" %></h1>
    	<%
    }
    for (String textSub : mainText) {
    	i++;
    	if (textSub.contains("</h1>") || textSub.contains("City</h2>")) {
    		continue;
        }

    	//String sectionImageId = textSub.substring(2, textSub.indexOf("\"", 2));
    	String sectionImageUrl = textSub.substring(2, textSub.indexOf("\"", 2));
    	
        String textSub1[] = new String[0];
        textSub1 = textSub.split("</h2>");//\n
		System.out.println("sec: "+textSub1[0].substring(textSub1[0].indexOf(">")+1));
        if(preview!=null && preview){
		    if (textSub1.length < 2) {%>
		
		    <div class="collapse-card" style="border:1px solid #eee;margin: 10px 10px;">
		        <div class="collapse-card__heading">
		            <div class="collapse-card__title">
		                <h2 style="margin: 5px 0;"><%=textSub1[0].substring(textSub1[0].indexOf(">")+1)%></h2>
		            </div>
		        </div>
		        <div class="collapse-card__body" style="padding: 0px;">
		
		            <div style="padding: 1rem;">
		            <%=textSub1[1].replace(mainDescArray[0], "").replace("<h3<h3","<h3")%>
		            </div>
		        </div>
		    </div>
		
		    <% } else if (textSub1.length > 1) { %>
		    <div class="collapse-card" style="border:1px solid #eee;margin: 10px 10px;">
		    <div class="collapse-card__heading">
		        <div class="collapse-card__title">
		            <h2 style="margin: 5px 0;"><%=textSub1[0].substring(textSub1[0].indexOf(">")+1)%></h2>
		        </div>
		    </div>
		    <div class="collapse-card__body" style="padding: 0px;">
		
		        <div style="padding: 1rem;" thumb-image="<%=sectionImageUrl%>" class="section-head">
		       <%
		       		String poiContent = textSub1[1];
		       		String[] pois = poiContent.split("<h3");
		       %>
		       <%=pois[0]%>
		       <%
		       		for(int j=1; j<pois.length; j++){
		       			%>
		       				<div class="poi"><%="<h3" + pois[j] %></div>
		       				<div class="clear-both"></div>
		       			<%
		       		}
		       %>
		        </div>
		    </div>
		    </div>
		    <% }
        }
        else {
        	//AttachmentDetails thumb = new ProposalAttachmentDataAccess().loadAttachmentById(sectionImageId);
        	//request.setAttribute("thumb", thumb);
        	request.setAttribute("thumbImage", sectionImageUrl);
        	String email = request.getParameter("email");
        	if(StringUtils.isBlank(email)){
        		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
        		ElementData user = ModuleAccess.getInstance().getElementById(UserAccountDataAccess.USER_MODULE, userId);
        		email = user.getName();
        	}
        	
        	boolean mid = false;
        	if(i==3) {
        		mid = true;
        		i = 0;
        	}
        	%>
	        	<div class="thumb<%=mid?" thumb-center":""%>">
	        	<a href="<%=server%>/guide?proposal=<%=request.getParameter("proposal")%>&amp;thumb=<%=sectionImageUrl%>&amp;email=<%=email%>&amp;v-key=<%=request.getParameter("v-key")%>" target="_blank"> <img width="100%" src="<%=sectionImageUrl%>"></img> </a>
	        		<div class="thumb-desc"><%=textSub1[0].substring(textSub1[0].indexOf(">")+1)%><div class="read-more">More<i class="fa fa-chevron-right" aria-hidden="true"></i></div></div>
	        	</div>
        	<%
        }
    
    } 
    
    if(preview==null){
    	%>
    	</div>
    	<%
    }
    %>

</div>