<%@page import="org.apache.log4j.Logger"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.exception.NoLongerAvaliableException"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.ProposalAttachmentDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.AttachmentDetails"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.ImageDetails"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.TextLibraryService"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"%>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<jsp:useBean id="proposal" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"></jsp:useBean>
<jsp:useBean id="content" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"></jsp:useBean>
<jsp:useBean id="userMetadata" scope="request" type="java.util.Map"></jsp:useBean>
<jsp:useBean id="company" scope="request" type="au.net.webdirector.common.datalayer.client.CategoryData"></jsp:useBean>

<%
Content conInfo = proposal.findGeneratedContentOfType("con-info");
Content conCustomFields = proposal.findGeneratedContentOfType("con-custom-fields");
AttachmentDetails attachment = null;
Map<String,String> mp = new TreeMap<String, String>();
mp.putAll(conInfo.getMetadata());

	request.setAttribute("page-break-before-always", true);
if(conCustomFields != null)
	mp.putAll(TextLibraryService.formatCustomFields(conCustomFields.getMetadata(), company));

// String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
// Map<String, String> userFields = new UserAccountDataAccess().getUserMetadata(userId);

request.setAttribute("textData", new TextLibraryService().generateTextForView(
    content.getData(),
    userMetadata,
	mp,
    content.getMetadata() ));

// inline image
if (content.getId() != null && content.getId().equals("con-info") == false) { 
	if(content.getAction() != null && content.getAction().equalsIgnoreCase("attachLibraryImageAction")) { //for image from the library
		try {
			ImageDetails imageDetails = new ImageLibraryService().loadImageById(content.getId(), request.getParameter("proposal"));
			Hashtable<String, String> ht = new Hashtable<String, String>();
			ht.put("Element_id", imageDetails.getId());
			ht.put("ATTRFILE_attachment", imageDetails.getPath());
			attachment = new AttachmentDetails(ht);
		} catch (NoLongerAvaliableException e) {
			// do nothing
		}
	} else {
		attachment = new ProposalAttachmentDataAccess().loadAttachmentById(content.getId());
	}
	
	Boolean preview = (Boolean) request.getAttribute("preview");
	int width;
	if (conInfo.getMetadata().get("landscapeMode") != null) {
		if (conInfo.getMetadata().get("landscapeMode").equalsIgnoreCase("on")) {
			content.getMetadata().put("landscapeMode", "on");
		} else {
			content.getMetadata().put("landscapeMode", "off");
		}
	}
    if(attachment==null){
        width = 0;
    }else if(preview != null) {
		width = ImageLibraryService.getImageWidthForPreview(content, attachment.getDownloadPath());
	} else {
		width = ImageLibraryService.getImageWidthForPdf(content, attachment.getDownloadPath());
	}
	String alignment = content.getMetadata().get("alignment");
	if(alignment == null || "".equals(alignment)) {
		alignment = "bottomwide";
	}
	if(alignment.equals("left") || alignment.equals("right") || alignment.equals("center")) { // for older proposals
		alignment = alignment.equals("left") ? "topleft-intext" : alignment.equals("right") ? "topright-intext" : "bottomwide";
	}
	String align = alignment.contains("left") ? "left" : alignment.contains("right") ?"right": "center";
	String pos = alignment.contains("top") ? "top":"bottom";
	String intext = String.valueOf(alignment.contains("intext"));
	
	content.getMetadata().put("align", align);
	content.getMetadata().put("pos", pos);
	content.getMetadata().put("intext", intext);

	if (StringUtils.isNotBlank(content.getMetadata().get("size"))) {
		Double step1 = Double.parseDouble(content.getMetadata().get("size")) / 100;
		Double step2 = step1 * width;
		if (step2 > 701) {
			content.getMetadata().put("width", Integer.toString(700));
		} else {
			content.getMetadata().put("width", Integer.toString(step2.intValue()));
		}
	}
	request.setAttribute("attachment", attachment);
}
%>

<div class="editorArea">
	<c:if test="${content.metadata.pos != 'top'}">
		<c:out value="${textData.dataForPdf}" escapeXml="false"/>
	</c:if>
	<% if (content.getId() != null && attachment != null) { %>
		<c:if test="${content.metadata.intext != 'true'}">
		<%-- <div style="width:100%; text-align: <c:out value="${content.metadata.align}" />"> --%>
		  	<p style="text-align: <c:out value="${content.metadata.align}" />">
		  		<img width="<c:out value="${content.metadata.width}" />"
			       src="/<c:out value="${initParam.storeContext}" /><c:out value="${attachment.encodedPath}" />"
			    />
		  	</p>
	  <!--  </div> -->
	  </c:if>
	  <c:if test="${content.metadata.intext== 'true'}">
	  	<img width="<c:out value="${content.metadata.width}" />"
	       src="/<c:out value="${initParam.storeContext}" /><c:out value="${attachment.encodedPath}" />"
	       style="float: <c:out value="${content.metadata.align}" />;
	  			<c:if test="${content.metadata.align == 'right'}">
	       			margin-left: 10px;
	  			</c:if>
	       		margin-right: 10px;"
	    />
	  </c:if>
	<% } %>
  	<c:if test="${content.metadata.pos == 'top'}">
		<c:out value="${textData.dataForPdf}" escapeXml="false"/>
	</c:if>
  <div style="clear: both;"></div>
</div>