<%@page import="au.corporateinteractive.utils.WhatsNextUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.TextLibraryService"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.*"%>
<%@page import="java.util.*"%>
<jsp:useBean id="proposal" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"></jsp:useBean>
<jsp:useBean id="section" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Section"></jsp:useBean>

<%
 Integer index = (Integer)request.getAttribute("sectionIndex");
 String title = section.getGeneratedTitle();
 String spectrumColor = (String)request.getAttribute("spectrumColor");
%>
<div class="sectionDiv sectionWhatsNext">
	<div class="sectionContent">
	<h2 class="sectionTitle">What's Next?</h2>
		<%
		List<Content> sectionContent = section.getContent();
		if (sectionContent.size() > 0) {
			%>
			<table width="100%" class="whatsNextTable noBreakRow">
			<%
			for (int j = 0 ; j < sectionContent.size() ; ++j)
			{
				Content content = sectionContent.get(j);
				String text = WhatsNextUtils.detectIconTextForTextblock(content.getDataForPdf(), j);
				
				%>
				<tr>
				  <td width="140" align="center" style="padding-top: 43px;">
				     <% if (text!=null){
				        String imgSrc = (request.getContextPath() + "/application/images/"+spectrumColor+"/" + (j == 0 ? "accept.png" : "chat.png"));
				       %>
				        <i class="fa fa-check-square-o"></i><div class="whatsNextImageText"><%=text%></div>
				       <% } %>
				    </td>
				 <td><div class="contentDiv">
				<%
				
				request.setAttribute("content", content);
				request.setAttribute("sectionTitle", title);
				request.setAttribute("contentIndex", j);
	
				if (content.getType().equals("text"))
				{
				%>
				  <jsp:include page="/application/pdf/section/fragment-text.jsp"></jsp:include>
				<%
				}
				else if (content.getType().equals("editor"))
				{
				%>
				  <jsp:include page="/application/pdf/section/fragment-editor.jsp"></jsp:include>
				<%
				}
				%></div>
				 </td>
				</tr>
				<%
			}
			 %>
			</table>
			<%
		}
		%>
	</div>
</div>
