<%@page import="au.corporateinteractive.qcloud.proposalbuilder.exception.NoLongerAvaliableException"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.PDFDetails"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.AttachmentDetails"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.ProposalAttachmentDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.PDFLibraryService"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService"%>
<%@page import="java.util.Map"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"%>
<jsp:useBean id="proposal" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"></jsp:useBean>
<jsp:useBean id="content" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"></jsp:useBean>

<%
    try
    {
	PDFDetails pdfDetails = new PDFLibraryService().loadPDFById(content.getId(), request.getParameter("proposal"));
	request.setAttribute("pdfData", pdfDetails);
    Map<String, String> metadata = content.getMetadata();

        request.setAttribute("page-break-before-always", true);
%>
<div class="noBreak">
  
  <h3><%= ESAPI.encoder().encodeForHTML(metadata.get("title")) %></h3>
  <div class="">
  	<table style="width: 100%;">
  	<tbody>
  	<tr>
  	<td style="width: 35%;">
    <div class="downloadCell">
      <a target="_blank" class="attachment" 
         href="<%=request.isSecure()?"https://":"http://"%><%=request.getServerName()%>:<%=request.getServerPort()%>/<c:out value="${initParam.storeContext}" /><c:out value="${pdfData.data}" />">
        <div class="downloadDiv">
        </div>  
        <div class="lower"> 
          <span>${SYSTEM_MESSAGE_ATTACHMENT_CLICK_HERE}</span>
        </div>
      </a>
    </div>
    </td>
    <td>
    <div class="col-sm-8">
      <div class="downloadDesc">
      </div>
    </div>
    </td>
    </tr>
    </tbody>
    </table>
    
    <div class="col-sm-8" style="padding-left:0px; padding-top: 6px">
      <div>
      	<%= ESAPI.encoder().encodeForHTML(metadata.get("description"))%>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>

<%
    }
    catch (NoLongerAvaliableException e)
    {
    }
%>
