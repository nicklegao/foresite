<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.Map"%>
<%@page import="webdirector.db.client.ClientDataAccess"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Section"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="java.util.Hashtable"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess" %>
<%@ page import="java.util.List" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
  Hashtable<String, String> proposal = (Hashtable<String, String>)request.getAttribute("proposalHashtable");
  
  Section section = (Section) request.getAttribute("section");
  
  boolean landscapeMode = false;
  for (Content c : section.getContent())
	{
		if (c.getType().equalsIgnoreCase("generated")
				&& "con-info".equals(c.getId()))
		{

			if (c.getMetadata().get("landscapeMode") != null) {
				landscapeMode = StringUtils.equals("on", c.getMetadata().get("landscapeMode"));
				break;
			}
		}
	}
  Content summary = section.getFirstContentByType("editor");
  String spectrumColor = (String)request.getAttribute("spectrumColor");

  if (proposal == null) {
	  String proposalId = request.getParameter("proposal");
	  proposal = new TravelDocsDataAccess().getTravelDocById(proposalId, true);
	  TravelDocsDataAccess.addFormattedDates(proposal);
  }

  TravelDocsDataAccess.addFormattedDates(proposal);
  String proposalId = proposal.get(TravelDocsDataAccess.E_ID);
	String proposalTitle = proposal.get(TravelDocsDataAccess.E_TRAVELDOC_TITLE);
	String travelReference = proposal.get(TravelDocsDataAccess.E_ITINERARY_REFERENCE);
  String clientName = proposal.get(TravelDocsDataAccess.E_CLIENT_NAME) + " " + proposal.get(TravelDocsDataAccess.E_CLIENT_LAST_NAME);
  String modifiedDate = proposal.get(TravelDocsDataAccess.MODIFIED_FOR_DISPLAY);
  String clientCompany = proposal.get(TravelDocsDataAccess.E_CLIENT_COMPANY);
  
  UserAccountDataAccess uada = new UserAccountDataAccess();
  CategoryData company = uada.getCompanyForCompanyId(proposal.get(TravelDocsDataAccess.E_OWNER));
	List<Hashtable<String, String>> users = uada.getUsersByCompanyId(company.getId());
	Hashtable<String, String> consultantHT = users.get(0);

  request.setAttribute("userMetadata", uada.getUserMetadata(consultantHT.get(UserAccountDataAccess.E_ID)));
  
  
  String consultantFirstName = consultantHT.get(UserAccountDataAccess.E_NAME);
  String consultantSurname = consultantHT.get(UserAccountDataAccess.E_SURNAME);
  String consultantPhone = consultantHT.get(UserAccountDataAccess.E_MOBILE_NUMBER);
  String consultantEmail = consultantHT.get(UserAccountDataAccess.E_EMAIL_ADDRESS);
  String companyName = company.getName();

  boolean showCoverpage = !company.getBoolean(uada.C1_PROPOSAL_HIDE_COVERPAGE);
  boolean hideCoverProposalInfo = company.getBoolean(uada.C1_PROPOSAL_HIDE_COVERPAGE_PROPOSAL_INFO);
  boolean hideCoverSenderInfo = company.getBoolean(uada.C1_PROPOSAL_HIDE_COVERPAGE_SENDER_INFO);
  boolean hideCoverBoxBackground = company.getBoolean(uada.C1_PROPOSAL_HIDE_COVERPAGE_BOX_BACKGROUND);
  
  if(showCoverpage){
%>
<div class="cover" <%if(landscapeMode){%>style="height:793px !important; width:1122px !important;"<%}%>>
    <div class="placeHolder">
    </div>
    <div class="coverpageSpectrum"></div>
   	<%if(!hideCoverProposalInfo){ %>
    <div class="proposalTitle" <%if(landscapeMode){%>style="top:347px;"<%}%>>
	    <div class="proposalCompany"><c:out value="<%=clientCompany%>" escapeXml="true"/></div>
		<% if (travelReference != null && travelReference.length() > 0)
		{
			proposalTitle = proposalTitle + " (#" + travelReference + ")";
		}%>

		<div class="proposalName"><c:out value="<%=proposalTitle%>" escapeXml="true"/></div>
    </div>
    <%}
   	if(!hideCoverSenderInfo){%>
    <div class="coverData" <%if(landscapeMode){%>style="top:541px;"<%}%>>
	    <div class="proposalInfo coverSection">
	        <div class="client coverTopLeft level1">
	            <div class="coverTitle">${TEXT_COVER_PREPARED_FOR}:</div>
	            <div><c:out value="<%=clientName%>" escapeXml="true"/></div>
	        </div>
	        <div class="proposalDate coverTopMiddle level1">
	            <div class="coverTitle">${TEXT_COVER_DATE}:</div>
	            <div><%=modifiedDate%></div>
	        </div>
	        <div class="proposalDate coverTopRight level1">
	            <div class="coverTitle">${TEXT_COVER_REFERENCE}:</div>
	            <div><%=proposalId %></div>
	        </div>
	    </div>
	    
	    <div class="authorInfo coverSection">
	        <div class="author coverLeft level2">
	            <div class="coverTitle">
	                ${TEXT_COVER_PREPARED_BY}:
	            </div>
	            <div>
	                <c:out value="<%=consultantFirstName%>" escapeXml="true"/> <c:out value="<%=consultantSurname%>" escapeXml="true"/>
	            </div>
	            <div>
	                <c:out value="<%=companyName%>" escapeXml="true"/>
	            </div>
	        </div>
	        <div class="contact coverRight level2">
	            <div class="coverTitle">
	                &nbsp;
	            </div>
	            <div>
	            	
	                <i class="fa fa-phone"></i>&nbsp;<c:out value="<%=consultantPhone%>" escapeXml="true"/>
	            </div>
	            <div>
	               <i class="fa fa-envelope"></i>&nbsp;<c:out value="<%=consultantEmail%>" escapeXml="true"/> 
	            </div>
	            <div>&nbsp;</div>
	        </div>
	    </div>

	  </div>
      <%} %>
</div>
<%}%>