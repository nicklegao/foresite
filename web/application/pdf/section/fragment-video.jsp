<%@page import="java.util.Map" %>
<%@page import="java.util.TreeMap" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.exception.NoLongerAvaliableException" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.AttachmentDetails" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.service.TextLibraryService" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.service.VideoLibraryService" %>
<jsp:useBean id="proposal" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"></jsp:useBean>
<jsp:useBean id="content" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"></jsp:useBean>
<jsp:useBean id="userMetadata" scope="request" type="java.util.Map"></jsp:useBean>

<%
    if (content.getMetadata() != null)
    {
        Content conInfo = proposal.findGeneratedContentOfType("con-info");

        AttachmentDetails attachment = null;
        Map<String, String> mp = new TreeMap<String, String>();
        mp.putAll(conInfo.getMetadata());
        try
        {
            request.setAttribute("videoData", new VideoLibraryService().loadVideoById(content.getId(), request.getParameter("proposal")));

            request.setAttribute("page-break-before-always", true);

            request.setAttribute("textData", new TextLibraryService().generateTextForView(
                    content.getData(),
                    userMetadata,
                    mp,
                    content.getMetadata()));

            String size = content.getMetadata().get("size");
            String width = Integer.toString(360);
            if (size.equalsIgnoreCase("small"))
            {
                width = "45%";
            }
            else if (size.equalsIgnoreCase("medium"))
            {
                width = "60%";
            }
            else if (size.equalsIgnoreCase("wide"))
            {
                width = "90%";
            }
            String height = Integer.toString(270);
            if (size.equalsIgnoreCase("small"))
            {
                height = "230";
            }
            else if (size.equalsIgnoreCase("medium"))
            {
                height = "270";
            }
            else if (size.equalsIgnoreCase("wide"))
            {
                height = "320";
            }

            String alignment = content.getMetadata().get("alignment");
            if (alignment == null || "".equals(alignment))
            {
                alignment = "bottomwide";
            }
            if (alignment.equals("left") || alignment.equals("right") || alignment.equals("center"))
            { // for older proposals
                alignment = alignment.equals("left") ? "topleft-intext" : alignment.equals("right") ? "topright-intext" : "bottomwide";
            }
            String align = alignment.contains("left") ? "left" : alignment.contains("right") ? "right" : "center";
            String pos = alignment.contains("top") ? "top" : "bottom";
            String intext = String.valueOf(alignment.contains("intext"));

            if (size.equalsIgnoreCase("wide"))
            {
                align = "center";
            }
            content.getMetadata().put("align", align);
            content.getMetadata().put("pos", pos);
            content.getMetadata().put("intext", intext);

            content.getMetadata().put("width", width);
            content.getMetadata().put("height", height);
            request.setAttribute("attachment", attachment);
%>


<div class="editorArea">
    <c:if test="${content.metadata.pos != 'top'}">
        <c:out value="${textData.dataForPdf}" escapeXml="false"/>
    </c:if>
    <c:if test="${content.metadata.intext != 'true'}">
        <%-- <div style="width:100%; text-align: <c:out value="${content.metadata.align}" />"> --%>
        <% if ((size.equalsIgnoreCase("wide") || align.equalsIgnoreCase("center")) || align.equalsIgnoreCase("right"))
        {
        if (align.equalsIgnoreCase("center")) {%>
        <div style="text-align: center;">
            <% } else { %>
            <div>
            <% } %>
        <a class="videoDiv" href="<c:out value="${videoData.link}"  />" style="width: 100%;height: <c:out value="${content.metadata.height}"/>px;
        <c:if test="${content.metadata.align == 'right'}">
                margin-left: 10px;
        </c:if>
                margin-right: 10px;">

            <% String margin = "margin-left: 10px;float:left;";
                String playMargin = "";
                if (align.equalsIgnoreCase("center"))
                {
                    margin = "margin:auto";
                    playMargin = "left: 45%;";
                }
                else if (align.equalsIgnoreCase("right"))
                {
                    margin = "margin-right: 10px; float:right;";
                    playMargin = "left: 70%;";
                } %>
            <img class="videoImage" width="<c:out value="${content.metadata.width}" />" height="<c:out value="${content.metadata.height}" />px" src="<c:out value="${videoData.thumbnail}" />" style="<%=margin%>"/>

            <i class="fa fa-youtube-play playButton" style="<%=playMargin%>"></i></a>
        <iframe width="<c:out value="${content.metadata.width}" />" height="${content.metadata.height}" src="<c:out value="${videoData.source}" />" frameborder="0" allowfullscreen="allowfullscreen" style="float: <c:out value="${content.metadata.align}"/>;
        <c:if test="${content.metadata.align == 'right'}">
                margin-left: 10px;
        </c:if>
                margin-right: 10px;"></iframe>
        </div>
        <% }
        else
        { %>
        <p style="text-align: <c:out value="${content.metadata.align}" />">
            <a class="videoDiv" style="width:<c:out value="${content.metadata.width}"/>;height: <c:out value="${content.metadata.height}"/>;" href="<c:out value="${videoData.link}"/>">
                <img class="videoImage" width="100%" height="${content.metadata.height}" src="<c:out value="${videoData.thumbnail}" />"/>
                <i class="fa fa-youtube-play playButton"></i></a>
            <iframe width="<c:out value="${content.metadata.width}" />" height="${content.metadata.height}" src="<c:out value="${videoData.source}" />" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
        </p>
        <% } %>
        <!-- </div> -->
    </c:if>
    <c:if test="${content.metadata.intext == 'true'}">
        <a class="videoDiv" href="<c:out value="${videoData.link}"  />" style="width: <c:out value="${content.metadata.width}"/>;height: <c:out value="${content.metadata.height}"/>; float: <c:out value="${content.metadata.align}"/>;
        <c:if test="${content.metadata.align == 'right'}">
                margin-left: 10px;
        </c:if>
                margin-right: 10px;">
            <img class="videoImage" width="100%" height="${content.metadata.height}" src="<c:out value="${videoData.thumbnail}" />" style="float: <c:out value="${content.metadata.align}"/>;
            <c:if test="${content.metadata.align == 'right'}">
                    margin-left: 10px;
            </c:if>
                    margin-right: 10px;"/>

            <i class="fa fa-youtube-play playButton"></i></a>
        <iframe width="<c:out value="${content.metadata.width}" />" height="${content.metadata.height}" src="<c:out value="${videoData.source}" />" frameborder="0" allowfullscreen="allowfullscreen" style="float: <c:out value="${content.metadata.align}"/>;
        <c:if test="${content.metadata.align == 'right'}">
                margin-left: 10px;
        </c:if>
                margin-right: 10px;"></iframe>

    </c:if>
    <c:if test="${content.metadata.pos == 'top'}">
        <c:out value="${textData.dataForPdf}" escapeXml="false"/>
    </c:if>
    <div style="clear: both;"></div>
</div>
<%
    }
    catch (NoLongerAvaliableException e)
    {
    }
}
else
{ %>

<%
    try
    {
        request.setAttribute("videoData", new VideoLibraryService().loadVideoById(content.getId(), request.getParameter("proposal")));

        request.setAttribute("page-break-before-always", true);
%>
<div class="noBreak videoHolder">
    <table class="videoTable" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="middle">
                <div class="videoContent">
                    <h3 class="videoTitle"><c:out value="${videoData.title}"/></h3>
                    <p class="videoText">
                        <c:out value="${videoData.caption}"/>
                    </p>
                </div>
            </td>
            <td class="videoCell" valign="top"><a class="videoDiv" href="<c:out value="${videoData.link}" />"><img class="videoImage" width="360" height="270" src="<c:out value="${videoData.thumbnail}" />"/>
                <i class="fa fa-youtube-play playButton"></i></a>
                <iframe width="360" height="270" src="<c:out value="${videoData.source}" />" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
            </td>
        </tr>
    </table>
</div>
<%
    }
    catch (NoLongerAvaliableException e)
    {
    }
%>

<% }

%>