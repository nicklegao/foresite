<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.ImageDetails"%>
<%@page import="java.io.File"%>
<%@page import="javax.imageio.ImageIO"%>
<%@page import="java.awt.image.BufferedImage"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.exception.NoLongerAvaliableException"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"%>
<jsp:useBean id="proposal" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"></jsp:useBean>
<jsp:useBean id="content" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"></jsp:useBean>

<%

try
{
	ImageDetails imageDetails = new ImageLibraryService().loadImageById(content.getId(), request.getParameter("proposal"));
	request.setAttribute("imageData", imageDetails);
	
	int width = ImageLibraryService.getImageWidthForPdf(content, imageDetails.getPath());

	request.setAttribute("page-break-before-always", true);
%>
	<div>
	<c:if test="${imageData.displayTitle}">
  		 <h3 style="text-align: <c:out value="${content.metadata.alignment}" />"><c:out value="${imageData.title}" /></h3>
	</c:if>
	 
	  
	  <c:if test="${content.metadata.alignment == 'center'}">
	  	<p style="text-align: <c:out value="${content.metadata.alignment}" />">
	  		<img alt="<c:out value="${imageData.caption}" />"
		  	   width="<%= width %>"
		       src="/<c:out value="${initParam.storeContext}" /><c:out value="${imageData.encodedPath}" />" 
		    />
	  	</p>
	  </c:if>
	  <c:if test="${content.metadata.alignment != 'center'}">
	  	<img alt="<c:out value="${imageData.caption}" />"
	  	   width="<%= width %>"
	       src="/<c:out value="${initParam.storeContext}" /><c:out value="${imageData.encodedPath}" />"
	       style="float: <c:out value="${content.metadata.alignment}" />; margin-right: 15px;"
	    />
	  </c:if>
	  
	  ${imageData.description}
	  <div style="clear: both;"></div>
	</div>
<%
}
catch (NoLongerAvaliableException e)
{
}
%>
