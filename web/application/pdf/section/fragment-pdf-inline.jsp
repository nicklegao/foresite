<%@page import="java.io.File"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="au.corporateinteractive.qcloud.pdf.service.PdfService2"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.exception.NoLongerAvaliableException"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.PDFDetails"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.AttachmentDetails"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.ProposalAttachmentDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.PDFLibraryService"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService"%>
<%@page import="java.util.Map"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"%>
<%@ page import="au.net.webdirector.common.datalayer.client.ModuleHelper" %>
<%@ page import="au.net.webdirector.common.datalayer.client.ElementData" %>
<jsp:useBean id="proposal" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"></jsp:useBean>
<jsp:useBean id="content" scope="request" type="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"></jsp:useBean>

<%
    try {
        Map<String, String> metadata = content.getMetadata();
        ElementData newItem = new ElementData();
        String rawURL = "";
        int pageNumber = 0;
        if (Boolean.valueOf(metadata.get("attached"))) {

            AttachmentDetails attach = new ProposalAttachmentDataAccess().loadAttachmentById(content.getId());
            rawURL = attach.getDownloadPath();
            File existingFile = new File(Defaults.getInstance().getStoreDir(), rawURL + "-0.png.encrypted");
            if (!existingFile.exists()) {
                PdfService2.generateSnapShots(new File(Defaults.getInstance().getStoreDir(), rawURL));
            }
            pageNumber = PdfService2.getPageNumber(new File(Defaults.getInstance().getStoreDir(), rawURL));
            pageContext.setAttribute("pageNumber", pageNumber);

        } else {
            PDFDetails pdfDetails = new PDFLibraryService().loadPDFById(content.getId(), request.getParameter("proposal"));
            request.setAttribute("pdfData", pdfDetails);
            pageNumber = PdfService2.getPageNumber(new File(Defaults.getInstance().getStoreDir(), pdfDetails.getData()));
            pageContext.setAttribute("pageNumber", pageNumber);
        }

%>
<div class="noBreak">
  	
  <div style="text-align:center;padding-top:12px;">
    <c:choose>
      <c:when test = "${proposalMode == 'pdf'}">
        <% for (int i = 1 ; i <= pageNumber ; i++) {
        	if ((request.getAttribute("last-page-nobreak") == null) || (i != pageNumber)){%>
                <div style="page-break-after:always;">
            <% } else { %>
                <div>
            <% } %>
                #THIS IS A PAGE PLACEHOLDER#
                </div> <%
            }
            request.setAttribute("page-break-after-always", true);
            request.setAttribute("page-break-before-always", true);
        %>
      </c:when>
      <c:otherwise>
       <c:forEach var = "i" begin = "0" end = "${pageNumber-1}">
          <c:choose>
              <c:when test="${i==0}">
                <c:set var = "extraStyle" value = ""/>
              </c:when>    
              <c:otherwise>
                <c:set var = "extraStyle" value = "border-top: lightgrey 1px dotted;"/>
              </c:otherwise>
          </c:choose>
          <%--
          <div class="row" style="padding:15px 0 15px 0;background-color:#f7f7f7;">
           --%>
          <div class="row" style="padding:15px 10px 15px 10px;${extraStyle}">
              <% if (Boolean.valueOf(metadata.get("attached"))) { %>
              <img src="/<c:out value="${initParam.storeContext}" /><%=rawURL%>-<c:out value="${i}" />.png.encrypted" style="width:100%"/>
             <% } else { 
             	
            	 %>
              <img src="/<c:out value="${initParam.storeContext}" /><c:out value="${pdfData.data}" />-<c:out value="${i}" />.png.encrypted" onerror="console.log('error use alt');this.src='/<c:out value="${initParam.storeContext}" /><c:out value="${pdfData.data}" />-<c:out value="${i}" />.png'" style="width:100%"/>
              <% } %>
          </div>
        </c:forEach>
        <%--
        <div class="row moreDetail noBreak">
			<% if (metadata.get("title").length() > 0) {%>
			<p><strong><%= metadata.get("title") %></strong></p>
			<% } else { %>
			<p><strong>${pdfData.title}</strong></p>
			<% } %>
			<% if (metadata.get("description").length() > 0) {%>
			<span><%= metadata.get("description") %></span>
			<% } else { %>
			<span>${pdfData.description}</span>
			<% } %>
		</div>
         --%>
      </c:otherwise>
    </c:choose>
  </div>
</div>


<%
    }
    catch (NoLongerAvaliableException e)
    {
    }
%>