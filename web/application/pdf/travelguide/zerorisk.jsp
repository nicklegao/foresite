<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.control.TravelWatchController" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.service.TravelWatchService" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.GregorianCalendar" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelGuideDataAccess" %>
<%@ page import="javax.mail.Store" %>
<%@ page import="au.corporateinteractive.qcloud.market.model.travel.ItineraryStyle" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="au.net.webdirector.common.datalayer.base.db.entity.StoreFile" %>
<%@ page import="au.net.webdirector.common.datalayer.base.db.entity.TextFile" %>
<%@ page import="com.google.gson.internal.StringMap" %>

<%

    if (!StringUtil.isNullOrEmpty(travelGuideID))  {
        TravelGuideDataAccess tgda = new TravelGuideDataAccess();
        StoreUtils stores = new StoreUtils();
        Gson gson = new Gson();
        ElementData travelSegments = tgda.getTravelWatchElementByID(travelGuideID);
        TextFile file = travelSegments.getTextFile(TravelGuideDataAccess.E_JSON_DATA);
        if (file == null) {
            return;
        }
        String contence = file.getContent();
        Map<String, Object> segment = gson.fromJson(contence, Map.class);
            StringMap countryInfo = (StringMap) segment.get("country");
            List<StringMap> travelArray = (List<StringMap>) segment.get("travel");
            List<StringMap> terrorismArray = (List<StringMap>) segment.get("terrorism");
            List<StringMap> crimeArray = (List<StringMap>) segment.get("crime");
            List<StringMap> dateArray = (List<StringMap>) segment.get("date");
            List<StringMap> weatherArray = (List<StringMap>) segment.get("weather");
            List<StringMap> demonstrationsArray = (List<StringMap>) segment.get("demonstrations");
            List<StringMap> healthArray = (List<StringMap>) segment.get("health");
            List<StringMap> emergencyProtocolArray = (List<StringMap>) segment.get("emergencyProtocol");
            if (countryInfo != null) {
%>
<div class="zerorisk-country-container" style="display: inline;">
    <div class="title-container">
        <div class="row">
            <div class="col-12" style="text-transform: uppercase;text-align: center;">
                <h1 style="margin: 0px;"><%=countryInfo.get("attr_categoryname")%></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="sub-title" style="text-align: center;">
                    <span>ZeroRisk International Advice</span>
                </div>
            </div>
        </div>
    </div>
    <%
        if (!StringUtil.isNullOrEmpty((String) countryInfo.get("attr_assessmentdescription"))) {
    %>
    <div class="country-description-container" style="border: 1px solid;border-radius: 4px;margin: 10px 10px;">
        <div class="country-description" style="padding: 10px 20px;">
            <div class="row">
                <div class="col-1" style="text-align: center;">
                    <div class="status-icon">
                        <i class="fas fa-3x fa-exclamation-triangle"></i>
                        <%
                            String levelCode = (String) countryInfo.get("attrdrop_assessmentalertlevel");
                            String level = "";
                            switch (levelCode.toLowerCase()) {
                                case "l":
                                    level = "Low";
                                    break;
                                case "l-m":
                                    level = "Low to Medium";
                                    break;
                                case "m":
                                    level = "Medium";
                                    break;
                                case "m-h":
                                    level = "Medium to High";
                                    break;
                                case "h":
                                    level = "High";
                                    break;
                                default:
                                    level = "Low";
                                    break;
                            }
                        %>
                        <span class="status-icon-text" style="display: block; line-height: 1; margin: 0; padding-top: 3px; text-transform: capitalize; letter-spacing: 0;"><%=level%></span>
                    </div>
                </div>
                <div class="col-11">
                    <span style="font-size: 12px;"><%=countryInfo.get("attr_assessmentdescription")%></span>
                </div>
            </div>
        </div>
    </div>

    <%
        }
        if (travelArray != null && travelArray.size() > 0) {
            for (Integer t = 0; t < travelArray.size(); t++) {
                StringMap element = travelArray.get(t);
    %>
    <div class="travel-container" style="margin: 20px 10px 0 10px;">
        <div class="row">
            <div class="col-8" style="padding-right: 0px;">
                <div class="travel-section-title">
                    <h3>Local Travel Information</h3>
                </div>
            </div>
            <div class="col-3" style="padding-right: 0px;text-align: right">
                <%
                    String levelCode = (String) element.get("attrdrop_assessmentalertlevel");
                    String level = "";
                    switch (levelCode.toLowerCase()) {
                        case "l":
                            level = "Low";
                            break;
                        case "l-m":
                            level = "Low to Medium";
                            break;
                        case "m":
                            level = "Medium";
                            break;
                        case "m-h":
                            level = "Medium to High";
                            break;
                        case "h":
                            level = "High";
                            break;
                        default:
                            level = "Low";
                            break;
                    }
                %>
                <%=level%>
            </div>
        </div>
        <div class="travel-info-container" style="margin: 20px 0px;">
            <div class="row">
                <div class="col">
                    <div class="travel-info-text"><%=((String) element.get("attrlong_description")).replaceAll("<br>", "<br></br>")%></div>
                </div>
            </div>
        </div>
    </div>
    <%
            }
        }
    %>
    <%
        if (terrorismArray != null && terrorismArray.size() > 0) {
            for (Integer t = 0; t < terrorismArray.size(); t++) {
                StringMap element = (StringMap) terrorismArray.get(t);
    %>
    <div class="terrorism-container" style="margin: -10px 10px;">
        <div class="row">
            <div class="col-8" style="padding-right: 0px;">
                <div class="travel-section-title">
                    <h3>Terrorism Threat</h3>
                </div>
            </div>
            <div class="col-3" style="padding-right: 0px;text-align: right">
                <%
                    String levelCode = (String) element.get("attrdrop_assessmentalertlevel");
                    String level = "";
                    switch (levelCode.toLowerCase()) {
                        case "l":
                            level = "Low";
                            break;
                        case "l-m":
                            level = "Low to Medium";
                            break;
                        case "m":
                            level = "Medium";
                            break;
                        case "m-h":
                            level = "Medium to High";
                            break;
                        case "h":
                            level = "High";
                            break;
                        default:
                            level = "Low";
                            break;
                    }
                %>
                <%=level%>
            </div>
        </div>
        <div class="terrorism-info-container" style="margin: 0px 0px;">
            <div class="row">
                <div class="col">
                    <div class="terrorism-info-text"><%=((String) element.get("attrlong_description")).replaceAll("<br>", "<br></br>")%></div>
                </div>
            </div>
        </div>
    </div>
    <%
            }
        }
    %>
    <%
        if (crimeArray != null && crimeArray.size() > 0) {
            for (Integer t = 0; t < crimeArray.size(); t++) {
                StringMap element = crimeArray.get(t);
    %>
    <div class="crime-container" style="margin: -10px 10px;">
        <div class="row">
            <div class="col-8" style="padding-right: 0px;">
                <div class="travel-section-title">
                    <h3>Local Crime Information</h3>
                </div>
            </div>
            <div class="col-3" style="padding-right: 0px;text-align: right">
                <%
                    String levelCode = (String) element.get("attrdrop_assessmentalertlevel");
                    String level = "";
                    switch (levelCode.toLowerCase()) {
                        case "l":
                            level = "Low";
                            break;
                        case "l-m":
                            level = "Low to Medium";
                            break;
                        case "m":
                            level = "Medium";
                            break;
                        case "m-h":
                            level = "Medium to High";
                            break;
                        case "h":
                            level = "High";
                            break;
                        default:
                            level = "Low";
                            break;
                    }
                %>
                <%=level%>
            </div>
        </div>
        <div class="crime-info-container" style="margin: 0px 0px;">
            <div class="row">
                <div class="col">
                    <div class="crime-info-text"><%=((String) element.get("attrlong_description")).replaceAll("<br>", "<br></br>")%></div>
                </div>
            </div>
        </div>
    </div>
    <%
            }
        }
    %>
    <%
        if ((dateArray != null && dateArray.size() > 0) || (demonstrationsArray != null && demonstrationsArray.size() > 0)) {
    %>
    <div class="date-container" style="margin: -10px 10px;">
        <div class="row">
            <div class="col-12" style="padding-right: 0px;">
                <div class="travel-section-title">
                    <h3>Anniversaries &amp; Demonstrations</h3>
                </div>
            </div>
        </div>
        <%
            for (Integer t = 0; t < dateArray.size(); t++) {
                StringMap element = dateArray.get(t);
        %>
        <div class="date-info-container" style="margin: 10px 0px;">
            <div class="row">
                <div class="col">
                    <div class="date-info-text"><%=((String) element.get("attrlong_description")).replaceAll("<br>", "<br></br>")%></div>
                </div>
            </div>
        </div>
        <%
            }
        %>
        <%
            for (Integer t = 0; t < demonstrationsArray.size(); t++) {
                StringMap element = demonstrationsArray.get(t);
        %>
        <div class="demonstrations-info-container" style="margin: 10px 0px;">
            <div class="row">
                <div class="col">
                    <div class="demonstrations-info-text"><%=((String) element.get("attrlong_description")).replaceAll("<br>", "<br></br>")%></div>
                </div>
            </div>
        </div>
        <%
            }
        %>
    </div>
    <%
        }
    %>
    <%
        if (weatherArray != null && weatherArray.size() > 0) {
            for (Integer t = 0; t < weatherArray.size(); t++) {
                StringMap element = weatherArray.get(t);
    %>
    <div class="weather-container" style="margin: -10px 10px;">
        <div class="row">
            <div class="col-8" style="padding-right: 0px;">
                <div class="weather-section-title">
                    <h3>Weather &amp; Disaster Information</h3>
                </div>
            </div>
            <div class="col-3" style="padding-right: 0px;text-align: right">
                <%
                    String levelCode = (String) element.get("attrdrop_assessmentalertlevel");
                    String level = "";
                    switch (levelCode.toLowerCase()) {
                        case "l":
                            level = "Low";
                            break;
                        case "l-m":
                            level = "Low to Medium";
                            break;
                        case "m":
                            level = "Medium";
                            break;
                        case "m-h":
                            level = "Medium to High";
                            break;
                        case "h":
                            level = "High";
                            break;
                        default:
                            level = "Low";
                            break;
                    }
                %>
                <%=level%>
            </div>
        </div>
        <div class="weather-info-container" style="margin: 20px 0px;">
            <div class="row">
                <div class="col">
                    <div class="weather-info-text"><%=((String) element.get("attrlong_description")).replaceAll("<br>", "<br></br>")%></div>
                </div>
            </div>
        </div>
    </div>
    <%
            }
        }
    %>
    <%
        if (healthArray != null && healthArray.size() > 0) {
            for (Integer t = 0; t < healthArray.size(); t++) {
                StringMap element = healthArray.get(t);
    %>
    <div class="health-container" style="margin: -10px 10px;">
        <div class="row">
            <div class="col-8" style="padding-right: 0px;">
                <div class="weather-section-title">
                    <h3>Health &amp; Vaccine Information</h3>
                </div>
            </div>
            <div class="col-3" style="padding-right: 0px;text-align: right">
                <%
                    String levelCode = (String) element.get("attrdrop_assessmentalertlevel");
                    String level = "";
                    switch (levelCode.toLowerCase()) {
                        case "l":
                            level = "Low";
                            break;
                        case "l-m":
                            level = "Low to Medium";
                            break;
                        case "m":
                            level = "Medium";
                            break;
                        case "m-h":
                            level = "Medium to High";
                            break;
                        case "h":
                            level = "High";
                            break;
                        default:
                            level = "Low";
                            break;
                    }
                %>
                <%=level%>
            </div>
        </div>
        <div class="health-info-container" style="margin: 20px 0px;">
            <div class="row">
                <div class="col">
                    <div class="health-info-text"><%=((String) element.get("attrlong_description")).replaceAll("<br>", "<br></br>")%></div>
                </div>
            </div>
        </div>
    </div>
    <%
            }
        }
    %>
    <%
        if (emergencyProtocolArray != null && emergencyProtocolArray.size() > 0) {
            for (Integer t = 0; t < emergencyProtocolArray.size(); t++) {
                StringMap element = emergencyProtocolArray.get(t);
    %>
    <div style="page-break-after: always;"></div>
    <div class="emergencyProtocol-container" style="margin: 20px 10px;">
        <div class="row">
            <div class="col-8" style="padding-right: 0px;">
                <div class="weather-section-title">
                    <h3>Critical Emergency Protocol</h3>
                </div>
            </div>
            <div class="col-3" style="padding-right: 0px;text-align: right">
                <%
                    String levelCode = (String) element.get("attrdrop_assessmentalertlevel");
                    String level = "";
                    switch (levelCode.toLowerCase()) {
                        case "l":
                            level = "Low";
                            break;
                        case "l-m":
                            level = "Low to Medium";
                            break;
                        case "m":
                            level = "Medium";
                            break;
                        case "m-h":
                            level = "Medium to High";
                            break;
                        case "h":
                            level = "High";
                            break;
                        default:
                            level = "Low";
                            break;
                    }
                %>
                <%=level%>
            </div>
        </div>
        <div class="emergencyProtocol-info-container" style="margin: 0px 0px;">
            <div class="row">
                <div class="col">
                    <div class="emergencyProtocol-info-text"><%=((String) element.get("attrlong_description")).replaceAll("<br>", "<br></br>")%></div>
                </div>
            </div>
        </div>
    </div>
    <%
            }
        }
    %>
</div>
<div style="page-break-after: always;"></div>
<%
            }
    }
%>