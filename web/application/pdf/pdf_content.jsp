<%@page import="java.util.Locale"%>
<%@page import="org.apache.commons.lang.StringUtils" %>
<%@page import="org.apache.commons.lang.WordUtils"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Section"%>
<div class="content">

  <%
  request.setAttribute("company", company);
  String localeCode = company.getString(uada.C1_LOCALE);
  Locale set = Locale.forLanguageTag("en-US");
  boolean hasEsign = proposal.getSignSettings().size() > 0;
  if (StringUtils.isNotBlank(localeCode))
  {
  	set = Locale.forLanguageTag(localeCode);
  }
  request.setAttribute("locale", set.toString());
  for (int i = 0 ; i < sections.size() ; ++i)
  {
  	if ((i+1) == sections.size()) {
      request.setAttribute("last-page-nobreak-section", true);
    }
    Section section = sections.get(i);
    request.setAttribute("section", section);
    request.setAttribute("sectionIndex", i);
    String title = section.getGeneratedTitle();
    if (section.getTypeAsEnum() != Section.SectionType.PRICING || section.isEnabled()) {
    %>
    <%
	boolean pageBroken = request.getAttribute("page-break-after-always") != null;
	if(pageBroken){
		request.removeAttribute("page-break-after-always");
	}
	%>
    <div class="tab-pane section sectionType<%=WordUtils.capitalizeFully(section.getTypeAsEnum().toString()) %> page-break-before-<%= pageBroken ? "off" : section.getBreakPageBeforeInPDF() %>" id="section<%=i%>">
    <%    
    if (section.getTypeAsEnum() == Section.SectionType.COVER)
    {
    %>
      <jsp:include page="/application/pdf/section/section_cover.jsp"></jsp:include>
    <%--<%--%>
    <%--}--%>
    <%--else if (section.getTypeAsEnum() == Section.SectionType.WHATS_NEXT--%>
    		<%--|| "What's Next?".equalsIgnoreCase(section.getTitle())) {--%>
    <%--%>--%>
        <%--<jsp:include page="/application/pdf/section/section_whats_next.jsp"></jsp:include>--%>
    <%    
    }
    else if (section.getTypeAsEnum() == Section.SectionType.DYNAMIC
            || section.getTypeAsEnum() == Section.SectionType.INTRODUCTION
            || section.getTypeAsEnum() == Section.SectionType.ITINERARY
            || section.getTypeAsEnum() == Section.SectionType.DESTINATIONGUIDE
            ) {
    %>
    <jsp:include page="/application/pdf/section/section_dynamic.jsp">
      <jsp:param value="<%=section.getTypeAsEnum() == Section.SectionType.INTRODUCTION %>" name="isIntroduction"/>
    </jsp:include>
    <%  
    }
    else if (section.getTypeAsEnum() == Section.SectionType.EXTRA) {
    %>
       <jsp:include page="/application/pdf/section/section_extra.jsp"></jsp:include>
    <%    
    }
    else if (section.getTypeAsEnum() == Section.SectionType.PRICING && section.isEnabled()) {
    %>
        <%--<jsp:include page="/application/pdf/section/section_pricing.jsp"></jsp:include>--%>
    <%    
    }
    else if (section.getTypeAsEnum() == Section.SectionType.SOLUTION) {
    %>
        <%--<jsp:include page="/application/pdf/section/section_solution.jsp"></jsp:include>--%>
    <%    
    }

    %>
 
    </div>
    <%    
    if (section.getTypeAsEnum() == Section.SectionType.COVER && StringUtils.equalsIgnoreCase("on", section.getContent().get(0).getMetadata().get("tableOfContent")))
    {
      int tableOfContentLevel = 0;
      try{
    	  tableOfContentLevel = Integer.parseInt( section.getContent().get(0).getMetadata().get("tableOfContentLevel") );
      }catch(Exception e){
        
      }
      request.setAttribute("tableOfContentLevel", tableOfContentLevel);
    %>
    <div class="tab-pane section sectionTypeDynamic page-break-before-on" id="section-toc">
      <%--<jsp:include page="/application/pdf/section/section_toc.jsp"></jsp:include>--%>
    </div>
    <%
    }
    }
  }
  %>
</div>