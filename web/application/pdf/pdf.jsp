<?xml version="1.0" encoding="UTF-8"?>
<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="au.corporateinteractive.qcloud.pdf.service.PdfService"%>
<%@page import="java.util.HashMap"%>
<%@page import="au.corporateinteractive.utils.PropertiesUtil"%>
<%@page import="java.util.Map"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.Currency"%>
<%@page import="java.util.Locale"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService"%>
<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%@page import="java.util.Hashtable"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.List"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.*" %>
<%@ page import="au.com.ci.system.classfinder.StringUtil" %>
<%@ page import="au.net.webdirector.common.utils.module.StoreUtils" %>
<%@ page import="au.net.webdirector.common.datalayer.client.ElementData" %>
<%@ page language="java" contentType="text/html;
charset=utf-8" pageEncoding="utf-8" isELIgnored="false" %>
<%
    request.setAttribute("proposalMode", "pdf");
    String proposalId = request.getParameter("traveldoc");
    TravelDocsDataAccess pda = new TravelDocsDataAccess();
    Hashtable<String, String> proposalHashtable = new TravelDocsDataAccess().getTravelDocById(proposalId, true);
    Proposal proposal = pda.loadTravelDoc(proposalId);
    pda.parseTravelDocForView(proposal, proposalHashtable.get(TravelDocsDataAccess.E_OWNER));
    Integer sectionCount = 0;
    UserAccountDataAccess uada = new UserAccountDataAccess();

    request.setAttribute("traveldoc", proposal);

    TravelDocsDataAccess.addFormattedDates(proposalHashtable);
    String modifiedDate = proposalHashtable.get(TravelDocsDataAccess.MODIFIED_FOR_DISPLAY);
    String clientCompany = proposalHashtable.get(TravelDocsDataAccess.E_CLIENT_COMPANY);
    String companyID = proposalHashtable.get(TravelDocsDataAccess.E_OWNER);

    request.setAttribute("proposalHashtable", proposalHashtable);

    CategoryData company = new UserAccountDataAccess().getCompanyForCompanyId(companyID);
    request.setAttribute("companyID", company.getId());

    List<Hashtable<String, String>> users = new UserAccountDataAccess().getUsersByCompanyId(companyID);
    ElementData owner = new UserAccountDataAccess().getUserElementWithUserID(users.get(0).get(UserAccountDataAccess.E_ID));

    String companyDateFormat = company.getString("ATTR_DateFormat");
    if(StringUtils.isBlank(companyDateFormat))
    {
    	companyDateFormat = "dd-MM-yyyy";
    }
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
    SimpleDateFormat csdf = new SimpleDateFormat(companyDateFormat);

    Content conInfo = proposal.findGeneratedContentOfType("con-info");
    Map<String, String> proposalMetadata = conInfo.getMetadata();

    boolean landscapeMode = StringUtils.equals("on", proposalMetadata.get("landscapeMode"));

    proposalMetadata.put("proposalId", proposalHashtable.get(TravelDocsDataAccess.E_ID));

    Date createDate = sdf.parse(proposalHashtable.get(TravelDocsDataAccess.E_CREATED_DATE));
    Date expiryDate = sdf.parse(proposalHashtable.get(TravelDocsDataAccess.E_EXPIRE_DATE));
    proposalMetadata.put("proposalCreate", csdf.format(createDate));
    proposalMetadata.put("proposalExpiry", csdf.format(expiryDate));

    String localeString = company.getString(UserAccountDataAccess.C1_LOCALE);
    Locale locale = Locale.forLanguageTag(localeString);
    Currency c = Currency.getInstance(locale);
    DecimalFormat df = new DecimalFormat("#0.00");
    proposalMetadata.put("proposalCost", c.getSymbol(locale) + df.format(Double.valueOf(proposalHashtable.get(TravelDocsDataAccess.E_MINIMUM_COST))));

    proposalMetadata.put("travelDocsStatus", proposalHashtable.get(TravelDocsDataAccess.E_PROPOSAL_STATUS));
    List<Section> sections = proposal.getSections();
%>
<head>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,IE=9,IE=8,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<link rel="shortcut icon" type="image/x-icon" href="/application/assets/new/images/favicon.ico" />

    <link rel="stylesheet" href="/application/assets/new/css/pdf.css" type="text/css" />
    <link rel="stylesheet" href="/application/assets/new/css/coverpage.css" type="text/css" />
    <link rel="stylesheet" href="/stores/_fonts/stylesheet.css" type="text/css" />
    <!-- STYLESHEETS -->

    <style type="text/css">
        [fuse-cloak],
        .fuse-cloak {
            display: none !important;
        }
        body {
            font-family: "Roboto", "Helvetica Neue", sans-serif;
            font-size: 13px;
            font-weight: normal;
            /*background-color: #00ff0c;*/
        }
        .travel-container h3 {
            margin: 0px;
        }

        .travel-info-text p, .terrorism-info-container p, .date-info-container p {
            margin: 0px !important;
        }
        .travel-container table {
            width: 100% !important;
        }

        .segment-inner-rows .row {
            margin-bottom: 4px;
        }

        .segment-outer-row, .segment-inner-rows {
            margin-bottom: 10px;
        }
  
    </style>
    <%
    	Map<String, String> styleSettings = null;
        for(Section section:sections) {
            if (section.getType().equalsIgnoreCase("cover")) {
                List<Content> coverItems = section.getContent();
                for (Content content:coverItems) {
                    if (content.getType().equalsIgnoreCase("cover")) {
                    	styleSettings = content.getCoverData().getMetadata();
                        %>
    <style type="text/css"><%=content.getCoverData().getCss()%>

    @page :first {
        background-image: <%=content.getCoverURL()%>;
        background-size: cover;
    <%--<%=content.getCoverURL()%>;--%>
        @bottom-left {
            background-color: transparent !important;
            content: none !important;
            color: transparent !important;
        }
        @bottom-right {
            background-color: transparent !important;
            color: transparent !important;
            content: none !important;
        }
        @bottom-center {
            background-color: transparent !important;
            content: none !important;
            color: transparent !important;
        }
    }

    @page  {
        size: A4 ;
        /*margin-left: 0;*/
        margin: 10px 0 30px 0;
    }
    @page {
        @bottom-left{
            content: '<%=company.getString("attr_categoryname")%>';
            font-family: "Roboto", "Helvetica Neue", sans-serif;
            font-size: 12px;
            padding-left: 15px;
            color: #58585a;
        }
        @bottom-center {
            content: counter(page) '/' counter(pages);
            font-family: "Roboto", "Helvetica Neue", sans-serif;
            color: #58585a;
            font-size: 12px;
        }
        @bottom-right{
            content: 'Booking Reference: #<%=proposal.getBooking()==null?"":proposal.getBooking().getBookingReference()%>';
            font-family: "Roboto", "Helvetica Neue", sans-serif;
            font-size: 12px;
            padding-right: 18px;
            color: #58585a;
        }
    }
        /*.proposalTitle {*/
            /*color: rgb(255, 246, 254) !important;*/
            /*background-color: rgb(255, 0, 0) !important;*/
        /*}*/
    .proposalTitle {
        top: 535px !important;
    }
    .coverpage-main-image {
        background-size: cover;
        background: none !important;
        width: 100%;
        height: 100%;
        position: relative;
    }
    .coverData {
        top: 770px !important;
    }
        .contentImageBlockHolder {
            width: 100%;
            height: auto;
        }
    </style>
    <%
                    break;
                    }
                }
                break;
            }
        }
    
    if(styleSettings != null) {
		%>
		<style type="text/css">
			body {
				font-family:"<%=styleSettings.get("fontFamilyBody")%>" !important;
				font-size:<%=styleSettings.get("fontSizeBody")%>px !important;
				line-height:1.5em !important;
				color:<%=styleSettings.get("fontColorBody")%> !important;
			}
			body p{
				margin: 0;
			}
			body .iti-segment *{
	        	line-height: <%=StringUtils.equalsIgnoreCase(styleSettings.get("fontFamilyBody"), "Noto Sans CJK TC Regular")?"1.5em":"1em"%>;
	        }
			body h1, body .h1 {
				font-family:"<%=styleSettings.get("fontFamilyH1")%>" !important;
				font-size:<%=styleSettings.get("fontSizeH1")%>px !important;
				line-height:1.5em !important;
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("customHeading1Color"))){%>color:<%=styleSettings.get("fontColorHeading1")%> !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("boldHeading1"))){%>font-weight:bold !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("uppercaseHeading1"))){%>text-transform:uppercase !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("underlineHeading1"))){%>text-decoration:underline !important;<%}%>
			}
			.segment .segment-heading .icon {
				font-size:<%=styleSettings.get("fontSizeH1")%>px !important;
			}
            body h2, body .h2 {
				font-family:"<%=styleSettings.get("fontFamilyH2")%>" !important;
				font-size:<%=styleSettings.get("fontSizeH2")%>px !important;
				line-height:1.5em !important;
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("customHeading2Color"))){%>color:<%=styleSettings.get("fontColorHeading2")%> !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("boldHeading2"))){%>font-weight:bold !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("uppercaseHeading2"))){%>text-transform:uppercase !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("underlineHeading2"))){%>text-decoration:underline !important;<%}%>
			}
            body h3, body .h3 {
				font-family:"<%=styleSettings.get("fontFamilyH3")%>" !important;
				font-size:<%=styleSettings.get("fontSizeH3")%>px !important;
				line-height:1.5em !important;
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("customHeading3Color"))){%>color:<%=styleSettings.get("fontColorHeading3")%> !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("boldHeading3"))){%>font-weight:bold !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("uppercaseHeading3"))){%>text-transform:uppercase !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("underlineHeading3"))){%>text-decoration:underline !important;<%}%>
			}
			body .section-head {
                font-family:"<%=styleSettings.get("fontFamilySection")%>" !important;
                font-size:<%=styleSettings.get("fontSizeSection")%>px !important;
                line-height:1.5em !important;
                <%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("customSectionColor"))){%>color:<%=styleSettings.get("fontColorSectionTitle")%> !important;<%}%>
                <%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("boldSectionTitle"))){%>font-weight:bold !important;<%}%>
                <%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("uppercaseSectionTitle"))){%>text-transform:uppercase !important;<%}%>
                <%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("underlineSectionTitle"))){%>text-decoration:underline !important;<%}%>
            }
            body .iti-label, body .iti-segment .row .col-2, body .iti-segment .row .col-3, .iti-segment .segment-outer-row > div:first-child {
            	font-family:"<%=styleSettings.get("fontFamilyDL")%>" !important;
				font-size:<%=styleSettings.get("fontSizeDL")%>px !important;
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("customDLColor"))){%>color:<%=styleSettings.get("fontColorDL")%> !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("boldDL"))){%>font-weight:bold !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("uppercaseDL"))){%>text-transform:uppercase !important;<%}%>
            }
            
            <%=PdfService.getResponsiveGridCss(754, "", 1)%>
		</style>
		<%
	}
        %>
    <!-- Icons.css -->
    <link type="text/css" rel="stylesheet" href="/application/assets/new/icons/fuse-icon-font/style.css" />
    <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/fontawesome-pro-5.8.1-web/css/all.css" />
    <link type="text/css" rel="stylesheet" href="/application/assets/new/css/itinerary.css" />
    <!-- <link type="text/css" rel="stylesheet" href="/application/assets/new/css/pdf.css" /> -->
<%--    <link type="text/css" rel="stylesheet" href="/application/assets/plugins/bootstrap/css/bootstrap.pdf.css" />--%>
	<!-- <link type="text/css" rel="stylesheet" href="/application/assets/plugins/bootstrap/css/bootstrap.min.css" /> -->
	
    <!-- Animate.css -->
</head>
<body>

<%--<div class="editor-content window-min-height panel-padding-sections panel-padding-blocks">--%>
    <%--<div class="editor-content-scroller">--%>
        <%--<div class="doc-effect-holder">--%>
            <%--<div class="blosck-droppable-area">--%>
                <%--<div class="doc-page-area">--%>
                    <%--&lt;%&ndash;<%@include file="/application/pdf/pdf_content.jsp" %>&ndash;%&gt;--%>
                    <%----%>
                <%--</div>--%>
            <%--</div>--%>

        <%--</div>--%>
    <%--</div>--%>
<%--</div>--%>
<div class="pdf-containter">

    <%@include file="/application/pdf/fragments/pdfContent.jsp" %>

</div>

</body>
</html>