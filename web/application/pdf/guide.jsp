<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="au.net.webdirector.common.datalayer.base.file.StoreFileAccessFactory"%>
<%@page import="au.net.webdirector.common.datalayer.base.file.StoreFileAccess"%>
<%@page import="webdirector.db.client.ClientFileUtils"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.db.ProposalAttachmentDataAccess"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.model.media.AttachmentDetails"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Section"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="java.util.Hashtable"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess" %>
<html>

<%@ page language="java" contentType="text/html; 
charset=utf-8" pageEncoding="utf-8" isELIgnored="false" %>
<%
	String proposalId = request.getParameter("proposal");
	String thumbImageUrl = request.getParameter("thumb");
	TravelDocsDataAccess pda = new TravelDocsDataAccess();
	Proposal proposal = pda.loadTravelDoc(proposalId);
	Hashtable<String, String> proposalHashtable = new TravelDocsDataAccess().getTravelDocById(proposalId);
	String companyID = proposalHashtable.get(TravelDocsDataAccess.E_OWNER);
	CategoryData company = new UserAccountDataAccess().getCompanyForCompanyId(companyID);

	String spectrumColor = proposal.loadSpectrumColor();
	String corporateCSS = String.format("/%s/%s/Categories/%s/gen/css-document/%s/pdf.css",
			Defaults.getInstance().getStoreContext(),
			UserAccountDataAccess.USER_MODULE,
			company.getId(),
			spectrumColor);
	String fontsCSS = String.format("/%s/_fonts/styleshevimet.css", Defaults.getInstance().getStoreContext());


	Content content = null;

	for (Section section : proposal.getSections())
	{
		if (section.getType().equals("destination-guide"))
		{
			for (Content con : section.getContent())
			{
				if (StringUtils.isNotBlank(con.getType()) && con.getType().equals("destination"))
				{
					if (con.getData() == null)
					{
						continue;
					}
					if (con.getData().contains(thumbImageUrl))
					{
						content = con;
					}
				}
			}
		}
	}

	String guide = "";
	String head = "";
	String city = "";
	if (content != null)
	{
		String data = content.getData();
		city = data.substring(4, data.indexOf("</h1>"));
		String headMatch = "<h2 class=\"section-head\" thumb-image=";
		int guideBeginIndex = data.indexOf(thumbImageUrl) - headMatch.length() - 1;
		int guideEndIndex = data.indexOf("<h2 class=\"section-head\" thumb-image=", guideBeginIndex + 1);
		guide = data.substring(guideBeginIndex, guideEndIndex < 0 ? data.length() : guideEndIndex);
		head = guide.substring(0, guide.indexOf("</h2>") + 5);
		head = head.replace("\">", "\">" + city + " ");
		guide = guide.substring(guide.indexOf("</h2>") + 5);
	}

%>

<head>
<link rel="stylesheet" href="<%=corporateCSS%>" />
<link rel="stylesheet" href="<%=fontsCSS%>" />
<link rel="stylesheet"
	href="/application/assets/plugins/font-awesome/css/font-awesome.css" />
<link rel="stylesheet"
	href="/application/assets/plugins/qcloud-iconfont/style.css" />
</head>

<body
	class="spectrum-<%=spectrumColor%> <%=StringUtils.isBlank(company.getString(UserAccountDataAccess.C1_PROPOSAL_TABLE_STYLE)) ? "tableStyle0" : company.getString(UserAccountDataAccess.C1_PROPOSAL_TABLE_STYLE)%>">

	<div class="content">
		<div class="section">
			<div class="sectionDiv">
				<div class="sectionContent">
					<div class="contentDiv">
						<div class="contentHolder">
							<%=head%>
							<%
					       		String[] pois = guide.split("<h3");
							%>
							<%=pois[0]%>
					       <%
					       		//ProposalAttachmentDataAccess pada = new ProposalAttachmentDataAccess();
					       		for(int j=1; j<pois.length-1; j++){
					       			String poi = pois[j];
					       			//int beginIndex = poi.indexOf("poi-id=\"") + 8;
					       			//String poiId = pois[j].substring(beginIndex, poi.indexOf("\"", beginIndex));
					       			//AttachmentDetails attachment = pada.loadAttachmentById(poiId);
					       			
					       			int beginIndex = poi.indexOf("poi-image=\"") + 11;
					       			String poiImageUrl = pois[j].substring(beginIndex, poi.indexOf("\"", beginIndex));
					       			%>
					       				<div class="poi">
					       					<%-- <img class="poi-thumb" src="<%="/"+Defaults.getInstance().getStoreContext()+attachment.getDownloadPath()%>"></img> --%>
					       					<img class="poi-thumb" src="<%=poiImageUrl%>"></img>
					       					<div class="poi-desc">
					       						<%="<h3" + pois[j] %>
					       					</div>
					       				</div>
					       				<div class="clear-both"></div>
					       			<%
					       		}
					       %>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>