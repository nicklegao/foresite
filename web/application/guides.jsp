<!DOCTYPE html>
<!--[if lt IE 9]><html class="bad-ie" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.control.StripeController"%>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.LinkedList" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Locale" %>
<%@page import="java.util.Map.Entry" %>
<%@page import="java.util.Set" %> 
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="com.google.gson.Gson" %>
<%@page import="au.com.ci.sbe.util.UrlUtils" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.CustomFields" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.StripeService" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.DateFormatUtils" %>
<%@ page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager" %>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%@ page import="au.net.webdirector.common.datalayer.client.ElementData" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.*" %>
<%@ page import="au.corporateinteractive.qcloud.market.enums.Market" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.control.UserController" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Element" %>
<%@ page import="au.net.webdirector.common.datalayer.base.db.entity.TextFile" %>
<html lang="en">
<!--<![endif]-->
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="false" compressJavaScript="false" compressCss="true">
    <head>

        <%


            TUserAccountDataAccess uada = new TUserAccountDataAccess();
            UserAccountDataAccess uda = new UserAccountDataAccess();
            TravelDocsDataAccess pda = new TravelDocsDataAccess();
            TravelDocsDashboardDataAccess pdda = new TravelDocsDashboardDataAccess();

            String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
            String userUsername = (String)session.getAttribute(Constants.PORTAL_USERNAME_ATTR);
            CategoryData company = uada.getUserCompanyForUserId(userId);
//            uada.updateCompanySettings(company, request.getServletContext());


            TravelDocsStylesDataAccess tds = new TravelDocsStylesDataAccess();
            CategoryData companyStyles = tds.getCompanyStylesWithId(company.getId());

            ElementData userDetails = uada.getUserWithId(userId);

//This is an example notification that your proposal has been viewed
//	FirebaseService.getInstance().sendNotificationToUser(userId, "Proposal " + "29292" + " has been viewed", "Proposal " + "20202" + " has been opened and if currently being viewed by your customer.", false);

            TUserRolesDataAccess urda = new TUserRolesDataAccess();
            UserPermissions up = urda.getPermissions(userId);


            String[] accessRights = uada.getUserAccessRights(userId);
            List<ElementData> userAccess = uada.getUsers(accessRights);
            List<HashMap<String, String>> userAccessNames = new LinkedList<HashMap<String, String>>();
            for (ElementData u : userAccess)
            {
                HashMap<String, String> details = new HashMap();
                details.put("value", u.getId());
                details.put("label",String.format("%s %s", u.getString(UserAccountDataAccess.E_NAME), u.getString(UserAccountDataAccess.E_SURNAME)));
                userAccessNames.add(details);
            }

// Javascript flags
            boolean[] trialExpiredVars = UserController.isTrialPeriodOver(session);
            int expireInDays = new UserAccountDataAccess().trialPeriodOverInWeek(company);
            int expireInDaysCounter = new UserAccountDataAccess().trialPerioddayCounter(company);
            boolean trialExpired = trialExpiredVars[0];
            Set<Entry<String, ElementData>> uCount = uada.getCompanyUsers(company.getId()).entrySet();
            int userCount = 1;
            if (uCount != null) {
                userCount = uCount.size();
            }
            Boolean trialAlmostExpired = trialExpiredVars[1];
            boolean noPhoneNumber = false;
            if (StringUtils.isBlank(userDetails.getString(UserAccountDataAccess.E_MOBILE_NUMBER))) {
                noPhoneNumber = true;
            }
            boolean showFirstLogin = false; //(!company.getBoolean(TUserAccountDataAccess.TUTORIAL_FIRST_ACCESS) && up.hasManageSettingsPermission());
            CustomFieldsDataAccess cfda = new CustomFieldsDataAccess();
//			CustomFields customFields = cfda.getCompanyCustomFields(CustomFields.PROPOSALS, company.getId());
//			CustomFields productCustomFields = cfda.getCompanyCustomFields(CustomFields.PRODUCTS, company.getId());


//Proposal count for tip guide
//            int countTotal = pdda.countProposalsTotal(accessRights);
            boolean hasProposalsBoolean = false;
//            if (countTotal > 0) {
//                hasProposalsBoolean = true;
//            }

            boolean hasUserAccess = up.hasManageUsersPermission();

//            DiskUsageDataAccess duda = new DiskUsageDataAccess();
//            ElementData diskUsageData = duda.getDiskUsageForCompanyId(company.getString("Category_id"));
            boolean trialAccount = StringUtils.equalsIgnoreCase(company.getString("attrdrop_pricingplan"), "TRIAL");


            if (trialExpired) {
                new EmailBuilder().prepareAccountReaccessedEmail(userDetails).addTo(userDetails.getString("attr_headline")).doSend();
            }

            TravelGuideDataAccess tgda = new TravelGuideDataAccess();
            List<ElementData> guides = tgda.getAllTravelWatchElements();

        %>

        <script>
            var dateFormatPattern = '<%= DateFormatUtils.getInstance(companyStyles.getString(TravelDocsStylesDataAccess.C_DATE_FORMAT), Locale.forLanguageTag(company.getString(UserAccountDataAccess.C1_LOCALE))).getJsPattern() %>';

            var expireInDaysCounter = <%=expireInDaysCounter %>;
            var hasUserAccess = <%=hasUserAccess %>;
            var trialExpired = <%=trialExpired %>;
            var trialAlmostExpired = <%=trialAlmostExpired %>;

            var needSetupPayment = false; <%--<%= StripeController.needSetupPayment(session) %>;--%>

            var expireInDays = <%=expireInDays %>;
            var noPhoneNumber = <%=noPhoneNumber %>;
            var showFirstLogin = <%=showFirstLogin %>;
            var userCount = <%=userCount %>;

            var userAccess = <%=new Gson().toJson(userAccessNames) %>;

            var tutorialFirstLoad = false; <%--"<%= uda.isUsersFirstAccess(userUsername)%>";--%>
            var userEmail = "<%= userUsername %>";
            var tab4Shown;

            <%--var accountSuspended = <%= StripeService.isCompanySuspended(company.getId()) %>;--%>
            var hasManageSubscriptionsPermission = <%= up.hasManageUsersPermission() %>;
            var companyAccountActive = <%=new AgentsDataAccess().doesCompanyHaveAgent(company.getId())%>;

        </script>

        <title>TravelDocs - Travel Safety Guides</title>
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="shortcut icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon">

        <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700italic,700,900,900italic" rel="stylesheet">

        <!-- STYLESHEETS -->
        <style type="text/css">
            [fuse-cloak],
            .fuse-cloak {
                display: none !important;
            }

        </style>
        <!-- Icons.css -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/icons/fuse-icon-font/style.css">
        <link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/all.css">
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/fontawesome-pro-5.8.1-web/css/all.css" />
        <!-- Animate.css -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/animate.css/animate.min.css">
        <!-- PNotify -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/pnotify/pnotify.custom.min.css">
        <!-- Nvd3 - D3 Charts -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/nvd3/build/nv.d3.min.css" />
        <!-- Fuse Html -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/fuse-html/fuse-html.min.css" />
        <!-- Swiper -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/swiper/css/swiper.min.css" />
        <!-- Datatable -->
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css" />
        <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.material.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.2/css/fixedHeader.dataTables.min.css">
        <!-- Main CSS -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/css/main.css">

        <link type="text/css" rel="stylesheet" href="/application/assets/new/css/dashboard.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

        <link type="text/css" rel="stylesheet" href="/application/assets/plugins/switchery/switchery.css" />
        <link type="text/css" rel="stylesheet" href="/application/assets/plugins/select2/select2.css" />
        <link rel="stylesheet" href="/application/assets/plugins/paper-collapse/paper-collapse.min.css" />

        <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/plugins/table.css",request) %>" />
        <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/froala_style.min.css",request) %>" />
        <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/froala_editor.pkgd.min.css",request) %>" />

        <!-- / STYLESHEETS -->

        <!-- JAVASCRIPT -->
        <!-- jQuery -->
        <script src="/application/assets/plugins/jquery/1.10.2/jquery.min.js"></script>
        <script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
        <script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.3.min.js"></script>
        <!-- Mobile Detect -->
        <script type="text/javascript" src="/application/assets/new/vendor/mobile-detect/mobile-detect.min.js"></script>
        <!-- Popper.js -->
        <script type="text/javascript" src="/application/assets/new/vendor/popper.js/index.js"></script>

        <!-- Nvd3 - D3 Charts -->
        <script type="text/javascript" src="/application/assets/new/vendor/d3/d3.min.js"></script>
        <script type="text/javascript" src="/application/assets/new/vendor/nvd3/build/nv.d3.min.js"></script>
        <!-- Data tables -->
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.material.min.js"></script>  <%--/application/assets/new/vendor/datatables.net/js/jquery.dataTables.min.js--%>
            <%--<script type="text/javascript" src="/application/assets/new/vendor/datatables-responsive/js/dataTables.responsive.js"></script>--%>
        <!-- PNotify -->
        <script type="text/javascript" src="/application/assets/new/vendor/pnotify/pnotify.custom.min.js"></script>
        <!-- Fuse Html -->
        <script type="text/javascript" src="/application/assets/new/vendor/fuse-html/fuse-html.min.js"></script>
        <!-- Main JS -->
        <script type="text/javascript" src="/application/assets/new/js/main.js"></script>
        <!-- / JAVASCRIPT -->

        <style>
            .swiper-container {
                width: 100%;
                height: 100%;
            }
            .swiper-slide {
                text-align: center;
                font-size: 18px;
                background: #fff;
                /* Center slide text vertically */
                display: -webkit-box;
                display: -ms-flexbox;
                display: -webkit-flex;
                display: flex;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                -webkit-justify-content: center;
                justify-content: center;
                -webkit-box-align: center;
                -ms-flex-align: center;
                -webkit-align-items: center;
                align-items: center;
            }
            .tableColumnHolder > .dropdown-item {
                line-height: 2.8rem !important;
                height: 2.8rem !important;
            }
            .tableColumnHolder > .dropdown-item > i {
                margin-right: 6px;
                visibility: hidden;
            }

            .tableColumnHolder > .dropdown-item.toggle-vis > i {
                visibility: visible;
            }
        </style>
    </head>

    <body class="layout layout-vertical layout-left-navigation layout-below-toolbar">
    <main>
        <div id="wrapper">

            <jsp:include page="/application/include/common-side-menu.jsp"></jsp:include>

            <div class="content-wrapper">

                <jsp:include page="/application/include/common-top-menu.jsp"></jsp:include>

                <div class="content custom-scrollbar">

                    <div id="project-dashboard" class="page-layout simple right-sidebar">

                        <div class="page-content-wrapper custom-scrollbar" style="overflow-y: scroll">
                            <!-- HEADER -->

                            <!-- / HEADER -->

                            <!-- CONTENT -->
                            <div class="page-content" >

                                <div class="tab-content" >
                                    <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab" >

                                        <!-- WIDGET GROUP -->
                                        <div class="widget-group row no-gutters">

                                            <!-- WIDGET 1 -->
                                            <div class="col-12">

                                                <div class="card" style="border-radius: 4px;">
                                                    <div class="card-header px-4 py-2 row no-gutters align-items-center justify-content-between">
                                                        <div class="row align-items-center" style="width: 100%;margin: 0;">
                                                            <div class="col-auto" style="display: flex;padding-left: 0;">
                                                                <span style="padding-right: 6px;padding-top: 5px;min-width: 120px;" id="shownStatusText">Show Guides For:</span><input type="text" class="form-control between user-success md-has-value" id="locationSearch" placeholder="Search Travel Locations">
                                                            </div>
                                                            <div class="col text-right">
                                                                <span>Travel Safety Guides generated daily.</span>
<%--                                                                <span>Show</span>--%>
<%--                                                                <select class="form-control" id="travelDocsDisplaySelector" >--%>
<%--                                                                    <option value="10">10</option>--%>
<%--                                                                    <option value="50" selected>50</option>--%>
<%--                                                                    <option value="100">100</option>--%>
<%--                                                                    <option value="200">200</option>--%>
<%--                                                                </select>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-body" style="padding-top: 0;padding-bottom: 0;">
                                                        <div class="table-responsive" >
                                                            <div class="travelGuideTable" id="travelGuideTable">
                                                                <table class="table">
                                                                    <thead>
                                                                    <tr>
                                                                        <th scope="col">Country</th>
                                                                        <th scope="col">Created Date</th>
                                                                        <th scope="col"></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <%
                                                                        for (ElementData guide : guides) {
                                                                    %>
                                                                    <tr>
                                                                        <th scope="row" class="countryNameRow"><%=guide.getString(TravelGuideDataAccess.E_COUNTRY)%></th>
                                                                        <td><%=guide.getString("Create_date")%></td>
                                                                        <td>
                                                                            <%
                                                                                TextFile pdf = guide.getTextFile(TravelGuideDataAccess.E_PDF_GUIDE);
                                                                                if (pdf != null) { %>
                                                                            <a href="<%=pdf.getUrl()%>" target="_blank" class="btn btn-secondary pull-left fuse-ripple-ready" >Download</a>
                                                                              <%  } else { %>
                                                                            <a href="#!" class="btn btn-secondary pull-left fuse-ripple-ready" >Generating PDF</a>
                                                                              <%  }
                                                                            %>
                                                                        </td>
                                                                    </tr>
                                                                    <%
                                                                        }
                                                                    %>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- WIDGET 1 -->
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- / CONTENT -->

                        </div>

                    </div>


                </div>
            </div>
        </div>
    </main>



    <!-- Bootstrap core JavaScript
      ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<%=UrlUtils.autocachedUrl("/application/js/log4javascript.js", request) %>"></script>
        <%--<script src="<%=UrlUtils.autocachedUrl("/application/js/logging.js", request) %>"></script>--%>
    <script src="<%=UrlUtils.autocachedUrl("/application/js/utils.js", request) %>"></script>

    <script type="text/javascript" src="/application/assets/new/js/apps/dashboard/project.js"></script>
    <!-- Fuse Html -->
    <script src="/application/assets/plugins/jquery-placeholder/js/jquery.placeholder.js"></script>
    <script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="/application/assets/plugins/moment/moment.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="/application/assets/plugins/moment-timezone/0.5.10/moment-timezone-with-data.min.js"></script>
    <script type="text/javascript" src="/application/assets/new/vendor/swiper/js/swiper.min.js"></script>
    <script src="/application/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="/application/assets/plugins/DataTables/buttons/js/dataTables.buttons.min.js"></script>
    <script src="/application/assets/plugins/DataTables/dataTables.colReorder.min.js"></script>
    <script src="/application/assets/plugins/DataTables/dataTables.colResize.js"></script>
    <script src="https://cdn.datatables.net/fixedcolumns/3.2.4/js/dataTables.fixedColumns.min.js"></script>
    <script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="/application/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"></script>
    <script src="/application/assets/plugins/timeago/jquery.timeago.js"></script>
    <script src="/application/assets/plugins/dynatable.0.3.1/jquery.dynatable.js"></script>
    <script src="/application/assets/plugins/dropzone/downloads/dropzone.min.js"></script>
    <script src="/application/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="/application/assets/plugins/bootstrap-datepicker/js/date.format.js"></script>
    <script src="/application/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="/application/assets/plugins/switchery/switchery.js"></script>
    <script src="/application/assets/plugins/nprogress/nprogress.js"></script>
    <script src="/application/assets/js/ui-modals.js"></script>
    <script src="/application/assets/plugins/Chart.js/Chart.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js"></script>
    <script src="/application/assets/plugins/paper-collapse/paper-collapse.min.js"></script>
    <script type="text/javascript" src="/application/js/randomIDGenerator.js"></script>
    <script src="/application/assets/plugins/select2/select2.js"></script>
    <script src="/application/assets/plugins/lz-string/lz-string.js"></script>


    <!-- Froala Editor -->
    <!-- <script src="/application/assets/plugins/froala-editor/js/froala_editor.pkgd.min.js"></script> -->
    <script src="/application/assets/plugins/froala_editor_3.0.0/js/froala_editor.pkgd.min.js"></script>

        <%--MAIN DASH JS--%>

    <script type="text/javascript" src="/application/assets/plugins/DateTime/dateTime.js"></script>
<%--    <script type="text/javascript" src="/application/js/dash.js"></script>--%>

    <script>

        $(document).ready(function()
        {

            $(document).on('input', '#locationSearch', function(e) {
                var val = $(this).val();
                //countryNameRow
                var rows = $('#travelGuideTable table tbody tr');
                $(rows).each(function(e) {
                    var country = $(this).find('.countryNameRow').html();
                    if (!country.toLowerCase().includes(val.toLowerCase())) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                });
            });
        });
    </script>

    </body>
</compress:html>
</html>