<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="java.util.Hashtable"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%@page import="java.util.List"%>
<%--<%@page--%>
<%--	import="au.corporateinteractive.qcloud.proposalbuilder.db.MarketDataAccess"%>--%>
<%--<%@page--%>
<%--	import="au.corporateinteractive.qcloud.proposalbuilder.db.DiskUsageDataAccess"%>--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ page
	import="au.corporateinteractive.qcloud.subscription.SubscriptionPlan"%>

<%
	String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
	UserAccountDataAccess uada = new UserAccountDataAccess();
	CategoryData companyData = uada.getUserCompanyForUserId(userId);
//	MarketDataAccess mda = new MarketDataAccess();
//	List<ElementData> apps = mda.getMarketApps(request, companyData.getString(UserAccountDataAccess.C_ADDRESS_COUNTRY));
	Hashtable<String, String> userDetails = uada.getUserWithId(userId);
	String corporateCSS = String.format("/%s/%s/Categories/%s/gen/css-document/dashboard-colours.css",
			Defaults.getInstance().getStoreContext(),
			UserAccountDataAccess.USER_MODULE,
			companyData.getId());
%>

<title>Extensions & Plugins</title>

<jsp:include page="/application/include/common-head.jsp"></jsp:include>

<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
<link rel="stylesheet" type="text/css" media="screen"
	href="/proposals/css/smartadmin-production-plugins.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="/proposals/css/smartadmin-production.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="/proposals/css/smartadmin-skins.css">
<link rel="stylesheet"
	href="/application/assets/plugins/rangeslider/rangeslider.css" />
<link rel="stylesheet" href="/application/css/dashboard.css"
	type="text/css" />
<link rel="stylesheet" href="/application/css/market.css"
	type="text/css" />
<link rel="stylesheet"
	href="/application/assets/plugins/magnific-popup/magnific-popup.css">

<link rel="stylesheet" href="<%=corporateCSS%>" />

<jsp:include page="ie8StdHeaders.jsp"></jsp:include>
<jsp:include page="dialog/backgroundLoading.jsp"></jsp:include>
<!-- GOOGLE FONT -->
<link rel="stylesheet"
	href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
<link rel="stylesheet"
	  href="/application/assets/plugins/switchery/switchery.css" />
<link rel="stylesheet" href="/application/assets/plugins/select2/select2.css" />
<link rel="stylesheet" href="/application/assets/plugins/sweetalert/sweetalert.css" />
</head>

<body class="market-body">
	<div class="blurred">
		<!--  Notification Area -->
		<div class='notificationMessage' style='display: none'></div>
		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">

				<span id="logo"> <img src="/application/images/qCloud.png"
					alt="QuoteCloud">
				</span>

			</div>

			<jsp:include page="include/i_navbar.jsp" />

			<!-- pulled right: nav area -->
			<div class="pull-right">

				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="/api/free/logout" title="Sign Out"
						data-action="userLogout"
						data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i
							class="fa fa-sign-out"></i></a>
					</span>
				</div>
				<!-- end logout button -->

				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a data-action="launchFullscreen" title="Full Screen"><i
							class="fa fa-arrows-alt"></i></a>
					</span>
				</div>
				<!-- end fullscreen button -->

			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->

		<div id="body">
			<div role="main">

				<div class="market-ribbon">
					<span class="ribbon-text">Extensions & Plugins</span>
				</div>

				<div id="content">

					<div id="market">
						<div class="modal-body smart-form">
							<div id="market-apps">
								<div class="content-left">
									<div class="apps-navbar">
										<div class="app-nav">
											<ul id="app-nav-categories">
												<li class="active" dataId="0"><a>All Apps</a></li>
<%--												<%--%>
<%--													List<CategoryData> appCategories = mda.getMarketAppCategories();--%>
<%--													for (CategoryData cat : appCategories)--%>
<%--													{--%>
<%--												%>--%>
<%--												<li dataId="<%=cat.getId()%>"><a><%=cat.getName()%></a></li>--%>
<%--												<%--%>
<%--													}--%>
<%--												%>--%>
												<hr>
												<li dataId="subscribed"><a>Your Apps</a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="content-right">
									<div class="">
										<ul id="apps-list">

										</ul>
									</div>
								</div>
							</div>
							<div id="app-details">
								<button id="backBtn" class="btn btn-default" title="Back">
									<i class="fa fa-arrow-left"></i> <span>BACK</span>
								</button>
								<button id="settingsBtn" style="float: right;" class="btn btn-default" title="Settings">
									<i class="fa fa-cogs"></i> <span>Settings</span>
								</button>
								<div class="app-info">
									<div class="app-details">
										<div class="img-wrap">
											<div class="app-thumbnail">
												<img>
											</div>
										</div>

										<div class="app-desc">
											<h2 id="appTitle"></h2>
											<h3 id="appLabel"></h3>
											<div class="sub-msg"></div>
											<div id=descContent></div>
										</div>
									</div>
									<div class="image-container">
										<div class="app-pricing">
											<span id="appCost"></span> <br /> <span id="terms"></span>
										</div>
										<div class="app-image-box box box5 shadow5" style="height: 327px;">
											<img class="app-image center-cropped">
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="action">
									<button id="btnComingSoon">COMING SOON</button>
									<button id="btnSubscribe" class="active">
										<span>FREE TRIAL</span>
									</button>
									<button id="btnUnsubscribe">UNSUBSCRIBE</button>
									<button id="btnReactivate">REACTIVATE</button>
									<a class="demo-video" style="padding: 18px 40px;">DEMO</a>
									<a onclick="displayTermsAndConditionsButton()" class="btnTermsOfService" style="padding:18px 40px;">Terms &amp; Conditions</a> <i class="updateDate">last updated on...</i>
								</div>
								<%--<div class="action" id="termsOfService">--%>
								<%--</div>--%>
								<div id="descBodyContent" class="desc-body"></div>
								<div class="slide-show">
									<div id="myCarousel" data-interval="false"
										class="carousel slide" data-ride="carousel">
										<!-- Indicators -->
										<ol class="carousel-indicators">

										</ol>

										<!-- Wrapper for slides -->
										<div class="carousel-inner"></div>

										<!-- Left and right controls -->
										<a class="left carousel-control" href="#myCarousel"
											data-slide="prev"> <span
											class="glyphicon glyphicon-chevron-left"></span> <span
											class="sr-only">Previous</span>
										</a> <a class="right carousel-control" href="#myCarousel"
											data-slide="next"> <span
											class="glyphicon glyphicon-chevron-right"></span> <span
											class="sr-only">Next</span>
										</a>
									</div>
									<div class="image-desc"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<div id="account-settings-modal" class="modal top-modal fade"
	tabindex="-1"></div>
<div id="terms-modal" class="modal top-modal fade" tabindex="-1">
	<div id="terms">
		<div class="modal-header">
			<div class="titleBar">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					<i class="fa fa-times-circle"></i>
				</button>
			</div>
			<h2 class="modal-title">Terms and conditions</h2>
		</div>

		<div class="modal-body">
			<div id="termsContent"></div>
			<div class="modal-footer" id="termsFooter">
				<div>
					<input class="terms-check" type="checkbox"> I agree with
					these terms
				</div>
				<div>
					<button id="agreementContinue" class="btn btn-default">Continue</button>
				</div>
			</div>

		</div>
	</div>
</div>
<!-- start: BOOTSTRAP EXTENDED MODALS -->
<div id="app-qcsabre-modal" class="modal top-modal fade" tabindex="-1"></div>
<div id="app-qctravelport-modal" class="modal top-modal fade" tabindex="-1"></div>
<div id="app-qcpayment-modal" class="modal top-modal fade" tabindex="-1"></div>
<div id="edit-management-modal" class="modal top-modal fade" tabindex="-1"></div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="/application/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/application/assets/plugins/switchery/switchery.js"></script>
<script src="/application/assets/plugins/select2/select2.js"></script>

<script src="/application/assets/plugins/sweetalert/sweetalert-dev.js"></script>
<script src="/application/assets/plugins/swalExtend/swalExtend.js"></script>
<script src="/application/assets/plugins/nprogress/nprogress.js"></script>
<script src="/application/assets/plugins/iCheck/jquery.icheck.min.js"></script>
<script
	src="/application/assets/plugins/magnific-popup/jquery.magnific-popup.js"></script>

<script
	src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
<script
	src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>

<script src="/application/assets/plugins/hopscotch-0.2.5/js/hopscotch.js"></script>
<script src="<%=UrlUtils.autocachedUrl("/application/js/tours/tour.js", request) %>"></script>
<script src="<%=UrlUtils.autocachedUrl("/application/js/tours/tourSchemas/sabre-setup-tour.js", request) %>"></script>

<script
	src="<%=UrlUtils.autocachedUrl("/application/js/loading.js", request)%>"></script>
<script
	src="<%=UrlUtils.autocachedUrl("/application/js/market.js", request)%>"></script>
<script
	src="<%=UrlUtils.autocachedUrl("/application/js/log4javascript.js", request)%>"></script>
<script
	src="<%=UrlUtils.autocachedUrl("/application/js/logging.js", request)%>"></script>
<script
	src="<%=UrlUtils.autocachedUrl("/application/js/utils.js", request)%>"></script>
<script
	src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>