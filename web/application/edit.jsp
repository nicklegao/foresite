<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.Map"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Section"%>
<%@page import="java.util.List"%>
<%@ page import="org.owasp.esapi.ESAPI" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.util.TravelDocsStatus" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.service.SecurityKeyService" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%@ page import="au.net.webdirector.common.Defaults" %>
<%@ page import="au.net.webdirector.common.datalayer.client.ElementData" %>
<%@ page import="au.com.ci.sbe.util.UrlUtils" %><%--
  Created by IntelliJ IDEA.
  User: coreybaines
  Date: 10/5/18
  Time: 10:12 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	String travelDocsID = request.getParameter("traveldoc");
	UserAccountDataAccess uda = new UserAccountDataAccess();
	String userUsername = (String)session.getAttribute(Constants.PORTAL_USERNAME_ATTR);

	Hashtable<String, String> traveldocHT = null;
	String portalUserId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
	String companyID = (String)session.getAttribute(Constants.PORTAL_COMPANY_ID_ATTR);
	CategoryData company = uda.getCompanyForCompanyId(companyID);

	boolean isTemplate = false;
	boolean isItineraryProposal = true;
	String vKey = "";
	Proposal proposal = null;
if(travelDocsID==null){
	traveldocHT = new Hashtable<String, String>();
	traveldocHT.put(TravelDocsDataAccess.E_OWNER, company.getId());
travelDocsID = "0";
isTemplate = true;
isItineraryProposal = false;

%>

<script type="text/javascript">
	var addTemplate = true;
	var isTemplate = <%=isTemplate%>;
</script>
<%
	}
	else {
		traveldocHT = new TravelDocsDataAccess().getTravelDocById(travelDocsID);
	isTemplate = traveldocHT.get(TravelDocsDataAccess.E_PROPOSAL_STATUS).equals(TravelDocsStatus.Template.getDbText());
	proposal = new TravelDocsDataAccess().loadTravelDoc(travelDocsID);
	isItineraryProposal = proposal.getBooking() != null;
	vKey = new SecurityKeyService().getViewKey(
	companyID,
	Integer.parseInt(travelDocsID),
	traveldocHT.get(TravelDocsDataAccess.E_KEY));
	
%>
<script type="text/javascript">
	var addTemplate = false;
	var traveldocMainTitle = "<%=traveldocHT.get("ATTR_traveldocTitle")%>";
	var travelDocBookingID = "<%=traveldocHT.get("ATTR_travelBookingReference")%>";
	var isTemplate = <%=isTemplate%>;

</script>
<%
	}

//	CategoryData company = uda.getUserCompanyForUserId(portalUserId);
	Hashtable<String, String> user = uda.getUserWithId(portalUserId);

	String corporateCSS = String.format("/%s/%s/Categories/%s/gen/css-document/edit-colours.css",
			Defaults.getInstance().getStoreContext(),
			UserAccountDataAccess.USER_MODULE,
			company.getId());

	%>

<script type="text/javascript">
	var ConName = "<%=user.get(UserAccountDataAccess.E_NAME)%> <%=user.get(UserAccountDataAccess.E_SURNAME)%>";
	var ConEmail = "<%=user.get(UserAccountDataAccess.E_EMAIL_ADDRESS)%>";
	var ConPhone = "<%=user.get(UserAccountDataAccess.E_MOBILE_NUMBER)%>";
	var ConCompany = "<%=company.getName()%>"
</script>
<html>
<head>
<%
if(proposal != null) {
	Map<String, String> styleSettings = null;
	List<Section> sections = proposal.getSections();
	for(Section section:sections) {
	    if (section.getType().equalsIgnoreCase("cover")) {
	        List<Content> coverItems = section.getContent();
	        for (Content content:coverItems) {
	            if (content.getType().equalsIgnoreCase("cover")) {
	            	styleSettings = content.getCoverData().getMetadata();
	            	break;
	            }
	        }
	        break;
	    }
	}
	if(styleSettings != null) {
		%>
		<style type="text/css" id="styleSettingsCSS">
			.editor-content {
				font-family:"<%=styleSettings.get("fontFamilyBody")%>" !important;
				font-size:<%=styleSettings.get("fontSizeBody")%>px !important;
				color:<%=styleSettings.get("fontColorBody")%> !important;
			}
			.editor-content p, .editor-content table{
				font-size:<%=styleSettings.get("fontSizeBody")%>px;
				color:<%=styleSettings.get("fontColorBody")%> !important;
			}
			.editor-content ol, .editor-content ul{
				font-size:<%=styleSettings.get("fontSizeBody")%>px;
			}
			.editor-content .fr-box.fr-basic .fr-element{
				font-family:"<%=styleSettings.get("fontFamilyBody")%>" !important;
				color:<%=styleSettings.get("fontColorBody")%> !important;
			}
			.editor-content h1 {
				font-family:"<%=styleSettings.get("fontFamilyH1")%>" !important;
				font-size:<%=styleSettings.get("fontSizeH1")%>px !important;
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("customHeading1Color"))){%>color:<%=styleSettings.get("fontColorHeading1")%> !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("boldHeading1"))){%>font-weight:bold !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("uppercaseHeading1"))){%>text-transform:uppercase !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("underlineHeading1"))){%>text-decoration:underline !important;<%}%>
			}
			.editor-content h2 {
				font-family:"<%=styleSettings.get("fontFamilyH2")%>" !important;
				font-size:<%=styleSettings.get("fontSizeH2")%>px !important;
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("customHeading2Color"))){%>color:<%=styleSettings.get("fontColorHeading2")%> !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("boldHeading2"))){%>font-weight:bold !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("uppercaseHeading2"))){%>text-transform:uppercase !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("underlineHeading2"))){%>text-decoration:underline !important;<%}%>
			}
			.editor-content h3 {
				font-family:"<%=styleSettings.get("fontFamilyH3")%>" !important;
				font-size:<%=styleSettings.get("fontSizeH3")%>px !important;
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("customHeading3Color"))){%>color:<%=styleSettings.get("fontColorHeading3")%> !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("boldHeading3"))){%>font-weight:bold !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("uppercaseHeading3"))){%>text-transform:uppercase !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("underlineHeading3"))){%>text-decoration:underline !important;<%}%>
			}
			.editor-content .edit-section-title input {
				font-family:"<%=styleSettings.get("fontFamilySection")%>" !important;
				font-size:<%=styleSettings.get("fontSizeSection")%>px !important;
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("customSectionColor"))){%>color:<%=styleSettings.get("fontColorSectionTitle")%> !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("boldSectionTitle"))){%>font-weight:bold !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("uppercaseSectionTitle"))){%>text-transform:uppercase !important;<%}%>
				<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("underlineSectionTitle"))){%>text-decoration:underline !important;<%}%>
			}
		</style>
		<%
	}
}
%>
	<title>TravelDocs - Editor</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">



	<link rel="shortcut icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon">

	
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700italic,700,900,900italic" rel="stylesheet">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
	
	<!-- STYLESHEETS -->
	<style type="text/css">
		[fuse-cloak],
		.fuse-cloak {
			display: none !important;
		}
	</style>

	<link rel="stylesheet" href="/stores/_fonts/stylesheet.css" type="text/css"/>

	<!-- Icons.css -->
	<link type="text/css" rel="stylesheet" href="/application/assets/new/icons/fuse-icon-font/style.css">
	<!-- Animate.css -->
	<link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/animate.css/animate.min.css">
	<!-- PNotify -->
	<link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/pnotify/pnotify.custom.min.css">
	<!-- Nvd3 - D3 Charts -->
	<link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/nvd3/build/nv.d3.min.css" />
	<!-- Fuse Html -->
	<link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/fuse-html/fuse-html.min.css" />
	<!-- Swiper -->
	<link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/swiper/css/swiper.min.css" />
	<!-- Datatable -->
	<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css" />
	<link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.material.min.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.2/css/fixedHeader.dataTables.min.css">
	<%--MODAL BOOTSTRAP--%>
	<link rel="stylesheet" href="/application/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" type="text/css"/>
	<link rel="stylesheet" href="/application/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" type="text/css"/>
	<!-- Main CSS -->
	<link type="text/css" rel="stylesheet" href="/application/assets/new/css/main.css">
	<link type="text/css" rel="stylesheet" href="/application/assets/new/css/itinerary.css">
	<link type="text/css" rel="stylesheet" href="/application/assets/new/css/editor.css">
	<link type="text/css" rel="stylesheet" href="/application/assets/new/css/coverpage.css">
	<link rel="stylesheet" href="/application/assets/plugins/switchery/switchery.css" />
	<link rel='stylesheet' href='/application/assets/plugins/spectrum/spectrum.css' />
    <link rel="stylesheet" href="/application/assets/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/application/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.css" />
	<link rel='stylesheet' href='/application/assets/new/css/zeroRisk.css' />
	
	<link rel="stylesheet" href="/application/assets/plugins/paper-collapse/paper-collapse.min.css" />
	<!-- / STYLESHEETS -->

	<!-- JAVASCRIPT -->
	<!-- jQuery -->
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script src="/application/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js"></script>
	<!-- Mobile Detect -->
	<script type="text/javascript" src="/application/assets/new/vendor/mobile-detect/mobile-detect.min.js"></script>
	<!-- Popper.js -->
	<script type="text/javascript" src="/application/assets/new/vendor/popper.js/index.js"></script>
	<!-- Bootstrap -->
	<script type="text/javascript" src="/application/assets/new/vendor/bootstrap/bootstrap.min.js"></script>
	<!-- Nvd3 - D3 Charts -->
	<script type="text/javascript" src="/application/assets/new/vendor/d3/d3.min.js"></script>
	<script type="text/javascript" src="/application/assets/new/vendor/nvd3/build/nv.d3.min.js"></script>
	<!-- Data tables -->
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.material.min.js"></script>  <%--/application/assets/new/vendor/datatables.net/js/jquery.dataTables.min.js--%>
	<%--<script type="text/javascript" src="/application/assets/new/vendor/datatables-responsive/js/dataTables.responsive.js"></script>--%>
	<!-- PNotify -->
	<script type="text/javascript" src="/application/assets/new/vendor/pnotify/pnotify.custom.min.js"></script>
	<!-- Fuse Html -->
	<script type="text/javascript" src="/application/assets/new/vendor/fuse-html/fuse-html.min.js"></script>
	<!-- Main JS -->
	<script type="text/javascript" src="/application/assets/new/js/main.js"></script>
	<!-- Interact.js -->
	<script src="https://unpkg.com/interactjs@1.3.3/dist/interact.min.js"></script>

	<!-- Include Editor style.. -->
	<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/plugins/table.css",request) %>" />
	<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/froala_style.min.css",request) %>" />
	<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/froala_editor.pkgd.min.css",request) %>" />

	<!-- Include JS file.. -->
	<script src="/application/assets/plugins/froala_editor_3.0.0/js/froala_editor.pkgd.min.js"></script>
	<script src="/application/assets/plugins/froala_editor_old/custom/config-proposalFreetext.js"></script>
    <%--<script src="/application/assets/plugins/froala-editor/js/plugins/table.js"></script>--%>
	<%--<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.1/js/froala_editor.min.js'></script>--%>

	<link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/fontawesome-pro-5.8.1-web/css/all.css">
	<!-- / JAVASCRIPT -->
	<script>
		var travelDocID = <%=travelDocsID%>
			var vKey = "";
	</script>
	<style>
		.cover.content-box-hidden .proposalTitle, .cover.content-box-hidden .coverData {
			background-color: transparent;
		}
	</style>
</head>

<!-- start: MAIN CONTAINER -->
<input type="hidden" id="param-proposal-id" value="<%=ESAPI.encoder().encodeForHTMLAttribute(travelDocsID) %>"/>
<input type="hidden" id="param-e-key" value="<%=ESAPI.encoder().encodeForHTMLAttribute(request.getParameter("e-key")) %>"/>
<input type="hidden" id="doc-v-key" value="<%=ESAPI.encoder().encodeForHTMLAttribute(vKey) %>"/>
<input type="hidden" id="proposal-template" value="<%=isTemplate %>"/>
<body>

<div class="editor-header">
	<div id="logo-group">
        <span id="logo" class="logo-text">
            <!-- <img src="/application/images/qCloud.png" alt="QuoteCloud"> -->
            <span>Td</span>
        </span>
<%--		<h3><span class="documentTag header-company" tag="companyName"><%=company.getName()%></span> </h3>--%>
	</div>
	<div class="main-editor-buttons">
		<button type="button" class="btn btn-default saveTravelDocAction">
			Save
		</button>
		<button type="button" class="btn btn-default button-green saveAndPreviewAction">
			<i class="fa fa-paper-plane"></i> &nbsp;Save &amp; Preview
		</button>
		<button type="button" class="btn btn-close" id="editorCloseButton" onclick="javascript:window.close();">
			 <i class="fa fa-times"></i> &nbsp;Close
		</button>
	</div>
</div>
<div class="froala-tool-holder" style="display: none;top:50px;min-height: 30px;position: absolute;width: calc(100% - 604px);left: 228px;z-index: 1002;background: #fff;color: #43454e;box-shadow: 0 2px 3px rgba(0, 0, 0, 0.15);">
</div>

<div class="editor-section-panel">


	<div class="section-tab-area">
		<ul id="document-section-tabs" class="nav nav-tabs tab-bricky">

		</ul>
	</div>
	<div class="section-tab-area-add">
		<ul id="document-section-add-tab" class="nav nav-tabs tab-bricky">
			<li class="addTabs">
				<div type="button" class="section-button" id="new-section-button">
					<div class="tab-label"> <i class="fa fa-plus"></i><span> Add Section </span></div>
				</div>
			</li>
		</ul>
	</div>

</div>

<div class="editor-options-panel">
	<div class="editor-blocks-holder">
		<div class="nav-panel">
			<ul class="nav-panel-set">
				<li class="nav-panel-icon" id="coverpage-nav-icon" style="display: none">
					<a class="active nav-icon" id="coverpage-nav-icon-button">
						<i class="far fa-file-image fa-3x"></i>
						<span class="nav-icon-title">Cover Page</span>
					</a>
				</li>
				<li class="nav-panel-icon" id="style-nav-icon">
					<a class="nav-icon" id="style-nav-icon-button">
						<i class="fas fa-paint-brush fa-3x"></i>
						<span class="nav-icon-title">Style</span>
					</a>
				</li>
				<li class="nav-panel-icon" id="blocks-nav-icon">
					<a class="active nav-icon" id="blocks-nav-icon-button">
						<i class="fas fa-th-large fa-3x"></i>
						<span class="nav-icon-title">Blocks</span>
					</a>
				</li>
				<li class="nav-panel-icon" id="text-nav-icon">
					<a class=" nav-icon" id="text-nav-icon-button">
						<i class="fas fa-font fa-3x"></i>
						<span class="nav-icon-title">Text</span>
					</a>
				</li>
				<li class="nav-panel-icon" id="images-nav-icon">
					<a class=" nav-icon" id="images-nav-icon-button">
						<i class="fas fa-images fa-3x"></i>
						<span class="nav-icon-title">Images</span>
					</a>
				</li>
				<li class="nav-panel-icon" id="videos-nav-icon">
					<a class=" nav-icon" id="videos-nav-icon-button">
						<i class="fab fa-youtube fa-3x"></i>
						<span class="nav-icon-title">Videos</span>
					</a>
				</li>
				<%--<li class="nav-panel-icon" id="pdf-nav-icon">--%>
					<%--<a class=" nav-icon">--%>
						<%--<i class="fas fa-file-pdf fa-3x"></i>--%>
						<%--<span class="nav-icon-title">PDF</span>--%>
					<%--</a>--%>
				<%--</li>--%>
				<%--<li class="nav-panel-icon" id="payments-nav-icon">--%>
					<%--<a class=" nav-icon" id="payments-nav-icon-button">--%>
						<%--<i class="fas fa-hand-holding-usd fa-3x"></i>--%>
						<%--<span class="nav-icon-title">Payments</span>--%>
					<%--</a>--%>
				<%--</li>--%>
				<%--<li class="nav-panel-icon" id="download-nav-icon">--%>
					<%--<a href="/pdf/<%=ESAPI.encoder().encodeForURL(travelDocsID) %>.pdf?traveldoc=<%=ESAPI.encoder().encodeForURL(travelDocsID) %>&email=<%=ESAPI.encoder().encodeForURL(request.getParameter("email")) %>&v-key=<%=ESAPI.encoder().encodeForURL(vKey) %>" target="_blank" class=" nav-icon" id="download-nav-icon-button">--%>
						<%--<i class="fas fa-download fa-3x"></i>--%>
						<%--<span class="nav-icon-title">Download</span>--%>
					<%--</a>--%>
				<%--</li>--%>
				<%--<li class="nav-panel-icon">--%>
					<%--<a class=" nav-icon">--%>
						<%--<i class="fas fa-ellipsis-h fa-3x"></i>--%>
						<%--<span class="nav-icon-title">More</span>--%>
					<%--</a>--%>
				<%--</li>--%>
			</ul>
		</div>
	</div>
</div>

<div class="editor-blocks-panel visible">
	<div class="side-bar-cover-container" style="display: none;">
		<div class="block-head">
			<div class="content-cus-title">
				<div class="title-back-button"><i class="fas fa-angle-left"></i></div>
				<h6 class="content-title">Cover Page Customizer</h6>
			</div>
		</div>
		<div class="block-holder">
			<div class="headed-scroller" style="display: none;">
			</div>
			<div class="block-container" id="coverpage-main-container">
				<div class="content-title"><h6>Cover Page Styles</h6></div>
				<h7>Cover Page:</h7>
				<div class="block-button-container" id="cover-page-chooser-button" data-id="coverpage-chooser">
					<div class="button-content cover-page-chooser-button-main">
						<i class="far fa-file-image fa-sm" style="position: absolute;top: 10px;"></i>
						<span id="coverpage-title"></span>
						<i class="fas fa-angle-right fa-sm" style="position:absolute;right:14px;top:11px"></i>
					</div>
				</div>
				<h7>Content Box Style:</h7>
				<div class="block-button-container" id="cover-page-visibility-switch">
					<div class="button-content">
						<input type="checkbox" class="js-switch" id="coverpageContentBoxSwitch" checked />
						<span id="coverpage-content-box-style-title">Choose content box color:</span>
						<input type="text" id="cover-page-content-box-color" class="form-control" name="coverPageContentBoxColor"  value="#f00808">
					</div>
				</div>
				<h7>Content Box Text Colour:</h7>
				<div class="block-button-container" id="cover-page-title-text-color">
					<div class="button-content">
						<span id="coverpage-content-box-title-text-title">Choose title text color:</span>
						<input type="text" id="cover-page-content-box-title-text-color" class="form-control" name="coverPageContentBoxTitleColor"  value="#fff">
					</div>
				</div>
				<div class="block-button-container" id="cover-page-content-text-color">
					<div class="button-content">
						<span id="coverpage-content-box-content-text-title">Choose content text color:</span>
						<input type="text" id="cover-page-content-box-content-text-color" class="form-control" name="coverPageContentBoxContentColor"  value="#f00808">
					</div>
				</div>
				<div class="content-title"><h6>Cover Page Content</h6></div>
				<h7>Change Title:</h7>
				<div class="block-input-container" id="cover-page-title-input">
					<input class="input-content" type="text" id="proposalTitleInput" data-field="travelDocsTitle" name="proposalTitleInput" value="" placeholder="TravelDoc Title">
				</div>
				<h7>Change Subtitle:</h7>
				<div class="block-input-container" id="cover-page-title-input">
					<input class="input-content" type="text" id="proposalSubtitleInput" data-field="travelDocsSubTitle" name="proposalTitleInput" value="" placeholder="TravelDoc Subtitle">
				</div>
				<h7>Add extra info:</h7>
				<div class="block-input-container" id="cover-page-title-input">
					<input class="input-content" type="text" id="proposalExtraInput" data-field="travelDocExtra" name="proposalTitleInput" value="" placeholder="TravelDoc Extra Info">
				</div>
				<%--<h7>Change Customer Name:</h7>
				<div class="block-input-container" id="cover-page-title-input">
					<input class="input-content" type="text" id="proposalCusNameInput" data-field="travelDocsCusName" name="proposalTitleInput" value="" placeholder="Customer Name">
				</div>
				<h7>Change consultant:</h7>
				<div class="block-input-container" id="cover-page-title-input">
					<input class="input-content" type="text" id="proposalConNameInput" data-field="travelDocsConName" name="proposalTitleInput" value="<%=user.get(UserAccountDataAccess.E_NAME)%> <%=user.get(UserAccountDataAccess.E_SURNAME)%>" placeholder="Consultant Name">
				</div>
				<h7>Change consultant company:</h7>
				<div class="block-input-container" id="cover-page-title-input">
					<input class="input-content" type="text" id="proposalConComInput" data-field="travelDocsConCompany" name="proposalTitleInput" value="<%=company.getName()%>" placeholder="Consultant Company">
				</div>--%>
				<%
					if (!isTemplate) {
				%>
				<h7>Change consultant Phone:</h7>
				<div class="block-input-container" id="cover-page-title-input">
					<input class="input-content" type="text" id="proposalConPhoneInput" data-field="travelDocsConPhone" name="proposalTitleInput" value="<%=user.get(UserAccountDataAccess.E_MOBILE_NUMBER)%>" placeholder="Consultant Phone">
				</div>
				<h7>Add additional email:</h7>
				<div class="block-input-container" id="cover-page-title-input">
					<input class="input-content" type="text" id="proposalAddEmailInput" data-field="travelDocsConEmails" name="proposalTitleInput" value="<%=user.get(UserAccountDataAccess.E_EMAIL_ADDRESS)%>" placeholder="Consultant Email">
				</div>
				<%
					}
				%>
				<%--<h7>Logo:</h7>--%>
				<%--<div class="block-button-container" id="cover-page-chooser-button" data-id="coverpage-chooser">--%>
					<%--<div class="button-content">--%>
						<%--<i class="far fa-file-image fa-sm"></i>--%>
						<%--<span id="coverpage-title">StrandBags</span>--%>
						<%--<i class="fas fa-angle-right fa-sm" style="position:absolute;right:14px;top:11px"></i>--%>
					<%--</div>--%>
				<%--</div>--%>
			</div>
			<div class="block-container" id="coverpage-dyn-loader" style="display: none;">
				
			</div>
		</div>
	</div>
	<div class="side-bar-style-container" style="display: none">
		<div class="block-head">
			<div class="content-cus-title">
				<div class="title-back-button"><i class="fas fa-angle-left"></i></div>
				<h6 class="content-title">Style Settings</h6>
			</div>
		</div>
		<div class="block-holder">
			<div class="headed-scroller" style="display: none;">
			</div>
			<div class="block-container" id="template-style-container" >
				<jsp:include page="/application/editStyle.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<div class="side-bar-loaded-container">
		<%--<div class="block-head">--%>
			<%--<div class="search-widget">--%>
				<%--<div class="search-auto">--%>
					<%--<input class="search-box-input" placeholder="Search your content...">--%>
					<%--<span class="search-icon">--%>
                    <%--<i class="fas fa-search"></i>--%>
                <%--</span>--%>
					<%--<span>--%>

                <%--</span>--%>
				<%--</div>--%>

			<%--</div>--%>
		<%--</div>--%>
			<div class="block-head">
				<div class="content-cus-title">
					<div class="title-back-button"><i class="fas fa-angle-left"></i></div>
					<h6 class="content-title">Content Blocks</h6>
				</div>
			</div>
		<div class="block-holder">
			<div class="headed-scroller" style="display: none;">
			</div>
			<div class="block-container">
				<div class="black-square-container">
					<div class="content-new-loader"></div>
				</div>
			</div>
		</div>
		<div class="block-footer">
			<%--<button type="button" class="btn btn-default open-options-modal">--%>
				<%--&nbsp;Search Marketplace--%>
			<%--</button>--%>
		</div>
	</div>

</div>

<div class="editor-content window-min-height panel-padding-sections panel-padding-blocks">
	<div class="editor-content-scroller">
		<div class="doc-effect-holder">
			<div class="blosck-droppable-area">
				<div class="doc-page-area">


				</div>
			</div>

		</div>
	</div>
</div>

<%--MODAL TIME BOYSSSS!!!!!!--%>
<!-- Modal -->
<div id="saveContentModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modal Header</h4>
			</div>
			<div class="modal-body">
				<p>Some text in the modal.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<div id="saveTemplateModal" class="modal top-modal fade" tabindex="-1">
	<form id="form-add-template">
		<div class="modal-header" style="border-bottom: 1px solid rgb(204, 204, 204);">
			<h2 class="modal-title">Save Template As</h2>
		</div>
		<div class="modal-body">

			<div class="form-horizontal">
				<div class="form-group">
					<label for="profile-name" class="col-sm-3 control-label">
						Title: </label>
					<div class="col-sm-12">
						<input type="text" id="template-title"
							   class="form-control" name="templateTitle" placeholder="Title" />
					</div>
				</div>
				<div class="form-group">
					<label for="profile-name" class="col-sm-3 control-label">
						Description: </label>
					<div class="col-sm-12">
	              <textarea rows="5" class="form-control" id="template-description"
							name="templateDescription" placeholder="Description"></textarea>
					</div>
				</div>
				<input type="hidden" id="save-template-proposal-id" name="proposalId" value="-1" />
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Close <i class="fa fa-times"></i></button>
			<button type="submit" template="true" class="btn btn-success">Save <i class="fa fa-save"></i></button>
		</div>
	</form>
</div>
<jsp:include page="/application/js/content-modules/edit_modules.jsp"></jsp:include>
<jsp:include page="dialog/backgroundLoading.jsp"></jsp:include>


<script src="/webdirector/free/csrf-token/init"></script>
<script src="/application/assets/plugins/moment/moment.js"></script>
<script src="/application/assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/application/assets/plugins/jquery-placeholder/js/jquery.placeholder.js"></script>
<script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
<script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script src="/application/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"></script>
<script src="/application/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/application/assets/plugins/bootstrap-datepicker/js/date.format.js"></script>
<script src="/application/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/application/assets/plugins/nprogress/nprogress.js"></script>
<script src="/application/assets/js/ui-modals.js"></script>
<script src='/application/assets/plugins/spectrum/spectrum.js'></script>
<script src="/application/assets/plugins/switchery/switchery.js"></script>
<script src="/application/assets/plugins/lz-string/lz-string.js"></script>
<script src="/application/js/md5.js"></script>
<script src="/application/assets/plugins/masonry/masonry.pkgd.min.js"></script>
<script src="/application/assets/plugins/imagesloaded/imagesloaded.pkgd.min.js"></script>


<script src="<%=UrlUtils.autocachedUrl("/application/js/log4javascript.js", request) %>"></script>
<script src="<%=UrlUtils.autocachedUrl("/application/js/logging.js", request) %>"></script>
<script src="<%=UrlUtils.autocachedUrl("/application/js/loading.js", request) %>"></script>
<script type="text/javascript" src="/application/assets/new/js/editor.js"></script>
<script type="text/javascript" src="/application/js/content-modules/modules.js"></script>

<script src="/application/assets/plugins/paper-collapse/paper-collapse.min.js"></script>
</body>
</html>


