
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
	String redirect = request.getParameter(Constants.PORTAL_LOGIN_REDIRECT_PARAM);

	String loginStatus = (String) session.getAttribute(Constants.LOGIN_STATUS);
	session.removeAttribute(Constants.LOGIN_STATUS);
	session.removeAttribute(Constants.LOGIN_ERROR);

	if (userId != null && userId.length() > 0) {
		if (redirect == null) {
			redirect = "/dashboard";
		}

		response.sendRedirect(redirect);
		return;
	}
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>


	<!--[if lt IE 9]>
	<script src="/application/assets/plugins/respond.js"></script>
	<![endif]-->
	<script src="/application/assets/plugins/jquery-placeholder/js/jquery.placeholder.js"></script>
	<script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="/application/assets/plugins/select2/select2.js"></script>
	<script src="/application/js/login.js"></script>
	<script src="/application/assets/plugins/sweetalert/sweetalert.min.js"></script>

	<style>
		.wrapper {
			overflow: hidden;
			margin: 0 auto;
		}
		.login-conatiner {
			background-image: url('/application/assets/new/images/backgrounds/travel-docs-pricing-bg.png');
			background-size: cover;
			background-position: bottom;
			background-repeat: no-repeat;
			position: relative;
			color: inherit;
			border-color: rgba(255, 255, 255, 0);
			padding-top: 4rem!important;
			padding-bottom: 4rem!important;
			min-height: 800px;
			height: 100vh !important;
		}
		.login-card-container {
			margin: auto;

			/* margin: 0;
            position: absolute;
            top: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
            left: 50%;
            -ms-transform: translateX(-50%);
            transform: translateX(-50%); */

		}
		.card {
			max-width: 850px;
			border: none !important;
			box-shadow: 29px 23px 63px -34px #9fa1a299 !important;
			-webkit-box-shadow: 29px 23px 63px -34px #9fa1a299 !important;
			-moz-box-shadow: 29px 23px 63px -34px #9fa1a299 !important;
			box-shadow: 29px 23px 63px -34px #9fa1a299 !important;
		}
		.login-card-container > .card {
			margin-top: -150px;
		}
		.card-image {
			width: 100%;
			height: auto;
			border-color: rgba(255, 255, 255, 0);
			background-color: rgba(255, 255, 255, 0);
			margin-left: -20px;
			margin-top: -54px;
			margin-right: 0px;
			margin-bottom: -20px;
		}
		.md-form {
			margin-top: 1rem;
			margin-bottom: 1rem;
			text-align: center !important;
		}
		.md-form input[type=date], .md-form input[type=datetime-local], .md-form input[type=email], .md-form input[type=number], .md-form input[type=password], .md-form input[type=search-md], .md-form input[type=search], .md-form input[type=tel], .md-form input[type=text], .md-form input[type=time], .md-form input[type=url], .md-form textarea.md-textarea {
			-webkit-transition: border-color .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
			-o-transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
			transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
			transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
			outline: 0;
			-webkit-box-shadow: none;
			box-shadow: none;
			border: none;
			border-bottom: 1px solid #ced4da;
			-webkit-border-radius: 0;
			border-radius: 0;
			-webkit-box-sizing: content-box;
			box-sizing: content-box;
			background-color: transparent;
		}
		.md-form .form-control {
			margin: 0 0 .5rem;
			margin-left: -10px !important;
			-webkit-border-radius: 0;
			border-radius: 0;
			padding: .3rem 0 .55rem;
			background-image: none;
			background-color: transparent;
			height: auto;
		}
		input, input[type=text], input[type=password], input[type=email], input[type=number], input[type=search], input[type=tel], input[type=url], input[type=reset], textarea, select {
			transition: background 0.3s;
			-moz-transition: background 0.3s;
			-webkit-transition: background 0.3s;
			-o-transition: background 0.3s;
			margin: 0;
			display: inline-block;
			vertical-align: middle;
			border: 1px solid #aaa;
			border-radius: 3px;
			color: #000;
			text-decoration: inherit;
		}
		.form-control {
			display: block;
			width: 100%;
			height: calc(2.25rem + 2px);
			padding: 0.375rem 0.75rem;
			font-size: 1rem;
			line-height: 1.5;
			color: #495057;
			background-color: #fff;
			background-clip: padding-box;
			border: 1px solid #ced4da;
			border-radius: 0.25rem;
			transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
		}
		.md-form input[type]:focus:not([readonly]) {
			-webkit-box-shadow: 0 1px 0 0 #f44d00;
			box-shadow: 0 1px 0 0 #f44d00;
			border-bottom: 1px solid #f44d00;
		}
		.md-form input[type]:focus:not([readonly])+label {
			color: #f44d00;
		}
		.md-form label {
			font-size: 1rem;
		}
		label {
			display: inline-block;
			margin-bottom: 0.5rem;
		}
		.md-form label {
			position: absolute;
			top: 1.65rem;
			left: 0;
			-webkit-transition: .2s ease-out;
			-o-transition: .2s ease-out;
			transition: .2s ease-out;
			cursor: text;
			color: #757575;
		}

		.md-form label.active {
			font-size: .8rem;
			-webkit-transform: translateY(-140%);
			-ms-transform: translateY(-140%);
			transform: translateY(-140%);
		}
		#loginButton {
			background: #f44d00 !important;
			font-size: 20px !important;
			width: 150px;
			-webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
			box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
			padding: .84rem 2.14rem;
			font-size: .81rem;
			-webkit-transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
			-o-transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
			transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
			transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
			margin: .375rem;
			border: 0;
			-webkit-border-radius: .125rem;
			border-radius: .125rem;
			cursor: pointer;
			text-transform: uppercase;
			white-space: normal;
			word-wrap: break-word;
			color: #fff;

			font-weight: 400;
			text-align: center;
		}
	</style>
</head>
<body>
<div class="wrapper">
	<div class="login-conatiner flex-center row ">
		<div class="col-auto login-card-container">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-7">
							<img src="../application/assets/new/images/backgrounds/Password-Reset.png" class="card-image">
						</div>
						<div class="col-5">
							<div class="login-form-container">
								<form class="text-center form-forgot-password" method="post" style="color: #757575;" action="/api/free/forgotPassword">
									<div class="errorHandler alert alert-danger" style="display: none">
										<i class="fa fa-times-circle"></i> Errors found. Please check below.
									</div>
									<div class="form-row" style="margin-top: 36px;">
										<div class="col">
											<!-- First name -->
											<div class="md-form">
												<input type="email" name="email" id="email" class="form-control">
												<label for="email" class="">Email Address</label>
											</div>
										</div>
									</div>
									<div class="form-row" style="position:absolute;bottom:0;">
										<div class="col">
											<div class="md-form mt-5">
												<button type="submit" value="Submit" class="submit g-recaptcha btn btn-primary btn-float btn-block btn-sm waves-effect submitButton" id="loginButton">Submit</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="helper-person">
					<img src="../application/assets/new/images/backgrounds/dcperson1.png" style="
                                        position: absolute;
                                    top: 50%;
                                    right: 0;
                                    transform: translateX(65%);
                                ">
				</div>
			</div>
		</div>
	</div>
</div>


<script src="/webdirector/free/csrf-token/init"></script>
<script src="" async defer></script>
<script>
	$(document).ready(function() {

		checkInputOnLoad();

		$(document).on('focusin', '.md-form input', function(e) {
			$(this).siblings('label').addClass('active');
		});
		$(document).on('focusout', '.md-form input', function(e) {
			if ($(this).val().trim() === "") {
				$(this).siblings('label').removeClass('active');
			}
		});
	});

	function checkInputOnLoad() {
		$('.md-form input').each(function() {
			if ($(this).val().trim() !== "") {
				$(this).next().addClass('active');
			}
		});
	}
</script>
<script>

	jQuery(document).ready(function() {

		$(document).on("click", ".submitButton", function() {
			event.preventDefault();
			$(this).html('<div class="spinner-border spinner-border-sm" role="status">\n' +
					'  <span class="sr-only">Loading...</span>\n' +
					'</div>');
			$('.form-forgot-password').submit();
		});

		Login.initForgotPassword();

		if($(window).width() < 768) {
			$(".device-warning").show();
			$("input, button").prop("disabled", true);
		}
		$(window).resize(function(){
			if($(window).width() < 768) {
				$(".device-warning").show();
				$("input, button").prop("disabled", true);
			} else {
				$(".device-warning").hide();
				$("input, button").prop("disabled", false);
			}
		});
		Login.initLogin();
	});
</script>
<jsp:include page="ga-tracking.jsp"></jsp:include>
<script src="https://apis.google.com/js/platform.js" async defer></script>
</body>
</html>