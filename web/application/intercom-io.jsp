<%@page import="au.corporateinteractive.qcloud.subscription.SubscriptionPlan"%>
<%@page import="io.intercom.utils.IntercomUtils"%>
<%@page import="io.intercom.js.IntercomSettings"%>
<%@page import="io.intercom.utils.IntercomConfiguration"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%
	{
		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		
		IntercomSettings intercomSettings = new IntercomUtils().settingsForUserId(userId);
		if (SubscriptionPlan.isTrialPlan(intercomSettings.getPlanName()))
		{
			%>
			<script>
				window.intercomSettings = <%=intercomSettings.asJson() %>;
				(function()
				{
					var w = window;
					var ic = w.Intercom;
					if (typeof ic === "function")
					{
						ic('reattach_activator');
						ic('update', intercomSettings);
					}
					else
					{
						var d = document;
						var i = function()
						{
							i.c(arguments)
						};
						i.q = [];
						i.c = function(args)
						{
							i.q.push(args)
						};
						w.Intercom = i;
						function l()
						{
							var s = d.createElement('script');
							s.type = 'text/javascript';
							s.async = true;
							s.src = 'https://widget.intercom.io/widget/<%=IntercomConfiguration.sharedInstance().getAppId() %>';
							var x = d.getElementsByTagName('script')[0];
							x.parentNode.insertBefore(s, x);
						}
						if (w.attachEvent)
						{
							w.attachEvent('onload', l);
						}
						else
						{
							w.addEventListener('load', l, false);
						}
					}
				})()
			</script>
			<%
		}
	}
%>
