
<div class="panel viewable" type="attachment" fixed="false">
  <div class="panel-heading">
    <i class="fa fa-paperclip"></i> Attach file
    <div class="panel-tools">
      <div class="pageBreakPanel newPDFLabel">
        <div class="pageBreakPanel-label">Start on a<span class="pageBreakPanel-label">new page (PDF)</span></div>
      </div>
      <div class="pageBreakPanel sectionControls">
        <input type="checkbox" class="js-switch newPageSwitch" />
        <a href="#" class="btn btn-xs btn-link move-up"> <i class="fa fa-chevron-up"></i></a>
        <a href="#" class="btn btn-xs btn-link move-down"> <i class="fa fa-chevron-down"></i></a>
        <a href="#" class="btn btn-xs btn-link disabled"> <i class="fa"></i></a>
        <a href="#" class="btn btn-xs btn-link collapse"> <i class="fa fa-window-minimize"></i></a>
        <a href="#" class="btn btn-xs btn-link lock"> <i class="fa fa-unlock"></i> <span></span></a>
        <a href="#" class="btn btn-xs btn-link remove"> <i class="fa fa-times"></i></a>
      </div>
    </div>
  </div>
  <div class="panel-body">
    <div class="form-horizontal">
      <div class="form-group">
        <label for="fileTitle" class="col-sm-3 control-label">
          Title:
        </label>
        <div class="col-sm-8">
          <input type="text" class="form-control" 
                  id="fileTitle" name="fileTitle" placeholder="Title"/>
        </div>
      </div>
      <div class="form-group">
        <label for="fileDescription" class="col-sm-3 control-label">
          Description:
        </label>
        <div class="col-sm-8"> 
          <textarea class="form-control" 
                  id="fileDescription"  name="fileDescription" placeholder="Description" ></textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label">
          Attachment:
        </label>
        <div class="col-sm-8">
          <input type="hidden" id="attachmentId" />
          <input type="hidden" id="fileName" />
          <div class="dropzone">
            <div class="fallback">
            	Attachments aren't supported by your browser. This section will be automatically removed when you save this proposal.
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="embedPDF"><input type="checkbox" id="embedPDFSwitch" class="js-switch embedPDFSwitch" /><span class="embedPDFSwitch-label" style="margin-left:1em;">Embed PDF inside your proposal</span></div>
  </div>
</div>
