<div class="panel viewable">
	<div class="panel-heading">
		<i class="fa fa-remove"></i> Content removed
		<div class="panel-tools">
			<div class="pageBreakPanel newPDFLabel">
				<div class="pageBreakPanel-label">Start on a<span class="pageBreakPanel-label">new page (PDF)</span></div>
			</div>
			<div class="pageBreakPanel sectionControls">
				<input type="checkbox" class="js-switch newPageSwitch" />
				<a href="#" class="btn btn-xs btn-link disabled"> <i class="fa"></i></a>
				<a href="#" class="btn btn-xs btn-link disabled"> <i class="fa"></i></a>
				<a href="#" class="btn btn-xs btn-link disabled"> <i class="fa"></i></a>
				<a href="#" class="btn btn-xs btn-link minimum"> <i class="fa fa-window-minimize"></i></a>
				<a href="#" class="btn btn-xs btn-link disabled"> <i class="fa"></i></a>
				<a href="#" class="btn btn-xs btn-link remove"> <i class="fa fa-times"></i></a>
			</div>
		</div>
	</div>
	<div class="panel-body" contenteditable="false">
		<div class="alert alert-danger">
			<h4>
				<i class="fa fa-warning"></i> Oh No!
			</h4>
			<p>
				You tried to restore a proposal with <strong>${contentType}</strong> content that has been removed from the content library.
			</p>
		</div>
	</div>
</div>