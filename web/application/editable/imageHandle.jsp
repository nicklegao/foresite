<div class="panel viewable" type="imageAttachment" fixed="false">
  <div class="panel-heading">
    <i class="qc qc-download"></i> Attached Image
    <div class="panel-tools">
        <div class="pageBreakPanel newPDFLabel">
            <div class="pageBreakPanel-label">Start on a<span class="pageBreakPanel-label">new page (PDF)</span></div>
        </div>
        <div class="pageBreakPanel sectionControls">
            <input type="checkbox" class="js-switch" />
            <a href="#" class="btn btn-xs btn-link move-up"> <i class="fa fa-chevron-up"></i></a>
            <a href="#" class="btn btn-xs btn-link move-down"> <i class="fa fa-chevron-down"></i></a>
            <a href="#" class="btn btn-xs btn-link clone"> <i class="fa fa-clone"></i></a>
            <a href="#" class="btn btn-xs btn-link minimum"> <i class="fa fa-window-minimize"></i></a>
            <a href="#" class="btn btn-xs btn-link lock"> <i class="fa fa-unlock"></i> <span></span></a>
            <a href="#" class="btn btn-xs btn-link remove"> <i class="fa fa-times"></i></a>
        </div>
    </div>
  </div>
  <div class="panel-body">
    <div class="form-horizontal">
      <div class="form-group">
        <label for="fileTitle" class="col-sm-3 control-label">
          Title:
        </label>
        <div class="col-sm-8">
          <input type="text" class="form-control" 
                  id="fileTitle" name="fileTitle" placeholder="Title"/>
        </div>
      </div>
      <div class="form-group">
        <label for="fileDescription" class="col-sm-3 control-label">
          Description:
        </label>
        <div class="col-sm-8"> 
          <textarea class="form-control" 
                  id="fileDescription"  name="fileDescription" placeholder="Description" ></textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label">
        </label>
        <div class="col-sm-8">
          <input type="hidden" 
                  id="attachmentId" name="attachmentId" />
          <img id="image" src=""  style="max-width:100%;width:auto;height:auto;"/>
        </div>
      </div>
      
      <div class="form-group">
        <label class="col-sm-3 control-label">
        </label>
      
        <div class="col-sm-8">
	      	<%--<div class="inlineImageAlignmentButtons btn-group" role="group">--%>
			  <%--<button type="button" class="btn btn-default" data-alignment="left">--%>
			  	<%--<i class="fa fa-picture-o fa-fw" aria-hidden="true"></i>--%>
			  	<%--<i class="fa fa-align-left fa-fw"></i>--%>
			  	<%--<br/>Left--%>
			  <%--</button>--%>
			  <%--<button type="button" class="btn btn-default" data-alignment="center">--%>
			  	<%--<i class="fa fa-align-center fa-fw"></i>--%>
			  	<%--<br/>Center--%>
			  <%--</button>--%>
			  <%--<button type="button" class="btn btn-default" data-alignment="right">--%>
			  	<%--<i class="fa fa-align-left fa-fw"></i>--%>
			  	<%--<i class="fa fa-picture-o fa-fw" aria-hidden="true"></i>--%>
			  	<%--<br/>Right--%>
			  <%--</button>--%>
			<%--</div>--%>
		</div>
      </div>
    </div> 
    
  </div>
</div>
