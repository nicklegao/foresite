<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="panel viewable" type="text" dataId="${textData.id}" fixed="false">
  <div class="panel-heading" style="">
  <i class="fa fa-lock"></i> Text
    <div class="panel-tools">
        <div class="pageBreakPanel newPDFLabel">
            <div class="pageBreakPanel-label">Start on a<span class="pageBreakPanel-label">new page (PDF)</span></div>
        </div>
        <div class="pageBreakPanel sectionControls">
            <input type="checkbox" class="js-switch newPageSwitch" />
            <a href="#" class="btn btn-xs btn-link move-up"> <i class="fa fa-chevron-up"></i></a>
            <a href="#" class="btn btn-xs btn-link move-down"> <i class="fa fa-chevron-down"></i></a>
            <a href="#" class="btn btn-xs btn-link clone"> <i class="fa fa-clone"></i></a>
            <a href="#" class="btn btn-xs btn-link minimum"> <i class="fa fa-window-minimize"></i></a>
            <a href="#" class="btn btn-xs btn-link lock"> <i class="fa fa-unlock"></i> <span></span></a>
            <a href="#" class="btn btn-xs btn-link remove"> <i class="fa fa-times"></i></a>
        </div>
    </div>
  </div>
  <div class="panel-body noselect" style="background: rgb(253, 221, 230);">
    <c:out value="${textData.data}" escapeXml="false"/>
    <div class="originalTemplate"><c:out value="${textData.template}" escapeXml="false"/></div>
  </div>
</div>
