<div class="panel viewable spreadsheet-droppable " type="spreadsheet" dataId="" fixed="false">
  <div class="panel-heading">
    <i class="fa fa-table"></i> Add Spreadsheet 
    <div class="panel-tools">
      <div class="pageBreakPanel newPDFLabel">
        <div class="pageBreakPanel-label"> Start on a <span class="pageBreakPanel-label">new page (PDF)</span> </div>
      </div>
      <div class="pageBreakPanel sectionControls"> 
        <input type="checkbox" class="js-switch newPageSwitch" /> 
        <a href="#" class="btn btn-xs btn-link move-up"> <i class="fa fa-chevron-up"></i></a> 
        <a href="#" class="btn btn-xs btn-link move-down"> <i class="fa fa-chevron-down"></i></a> 
        <a href="#" class="btn btn-xs btn-link clone"> <i class="fa fa-clone"></i></a>
        <a href="#" class="btn btn-xs btn-link minimum"> <i class="fa fa-window-minimize"></i></a> 
        <a href="#" class="btn btn-xs btn-link lock"> <i class="fa fa-unlock"></i> <span></span></a>
        <a href="#" class="btn btn-xs btn-link remove"> <i class="fa fa-times"></i></a>
        
      </div>
    </div>
  </div>
  <div class="panel-body">
  	<div class="spreadsheet noScroll" style="width:100%;">
  	</div>
  </div>
</div>
