<div class="panel viewable sign-droppable " type="sign" dataId="" fixed="false">
  <div class="panel-heading">
    <i class="fa fa-pencil-square-o"></i> Add Signature
    <div class="panel-tools">
      <div class="pageBreakPanel newPDFLabel">
        <div class="pageBreakPanel-label"> Start on a <span class="pageBreakPanel-label">new page (PDF)</span> </div>
      </div>
      <div class="pageBreakPanel sectionControls"> 
        <input type="checkbox" class="js-switch newPageSwitch" /> 
        <a href="#" class="btn btn-xs btn-link move-up"> <i class="fa fa-chevron-up"></i></a> 
        <a href="#" class="btn btn-xs btn-link move-down"> <i class="fa fa-chevron-down"></i></a> 
        <a href="#" class="btn btn-xs btn-link disabled"> <i class="fa"></i></a>
        <a href="#" class="btn btn-xs btn-link minimum"> <i class="fa fa-window-minimize"></i></a>
        <a href="#" class="btn btn-xs btn-link lock"> <i class="fa fa-unlock"></i> <span></span></a>
        <a href="#" class="btn btn-xs btn-link remove"> <i class="fa fa-times"></i></a> </div>
    </div>
  </div>
  <div class="panel-body">
    <h3> <button type="button" class="btn btn-default addNewSignAction" id="addNewSign"> <i class="fa fa-plus"></i> Add New Signature </button> </h3>
    <table class="table table-bordered table-hover signTbl" id="signTable">
      <thead>
        <tr>
          <th class="text-center">Person to Sign</th>
          <th class="text-center">Action</th>
        </tr>
      </thead>
      <tbody>
        <tr id='personOne'>
          <td><select name='email' placeholder='Person To Sign' class="form-control"> </select></td>
          <td style="">
            <btn class="btn btn-sm btn-danger removeBtn" onClick="removeSignatureRow(this)"> <i class="fa fa-times-circle"></i></btn>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="panel-footer">
    <div class="row pricingTableFormatSwitches">
      <div class="col-md-4 inline-group hide-items-group">
        <h5>Signature Type</h5>
        <div class="checkbox-group"> <input type="checkbox" class="signSwitch" id="hideDrawSign"> <label style="margin: 3px 8px;" for="hideDrawSign">Draw</label> </div>
        <div class="checkbox-group"> <input type="checkbox" class="signSwitch" id="hideTypeSign"> <label style="margin: 3px 8px;" for="hideTypeSign">Type</label> </div>
        <div class="checkbox-group"> <input tabindex="5" type="checkbox" class="signSwitch" id="hideUploadSign"> <label style="margin: 3px 8px;" for="hideUploadSign">Upload</label> </div>
      </div>
      <%--
      <div class="col-md-4 inline-group hide-items-group">
        <h5>Select Your Themes</h5>
        <div class="dropdown theme-chooser">
          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"> <span class="theme-name">Theme one</span> <span class="caret"></span> </button>
          <ul class="dropdown-menu">
            <li><a href="#">Theme one</a></li>
            <li><a href="#">Theme two</a></li>
            <li><a href="#">Theme three</a></li>
            <li><a href="#">Theme four</a></li>
          </ul>
        </div>
      </div>
      --%>
    </div>
  </div>
</div>
