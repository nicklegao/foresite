<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.CustomFieldsDataAccess" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.CustomField" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.CustomFields" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Pricing" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions" %>
<%@page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager" %>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData" %>

<%

    UserAccountDataAccess uada = new UserAccountDataAccess();
    String portalUserId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
    CategoryData company = uada.getUserCompanyForUserId(portalUserId);
    CustomFieldsDataAccess cfda = new CustomFieldsDataAccess();
    CustomFields productCustomFields = cfda.getCompanyCustomFields(CustomFields.PRODUCTS, company.getId());
    TUserRolesDataAccess urda = new TUserRolesDataAccess();
    UserPermissions up = urda.getPermissions(portalUserId);
%>
<div class="panel fixed-panel viewable pricing-droppable not-last" type="generated" dataId="con-plans" dataOrder="0">
    <div class="panel-heading">
        <i class="fa fa-table"></i> Pricing Table
        <input type="hidden" name="pricingTag" value=""/>
        <div class="panel-tools">
            <!-- <a id="scrollH" href="#" class="btn btn-xs btn-link disabled"><span class="qc qc-scrollH"></span></a>  -->
            <div class="pageBreakPanel newPDFLabel">
                <div class="pageBreakPanel-label">Start on a<span class="pageBreakPanel-label">new page (PDF)</span></div>
            </div>
            <div class="pageBreakPanel sectionControls">
                <input type="checkbox" class="js-switch newPageSwitch"/>
                <a href="#" class="btn btn-xs btn-link move-up"> <i class="fa fa-chevron-up"></i></a>
                <a href="#" class="btn btn-xs btn-link move-down"> <i class="fa fa-chevron-down"></i></a>
                <a href="#" class="btn btn-xs btn-link clone"> <i class="fa fa-clone"></i></a>
                <a href="#" class="btn btn-xs btn-link minimum"> <i class="fa fa-window-minimize"></i></a>
                <a href="#" class="btn btn-xs btn-link remove"> <i class="fa fa-times"></i></a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div id="scrollH" class="scrollmore-horizontal-wrapper">
            <p>Scroll for more</p>
            <div class="mouse-horizontal"></div>
        </div>
        <div class="button-container1 libraryActions" style="margin: -5px 0 5px; z-index: 2002;">
      <%--<% if(up.hasApplyPricingDiscounts()){ %>--%>
            <%--<% } %>--%>
          <div id="margin-discounts" class="btn-group ">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="discount-label">Default Discount or Margin</span> <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">

              </ul>
          </div>
            <button type="button" class="btn btn-default addCustomPricingAction">
                <i class="fa fa-plus"></i> Add Custom Price
            </button>
            <button type="button" class="btn btn-default addProductSectionAction">
                <i class="fa fa-plus"></i> Add Sub-heading
            </button>
        </div>
        <div class="alert alert-block alert-warning missingPlans">
            <h4 class="alert-heading"><i class="fa fa-info-circle"></i> Info!</h4>
            <p>
                Please click <b>+Add Custom Price</b> to create a new price line item, or click on the 'Products' button on the right to view the product catalogue, then drag & drop some products into this Pricing Table section.
            </p>
        </div>

        <div class="requiresPlans" style="display:none;">
            <div class="scroll-holder">
                <table id="pricingTable" class="table table-hover table-striped pricingTable">
                    <!-- header -->
                    <thead>
                    <tr>
                        <th></th>
                        <th class="descriptionColumn" sortable="rowDescription"><span class="sort">Product</span></th>
                        <th width="38"></th>
                        <% 
                        for (CustomField field : productCustomFields)
                        {
          	              if(field.isOn() && (field.getId() != null)){
                        %>
                        <th class="customFieldColumn" sortable="<%=field.getId() %>"><span class="sort"><%=field.getLabel() %></span></th>
                        <% 
                          }
                        }
                        %>
                        <th class="unitPriceColumn" sortable="unitCostSelector"><span class="sort">Price</span></th>

                        <% if(!company.getBoolean(uada.C1_HIDE_TERMS_ALWAYS)) {%>
                        <th class="upfrontColumn" sortable="oneOffCheckbox" ><span class="sort">One-off</span></th>

                        <th class="termColumn" sortable="planTermSelector">
                            <span class="sort">Term</span>
                            <div id="termTypeDiv" class="dropdown termTypeDiv" style="max-width:70px">
                                <button class="btn btn-default dropdown-toggle" type="button" style="height:21px; padding:0px 6px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <span style="font-size: 12px;" id="termType"></span>
                                    <span class="caret"></span>
                                </button>
                                <ul id="termDropDown" class="dropdown-menu">
					                <% for(Pricing.TermType termType : Pricing.TermType.values()){%>
					                <li><a><%=termType%></a></li>
                                    <%}%>
                                </ul>
                            </div>
                        </th>
                        <%}%>


                        <th class="qtyColumn" sortable="qtyChooser"><span class="sort">Quantity</span></th>
                        <th class="totalPriceColumn" sortable="productTotalCost"><span class="sort">Cost</span></th>
						<%
						if(company.getBoolean(uada.C1_ENABLE_ADDITION))
						{
							%>
                        <th class="additionColumn" sortable="additionSelector"><span class="sort"> Additions </span></th>
							<%
						}
						%>
						<%
						if(company.getBoolean(uada.C1_ENABLE_TAX))
						{
							%>
                        <th class="taxColumn" sortable="taxSelector"><span class="sort"><%= company.getString(UserAccountDataAccess.C1_TAX_LABEL) %> %</span></th>
							<%
						}
						%>
						<%
						if(company.getBoolean(uada.C1_ENABLE_TAX) || company.getBoolean(uada.C1_ENABLE_ADDITION))
						{
							%>
                        <th class="totalCostColumn" sortable="productTotalCostIncTax"><span class="sort">Total Cost</span></th>
							<%
						}
						%>
                        <th class="actionColumn"><span></span></th>
                    </tr>
                    </thead>
                    <tbody class="customPricingRows standard">
                    </tbody>
                </table>
                <table id="pricingFooter" class="table table-hover table-striped pricingFooter">
                    <tr>
                        <th></th>
                        <th class="descriptionColumn"></th>
                        <th width="38"></th>
                        <th class=""></th>
                        <th class="text-right">Regular Price</th>
                        <th class="text-right">Discount Applied</th>
                        <th class="text-right">Margin Applied</th>
                        <% if(company.getBoolean(uada.C1_ENABLE_ADDITION)){ %>
                        <th class="text-right">Additions</th>
                        <% } %>
                        <% if(company.getBoolean(uada.C1_ENABLE_TAX)){ %>
                        <th class="text-right total-tax"><%=company.getString(UserAccountDataAccess.C1_TAX_LABEL) %></th>
                        <% } %>
                        <th class="text-right">Cost</th>
                        <th class="upfrontColumn"></th>
                        <th class="actionColumn"></th>
                    </tr>
                    <% if(!company.getBoolean(uada.C1_HIDE_TERMS_ALWAYS)) {%>
                    <tr class="minimumMothlyCostRow">
                        <th id="termFooter" class="text-right" colspan="4">Monthly</th>
                        <th class="text-right"><span class="price moneytaryValue">0.00</span></th>
                        <th class="text-right"><span class="discount moneytaryValue">0.00</span></th>
                        <th class="text-right"><span class="margin moneytaryValue">0.00</span></th>
                        <% if(company.getBoolean(uada.C1_ENABLE_ADDITION)){ %>
                        <th class="text-right"><span class="addition moneytaryValue">0.00</span></th>
                        <% } %>
                        <% if(company.getBoolean(uada.C1_ENABLE_TAX)){ %>
                        <th class="text-right total-tax"><span class="tax moneytaryValue">0.00</span></th>
                        <% } %>
                        <th class="text-right"><span class="cost moneytaryValue">0.00</span></th>
                        <th colspan="2"></th>
                    </tr>
                    <tr class="fullTermCostRow">
                        <th class="text-right" colspan="4">Full Term</th>
                        <th class="text-right"><span class="price moneytaryValue">0.00</span></th>
                        <th class="text-right"><span class="discount moneytaryValue">0.00</span></th>
                        <th class="text-right"><span class="margin moneytaryValue">0.00</span></th>
                        <% if(company.getBoolean(uada.C1_ENABLE_ADDITION)){ %>
                        <th class="text-right"><span class="addition moneytaryValue">0.00</span></th>
                        <% } %>
                        <% if(company.getBoolean(uada.C1_ENABLE_TAX)){ %>
                        <th class="text-right total-tax"><span class="tax moneytaryValue">0.00</span></th>
                        <% } %>
                        <th class="text-right"><span class="cost moneytaryValue">0.00</span></th>
                        <th colspan="2"></th>
                    </tr>
                    <tr class="onOffCostRow">
                        <th class="text-right" colspan="4">One Off</th>
                        <th class="text-right"><span class="price moneytaryValue">0.00</span></th>
                        <th class="text-right"><span class="discount moneytaryValue">0.00</span></th>
                        <th class="text-right"><span class="margin moneytaryValue">0.00</span></th>
                        <% if(company.getBoolean(uada.C1_ENABLE_ADDITION)){ %>
                        <th class="text-right"><span class="addition moneytaryValue">0.00</span></th>
                        <% } %>
                        <% if(company.getBoolean(uada.C1_ENABLE_TAX)){ %>
                        <th class="text-right total-tax"><span class="tax moneytaryValue">0.00</span></th>
                        <% } %>
                        <th class="text-right"><span class="cost moneytaryValue">0.00</span></th>
                        <th colspan="2"></th>
                    </tr>
                    <% } %>

                    <tr class="grandTotalRow">
                        <th class="text-right" colspan="4">Total</th>
                        <th class="text-right"><span class="price moneytaryValue">0.00</span></th>
                        <th class="text-right"><span class="discount moneytaryValue">0.00</span></th>
                        <th class="text-right"><span class="margin moneytaryValue">0.00</span></th>
                        <% if(company.getBoolean(uada.C1_ENABLE_ADDITION)){ %>
                        <th class="text-right"><span class="addition moneytaryValue">0.00</span></th>
                        <% } %>
                        <% if(company.getBoolean(uada.C1_ENABLE_TAX)){ %>
                        <th class="text-right total-tax"><span class="tax moneytaryValue" data-percentage="<%=company.getString(UserAccountDataAccess.C1_TAX_PERCENTAGE) %>" data-label="<%=company.getString(UserAccountDataAccess.C1_TAX_LABEL) %>">0.00</span></th>
                        <% } %>
                        <th class="text-right"><span class="cost moneytaryValue">0.00</span></th>
                        <th colspan="2"></th>
                    </tr>
                    <tr class="overallTotalRow">
                        <th class="text-right" colspan="4">Overall Total</th>
                        <th class="text-right"><span class="price moneytaryValue">0.00</span></th>
                        <th class="text-right"><span class="discount moneytaryValue">0.00</span></th>
                        <th class="text-right"><span class="margin moneytaryValue">0.00</span></th>
                        <% if(company.getBoolean(uada.C1_ENABLE_ADDITION)){ %>
                        <th class="text-right"><span class="addition moneytaryValue">0.00</span></th>
                        <% } %>
                        <% if(company.getBoolean(uada.C1_ENABLE_TAX)){ %>
                        <th class="text-right total-tax"><span class="tax moneytaryValue">0.00</span></th>
                        <% } %>
                        <th class="text-right"><span class="cost moneytaryValue">0.00</span></th>
                        <th colspan="2"></th>
                    </tr>
                </table>
            </div>

            <div class="pricingTableFormatSwitches">

                <div class="inline-group hide-items-group hideTerm">
                    <h5>Columns</h5>
                    <div class="checkbox-group">
                        <input tabindex="5" type="checkbox" id="hidePriceColumn" data-target-class="unitPriceColumn">
                        <label style="margin: 3px 8px;" for="hidePriceColumn">Price</label>
                    </div>
                    <% if(!company.getBoolean(uada.C1_HIDE_TERMS_ALWAYS)) {%>
                    <div class="checkbox-group">
                        <input tabindex="5" type="checkbox" id="hideTermColumn" data-target-class="termColumn">
                        <label style="margin: 3px 8px;" for="hideTermColumn">Term</label>
                    </div>
                    <% } %>
                    <div class="checkbox-group">
                        <input tabindex="5" type="checkbox" id="hideQtyColumn" data-target-class="qtyColumn">
                        <label style="margin: 3px 8px;" for="hideQtyColumn">Quantity</label>
                    </div>
                    <div class="checkbox-group">
                        <input tabindex="6" type="checkbox" id="hideTotalPriceColumn" data-target-class="totalPriceColumn">
                        <label style="margin: 3px 8px;" for="hideTotalPriceColumn">Cost</label>
                    </div>
                    <% if(company.getBoolean(uada.C1_ENABLE_ADDITION)) {%>
                    <div class="checkbox-group">
                        <input tabindex="5" type="checkbox" id="hideAdditionColumn" data-target-class="additionColumn">
                        <label style="margin: 3px 8px;" for="hideAdditionColumn">Additions</label>
                    </div>
                    <% } %>
                    <% if(company.getBoolean(UserAccountDataAccess.C1_ENABLE_TAX)){ %>
                    <div class="checkbox-group">
                        <input tabindex="6" type="checkbox" id="hideTaxColumn" data-target-class="taxColumn">
                        <label style="margin: 3px 8px;" for="hideTaxColumn">Tax</label>
                    </div>
                    <% } %>
                    <% if(company.getBoolean(uada.C1_ENABLE_ADDITION) || company.getBoolean(UserAccountDataAccess.C1_ENABLE_TAX)) {%>
                    <div class="checkbox-group">
                        <input tabindex="7" type="checkbox" id="hideTotalCostColumn" data-target-class="totalCostColumn">
                        <label style="margin: 3px 8px;" for="hideTotalCostColumn">Total Cost</label>
                    </div>
                    <% } %>
                </div>

                <div class="inline-group hide-items-group">
                    <h5>Totals</h5>
                    <div class="checkbox-group">
                        <input tabindex="5" type="checkbox" id="hideGrandTotalPriceRow" data-target-class="grandTotalRow">
                        <label style="margin: 3px 8px;" for="hideGrandTotalPriceRow">Table Grand Total</label>
                    </div>
                    <div class="checkbox-group hideOverallTotalPriceRow">
                        <input tabindex="6" type="checkbox" id="hideOverallTotalPriceRow" data-target-class="overallTotalRow">
                        <label style="margin: 3px 8px;" for="hideOverallTotalPriceRow">Overall Total Row</label>
                    </div>
                    <div class="checkbox-group hideOnOffCostRow">
                        <input tabindex="6" type="checkbox" id="hideOnOffCostRow" data-target-class="onOffCostRow">
                        <label style="margin: 3px 8px;" for="hideOnOffCostRow">One Off Total Row</label>
                    </div>
                    <% if(!company.getBoolean(uada.C1_HIDE_TERMS_ALWAYS)) {%>
                    <div class="checkbox-group hideMinimumMothlyCostRow">
                        <input tabindex="6" type="checkbox" id=hideMinimumMonthlyCost data-target-class="minimumMothlyCostRow">
                        <label style="margin: 3px 8px;" for="hideMinimumMonthlyCost">Minimum <span id="termFooter">Monthly</span> Cost</label>
                    </div>
                    <div class="checkbox-group hideFullTermCostRow">
                        <input tabindex="6" type="checkbox" id=hideFullTermCost data-target-class="fullTermCostRow">
                        <label style="margin: 3px 8px;" for="hideFullTermCost">Full Term Cost</label>
                    </div>
                    <% } %>
                </div>

                <div class="inline-group hide-items-group">
                    <h5>Others</h5>
                    <div class="checkbox-group hideDiscountFromPriceRow">
                        <input tabindex="6" type="checkbox" id="hideDiscountFromPriceRow" data-target-class="barDiscount">
                        <label style="margin: 3px 8px;" for="hideDiscountFromPriceRow">Price Discounts Visible on Proposals</label>
                    </div>
                    <% if(company.getBoolean(UserAccountDataAccess.C1_ENABLE_TAX)){ %>
                    <div class="checkbox-group">
                        <input tabindex="5" type="checkbox" id="removeTax" data-target-class="taxRow">
                        <label style="margin: 3px 8px;" for="removeTax">Remove Tax</label>
                    </div>
                    <% } %>
                </div>


                <% if (productCustomFields.hasOnCustomFields() > 0 ) { %>
                <div class="inline-group hide-items-group">
                    <h5>Custom Fields</h5>
                    <% int i = 0;
                        for (CustomField field : productCustomFields)
                    {
                        if(field.isOn() && (field.getId() != null)){
                        	i++;
                    %>
                    <div class="checkbox-group hideDiscountFromPriceRow">
                        <input tabindex="6" type="checkbox" id="<%="hideCustomColumn" + i%>" data-target-class="<%=field.getId() %>">
                        <label style="margin: 3px 8px;" for="<%="hideCustomColumn" + i%>"><%=field.getLabel() %></label>
                    </div>
                    <% }
                    }%>
                </div>
                <% } %>



            </div>

        </div>

    </div>
</div>
