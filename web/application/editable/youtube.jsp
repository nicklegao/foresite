<%@page import="au.net.webdirector.common.Defaults"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.VideoDetails" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	VideoDetails videoData = (VideoDetails) request.getAttribute("videoData");
%>
<%-- <div class="panel viewable" type="video" dataId="${videoData.id}" fixed="false"> --%>
<div class="panel editable" type="video" dataId="${videoData.id}" fixed="false">
	<div class="panel-heading">
		<i class="fa fa-play"></i> Inline Video
		<div class="panel-tools">
			<div class="pageBreakPanel newPDFLabel">
				<div class="pageBreakPanel-label">Start on a<span class="pageBreakPanel-label">new page (PDF)</span></div>
			</div>
			<div class="pageBreakPanel sectionControls">
				<input type="checkbox" class="js-switch newPageSwitch" />
				<% if (!"false".equals(request.getParameter("movable"))) { %>
				<a href="#" class="btn btn-xs btn-link move-up"> <i class="fa fa-chevron-up"></i></a>
				<a href="#" class="btn btn-xs btn-link move-down"> <i class="fa fa-chevron-down"></i></a>
				<% } else {%>
				<a href="#" class="btn btn-xs btn-link disabled"> <i class="fa"></i></a>
				<a href="#" class="btn btn-xs btn-link disabled"> <i class="fa"></i></a>
				<% } %>
				<a href="#" class="btn btn-xs btn-link disabled"> <i class="fa"></i></a>
                <a href="#" class="btn btn-xs btn-link minimum"> <i class="fa fa-window-minimize"></i></a>
				<a href="#" class="btn btn-xs btn-link lock"> <i class="fa fa-unlock"></i> <span></span></a>
				<% if (!"false".equals(request.getParameter("deletable"))) { %>
				<a href="#" class="btn btn-xs btn-link remove"> <i class="fa fa-times"></i></a>
				<% } else { %>
				<a href="#" class="btn btn-xs btn-link disabled"> <i class="fa"></i></a>
				<% } %>
			</div>
		</div>
	</div>

	<div class="panel-froala" style="list-style-position:inside;">
		${videoData.caption}
	</div>

	<div class="freetext-inline-image clearfix" style="margin: 10px" contenteditable="false">
		<input type="hidden" id="attachmentId" <%if(videoData != null) { %> value="${videoData.id}" <%} %>/>
		<iframe class="videoId" width="560" height="315" src="<c:out value="${videoData.source}" />" frameborder="0" allowfullscreen></iframe>



		<div class="radial" id="radialImageAlignmentButtons" class="radialImageAlignmentButtons">
			<%--First button allowing the user to select the position of the image--%>
			<span class="bar" id="fa-4" style="font-size: 7px;font-family: 'Open Sans';">
				<div class="holder slide-menu imageAlignmentEditor" style="font-size: 13px;">
					<input type="hidden" name="itemAlignment">
					<input type="hidden" name="type" value="video">
					<input type="hidden" name="itemSize" value="video">
					<div class="slide-menu-item slide-menu-button">
						<div class="dropdown">
							<div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<i style="font-size: 8px;" class="fa fa-arrow-right fa-fw"></i>
								<i style="font-size: 8px; margin-left:-3px;" class="fa fa-picture-o fa-fw"></i>
								<i style="font-size: 8px; margin-left:-3px;" class="fa fa-arrow-left fa-fw"></i>
								<br>ALIGNMENT
							</div>
							<ul class="dropdown-menu">
								<li data-alignment="left">
								   <a href="#">
									   <i style="font-size: 8px;margin-left:6px;" class="fa fa-picture-o fa-fw"></i>
									   <i style="font-size: 8px;margin-left:-3px;" class="fa fa-arrow-left fa-fw"></i>
									   Align Left
								   </a>
								</li>
								<li data-alignment="right">
								   <a href="#">
									   <i style="font-size: 8px;margin-left:6px;" class="fa fa-picture-o fa-fw"></i>
									   <i style="font-size: 8px;margin-left:-3px;" class="fa fa-arrow-right fa-fw"></i>
									   Align Right
								   </a>
								</li>
								<li data-alignment="center">
								   <a href="#">
									   <i style="font-size: 8px;" class="fa fa-arrow-right fa-fw"></i>
									   <i style="font-size: 8px; margin-left:-3px;" class="fa fa-picture-o fa-fw"></i>
									   <i style="font-size: 8px; margin-left:-3px;" class="fa fa-arrow-left fa-fw"></i>
									   Align Center</a>
								</li>
								<li data-alignment="wide">
								   <a href="#">
									   <i style="font-size: 8px;" class="fa fa-arrow-left fa-fw"></i>
									   <i style="font-size: 8px; margin-left:-3px;" class="fa fa-picture-o fa-fw"></i>
									   <i style="font-size: 8px; margin-left:-3px;" class="fa fa-arrow-right fa-fw"></i>
									   Align Wide</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="slide-menu-item slide-menu-button">
						<div class="dropdown">
							<div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<i class="fa fa-picture-o"> </i>
								<i class="fa fa-align-justify"> </i>
								<br>POSITION
							</div>
							<ul class="dropdown-menu">
								<li data-alignment="topleft-intext">
								   <a href="#">
									   <span style="float: left; line-height: 8px; margin-right: 2px;">
									   <div>
											<i class="fa fa-picture-o fa-fw" style="font-size: 8px;" aria-hidden="true"></i>
											<i class="fa fa-align-left fa-fw" style="font-size: 8px; margin-left:-5px"></i>
										</div>
										<div>
											<i class="fa fa-align-justify fa-fw" style="font-size: 8px;"></i>
											<i class="fa fa-align-left fa-fw" style="font-size: 8px; margin-left:-5px"></i>
										</div>
									   </span>
									   Top Left in Text
								   </a>
								</li>
								<li data-alignment="topright-intext">
								   <a href="#">
									   <span style="float: left; line-height: 8px; margin-right: 2px;">
									   <div>
											<i class="fa fa-align-left fa-fw" style="font-size: 8px;"></i>
											<i class="fa fa-picture-o fa-fw" style="font-size: 8px; margin-left: -5px;" aria-hidden="true"></i>
										</div>
										<div>
											<i class="fa fa-align-justify fa-fw" style="font-size: 8px;"></i>
											<i class="fa fa-align-left fa-fw" style="font-size: 8px; margin-left:-5px"></i>
										</div>
									   </span>
									   Top Right in Text
								   </a>
								</li>
								<li data-alignment="top">
								   <a href="#">
									   <span style="float: left; line-height: 8px; margin-right: 2px;">
									   <div>
											<i class="fa fa-picture-o fa-fw" style="font-size: 8px;" aria-hidden="true"></i>
									   </div>
									   <div>
											<i class="fa fa-align-justify fa-fw" style="font-size: 8px;"></i>
										</div>
									   </span>
									   Above Text
								   </a>
								</li>
								<li data-alignment="below">
								   <a href="#">
									   	<span style="float: left; line-height: 8px; margin-right: 2px;">
										<div>
											<i class="fa fa-align-justify fa-fw" style="font-size: 8px;"></i>
										</div>
									    <div>
											<i class="fa fa-picture-o fa-fw" style="font-size: 8px;" aria-hidden="true"></i>
										</div>
										</span>
									   	Below Text
								   </a>
								</li>
							</ul>
						</div>
					</div>
					<div class="slide-menu-item slide-menu-button">
						<div class="dropdown">
							<div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<i class="fa fa-picture-o"> </i>
								<i class="fa fa-align-justify"> </i>
								<br>SIZE
							</div>
							<ul class="dropdown-menu">
								<li data-size="small">
								   <a href="#">
									   <i style="font-size: 8px;" class="fa fa-picture-o fa-fw"></i>
									   Small
								   </a>
								</li>
								<li data-size="medium">
								   <a href="#">
									   <i style="font-size: 8px;" class="fa fa-picture-o fa-fw"></i>
									   Medium
								   </a>
								</li>
								<li data-size="wide">
								   <a href="#">
									   <i style="font-size: 8px;" class="fa fa-arrow-left fa-fw"></i>
									   <i style="font-size: 8px; margin-left:-3px;" class="fa fa-picture-o fa-fw"></i>
									   <i style="font-size: 8px; margin-left:-3px;" class="fa fa-arrow-right fa-fw"></i>
									   Wide
								   </a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</span>
			<%--END First button allowing the user to select the position of the image--%>
			<button class="fab fab-4 ripple">
				<div class="hamburger--slider">
					<div class="hamburger-box" style="margin-top:5px;">
						<div class="hamburger-inner"></div>
					</div>
				</div>
			</button>
		</div>
		<%--END New radial material design button--%>
	</div>

	<div class="inlineImage-addText">
		<button class="btn btn-primary addText" style="width:150px;"
				data-toggle="tooltip" data-placement="right"
				title="Add text block to this video."><i class="fa fa-keyboard-o"></i> Add Text</button>
	</div>
</div>
