<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.media.ImageDetails"%>

<%
  ImageDetails imageData = (ImageDetails) request.getAttribute("imageData");
  Boolean imageContentAvailable = (Boolean)request.getAttribute("contentAvailable");
  Boolean isDestination = (Boolean)request.getAttribute("isDestination");

  System.out.println(isDestination);
  if(imageContentAvailable == null) {
    imageContentAvailable = true;
  }
  if(isDestination != null && isDestination) { %>
<div class="panel editable" type="destination" fixed="false">
    <% } else { %>
  <div class="panel editable textblock" type="editor" fixed="false">
    <% } %>
    <div class="panel-heading">
      <% if(isDestination != null && isDestination) { %>
      <i class="fa fa-plane"></i> <span>Destination Guide</span>
      <% } else { %>
      <i class="fa fa-keyboard-o"></i> <span>Free text</span>
      <% } %>

      <div class="panel-tools">
        <div class="pageBreakPanel newPDFLabel">
          <div class="pageBreakPanel-label">Start on a<span class="pageBreakPanel-label">new page (PDF)</span></div>
        </div>
        <div class="pageBreakPanel sectionControls">
          <input type="checkbox" class="js-switch newPageSwitch" />
          <% if (!"false".equals(request.getParameter("movable"))) { %>
          <a href="#" class="btn btn-xs btn-link move-up"> <i class="fa fa-chevron-up"></i></a>
          <a href="#" class="btn btn-xs btn-link move-down"> <i class="fa fa-chevron-down"></i></a>
          <% } else {%>
          <a href="#" class="btn btn-xs btn-link disabled"> <i class="fa"></i></a>
          <a href="#" class="btn btn-xs btn-link disabled"> <i class="fa"></i></a>
          <% } %>
          <a href="#" class="btn btn-xs btn-link clone"> <i class="fa fa-clone"></i></a>
          <a href="#" class="btn btn-xs btn-link minimum"> <i class="fa fa-window-minimize"></i></a>
          <a href="#" class="btn btn-xs btn-link lock"> <i class="fa fa-unlock"></i> <span></span></a>
          <% if (!"false".equals(request.getParameter("deletable"))) { %>
          <a href="#" class="btn btn-xs btn-link remove"> <i class="fa fa-times"></i></a>
          <% } else { %>
          <a href="#" class="btn btn-xs btn-link disabled"> <i class="fa"></i></a>
          <% } %>
        </div>
      </div>
    </div>

    <div class="panel-froala" style="list-style-position:inside;">
      ${textData.data}
    </div>
    <div class="freetext-inline-image clearfix" contenteditable="false">
      <input type="hidden" id="attachmentId" <%if(imageData != null) { %> value="${imageData.id}" <%} %>/>
      <input id="imageUpload" type="file" accept="image/gif,image/jpeg,image/png" style="display:none;"/>
      <button class="btn btn-primary choseFile" style="width:150px;"
              data-toggle="tooltip" data-placement="right"
              title="Add an image to this text block."><i class="fa fa-file-image-o"></i> Add Image</button>
      <button class="btn btn-default pull-right cropImage" style="width:150px;"><i class="fa fa-upload"></i> Crop &amp; Upload</button>
      <button class="btn btn-link crop-loading"
              title="Uploading"><i class="fa fa-spin fa-spinner "></i> Image Uploading</button>
      <img id="image" <%if(imageContentAvailable) { %> src="${initParam.storeContext}${imageData.path}" <%} %> style="max-width:100%; display:none;border:1px dashed #DDD" />
      <img id="JcropPhotoFrame" style="max-width:100%;"/>

      <%--<button type="button" class="btn btn-danger removeFreetextImageAttachment" style="width:150px;"><i class="fa fa-trash"></i> Delete Image</button>--%>


      <div class="radial" id="radialImageAlignmentButtons" class="radialImageAlignmentButtons">
        <%--First button allowing the user to select the position of the image--%>
        <span class="bar" id="fa-4" style="font-size: 7px;font-family: 'Open Sans';">
				<div class="holder slide-menu imageAlignmentEditor" style="font-size: 13px;">
					<input type="hidden" name="itemAlignment">
					<input type="hidden" name="type" value="images">
					<div class="slide-menu-item slide-menu-button">
						<div class="dropdown">
							<div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<i style="font-size: 8px;" class="fa fa-arrow-right fa-fw"></i>
								<i style="font-size: 8px; margin-left:-3px;" class="fa fa-picture-o fa-fw"></i>
								<i style="font-size: 8px; margin-left:-3px;" class="fa fa-arrow-left fa-fw"></i>
								<br>ALIGNMENT
							</div>
							<ul class="dropdown-menu">
								<li data-alignment="left">
								   <a href="#">
									   <i style="font-size: 8px;margin-left:6px;" class="fa fa-picture-o fa-fw"></i>
									   <i style="font-size: 8px;margin-left:-3px;" class="fa fa-arrow-left fa-fw"></i>
									   Align Left
								   </a>
								</li>
								<li data-alignment="right">
								   <a href="#">
									   <i style="font-size: 8px;margin-left:6px;" class="fa fa-picture-o fa-fw"></i>
									   <i style="font-size: 8px;margin-left:-3px;" class="fa fa-arrow-right fa-fw"></i>
									   Align Right
								   </a>
								</li>
								<li data-alignment="center">
								   <a href="#">
									   <i style="font-size: 8px;" class="fa fa-arrow-right fa-fw"></i>
									   <i style="font-size: 8px; margin-left:-3px;" class="fa fa-picture-o fa-fw"></i>
									   <i style="font-size: 8px; margin-left:-3px;" class="fa fa-arrow-left fa-fw"></i>
									   Align Center</a>
								</li>
								<li data-alignment="wide">
								   <a href="#">
									   <i style="font-size: 8px;" class="fa fa-arrow-left fa-fw"></i>
									   <i style="font-size: 8px; margin-left:-3px;" class="fa fa-picture-o fa-fw"></i>
									   <i style="font-size: 8px; margin-left:-3px;" class="fa fa-arrow-right fa-fw"></i>
									   Align Wide</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="slide-menu-item slide-menu-button">
						<div class="dropdown">
							<div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<i class="fa fa-picture-o"> </i>
								<i class="fa fa-align-justify"> </i>
								<br>POSITION
							</div>
							<ul class="dropdown-menu">
								<li data-alignment="topleft-intext">
								   <a href="#">
									   <span style="float: left; line-height: 8px; margin-right: 2px;">
									   <div>
											<i class="fa fa-picture-o fa-fw" style="font-size: 8px;" aria-hidden="true"></i>
											<i class="fa fa-align-left fa-fw" style="font-size: 8px; margin-left:-5px"></i>
										</div>
										<div>
											<i class="fa fa-align-justify fa-fw" style="font-size: 8px;"></i>
											<i class="fa fa-align-left fa-fw" style="font-size: 8px; margin-left:-5px"></i>
										</div>
									   </span>
									   Top Left in Text
								   </a>
								</li>
								<li data-alignment="topright-intext">
								   <a href="#">
									   <span style="float: left; line-height: 8px; margin-right: 2px;">
									   <div>
											<i class="fa fa-align-left fa-fw" style="font-size: 8px;"></i>
											<i class="fa fa-picture-o fa-fw" style="font-size: 8px; margin-left: -5px;" aria-hidden="true"></i>
										</div>
										<div>
											<i class="fa fa-align-justify fa-fw" style="font-size: 8px;"></i>
											<i class="fa fa-align-left fa-fw" style="font-size: 8px; margin-left:-5px"></i>
										</div>
									   </span>
									   Top Right in Text
								   </a>
								</li>
								<li data-alignment="top">
								   <a href="#">
									   <span style="float: left; line-height: 8px; margin-right: 2px;">
									   <div>
											<i class="fa fa-picture-o fa-fw" style="font-size: 8px;" aria-hidden="true"></i>
									   </div>
									   <div>
											<i class="fa fa-align-justify fa-fw" style="font-size: 8px;"></i>
										</div>
									   </span>
									   Above Text
								   </a>
								</li>
								<li data-alignment="below">
								   <a href="#">
									   	<span style="float: left; line-height: 8px; margin-right: 2px;">
										<div>
											<i class="fa fa-align-justify fa-fw" style="font-size: 8px;"></i>
										</div>
									    <div>
											<i class="fa fa-picture-o fa-fw" style="font-size: 8px;" aria-hidden="true"></i>
										</div>
										</span>
									   	Below Text
								   </a>
								</li>
							</ul>
						</div>
					</div>
					<div class="slide-menu-item">
						<label style="margin-bottom: 3px">Width</label>
						<div>
							<input type="number" name="itemWidth" min="0" max="100" value="100" style="padding:1px; font-size: inherit;">%
							<%--<datalist id="numList">--%>
								<%--<option>25</option>--%>
								<%--<option>50</option>--%>
								<%--<option>75</option>--%>
								<%--<option>100</option>--%>
							<%--</datalist>--%>
						</div>
					</div>
				</div>
			</span>
        <%--END First button allowing the user to select the position of the image--%>
        <button class="fab fab-4 ripple">
          <div class="hamburger--slider">
            <div class="hamburger-box" style="margin-top:5px;">
              <div class="hamburger-inner"></div>
            </div>
          </div>
        </button>
      </div>
      <%--END New radial material design button--%>

      <div id="content-removed" class="alert alert-danger" style="display:<%if(!imageContentAvailable) { %>block<%} else { %>none<% } %>;">
        <h4>
          <i class="fa fa-warning"></i> Oh No!
        </h4>
        <p>
          You tried to restore a proposal with <strong>image</strong> content that has been removed from the content library.
        </p>
      </div>
    </div>
    <div class="inlineImage-addText">
      <button class="btn btn-primary addText" style="width:150px;"
              data-toggle="tooltip" data-placement="right"
              title="Add text block to this image."><i class="fa fa-keyboard-o"></i> Add Text</button>
    </div>

  </div>

