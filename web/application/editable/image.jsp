<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="panel viewable" type="image" dataId="${imageData.id}" fixed="false">
  <div class="panel-heading">
    <i class="fa fa-image"></i> Inline Image
    <div class="panel-tools">
		<div class="pageBreakPanel newPDFLabel">
			<div class="pageBreakPanel-label">Start on a<span class="pageBreakPanel-label">new page (PDF)</span></div>
		</div>
		<div class="pageBreakPanel sectionControls">
			<input type="checkbox" class="js-switch newPageSwitch" />
			<a href="#" class="btn btn-xs btn-link move-up"> <i class="fa fa-chevron-up"></i></a>
			<a href="#" class="btn btn-xs btn-link move-down"> <i class="fa fa-chevron-down"></i></a>
			<a href="#" class="btn btn-xs btn-link clone"> <i class="fa fa-clone"></i></a>
			<a href="#" class="btn btn-xs btn-link minimum"> <i class="fa fa-window-minimize"></i></a>
			<a href="#" class="btn btn-xs btn-link lock"> <i class="fa fa-unlock"></i> <span></span></a>
			<a href="#" class="btn btn-xs btn-link remove"> <i class="fa fa-times"></i></a>
		</div>
    </div>
  </div>
  
  <div class="panel-body">
  	<c:if test="${imageData.displayTitle}">
  		<h3><c:out value="${imageData.title}" /></h3>
	</c:if>
    
    
    <img alt="<c:out value="${imageData.title}" />"
         width="560" 
         src="<c:out value="${initParam.storeContext}" /><c:out value="${imageData.path}" />">
    
    ${imageData.description}
    
    <%--<div class="inlineImageAlignmentButtons btn-group" role="group">--%>
	  <%--<button type="button" class="btn btn-default" data-alignment="left">--%>
	  	<%--<i class="fa fa-picture-o fa-fw" aria-hidden="true"></i>--%>
	  	<%--<i class="fa fa-align-left fa-fw"></i>--%>
	  	<%--<br/>Left--%>
	  <%--</button>--%>
	  <%--<button type="button" class="btn btn-default" data-alignment="center">--%>
	  	<%--<i class="fa fa-align-center fa-fw"></i>--%>
	  	<%--<br/>Center--%>
	  <%--</button>--%>
	  <%--<button type="button" class="btn btn-default" data-alignment="right">--%>
	  	<%--<i class="fa fa-align-left fa-fw"></i>--%>
	  	<%--<i class="fa fa-picture-o fa-fw" aria-hidden="true"></i>--%>
	  	<%--<br/>Right--%>
	  <%--</button>--%>
	<%--</div>--%>
	
  </div>
</div>