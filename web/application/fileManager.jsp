<!DOCTYPE html>
<!--[if lt IE 9]><html class="bad-ie" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.control.StripeController"%>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.LinkedList" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Locale" %>
<%@page import="java.util.Map.Entry" %>
<%@page import="java.util.Set" %>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.google.gson.Gson" %>
<%@page import="au.com.ci.sbe.util.UrlUtils" %>
<%@ page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager" %>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%@ page import="au.net.webdirector.common.datalayer.client.ElementData" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.*" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.control.UserController" %>
<%@ page import="au.com.ci.system.classfinder.StringUtil" %>
<html lang="en">
<!--<![endif]-->
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="false" compressJavaScript="false" compressCss="true">
    <head>

        <%


            TUserAccountDataAccess uada = new TUserAccountDataAccess();
            UserAccountDataAccess uda = new UserAccountDataAccess();

            String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
            String userUsername = (String)session.getAttribute(Constants.PORTAL_USERNAME_ATTR);
            CategoryData company = uada.getUserCompanyForUserId(userId);
//            uada.updateCompanySettings(company, request.getServletContext());
            String libraryType = request.getParameter("libraryType");


            ElementData userDetails = uada.getUserWithId(userId);

//This is an example notification that your proposal has been viewed
//	FirebaseService.getInstance().sendNotificationToUser(userId, "Proposal " + "29292" + " has been viewed", "Proposal " + "20202" + " has been opened and if currently being viewed by your customer.", false);

            TUserRolesDataAccess urda = new TUserRolesDataAccess();
            UserPermissions up = urda.getPermissions(userId);

            String[] accessRights = uada.getUserAccessRights(userId);
            List<ElementData> userAccess = uada.getUsers(accessRights);
            List<HashMap<String, String>> userAccessNames = new LinkedList<HashMap<String, String>>();
            for (ElementData u : userAccess)
            {
                HashMap<String, String> details = new HashMap();
                details.put("value", u.getId());
                details.put("label",String.format("%s %s", u.getString(UserAccountDataAccess.E_NAME), u.getString(UserAccountDataAccess.E_SURNAME)));
                userAccessNames.add(details);
            }

// Javascript flags
            boolean[] trialExpiredVars = UserController.isTrialPeriodOver(session);
            int expireInDays = new UserAccountDataAccess().trialPeriodOverInWeek(company);
            int expireInDaysCounter = new UserAccountDataAccess().trialPerioddayCounter(company);
            boolean trialExpired = trialExpiredVars[0];
            Set<Entry<String, ElementData>> uCount = uada.getCompanyUsers(company.getId()).entrySet();
            int userCount = 1;
            if (uCount != null) {
                userCount = uCount.size();
            }
            Boolean trialAlmostExpired = trialExpiredVars[1];
            boolean noPhoneNumber = false;
            if (StringUtils.isBlank(userDetails.getString(UserAccountDataAccess.E_MOBILE_NUMBER))) {
                noPhoneNumber = true;
            }
            boolean showFirstLogin = false; //(!company.getBoolean(TUserAccountDataAccess.TUTORIAL_FIRST_ACCESS) && up.hasManageSettingsPermission());
//			CustomFields customFields = cfda.getCompanyCustomFields(CustomFields.PROPOSALS, company.getId());
//			CustomFields productCustomFields = cfda.getCompanyCustomFields(CustomFields.PRODUCTS, company.getId());


//Proposal count for tip guide
//            int countTotal = pdda.countProposalsTotal(accessRights);
            boolean hasProposalsBoolean = false;
//            if (countTotal > 0) {
//                hasProposalsBoolean = true;
//            }

            boolean hasUserAccess = up.hasManageUsersPermission();

//            DiskUsageDataAccess duda = new DiskUsageDataAccess();
//            ElementData diskUsageData = duda.getDiskUsageForCompanyId(company.getString("Category_id"));
            boolean trialAccount = StringUtils.equalsIgnoreCase(company.getString("attrdrop_pricingplan"), "TRIAL");


            if (trialExpired) {
                new EmailBuilder().prepareAccountReaccessedEmail(userDetails).addTo(userDetails.getString("attr_headline")).doSend();
            }


            if (!up.hasManageSettingsPermission()) {
                response.sendRedirect("/dashboard");
                return;
            }

        %>

        <script>
			var expireInDaysCounter = <%=expireInDaysCounter %>;
			var hasUserAccess = <%=hasUserAccess %>;
			var trialExpired = <%=trialExpired %>;
			var trialAlmostExpired = <%=trialAlmostExpired %>;

			var needSetupPayment = false; <%--<%= StripeController.needSetupPayment(session) %>;--%>

			var expireInDays = <%=expireInDays %>;
			var noPhoneNumber = <%=noPhoneNumber %>;
			var showFirstLogin = <%=showFirstLogin %>;
			var userCount = <%=userCount %>;

			var userAccess = <%=new Gson().toJson(userAccessNames) %>;

			<%--var dateFormatPattern = "<%= DateFormatUtils.getInstance(company.getString(UserAccountDataAccess.C1_DATE_FORMAT), Locale.forLanguageTag(company.getString(UserAccountDataAccess.C1_LOCALE))).getJsPattern() %>";--%>

			var tutorialFirstLoad = "<%= uda.isUsersFirstAccess(userUsername)%>";
			var userEmail = "<%= userUsername %>";
			var tab4Shown;

			<%--var accountSuspended = <%= StripeService.isCompanySuspended(company.getId()) %>;--%>
			var hasManageSubscriptionsPermission = <%= up.hasManageUsersPermission() %>;

			var libraryType = "<%= request.getParameter("libraryType") %>";

        </script>

        <title>TravelDocs - Dashboard</title>
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="shortcut icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon">

        <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700italic,700,900,900italic" rel="stylesheet">

        <!-- STYLESHEETS -->
        <style type="text/css">
            .content-wrapper {
                height: 100%;
            }
            .content.custom-scrollbar {
                height: 100%;
            }
            [fuse-cloak],
            .fuse-cloak {
                display: none !important;
            }
            body {
                background-color: white !important;
            }

            tr.ds-selected td {
                background: rgba(0, 0, 0, 0.21);
            }

            #file-manager.page-layout.simple.right-sidebar > .page-content-wrapper .page-header {
                height: 9rem;
                min-height: 9rem;
                max-height: 9rem;
            }
        </style>
        <!-- Icons.css -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/icons/fuse-icon-font/style.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/fontawesome-pro-5.8.1-web/css/all.css" />
        <!-- Animate.css -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/animate.css/animate.min.css">
        <!-- PNotify -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/pnotify/pnotify.custom.min.css">
        <!-- Nvd3 - D3 Charts -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/nvd3/build/nv.d3.min.css" />
        <!-- Fuse Html -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/fuse-html/fuse-html.min.css" />
        <!-- Swiper -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/swiper/css/swiper.min.css" />
        <!-- Datatable -->
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css" />
        <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.material.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.2/css/fixedHeader.dataTables.min.css">
        <!-- Main CSS -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/css/main.css">
        <link type="text/css" rel="stylesheet" href="/application/assets/new/css/dashboard.css">



        <!-- <link rel="stylesheet" type="text/css" media="screen" href="/application/assets/plugins/bootstrap-qcloud/css/bootstrap.min.css"> -->
        <link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/all.css">
        <link rel="stylesheet" href="/application/assets/plugins/bootstrap-daterangepicker/daterangepicker.css">

        <link rel="stylesheet" href="/application/assets/plugins/qcloud-iconfont/style.css" />
        <link rel="stylesheet" href="/application/assets/fonts/style.css" />
        <link rel="stylesheet" href="/application/assets/plugins/iCheck/skins/all.css" />

        <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/css/styles.css", request) %>" /><!-- Try get rid of this -->
        <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/css/library.css", request) %>" /><!-- Try get rid of this -->
        

        <link rel="stylesheet" href="/application/assets/css/print.css" media="print" />

        <!-- For this page only -->
        <link rel="stylesheet" href="/application/assets/plugins/DataTables/media/css/DT_bootstrap.css" />
        <link rel="stylesheet" href="/application/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" />
        <link rel="stylesheet" href="/application/assets/plugins/DataTables/buttons/css/buttons.dataTables.min.css" />
        <link rel="stylesheet" href="/application/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" />
        <link rel="stylesheet" href="/application/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" />
        <link rel="stylesheet" href="/application/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css">
        <link rel="stylesheet" href="/application/assets/plugins/jquery-fixedheadertable/css/defaultTheme.css" />
        <link rel="stylesheet" href="/application/assets/plugins/sweetalert/sweetalert.css" />
        <link rel="stylesheet" href="/application/assets/plugins/swalExtend/swalExtend.css" />
        <link rel="stylesheet" href="/application/assets/plugins/hopscotch-0.2.5/css/hopscotch.css">
        <link rel="stylesheet" href="/application/assets/plugins/dynatable.0.3.1/jquery.dynatable.css">
        <link rel="stylesheet" href="/application/assets/plugins/dropzone/downloads/css/dropzone.css" />
        <link rel="stylesheet" href="/application/assets/plugins/Jcrop/css/jquery.Jcrop.min.css" />

        <!-- <link rel="stylesheet" href="/application/css/modal.css" type="text/css"/> -->
        <link rel="stylesheet" href="/stores/_fonts/stylesheet.css" type="text/css"/>
        <link rel="stylesheet" href="/application/assets/fonts/sign-fonts.css" />

        <%--<link type="text/css" rel="stylesheet" href="/application/assets/materialize/css/materialize.min.css"  media="screen,projection"/>--%>
        <!-- / STYLESHEETS -->

<!-- Froala Editor -->
<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala-editor/css/froala_style.min.css",request) %>" />
<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala-editor/css/froala_editor.pkgd.min.css",request) %>" />
<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala-editor/css/plugins/table.min.css",request) %>" />

        <!-- JAVASCRIPT -->
        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
        <script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.3.min.js"></script>
        <!-- Mobile Detect -->
        <script type="text/javascript" src="/application/assets/new/vendor/mobile-detect/mobile-detect.min.js"></script>
        <!-- Popper.js -->
        <script type="text/javascript" src="/application/assets/new/vendor/popper.js/index.js"></script>
        <!-- Bootstrap -->
        <script type="text/javascript" src="/application/assets/new/vendor/bootstrap/bootstrap.min.js"></script>
        <!-- Nvd3 - D3 Charts -->
        <script type="text/javascript" src="/application/assets/new/vendor/d3/d3.min.js"></script>
        <script type="text/javascript" src="/application/assets/new/vendor/nvd3/build/nv.d3.min.js"></script>
        <!-- Data tables -->
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.material.min.js"></script>  <%--/application/assets/new/vendor/datatables.net/js/jquery.dataTables.min.js--%>
            <%--<script type="text/javascript" src="/application/assets/new/vendor/datatables-responsive/js/dataTables.responsive.js"></script>--%>

        <script type="text/javascript" src="/application/assets/plugins/dragselect/DragSelect.js"></script>
        <!-- PNotify -->
        <script type="text/javascript" src="/application/assets/new/vendor/pnotify/pnotify.custom.min.js"></script>
        <!-- Fuse Html -->
        <script type="text/javascript" src="/application/assets/new/vendor/fuse-html/fuse-html.min.js"></script>
        <!-- Main JS -->
        <script type="text/javascript" src="/application/assets/new/js/main.js"></script>
        <!-- / JAVASCRIPT -->

    </head>

    <body class="layout layout-vertical layout-left-navigation layout-below-toolbar">
    <main>
        <div id="wrapper">

            <jsp:include page="/application/include/common-side-menu.jsp"></jsp:include>

            <div class="content-wrapper">

                <jsp:include page="/application/include/common-top-menu.jsp"></jsp:include>

                <div class="content-wrapper">
                    <div class="content custom-scrollbar">

                        <div id="file-manager" class="page-layout simple right-sidebar library-<%= request.getParameter("libraryType") %> level-1">

                            <div class="page-content-wrapper custom-scrollbar">

                                <!-- HEADER -->
                                <div class="page-header bg-primary text-auto p-6">

                                    <!-- HEADER CONTENT-->
                                    <div class="header-content d-flex flex-column justify-content-between">

                                        <!-- TOOLBAR -->
                                        <div class="toolbar row no-gutters justify-content-between">

                                            <button type="button" class="btn btn-icon back-button">
                                                <i class="icon icon-backburger back-button-icon" style="display: none"></i>
                                            </button>

                                            <span class="h4"><%=StringUtil.isNullOrEmpty(libraryType) ? "TravelDocs Library" : StringUtils.capitalize(libraryType)%></span>

                                            <div class="right-side row no-gutters">

<%--                                                <button type="button" class="btn btn-icon">--%>
<%--                                                    <i class="icon icon-magnify"></i>--%>
<%--                                                </button>--%>

                                            </div>

                                        </div>
                                        <!-- / TOOLBAR -->

                                        <!-- BREADCRUMB -->
                                        <%--<div class="breadcrumb text-truncate row no-gutters align-items-center pl-0 pl-sm-20">--%>

                                            <%----%>

                                            <%--&lt;%&ndash;<i class="icon-chevron-right separator"></i>&ndash;%&gt;--%>

                                            <%--&lt;%&ndash;<span class="h4">Documents</span>&ndash;%&gt;--%>

                                        <%--</div>--%>
                                        <!-- / BREADCRUMB -->

                                    </div>
                                    <!-- / HEADER CONTENT -->

                                    <!-- ADD FILE BUTTON -->
                                    <div class="file-add-container button-fab-right" style="position: relative">
                                        <%-- <button id="add-file-button" type="button" class="btnMain btn btn-danger btn-fab fuse-ripple-ready" aria-label="Add file" style="top: 0;left: 0;">
                                            <i id="addFileOpenClose" class="icon icon-plus"></i>
                                        </button>
                                        <button id="add-folder-button" type="button" data-toggle="menuButtonTooltip" data-placement="top" title="Create Folder" class="btn1 btn btn-secondary btn-fab btn-sm fuse-ripple-ready" aria-label="Create Folder" >
                                            <i class="fa fa-folder"></i>
                                        </button>
                                        <button id="add-image-button" type="button" data-toggle="menuButtonTooltip" data-placement="top" title="Upload Image" class="btn1 btn btn-secondary btn-fab btn-sm fuse-ripple-ready" aria-label="Add Image" >
                                            <i class="icon icon-file-image-box"></i>
                                        </button>
                                        <button id="add-cover-button" type="button" data-toggle="menuButtonTooltip" data-placement="top" title="Upload Cover" class="btn1 btn btn-secondary btn-fab btn-sm fuse-ripple-ready" aria-label="Add Cover" >
                                            <i class="icon icon-file-image-box"></i>
                                        </button>
                                        <input type="file" id="addCover" accept="image/*" style="display:none;" />
                                        <button id="add-video-button" type="button" data-toggle="menuButtonTooltip" data-placement="top" title="Link Video" class="btn1 btn btn-secondary btn-fab btn-sm fuse-ripple-ready" aria-label="Add Video" >
                                            <i class="icon icon-youtube-play"></i>
                                        </button>
                                        <button id="add-text-button" type="button" data-toggle="menuButtonTooltip" data-placement="top" title="Create Text" class="btn1 btn btn-secondary btn-fab btn-sm fuse-ripple-ready" aria-label="Add Text" >
                                            <i class="icon icon-clipboard-text"></i>
                                        </button>
                                        <button id="add-pdf-button" type="button" data-toggle="menuButtonTooltip" data-placement="top" title="Upload PDF" class="btn1 btn btn-secondary btn-fab btn-sm fuse-ripple-ready" aria-label="Add PDF" >
                                            <i class="icon icon-file-pdf-box"></i>
                                        </button>
                                        <button id="add-spreadsheet-button" type="button" data-toggle="menuButtonTooltip" data-placement="top" title="Upload Spreadsheet" class="btn1 btn btn-secondary btn-fab btn-sm fuse-ripple-ready" aria-label="Add Spreadsheet" >
                                            <i class="icon icon-spreadsheet"></i>
                                        </button>
                                        <button id="add-coverphoto-button" type="button" class="btn1 btn btn-secondary btn-fab btn-sm fuse-ripple-ready" data-toggle="menuButtonTooltip" data-placement="top" title="Upload Cover Photo" aria-label="Add Cover Photo" >
                                            <i class="icon icon-file-image"></i>
                                        </button> --%>
                                        <button id="add-project-button" type="button" data-toggle="menuButtonTooltip" data-placement="top" title="Add Project" class="btn1 btn btn-secondary btn-fab btn-sm fuse-ripple-ready" aria-label="Add Project" >
                                            <i class="icon icon-clipboard-text"></i>
                                        </button>
                                        <!-- <a href="template" style="-webkit-appearance: inherit;" target="_blank" data-toggle="menuButtonTooltip" data-placement="top"  title="Create Template" class="btn1 btn btn-secondary btn-fab btn-sm fuse-ripple-ready" id="add-template-button" type="button"><i class="icon icon-file-multiple"></i></a> -->
                                        <%--<button id="add-template-button" type="button" data-toggle="menuButtonTooltip" data-placement="top" title="Create Template" class="btn5 btn btn-secondary btn-fab btn-sm fuse-ripple-ready" aria-label="Add Template" >--%>
                                            <%--<i class="icon icon-file-multiple"></i>--%>
                                        <%--</button>--%>
                                    </div>
                                    <!-- / ADD FILE BUTTON -->

                                </div>
                                <!-- / HEADER -->

                                <!-- CONTENT -->
                                <div class="page-content custom-scrollbar" style="height: calc(100% - 155px);overflow-y: scroll;">
                                    <jsp:include page="/application/dialog/contentLibrary/contentLibraryList.jsp"></jsp:include>
                                    <!-- LIST VIEW -->
                                    <table class="table list-view" id="contentTable" style="display: none;">

                                        <thead>

                                        <tr>
                                            <th></th>
                                            <th>Name</th>
                                            <th class="d-none d-md-table-cell">Type</th>
                                            <th class="d-none d-lg-table-cell">Last Modified</th>
                                            <th class="d-table-cell d-none">Actions</th>
                                        </tr>

                                        </thead>

                                        <tbody class="SubFileBody">

                                        </tbody>

                                        <tbody class="mainFileBody">

                                        <tr data-type="text">
                                            <td class="file-icon">
                                                <i class="icon-archive"></i>
                                            </td>
                                            <td class="name">Texts</td>
                                            <td class="type d-none d-md-table-cell">folder</td>
                                            <td class="last-modified d-none d-lg-table-cell">July 8, 2015</td>
                                            <td class="d-table-cell d-none">
                                                <button type="button" class="btn btn-icon" data-fuse-bar-toggle="file-manager-info-sidebar">
                                                    <i class="fas fa-ellipsis-v fa-lg"></i>
                                                </button>
                                            </td>
                                        </tr>

                                        <tr data-type="image">
                                            <td class="file-icon">
                                                <i class="icon-archive"></i>
                                            </td>
                                            <td class="name">Images</td>
                                            <td class="type d-none d-md-table-cell">folder</td>
                                            <td class="last-modified d-none d-lg-table-cell">July 8, 2015</td>
                                            <td class="d-table-cell d-none">
                                                <button type="button" class="btn btn-icon" data-fuse-bar-toggle="file-manager-info-sidebar">
                                                    <i class="fas fa-ellipsis-v fa-lg"></i>
                                                </button>
                                            </td>
                                        </tr>

                                        <tr data-type="video">
                                            <td class="file-icon">
                                                <i class="icon-archive"></i>
                                            </td>
                                            <td class="name">Videos</td>
                                            <td class="type d-none d-md-table-cell">folder</td>
                                            <td class="last-modified d-none d-lg-table-cell">July 8, 2015</td>
                                            <td class="d-table-cell d-none">
                                                <button type="button" class="btn btn-icon" data-fuse-bar-toggle="file-manager-info-sidebar">
                                                    <i class="fas fa-ellipsis-v fa-lg"></i>
                                                </button>
                                            </td>
                                        </tr>

                                        <%--<tr data-type="pdf">--%>
                                            <%--<td class="file-icon">--%>
                                                <%--<i class="icon-archive"></i>--%>
                                            <%--</td>--%>
                                            <%--<td class="name">PDFs</td>--%>
                                            <%--<td class="type d-none d-md-table-cell">folder</td>--%>
                                            <%--<td class="owner d-none d-sm-table-cell">me</td>--%>
                                            <%--<td class="size d-none d-sm-table-cell"></td>--%>
                                            <%--<td class="last-modified d-none d-lg-table-cell">July 8, 2015</td>--%>
                                            <%--<td class="d-table-cell d-none">--%>
                                                <%--<button type="button" class="btn btn-icon" data-fuse-bar-toggle="file-manager-info-sidebar">--%>
                                                    <%--<i class="icon icon-information-outline"></i>--%>
                                                <%--</button>--%>
                                            <%--</td>--%>
                                        <%--</tr>--%>

                                        <%--<tr>--%>
                                            <%--<td class="file-icon">--%>
                                                <%--<i class="icon-archive"></i>--%>
                                            <%--</td>--%>
                                            <%--<td class="name">SpreadSheets</td>--%>
                                            <%--<td class="type d-none d-md-table-cell">folder</td>--%>
                                            <%--<td class="owner d-none d-sm-table-cell">me</td>--%>
                                            <%--<td class="size d-none d-sm-table-cell"></td>--%>
                                            <%--<td class="last-modified d-none d-lg-table-cell">July 8, 2015</td>--%>
                                            <%--<td class="d-table-cell d-xl-none">--%>
                                                <%--<button type="button" class="btn btn-icon" data-fuse-bar-toggle="file-manager-info-sidebar">--%>
                                                    <%--<i class="icon icon-information-outline"></i>--%>
                                                <%--</button>--%>
                                            <%--</td>--%>
                                        <%--</tr>--%>

                                        <tr data-type="coverpage">
                                            <td class="file-icon">
                                                <i class="icon-archive"></i>
                                            </td>
                                            <td class="name">Cover Pages</td>
                                            <td class="type d-none d-md-table-cell">folder</td>
                                            <td class="last-modified d-none d-lg-table-cell">July 8, 2015</td>
                                            <td class="d-table-cell d-none">
                                                <button type="button" class="btn btn-icon" data-fuse-bar-toggle="file-manager-info-sidebar">
                                                    <i class="fas fa-ellipsis-v fa-lg"></i>
                                                </button>
                                            </td>
                                        </tr>

                                        <tr data-type="template">
                                            <td class="file-icon">
                                                <i class="icon-archive"></i>
                                            </td>
                                            <td class="name">Templates</td>
                                            <td class="type d-none d-md-table-cell">folder</td>
                                            <td class="last-modified d-none d-lg-table-cell">July 8, 2015</td>
                                            <td class="d-table-cell d-none">
                                                <button type="button" class="btn btn-icon fuse-ripple-ready" data-fuse-bar-toggle="file-manager-info-sidebar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v fa-lg"></i>
                                                </button>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                    <!-- / LIST VIEW -->
                                </div>
                                <!-- / CONTENT -->
                            </div>

                            <%--<aside class="page-sidebar custom-scrollbar" data-fuse-bar="file-manager-info-sidebar" data-fuse-bar-position="right" data-fuse-bar-media-step="lg">--%>
                                <%--<!-- SIDEBAR HEADER -->--%>
                                <%--<div class="header bg-secondary text-auto d-flex flex-column justify-content-between p-6">--%>

                                    <%--<!-- TOOLBAR -->--%>
                                    <%--<div class="toolbar row no-gutters align-items-center justify-content-end">--%>

                                        <%--<button type="button" class="btn btn-icon" id="editFile" style="display: none;">--%>
                                            <%--<i class="icon-lead-pencil"></i>--%>
                                        <%--</button>--%>

                                        <%--<button type="button" class="btn btn-icon" id="deleteFile">--%>
                                            <%--<i class="icon-delete"></i>--%>
                                        <%--</button>--%>

                                        <%--&lt;%&ndash;<button type="button" class="btn btn-icon" id="downloadFile">&ndash;%&gt;--%>
                                            <%--&lt;%&ndash;<i class="icon icon-download"></i>&ndash;%&gt;--%>
                                        <%--&lt;%&ndash;</button>&ndash;%&gt;--%>

                                        <%--<button type="button" class="btn btn-icon" id="optionFile">--%>
                                            <%--<i class="icon icon-dots-vertical"></i>--%>
                                        <%--</button>--%>

                                    <%--</div>--%>
                                    <%--<!-- / TOOLBAR -->--%>

                                    <%--<!-- INFO -->--%>
                                    <%--<div>--%>

                                        <%--<div class="mainTitle mb-2">TravelDocs Storage</div>--%>

                                        <%--<div class="subtitle text-muted">--%>
                                            <%--<span>Updated</span>--%>
                                            <%--: May 8, 2017--%>
                                        <%--</div>--%>

                                    <%--</div>--%>
                                    <%--<!-- / INFO-->--%>

                                <%--</div>--%>
                                <%--<!-- / SIDEBAR HEADER -->--%>

                                <%--<!-- SIDENAV CONTENT -->--%>
                                <%--<div class="content">--%>

                                    <%--<div class="file-details">--%>

                                        <%--<div class="preview file-icon row no-gutters align-items-center justify-content-center" id="sidebarIcon">--%>
                                            <%--<i class="icon-folder s-12"></i>--%>
                                        <%--</div>--%>

                                        <%--<div class="offline-switch row no-gutters align-items-center justify-content-between px-6 py-4">--%>

                                            <%--<span id="statType">System Stats</span>--%>


                                        <%--</div>--%>

                                        <%--<div class="title px-6 py-4">Info</div>--%>

                                        <%--<table class="table">--%>

                                            <%--<tr class="type">--%>
                                                <%--<th class="pl-6">Type</th>--%>
                                                <%--<td>Main Directory</td>--%>
                                            <%--</tr>--%>

                                            <%--<tr class="location">--%>
                                                <%--<th class="pl-6">Location</th>--%>
                                                <%--<td>My Files > Documents</td>--%>
                                            <%--</tr>--%>
                                        <%--</table>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                                <%--<!-- / SIDENAV CONTENT -->--%>
                            <%--</aside>--%>
                        </div>

                    </div>
                </div>
                <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
                    <div class="list-group" class="date">

                        <div class="list-group-item subheader">TODAY</div>

                        <div class="list-group-item two-line">

                            <div class="text-muted">

                                <div class="h1"> Friday</div>

                                <div class="h2 row no-gutters align-items-start">
                                    <span> 4</span>
                                    <span class="h6">th</span>
                                    <span> Apr</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="list-group">

                        <div class="list-group-item subheader">Events</div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Group Meeting</h3>
                                <p>In 32 Minutes, Room 1B</p>
                            </div>
                        </div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Public Beta Release</h3>
                                <p>11:00 PM</p>
                            </div>
                        </div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Dinner with David</h3>
                                <p>17:30 PM</p>
                            </div>
                        </div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Q&amp;A Session</h3>
                                <p>20:30 PM</p>
                            </div>
                        </div>

                    </div>

                    <div class="divider"></div>

                    <div class="list-group">

                        <div class="list-group-item subheader">Notes</div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Best songs to listen while working</h3>
                                <p>Last edit: May 8th, 2015</p>
                            </div>
                        </div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Useful subreddits</h3>
                                <p>Last edit: January 12th, 2015</p>
                            </div>
                        </div>

                    </div>

                    <div class="divider"></div>

                    <div class="list-group">

                        <div class="list-group-item subheader">Quick Settings</div>

                        <div class="list-group-item">

                            <div class="list-item-content">
                                <h3>Notifications</h3>
                            </div>

                            <div class="secondary-container">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" />
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </div>

                        </div>

                        <div class="list-group-item">

                            <div class="list-item-content">
                                <h3>Cloud Sync</h3>
                            </div>

                            <div class="secondary-container">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" />
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </div>

                        </div>

                        <div class="list-group-item">

                            <div class="list-item-content">
                                <h3>Retro Thrusters</h3>
                            </div>

                            <div class="secondary-container">

                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" />
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <div id="create-template-modal" class="modal fade" tabindex="-1" role="dialog"
         aria-labelledby="create-template-modal-label" aria-hidden="true"></div>

    <div id="templateDeleteModal"
         class="modal fade"
         tabindex="-1"
         data-backdrop="static"
         data-keyboard="false">

        <div class="alert alert-block alert-danger fade in" style="margin: 0">
            <h4 class="alert-heading">
                <i class="icon icon-alert-decagrams"></i>
                </i> Confirmation</h4>
            <p>
                Are you sure you would like to delete this template?
            </p>
            <p>
                <a class="btn btn-success templateDeleteConfirmed" href="#">
                    <i class="fa fa-check"></i>
                    &nbsp;Yes
                </a>
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                    &nbsp;No
                </button>
            </p>
        </div>
    </div>

	<div id="images-library-add-edit-modal" class="modal modal-image-detail fade add-edit-modal" data-backdrop="static"
    data-keyboard="false" tabindex="-1" style="background: rgba(85, 85, 85, 0.0);box-shadow: 0 3px 9px rgba(0, 0, 0, 0.0);">
    <div class="modal-content">
        <div class="modal-header">
            <div class="titleBar">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="fal fa-times-circle"></i>
				</button>
            </div>
            <h2 class="modal-title">Modal title</h2>
        </div>
        <form id="add-edit-images-form" method="post" enctype="multipart/form-data">
            <div class="modal-body image-info">
              <div class="">
                <div class="col-md-12 image-panel" style="min-height:369px;">
                
				<div class="row">
					<label for="image"></label>
					<div id="image-container">
						<img id="thumbnailImage" src="" alt="Selected Image">
						<svg height="0" width="0">
							<defs>
							<clipPath id="svgPath">
								
							</clipPath>
							</defs>
						</svg>
						
					</div>
				</div>
				<div class="image-action-container">
				  	<input type="file" id="imageUploadButton" accept="image/*" style="display:none;">
				  					  	
                    <div id="image-alert" style="display:none" class="alert alert-dismissible alert-info">
					  <strong>Heads up!</strong> The above image illustrates the crop that will be performed. 
					  The dimension of the new image will be <span id="cropped-size" class="label label-primary"><span id="img-width"></span> x <span id="img-height"></span> px</span>. 
					  To save this image cropped please click the <strong>"Save changes"</strong> button. 
					  To change your mind click the <strong>"Close"</strong> button to reupload the original image again.
					</div>
					
					<div id="image-alert-browser" style="display:none" class="alert alert-dismissible alert-danger">
					  <strong>Warning!</strong> Internet Explorer or Microsoft Edge users might experience some issues with the crop feature. Please, use Chrome or Firefox.
					</div>
				</div>
                  
                <div id="jCropModalImage" class="modal fade" tabindex="-1">
			      <div class="modal-header">
			        <div class="titleBar">
			          	<button type="button" data-dismiss="modal"
                                aria-hidden="true" class="cancel close cancelImageUpload">
							<i class="fal fa-times-circle"></i>
						</button>
			        </div>
			        <h2 class="modal-title">Crop</h2>
			      </div>
			      <div class="modal-body">
			        <img id="JcropPhotoFrame" src=""/>
			         <div class="alert alert-danger uploadErrorSize" role="alert">
			        	The image is too big. (Max 6MB)
			        </div>
			        
			      </div>
			      <div class="modal-footer">
					<i class="fa fa-spin fa-spinner uploadLoading"></i>
			        <button type="button" class="btn btn-danger cancelImageUpload" data-dismiss="modal">
			          <i class="fal fa-times-circle"></i> Close
			        </button>
			        <button type="submit" class="btn btn-success uploadFile disabled"  data-dismiss="modal">
			          <i class="qc qc-edit"></i> Save
			        </button> 
			      </div>
			  	</div>
	
                </div>
                <div class="col-md-12">
									<input type="hidden" id="elID" name="elementId">
                	<div id="form-container">
							<div class="row">
								<div class="col-md-6">
									
						      	  <div class="form-group">
								    <label for="folder">Folder</label>
								    <select name="folder" class="form-control" required></select>
								  </div>
								  <div class="form-group hidden" id="folderNameId">
									<label for="assetName">Folder Name</label> 
									<input type="text" class="form-control" id="newFolderText" name="newFolder" placeholder="Enter new folder name here...">
								  </div>
						      	  <div class="form-group">
									<label class="subFolderLabel" for="subFolder">Sub Folder</label>
						               <select name="subFolder" class="form-control subFolderSelect"></select>
								  </div>
						          <div class="form-group hidden" id="subFolderNameId">
						             <label for="subFolderAssetName">Sub Folder Name</label> 
						             <input type="text" class="form-control" name="newSubFolder" id="newSubFolderText" placeholder="Enter new subfolder name here...">
						          </div>
								</div>
								<div class="form-group col-md-6">
									<label for="assetName">Image Title</label> <input type="text"
										class="form-control" name="assetName">
								</div>
							</div>
						</div>
						<div class="row">
						<div class="col-md-6">
							<button type="button" id="cropImage" class="btn btn-primary" style="display:none">
								<i class="fa fa-crop"></i> Crop
							</button>
						</div>
						<div class="col-md-6">
							<button type="button" class="delete-library-item-action-details btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
							<button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> Close</button>
							<button disabled="true" id="save-changes" type="submit" class="btn pulse-button-info  btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save changes</button>
						</div>
					 </div>
					</div>  
              <div class="">
                <div class="col-md-12" style="margin-top: 20px;">
                  <%--<div class="form-group">--%>
                    <%--<label for=description>Text Content</label> --%>
                    <%--<!-- <textarea class="form-control" name="description" rows="10" id="imageLibDescTextArea"></textarea> -->--%>
                    <%--<div class="panel-froala" style="list-style-position:inside;"></div>--%>
                  <%--</div>--%>
                  
                  <div id="infoContainer">
                   	<div class="row">
						<div class="col-md-4">
							<p><strong>File name: </strong><span id="fileName"></span></p>
                       		<p><strong>File type: </strong><span id="fileType"></span></p>
						</div>
						<div class="col-md-4">
							<p><strong>Uploaded on: </strong><span id="uploadedOn"></span></p>
                       		<p><strong>File size: </strong><span id="fileSize"></span></p>
						</div>
						<div class="col-md-4">
							<p><strong>Dimensions: </strong><span id="dimensions"></span></p>
						</div>
                   </div>
               	</div>
                </div>
              </div>
              <div style="clear: both;"></div>
            </div>
		</div>
        </form>
    </div>

<div id="cover-library-add-edit-modal" class="modal modal-image-detail fade add-edit-modal" data-backdrop="static"
    data-keyboard="false" tabindex="-1" style="background: rgba(85, 85, 85, 0.0);box-shadow: 0 3px 9px rgba(0, 0, 0, 0.0);">
    <div class="modal-content">
        <div class="modal-header">
            <div class="titleBar">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="fal fa-times-circle"></i>
				</button>
            </div>
            <h2 class="modal-title">Modal title</h2>
        </div>
        <form id="add-edit-cover-form" method="post" enctype="multipart/form-data">
            <div class="modal-body image-info">
              <div class="">
                <div class="col-md-12 image-panel" style="min-height:369px;">
                
				<div class="row">
					<label for="image"></label>
					<div id="image-container">
						<img id="thumbnailImage" src="" alt="Selected Image">
						<svg height="0" width="0">
							<defs>
							<clipPath id="svgPath">
								
							</clipPath>
							</defs>
						</svg>
						
					</div>
				</div>
				<div class="image-action-container">
				  	<input type="file" id="coverUploadButton" accept="image/*">
				  					  	
                    <div id="image-alert" style="display:none" class="alert alert-dismissible alert-info">
					  <strong>Heads up!</strong> The above image illustrates the crop that will be performed. 
					  The dimension of the new image will be <span id="cropped-size" class="label label-primary"><span id="img-width"></span> x <span id="img-height"></span> px</span>. 
					  To save this image cropped please click the <strong>"Save changes"</strong> button. 
					  To change your mind click the <strong>"Close"</strong> button to reupload the original image again.
					</div>
					
					<div id="image-alert-browser" style="display:none" class="alert alert-dismissible alert-danger">
					  <strong>Warning!</strong> Internet Explorer or Microsoft Edge users might experience some issues with the crop feature. Please, use Chrome or Firefox.
					</div>
				</div>
                  
                <div id="jCropModalImage" class="modal fade" tabindex="-1">
			      <div class="modal-header">
			        <div class="titleBar">
			          	<button type="button" data-dismiss="modal"
                                aria-hidden="true" class="cancel close cancelImageUpload">
							<i class="fal fa-times-circle"></i>
						</button>
			        </div>
			        <h2 class="modal-title">Crop</h2>
			      </div>
			      <div class="modal-body">
			        <img id="JcropPhotoFrame" src=""/>
			         <div class="alert alert-danger uploadErrorSize" role="alert">
			        	The image is too big. (Max 6MB)
			        </div>
			        
			      </div>
			      <div class="modal-footer">
					<i class="fa fa-spin fa-spinner uploadLoading"></i>
			        <button type="button" data-dismiss="modal"
                            aria-hidden="true" class="btn btn-danger cancelImageUpload">
			          <i class="fal fa-times-circle"></i> Close
			        </button>
			        <button type="submit" class="btn btn-success uploadFile disabled">
			          <i class="qc qc-edit"></i> Save
			        </button> 
			      </div>
			  	</div>
	
                </div>
                <div class="col-md-12">
									<input type="hidden" id="elID" name="elementId">
                	<div id="form-container">
							<div class="row">
								<!-- <div class="col-md-6">
									
						      	  <div class="form-group">
								    <label for="folder">Folder</label>
								    <select name="folder" class="form-control" required></select>
								  </div>
								  <div class="form-group hidden" id="folderNameId">
									<label for="assetName">Folder Name</label> 
									<input type="text" class="form-control" id="newFolderText" name="newFolder" placeholder="Enter new folder name here...">
								  </div>
						      	  <div class="form-group">
									<label class="subFolderLabel" for="subFolder">Sub Folder</label>
						               <select name="subFolder" class="form-control subFolderSelect"></select>
								  </div>
						          <div class="form-group hidden" id="subFolderNameId">
						             <label for="subFolderAssetName">Sub Folder Name</label> 
						             <input type="text" class="form-control" name="newSubFolder" id="newSubFolderText" placeholder="Enter new subfolder name here...">
						          </div>
								</div> -->
								<div class="form-group col-md-6">
									<label for="assetName">Image Title</label> <input type="text"
										class="form-control" name="assetName">
								</div>
							</div>
						</div>
						<div class="row">
						<div class="col-md-6">
							<button type="button" id="cropImage" class="btn btn-primary" style="display:none">
								<i class="fa fa-crop"></i> Crop
							</button>
						</div>
						<div class="col-md-6">
							<button type="button" class="delete-library-item-action-details btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
							<button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> Close</button>
							<button disabled="true" id="save-changes" type="submit" class="btn pulse-button-info  btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save changes</button>
						</div>
					 </div>
					</div>  
              <div class="">
                <div class="col-md-12" style="margin-top: 20px;">
                  <!-- <div class="form-group">
                    <label for=description>Text Content</label> 
                    <textarea class="form-control" name="description" rows="10" id="imageLibDescTextArea"></textarea>
                    <div class="panel-froala" style="list-style-position:inside;"></div>
                  </div> -->
                  
                  <div id="infoContainer">
                   	<div class="row">
						<div class="col-md-4">
							<p><strong>File name: </strong><span id="fileName"></span></p>
                       		<p><strong>File type: </strong><span id="fileType"></span></p>
						</div>
						<div class="col-md-4">
							<p><strong>Uploaded on: </strong><span id="uploadedOn"></span></p>
                       		<p><strong>File size: </strong><span id="fileSize"></span></p>
						</div>
						<div class="col-md-4">
							<p><strong>Dimensions: </strong><span id="dimensions"></span></p>
						</div>
                   </div>
               	</div>
                </div>
              </div>
              <div style="clear: both;"></div>
            </div>
		</div>
        </form>
    </div>
    
	<div id="imageDeleteModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">

		<div class="alert alert-block alert-danger fade in" style="margin: 0">
			<h4 class="alert-heading"><i class="fa fa-warning"></i> Confirmation</h4>
			<p>
				Are you sure you would like to delete the image?
			</p>
			<p>
				<a class="btn btn-success imageDeleteConfimed" href="#">
					<i class="fa fa-check"></i> &nbsp;Yes
				</a>
				<button type="button" class="btn btn-danger cancelDeleteButton">
					<i class="fa fa-times"></i>
					&nbsp;No
				</button>
			</p>
		</div>
	</div>

	<div id="imageMultiDeleteModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">

		<div class="alert alert-block alert-danger fade in" style="margin: 0">
			<h4 class="alert-heading"><i class="fa fa-warning"></i> Confirmation</h4>
			<p>
				Are you sure you would like to delete these images?
			</p>
			<p>
				<a class="btn btn-success imageMultiDeleteConfimed" href="#">
					<i class="fa fa-check"></i> &nbsp;Yes
				</a>
				<button type="button" class="btn btn-danger cancelMultiDeleteButton">
					<i class="fa fa-times"></i>
					&nbsp;No
				</button>
			</p>
		</div>
	</div>

    <!-- /.modal-content -->
</div>

<div id="text-library-add-edit-modal" class="modal fade add-edit-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="display: none;">
  <div class="modal-content">
    <div class="modal-header">
	    <div class="titleBar">
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	      <i class="fal fa-times-circle"></i>
	      </button>
	    </div>
	    <h2 class="modal-title">Modal title</h2>
    </div>
    <form id="add-edit-text-form" method="post" enctype="multipart/form-data">
	      	  <input type="hidden" name="elementId">
	    <div class="modal-body">
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="folder">Folder</label>
                        <select name="folder" class="form-control" required></select>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label class="subFolderLabel" for="subFolder">Sub Folder</label>
                        <select name="subFolder" class="form-control subFolderSelect"></select>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col">
                    <div class="form-group hidden" id="folderNameId">
                        <label for="assetName">Folder Name</label>
                        <input type="text" class="form-control" id="newFolderText" name="newFolder" placeholder="Enter new folder name here...">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group hidden" id="subFolderNameId">
                        <label for="subFolderAssetName">Sub Folder Name</label>
                        <input type="text" class="form-control" name="newSubFolder" id="newSubFolderText" placeholder="Enter new subfolder name here...">
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col">
                    <div class="form-group">
                        <label for="assetName">Asset Name</label>
                        <input type="text" class="form-control" name="assetName">
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col"><div class="form-group">
                    <label for="textBlock">Text Block</label>
                    <!-- remove ck -->
                    <!-- <textarea id="textBlockTextarea" class="form-control" name="textBlock" rows="10"></textarea> -->
                    <div class="panel-froala">
                    </div>
                </div>
                </div>
            </div>
            <div class="row mt-3" style="display: none;">
                <div class="col">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="lockContent"> Lock Content
                        </label>
                    </div>
                </div>
            </div>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      <button type="submit" class="btn btn-primary">Save changes</button>
	    </div>
	    <div id="modal-popup" style="position: relative;"></div>
    </form>
  </div><!-- /.modal-content -->
 </div>

<div id="pdf-library-add-edit-modal" class="modal fade add-edit-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="display: none;">
  <div class="modal-content">
    <div class="modal-header">
	    <div class="titleBar">
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	      <i class="fal fa-times-circle"></i>
	      </button>
	    </div>
	    <h2 class="modal-title">Modal title</h2>
    </div>
    <form id="add-edit-pdf-form" method="post" enctype="multipart/form-data">
  	    <input type="hidden" name="elementId">
	    <div class="modal-body">
      	  <div class="form-group">
		    <label for="folder">Folder</label>
		    <select name="folder" class="form-control" required></select>
		  </div>
		  <div class="form-group hidden" id="folderNameId">
			<label for="assetName">Folder Name</label> 
			<input type="text" class="form-control" id="newFolderText" name="newFolder" placeholder="Enter new folder name here...">
		  </div>
      	  <div class="form-group">
			<label class="subFolderLabel" for="subFolder">Sub Folder</label>
               <select name="subFolder" class="form-control subFolderSelect"></select>
		  </div>
          <div class="form-group hidden" id="subFolderNameId">
             <label for="subFolderAssetName">Sub Folder Name</label> 
             <input type="text" class="form-control" name="newSubFolder" id="newSubFolderText" placeholder="Enter new subfolder name here...">
          </div>
	      	  <div class="form-group pdfNameGroup">
			    <label for="assetName">Asset Name</label>
			    <input type="text" class="form-control" name="assetName">
			  </div>
	      	  <div class="form-group">
	    			<div action="/api/library/add" method="post" enctype="multipart/form-data" id="uploadPDFDrop" class="pdfDropzone dropzone" style="min-height:220px;">
	    
		  			<div class="fallback">
		    		<input name="file[]" type="file" multiple />
		  		</div>
				</div>
			</div>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      <button type="submit" class="btn btn-primary pdfUploadButton">Save changes</button>
	    </div>
    </form>
  </div><!-- /.modal-content -->
 </div>
 
 
 <div id="content-library-folder-delete-modal" class="modal fade" data-backdrop="static" data-keyboard="false"
	tabindex="-1" style="display: none;">
	<div class="modal-content">
		<form id="folder-delete-form">
			<input type="hidden" name="folderId">
			<input type="hidden" name="libraryType">
		    <div class="modal-header">
			    <div class="titleBar">
			      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      <i class="fal fa-times-circle"></i>
			      </button>
			    </div>
			    <h2 class="modal-title">Modal title</h2>
		    </div>
			<div class="modal-body">
				<div class="form-group">
					<label>Folder to be Deleted</label>
					<input class="form-control" type="text" id="folderToBeDeleted" disabled>
				</div>
				<div class="form-group">
					<label for="transferTo">Transfer Contents to Existing Folder</label>
					<select class="form-control" name="transferTo">
					</select>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
			</div>
		</form>
	</div>
	<!-- /.modal-content -->
</div>

<div id="videos-library-add-edit-modal" class="modal fade add-edit-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="display: none;">
  <div class="modal-content">
    <div class="modal-header">
	    <div class="titleBar">
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	      <i class="fal fa-times-circle"></i>
	      </button>
	    </div>
	    <h2 class="modal-title">Modal title</h2>
    </div>
    <form id="add-edit-videos-form" method="post" enctype="multipart/form-data">
   	  <input type="hidden" name="elementId">
	    <div class="modal-body">
	      <div class="form-group">
		    <label for="folder">Folder</label>
		    <select name="folder" class="form-control" required></select>
		  </div>
		  <div class="form-group hidden" id="folderNameId">
			<label for="assetName">Folder Name</label> 
			<input type="text" class="form-control" id="newFolderText" name="newFolder" placeholder="Enter new folder name here...">
		  </div>
      	  <div class="form-group">
			<label class="subFolderLabel" for="subFolder">Sub Folder</label>
               <select name="subFolder" class="form-control subFolderSelect"></select>
		  </div>
          <div class="form-group hidden" id="subFolderNameId">
             <label for="subFolderAssetName">Sub Folder Name</label> 
             <input type="text" class="form-control" name="newSubFolder" id="newSubFolderText" placeholder="Enter new subfolder name here...">
          </div>
          
	      	  <div class="form-group">
			    <label for="assetName">Video Title</label>
			    <input type="text" class="form-control" name="assetName"  required>
			  </div>
	      	  <%--<div class="form-group" style="position:relative;">--%>
			    <%--<label for="caption">Caption</label>--%>
			    <%--<input type="text" class="form-control" name="caption" style="padding-right: 78px !important;">--%>
				  <%--<p class="qc-label-info" id="warningText">Your video can have accompanying text displayed when your customer views your traveldocs. Please add this text to the Caption field above.</p>--%>
				  <%--<span class="qc-label-info" id="warningLimit" style="position: absolute; top: 52px; right: 7px;">0/255</span>--%>
			  <%--</div>--%>
	      	  <div class="form-group">
			    <label for="videoLink">Video Link</label>
			    <input type="text" id="videoLinkUrl" class="form-control" name="videoLink"  required>
			  	<p class="qc-label-info">It's super easy to embed videos in TravelDocs, simply copy and paste either a Vimeo or YouTube URL in the above Video Link field.  For example, https://www.youtube.com/watch?v=YnGmedwOYuQ would be a URL for a YouTube video.</p>
			  </div>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      <button type="submit" class="btn btn-primary videoSubmit">Save changes</button>
	    </div>
    </form>
  </div><!-- /.modal-content -->
 </div>
 
 
 
<div id="project-library-add-edit-modal" class="modal modal-image-detail fade add-edit-modal" data-backdrop="static"
    data-keyboard="false" tabindex="-1" style="background: rgba(85, 85, 85, 0.0);box-shadow: 0 3px 9px rgba(0, 0, 0, 0.0);">
    <div class="modal-content">
        <div class="modal-header">
            <div class="titleBar">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="fal fa-times-circle"></i>
				</button>
            </div>
            <h2 class="modal-title">Modal title</h2>
        </div>
        <form id="add-edit-project-form" method="post" enctype="multipart/form-data">
        	<input type="hidden" name="elementId">
            <div class="modal-body image-info">
            	<div class="form-group">
			    	<label for="projectName">Project Name</label>
			    	<input type="text" class="form-control" name="projectName" required>
			  </div>
			</div>
		    <div class="modal-footer">
		      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      <button type="submit" class="btn btn-primary projectSubmit">Save changes</button>
		    </div>
        </form>
    </div>
    
	<div id="imageDeleteModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">

		<div class="alert alert-block alert-danger fade in" style="margin: 0">
			<h4 class="alert-heading"><i class="fa fa-warning"></i> Confirmation</h4>
			<p>
				Are you sure you would like to delete the image?
			</p>
			<p>
				<a class="btn btn-success imageDeleteConfimed" href="#">
					<i class="fa fa-check"></i> &nbsp;Yes
				</a>
				<button type="button" class="btn btn-danger cancelDeleteButton">
					<i class="fa fa-times"></i>
					&nbsp;No
				</button>
			</p>
		</div>
	</div>

	<div id="imageMultiDeleteModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">

		<div class="alert alert-block alert-danger fade in" style="margin: 0">
			<h4 class="alert-heading"><i class="fa fa-warning"></i> Confirmation</h4>
			<p>
				Are you sure you would like to delete these images?
			</p>
			<p>
				<a class="btn btn-success imageMultiDeleteConfimed" href="#">
					<i class="fa fa-check"></i> &nbsp;Yes
				</a>
				<button type="button" class="btn btn-danger cancelMultiDeleteButton">
					<i class="fa fa-times"></i>
					&nbsp;No
				</button>
			</p>
		</div>
	</div>

    <!-- /.modal-content -->
</div>
 
    <!-- Bootstrap core JavaScript
      ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<%=UrlUtils.autocachedUrl("/application/js/log4javascript.js", request) %>"></script>
        <%--<script src="<%=UrlUtils.autocachedUrl("/application/js/logging.js", request) %>"></script>--%>
    <script src="<%=UrlUtils.autocachedUrl("/application/js/utils.js", request) %>"></script>
    <script src="<%=UrlUtils.autocachedUrl("/application/js/loading.js", request) %>"></script>

    <script type="text/javascript" src="/application/assets/new/js/apps/dashboard/project.js"></script>
    <!-- Fuse Html -->
    <script src="/application/assets/plugins/jquery-placeholder/js/jquery.placeholder.js"></script>
    <script src="/application/assets/plugins/moment/moment.js"></script>
    <script src="/application/assets/plugins/moment-timezone/0.5.10/moment-timezone-with-data.min.js"></script>
    <script type="text/javascript" src="/application/assets/new/vendor/swiper/js/swiper.min.js"></script>
    <script src="/application/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="/application/assets/plugins/DataTables/buttons/js/dataTables.buttons.min.js"></script>
    <script src="/application/assets/plugins/DataTables/dataTables.colReorder.min.js"></script>
    <script src="/application/assets/plugins/DataTables/dataTables.colResize.js"></script>
    <script src="https://cdn.datatables.net/fixedcolumns/3.2.4/js/dataTables.fixedColumns.min.js"></script>
    <script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="/application/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"></script>
    <script src="/application/assets/plugins/timeago/jquery.timeago.js"></script>
    <script src="/application/assets/plugins/dynatable.0.3.1/jquery.dynatable.js"></script>
    <script src="/application/assets/plugins/dropzone/downloads/dropzone.min.js"></script>
    <script src="/application/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="/application/assets/plugins/bootstrap-datepicker/js/date.format.js"></script>
    <script src="/application/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="/application/assets/plugins/switchery/switchery.js"></script>
    <script src="/application/assets/plugins/nprogress/nprogress.js"></script>
    <script src="/application/assets/js/ui-modals.js"></script>
    <script src="/application/assets/plugins/Chart.js/Chart.min.js"></script>
    <script src="/application/assets/plugins/moment/moment.js"></script>
    <script src="/application/assets/plugins/moment-timezone/0.5.10/moment-timezone-with-data.min.js"></script>
    <script src="/application/assets/plugins/jquery-placeholder/js/jquery.placeholder.js"></script>
        <script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
        <script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
        <script src="/application/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"></script>
    <script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="/application/assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
    <script src="/application/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
    <script src="/application/assets/plugins/sweetalert/sweetalert-dev.js"></script>
    <script src="/application/assets/plugins/iCheck/jquery.icheck.min.js"></script>
    <script src="/application/js/fileManager.js"></script>
    
    <script src="/application/js/library/<%=request.getParameter("libraryType") %>.js"></script>
    <script src="/application/assets/plugins/jquery.form/jquery.form.js"></script>
	<script src="/application/assets/plugins/Jcrop/js/jquery.Jcrop.min.js"></script>
  <!-- Froala Editor -->
<script src="/application/assets/plugins/froala-editor/js/froala_editor.pkgd.min.js"></script>

    <%--<script type="text/javascript" src="/application/assets/materialize/js/materialize.min.js"></script>--%>
        <%--MAIN DASH JS--%>
<script>
    Dropzone.autoDiscover = false;

    $('#add-file-button').on('touchstart click', function() {
        var transitionLeft = 8;
        var transitionSpeed = 60;
		if ($('#addFileOpenClose').hasClass('icon-plus')) {
			$('#addFileOpenClose').removeClass('icon-plus').addClass('icon-close');
			$('.file-add-container .btn1:visible').each(function(){
				transitionLeft = transitionLeft+transitionSpeed;
				$(this).animate({
	                opacity: 1,
	                left: transitionLeft + "px",
	            }, 200);
			})
		} else {
			$('#addFileOpenClose').removeClass('icon-close').addClass('icon-plus');
			$('.file-add-container .btn1:visible').each(function(){
				$(this).animate({
	                opacity: 0,
	                left: transitionLeft + "px",
	            }, 200);
			})
		}

		return false;
	});
	$('[data-toggle="menuButtonTooltip"]').tooltip()

    var ds;
	var ds2;

    function initialiseDragSelect() {

        /* ds2 = new DragSelect({
            selectables: document.querySelectorAll('.sub-folders .item'),
            area: document.getElementsByClassName('sub-folders')[0],
            multiSelectMode: true,
            callback: function(element, event) {
                if($(event.target).closest('.item-actions').length > 0)
                {
                    $(event.target).closest('.item').addClass('ds-selected');
                }
                else
                {
                    $(event.target).closest('.item').removeClass('ds-selected');
                }
            }
        }) */


    }

    initialiseDragSelect();


</script>
    </body>
</compress:html>
</html>