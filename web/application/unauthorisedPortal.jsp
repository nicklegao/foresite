<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<!--[if lt IE 9]><html class="bad-ie" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<html lang="en">
<!--<![endif]-->
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
<head>
	<title>Error</title>
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<link rel="stylesheet" href="/application/assets/plugins/bootstrap-qcloud/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/font-awesome.min.css" />

	<link rel="stylesheet" href="/application/assets/plugins/qcloud-iconfont/style.css" />

	<link rel="stylesheet" href="/application/assets/fonts/style.css" />

	<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/css/login.css", request) %>" type="text/css"/>
	<!--[if IE 7]>
	<link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
	<![endif]-->
</head>
<body class="login">
	<div class="container">
		<div class="row">
			<div class="main-login col-xs-offset-2 col-xs-8">
		
				<div class="box-login">
					<div class="box-header"><div class="logo"></div></div>
					<h1>Oops!</h1>
					<p>
						Unfortunately the page you are trying to access is not available.
						<br/>
						Please contact your company administrator to give you permission to access. 
						<br/>
						<%
						if (session.getAttribute(Constants.PORTAL_USER_ID_ATTR) != null)
						{
						%>
							<br>
							<a class="btn btn-success" href="/dashboard">Return to dashboard <i class="qc qc-next"></i></a>
						<%
						}
						%>
					</p>
				</div>
			</div>
			
			<!-- start: COPYRIGHT -->
			<div class="copyright col-xs-offset-2 col-xs-8">
				<!-- &copy; QuoteCloud  -->
			</div>
			<!-- end: COPYRIGHT -->
		</div>
	</div>

	<!--[if lt IE 9]>
	<script src="/application/assets/plugins/respond.js"></script>
	<![endif]-->
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
	<script src="/application/assets/plugins/bootstrap-qcloud/js/bootstrap.min.js"></script>
	
	<script src="/application/js/three.min.js"></script>
	<script src="/application/js/clouds.min.js"></script>
	<script id="vs" type="x-shader/x-vertex">			
				varying vec2 vUv;

				void main() {
					vUv = uv;
					gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
				}
	</script>		
	<script id="fs" type="x-shader/x-fragment">			
				uniform sampler2D map;

				uniform vec3 fogColor;
				uniform float fogNear;
				uniform float fogFar;

				varying vec2 vUv;		

				void main() {					
					float depth = gl_FragCoord.z / gl_FragCoord.w;
					float fogFactor = smoothstep( fogNear, fogFar, depth );
					gl_FragColor = texture2D( map, vUv );
					gl_FragColor.w *= pow( gl_FragCoord.z, 20.0 );
					gl_FragColor = mix( gl_FragColor, vec4( fogColor, gl_FragColor.w ), fogFactor );
				}
	</script>
</body>
</compress:html>
</html>