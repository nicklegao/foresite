<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions"%>
<%@page import="au.net.webdirector.common.datalayer.admin.db.DataAccess"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.TransactionDataAccess" %>
<%@page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager" %>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@page import="au.net.webdirector.common.datalayer.client.TransactionModuleAccess" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
		 pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<%
	TUserRolesDataAccess urda = new TUserRolesDataAccess();
	UserPermissions up = urda.getPermissions(2);
	%>
	<div>User: <%=up.getUser() %></div>
	<div style="color:<%=up.hasManageStylePermission()?"green":"red" %>;">Manage Style: <%=up.hasManageStylePermission() %></div>
	<div style="color:<%=up.hasManageCoversPermission()?"green":"red" %>;">Manage Covers: <%=up.hasManageCoversPermission() %></div>
	<div style="color:<%=up.hasManageTemplatesPermission()?"green":"red" %>;">Manage Templates: <%=up.hasManageTemplatesPermission() %></div>
	<div style="color:<%=up.hasManageUsersPermission()?"green":"red" %>;">Manage Users: <%=up.hasManageUsersPermission() %></div>
	<div style="color:<%=up.hasManageSettingsPermission()?"green":"red" %>;">Manage Settings: <%=up.hasManageSettingsPermission() %></div>
	<div style="color:<%=up.hasViewAllProposalsPermission()?"green":"red" %>;">View All Proposals: <%=up.hasViewAllProposalsPermission() %></div>
	<div style="color:<%=up.hasViewTeamProposalsPermission()?"green":"red" %>;">View Team proposals: <%=up.hasViewTeamProposalsPermission() %></div>
	<div style="color:<%=up.hasCreateOnBehalfPermission()?"green":"red" %>;">Create On Behalf: <%=up.hasCreateOnBehalfPermission() %></div>
	<div style="color:<%=up.hasManagePricingStylePermission()?"green":"red" %>;">Manage Pricing Style: <%=up.hasManagePricingStylePermission() %>
	</div>
</body>
</html>