<!DOCTYPE html>
<!--[if lt IE 9]><html class="bad-ie" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<%@page import="java.util.Hashtable" %>
<%@page import="java.util.Locale"%>
<%@page import="au.com.ci.sbe.util.UrlUtils" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.CustomFieldsDataAccess" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.CustomField" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.CustomFields" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.DateFormatUtils" %>
<%@page import="au.net.webdirector.common.Defaults" %>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess" %>


<html lang="en">

<!--<![endif]-->
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
<head>

<%
UserAccountDataAccess uada = new UserAccountDataAccess();
TravelDocsDataAccess pda = new TravelDocsDataAccess();

String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
String userUsername = (String)session.getAttribute(Constants.PORTAL_USERNAME_ATTR);
CategoryData company = uada.getUserCompanyForUserId(userId);
String companyId = company.getId();
String corporateCSS = String.format("/%s/%s/Categories/%s/gen/css-document/dashboard-colours.css",
		Defaults.getInstance().getStoreContext(),
		UserAccountDataAccess.USER_MODULE,
		companyId);
Hashtable<String, String> userDetails = uada.getUserWithId(userId);
	String lineStyle = company.getString(UserAccountDataAccess.C1_TABLE_STYLE_LINE);
	String lineColor = company.getString(UserAccountDataAccess.C1_TABLE_STYLE_COLOR);
	Integer lineWeight = company.getInt(UserAccountDataAccess.C1_TABLE_STYLE_WEIGHT);
	if (lineWeight == null || lineWeight < 1) {
		lineWeight = 1;
	}
	if (lineStyle == null) {
		lineStyle = "default";
	}
	if (lineColor == null) {
		lineColor = "#ddd";
	}

String[] accessRights = uada.getUserAccessRights(userId);



/* importing custom product fields */
CustomFieldsDataAccess cfda = new CustomFieldsDataAccess();
CustomFields productCustomFields = cfda.getCompanyCustomFields(CustomFields.PRODUCTS, company.getId());
int colspan = productCustomFields.hasOnCustomFields()+4;

//Try to load a pricing style otherwise use default
ClientFileUtils clientFileUtils = new ClientFileUtils();
VersionParserIntf parser = null;
String json = clientFileUtils.readTextContents(clientFileUtils.getFilePathFromStoreName(company.getString(TUserAccountDataAccess.C1_PROPOSAL_PRICING_STYLES))).toString();
PricingTableStyle pricingTableStyle = null;
try
{
	int version = 0;
	if(StringUtils.isNotBlank(json))
	{
		JSONObject object = new JSONObject(json);
		if (object != null)
		{
			version = object.getInt("version");
		}
	}
	VersionManager<PricingTableStyle> versionManager = VersionManager.getInstance(PricingTableStyle.class);
	parser = new PricingTableParser();
	//Use for getting the footer to get labels;
	pricingTableStyle = versionManager.readAsLatest(version, json, PricingTableStyle.CURRENT_VERSION);
	parser.setParserObject(pricingTableStyle);
}
catch (Exception e)
{
	parser = null;
	e.printStackTrace();
}

String fontStyle = "";
String notesStyle = "";
String notesColor = "";
String notesSize = "";
if(pricingTableStyle != null && pricingTableStyle.getRows() != null && pricingTableStyle.getRows().getNotes() != null)
{
	notesStyle = pricingTableStyle.getRows().getNotes().get("style");
	notesColor = pricingTableStyle.getRows().getNotes().get("color");
	notesSize = pricingTableStyle.getRows().getNotes().get("size");
	if (pricingTableStyle.getRows().getNotes().get("fontStyle") != null) {
		fontStyle = pricingTableStyle.getRows().getNotes().get("fontStyle");
	}
}

String iconsSize = "";
if(pricingTableStyle != null && pricingTableStyle.getStyling() != null && pricingTableStyle.getStyling().getIcons() != null)
{
	iconsSize = pricingTableStyle.getStyling().getIcons().get("size");
}

String subheadingStyle = "";
String subheadingWeight = "";
String subheadingSize = "";
if(pricingTableStyle != null && pricingTableStyle.getRows() != null && pricingTableStyle.getRows().getHeadings() != null)
{
	subheadingStyle = pricingTableStyle.getRows().getHeadings().get("style");
	subheadingWeight = pricingTableStyle.getRows().getHeadings().get("weight");
	subheadingSize = pricingTableStyle.getRows().getHeadings().get("size");
}

%>
<script>
var dateFormatPattern = '<%= DateFormatUtils.getInstance(company.getString(UserAccountDataAccess.C1_DATE_FORMAT), Locale.forLanguageTag(company.getString(UserAccountDataAccess.C1_LOCALE))).getJsPattern() %>';
var userId = '<%=userId%>';
var palette1 = '<%=company.getString(UserAccountDataAccess.C1_PALETTE1)%>';
var palette2 = '<%=company.getString(UserAccountDataAccess.C1_PALETTE2)%>';
var palette3 = '<%=company.getString(UserAccountDataAccess.C1_PALETTE3)%>';
var palette4 = '<%=company.getString(UserAccountDataAccess.C1_PALETTE4)%>';
</script>
<title>Price Table Layout Editor</title>

<jsp:include page="/application/include/common-head.jsp"></jsp:include>

<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
<link rel="stylesheet" type="text/css" media="screen" href="/proposals/css/smartadmin-production-plugins.css">
<link rel="stylesheet" type="text/css" media="screen" href="/proposals/css/smartadmin-production.css">
<link rel="stylesheet" type="text/css" media="screen" href="/proposals/css/smartadmin-skins.css">
<link rel="stylesheet" href="/application/assets/plugins/rangeslider/rangeslider.css" />
<link rel="stylesheet" href="<%=corporateCSS%>" />


	<link rel="shortcut icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon">

	<link rel='stylesheet' href='/application/assets/plugins/spectrum/spectrum.css' />
	<link rel='stylesheet' href='/application/assets/plugins/paper-collapse/paper-collapse.min.css' />
	<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/css/dashboard.css", request) %>" type="text/css"/>
	<!--<link rel="stylesheet" href="/application/assets/plugins/bootstrap-qcloud/css/bootstrap.min.css" type="text/css"/>-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="/application/css/customizableEditor.css" type="text/css"/>

<!-- Froala Editor -->
<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/plugins/table.css",request) %>" />
	<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/froala_style.min.css",request) %>" />
	<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/froala_editor.pkgd.min.css",request) %>" />
<%-- <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala-editor/css/froala_style.min.css",request) %>" />
<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala-editor/css/froala_editor.pkgd.min.css",request) %>" /> --%>
<jsp:include page="ie8StdHeaders.jsp"></jsp:include>

<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
</head>

<body class="mainContent">
  <!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="/application/images/qCloud.png" alt="QuoteCloud"> </span>					
			</div>
			<!-- pulled right: nav area -->
			<div class="pull-right">						
				<div class="main-editor-buttons">
					<button type="button" class="btn btn-default saveProposalStyles">
							<i class="fa fa-save"></i>
							&nbsp;Save
					</button>
					<button type="button" class="btn btn-default btn-close" onclick="location.href='/dashboard';">
							<i class="fa clip-close"></i>
							&nbsp;Close
					</button>			
				</div>
			</div>
			<!-- end pulled right: nav area -->
			
			
		</header>
		<!-- END HEADER -->
		
			<div class="clearfix" id="content">
					<!-- Content goes here -->


				<div class="col col-md-8 col-sm-8 mainPanel" style="z-index: 1">
					<div class="tablediv col col-md-7">
						<%@ include file="/application/editableTable.jsp" %>
					</div>
				</div>



				<div class="col col-md-2 col-sm-4 rightPanel">
					<div class="collapse-card">
						<div class="collapse-card__heading">
							<div class="collapse-card__title">
								<i class="fa fa-filter fa-2x fa-fw"></i>
								<span style="font-size: 14px">Custom Fields</span>
								<button class="btn btn-primary btn-xs pull-right  customFieldsAction" data-toggle="modal">
									Manage
								</button>
							</div>
						</div>
						<div class="collapse-card__body" id="collapsableCustomFields">
							<div class="panel-body">
								<%
									if (productCustomFields.hasOnCustomFields() > 0)
									{
										for (CustomField field : productCustomFields)
										{
											if (field.isOn() && (field.getId() != null))
											{ %>
								<div class="customField" sortable="<%=field.getId() %>">
									<span class="sort"><%=field.getLabel() %></span>
									<span style="display:none" class="type"><%=field.getType() %></span>
									<i class="fa fa-bars" style="position: absolute;right: 12px;top: 12px;"></i>
								</div>
								<% }
								}
								}
								else
								{
								%>
								<p>You do not have any custom fields enabled. You can edit custom fields by clicking on the manage button.</p>
								<% } %>
							</div>
						</div>
					</div>
					<div class="collapse-card">
						<div class="collapse-card__heading">
							<div class="collapse-card__title">
								<i class="fa fa-paint-brush fa-2x fa-fw"></i>
								<span style="font-size: 14px">Table Styles</span>
							</div>
						</div>
						<div class="collapse-card__body">
							<div class="row" style="margin: 10px 0">
								<div class="col-sm-5 f-field" style="padding-left: 0;">
									<select class="form-control" name="borderStyleType" id="borderStyleType">
										<option value="solid" <%=(lineStyle.equalsIgnoreCase("solid"))?"selected":"" %>>Solid</option>
										<option value="dashed" <%=(lineStyle.equalsIgnoreCase("dashed"))?"selected":"" %>>Dashed</option>
										<option value="dotted" <%=(lineStyle.equalsIgnoreCase("dotted"))?"selected":"" %>>Dotted</option>
									</select>
								</div>
								<div class="col-sm-7 f-field" style="height: 34px;padding-left: 0;padding-top: 7px;">
									<span style="">Line Style</span>
								</div>
							</div>
							<div class="row" style="margin: 10px 0">
								<div class="col-sm-5 f-field" style="padding-left: 0;">
									<input type="number" min="1" max="5" id="borderStyleSpacing" class="form-control" name="borderStyleSpacing" placeholder="1" value="<%=lineWeight%>" style="width: 50%">
								</div>
								<div class="col-sm-7 f-field" style="height: 34px;padding-left: 0;padding-top: 7px;">
									<span style="">Border Weight</span>
								</div>
							</div>
							<div class="row" style="margin: 10px 0">
								<div class="col-sm-5 f-field" style="padding-left: 0;">
									<select class="form-control" name="borderStyleColor" id="borderStyleColor">
										<option value="#ddd" <%=(lineColor.equalsIgnoreCase("#ddd"))?"selected":"" %>>Light Grey</option>
										<option value="#828282" <%=(lineColor.equalsIgnoreCase("#828282"))?"selected":"" %>>Dark Grey</option>
									</select>
									<%--<input type="text" id="borderStyleColor" class="form-control" name="borderStyleColor" placeholder="#ddd" value="#ddd" style="margin: auto;">--%>
								</div>
								<div class="col-sm-7 f-field" style="height: 34px;padding-left: 0;padding-top: 7px;">
									<span style="">Border Colour</span>
								</div>
							</div>
						</div>
					</div>
					<div class="collapse-card">
						<div class="collapse-card__heading">
							<div class="collapse-card__title">
								<i class="fa fa-file-text fa-2x fa-fw"></i>
								<span style="font-size: 14px">Notes Style</span>
							</div>
						</div>
						<div class="collapse-card__body">
							<div class="row" style="margin: 10px 0">
								<div class="col-sm-5 f-field" style="padding-left: 0;">
									<select class="form-control" name="notesFontStyleType" id="notesFontStyleType">
										<option value="italic" <%=((fontStyle == "") || (fontStyle.equalsIgnoreCase("italic")))?"selected":"" %>>Italics</option>
										<option value="editor" <%=(fontStyle.equalsIgnoreCase("editor"))?"selected":"" %>>Editor</option>
									</select>
								</div>
								<div class="col-sm-7 f-field" style="height: 34px;padding-left: 0;padding-top: 7px;">
									<span style="">Note Font Style</span>
								</div>
							</div>
							<div class="row" style="margin: 10px 0">
								<div class="col-sm-5 f-field" style="padding-left: 0;">
									<select class="form-control" name="notesStyleType" id="notesStyleType">
										<option value="solid" <%=(notesStyle.equalsIgnoreCase("solid"))?"selected":"" %>>Solid</option>
										<option value="dashed" <%=(notesStyle.equalsIgnoreCase("dashed"))?"selected":"" %>>Dashed</option>
										<option value="dotted" <%=(notesStyle.equalsIgnoreCase("dotted"))?"selected":"" %>>Dotted</option>
									</select>
								</div>
								<div class="col-sm-7 f-field" style="height: 34px;padding-left: 0;padding-top: 7px;">
									<span style="">Note Line Style</span>
								</div>
							</div>
							<div class="row" style="margin: 10px 0">
								<div class="col-sm-5 f-field" style="padding-left: 0;">
									<input type="number" min="1" max="32" id="notesStyleSize" class="form-control" name="notesStyleSize" placeholder="10" value="<%=notesSize%>" style="width: 50%">
								</div>
								<div class="col-sm-7 f-field" style="height: 34px;padding-left: 0;padding-top: 7px;">
									<span style="">Note Line Weight</span>
								</div>
							</div>
							<div class="row" style="margin: 10px 0">
								<div class="col-sm-5 f-field" style="padding-left: 0;">
									<select class="form-control" name="notesStyleColor" id="notesStyleColor">
										<option value="#ebebeb" <%=(notesColor.equalsIgnoreCase("#ebebeb"))?"selected":"" %>>Grey</option>
										<option value="theme" <%=(notesColor.equalsIgnoreCase("theme"))?"selected":"" %>>Theme</option>
									</select>
								</div>
								<div class="col-sm-7 f-field" style="height: 34px;padding-left: 0;padding-top: 7px;">
									<span style="">Note Line Colour</span>
								</div>
							</div>
						</div>
					</div>

					<div class="collapse-card">
						<div class="collapse-card__heading">
							<div class="collapse-card__title">
								<i class="fa fa-picture-o fa-2x fa-fw"></i>
								<span style="font-size: 14px">Icons Style</span>
							</div>
						</div>
						<div class="collapse-card__body">
							<div class="row" style="margin: 10px 0">
								<div class="col-sm-5 f-field" style="padding-left: 0;">
									<input type="number" min="1" max="32" id="iconsStyleSize" class="form-control" name="iconsStyleSize" placeholder="25" value="<%=iconsSize%>" style="width: 50%">
								</div>
								<div class="col-sm-7 f-field" style="height: 34px;padding-left: 0;padding-top: 7px;">
									<span style="">Icon Font Size</span>
								</div>
							</div>
						</div>
					</div>

					<div class="collapse-card">
						<div class="collapse-card__heading">
							<div class="collapse-card__title">
								<i class="fa fa-font fa-2x fa-fw"></i>
								<span style="font-size: 14px">Sub-Heading Style</span>
							</div>
						</div>
						<div class="collapse-card__body">
							<div class="row" style="margin: 10px 0">
								<div class="col-sm-5 f-field" style="padding-left: 0;">
									<input type="number" min="1" max="32" id="subHeadingStyleSize" class="form-control" name="subHeadingStyleSize" placeholder="25" value="<%= subheadingSize %>" style="width: 50%">
								</div>
								<div class="col-sm-7 f-field" style="height: 34px;padding-left: 0;padding-top: 7px;">
									<span style="">Font Size</span>
								</div>
							</div>
							<div class="row" style="margin: 10px 0">
								<div class="col-sm-5 f-field" style="padding-left: 0;">
									<select class="form-control" name="subHeadingStyleType" id="subHeadingStyleType">
										<option value="normal" <%=(subheadingStyle.equalsIgnoreCase("normal"))?"selected":"" %>>Normal</option>
										<option value="italic" <%=(subheadingStyle.equalsIgnoreCase("italic"))?"selected":"" %>>Italic</option>
									</select>
								</div>
								<div class="col-sm-7 f-field" style="height: 34px;padding-left: 0;padding-top: 7px;">
									<span style="">Font Style</span>
								</div>
							</div>
							<div class="row" style="margin: 10px 0">
								<div class="col-sm-5 f-field" style="padding-left: 0;">
									<select class="form-control" name="subHeadingStyleWeight" id="subHeadingStyleWeight">
										<option value="bold" <%=(subheadingWeight.equalsIgnoreCase("bold"))?"selected":"" %>>Bold</option>
										<option value="normal" <%=(subheadingWeight.equalsIgnoreCase("normal"))?"selected":"" %>>Normal</option>
										<option value="lighter" <%=(subheadingWeight.equalsIgnoreCase("lighter"))?"selected":"" %>>Light</option>
									</select>
								</div>
								<div class="col-sm-7 f-field" style="height: 34px;padding-left: 0;padding-top: 7px;">
									<span style="">Font Weight</span>
								</div>
							</div>
						</div>
					</div>

					<%--
					<div class="collapse-card">
						<div class="collapse-card__heading">
							<div class="collapse-card__title">
								<i class="fa fa-paint-brush fa-2x fa-fw"></i>
								<span style="font-size: 14px">Icon Settings</span>
							</div>
						</div>
						<div class="collapse-card__body">
							<div class="row" style="margin: 10px 0">
								<div class="col-sm-5 f-field" style="padding-left: 0;">
									<input type="number" min="1" max="32" id="notesStyleSize" class="form-control" name="notesStyleSize" placeholder="1" value="<%=lineWeight%>" style="width: 50%">
								</div>
								<div class="col-sm-7 f-field" style="height: 34px;padding-left: 0;padding-top: 7px;">
									<span style="">Line Weight</span>
								</div>
							</div>
							<div class="row" style="margin: 10px 0">
								<div class="col-sm-5 f-field" style="padding-left: 0;">
									<select class="form-control" name="notesStyleColor" id="notesStyleColor">
										<option value="#ebebeb" <%=(lineColor.equalsIgnoreCase("#ebebeb"))?"selected":"" %>>Grey</option>
										<option value="theme" <%=(lineColor.equalsIgnoreCase("#828282"))?"selected":"" %>>Theme</option>
									</select>
								</div>
								<div class="col-sm-7 f-field" style="height: 34px;padding-left: 0;padding-top: 7px;">
									<span style="">Border Colour</span>
								</div>
							</div>
						</div>
					</div>
  					--%>
						<%--

                        <div class="panel-group hand" id="accordion1">

                            <div class="panel panel-default customfield-panel">
                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion1" href="#collapseFirst">
                                    Custom Fields
                                    <button class="btn btn-primary btn-xs pull-right  customFieldsAction" data-toggle="modal">
                                        Manage
                                    </button>
                                </div>
                                <div id="collapseFirst" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <%
                                            if (productCustomFields.hasOnCustomFields() > 0)
                                            {
                                                for (CustomField field : productCustomFields)
                                                {
                                                    if (field.isOn() && (field.getId() != null))
                                                    { %>
                                        <div class="customField" sortable="<%=field.getId() %>">
                                            <span class="sort"><%=field.getLabel() %></span>
                                            <span style="display:none" class="type"><%=field.getType() %></span>
                                        </div>
                                        <% }
                                        }
                                        }
                                        else
                                        {
                                        %>
                                        <p>You do not have any custom fields enabled. You can edit custom fields by clicking on the manage button.</p>
                                        <% } %>
                                    </div>

                                </div>


                                                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion1" href="#CollapseSecond">Styles
                                                                            </div>
                                                                            <div id="CollapseSecond" class="panel-collapse collapse">
                                                                                <div class="panel-body">
                                                                                    <div class="panel-group" id="elements">
                                                                                        <div class="panel panel-default">
                                                                                            <div class="panel-heading" data-toggle="collapse" data-parent="#elements" href="#CollapseSecondTwo">Themes
                                                                                            </div>
                                                                                            <div id="CollapseSecondTwo" class="panel-collapse collapse in">
                                                                                                <div class="panel-body">
                                                                                              <!--   START theme selector -->
                                                                                                    <div class="dropdown">
                                                                                                           <!-- Dropdown Trigger -->
                                                                                                          <a class='dropdown-button btn' href='#' data-activates='dropdown1'>Themes!</a>

                                                                                                          <!-- Dropdown Structure -->
                                                                                                          <ul id='dropdown1' class='dropdown-content'>
                                                                                                            <li><a href="#!">Default</a></li>
                                                                                                            <li class="divider"></li>
                                                                                                            <li><a href="#!">Custom 1</a></li>
                                                                                                            <li><a href="#!">Custom 2</a></li>
                                                                                                          </ul>

                                                                                                    </div>
                                                                                                <!--  END theme selector -->
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="panel-heading" data-toggle="collapse" data-parent="#elements" href="#CollapseSecondOne">Palettes
                                                                                            </div>
                                                                                            <div id="CollapseSecondOne" class="panel-collapse collapse">
                                                                                                <div class="panel-body">

                                                                                                       <!-- Start Color Palettes section -->
                                                                                                            <div id="edit-palette">
                                                                                                            <div class=row>
                                                                                                                        <section class="col col-6 color-section">
                                                                                                                        <span class="section-text">Headings</span>
                                                                                                                            <label class="input"> <input id="palette-1"
                                                                                                                                class="form-control color {hash:true}" name="palette1"
                                                                                                                                placeholder="Hex Colour"
                                                                                                                                value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_PALETTE1))%>">
                                                                                                                            </label>
                                                                                                                        </section>
                                                                                                                        <section class="col col-6 color-section">
                                                                                                                        <span class="section-text">Even Rows</span>
                                                                                                                            <label class="input"> <input id="palette-2"
                                                                                                                                class="form-control color {hash:true}" name="palette2"
                                                                                                                                placeholder="Hex Colour"
                                                                                                                                value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_PALETTE2))%>">
                                                                                                                            </label>
                                                                                                                        </section>
                                                                                                                        <section class="col col-6 color-section">
                                                                                                                        <span class="section-text">Odd Rows</span>
                                                                                                                            <label class="input"> <input id="palette-3"
                                                                                                                                class="form-control color {hash:true}" name="palette3"
                                                                                                                                placeholder="Hex Colour"
                                                                                                                                value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(UserAccountDataAccess.C1_PALETTE3))%>">
                                                                                                                            </label>
                                                                                                                        </section >
                                                                                                                        <section id="coverColor" class=" col col-6 color-section">
                                                                                                                        <span class="section-text">Background</span>
                                                                                                                        <label class="input"> <input id="palette-4"
                                                                                                                            class="form-control color {hash:true}"
                                                                                                                            name="backgroundColour" placeholder="Hex Colour"
                                                                                                                            value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_BACKGROUND_COLOUR))%>">
                                                                                                                        </label>
                                                                                                                    </section>

                                                                                                                </div>

                                                                                                                </div>
                                                                                                            <!-- End Color Palettes section -->
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>

						</div> --%>
					</div>
				</div>

					</div>
				</div>

  <div id="custom-fields-modal" class="modal top-modal fade" tabindex="-1"></div>

 <!-- Bootstrap core JavaScript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
  <script src="/application/assets/plugins/jquery-placeholder/js/jquery.placeholder.js"></script>
  <script src="/application/assets/plugins/bootstrap-qcloud/js/bootstrap.min.js"></script>
  <script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
  <script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
  <script src="/application/assets/js/ui-modals.js"></script>
  <script src="/application/assets/plugins/jquery.form/jquery.form.js"></script>
  <script src="/application/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
  <script src="/application/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
  <script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="/application/assets/plugins/iCheck/jquery.icheck.min.js"></script>
  <script src="/application/assets/plugins/sweetalert/sweetalert.min.js"></script>
  <script src="<%=UrlUtils.autocachedUrl("/application/js/loading.js", request) %>"></script>
  <script src="/application/js/screenCheck.js"></script>
  <script src='/application/assets/plugins/spectrum/spectrum.js'></script>
  <link rel='stylesheet' href='/application/assets/plugins/spectrum/spectrum.css' />

  <script src='/application/assets/plugins/paper-collapse/paper-collapse.min.js'></script>
  <script src='/application/assets/plugins/spectrum/spectrum.js'></script>
  <script src="<%=UrlUtils.autocachedUrl("/application/js/customizableEditor.js", request)%>"></script>
  <script src="/application/assets/plugins/iconPicker/fontawesome/dist/js/fontawesome-iconpicker.js"></script>
  
<!-- Froala Editor -->
<!-- <script src="/application/assets/plugins/froala-editor/js/froala_editor.pkgd.min.js"></script> -->
<script src="/application/assets/plugins/froala_editor_3.0.0/js/froala_editor.pkgd.min.js"></script>

</body>
</compress:html>
</html>