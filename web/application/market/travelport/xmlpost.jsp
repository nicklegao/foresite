<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>XML Post Form</title>

  <script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>

</head>
<body>
    <div align="center">
        <form:form action="/service/xmlpost/travelport" method="post" commandName="xmlPost" enctype="multipart/form-data">
            <table border="0">
                <tr>
                    <td colspan="2" align="center"><h2>XML POST END POINT</h2></td>
                </tr>
                <tr>
                    <td>User Name:</td>
                    <td><form:input path="loginId" /></td>
                </tr>
                <tr>
                    <td>Password:</td>
                    <td><form:password path="password" /></td>
                </tr>
                <tr>
                    <td>File to upload: </td>
                    <td><input type="file" name="file"></td>
                </tr>
                
                <tr>
                    <td colspan="2">&nbsp;</td>
                    
                </tr>
                
                
                    <td colspan="2" align="center"><input type="submit" value="Process XML" /></td>
                </tr>
            </table>
        </form:form>
    </div>
</body>
</html>