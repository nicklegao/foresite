<%@ page language="java" contentType="text/html; charset=UTF-8"     pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>XML POST Success</title>
</head>
<body>
    <div align="center">
        <table border="0">
            <tr>
                <td colspan="2" align="center"><h2>XML POST END POINTS</h2></td>
            </tr>
            <tr>
                <td>Status:</td>
                <td>${status}</td>
            </tr>
            <tr>
                <td>Message :</td>
                <td>${message}</td>
            </tr>
 
        </table>
    </div>
</body>
</html>