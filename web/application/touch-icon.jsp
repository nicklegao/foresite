<meta name="apple-mobile-web-app-status-bar-style" content="black" />

<%-- generated with http://www.favicon-generator.org/ --%>
<link rel="apple-touch-icon" sizes="57x57" href="/application/images/icons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/application/images/icons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/application/images/icons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/application/images/icons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/application/images/icons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/application/images/icons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/application/images/icons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/application/images/icons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/application/images/icons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/application/images/icons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/application/images/icons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/application/images/icons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/application/images/icons/favicon-16x16.png">
<link rel="icon" type="image/png" href="/application/images/icons/favicon.ico">
<%--<link rel="manifest" href="/application/images/icons/manifest.json">--%>
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">