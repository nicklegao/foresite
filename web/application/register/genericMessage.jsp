<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.ClientApplicationProperties"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
	String redirect = request.getParameter(Constants.PORTAL_LOGIN_REDIRECT_PARAM);

	String loginStatus = (String) session.getAttribute(Constants.LOGIN_STATUS);
	session.removeAttribute(Constants.LOGIN_STATUS);
	String loginError = (String) session.getAttribute(Constants.LOGIN_ERROR);
	session.removeAttribute(Constants.LOGIN_ERROR);

	if (userId != null && userId.length() > 0)
	{
		UserAccountDataAccess ada = new UserAccountDataAccess();
		ElementData user = ada.getUserElementWithUserID(userId);
		if (redirect == null)
		{
			redirect = "/dashboard";
		}

		response.sendRedirect(redirect);
		return;
	}
	String loginErrorUsername = (String)session.getAttribute(Constants.LOGIN_ERROR_USERNAME);
	if (loginErrorUsername == null)
	{
		loginErrorUsername = "";
	}
	java.util.Calendar calendar = java.util.Calendar.getInstance();
	int currentYear = calendar.get(java.util.Calendar.YEAR);
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>


	<!--[if lt IE 9]>
	<script src="/application/assets/plugins/respond.js"></script>
	<![endif]-->
	<script src="/application/assets/plugins/jquery-placeholder/js/jquery.placeholder.js"></script>
	<script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="/application/assets/plugins/select2/select2.js"></script>
	<script src="/application/js/login.js"></script>
	<script src="/application/assets/plugins/sweetalert/sweetalert.min.js"></script>

	<style>
		.wrapper {
			overflow: hidden;
			margin: 0 auto;
		}
		.login-conatiner {
			background-image: url('/application/assets/new/images/backgrounds/dtbackground1.png');
			background-size: cover;
			background-position: bottom;
			background-repeat: no-repeat;
			position: relative;
			color: inherit;
			border-color: rgba(255, 255, 255, 0);
			padding-top: 4rem!important;
			padding-bottom: 4rem!important;
			min-height: 800px;
			height: 100vh !important;
		}
		.login-card-container {
			margin: auto;

			/* margin: 0;
            position: absolute;
            top: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
            left: 50%;
            -ms-transform: translateX(-50%);
            transform: translateX(-50%); */

		}
		.card {
			max-width: 850px;
			border: none !important;
			box-shadow: 29px 23px 63px -34px #9fa1a299 !important;
			-webkit-box-shadow: 29px 23px 63px -34px #9fa1a299 !important;
			-moz-box-shadow: 29px 23px 63px -34px #9fa1a299 !important;
			box-shadow: 29px 23px 63px -34px #9fa1a299 !important;
		}
		.login-card-container > .card {
			margin-top: -150px;
		}
		.card-image {
			width: 100%;
			height: auto;
			border-color: rgba(255, 255, 255, 0);
			background-color: rgba(255, 255, 255, 0);
			margin-left: -20px;
			margin-top: -54px;
			margin-right: 0px;
			margin-bottom: -20px;
		}
		.md-form {
			margin-top: 1rem;
			margin-bottom: 1rem;
			text-align: center !important;
		}
		.md-form input[type=date], .md-form input[type=datetime-local], .md-form input[type=email], .md-form input[type=number], .md-form input[type=password], .md-form input[type=search-md], .md-form input[type=search], .md-form input[type=tel], .md-form input[type=text], .md-form input[type=time], .md-form input[type=url], .md-form textarea.md-textarea {
			-webkit-transition: border-color .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
			-o-transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
			transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
			transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
			outline: 0;
			-webkit-box-shadow: none;
			box-shadow: none;
			border: none;
			border-bottom: 1px solid #ced4da;
			-webkit-border-radius: 0;
			border-radius: 0;
			-webkit-box-sizing: content-box;
			box-sizing: content-box;
			background-color: transparent;
		}
		.md-form .form-control {
			margin: 0 0 .5rem;
			margin-left: -10px !important;
			-webkit-border-radius: 0;
			border-radius: 0;
			padding: .3rem 0 .55rem;
			background-image: none;
			background-color: transparent;
			height: auto;
		}
		input, input[type=text], input[type=password], input[type=email], input[type=number], input[type=search], input[type=tel], input[type=url], input[type=reset], textarea, select {
			transition: background 0.3s;
			-moz-transition: background 0.3s;
			-webkit-transition: background 0.3s;
			-o-transition: background 0.3s;
			margin: 0;
			display: inline-block;
			vertical-align: middle;
			border: 1px solid #aaa;
			border-radius: 3px;
			color: #000;
			text-decoration: inherit;
		}
		.form-control {
			display: block;
			width: 100%;
			height: calc(2.25rem + 2px);
			padding: 0.375rem 0.75rem;
			font-size: 1rem;
			line-height: 1.5;
			color: #495057;
			background-color: #fff;
			background-clip: padding-box;
			border: 1px solid #ced4da;
			border-radius: 0.25rem;
			transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
		}
		.md-form input[type]:focus:not([readonly]) {
			-webkit-box-shadow: 0 1px 0 0 #f44d00;
			box-shadow: 0 1px 0 0 #f44d00;
			border-bottom: 1px solid #f44d00;
		}
		.md-form input[type]:focus:not([readonly])+label {
			color: #f44d00;
		}
		.md-form label {
			font-size: 1rem;
		}
		label {
			display: inline-block;
			margin-bottom: 0.5rem;
		}
		.md-form label {
			position: absolute;
			top: 1.65rem;
			left: 0;
			-webkit-transition: .2s ease-out;
			-o-transition: .2s ease-out;
			transition: .2s ease-out;
			cursor: text;
			color: #757575;
		}

		.md-form label.active {
			font-size: .8rem;
			-webkit-transform: translateY(-140%);
			-ms-transform: translateY(-140%);
			transform: translateY(-140%);
		}
		#loginButton {
			background: #f44d00 !important;
			font-size: 20px !important;
			width: 150px;
			-webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
			box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
			padding: .84rem 2.14rem;
			font-size: .81rem;
			-webkit-transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
			-o-transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
			transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
			transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
			margin: .375rem;
			border: 0;
			-webkit-border-radius: .125rem;
			border-radius: .125rem;
			cursor: pointer;
			text-transform: uppercase;
			white-space: normal;
			word-wrap: break-word;
			color: #fff;

			font-weight: 400;
			text-align: center;
		}


		@media all and (max-width: 750px) {
			/*.edge-flex-container {*/
			/*    padding-right: 0 !important;*/
			/*}*/
			/*.login-card-container > .card {*/
			/*    margin-top: 0px !important;*/
			/*}*/
			/*.wrapper {*/
			/*    overflow: auto !important;*/
			/*}*/
			.mobile-block {
				position: relative !important;
			}
			#loginButton {
				margin: auto !important;
			}
			#loginImageCol {
				display: none !important;
			}
			.login-form-container {
				min-width: 320px;
			}
		}
	</style>
</head>
<body>
<div class="wrapper">
	<div class="login-conatiner flex-center row ">
		<div class="col-auto login-card-container">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-7" id="loginImageCol">
							<img src="../../../application/assets/new/images/backgrounds/Information-Message.png" class="card-image">
						</div>
						<div class="col-5">
							<div class="login-form-container my-5">
								<div class="row mb-3">
									<div class="col">
										<h1><c:out value="${title}" /></h1>
									</div>
								</div>
								<div class="row mb-3">
									<div class="col">
										<span><c:out value="${message}" /></span>
									</div>
								</div>
								<div class="row">
									<div class="col"></div>
									<div class="col-auto">
										<c:if test="${showLogin}">
											<a href="/" class="submit g-recaptcha btn btn-primary btn-float btn-block btn-sm waves-effect submitButton" id="loginButton">Login</a>
										</c:if>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="helper-person">
					<img src="../../application/assets/new/images/backgrounds/dcperson1.png" style="
                                        position: absolute;
                                    top: 50%;
                                    right: 0;
                                    transform: translateX(65%);
                                ">
				</div>
			</div>
			<!-- start: COPYRIGHT -->
<%--			<div class="copyright" style="">--%>
<%--				TravelDocs version <%=ESAPI.encoder().encodeForHTML(ClientApplicationProperties.sharedInstance().getApplicationVersion()) %> &copy; Copyright TravelDocs 2017-<%= currentYear %>--%>
<%--			</div>--%>
		</div>
	</div>
</div>
</body>
</html>