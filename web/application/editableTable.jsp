<%@page import="au.corporateinteractive.qcloud.proposalbuilder.moduleActions.StyleGenerationItem"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.json.pricingStyles.PricingTableParser" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.json.pricingStyles.PricingTableStyle" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.json.pricingStyles.util.VersionParserIntf" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.json.util.version.VersionManager" %>
<%@ page import="webdirector.db.client.ClientFileUtils" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.StaticTextDataAccess" %>
<%@ page import="java.util.Currency" %>
<%@ page import="java.text.Normalizer" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/css/editableTable.css", request) %>" type="text/css"/>

    <link rel="stylesheet" href="/application/assets/plugins/iconPicker/fontawesome/dist/css/fontawesome-iconpicker.min.css" type="text/css"/>

<%
String currencySymbol = StyleGenerationItem.currencySymbolWithFallback(company.getString(UserAccountDataAccess.C1_LOCALE), "en-US");
String currencySymbolHtml = "\"" + ESAPI.encoder().encodeForCSS(Normalizer.normalize(currencySymbol, Normalizer.Form.NFKD)) + "\"";
%>
	<style>
		.dataRow.separator td{
			padding:0px;
		}
		.dataRow.subTotalRow{
			font-size: 14px;
    		font-weight: bold;
		}
		.currency:before{
			content: <%=currencySymbolHtml%>;
		}
	</style>
</head>
<body>

<div class="table-heading">
    <span class="input-icon">
        <div class="noTitleSection">
            <input type="text" id="pricing-title" style="padding-left:18px;width: 100%;" placeholder="So what's it going to cost?" class="sectionTitle" value="<%=company.getString(uada.C1_PRICING_TITLE) %>">
        </div>
    </span>
</div>
<div class="table-content">
    <button style="display:none;" class="btn btn-link add-col">Add Column</button>
    <button style="display:none;" class="btn btn-link add-row">Add Row</button>
    <%
    if (parser != null)
    {
    %>
    <%= parser.parse("Editor", userId) %>
    <% }
    //This isn't really needed anymore as the default values should be generated through converters but this is left here as a fallback.
    else
    { %>

    <div id="tblDiv" class="responsive-table striped tbl">
        <table id="dragableTbl" data-version="<%= PricingTableStyle.CURRENT_VERSION %>" class="table stripped">
            <thead>
            <tr class="tableHeader iconRow hand" valign="top">
                <th class="leftAlign" data-type="products" data-alignment="left">
                    <div class="dropdown-group">
                        <span data-toggle="dropdown"class="options options-column"><i class="fa fa-bars"></i></span>
                        <ul class="dropdown-menu">
                            <li><a href="#" class="alignColumnLeft">Left Align</a></li>
                            <li><a href="#" class="alignColumnCenter">Center Align</a></li>
                            <li><a href="#" class="alignColumnRight">Right Align</a></li>
                        </ul>
                    </div>
                    <i class="fa ifont fa-shopping-cart picker-target fa-3x" data-pricingicon="fa-shopping-cart"></i>
                    <input type="text" class="form-control input icp-auto" value="PRODUCTS/SERVICE"/></th>
                <th data-type="quantity" data-alignment="center">
                    <div class="dropdown-group">
                    <span data-toggle="dropdown"class="options options-column"><i class="fa fa-bars"></i></span>
                    <ul class="dropdown-menu">
                        <li><a href="#" class="alignColumnLeft">Left Align</a></li>
                        <li><a href="#" class="alignColumnCenter">Center Align</a></li>
                        <li><a href="#" class="alignColumnRight">Right Align</a></li>
                        </ul>
                    </div>
                    <i class="fa ifont fa-hashtag picker-target fa-3x" data-pricingicon="fa-hashtag"></i><input type="text" class="form-control input" value="QTY"/>
                </th>
                <th data-type="term" data-alignment="center">
                    <div class="dropdown-group">
                        <span data-toggle="dropdown"class="options options-column"><i class="fa fa-bars"></i></span>
                        <ul class="dropdown-menu">
                            <li><a href="#" class="alignColumnLeft">Left Align</a></li>
                            <li><a href="#" class="alignColumnCenter">Center Align</a></li>
                            <li><a href="#" class="alignColumnRight">Right Align</a></li>
                        </ul>
                    </div>
                    <i class="fa ifont fa-clock-o picker-target fa-3x" data-pricingicon="fa-clock-o"></i><input type="text" class="form-control input" value="TERM"/>
                </th>
                <th data-type="price">
                    <div class="dropdown-group" data-alignment="center">
                        <span data-toggle="dropdown"class="options options-column"><i class="fa fa-bars"></i></span>
                        <ul class="dropdown-menu">
                            <li><a href="#" class="alignColumnLeft">Left Align</a></li>
                            <li><a href="#" class="alignColumnCenter">Center Align</a></li>
                            <li><a href="#" class="alignColumnRight">Right Align</a></li>
                        </ul>
                    </div>
                    <i class="fa ifont moneytaryValue picker-target fa-3x" data-pricingicon="moneytaryValue"> </i><input type="text" class="form-control input" value="PRICE"/>
                </th>
                <th data-type="total">
                    <div class="dropdown-group" data-alignment="center">
                        <span data-toggle="dropdown"class="options options-column"><i class="fa fa-bars"></i></span>
                        <ul class="dropdown-menu">
                            <li><a href="#" class="alignColumnLeft">Left Align</a></li>
                            <li><a href="#" class="alignColumnCenter">Center Align</a></li>
                            <li><a href="#" class="alignColumnRight">Right Align</a></li>
                        </ul>
                    </div>
                    <i class="fa ifont moneytaryValue picker-target fa-3x" data-pricingicon="moneytaryValue"> </i><input type="text" class="form-control input" value="TOTAL Exc. TAX"/>
                </th>
                <th data-type="addition">
                    <div class="dropdown-group" data-alignment="center">
                        <span data-toggle="dropdown"class="options options-column"><i class="fa fa-bars"></i></span>
                        <ul class="dropdown-menu">
                            <li><a href="#" class="alignColumnLeft">Left Align</a></li>
                            <li><a href="#" class="alignColumnCenter">Center Align</a></li>
                            <li><a href="#" class="alignColumnRight">Right Align</a></li>
                        </ul>
                    </div>
                    <i class="fa ifont moneytaryValue picker-target fa-3x" data-pricingicon="moneytaryValue"> </i><input type="text" class="form-control input" value="ADDITION FEE"/>
                </th>
                <th data-type="tax">
                    <div class="dropdown-group" data-alignment="center">
                        <span data-toggle="dropdown"class="options options-column"><i class="fa fa-bars"></i></span>
                        <ul class="dropdown-menu">
                            <li><a href="#" class="alignColumnLeft">Left Align</a></li>
                            <li><a href="#" class="alignColumnCenter">Center Align</a></li>
                            <li><a href="#" class="alignColumnRight">Right Align</a></li>
                        </ul>
                    </div>
                    <i class="fa ifont moneytaryValue picker-target fa-3x" data-pricingicon="moneytaryValue"> </i><input type="text" class="form-control input" value="TAX"/>
                </th>
                <th data-type="total_inc_tax">
                    <div class="dropdown-group" data-alignment="center">
                        <span data-toggle="dropdown"class="options options-column"><i class="fa fa-bars"></i></span>
                        <ul class="dropdown-menu">
                            <li><a href="#" class="alignColumnLeft">Left Align</a></li>
                            <li><a href="#" class="alignColumnCenter">Center Align</a></li>
                            <li><a href="#" class="alignColumnRight">Right Align</a></li>
                        </ul>
                    </div>
                    <i class="fa ifont moneytaryValue picker-target fa-3x" data-pricingicon="moneytaryValue"> </i><input type="text" class="form-control input" value="TOTAL"/>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="leftAlign product-item">
                    <p>Service Line Item</p>
                    <p class="notes">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </td>
                <td><p>99</p></td>
                <td><p>12</p></td>
                <td><p class="currency">999.00</p></td>
                <td><p class="currency">9999.00</p></td>
                <td><p class="currency">9999.00</p></td>
                <td><p class="currency">9999.00</p></td>
                <td><p class="currency">9999.00</p></td>
            </tr>
            <tr>
                <td class="leftAlign"><p>Discount</p></td>
                <td><p>99</p></td>
                <td><p>12</p></td>
                <td><p class="currency">999.00</p></td>
                <td><p class="currency">9999.00</p></td>
                <td><p class="currency">9999.00</p></td>
                <td><p class="currency">9999.00</p></td>
                <td><p class="currency">9999.00</p></td>
            </tr>
            <tr class="dataRow separator"><td colspan="8"></td></tr>
            <tr class="dataRow subTotalRow">
                <td class="leftAlign"><p><input type="text" data-mapping="TEXT_PRICING_BODY_TOTAL" class="form-control input" value="Totals"></p></td>
                <td></td>
                <td></td>
                <td></td>
                <td><p class="currency">9999.00</p></td>
                <td><p class="currency">9999.00</p></td>
                <td><p class="currency">9999.00</p></td>
                <td><p class="currency">9999.00</p></td>
            </tr>
            <tr class="dataRow separator"><td colspan="8"></td></tr>
            <tr>
                <td></td>
                <td></td>
                <td><input type="text" class="form-control " data-mapping="TEXT_PRICING_FOOTER_PRICE" value="Price"/></td>
                <td><input type="text" class="form-control " data-mapping="TEXT_PRICING_FOOTER_DISCOUNTS" value="Discount"/></td>
                <td><input type="text" class="form-control " data-mapping="TEXT_PRICING_FOOTER_ADDITION" value="Additions"/></td>
                <td><input type="text" class="form-control " data-mapping="TEXT_PRICING_FOOTER_TAX" value="Tax"/></td>
                <td><input type="text" class="form-control " data-mapping="TEXT_PRICING_FOOTER_TOTAL" value="Total"/></td>
            </tr>
            <tr>
                <td colspan="2"><input type="text" class="form-control " data-mapping="TEXT_PRICING_FOOTER_ONE_OFF_COST" value="One-off cost"/></td>
                <td><p class="currency">999.00</p></td>
                <td><p class="currency">9999.00</p></td>
                <td><p class="currency">9999.00</p></td>
                <td><p class="currency">9999.00</p></td>
                <td><p class="currency">9999.00</p></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="2"><input type="text" class="form-control " data-mapping="TEXT_PRICING_FOOTER_ONE_OFF_DISCOUNTS" value="One-off Discounts"/></td>
                <td><p class="currency">9999.00</p></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="2"><input type="text" class="form-control " data-mapping="TEXT_PRICING_FOOTER_FULL_TERM_COST" value="Full Term Cost"/></td>
                <td><p class="currency">9999.00</p></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="2"><input type="text" class="form-control " data-mapping="TEXT_PRICING_FOOTER_TAX" value="Tax"/></td>
                <td><p class="currency">9999.00</p></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="2"><input type="text" class="form-control " data-mapping="TEXT_PRICING_FOOTER_SUB_TOTAL" value="Sub-total"/></td>
                <td><p class="currency">9999.00</p></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="2"><input type="text" class="form-control " data-mapping="TEXT_PRICING_FOOTER_TOTAL" value="Total"/></td>
                <td><p class="currency">9999.00</p></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="2"><input type="text" class="form-control " data-mapping="TEXT_PRICING_FOOTER_OVERALL_TOTAL" value="Grand Total"/></td>
                <td><p class="currency">9999.00</p></td>
            </tr>
            </tbody>
        </table>
        <div class="contentDiv ">
            <div class="contentHolder">
                <%-- <div id="footerContent" class="editable-content" id="static-want-more-detail">
                    <%= new StaticTextDataAccess().readContentWithUserId(userId, StaticTextDataAccess.EHEADLINE_PRICING_WANT_MORE_DETAIL) %>
                </div> --%>
                <div class="panel-froala" id="static-want-more-detail">
                	<%= new StaticTextDataAccess().readContentWithUserId(userId, StaticTextDataAccess.EHEADLINE_PRICING_WANT_MORE_DETAIL) %>
                </div>
            </div>
        </div>
    </div>
</div>

<% } %>

<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<!-- Froala Editor -->
<script src="/application/assets/plugins/froala-editor/custom/config-proposalFreetext.js"></script>

<script src="<%=UrlUtils.autocachedUrl("/application/js/editableTable.js", request)%>"></script>


</body>
</html>