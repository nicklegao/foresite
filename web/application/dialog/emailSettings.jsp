<%@page import="com.stripe.model.Customer"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.StripeService"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.corporateinteractive.qcloud.subscription.SubscriptionPlan"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ page import="au.net.webdirector.common.datalayer.client.ModuleAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.base.db.entity.MultiOptions" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.EmailTemplateDataAccess" %>
<%
UserAccountDataAccess uada = new UserAccountDataAccess();
String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
CategoryData companyData = uada.getUserCompanyForUserId(userId);
String companyName = uada.getUserCompanyNameForUserId(userId);
Customer stripeCustomer = StripeService.getInstance().retrieveStripeCustomer(companyData);
String cardNumber = StripeService.getLast4CardNumber(stripeCustomer);
%>
<div id="account-settings">
    <style>
    input.form-control {
      padding: 0 10px;
    }
    select.form-control {
      padding: 0 6px;
    }
    .smart-form *, .smart-form *:after, .smart-form *:before {
      box-sizing: border-box;
    }
    .smart-form .col-block {
        min-height: 1px;
        padding-right: 15px;
        padding-left: 15px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        clear: both;
    }
    /**
     * The CSS shown here will not be introduced in the Quickstart guide, but shows
     * how you can use CSS to style your Element's container.
     */
    .StripeElement {
      background-color: white;
      padding: 9px;
      border: 1px solid #ccc;
      min-height: 42px;
    }
    
    .StripeElement--focus {
      box-shadow: 0 1px 3px 0 #cfd7df;
    }
    
    .StripeElement--invalid {
      border-color: #fa755a;
    }
    
    .StripeElement--webkit-autofill {
      background-color: #fefde5 !important;
    }
    
    #card-element-placeholder .card-number{
      color: #32325d;
      line-height: 24px;
      font-size: 16px;
      height: 24px;
    }
    p.note{
      font-size: 11px;
    }    
    img.stripe-seal{
      width: 130px;
      display: block;
      float: left;
      margin-right: 4px;
    }
    </style>
	<form class="form-account-settings" method="POST">
	<div class="modal-header">
		<div class="titleBar">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">
				<i class="fal fa-times-circle"></i>
			</button>
		</div>
		<h2 class="modal-title">Settings</h2>
	</div>
	<div class="modal-body smart-form">
		<div class="form-horizontal">
		
		<div class="company-details">
		    <header>Company Details</header>
			<div class="row">
				<%
					EmailTemplateDataAccess etda = new EmailTemplateDataAccess();
					String content = etda.readContentWithUserId(companyData.getId(), "Proposal Ready");
				%>
				<section class="col col-10">
					<label for="subject1" class="">Enter Sales Quote Ready Email Subject</label>
					<input value="<%=ESAPI.encoder().encodeForHTMLAttribute(etda.readSubjectWithUserId(companyData.getId(), "Proposal Ready"))%>" name="subject1" type="text" class="form-control" id="subject1" placeholder="Write subject here..." aria-required="true" aria-invalid="false">
				</section>
				<section class="col col-12">
					<label for="email-template-ready">Sales Quote Ready Template</label>
					<div class="panel-froala" id="email-template-ready"><%= content %></div>
				</section>
				<section class="col col-12">
					<button type="button" class="btn btn-warning loadDefaultEmailTemplate" style="padding: 6px 12px;" data-name="Proposal Ready" data-editor="#email-template-ready">
						<i class="fa fa-sync" aria-hidden="true"></i> Load Default
					</button>
				</section>
			</div>
			<div class="row">
				<%
					String content2 = etda.readContentWithUserId(companyData.getId(), "Proposal New Revision Ready");
				%>
				<section class="col col-10">
					<label for="subject2" class="">Enter New Revision Ready Email Subject</label>
					<input value="<%=ESAPI.encoder().encodeForHTMLAttribute(etda.readSubjectWithUserId(companyData.getId(), "Proposal New Revision Ready"))%>" name="subject2" type="text" class="form-control" id="subject2" placeholder="Write subject here..." aria-required="true" aria-invalid="false">
				</section>
				<section class="col col-12">
					<label for="email-template-revision">New Revision Ready Template</label>
					<div class="panel-froala" id="email-template-revision"><%= content2 %></div>
				</section>
				<section class="col col-12">
					<button type="button" class="btn btn-warning loadDefaultEmailTemplate" style="padding: 6px 12px;" data-name="Proposal New Revision Ready" data-editor="#email-template-revision">
						<i class="fa fa-sync" aria-hidden="true"></i> Load Default
					</button>
				</section>
			</div>
		</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" id="closeForm" class="btn btn-danger"
			data-dismiss="modal">
			<i class="fa fa-times"></i> Close
		</button>
		<button type="submit" class="btn btn-success">
        	<i class="fa fa-save"></i> Save
      	</button>
	</div>
	</form>
</div>
<script>
var stripe_pk = '<%= StripeService.getPublishableKey() %>';
</script>
<script src="/application/js/form-validation/abn.js"></script>
<script src="/application/js/form-validation/modal-account-settings.js"></script>
