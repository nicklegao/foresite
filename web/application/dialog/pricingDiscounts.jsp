<%@page import="au.corporateinteractive.qcloud.proposalbuilder.moduleActions.StyleGenerationItem"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Comparator"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.Locale"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page	import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page	import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.OptionsDataAccess"%>
<%@page import="java.util.List"%>
<%

int customFields = 5;

UserAccountDataAccess uada = new UserAccountDataAccess();
String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
CategoryData company = uada.getUserCompanyForUserId(userId);
String currencySymbol = StyleGenerationItem.currencySymbolWithFallback(company.getString(UserAccountDataAccess.C1_LOCALE), "en-US");
%>

<script type="text/javascript">
	var currencySymbol = '<%=currencySymbol%>';
</script>
<style>
.form-pricing-discounts .col-sm-4, .form-pricing-discounts .col-sm-3 {
    padding-right: 0px;
}
</style>
<div id="pricing-discounts">
	<form class="form-pricing-discounts" method="POST">
		<div class="modal-header">
			<div class="titleBar">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					<i class="fal fa-times-circle"></i>
				</button>
			</div>
			<h2 class="modal-title">Pricing Discounts &amp; Thresholds</h2>
		</div>
		
		<div class="modal-body">
			<div class="row">
	   			<div class="col-sm-4">
	   				<div class="form-group">
	    				Label
	   				</div>
	   			</div>
	   			<div class="col-sm-4">   
	   				<div class="form-group"> 
	   					Percentage (%)	
					</div>
				</div>
	   			<div class="col-sm-3">
	   				<div class="form-group">
	   					Threshold (<%=currencySymbol%>) <a href="#" data-toggle="popover" data-placement="bottom" data-trigger="focus">(?)</a>
	     			</div>
	   			</div>
	   			<div class="col-sm-1">
	   				<div class="form-group">
	    			</div>
	    		</div>
			</div>
			<div id="fields">
			
			</div>
	   		<a class="btn btn-primary addDiscount">
	        	<i class="fa fa-plus"></i> Add
	      	</a>
		</div>
		
		<div class="modal-footer">
			<button type="button" id="closeForm" class="btn btn-danger"
				data-dismiss="modal">
				<i class="fa fa-times"></i> Close
			</button>
			<button type="submit" class="btn btn-success">
	        	<i class="fa fa-save"></i> Save
	      	</button>
		</div>
	</form>
	
	<div id="fragments" style="display: none;">
		<div class="row">
   			<div class="col-sm-4">
   				<div class="form-group">
    				<input type="text" class="form-control pricing-label" name=pricing-label- placeholder="label" required>
   				</div>
   			</div>
   			<div class="col-sm-3">
   				<div class="form-group">
	   				<div class="input-group">
						<input type="number" class="form-control pricing-percentage" min="0" max="100" name="pricing-percentage" data-value="discount" placeholder="%" required>
						<div class="input-group-btn">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="button-label">Discount</span> <span class="caret"></span></button>
							<ul class="dropdown-menu discount-type">
								<li><a href="#" data-value="discount">Discount</a></li>
								<li><a href="#" data-value="markup">Markup</a></li>
							</ul>
						</div>
					</div>   
				</div>
			</div>
   			<div class="col-sm-4">
   				<div class="form-group">
   					<div class="input-group">
     					<input type="number" min="0" class="form-control pricing-threshold" name="pricing-threshold" placeholder="threshold">
				    </div>
     			</div>
   			</div>
   			<div class="col-sm-1">
   				<a class="btn btn-bricky btn-xs delete tooltips" data-placement="top" data-original-title="Delete" style="height: 22px;width: 22px;color: #fff;background-color: #ba131a;margin-top: 5px;"> 
					<i class="fa fa-times"> </i>
				</a>
    		</div>
		</div>
	</div>
</div>
<script src="/application/js/form-validation/modal-pricing-discounts.js"></script>
