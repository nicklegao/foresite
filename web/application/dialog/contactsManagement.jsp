<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/css/contentLibrary.css",request) %>" />
<div class="modal-header">
	<div class="titleBar">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			<i class="fal fa-times-circle"></i>
		</button>
	</div>
	<h2 class="modal-title">Manage Contacts</h2>
</div>
<div class="modal-body">
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
	  <li role="presentation" class="active"><a href="#contacts" aria-controls="items" role="tab" data-toggle="tab">Contacts</a></li>
	</ul>
	
	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="contacts">
			<%--<table class="table table-hover contacts-table" id="contacts-table">--%>
				<%--<thead class="searchHead">--%>
					<%--<tr >--%>
						<%--<th>Contact Email</th>--%>
						<%--<th>First Name</th>--%>
						<%--<th>Last Name</th>--%>
						<%--<th>Company</th>--%>
						<%--<th>Address</th>--%>
						<%--<th>Action</th>--%>
					<%--</tr>--%>
					<%--<tr class="item-filters filters">--%>
						<%--<th><input type="text" placeholder="Filter..." class="form-control input-sm"></th>--%>
						<%--<th><input type="text" placeholder="Filter..." class="form-control input-sm"></th>--%>
						<%--<th><input type="text" placeholder="Filter..." class="form-control input-sm"></th>--%>
						<%--<th><input type="text" placeholder="Filter..." class="form-control input-sm"></th>--%>
						<%--<th><input type="text" placeholder="Filter..." class="form-control input-sm"></th>--%>
						<%--<th>--%>
							<%--<button class="btn btn-sm btn-danger clearColumnFilters columnFiltersEnabled" style="display: inline-block;">--%>
								<%--<i class="fa fa-eraser"></i> Clear Filters</button>--%>
						<%--</th>--%>
					<%--</tr>--%>
				<%--</thead>--%>
				<%--<tbody>--%>
					<%--<c:forEach var="item" items="${contacts}">--%>
						<%--<tr data-element-id="<c:out value="${item.element_id}" />">--%>
							<%--<td><c:out value="${item.attr_headline}" /></td>--%>
							<%--<td><c:out value="${item.attr_firstname}" /></td>--%>
							<%--<td><c:out value="${item.attr_secondname}" /></td>--%>
							<%--<td><c:out value="${item.attr_companyname}" /></td>--%>
							<%--<td><c:out value="${item.attr_address}" /></td>--%>
							<%--<td>--%>
								<%--<div class="btn-group">--%>
								  <%--<button type="button" class="btn btn-default edit-contacts-action">--%>
								  	<%--<i class="fa fa-pencil-square" aria-hidden="true"></i>--%>
								  	<%--Edit--%>
								  <%--</button>--%>
								  <%--<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--%>
								    <%--<span class="caret"></span>--%>
								    <%--<span class="sr-only">Toggle Dropdown</span>--%>
								  <%--</button>--%>
								  <%--<ul class="dropdown-menu">--%>
								    <%--<li><a href="#" class="delete-contact-action">Delete</a></li>--%>
								  <%--</ul>--%>
								<%--</div>--%>
							<%--</td>--%>
						<%--</tr>--%>
					<%--</c:forEach>--%>
				<%--</tbody>--%>
			<%--</table>--%>

			<table class="table table-hover contacts-table" id="contacts-table"></table>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button class="btn btn-primary pull-left add-contacts-action">
		<i class="fa fa-plus-square-o" aria-hidden="true"></i>
		&nbsp;Add Contact
	</button>
	<button class="btn btn-primary pull-left export-all-contacts-action">
		<i class="fa fa-download" aria-hidden="true"></i>
		&nbsp;Export All Contacts
	</button>
	<button class="btn btn-primary pull-left download-sample-contacts-action">
		<i class="fa fa-download" aria-hidden="true"></i>
		&nbsp;Download Sample CSV File
	</button>
	<button class="btn btn-primary pull-left import-all-contacts-action">
		<i class="fa fa-upload" aria-hidden="true"></i>
		&nbsp;Import All Contacts
	</button>
	<button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> Close</button>
</div>

<script src="/application/js/contactsManagement.js"></script>
<!-- Froala Editor -->
<script src="/application/assets/plugins/froala-editor/custom/config-proposalFreetext.js"></script>
