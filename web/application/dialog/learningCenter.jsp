<div id="disk-usage">
    <div class="modal-header-new">
        <div class="titleBarNew">
            <button type="button" class="close" data-dismiss="modal"
                    aria-hidden="true">
                <i class="fal fa-times-circle"></i>
            </button>
        </div>
        <h2 class="modal-title">Learning Center</h2>
    </div>

    <div class="modal-body-new smart-form">
        <div class="modal-header-new">
            <div class="container">
                <ul>
                    <li class="one"><a href="#">Home</a></li>
                    <li class="two"><a href="#">Dashboard Guide</a></li>
                    <li class="three"><a href="#">Editor Guide</a></li>
                    <li class="four"><a href="#">Help Request</a></li>
                    <li class="five"><a href="#">Support Chat</a></li>
                    <li class="six"><a href="#">Support Center</a></li>
                    <hr />
                </ul>
            </div>
        </div>

        <p style="float: right;font-size: 9px;padding-right: 18px;padding-bottom: 12px;">The above usage statistics are updated daily</p>
    </div>
</div>