
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.CoverPage"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.control.CoverPageController"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Set"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.CustomFieldsDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.CustomFields"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="java.util.Hashtable"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>

<%

	String dialogTitle = "Create new TravelDoc Template";



	String copyProposal = request.getParameter("copyProposal");
	Hashtable<String, String> copyTarget = null;
	String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
	UserAccountDataAccess uada = new UserAccountDataAccess();
	TravelDocsDataAccess pda = new TravelDocsDataAccess();

	boolean estimatedValue;
	CustomFields cf;
	


	CustomFieldsDataAccess cfda = new CustomFieldsDataAccess();
	TUserRolesDataAccess urda = new TUserRolesDataAccess();

	CategoryData company = uada.getUserCompanyForUserId(userId);

	UserPermissions up = urda.getPermissions(userId);


	String corporateCSS = String.format("/%s/%s/Categories/%s/gen/css-document/dashboard-colours.css",
			Defaults.getInstance().getStoreContext(),
			UserAccountDataAccess.USER_MODULE,
			company.getId());

	if (copyProposal != null
			&& Integer.parseInt(copyProposal) > 0)
	{
		Set<String> accessRights = uada.getUserAccessRightsSet(userId);

		copyTarget = pda.getTravelDocById(copyProposal);

		if (accessRights.contains(copyTarget.get(TravelDocsDataAccess.E_OWNER)) == false)
		{
			//Not allowed to copy this proposal
			copyTarget = null;
			copyProposal = null;
		}
		else
		{
			dialogTitle = "Copy existing proposal";
		}
	}
	else
	{
		copyProposal = null;
	}

	List<CoverPage> coverPageList = new CoverPageController().coverPageList(company, userId, company.getId());



%>
<script src="/application/assets/plugins/jquery.easy-autocomplete/jquery.easy-autocomplete.min.js" data-poll="1000" data-relative-urls="false"></script>
<link rel="stylesheet" type="text/css" media="screen" href="/application/assets/plugins/jquery.easy-autocomplete/easy-autocomplete.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="/application/assets/plugins/jquery.easy-autocomplete/easy-autocomplete.themes.min.css">
<script>

	$(document).ready(function(){


		$('.non-filled').keyup(function(){
			if($(this).val() == ''){
				$(this).addClass('non-filled');
			}else{
				$(this).removeClass('non-filled');
			}
		})

		$('.table-checkbox').iCheck({
			checkboxClass : 'icheckbox_square-orange'
		});
	});

</script>

<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl(corporateCSS, request, false, true) %>" />
<div class="modal-header">
	<div class="titleBar">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fal fa-times-circle"></i></button>
	</div>
	<h2 class="modal-title"><%=ESAPI.encoder().encodeForHTML(dialogTitle) %></h2>
</div>
<form class="form-create-traveldoc form-horizontal" action="/api/traveldoc/create" method="POST">
	<div class="form-steps">
		<div class="modal-body">
			<div class="form-bootstrapWizard">
				<ul class="bootstrapWizard form-wizard two-steps">
					<li class="active" data-target="#step1">
						<a href="#tab1" data-toggle="tab"> <span class="step">1</span> <span class="title">Basic Information</span> </a>
					</li>
					<li data-target="#step2">
						<a href="#tab2" data-toggle="tab"> <span class="step">2</span> <span class="title">Cover Page</span> </a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="tab-content" style="margin-top: 30px">
				<div class="tab-pane active" id="tab1">
					<h3><strong>Step 1 </strong> - Basic Information</h3>

					<div class="form-group step1">
						<label for="proposalTitle" class="col-sm-3 control-label">
							Proposal Title:
						</label>
						<div class="col-sm-9">
							<input type="text" id="proposalTitle" class="form-control" name="custProposalTitle" placeholder="Main title of this proposal">
						</div>
					</div>

					<div class="form-group step2">
						<label class="col-sm-3 control-label">
							Color scheme:
						</label>
						<div class="col-sm-9">
							<div id="colorPicker" class="btn-group btn-block" data-toggle="buttons">
								<label class="btn btn-qcloud-design-palette1 active">
									<input type="radio" name="spectrumColor" value="palette1" checked> <i class="fa fa-check selectedIndicator"></i>
								</label>
								<label class="btn btn-qcloud-design-palette2">
									<input type="radio" name="spectrumColor" value="palette2"> <i class="fa fa-check selectedIndicator"></i>
								</label>
								<label class="btn btn-qcloud-design-palette3">
									<input type="radio" name="spectrumColor" value="palette3"> <i class="fa fa-check selectedIndicator"></i>
								</label>
								<label class="btn btn-qcloud-design-palette4">
									<input type="radio" name="spectrumColor" value="palette4"> <i class="fa fa-check selectedIndicator"></i>
								</label>
							</div>
							<label class="qc-label-info">This colour palette can be changed in the "Edit Styles" option in the Configuration menu.</label>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="tab2">
					<h3><strong>Step 2</strong> - Cover Page</h3>
					<div class="form-group step3">
						<label class="col-sm-3 control-label">
							Cover Page Image:
						</label>
						<div class="col-sm-9">
							<div id="coverPicker">
								<%
									int selectedCover = -1;
									for(CoverPage cover : coverPageList) {
								%>
								<label class="coverPageOption flipInY animated <%=selectedCover==0?"active":"" %>" pageId="<%= cover.getId() %>">
									<img class="coverpageImage" src="<%= cover.getThumbnail() %>"/>
									<div class="overlayBox topBox">Proposal</div>
									<div class="overlayBox bottomBox documentTag" tag="companyName"></div>
									<i class="fa fa-check"></i>
								</label>
								<%
										if (selectedCover == -1)
										{
											selectedCover = Integer.parseInt(cover.getId());
										}
									} %>
							</div>
							<input id="coverPageHiddenField" type="hidden" class="ignore" name="coverPageId" value="<%=selectedCover %>"/>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal-footer">
			<ul class="pager wizard no-margin">
				<li class="previous disabled">
					<a href="javascript:void(0);" class="btn btn-lg btn-default previousButton"> Previous </a>
				</li>
				<li class="c">
					<a href="javascript:void(0);" class="btn btn-lg btn-default nextButton"> Next </a>
				</li>
				<li>
					<button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
				</li>
				<li class="next finish" style="display:none;">
					<button type="submit" class="btn btn-success nextButton"><i class="fa fa-save"></i> Create </button>
				</li>
			</ul>
			<!--
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Create</button> -->
		</div>
	</div>
</form>
<style>
	.pac-container {
		z-index: 1100 !important;
	}
</style>