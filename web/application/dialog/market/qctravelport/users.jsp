<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelportDataAccess"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page
        import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page
        import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor"
           prefix="compress"%><compress:html enabled="true"
                                             compressJavaScript="false" compressCss="true">

        <%
            UserAccountDataAccess uada = new UserAccountDataAccess();
            String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
            String companyId = uada.getUserCompanyForUserId(userId).getId();
            String id = request.getParameter("id");

            ElementData travelportData = new TravelportDataAccess().getTravelportUserByUserId(id);
            String travelportConsultantId = travelportData == null ? "" : travelportData.getString(TravelportDataAccess.E_CONSULTANT_ID);
        %>
    <script>
		var userID = <%=id %>;

    </script>

        <form id="settings-travelport-users" method="POST">
            <div class="modal-header">
                <div class="titleBar">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">
                        <i class="fal fa-times-circle"></i>
                    </button>
                </div>
                <h2 class="modal-title">Travelport Itinerary Settings</h2>
            </div>
            <div class="modal-body smart-form">
                <div class="" action="" style="background: #fff; padding: 20px;">
                    <header>Travelport Consultant Information</header>
                    <div class="row">
                        <section class="col col-10">
                        	<div class="form-group">
	                            <label for="travelportConsultantId">Consultant ID</label>
	                            <input
	                                    class="form-control" type="text" id="travelportConsultantId"
	                                    name="travelportConsultantId"
	                                    value="<%=(travelportConsultantId == null ? "" : travelportConsultantId)%>" />
	                         </div>
                        </section>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
                    <i class="fa fa-times"></i> Close
                </button>

                <button id="saveUserbutton" type="submit" class="btn btn-success">
                    <i class="fa fa-save"></i> Save
                </button>

            </div>
        </form>

    <script>
    $(document).ready(function()
		{
			var $modal = $("#settings-travelport-users").closest(".modal");

			$('#settings-travelport-users').validate({
				errorElement : 'span',
				errorClass : 'help-block',
				errorPlacement : function(error, element)
				{
					error.insertAfter(element);
				},
				ignore : '',
				rules : {
					travelportConsultantId : {
						remote : '/api/travelportIdValid?id=' + userID
					}
				},
				messages : {
					travelportConsultantId : {
						remote : 'User with this consultant id already exists.'
					}
				},
				highlight : function(element)
				{
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
				},
				unhighlight : function(element)
				{
					$(element).closest('.form-group').removeClass('has-error');
				},
				success : function(label, element)
				{
					$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
				},
				submitHandler : function(form)
				{
					$modal.modal('hide');
					startLoadingForeground();
					var $form = $(form);
					var travelportConsultantId = $('#travelportConsultantId').val();
					var data = {
							"travelportConsultantId": travelportConsultantId, 
							"id": userID
						};
					$.ajax({
						url : '/api/market/travelport/updateUser',
						data : data,
						success : function(data)
						{
							stopLoadingForeground();
							if (data.success === false)
							{
								swal({
									title : 'Error',
									type : 'error',
									text : data.message
								})
							}

						},
						error : function()
						{
							stopLoadingForeground();

							alert("Unable to update your travelport user settings. Please try again later.");
						}
					});
				}
			});

		});
    </script>
</compress:html>