<%@page import="au.corporateinteractive.qcloud.market.enums.Market"%>
<%@page
        import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager"%>
<%@page
        import="au.net.webdirector.common.datalayer.base.db.entity.MultiOptions"%>
<%@page import="java.util.List"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page
        import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess"%>
<%@page
        import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="java.util.Hashtable"%>
<%@page
        import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.*" %>
<%@ page import="java.util.Vector" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor"
           prefix="compress"%><compress:html enabled="true"
                                             compressJavaScript="true" compressCss="true">

    <%
        UserAccountDataAccess uada = new UserAccountDataAccess();
        TravelDocsDataAccess pda = new TravelDocsDataAccess();
        String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
        String companyId = uada.getUserCompanyForUserId(userId).getId();
        CategoryData company = uada.getUserCompanyForUserId(userId);
        String teamId = request.getParameter("id");

        CategoryData team = uada.getTeamForTeamId(teamId);

        CategoryData sabreTeamCategory = new SabreDataAccess().getCategoryByTeamId(teamId);
    %>

    <div id="settings-qcpayment-teams" data-team="<%=teamId %>">
        <div class="modal-header">
            <div class="titleBar">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <h2 class="modal-title">QC Payments Settings</h2>
        </div>
        <div class="modal-body">
            <div class="" action="" style="background: #fff; padding: 20px;">
                <div class="form-group">
					<label for="stripePK">Stripe API Publishable key</label> 
					 <input class="form-control" type="text" id="stripePK" name="stripePK" value="<%=team == null ? "" : team.getString(UserAccountDataAccess.C2_STRIPE_PK) == null ? "" : team.getString(UserAccountDataAccess.C2_STRIPE_PK)%>" />
				</div>
				<div class="form-group">
					<label for="stripeSK">Stripe API Secret key</label> 
					 <input class="form-control" type="text" id="stripeSK" name="stripeSK" value="<%=team == null ? "" : team.getString(UserAccountDataAccess.C2_STRIPE_SK) == null ? "" : team.getString(UserAccountDataAccess.C2_STRIPE_SK)%>" />
				</div>
				<div class="form-group">
					<p><i>*The pubishable key and secret key are obtained when register on Stripe</i></p>
					<a class="btn btn-primary" target="_blank" href="//stripe.com">
						Register with Stripe
					</a>
				</div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
                <i class="fa fa-times"></i> Close
            </button>

            <button id="saveTeambutton" class="btn btn-success">
                <i class="fa fa-save"></i> Save
            </button>
        </div>
    </div>

    <script src="/application/js/market/qcpayment/teams.js"></script>
</compress:html>