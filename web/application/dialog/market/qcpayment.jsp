<%@page import="org.json.simple.JSONValue"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page	import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.*" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="au.net.webdirector.common.datalayer.client.ElementData" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager" %>
<compress:html enabled="true" compressJavaScript="true" compressCss="true">

<%

    UserAccountDataAccess uada = new UserAccountDataAccess();
    TravelDocsDataAccess pda = new TravelDocsDataAccess();
    String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
    CategoryData company = uada.getUserCompanyForUserId(userId);

    TUserRolesDataAccess urda = new TUserRolesDataAccess();
    List<CategoryData> teams = uada.getCompanyTeams(company.getId());

%>
<div id="settings-management">
        <div class="modal-header">
            <div class="titleBar">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <h2 class="modal-title">QC Payments Settings</h2>
        </div>
        <div class="modal-body smart-form">
            <div class="form-horizontal">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#teamsSettings" aria-controls="teamsSettings" role="tab" data-toggle="tab">Teams</a></li>
                </ul>


                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="teamsSettings">
                        <header>Select team to edit</header>
                        <div class="row">
                        	<table id="teamsTable" class="table table-bordered table-hover table-full-width" data-module="teams">
                        	</table>
                        	<%
                        	List<HashMap<String, String>> teamsList = new ArrayList<HashMap<String, String>>();
                        	 for (CategoryData team : teams)
                             {
                        		 HashMap<String, String> teamHash = new HashMap<String, String> ();
                        		 teamHash.put("id", team.getId());
                        		 teamHash.put("team", team.getString(UserAccountDataAccess.C_NAME));
                        		 teamsList.add(teamHash);
                             }
                        	
                        	String jTeamsList = JSONValue.toJSONString(teamsList);
                        	%>
                            <%-- <table class="table table-bordered table-hover table-full-width" id="table-teams" data-module="teams" width="100%">
                                <thead>
                                <tr>
                                    <th>Team</th>
                                    <th width="140">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <%
                                    for (CategoryData team : teams)
                                    {
                                %>
                                <tr>
                                    <td><%=team.getName()%></td>
                                    <td>
                                        <div class="btn-group" data-id="<%= team.getId() %>" >
                                            <a class="btn btn-default edit-item tooltips" style="padding: 6px 12px;" href="#">
                                                <i class="qc qc-edit"></i> Edit
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <%
                                    }
                                %>
                                </tbody>
                            </table> --%>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    <div id="fragments" style="display: none;">
        <div class="row customStatus" style="margin: 0;padding: 10px 15px;">
            <div class="col-sm-11">
                <div class="form-group" style="margin: 0;">
                    <input type="text" class="form-control reason-label" name=reason-label- placeholder="label" required>
                </div>
            </div>
            <div class="col-sm-1">
                <a class="btn btn-bricky btn-xs delete tooltips" data-placement="top" data-original-title="Delete" style="float: right;height: 22px;width: 22px;color: #fff;background-color: #ba131a;margin-top: 5px;">
                    <i class="fa fa-times"> </i>
                </a>
            </div>
        </div>
    </div>
</div>

<script src="/application/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script src="/application/assets/plugins/DataTables/media/js/dataTables.fixedColumns.min.js"></script> <!-- min version may cause performance issue in Firefox, but working fine in IE and Chrome -->
  <script src="/application/assets/plugins/DataTables/buttons/js/dataTables.buttons.min.js"></script>
  <script src="/application/assets/plugins/DataTables/buttons/js/buttons.colVis.min.js"></script>
  <script src="/application/assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
  <script src="/application/assets/plugins/DataTables/dataTables.colReorder.min.js"></script>
  <script src="/application/assets/plugins/DataTables/dataTables.colResize.js"></script>
<script>
	$(document).ready(function(){
		
		var teams = JSON.parse('<%=jTeamsList%>');
		var teamsTable = $('#teamsTable').DataTable({
			"autoWidth" : false,
			"serverSide" : false,
			"oLanguage" : {
				"sLengthMenu" : "Show _MENU_ teams",
				"sSearch" : "Search ",
				"sInfo" : 'Showing _START_ to _END_ of _TOTAL_ teams',
				"sInfoFiltered" : ' - filtered from _MAX_ teams',
				"sInfoEmpty" : "No teams found"
			},
			columns : [{
				title : 'Team',
				data : 'team'
			},{
				title : 'Action',
				orderable : false,
				data : 'id',
				render : function(data, type, row)
				{
					if (type == "display")
					{
						var actions = '<div class="btn-group" data-id="'+row.id+'"> <a class="btn btn-default edit-item tooltips" style="padding: 6px 12px;" href="#"> <i class="qc qc-edit"></i>Edit</a></div>';
						return actions;
					}
					return data;
				}
			}
			]
		});
		teamsTable.rows.add(teams);
		teamsTable.draw();

		$('.dataTables_filter input').addClass('form-control input-sm');
		
	});
</script>

<script src="/application/js/market/qcpayment.js"></script>

</compress:html>


