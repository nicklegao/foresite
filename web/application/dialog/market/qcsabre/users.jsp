<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.SabreDataAccess"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page
        import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess"%>
<%@page
        import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page
        import="au.corporateinteractive.qcloud.proposalbuilder.db.MaestranoUserAccountDataAccess"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="java.util.Hashtable"%>
<%@page
        import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page
        import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor"
           prefix="compress"%><compress:html enabled="true"
                                             compressJavaScript="true" compressCss="true">

        <%
            UserAccountDataAccess uada = new UserAccountDataAccess();
            String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
            String companyId = uada.getUserCompanyForUserId(userId).getId();
            String id = request.getParameter("id");

            //market app access

            boolean itinerary = true;

            TUserRolesDataAccess urda = new TUserRolesDataAccess();
            UserPermissions up = urda.getPermissions(userId);
            ElementData sabreData = new SabreDataAccess().getSabreUserByUserId(id);
            String sabreConsultantId = sabreData == null ? "" : sabreData.getString(SabreDataAccess.E_CONSULTANT_ID);

            ElementData user = ModuleAccess.getInstance().getElementById(uada.USER_MODULE, id);
        %>
    <script>
		var userID = <%=id %>;

    </script>

        <form id="settings-sabre-users" method="POST">
            <div class="modal-header">
                <div class="titleBar">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">
                        <i class="fal fa-times-circle"></i>
                    </button>
                </div>
                <h2 class="modal-title">Sabre Itinerary Settings</h2>
            </div>
            <div class="modal-body smart-form">
                <div class="" action="" style="background: #fff; padding: 20px;">
                    <header>Sabre Consultant Information</header>
                    <div class="row">
                        <section class="col col-10">
                            <label for="sabreConsultantId">Consultant ID</label>
                            <input
                                    class="form-control" type="text" id="sabreConsultantId"
                                    name="sabreConsultantId"
                                    value="<%=(sabreConsultantId == null ? "" : sabreConsultantId)%>" />
                        </section>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
                    <i class="fa fa-times"></i> Close
                </button>

                <button id="saveUserbutton" type="submit" class="btn btn-success">
                    <i class="fa fa-save"></i> Save
                </button>

            </div>
        </form>

    <script>
    $(document).ready(function()
  		{

    		var $modal = $("#settings-sabre-users").closest(".modal");
    		
    		$('#settings-sabre-users').validate({
				errorElement : 'span',
				errorClass : 'help-block',
				errorPlacement : function(error, element)
				{
					error.insertAfter(element);
				},
				ignore : '',
				rules : {
					sabreConsultantId : {
						required: true,
						remote : '/api/sabreIdValid?id=' + userID
					}
				},
				messages : {
					sabreConsultantId : {
						remote : 'User with this consultant id already exists.',
						required: 'Consultant id required.'
					}
				},
				highlight : function(element)
				{
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
				},
				unhighlight : function(element)
				{
					$(element).closest('.form-group').removeClass('has-error');
				},
				success : function(label, element)
				{
					$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
				},
				submitHandler : function(form)
				{
					$modal.modal('hide');
					startLoadingForeground();
					var sabreConsultantId = $('#sabreConsultantId').val();
	  				var params = {"sabreConsultantId": sabreConsultantId, "id": userID};
	  				
					$.ajax({
  						url : '/api/market/sabre/updateUser',
  						data : params,
  						success : function(data)
  						{
  							stopLoadingForeground();
  							if (data.success === false)
  							{
  								swal({
  									title : 'Error',
  									type : 'error',
  									text : data.message
  								})
  							}

  						},
  						error : function()
  						{
  							stopLoadingForeground();

  							alert("Unable to update your Sabre user settings. Please try again later.");
  						}
  					});
				}
    		});

  		});
    </script>
</compress:html>