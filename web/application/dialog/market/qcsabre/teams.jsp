<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%@page import="au.corporateinteractive.qcloud.market.enums.Market"%>
<%@page
        import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager"%>
<%@page
        import="au.net.webdirector.common.datalayer.base.db.entity.MultiOptions"%>
<%@page import="java.util.List"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page
        import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess"%>
<%@page
        import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="java.util.Hashtable"%>
<%@page
        import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.*" %>
<%@ page import="java.util.Vector" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor"
           prefix="compress"%><compress:html enabled="true"
                                             compressJavaScript="true" compressCss="true">

    <%
        UserAccountDataAccess uada = new UserAccountDataAccess();
        TravelDocsDataAccess pda = new TravelDocsDataAccess();
        String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
        String companyId = uada.getUserCompanyForUserId(userId).getId();
        CategoryData company = uada.getUserCompanyForUserId(userId);
        String teamId = request.getParameter("id");

        CategoryData team = uada.getTeamForTeamId(teamId);

        CategoryData sabreTeamCategory = new SabreDataAccess().getCategoryByTeamId(teamId);
    %>
    <script>
		var teamId = <%=teamId %>;

    </script>

    <form id="settings-sabre-teams" method="POST">
        <div class="modal-header">
            <div class="titleBar">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <h2 class="modal-title">Sabre Itinerary Settings</h2>
        </div>
        <div class="modal-body smart-form">
            <div class="" action="" style="background: #fff; padding: 20px;">
                <header>Itinerary Proposal Styles</header>
                <div class="row" style="padding: 25px 25px">
                    <label>Proposal Template</label> <select
                        id="proposalTemplate" style="width: 100%;"
                        name="proposalTemplate">
                    <option value=""></option>
                    <%
                        Vector<Hashtable<String, String>> templates = pda.getTemplates(companyId, company.getName());

                        for (Hashtable<String, String> ht : templates)
                        {
                    %>
                    <option
                            <%=team == null ? "" : ht.get(TravelDocsDataAccess.E_ID).equals(team.getString(UserAccountDataAccess.C1_PROPOSAL_TEMPLATE)) ? "selected" : ""%>
                            value="<%=ht.get(TravelDocsDataAccess.E_ID)%>"><%=ht.get(TravelDocsDataAccess.E_TEMPLATE_TITLE)%></option>
                    <%
                        }
                    %>
                    </select>
                </div>
                <div class="row" style="padding: 5px 25px;">
                    <label for="itiSyles">Itinerary Style</label> <select
                        id="itiStyles" style="width: 100%;" name="itiStyle">
                    <option value=""></option>
                    <%
                    	CategoryData defaultStyleCategory = ModuleAccess.getInstance().getCategoryByName(ItineraryDataAccess.ITINERARY_MODULE, "Default");
                        List<ElementData> itineraryStyles = new ItineraryDataAccess().getItineraryStyles(companyId);
                        for (ElementData itineraryStyle : itineraryStyles)
                        {
                    %>
                    <option
                            <%=team == null ? "" : itineraryStyle.getId().equals(team.getString(UserAccountDataAccess.C1_ITINERARY_TEMPLATE)) ? "selected" : ""%>
                            default="<%=StringUtils.equalsIgnoreCase(defaultStyleCategory.getId(), itineraryStyle.getParentId())? true : false%>"
                            value="<%=itineraryStyle.getId()%>"><%=itineraryStyle.getName()%></option>
                    <%
                        }
                    %>
                    </select>
                </div>
                
                <div class="row" style="padding: 5px 25px;">
                    <input type="checkbox" class="js-switch" name="autoSendItinerary" id="autoSendItinerary"
                           value="switch"
                            <%=team == null ? "" : team.getBoolean(TUserAccountDataAccess.C1_AUTO_SEND_ITINERARY) ? "checked='checked'" : ""%> />
                    <span>Auto send itinerary proposal</span>
                </div>
                
                <div class="row" style="padding: 5px 25px;">
                    <input type="checkbox" class="js-switch" name="autoDeleteItinerary" id="autoDeleteItinerary"
                           value="switch"
                            <%=sabreTeamCategory != null && sabreTeamCategory.getBoolean(SabreDataAccess.C_AUTO_DELETE_ITINERARY) ? "checked='checked'" : ""%> />
                    <span>Auto delete itinerary proposal</span>
                    <div id="delMonths" style="display:inline; <%=sabreTeamCategory != null && sabreTeamCategory.getBoolean(SabreDataAccess.C_AUTO_DELETE_ITINERARY) ? "" : "visibility: hidden;" %>">
	                    <span> in </span>
	                    <input id="noOfMonths" class="ignore-validation" type="number" min="1" max="12" style="width: 35px" value="<%=sabreTeamCategory == null ? 1 : Math.max(sabreTeamCategory.getInt(SabreDataAccess.C_DELETE_IN_MONTHS), 1)%>"/>
	                    <span> month</span><span id="month-s" style="width: 35px; display:<%=sabreTeamCategory != null && Math.max(sabreTeamCategory.getInt(SabreDataAccess.C_DELETE_IN_MONTHS), 1)>1?"inline;":"none;"%>">s</span>
                   </div>
                </div>
                
                <header>Sabre Team Information</header>
                <div class="row">
                    <section class="col col-10">
                    	<div class="form-group">
                        <label>Agency Code</label> <input
                            class="form-control" type="text" id="sabreAgencyCode"
                            name="sabreAgencyCode"
                            value="<%=sabreTeamCategory != null && sabreTeamCategory.getString(SabreDataAccess.C_AGENCY_CODE) != null ? sabreTeamCategory.getString(SabreDataAccess.C_AGENCY_CODE) : ""%>" />
                    	</div>
                    </section>
                </div>
                <div class="row" style="padding: 5px 15px;">
                    <section class="col col-10">
                    	<div class="form-group">
                        <label>Password</label> 
                        <input class="form-control" type="password" id="sabrePassword" 
                        	name="sabrePassword" value="<%=sabreTeamCategory != null && sabreTeamCategory.getString(SabreDataAccess.C_PASSWORD) != null ? sabreTeamCategory.getString(SabreDataAccess.C_PASSWORD) : ""%>" />
                    	</div>
                    </section>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
                <i class="fa fa-times"></i> Close
            </button>

            <button id="saveTeambutton" type= "submit" class="btn btn-success">
                <i class="fa fa-save"></i> Save
            </button>

        </div>
    </form>
	<script>
		$(document).ready(function()
		{
			console.log('log');
			var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

			elems.forEach(function(html)
			{
				var switchery = new Switchery(html, {
					size : 'small'
				});
			});


			$('#proposalTemplate').select2();
			$('#itiStyles').select2();
			
			var autoDel = $('#autoDeleteItinerary');
			autoDel.change(function(){
				$('#delMonths').css('visibility',autoDel.is(':checked')?'visible' : 'hidden');
			});
			$('#noOfMonths').keypress(function (evt) {
			    evt.preventDefault();
			});
			
			$('#noOfMonths').change(function (evt) {
			    if($('#noOfMonths').val()>1){
			    	$('#month-s').show();
			    }
			    else {
			    	$('#month-s').hide();
			    }
			});
			
			var $modal = $("#settings-sabre-teams").closest(".modal");

			$('#settings-sabre-teams').validate({
				errorElement : 'span',
				errorClass : 'help-block',
				errorPlacement : function(error, element)
				{
					error.insertAfter(element);
				},
				ignore : '.ignore-validation',
				rules : {
					sabreAgencyCode : {
						required : true
					},
					sabrePassword : {
						required : true
					}
				},
				messages : {
					sabreAgencyCode : {
						required : 'Agency code required'
					},
					sabrePassword : {
						required : 'Password required'
					}
				},
				highlight : function(element)
				{
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
				},
				unhighlight : function(element)
				{
					$(element).closest('.form-group').removeClass('has-error');
				},
				success : function(label, element)
				{
					$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
				},
				submitHandler : function(form)
				{
					$modal.modal('hide');
					startLoadingForeground();
					var sabrePassword = $('#sabrePassword').val();
					var sabreAgencyCode = $('#sabreAgencyCode').val();
					var autoSendItinerary = $('#autoSendItinerary').is(":checked");
					var itiStyle = $('#itiStyles').val();
					var proposalTemplate = $('#proposalTemplate').val();
					var autoDeleteItinerary = $('#autoDeleteItinerary').is(":checked");
					var noOfMonths = $('#noOfMonths').val();
					var params = {
						"proposalTemplate" : proposalTemplate,
						"id" : teamId,
						"sabrePassword" : sabrePassword,
						"sabreAgencyCode" : sabreAgencyCode,
						"autoSendItinerary" : autoSendItinerary,
						"itiStyle" : itiStyle,
						"autoDeleteItinerary" : autoDeleteItinerary,
						"noOfMonths" : noOfMonths
					};
					
					$.ajax({
						url : '/api/market/sabre/updateTeam',
						data : params,
						success : function(data)
						{
							stopLoadingForeground();
							if (data.success === false)
							{
								swal({
									title : 'Error',
									type : 'error',
									text : data.message
								})
							}

						},
						error : function()
						{
							stopLoadingForeground();

							alert("Unable to update your Sabre team settings. Please try again later.");
						}
					});
				}
			});

		});

	</script>
</compress:html>