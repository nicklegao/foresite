<!DOCTYPE html>
<html>
<head> 
<%--
<link rel="stylesheet" type="text/css" media="screen" href="/application/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css">
 --%>
<link rel="stylesheet" type="text/css" href="/application/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
<style>
div.per-page.short-select-option{
  display: inline-block;
}
.pick-date input
{
  min-width: 190px;
  text-align: center;
}
</style>
</head>
<body class="iframe-body">
<!-- START CONTENT AREA -->   
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Invoice List</h4>
</div>
<div class="modal-body">
  <table id="invoiceTable" class="table" cellspacing="0" width="100%">
    <thead>
      <tr>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-danger" data-dismiss="modal">
    <i class="fa fa-times"></i> Close
  </button>
</div>
<!-- END CONTENT AREA -->

<script src="/application/assets/plugins/moment/moment.js"></script>
<script src="/application/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script src="/application/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="/application/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script> 
<script src="/application/dialog/showInvoices.js"></script>
</body>
</html>
