<%@page import="au.corporateinteractive.qcloud.proposalbuilder.moduleActions.StyleGenerationItem"%>
<%@page import="java.util.Date"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.DateFormatUtils"%>
<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%@page import="au.corporateinteractive.qcloud.subscription.SubscriptionPlan"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Comparator"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.Locale"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page	import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="java.util.List"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TDiscountsDataAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.StatusReason" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.*" %>

<%

UserAccountDataAccess uada = new UserAccountDataAccess();
String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
CategoryData company = uada.getUserCompanyForUserId(userId);
Locale[] locales = Locale.getAvailableLocales();
Comparator<Locale> localeComparator = new Comparator<Locale>() {
	public int compare(Locale locale1, Locale locale2) {
		return locale1.getDisplayCountry().compareTo(locale2.getDisplayCountry());
	}
};
Arrays.sort(locales, localeComparator);

String passwordExpiry = company.getString(uada.C1_PASSWORD_EXPIRY_DATE);

	String mnoGrpId = company.getString(MaestranoUserAccountDataAccess.C_MAESTRANO_GROUP_ID);
	String mnoTenantKey = company.getString(MaestranoUserAccountDataAccess.C_MAESTRANO_TENANT_KEY);
	
	String currencySymbol = StyleGenerationItem.currencySymbolWithFallback(company.getString(UserAccountDataAccess.C1_LOCALE), "en-US");
%>
<div id="settings-management">
	<form class="form-settings-management" method="POST">
	<div class="modal-header">
		<div class="titleBar">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">
				<i class="fal fa-times-circle"></i>
			</button>
		</div>
		<h2 class="modal-title">Settings</h2>
	</div>
	<div class="modal-body smart-form">
	<div class="form-horizontal">
		<!-- Nav tabs -->
	 <ul class="nav nav-tabs" role="tablist">
	    <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
	    <li role="presentation"><a href="#security" aria-controls="security" role="tab" data-toggle="tab">Integrations</a></li>
	    <li role="presentation"><a href="#order-processing" aria-controls="order-processing" role="tab" data-toggle="tab">Order Processing</a></li>
	    <li role="presentation"><a href="#email" aria-controls="email" role="tab" data-toggle="tab">E-mail</a></li>
		 <li role="presentation"><a href="#dashboard-settings" aria-controls="dashboard-settings" role="tab" data-toggle="tab">Dashboard</a></li>
		 <li role="presentation"><a href="#helper-content" aria-controls="helper-content" role="tab" data-toggle="tab">Pricing Table</a></li>
		 <li role="presentation"><a href="#proposal-status" aria-controls="proposal-status" role="tab" data-toggle="tab">Proposal Status</a></li>
						
		 <% if(StringUtils.isNotBlank(mnoGrpId) && StringUtils.isNotBlank(mnoTenantKey)){ %>
		 <li role="presentation"><a href="#maestrano-Sync" aria-controls="maestrano-Sync" role="tab" data-toggle="tab">Maestrano Sync</a></li>
		 <% } %>
	  </ul>
	 
	 
	 <!-- Tab panes -->
	  <div class="tab-content">
	    <div role="tabpanel" class="tab-pane active" id="general">
			<header>Proposal Expiry Date</header>
			<div class="row">
				<section class="col col-12">
					<label for="expiryDate" class="label"> Select when
						you will be notified of expiring proposals:
					</label>
					<select class="form-control" id="expiryDate" name="expiryDate">
							<%-- <option value="0" <%= "0".equals(companyData.getString(uada.C1_EXPIRY_DATE))?"selected":"" %>>Never</option> --%>
							<option value="30" <%= "30".equals(company.getString(uada.C1_EXPIRY_DATE))?"selected":"" %>>30 Days</option>
							<option value="60" <%= "60".equals(company.getString(uada.C1_EXPIRY_DATE))?"selected":"" %>>60 Days</option>
							<option value="90" <%= "90".equals(company.getString(uada.C1_EXPIRY_DATE))?"selected":"" %>>90 Days</option>
							<option value="120" <%= "120".equals(company.getString(uada.C1_EXPIRY_DATE))?"selected":"" %>>120 Days</option>
					</select>
				</section>
			</div>

			<header>Password Expiry Date</header>
			<div class="row">
				<section class="col col-12">
					<label for="expiryDate" class="label"> Select when
						the user password expires:
					</label>
					<select class="form-control" id="passwordExpiryDate" name="passwordExpiryDate">
							<option value="3" <%= "3".equals(passwordExpiry)?"selected":"" %>>3 Months</option>
							<option value="6" <%= "6".equals(passwordExpiry)||"0".equals(passwordExpiry)||"".equals(passwordExpiry)?"selected":"" %>>6 Months</option>
							<option value="9" <%= "9".equals(passwordExpiry)?"selected":"" %>>9 Months</option>
					</select>
				</section>
			</div>
			
			<%--<header>Encryption</header>--%>
			<%--<div class="row">--%>
				<%--<section class="col col-12">--%>
					<%--<label for="expiryDate" class="label"> Select if you want to encrypt your client's information--%>
					<%--</label>--%>
					<%--<input type="checkbox" id="encryptClientData" name="encryptClientData" <%=company.getBoolean(uada.C1_ENCRYPT_CLIENT_DATA)?"checked":"" %>> Enable encryption--%>
					<%--<i class="label" style="margin-top:15px;"> * The encryption feature only encrypts the client data saved while this setting is turned on. The encrypted data will get decrypted if edited while this setting is turned off.--%>
					  <%--</i>--%>
				<%--</section>--%>
			<%--</div>--%>

			<header>Locale</header>
			<div class="row">
				<section class="col col-12">
					<label for="locale" class="label"> Select what locale your organisation is based in. This will affect things such as currency and date displayed: </label>
					<select class="form-control" id="locale" name="locale">
                		<%
            				for (Locale locale : locales){
            					if(!StringUtils.isEmpty(locale.getDisplayCountry()) && !StringUtils.isEmpty(locale.getDisplayCountry(Locale.ENGLISH))) {
            					String selectedClause = locale.toLanguageTag().equals(company.getString(uada.C1_LOCALE))?"selected":"";
          				%>
            					<option value="<%=ESAPI.encoder().encodeForHTMLAttribute(locale.toLanguageTag()) %>" <%= selectedClause %>>
            						<%=ESAPI.encoder().encodeForHTML(locale.getDisplayCountry())%> (<%=ESAPI.encoder().encodeForHTML(locale.getDisplayLanguage())%>)
            					</option>
          				<%	}
						}	%>
            		</select>
				</section>
			</div>

			<header>Date Format</header>
			<div class="row">
				<section class="col col-12">
					<label for="dateformat" class="label"> Select your preferred date format. This will affect the date display in your proposal: </label>
					<select class="form-control" id="dateformat" name="dateformat">
              		<%
              		for (DateFormatUtils instance : DateFormatUtils.getAvailableInstances(Locale.forLanguageTag(company.getString(uada.C1_LOCALE)))){
        				%>
      					<option value="<%=ESAPI.encoder().encodeForHTMLAttribute(instance.getCode()) %>" <%= instance.getCode().equals(company.getString(uada.C1_DATE_FORMAT)) ? "selected" : "" %>>
      						<%=ESAPI.encoder().encodeForHTML(instance.getCode())%> (<%=ESAPI.encoder().encodeForHTML(instance.format(new Date()))%>)
      					</option>
        				<%
					}
                      %>
            		</select>
				</section>
			</div>

			<header>Time Zone</header>
			<div class="row">
				<section class="col col-12">
					<label for="timeZone" class="label"> Select the time zone for your company. This will affect the date display in your proposal: </label>
					<select class="form-control" id="timeZone" name="timeZone">
            		</select>
            		<input value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(UserAccountDataAccess.C1_TIME_ZONE))%>"
				      	type="hidden" id="timeZoneInput">
				</section>
			</div>
		</div>
		 <div role="tabpanel" class="tab-pane" id="security">
		<header>API Credentials</header>
			<div class="row">
				<section class="col col-6">
					<label for="apiIdField" class="label"> API ID</label>
				      <input value="<%=ESAPI.encoder().encodeForHTML(company.getString(UserAccountDataAccess.C1_API_ID))%>"
				      	type="text" class="form-control" readonly id="apiIdField">
				</section>

				<section class="col col-6">
					<label for="apiKeyField" class="label"> API Key</label>
				      <input value="<%=ESAPI.encoder().encodeForHTML(company.getString(UserAccountDataAccess.C1_API_KEY))%>"
				      	type="text" class="form-control" readonly id="apiKeyField">
				</section>

				<section class="col col-12">
				      <button type="button" class="btn btn-warning regenerateApiCredentialsAction" data-dismiss="modal" style="padding: 6px 12px;">
				      	<i class="fa fa-key" aria-hidden="true"></i> Regenerate API Credentials
				      </button>
				</section>
				<section class="col col-12">
					<i class="label" style="margin-top:15px;"> * The QuoteCloud API Credentials are used when another software application needs to send or receive data from TravelDocs. For example, Zapier's integration platform, many accounting platforms, and different CRM's.
					</i>
				</section>
			</div>
			 <header>Integrations</header>
			 <div class="row">
				 <section class="col col-3">
					 <button type="button" onclick="window.open('https://zapier.com/developer/invite/75448/0c794bf9d37d63407042e1d5dee3f21a/', '_blank')" class="btn btn-info" data-dismiss="modal" style="width: 140px;padding: 6px 12px;">
						 <i class="fa fa-link" aria-hidden="true"></i> Zapier
					 </button>
				 </section>
				 <section class="col col-3">
					 <button type="button" onclick="window.open('https://smb.maestrano.com/', '_blank')" class="btn btn-info" data-dismiss="modal" style="width: 140px;padding: 6px 12px;">
						 <i class="fa fa-link" aria-hidden="true"></i> Maestrano
					 </button>
				 </section>
				 <section class="col col-3">
					 <a href="#email-support" class="btn btn-warning" style="width: 140px;padding: 6px 12px;" data-toggle="modal">
						 <i class="fa fa-question" aria-hidden="true"></i> I Need Help
					 </a>
				 </section>
				 <section class="col col-12">
					 <i class="label" style="margin-top:15px;"> * QuoteCloud integrations are handled by our integration partners for information on there platforms please click the links above.
					 </i>
				 </section>
			 </div>
			 <header>Personal Data Security</header>
			 <div class="row">
				 <section class="col col-12">
					 <input type="checkbox" id="enableEncryptedDataSync" name="enableEncryptedDataSync" <%=company.getBoolean(uada.ALLOW_ENCRYPTED_DATA_SYNC)?"checked":"" %>> Send personal data decrypted
					 <i class="label" style="margin-top:15px;"> * INFO: By switching on Send Data Decrypted you are giving your consent to allowing personal data encrypted within your proposals to be transferred to third party services such as Zapier as decrypted data.
					 </i>
				 </section>

			 </div>
		</div>
		<div role="tabpanel" class="tab-pane" id="order-processing">
			<%--<header>Order Now Label</header>--%>
			<%--<div class="row">--%>
				<%--<section class="col col-12">--%>
					<%--<label for="orderNowLabel" class="label"> Define the label for the "Order Now" button.--%>
					<%--("Order Now" will be used as the default value in case nothing is defined)</label>--%>
				      <%--<input value="<%=ESAPI.encoder().encodeForHTML(company.getString(UserAccountDataAccess.C1_ORDER_NOW_LABEL))%>"--%>
				      	<%--type="text" class="form-control" id="orderNowLabel" name="orderNowLabel" maxlength="20">--%>
				<%--</section>--%>
			<%--</div>--%>

			<header>Order Now Redirect</header>
			<div class="row">
				<section class="col col-12">
					<label for="orderNowRedirectLink" class="label"> Define where the customer is redirected to after they Accept a Proposal.
						(You must enter the full link including http:// or https://)</label>
				      <input value="<%=ESAPI.encoder().encodeForHTML(company.getString(UserAccountDataAccess.C1_ORDER_NOW_REDIRECT_LINK))%>"
				      	type="text" class="form-control" id="orderNowRedirectLink" name="orderNowRedirectLink" placeholder="http://www.example.com">
				</section>
			</div>
		</div>

		<div role="tabpanel" class="tab-pane" id="email">
			<header>Email Subject Line</header>
			<div class="row">
				<section class="col col-12">
					<label for="subjectLine" class="label">
						Choose a default subject line to always be used when emailing proposals:
					</label>
					<select class="form-control" id="subjectLine" name="subjectLine">
						<% if (company.getString(uada.EMAIL_STATUS_DEFAULT) == null) { %>
						<option value="1" selected>Use System Default</option>
						<% } else { %>
						<option value="1" <%= "1".equals(company.getString(uada.EMAIL_STATUS_DEFAULT))?"selected":"" %>>Use System Default</option>
						<% } %>
						<option value="2" <%= "2".equals(company.getString(uada.EMAIL_STATUS_DEFAULT))?"selected":"" %>>Your TravelDoc from <%=uada.getUserCompanyNameForUserId((String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR))%> [PROPOSALID] </option>
						<option value="other" <%= "other".equals(company.getString(uada.EMAIL_STATUS_DEFAULT))?"selected":"" %>>Custom Email Subject</option>
					</select>
				</section>
				<section id="otherSubjectText" class="col col-9" <%= "other".equals(company.getString(uada.EMAIL_STATUS_DEFAULT))?"":"hidden" %>>
					<label for="statusSubjectString" class="label">Enter custom subject</label>
					<input value="<%=company.getString(uada.EMAIL_STATUS_STRING)%>" name="statusSubjectString" type="text" class="form-control" id="statusSubjectString" placeholder="Write subject here..." aria-required="true" aria-invalid="false">
				</section>
			</div>
		<header>Email Templates</header>
				<div class="row">
					<%
					EmailTemplateDataAccess etda = new EmailTemplateDataAccess();
					String content = etda.readContentWithUserId(company.getId(), "Proposal Ready");
					%>
					<section class="col col-12">
						<label for="email-template-ready">Proposal Ready Template</label>
						<%-- <div class="email-template-content editable-content" id="email-template-ready">
							<%= content %>
						</div> --%>
						<div class="panel-froala" id="email-template-ready"><%= content %></div>
					</section>
					<%
					String content2 = etda.readContentWithUserId(company.getId(), "Proposal New Revision Ready");
					%>
					<section class="col col-12">
						<label for="email-template-revision">Proposal New Revision Ready Template</label>
						<%-- <div class="email-template-content editable-content" id="email-template-revision">
							<%= content2 %>
						</div> --%>
						<div class="panel-froala" id="email-template-revision"><%= content2 %></div>
					</section>
				</div>
		</div>

		  <div role="tabpanel" class="tab-pane" id="dashboard-settings">
			  <header>Edit Restrictions</header>
			  <div class="row">
				  <section class="col col-12">
					  <label for="enableTeamEditing" class="label"> Allow teams to edit all proposals:
					  </label>
					  <input type="checkbox" id="enableTeamEditing" name="enableTeamEditing" <%=company.getBoolean(uada.PATCH_ALLOW_TEAM_EDITING)?"checked":"" %>> Enable Team Editing
					  <i class="label" style="margin-top:15px;"> * WARNING: If you activate team editing, all team members will be able to edit each others proposals however if multiple people save the same proposal data maybe lost.
					  </i>
				  </section>

			  </div>
		  </div>

		  <div role="tabpanel" class="tab-pane" id="helper-content">
		      

			<header>Tax</header>
			<div class="row">
				<section class="col col-8">
					<label for="enableTax" class="label"> Tax Information:
					</label>
					<input type="checkbox" id="enableTax" name="enableTax" <%=company.getBoolean(uada.C1_ENABLE_TAX)?"checked":"" %>> Enable Tax
				</section>
				<section class="col col-6" id="taxLabelSection">
					<label for="taxLabel" class="label"> Label</label>
				      <input value="<%=company.getString(uada.C1_TAX_LABEL) %>" name="taxLabel" type="text" class="form-control" id="taxLabel" placeholder="Tax name eg:(GST, VAT...)" style="box-shadow: 0 0 0 0 rgba(255,0,0,0.4) !important;">
				</section>

				<section class="col col-6" id="taxPercentageSection">
					<label for="taxPercentage" class="label"> Percentage</label>
				      <input value="<%=company.getString(uada.C1_TAX_PERCENTAGE) %>" name="taxPercentage" type="number" class="form-control" id="taxPercentage" style="box-shadow: 0 0 0 0 rgba(255,0,0,0.4) !important;">
				</section>
			</div>

			<header>Additions</header>
			<div class="row">
				<section class="col col-12">
					<label for="enableAddition" class="label">  Select this option if you wish to show the <strong>Additions</strong> column in the pricing table. This column can be used to show special additional costs for a line item, for example if you have an airfare booking as a line item, you may use this field to show Airport Tax.
					</label>
					<input type="checkbox" id="enableAddition" name="enableAddition" <%=company.getBoolean(uada.C1_ENABLE_ADDITION)?"checked":"" %>> Enable Additions
				</section>
			</div>
		  
			  <header>Terms</header>
			  <div class="row">
				  <section class="col col-12">
					  <label for="estimatedValue" class="label"> Select this option if you wish to switch off the ability to have recurring costs in proposals, ie if you never sell any product or service on a recurring fee basis, eg monthly, quarterly, etc.  Please note, if you have created proposals that use a recurring cost term this option will not affect existing proposals.
					  </label>
					  <input type="checkbox" id="hideTermsAlways" name="hideTermsAlways" <%=company.getBoolean(uada.C1_HIDE_TERMS_ALWAYS)?"checked":"" %>> Hide terms
				  </section>
			  </div>
			  <header>Estimated pricing value</header>
			  <div class="row">
				  <section class="col col-12">
					  <label for="estimatedValue" class="label"> Select if the estimated pricing value will be available for the consultants <a href="javascript:void(0);" data-content="settingsEstimatedValue" class="helpAction">(?)</a>:
					  </label>
					  <input type="checkbox" id="estimatedValue" name="estimatedValue" <%=company.getBoolean(uada.C1_ESTIMATED_VALUE)?"checked":"" %>> Estimated Value
				  </section>
			  </div>

			  <header>Rounding</header>
			  <div class="row">
				  <section class="col col-12">
					  <label for="roundingValue" class="label"> Round prices to the nearest whole amount. (e.g. to the nearest <%=currencySymbol%>1.51 will become <%=currencySymbol%>2):
					  </label>
					  <input type="checkbox" id="roundingValue" name="roundingValue" <%=company.getBoolean(uada.C1_ROUNDING)?"checked":"" %>> Rounding
					  <i class="label" style="margin-top:15px;"> * If you change this configuration, any existing proposals will still have the previous setting, Sales Consultants will need to reopen and save old proposals to invoke a change to the price rounding setting.
					  </i>
				  </section>
			  </div>
			  <%--<header>Editable Content</header>--%>
			  <%--<div class="row">--%>
				  <%--&lt;%&ndash;--%>
                            <%--<section class="col col-12">--%>
                                <%--<strong>Pricing Help Text</strong>--%>
                                <%--<p>Displayed in the editor in the pricing section when creating proposals. This is not displayed on proposals.</p>--%>
                                <%--<div class="editable-content" id="static-pricing-info-text">--%>
                                    <%--<%= new StaticTextDataAccess().readContentWithUserId(userId, StaticTextDataAccess.EHEADLINE_PRICING) %>--%>
                                <%--</div>--%>
                            <%--</section>--%>
                   <%--&ndash;%&gt;--%>

				  <%--<section class="col col-12">--%>
					  <%--<strong>Pricing: Want more detail?</strong>--%>
					  <%--<p>Displayed underneath the pricing table when displayed to customers.</p>--%>
					  <%--<div class="editable-content" id="static-want-more-detail">--%>
						  <%--<%= new StaticTextDataAccess().readContentWithUserId(userId, StaticTextDataAccess.EHEADLINE_PRICING_WANT_MORE_DETAIL) %>--%>
					  <%--</div>--%>
				  <%--</section>--%>
			  <%--</div>--%>
		  </div>

		  <div role="tabpanel" class="tab-pane" id="proposal-status">
			  <header>Status Change Reason</header>
			  <div id="fields">
				  <div class="row" style="margin: 0;padding: 10px 15px;">
					  <div class="col-sm-11">
						  <div class="form-group" style="margin: 0;">
							  <input type="text" class="form-control reason-label" name=reason-label- placeholder="Too expensive" disabled>
						  </div>
					  </div>
					  <div class="col-sm-1">
						  <a class="btn btn-bricky btn-xs delete tooltips" data-placement="top" data-original-title="Delete" style="float: right;height: 22px;width: 22px;color: #fff;background-color: #ba131a;margin-top: 5px;" disabled>
							  <i class="fa fa-times"> </i>
						  </a>
					  </div>
				  </div>
				  <div class="row" style="margin: 0;padding: 10px 15px;">
					  <div class="col-sm-11">
						  <div class="form-group" style="margin: 0;">
							  <input type="text" class="form-control reason-label" name=reason-label- placeholder="No longer interested" disabled>
						  </div>
					  </div>
					  <div class="col-sm-1">
						  <a class="btn btn-bricky btn-xs delete tooltips" data-placement="top" data-original-title="Delete" style="float: right;height: 22px;width: 22px;color: #fff;background-color: #ba131a;margin-top: 5px;" disabled>
							  <i class="fa fa-times"> </i>
						  </a>
					  </div>
				  </div>
				  <div class="row" style="margin: 0;padding: 10px 15px;">
					  <div class="col-sm-11">
						  <div class="form-group" style="margin: 0;">
							  <input type="text" class="form-control reason-label" name=reason-label- placeholder="Lost to competitor" disabled>
						  </div>
					  </div>
					  <div class="col-sm-1">
						  <a class="btn btn-bricky btn-xs delete tooltips" data-placement="top" data-original-title="Delete" style="float: right;height: 22px;width: 22px;color: #fff;background-color: #ba131a;margin-top: 5px;" disabled>
							  <i class="fa fa-times"> </i>
						  </a>
					  </div>
				  </div>
				  <div class="row" style="margin: 0;padding: 10px 15px;">
					  <div class="col-sm-11">
						  <div class="form-group" style="margin: 0;">
							  <input type="text" class="form-control reason-label" name=reason-label- placeholder="No longer required" disabled>
						  </div>
					  </div>
					  <div class="col-sm-1">
						  <a class="btn btn-bricky btn-xs delete tooltips" data-placement="top" data-original-title="Delete" style="float: right;height: 22px;width: 22px;color: #fff;background-color: #ba131a;margin-top: 5px;" disabled>
							  <i class="fa fa-times"> </i>
						  </a>
					  </div>
				  </div>
			  </div>
			  <a class="btn btn-primary addStatusReason" style="padding: 8px 20px;">
				  <i class="fa fa-plus"></i> Add
			  </a>
		  </div>

		  <% if(StringUtils.isNotBlank(mnoGrpId) && StringUtils.isNotBlank(mnoTenantKey)){ %>
		  <div role="tabpanel" class="tab-pane" id="maestrano-Sync">
			  <header>Select what data to share with Maestrano</header>
			  <div class="row">
				  <section class="col col-12">
					  <input type="checkbox" id="maestranoProductSync" name="maestranoProductSync" <%=company.getBoolean(TUserAccountDataAccess.C1_MAESTRANO_SYNC)?"checked":"" %>> Product Sync
					  <i class="label" style="margin-top:15px;"> * This will allow QuoteCloud to share product data between all other applications in your Maestrano dashboard.
					  </i>
				  </section>
				  <section class="col col-12">
					  <input type="checkbox" id="maestranoQuoteSync" name="maestranoQuoteSync" <%=company.getBoolean(TUserAccountDataAccess.C1_MAESTRANO_QUOTE_SYNC)?"checked":"" %>> Quote Sync
					  <i class="label" style="margin-top:15px;"> * This will sync quote data from your sent proposals to other applications in your Maestrano dashboard.
					  </i>
				  </section>
				  <%--<section class="col col-12">--%>
					  <%--<input type="checkbox" id="maestranoPDFSync" name="maestranoPDFSync" <%=company.getBoolean(TUserAccountDataAccess.C1_MAESTRANO_PDF_SYNC)?"checked":"" %>> PDF Sync--%>
					  <%--<i class="label" style="margin-top:15px;"> * This will sync sent proposal PDF's to other applications in your Maestrano dashboard.--%>
					  <%--</i>--%>
				  <%--</section>--%>
			  </div>
		  </div>
		  <% } %>
	  
	  </div>
	  </div>
	</div>
	<div class="modal-footer">
		<button type="button" id="closeForm" class="btn btn-danger"
			data-dismiss="modal">
			<i class="fa fa-times"></i> Close
		</button>
		<button type="button" id="submitButton" class="btn btn-success">
        	<i class="fa fa-save"></i> Save
      	</button>
	</div>
	</form>

	<div id="fragments" style="display: none;">
		<div class="row customStatus" style="margin: 0;padding: 10px 15px;">
			<div class="col-sm-11">
				<div class="form-group" style="margin: 0;">
					<input type="text" class="form-control reason-label" name=reason-label- placeholder="label" required>
				</div>
			</div>
			<div class="col-sm-1">
				<a class="btn btn-bricky btn-xs delete tooltips" data-placement="top" data-original-title="Delete" style="float: right;height: 22px;width: 22px;color: #fff;background-color: #ba131a;margin-top: 5px;">
					<i class="fa fa-times"> </i>
				</a>
			</div>
		</div>
	</div>
</div>

<script src="/application/js/form-validation/abn.js"></script>
<script src="/application/js/form-validation/acn.js"></script>
<script src="/application/assets/plugins/ckeditor/ckeditor.js"></script>
<script src="/application/assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="/application/assets/plugins/switchery/switchery.js"></script>
<script src="/application/js/form-validation/modal-settings-management.js"></script>

<!-- Froala Editor -->
<script src="/application/assets/plugins/froala-editor/custom/config-proposalEmailText.js"></script>


