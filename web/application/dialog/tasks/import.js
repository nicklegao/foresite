$(function()
{
	console.log('tasks/import')
	$('#import-tasks-form').validate({
		ignore : '',
		rules : {
			csvUpload : "required",
			projectId : "required",
			projectName : {
				required : function()
				{
					return $('#import-tasks-form [name="projectName"]').is(":visible");
				}
			},
		},
		messages : {
			csvUpload : "Please choose a CSV file to import",
		},
		submitHandler : function(form)
		{
			startLoadingForeground();
			$(form).ajaxSubmit({
				success : function(response)
				{
					stopLoadingForeground();
					if (response.success)
					{
						startTasksImportStatusCheck();
					}
					else
					{
						Swal.fire({
							title : "Error",
							type : "warning",
							html : response.message,
						});
					}
				}
			});
		}
	});

	$('#import-tasks-form [name="projectId"]').change(function()
	{
		if ($(this).val() == '0')
		{
			$('#import-tasks-form [name="projectName"]').val('').show();
		}
		else
		{
			$('#import-tasks-form [name="projectName"]').val('').hide();
		}
	}).change();

	importTasksAction();
})

function importTasksAction()
{
	var $modal = $('#import-tasks-modal');
	$('#csvUpload-btn', $modal).off('click').on("click", function()
	{
		$('#csvUpload', $modal).val("");
		$('#csvUpload', $modal).click();
	});
	$('#csvUpload', $modal).off('change').on('change', function()
	{
		var name = $(this).val();
		if (name == '')
		{
			$('#csvUpload-filename', $modal).text('');
			return;
		}
		var idx = name.lastIndexOf('\\');
		if (idx == -1)
		{
			idx = name.lastIndexOf('/');
		}
		$('#csvUpload-filename', $modal).text(idx == -1 ? '' : name.substring(idx + 1));
	})
	$('form', $modal).clearForm().find('input').change();
	// $('[name="categoryId"]', $modal).val(folder ?
	// $(folder).closest('tr').attr('data-element-id') : '');
	new Switchery($('[name=deleteAll]', $modal)[0]);
	$.ajax({
		url : '/api/process/status/get',
		type : 'post',
		data : {
			processor : "au.corporateinteractive.qcloud.proposalbuilder.service.TasksImporter"
		}
	}).done(function(data)
	{
		if (data == '')
		{
			$modal.removeClass("importing")
		}
		else
		{
			$modal.addClass("importing");
			startTasksImportStatusCheck();
		}
	})
}

function startTasksImportStatusCheck()
{
	var $modal = $('#import-tasks-modal');
	var check = function()
	{
		if (!$modal.is(':visible'))
		{
			return;
		}
		$.ajax({
			url : '/api/process/status/get',
			type : 'post',
			data : {
				processor : "au.corporateinteractive.qcloud.proposalbuilder.service.TasksImporter"
			}
		}).done(function(data)
		{
			if (data == '')
			{
				$('#import-tasks-modal').modal('hide');
				redrawTable(true);
			}
			else if (!data.ended)
			{
				$('.progress-bar', $modal).css('width', (100.0 * data.processed / data.total) + '%');
				$('.progress-bar-text', $modal).text(data.processed + " / " + data.total);
				$('.stage-text', $modal).text(data.stage);
				$modal.addClass("importing");
				setTimeout(check, 1000);
			}
			else
			{
				$('.progress-bar', $modal).css('width', (100.0 * data.processed / data.total) + '%');
				$('.progress-bar-text', $modal).text(data.processed + " / " + data.total);
				Swal.fire({
					title : data.success ? "Success" : "Error",
					type : data.success ? "success" : "warning",
					html : data.message,
				}).then(function(result)
				{
					$.ajax({
						url : '/api/process/status/remove',
						type : 'post',
						data : {
							processor : "au.corporateinteractive.qcloud.proposalbuilder.service.TasksImporter"
						}
					});
					if (data.success)
					{
						$modal.modal('hide');
						redrawTable(true);
					}
					else
					{
						$modal.removeClass("importing");
					}
				});
			}
		})
	};
	setTimeout(check, 1000);
}
