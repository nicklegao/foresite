<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.ProjectsDataAccess"%>
<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%

CategoryData company = new UserAccountDataAccess().getUserCompanyForUserId((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR));
%>
<style>
.import-modal .hidden-on-importing{
  display: initial;
}
.import-modal .show-on-importing{
  display: none;
}

.import-modal.importing .hidden-on-importing{
  display: none;
}
.import-modal.importing .show-on-importing{
  display: initial;
}
.qcloud-progress-bar.progress .progress-bar-text { 
    position: absolute;
    width: 100%;
    color: white;
    text-shadow: 0px 0px 1px black;
    font-weight: bold;
    text-align:center;
    color:white;
}
</style>
<div class="modal-content">
    <div class="modal-header">
      <div class="titleBar">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="fal fa-times-circle"></i>
        </button>
      </div>
      <h2 class="modal-title">Import Tasks</h2>
    </div>
    <form id="import-tasks-form" method="post" enctype="multipart/form-data" action="/api/tasks/import">
      <div class="modal-body">
        <div class="hidden-on-importing">
          <div class="form-group">
            <label for="csvUpload">Project</label>
            <div>
              <select name="projectId" class="form-control">
              	<option value="" selected> Please choose a project </option>
              	<%
              	for(ElementData project : new ProjectsDataAccess().list(company)){
              	%>
              	<option value="<%= project.getId() %>"><%= ESAPI.encoder().encodeForHTML(project.getName()) %></option>
              	<%	
              	}
              	%>
              	<option value="0"> Create a new project </option>
              </select>
              <input class="form-control" name="projectName" value="" placeholder="Enter the name of the new project" />
            </div>
          </div>
          <div class="form-group">
            <label for="csvUpload">Import File</label>
            <div>
              <button type="button" class="btn btn-success" id="csvUpload-btn"><i class="fas fa-cloud-upload-alt"></i> Choose a CSV File</button>
              <span id="csvUpload-filename"></span>
            </div>
            <input type="file" id="csvUpload" name="csvUpload" accept=".csv" style="display:none;">
          </div>
          <hr/>
          <div class="form-group">
            <label>
              <input type="checkbox" class="form-control" name="deleteAll"> Delete all data before import
            </label>
          </div>
          <div class="form-group alert alert-warning">
            <label>
              <input type="checkbox" class="form-control hide" name="overWrite" checked> Warning! Existing tasks with matching ID will be overwritten.
            </label>
          </div>
          <hr/>        
        </div>
        <div class="show-on-importing">
          <h4 class="alert-heading"><i class="fa fa-cog fa-spin"></i> <span class="stage-text">Importing</span>...</h4> 
          <p> Please wait while we are importing your data. </p> 
          <div class="progress qcloud-progress-bar"> 
            <div class="progress-bar"
                  role="progressbar" 
                  aria-valuenow="0" 
                  aria-valuemin="0" 
                  aria-valuemax="100"
                  style="width: 0%; background-color:#13b5ea;">
            </div>
            <div class="progress-bar-text"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary hidden-on-importing">Import</button>
      </div>
    </form>
  </div><!-- /.modal-content -->
  <script src="<%=UrlUtils.autocachedUrl("/application/dialog/tasks/import.js", request) %>"></script>
