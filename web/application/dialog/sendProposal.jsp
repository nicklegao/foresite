<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%@page import="java.io.File"%>
<%@page import="java.io.StringReader"%>
<%@page import="java.text.DecimalFormat" %>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Hashtable"%>
<%@page import="org.apache.commons.lang.StringUtils" %>
<%@page import="org.owasp.esapi.ESAPI" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.EmailTemplateDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder" %>
<%@page import="au.corporateinteractive.utils.FreeMarkerUtils" %>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleHelper" %>
<%@page import="freemarker.core.ParseException" %>
<%@page import="freemarker.template.Configuration" %>
<%@page import="freemarker.template.Template" %>
<%@page import="freemarker.template.TemplateException" %>
<%@page import="freemarker.template.TemplateExceptionHandler" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.util.TravelDocsStatus" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.service.SecurityKeyService" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor"
           prefix="compress" %>
<%
String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
UserAccountDataAccess uada = new UserAccountDataAccess();
CategoryData company = uada.getUserCompanyForUserId(userId);
String userCompanyName = uada.getUserCompanyNameForUserId(userId);
String proposalId = request.getParameter("proposal");
if (proposalId == null || proposalId.length() == 0)
{
  response.setStatus(400);
  return;
}
TravelDocsDataAccess pda = new TravelDocsDataAccess();
	Hashtable<String, String> travelDocHT = pda.getTravelDocById(proposalId, true);

Hashtable<String, String> newestProposalRevision = pda.getNewestTravelDocRevisionById(proposalId);

Proposal proposal = pda.loadTravelDoc(proposalId);
Content conInfo = proposal.findGeneratedContentOfType("con-info");

String proposalTitle = conInfo.getMetadata().get("proposalTitle");
String companyName = conInfo.getMetadata().get("companyName");
String contactName = conInfo.getMetadata().get("contactName") + (StringUtils.isBlank(conInfo.getMetadata().get("contactLastName")) ? "" : (" " + conInfo.getMetadata().get("contactLastName")));
String contactAddress = conInfo.getMetadata().get("contactAddress");
String contactEmail = conInfo.getMetadata().get("contactEmail");
String ccEmails = conInfo.getMetadata().get("cc-emails");

boolean readyToSend = (contactName!=null && contactName.length() > 0)
                    && (contactEmail!=null && contactEmail.length() > 0);

EmailTemplateDataAccess etda = new EmailTemplateDataAccess();
String subject;
String element;
String status = company.getString(uada.EMAIL_STATUS_DEFAULT);
boolean did = Integer.parseInt(newestProposalRevision.get(TravelDocsDataAccess.E_REVISION)) <= 1;
	if ((status != null && "2".equals(status)) || (status != null && status.equalsIgnoreCase("")) || ((status != null && "1".equals(status) && ((null != newestProposalRevision && Integer.parseInt(newestProposalRevision.get(TravelDocsDataAccess.E_REVISION)) <= 1) && !"other".equals(status)))))
{
	subject = "Your TravelDoc Itinerary from " + userCompanyName + " [" + conInfo.getMetadata().get("bookingId") + "]";
	element = EmailBuilder.PROPOSAL_READY_EMAIL_TEMPLATE_ELEMENT;
} else if ((company.getString(uada.EMAIL_STATUS_DEFAULT) != null && "other".equals(company.getString(uada.EMAIL_STATUS_DEFAULT)))) {
	subject = company.getString(uada.EMAIL_STATUS_STRING) + " -- Proposal ID: [" + new DecimalFormat("#0000000").format(Integer.parseInt(proposalId)) + "]";
	element = EmailBuilder.PROPOSAL_READY_EMAIL_TEMPLATE_ELEMENT;
}
else
{
	subject = "Your Revised TravelDoc from " + userCompanyName + " [" + new DecimalFormat("#0000000").format(Integer.parseInt(proposalId)) + "]";
	element = EmailBuilder.PROPOSAL_REVISION_READY_EMAIL_TEMPLATE_ELEMENT;
}

	String emailTemplate = etda.readContentWithUserId(company.getId(), "Itinerary Ready");
	if(StringUtils.isBlank(emailTemplate)) {
		emailTemplate = etda.readDefaultTemplate("Itinerary Ready").get("content");
	}

	String key = "";
	key = travelDocHT.get(TravelDocsDataAccess.E_KEY);

	SecurityKeyService sks = new SecurityKeyService();
	String eKey = sks.getEditKey(Integer.parseInt(userId), Integer.parseInt(proposalId), key);

// Parse email template //
Hashtable<String, String> proposalHT = pda.getTravelDocById(proposalId);

//process json style template interpolations with the actual template interpolations based off of database column namess
HashMap<String, String> jsonToDatabaseMap = EmailBuilder.preprocessProposalSendEmailTemplate(request, conInfo, proposalHT);
	TravelDocsStatus travelDocsStatus = TravelDocsStatus.withValue(proposalHT.get(TravelDocsDataAccess.E_PROPOSAL_STATUS));

try
{
	Configuration cfg = new Configuration();
	cfg.setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);
	Template eTemplate = new Template("email template",
			new StringReader(emailTemplate),
			cfg);

	emailTemplate = FreeMarkerUtils.processTemplateIntoString(eTemplate, jsonToDatabaseMap);
}
catch (ParseException e)
{
	throw new Exception("Error parsing the template");
}
catch (TemplateException e)
{
	throw new Exception("Error parsing the template: " + e.getMessage());
}
%>

<link rel="stylesheet" href="/application/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css">

<div class="modal-header line bottom">
	<div class="titleBar">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="fal fa-times-circle"></i>
        </button>
      </div>
	<h4 class="modal-title">Send Proposal</h4>
</div>

<div id="sendProposalDialog" class="modal-body">
	<p>Please confirm the following customer details:</p>

	<dl class="dl-horizontal">
		<dt>Booking ID:</dt>
		<dd><%= conInfo.getMetadata().get("bookingId") %></dd>

		<dt>Contact Name:</dt>
		<dd><%=ESAPI.encoder().encodeForHTML(contactName) %></dd>

		<dt>Contact Account:</dt>
		<dd><%=ESAPI.encoder().encodeForHTML(contactAddress) %></dd>

<%--		<dt>Company Name:</dt>--%>
<%--		<dd><%=ESAPI.encoder().encodeForHTML(companyName) %></dd>--%>

		<% if (!readyToSend) { %>
		<dt>Contact Email:</dt>
    	<dd><%=ESAPI.encoder().encodeForHTML(contactEmail) %></dd>
    	<% } %>

		<% if (!readyToSend && ccEmails != null && ccEmails.length() > 0) { %>
		<dt>Additional Email(s):</dt>
		<dd><%=ESAPI.encoder().encodeForHTML(ccEmails) %></dd>
		<% } %>
	</dl>

	<p>Please fill in the email which will be sent to the traveller:</p>
	<input type="hidden" id="travelDoc-user-e-key" value="<%=ESAPI.encoder().encodeForHTMLAttribute(eKey) %>"/>
	
	<form id="proposalSendEmail">
		<div class="form-group">
			<label for="emailTo">To</label>
				<input type="text" class="form-control" id="emailTo"
					   value="<%=ESAPI.encoder().encodeForHTMLAttribute(contactEmail) %>" readonly>
		</div>
		<div class="form-group">
			<label for="emailCc">Cc (Type email address and press enter)</label>
				<input type="text" class="form-control" id="emailCc"
					value="<%=ESAPI.encoder().encodeForHTMLAttribute(ccEmails) %>">
		</div>
		<div class="form-group">
			<label for="emailSubject">Subject</label>
				<input type="text" class="form-control" id="emailSubject"
					value="<%=ESAPI.encoder().encodeForHTMLAttribute(subject) %>">
		</div>
		<div class="form-group">
			<label for="emailBody">Body</label>
			<% System.out.println(emailTemplate); %>
				<div id="emailBody"><%=emailTemplate %></div>
		</div>
<%--		<% if(StringUtils.isNotBlank(proposalHT.get(TravelDocsDataAccess.E_FILE_CACHE))){--%>
<%--			File pdf = ModuleHelper.getStoredFile(proposalHT.get(TravelDocsDataAccess.E_FILE_CACHE));--%>
<%--            double fileSizeMb = pdf.length()/1024.0/1024.0 * 100;--%>
<%--            boolean overSize = fileSizeMb > 10 * 100;--%>
<%--        %>--%>
<%--&lt;%&ndash;		<div class="checkbox">&ndash;%&gt;--%>
<%--&lt;%&ndash;		  <label style="margin: 3px 0px;" for="attachPdf">&ndash;%&gt;--%>
<%--&lt;%&ndash;	        <input type="checkbox"&ndash;%&gt;--%>
<%--&lt;%&ndash;	              name="attachPdf"&ndash;%&gt;--%>
<%--&lt;%&ndash;	              id="attachPdf" <%= overSize ? "disabled" : "" %>>&ndash;%&gt;--%>
<%--&lt;%&ndash;	              <span style="<%= overSize ? "color:grey" : "" %>">Attach the proposal PDF file to the email</span>&ndash;%&gt;--%>
<%--&lt;%&ndash;                <% if(overSize){ %>&ndash;%&gt;--%>
<%--&lt;%&ndash;                <div style="color: darkred;padding-left:7px;">&ndash;%&gt;--%>
<%--&lt;%&ndash;                <small>&ndash;%&gt;--%>
<%--&lt;%&ndash;                  Oops, it appears on this occasion your proposal PDF file is too big to attach to your email (max file size is 10mb and yours is <%= new DecimalFormat("0.00").format(fileSizeMb/100) %>mb).  &ndash;%&gt;--%>
<%--&lt;%&ndash;                  But no worries, they can still click to download the PDF version when they receive your email.&ndash;%&gt;--%>
<%--&lt;%&ndash;                </small>&ndash;%&gt;--%>
<%--&lt;%&ndash;                </div>&ndash;%&gt;--%>
<%--&lt;%&ndash;                <% } %>&ndash;%&gt;--%>
<%--&lt;%&ndash;	      </label>&ndash;%&gt;--%>
<%--&lt;%&ndash;	    </div>&ndash;%&gt;--%>
<%--        <% } %>--%>
	</form>

</div>

<div class="modal-footer line top mg-envelope">
	<button type="button" data-dismiss="modal" class="btn btn-danger">
		Cancel</button>
	<a href="#" traveldocID="<%=proposalId %>"
	   class="btn btn-success doSendTraveldocsConfirmedAction"> Send </a>
<%--	<% if (readyToSend) { %>--%>
<%--	<% } %>--%>
</div>

<script src="/application/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
<script>
$(document).ready(function() {

	setTimeout(function()
			{
				new FroalaEditor('#emailBody', {
					toolbarButtons : [ "bold", "italic", "underline", "strikeThrough", "subscript", "superscript", "fontSize", "color", "clearFormatting", "formatOL", "formatUL", "outdent", "indent", "quote", "insertLink", "dataItems" ],
					enter : FroalaEditor.ENTER_P,
					key : '7F4D3F3H3cA5A4B3A1E4B2G2E3D1C6vDIG1QCYRWa1GPId1f1I1=='
				});
				// $('#emailBody').froalaEditor({
				// 	key:'DLAHYKAJOEc1HQDUH==',
				// 	enter: $.FroalaEditor.ENTER_P,
				// 	toolbarSticky: true,
				// 	scrollableContainer: '#sendProposalDialog'
				// });
			},500);
	// $('input').iCheck({
	//   checkboxClass: 'icheckbox_square-orange',
	//   radioClass: 'iradio_square-orange',
	//   increaseArea: '20%' // optional
	// });
	//
	$("#emailCc").tagsInput({
      defaultText: 'Add a email',
      width: 'auto',
      minInputWidth : '390px',
      placeholderColor : '#858585'
    });

});
</script>