<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Comparator"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.Locale"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page	import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page	import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.OptionsDataAccess"%>
<%@page import="java.util.List"%>
<%

int customFields = 5;

%>
<div id="custom-fields">
	<form class="form-custom-fields" method="POST">
		<div class="modal-header">
			<div class="titleBar">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					<i class="fal fa-times-circle"></i>
				</button>
			</div>
			<h2 class="modal-title">Custom Fields</h2>
		</div>
		
		<div class="modal-body">
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#proposals" aria-controls="users" role="tab" data-toggle="tab">Proposals</a></li>
				<li role="presentation"><a href="#products" aria-controls="profile" role="tab" data-toggle="tab">Products</a></li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="proposals">
					<div class="row heading">
		      			<div class="col-sm-1">
			   				<div class="form-group">
			      				On/Off
			   				</div>
		      			</div>
		      			<div class="col-sm-5">
			   				<div class="form-group">
			   					Label
			   				</div>
		   				</div>
		      			<div class="col-sm-4">
			   				<div class="form-group">
			   					Type
			   				</div>
		   				</div>
		   				<div class="col-sm-2">
			   				<div class="form-group">
			      				Required
			   				</div>
		      			</div>
		      		</div>
		      		<% for(int i=1;i<=customFields;i++){ %>
		    		<div class="row">
		    			<input type="hidden" id="field-c<%=i%>">
		      			<div class="col-sm-1">
		      				<div class="form-group">
			      				<input type="checkbox" id="on-attr_customfield<%=i %>" class="custom-on attr_customfield<%=i %>" data-key="attr_customfield<%=i %>" name="on-attr_customfield<%=i %>">
		      				</div>
		      			</div>
		      			<div class="col-sm-5">
		      				<div class="form-group">
			      				<input type="text" id="label-attr_customfield<%=i %>" class="form-control custom-label custom-field attr_customfield<%=i %>" data-key="attr_customfield<%=i %>" name="label-attr_customfield<%=i %>" placeholder=""  value="">
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<select class="form-control custom-type custom-field attr_customfield<%=i %>" id="type-attr_customfield<%=i%>" data-key="attr_customfield<%=i %>" name="type-attr_customfield<%=i%>">
									<option value="text" >Text</option>
									<option value="dropdown" >Dropdown</option>
									<option value="integer" >Integer</option>
									<option value="currency" >Currency</option>
									<option value="date" >Date</option>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<input type="checkbox" id="required-attr_customfield<%=i %>" class="custom-required attr_customfield<%=i %>" data-key="attr_customfield<%=i %>" name="required-attr_customfield<%=i %>">
							</div>
						</div>
		      			<div class="col-sm-12">
		      				<div class="form-group custom-values-group">
		      					<input type="text" id="values-attr_customfield<%=i%>" class="form-control custom-value custom-field attr_customfield<%=i %>" data-key="attr_customfield<%=i %>" name="values-attr_customfield<%=i%>" placeholder=""  value="">
							</div>
						</div>
		      		</div>
		      		<% } %>
				</div>
				
				<div role="tabpanel" class="tab-pane" id="products">
					<div class="row heading">
		      			<div class="col-sm-1">
			   				<div class="form-group">
			      				On/Off
			   				</div>
		      			</div>
		      			<div class="col-sm-5">
			   				<div class="form-group">
			   					Label
			   				</div>
		   				</div>
		      			<div class="col-sm-4">
			   				<div class="form-group">
			   					Type
			   				</div>
		   				</div>
		   				<div class="col-sm-2">
			   				<div class="form-group">
			      				Required
			   				</div>
		      			</div>
		      		</div>
		      		<% for(int i=1;i<=customFields;i++){ %>
		    		<div class="row">
		    			<input type="hidden" id="field-c<%=i%>">
		      			<div class="col-sm-1">
		      				<div class="form-group">
			      				<input type="checkbox" id="on-attr_customfield<%=i %>" class="custom-on attr_customfield<%=i %>" data-key="attr_customfield<%=i %>" name="on-attr_customfield<%=i %>">
		      				</div>
		      			</div>
		      			<div class="col-sm-5">
		      				<div class="form-group">
			      				<input type="text" id="label-attr_customfield<%=i %>" class="form-control custom-label custom-field attr_customfield<%=i %>" data-key="attr_customfield<%=i %>" name="label-attr_customfield<%=i %>" placeholder=""  value="">
							</div>
						</div>
		      			<div class="col-sm-4">
		      				<div class="form-group">
			      				<select class="form-control custom-type custom-field attr_customfield<%=i %>" id="type-attr_customfield<%=i%>" data-key="attr_customfield<%=i %>" name="type-attr_customfield<%=i%>">
									<option value="text" >Text</option>
									<option value="dropdown" >Dropdown</option>
                                    <option value="integer" >Integer</option>
                                    <option value="currency" >Currency</option>
                                    <option value="date" >Date</option>
								</select>
			      			</div>
		      			</div>
						<div class="col-sm-2">
							<div class="form-group">
								<input type="checkbox" id="required-attr_customfield<%=i %>" class="custom-required attr_customfield<%=i %>" data-key="attr_customfield<%=i %>" name="required-attr_customfield<%=i %>">
							</div>
						</div>
		      			<div class="col-sm-12">
		      				<div class="form-group custom-values-group">
		      					<input type="text" id="values-attr_customfield<%=i%>" class="form-control custom-value custom-field attr_customfield<%=i %>" data-key="attr_customfield<%=i %>" name="values-attr_customfield<%=i%>" placeholder=""  value="">
							</div>
						</div>
		      		</div>
		      		<% } %>
					<%--<span style="font-size: 11px;font-style: italic;"><a href="#email-support" data-toggle="modal">Note: Please click here to contact QuoteCloud support to have these options added to the pricing tables in your proposals</a></span>--%>
				</div>
			</div>
		</div>
		
		<div class="modal-footer">
			<button type="button" id="closeForm" class="btn btn-danger"
				data-dismiss="modal">
				<i class="fa fa-times"></i> Close
			</button>
			<button type="submit" class="btn btn-success disabled" id="saveForm">
	        	<i class="fa fa-save"></i> Save
	      	</button>
		</div>
	</form>
</div>

<script src="/application/js/form-validation/modal-custom-fields.js"></script>

</compress:html>