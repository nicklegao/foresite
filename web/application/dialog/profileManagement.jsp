<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.MaestranoUserAccountDataAccess"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="java.util.Hashtable"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
<div id="profile-management">
<%
UserAccountDataAccess uada = new UserAccountDataAccess();

String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
Hashtable<String, String> userDetails = uada.getUserWithId(userId);

CategoryData company = uada.getUserCompanyForUserId(userId);

String email = userDetails.get(UserAccountDataAccess.E_EMAIL_ADDRESS);
String emailLabel = "Maestrano";
if (MaestranoUserAccountDataAccess.isMaestranoUser(userDetails)) {
	email = userDetails.get(MaestranoUserAccountDataAccess.E_MAESTRANO_REAL_EMAIL);
	String tenantKey = company.getString(MaestranoUserAccountDataAccess.C_MAESTRANO_TENANT_KEY);
	if (tenantKey.startsWith("nab"))
		emailLabel = "NAB BIO";
}

boolean pleaseEnterPhoneNumber = StringUtils.isBlank(userDetails.get(UserAccountDataAccess.E_MOBILE_NUMBER));
%>
  <form class="form-my-profile" action="/api/updateAccount" method="POST">
    <div class="modal-header">
      <div class="titleBar">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
      </div>
      <h2 class="modal-title">My Profile</h2>
    </div>
    <div class="modal-body">
      <p>The following details will appear in your proposals and in emails sent to customers on your behalf:</p>
      <div class="form-horizontal">
        <div class="form-group">
          <label for="profile-name" class="col-sm-3 control-label">
            Name:
          </label>
          <div class="col-sm-8">
	          <input type="text" id="profile-name" class="form-control" name="myName" placeholder="Name"
	                 value="<%=ESAPI.encoder().encodeForHTMLAttribute(userDetails.get(UserAccountDataAccess.E_NAME)) %>">
          </div>
        </div>
        <div class="form-group">
          <label for="profile-name" class="col-sm-3 control-label">
            Surname:
          </label>
          <div class="col-sm-8">
            <input type="text" id="profile-surname" class="form-control" name="mySurname" placeholder="Surname" 
                   value="<%=ESAPI.encoder().encodeForHTMLAttribute(userDetails.get(UserAccountDataAccess.E_SURNAME)) %>">
          </div>
        </div>
        <% if (pleaseEnterPhoneNumber) { %>
	        <div class="form-group">
	          <div class="col-sm-8 col-sm-offset-3">
      			<b style="color: red">Please enter your Mobile Number so recipients of your proposals can see it and can contact you over the phone:</b>
	          </div>
	        </div>
      	<% } %>
        <div class="form-group">
          <label for="profile-phone" class="col-sm-3 control-label">
            Phone:
          </label>
          <div class="col-sm-8">
            <input type="text" id="profile-phone" class="form-control" name="myMobile" placeholder="Mobile"
                   value="<%=ESAPI.encoder().encodeForHTMLAttribute(userDetails.get(UserAccountDataAccess.E_MOBILE_NUMBER)) %>">
          </div>
        </div>
        <div class="form-group">
          <label for="profile-email" class="col-sm-3 control-label">
            Email:
          </label>
          <div class="col-sm-6">
             <input type="email" id="profile-email" class="form-control" name="myEmail" placeholder="Email Address"
                    readonly
                    value="<%=ESAPI.encoder().encodeForHTMLAttribute(email) %>">
          </div>
            <div class="col-sm-2">
                <button type="button" id="changeEmailButton" class="btn btn-warning">
                    <i class="fa fa-pencil"></i> Change </button>
            </div>
        </div>
        
        <% if (MaestranoUserAccountDataAccess.isMaestranoUser(userDetails)) { %>
      		<p>These are your <%=emailLabel %> credentials:</p>
	        <div class="form-group">
	          <label class="col-sm-3 control-label">
	            <%=emailLabel %> Email:
	          </label>
	          <div class="col-sm-8">
	             <input type="email" class="form-control"
	                    readonly  
	                    value="<%=ESAPI.encoder().encodeForHTMLAttribute(userDetails.get(UserAccountDataAccess.E_EMAIL_ADDRESS)) %>">
	          </div>
	        </div>
        	<div class="form-group">
	          <label class="col-sm-3 control-label">
	            <%=emailLabel %> User ID:
	          </label>
	          <div class="col-sm-8">
	             <input type="text" class="form-control"
	                    readonly  
	                    value="<%=ESAPI.encoder().encodeForHTMLAttribute(userDetails.get(MaestranoUserAccountDataAccess.E_MAESTRANO_USER_ID)) %>">
	          </div>
	        </div>
        <% } %>
        <%--
        <div class="form-group">
          <label for="profile-phone" class="col-sm-3 control-label">
            Footer:
          </label>
          <div class="col-sm-8">
            <textarea id="profile-signature" class="form-control" rows="3" name="myEmailFooter"><%=uada.getEmailFooterForUserId(userId).toString() %></textarea>
          </div>
        </div>
         --%>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
        <i class="fa fa-times"></i> Close
      </button>
      <button type="submit" class="btn btn-success">
        <i class="fa fa-save"></i> Save
      </button>
    </div>
  </form>
  <script src="/application/js/form-validation/modal-my-profile.js"></script>
</div>
</compress:html>