<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="java.util.Hashtable"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.OptionsDataAccess"%>
<%@page import="au.net.webdirector.common.Defaults" %>
<%@page import="java.util.List"%>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="org.json.JSONObject" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
<%
UserAccountDataAccess uada = new UserAccountDataAccess();

String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
Hashtable<String, String> userDetails = uada.getUserWithId(userId);
CategoryData company = uada.getUserCompanyForUserId(userId);
String storeName = uada.getUserCompanyNameForUserId(userId);
OptionsDataAccess oda = new OptionsDataAccess();
List<String> fonts = oda.getFonts();

String tableStyle = ESAPI.encoder().encodeForHTMLAttribute(company.getString(UserAccountDataAccess.C1_PROPOSAL_TABLE_STYLE));
String tabStyle = ESAPI.encoder().encodeForHTMLAttribute(company.getString(UserAccountDataAccess.C1_PROPOSAL_TAB_STYLE));

//Proposal Header Styles
String topStyleInfo = company.getString(TUserAccountDataAccess.C1_HEADER_TOP_DATA);
	JSONObject topStyle = new JSONObject();
	JSONObject topStyleLeft = new JSONObject();
	JSONObject topStyleCenter = new JSONObject();
	JSONObject topStyleRight = new JSONObject();
if (topStyleInfo != null && topStyleInfo.length() > 0) {
	topStyle = new JSONObject(topStyleInfo);
	topStyleLeft = topStyle.getJSONObject("left");
	topStyleCenter = topStyle.getJSONObject("center");
	topStyleRight = topStyle.getJSONObject("right");

}

Boolean topHeaderActive = company.getBoolean(TUserAccountDataAccess.C1_HEADER_TOP_ACTIVE);
String topHeaderFontColor = company.getString(TUserAccountDataAccess.C1_HEADER_TOP_FONT_COLOR);
String topHeaderBackground = company.getString(TUserAccountDataAccess.C1_HEADER_TOP_BACKGROUND);

String bottomStyleInfo = company.getString(TUserAccountDataAccess.C1_HEADER_BOTTOM_DATA);
	JSONObject bottomStyle = new JSONObject();
	JSONObject bottomStyleLeft = new JSONObject();
	JSONObject bottomStyleCenter = new JSONObject();
	JSONObject bottomStyleRight = new JSONObject();
	if (bottomStyleInfo != null && bottomStyleInfo.length() > 0) {
		bottomStyle = new JSONObject(bottomStyleInfo);
		bottomStyleLeft = bottomStyle.getJSONObject("left");
		bottomStyleCenter = bottomStyle.getJSONObject("center");
		bottomStyleRight = bottomStyle.getJSONObject("right");

	}

Boolean bottomHeaderActive = company.getBoolean(TUserAccountDataAccess.C1_HEADER_BOTTOM_ACTIVE);
String bottomHeaderFontColor = company.getString(TUserAccountDataAccess.C1_HEADER_BOTTOM_FONT_COLOR);
String bottomHeaderBackground = company.getString(TUserAccountDataAccess.C1_HEADER_BOTTOM_BACKGROUND);


boolean showTabs = !company.getBoolean(UserAccountDataAccess.C1_PROPOSAL_SHOW_TABS);
boolean showCoverpage = !company.getBoolean(TUserAccountDataAccess.C1_PROPOSAL_HIDE_COVERPAGE);
	boolean hideCoverProposalInfo = company.getBoolean(TUserAccountDataAccess.C1_PROPOSAL_HIDE_COVERPAGE_PROPOSAL_INFO);

boolean appendPDFBool = company.getBoolean(TUserAccountDataAccess.C1_PROPOSAL_HIDE_COVERPAGE_PROPOSAL_INFO);
boolean hideCoverSenderInfo = company.getBoolean(TUserAccountDataAccess.C1_PROPOSAL_HIDE_COVERPAGE_SENDER_INFO);
boolean hideCoverBoxBackground = company.getBoolean(TUserAccountDataAccess.C1_PROPOSAL_HIDE_COVERPAGE_BOX_BACKGROUND);


	//Section Header Styles
	boolean uppercaseSection = company.getBoolean(UserAccountDataAccess.C1_SECTION_UPPERCASE);
	boolean underlineSection = company.getBoolean(UserAccountDataAccess.C1_SECTION_UNDERLINE);
	boolean boldSection = company.getBoolean(UserAccountDataAccess.C1_SECTION_BOLD);
	boolean customSectionFontColor = company.getBoolean(UserAccountDataAccess.C1_SECTION_CUSTOM_FONT_COLOR);

	//H1 styles
	boolean uppercaseH1 = company.getBoolean(UserAccountDataAccess.C1_H1_UPPERCASE);
	boolean underlineH1 = company.getBoolean(UserAccountDataAccess.C1_H1_UNDERLINE);
	boolean boldH1 = company.getBoolean(UserAccountDataAccess.C1_H1_BOLD);
	boolean customH1FontColor = company.getBoolean(UserAccountDataAccess.C1_H1_CUSTOM_FONT_COLOR);

	//H2 styles
	boolean uppercaseH2 = company.getBoolean(UserAccountDataAccess.C1_H2_UPPERCASE);
	boolean underlineH2 = company.getBoolean(UserAccountDataAccess.C1_H2_UNDERLINE);
	boolean boldH2 = company.getBoolean(UserAccountDataAccess.C1_H2_BOLD);
	boolean customH2FontColor = company.getBoolean(UserAccountDataAccess.C1_H2_CUSTOM_FONT_COLOR);

	//H3 styles
	boolean uppercaseH3 = company.getBoolean(UserAccountDataAccess.C1_H3_UPPERCASE);
	boolean underlineH3 = company.getBoolean(UserAccountDataAccess.C1_H3_UNDERLINE);
	boolean boldH3 = company.getBoolean(UserAccountDataAccess.C1_H3_BOLD);
	boolean customH3FontColor = company.getBoolean(UserAccountDataAccess.C1_H3_CUSTOM_FONT_COLOR);
%>


<link rel="stylesheet" href="/application/assets/plugins/switchery/switchery.css" />
<script src="/application/assets/plugins/switchery/switchery.js"></script>
<div id="edit-styles">
  <form class="form-company-styles" enctype="multipart/form-data" method="POST">
    <div class="modal-header line bottom">
      <div class="titleBar">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="fal fa-times-circle"></i>
        </button>
      </div>
      <h2 class="modal-title">Edit Styles</h2>
    </div>
    <div class="modal-body scrollable smart-form">
    
    <div>

	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" role="tablist">
	  	<li role="presentation" class="active"><a href="#proposal-headings" aria-controls="proposal-headings" role="tab" data-toggle="tab">Headings and Fonts</a></li>
	  	<li role="presentation"><a href="#proposal-colours" aria-controls="proposal-colours" role="tab" data-toggle="tab">Colours</a></li>
	    <li role="presentation"><a href="#table-styles" aria-controls="table-styles" role="tab" data-toggle="tab">Table Styles</a></li>
	    <li role="presentation"><a href="#tab-styles" aria-controls="tab-styles" role="tab" data-toggle="tab">Tab Styles</a></li>
	    <li role="presentation"><a href="#coverpage" aria-controls="companylogo" role="tab" data-toggle="tab">Cover Page</a></li>
	  	<li role="presentation"><a href="#companylogo" aria-controls="companylogo" role="tab" data-toggle="tab">Logo</a></li>
	  	<li role="presentation"><a href="#contentSettings" aria-controls="companylogo" role="tab" data-toggle="tab">Content</a></li>
	  </ul>
	
	  <!-- Tab panes -->
	  <div class="tab-content">
		  <div role="tabpanel" class="tab-pane active" id="proposal-headings">

			  <div class="f-group">
				  <h2 class="text-center">Proposal Font Options</h2>

					  <%--SECTION HEADINGS CARDS--%>
				  <div class="collapse-card">
					  <div class="collapse-card__heading">
						  <div class="collapse-card__title">
							  <i class="fa fa-header fa-2x fa-fw"></i>
							  <span class="styleHeadingTitles">Section Heading</span>
							  <% if (underlineSection) { %>
							  <i class="fa fa-underline fa-2x fa-fw pull-right"></i>
							  <% } %>
							  <% if (boldSection) { %>
							  <i class="fa fa-bold fa-2x fa-fw pull-right"></i>
							  <% } %>
							  <% if (uppercaseSection) { %>
							  <i class="fa fa-text-height fa-2x fa-fw pull-right"></i>
							  <% } %>
							  <% if (customSectionFontColor) { %>
							  <i class="fa fa-paint-brush fa-2x fa-fw pull-right"></i>
							  <% } %>
						  </div>
					  </div>
					  <div class="collapse-card__body">
						  <div class="row heading">
							  <div class="col-sm-3 text-left"></div>
							  <div class="col-sm-4 f-field text-left">Font</div>
							  <div class="col-sm-2 f-field text-left">Size</div>
							  <div class="col-sm-2 f-field text-left font-section-color-title">Font Colour</div>
						  </div>
						  <div class="row">
							  <div class="col-sm-3 f-label"><span>Section Heading</span></div>
							  <div class="col-sm-4 f-field">
								  <select class="form-control" name="fontFamilySECTION">
									  <%
										  for (String font : fonts){
											  String selectedClause = font.equals(company.getString(uada.C1_SECTION_FONT))?"selected":"";
									  %>
									  <option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>" <%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
									  <% } %>
								  </select>
							  </div>
							  <div class="col-sm-2 f-field">
								  <input type="text" id="font-size-h1" class="form-control" name="fontSizeSECTION" placeholder="Font Size" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_SECTION_SIZE)) %>">
							  </div>
							  <div class="col-sm-2 f-field fontColorSectionTitleSection">
								  <input type="text" id="color-header" class="form-control" name="fontColorSectionTitle" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_SECTION_HEADER_COLOR)) %>">
							  </div>
						  </div>
						  <div style="padding-top: .5em;">
							  <section style="padding-right:0px;padding-left: 60px">
								  <input class="table-checkbox" type="checkbox" name="customSectionColor" value="show" <%=customSectionFontColor?"checked":"" %> /> Custom Font Color
							  </section>
							  <section style="padding-right:0px;padding-left: 60px">
								  <input class="table-checkbox" type="checkbox" name="uppercaseSectionTitle" value="show" <%=uppercaseSection?"checked":"" %> /> Uppercase Headings
							  </section>
							  <section style="padding-right:0px;padding-left: 60px">
								  <input class="table-checkbox" type="checkbox" name="underlineSectionTitle" value="show" <%=underlineSection?"checked":"" %> /> Underline Headings
							  </section>
							  <section style="padding-right:0px;padding-left: 60px">
								  <input class="table-checkbox" type="checkbox" name="boldSectionTitle" value="show" <%=boldSection?"checked":"" %> /> Bold Headings
							  </section>
						  </div>
					  </div>
				  </div>
					  <%--SECTION HEADINGS CARDS END--%>


					  <%--H1 CARDS--%>
					  <div class="collapse-card">
						  <div class="collapse-card__heading">
							  <div class="collapse-card__title">
								  <i class="fa fa-header fa-2x fa-fw"></i>
								  <span class="styleHeadingTitles">Heading 1</span>
								  <% if (underlineH1) { %>
								  <i class="fa fa-underline fa-2x fa-fw pull-right"></i>
								  <% } %>
								  <% if (boldH1) { %>
								  <i class="fa fa-bold fa-2x fa-fw pull-right"></i>
								  <% } %>
								  <% if (uppercaseH1) { %>
								  <i class="fa fa-text-height fa-2x fa-fw pull-right"></i>
								  <% } %>
								  <% if (customH1FontColor) { %>
								  <i class="fa fa-paint-brush fa-2x fa-fw pull-right"></i>
								  <% } %>
							  </div>
						  </div>
						  <div class="collapse-card__body">
							  <div class="row heading">
								  <div class="col-sm-3 text-left"></div>
								  <div class="col-sm-4 f-field text-left">Font</div>
								  <div class="col-sm-2 f-field text-left">Size</div>
								  <div class="col-sm-2 f-field text-left font-section-color-title-h1">Font Colour</div>
							  </div>
							  <div class="row">
								  <div class="col-sm-3 f-label"><span>Heading 1</span></div>
								  <div class="col-sm-4 f-field">
									  <select class="form-control" name="fontFamilyH1">
										  <%
											  for (String font : fonts){
												  String selectedClause = font.equals(company.getString(uada.C1_H1_FONT))?"selected":"";
										  %>
										  <option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>" <%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
										  <% } %>
									  </select>
								  </div>
								  <div class="col-sm-2 f-field">
									  <input type="text" id="font-size-h1" class="form-control" name="fontSizeH1" placeholder="Font Size" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_H1_SIZE)) %>">
								  </div>
								  <div class="col-sm-2 f-field fontColorHeading1">
									  <input type="text" id="color-header1" class="form-control" name="fontColorHeading1" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_H1_HEADER_COLOR)) %>">
								  </div>
							  </div>
							  <div style="padding-top: .5em;">
								  <section style="padding-right:0px;padding-left: 60px">
									  <input class="table-checkbox" type="checkbox" name="customHeading1Color" value="show" <%=customH1FontColor?"checked":"" %> /> Custom Font Color
								  </section>
								  <section style="padding-right:0px;padding-left: 60px">
									  <input class="table-checkbox" type="checkbox" name="uppercaseHeading1" value="show" <%=uppercaseH1?"checked":"" %> /> Uppercase Headings
								  </section>
								  <section style="padding-right:0px;padding-left: 60px">
									  <input class="table-checkbox" type="checkbox" name="underlineHeading1" value="show" <%=underlineH1?"checked":"" %> /> Underline Headings
								  </section>
								  <section style="padding-right:0px;padding-left: 60px">
									  <input class="table-checkbox" type="checkbox" name="boldHeading1" value="show" <%=boldH1?"checked":"" %> /> Bold Headings
								  </section>
							  </div>
						  </div>
					  </div>
						  <%--H1 CARDS END--%>


					  <%--H2 CARDS--%>
				  <div class="collapse-card">
					  <div class="collapse-card__heading">
						  <div class="collapse-card__title">
							  <i class="fa fa-header fa-2x fa-fw"></i>
							  <span class="styleHeadingTitles">Heading 2</span>
							  <% if (underlineH2) { %>
							  <i class="fa fa-underline fa-2x fa-fw pull-right"></i>
							  <% } %>
							  <% if (boldH2) { %>
							  <i class="fa fa-bold fa-2x fa-fw pull-right"></i>
							  <% } %>
							  <% if (uppercaseH2) { %>
							  <i class="fa fa-text-height fa-2x fa-fw pull-right"></i>
							  <% } %>
							  <% if (customH2FontColor) { %>
							  <i class="fa fa-paint-brush fa-2x fa-fw pull-right"></i>
							  <% } %>
						  </div>
					  </div>
					  <div class="collapse-card__body">
						  <div class="row heading">
							  <div class="col-sm-3 text-left"></div>
							  <div class="col-sm-4 f-field text-left">Font</div>
							  <div class="col-sm-2 f-field text-left">Size</div>
							  <div class="col-sm-2 f-field text-left font-section-color-title-h2">Font Colour</div>
						  </div>
						  <div class="row">
							  <div class="col-sm-3 f-label"><span>Heading 2</span></div>
							  <div class="col-sm-4 f-field">
								  <select class="form-control" name="fontFamilyH2">
									  <%
										  for (String font : fonts){
											  String selectedClause = font.equals(company.getString(uada.C1_H2_FONT))?"selected":"";
									  %>
									  <option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>" <%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
									  <% } %>
								  </select>
							  </div>
							  <div class="col-sm-2 f-field">
								  <input type="text" id="font-size-h2" class="form-control" name="fontSizeH2" placeholder="Font Size" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_H2_SIZE)) %>">
							  </div>
							  <div class="col-sm-2 f-field fontColorHeading2">
								  <input type="text" id="color-header2" class="form-control" name="fontColorHeading2" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_H2_HEADER_COLOR)) %>">
							  </div>
						  </div>
						  <div style="padding-top: .5em;">
							  <section style="padding-right:0px;padding-left: 60px">
								  <input class="table-checkbox" type="checkbox" name="customHeading2Color" value="show" <%=customH2FontColor?"checked":"" %> /> Custom Font Color
							  </section>
							  <section style="padding-right:0px;padding-left: 60px">
								  <input class="table-checkbox" type="checkbox" name="uppercaseHeading2" value="show" <%=uppercaseH2?"checked":"" %> /> Uppercase Headings
							  </section>
							  <section style="padding-right:0px;padding-left: 60px">
								  <input class="table-checkbox" type="checkbox" name="underlineHeading2" value="show" <%=underlineH2?"checked":"" %> /> Underline Headings
							  </section>
							  <section style="padding-right:0px;padding-left: 60px">
								  <input class="table-checkbox" type="checkbox" name="boldHeading2" value="show" <%=boldH2?"checked":"" %> /> Bold Headings
							  </section>
						  </div>
					  </div>
				  </div>
					  <%--H2 CARDS END--%>


					  <%--H3 CARDS--%>
				  <div class="collapse-card">
					  <div class="collapse-card__heading">
						  <div class="collapse-card__title">
							  <i class="fa fa-header fa-2x fa-fw"></i>
							  <span class="styleHeadingTitles">Heading 3</span>
							  <% if (underlineH3) { %>
							  <i class="fa fa-underline fa-2x fa-fw pull-right"></i>
							  <% } %>
							  <% if (boldH3) { %>
							  <i class="fa fa-bold fa-2x fa-fw pull-right"></i>
							  <% } %>
							  <% if (uppercaseH3) { %>
							  <i class="fa fa-text-height fa-2x fa-fw pull-right"></i>
							  <% } %>
							  <% if (customH3FontColor) { %>
							  <i class="fa fa-paint-brush fa-2x fa-fw pull-right"></i>
							  <% } %>
						  </div>
					  </div>
					  <div class="collapse-card__body">
						  <div class="row heading">
							  <div class="col-sm-3 text-left"></div>
							  <div class="col-sm-4 f-field text-left">Font</div>
							  <div class="col-sm-2 f-field text-left">Size</div>
							  <div class="col-sm-2 f-field text-left font-section-color-title-h3">Font Colour</div>
						  </div>
						  <div class="row">
							  <div class="col-sm-3 f-label"><span>Heading 3</span></div>
							  <div class="col-sm-4 f-field">
								  <select class="form-control" name="fontFamilyH3">
									  <%
										  for (String font : fonts){
											  String selectedClause = font.equals(company.getString(uada.C1_H3_FONT))?"selected":"";
									  %>
									  <option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>" <%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
									  <% } %>
								  </select>
							  </div>
							  <div class="col-sm-2 f-field">
								  <input type="text" id="font-size-h3" class="form-control" name="fontSizeH3" placeholder="Font Size" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_H3_SIZE)) %>">
							  </div>
							  <div class="col-sm-2 f-field fontColorHeading3">
								  <input type="text" id="color-header3" class="form-control" name="fontColorHeading3" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_H3_HEADER_COLOR)) %>">
							  </div>
						  </div>
						  <div style="padding-top: .5em;">
							  <section style="padding-right:0px;padding-left: 60px">
								  <input class="table-checkbox" type="checkbox" name="customHeading3Color" value="show" <%=customH3FontColor?"checked":"" %> /> Custom Font Color
							  </section>
							  <section style="padding-right:0px;padding-left: 60px">
								  <input class="table-checkbox" type="checkbox" name="uppercaseHeading3" value="show" <%=uppercaseH3?"checked":"" %> /> Uppercase Headings
							  </section>
							  <section style="padding-right:0px;padding-left: 60px">
								  <input class="table-checkbox" type="checkbox" name="underlineHeading3" value="show" <%=underlineH3?"checked":"" %> /> Underline Headings
							  </section>
							  <section style="padding-right:0px;padding-left: 60px">
								  <input class="table-checkbox" type="checkbox" name="boldHeading3" value="show" <%=boldH3?"checked":"" %> /> Bold Headings
							  </section>
						  </div>
					  </div>
				  </div>
					  <%--H3 CARDS END--%>

					  <%--BODY CARDS--%>
				  <div class="collapse-card">
					  <div class="collapse-card__heading">
						  <div class="collapse-card__title">
							  <i class="fa fa-align-justify fa-2x fa-fw"></i>
							  <span class="styleHeadingTitles">Body</span>
						  </div>
					  </div>
					  <div class="collapse-card__body">
						  <div class="row heading">
							  <div class="col-sm-3 text-left"></div>
							  <div class="col-sm-4 f-field text-left">Font</div>
							  <div class="col-sm-2 f-field text-left">Size</div>
							  <div class="col-sm-2 f-field text-left">Font Colour</div>
						  </div>
						  <div class="row">
							  <div class="col-sm-3 f-label"><span>Body</span></div>
							  <div class="col-sm-4 f-field">
								  <select class="form-control" name="fontFamilyBody">
									  <% for (String font : fonts){
										  String selectedClause = font.equals(company.getString(uada.C1_BODY_FONT))?"selected":"";
									  %>
									  <option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>" <%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
									  <% } %>
								  </select>
							  </div>
							  <div class="col-sm-2 f-field">
								  <input type="text" id="font-size-body" class="form-control" name="fontSizeBody" placeholder="Font Size"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_BODY_SIZE)) %>">
							  </div>
							  <div class="col-sm-2 f-field">
								  <input type="text" id="color-body" class="form-control" name="fontColorBody" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_BODY_COLOR)) %>">
							  </div>
						  </div>
					  </div>
				  </div>
					  <%--BODY CARDS END--%>
			  <h2 class="text-center">Proposal Page Options</h2>
					  <%--HEADER CARDS--%>
				  <div class="collapse-card">
					  <div class="collapse-card__heading">
						  <div class="collapse-card__title">
							  <i class="fa fa-align-justify fa-2x fa-fw"></i>
							  <span class="styleHeadingTitles">Page Header</span>
						  </div>
					  </div>
					  <div class="collapse-card__body">
						  <div class="row">
							  <section style="padding-right:0px;padding-left: 60px">
								  <input class="table-checkbox" type="checkbox" name="topHeaderActive" value="show" <%=topHeaderActive?"checked":"" %> /> Show top header all content pages
							  </section>
						  </div>
						  <h3 class="text-center">Header Colours</h3>
						  <div class="row heading">
							  <div class="col-sm-3 text-left"></div>
							  <div class="col-sm-3 f-field text-center">Font Colour</div>
							  <div class="col-sm-3 f-field text-center">Background Colour</div>
							  <div class="col-sm-3 f-field text-left"></div>
						  </div>
						  <div class="row">
							  <div class="col-sm-3 f-field"></div>
							  <div class="col-sm-3 f-field text-center">
								  <input type="text" id="color-header-font" class="form-control" name="headerColorFont" value="<%=ESAPI.encoder().encodeForHTMLAttribute(topHeaderFontColor) %>">
							  </div>
							  <div class="col-sm-3 f-field text-center">
								  <input type="text" id="color-header-background" class="form-control" name="headerColorBackground" value="<%=ESAPI.encoder().encodeForHTMLAttribute(topHeaderBackground) %>">
							  </div>
							  <div class="col-sm-3 f-field">
							  </div>
						  </div>
						  </br>
						  <h3 class="text-center">Header Sections</h3>
						  <div class="row heading">
							  <div class="col-sm-1 text-left"></div>
							  <div class="col-sm-3 text-left">Left</div>
							  <div class="col-sm-3 f-field text-left">Center</div>
							  <div class="col-sm-3 f-field text-left">Right</div>
							  <div class="col-sm-1 text-left"></div>
						  </div>
						  <div class="row">
							  <div class="col-sm-1 f-field">
							  </div>
							  <div class="col-sm-3 f-field">
								  <select class="form-control" name="topStyleLeft" positionId="Left" dataId="topLeftData" id="topStyleLeft">
									  <% String topLeft = "";
									  if (topStyleLeft != null && topStyleLeft.has("option")) {
									  	topLeft = topStyleLeft.getString("option");
									  }%>
									  <option value="none" <%=topLeft.equalsIgnoreCase("none") ?"selected":"" %>>Blank</option>
									  <option value="image" <%=topLeft.equalsIgnoreCase("image") ?"selected":"" %>>Logo</option>
									  <option value="text" <%=topLeft.equalsIgnoreCase("text") ?"selected":"" %>>Text</option>
									  <option value="page" <%=topLeft.equalsIgnoreCase("page") ?"selected":"" %>>Page Counter</option>
								  </select>
							  </div>
							  <div class="col-sm-3 f-field">
								  <select class="form-control" name="topStyleCenter" positionId="Center" dataId="topCenterData" id="topStyleCenter">
									  <% String topCenter = "";
										  if (topStyleCenter != null && topStyleCenter.has("option")) {
											  topCenter = topStyleCenter.getString("option");
										  }%>
									  <option value="none" <%=topCenter.equalsIgnoreCase("none") ?"selected":"" %>>Blank</option>
									  <option value="text" <%=topCenter.equalsIgnoreCase("text") ?"selected":"" %>>Text</option>
									  <option value="page" <%=topCenter.equalsIgnoreCase("page") ?"selected":"" %>>Page Counter</option>
								  </select>
							  </div>
							  <div class="col-sm-3 f-field">
								  <select class="form-control" name="topStyleRight" positionId="Right" dataId="topRightData" id="topStyleRight">
									  <% String topRight = "";
										  if (topStyleRight != null && topStyleRight.has("option")) {
											  topRight = topStyleRight.getString("option");
										  }%>
									  <option value="none" <%=topRight.equalsIgnoreCase("none") ?"selected":"" %>>Blank</option>
									  <option value="image" <%=topRight.equalsIgnoreCase("image") ?"selected":"" %>>Logo</option>
									  <option value="text" <%=topRight.equalsIgnoreCase("text") ?"selected":"" %>>Text</option>
									  <option value="page" <%=topRight.equalsIgnoreCase("page") ?"selected":"" %>>Page Counter</option>
								  </select>
							  </div>
							  <div class="col-sm-3 f-field">
							  </div>
						  </div>

						  <div class="row" id="topLeftData">
							  <% if (topStyleLeft != null && topStyleLeft.has("data")) {
								  String topLeftData = topStyleLeft.getString("data");
								  String topLeftType = topStyleLeft.getString("type");
								  switch (topLeftType) {
									  case "image":%>

							  <div class="row">
								  <section class="logo-selector">
									  <input class="" id="topLeftData-image-string" data-id="<%= ESAPI.encoder().encodeForHTMLAttribute(topLeftData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(topLeftData)%>" name="topLeftData-image-string" hidden/>
									  <input class="header-upload" id="topLeftData-image" data-id="<%= Defaults.getInstance().getStoreContext() + ESAPI.encoder().encodeForHTMLAttribute(topLeftData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(topLeftData)%>" name="topLeftData-image" type="file" />
								  </section>
								  <input id="removeheaderImage" type="checkbox" name="removeheaderImage" class="hidden">
							  </div>
									  	<%break;
									  case "text": %>
							  			<div class="col-sm-1 f-field"></div>
							  			<div class="col-sm-10 f-field"><h5>Left Text</h5>
											<input type="text" id="topLeftData-text" class="form-control" name="topLeftData-text" placeholder="Content text"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(topLeftData)%>"></div>
							  			<div class="col-sm-1 f-field"></div>
									  	<% break;
									  default:
									  	break;
								  }
								  }%>
						  </div>
						  <div class="row" id="topCenterData">
							  <% if (topStyleCenter != null && topStyleCenter.has("data")) {
								  String topCenterData = topStyleCenter.getString("data");
								  String topCenterType = topStyleCenter.getString("type");
								  switch (topCenterType) {
									  case "image":%>

							  <div class="row">
								  <section class="logo-selector">
									  <input class="" id="topCenterData-image-string" data-id="<%= ESAPI.encoder().encodeForHTMLAttribute(topCenterData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(topCenterData)%>" name="topCenterData-image-string" hidden/>
									  <input class="header-upload" id="topCenterData-image" data-id="<%= Defaults.getInstance().getStoreContext() + ESAPI.encoder().encodeForHTMLAttribute(topCenterData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(topCenterData)%>" name="topCenterData-image" type="file" />
								  </section>
								  <input id="removeheaderImage" type="checkbox" name="removeheaderImage" class="hidden">
							  </div>
							  <%
										  break;
									  case "text": %>
							  <div class="col-sm-1 f-field"></div>
							  <div class="col-sm-10 f-field"><h5>Center Text</h5>
								  <input type="text" id="topCenterData-text" class="form-control"  name="topCenterData-text" placeholder="Content text"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(topCenterData)%>"></div>
							  <div class="col-sm-1 f-field"></div>
							  <% break;
								  default:
									  break;
							  }
								  }%>
						  </div>
						  <div class="row" id="topRightData">
							  <% if (topStyleRight != null && topStyleRight.has("data")) {
								  String topRightData = topStyleRight.getString("data");
								  String topBottomType = topStyleRight.getString("type");
								  switch (topBottomType) {
									  case "image":%>

							  <div class="row">
								  <section class="logo-selector">
									  <input class="" id="topRightData-image-string" data-id="<%= ESAPI.encoder().encodeForHTMLAttribute(topRightData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(topRightData)%>" name="topRightData-image-string" hidden/>
									  <input class="header-upload" id="topRightData-image" data-id="<%= Defaults.getInstance().getStoreContext() + ESAPI.encoder().encodeForHTMLAttribute(topRightData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(topRightData)%>" name="topRightData-image" type="file" />
								  </section>
								  <input id="removeheaderImage" type="checkbox" name="removeheaderImage" class="hidden">
							  </div>
							  <%
										  break;
									  case "text": %>
							  <div class="col-sm-1 f-field"></div>
							  <div class="col-sm-10 f-field"><h5>Right Text</h5>
								  <input type="text" id="topRightData-text" class="form-control" name="topRightData-text" placeholder="Content text"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(topRightData)%>"></div>
							  <div class="col-sm-1 f-field"></div>
							  <% break;
								  default:
									  break;
							  }
								  }%>
						  </div>
					  </div>
				  </div>
					  <%--HEADER CARDS END--%>

					  <%--FOOTER CARDS--%>
				  <div class="collapse-card">
					  <div class="collapse-card__heading">
						  <div class="collapse-card__title">
							  <i class="fa fa-align-justify fa-2x fa-fw"></i>
							  <span class="styleHeadingTitles">Page Footer</span>
						  </div>
					  </div>
					  <div class="collapse-card__body">
						  <div class="row">
							  <section style="padding-right:0px;padding-left: 60px">
								  <input class="table-checkbox" type="checkbox" name="bottomHeaderActive" value="show" <%=bottomHeaderActive?"checked":"" %> /> Show bottom footer all content pages
							  </section>
						  </div>
						  <h3 class="text-center">Footer Colours</h3>
						  <div class="row heading">
							  <div class="col-sm-3 text-left"></div>
							  <div class="col-sm-3 f-field text-center">Font Colour</div>
							  <div class="col-sm-3 f-field text-center">Background Colour</div>
							  <div class="col-sm-3 f-field text-left"></div>
						  </div>
						  <div class="row">
							  <div class="col-sm-3 f-field"></div>
							  <div class="col-sm-3 f-field text-center">
								  <input type="text" id="color-footer-font" class="form-control" name="footerColorFont" value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomHeaderFontColor) %>">
							  </div>
							  <div class="col-sm-3 f-field text-center">
								  <input type="text" id="color-footer-background" class="form-control" name="footerColorBackground" value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomHeaderBackground) %>">
							  </div>
							  <div class="col-sm-3 f-field">
							  </div>
						  </div>
					  		</br>
						  <h3 class="text-center">Footer Sections</h3>
						  <div class="row heading">
							  <div class="col-sm-1 text-left"></div>
							  <div class="col-sm-3 text-left">Left</div>
							  <div class="col-sm-3 f-field text-left">Center</div>
							  <div class="col-sm-3 f-field text-left">Right</div>
							  <div class="col-sm-1 text-left"></div>
						  </div>
						  <div class="row">
							  <div class="col-sm-1 f-field">
							  </div>
							  <div class="col-sm-3 f-field">
								  <select class="form-control" name="bottomStyleLeft" dataId="bottomLeftData" positionId="Left" id="bottomStyleLeft">
									  <% String bottomLeft = "";
										  if (bottomStyleLeft != null && bottomStyleLeft.has("option")) {
											  bottomLeft = bottomStyleLeft.getString("option");
										  }%>
									  <option value="none" <%=bottomLeft.equalsIgnoreCase("none") ?"selected":"" %>>Blank</option>
									  <option value="image" <%=bottomLeft.equalsIgnoreCase("image") ?"selected":"" %>>Logo</option>
									  <option value="text" <%=bottomLeft.equalsIgnoreCase("text") ?"selected":"" %>>Text</option>
									  <option value="page" <%=bottomLeft.equalsIgnoreCase("page") ?"selected":"" %>>Page Counter</option>
								  </select>
							  </div>
							  <div class="col-sm-3 f-field">
								  <select class="form-control" name="bottomStyleCenter" dataId="bottomCenterData" positionId="Center" id="bottomStyleCenter">
									  <% String bottomCenter = "";
										  if (bottomStyleCenter != null && bottomStyleCenter.has("option")) {
											  bottomCenter = bottomStyleCenter.getString("option");
										  }%>
									  <option value="none" <%=bottomCenter.equalsIgnoreCase("none") ?"selected":"" %>>Blank</option>
									  <option value="text" <%=bottomCenter.equalsIgnoreCase("text") ?"selected":"" %>>Text</option>
									  <option value="page" <%=bottomCenter.equalsIgnoreCase("page") ?"selected":"" %>>Page Counter</option>
								  </select>
							  </div>
							  <div class="col-sm-3 f-field">
								  <select class="form-control" name="bottomStyleRight" dataId="bottomRightData" positionId="Right" id="bottomStyleRight">
									  <% String bottomRight = "";
										  if (bottomStyleRight != null && bottomStyleRight.has("option")) {
											  bottomRight = bottomStyleRight.getString("option");
										  }%>
									  <option value="none" <%=bottomRight.equalsIgnoreCase("none") ?"selected":"" %>>Blank</option>
									  <option value="image" <%=bottomRight.equalsIgnoreCase("image") ?"selected":"" %>>Logo</option>
									  <option value="text" <%=bottomRight.equalsIgnoreCase("text") ?"selected":"" %>>Text</option>
									  <option value="page" <%=bottomRight.equalsIgnoreCase("page") ?"selected":"" %>>Page Counter</option>
								  </select>
							  </div>
							  <div class="col-sm-3 f-field">
							  </div>
						  </div>

						  <div class="row" id="bottomLeftData">
							  <%  if (bottomStyleLeft != null && bottomStyleLeft.has("data")) {
								  String bottomLeftData = bottomStyleLeft.getString("data");
								  String bottomLeftType = bottomStyleLeft.getString("type");
								  switch (bottomLeftType) {
									  case "image":%>
							  <div class="row">
								  <section class="logo-selector">
									  <input class="" id="bottomLeftData-image-string" data-id="<%= ESAPI.encoder().encodeForHTMLAttribute(bottomLeftData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomLeftData)%>" name="bottomLeftData-image-string" hidden/>
									  <input class="header-upload" id="bottomLeftData-image" data-id="<%= Defaults.getInstance().getStoreContext() + ESAPI.encoder().encodeForHTMLAttribute(bottomLeftData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomLeftData)%>"  name="bottomLeftData-image" type="file" />
								  </section>
								  <input id="removeheaderImage" type="checkbox" name="removeheaderImage" class="hidden">
							  </div>
							  <%
										  break;
									  case "text": %>
							  <div class="col-sm-1 f-field"></div>
							  <div class="col-sm-10 f-field"><h5>Left Text</h5>
								  <input type="text" id="bottomLeftData-text" class="form-control" name="bottomLeftData-text" placeholder="Content text"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomLeftData)%>"></div>
							  <div class="col-sm-1 f-field"></div>
							  <% break;
								  default:
									  break;
							  }
								  }%>
						  </div>
						  <div class="row" id="bottomCenterData">
							  <% if (bottomStyleCenter != null && bottomStyleCenter.has("data")) {
									  String bottomCenterData = bottomStyleCenter.getString("data");
								  String bottomCenterType = bottomStyleCenter.getString("type");
								  switch (bottomCenterType) {
									  case "image":%>

							  <div class="row">
								  <section class="logo-selector">
									  <input class="" id="bottomCenterData-image-string" data-id="<%= ESAPI.encoder().encodeForHTMLAttribute(bottomCenterData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomCenterData)%>" name="bottomCenterData-image-string" hidden/>
									  <input class="header-upload" id="bottomCenterData-image" data-id="<%= Defaults.getInstance().getStoreContext() + ESAPI.encoder().encodeForHTMLAttribute(bottomCenterData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomCenterData)%>" name="bottomCenterData-image" type="file" />
								  </section>
								  <input id="removeheaderImage" type="checkbox" name="removeheaderImage" class="hidden">
							  </div>
							  <%
										  break;
									  case "text": %>
							  <div class="col-sm-1 f-field"></div>
							  <div class="col-sm-10 f-field"><h5>Center Text</h5>
								  <input type="text" id="bottomCenterData-text" class="form-control" name="bottomCenterData-text" placeholder="Content text"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomCenterData)%>"></div>
							  <div class="col-sm-1 f-field"></div>
							  <% break;
								  default:
									  break;
							  }
								  }%>
						  </div>
						  <div class="row" id="bottomRightData">
							  <% if (bottomStyleRight != null && bottomStyleRight.has("data")) {
									  String bottomRightData = bottomStyleRight.getString("data");
								  String bottomRightType = bottomStyleRight.getString("type");
								  switch (bottomRightType) {
									  case "image":%>

							  <div class="row">
								  <section class="logo-selector">
									  <input class="" id="bottomRightData-image-string" data-id="<%= ESAPI.encoder().encodeForHTMLAttribute(bottomRightData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomRightData)%>" name="bottomRightData-image-string" hidden/>
									  <input class="header-upload" id="bottomRightData-image" data-id="<%= Defaults.getInstance().getStoreContext() + ESAPI.encoder().encodeForHTMLAttribute(bottomRightData) %>" value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomRightData)%>" name="bottomRightData-image" type="file" />
								  </section>
								  <input id="removeheaderImage" type="checkbox" name="removeheaderImage" class="hidden">
							  </div>
							  <%
										  break;
									  case "text": %>
							  <div class="col-sm-1 f-field"></div>
							  <div class="col-sm-10 f-field"><h5>Right Text</h5>
								  <input type="text" id="bottomRightData-text" class="form-control" name="bottomRightData-text" placeholder="Content text"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(bottomRightData)%>"></div>
							  <div class="col-sm-1 f-field"></div>
							  <% break;
								  default:
									  break;
							  }
								  }%>
						  </div>
					  </div>
				  </div>
					  <%--FOOTER CARDS END--%>
		  </div>
		  </div>

		  <div role="tabpanel" class="tab-pane" id="proposal-colours">

			  <div class="row">
				  <div class="col col-6">
					  <h2 class="text-center">Palettes</h2>
					  <div class="row" id="edit-palette">
						  <section class="col col-3">
							  <label class="input"> <input id="palette-1"
														   class="form-control color {hash:true}" name="palette1"
														   placeholder="Hex Colour"
														   value="<%=ESAPI.encoder().encodeForHTMLAttribute(
			company.getString(uada.C1_PALETTE1))%>">
							  </label>
						  </section>
						  <section class="col col-3">
							  <label class="input"> <input id="palette-2"
														   class="form-control color {hash:true}" name="palette2"
														   placeholder="Hex Colour"
														   value="<%=ESAPI.encoder().encodeForHTMLAttribute(
			company.getString(uada.C1_PALETTE2))%>">
							  </label>
						  </section>
						  <section class="col col-3">
							  <label class="input"> <input id="palette-3"
														   class="form-control color {hash:true}" name="palette3"
														   placeholder="Hex Colour"
														   value="<%=ESAPI.encoder().encodeForHTMLAttribute(
			company.getString(UserAccountDataAccess.C1_PALETTE3))%>">
							  </label>
						  </section>
						  <section class="col col-3">
							  <label class="input"> <input id="palette-4"
														   class="form-control color {hash:true}" name="palette4"
														   placeholder="Hex Colour"
														   value="<%=ESAPI.encoder().encodeForHTMLAttribute(
			company.getString(UserAccountDataAccess.C1_PALETTE4))%>">
							  </label>
						  </section>
					  </div>
				  </div>
				  <div class="col col-6">
					  <h2 class="text-center">Background Colour</h2>
					  <div class="row">
						  <section class="text-center background-selector">
							  <label class="input"> <input id="background-color"
														   class="form-control color {hash:true}"
														   name="backgroundColour" placeholder="Hex Colour"
														   value="<%=ESAPI.encoder().encodeForHTMLAttribute(
			company.getString(uada.C1_BACKGROUND_COLOUR))%>">
							  </label>
						  </section>
					  </div>
				  </div>
			  </div>
			  <div class="h3 hidden">Section Background</div>
			  <div class="row hidden">
				  <div class="col col-12">
					  <section class="background-selector">
						  <label class="input">
							  <input id="background-color" class="form-control color {hash:true}" name="sectionBackgroundColour" placeholder="Hex Colour"
									 value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_SECTION_BACKGROUND_COLOUR)) %>">
						  </label>
					  </section>
				  </div>
			  </div>
		  </div>
	    
	    <div role="tabpanel" class="tab-pane" id="table-styles">
				
	        <h2 class="text-center">Default Table Styles</h2>
	        <div class="row" id="edit-table-style">
	         <%
	         for(int i=0;i<6;i++){
	         %>
	         <section class="col col-4">
			    <label>
			    <input class="table-radio" type="radio" name="tableStyle" value="tableStyle<%= i %>" <%=StringUtils.isBlank(tableStyle) && i==0 || tableStyle.equalsIgnoreCase("tableStyle"+i)?"checked":"" %> /> Table Style <%= i + 1 %>
			    <div class="tableStyle" style="color:<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_BODY_COLOR)) %>">
			    	<div class="sectionDiv">
			    		<div class="sectionContent">
				    		<div class="editorArea"> 
				    			<table class="tableStyle<%=i%>"> <tbody> <tr class="titleRow" style="color:<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_PALETTE1)) %>"> <td>City</td> <td>Country</td> </tr> <tr> <td>Sydney</td> <td>Australia</td> </tr> <tr> <td>New York</td> <td>USA</td> </tr> <tr> <td>Rome</td> <td>Italy</td> </tr> <tr> <td>Berlin</td> <td>Germany</td> </tr> </tbody> </table>
			    			</div>
		    			</div>
		   			</div>
	   			</div>
			</label>
			</section>
	        <%
	        }
	        %>
	        </div>
	
		</div>
		
	    <div role="tabpanel" class="tab-pane" id="tab-styles">
    	 	<h2 style="position:relative;" class="text-center">Proposal Tab Styles
		    	 <div style="position:absolute; right:10px; bottom:4px; font-size:14px; font-weight:normal;">
					<span>Show Tabs</span>
					<input type="checkbox" name="showTabs" <%=showTabs?"checked='checked'":""%> value="show" class="tab-switch" data-id="#edit-tab-style"/>
				</div>
			</h2>
	    	 <div class="row <%=showTabs?"":"tab-content-disabled"%>" id="edit-tab-style">
				<section class="col col-6">
					<label> <input class="table-radio" type="radio" 
						name="tabStyle" value="tabDefault" <%=(tabStyle.equalsIgnoreCase("tabDefault") || tabStyle.equalsIgnoreCase(""))?"checked":"" %>/> Tab Style 1
					</label>
					<div class="sectionContent">
						<div id="nav-tab-style">
							<ul class="tabDefault">
								<li><a> <span>1</span> Section One
								</a></li>
								<li><a> <span>2</span> Section Two
								</a></li>
								<li><a> <span>3</span> Section Three
								</a></li>
							</ul>
						</div>
					</div>
				</section>
				<section class="col col-6">
					<label> <input class="table-radio" type="radio"
						name="tabStyle" value="tabStyle1" <%=tabStyle.equalsIgnoreCase("tabStyle1")?"checked":"" %>/> Tab Style 2
					</label>
					<div class="sectionContent">
						<div id="nav-tab-style">
							<ul class="tabStyle1">
								<li class="ripple"><a> <span>1</span> Section One
								</a></li>
								<li class="ripple"><a> <span>2</span> Section Two
								</a></li>
								<li class="ripple"><a> <span>3</span> Section Three
								</a></li>
							</ul>
						</div>
					</div>
				</section>
			</div>
	    </div>

		<div role="tabpanel" class="tab-pane" id="coverpage">
			<h2 style="position:relative;"  class="text-center">Cover Page
				<div style="position:absolute; right:10px; bottom:4px; font-size:14px; font-weight:normal;">
					<span>Show Cover Page</span>
					<input type="checkbox" name="showCoverpage" <%=showCoverpage?"checked='checked'":""%> value="show" class="tab-switch" data-id="#edit-show-coverpage"/>
				</div>
			</h2>
			<div class="row <%=showCoverpage?"":"tab-content-disabled"%>" id="edit-show-coverpage">
			<section class="col col-6">
				<section class="col col-12" style="padding-right:0px;padding-left: 0px">
					<input class="table-checkbox" type="checkbox" name="coverPageProposalInfo" value="show" <%=hideCoverProposalInfo?"checked":"" %> /> Hide Proposal Title & Subtitle details
				</section>
				<section class="col col-12" style="padding-right:0px;padding-left: 0px">
					<input class="table-checkbox" type="checkbox" name="coverPageSenderInfo" value="show" <%=hideCoverSenderInfo?"checked":"" %> /> Hide Sender & Recipient details
				</section>
				<section class="col col-12" style="padding-right:0px;padding-left: 0px">
					<input class="table-checkbox" type="checkbox" name="coverPageBoxBackground" value="show" <%=hideCoverBoxBackground?"checked":"" %> /> Make content areas transparent
				</section>
			</section>
			<section class="col col-6" style="margin-top: 2em;">
				<section class="col col-12 cover-field">
					<%
						String color = company.getString(uada.C1_PROPOSAL_COVERPAGE_FONT_COLOR).equals("")?"#000000":company.getString(uada.C1_PROPOSAL_COVERPAGE_FONT_COLOR);
					%>
	      			<input type="text" id="cover-color" class="form-control" name="coverPageColor"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(color) %>"> Cover Page font color
				</section>
			</section>
				<div class="row" style="margin-top: 2em;">
					<section class="col col-12" style="width: 100%;">
				<div class="f-group">
					<h2 class="text-center">Cover Page Fonts</h2>
					<div class="row heading">
						<div class="col-sm-3 text-left"></div>
						<div class="col-sm-4 f-field text-left">Font</div>
						<div class="col-sm-2 f-field text-left">Size</div>
					</div>
					<div class="row">
						<div class="col-sm-3 f-label"><span>Company</span></div>
						<div class="col-sm-4 f-field">
							<select class="form-control" name="fontFamilyCoverCompany">
								<% for (String font : fonts){
									String selectedClause = font.equals(company.getString(uada.C1_COVER_COMPANY_FONT))?"selected":"";
								%>
								<option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>" <%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
								<% } %>
							</select>
						</div>
						<div class="col-sm-2 f-field">
							<input type="number" id="font-size-body" class="form-control" name="fontSizeCoverCompany" placeholder="Font Size"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_COVER_COMPANY_SIZE)) %>">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 f-label">
							<span>Title</span>
						</div>
						<div class="col-sm-4 f-field">
							<select class="form-control" name="fontFamilyCoverTitle">
								<% for (String font : fonts){
									String selectedClause = font.equals(company.getString(uada.C1_COVER_TITLE_FONT))?"selected":"";
								%>
								<option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>" <%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
								<% } %>
							</select>
						</div>
						<div class="col-sm-2 f-field">
							<input type="number" id="font-size-body" class="form-control" name="fontSizeCoverTitle" placeholder="Font Size"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_COVER_TITLE_SIZE)) %>">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 f-label"><span>Extra Information</span></div>
						<div class="col-sm-4 f-field">
							<select class="form-control" name="fontFamilyCoverExtra">
								<% for (String font : fonts){
									String selectedClause = font.equals(company.getString(uada.C1_COVER_EXTRA_FONT))?"selected":"";
								%>
								<option value="<%=ESAPI.encoder().encodeForHTMLAttribute(font) %>" <%=selectedClause %>><%=ESAPI.encoder().encodeForHTML(font) %></option>
								<% } %>
							</select>
						</div>
						<div class="col-sm-2 f-field">
							<input type="number" id="font-size-body" class="form-control" name="fontSizeCoverExtra" placeholder="Font Size"  value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_COVER_EXTRA_SIZE)) %>">
						</div>
					</div>
				</div>
			</section>
				</div>
			</div>
		</div>

		  <div role="tabpanel" class="tab-pane" id="companylogo">
			  <h2 class="text-center">Logo</h2>
			  <div class="row">
				  <section class="logo-selector">
					  <input id="logo-upload" name="logo" type="file" />
				  </section>
				  <input id="removeLogo" type="checkbox" name="removeLogo" class="hidden">
			  </div>
		  </div>

		  <div role="tabpanel" class="tab-pane" id="contentSettings">
			  <h2 class="text-center">Proposal Content Settings</h2>
			  <%--<div class="row">--%>
				  <%--<section class="col col-6">--%>
					  <%--<section class="col col-12" style="padding-right:0px">--%>
						  <%--<input class="table-checkbox" type="checkbox" name="appendPDFSetting" value="show" <%=hideCoverProposalInfo?"checked":"" %> /> Append PDFs when attached--%>
					  <%--</section>--%>
				  <%--</section>--%>
			  <%--</div>--%>
			  <div class="row padding-style">
				  <h2>Padding</h2>
				  <div class="padding-wrapper">
					  <div class="padding-flex-top">
					  	<span class="unit-px">
					  		<input type="number" id="content-top-padding" class="form-control" name="contentTopPadding" placeholder="-" min="0" max="100" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_CONTENT_PADDING_TOP)) %>">
					  	</span>
					  </div>
					  <div class="padding-flex-middle">
					  	<span class="unit-px">
					  		<input type="number" id="content-left-padding" class="form-control" name="contentLeftPadding" placeholder="-" min="0" max="100" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_CONTENT_PADDING_LEFT)) %>">
					  	</span>
					  <div class="demo-area">
								<span class="demo-title">Section Title</span>
								<div class="padding-demo">
									<div class="padding-box">Content</div>
								</div>
						</div>
					  	<span class="unit-px">
					  		<input type="number" id="content-right-padding" class="form-control" name="contentRightPadding" placeholder="-" min="0" max="100" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_CONTENT_PADDING_RIGHT)) %>">
					  </span>
					  </div>
					  <div class="padding-flex-bottom">
						<span class="unit-px">
					  		<input type="number" id="content-bottom-padding" class="form-control" name="contentBottomPadding" placeholder="-" min="0" max="100" value="<%=ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_CONTENT_PADDING_BOTTOM)) %>">
					  </span>
					  </div>
				  </div>
			  </div>
		  </div>
	  </div>

</div>

    </div>
    <div class="modal-footer line top">
      <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
        Close
      </button>
      <button type="submit" class="btn btn-success">
        <i class="fa fa-save"></i> Save
      </button>
    </div>
  </form>
</div>
<script src='/application/assets/plugins/spectrum/spectrum.js'></script>
<link rel='stylesheet' href='/application/assets/plugins/spectrum/spectrum.css' />
<script src='/application/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js'></script>
<script src="/application/assets/plugins/dropzone/downloads/dropzone.min.js"></script>
<link rel="stylesheet" href="/application/assets/plugins/bootstrap-fileinput/fileinput.css" />
<script src="/application/assets/plugins/bootstrap-fileinput/plugins/canvas-to-blob.js"></script>
<script src="/application/assets/plugins/bootstrap-fileinput/fileinput.js"></script>
<script src="/application/assets/plugins/bootstrap-fileinput/themes/fa/fa.js"></script>
<script src="/application/assets/plugins/jQuery-slimScroll-master/jquery.slimscroll.min.js"></script>
<script src="/application/js/form-validation/modal-edit-styles.js"></script>
<script>
	$(function () {
		$(".collapse-card").paperCollapse();
	});
  
  $("#logo-upload").fileinput({
    <% if(!company.getString(uada.C1_LOGO).isEmpty()) { %>
    initialPreview: [
            '<img src="<%= Defaults.getInstance().getStoreContext() + ESAPI.encoder().encodeForHTMLAttribute(company.getString(uada.C1_LOGO)) %>" class="file-preview-image" alt="Logo" title="Logo">',
        ],
        <% } %>
    showZoom: false,
    overwriteInitial: true,
    browseClass: "btn btn-primary",
    theme:"fa",
    showClose: false,
    showCaption: false,
    showUpload: false
    
  }).on('change', function(event) {
	$("#removeLogo").prop('checked',false); 
  }).on('filecleared', function(event) {
	$("#removeLogo").prop('checked',true);   
  });


</script>

</compress:html>