<%@page import="au.corporateinteractive.qcloud.proposalbuilder.control.UserController"%>
<%@page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager"%>
<%@page import="java.util.Arrays"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess"%>
<%@page import="java.util.List"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="java.util.Hashtable"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress"%>
<compress:html enabled="true" compressJavaScript="true" compressCss="true">
<div id="">
	<%
		UserAccountDataAccess uada = new UserAccountDataAccess();
	
		TUserRolesDataAccess urda = new TUserRolesDataAccess();
		
		String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
		String companyId = uada.getUserCompanyForUserId(userId).getId();

		Hashtable<String, ElementData> companyUsers = uada.getCompanyUsers(companyId);
		String[] accessRights = uada.getUserAccessRights(userId);
		List<CategoryData> teams = uada.getCompanyTeams(companyId);
		List<ElementData> roles = urda.getCompanyRoles(companyId);
		String tab = request.getParameter("tabModule");
	%>
	<form class="form-" action="" method="POST">
		<div class="modal-header">
			<div class="titleBar">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fal fa-times-circle"></i></button>
			</div>
			<h2 class="modal-title">Users</h2>
		</div>
		<div class="modal-body">
			<div class="container contentContainer">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="<%= StringUtils.isBlank(tab) || tab.equals("User") ? "active" : "" %>"><a href="#users_tab" class="userButton" aria-controls="users" role="tab" data-module="User" data-toggle="tab">Users</a></li>
					<li role="presentation" class="<%= StringUtils.isNotBlank(tab) && tab.equals("Team") ? "active" : "" %>"><a href="#teams_tab" class="teamsButton" aria-controls="profile" role="tab" data-module="Team" data-toggle="tab">Teams</a></li>
					<li role="presentation" class="<%= StringUtils.isNotBlank(tab) && tab.equals("Role") ? "active" : "" %>"><a href="#roles_tab" class="rolesButton" aria-controls="messages" role="tab" data-module="Role" data-toggle="tab">Roles</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane <%= StringUtils.isBlank(tab) || tab.equals("User") ? "active" : "" %>" id="users_tab">
						<table class="table table-bordered table-hover table-full-width" id="table-users" data-module="User" width="100%">
							<thead>
								<tr>
									<th>Name</th>
									<th>Team</th>
									<th>Roles</th>
									<th width="140">Actions</th>
								</tr>
							</thead>
							<tbody>
								<%
									for (ElementData user : companyUsers.values())
										{
								%>
								<tr>
									<td><%=user.get(uada.E_NAME) + " " + user.get(uada.E_SURNAME)%> <%=(user.getId().equals(userId) ? "(You)" : "")%></td>
									<td><%=((CategoryData) user.get("team")).getName()%></td>
									<td><%
										List<ElementData> userRoles= urda.getRolesForUserId(user.getId());
										
										for(int i=0;i<userRoles.size();i++){
										%>
										<%=userRoles.get(i).getName()+(i<userRoles.size()-1?",":"") %>
										<% } %>
									</td>									
									<td>
										<div class="btn-group" data-id="<%= user.getId() %>">
											<a class="btn btn-secondary edit-item tooltips" href="#">
												Edit
											</a>
											<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" <%=(user.getId().equals(userId) ? "disabled" : "")%>>
												<span class="caret"></span>
												<span class="sr-only">Toggle Dropdown</span>
											</button>
											<ul class="dropdown-menu">
												<li>
													<a class="export-item">
														<i class="fa fa-arrow-down"></i>
														<span> Export</span>
													</a>
												</li>
												<% if(user.getString(uada.E_SURNAME).contains("(Pending Verification")) { %>
												<li>
													<a class="resend-activation-item">
														<i class="fa fa-envelope"></i>
														<span> Resend Activation Email</span>
													</a>
												</li>
												<% } %>
												<% if(!user.getId().equals(userId)) { %>
												<li>
													<a class="delete-item">
														<i class="fa fa-times"></i>
														<span> Delete</span>
													</a>
												</li>
												<% } %>
											</ul>
										</div>
									</td>
								</tr>
								<%
									}
								%>
							</tbody>
						</table>
					</div>
					<div role="tabpanel" class="tab-pane <%= StringUtils.isNotBlank(tab) && tab.equals("Team") ? "active" : "" %>" id="teams_tab">
						<table class="table table-bordered table-hover table-full-width" id="table-teams" data-module="Team" width="100%">
							<thead>
								<tr>
									<th>Team</th>
									<th width="140">Actions</th>
								</tr>
							</thead>
							<tbody>
								<%
									for (CategoryData team : teams)
										{
								%>
								<tr>
									<td><%=team.getName()%></td>
									<td>
										<div class="btn-group" data-id="<%= team.getId() %>" >
											<a class="btn btn-secondary edit-item tooltips" href="#">
												 Edit
											</a> 
											<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<span class="caret"></span>
												<span class="sr-only">Toggle Dropdown</span>
											</button>
											<ul class="dropdown-menu">
												<li>
													<a class="delete-item">
														<i class="fa fa-times"></i>
														<span> Delete</span>
													</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
								<%
									}
								%>
							</tbody>
						</table>
					</div>
					<div role="tabpanel" class="tab-pane <%= StringUtils.isNotBlank(tab) && tab.equals("Role") ? "active" : "" %>" id="roles_tab">
						<table class="table table-bordered table-hover table-full-width" id="table-roles" data-module="Role" width="100%">
							<thead>
								<tr>
									<th>Role</th>
									<th width="140">Actions</th>
								</tr>
							</thead>
							<tbody>
								<%
									for (ElementData role : roles)
										{
								%>
								<tr>
									<td><%=role.getName()%></td>
									<td>
										<div class="btn-group" data-id="<%= role.getId() %>">
											<a class="btn btn-secondary edit-item tooltips" href="#">
												Edit
											</a> 
											<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<span class="caret"></span>
												<span class="sr-only">Toggle Dropdown</span>
											</button>
											<ul class="dropdown-menu">
												<li>
													<a class="delete-item">
														<i class="fa fa-times"></i>
														<span> Delete</span>
													</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
								<%
									}
								%>
							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>
		<div class="modal-footer">
		    <% if(!UserController.isTrialPeriodOver(session)[0]){ %>
			<button type="button" id="addItem" class="btn btn-info pull-left">
				<i class="fa fa-plus"></i>
				<span class="addItemText">Add User</span>
			</button>
			<button type="button" id="exportUsers" class="btn btn-warning pull-left">
				<i class="fa fa-arrow-down"></i>
				<span class="exportUsersText">Export Users</span>
			</button>
			<% } %>
			<button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
				<i class="fa fa-times"></i> Close
			</button>
			<!-- 
			<button type="submit" class="btn btn-success">
				<i class="fa fa-save"></i> Save
			</button>
			-->
		</div>
	</form>
	<script src="/application/js/dashboardUsersManagement.js"></script>
</div>
</compress:html>