<div id="contacts-add-edit-modal" class="modal fade add-edit-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="display: none;">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">Modal title</h4>
		</div>
		<form id="add-edit-contacts-form" method="post" enctype="multipart/form-data">
			<div class="modal-body">
				<input type="hidden" name="elementId">
				<div class="form-group">
					<label for="assetName">Contact Email</label>
					<input type="text" class="form-control" name="assetName">
				</div>
				<div class="form-group">
					<label for="firstName">First Name</label>
					<input type="text" class="form-control" name="firstName">
				</div>
				<div class="form-group">
					<label for="lastName">Last Name</label>
					<input type="text" class="form-control" name="lastName">
				</div>
				<div class="form-group">
					<label for="companyName">Company</label>
					<input type="text" class="form-control" name="companyName">
				</div>
				<div class="form-group">
					<label for="address">Address</label>
					<input type="text" class="form-control" id="addressBook" name="address">
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
			</div>
		</form>
	</div><!-- /.modal-content -->
</div>