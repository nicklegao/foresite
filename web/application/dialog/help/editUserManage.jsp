<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
<div id="help-managed">
    <div class="modal-header">
      <div class="titleBar">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
      </div>
      <h2 class="modal-title">What is the Manages function?</h2>
    </div>
    <div class="modal-body">
		<p>The feature allows you to tell QuoteCloud that this User is the manager of other Sales Consultants, and is allowed to see the proposals of these Sales Consultants on his/her Sales Proposal Dashboard (i.e. the Dashboard you see when you first login).</p>  
		<p>To set up simply add each Sales Consultant from the drop down list to assign them to this User.</p>
		
		<p>Special Note: If you allocate a Sales Consultant that is also setup to manage other Sales Consultants already, this User you are editing now will automatically inherit those Sales Consultants onto their Dashboard also, i.e. you can create a hierarchal structure.</p>
		
		<p>e.g.</p>
		
		<p>In the example of users below:</p>
		
		<p>Australian Country Manager, manages:</p>
		<ul>
			<li>Sales Consultant 1</li>
			<li>Sales Consultant 2</li>
			<li>Sales Consultant 3</li>
		</ul>
		<p>New Zealand Country Manager, manages:</p>
		<ul>
			<li>Sales Consultant 4</li>
		</ul>
		<p>UK Country Manager, manages: </p>
		<ul>
			<li>Sales Consultant 5</li>
		</ul>
			
		<p>If you wanted to create an Asia Pacific Regional Manager to only see Sales Proposal in Australia and New Zealand, simply add the users, Australian Country Manager and  New Zealand Country Manager, and QuoteCloud will automatically allocate the Sales Consultants these two users are setup to manage.</p>
    
    </div>
    <div class="modal-footer">
      <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
        <i class="fa fa-times"></i> Close
      </button>
    </div>
</div>
</compress:html>