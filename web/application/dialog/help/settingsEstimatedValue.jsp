<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
<div id="help-managed">
    <div class="modal-header">
      <div class="titleBar">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
      </div>
      <h2 class="modal-title">What is the Estimated Value Discounting Function?</h2>
    </div>
    <div class="modal-body" style="font-size: 12px">
		<p>The Estimated Value Discounting function allows a sales consultant to apply a higher level of discount than would be normally triggered by the total value of the price line items in a Sales Quotation.  This is commonly used when a Sales Consultant is created more than one Sales Quotation for customer for a given project / purchase order.  The total of all these Sales Quotations would should trigger a discount for the customer, but individually they won't.</p>
		
		<p>By allowing the Sales Consultant to tell QuoteCloud that the Estimated Value for all the Sales Quotations would be enough to allow the discount to trigger, QuoteCloud will automatically apply the correct discounts to each Sales Proposal.</p>
		
		<p>For example, the discounts available in the Pricing Discounts / Threshold function are:</p>
		
		<p>"Discount Band 1" is 5% discount for sales quotations above a value of $10,000</p>
		
		<p>As a Sales Consultant, I create a Sales Quotation of the value of only $4,000, but I know the sale is worth more to me in the future because I am going to create multiple sales quotations for this customer.  I want to trigger the "Discount Band 1" discount in this proposal, even though it is only valued at $4,000.  To do this, when I create the proposal I can enter the Estimated Discount Value to be $10,000 (or above).  This will then invoke the 5% discount on this proposal.</p>
		
		<p>How does QuoteCloud automatically work out the Sales Quotation discounting?</p>
		
		<ol>
		<li>Sums up the total value of the proposal before discounting (i.e. Actual Value)</li>
		<li>Apply automatic discounting by:
			<ul>
				<li>if the "Estimated Value Discounting Function" is being used:<br/> based on Estimated Value Discount value, automatically apply the discount percentage to the Actual Value</li>
				<li>Otherwise:<br/> if based automatically apply any discount percentage available in the "Pricing Discounts / Threshold" to the Actual Value.</li>
			</ul>
		</li>
		<li>Allow the Sales Consultant to apply manual discounts to line items or the entire Sales Quotation.</li>
		</ol>
    </div>
    <div class="modal-footer">
      <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
        <i class="fa fa-times"></i> Close
      </button>
    </div>
</div>
</compress:html>