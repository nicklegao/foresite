<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.SecurityKeyService"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="java.util.Map"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Set"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.List"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess" %>
<%
TravelDocsDataAccess pda = new TravelDocsDataAccess();

UserAccountDataAccess uada = new UserAccountDataAccess();
String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
String userEmail = (String) session.getAttribute(Constants.PORTAL_USERNAME_ATTR);
CategoryData company = uada.getUserCompanyForUserId(userId);
SecurityKeyService sks = new SecurityKeyService();

%>
      <div class="container contentContainer">
        <% List<Hashtable<String, String>> templates = pda.getSampleTemplates();
        if(templates.size()>0){
        %>
        <p>We have some great proposal templates to help get you started, tick the templates you would like us to setup in your account.</p>
        
      <form class="form-choose-sample-templates" method="POST">  
      <table class="table table-bordered table-hover table-full-width" id="table-sample-templates" data-module="User" width="100%">
        <thead>
          <tr>
            <th>Select</th>
            <th>Category</th>
            <th>Template</th>
            <th width="100">Action</th>
          </tr>
        </thead>
        <tbody>
        <%
        for (Hashtable<String, String> ht : templates)
            {
              String proposalId = ht.get(TravelDocsDataAccess.E_ID);
              String vKey = sks.getViewKey(company.getId(), Integer.parseInt(proposalId), ht.get(TravelDocsDataAccess.E_KEY));
          %>
            <tr class="templates-item" data-id="<%=proposalId %>">
              <td class="template-items">
                <input type="checkbox" name="templateId" value="<%=ht.get(TravelDocsDataAccess.E_ID) %>">
              </td>
              <td class="template-items">
                <span class="templates-item-from"><%=ESAPI.encoder().encodeForHTML(ht.get("ATTR_categoryName")) %></span>
              </td>
              <td class="template-items">
                <span class="templates-item-from"><%=ESAPI.encoder().encodeForHTML(ht.get(TravelDocsDataAccess.E_TEMPLATE_TITLE)) %></span>
                <span class="messages-item-subject"><%=ESAPI.encoder().encodeForHTML(ht.get(TravelDocsDataAccess.E_TEMPLATE_DESCRIPTION)) %></span>
              </td>
              <td class="template-actions">
                <a data-original-title="Preview"
                href="/view?proposal=<%= proposalId %>&v-key=<%=vKey %>"
                data-placement="top"
                target="view-<%= proposalId %>"
                class="btn btn-default btn-xs tooltips"
                data-id="<%= proposalId %>"><i class="fa fa-eye"></i> Preview</a>
              </td>
            </tr>
          <% } %>
        </tbody>
      </table>
      </form>
      <% }else{ %>
      <div>
        <span>We don't have any sample templates yet. Please try later in the "Manage Templates" menu.</span>
      </div>
      <% } %>
    </div>
  
  <script>
  $(function(){
    $('[id="table-sample-templates"] input[type="checkbox"]', "#<%= request.getParameter("context")%>").iCheck({
	  checkboxClass : 'icheckbox_square-orange'
	});
    
    
    var domHead = "<'navbar' <'navbar-form navbar-left'<'form-group' l>> <'navbar-form navbar-right'<'form-group' f>> >";
  	var domBody = "t";
  	var domFooter = "<'navbar' <'navbar-form navbar-left' i> <'navbar-form navbar-right'<'form-group' p>> <'navbar-form navbar-right' <'#actionsHolder'>> >";


  	if(typeof $dataTable != 'undefined') {
  	  $dataTable.destroy();
  	}
  	$dataTable = $('[id="table-sample-templates"]', "#<%= request.getParameter("context")%>").DataTable({
  		"oLanguage" : {
  			"sLengthMenu" : "_MENU_",
  			"sSearch" : "",
  			"oPaginate" : {
  				"sPrevious" : "",
  				"sNext" : ""
  			},
  		},
  		"columns" : [ {
  			"orderable" : false
  		}, null,null, {
  			"orderable" : false
  		} ],
  		"sDom" : domHead + domBody + domFooter
  	});
  	
  	$('[id="table-sample-templates_wrapper"] .dataTables_filter', "#<%= request.getParameter("context")%>").prepend('<label class="search-label"><span class="search-icon fa fa-search"></span></label>');
  	$('[id="table-sample-templates_wrapper"] .dataTables_filter input', "#<%= request.getParameter("context")%>").addClass("search-box form-control").attr("placeholder", "Search...");

  	$('[id="table-sample-templates_wrapper"] .dataTables_length select', "#<%= request.getParameter("context")%>").addClass("form-control");
  	$('[id="table-sample-templates_wrapper"] .dataTables_length', "#<%= request.getParameter("context")%>").closest(".form-group").before($("<span>").text('Show ')).after($("<span>").text(' templates '));
  });
  
  
  
  function chooseSampleTemplates($form, callback){
    startLoadingForeground();
	var ids = [];
	$('[type="checkbox"]:checked', $dataTable.rows().nodes()).each(function(){
	  ids.push(this.value);
	})
    $.ajax({
      url : '/api/updateFirstLoginTemplates',
      data : {ids: ids.join(",")},
      success : function(data)
      {
        stopLoadingForeground();
        if (data)
        {
          callback & callback();
        }
        else
        {
          swal({
            title : "Error",
            text : "It wasn't possible to change the settings. Contact your Qcloud administrator and make sure you have permission to manage settings.",
            type : "error"
          });
        }
      },
      error : function(data)
      {
        stopLoadingForeground();
        alert("Unable to update your settings. Please try again later.");
      }
    });
  }
  </script>
