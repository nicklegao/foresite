<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.SecurityKeyService"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="java.util.Map"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Set"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Vector"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess"%>
<%
TravelDocsDataAccess pda = new TravelDocsDataAccess();

UserAccountDataAccess uada = new UserAccountDataAccess();
String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
CategoryData company = uada.getUserCompanyForUserId(userId);
SecurityKeyService sks = new SecurityKeyService();

%>
<div id="get-sample-template">
  	<div class="modal-header">
      <div class="titleBar">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        	<i class="fal fa-times-circle"></i>
        </button>
      </div>
      <h2 class="modal-title">Choose Sample Templates</h2>
    </div>
    <div class="modal-body">
   	  <jsp:include page="getSampleTemplateList.jsp">
        <jsp:param value="get-sample-template" name="context"/>
      </jsp:include>	
    </div>
    <div class="modal-footer">
      <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
        <i class="fa fa-times"></i> Close
      </button>
      <button type="button" id="saveSampleTemplate" class="btn btn-success">
        <i class="fa fa-download"></i> Save
      </button>
    </div>
</div>
  
<script>
$(function(){
  $('#saveSampleTemplate').click(function(){
    chooseSampleTemplates($('#get-sample-template form'), function(){
      $('#get-sample-template-modal').modal('hide');
      $('#template-management-modal').load('/application/dialog/templateManagement.jsp');
    });
  });
});
</script>