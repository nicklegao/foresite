<%@page
	import="au.net.webdirector.common.datalayer.base.db.entity.MultiOptions"%>
<%@page import="java.util.List"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="java.util.Hashtable"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor"
	prefix="compress"%><compress:html enabled="true"
	compressJavaScript="true" compressCss="true">

	<link rel="stylesheet" href="/application/assets/plugins/select2/select2.css" />
<div id="edit-user">
	<%
		UserAccountDataAccess uada = new UserAccountDataAccess();
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			String companyId = uada.getUserCompanyForUserId(userId).getId();
			String id = request.getParameter("id");
			String action = request.getParameter("action");

			String tab = request.getParameter("tabModule");

			//market app access
			boolean travelport = false;
			boolean approvalUsers = false;

			TUserRolesDataAccess urda = new TUserRolesDataAccess();
			UserPermissions up = urda.getPermissions(userId);
			Hashtable<String, String> userData = null;
			String editUserCompany = "";
			if (StringUtils.isNotBlank(id))
			{
				userData = uada.getUserWithId(id);
				editUserCompany = uada.getUserCompanyForUserId(id).getId();
			}

			ElementData user = ModuleAccess.getInstance().getElementById(uada.USER_MODULE, id);
			//Get the team id we are modifying
			Boolean addCheck = action.equals("add");
			Boolean editCheck = action.equals("edit") && userData != null && companyId.equals(editUserCompany);
			if (up.hasManageUsersPermission() && (editCheck || addCheck))
			{
	%>
	<form class="form-edit-user" action="" method="POST">
		<%
			if (editCheck)
					{
		%>
		<input type="hidden" id="id" name="id"
			value="<%=userData.get(uada.E_ID)%>" />
		<%
			}
		%>
		<input type="hidden" id="action" name="action" value=<%=action%> />
		<div class="modal-header">
			<div class="titleBar">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					<i class="fal fa-times-circle"></i>
				</button>
			</div>
			<h2 class="modal-title">User</h2>
		</div>
		<div class="modal-body">
			<div class="form-group" style="margin-bottom: 10px;">
				<label for="username" style="font-size: 14px;font-weight: normal;color: black;">Username</label>
				<input type="text" name="disable_autocomplete" style="opacity:0;position:absolute;top:-999px;"/>
				 <input
					class="form-control" 
					<%=editCheck ? "readonly" : "name=\"username\""%>
					value="<%=editCheck ? userData.get(uada.E_HEADLINE) : ""%>" />
			</div>

			<div class="container contentContainer">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation"
						class=" user-tab personal-tab_button"><a class="<%=StringUtils.isBlank(tab) || tab.equals("Personal") ? "active" : ""%>"
						href="#personal_tab" aria-controls="personal" role="tab"
						data-module="Personal" data-toggle="tab"
						id="personal_tab_button_a">Personal</a></li>
					<li role="presentation"
						class="user-tab enrollment_tab_button"><a  class="<%=StringUtils.isNotBlank(tab) && tab.equals("Enrollment") ? "active" : ""%> "
						href="#enrollment_tab" aria-controls="enrollment" role="tab"
						data-module="Enrollment" data-toggle="tab"
						id="enrollment_tab_button_a">Privileges</a></li>
					<%--<li role="presentation"--%>
						<%--class=" user-tab notification_tab_button"><a class="<%=StringUtils.isNotBlank(tab) && tab.equals("Notifications") ? "active" : ""%>"--%>
						<%--href="#notifications_tab" aria-controls="notifications" role="tab"--%>
						<%--data-module="Notifications" data-toggle="tab"--%>
						<%--id="notification_tab_button_a">Notifications</a></li>--%>
<%--					<%--%>
<%--						if (itinerary)--%>
<%--								{--%>
<%--					%>--%>
<%--					<li role="presentation"--%>
<%--						class="<%=StringUtils.isNotBlank(tab) && tab.equals("Itinerary") ? "active" : ""%> user-tab itinerary_tab_button"><a--%>
<%--						href="#itinerary_tab" aria-controls="itinerary" role="tab"--%>
<%--						data-module="Itinerary" data-toggle="tab" id="itinerary_tab_button_a">Itinerary</a></li>--%>
<%--					<%--%>
<%--						}--%>
<%--					%>--%>
<%--					<%--%>
<%--						if (approvalUsers)--%>
<%--								{--%>
<%--					%>--%>
<%--					<li role="presentation"--%>
<%--						class="<%=StringUtils.isNotBlank(tab) && tab.equals("Workflow") ? "active" : ""%> user-tab workflow_tab_button"><a--%>
<%--						href="#workflow_tab" aria-controls="workflow" role="tab"--%>
<%--						data-module="Workflow" data-toggle="tab" id="workflow_tab_button_a">Workflow</a></li>--%>
<%--					<%--%>
<%--						}--%>
<%--					%>--%>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content tab-user-content">
					<div role="tabpanel"
						class=" tab-pane <%=StringUtils.isBlank(tab) || tab.equals("Text") ? "active" : ""%>"
						id="personal_tab">
						<div class="form-group">
							<label for="emailAddress">Email Address</label> <input class="form-control"
																		type="text" id="emailAddress" name="emailAddress"
																		value="<%=editCheck ? userData.get(uada.E_EMAIL_ADDRESS) : ""%>" />
						</div>
						<div class="form-group">
							<label for="name">First Name</label> <input class="form-control"
								type="text" id="name" name="name"
								value="<%=editCheck ? userData.get(uada.E_NAME) : ""%>" />
						</div>
						<div class="form-group">
							<label for="surname">Surname</label> <input class="form-control"
								type="text" id="surname" name="surname"
								value="<%=editCheck ? userData.get(uada.E_SURNAME) : ""%>" />
						</div>
						<div class="form-group">
							<label for="resetPassword"><%=editCheck ? "Reset" : ""%>
								Password</label><input type="password" style="opacity:0;position:absolute;top:-999px;"/><input class="form-control" type="password"
								id="resetPassword" name="resetPassword" />
						</div>
						<div class="form-group">
							<label for="confirmPassword">Confirm Password</label> <input
								class="form-control" type="password" id="confirmPassword"
								name="confirmPassword" />
						</div>
						<div class="form-group">
							<label for="phone">Phone</label> <input class="form-control"
								type="text" id="phone" name="phone"
								value="<%=editCheck ? userData.get(uada.E_MOBILE_NUMBER) : ""%>" />
						</div>
					</div>
					<div role="tabpanel"
						class="tab-pane <%=StringUtils.isNotBlank(tab) && tab.equals("Images") ? "active" : ""%>"
						id="enrollment_tab">
<%--						<div class="form-group">--%>
<%--							<label for="teams">Team</label> <select id="teams"--%>
<%--								style="width: 100%;" name="teams">--%>
<%--								<option <%=addCheck ? "selected" : ""%> value=""></option>--%>
<%--								<%--%>
<%--									List<CategoryData> teams = uada.getCompanyTeams(companyId);--%>
<%--											for (CategoryData team : teams)--%>
<%--											{--%>
<%--								%>--%>
<%--								<option--%>
<%--									<%=userData != null && team.getId().equals(userData.get(uada.C_ID)) ? "selected" : ""%>--%>
<%--									value="<%=team.getId()%>"><%=team.getName()%></option>--%>
<%--								<%--%>
<%--									}--%>
<%--								%>--%>
<%--							</select>--%>
<%--						</div>--%>
						<div class="form-group">
							<label for=roles">Roles</label> <select style="width: 100%;"
								multiple id="roles" name="roles">
								<%
									List<ElementData> roles = urda.getCompanyRoles(companyId);
											MultiOptions activeRoles = ModuleAccess.getInstance().getMultiOptions(user, uada.E_USER_ROLES,
													uada.USER_MODULE);
											for (ElementData role : roles)
											{
												boolean active = false;
												if (StringUtils.isNotBlank(id))
												{
													for (String activeRole : activeRoles.getValues())
													{
														if (activeRole.equals(role.getId()))
														{
															active = true;
														}
													}
												}
								%>
								<option <%=active ? "selected" : ""%> value="<%=role.getId()%>"><%=role.getName()%></option>
								<%
									}
								%>
							</select>
						</div>
						<div class="form-group hidden">
							<label for=manages">Manages <a href="javascript:void(0);"
								data-content="editUserManage" class="helpAction">(?)</a></label> <select
								style="width: 100%;" multiple id="manages" name="manages">
								<%
									Hashtable<String, ElementData> users = uada.getCompanyUsers(companyId);
											MultiOptions manages = ModuleAccess.getInstance().getMultiOptions(user, uada.E_MANAGES_MULTI,
													uada.USER_MODULE);
											for (String key : users.keySet())
											{
												ElementData companyUser = users.get(key);
												boolean active = false;
												if (StringUtils.isNotBlank(id))
												{
													for (String managed : manages.getValues())
													{
														if (managed.equals(companyUser.getId()))
														{
															active = true;
														}
													}
												}
												if (!user.getId().equals(companyUser.getId()))
												{
								%>
								<option <%=active ? "selected" : ""%>
									value="<%=companyUser.getId()%>"><%=companyUser.getName()%></option>
								<%
									}
											}
								%>
							</select>
						</div>
						<% if (approvalUsers) {%>
						<div class="form-group">
							<label for="proposalSendApproval">Proposal Send Approval Users </label>
							<select style="width: 100%;" multiple id="proposalSendApproval" name="proposalSendApproval">
							<%
								MultiOptions approvalUsersList = ModuleAccess.getInstance().getMultiOptions(user, uada.E_APPROVAL_USERS_MULTI,
										uada.USER_MODULE);
								for (String key : users.keySet())
								{
									ElementData companyUser = users.get(key);
									boolean active = false;
									if (StringUtils.isNotBlank(id))
									{
										for (String approvalList : approvalUsersList.getValues())
										{
											if (approvalList.equals(companyUser.getId()))
											{
												active = true;
											}
										}
									}
									if (!user.getId().equals(companyUser.getId()))
									{
							%>
							<option <%=active ? "selected" : ""%>
									value="<%=companyUser.getId()%>"><%=companyUser.getName()%></option>
							<%
									}
								}
							%>
						</select>
							<small>Note: Select users above who will have to approve a proposal before it is sent.</small>
						</div>
						<% } %>
					</div>
					<%
						if (approvalUsers)
								{
					%>
					<div role="tabpanel"
						class="tab-pane <%=StringUtils.isNotBlank(tab) && tab.equals("Workflow") ? "active" : ""%>"
						id="workflow_tab">
						<div class="form-group">
							<label for="proposalSendApproval">Proposal Approval User(s)</label>
							<select style="width: 100%;" multiple id="proposalSendApproval" name="proposalSendApproval">
							<%
								MultiOptions approvalUsersList = ModuleAccess.getInstance().getMultiOptions(user, uada.E_APPROVAL_USERS_MULTI,
										uada.USER_MODULE);
								for (String key : users.keySet())
								{
									ElementData companyUser = users.get(key);
									boolean active = false;
									if (StringUtils.isNotBlank(id))
									{
										for (String approvalList : approvalUsersList.getValues())
										{
											if (approvalList.equals(companyUser.getId()))
											{
												active = true;
											}
										}
									}
									if (!user.getId().equals(companyUser.getId()))
									{
							%>
							<option <%=active ? "selected" : ""%>
									value="<%=companyUser.getId()%>"><%=companyUser.getName()%></option>
							<%
									}
								}
							%>
						</select>
							<small>Note: Select users above who will have to approve a proposal before it is sent.</small>
						</div>

					</div>
					<%
						}
					%>

				</div>
			</div>
		</div>
		<div class="modal-footer">
			<div class="row" style="width: 100%;margin: 0;">
				<div class="col">
					<% if(editCheck && userData.get(uada.E_SURNAME) != null && userData.get(uada.E_SURNAME).contains("(Pending Verification")) { %>
						<button type="button" id="resendActivationEmail" class="warning-button">
							Resend Activation Email
						</button>
					<% } %>
					<button type="button" id="closeForm" class="cancel-button" style="margin-left: 10px;"
							data-dismiss="modal">
						Close
					</button>
					<button type="submit" class="success-button" style="color: white !important;margin-left: 10px;">
						Save
					</button>
				</div>
			</div>
		</div>
	</form>
	<%
		}
			else
			{
	%>
	<div class="modal-header">
		<div class="titleBar">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true"></button>
		</div>
		<h2 class="modal-title">User</h2>
	</div>
	<div class="modal-body">You do not have permissions modify this
		user</div>
	<div class="modal-footer">
		<button type="button" id="closeForm" class="btn btn-danger"
			data-dismiss="modal">
			<i class="fa fa-times"></i> Close
		</button>
	</div>
	<%
		}
	%>
	<script src="/application/assets/plugins/select2/select2.js"></script>
	<script src="/application/js/form-validation/modal-edit-user.js"></script>
</div>
</compress:html>