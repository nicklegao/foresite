<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.ProposalDashboardDataAccess"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.dashboard.datatable.DatatableProcessingRequest"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.List"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.*" %>
<%
TravelDocsHistoryDataAccess phda = new TravelDocsHistoryDataAccess();
ProposalDashboardDataAccess pda = new au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDashboardDataAccess();
UserAccountDataAccess uada = new UserAccountDataAccess();

String userEmail = (String) session.getAttribute(Constants.PORTAL_USERNAME_ATTR);
String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
String[] accessRights = uada.getUserAccessRights((String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR));
DatatableProcessingRequest dpr = new DatatableProcessingRequest();

List<Hashtable<String, String>> proposalData = pda.loadProposalsFiltered(dpr, accessRights);

SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
SimpleDateFormat formatter = new SimpleDateFormat("dd MMM - hh:mm aa");
%>
<div class="modal-header">
	<div class="titleBar">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			<i class="fal fa-times-circle"></i>
		</button>
	</div>
	<h2 class="modal-title">New Customer Comments</h2>
</div>
<div class="modal-body" id="comments-feed-scroll">
	<section id="timeline">
		<% 
		int triggerId = 0;
		for (Hashtable<String, String> proposalHT : proposalData) { 
			String element_id = proposalHT.get(TravelDocsDataAccess.E_ID);
			int notifications = phda.getNotificationsForProposal(element_id);
			
			if (notifications > 0) {
				
		%>
		<div id="fadein-trigger<%= triggerId %>" class="timeline-block">
			<div class="timeline-icon timeline-icon<%= triggerId %> alert">
				<a href="#" class="markAsReadAction icon-container" data-id="<%= ESAPI.encoder().encodeForHTMLAttribute(element_id) %>">
					<i class="fal fa-times-circle fa-2x"></i>
				</a>
			</div> <!-- timeline-img -->
			
			<div class="timeline-content timeline-content-left timeline-content-left<%= triggerId %>">
				<h2><%= notifications %> Notification<%= notifications > 1 ? "s" : "" %></h2>
				<h3><%= ESAPI.encoder().encodeForHTML(proposalHT.get(TravelDocsDataAccess.E_PROPOSAL_TITLE)) %></h3>
				<h4><%= ESAPI.encoder().encodeForHTML(proposalHT.get(TravelDocsDataAccess.E_CLIENT_COMPANY)) %></h4>
				<p>Contact: <%= ESAPI.encoder().encodeForHTML(proposalHT.get(TravelDocsDataAccess.E_CLIENT_NAME)) %></p>
			</div> <!-- timeline-content -->
			
			<div class="timeline-content timeline-content-right timeline-content-right<%= triggerId++ %>">
				<% for (Hashtable<String, String> historyHT : phda.getUnreadHistory(element_id) ) {
					Date parseDate = sdf.parse(historyHT.get("Create_date"));%>
					<span class="date"><%= ESAPI.encoder().encodeForHTML(formatter.format(parseDate)) %></span>
					<p class="list-group-item-text"> 
						<i class="fa fa-quote-left"></i> 
							<%= ESAPI.encoder().encodeForHTML(phda.getDetailedNote(element_id, historyHT.get(TravelDocsDataAccess.E_ID))) %>
						<i class="fa fa-quote-right"></i> 
					</p>
				<% } %>				
			</div> <!-- timeline-content -->
		</div> <!-- timeline-block -->
		<% }
		} %>
		<!-- timeline-img
		<div class="timeline-img notification">
			<img src="img/icon-picture.svg" alt="Picture">
		</div>  -->

	</section>
	
</div>
<div class="modal-footer">
	<button type="button" data-dismiss="modal" class="btn btn-danger closeManagementDialog"><i class="fa fa-times"></i> Close</button>
</div>
<script src="/application/assets/plugins/scrollmagic/uncompressed/plugins/TweenMax.js"></script>
<script src="/application/assets/plugins/scrollmagic/uncompressed/ScrollMagic.js"></script>
<script src="/application/assets/plugins/scrollmagic/uncompressed/plugins/animation.gsap.js"></script>
<script src="/application/assets/plugins/scrollmagic/uncompressed/plugins/debug.addIndicators.js"></script>
<script src="/application/js/commentsFeed.js"></script>