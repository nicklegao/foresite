<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess"%>
<%@page import="java.util.List"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.entity.MultiOptions"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.SecurityKeyService"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="java.util.Map"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Set"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Vector"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess" %>
<%
TravelDocsDataAccess pda = new TravelDocsDataAccess();

UserAccountDataAccess uada = new UserAccountDataAccess();
String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
CategoryData company = uada.getUserCompanyForUserId(userId);
SecurityKeyService sks = new SecurityKeyService();
CategoryData companyInfo = uada.getUserCompanyForUserId(userId);
String companyId = companyInfo.getId();
List<CategoryData> teams = new UserAccountDataAccess().usersTeams(userId, companyId);

%>
<div id="template-management">
  	<div class="modal-header">
      <div class="titleBar">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        	<i class="fal fa-times-circle"></i>
        </button>
      </div>
      <h2 class="modal-title">Manage Templates</h2>
    </div>
    <div class="modal-body">
   		<div class="container contentContainer">
   			<% Vector<Hashtable<String, String>> templates = pda.getTemplatesForTeam(company.getId(), company.getName(), userId);
   			if(templates.size()>0){
   			%>
			<table class="table table-bordered table-hover table-full-width" id="table-users" data-module="User" width="100%">
				<thead>
					<tr>
						<th>Template</th>
						<th width="200">Actions</th>
					</tr>
				</thead>
				<tbody>
				<%
				MultiOptions activeProposalLibrary = ModuleAccess.getInstance().getMultiOptions(teams.get(0), TUserAccountDataAccess.E_TEAM_TEMPLATE_FOLDERS,"users");
				for (Hashtable<String, String> ht : templates){
		        	String proposalId = ht.get(TravelDocsDataAccess.E_ID);
		        	String eKey = sks.getEditKey(Integer.parseInt(userId), Integer.parseInt(proposalId), ht.get(TravelDocsDataAccess.E_KEY));
					String librarySwitchDBRef = TUserAccountDataAccess.librarySwitchDBRef("templates");
					System.out.print(teams.get(0).getBoolean(librarySwitchDBRef));
		        	if (teams.get(0).getBoolean(librarySwitchDBRef)) {
		        		if (StringUtils.isNotBlank(teams.get(0).getId())) {
		        			for (String activeProposal : activeProposalLibrary.getValues()) {
								if (activeProposal.equals(proposalId)) {%>
							<tr class="templates-item"
								data-id="<%=ht.get(TravelDocsDataAccess.E_ID) %>">
								<td class="template-items"><span class="templates-item-from"><%=ESAPI.encoder().encodeForHTML(ht.get(TravelDocsDataAccess.E_TEMPLATE_TITLE)) %></span>
									<span class="messages-item-subject"><%=ESAPI.encoder().encodeForHTML(ht.get(TravelDocsDataAccess.E_TEMPLATE_DESCRIPTION)) %></span>
								</td>
								<td class="template-actions"><a alt="Edit"
								href="/edit?proposal=<%= proposalId %>&amp;e-key=<%= eKey %>"
								target="edit-<%= proposalId %>"
								class="btn col-sm-6 btn-default tooltips"><i
								class="qc qc-edit"></i> Edit</a> <a data-original-title="Delete"
								href="#templateDeleteModal" data-placement="top"
								data-toggle="modal"
								class="btn btn-default col-sm-6 btn-qcloudicon  tooltips templateDeleteAction"
								data-id="<%= proposalId %>">
									<i class="qc qc-trash"></i>Delete</a></td>
							</tr>
							<% }
		        			}
	    				}
		        	} else { %>
							<tr class="templates-item"
								data-id="<%=ht.get(TravelDocsDataAccess.E_ID) %>">
								<td class="template-items"><span class="templates-item-from"><%=ESAPI.encoder().encodeForHTML(ht.get(TravelDocsDataAccess.E_TEMPLATE_TITLE)) %></span>
									<span class="messages-item-subject"><%=ESAPI.encoder().encodeForHTML(ht.get(TravelDocsDataAccess.E_TEMPLATE_DESCRIPTION)) %></span>
								</td>
								<td class="template-actions"><a alt="Edit"
								href="/edit?proposal=<%= proposalId %>&amp;e-key=<%= eKey %>"
								target="edit-<%= proposalId %>"
								class="btn col-sm-6 btn-default tooltips"><i
								class="qc qc-edit"></i> Edit</a> <a data-original-title="Delete"
								href="#templateDeleteModal" data-placement="top"
								data-toggle="modal"
								class="btn btn-default col-sm-6 btn-qcloudicon  tooltips templateDeleteAction"
								data-id="<%= proposalId %>">
									<i class="qc qc-trash"></i>Delete</a></td>
							</tr>
					<% } } %>
				</tbody>
			</table>
			<% } else { %>
			<div style="background: #fff; padding: 8px;">
				<span>You don't have any templates saved right now. To create one, click the blue add button below and starts designing.</span>
                <br/>
                <br/>
                <span>Or, click the green button below to get some free samples.</span>
			</div>
			<% } %>
		</div>
    </div>
    
    
    <div class="modal-footer">
      <button type="button" id="get-samples" class="btn btn-success pull-left">
        <i class="fa fa-download"></i> Get Sample Templates
      </button>
      <a type="button" href="/template" target="_blank" id="addTemplate" class="btn btn-primary">
        <i class="fa fa-plus-square-o"></i> Add
      </a>
      <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
        <i class="fa fa-times"></i> Close
      </button>
    </div>
    
	<div id="templateDeleteModal" 
	        class="modal fade" 
	        tabindex="-1" 
	        data-backdrop="static" 
	        data-keyboard="false">
	  
	    <div class="alert alert-block alert-danger fade in" style="margin: 0">
	      <h4 class="alert-heading"><i class="fa fa-warning"></i> Confirmation</h4>
	      <p>
	        Are you sure you would like to delete this template?
	      </p>
	      <p>
	        <a class="btn btn-success templateDeleteConfirmed" href="#">
	          <i class="fa fa-check"></i>
	          &nbsp;Yes
	        </a>
	        <button type="button" class="btn btn-danger" data-dismiss="modal">
	          <i class="fa fa-times"></i>
	          &nbsp;No
	        </button>
	      </p>
	    </div>
	</div>
</div>
  
  <script>
  $(document).ready(function(){
  	$("#template-management").on("click", ".templateDeleteAction", templateDeleteAction);
  	$(".templateDeleteConfirmed").on("click", templateDeleteConfirmed);
  	$("#get-samples").on("click", getSampleTemplateAction);
  })
  
  function getSampleTemplateAction(event){
    var $modal = $('#get-sample-template-modal');
    $modal.load('/application/dialog/getSampleTemplate.jsp', {}, function()
  	{
  		$modal.modal({
  			width : '760px',
  			backdrop : 'static',
  			keyboard : false
  		});
  	});
  }
  
  function templateDeleteAction(event){
	  var proposalId = $(this).attr("data-id");
		
	  $("#templateDeleteModal .templateDeleteConfirmed").attr("data-id", proposalId);
  }
  
  function templateDeleteConfirmed(event){
	  var proposalId = $("#templateDeleteModal .templateDeleteConfirmed").attr("data-id");
	  $("#templateDeleteModal").modal("hide");
	  startLoadingForeground();
	  $.ajax({
	    url: '/api/proposal/delete',
	    data: {
	    	proposalId: proposalId
	    }
	  }).done(function( data, textStatus, jqXHR ) {
		  if(data){
		    $('#template-management-modal').load('/application/dialog/templateManagement.jsp');
	      }else{
	    	  swal({
	    		  title: "Error",
	    		  text: "It wasn't possible to delete the template. Contact your Qcloud administrator and make sure you have permission to manage templates.",
	    		  type: "error"
	    	  });
	      }
	      stopLoadingForeground();
	  });
  }
  </script>
<!-- <script src="/application/js/form-validation/modal-edit-styles.js"></script> -->

</compress:html>