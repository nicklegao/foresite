<%@page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions"%>
<%@page import="java.util.Hashtable"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ page import="com.warrenstrange.googleauth.GoogleAuthenticator" %>
<%@ page import="au.corporateinteractive.utils.GoogleAuthUtils" %>
<%@ page import="com.warrenstrange.googleauth.GoogleAuthenticatorConfig" %>
<%@ page import="com.warrenstrange.googleauth.GoogleAuthenticatorQRGenerator" %>
<%@ page import="com.warrenstrange.googleauth.GoogleAuthenticatorKey" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
    <div id="setup-auth-email">
        <%
            UserAccountDataAccess uada = new UserAccountDataAccess();
            String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
            ElementData userData = uada.getUserElementWithUserID(userId);

            boolean emailVerification = !UserAccountDataAccess.isUserUsingMobileTFA(userData);
        %>
        <form class="form-email-auth">
            <input type="hidden" id="id" name="id" value="<%= userId %>" />
            <div class="modal-header">
                <div class="titleBar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <h2 class="modal-title">Setup Security</h2>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="hidden" name="updateEmailVerification" value="<%= !emailVerification %>" />
                    <label for="verificationToken">Enter the code sent to your email below to <%= emailVerification ? "disable" : "enable" %> email two factor authentication</label>
                    <input type="text" name="verificationToken" id="verificationToken" placeholder="Verification Code" style="border-bottom: 1px solid;" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
                    <i class="fa fa-times"></i> Close
                </button>
                <button type="submit" class="btn btn-success">
                    <i class="fa fa-save"></i> Save
                </button>
            </div>
        </form>
        <script src="/application/js/form-validation/modal-email-2fa-setup.js"></script>
    </div>
</compress:html>