<div id="content-library-folder-edit-modal" class="modal fade" data-backdrop="static" data-keyboard="false"
	tabindex="-1" style="display: none;">
	<div class="modal-content">
		<form id="folder-edit-form">
			<input type="hidden" name="folderId">
			<input type="hidden" name="libraryType">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Edit Folder</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="folderName">Folder Name</label>
					<input type="text" class="form-control" name="folderName">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="editFolderClose">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
			</div>
		</form>
	</div>
	<!-- /.modal-content -->
</div>
