<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/css/contentLibrary.css",request) %>" />
<div class="modal-header">
	<div class="titleBar">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			<i class="fal fa-times-circle"></i>
		</button>
	</div>
	<h2 class="modal-title">Manage Content Library</h2>
</div>
<div class="modal-body">
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
	  <li role="presentation" class="active"><a href="#items" aria-controls="items" role="tab" data-toggle="tab">Items</a></li>
	  <li role="presentation"><a href="#folders" aria-controls="folders" role="tab" data-toggle="tab">Folders</a></li>
	</ul>
	
	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="items">
			<table class="table table-hover content-library-table" id="content-library-table">
				<thead class="searchHead">
					<tr >
						<th>Folder</th>
						<th>Name</th>
						<th>Locked</th>
						<th>Action</th>
					</tr>
					<tr class="item-filters filters">
						<th><input type="text" placeholder="Filter..." class="form-control input-sm"></th>
						<th><input type="text" placeholder="Filter..." class="form-control input-sm"></th>
						<th>
							<select class="form-control input-sm">
								<option value="">Filter...</option>
								<option value="1">ON</option>
								<option value="0">OFF</option>
							</select>
						</th>
						<th>
							<button class="btn btn-sm btn-danger clearColumnFilters columnFiltersEnabled" style="display: inline-block;">
								<i class="fa fa-eraser"></i> Clear Filters</button>
						</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${libraryItems}">
						<tr data-element-id="<c:out value="${item.element_id}" />" data-library-type="<c:out value="${libraryType}" />">
							<td><c:out value="${item.attr_categoryname}" /></td>
							<td><c:out value="${item.attr_headline}" /></td>
							
							<%
							boolean isOneOff = ((ElementData)pageContext.getAttribute("item")).getBoolean("attrcheck_lock_content");
							%>
							<td data-search="<%=isOneOff?"1":"0"%>"
								data-order="<%=isOneOff?"1":"0"%>">
								<c:if test="${item.attrcheck_lock_content == 1}">
									<i class="fa fa-check" aria-hidden="true"></i>
								</c:if>
							</td>
							<td>
								<div class="btn-group">
								  <button type="button" class="btn btn-default edit-library-item-action">
								  	<i class="fa fa-pencil-square" aria-hidden="true"></i>
								  	Edit
								  </button>
								  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    <span class="caret"></span>
								    <span class="sr-only">Toggle Dropdown</span>
								  </button>
								  <ul class="dropdown-menu">
								    <li><a href="#" class="delete-library-item-action">Delete</a></li>
								  </ul>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<div role="tabpanel" class="tab-pane" id="folders">
			<table class="table table-hover content-library-table" id="content-library-folder-table">
				<thead>
					<tr>
						<th>Name</th>
						<th>Action</th>
					</tr>
					<tr class="folder-filters filters">
						<th><input type="text" placeholder="Filter..." class="form-control input-sm"></th>
						<th>
							<button class="btn btn-sm btn-danger clearColumnFilters columnFiltersEnabled" style="display: inline-block;">
								<i class="fa fa-eraser"></i> Clear Filters</button>
						</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="folder" items="${libraryFolders}">
						<tr data-element-id="<c:out value="${folder.category_id}" />" 
							data-library-type="<c:out value="${libraryType}" />" 
							data-folder-name="<c:out value="${folder.attr_categoryname}" />">
							<td><c:out value="${folder.attr_categoryname}" /></td>
							<td>
								<div class="btn-group">
								  <button type="button" class="btn btn-default edit-library-folder-action">
								  	<i class="fa fa-pencil-square" aria-hidden="true"></i>
								  	Edit
								  </button>
								  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    <span class="caret"></span>
								    <span class="sr-only">Toggle Dropdown</span>
								  </button>
								  <ul class="dropdown-menu">
								    <li><a href="#" class="delete-library-folder-action">Delete</a></li>
								  </ul>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button class="btn btn-primary pull-left add-library-item-action" data-library-type="<c:out value="${libraryType}" />">
		<i class="fa fa-plus-square-o" aria-hidden="true"></i>
		&nbsp;Add Text
	</button>
	<button class="btn btn-primary pull-left add-library-folder-action" data-library-type="<c:out value="${libraryType}" />">
		<i class="fa fa-plus-square-o" aria-hidden="true"></i>
		&nbsp;Add Folder
	</button>
	<button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> Close</button>
</div>

<!-- Froala Editor -->
<script src="/application/assets/plugins/froala-editor/custom/config-proposalTextLibrary.js"></script>