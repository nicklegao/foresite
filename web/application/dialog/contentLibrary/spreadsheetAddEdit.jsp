<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.CustomField"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.CustomFields"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.CustomFieldsDataAccess"%>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%
UserAccountDataAccess uada = new UserAccountDataAccess();

String userId = (String) request.getSession().getAttribute(Constants.PORTAL_USER_ID_ATTR);
String companyId = uada.getUserCompanyForUserId(userId).getId();
CustomFieldsDataAccess cfda = new CustomFieldsDataAccess();
  CategoryData company = uada.getCompanyForCompanyId(companyId);
CustomFields productCustomFields = cfda.getCompanyCustomFields(CustomFields.PRODUCTS, companyId);
%>
<style>
.form-control.error {
    background: #fdecec;
    border: 1px solid #b11d1d;
}
label.error {
    color: red;
    margin-top: 5px;
}
</style>
<div id="spreadsheet-library-add-edit-modal" class="modal fade add-edit-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="display: none;">
  <div class="modal-content">
    <div class="modal-header">
		<div class="titleBar">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">
				<i class="fal fa-times-circle"></i>
			</button>
		</div>
		<h2 class="modal-title">Modal Title</h2>
	</div>
    <form id="add-edit-spreadsheet-form" method="post" enctype="multipart/form-data">
	    <div class="modal-body">
	      <input type="hidden" name="elementId">
          <div>
             <div class="form-group">
               <label for="folder">Folder</label>
               <select name="folder" class="form-control" id="spreadsheetNameSelector"></select>
               <div class="form-group hidden" id="folderNameId">
                 <label for="assetName">Folder Name</label> <input type="text"
                   class="form-control" name="newFolder" placeholder="Enter new folder name here...">
               </div>
               <label class="subFolderLabel hidden" for="subFolder">Sub Folder</label>
               <select name="subFolder" class="form-control subFolderSelect hidden"></select>
               <div class="form-group hidden" id="subFolderNameId">
                 <label for="subFolderAssetName">Sub Folder Name</label> <input type="text"
                    class="form-control" name="newSubFolder" id="newSubFolderField" placeholder="Enter new subfolder name here...">
               </div>
             </div>
             <hr>
             <div class="form-group">
               <label for="spreadsheetName">Name</label>
               <input type="text" class="form-control" name="spreadsheetName">
             </div>
             <div class="form-group">
               <label>Spreadsheet</label>
               <input type="hidden" name="spreadsheet" class="spreadsheet-val"/>
               <div class="spreadsheet" style="width:100%;"></div>
             </div>
           </div>  
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      <button type="submit" class="btn btn-primary">Save changes</button>
	    </div>
    </form>
  </div><!-- /.modal-content -->
 </div>