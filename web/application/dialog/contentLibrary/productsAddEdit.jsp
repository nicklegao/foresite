<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.CustomField"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.CustomFields"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.CustomFieldsDataAccess"%>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%
UserAccountDataAccess uada = new UserAccountDataAccess();

String userId = (String) request.getSession().getAttribute(Constants.PORTAL_USER_ID_ATTR);
String companyId = uada.getUserCompanyForUserId(userId).getId();
CustomFieldsDataAccess cfda = new CustomFieldsDataAccess();
  CategoryData company = uada.getCompanyForCompanyId(companyId);
CustomFields productCustomFields = cfda.getCompanyCustomFields(CustomFields.PRODUCTS, companyId);
%>
<style>
.form-control.error {
    background: #fdecec;
    border: 1px solid #b11d1d;
}
label.error {
    color: red;
    margin-top: 5px;
}
</style>
<div id="products-library-add-edit-modal" class="modal fade add-edit-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="display: none;">
  <div class="modal-content">
    <div class="modal-header">
		<div class="titleBar">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">
				<i class="fal fa-times-circle"></i>
			</button>
		</div>
		<h2 class="modal-title">Modal Title</h2>
	</div>
    <form id="add-edit-products-form" method="post" enctype="multipart/form-data">
	    <div class="modal-body">
	      	  <input type="hidden" name="elementId">
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#product" aria-controls="product" role="tab" data-toggle="tab">Product</a></li>
              <% if(productCustomFields != null && productCustomFields.hasOnCustomFields() > 0){ %>
              <li role="presentation"><a href="#customFields" aria-controls="customFields" role="tab" data-toggle="tab">Custom Fields</a></li>
              <% } %>
              <li role="presentation"><a href="#discountRules" aria-controls="discountRules" role="tab" data-toggle="tab">Discount Rules</a></li>
            </ul>
            <div class="tab-content" style="border-width:1px 0 0 0;">
              <div role="tabpanel" class="tab-pane active" id="product">
                
                <div class="form-group">
                  <label for="folder">Folder</label>
                  <select name="folder" class="form-control" id="productNameSelector"></select>
                  <div class="form-group hidden" id="folderNameId">
                    <label for="assetName">Folder Name</label> <input type="text"
                      class="form-control" name="newFolder" placeholder="Enter new folder name here...">
                  </div>
                  <label class="subFolderLabel hidden" for="subFolder">Sub Folder</label>
                  <select name="subFolder" class="form-control subFolderSelect hidden"></select>
                  <div class="form-group hidden" id="subFolderNameId">
                    <label for="subFolderAssetName">Sub Folder Name</label> <input type="text"
                       class="form-control" name="newSubFolder" id="newSubFolderField" placeholder="Enter new subfolder name here...">
                  </div>
                </div>
                <hr>
                <div class="form-group">
                  <label for="productName">Product Name</label>
                  <input type="text" class="form-control" name="productName">
                </div>
                <div class="form-group">
                  <label for="description">Sales Proposal Description Line</label>
                  <textarea class="form-control" name="description"></textarea>
                </div>
                <div class="form-group" style="height: 60px;">
                  <div class="col-sm-4" style="padding-left: 0;">
                    <label for="price">Price</label>
                    <input type="number" class="form-control" name="price">
                  </div>
                  <div class="col-sm-4">
                    <% if(!company.getBoolean(uada.C1_HIDE_TERMS_ALWAYS)) {%>
                    <div class="form-group">
                      <label for="price">Term (Months)</label>
                      <input type="number" class="form-control" name="term">
                    </div>
                    <% } %>
                  </div>
                  <div class="col-sm-4">
                    <div class="checkbox" style="margin: 30px 0 7px 0px;">
                      <label>
                        <input type="checkbox" class="form-control" name="oneOff" <%=company.getBoolean(uada.C1_HIDE_TERMS_ALWAYS)?"checked":"" %>> One-off
                      </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="file" id="imageUpload" accept="image/*" style="display:none;">
                  <input type="text" name="image" style="display:none;">
                  <input type="checkbox" name="deleteImage" style="display:none;">
                  <label for="note">Thumbnail</label>
                  <div id="thumbnail"></div>
                  <div id="thumbnail-buttons">
                  </div>
                </div>
                <div class="form-group">
                  <label for="note">Product Notes</label>
                  <%--<texarea class="form-control" name="note" id="froala-notes-area"></texarea>--%>
                  <div class="panel-froala">
                  </div>
                  <i><small>Info: Text will appear below the product line item in the pricing table.</small></i>
                </div>
              </div>
              <% if(productCustomFields.hasOnCustomFields() > 0){ %>
              <div role="tabpanel" class="tab-pane" id="customFields">
              <%
              for(CustomField field : productCustomFields){ 
                if(field.isOn()){ 
              %>
                <div class="form-group">
                  <label for="note"><%=field.getLabel() %></label>
                  <% if(field.getType().equals("text") || field.getType().equals("date")){ %>
                  <input type="text" class="form-control custom-field  cust-fld-<%= field.getType() %> <%=field.getId() %>" name="<%=field.getId() %>">
                  <% }else if(field.getType().equals("integer")){ %>
                  <input type="number" class="form-control custom-field  cust-fld-<%= field.getType() %> <%=field.getId() %>" name="<%=field.getId() %>">
                  <% }else if(field.getType().equals("currency")){ %>
                  <input type="number" step="0.01" class="form-control custom-field  cust-fld-<%= field.getType() %> <%=field.getId() %>" name="<%=field.getId() %>">
                  <% }else if(field.getType().equals("dropdown")){ %>
                  <select name="<%=field.getId() %>" class="form-control custom-field <%=field.getId() %>">
                    <option value=""></option>
                    <%for(String value : field.getValues().split("\\|")){%>
                    <option value="<%=value %>"><%=value %></option>
                    <% } %>
                  </select>
                  <% } %>
                </div>
              <%
                }
              }
              %>
              </div>
              <% } %>
              <div role="tabpanel" class="tab-pane" id="discountRules">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      Label
                    </div>
                  </div>
                  <div class="col-sm-3">   
                    <div class="form-group"> 
                      Quantity  
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      Price
                    </div>
                  </div>
                </div>
                <div>
                  <% for(int i=1;i<6;i++){ %>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <input type="text" class="form-control attr_label<%= i %>" name="attr_label<%= i %>" placeholder="Label">
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="form-group">
                        <div class="input-group">
                        <input type="number" class="form-control attrfloat_qty<%= i %>" min="0" name="attrfloat_qty<%= i %>" placeholder="Qty">
                        </div>   
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="form-group">
                        <div class="input-group">
                          <input type="number" min="0" class="form-control attrcurrency_price<%= i %>" name="attrcurrency_price<%= i %>" placeholder="Price">
                        </div>
                      </div>
                    </div>
                  </div>
                  <% } %>
                  <i><small>Info: Any changes to discounts will not affect existing proposals. Your Sales Consultants will need to re-open and save the proposal for QuoteCloud to recalculate discounts.</small></i>
                </div>
              </div>
            </div>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      <button type="submit" class="btn btn-primary">Save changes</button>
	    </div>
    </form>
  </div><!-- /.modal-content -->
 </div>