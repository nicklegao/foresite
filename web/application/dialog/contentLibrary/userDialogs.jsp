<div id="content-library-modal" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" style="display: none;"></div>
<jsp:include page="contentAddEditFolder.jsp"></jsp:include>
<jsp:include page="contentDeleteFolder.jsp"></jsp:include>
<jsp:include page="productsAddEdit.jsp"></jsp:include>
<jsp:include page="productsImport.jsp"></jsp:include>
<jsp:include page="textAddEdit.jsp"></jsp:include>
<jsp:include page="pdfAddEdit.jsp"></jsp:include>
<jsp:include page="imagesAddEdit.jsp"></jsp:include>
<jsp:include page="videosAddEdit.jsp"></jsp:include>
<jsp:include page="spreadsheetAddEdit.jsp"></jsp:include>
<jsp:include page="../contactsAddEdit.jsp"></jsp:include>
<jsp:include page="../contactsImport.jsp"></jsp:include>
