<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.entity.StoreFile"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/css/contentLibrary.css",request) %>" />

<div class="modal-header">
	<div class="titleBar">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			<i class="fal fa-times-circle"></i>
		</button>
	</div>
	<h2 class="modal-title">Manage Content Library</h2>
</div>
<div class="modal-body">
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
	  <li role="presentation" class="active"><a href="#items" aria-controls="items" role="tab" data-toggle="tab">Items</a></li>
	  <li role="presentation"><a href="#folders" aria-controls="folders" role="tab" data-toggle="tab">Folders</a></li>
	  <li role="presentation">
	  	<a id="add-item" role="button" data-toggle="collapse" href="#addImagesContainer" aria-expanded="false" aria-controls="addImagesContainer">
	  		<i class="fa fa-plus-square-o" aria-hidden="true"></i>
			&nbsp;Add images
		</a>
	  </li>
	</ul>
	
	
	<div class="collapse" id="addImagesContainer">
	  <div class="well">
	    <form action="/api/library/add/images/bulk" method="post" enctype="multipart/form-data" id="uploadImages" class="dropzone">
	    
		  <div class="fallback">
		    <input name="file[]" type="file" multiple />
		  </div>
		</form>
	  </div>
	</div>


	<!-- Tab panes -->
	<div class="tab-content">
	  <div role="tabpanel" class="tab-pane active" id="items">
	  
	  
	
		<c:choose>
		  <c:when test="${module != 'gen_images'}">
		  	<table class="table table-hover content-library-table" id="content-library-table">
			<thead>
				<tr>
					<th>Folder</th>
					<th>Name</th>
					<th width="150">Image</th>
					<th>Action</th>
				</tr>
				
					<tr class="item-filters filters">
						<th><input type="text" placeholder="Filter..." class="form-control input-sm"></th>
						<th><input type="text" placeholder="Filter..." class="form-control input-sm"></th>
						<th></th>
						<th>
							<button class="btn btn-sm btn-danger clearColumnFilters columnFiltersEnabled" style="display: inline-block;">
								<i class="fa fa-eraser"></i> Clear Filters</button>
						</th>
					</tr>
				
			</thead>
			<tbody>
				<c:forEach var="item" items="${libraryItems}">
					<%
					ElementData itemData = (ElementData)pageContext.getAttribute("item");
					%>
					<tr data-element-id="<c:out value="${item.element_id}" />" data-library-type="<c:out value="${libraryType}" />">
						<td><c:out value="${item.attr_categoryname}" /></td>
						<td><c:out value="${item.attr_headline}" /></td>
						<td class="text-center">
						<%
						StoreFile imageStoreFile = itemData.getStoreFile(ImageLibraryService.E_IMAGE_FILE);
						if(imageStoreFile!=null){
						String imageThumbnail = imageStoreFile.getThumbNailPath();
						%>
							<a href="/stores<c:out value="${item.attrfile_image}" />" target="_blank">
								<img src="/stores<%=imageThumbnail %>" height="90"/>
							</a>
						<% } %>
						</td>
						<td>
							<div class="btn-group">
							  <button type="button" class="btn btn-default edit-library-item-action">
							  	<i class="fa fa-pencil-square" aria-hidden="true"></i>
							  	Edit
							  </button>
							  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							    <span class="caret"></span>
							    <span class="sr-only">Toggle Dropdown</span>
							  </button>
							  <ul class="dropdown-menu">
							    <li><a href="#" class="delete-library-item-action">Delete</a></li>
							  </ul>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		  </c:when>
		  <c:otherwise>
		  
		  <table class="table table-hover content-library-table dataTable no-footer" role="grid">
		    <thead>
				<tr class="folder-filters filters" role="row">
					<th rowspan="1" colspan="1"><div id="search-row" ></div></th>
					<th rowspan="1" colspan="1">
					 <select class="form-control input-sm" id="search-folder" name="folder">
						  <option>All folders</option>
						  <c:forEach var="folder" items="${libraryFolders}">
						 	<option><c:out value="${folder.attr_categoryname}" /></option>
						  </c:forEach>
						</select>
					</th>
				</tr>
			</thead>
		</table>
		  
			<ul id="images-list" class="row-fluid">
			<c:forEach var="item" items="${libraryItems}">
			<%ElementData itemData = (ElementData)pageContext.getAttribute("item");%>
			  <li class="image-item" data-element-id="<c:out value="${item.element_id}" />" data-library-type="<c:out value="${libraryType}" />">
			    <div class="image-container" data-category-name="<c:out value="${item.attr_categoryname}" />" >
			      <div class="image-thumbnail">
			      <%
					StoreFile imageStoreFile = itemData.getStoreFile(ImageLibraryService.E_IMAGE_FILE);
					if(imageStoreFile!=null){
						String imageThumbnail = imageStoreFile.getPath();%>
						<a href="javascript:;" class="library-item-cell edit-library-item-action">
			        		<div class="center-cropped"  style="background: url('/stores<%=imageThumbnail %>?image-size=156x156-fit')"></div>
			        	</a>
			        <% }else{ %>
			        	<a href="javascript:;" class="library-item-cell edit-library-item-action">
			        		<div class="center-cropped"  style="background: url('/application/assets/images/images_placeholder.png')"></div>
			        	</a>
			        <% } %>
<%-- 			        <input data-element-id="<c:out value="${item.element_id}" />" type="checkbox" id="delete-image" name="delete-image">
 --%>			      </div>
			      <div class="caption">
			        <h3><c:out value="${item.attr_headline}" /></h3>
			      </div>
			    </div>
			  </li>
			  </c:forEach>
			</ul>
		  </c:otherwise>
		</c:choose>
	  	
	  </div>
	  <div role="tabpanel" class="tab-pane" id="folders">
			<table class="table table-hover content-library-table" id="content-library-folder-table">
				<thead>
					<tr>
						<th>Name</th>
						<th>Action</th>
					</tr>
					<tr class="filters">
						<th><input type="text" placeholder="Filter..." class="form-control input-sm"></th>
						<th>
							<button class="btn btn-sm btn-danger clearColumnFilters columnFiltersEnabled" style="display: inline-block;">
								<i class="fa fa-eraser"></i> Clear Filters</button>
						</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="folder" items="${libraryFolders}">
						<tr data-element-id="<c:out value="${folder.category_id}" />" 
							data-library-type="<c:out value="${libraryType}" />" 
							data-folder-name="<c:out value="${folder.attr_categoryname}" />">
							<td><c:out value="${folder.attr_categoryname}" /></td>
							<td>
								<div class="btn-group">
								  <button type="button" class="btn btn-default edit-library-folder-action">
								  	<i class="fa fa-pencil-square" aria-hidden="true"></i>
								  	Edit
								  </button>
								  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    <span class="caret"></span>
								    <span class="sr-only">Toggle Dropdown</span>
								  </button>
								  <ul class="dropdown-menu">
								    <li><a href="#" class="delete-library-folder-action">Delete</a></li>
								  </ul>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
	  </div>
	</div>
</div>
<div class="modal-footer">
	<%--<button class="btn btn-primary pull-left add-library-item-action" data-library-type="<c:out value="${libraryType}" />">--%>
		<%--<i class="fa fa-plus-square-o" aria-hidden="true"></i>--%>
		<%--&nbsp;Add image--%>
	<%--</button>--%>
		<button class="btn btn-primary pull-left add-library-folder-action" data-library-type="<c:out value="${libraryType}" />">
			<i class="fa fa-plus-square-o" aria-hidden="true"></i>
			&nbsp;Add Folder
		</button>
		<c:if test = "${libraryType == 'images'}">
		<button class="btn btn-primary pull-left select-library-item-action" data-library-type="<c:out value="${libraryType}" />">
			<i class="fa fa-plus-square-o" aria-hidden="true"></i>
			&nbsp;Select Multiple
		</button>
		</c:if>
	<button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> Close</button>
</div>

<!-- Froala Editor -->
<script src="/application/assets/plugins/froala-editor/custom/config-proposalFreetext.js"></script>