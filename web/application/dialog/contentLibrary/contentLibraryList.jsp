<style>
    .library {
        display: flex;
        flex-direction: column;
        height: 100%;
    }
    .library .content {
        height: 100%;
    }
    .labels {
        display: flex;
        padding: 15px 0;
        border-bottom: 1px solid #DDD;
        min-height: 50px;
    }
    .root-library {
        display: none;
        height: 100%;
    }
    .sub-folders {
        position: relative;
        height: 100%;
        padding: 0px;
        background: #FFF;
    }
    .item-icon {
        flex: 0.4;
    }
    .item-name, .item-modified {
        flex: 1.9;
    }
    .item-type, .item-actions, .item-thumbnail {
        flex: 0.9;
        text-align: center;
    }
    .item-type, .item-upload {
        flex: 0.9;
        position: relative;
    }
    .item-template, .item-thumbnail {
        display: none;
    }
    .item-thumbnail img{
    	max-width: 120px;
    	max-height: 90px;
    }
    #file-manager.library-videos.level-2 .item-thumbnail, #file-manager.library-videos.level-3 .item-thumbnail{
    	display: block;
    }
    .content .item {
        display: flex;
        align-items: center;
        padding: 5px 0;
        border-bottom: 1px solid #DDD;
        cursor: pointer;
    }
    .content .item-icon {
        text-align: center;
    }
    .content .item-actions .fa-lg {
        line-height: 1em;
    }
    .item:hover {
        /* background: rgba(0, 0, 0, 0.21); */
    }
    .item.ds-selected {
        background: rgba(0, 0, 0, 0.21);
    }

    .item-actions [type="button"] {
        -webkit-appearance: inherit;
    }

    .dropzone .dz-preview .dz-progress {
        top: 0;
        left: 0;
        right: 0;
    }


    .dropzone .dropzone-placeholder {
        display: none;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(12, 12, 12, 0.8);
        pointer-events: none;
    }

    .dropzone.dropzone-visible .dropzone-placeholder {
        display: block;
    }

    .dropzone .dropzone-placeholder .placeholder-information {
        position: absolute;
        top: 30px;
        right: 30px;
        bottom: 30px;
        left: 30px;
        border: 10px dashed #2196F3;
        border-radius: 46px;
        vertical-align: middle;
        pointer-events: none;
    }

    .dropzone .dropzone-placeholder .placeholder-information p {
        text-align: center;
        font-size: 48px;
        color: #2196F3;
        top: 50%;
        position: absolute;
        left: 50%;
        transform: translate(-50%);
        line-height: 0px;
        pointer-events: none;
    }

</style>

<div class="library">
    <div class="labels">
        <div class="item-icon"></div>
        <div class="item-name">Name</div>
        <div class="item-type">Type</div>
        <div class="item-thumbnail">Thumbnail</div>
        <div class="item-modified"> </div>
        <div class="item-actions">Actions</div>
    </div>
    <div class="content">
        <div class="sub-folders dropzone">
        </div>
        <div class="item-template">
            <div class="item">
                <div class="item-icon"></div>
                <div class="item-name"></div>
                <div class="item-type"></div>
                <div class="item-thumbnail"></div>
                <div class="item-modified"></div>
                <div class="item-actions dropdown">
                    <div class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v fa-lg"></i>
                    </div>
                    <div class="dropdown-menu">
                        <button class="dropdown-item buttonEditTemplate" type="button">Edit</button>
                        <button class="dropdown-item buttonCopyTemplate" type="button">Copy</button>
                        <button class="dropdown-item buttonInfoTemplate" id="buttonInfoTemplate" type="button">Info</button>
                        <button class="dropdown-item buttonDownloadItem" type="button">Download</button>
                        <button class="dropdown-item buttonDeleteItem" type="button">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>