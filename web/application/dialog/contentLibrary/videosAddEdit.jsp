<div id="videos-library-add-edit-modal" class="modal fade add-edit-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="display: none;">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Modal title</h4>
    </div>
    <form id="add-edit-videos-form" method="post" enctype="multipart/form-data">
	    <div class="modal-body">
	      	  <input type="hidden" name="elementId">
	      	  <div class="form-group">
			    <label for="folder">Folder</label>
			    <select name="folder" class="form-control"  required></select>
			    <div class="form-group hidden" id="folderNameId">
						<label for="assetName">Folder Name</label> <input type="text"
							class="form-control" name="newFolder" placeholder="Enter new folder name here...">
					</div>
			  </div>
			  <hr>
	      	  <div class="form-group">
			    <label for="assetName">Video Title</label>
			    <input type="text" class="form-control" name="assetName"  required>
			  </div>
	      	  <div class="form-group" style="position:relative;">
			    <label for="caption">Caption</label>
			    <input type="text" class="form-control" name="caption" style="padding-right: 54px;" required>
				  <label class="qc-label-info" id="warningText">Your video can have accompanying text displayed when your customer views your proposal. Please add this text to the Caption field above.</label>
				  <label class="qc-label-info" id="warningLimit" style="position: absolute; top: 33px; right: 7px;">0/255</label>
			  </div>
	      	  <div class="form-group">
			    <label for="videoLink">Video Link</label>
			    <input type="text" id="videoLinkUrl" class="form-control" name="videoLink"  required>
			  	<label class="qc-label-info">It's super easy to embed videos in QuoteCloud, simply copy and paste either a Vimeo or YouTube URL in the above Video Link field.  For example, https://www.youtube.com/watch?v=YnGmedwOYuQ would be a URL for a YouTube video.</label>
			  </div>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      <button class="btn btn-primary videoSubmit">Save changes</button>
	    </div>
    </form>
  </div><!-- /.modal-content -->
 </div>