<div id="text-library-add-edit-modal" class="modal fade add-edit-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="display: none;">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Modal title</h4>
    </div>
    <form id="add-edit-text-form" method="post" enctype="multipart/form-data">
	    <div class="modal-body">
	      	  <input type="hidden" name="elementId">
	      	  <div class="form-group">
			    <label for="folder">Folder</label>
			    <select name="folder" class="form-control"></select>
					<div class="form-group hidden" id="folderNameId">
						<label for="assetName">Folder Name</label> <input type="text"
							class="form-control" name="newFolder" placeholder="Enter new folder name here...">
					</div>

				</div>
			  <hr>
	      	  <div class="form-group">
			    <label for="assetName">Asset Name</label>
			    <input type="text" class="form-control" name="assetName">
			  </div>
	      	  <div class="form-group">
			    <label for="textBlock">Text Block</label>
			    <!-- remove ck -->
			    <!-- <textarea id="textBlockTextarea" class="form-control" name="textBlock" rows="10"></textarea> -->
			    <div class="panel-froala">
				</div>
			  </div>
			  <div class="checkbox">
			    <label>
			      <input type="checkbox" name="lockContent"> Lock Content
			    </label>
			  </div>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      <button type="submit" class="btn btn-primary">Save changes</button>
	    </div>
    </form>
  </div><!-- /.modal-content -->
 </div>