<div id="content-library-folder-delete-modal" class="modal fade" data-backdrop="static" data-keyboard="false"
	tabindex="-1" style="display: none;">
	<div class="modal-content">
		<form id="folder-delete-form">
			<input type="hidden" name="folderId">
			<input type="hidden" name="libraryType">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Delete Folder</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Folder to be Deleted</label>
					<input class="form-control" type="text" id="folderToBeDeleted" disabled>
				</div>
				<div class="form-group">
					<label for="transferTo">Transfer Contents to Existing Folder</label>
					<select class="form-control" name="transferTo">
					</select>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
			</div>
		</form>
	</div>
	<!-- /.modal-content -->
</div>
