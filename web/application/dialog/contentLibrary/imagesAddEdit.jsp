<div id="images-library-add-edit-modal" class="modal modal-image-detail fade add-edit-modal" data-backdrop="static"
    data-keyboard="false" tabindex="-1" style="background: rgba(85, 85, 85, 0.0);box-shadow: 0 3px 9px rgba(0, 0, 0, 0.0);">
    <div class="modal-content">
        <div class="modal-header">
            <div class="titleBar">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="fal fa-times-circle"></i>
				</button>
            </div>
            <h4 class="modal-title">Modal title</h4>
        </div>


        <form id="add-edit-images-form" method="post" enctype="multipart/form-data">

            <div class="modal-body image-info">
              <div class="">
                <div class="col-md-12 image-panel" style="min-height:369px;">
                
				<div class="row">
					<label for="image"></label>
					<div id="image-container">
					<!-- 	<img id="thumbnailImage" alt="thumbnail" src="" style="display: none" />
						<svg height="0" width="0">
							<defs>
								<clipPath id=svgPath>
									<rect x="0" y="0" width="100" height="100" opacity="1" />
								</clipPath>
							</defs>
						</svg> -->
						
						
						<img id="thumbnailImage" src="" alt="Selected Image">
						<svg height="0" width="0">
							<defs>
							<clipPath id="svgPath">
								
							</clipPath>
							</defs>
						</svg>
						
					</div>
				</div>
				<div class="image-action-container">
				  	<input type="file" id="imageUploadButton" accept="image/*" style="display:none;">
				  					  	
                    <div id="image-alert" class="alert alert-dismissible alert-info">
					  <strong>Heads up!</strong> The above image illustrates the crop that will be performed. 
					  The dimension of the new image will be <span id="cropped-size" class="label label-primary"><span id="img-width"></span> x <span id="img-height"></span> px</span>. 
					  To save this image cropped please click the <strong>"Save changes"</strong> button. 
					  To change your mind click the <strong>"Close"</strong> button to reupload the original image again.
					</div>
					
					<div id="image-alert-browser" style="display:none" class="alert alert-dismissible alert-danger">
					  <strong>Warning!</strong> Internet Explorer or Microsoft Edge users might experience some issues with the crop feature. Please, use Chrome or Firefox.
					</div>
				</div>
                  
                <div id="jCropModalImage" class="modal fade" tabindex="-1">
			      <div class="modal-header">
			        <div class="titleBar">
			          	<button type="button" data-dismiss="modal"
								aria-hidden="true" class="close cancel cancelImageUpload">
							<i class="fal fa-times-circle"></i>
						</button>
			        </div>
			        <h2 class="modal-title">Crop</h2>
			      </div>
			      <div class="modal-body">
			        <img id="JcropPhotoFrame" src=""/>
			         <div class="alert alert-danger uploadErrorSize" role="alert">
			        	The image is too big. (Max 6MB)
			        </div>
			        
			      </div>
			      <div class="modal-footer">
					<i class="fa fa-spin fa-spinner uploadLoading"></i>
			        <button type="button" class="btn btn-danger cancelImageUpload">
			          <i class="fa fa-times"></i> Close
			        </button>
			        <button type="submit" class="btn btn-success uploadFile disabled">
			          <i class="qc qc-edit"></i> Save
			        </button> 
			      </div>
			  	</div>
	
                </div>
                <div class="col-md-12">
                	<div id="form-container">
							<div class="row">
								<div class="col-md-6">
									<input type="hidden" id="elID" name="elementId">
									<div class="form-group">
										<label for="folder">Folder</label> <select name="folder"
											class="form-control"></select>
										<div class="form-group hidden" id="folderNameId">
											<label for="assetName">Folder Name</label> <input type="text"
												class="form-control" name="newFolder"
												placeholder="Enter new folder name here...">
										</div>
									</div>
								</div>
								<div class="form-group col-md-6">
									<label for="assetName">Image Title</label> <input type="text"
										class="form-control" name="assetName">
								</div>
							</div>
						</div>
						<div class="row">
<%--						<div class="col-md-6">--%>
<%--							&lt;%&ndash;<button type="button" id="chooseImage" class="btn btn-primary">&ndash;%&gt;--%>
<%--								&lt;%&ndash;<i class="fa fa-cloud-upload"></i> Upload&ndash;%&gt;--%>
<%--							&lt;%&ndash;</button>&ndash;%&gt;--%>
<%--&lt;%&ndash;							<button type="button" id="cropImage" class="btn btn-primary" style="display:none">&ndash;%&gt;--%>
<%--&lt;%&ndash;								<i class="fa fa-crop"></i> Crop&ndash;%&gt;--%>
<%--&lt;%&ndash;							</button>&ndash;%&gt;--%>
<%--						</div>--%>
						<div class="col-md-12">
							<button type="button" class="delete-library-item-action-details btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
							<button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> Close</button>
							<button disabled="true" id="save-changes" type="submit" class="btn pulse-button-info  btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Save</button>
						</div>
					 </div>
                    <div id="infoContainer">
                    	<div class="row">
							<div class="col-md-4">
								<p><strong>File name: </strong><span id="fileName"></span></p>
                        		<p><strong>File type: </strong><span id="fileType"></span></p>
							</div>
							<div class="col-md-4">
								<p><strong>Uploaded on: </strong><span id="uploadedOn"></span></p>
                        		<p><strong>File size: </strong><span id="fileSize"></span></p>
							</div>
							<div class="col-md-4">
								<p><strong>Dimensions: </strong><span id="dimensions"></span></p>
							</div>
                    	</div>
                	</div>
						<%--<div class="form-group">--%>
							<%--<div class="checkbox">--%>
								<%--<label> <input type="checkbox" id="displayTitle"--%>
									<%--name="displayTitle"> Display the image title in the--%>
									<%--sales proposal?--%>
								<%--</label>--%>
							<%--</div>--%>
						<%--</div>--%>
					</div>  
              <%--<div class="">--%>
                <%--<div class="col-md-12" style="margin-top: 20px;">--%>
                  <%--<div class="form-group">--%>
                    <%--<label for=description>Text Content</label> --%>
                    <%--<!-- <textarea class="form-control" name="description" rows="10" id="imageLibDescTextArea"></textarea> -->--%>
                    <%--<div class="panel-froala" style="list-style-position:inside;"></div>--%>
                  <%--</div>--%>
                <%--</div>--%>
              <%--</div>--%>
              <div style="clear: both;"></div>
            </div>


        </form>
    </div>


	<div id="imageDeleteModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">

		<div class="alert alert-block alert-danger fade in" style="margin: 0">
			<h4 class="alert-heading"><i class="fa fa-warning"></i> Confirmation</h4>
			<p>
				Are you sure you would like to delete the image?
			</p>
			<p>
				<a class="btn btn-success imageDeleteConfimed" href="#">
					<i class="fa fa-check"></i> &nbsp;Yes
				</a>
				<button type="button" class="btn btn-danger cancelDeleteButton">
					<i class="fa fa-times"></i>
					&nbsp;No
				</button>
			</p>
		</div>
	</div>

	<div id="imageMultiDeleteModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">

		<div class="alert alert-block alert-danger fade in" style="margin: 0">
			<h4 class="alert-heading"><i class="fa fa-warning"></i> Confirmation</h4>
			<p>
				Are you sure you would like to delete these images?
			</p>
			<p>
				<a class="btn btn-success imageMultiDeleteConfimed" href="#">
					<i class="fa fa-check"></i> &nbsp;Yes
				</a>
				<button type="button" class="btn btn-danger cancelMultiDeleteButton">
					<i class="fa fa-times"></i>
					&nbsp;No
				</button>
			</p>
		</div>
	</div>

    <!-- /.modal-content -->
</div>
</div>