<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page import="java.util.List"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.PaymentDataAccess"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.maestrano.controller.SsoController"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.corporateinteractive.qcloud.subscription.SubscriptionPlan"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="java.util.Hashtable"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>

<%
UserAccountDataAccess uada = new UserAccountDataAccess();

String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
CategoryData company = uada.getUserCompanyForUserId(userId);

List<ElementData> payments = new PaymentDataAccess().getPayments(company.getId(), null);
%>

<div class="modal-header">
	<h2 class="modal-title">Payment Transactions List</h2>
</div>

<div class="modal-body">
  <table class="table table-bordered table-hover table-full-width" id="table-transactions" data-module="Transactions" width="100%">
    <thead>
      <tr>
        <th>Transaction Date Time</th>
        <th>Cycle</th>
        <th>Plan</th>
        <th>Due</th>
        <th>Action</th>
        <th>CR Adjustment (refunds)</th>
        <th>CR Charged</th>
        <th>CR Closing  Balance</th>
        <th>PP Charged</th>
        <th>Tx ID</th>
        <th>Notes</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      <% for(ElementData payment : payments){ %>
      <tr>
        <td><%= payment.getString(PaymentDataAccess.E_TX_DATE_TIME) %></td>
        <td><%= payment.getString(PaymentDataAccess.E_CYCLE) %></td>
        <td><%= payment.getString(PaymentDataAccess.E_PLAN) %></td>
        <td><%= payment.getString(PaymentDataAccess.E_DUE) %></td>
        <td><%= payment.getString(PaymentDataAccess.E_ACTION) %></td>
        <td><%= payment.getString(PaymentDataAccess.E_CR_ADJUST) %></td>
        <td><%= payment.getString(PaymentDataAccess.E_CR_CHARGED) %></td>
        <td><%= payment.getString(PaymentDataAccess.E_CR_BALANCE) %></td>
        <td><%= payment.getString(PaymentDataAccess.E_PAYPAL_CHARGED) %></td>
        <td><%= payment.getString(PaymentDataAccess.E_HEADLINE) %></td>
        <td><%= payment.getString(PaymentDataAccess.E_NOTES) %></td>
        <td><%= payment.getString(PaymentDataAccess.E_STATUS) %></td>
      </tr>
      <% } %>
    </tbody>
  </table>
</div>
<div class="modal-footer">
	<button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
		<i class="fa fa-times"></i> Close
	</button>
</div>
<!-- 
<script src="/application/js/form-validation/modal-change-subscription.js"></script>
 -->