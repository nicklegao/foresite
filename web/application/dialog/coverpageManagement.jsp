<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.CoverPageDataAccess"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService"%>
<%@page import="java.util.Map"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="java.util.Hashtable"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.db.OptionsDataAccess"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="java.util.List"%>
<%@ page import="java.util.LinkedHashMap" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor"
	prefix="compress"%><compress:html enabled="true"
	compressJavaScript="true" compressCss="true">
<%
UserAccountDataAccess uada = new UserAccountDataAccess();
CoverPageDataAccess cpda = new CoverPageDataAccess();

String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
CategoryData company = uada.getUserCompanyForUserId(userId);
Hashtable<String, String> userDetails = uada.getUserWithId(userId);
String companyName = uada.getUserCompanyNameForUserId(userId);
	List<LinkedHashMap<String, String>> coverPages = new ImageLibraryService().getCoverpageThumbs(company.getId(), true);
ModuleAccess ma = ModuleAccess.getInstance();
%>
<div id="coverpage-management">
	<div class="modal-header line bottom">
		<div class="titleBar">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">
				<i class="fal fa-times-circle"></i>
			</button>
		</div>
		<h2 class="modal-title">Cover Pages</h2>
	</div>
	<div class="modal-body">
		<div>
			<input type="file" id="coverUploadButton" accept="image/*" style="display:none;">
		</div>
	    <div id="coverPicker"></div>
	</div>
	<div class="modal-footer" style="clear: both;">
		<i class="fa fa-spin fa-spinner uploadLoading"></i>
		<button type="button" data-dismiss="modal" class="btn btn-danger cancelUpload">
	        <i class="fa fa-times"></i> Close
        </button>
		<button type="button" class="btn btn-primary choseFile">
			<i class="fa fa-cloud-upload"></i> Upload
		</button>
    </div>
	
	<div id="jCropModal" class="modal fade" tabindex="-1">
	      <div class="modal-header">
	        <div class="titleBar">
	          	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="fal fa-times-circle"></i>
				</button>
	        </div>
	        <h2 class="modal-title">Crop</h2>
	      </div>
	      <div class="modal-body">
	        <img id="JcropPhotoFrame" src=""/>
	        <div class="alert alert-danger uploadError" role="alert">
	        	The image width must be bigger than 793px and the height bigger than 1122px.
	        </div>
	      </div>
	      <div class="modal-footer">
			<div class="orient-check">
				<div class="col-sm-8">
					<input id="landscapeMode" type="checkbox" class="js-switch"
						name="landscapeMode" value="on" />
				</div>
			</div>
			<i class="fa fa-spin fa-spinner uploadLoading"></i>
	        <button type="button" data-dismiss="modal" class="btn btn-danger cancelUpload">
	          <i class="fa fa-times"></i> Close
	        </button>
	        <button type="submit" class="btn btn-success uploadFile disabled">
	          <i class="qc qc-edit"></i> Save
	        </button> 
	      </div>
	  </div>
	
	<div id="coverpageDeleteModal" 
	        class="modal fade" 
	        tabindex="-1" 
	        data-backdrop="static" 
	        data-keyboard="false">
	  
	    <div class="alert alert-block alert-danger fade in" style="margin: 0">
	      <h4 class="alert-heading"><i class="fa fa-warning"></i> Confirmation</h4>
	      <p>
	        Are you sure you would like to delete this cover page?
	      </p>
	      <p>
	        <a class="btn btn-success coverpageDeleteConfirmed" href="#">
	          <i class="fa fa-check"></i>
	          &nbsp;Yes
	        </a>
	        <button type="button" class="btn btn-danger" data-dismiss="modal">
	          <i class="fa fa-times"></i>
	          &nbsp;No
	        </button>
	      </p>
	    </div>
	</div>
	
</div>

<link rel="stylesheet" href="/application/assets/plugins/bootstrap-fileinput/fileinput.css" />
<link rel="stylesheet" href="/application/assets/plugins/Jcrop/css/jquery.Jcrop.min.css" />

<script	src='/application/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js'></script>
<script src="/application/assets/plugins/bootstrap-fileinput/plugins/canvas-to-blob.js"></script>
<script src="/application/assets/plugins/bootstrap-fileinput/fileinput.js"></script>
<script src="/application/assets/plugins/Jcrop/js/jquery.Jcrop.min.js"></script>
<script	src="/application/assets/plugins/jQuery-slimScroll-master/jquery.slimscroll.min.js"></script>
<script src="/application/js/dashboardCoverpageManagement.js"></script>

</compress:html>