<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%@page import="java.io.File"%>
<%@page import="java.io.StringReader"%>
<%@page import="java.text.DecimalFormat" %>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Hashtable"%>
<%@page import="org.apache.commons.lang.StringUtils" %>
<%@page import="org.owasp.esapi.ESAPI" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.EmailTemplateDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.EmailBuilder" %>
<%@page import="au.corporateinteractive.utils.FreeMarkerUtils" %>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleHelper" %>
<%@page import="freemarker.core.ParseException" %>
<%@page import="freemarker.template.Configuration" %>
<%@page import="freemarker.template.Template" %>
<%@page import="freemarker.template.TemplateException" %>
<%@page import="freemarker.template.TemplateExceptionHandler" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.util.TravelDocsStatus" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor"
           prefix="compress" %>
<compress:html enabled="true"
               compressJavaScript="true" compressCss="true">
    <%
        String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
        UserAccountDataAccess uada = new UserAccountDataAccess();
        CategoryData company = uada.getUserCompanyForUserId(userId);
        String userCompanyName = uada.getUserCompanyNameForUserId(userId);
        String proposalId = request.getParameter("proposal");
        if (proposalId == null || proposalId.length() == 0)
        {
            response.setStatus(400);
            return;
        }
        TravelDocsDataAccess pda = new TravelDocsDataAccess();

        Hashtable<String, String> newestProposalRevision = pda.getNewestProposalRevisionById(proposalId);

        Proposal proposal = pda.loadTravelDoc(proposalId);
        Content conInfo = proposal.findGeneratedContentOfType("con-info");

        String proposalTitle = conInfo.getMetadata().get("proposalTitle");
        String companyName = conInfo.getMetadata().get("companyName");
        String contactName = conInfo.getMetadata().get("contactName") + (StringUtils.isBlank(conInfo.getMetadata().get("contactLastName")) ? "" : (" " + conInfo.getMetadata().get("contactLastName")));
        String contactAddress = conInfo.getMetadata().get("contactAddress");
        String contactEmail = conInfo.getMetadata().get("contactEmail");
        String ccEmails = conInfo.getMetadata().get("cc-emails");

        boolean readyToSend = (contactName!=null && contactName.length() > 0)
                && (contactEmail!=null && contactEmail.length() > 0);

        EmailTemplateDataAccess etda = new EmailTemplateDataAccess();
        String subject;
        String element;
        String status = company.getString(uada.EMAIL_STATUS_DEFAULT);
        boolean did = Integer.parseInt(newestProposalRevision.get(TravelDocsDataAccess.E_REVISION)) <= 1;
        if ((status != null && "2".equals(status)) || (status != null && status.equalsIgnoreCase("")) || ((status != null && "1".equals(status) && ((null != newestProposalRevision && Integer.parseInt(newestProposalRevision.get(TravelDocsDataAccess.E_REVISION)) <= 1) && !"other".equals(status)))))
        {
            subject = "Your TravelDoc from " + userCompanyName + " [" + new DecimalFormat("#0000000").format(Integer.parseInt(proposalId)) + "]";
            element = EmailBuilder.PROPOSAL_READY_EMAIL_TEMPLATE_ELEMENT;
        } else if ((company.getString(uada.EMAIL_STATUS_DEFAULT) != null && "other".equals(company.getString(uada.EMAIL_STATUS_DEFAULT)))) {
            subject = company.getString(uada.EMAIL_STATUS_STRING) + " -- Proposal ID: [" + new DecimalFormat("#0000000").format(Integer.parseInt(proposalId)) + "]";
            element = EmailBuilder.PROPOSAL_READY_EMAIL_TEMPLATE_ELEMENT;
        }
        else
        {
            subject = "Your Revised TravelDoc from " + userCompanyName + " [" + new DecimalFormat("#0000000").format(Integer.parseInt(proposalId)) + "]";
            element = EmailBuilder.PROPOSAL_REVISION_READY_EMAIL_TEMPLATE_ELEMENT;
        }
        String emailTemplate = etda.readContentWithUserId(company.getId(), element);

// Parse email template //
        Hashtable<String, String> proposalHT = pda.getTravelDocById(proposalId);

//process json style template interpolations with the actual template interpolations based off of database column names
        HashMap<String, String> jsonToDatabaseMap = EmailBuilder.preprocessProposalSendEmailTemplate(request, conInfo, proposalHT);
        TravelDocsStatus travelDocsStatus = TravelDocsStatus.withValue(proposalHT.get(TravelDocsDataAccess.E_PROPOSAL_STATUS));

        try
        {
            Configuration cfg = new Configuration();
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);
            Template eTemplate = new Template("email template",
                    new StringReader(emailTemplate),
                    cfg);

            emailTemplate = FreeMarkerUtils.processTemplateIntoString(eTemplate, jsonToDatabaseMap);
        }
        catch (ParseException e)
        {
            throw new Exception("Error parsing the template");
        }
        catch (TemplateException e)
        {
            throw new Exception("Error parsing the template: " + e.getMessage());
        }
    %>

    <link rel="stylesheet" href="/application/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css">

    <!-- froala editor -->
    <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala-editor/css/froala_style.min.css",request) %>" />
    <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala-editor/css/plugins/table.css",request) %>" />
    <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala-editor/css/froala_editor.pkgd.min.css",request) %>" />

    <div class="modal-header line bottom">
        <div class="titleBar">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fal fa-times-circle"></i>
            </button>
        </div>
        <h4 class="modal-title">Request Approval</h4>
    </div>

    <div id="sendProposalDialog" class="modal-body">

        <form id="proposalSendEmail">
            <div class="form-group">
                <label for="emailBody">Send a comment:</label>
                <div id="emailBody"></div>
            </div>
        </form>

        <% if (!readyToSend) { %>
        <div style="clear: both;" class="alert alert-danger">
            <strong>Oh snap!</strong> Your proposal is missing information. Please
            update the details above by editing the cover page.
        </div>
        <% } %>

    </div>

    <div class="modal-footer line top mg-envelope">
        <button type="button" data-dismiss="modal" class="btn btn-danger">
            Cancel</button>
        <% if (readyToSend) { %>
        <a href="#" proposalId="<%=proposalId %>"
           class="btn btn-success doRequestApprovalConfirmedAction"> Send Request </a>
        <% } %>
    </div>

    <script src="/application/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>

    <!-- Froala Editor -->
    <script src="/application/assets/plugins/froala-editor/js/froala_editor.pkgd.min.js"></script>
    <script src="/application/assets/plugins/froala-editor/js/plugins/table.min.js"></script>
    <script src="/application/assets/plugins/froala-editor/custom/config-proposalEmailText.js"></script>
    <script>
		$(document).ready(function() {

			setTimeout(function()
			{
				$('#emailBody').froalaEditor({
					key:'DLAHYKAJOEc1HQDUH==',
					enter: $.FroalaEditor.ENTER_P,
					toolbarSticky: true,
					scrollableContainer: '#sendProposalDialog'
				});
			},500);


		});
    </script>
</compress:html>