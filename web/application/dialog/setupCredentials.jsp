<div class="modal-header">
    <div class="titleBar">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
    </div>
    <h2 class="modal-title">Link Account</h2>
</div>

<form class="form-create-proposal form-horizontal" action="/api/proposal/create" method="POST">
    <div class="form-steps">
        <div class="modal-body">
            <div class="form-bootstrapWizard">
                <ul class="bootstrapWizard form-wizard three-steps">
                    <li class="active" data-target="#step1">
                        <a href="#tab1" data-toggle="tab"> <span class="step">1</span> <span class="title">Setup GDS</span> </a>
                    </li>
                    <li data-target="#step2">
                        <a href="#tab2" data-toggle="tab"> <span class="step">2</span> <span class="title">Setup Users</span> </a>
                    </li>
                    <li data-target="#step3">
                        <a href="#tab3" data-toggle="tab"> <span class="step">3</span> <span class="title">Setup Plan</span> </a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="tab-content" style="margin-top: 30px">
                <div class="tab-pane active" id="tab1">
                    <h3><strong>Step 1 </strong> - Setup GDS</h3>

                    <div class="form-group step1">
                        <label for="proposalTitle" class="col-sm-3 control-label">
                            Proposal Title:
                        </label>
                        <div class="col-sm-9">
                            <input type="text" id="proposalTitle" class="form-control" name="custProposalTitle" placeholder="Main title of this proposal">
                        </div>
                    </div>

                    <div class="form-group step2">
                        <label class="col-sm-3 control-label">
                            Color scheme:
                        </label>
                        <div class="col-sm-9">
                            <div id="colorPicker" class="btn-group btn-block" data-toggle="buttons">
                                <label class="btn btn-qcloud-design-palette1 active">
                                    <input type="radio" name="spectrumColor" value="palette1" checked> <i class="fa fa-check selectedIndicator"></i>
                                </label>
                                <label class="btn btn-qcloud-design-palette2">
                                    <input type="radio" name="spectrumColor" value="palette2"> <i class="fa fa-check selectedIndicator"></i>
                                </label>
                                <label class="btn btn-qcloud-design-palette3">
                                    <input type="radio" name="spectrumColor" value="palette3"> <i class="fa fa-check selectedIndicator"></i>
                                </label>
                                <label class="btn btn-qcloud-design-palette4">
                                    <input type="radio" name="spectrumColor" value="palette4"> <i class="fa fa-check selectedIndicator"></i>
                                </label>
                            </div>
                            <label class="qc-label-info">This colour palette can be changed in the "Edit Styles" option in the Configuration menu.</label>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab2">
                    <h3><strong>Step 2</strong> - Setup Users</h3>
                    <div class="form-group step4">
                        <label for="contactEmail" class="col-sm-3 control-label">
                            Contact Email:
                        </label>
                        <div class="col-sm-9">
                            <input type="email" id="contactEmail" class="form-control non-filled" name="custContactEmail" placeholder="Email Address">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="contactName" class="col-sm-3 control-label">
                            Contact Name:
                        </label>
                        <div class="col-sm-9">
                            <div style="width:49.5%;float:left;">
                                <input type="text" id="contactName" class="form-control non-filled step5" name="custContactName" placeholder="First name of the recipient">
                            </div>
                            <div style="width:49.5%;float:right;">
                                <input type="text" id="contactLastName" class="form-control non-filled step6" name="custContactLastName" placeholder="Last name of the recipient">
                            </div>
                        </div>
                    </div>
                    <div class="form-group step7">
                        <label for="companyName" class="col-sm-3 control-label">
                            Company Name:
                        </label>
                        <div class="col-sm-9">
                            <input type="text" id="companyName" class="form-control non-filled" name="custCompanyName" placeholder="Name of the company this proposal is for">
                        </div>
                    </div>
                    <div class="form-group step8">
                        <label for="contactAddress" class="col-sm-3 control-label">
                            Address:
                        </label>
                        <div class="col-sm-9">
                            <input type="text" id="contactAddress" class="form-control non-filled" name="custContactAddress" placeholder="Address of the recipient">
                        </div>
                    </div>
                    <div class="form-group step9">
                        <label for="cc-emails" class="col-sm-3 control-label">
                            Additional Email(s):
                        </label>
                        <div class="col-sm-9">
                            <input id="cc-emails" type="text" class="tags" name="custCcEmails">
                            <p style="margin:0">Type email address and press enter</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div for="cc-emails" class="col-sm-12">
                            <input class="table-checkbox" type="checkbox" name="saveContact" value="show" /> Save contact details
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab3">
                    <h3><strong>Step 3</strong> - Proposal Template</h3>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            Please choose a template:
                        </label>
                        <div class="col-sm-12">
                            <!-- <input id="template-chooser" type="hidden" class="tags ignore" name="chosenTemplate"/> -->
                            <div class="templates boxed">
                                <ul class="templates-list">
                                    <li class="templates-item active starred" data-id="0">
                                        <input type="radio" name="chosenTemplate" value="0" checked>  <span class="templates-item-star" title="Mark as starred"><i class="fa fa-check"></i></span>
                                        <span class="templates-item-from">Blank</span>
                                        <span class="messages-item-subject">As a administrator you can create a blank template. </span>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="modal-footer">
            <ul class="pager wizard no-margin">
                <li class="previous disabled">
                    <a href="javascript:void(0);" class="btn btn-lg btn-default previousButton"> Previous </a>
                </li>
                <li class="next">
                    <a href="javascript:void(0);" class="btn btn-lg btn-default nextButton"> Next </a>
                </li>
                <li>
                    <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
                </li>
                <li class="next finish" style="display:none;">
                    <button type="submit" class="btn btn-success nextButton"><i class="fa fa-save"></i> Create </button>
                </li>
            </ul>
            <!--
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Create</button> -->
        </div>
    </div>
</form>