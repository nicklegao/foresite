<%@page import="java.math.BigDecimal"%>
<%--<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.DiskUsageDataAccess"%>--%>
<%@page import="java.text.NumberFormat"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.StripeService"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.control.UserController"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.maestrano.controller.SsoController"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.corporateinteractive.qcloud.subscription.SubscriptionPlan"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="java.util.Hashtable"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>

<%
UserAccountDataAccess uada = new UserAccountDataAccess();

String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
CategoryData company = uada.getUserCompanyForUserId(userId);

String currentSubscription = company.getString(UserAccountDataAccess.C_PRICING_PLAN);
String firstAvailSubscription = SubscriptionPlan.getCurrentSubscriptionPlansWithoutTrial(company.getString(uada.C_ADDRESS_COUNTRY)).get(0).getName();
Hashtable<String, String> user = uada.getUserWithId(userId);
int currentUserCount = uada.getUserCountInCompanyWithTeamId(user.get(UserAccountDataAccess.E_CATEGORY_ID));

//Javascript flags
boolean[] trialExpiredVars = UserController.isTrialPeriodOver(session);
boolean trialExpired = trialExpiredVars[0];
boolean trialAlmostExpired = trialExpiredVars[1];
boolean planCancelled = StripeService.isCompanyCancelled(company);

SubscriptionPlan starterPlan = SubscriptionPlan.getCurrentSubscriptionPlansWithoutTrial(company.getString(uada.C_ADDRESS_COUNTRY)).get(0);
SubscriptionPlan addUserPlan = SubscriptionPlan.getAdditionalUserPlan(company.getString(uada.C_ADDRESS_COUNTRY));
SubscriptionPlan addDataPlan = SubscriptionPlan.getAdditionalDataPlan(company.getString(uada.C_ADDRESS_COUNTRY));
int currentUserAmount = uada.getUserCountInCompanyWithCompanyId(company.getId());
addUserPlan.setQuantity(currentUserAmount - starterPlan.getMaxUsers());
//double diskUsageGB = new DiskUsageDataAccess().getCurrentDiskUsageGB(company.getId());
double maxDiskUsageGB = starterPlan.getMaxData() + addUserPlan.getMaxData() * addUserPlan.getQuantity();
int newDataPlanQty = new BigDecimal(0.0 - maxDiskUsageGB).divide(new BigDecimal(addDataPlan.getMaxData())).setScale(0, BigDecimal.ROUND_CEILING).intValue();
addDataPlan.setQuantity(newDataPlanQty);

NumberFormat nf = new DecimalFormat("0.00");
%>
<style>
.plan-selector{
  border:1px solid gray;
  padding:6px;
  margin:6px;
  min-height: 80px;
  cursor: pointer;
}
.plan-selector:hover{
  background-color: lightblue; 
}
.plan-selector.disabled{
  color: #888;
  background-color: lightgrey;
}
.plan-selector.active:before {
    display: block;
    position: absolute;
    content: "\f00c";
    color: #fff;
    right: 27px;
    top: 7px;
    font-family: FontAwesome;
    z-index: 2;
}
.plan-selector.active{
  background-color: lightblue; 
}
.plan-selector.active:after {
    width: 0;
    height: 0;
    border-top: 35px solid #13b5ea;
    border-left: 35px solid rgba(0,0,0,0);
    position: absolute;
    display: block;
    right: 21px;
    top: 6px;
    content: "";
    z-index: 1;
}
.plan-selector.current{
  background-color: lightgreen; 
}
</style>
<div class="modal-header">
  <h2 class="modal-title">
  <% if (planCancelled) { %>
  Welcome back!
  <% }else if (trialExpired) { %>
  We hope you enjoyed your free trial!
  <% }else{ %>
  Select Subscription
  <% } %>
  </h2>
</div>

<form class="form-change-subscription" action="/api/changeSubscription" method="POST">
<input name="currentPlan" value="<%= currentSubscription %>" type="hidden">
<input name="pricingPlan" value="<%= firstAvailSubscription %>" type="hidden">
<div class="modal-body">
	<% if (planCancelled) { %>
	<p>
		We are glad to see you coming back.
	</p>
	<% 
	}else{ 
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy, hh:mm aaa");
		Date expiryDate = company.getDate(UserAccountDataAccess.C_TRIAL_PLAN_EXPIRY);
	%>
	<p>Your free trial <%= trialExpired ? "ended" : "will end"%> on: <%=sdf.format(expiryDate) %></p>
	<p>
		Hi there! We hope you have enjoyed using QuoteCloud, and have seen how easy life can become with the great features in our product.
		<% if(trialExpired){ %> 
		Unfortunately, your free trial has now finished,
		<% } %> 
		<% if(trialAlmostExpired){ %> 
		Your free trial will finish soon,
		<% } %> 
		we hope you will continue to enjoy using QuoteCloud with a monthly subscription.
	</p>
	<% } %>
	
	
	<p>
    Our pricing for QuoteCloud is <%= addUserPlan.getCurrencySymbol() + nf.format(addUserPlan.getPrice()) %> per user (with a storage allowance of <%= (int)(addUserPlan.getMaxData() * 1000) %>MB per user, shared across all your users).
    The minimum purchase is our <%= starterPlan.getMaxUsers()%> user Starter Plan which costs just <%= starterPlan.getCurrencySymbol()+ nf.format(starterPlan.getPrice()) %>. 
    </p>
	<% if(addUserPlan.getQuantity() > 0){ %>
	<p>
	You currently have <%= currentUserAmount %> users created if you would like to delete some unwanted user accounts simply click on the "Manage Users" button below. 
	</p>
	<% } %>
	<% if(addDataPlan.getQuantity() > 0){ %>
	<p>
	We notice you have used <%= 0.0 %>GB of storage, which means you have been really busy well done on the huge sales effort!
	Extra data costs <%= addDataPlan.getCurrencySymbol()+ nf.format(addDataPlan.getPrice()) %> per <%= (int)addDataPlan.getMaxData()%>GB, 
	we will allocate <%= (int)(addDataPlan.getMaxData() * addDataPlan.getQuantity()) %>GB extra storage to your <%= starterPlan.getMaxUsers()%> user subscription plan.
	</p>
	<% } %>
	<p>
	Your monthly charges will be <%= starterPlan.getCurrencySymbol()+ nf.format(starterPlan.getPrice() + addDataPlan.getPrice() * addDataPlan.getQuantity() + addUserPlan.getPrice() * addUserPlan.getQuantity()) %>
	<% if(addUserPlan.getQuantity() > 0 || addDataPlan.getQuantity() > 0){ %>
	 (
	  <% if(addUserPlan.getQuantity() > 0){ %>
	  	<%= currentUserAmount %> users
	  <% } %>
	  <% if(addDataPlan.getQuantity() > 0){ %>
	   	<% if(addUserPlan.getQuantity() > 0){ %>
	   	and 
	   	<% } %>
	   	<%= (int)(starterPlan.getMaxData() + addDataPlan.getMaxData() * addDataPlan.getQuantity()) %>GB total storage
	  <% } %>
	   
	 ) 
	<% } %> 
	, please click the "Accept" button below to begin your subscription to QuoteCloud.
    This subscription can be cancelled at any time - there is no lock in contract :)
	</p>
	
	<div class="form-horizontal hidden">
       <div class="form-group">
		<% for (SubscriptionPlan plan : SubscriptionPlan.getCurrentSubscriptionPlansWithoutTrial(company.getString(uada.C_ADDRESS_COUNTRY))) { %>
        <div class="col-sm-6">
          <div class="footer-head waves-effect waves-button waves-float plan-selector <%=plan.willExceedUserLimit(currentUserCount) ? "disabled" : "" %> <%=StringUtils.equalsIgnoreCase(plan.getName(), currentSubscription) ? "active current" : "" %>" data-plan="<%=plan.getName() %>">
            <div class="pricing-plan-title">
              <div><span class="name"><%=plan.getName() %></span>
                <i class="price"><%= plan.getCurrencySymbol()+ new DecimalFormat("0.00").format(plan.getPrice()) %></i>
                <span class="currency"><%= plan.getCurrencyCode()   %></span>
                <span class="period">/ Monthly</span>
              </div>
            </div>
            <div class="pricing-plan-description">
              <span><%= plan.getMaxUsers() %> Users</span>
              <span><%= plan.getMaxData() %>GB Storage</span>
            </div>
          </div>
        </div>
		<% break;} %>
       </div>
     </div>     
</div>
<div class="modal-footer">
    
	<% if (!trialExpired) { %>
	<button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
		<i class="fa fa-times"></i> Close
	</button>
	<% } else {%>
	<button type="button" id="manageUserBtn" class="btn btn-info" onclick="usersManagementAction()">
		<i class="fa fa-file-text fa-fw"></i> Manage Users &amp; Roles
	</button>
	<button type="button" id="logoutBtn" class="btn btn-danger" data-dismiss="modal">
		<i class="fa fa-times"></i> No thanks
	</button>
	<% } %>
	<button type="submit" class="btn btn-success">
       	<i class="fa fa-check"></i> Accept
    </button>
</div>
</form>

<script src="/application/js/form-validation/modal-change-subscription.js"></script>