<%@page import="com.stripe.model.Customer"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.service.StripeService"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.corporateinteractive.qcloud.subscription.SubscriptionPlan"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ page import="au.net.webdirector.common.datalayer.client.ModuleAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.base.db.entity.MultiOptions" %>
<%
UserAccountDataAccess uada = new UserAccountDataAccess();
String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
CategoryData companyData = uada.getUserCompanyForUserId(userId);
String companyName = uada.getUserCompanyNameForUserId(userId);
Customer stripeCustomer = StripeService.getInstance().retrieveStripeCustomer(companyData);
String cardNumber = StripeService.getLast4CardNumber(stripeCustomer);
%>
<div id="account-settings">
    <style>
    input.form-control {
      padding: 0 10px;
    }
    select.form-control {
      padding: 0 6px;
    }
    .smart-form *, .smart-form *:after, .smart-form *:before {
      box-sizing: border-box;
    }
    .smart-form .col-block {
        min-height: 1px;
        padding-right: 15px;
        padding-left: 15px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        clear: both;
    }
    /**
     * The CSS shown here will not be introduced in the Quickstart guide, but shows
     * how you can use CSS to style your Element's container.
     */
    .StripeElement {
      background-color: white;
      padding: 9px;
      border: 1px solid #ccc;
      min-height: 42px;
    }
    
    .StripeElement--focus {
      box-shadow: 0 1px 3px 0 #cfd7df;
    }
    
    .StripeElement--invalid {
      border-color: #fa755a;
    }
    
    .StripeElement--webkit-autofill {
      background-color: #fefde5 !important;
    }
    
    #card-element-placeholder .card-number{
      color: #32325d;
      line-height: 24px;
      font-size: 16px;
      height: 24px;
    }
    p.note{
      font-size: 11px;
    }    
    img.stripe-seal{
      width: 130px;
      display: block;
      float: left;
      margin-right: 4px;
    }
    </style>
	<form class="form-account-settings" method="POST">
	<div class="modal-header">
		<div class="titleBar">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">
				<i class="fal fa-times-circle"></i>
			</button>
		</div>
		<h2 class="modal-title">Settings</h2>
	</div>
	<div class="modal-body smart-form">
		<div class="form-horizontal">
		
		<div class="company-details">
		    <header>Company Details</header>
			<div class="row">
				<section class="col col-6">
					<label for="companyName" class="label"> Company Name</label> 
				      <input value="<%=ESAPI.encoder().encodeForHTML(companyData.getString("attr_categoryname"))%>" 
				      	type="text" class="form-control" id="companyName" name="companyName" placeholder="Company Name">
				</section>
				<section class="col col-6">
					<label for="abnacn" class="label"> Company ID/Registration Number</label> 
                      <input type="text" class="form-control" name="abnacn" id="abnacn" placeholder="Company ID/Registration Number" 
                      value="<%=ESAPI.encoder().encodeForHTML(companyData.getString("attr_abnacn"))%>">
				</section>
				<section class="col col-xs-12">
					<label for="addressLine1" class="label"> Address Line 1</label> 
				      <input value="<%=ESAPI.encoder().encodeForHTML(companyData.getString("attr_addressLine1"))%>" 
				      	type="text" class="form-control" id="addressLine1" name="addressLine1" placeholder="Address Line 1">
				</section>
				<section class="col col-xs-12">
					<label for="addressLine2" class="label"> Address Line 2</label> 
                      <input type="text" class="form-control" name="addressLine2" id="addressLine2" placeholder="Address Line 2" 
                      value="<%=ESAPI.encoder().encodeForHTML(companyData.getString("attr_addressLine2"))%>">
				</section>
				<section class="col col-6">
					<label for="citySuburb" class="label"> City/Suburb</label> 
				      <input value="<%=ESAPI.encoder().encodeForHTML(companyData.getString("attr_citySuburb"))%>" 
				      	type="text" class="form-control" id="citySuburb" name="citySuburb" placeholder="City/Suburb">
				</section>
				<section class="col col-6">
					<label for="postcode" class="label"> Postcode</label> 
                      <input type="text" class="form-control" name="postcode" id="postcode" placeholder="Postcode" 
                      value="<%=ESAPI.encoder().encodeForHTML(companyData.getString("attr_postcode"))%>">
				</section>
				<section class="col col-6">
					<label for="state" class="label"> State</label> 
				      <input value="<%=ESAPI.encoder().encodeForHTML(companyData.getString("attr_state"))%>" 
				      	type="text" class="form-control" id="state" name="state" placeholder="State">
				</section>
				<section class="col col-6">
					<label for="country" class="label"> Country</label> 
                    <select name="country" class="form-control" disabled>
  						<% 
                            for(String country : SubscriptionPlan.getSupportCountries()){
                          	if(!StringUtils.equalsIgnoreCase(country, companyData.getString("attrdrop_country"))){
                                continue;
                              }
                            %>
                              <option value="<%= country %>"><%= country %></option>
                            <% 
                            } 
                          %>
  					</select>
				</section>
			</div>
		</div>
			<div class="payment-details">
				<header>Payment Details</header>
				<div class="row">
					<section class="col-block" id="card-section">
	  				    <label for="card-element" class="label">
	                      Credit or debit card
	                    </label>
	                    <div id="card-element-placeholder" class="StripeElement StripeElement--empty" style="display:none;">
	                    <% if(StringUtils.isNotBlank(cardNumber)){ %>
	                      <span class="card-number">XXXX XXXX XXXX <%= cardNumber %></span>
	                      <button type="button" class="btn btn-info btn-xs pull-right" id="btn-load-stripe"><i class="fa fa-pencil"></i> Update</button>
	                    <% } else { %>  
	                      <button type="button" class="btn btn-info btn-xs pull-right" id="btn-load-stripe"><i class="fa fa-pencil"></i> Add</button>
	                    <% } %>  
	                    </div>
	                
	                    <!-- Used to display form errors -->
	                    <div id="card-element-wrapper">
	                      <div id="card-element">
	                        <!-- a Stripe Element will be inserted here. -->
	                      </div>
	                      <div id="card-errors" class="pull-left"></div>
	                      <button type="button" id="cancel-btn" class="btn btn-danger btn-xs pull-right"><i class="fa fa-ban"></i> Cancel</button>
	                    </div>
					</section>
	                <section class="col-block">
	                  <p class="note">
	                    <img src="/application/images/Stripe-Seal.png" class="stripe-seal">
	                    Your payments are managed &amp; processed using the Stripe payment gateway.  No Credit Card information is held by QuoteCloud directly.
	                  </p>
	                </section>
					<%
						MultiOptions manages = ModuleAccess.getInstance().getMultiOptions(companyData, uada.C_BILLING_USERS,
								uada.USER_MODULE);
						String emailList = StringUtils.join(manages.getValues(), ",");
					%>
					<section class="col col-xs-12">
						<label for="payment-user-email-list" class="label"> Users who receive invoice related emails:</label>
						<input id="payment-user-email-list" type="text" class="tags" value="<%=emailList%>" name="payment-user-email-list">
					</section>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" id="closeForm" class="btn btn-danger"
			data-dismiss="modal">
			<i class="fa fa-times"></i> Close
		</button>
		<button type="submit" class="btn btn-success">
        	<i class="fa fa-save"></i> Save
      	</button>
	</div>
	</form>
</div>
<script>
var stripe_pk = '<%= StripeService.getPublishableKey() %>';
</script>
<script src="/application/js/form-validation/abn.js"></script>
<script src="/application/js/form-validation/acn.js"></script>
<script src="/application/js/form-validation/modal-account-settings.js"></script>
