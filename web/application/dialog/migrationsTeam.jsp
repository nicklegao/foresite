<%@page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page import="java.util.List"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.MaestranoUserAccountDataAccess"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="java.util.Hashtable"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
<div id="migration-team">
<%
UserAccountDataAccess uada = new UserAccountDataAccess();
String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
String companyId = uada.getUserCompanyForUserId(userId).getId();

TUserRolesDataAccess urda = new TUserRolesDataAccess();
UserPermissions up = urda.getPermissions(userId);

//Get the team id we are modifying
String oldTeamId = request.getParameter("id");
CategoryData oldTeam = uada.getTeamForTeamId(oldTeamId);
List<CategoryData> teams = uada.getCompanyTeams(companyId);
List<ElementData> users = ModuleAccess.getInstance().getElementsByParentId(uada.USER_MODULE, oldTeamId);

if(up.hasManageUsersPermission())
{
%>
  <form class="form-migration-team" action="" method="POST">
  	<input type="hidden" id="id" name="id" value="<%= oldTeamId %>" />
    <div class="modal-header">
      <div class="titleBar">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
      </div>
      <h2 class="modal-title">Teams</h2>
    </div>
    <div class="modal-body">
      <div class="form-group">
      	<label for="teamName">Migrate users in team <%= oldTeam.getName() %> to:</label>
      		<select class="form-control" name="newId">
	      	<% 	
	      	for(CategoryData team: teams) { 
     			if(!team.getId().equals(oldTeamId)) { 
     		%>
	      		<option value="<%= team.getId() %>"><%= team.getName() %></option>	
	      	<% 	
	      		}	
	      	} 
	      	%>
      		</select>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
        <i class="fa fa-times"></i> Close
      </button>
      <button type="submit" class="btn btn-success">
        <i class="fa fa-save"></i> Save
      </button>
    </div>
  </form>
<% 
} else {
%>
    <div class="modal-header">
      <div class="titleBar">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
      </div>
      <h2 class="modal-title">Teams</h2>
    </div>
    <div class="modal-body">
      You do not have permissions modify this team
    </div>
    <div class="modal-footer">
      <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
        <i class="fa fa-times"></i> Close
      </button>
    </div>
<%
}
%>
  <script src="/application/js/form-validation/modal-migration-team.js"></script>
</div>
</compress:html>