<%@page import="java.util.Hashtable"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
<div id="notification-management">
<%
UserAccountDataAccess uada = new UserAccountDataAccess();

String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
Hashtable<String, String> userDetails = uada.getUserWithId(userId);

boolean emailDailySummary = userDetails.get(UserAccountDataAccess.E_EMAIL_DAILY_SUMMARY).equals("1");
boolean emailNewComment = userDetails.get(UserAccountDataAccess.E_EMAIL_NEW_COMMENT).equals("1");
boolean emailHandover = userDetails.get(UserAccountDataAccess.E_EMAIL_HANDOVER).equals("1");
boolean emailStatusChange = userDetails.get(UserAccountDataAccess.E_EMAIL_STATUS_CHANGE).equals("1");
boolean emailProposalOpened = userDetails.get(UserAccountDataAccess.E_EMAIL_OPENED).equals("1");
%>
  <form class="form-notification-management" action="/api/updateNotifications" method="POST">
    <div class="modal-header">
      <div class="titleBar">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
      </div>
      <h2 class="modal-title">Notifications</h2>
    </div>
    <div class="modal-body">
      <p>Please choose the email notifications that you wish to receive:</p>
      
      <label style="margin: 3px 8px;" for="emailNewComment">
        <input type="checkbox"
              name="emailNewComment"
              id="emailNewComment"
              <%=emailNewComment?"checked":"" %>>
        <strong>New comment from customer</strong><br/>
        An email alerting you that a customer has added a question/comment into the discussion section of your proposal.
      </label><br/>
      
      <label style="margin: 3px 8px;" for="emailHandover">
        <input type="checkbox"
              name="emailHandover"
              id="emailHandover"
              <%=emailHandover?"checked":"" %>>
        <strong>Handover confirmation</strong><br/>
        An email alerting you that a proposal has been handed over to/from you.
      </label><br/>
      
      <label style="margin: 3px 8px;" for="emailStatusChange">
        <input type="checkbox"
              name="emailStatusChange"
              id="emailStatusChange"
              <%=emailStatusChange?"checked":"" %>>
        <strong>Proposal status change</strong><br/>
        An email alerting you every time your proposal changes status from sent to read to comment received or accepted.
      </label><br/>
      
      <label style="margin: 3px 8px;" for="emailProposalOpened">
        <input type="checkbox"
              name="emailProposalOpened"
              id="emailProposalOpened"
              <%=emailProposalOpened?"checked":"" %>>
        <strong>Proposal Opened</strong><br/>
        An email alerting you every time your proposal is opened by the client or they try opening an old revision.
      </label><br/>
      
    </div>
    <div class="modal-footer">
      <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
        <i class="fa fa-times"></i> Close
      </button>
      <button type="submit" class="btn btn-success">
        <i class="fa fa-save"></i> Save
      </button>
    </div>
  </form>
  <script src="/application/assets/plugins/switchery/switchery.js"></script>
  <script src="/application/js/form-validation/modal-manage-notifications.js"></script>
</div>
</compress:html>