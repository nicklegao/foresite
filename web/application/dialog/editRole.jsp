<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions"%>
<%@page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager" %>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
<div id="edit-role">
<%

String roleId = request.getParameter("id");
String action = request.getParameter("action");

Boolean addCheck = action.equals("add");
Boolean editCheck;

UserPermissions up;
ElementData roleData;


UserAccountDataAccess uada = new UserAccountDataAccess();
String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
String companyId = uada.getUserCompanyForUserId(userId).getId();

TUserRolesDataAccess urda = new TUserRolesDataAccess();
up = urda.getPermissions(userId);
roleData = urda.getRoleForId(roleId);
CategoryData roleCategoryData = urda.getRoleFolderCompanyId(companyId);

editCheck = action.equals("edit") && roleData != null && roleCategoryData.getString(urda.C_COMPANY_ID).equals(companyId);


if(up.hasManageUsersPermission() && (editCheck || addCheck))
{
%>
  <form class="form-edit-role" action="" method="POST">
  	<% if(editCheck) { %>
  	<input type="hidden" id="id" name="id" value="<%= roleData.getId() %>" />
  	<% } %>
  	<input type="hidden" id="action" name="action" value=<%= action %> />
    <div class="modal-header">
      <div class="titleBar">
        <button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">
			<i class="fal fa-times-circle"></i>
		</button>
      </div>
      <h2 class="modal-title">Roles</h2>
    </div>
    <div class="modal-body smart-form">
      <div class="form-group" style="margin-bottom: 10px;">
      	<label for="roleName" style="font-size: 14px;font-weight: normal;color: black;">Role Name</label>
      	<input class="form-control" style="padding-left: 5px; width:50%;" id="roleName" name="roleName" value="<%= editCheck ? roleData.getName() : "" %>" />
      </div>
      <div class="form-horizontal">
<%--      	<ul class="nav nav-tabs" style="bordor-bottom:1px solid #ddd;"role="tablist">--%>
<%--		    <li class="active"><a href="#proposalStyling" aria-controls="proposalStyling" role="tab" data-toggle="tab">System Config</a></li>--%>
<%--		    &lt;%&ndash;<li><a href="#users" aria-controls="users" role="tab" data-toggle="tab">Users & workflow</a></li>&ndash;%&gt;--%>
<%--&lt;%&ndash;		    <li><a href="#contentLibraries" aria-controls="contentLibraries" role="tab" data-toggle="tab">Content Libraries</a></li>&ndash;%&gt;--%>
<%--&lt;%&ndash;		    <li><a href="#systemConfiguration" aria-controls="systemConfiguration" role="tab" data-toggle="tab">System Config</a></li>&ndash;%&gt;--%>
<%--&lt;%&ndash;		    <li><a href="#pricing" aria-controls="pricing" role="tab" data-toggle="tab">Pricing</a></li>&ndash;%&gt;--%>
<%--			&lt;%&ndash;<li><a href="#dashboardSettings" aria-controls="dashboardSettings" role="tab" data-toggle="tab">Dashboard</a></li>&ndash;%&gt;--%>
<%--&lt;%&ndash;			<li><a href="#integrationSettings" aria-controls="integrationSettings" role="tab" data-toggle="tab">Integrations</a></li>&ndash;%&gt;--%>
<%--&lt;%&ndash;		    <li><a href="#analyticsSettings" aria-controls="#analyticsSettings" role="tab" data-toggle="tab">Analytics</a></li>&ndash;%&gt;--%>
<%--		    <!-- <li><a href="#workflow" aria-controls="workflow" role="tab" data-toggle="tab">Workflow</a></li> -->--%>
<%--		  </ul>--%>
      
      <div class="tab-content">
      	<div role="tabpanel" class="tab-pane active" id="proposalStyling">
      		<%-- <div class="row" style="padding:5px 15px;">
				<section class="col col-12">
					<input type="checkbox" id="manageTemplates" name="manageTemplates" <%=editCheck && roleData.getBoolean(TUserRolesDataAccess.MANAGE_TEMPLATES) ? "checked='checked' value='1'"  : "value='0'"%>>
					<label for="manageTemplates">Access to manage <strong>Templates &amp; Content Libraries</strong></label>
				</section>
			</div> --%>
      		<div class="row" style="padding:5px 15px;">
				<section class="col col-12">
					<input type="checkbox" id="manageProjects" name="manageProjects" <%=editCheck && roleData.getBoolean(TUserRolesDataAccess.MANAGE_PROJECTS) ? "checked='checked' value='1'"  : "value='0'"%>>
					<label for="manageTemplates">Access to manage <strong>Projects</strong></label>
				</section>
			</div>
			<div class="row" style="padding:5px 15px;">
				<section class="col col-12">
					<input type="checkbox" id="manageCompanySettings" name="manageCompanySettings" <%= editCheck && roleData.getBoolean(TUserRolesDataAccess.MANAGE_SETTINGS) ? "checked='checked' value='1'" : "value='0'" %> />
					<label for="manageCompanySettings">Access to manage System Settings</label>
				</section>
			</div>
			<div class="row" style="padding:5px 15px;">
				<section class="col col-12">
					<input type="checkbox" id="manageUsers" name="manageUsers" <%= editCheck && roleData.getBoolean(TUserRolesDataAccess.MANAGE_USERS) ? "checked='checked' value='1'" : "value='0'" %> />
					<label for="manageUsers">Access to User Management</label>
				</section>
			</div>
      	</div>
    </div>
    </div>
    <div class="modal-footer">
		<div class="row" style="width: 100%;margin: 0;">
			<div class="col text-right">
				<button type="button" id="closeForm" class="cancel-button"
						data-dismiss="modal">
					Close
				</button>
				<button type="submit" class="success-button" style="color: white !important; margin-left: 10px;">
					Save
				</button>
			</div>
		</div>
    </div>
  </form>
<% 
} else {
%>
    <div class="modal-header">
      <div class="titleBar">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
      </div>
      <h2 class="modal-title">Roles</h2>
    </div>
    <div class="modal-body">
      You do not have permissions modify this role
    </div>
    <div class="modal-footer">
      <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
       Close
      </button>
    </div>
<%
}
%>
  <script src="/application/js/form-validation/modal-edit-role.js"></script>
</div>
</compress:html>