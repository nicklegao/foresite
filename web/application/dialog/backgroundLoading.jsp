<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
<div id="foreground-loading" 
      class="modal fade" 
      tabindex="-1" 
      data-backdrop="static" 
      data-keyboard="false">

  <div class="alert alert-block alert-default fade in" style="margin: 0; color: #000; opacity: 1 !important;">
    <h4 class="alert-heading"><i class="fa fa-cog fa-spin"></i> <span id="what-we-do">Processing...</span></h4>
    <p>
      Please wait while we figure things out.
    </p>
    
    <div class="progress qcloud-progress-bar">
      <div class="progress-bar"
            role="progressbar" 
            aria-valuenow="0" 
            aria-valuemin="0" 
            aria-valuemax="100"
            style="width: 0%; background-color:#13b5ea;">
      </div>
      <div class="progress-bar-text"></div>
    </div>
  </div>

</div>
</compress:html>