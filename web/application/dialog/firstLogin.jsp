<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.StaticTextDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.subscription.SubscriptionPlan"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.EmailTemplateDataAccess"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Comparator"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.Locale"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page	import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page	import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.OptionsDataAccess"%>
<%@page import="java.util.List"%>
<%

UserAccountDataAccess uada = new UserAccountDataAccess();
String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
CategoryData company = uada.getUserCompanyForUserId(userId);
Locale[] locales = Locale.getAvailableLocales();
Comparator<Locale> localeComparator = new Comparator<Locale>() {
	public int compare(Locale locale1, Locale locale2) {
		return locale1.getDisplayCountry().compareTo(locale2.getDisplayCountry());
	}
};
Arrays.sort(locales, localeComparator);
%>
<div id="first-login" class="form-steps">
	<div class="modal-header">
		<div class="titleBar">
			<!-- <button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">
				<i class="fal fa-times-circle"></i>
			</button> -->
		</div>
		<h2 class="modal-title">Hello, Welcome to QuoteCloud</h2>
	</div>
	<div class="modal-body">
		<div class="">
            <div class="form-bootstrapWizard">
              <ul class="bootstrapWizard form-wizard two-steps">
                <li class="active" data-target="#step1"> <a href="#fl-tab1" data-toggle="tab"> <span class="step">1</span> <span class="title">Your Locale</span> </a> </li>
                <li data-target="#step2"> <a href="#fl-tab2" data-toggle="tab"> <span class="step">2</span> <span class="title">Sample Templates</span> </a> </li>
                <%-- 
                <li data-target="#step3"> <a href="#tab3" data-toggle="tab"> <span class="step">3</span> <span class="title">Colours</span> </a> </li>
                <li data-target="#step4"> <a href="#tab4" data-toggle="tab"> <span class="step">4</span> <span class="title">Extra Information</span> </a> </li>
                 --%>
              </ul>
              <div class="clearfix"></div>
            </div>
            
            <div class="tab-content" style="margin-top: 30px">
              <div class="tab-pane active" id="fl-tab1">
	            <form class="form-settings-management" method="POST">
    			<div class="row smart-form">
    				<section class="col col-xs-12">
    					<label for="locale" class="label"> Please check your locale settings, so we can display the correct currency symbol : </label>
    					<select class="form-control" id="locale" name="locale" placeholder="Please choose">
    						<option>
    						</option>
                    		<%    
                				for (Locale locale : locales){
                					if(!StringUtils.isEmpty(locale.getDisplayCountry()) && !StringUtils.isEmpty(locale.getDisplayCountry(Locale.ENGLISH))) {
              				%>
                					<option value="<%=ESAPI.encoder().encodeForHTMLAttribute(locale.toLanguageTag()) %>" <%= locale.toLanguageTag().equalsIgnoreCase(company.getString(TUserAccountDataAccess.C1_LOCALE)) ?"selected":"" %>>
                						<%=ESAPI.encoder().encodeForHTML(locale.getDisplayCountry())%> (<%=ESAPI.encoder().encodeForHTML(locale.getDisplayLanguage())%>)
                					</option>
              				<%	}
    						}	%>
                		</select>
    				</section>
    			</div>
    			<div class="row smart-form">
    				<section class="col col-xs-12">
    					<label for="locale" class="label"> Please check your time zone settings, so we can display the correct date : </label>
    					<select class="form-control" id="timeZone" name="timeZone" placeholder="Please choose">
            			</select>
    				</section>
    			</div>
                </form>
			  </div>
              <div class="tab-pane" id="fl-tab2">
                <jsp:include page="getSampleTemplateList.jsp">
                  <jsp:param value="first-login" name="context"/>
                </jsp:include>
              </div>
			</div>
		
		</div>
	</div>
	<div class="modal-footer">
		<ul class="pager wizard no-margin">
			<li class="previous disabled">
				<button type="button" href="javascript:void(0);" class="btn btn-default"><i class="fa fa-arrow-left"></i> Previous </button>
			</li>
			<li class="cancel">
				<button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
			</li>
			<li class="next">
				<button type="button" href="javascript:void(0);" class="btn btn-success"><i class="fa fa-arrow-right"></i> Next </button>
			</li>
			<li class="next finish" style="display:none;">
				<button type="button" class="btn btn-success"><i class="fa fa-check"></i>  Finish </button>
			</li>
		</ul>
	</div>
</div>

<script src="/application/js/form-validation/modal-first-login.js"></script>

</compress:html>