<style>
.import-modal .hidden-on-importing{
  display: initial;
}
.import-modal .show-on-importing{
  display: none;
}

.import-modal.importing .hidden-on-importing{
  display: none;
}
.import-modal.importing .show-on-importing{
  display: initial;
}
</style>
<div id="contacts-import-modal" class="modal fade import-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="display: none;">
  <div class="modal-content">
    <div class="modal-header">
      <div class="titleBar">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="fal fa-times-circle"></i>
        </button>
      </div>
      <h2 class="modal-title">Import Contacts</h2>
    </div>
    <form id="import-contacts-form" method="post" enctype="multipart/form-data" action="/api/contacts/import">
      <div class="modal-body">
        <div class="hidden-on-importing">
          <div class="form-group">
            <label for="csvUpload">Import File</label>
            <div>
              <button type="button" class="btn btn-success" id="csvUpload-btn"><i class="fa fa-cloud-upload"></i> Choose a CSV File</button>
              <span id="csvUpload-filename"></span>
            </div>
            <input type="file" id="csvUpload" name="csvUpload" accept=".csv" style="display:none;">
          </div>
          <hr/>
          <div class="form-group">
            <label>
              <input type="checkbox" class="form-control" name="deleteAll"> Delete all data before import
            </label>
          </div>
          <div class="form-group">
            <label>
              <input type="checkbox" class="form-control" name="overWrite"> Overwrite existing contacts with matching email address
            </label>
          </div>
          <hr/>        
        </div>
        <div class="show-on-importing">
          <h4 class="alert-heading"><i class="fa fa-cog fa-spin"></i> <span class="stage-text">Importing</span>...</h4> 
          <p> Please wait while we are importing your data. </p> 
          <div class="progress qcloud-progress-bar"> 
            <div class="progress-bar"
                  role="progressbar" 
                  aria-valuenow="0" 
                  aria-valuemin="0" 
                  aria-valuemax="100"
                  style="width: 0%; background-color:#13b5ea;">
            </div>
            <div class="progress-bar-text"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary hidden-on-importing">Import</button>
      </div>
    </form>
  </div><!-- /.modal-content -->
 </div>