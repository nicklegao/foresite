<%--<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.DiskUsageDataAccess"%>--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ page import="au.corporateinteractive.qcloud.subscription.SubscriptionPlan" %>

<%
	DiskUsageDataAccess duda = new DiskUsageDataAccess();
	UserAccountDataAccess uada = new UserAccountDataAccess();
	String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
	CategoryData companyData = uada.getUserCompanyForUserId(userId);
	ElementData diskUsageData = duda.getDiskUsageForCompanyId(companyData.getString("Category_id"));

	SubscriptionPlan subscriptionPlan = SubscriptionPlan.getPlanByName(companyData.getString(UserAccountDataAccess.C_ADDRESS_COUNTRY), companyData.getString(UserAccountDataAccess.C_PRICING_PLAN));
%>
<script>
    var coverData = Math.round(<%=ESAPI.encoder().encodeForHTML(diskUsageData.getString("ATTRFLOAT_coverPage")) %>);
    var imageData = Math.round(<%=ESAPI.encoder().encodeForHTML(diskUsageData.getString("ATTRFLOAT_imageLibrary")) %>);
    var pdfData = Math.round(<%=ESAPI.encoder().encodeForHTML(diskUsageData.getString("ATTRFLOAT_pdf")) %>);
    var textData = Math.round(<%=ESAPI.encoder().encodeForHTML(diskUsageData.getString("ATTRFLOAT_text")) %>);
    var videoData = Math.round(<%=ESAPI.encoder().encodeForHTML(diskUsageData.getString("ATTRFLOAT_videoLibrary")) %>);
    var productData = Math.round(<%=ESAPI.encoder().encodeForHTML(diskUsageData.getString("ATTRFLOAT_products")) %>);
    var proposalAttchmentData = Math.round(<%=ESAPI.encoder().encodeForHTML(diskUsageData.getString("ATTRFLOAT_proposalAttachment")) %>);
    var proposalData = Math.round(<%=ESAPI.encoder().encodeForHTML(diskUsageData.getString("ATTRFLOAT_proposals")) %>);

    var totalUsedData = (coverData + imageData + pdfData + textData + videoData + productData + proposalAttchmentData + proposalData);
    var totalStorage = Math.round(<%=subscriptionPlan.getMaxData()%> * 1024);

	var totalBar = totalStorage;
	if (totalUsedData > totalStorage) {
		totalBar = totalUsedData;
	}
</script>
<div id="disk-usage">
	<div class="modal-header">
		<div class="titleBar">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">
				<i class="fal fa-times-circle"></i>
			</button>
		</div>
		<h2 class="modal-title">Disk Usage</h2>
	</div>
	
	<div class="modal-body smart-form">
	
		<div class="form-horizontal" style="text-align: left;">
			<header class="dataTitle"></header>
			<div>
				<%if(diskUsageData!=null) { 
					String companyDateFormat = companyData.getString("ATTR_DateFormat");
					String createDateString = diskUsageData.getString("Create_Date");
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date createDate = sdf.parse(createDateString); 
					SimpleDateFormat csdf = new SimpleDateFormat(companyDateFormat);
				%>
				<div class="containerBlock" style="display: inline-flex;width: 100%; box-shadow: 0 1px 3px rgba(0,0,0,.12), 0 1px 2px rgba(0,0,0,.24); background: white; margin-top: 10px;">
					<div id="diskUsageGraph" style="width: 610px;height:70px;background:red;padding: 10px; padding-bottom: 0px"></div>
				</div>

				<div class="containerBlock" style="display: inline-flex;width: 100%">

					<div class="row" style="margin-right: -15px; margin-left: -15px;">
						<div class="datamaterialBlock dataStatSquare" style="margin-left: 5px;">
							<p style="padding-top: 10px">PDF Usage</p>
							<p class="dataStatTopText" id="pdfMBSize"></p>
							<p class="dataStatPercentageText" id="pdfPercentTotal"></p>
							<p class="dataStatBottomText">of<br>total usage</p>
							<div id="pdf_donuts" style="width: 10em;height:10em;background:red;"></div>
						</div>

						<div class="datamaterialBlock dataStatSquare" style="margin-left: 5px;">
							<p style="padding-top: 10px">Image Usage</p>
							<p class="dataStatTopText" id="imageMBSize"></p>
							<p class="dataStatPercentageText" id="imagePercentTotal"></p>
							<p class="dataStatBottomText">of<br>total usage</p>
							<div id="image_donuts" style="width: 10em;height:10em;background:red;"></div>
						</div>
					</div>
					<div class="row" style="margin-right: -15px; margin-left: -15px;">

						<div class="datamaterialBlock dataStatSquare" style="margin-left: 20px;">
							<p style="padding-top: 10px">Video Usage</p>
							<p class="dataStatTopText" id="videoMBSize"></p>
							<p class="dataStatPercentageText" id="videoPercentTotal"></p>
							<p class="dataStatBottomText">of<br>total usage</p>
							<div id="video_donuts" style="width: 10em;height:10em;background:red;"></div>
						</div>

						<div class="datamaterialBlock dataStatSquare" style="margin-left: 20px;">
							<p style="padding-top: 10px">Cover Page Usage</p>
							<p class="dataStatTopText" id="coverMBSize"></p>
							<p class="dataStatPercentageText" id="coverPercentTotal"></p>
							<p class="dataStatBottomText">of<br>total usage</p>
							<div id="cover_donuts" style="width: 10em;height:10em;background:red;"></div>
						</div>
					</div>
					<div class="row" style="margin-right: -15px; margin-left: -15px;">

						<div class="datamaterialBlock dataStatSquare" style="margin-left: 20px;">
							<p style="padding-top: 10px">Product Usage</p>
							<p class="dataStatTopText" id="productMBSize"></p>
							<p class="dataStatPercentageText" id="productPercentTotal"></p>
							<p class="dataStatBottomText">of<br>total usage</p>
							<div id="product_donuts" style="width: 10em;height:10em;background:red;"></div>
						</div>

						<div class="datamaterialBlock dataStatSquare" style="margin-left: 20px;">
							<p style="padding-top: 10px">Proposal Usage</p>
							<p class="dataStatTopText" id="proposalMBSize"></p>
							<p class="dataStatPercentageText" id="proposalPercentTotal"></p>
							<p class="dataStatBottomText">of<br>total usage</p>
							<div id="proposal_donuts" style="width: 10em;height:10em;background:red;"></div>
						</div>
					</div>
					<div class="row" style="margin-right: -15px; margin-left: -15px;">

						<div class="datamaterialBlock dataStatSquare" style="margin-left: 20px;">
							<p style="padding-top: 10px">Text Usage</p>
							<p class="dataStatTopText" id="textMBSize"></p>
							<p class="dataStatPercentageText" id="textPercentTotal"></p>
							<p class="dataStatBottomText">of<br>total usage</p>
							<div id="text_donuts" style="width: 10em;height:10em;background:red;"></div>
						</div>

						<div class="datamaterialBlock dataStatSquare" style="margin-left: 20px;">
							<p style="padding-top: 10px">Attachment Usage</p>
							<p class="dataStatTopText" id="proposalAttachMBSize"></p>
							<p class="dataStatPercentageText" id="proposalAttachPercentTotal"></p>
							<p class="dataStatBottomText">of<br>total usage</p>
							<div id="proposalAttach_donuts" style="width: 10em;height:10em;background:red;"></div>
						</div>
					</div>

				</div>





				<%} else {%>
				<div class="text-center">No details available</div>
				<%} %>
			</div>
		</div>
		<p style="float: right;font-size: 9px;padding-right: 18px;padding-bottom: 12px;">The above usage statistics are updated daily</p>
	</div>
	<script src="/application/assets/plugins/echarts/echarts.common.min.js"></script>

	<script>

		$('.dataTitle').text("Total Allowance  - " + Math.round(((totalUsedData / (totalStorage)) * 100)) + '%' + " of " + Math.round(totalStorage/1024) + "Gb's used")
		// based on prepared DOM, initialize echarts instance
    var myChart = echarts.init(document.getElementById('diskUsageGraph'));

    // specify chart configuration item and data
    option = {
    legend: {
        show: false
    },
    grid: {
        top: '20%',
        left: '0',
        right: '0',
        bottom: '35%',
        width: '100%'
    },
        xAxis : [
            {
                type : 'value',
                axisLabel : {
                    show:true,
                    interval: 'auto',
                    margin: 8,
                    formatter: '{value} Mb',    // Template formatter!
                    textStyle: {
                        color: '#a9a9a9',
                        fontFamily: 'arial',
                        fontSize: 10,
                        fontStyle: 'normal',
                        fontWeight: 'bold'
                    }
                },
				max : totalBar
            }
        ],
    yAxis: {
    	type: '',
        data: ['']
    },
    series: [
        {
            name: 'Usage',
            type: 'bar',
            stack: 'usage',
            label: {
                normal: {
                    show: false,
                    position: 'insideRight'
                }
            },
            data: [totalUsedData]
        }
    ]
};

    var labelTop = {
        normal : {
            label : {
                color: '#a9a9a9',
                show : true,
                position : 'center',
                formatter : '{b}',
                textStyle: {
                    baseline : 'bottom'
                }
            },
            labelLine : {
                show : false
            },
            emphasis: {
                color: '#b9b9b9'
            }
        }
    };

    var labelFromatter = {
        normal : {
            label : {
                formatter : function (params){
					var total = Math.round(((params.value / totalStorage) * 100));
                    return total + '%'
                },
                textStyle: {
                    baseline : 'top'
                }
            }
        }
    };


    var labelBottom = {
        normal : {
            color: '#a9a9a9',
            label : {
                show : true,
                position : 'center'
            },
            labelLine : {
                show : false
            }
        },
        emphasis: {
            color: '#b9b9b9'
        }
    };

    //THIS IS FOR THE LOWER INDICATORS
    var pdfChart = echarts.init(document.getElementById('pdf_donuts'));

    // specify chart configuration item and data
    pdfOption = {
        legend: {
            show: false
        },
        series: [
            {
                name: 'PDF Usage',
                type: 'pie',
                radius: ['55%', '70%'],
                avoidLabelOverlap: false,
                stack: 'usage',
                itemStyle : labelFromatter,
                label: {
                    normal: {
                        show: false,
                        position: 'insideRight'
                    }
                },
                data: [{value:pdfData, name:'PDF Usage'},
                    {value:(totalUsedData - pdfData), name:'Free Space'}]
            }
        ]
    };

    //THIS IS FOR THE LOWER INDICATORS
    var imageChart = echarts.init(document.getElementById('image_donuts'));

    // specify chart configuration item and data
    imageOption = {
        legend: {
            show: false
        },
        series: [
            {
                name: 'Image Usage',
                type: 'pie',
                radius: ['55%', '70%'],
                avoidLabelOverlap: false,
                stack: 'usage',
                itemStyle : labelFromatter,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    }
                },
                data: [{value:imageData, name:'Image Usage'},
                    {value:(totalUsedData - imageData), name:'Free Space'}]
            }
        ]
    };

    //THIS IS FOR THE LOWER INDICATORS
    var videoChart = echarts.init(document.getElementById('video_donuts'));

    // specify chart configuration item and data
    videoOption = {
        legend: {
            show: false
        },
        series: [
            {
                name: 'Video Usage',
                type: 'pie',
                radius: ['55%', '70%'],
                avoidLabelOverlap: false,
                stack: 'usage',
                itemStyle : labelFromatter,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    }
                },
                data: [{value:videoData, name:'Video Usage'},
                    {value:(totalUsedData - videoData), name:'Free Space'}]
            }
        ]
    };

    //THIS IS FOR THE LOWER INDICATORS
    var coverChart = echarts.init(document.getElementById('cover_donuts'));

    // specify chart configuration item and data
    coverOption = {
        legend: {
            show: false
        },
        series: [
            {
                name: 'Cover Usage',
                type: 'pie',
                radius: ['55%', '70%'],
                avoidLabelOverlap: false,
                stack: 'usage',
                itemStyle : labelFromatter,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    }
                },
                data: [{value:coverData, name:'Cover Usage'},
                    {value:(totalUsedData - coverData), name:'Free Space'}]
            }
        ]
    };



    //THIS IS FOR THE LOWER INDICATORS
    var productChart = echarts.init(document.getElementById('product_donuts'));

    // specify chart configuration item and data
    productOption = {
        legend: {
            show: false
        },
        series: [
            {
                name: 'Product Usage',
                type: 'pie',
                radius: ['55%', '70%'],
                avoidLabelOverlap: false,
                stack: 'usage',
                itemStyle : labelFromatter,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    }
                },
                data: [{value:productData, name:'Product Usage'},
                    {value:(totalUsedData - productData), name:'Free Space'}]
            }
        ]
    };



    //THIS IS FOR THE LOWER INDICATORS
    var proposalChart = echarts.init(document.getElementById('proposal_donuts'));

    // specify chart configuration item and data
    proposalOption = {
        legend: {
            show: false
        },
        series: [
            {
                name: 'Proposal Usage',
                type: 'pie',
                radius: ['55%', '70%'],
                avoidLabelOverlap: false,
                stack: 'usage',
                itemStyle : labelFromatter,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    }
                },
                data: [{value:proposalData, name:'Proposal Usage'},
                    {value:(totalUsedData - proposalData), name:'Free Space'}]
            }
        ]
    };



    //THIS IS FOR THE LOWER INDICATORS
    var textChart = echarts.init(document.getElementById('text_donuts'));

    // specify chart configuration item and data
    textOption = {
        legend: {
            show: false
        },
        series: [
            {
                name: 'Text Usage',
                type: 'pie',
                radius: ['55%', '70%'],
                avoidLabelOverlap: false,
                stack: 'usage',
                itemStyle : labelFromatter,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    }
                },
                data: [{value:textData, name:'Text Usage'},
                    {value:(totalUsedData - textData), name:'Free Space'}]
            }
        ]
    };



    //THIS IS FOR THE LOWER INDICATORS
    var proposalAttachChart = echarts.init(document.getElementById('proposalAttach_donuts'));

    // specify chart configuration item and data
    proposalAttachOption = {
        legend: {
            show: false
        },
        series: [
            {
                name: 'Proposal Attachment Usage',
                type: 'pie',
                radius: ['55%', '70%'],
                avoidLabelOverlap: false,
                stack: 'usage',
                itemStyle : labelFromatter,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    }
                },
                data: [{value:proposalAttchmentData, name:'Proposal Attachment Usage'},
                    {value:(totalUsedData - proposalAttchmentData), name:'Free Space'}]
            }
        ]
    };

    // use configuration item and data specified to show chart
        $('#pdfMBSize').text(pdfData + 'mb');
        $('#pdfPercentTotal').text(Math.round(((pdfData / totalUsedData) * 100)) + '%');
    pdfChart.setOption(pdfOption);
    $('#imageMBSize').text(imageData + 'mb');
        $('#imagePercentTotal').text(Math.round(((imageData / totalUsedData) * 100)) + '%');
    imageChart.setOption(imageOption);
    $('#videoMBSize').text(videoData + 'mb');
        $('#videoPercentTotal').text(Math.round(((videoData / totalUsedData) * 100)) + '%');
    videoChart.setOption(videoOption);
    $('#coverMBSize').text(coverData + 'mb');
        $('#coverPercentTotal').text(Math.round(((coverData / totalUsedData) * 100)) + '%');
    coverChart.setOption(coverOption);
    $('#productMBSize').text(productData + 'mb');
        $('#productPercentTotal').text(Math.round(((productData / totalUsedData) * 100)) + '%');
    productChart.setOption(productOption);
    $('#proposalMBSize').text(proposalData + 'mb');
        $('#proposalPercentTotal').text(Math.round(((proposalData / totalUsedData) * 100)) + '%');
    proposalChart.setOption(proposalOption);
    $('#textMBSize').text(textData + 'mb');
        $('#textPercentTotal').text(Math.round(((textData / totalUsedData) * 100)) + '%');
    textChart.setOption(textOption);
    $('#proposalAttachMBSize').text(proposalAttchmentData + 'mb');
        $('#proposalAttachPercentTotal').text(Math.round(((proposalAttchmentData / totalUsedData) * 100)) + '%');
    proposalAttachChart.setOption(proposalAttachOption);


    // use configuration item and data specified to show chart
    myChart.setOption(option);
	</script>
</div>