<%@page import="au.corporateinteractive.qcloud.market.enums.Market"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page import="java.util.Vector"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.model.CoverPage"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.control.CoverPageController"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserAccountDataAccess"%>
<%@page
	import="au.net.webdirector.common.datalayer.base.db.entity.MultiOptions"%>
<%@page
	import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.service.ImageLibraryService"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="java.util.Hashtable"%>
<%@page
	import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="java.util.List"%>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.*" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.control.LibraryController" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor"
	prefix="compress"%><compress:html enabled="true"
	compressJavaScript="true" compressCss="true">
	<link rel="stylesheet" href="/application/css/image-picker.css" />
<link rel="stylesheet"
	href="/application/assets/plugins/switchery/switchery.css" />

<div id="edit-team">
	<%
		TUserAccountDataAccess uada = new TUserAccountDataAccess();
			String userId = (String) session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
			CategoryData companyInfo = uada.getUserCompanyForUserId(userId);
			String companyId = companyInfo.getId();

			UserPermissions up;
			TUserRolesDataAccess urda = new TUserRolesDataAccess();
			up = urda.getPermissions(userId);

			//Get library content
			LibraryController clmc = new LibraryController();
			CoverPageController cpc = new CoverPageController();
			TravelDocsDataAccess pda = new TravelDocsDataAccess();
			List<CategoryData> libraryProductFolders = clmc.getLibrary("products", companyId);
			List<CategoryData> libraryTextFolders = clmc.getLibrary("text", companyId);
			List<CategoryData> libraryImageFolders = clmc.getLibrary("images", companyId);
			List<CategoryData> libraryVideoFolders = clmc.getLibrary("videos", companyId);
//			List<CategoryData> libraryPDFFolders = clmc.getLibrary("pdf", companyId);
//			List<CategoryData> librarySpreadsheetFolders = clmc.getLibrary("spreadsheet", companyId);
			List<CoverPage> coverPageFiles = cpc.allCoverPageList(companyInfo, userId, companyId);
			Vector<Hashtable<String, String>> templatesFiles = pda.getTemplates(companyInfo.getId(), companyInfo.getName());

			//market app access
			boolean travelport = false;
			boolean arrivalGuide = false;
			boolean itineraryPlugins = true;

			//Get the team id we are modifying
			String teamId = request.getParameter("id");

			String action = request.getParameter("action");
			CategoryData team = uada.getTeamForTeamId(teamId);

			CategoryData sabreTeamCategory = new SabreDataAccess().getCategoryByTeamId(teamId);
			
			CategoryData travelPortTeamCategory = new TravelportDataAccess().getTravelportTeamByTeamId(teamId);

			String tab = request.getParameter("tabModule");

			//Get library switches
			boolean productActive = false;
			boolean textActive = false;
			boolean imageActive = false;
			boolean videoActive = false;
			boolean pdfActive = false;
			boolean spreadsheetActive = false;
			boolean coverPageActive = false;
			boolean templatesActive = false;
			String folderHelp = "By allocating category folders to a team, you are restricting access to only these folders.  If no folders are assigned, the team will have access to no content in this library.  Changes to this setting will not affect any existing proposals already created.";

			if (team != null)
			{
				//Get library switches
				productActive = team.getBoolean(TUserAccountDataAccess.E_TEAM_PRODUCT_ACTIVE);
				textActive = team.getBoolean(TUserAccountDataAccess.E_TEAM_TEXT_ACTIVE);
				imageActive = team.getBoolean(TUserAccountDataAccess.E_TEAM_IMAGE_ACTIVE);
				videoActive = team.getBoolean(TUserAccountDataAccess.E_TEAM_VIDEO_ACTIVE);
				pdfActive = team.getBoolean(TUserAccountDataAccess.E_TEAM_PDF_ACTIVE);
				spreadsheetActive = team.getBoolean(TUserAccountDataAccess.E_TEAM_SPREADSHEET_ACTIVE);
				coverPageActive = team.getBoolean(TUserAccountDataAccess.E_TEAM_COVERPAGE_ACTIVE);
				templatesActive = team.getBoolean(TUserAccountDataAccess.E_TEAM_TEMPLATE_ACTIVE);
			}

			Boolean editCheck = team != null && team.getParentId().equals(companyId) && action.equals("edit");
			Boolean addCheck = action.equals("add");
			if (up.hasManageUsersPermission() && (editCheck || addCheck))
			{
	%>
	<form class="form-edit-team" action="" method="POST">
		<%
			if (editCheck)
					{
		%>
		<input type="hidden" id="id" name="id" value="<%=team.getId()%>" />
		<%
			}
		%>
		<input type="hidden" id="action" name="action" value=<%=action%> />
		<div class="modal-header">
			<div class="titleBar">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					<i class="fal fa-times-circle"></i>
				</button>
			</div>
			<h2 class="modal-title">Teams</h2>
		</div>
		<div class="modal-body">
			<div class="form-group">
				<label for="teamName">Team Name</label> <input type="text"
					class="form-control" id="teamName" name="teamName"
					value="<%=editCheck ? team.getName() : ""%>" />
			</div>
			<div class="container contentContainer">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation"
						class="<%=StringUtils.isBlank(tab) || tab.equals("Products") ? "active" : ""%>"><a
						href="#products_tab" aria-controls="products" role="tab"
						data-module="Products" data-toggle="tab">Products</a></li>
					<li role="presentation"
						class="<%=StringUtils.isNotBlank(tab) && tab.equals("Text") ? "active" : ""%>"><a
						href="#text_tab" aria-controls="text" role="tab"
						data-module="Text" data-toggle="tab">Text</a></li>
					<li role="presentation"
						class="<%=StringUtils.isNotBlank(tab) && tab.equals("Images") ? "active" : ""%>"><a
						href="#images_tab" aria-controls="images" role="tab"
						data-module="Images" data-toggle="tab">Images</a></li>
					<li role="presentation"
						class="<%=StringUtils.isNotBlank(tab) && tab.equals("Videos") ? "active" : ""%>"><a
						href="#videos_tab" aria-controls="videos" role="tab"
						data-module="Videos" data-toggle="tab">Videos</a></li>
					<li role="presentation"
						class="<%=StringUtils.isNotBlank(tab) && tab.equals("Pdf") ? "active" : ""%>"><a
						href="#pdf_tab" aria-controls="pdf" role="tab" data-module="Pdf"
						data-toggle="tab">PDF</a></li>
					<li role="presentation"
						class="<%=StringUtils.isNotBlank(tab) && tab.equals("Spreadsheet") ? "active" : ""%>"><a
						href="#spreadsheet_tab" aria-controls="spreadsheet" role="tab" data-module="Spreadsheet"
						data-toggle="tab">Spreadsheet</a></li>
					<li role="presentation"
						class="<%=StringUtils.isNotBlank(tab) && tab.equals("Coverpage") ? "active" : ""%>"><a
						href="#coverpage_tab" aria-controls="coverpage" role="tab"
						data-module="Coverpage" data-toggle="tab">Cover Pages</a></li>
					<li role="presentation"
						class="<%=StringUtils.isNotBlank(tab) && tab.equals("Templates") ? "active" : ""%>"><a
						href="#templates_tab" aria-controls="templates" role="tab"
						data-module="Templates" data-toggle="tab">Templates</a></li>
					<%
						if (itineraryPlugins)
						{
					%>
					<li role="presentation"
						class="<%=StringUtils.isNotBlank(tab) && tab.equals("Itinerary") ? "active" : ""%>"><a
						href="#itinerary_tab" aria-controls="itinerary" role="tab"
						data-module="Itinerary" data-toggle="tab">Itinerary</a></li>
					<%
						}
					%>
				</ul>

				<!-- Tab panes -->

				<!-- Products Tab -->
				<div class="tab-content">
					<div role="tabpanel"
						class="tab-pane <%=StringUtils.isBlank(tab) || tab.equals("Products") ? "active" : ""%>"
						id="products_tab">
						<!--  Switch for turning Folder type on and off -->
						<div class="navbar team-navbar">
							<div class="navbar-form navbar-left">
								<div class="form-group">
									<%
										if (libraryProductFolders != null)
												{
									%>
									<input type="checkbox" class="js-switch" name="productSwitch"
										id="productSwitch" value="switch"
										<%=productActive ? "checked='checked'" : ""%> /> <span
										class="activeSwitchLabel">There are no product
										restrictions</span>
									<%
										}
												else
												{
									%>
									<input type="checkbox" class="js-switch" name="templateSwitch"
										value="switch" disabled /> <span>Restriction disabled:
										There are no products to restrict</span>
									<%
										}
									%>
								</div>
							</div>
						</div>
						<div id="multi-container-header">
							<div class="col-sm-6 col-xs-12" style="text-align: center;float: left;">
								<span>Visible Folders</span>
							</div>
							<div class="col-sm-6 col-xs-12" style="text-align: center;float: left;">
								<span>Hidden Folders</span>
							</div>
						</div>
						<div id="multi-container">
							<select class="form-control input-multidrop valid"
								multiple="true" multiple id="productFolders"
								name="productFolders" style="" aria-invalid="false">
								<%
									if (libraryProductFolders != null)
											{
												if (team != null)
												{
													MultiOptions activeProductsLibrary = ModuleAccess.getInstance().getMultiOptions(team, TUserAccountDataAccess.E_TEAM_PRODUCT_FOLDERS, "useraccounts");
													for (CategoryData folder : libraryProductFolders)
													{
														boolean active = false;
														if (StringUtils.isNotBlank(teamId))
														{
															for (String activeProducts : activeProductsLibrary.getValues())
															{
																if (activeProducts.equals(folder.getId()))
																{
																	active = true;
																}
															}
														}
								%>
								<option <%=active ? "selected" : ""%>
									value="<%=folder.getId()%>"><%=folder.getName()%></option>
								<%
									}
												}
												else
												{
													for (CategoryData folder : libraryProductFolders)
													{
								%>
								<option value="<%=folder.getId()%>"><%=folder.getName()%></option>
								<%
									}
												}
											}
								%>
							</select>
						</div>
						<p id="folderHelperText"
							style="font-size: x-small; font-style: italic;"><%=folderHelp%></p>
					</div>

					<!-- Text Tab -->
					<div role="tabpanel"
						class="tab-pane <%=StringUtils.isNotBlank(tab) && tab.equals("Text") ? "active" : ""%>"
						id="text_tab">
						<!--  Switch for turning Folder type on and off -->
						<div class="navbar team-navbar">
							<div class="navbar-form navbar-left">
								<div class="form-group">
									<%
										if (libraryTextFolders != null)
												{
									%>
									<input type="checkbox" class="js-switch" name="textSwitch"
										value="switch" <%=textActive ? "checked='checked'" : ""%> />
									<span class="activeSwitchLabel">There are no text
										restrictions</span>
									<%
										}
												else
												{
									%>
									<input type="checkbox" class="js-switch" name="templateSwitch"
										value="switch" disabled /> <span>Restriction disabled:
										There are no text files to restrict</span>
									<%
										}
									%>
								</div>
							</div>
						</div>
						<div id="multi-container-header">
							<div class="col-sm-6 col-xs-12" style="text-align: center;float: left;">
								<span>Visible Folders</span>
							</div>
							<div class="col-sm-6 col-xs-12" style="text-align: center;float: left;">
								<span>Hidden Folders</span>
							</div>
						</div>
						<div id="multi-container">
							<select class="form-control input-multidrop valid"
								multiple="true" multiple id="textFolders" name="textFolders"
								style="" aria-invalid="false">
								<%
									if (libraryTextFolders != null)
											{
												if (team != null)
												{
													MultiOptions activeTextLibrary = ModuleAccess.getInstance().getMultiOptions(team, TUserAccountDataAccess.E_TEAM_TEXT_FOLDERS, "useraccounts");
													for (CategoryData folder : libraryTextFolders)
													{
														boolean active = false;
														if (StringUtils.isNotBlank(teamId))
														{
															for (String activeText : activeTextLibrary.getValues())
															{
																if (activeText.equals(folder.getId()))
																{
																	active = true;
																}
															}
														}
								%>
								<option <%=active ? "selected" : ""%>
									value="<%=folder.getId()%>"><%=folder.getName()%></option>
								<%
									}
												}
												else
												{
													for (CategoryData folder : libraryTextFolders)
													{
								%>
								<option value="<%=folder.getId()%>"><%=folder.getName()%></option>
								<%
									}
												}
											}
								%>
							</select>
						</div>
						<p id="folderHelperText"
							style="font-size: x-small; font-style: italic;"><%=folderHelp%></p>
					</div>

					<!-- Images Tab -->
					<div role="tabpanel"
						class="tab-pane <%=StringUtils.isNotBlank(tab) && tab.equals("Images") ? "active" : ""%>"
						id="images_tab">
						<!--  Switch for turning Folder type on and off -->
						<div class="navbar team-navbar">
							<div class="navbar-form navbar-left">
								<div class="form-group">
									<%
										if (libraryImageFolders != null)
												{
									%>
									<input type="checkbox" class="js-switch" name="imageSwitch"
										value="switch" <%=imageActive ? "checked='checked'" : ""%> />
									<span class="activeSwitchLabel">There are no image
										restrictions</span>
									<%
										}
												else
												{
									%>
									<input type="checkbox" class="js-switch" name="templateSwitch"
										value="switch" disabled /> <span>Restriction disabled:
										There are no images to restrict</span>
									<%
										}
									%>
								</div>
							</div>
						</div>
						<div id="multi-container-header">
							<div class="col-sm-6 col-xs-12" style="text-align: center;float: left;">
								<span>Visible Folders</span>
							</div>
							<div class="col-sm-6 col-xs-12" style="text-align: center;float: left;">
								<span>Hidden Folders</span>
							</div>
						</div>
						<div id="multi-container">
							<select multiple id="imageFolders" name="imageFolders"
								multiple="true" class="form-control input-multidrop valid"
								style="" aria-invalid="false">
								<%
									if (libraryImageFolders != null)
											{
												if (team != null)
												{
													MultiOptions activeImageLibrary = ModuleAccess.getInstance().getMultiOptions(team, TUserAccountDataAccess.E_TEAM_IMAGE_FOLDERS, "useraccounts");
													for (CategoryData folder : libraryImageFolders)
													{
														boolean active = false;
														if (StringUtils.isNotBlank(teamId))
														{
															for (String activeImage : activeImageLibrary.getValues())
															{
																if (activeImage.equals(folder.getId()))
																{
																	active = true;
																}
															}
														}
								%>
								<option <%=active ? "selected" : ""%>
									value="<%=folder.getId()%>"><%=folder.getName()%></option>
								<%
									}
												}
												else
												{
													for (CategoryData folder : libraryImageFolders)
													{
								%>
								<option value="<%=folder.getId()%>"><%=folder.getName()%></option>
								<%
									}
												}
											}
								%>
							</select>
						</div>
						<p id="folderHelperText"
							style="font-size: x-small; font-style: italic;"><%=folderHelp%></p>
					</div>

					<!-- Videos Tab -->
					<div role="tabpanel"
						class="tab-pane <%=StringUtils.isNotBlank(tab) && tab.equals("Videos") ? "active" : ""%>"
						id="videos_tab">
						<!--  Switch for turning Folder type on and off -->
						<div class="navbar team-navbar">
							<div class="navbar-form navbar-left">
								<div class="form-group">
									<%
										if (libraryVideoFolders != null)
												{
									%>
									<input type="checkbox" class="js-switch" name="videoSwitch"
										value="switch" <%=videoActive ? "checked='checked'" : ""%> />
									<span class="activeSwitchLabel">There are no video
										restrictions</span>
									<%
										}
												else
												{
									%>
									<input type="checkbox" class="js-switch" name="templateSwitch"
										value="switch" disabled /> <span>Restriction disabled:
										There are no videos to restrict</span>
									<%
										}
									%>
								</div>
							</div>
						</div>
						<div id="multi-container-header">
							<div class="col-sm-6 col-xs-12" style="text-align: center;float: left;">
								<span>Visible Folders</span>
							</div>
							<div class="col-sm-6 col-xs-12" style="text-align: center;float: left;">
								<span>Hidden Folders</span>
							</div>
						</div>
						<div id="multi-container">
							<select multiple id="videoFolders" name="videoFolders"
								multiple="true" class="form-control input-multidrop valid"
								style="" aria-invalid="false">
								<%
									if (libraryVideoFolders != null)
											{
												if (team != null)
												{
													MultiOptions activeVideoLibrary = ModuleAccess.getInstance().getMultiOptions(team, TUserAccountDataAccess.E_TEAM_VIDEO_FOLDERS, "useraccounts");
													for (CategoryData folder : libraryVideoFolders)
													{
														boolean active = false;
														if (StringUtils.isNotBlank(teamId))
														{
															for (String activeVideo : activeVideoLibrary.getValues())
															{
																if (activeVideo.equals(folder.getId()))
																{
																	active = true;
																}
															}
														}
								%>
								<option <%=active ? "selected" : ""%>
									value="<%=folder.getId()%>"><%=folder.getName()%></option>
								<%
									}
												}
												else
												{
													for (CategoryData folder : libraryVideoFolders)
													{
								%>
								<option value="<%=folder.getId()%>"><%=folder.getName()%></option>
								<%
									}
												}
											}
								%>
							</select>
						</div>
						<p id="folderHelperText"
							style="font-size: x-small; font-style: italic;"><%=folderHelp%></p>
					</div>

					<%--<!-- PDF Tab -->--%>
					<%--<div role="tabpanel"--%>
						<%--class="tab-pane <%=StringUtils.isNotBlank(tab) && tab.equals("Pdf") ? "active" : ""%>"--%>
						<%--id="pdf_tab">--%>
						<%--<!--  Switch for turning Folder type on and off -->--%>
						<%--<div class="navbar team-navbar">--%>
							<%--<div class="navbar-form navbar-left">--%>
								<%--<div class="form-group">--%>
									<%--<%--%>
									<%--if (libraryPDFFolders != null)--%>
									<%--{--%>
									<%--%>--%>
									<%--<input type="checkbox" class="js-switch" name="pdfSwitch"--%>
										<%--value="switch" <%=pdfActive ? "checked='checked'" : ""%> /> <span--%>
										<%--class="activeSwitchLabel">There are no PDF restrictions</span>--%>
									<%--<%--%>
									<%--}--%>
									<%--else--%>
									<%--{--%>
									<%--%>--%>
									<%--<input type="checkbox" class="js-switch" name="templateSwitch"--%>
										<%--value="switch" disabled /> <span>Restriction disabled:--%>
										<%--There are no PDF files to restrict</span>--%>
									<%--<%--%>
									<%--}--%>
									<%--%>--%>
								<%--</div>--%>
							<%--</div>--%>
						<%--</div>--%>
						<%--<div id="multi-container-header">--%>
							<%--<div class="col-sm-6 col-xs-12" style="text-align: center;float: left;">--%>
								<%--<span>Visible Folders</span>--%>
							<%--</div>--%>
							<%--<div class="col-sm-6 col-xs-12" style="text-align: center;float: left;">--%>
								<%--<span>Hidden Folders</span>--%>
							<%--</div>--%>
						<%--</div>--%>
						<%--<div id="multi-container">--%>
							<%--<select multiple id="pdfFolders" name="pdfFolders"--%>
								<%--multiple="true" class="form-control input-multidrop valid"--%>
								<%--style="" aria-invalid="false">--%>
								<%--<%--%>
									<%--if (libraryPDFFolders != null)--%>
											<%--{--%>
												<%--if (team != null)--%>
												<%--{--%>
													<%--MultiOptions activePDFLibrary = ModuleAccess.getInstance().getMultiOptions(team, TUserAccountDataAccess.E_TEAM_PDF_FOLDERS, "users");--%>
													<%--for (CategoryData folder : libraryPDFFolders)--%>
													<%--{--%>
														<%--boolean active = false;--%>
														<%--if (StringUtils.isNotBlank(teamId))--%>
														<%--{--%>
															<%--for (String activePDF : activePDFLibrary.getValues())--%>
															<%--{--%>
																<%--if (activePDF.equals(folder.getId()))--%>
																<%--{--%>
																	<%--active = true;--%>
																<%--}--%>
															<%--}--%>
														<%--}--%>
								<%--%>--%>
								<%--<option <%=active ? "selected" : ""%>--%>
									<%--value="<%=folder.getId()%>"><%=folder.getName()%></option>--%>
								<%--<%--%>
									<%--}--%>
												<%--}--%>
												<%--else--%>
												<%--{--%>
													<%--for (CategoryData folder : libraryPDFFolders)--%>
													<%--{--%>
								<%--%>--%>
								<%--<option value="<%=folder.getId()%>"><%=folder.getName()%></option>--%>
								<%--<%--%>
									<%--}--%>
												<%--}--%>
											<%--}--%>
								<%--%>--%>
							<%--</select>--%>
						<%--</div>--%>
						<%--<p id="folderHelperText"--%>
							<%--style="font-size: x-small; font-style: italic;"><%=folderHelp%></p>--%>
					<%--</div>--%>
					<%----%>
					<%--<!-- Spreadsheet tab -->--%>
					<%--<div role="tabpanel"--%>
						<%--class="tab-pane <%=StringUtils.isNotBlank(tab) && tab.equals("Spreadsheet") ? "active" : ""%>"--%>
						<%--id="spreadsheet_tab">--%>
						<%--<!--  Switch for turning Folder type on and off -->--%>
						<%--<div class="navbar team-navbar">--%>
							<%--<div class="navbar-form navbar-left">--%>
								<%--<div class="form-group">--%>
									<%--<%--%>
									<%--if (librarySpreadsheetFolders != null)--%>
									<%--{--%>
									<%--%>--%>
									<%--<input type="checkbox" class="js-switch" name="spreadsheetSwitch"--%>
										<%--value="switch" <%=spreadsheetActive ? "checked='checked'" : ""%> /> <span--%>
										<%--class="activeSwitchLabel">There are no Spreadsheet restrictions</span>--%>
									<%--<%--%>
									<%--}--%>
									<%--else--%>
									<%--{--%>
									<%--%>--%>
									<%--<input type="checkbox" class="js-switch" name="templateSwitch"--%>
										<%--value="switch" disabled /> <span>Restriction disabled:--%>
										<%--There are no Spreadsheet files to restrict</span>--%>
									<%--<%--%>
									<%--}--%>
									<%--%>--%>
								<%--</div>--%>
							<%--</div>--%>
						<%--</div>--%>
						<%--<div id="multi-container-header">--%>
							<%--<div class="col-sm-6 col-xs-12" style="text-align: center;float: left;">--%>
								<%--<span>Visible Folders</span>--%>
							<%--</div>--%>
							<%--<div class="col-sm-6 col-xs-12" style="text-align: center;float: left;">--%>
								<%--<span>Hidden Folders</span>--%>
							<%--</div>--%>
						<%--</div>--%>
						<%--<div id="multi-container">--%>
							<%--<select multiple id="spreadsheetFolders" name="spreadsheetFolders"--%>
								<%--multiple="true" class="form-control input-multidrop valid"--%>
								<%--style="" aria-invalid="false">--%>
								<%--<%--%>
									<%--if (librarySpreadsheetFolders != null)--%>
											<%--{--%>
												<%--if (team != null)--%>
												<%--{--%>
													<%--MultiOptions activeSpreadsheetLibrary = ModuleAccess.getInstance().getMultiOptions(team, TUserAccountDataAccess.E_TEAM_SPREADSHEET_FOLDERS, "users");--%>
													<%--for (CategoryData folder : librarySpreadsheetFolders)--%>
													<%--{--%>
														<%--boolean active = false;--%>
														<%--if (StringUtils.isNotBlank(teamId))--%>
														<%--{--%>
															<%--for (String activeSpreadsheet : activeSpreadsheetLibrary.getValues())--%>
															<%--{--%>
																<%--if (activeSpreadsheet.equals(folder.getId()))--%>
																<%--{--%>
																	<%--active = true;--%>
																<%--}--%>
															<%--}--%>
														<%--}--%>
								<%--%>--%>
								<%--<option <%=active ? "selected" : ""%>--%>
									<%--value="<%=folder.getId()%>"><%=folder.getName()%></option>--%>
								<%--<%--%>
									<%--}--%>
												<%--}--%>
												<%--else--%>
												<%--{--%>
													<%--for (CategoryData folder : librarySpreadsheetFolders)--%>
													<%--{--%>
								<%--%>--%>
								<%--<option value="<%=folder.getId()%>"><%=folder.getName()%></option>--%>
								<%--<%--%>
									<%--}--%>
												<%--}--%>
											<%--}--%>
								<%--%>--%>
							<%--</select>--%>
						<%--</div>--%>
						<%--<p id="folderHelperText"--%>
							<%--style="font-size: x-small; font-style: italic;"><%=folderHelp%></p>--%>
					<%--</div>--%>

					<!-- Cover Pages Tab -->
					<div role="tabpanel"
						class="tab-pane <%=StringUtils.isNotBlank(tab) && tab.equals("Coverpage") ? "active" : ""%>"
						id="coverpage_tab">
						<!--  Switch for turning Folder type on and off -->
						<div class="navbar team-navbar">
							<div class="navbar-form navbar-left">
								<div class="form-group">
									<%
										if (coverPageFiles != null)
												{
									%>
									<input type="checkbox" class="js-switch" name="coverpageSwitch"
										value="switch" <%=coverPageActive ? "checked='checked'" : ""%> />
									<span class="activeSwitchLabelCoverImages">There are no
										Cover Page restrictions</span>
									<%
										}
												else
												{
									%>
									<input type="checkbox" class="js-switch" name="templateSwitch"
										value="switch" disabled /> <span>Restriction disabled:
										There are no cover pages to restrict</span>
									<%
										}
									%>
								</div>
							</div>
						</div>
						<%--<div id="multi-container-header">--%>
						<%--<div class="col-sm-6 col-xs-12" style="text-align: center;float: left;">--%>
						<%--<span>Visible Folders</span>--%>
						<%--</div>--%>
						<%--<div class="col-sm-6 col-xs-12" style="text-align: center;float: left;">--%>
						<%--<span>Hidden Folders</span>--%>
						<%--</div>--%>
						<%--</div>--%>
						<div id="multi-container">
							<select multiple="multiple" id="coverPageFiles"
								name="coverPageFiles" id="coverPageFiles"
								class="image-picker show-html">
								<%
									if (coverPageFiles != null)
											{
												if (team != null)
												{
													MultiOptions activeCoverPageLibrary = ModuleAccess.getInstance().getMultiOptions(team, TUserAccountDataAccess.E_TEAM_COVERPAGE_FOLDERS, "useraccounts");
													for (CoverPage files : coverPageFiles)
													{
														boolean active = false;
														if (StringUtils.isNotBlank(teamId))
														{
															for (String activeCoverPage : activeCoverPageLibrary.getValues())
															{
																if (activeCoverPage.equals(files.getId()))
																{
																	active = true;
																}
															}
														}
														String fileRaw = files.getThumbnail();
														String fileName = fileRaw.substring(fileRaw.lastIndexOf('_') + 1);
								%>
								<option data-img-src="<%=files.getThumbnail()%>"
									<%=active ? "selected" : ""%> value="<%=files.getId()%>"><%=fileName%></option>
								<%
									}
												}
												else
												{
													for (CoverPage files : coverPageFiles)
													{
														String fileRaw = files.getThumbnail();
														String fileName = fileRaw.substring(fileRaw.lastIndexOf('_') + 1);
								%>
								<option data-img-src="<%=files.getThumbnail()%>"
									value="<%=files.getId()%>"><%=fileName%></option>
								<%
									}
												}
											}
								%>
							</select>
						</div>
						<p id="folderHelperText"
							style="font-size: x-small; font-style: italic;"><%=folderHelp%></p>
					</div>

					<!-- Templates Tab -->
					<div role="tabpanel"
						class="tab-pane <%=StringUtils.isNotBlank(tab) && tab.equals("Templates") ? "active" : ""%>"
						id="templates_tab">
						<!--  Switch for turning Folder type on and off -->
						<div class="navbar team-navbar">
							<div class="navbar-form navbar-left">
								<div class="form-group">
									<%
										if (templatesFiles != null)
												{
									%>
									<input type="checkbox" class="js-switch" name="templateSwitch"
										value="switch" <%=templatesActive ? "checked='checked'" : ""%> />
									<span class="activeSwitchLabel">There are no Template
										restrictions</span>
									<%
										}
												else
												{
									%>
									<input type="checkbox" class="js-switch" name="templateSwitch"
										value="switch" disabled /> <span>Restriction disabled:
										There are no templates to restrict</span>
									<%
										}
									%>
								</div>
							</div>
						</div>
						<div id="multi-container-header">
							<div class="col-sm-6 col-xs-12" style="text-align: center;float: left;">
								<span>Visible Folders</span>
							</div>
							<div class="col-sm-6 col-xs-12" style="text-align: center;float: left;">
								<span>Hidden Folders</span>
							</div>
						</div>
						<div id="multi-container">
							<select multiple id="templateFiles" name="templateFiles"
								multiple="true" class="form-control input-multidrop valid"
								style="" aria-invalid="false">
								<%
									if (templatesFiles != null)
											{
												if (team != null)
												{
													MultiOptions activeProposalLibrary = ModuleAccess.getInstance().getMultiOptions(team, TUserAccountDataAccess.E_TEAM_TEMPLATE_FOLDERS, "useraccounts");
													for (Hashtable<String, String> file : templatesFiles)
													{
														boolean active = false;
														String proposalId = file.get(TravelDocsDataAccess.E_ID);
														if (StringUtils.isNotBlank(teamId))
														{
															for (String activeProposal : activeProposalLibrary.getValues())
															{
																if (activeProposal.equals(proposalId))
																{
																	active = true;
																}
															}
														}
								%>
								<option <%=active ? "selected" : ""%> value="<%=proposalId%>"><%=ESAPI.encoder().encodeForHTML(file.get(TravelDocsDataAccess.E_TEMPLATE_TITLE))%>
									:
									<%=ESAPI.encoder().encodeForHTML(file.get(TravelDocsDataAccess.E_TEMPLATE_DESCRIPTION))%></option>
								<%
									}
												}
												else
												{
													for (Hashtable<String, String> file : templatesFiles)
													{
														String proposalId = file.get(TravelDocsDataAccess.E_ID);
								%>
								<option value="<%=proposalId%>"><%=ESAPI.encoder().encodeForHTML(file.get(TravelDocsDataAccess.E_TEMPLATE_TITLE))%>
									:
									<%=ESAPI.encoder().encodeForHTML(file.get(TravelDocsDataAccess.E_TEMPLATE_DESCRIPTION))%></option>
								<%
									}
												}
											}
								%>
							</select>
						</div>
						<p id="folderHelperText"
							style="font-size: x-small; font-style: italic;"><%=folderHelp%></p>
					</div>

					<!-- Itinerary Plugins -->
					<%
						if (itineraryPlugins)
						{
					%>
					<div role="tabpanel"
						class="tab-pane <%=StringUtils.isNotBlank(tab) && tab.equals("Itinerary") ? "active" : ""%>"
						id="itinerary_tab">

						<div class="form-group">
							<label>Proposal Template</label> <select
								id="proposalTemplate" style="width: 100%;"
								name="proposalTemplate">
								<option <%=addCheck ? "selected" : ""%> value=""></option>
								<%
									Vector<Hashtable<String, String>> templates = pda.getTemplates(companyId, companyInfo.getName());

												for (Hashtable<String, String> ht : templates)
												{
								%>
								<option
									<%=team == null ? "" : ht.get(TravelDocsDataAccess.E_ID).equals(team.getString(UserAccountDataAccess.C1_PROPOSAL_TEMPLATE)) ? "selected" : ""%>
									value="<%=ht.get(TravelDocsDataAccess.E_ID)%>"><%=ht.get(TravelDocsDataAccess.E_TEMPLATE_TITLE)%></option>
								<%
									}
								%>
							</select>
						</div>

						<div class="form-group">
							<label for="itiSyles">Itinerary Style</label> <select
								id="itiStyles" style="width: 100%;" name="itiStyle">
								<option <%=addCheck ? "selected" : ""%> value=""></option>
								<%
									List<ElementData> itineraryStyles = new ItineraryDataAccess().getItineraryStyles(companyId);
												for (ElementData itineraryStyle : itineraryStyles)
												{
								%>
								<option
									<%=team == null ? "" : itineraryStyle.getId().equals(team.getString(UserAccountDataAccess.C1_ITINERARY_TEMPLATE)) ? "selected" : ""%>
									value="<%=itineraryStyle.getId()%>"><%=itineraryStyle.getName()%></option>
								<%
									}
								%>
							</select>
						</div>

						<div class="form-group">
							<input type="checkbox" class="js-switch" name="autoSendItinerary"
								value="switch"
								<%=team == null ? "" : team.getBoolean(TUserAccountDataAccess.C1_AUTO_SEND_ITINERARY) ? "checked='checked'" : ""%> />
							<span>Auto send itinerary proposal</span>
						</div>

						<!-- Travelport Credential -->
						<%
							if (travelport)
										{
						%>
						<%-- <h3>Travelport</h3>
							<div class="form-group">
								<label>Travelport Branch Code</label> <input
									class="form-control" type="text" id="travelportBranchCode"
									name="travelportBranchCode"
									value="<%=travelPortTeamCategory == null ? "" : travelPortTeamCategory.getString(TravelportDataAccess.C2_BRANCH_CODE) == null ? "" : travelPortTeamCategory.getString(TravelportDataAccess.C2_BRANCH_CODE)%>" />
							</div>
							<div class="form-group">
								<label>Password</label> <input class="form-control"
									type="password" id="travelportPassword" name="travelportPassword"
									value="<%=travelPortTeamCategory == null ? "" : travelPortTeamCategory.getString(TravelportDataAccess.C2_PASSWORD) == null ? "" : travelPortTeamCategory.getString(TravelportDataAccess.C2_PASSWORD)%>" />
							</div> --%>
						<%
							}
						%>

						<!-- ArrivalGuide Credential -->
						<%
							if (arrivalGuide)
										{
						%>
						<h3>ArrivalGuide</h3>
						<div class="form-group">
							<label>API Key</label> <input class="form-control"
								type="text" id="arrivalGuideApiKey" name="arrivalGuideApiKey"
								value="<%=team == null ? "" : team.get(TUserAccountDataAccess.C1_ARRIVALGUIDE_API) == null ? "" : team.getString(TUserAccountDataAccess.C1_ARRIVALGUIDE_API)%>" />
						</div>
						<%
							}
						%>

					</div>
					<%
						}
					%>

				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" id="closeForm" class="btn btn-danger"
				data-dismiss="modal">
				<i class="fa fa-times"></i> Close
			</button>
			<button type="submit" class="btn btn-success">
				<i class="fa fa-save"></i> Save
			</button>
		</div>
	</form>
	<%
		}
			else
			{
	%>
	<div class="modal-header">
		<div class="titleBar">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true"></button>
		</div>
		<h2 class="modal-title">Teams</h2>
	</div>
	<div class="modal-body">You do not have permissions modify this
		team</div>
	<div class="modal-footer">
		<button type="button" id="closeForm" class="btn btn-danger"
			data-dismiss="modal">
			<i class="fa fa-times"></i> Close
		</button>
	</div>
	<%
		}
	%>
	<script src="/application/js/image-picker.js"></script>
	<script src="/application/assets/plugins/ui.multiselect.js"></script>
	<script src="/application/assets/plugins/switchery/switchery.js"></script>
	<script src="/application/assets/plugins/select2/select2.js"></script>
	<script src="/application/js/form-validation/modal-edit-team.js"></script>
	<!-- <script src="/application/js/form-validation/modal-.js"></script> -->
</div>
</compress:html>