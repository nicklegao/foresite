<%@page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions"%>
<%@page import="java.util.Hashtable"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ page import="com.warrenstrange.googleauth.GoogleAuthenticator" %>
<%@ page import="au.corporateinteractive.utils.GoogleAuthUtils" %>
<%@ page import="com.warrenstrange.googleauth.GoogleAuthenticatorConfig" %>
<%@ page import="com.warrenstrange.googleauth.GoogleAuthenticatorQRGenerator" %>
<%@ page import="com.warrenstrange.googleauth.GoogleAuthenticatorKey" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
    <div id="setup-auth-mobile">
        <%
            UserAccountDataAccess uada = new UserAccountDataAccess();
            String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
            ElementData userData = uada.getUserElementWithUserID(userId);

            GoogleAuthenticatorConfig authConfig = new GoogleAuthenticatorConfig
                    .GoogleAuthenticatorConfigBuilder()
                    .build();

            GoogleAuthenticatorKey key = new GoogleAuthenticatorKey.Builder(userData.getString("ATTR_totpSecret"))
                    .setConfig(authConfig)
                    .setVerificationCode(0)
                    .setScratchCodes(new ArrayList<Integer>())
                    .build();

            String qrImage = GoogleAuthenticatorQRGenerator.getOtpAuthURL("TravelDocs", userData.getName(), key);

            boolean mobileVerification = UserAccountDataAccess.isUserUsingMobileTFA(userData);
        %>
        <form class="form-mobile-auth">
            <input type="hidden" id="id" name="id" value="<%= userId %>" />
            <div class="modal-header">
                <div class="titleBar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <h2 class="modal-title"><%= mobileVerification ? "Disable 2FA" : "Setup Security" %></h2>
            </div>
            <div class="modal-body">
                <div class="form-group text-center">
                    <input type="hidden" name="updateMobileVerification" value="<%= !mobileVerification %>" />
                    <h2>Two-Factor Security Code</h2>
                    <% if(mobileVerification) { %>
                        <h6 class="text-muted">Before you can disable mobile two factor authentication you will need to input your generated code.</h6>
                    <% } else { %>
                        <h6 class="text-muted">Please scan the QR code below using an authenticator app such as Google Authenticator or Authy. Then enter the code below.</h6>
                        <img src="<%= qrImage %>" style="min-height: 200px;"/>
                    <% } %>
                    <br>
                    <input type="text" name="verifytotp" placeholder="Confirmation Code" style="text-align: center;appearance: textfield;border: 2px solid rgb(238, 238, 238); margin-top: 20px" id="verifyotp" />
                </div>
            </div>
            <div class="modal-footer">
                <div class="row" style="width: 100%;margin: 0">
                    <div class="col text-left">
                        <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
                            <i class="fa fa-times"></i> Close
                        </button>
                    </div>
                    <div class="col text-right">
                        <button type="submit" class="btn btn-success" style="color: rgba(255, 255, 255, 0.870588) !important;">
                            <i class="fa fa-save" style="color: rgba(255, 255, 255, 0.870588) !important;"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <script src="/application/js/form-validation/modal-mobile-2fa-setup.js"></script>
    </div>
</compress:html>