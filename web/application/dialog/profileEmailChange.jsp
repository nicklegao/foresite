<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.MaestranoUserAccountDataAccess"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="java.util.Hashtable"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
    <div id="profile-email-change">
        <%
            UserAccountDataAccess uada = new UserAccountDataAccess();

            String userId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
            Hashtable<String, String> userDetails = uada.getUserWithId(userId);

            CategoryData company = uada.getUserCompanyForUserId(userId);

            String email = userDetails.get(UserAccountDataAccess.E_EMAIL_ADDRESS);
            String emailLabel = "Maestrano";
            if (MaestranoUserAccountDataAccess.isMaestranoUser(userDetails)) {
                email = userDetails.get(MaestranoUserAccountDataAccess.E_MAESTRANO_REAL_EMAIL);
                String tenantKey = company.getString(MaestranoUserAccountDataAccess.C_MAESTRANO_TENANT_KEY);
                if (tenantKey.startsWith("nab"))
                    emailLabel = "NAB BIO";
            }

            boolean pleaseEnterPhoneNumber = StringUtils.isBlank(userDetails.get(UserAccountDataAccess.E_MOBILE_NUMBER));
        %>
        <form class="form-change-email" action="" method="POST">
            <div class="modal-header">
                <div class="titleBar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <h2 class="modal-title">Change Email Address</h2>
            </div>
            <div class="modal-body">
                <p>To change your email address for <%=ESAPI.encoder().encodeForHTMLAttribute(email) %>, enter the password followed by the new email address:</p>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="account-password" class="col-sm-3 control-label">
                            Current Password:
                        </label>
                        <div class="col-sm-8">
                            <input type="password" id="account-password" class="form-control" name="accountPassword" placeholder="Current Password"
                                   value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="new-email" class="col-sm-3 control-label">
                            New Email:
                        </label>
                        <div class="col-sm-8">
                            <input type="email" id="new-email" class="form-control" name="newEmail" placeholder="New Email Address"
                                   value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="closeForm" class="btn btn-danger" data-dismiss="modal">
                    <i class="fa fa-times"></i> Close
                </button>
                <button type="submit" id="submitNewEmail" class="btn btn-success">
                    <i class="fa fa-save"></i> Save
                </button>
            </div>
        </form>

        <script src="/application/js/form-validation/modal-change-email.js"></script>
    </div>
</compress:html>