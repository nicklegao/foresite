@echo off
cd /d %~dp0

echo Dashboard
cmd /C lessc dashboard.less > dashboard.css 


echo Editor
cmd /C lessc edit.less > edit.css 


echo View
cmd /C lessc view.less > view.css 


echo PDF (no longer built due to change of logic for qcloud)
rem cmd /C lessc ..\css-document\pdf-palette1.less > ..\css-document\palette1\pdf.css
rem cmd /C lessc ..\css-document\pdf-palette2.less > ..\css-document\palette2\pdf.css
rem cmd /C lessc ..\css-document\pdf-palette3.less > ..\css-document\palette3\pdf.css
rem cmd /C lessc ..\css-document\pdf-palette4.less > ..\css-document\palette4\pdf.css


echo Styles
cmd /C lessc styles.less > styles.css 