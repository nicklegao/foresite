var switcheryCoverpageVisibility;
var backgroundProcessing = 0;
var lastSave = "";
var restoredTravelDoc = undefined;
var contentBlocks;
$(document).ready(function() {

	$(document).ajaxComplete(function(event, xhr, ajaxOptions)
	{
		if (xhr.status == 401)
		{
			swal({
				title : "You've been logged out!",
				text : "Due to inactivity, you have been logged out. We autosaved your latest work so you can recover it after login.",
				type : "warning"
			}, function()
			{
				window.location.reload();
			});
			logger.info("HTTP 401(Unauthorised) recieved... autosaving before closing.");
			saveToLocalStorage();
		}
		if (xhr.status == 500)
		{
			swal({
				title : "Oops, that's embarrassing",
				text : "Something went wrong on the server. Please try again later.",
				type : "warning"
			}, function()
			{

			});
			logger.info("HTTP 500(Internal Server Error) recieved.");
			saveToLocalStorage();
		}

	});

    setInterval(function()
    {
        $.ajax({
            type : 'GET',
            url : '/api/traveldoc/sessionTimeout',
            cache : false,
            contentType : false,
            processData : false
        }).done(function(data)
        {
            console.log('Session alive');
        });
    }, (1000 * 60) * 5);


	$(".saveTravelDocAction").click(doSaveTravelDoc);
	$(".saveAndPreviewAction").click(doSaveAndPreviewTravelDoc);

	$(document).on('click', '.cht-button-delete', function() {
		var ele = $(this);
		var mainEle = $(ele).closest('.content-holder');
		var type = $(mainEle).data('type');
		if (type.indexOf("itinerary") >= 0) {
			$('#block-itinerary').css("color", "");
			$('#block-itinerary').draggable( 'enable' );
			$('div[data-type="block-pricing-table"]').each(function()
			{
				console.log($(this).data('id'));
				var ele = $(this).data('id');
				if (ele != null && ele.indexOf("itinerary") >= 0) {
					$(this).remove();
				}
			});
		}
		$(ele).closest('.content-holder').remove();
	});

    $(document).on('keyup', '.sectionNameChanger', function(e) {
        if (e.keyCode == 13) {
        	var parent = $(this).parent('.tab-label');
            $(parent).siblings('.editButton').click();
        }
    });
    $(document).on('click', '#new-section-button', function() {
        setupSections();
    });

    $(document).on('click', 'div.editButton', function() {
    	if ($(this).hasClass('active')) {
            var input = $(this).siblings('.tab-label');
            var parentButton = $(this).parent();
            var text = $(input).find('input').val();
            text = text.length==0 ? $(input).find('input').attr('placeholder') : text;
            $(input).empty();
            $(input).html(text);
            $(parentButton).data('title', text);
            var section = '#' + $(parentButton).data('section');
            $(section).data('title', text);
            $(this).removeClass('active');
		} else {
            var input = $(this).siblings('.tab-label');
            var text = $(input).text();
            $(input).empty();
            $(input).html('<input class="sectionNameChanger" type="text" val="'+text+'" placeholder="'+text+'" />');
            $(this).addClass('active');
            $(input).focus();
		}
    });


	if (addTemplate) {
		loadBlockTiles();
		setupCoverPage();
	} else {
        loadBlockTiles();
        restoreTravelDoc();
        // setupContentBlocks();
        setupDropZones();
        setupSwappables();
	}

	$('#waypointsTable tr').hover(function() {
		$(this).addClass('hover');
	}, function() {
		$(this).removeClass('hover');
	});

	$("ul#document-section-tabs").on("click", "button.section-button", function(){
		var sectionName = $(this).data('section');
		$(this).addClass('active');
		$(this).parent().siblings().find('.section-button').removeClass('active');
        $('.editor-content-scroller').animate({

            scrollTop: 0
        }, 0);
		$('.editor-content-scroller').animate({

			scrollTop: $("#"+sectionName).offset().top - 140
		}, 0);
	});

//	$('.editor-content-scroller').scroll(function () {
//		var winTop = $(this).scrollTop();
//		var winHeight = $(this).height();
//		var winBottom = winTop + winHeight;
//
//		$( '#content-section-coverpage' ).each(function (index, item) {
//			var itemTop = $(item).position().top;
//			var itemHeight = $(item).height();
//			var itemBottom = itemTop + itemHeight;
//
//			if (itemTop <= (winTop) && (itemBottom-(itemBottom/4)) > (winBottom / 2)) {
//				$('#coverpage-nav-icon').show();
//                $('#blocks-nav-icon-button').hide();
//                $('#text-nav-icon-button').hide();
//                $('#images-nav-icon-button').hide();
//                $('#videos-nav-icon-button').hide();
//				$('.side-bar-loaded-container').hide();
//				if(!$('#style-nav-icon-button').hasClass('active')) {
//					$('#coverpage-nav-icon-button').addClass('active');
//					$('.side-bar-cover-container').show();
//				}
//			} else {
//				$('#coverpage-nav-icon').hide();
//                $('#blocks-nav-icon-button').show();
//                $('#text-nav-icon-button').show();
//                $('#images-nav-icon-button').show();
//                $('#videos-nav-icon-button').show();
//				$('.side-bar-cover-container').hide();
//				if(!$('#style-nav-icon-button').hasClass('active')) {
//					$('#block-nav-icon-button').addClass('active');
//					$('.side-bar-loaded-container').show();
//				}
//			}
//		});
//	});

    $(".doc-page-area").on("click", "#uploadImageButton", function(){
        $(this).next('input').click();
    });


    $(".doc-page-area").on("click", "#videoSaveButton", function(){
        var container = $(this).parents().eq(1);
        var code = $(container).find('#videoCode').val();
        if (code != null && code.length > 9) {
        	 var video ;
        	if (code.toLowerCase().includes("youtube")) {
        		var split = code.split('v=');
        		if (split.length > 1 && split[1].length < 12) {
                    video = $("<iframe src=\"https://www.youtube.com/embed/"+split[1]+"\" style=\"width: 100%;border:none;\"></iframe>");
				}
			} else if (code.toLowerCase().includes("vimeo")) {
                var split = code.split('.com/');
                if (split.length > 1 && split[1].length < 12) {
                    video = $("<iframe src=\"https://player.vimeo.com/video/"+split[1]+"\" style=\"width: 100%;border:none;\"></iframe>");
                }
			}
        	$(container).empty().append($('<div class="iframe-wrapper">').append(video));
        	var videoEdit = container.siblings('.touch-container').find('#edit-content-block').data('custom-videoEdit');
        	
        	videoEdit.options.properties.url = code;
        	videoEdit.element.video = video;
        	
        	container.addClass("ratio_16x9");
		}
    });

    $(".doc-page-area").on("change", "#imageUpload", function(evt)
    {
        var formData = new FormData();
        var proposalId = $("#param-proposal-id").val();
        var container = $(this).closest('.content-holder')

        var file = evt.target.files;
        
        formData.append("proposal", proposalId);

        if (FileReader && file && file[0])
        {
            formData.append("image", file[0]);
            var reader = new FileReader();
            reader.onload = function(e)
            {
                switch (file[0].type)
                {
                    case "image/gif":
                    case "image/jpeg":
                    case "image/png":
                        if (file[0].size <= 5242880)
                        {
                            $.ajax({
                                type : 'POST',
                                url : '/api/traveldoc/attach/image',
                                data : formData,
                                cache : false,
                                contentType : false,
                                processData : false
                            }).done(function(data)
                            {
                                if (data.success)
                                {
                                    loadImageContainer(data.link, data.name, container);
                                }
                                else
                                {

                                }
                            });
                        }
                        else
                        {
                            swal("Upload failed", "The file you selected is too large. Attachments can be a maximum of 5MB.", "error");
                        }
                        break;
                    default:
                        swal("File type not supported!", "Only gif, jpeg and png are supported.", "error");
                }
            };
            reader.readAsDataURL(file[0]);
        }
        else
        {
            alert("This browser do not support file upload.");
        }

    });

	$("div.editor-blocks-panel").on("click", ".block-button-container", function(){
		var buttonId = $(this).data('id');

		if (buttonId == 'coverpage-chooser') {
			$('.side-bar-cover-container .block-head .content-title').text('CoverPage Chooser');
			$('.title-back-button').show();
			$('.title-back-button').data('id', 'coverpage-main').data('type', 'coverpage');
			$('#coverpage-main-container').hide();
			$('#coverpage-dyn-loader').show().html('<div class="content-new-loader"></div>');
			setupCoverPageSubContainer(buttonId);

		}
	});

    $(document).on('click', '.block-button-container.content-folder', function() {
        $('.black-square-container').empty().html('<div class="content-new-loader"></div>');
       
        $('.title-back-button').show();
        if($(this).data('type')=='sub-folder') {
        	$('.title-back-button').data('folder', $(this).data('id'));
        }
        var subfolder = $(this).data('type')=='sub-folder' ? $(this).find('span').text() : "";
        
        var libraryType;
        if ($(this).hasClass('text-folder')) {
        	libraryType = "text";
		} else if ($(this).hasClass('images-folder')) {
			libraryType = "images";
        } else if ($(this).hasClass('videos-folder')) {
        	libraryType = "videos";
        }
        
        $('.title-back-button').data('type', libraryType);
        $('.side-bar-loaded-container .content-title').text($(this).find('span').text());
        loadLibrary(libraryType, $(this).data('id'), subfolder);
    });



	$("ul.nav-panel-set").on("click", "li", function(){
		var buttonId = $(this).attr('id');
		var activeEle = $(this).find('.nav-icon');

		if ($(activeEle).hasClass('active')) {
			if ($('.editor-blocks-panel').hasClass('visible')) {
                $('.editor-blocks-panel').hide();
                $('.editor-options-panel').attr('style', 'border-left:1px solid lightgray;');
                $('.editor-blocks-panel').removeClass('visible');
                $('.panel-padding-blocks').attr('style', 'padding-right:75px;');
                $('.froala-tool-holder').css('width', '100%').css('width', '-=306px');
                return;
			} else {
                $('.editor-blocks-panel').show();
                $('.editor-options-panel').removeAttr('style');
                $('.editor-blocks-panel').addClass('visible');
                $('.panel-padding-blocks').removeAttr('style');
                $('.panel-padding-blocks').attr('style', 'padding-right:375px;');
                $('.froala-tool-holder').css('width', '100%').css('width', '-=604px');

			}
		} else {
			if (!($('.editor-blocks-panel').hasClass('visible'))) {
                $('.editor-blocks-panel').show();
                $('.editor-options-panel').removeAttr('style');
                $('.editor-blocks-panel').addClass('visible');
                $('.panel-padding-blocks').removeAttr('style');
                $('.panel-padding-blocks').attr('style', 'padding-right:375px;');
                $('.froala-tool-holder').css('width', '100%').css('width', '-=604px');
			}
		}

		$('a.nav-icon').each(function() {
			if ($(this).attr('id').indexOf(buttonId)  >= 0) {
				$(this).addClass('active');
			} else {
				$(this).removeClass('active');
			}

		});

		if (buttonId == 'coverpage-nav-icon') {
			$('#coverpage-nav-icon-button').addClass('active');
			$('.side-bar-style-container').hide();
			$('.side-bar-cover-container').show();
			$('.side-bar-loaded-container').hide();

		} else if (buttonId == 'style-nav-icon') {
				$('#style-nav-icon-button').addClass('active');
				$('.side-bar-style-container').show();
				$('.side-bar-cover-container').hide();
				$('.side-bar-loaded-container').hide();

		} else if (buttonId == 'blocks-nav-icon') {
			$('.side-bar-style-container').hide();
			$('.side-bar-loaded-container').show();
			$('.side-bar-loaded-container .content-title').text('Content Blocks');
			$('button.open-options-modal').html('Search Marketplace');
			$('.black-square-container').empty().html('<div class="content-new-loader"></div>');
			loadBlockTiles();

		} else if (buttonId == 'text-nav-icon') {
			$('.side-bar-style-container').hide();
			$('.side-bar-loaded-container').show();
			$('.side-bar-loaded-container .content-title').text('Text Library');
			$('button.open-options-modal').html('Manage Text Library');
			$('.black-square-container').empty().html('<div class="content-new-loader"></div>');
			loadLibrary('text');
		} else if (buttonId == 'images-nav-icon') {
			$('.side-bar-style-container').hide();
			$('.side-bar-loaded-container').show();
			$('.side-bar-loaded-container .content-title').text('Image Library');
			$('button.open-options-modal').html('Manage Image Library');
			$('.black-square-container').empty().html('<div class="content-new-loader"></div>');
			loadLibrary('images');
		} else if (buttonId == 'videos-nav-icon') {
			$('.side-bar-style-container').hide();
			$('.side-bar-loaded-container').show();
			$('.side-bar-loaded-container .content-title').text('Video Library');
			$('button.open-options-modal').html('Manage Video Library');
			$('.black-square-container').empty().html('<div class="content-new-loader"></div>');
			loadLibrary('videos');
		}
	});

	$("div.editor-blocks-panel").on("click", ".content-files-column.coverpage-image", function(){
		var buttonId = 'url('+$('img', this).attr("src").replace('_thumb','')+')';
		var imageName = $($(this).find('img')).attr('alt');

		if (buttonId != null) {
			$('.cover-page-chooser-button-main').data('bg', buttonId);
            coverpageStyles("coverpageBGImage", buttonId);
			showMainCoverPageMenu("coverpage", imageName);
		} else {
			showMainCoverPageMenu();
		}
	});

	$("div.block-head").on("click", ".title-back-button", function(){
		var libraryType = $(this).data('type');
		var folder = $(this).data('folder');

		if(!folder){
			$('.title-back-button').hide();
		}
		$('.title-back-button').removeData( "id" ).removeData("folder");
		
		if (libraryType == 'coverpage') {
			$('.side-bar-cover-container .block-head .content-title').text('Cover Page Customizer');
			$('#coverpage-main-container').show();
			$('#coverpage-dyn-loader').hide().empty();

		} else if (libraryType == 'text') {
            $('.side-bar-loaded-container .content-title').text('Text Library');
            $('.black-square-container').empty().html('<div class="content-new-loader"></div>');
            loadLibrary('text', folder);
        } else if (libraryType == 'images') {
            $('.side-bar-loaded-container .content-title').text('Image Library');
            $('.black-square-container').empty().html('<div class="content-new-loader"></div>');
            loadLibrary('images', folder);
        } else if (libraryType == 'videos') {
            $('.side-bar-loaded-container .content-title').text('Video Library');
            $('.black-square-container').empty().html('<div class="content-new-loader"></div>');
            loadLibrary('videos', folder);
        }
	});
	$('#coverpageVisible').each(function() {
		switcheryCoverpageVisibility = new Switchery(this, {
			color : '#009b46',
			size : 'small'
		});

		$(this).change(function()
		{
			if ($(this).is(':checked'))
			{
				$('#coverpage-visibility-title').text('Cover Page Visible');
			}
			else
			{
				$('#coverpage-visibility-title').text('Cover Page Hidden');
			}
		}).change();
	});

	$('#coverpageContentBoxSwitch').each(function() {
		switcheryCoverpageVisibility = new Switchery(this, {
			color : '#009b46',
			size : 'small'
		});

		$(this).change(function()
		{
			if ($(this).is(':checked'))
			{
				$('#coverpage-content-box-style-title').text('Choose color:');
				$('#cover-page-visibility-switch .button-content .round-replacer-small').show();
				$('.cover.coverpage-main-image').removeClass('content-box-hidden');
				var backgroundColor = $('div.proposalTitle').css('background-color');
				var blockColorHolder = $("div#cover-page-visibility-switch").find('.round-replacer-small');
				$(blockColorHolder).css('background-color', backgroundColor);
				$(blockColorHolder).find('.sp-preview-inner').css('background-color', backgroundColor);
			}
			else
			{
				$('#coverpage-content-box-style-title').text('Content box hidden');
				$('#cover-page-visibility-switch .button-content .round-replacer-small').hide();
				$('.cover.coverpage-main-image').addClass('content-box-hidden');
				// $('.proposalTitle').css('background-color', 'rgba(0, 0, 0,
				// 0);');
				// $('.coverData').css('background-color', 'rgba(0, 0, 0, 0);');
			}
		}).change();
	});

	$("#cover-page-content-box-color").spectrum({
		preferredFormat : "hex",
		replacerClassName : 'round-replacer-small',
		showInput : true,
		hide : function(tinycolor)
		{
			$('.tableStyle .editorArea').css('color', tinycolor.toHexString());
		},
		change: function(color) {
            coverpageStyles("coverpageTitleBG",color.toHexString());
		}
	});

	$("#cover-page-content-box-title-text-color").spectrum({
		preferredFormat : "hex",
		replacerClassName : 'round-replacer-small',
		showInput : true,
		hide : function(tinycolor)
		{
			$('.tableStyle .editorArea').css('color', tinycolor.toHexString());
		},
		change: function(color) {
            coverpageStyles("coverpageTitleColor",color.toHexString());
		}
	});

	$("#cover-page-content-box-content-text-color").spectrum({
		preferredFormat : "hex",
		replacerClassName : 'round-replacer-small',
		showInput : true,
		hide : function(tinycolor)
		{
			$('.tableStyle .editorArea').css('color', tinycolor.toHexString());
		},
		change: function(color) {
            coverpageStyles("coverpageInfoColor",color.toHexString());
            coverpageStyles("coverpageAuthorColor",color.toHexString());
		}
	});

	$('#proposalTitleInput, #proposalSubtitleInput, #proposalExtraInput, #proposalCusNameInput, #proposalConNameInput, #proposalConComInput, #proposalConPhoneInput, #proposalAddEmailInput').on('input', function() {
		var field = $(this).data('field');
		$('.' + field).text($(this).val());
        $('#' + field).text($(this).val());
	});

	$('#form-add-template').validate({
		errorElement : 'span',
		errorClass : 'help-block',
		errorPlacement : function(error, element)
		{

		},
		ignore : '',
		rules : {
			templateTitle : {
				minlength : 2,
				maxlength : 50,
				required : true
			},
			templateDescription : {
				minlength : 2,
				maxlength : 255,
				required : true
			},
		},
		highlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight : function(element)
		{
			$(element).closest('.form-group').removeClass('has-error');
		},
		success : function(label, element)
		{
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		submitHandler : function(form)
		{
			processSaveTravelDoc();
		}
	});

//	$(document).on('mouseover', '.content-holder', function(){
//		if($(this).hasClass('linked')){
//			$(this).prev('.droppable-holder').addClass('active');
//			$(this).next('.droppable-holder').addClass('active');
//		}
//	}).on('mouseout', '.content-holder', function(){
//		var contentHolder = $(this);
//		if(contentHolder.hasClass('linked')){
//			setTimeout(function(){ 
//				contentHolder.prev('.droppable-holder').removeClass('active');
//				contentHolder.next('.droppable-holder').removeClass('active');
//			},1000); 
//		}
//	});
	
//	 $(contentHolder).droppable({ tolerance:
//		 'pointer', over: function(event, ui) {
//		 console.log('in');
//		 $(this).prev('.droppable-holder').addClass('active');
//		 $(this).next('.droppable-holder').addClass('active'); },
//		 out: function(event, ui) {
//		 console.log('out');
//		 $(this).prev('.droppable-holder').removeClass('active');
//		 $(this).next('.droppable-holder').removeClass('active'); },
//		 drop: function(event, ui) {
//		 $('.droppable-holder').removeClass('active'); }
//		 });
	
	$(document).on('keyup', '.section-title-input', function(){
		var input = $(this);
		var title = input.val().length==0?'New Section':input.val();
		var section = input.closest('.doc-page-area-sheet')
		section.attr('data-title', title);
		var sectionId = section.attr('id');
		var tabButton = $('#document-section-tabs [data-section='+sectionId+']');
		tabButton.attr('data-title', title).data('title', title);
		$('.tab-label', tabButton).text(title);
	})
	
	$("#document-section-tabs").on('click', '.removeButton', deleteCurrentSection);
	
	setupSortables();
	
	setupContentBlocks();
	window.onbeforeunload = confirmExit;
	
	$('#document-section-tabs').sortable({
		handle: '.section-sort-handle',
		items: '.dynaTab',
		start: function(event, ui){
			$(ui.item).find('.section-sort-handle').addClass('active');
		},
		stop: function(event, ui){
			$(ui.item).find('.section-sort-handle').removeClass('active');
		},
		update: function(event, ui){
			var currentSection = $(ui.item).find('.section-button').attr('data-section');
			var prevSection = $(ui.item).prev().find('.section-button').attr('data-section');
			$('.doc-page-area-sheet[id='+currentSection+']').insertAfter($('.doc-page-area-sheet[id='+prevSection+']'));
		}
	});
	
	$('.editor-content-scroller').on('scroll',function(){
		var activeTab;
		var mostVisible = 0;
		$('.doc-page-area-sheet').each(function(){
			var elementTop = $(this).offset().top;
			var elementBottom = elementTop + $(this).outerHeight();
			var viewportTop = $(window).scrollTop();
			var viewportBottom = viewportTop + $(window).height();
			if( elementBottom > viewportTop && elementTop < viewportBottom) {
				var visibleHeight = $(this).outerHeight() - ((elementTop<viewportTop)?(viewportTop-elementTop):0) - (elementBottom>viewportBottom?(elementBottom-viewportBottom):0);
				if(mostVisible < visibleHeight) {
					activeSectionId = $(this).attr('id');
					mostVisible = visibleHeight;
				}
			}
		});
		if($('#document-section-tabs .section-button.active').attr('id') != activeSectionId) {
			$('#document-section-tabs .section-button.active').removeClass('active');
			$('#document-section-tabs .section-button[data-section='+activeSectionId+']').addClass('active');
			if (activeSectionId=='content-section-coverpage') {
				$('#coverpage-nav-icon').show();
	            $('#blocks-nav-icon-button').hide();
	            $('#text-nav-icon-button').hide();
	            $('#images-nav-icon-button').hide();
	            $('#videos-nav-icon-button').hide();
				$('.side-bar-loaded-container').hide();
				if(!$('#style-nav-icon-button').hasClass('active')) {
					$('#coverpage-nav-icon-button').addClass('active');
					$('.side-bar-cover-container').show();
				}
			} else {
				$('#coverpage-nav-icon').hide();
	            $('#blocks-nav-icon-button').show();
	            $('#text-nav-icon-button').show();
	            $('#images-nav-icon-button').show();
	            $('#videos-nav-icon-button').show();
				$('.side-bar-cover-container').hide();
				if(!$('#style-nav-icon-button').hasClass('active')) {
					$('#block-nav-icon-button').addClass('active');
					$('.side-bar-loaded-container').show();
				}
			}
		}
	});
	
	$('.side-bar-style-container [name]').on('change keyup paste', function(){
		setupStyleSettingsCSS();
	})
	
	$('.side-bar-style-container input[type=checkbox]').on('change', function(){
		setupActiveStyles($(this));
	});
	
	$(document).on('click', '#edit-content-block', function(){
		showEditPanel($(this));
	});
	$(document).on('show.bs.dropdown', '.vecontrol-append .dropdown', function(){
		$(this).closest('.vecontrol-append').addClass('active');
	});
	$(document).on('hide.bs.dropdown', '.vecontrol-append .dropdown', function(){
		$(this).closest('.vecontrol-append').removeClass('active');
	});
});


function saveToLocalStorage()
{
	if (traveldocChanged())
	{
		var travelDocId = $("#param-proposal-id").val();
		var travelDoc = getCurrentTravelDoc();
		logger.info("Saving TravelDoc " + travelDocId + " to localStorage");
		localStorage.setItem('unsavedTravelDoc-' + travelDocId, JSON.stringify(travelDoc));
		lastSave = MD5(JSON.stringify(travelDoc));
	}
}
function confirmExit()
{
	if (traveldocChanged())
	{
		return "You have made changes to this travel document. Are you sure you would like to navigate away from this page without saving?";
	}
}

function traveldocChanged()
{
	var currentHash = MD5(JSON.stringify(getCurrentTravelDoc()));

	return (lastSave != "" && lastSave != currentHash);
}

function setupContentBlocks(){
	$.ajax({
		type : "GET",
		url : '/api/blocks/getAllBlocks'
	}).done(function(data, textStatus, jqXHR)
	{
		contentBlocks = data;
	});
}

function deleteCurrentSection()
{
	var $tabButton = $("#document-section-tabs .section-button.active");
	var $tab = $tabButton.parent();
	var editorToDelete = $tabButton.attr('data-section');
	if (!$('#'+editorToDelete).find('#delete-content-block').hasClass('disabled'))
	{
		swal({
			title : "Warning!",
			text : "Are you sure you would like to remove the current section?",
			type : "warning",
			showCancelButton : true,
			cancelButtonText : "No",
			confirmButtonColor : "#BA131A",
			confirmButtonText : "Yes, delete it",
			closeOnConfirm : true
		}, function(confirm)
		{
			if (confirm)
			{

				var $switchTo = $tab.next();
				if ($switchTo.length == 0)
				{
					$switchTo = $tab.prev();
				}
				$("button", $switchTo).click();


				$('#' + editorToDelete).remove();

				$tab.slideUp(function()
				{
					$(this).remove();
				});
			}
		});
	}
	else
	{
		swal({
			title : "Warning!",
			text : "You cannot delete this section as it contains locked content.",
			type : "warning",
			closeOnConfirm : true
		});
	}
}
function loadImageContainer(link, name, container) {

    var imageDiv = "<div style=\" width: 100%; text-align: center;\"><img class=\"contentImageBlockHolder\" src=\""+link+"\" alt=\""+name+"\" style=\"max-width: 100%;\"></div>";
    var content = $('<div class="item" style="vertical-align: top;display: inline-block;text-align: center;padding: 15px;border: 1px dotted;">');
    var imageDiv = "<div style=\" width: 100%; height: auto;text-align: center;\"><img class=\"contentImageBlockHolder\" src=\""+link+"\" alt=\""+name+"\" style=\"max-width: 100%;\"></div>";
    content.append(imageDiv);
    $(container).empty();
    createBlockWithMenu(container, content);
}
function loadTextContainer(text, container) {

    var textDiv = '<div class="panel-froala" style="width: 100%; min-height: 50px;">'+text+'</div>';
    
    $(container).empty();
    createBlockWithMenu(container, textDiv);
}

function loadVideoContainer(videolink, container) {

    var videoDiv = '<div class="videoContainer ratio_16x9" style="vertical-align: top;display: inline-block;text-align: center;width: 100%;padding: 15px;border: 1px dotted;"><div class=\"iframe-wrapper\"><iframe src="'+videolink+'" style="width: 100%;"></iframe></div></div>';
    $(container).empty();
    var url;
    if(videolink.indexOf('youtube')>=0) {
    	url = "//www.youtube.com/watch?v=" + videolink.substring(videolink.lastIndexOf('ed/')+3, videolink.lastIndexOf('?')<0?videolink.length:videolink.lastIndexOf('?'));
    }
    else {
    	url = "//www.vimeo.com/" + videolink.substring(videolink.lastIndexOf('o/')+2, videolink.lastIndexOf('?')<0?videolink.length:videolink.lastIndexOf('?'))
    }
    container.data('properties', {properties: {url: url, aspect_ratio:'16x9'}});
    createBlockWithMenu(container, videoDiv);
}

function restoreTravelDoc() {
    var travelDocID = $("#param-proposal-id").val();
    var eKey = $("#param-e-key").val();

    if (localStorage['unsavedTravelDoc-' + travelDocID])
    {
        swal({
            title : "Unsaved TravelDoc!",
            text : "Your last TravelDoc wasn't saved properly, would you like to restore it?",
            type : "warning",
            showCancelButton : true,
            cancelButtonText : "No, ignore",
            confirmButtonText : "Yes, restore it!",
            closeOnConfirm : true
        }, function(restore)
        {
            if (restore)
            {
                restoreStorageTravelDoc(travelDocID, eKey);
            }
            else
            {
            	restoreServerTravelDoc(travelDocID, eKey);
            }
        });
    }
    else
    {
        restoreServerTravelDoc(travelDocID, eKey);
    }
}

function restoreStorageTravelDoc(travelDocID, eKey)
{

	unsavedTravelDoc = JSON.parse(localStorage['unsavedTravelDoc-' + travelDocID]);

	logger.info("Loading TravelDoc from local storage.");
	startLoadingForeground(loading_and_decrypting);

	// don't make the request straight away...
	// let the browser finish loading and the loading dialog to display
	setTimeout(function()
	{

		restoredTravelDoc = unsavedTravelDoc;
		restoreFromData(unsavedTravelDoc);

	}, 750);
}

function restoreServerTravelDoc(travelDocID, eKey) {
    startLoadingForeground(loading_and_decrypting);
    setTimeout(function()
    {

        $.ajax('/api/traveldoc/loadData', {
            type : "POST",
            dataType : 'json',
            data : {
                traveldocId : travelDocID,
                eKey : eKey
            }
        }).done(function(data, textStatus, jqXHR)
        {
            restoredTravelDoc = data;
            restoreFromData(data);
        });

    }, 750);
}

function restoreFromData(data) {
	logger.info("Raw data recieved");

    restoreSections(data.sections);

    logger.info("Done firing events for rebuilding document.");

    stopLoadingForeground();

    $(document).one('foregroundLoadingComplete', function()
    {

		if ($('#document-section-tabs .section-button.active').attr('data-section') == 'content-section-coverpage') {
			$('#coverpage-nav-icon').show();
			$('#blocks-nav-icon-button').hide();
			$('#text-nav-icon-button').hide();
			$('#images-nav-icon-button').hide();
			$('#videos-nav-icon-button').hide();
			$('#coverpage-nav-icon-button').addClass('active');
			$('.side-bar-cover-container').show();
			$('.side-bar-style-container').hide();
			$('.side-bar-loaded-container').hide();
		}
        $('.documentTagSrc.cust-fld-date').each(function()
        {
            $(this).datepicker('update', moment($(this).val()).toDate())
        });
        initFroalaEditor();
        // Initialise drop area when finished loading proposal
        $.each($('.content-holder:not([data-type=block-columns])'), function(j, content)
        {
            if ($(content).attr('data-type')) {
            	var type = $(content).attr('data-type');
            	var editable = type=='block-video' || type=='content-library-videos';
                var toolbar = $('<div class="touch-container">');
            	toolbar.append($('<span class="block-handle"><a><i class="fas fa-arrows-alt"></i> '+ve_elements[type]+'</a></span>'));
                var contentHolder = $('<span class="content-holder-toolbar">');
                var toolbarHolder = $('<div class="toolbar-button-holder">');
                var save = $('<div type="button" data-toggle="modal" data-target="#saveContentModal" class="cht-btn cht-button-save">').append($('<i class="fas fa-save"></i>'));
                var view = $('<div type="button" class="cht-btn cht-button-hide">').append($('<i class="fas fa-eye-slash"></i>'));
                var move = $('<div class="cht-btn cht-button-move">').append($('<i class="fas fa-arrows-alt"></i>'));
                var edit = $('<div type="button" id="edit-content-block" data-ve_type="'+ve_widgets[type]+'" class="cht-btn cht-button-edit'+(editable?"":"hidden")+'">').append($('<i class="fa fa-edit"></i>'));
                
                var trash = $('<div type="button" id="delete-content-block" class="cht-btn cht-button-delete">').append($('<i class="fas fa-trash-alt"></i>'));
                
                toolbarHolder.append(save).append(view).append(move).append(edit).append(trash);
                contentHolder.append(toolbarHolder);
                toolbar.append(contentHolder);
                $(content).append(toolbar);
                
                if(editable) {
                	var properties = $(content).data('properties');
                	edit[ve_widgets[type]](properties?properties : DEFAULTS[ve_widgets[type]]);
                }
                
                if (type == "block-itinerary") {
                    $('#block-itinerary').css('color', 'rgba(0, 0, 0, 0.17)');
                    $('#block-itinerary').draggable( 'disable' );
				}
            }

        });
        setTimeout(function(){
        	lastSave = MD5(JSON.stringify(getCurrentTravelDoc()));
        }, 1000);

    });
}

function initFroalaEditor()
{

    var uninitialisedFroalaEditors = $(".panel-froala");
    var header = {};
    header[csrfTokenName] = csrfTokenValue;
    new FroalaEditor('.panel-froala', {
    	zIndex: 2003,
        enter : FroalaEditor.ENTER_P,
        fontSize : [ '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '22', '24', '26', '28', '30', '60', '96' ],
        fontSizeDefaultSelection : '14',
        toolbarButtons : [ "bold", "italic", "underline", "strikeThrough", "subscript", "superscript", "|", "fontSize", "textColor", "backgroundColor", "paragraphStyle", "|", "paragraphFormat", "align", "|", "formatOL", "formatUL", "|", "outdent", "indent", "|", "quote", "insertLink", "insertTable", "insertImage", "insertVideo", "|", "specialCharacters", "insertHR", "selectAll", "clearFormatting", "|", "spellChecker", "|", "undo", "redo", "dataItems" ],
        pluginsEnabled : [ "align", "charCounter", "codeBeautifier", "codeView", "colors", "draggable", "embedly", "emoticons", "entities", "file", "fontFamily", "fontSize", "image", "imageManager", "inlineStyle", "lineBreaker", "link", "lists", "paragraphFormat", "paragraphStyle", "quote", "save", "table", "url", "wordPaste" ],
        key : 'vYA6mE5C5G4A3jC1QUd1Xd1OZJ1ABVJRDRNGGUE1ITrE1D4A3C11B1B6E6B1F4I3==',
        quickInsertButtons : [ 'image', 'table', 'ul', 'ol', 'hr' ],
        scrollableContainer: '.doc-page-area',
        toolbarContainer: '.froala-tool-holder',
        charCounterCount: false,
        attribution: false,
		// Set the image upload parameter.
		imageUploadParam : 'image',
		// Set the image upload URL.
		imageUploadURL : '/api/traveldoc/attach/image',
		// Additional upload params.
		imageUploadParams : {
			proposal : $('#param-proposal-id').val()
		},
		requestHeaders : header,
        events: {
       	 initialized: function(ev) {
       		 var editor = this;
       		 editor.$el.attr('data-editor-id', editor.id);
       		 customizeFroalaEditor(ev, editor);
       	 },
       	 'toolbar.show': function(){
       		console.log('show');
       		$(this.$el).closest('.panel-froala').addClass('z-down');
       	 },
       	'toolbar.hide': function(){
       		console.log('hide');
       		$(this.$el).closest('.panel-froala').removeClass('z-down');
       	 }
        }
   });
}

function restoreSections(sections)
{
    var pricingTableAdded = false;
    var itineraryTableAdded = false;
    $.each(sections, function(i, section)
    {

        var currentSectionId = 0;
        // Create and/or move the section into the correct index.

        if (section.type.toLowerCase() == 'dynamic')
        {
            logger.info("Restoring dynamic section with title: " + section.title);
             currentSectionId = setupSections(section.title, section);
            // currentSectionId = createSection(section.title, false);
        }
        else if (section.type.toLowerCase() == 'cover')
        {
            logger.info("Restoring coverpage");
            currentSectionId = setupSections(section.title, section);
        }
        else
        {
            logger.error("Unknown section type: " + section.type);
            // currentSectionId = createSection(section.title || section.type,
			// false);
        }
		if (section.type != 'generated') {
            reorderSectionToIndex(currentSectionId, i);

            var $currentSection = $("#" + currentSectionId + " .page-content-area");

            // Populate the content
            $.each(section.content, function(j, content)
            {
                if (content.type != 'generated') {
                    restoreContent(j, content, $currentSection);
				}
            });

		}

    });
}

function insertContentIntoEditor(targetSection, dataIndex, itemType, itemId, autoscroll, fixedPanel, top)
{
    var sendData = {};
    var proposalId = $("#param-proposal-id").val();
    sendData.type = itemType;
    sendData.proposalId = proposalId;
    if (itemId != undefined)
    {
        sendData.id = itemId;
    }

    return $.ajax({
        type : "POST",
        url : '/api/editor/content',
        data : sendData
    }).done(function(data, textStatus, jqXHR)
    {
        if (!top)
        {
            insertContentIntoPanel(targetSection, dataIndex, autoscroll, fixedPanel, data);
            if (itemType == 'Spreadsheet')
            {
                var $panel = $('.panel[dataOrder="' + dataIndex + '"]', targetSection);
                if (itemId)
                {
                    $.ajax({
                        type : "POST",
                        url : '/api/editor/spreadsheet',
                        data : {
                            itemId : itemId
                        }
                    }).done(function(data, textStatus, jqXHR)
                    {
                        console.log(data);
                        initKendoSpreadsheet($panel, data);
                    });
                }
                else
                {
                    initKendoSpreadsheet($panel);
                }
            }
        }
        if (itemType == "itinerary")
        {
            showHideRemarks();
        }
    });
}

function restoreContent(j, content, $currentSection)
{

    if (content.type == 'generated')
    {
        if (content.id == 'con-info')
        {
            var $docCover = $("#doc-cover");
            $.each(content.metadata, function(key, value)
            {
                var input = $("#" + key, $docCover);
                if (input.is(':checkbox'))
                {
                    input.prop('checked', value == 'on');
                }
                else
                {
                    input.val(value);
                }
            });


            var $tableOfContent = $('#tableOfContent').change(function()
            {
                if ($('#tableOfContent').is(':checked'))
                {
                    $('#tableOfContentLevelGroup').show();
                }
                else
                {
                    $('#tableOfContentLevelGroup').hide();
                }
            }).change();

            var $globalTitleHidden = $('#globalTitleHidden').change(function()
            {
                if ($('#globalTitleHidden').is(':checked'))
                {
                    hideHeadings(true);
                }
                else
                {
                    var all = true;
                    $('.noTitle-switch').each(function()
                    {
                        if (!($(this).is(':checked')))
                        {
                            all = false;
                        }
                    });
                    if (all)
                    {
                        hideHeadings(false);
                    }
                }
            }).change();

            var tableOfContentSwitchery = new Switchery($tableOfContent.get(0), {
                size : 'small'
            });

            var landscapeModeSwitchery = new Switchery($('#landscapeMode').get(0), {
                size : 'small'
            });
            var globalTitleHiddenSwitchery = new Switchery($globalTitleHidden.get(0), {
                color : '#fc0d00',
                size : 'small'
            });

            if ($('#showRemarks').length > 0)
            {
                var remarksSwitchery = new Switchery($('#showRemarks').get(0), {
                    size : 'small'
                });
            }

            // if (content.metadata['globalTitleHidden'] === "on")
            // {
            // $('#globalTitleHidden').click();
            // }


            $(".documentTagSrc", $docCover).keyup();// special
            if (content.metadata['cc-emails'] != undefined)
            {
                $("#cc-emails", $docCover).importTags(content.metadata['cc-emails']);
            }

            $("#displayOrderButton").prop('checked', content.metadata['displayOrderButton'] === "true");
            $("#showOrderChk").addClass(content.metadata['displayOrderButton'] == "true" ? "checked" : "");
            // Restore coverpage and color if possible...
            if (content.metadata['spectrumColor'] != undefined)
            {
                var spectrumColor = content.metadata['spectrumColor'];
                logger.info("Restoring doc color:" + spectrumColor);
                $('#colorPicker .btn-qcloud-design-' + spectrumColor).click();

                var coverPageImage = content.metadata['coverPageImage'];
                logger.info("Restoring coverPageImage ID:" + coverPageImage);
                $('#coverPicker .coverPageOption[pageId="' + coverPageImage + '"]').click();
            }
            else
            {
                $('#colorPicker .btn').first().click();
            }
        }
        else if (content.id == 'con-custom-fields')
        {
            var $docCover = $("#doc-cover");
            $.each(content.metadata, function(key, value)
            {
                if (customFields[key] && customFields[key].type == 'date')
                {
                    var _d = moment(value, 'YYYY-MM-DD');
                    if (_d.isValid())
                    {
                        $("#" + key, $docCover).datepicker('update', _d.toDate());
                    }
                    else
                    {
                        $("#" + key, $docCover).datepicker('update', '');
                    }
                }
                else
                {
                    $("#" + key, $docCover).val(value);
                }
            });
            // $(".documentTagSrc", $docCover).keyup();// special
        }
        else if (content.id.startsWith('con-fnb-'))
        {
            var categoryId = content.id.substring(8);
            var newSectionIndex = $("#doc-solution .contentContainer > .panel").length;
            logger.info('Restoring fNb category at index:' + newSectionIndex + ', with ID:' + categoryId);

            var $fnbPanel = $("#htmlFragments .con-fnb").clone();
            $fnbPanel.attr('dataId', 'con-fnb-' + categoryId);
            $fnbPanel.attr("dataOrder", newSectionIndex);

            $("#doc-solution .contentContainer").append($fnbPanel);
        }
        else if (content.id == 'con-plans')
        {
            addPricingTableAction(j);
            var $panel = $('.panel[dataOrder="' + j + '"]', $currentSection);
            if (content.breakPageBeforeInPDF === "on")
            {
                $panel.find('.panel-heading').find('.js-switch').trigger('click');
            }
            pricingTableAdded = true;
            return;
        }
        else if (content.id == 'con-iti')
        {
            return;
        }
        var $targetContentPane = $('.panel[type="generated"][dataid="' + content.id + '"]');
        $targetContentPane.attr("dataOrder", j);

        var $siblingContent = $targetContentPane.siblings(".panel");
        $.each($siblingContent, function(k, sibling)
        {
            var siblingOrder = parseInt($(sibling).attr('dataOrder'));
            if (siblingOrder < j)
            {
                $targetContentPane.insertAfter($(sibling));
            }
        });
    }
    else
    {
        var dynType = undefined;
        if (content.type == 'block-image')
        {
            dynType = 'Images';
        }
        else if (content.type == 'block-video')
        {
            dynType = 'Videos';
        }
        else if (content.type == 'block-text')
        {
            dynType = 'Text';
        }
        else if (content.type == 'block-destination')
        {
            dynType = 'destination';
        }
        else if (content.type == 'block-attachment')
        {
            dynType = 'fileHandle';
        }
        else if (content.type == 'block-itinerary')
        {
            dynType = 'itinerary';
        }
        else if (content.type == 'block-pricing-table')
        {
            dynType = 'pricing';
        }
		else if (content.type == 'block-columns')
		{
			dynType = 'columns';
		}
		else if (content.type == 'block-zerorisk')
		{
			dynType = 'zerorisk';
		}
        else
        {
            logger.error("Unknown content type: " + content.type);
        }

        if (dynType != undefined)
        {

            // have determined the type so load it.
            // var callback = insertContentIntoEditor($currentSection, j,
			// dynType, content.id, false);

            // do some post-initilisation

			if (dynType == 'columns')
			{
				var colContent = content.colContent;
                $.each(colContent.elements, function(j, contentSub)
                {
                    if (contentSub.type != 'generated') {
                        restoreContent(j, contentSub, $currentSection);
                    }
                });
			}
			if (dynType == 'Text')
			{
                $("div[data-type='block-text']").each(function() {
                    // initFroalaEditor();
				});

			}
            if (dynType == 'itinerary') {

                $('#block-itinerary').css('color', 'rgba(0, 0, 0, 0.17)');
                $('#block-itinerary').draggable( 'disable' );
                var itineraryContent = $("div[data-type='block-itinerary']");
                $(itineraryContent).empty();
                restoreBlock($(itineraryContent), "itinerary", "itinerary")
			}
            if (dynType == 'pricing') {
                $("div[data-type='block-pricing-table']").each(function() {
                    var id = $(this).data('id');
                    if (id.indexOf("pricing-table-itinerary") >= 0) {
                        $(this).empty();
                        restoreBlock($(this), "pricing", "itinerary")
                    }
                });
            }
            if (dynType == 'columns') {
                $("div[data-type='block-pricing-table']").each(function() {
                    var id = $(this).data('id');
                    if (id.indexOf("pricing-table-itinerary") >= 0) {
                        $(this).empty();
                        restoreBlock($(this), "pricing", "itinerary")
                    }
                });
            }
            if (dynType == 'zerorisk') {

				var element = $($("div[data-type='block-zerorisk']")[0]);
				restoreZeroRiskBlockWithMenu(element, "<div class=\"zerorisk-container\"></div>", "", "");
				$('#block-zerorisk').css('color', 'rgba(0, 0, 0, 0.17)');
				$('#block-zerorisk').draggable( 'disable' );
			}
        }
    }
}


function showMainCoverPageMenu(eleChanged, eleValue) {
	if (eleChanged != null) {
		if (eleChanged == "coverpage") {
			$('#coverpage-title').text(eleValue);
		}
	}
	$('.side-bar-cover-container .block-head .content-title').text('Cover Page Customizer');
	$('.title-back-button').hide();
	$('.title-back-button').removeData( "id" );
	$('#coverpage-main-container').show();
	$('#coverpage-dyn-loader').hide().empty();
}

function setupCoverPageSubContainer(eleID) {
	
	var folder = true;
	var folderCount = 3;
	var files = true;
	var filescount = 10;

	var folderhtml = '';
	var fileshtml = '';
	
	$.ajax({
		url: "/api/loadCoverPages",
		success: function(data) {
		
			var lib = data.libraryItems;
			var fold = data.libraryFolders;

			var folders = [];
			var templatesFiles = [];
			var folderhtml = '';
			var fileshtml = '';

			/*
			 * if (fold.length > 0) { folderhtml += '<h7>Folders:</h7>'; }
			 */
			
			/*
			 * if (folder && folderCount > 0) { folderhtml += '<h7>Folders:</h7>';
			 * for (var i=1;i<=folderCount;i++) { folderhtml += '<div
			 * class="block-button-container" id="cover-page-chooser-button"
			 * data-id="coverpage-chooser" style="margin-bottom: 5px;">' + '<div
			 * class="button-content">' + '<i class="far fa-folder fa-sm"></i>' + '<span>Folder '+
			 * i +'</span>' + '<i class="fas fa-angle-right fa-sm"
			 * style="position:absolute;right:14px;top:11px"></i>' + '</div>' + '</div>'; } }
			 */
			if (data && data.pages.length > 0) {

				$.each(data.pages, function(){
					fileshtml += "<div class=\"content-files-column coverpage-image\"><img src=\""+this.thumbnail+"\" alt=\""+this.name+"\"></div>" ;
					
				});
// fileshtml += "<div class=\"content-files-column coverpage-image\"
// style=\"background:
// url('/application/assets/images/coverpages/cropped_bags4optimised.jpg');\"><div
// class=\"content-info\"><p style=\"margin: 0;\">Demo File</p></div></div>" +
// "<div class=\"column\" style=\"flex: 1;\"></div>";
// fileshtml += "<div class=\"content-files-column coverpage-image\"
// style=\"background:
// url('/application/assets/images/coverpages/cropped_helloworld.jpg');\"><div
// class=\"content-info\"><p style=\"margin: 0;\">Demo File</p></div></div>" +
// "</div>";
// fileshtml += "<div class=\"row content-container-row\" style=\"display:
// flex;margin: 0;padding-bottom:10px;\">" +
// "<div class=\"content-files-column coverpage-image\" style=\"background:
// url('/application/assets/images/coverpages/Travel_Solutions.jpg');\"><div
// class=\"content-info\"><p style=\"margin: 0;\">Demo File</p></div></div>" +
// "<div class=\"column\" style=\"flex: 1;\"></div>";
// fileshtml += "<div class=\"content-files-column coverpage-image\"
// style=\"background:
// url('/application/assets/images/coverpages/zerorisk.jpg');\"><div
// class=\"content-info\"><p style=\"margin: 0;\">Demo File</p></div></div>" +
// "</div>";

				// for (var i=1;i<=filescount;i++)
				// {
				// if (i == filescount) {
				// if ((filescount/2) % 1 == 0) {
				// fileshtml += '<div class="content-files-column
				// coverpage-image" style="background:
				// url(https://my.quotecloud.net/storefs/GEN_COVERPAGES/4/ATTRFILE_image/cropped_bags4optimised.jpg);"><div
				// class="content-info"><p style="margin: 0;">File ' + i +
				// '</p></div></div>' +
				// '</div>';
				// } else {
				// fileshtml += '<div class="content-files-column
				// coverpage-image" style="background:
				// url(https://my.quotecloud.net/stores/GEN_COVERPAGES/6/ATTRFILE_image/cropped_helloworld.jpg);"><div
				// class="content-info"><p style="margin: 0;">File ' + i +
				// '</p></div></div>' +
				// '<div class="column" style="flex: 1;"></div><div
				// class="content-files-column coverpage-image"
				// style="background:
				// url(https://my.quotecloud.net/stores/GEN_COVERPAGES/4/ATTRFILE_image/cropped_bags4optimised.jpg);"><div
				// class="content-info"><p style="margin: 0;">File ' + i +
				// '</p></div></div>' +
				// '</div></div>';
				// }
				// } else {
				// if (i % 2 === 0)
				// {
				// /* we are even */
				// fileshtml += '<div class="content-files-column
				// coverpage-image" style="background:
				// url(https://my.quotecloud.net/stores/GEN_COVERPAGES/6/ATTRFILE_image/cropped_helloworld.jpg);"><div
				// class="content-info"><p style="margin: 0;">File ' + i +
				// '</p></div></div>' +
				// '</div><div class="row content-container-row" style="display:
				// flex;margin: 0;padding-bottom:10px;">';
				// }
				// else
				// {
				// /* we are odd */
				// fileshtml += '<div class="content-files-column
				// coverpage-image" style="background:
				// url(https://my.quotecloud.net/stores/GEN_COVERPAGES/4/ATTRFILE_image/cropped_bags4optimised.jpg);"><div
				// class="content-info"><p style="margin: 0;">File ' + i +
				// '</p></div></div>' +
				// '<div class="column" style="flex: 1;"></div>';
				// }
				// }
				// }
			}
			$('#coverpage-dyn-loader').empty();
			$('#coverpage-dyn-loader').html(fileshtml);
			setTimeout(function(){
				var msnry = new Masonry( '#coverpage-dyn-loader', {
					  itemSelector: '.coverpage-image',
					  columnWidth: 120,
					  gutter: 15
					});
			}, 100);
		},
		error: function() {
			$('#notification-bar').text('An error occurred');
		}
	});
	
}

function deleteImage( $item ) {
	$item = $item.clone();
	$item.fadeOut(function() {
		var $list = $( "ul", $trash ).length ?
			$( "ul", $trash ) :
			$( "<ul class='gallery ui-helper-reset'/>" ).appendTo( $trash );

		$item.find( "a.ui-icon-trash" ).remove();
		$item.append( recycle_icon ).appendTo( $list ).fadeIn(function() {
			$item
				.animate({ width: "48px" })
				.find( "img" )
				.animate({ height: "36px" });
		});
	});
}

function dropItem(clone, dropElem) {
	$('.subtitle.fancy.drop-label').each(function() {
		$(this).remove();
	});

	if (($(clone).hasClass('block-icon'))) {
        // var headerArea = "<div class=\"droppable-holder sortable
		// ui-droppable\" style=\"\n" +
        // "width: 100%; height: 40px;border: 1px solid
		// lightblue;background-color: lightblue;margin-bottom: 20px;display:
		// none;\"><p style=\"\n" +
        // "text-align: center;padding-top: 8;\">Drop your block
		// here</p></div>";
        var contentItem = "<div class=\"content-new-holder\" data-type=\""+ $(clone).attr('id')+"\" data-id=\""+ $(clone).data('id') +"\"><div class=\"content-new-loader\"></div></div>";
        var content = contentItem;
        $(content).insertBefore(dropElem);
		$('.content-new-holder').animate({height:'50px', width:'100%'},
			400, function() {
				$('.content-new-holder').height('auto');
				setTimeout(function() {
					loadContent($('.content-new-holder'))
					// loadDummyContent($('.content-new-holder'));
				}, 500);
			});
	}

}

function buildDependancies(eleType) {

	if (eleType.indexOf("itinerary") >= 0) {

		// Build pricing table holder
		var pricingTile = $('#block-pricing-table');

        // var headerArea = "<div class=\"droppable-holder sortable
		// ui-droppable\" style=\"\n" +
        // "width: 100%; height: 40px;border: 1px solid
		// lightblue;background-color: lightblue;margin-bottom: 20px;display:
		// none;\"><p style=\"\n" +
        // "text-align: center;padding-top: 8;\">Drop your block
		// here</p></div>";
		var contentItem = "<div class=\"content-new-holder\" data-type=\""+ $(pricingTile).attr('id')+"\" data-id=\""+ "itinerary-" + $(pricingTile).data('id') +"\"><div class=\"content-new-loader\"></div></div>";
		var content = contentItem;
		$(content).insertAfter($("div[data-type='block-itinerary']"));
		$('.content-new-holder').animate({height:'50px', width:'100%'},
			400, function() {
				$('.content-new-holder').height('auto');
				setTimeout(function() {
					loadContent($('.content-new-holder'))
				}, 500);
		});



		// var itineraryPrcingEle =
		// $("div").find('[data-type="block-pricing-table-itinerary"]');
		// if (itineraryPrcingEle.length == 0) {
		// var itineraryEle = $("div").find('[data-type="block-itinerary"]');
		// var contentItem = "<div class=\"content-new-holder\"
		// data-type=\"block-pricing-table-itinerary\"><div
		// class=\"content-new-loader\"></div></div>";
		// $(contentItem).insertBefore(itineraryEle);
		// $('.content-new-holder').animate({height:'50px', width:'100%'},
		// 400, function() {
		// $('.content-new-holder').height('auto');
		// setTimeout(function() {
		// loadContent($('.content-new-holder'))
		// }, 500);
		// });
		// }
	}


}


function loadContent(ele, library) {

	var eleType = $(ele).attr('data-type');
	var eleId = $(ele).attr('data-id');

	// if (eleType.indexOf("pricing") >= 0) {
	//
	// createBlockWithMenu(ele, pricingtableview, "");
	// return;
	// }
	
	if(eleType=='block-columns') {
		$(ele).data('id', (eleType + "-" + uniqId()));
		var div = $('<div>');
		$(ele).append(div);
		div.newrow();
		createBlockWithMenu(ele, "", "", "");
	} else {
		$.ajax({
			type: 'POST',
			url: '/api/blocks/getBlockOfType',
			data: {
				travelDocId: travelDocID,
				eleType: eleType,
				eleId: eleId
			},
			success: function (data) {
				console.log(data);

				if (eleType.indexOf("itinerary") >= 0) {
					$(ele).removeAttr('data-id');
					$(ele).data('id', (eleType + "-" + uniqId()));
					createBlockWithMenu(ele, data, "", "");
					$('#block-itinerary').css('color', 'rgba(0, 0, 0, 0.17)');
					$('#block-itinerary').draggable( 'disable' );
					// buildDependancies(eleType);
				} else if (eleType.indexOf("pricing") >= 0) {
					if (eleId.indexOf('itinerary') >= 0) {
						$(ele).removeAttr('data-id');
						var id = (eleType + "-itinerary-" + uniqId());
						ele = $(ele).attr('data-id', id);// data('id', id);
					} else {
						$(ele).removeAttr('data-id');
	                    ele = $(ele).data('id', (eleType + "-" + uniqId()));
					}
					createBlockWithMenu(ele, data, loadPricingToolbar(ele), "");
				} else if (eleType.indexOf("image") >= 0) {
					$(ele).removeAttr('data-id');
					$(ele).data('id', (eleType + "-" + uniqId()));
					var link = $(ele).data('url');
					if (link != null && link != "") {
						createBlockWithMenu(ele, data, "", "", "images");
					} else {
						createBlockWithMenu(ele, data, "", "");
					}

				} else if (eleType.indexOf("zerorisk") >= 0) {
					$(ele).removeAttr('data-id');
					$(ele).data('id', (eleType + "-" + uniqId()));

					createZeroRiskBlockWithMenu(ele, data, "", "");
					$('#block-zerorisk').css('color', 'rgba(0, 0, 0, 0.17)');
					$('#block-zerorisk').draggable( 'disable' );
				} else {
					$(ele).removeAttr('data-id');
					$(ele).data('id', (eleType + "-" + uniqId()));

					createBlockWithMenu(ele, data, "", "");
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	}


}

function loadMainToolbar(ele, topSubBar, TopToolbarAddon) {
//	var toolbar = "";
	var type = $(ele).attr('data-type');
//	var openingDiv = "<div class=\"touch-container\"> " +
//		"<span class=\"block-handle\"><a><i class=\"fas fa-arrows-alt\"></i> "+ve_elements[type]+"</a></span>" +
//		"<span class=\"content-holder-toolbar\">" +
//		"	<div class=\"toolbar-button-holder\">";

	var editable = type=='block-video' || type=='content-library-videos';
    var toolbar = $('<div class="touch-container">');
	toolbar.append($('<span class="block-handle"><a><i class="fas fa-arrows-alt"></i> '+ve_elements[type]+'</a></span>'));
    var contentHolder = $('<span class="content-holder-toolbar">');
    var toolbarHolder = $('<div class="toolbar-button-holder">');
    var save = $('<div type="button" data-toggle="modal" data-target="#saveContentModal" class="cht-btn cht-button-save">').append($('<i class="fas fa-save"></i>'));
    var view = $('<div type="button" class="cht-btn cht-button-hide">').append($('<i class="fas fa-eye-slash"></i>'));
    var move = $('<div class="cht-btn cht-button-move">').append($('<i class="fas fa-arrows-alt"></i>'));
    var edit = $('<div type="button" id="edit-content-block" data-ve_type="'+ve_widgets[type]+'" class="cht-btn cht-button-edit'+(editable?"":"hidden")+'">').append($('<i class="fa fa-edit"></i>'));
    
    var trash = $('<div type="button" id="delete-content-block" class="cht-btn cht-button-delete">').append($('<i class="fas fa-trash-alt"></i>'));
    
    toolbarHolder.append(save).append(view).append(move).append(edit).append(trash);
    contentHolder.append(toolbarHolder);
    toolbar.append(contentHolder);
    if(topSubBar != null) {
    	toolbar.append(topSubBar);
    }
    
    $(ele).append(toolbar);

    if(editable) {
    	var properties = $(ele).data('properties');
    	edit[ve_widgets[type]](properties?properties : DEFAULTS[ve_widgets[type]]);
    }
    
    
//	var closingDiv = "</div></span>" + (topSubBar == null ? "":topSubBar) + "</div>";

	var name = "<div class=\"content-holder-name\"></div>";

//	var edit = type=='block-video' || type=='content-library-videos';
	
//	if (type === "block-text" || type === "block-image" || type === "block-video" || type === "block-pdf") {
//		toolbar = openingDiv + saveButton + hideButton + moveButton + (edit?editButton:'') +deleteButton + closingDiv;
//	} else {
//		toolbar = openingDiv + hideButton + moveButton + deleteButton + closingDiv;
//	}
	return toolbar;
}

function loadPricingToolbar(ele) {
	var toolbar = "";
	var openingDiv = "<div class=\"content-holder-sub-toolbar\">" +
		"	<div class=\"toolbar-button-holder\">";

	// var saveButton = "<div type=\"button\" data-toggle=\"modal\"
	// data-target=\"#saveContentModal\" class=\"cht-btn cht-button-save\">\n" +
	// " <i class=\"fas fa-save\"></i>\n" +
	// " </div>";
	// var hideButton = "<div type=\"button\" class=\"cht-btn
	// cht-button-hide\">\n" +
	// " <i class=\"fas fa-eye-slash\"></i>\n" +
	// " </div>";
	// var moveButton = "<div class=\"cht-btn cht-button-move\">\n" +
	// " <i class=\"fas fa-arrows-alt\"></i>\n" +
	// " </div>";
	var addButton = "<div type=\"button\" id=\"pricing-add-column\" class=\"cht-btn cht-button-pricing-add-column\" style='padding: 0 0px 0px 0px;'>\n" +
		"        	<i class=\"fas fa-plus\"></i>\n" +
		"    	</div>";
	var closingDiv = "</div></div>";

	var name = "<div class=\"content-holder-name\"></div>";

	toolbar = openingDiv + addButton + closingDiv;
	return toolbar;
}


function createBlockWithMenu(ele, blockData, topMenuData, sideMenuData, library) {

	$(ele).addClass('content-holder').addClass('sortable').removeClass('content-new-holder');
	$(ele).append(blockData);

	$(ele).find( ".content-new-loader" ).remove();
	 if($(ele).attr('data-type')!='block-columns') {
		var toolbar = loadMainToolbar(ele, (topMenuData == null ? "":topMenuData));
	 }
	// if (sideManuData != null && sideManuData != "") {
	// $(ele).append(sideManuData);
	// }

	if (library != null) {
		if (library == "images") {

			var link = $(ele).data('url');
			var name = $(ele).data('name');
			var container = $(ele).find('div.item');
			loadImageContainer(link, name, container);
		}
	}
	if ($(ele).attr('data-type').indexOf('text') >= 0) {
		initFroalaEditor();
	}
	initDroppables();
	setupSwappables()
}

function createZeroRiskBlockWithMenu(ele, blockData, topMenuData, sideMenuData, library) {
	if (isTemplate) {
		$(ele).addClass('content-holder').addClass('sortable').removeClass('content-new-holder');

		if ($(ele).find( ".content-new-loader" ).length > 0) {
			$(ele).find( ".content-new-loader" ).remove();
		}

		blockData = "<div class=\"item\" style=\"vertical-align: top;display: inline-block;text-align: center;width: 100%;padding: 15px;border: 1px dotted;\"><i class=\"fas fa-compass fa-7x\"></i><span class=\"caption\" style=\"display: block; padding-top: 10px;\">This is where the ZeroRisk content will be loaded</span></div>";
		$(ele).append(blockData);
		var toolbar = loadMainToolbar(ele, (topMenuData == null ? "":topMenuData));

		initDroppables();
		setupSwappables();
		return;
	}
	$.ajax({
		url : '/api/free/travel-watch/' + travelDocID,
		type : 'GET',
		beforeSend: function( xhr ) {
			console.log('BEFORE');
		},
		success: function (data) {
			// your callback here

			$(ele).addClass('content-holder').addClass('sortable').removeClass('content-new-holder');

			if ($(ele).find( ".content-new-loader" ).length > 0) {
				$(ele).find( ".content-new-loader" ).remove();
			}

			blockData = setupZeroRiskViewForFeed(blockData, data);
			$(ele).append(blockData);
			var toolbar = loadMainToolbar(ele, (topMenuData == null ? "":topMenuData));

			initDroppables();
			setupSwappables();
		},
		error: function (error) {
			// handle error
			alert("ERROR");
		}
	});
}

function restoreZeroRiskBlockWithMenu(ele, blockData, topMenuData, sideMenuData, library) {
	if (isTemplate) {
		$(ele).addClass('content-holder').addClass('sortable').removeClass('content-new-holder');

		if ($(ele).find( ".content-new-loader" ).length > 0) {
			$(ele).find( ".content-new-loader" ).remove();
		}

		blockData = "<div class=\"item\" style=\"vertical-align: top;display: inline-block;text-align: center;width: 100%;padding: 15px;border: 1px dotted;\"><i class=\"fas fa-compass fa-7x\"></i><span class=\"caption\" style=\"display: block; padding-top: 10px;\">This is where the ZeroRisk content will be loaded</span></div>";
		$(ele).append(blockData);
		var toolbar = loadMainToolbar(ele, (topMenuData == null ? "":topMenuData));

		initDroppables();
		setupSwappables();
		return;
	}
	$.ajax({
		url : '/api/free/travel-watch/' + travelDocID,
		type : 'GET',
		async : false,
		beforeSend: function( xhr ) {
			console.log('BEFORE');
		},
		success: function (data) {
			// your callback here


			$(ele).addClass('content-holder').addClass('sortable').removeClass('content-new-holder');

			if ($(ele).find( ".content-new-loader" ).length > 0) {
				$(ele).find( ".content-new-loader" ).remove();
			}

			blockData = setupZeroRiskViewForFeed(blockData, data);
			$(ele).append(blockData);
			var toolbar = loadMainToolbar(ele, (topMenuData == null ? "":topMenuData));

			initDroppables();
			setupSwappables();

		},
		error: function (error) {
			// handle error
			console.log('ERROR');
			alert("ERROR");
		}
	});
}

function setupZeroRiskViewForFeed(blockData, data) {

	for (var i=0;i<data.length;i++) {
		var info = data[i];
		var countryInfo = info.country;
		if (countryInfo == null) {
			continue;
		}

		var countryBlock = $('<div class="country-container" data-country="'+countryInfo.attr_categoryname+'"></div>');
		countryBlock = $(countryBlock).append('<div class="title-container"><div class="main-title" style="text-transform: uppercase;text-align: center;"><h1>'+countryInfo.attr_categoryname+'</h1></div>' +
			'<div class="row">' +
			'<div class="col" style="padding-right: 0px;">' +
			'<hr style="margin: 12px 0px;">' +
			'</div>' +
			'<div class="col-auto"><div class="sub-title" style="text-align: center;"><h6>ZeroRisk Advice</h6></div></div> ' +
			'<div class="col" style="padding-right: 0px;">' +
			'<hr style="margin: 12px 0px;">' +
			'</div>' +
			'</div>' +
			'</div>');

		if (countryInfo.attr_assessmentdescription !== "") {
			countryBlock = $(countryBlock).append('<div class="country-description-container" style="border: 1px solid;border-radius: 4px;margin: 20px 10px;">' +
				'<div class="country-description" style="padding: 10px 20px;">' +
				'<div class="row">' +
				'<div class="col-auto" style="text-align: center;">' +
				'<div class="status-icon"><i class="fas fa-3x fa-exclamation-triangle"></i><span class="status-icon-text" style="display: block; line-height: 1; margin: 0; padding-top: 3px; text-transform: capitalize; letter-spacing: 0;">'+countryInfo.attrdrop_assessmentalertlevel+'</span> </div>' +
				'</div>' +
				'<div class="col"><span style="font-size: 12px;">'+countryInfo.attr_assessmentdescription+'</span></div>' +
				'</div>' +
				'</div>' +
				'</div>');
		} else {
			countryBlock = $(countryBlock).append('<div class="country-description-container" style="border: 1px solid;border-radius: 4px;margin: 20px 10px;">' +
				'<div class="country-description" style="padding: 10px 20px;">' +
				'<div class="row">' +
				'<div class="col-auto" style="text-align: center;">' +
				'<div class="status-icon"><i class="fas fa-3x fa-exclamation-triangle" style="color: green;"></i><span class="status-icon-text" style="display: block; line-height: 1; margin: 0; padding-top: 3px; text-transform: capitalize; letter-spacing: 0;">Safe Zone</span> </div>' +
				'</div>' +
				'<div class="col"><span style="font-size: 12px;">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</span></div>' +
				'</div>' +
				'</div>' +
				'</div>');
		}
		if (info.alerts != null && info.alerts.length > 0) {
			var alertsBlock = $('<div class="alerts-container" style="margin: 20px 10px;"><div class="row"><div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div><div class="col-auto"><div class="alert-section-title"><h6>Recent '+countryInfo.attr_categoryname.toLowerCase().capitalize()+' Alerts</h6></div></div><div class="col"><hr style="margin: 12px 0px;"></div> </div></div>');

			for (var a=0;a < info.alerts.length; a++) {
				var alert = info.alerts[a];
				var alertString = alert.attrtext_content;
				if (alertString.length > 400) {
					alertString = alertString.substring(0,400) +'...';
				}
				var date = new Date(alert.attrdate_start_date);
				var color = "green";
				if (alert.attr_rating === "Low") {
					var color = "green";
				} else if (alert.attr_rating === "Medium") {
					var color = "orange";
				} else {
					var color = "red";
				}
				alertsBlock = $(alertsBlock).append('<div class="alert-info-container" style="border: 1px solid;border-radius: 4px;margin: 20px 0px;">' +
					'<div class="country-description" style="padding: 10px 20px;">' +
					'<div class="row">' +
					'<div class="col">' +
					'<div class="row">' +
					'<div class="col"><div class="alert-text" style="max-height: 80px"><span style="font-size: 12px;">'+alertString+'...</span></div></div></div>' +
					'<div class="row">' +
					'<div class="col"><span style="font-size: 12px;">'+alert.attr_address+' -- '+date.getDay() + '/'+date.getMonth()+'/'+date.getFullYear()+'</span></div><div class="col-auto"><a href="#" style="font-size: 12px;">Read More</a></div> </div>' +
					' </div>' +
					'<div class="col-auto" style="text-align: center;">' +
					'<div class="status-icon"><i class="fas fa-3x fa-exclamation-triangle" style="color: '+color+';"></i><span class="status-icon-text" style="display: block; line-height: 1; margin: 0; padding-top: 3px; text-transform: capitalize; letter-spacing: 0;">'+alert.attr_rating+'</span> </div>' +
					'</div>' +
					'</div>' +
					'</div>' +
					'</div>');
			}
			countryBlock = $(countryBlock).append(alertsBlock);
		} else {
			var alertsBlock = $('<div class="alerts-container" style="margin: 20px 10px;"><div class="row"><div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div><div class="col-auto"><div class="alert-section-title"><h6>Recent '+countryInfo.attr_categoryname.toLowerCase().capitalize()+' Alerts</h6></div></div><div class="col"><hr style="margin: 12px 0px;"></div> </div></div>');

			alertsBlock = $(alertsBlock).append('<div class="alert-info-container" style="border: 1px solid;border-radius: 4px;margin: 20px 0px;">' +
				'<div class="country-description" style="padding: 10px 20px;">' +
				'<div class="row">' +
				'<div class="col text-center">' +
				'<span>No ZeroRisk alerts reported in this area at present.</span>' +
				'</div>' +
				'</div>' +
				'</div>');
			countryBlock = $(countryBlock).append(alertsBlock);
		}

		if (info.travel != null && info.travel.length > 0) {
			var travelBlockInside = '';
			var travelRating = '';
			for (var t=0;t < info.travel.length; t++) {
				var travel = info.travel[t];
				travelRating = travel.attrdrop_assessmentalertlevel;
				var travelElementString = '';

				if ((travel.attr_video != null && travel.attr_video != "")) {
					var url = '';
					if (travel.attr_video.includes('youtub')) {
						var code = travel.attr_video.replace('https://www.youtube.com/watch?v=','').replace('https://youtu.be/', '');
						url = '<iframe width="420" height="315"\n' +
							'src="https://www.youtube.com/embed/'+code+'"></iframe>';
					} else if (travel.attr_video.includes('vimeo')) {

					}
					if (t % 2 == 0) {

						travelElementString = '<div class="travel-info-container" style="margin: 20px 0px;">' +
							'<div class="row">' +
							'<div class="col-auto">'+url+'</div>' +
							'<div class="col"> ' +
							'<div class="travel-info-text">'+travel.attrlong_description+'</div> ' +
							'</div></div>' +
							'</div>';
					} else {
						travelElementString = '<div class="travel-info-container" style="margin: 20px 0px;">' +
							'<div class="row"><div class="col"> ' +
							'<div class="travel-info-text">'+travel.attrlong_description+'</div> ' +
							'</div>' +
							'<div class="col-auto">'+url+'</div>' +
							'</div>' +
							'</div>';
					}
				} else if (travel.attrfile_image != null && travel.attrfile_image != "") {
					if (t % 2 == 0) {
						travelElementString = '<div class="travel-info-container" style="margin: 20px 0px;">' +
							'<div class="row">' +
							'<div class="col-auto"><img src="'+travel.attrfile_image+'" alt="'+travel.attr_headline+'"></div>' +
							'<div class="col"> ' +
							'<div class="travel-info-text">'+travel.attrlong_description+'</div> ' +
							'</div></div>' +
							'</div>';
					} else {
						travelElementString = '<div class="travel-info-container" style="margin: 20px 0px;">' +
							'<div class="row"><div class="col"> ' +
							'<div class="travel-info-text">'+travel.attrlong_description+'</div> ' +
							'</div>' +
							'<div class="col-auto"><img src="'+travel.attrfile_image+'" alt="'+travel.attr_headline+'"></div>' +
							'</div>' +
							'</div>';
					}
				} else {
					travelElementString = '<div class="travel-info-container" style="margin: 20px 0px;">' +
						'<div class="row"><div class="col"> ' +
						'<div class="travel-info-text">'+travel.attrlong_description+'</div> ' +
						'</div></div>' +
						'</div>';
				}
				travelBlockInside = travelElementString;
			}
			var travelBlock = $('<div class="travel-container" style="margin: 20px 10px;"><div class="row"><div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div><div class="col-auto"><div class="travel-section-title"><h6>Local Travel Information</h6></div></div><div class="col" style="padding-right: 0px;"><hr style="margin: 12px 0px;"></div><div class="col-auto travel-rating">'+travelRating+'</div></div></div>');
			travelBlock = $(travelBlock).append(travelBlockInside);
			countryBlock = $(countryBlock).append(travelBlock);
		}

		if (info.terrorism != null && info.terrorism.length > 0) {
			var assetBlockInside = '';
			var assetRating = '';
			for (var t=0;t < info.terrorism.length; t++) {
				var asset = info.terrorism[t];
				assetRating = asset.attrdrop_assessmentalertlevel;
				var assetElementString = '';

				if ((asset.attr_video != null && asset.attr_video != "")) {
					var url = '';
					if (asset.attr_video.includes('youtub')) {
						var code = asset.attr_video.replace('https://www.youtube.com/watch?v=','').replace('https://youtu.be/', '');
						url = '<iframe width="420" height="315"\n' +
							'src="https://www.youtube.com/embed/'+code+'"></iframe>';
					} else if (asset.attr_video.includes('vimeo')) {

					}
					if (t % 2 == 0) {

						assetElementString = '<div class="terrorism-info-container" style="margin: 20px 0px;">' +
							'<div class="row">' +
							'<div class="col-auto">'+url+'</div>' +
							'<div class="col"> ' +
							'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
							'</div></div>' +
							'</div>';
					} else {
						assetElementString = '<div class="terrorism-info-container" style="margin: 20px 0px;">' +
							'<div class="row"><div class="col"> ' +
							'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
							'</div>' +
							'<div class="col-auto">'+url+'</div>' +
							'</div>' +
							'</div>';
					}
				} else if (asset.attrfile_image != null && asset.attrfile_image != "") {
					if (t % 2 == 0) {
						assetElementString = '<div class="terrorism-info-container" style="margin: 20px 0px;">' +
							'<div class="row">' +
							'<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
							'<div class="col"> ' +
							'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
							'</div></div>' +
							'</div>';
					} else {
						assetElementString = '<div class="terrorism-info-container" style="margin: 20px 0px;">' +
							'<div class="row"><div class="col"> ' +
							'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
							'</div>' +
							'<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
							'</div>' +
							'</div>';
					}
				} else {
					assetElementString = '<div class="terrorism-info-container" style="margin: 20px 0px;">' +
						'<div class="row"><div class="col"> ' +
						'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
						'</div></div>' +
						'</div>';
				}
				assetBlockInside = assetElementString;
			}
			var assetBlock = $('<div class="asset-container" style="margin: 20px 10px;">' +
				'<div class="row">' +
				'<div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div>' +
				'<div class="col-auto">' +
				'<div class="asset-section-title">' +
				'<h6>Terrorism Threat</h6>' +
				'</div>' +
				'</div>' +
				'<div class="col"><hr style="margin: 12px 0px;"></div>' +
				'<div class="col-auto travel-rating">'+assetRating+'</div>' +
				'</div>' +
				'</div>');
			assetBlock = $(assetBlock).append(assetBlockInside);
			countryBlock = $(countryBlock).append(assetBlock);
		}

		if (info.crime != null && info.crime.length > 0) {
			var assetBlockInside = '';
			var assetRating = '';
			for (var t=0;t < info.crime.length; t++) {
				var asset = info.crime[t];
				assetRating = asset.attrdrop_assessmentalertlevel;
				var assetElementString = '';

				if ((asset.attr_video != null && asset.attr_video != "")) {
					var url = '';
					if (asset.attr_video.includes('youtub')) {
						var code = asset.attr_video.replace('https://www.youtube.com/watch?v=','').replace('https://youtu.be/', '');
						url = '<iframe width="420" height="315"\n' +
							'src="https://www.youtube.com/embed/'+code+'"></iframe>';
					} else if (asset.attr_video.includes('vimeo')) {

					}
					if (t % 2 == 0) {

						assetElementString = '<div class="crime-info-container" style="margin: 20px 0px;">' +
							'<div class="row">' +
							'<div class="col-auto">'+url+'</div>' +
							'<div class="col"> ' +
							'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
							'</div></div>' +
							'</div>';
					} else {
						assetElementString = '<div class="crime-info-container" style="margin: 20px 0px;">' +
							'<div class="row"><div class="col"> ' +
							'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
							'</div>' +
							'<div class="col-auto">'+url+'</div>' +
							'</div>' +
							'</div>';
					}
				} else if (asset.attrfile_image != null && asset.attrfile_image != "") {
					if (t % 2 == 0) {
						assetElementString = '<div class="crime-info-container" style="margin: 20px 0px;">' +
							'<div class="row">' +
							'<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
							'<div class="col"> ' +
							'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
							'</div></div>' +
							'</div>';
					} else {
						assetElementString = '<div class="crime-info-container" style="margin: 20px 0px;">' +
							'<div class="row"><div class="col"> ' +
							'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
							'</div>' +
							'<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
							'</div>' +
							'</div>';
					}
				} else {
					assetElementString = '<div class="crime-info-container" style="margin: 20px 0px;">' +
						'<div class="row"><div class="col"> ' +
						'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
						'</div></div>' +
						'</div>';
				}
				assetBlockInside = assetElementString;
			}
			var assetBlock = $('<div class="asset-container" style="margin: 20px 10px;">' +
				'<div class="row">' +
				'<div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div>' +
				'<div class="col-auto">' +
				'<div class="asset-section-title">' +
				'<h6>Local Crime Information</h6>' +
				'</div>' +
				'</div>' +
				'<div class="col"><hr style="margin: 12px 0px;"></div>' +
				'<div class="col-auto travel-rating">'+assetRating+'</div>' +
				'</div>' +
				'</div>');
			assetBlock = $(assetBlock).append(assetBlockInside);
			countryBlock = $(countryBlock).append(assetBlock);
		}

		if (info.date != null && info.date.length > 0) {
			var assetBlockInside = '';
			var demoBlockInside = '';
			var assetRating = '';
			for (var t=0;t < info.date.length; t++) {
				var asset = info.date[t];
				assetRating = asset.attrdrop_assessmentalertlevel;
				var assetElementString = '';

				if ((asset.attr_video != null && asset.attr_video != "")) {
					var url = '';
					if (asset.attr_video.includes('youtub')) {
						var code = asset.attr_video.replace('https://www.youtube.com/watch?v=','').replace('https://youtu.be/', '');
						url = '<iframe width="420" height="315"\n' +
							'src="https://www.youtube.com/embed/'+code+'"></iframe>';
					} else if (asset.attr_video.includes('vimeo')) {

					}
					if (t % 2 == 0) {

						assetElementString = '<div class="date-info-container" style="margin: 20px 0px;">' +
							'<div class="row">' +
							'<div class="col-auto">'+url+'</div>' +
							'<div class="col"> ' +
							'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
							'</div></div>' +
							'</div>';
					} else {
						assetElementString = '<div class="date-info-container" style="margin: 20px 0px;">' +
							'<div class="row"><div class="col"> ' +
							'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
							'</div>' +
							'<div class="col-auto">'+url+'</div>' +
							'</div>' +
							'</div>';
					}
				} else if (asset.attrfile_image != null && asset.attrfile_image != "") {
					if (t % 2 == 0) {
						assetElementString = '<div class="date-info-container" style="margin: 20px 0px;">' +
							'<div class="row">' +
							'<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
							'<div class="col"> ' +
							'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
							'</div></div>' +
							'</div>';
					} else {
						assetElementString = '<div class="date-info-container" style="margin: 20px 0px;">' +
							'<div class="row"><div class="col"> ' +
							'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
							'</div>' +
							'<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
							'</div>' +
							'</div>';
					}
				} else {
					assetElementString = '<div class="date-info-container" style="margin: 20px 0px;">' +
						'<div class="row"><div class="col"> ' +
						'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
						'</div></div>' +
						'</div>';
				}
				assetBlockInside = assetElementString;
			}
			if (info.demonstrations != null && info.demonstrations.length > 0) {
				for (var t=0;t < info.demonstrations.length; t++) {
					var asset = info.demonstrations[t];
					var demoElementString = '';

					if ((asset.attr_video != null && asset.attr_video != "")) {
						var url = '';
						if (asset.attr_video.includes('youtub')) {
							var code = asset.attr_video.replace('https://www.youtube.com/watch?v=','').replace('https://youtu.be/', '');
							url = '<iframe width="420" height="315"\n' +
								'src="https://www.youtube.com/embed/'+code+'"></iframe>';
						} else if (asset.attr_video.includes('vimeo')) {

						}
						if (t % 2 == 0) {

							demoElementString = '<div class="demonstrations-info-container" style="margin: 20px 0px;">' +
								'<div class="row">' +
								'<div class="col-auto">'+url+'</div>' +
								'<div class="col"> ' +
								'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
								'</div></div>' +
								'</div>';
						} else {
							demoElementString = '<div class="demonstrations-info-container" style="margin: 20px 0px;">' +
								'<div class="row"><div class="col"> ' +
								'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
								'</div>' +
								'<div class="col-auto">'+url+'</div>' +
								'</div>' +
								'</div>';
						}
					} else if (asset.attrfile_image != null && asset.attrfile_image != "") {
						if (t % 2 == 0) {
							demoElementString = '<div class="demonstrations-info-container" style="margin: 20px 0px;">' +
								'<div class="row">' +
								'<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
								'<div class="col"> ' +
								'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
								'</div></div>' +
								'</div>';
						} else {
							demoElementString = '<div class="demonstrations-info-container" style="margin: 20px 0px;">' +
								'<div class="row"><div class="col"> ' +
								'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
								'</div>' +
								'<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
								'</div>' +
								'</div>';
						}
					} else {
						demoElementString = '<div class="demonstrations-info-container" style="margin: 20px 0px;">' +
							'<div class="row"><div class="col"> ' +
							'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
							'</div></div>' +
							'</div>';
					}
					demoBlockInside = demoElementString;
				}
			}
			var assetBlock = $('<div class="asset-container" style="margin: 20px 10px;">' +
				'<div class="row">' +
				'<div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div>' +
				'<div class="col-auto">' +
				'<div class="asset-section-title">' +
				'<h6>Anniversaries &amp; Demonstrations</h6>' +
				'</div>' +
				'</div>' +
				'<div class="col"><hr style="margin: 12px 0px;"></div>' +
				'<div class="col-auto travel-rating">'+assetRating+'</div>' +
				'</div>' +
				'</div>');
			assetBlock = $(assetBlock).append(assetBlockInside);
			assetBlock = $(assetBlock).append(demoBlockInside);
			countryBlock = $(countryBlock).append(assetBlock);
		}

		if (info.weather != null && info.weather.length > 0) {
			if (asset.attrlong_description != null && asset.attrlong_description != "") {
				var assetBlockInside = '';
				var assetRating = '';
				for (var t=0;t < info.weather.length; t++) {
					var asset = info.weather[t];
					assetRating = asset.attrdrop_assessmentalertlevel;
					var assetElementString = '';

					if ((asset.attr_video != null && asset.attr_video != "")) {
						var url = '';
						if (asset.attr_video.includes('youtub')) {
							var code = asset.attr_video.replace('https://www.youtube.com/watch?v=','').replace('https://youtu.be/', '');
							url = '<iframe width="420" height="315"\n' +
								'src="https://www.youtube.com/embed/'+code+'"></iframe>';
						} else if (asset.attr_video.includes('vimeo')) {

						}
						if (t % 2 == 0) {

							assetElementString = '<div class="weather-info-container" style="margin: 20px 0px;">' +
								'<div class="row">' +
								'<div class="col-auto">'+url+'</div>' +
								'<div class="col"> ' +
								'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
								'</div></div>' +
								'</div>';
						} else {
							assetElementString = '<div class="weather-info-container" style="margin: 20px 0px;">' +
								'<div class="row"><div class="col"> ' +
								'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
								'</div>' +
								'<div class="col-auto">'+url+'</div>' +
								'</div>' +
								'</div>';
						}
					} else if (asset.attrfile_image != null && asset.attrfile_image != "") {
						if (t % 2 == 0) {
							assetElementString = '<div class="weather-info-container" style="margin: 20px 0px;">' +
								'<div class="row">' +
								'<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
								'<div class="col"> ' +
								'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
								'</div></div>' +
								'</div>';
						} else {
							assetElementString = '<div class="weather-info-container" style="margin: 20px 0px;">' +
								'<div class="row"><div class="col"> ' +
								'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
								'</div>' +
								'<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
								'</div>' +
								'</div>';
						}
					} else {
						assetElementString = '<div class="weather-info-container" style="margin: 20px 0px;">' +
							'<div class="row"><div class="col"> ' +
							'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
							'</div></div>' +
							'</div>';
					}
					assetBlockInside = assetElementString;
				}
				var assetBlock = $('<div class="asset-container" style="margin: 20px 10px;">' +
					'<div class="row">' +
					'<div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div>' +
					'<div class="col-auto">' +
					'<div class="asset-section-title">' +
					'<h6>Weather &amp; Disaster Information</h6>' +
					'</div>' +
					'</div>' +
					'<div class="col"><hr style="margin: 12px 0px;"></div>' +
					'<div class="col-auto travel-rating">'+assetRating+'</div>' +
					'</div>' +
					'</div>');
				assetBlock = $(assetBlock).append(assetBlockInside);
				countryBlock = $(countryBlock).append(assetBlock);
			}
		}

		if (info.health != null && info.health.length > 0) {
			if (asset.attrlong_description != null && asset.attrlong_description != "") {
				var assetBlockInside = '';
				var assetRating = '';
				for (var t=0;t < info.health.length; t++) {
					var asset = info.health[t];
					assetRating = asset.attrdrop_assessmentalertlevel;
					var assetElementString = '';

					if ((asset.attr_video != null && asset.attr_video != "")) {
						var url = '';
						if (asset.attr_video.includes('youtub')) {
							var code = asset.attr_video.replace('https://www.youtube.com/watch?v=','').replace('https://youtu.be/', '');
							url = '<iframe width="420" height="315"\n' +
								'src="https://www.youtube.com/embed/'+code+'"></iframe>';
						} else if (asset.attr_video.includes('vimeo')) {

						}
						if (t % 2 == 0) {

							assetElementString = '<div class="health-info-container" style="margin: 20px 0px;">' +
								'<div class="row">' +
								'<div class="col-auto">'+url+'</div>' +
								'<div class="col"> ' +
								'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
								'</div></div>' +
								'</div>';
						} else {
							assetElementString = '<div class="health-info-container" style="margin: 20px 0px;">' +
								'<div class="row"><div class="col"> ' +
								'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
								'</div>' +
								'<div class="col-auto">'+url+'</div>' +
								'</div>' +
								'</div>';
						}
					} else if (asset.attrfile_image != null && asset.attrfile_image != "") {
						if (t % 2 == 0) {
							assetElementString = '<div class="health-info-container" style="margin: 20px 0px;">' +
								'<div class="row">' +
								'<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
								'<div class="col"> ' +
								'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
								'</div></div>' +
								'</div>';
						} else {
							assetElementString = '<div class="health-info-container" style="margin: 20px 0px;">' +
								'<div class="row"><div class="col"> ' +
								'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
								'</div>' +
								'<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
								'</div>' +
								'</div>';
						}
					} else {
						assetElementString = '<div class="health-info-container" style="margin: 20px 0px;">' +
							'<div class="row"><div class="col"> ' +
							'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
							'</div></div>' +
							'</div>';
					}
					assetBlockInside = assetElementString;
				}
				var assetBlock = $('<div class="asset-container" style="margin: 20px 10px;">' +
					'<div class="row">' +
					'<div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div>' +
					'<div class="col-auto">' +
					'<div class="asset-section-title">' +
					'<h6>Health &amp; Vaccine Information</h6>' +
					'</div>' +
					'</div>' +
					'<div class="col"><hr style="margin: 12px 0px;"></div>' +
					'<div class="col-auto travel-rating">'+assetRating+'</div>' +
					'</div>' +
					'</div>');
				assetBlock = $(assetBlock).append(assetBlockInside);
				countryBlock = $(countryBlock).append(assetBlock);
			}
		}

		if (info.emergencyProtocol != null && info.emergencyProtocol.length > 0) {
			if (asset.attrlong_description != null && asset.attrlong_description != "") {
				var assetBlockInside = '';
				var assetRating = '';
				for (var t=0;t < info.emergencyProtocol.length; t++) {
					var asset = info.emergencyProtocol[t];
					assetRating = asset.attrdrop_assessmentalertlevel;
					var assetElementString = '';

					assetElementString = '<div class="emergencyProtocol-info-container" style="margin: 20px 0px;">' +
						'<div class="row"><div class="col"> ' +
						'<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
						'</div></div>' +
						'</div>';
					assetBlockInside = assetElementString;
				}
				var assetBlock = $('<div class="asset-container" style="margin: 20px 10px;">' +
					'<div class="row">' +
					'<div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div>' +
					'<div class="col-auto">' +
					'<div class="asset-section-title">' +
					'<h6>Critical Emergency Protocol</h6>' +
					'</div>' +
					'</div>' +
					'<div class="col"><hr style="margin: 12px 0px;"></div>' +
					'<div class="col-auto travel-rating">'+assetRating+'</div>' +
					'</div>' +
					'</div>');
				assetBlock = $(assetBlock).append(assetBlockInside);
				countryBlock = $(countryBlock).append(assetBlock);
			}
		}

		if (info.emergancyContact != null && info.emergancyContact.length > 0) {
			if (info.emergancyContact[0] != "") {
				var assetBlockInside = '';
				for (var t=0;t < info.emergancyContact.length; t++) {
					var asset = info.emergancyContact[t];
					var assetElementString = '';

					assetElementString = '<div class="emergancy-contact-info-container" style="margin: 20px 0px;">' +
						'<div class="row"><div class="col"> ' +
						'<div class="travel-info-text">'+asset+'</div> ' +
						'</div></div>' +
						'</div>';

					assetBlockInside = assetElementString;
				}
				var assetBlock = $('<div class="asset-container" style="margin: 20px 10px;"><div class="row"><div class="col"><div class="asset-section-title" style="padding-left: 40px;"><h6>Emergency Contact Information</h6></div></div><div class="col-auto">Important</div></div></div>');
				assetBlock = $(assetBlock).append(assetBlockInside);
				countryBlock = $(countryBlock).append(assetBlock);
			}
		}

		blockData = $(blockData).append(countryBlock);
	}

	return blockData;
}



function loadDummyContent(ele) {
	var toolbar = "";
	var openingDiv = "<div class=\"touch-container\"> " +
		"<span class=\"block-handle\"><a><i class=\"fas fa-arrows-alt\"></i> "+ve_elements[$(ele).attr('data-type')]+"</a></span>" +
		"<span class=\"content-holder-toolbar\">" +
		"	<div class=\"toolbar-button-holder\">";

	var saveButton = "<div type=\"button\" data-toggle=\"modal\" data-target=\"#saveContentModal\" class=\"cht-btn cht-button-save\">\n" +
		"        	<i class=\"fas fa-save\"></i>\n" +
		"    	</div>";
	var hideButton = "<div type=\"button\" class=\"cht-btn cht-button-hide\">\n" +
		"        	<i class=\"fas fa-eye-slash\"></i>\n" +
		"    	</div>";
	var moveButton = "<div class=\"cht-btn cht-button-move\">\n" +
		"        	<i class=\"fas fa-arrows-alt\"></i>\n" +
		"    	</div>";
	var deleteButton = "<div type=\"button\" id=\"delete-content-block\" class=\"cht-btn cht-button-delete\">\n" +
		"        	<i class=\"fas fa-trash-alt\"></i>\n" +
		"    	</div>";
	var closingDiv = "</div></span></div>";

	var name = "<div class=\"content-holder-name\"></div>";

	if ($(ele).attr('data-type') === "block-text" || $(ele).attr('data-type') === "block-image" || $(ele).attr('data-type') === "block-video" || $(ele).attr('data-type') === "block-pdf") {
		toolbar = openingDiv + saveButton + hideButton + moveButton + deleteButton + closingDiv;
	} else {
		toolbar = openingDiv + hideButton + moveButton + deleteButton + closingDiv;
	}
	$(ele).addClass('content-holder').addClass('sortable').removeClass('content-new-holder');

	if ($(ele).attr('data-type') === "block-text") {
		$(ele).append('<textarea style="width: 100%; min-height: 50px; height: 50px;"></textarea>')
	} else {
		$(ele).height('200px');
	}
	$(ele).find( ".content-new-loader" ).remove();
	$(ele).append(toolbar);
	// $(ele).droppable({
	// over: function(event, ui) {
     // var dropHolder = "<div class=\"droppable-holder dropHolderTop
		// sortable ui-droppable\" style=\"\n" +
     // "width: 100%; height: 40px;border: 1px solid
		// lightblue;background-color: lightblue;margin-bottom: 20px;display:
		// none;\"><p style=\"\n" +
     // "text-align: center;padding-top: 8;\">Drop your block
		// here</p></div>";
     // $(dropHolder).insertBefore($(this));
	// },
    //
	// out: function(event, ui) {
	// $('.dropHolderTop').remove();
	// },
	// drop: function( event, ui ) {
	// var clone = $(ui.draggable).clone(true).detach();
	// $('#droppable-label').remove();
	// var contentItem = "<div class=\"content-new-holder\" data-type=\""+
	// $(clone).attr('id')+"\" data-id=\""+ $(clone).data('id') +"\"><div
	// class=\"content-new-loader\"></div></div>";
	// $(contentItem).insertBefore($( this ));
	// $('.content-new-holder').animate({height:'50px', width:'100%'},
	// 400, function() {
	// $('.content-new-holder').height('auto');
	// setTimeout(function() {
	// loadContent($('.content-new-holder'));
	// }, 500);
	// });
    //
	// }
	// });

	if ($(ele).attr('data-type') === "block-text") {
		initFroalaEditor();
	}
}

function loadBlockTiles(){

	$.ajax({
		type : "GET",
		url : '/api/blocks/getAllBlocks'
	}).done(function(data, textStatus, jqXHR)
	{
		$('.black-square-container').empty();
		for (var i = 0; i < data.length; i++) {
			var info = data[i];
			var tile = "<div class=\"block-icon draggable\" data-id=\""+ info.element_id +"\" id=\""+info.attr_blockid+"\">\n" +
				"           <span class=\"block-bg\">\n" +
				"              <span class=\"block-icon-img\">\n" +
				"                 "+info.attr_icon+
				"              </span>\n" +
				"           <span class=\"block-icon-label\">"+info.attr_headline+"</span>\n" +
				"           </span>\n" +
				"      </div>";
			$('.black-square-container').append(tile);
		}
		setupDroppers();
		setupDoubleClick('.black-square-container .draggable');
		// addRowToD/isplay($pricingTable, data, true);
	});
}

function loadLibrary(type, folder, subFolder) {

	$.ajax({
		url: "/api/library/contentmanagement",
		data: {"libraryType":type, "folder":folder, "subFolder":subFolder},
		success: function(data) {
			$('.black-square-container').empty();
			var lib = data.libraryItems;
			var fold = data.libraryFolders;

			var folders = [];
			var templatesFiles = [];
			var fileshtml = '';
			
			var folderContainer = $('<div class="library-folder-container">');
			var fileContainer = $('<div class="library-item-container">');

			for (var i = 0; i < fold.length; i++) {
				var template = fold[i];
				folders.push(template.attr_categoryname);
				var folderhtml = '<div class="block-button-container '+type+'-folder content-folder" data-type="'+(folder?"sub-folder":"folder")+'" data-id="'+template.category_id+'" style="margin-bottom: 5px;">' +
					'<div class="button-content">' +
					'<i class="far fa-folder fa-sm"></i>' +
					'<span>'+ template.attr_categoryname +'</span>' +
					'<i class="fas fa-angle-right fa-sm" style="position:absolute;right:14px;top:11px"></i>' +
					'</div>' +
					'</div>';
				folderContainer.append($(folderhtml));
			}

			for (var i = 0; i < lib.length; i++) {
				var template = lib[i];
				if (template.attr_categoryname == null) {
                    if (template.ATTR_subCategory != null && template.ATTR_subCategory != "") {
                        folders.push(template.attr_categoryname);
                        var folderhtml = '<div class="block-button-container" data-type="folder" data-id="'+template.category_id+'" style="margin-bottom: 5px;">' +
                            '<div class="button-content">' +
                            '<i class="far fa-folder fa-sm"></i>' +
                            '<span>'+ template.attr_categoryname +'</span>' +
                            '</div>' +
                            '</div>';
                        folderContainer.append($(folderhtml));
                    } else {
                    	var fileshtml;
                    	 if (type.indexOf("image") >= 0 || type.indexOf("videos") >= 0) {
                    		 fileshtml = '<div class="content-files-column library-draggable" library-type="'+type+'" id="content-library-'+type+'" data-name="'+ template.attr_headline +'"><img style="max-width:100%;max-height:100%;" src="'+(type.indexOf("image") >= 0 ? '/stores'+template.attrfile_image : template.thumbnail)+'"></div>' ;
	                     }
                         else {
                             templatesFiles.push(template);
                             fileshtml = '<div class="block-button-container library-draggable" library-type="'+type+'" id="content-library-'+type+'" data-name="' + template.attr_headline + '" data-type="file" data-id="'+template.category_id+'" data-eleid="' + template.element_id + '" style="margin-bottom: 5px;">' +
                                 '<div class="button-content">' +
                                 '<i class="far fa-file-alt fa-sm"></i>' +
                                 '<span>'+ template.attr_headline +'</span>' +
                                 '</div>' +
                                 '</div>';
                         }
                    	 
                    	 var data = template.library_data;
                    	 if(type.indexOf('videos') >= 0) {
                    		 if (data != null && data.length > 9) {
                	        	if (data.toLowerCase().includes("youtube")) {
                	        		var split = data.split('v=');
                	        		if (split.length > 1 && split[1].length < 12) {
                	                    data = "https://www.youtube.com/embed/"+split[1];
                					}
                				} else if (data.toLowerCase().includes("vimeo")) {
                	                var split = data.split('.com/');
                	                if (split.length > 1 && split[1].length < 12) {
                	                    data = "https://player.vimeo.com/video/"+split[1];
                	                }
                				}

                			}
                    	 }
                    	 fileContainer.append($(fileshtml).data('data', data));
                    }

                }

			}



			$('.black-square-container').empty();
			$('.black-square-container').append(folderContainer).append(fileContainer);
			if (type == "images" || type == "videos") {
				var libraryItemsContainer = $('.black-square-container .library-item-container');
				libraryItemsContainer.imagesLoaded(function(){
					libraryItemsContainer.masonry({
						itemSelector: '.content-files-column',
						columnWidth: 120,
						gutter: 15
					});
				});
			}
			setupLibraryDroppers();
			setupDoubleClick('.library-draggable');
		},
		error: function() {
			$('#notification-bar').text('An error occurred');
		}
	});
}

function setupDroppers() {
	$('.draggable').draggable({
		handle: 'span, svg',
		appendTo: '.editor-content',
		revert: "invalid",
		helper: 'clone',
		start: function(event, ui) {
			$('.vecontrol-in').addClass('drag-in-progress');
			$(ui.helper).addClass("ui-draggable-helper").css("pointer-events","none");
			// small-drop
            var dropHolder = "<div class=\"droppable-holder insertedDrop sortable ui-droppable\" ></div>";

			var id = $(this).attr('id');
			var isSmall = ((id.indexOf('newpage') === -1) && (id.indexOf('columns') === -1)) ? false : true;
			$('.section-button:not(#new-section-button)').each(function() {

//				if ($(this).hasClass('active')) {
					var section = $(this).data('section');
					if (section.indexOf('-coverpage') === -1) {
						var holder = $('#'+section).find('.droppable-holder');
						var contentHolder = $('#'+section).find('.content-holder');
						$(holder).each(function() {
							var hoderID = $(this).attr('id');
							if (isSmall) {
								if (!($(this).hasClass('small-drop'))) {
									if (!($(contentHolder).hasClass('linked'))) {
                                        $(dropHolder).insertBefore($(contentHolder));
                                        $(contentHolder).addClass('linked');
									}
                                    $(this).show();
								}
							} else {
                                if (!($(contentHolder).hasClass('linked'))) {
                                    $(dropHolder).insertBefore($(contentHolder));
                                    $(contentHolder).addClass('linked');
                                }
                                $(this).show();
							}
						})
					}
//				}
			});
			initDroppables();

		},
		stop: function(event, ui) {
			$('.vecontrol-in').removeClass('drag-in-progress');
            $('.droppable-holder').hide();
            $('.content-holder').removeClass('linked');
            $('.insertedDrop').remove();
		}
	});
}

function setupLibraryDroppers() {
    $('.library-draggable').draggable({
        // handle: 'span, svg',
        appendTo: '.editor-content',
        revert: "invalid",
        helper: function(e){
        	var $libraryItem = $(this);
        	var libraryType = $libraryItem.attr('library-type');
        	if(libraryType=='images' || libraryType=='videos') {
        		var image = $libraryItem.find('img').attr('src');
            	return $("<img class=\"images-drag-helper\" src=\""+image+"\" alt=\"hshs\" style=\"max-width:128px;max-height:150px;pointer-events:none;\" data-name=\""+$(this).data('name')+"\" data-url=\""+image+"\" id=\"content-library-images\"></img>");
        	}
        	return $('<div class="'+libraryType+'"-drag-helper"><span>'+$libraryItem.find('.button-content span').text()+'</span></div>');
        },
        start: function(event, ui) {
        	$('.vecontrol-in').addClass('drag-in-progress');
            $(ui.helper).addClass("ui-draggable-helper");
            // small-drop
            var dropHolder = "<div class=\"droppable-holder insertedDrop sortable ui-droppable\" ></div>";


            $('.section-button').each(function() {

                if ($(this).hasClass('active')) {
                    var section = $(this).data('section');
                    if (section.indexOf('-coverpage') === -1) {
                        var holder = $('#'+section).find('.droppable-holder');
                        var contentHolder = $('#'+section).find('.content-holder');
                        $(holder).each(function() {
                            if (!($(contentHolder).hasClass('linked'))) {
                                $(dropHolder).insertBefore($(contentHolder));
                                $(contentHolder).addClass('linked');
                            }
                           $(this).show();
                        });
                    }
                }
            });
            initDroppables();
            

        },
        stop: function(event, ui) {
        	$('.vecontrol-in').removeClass('drag-in-progress');
            $('.droppable-holder').hide();
            $('.content-holder').removeClass('linked');
            $('.insertedDrop').remove();
        }
    });
}

function setupDoubleClick(selector){
	$(selector).dblclick(function(){
		var activeSection = $('#document-section-tabs .dynaTab .section-button.active').attr('data-section');
		var dropzone = $('.doc-page-area-sheet[id='+activeSection+'] .page-content-area > .droppable-holder:last-child');
	    insertContentBlock($(this), dropzone);
	});
}


function setupDropZones() {
	$( ".droppable-holder" ).each(function() {
		$(this).droppable({
			tolerance: 'pointer',
            over: function(event, ui) {
            	console.log('setting green');
                //$(this).css("background-color", "green");
            },

            out: function(event, ui) {
            	console.log('setting blue');
                //$(this).css("background-color", "lightblue");
			},
			drop: function( event, ui ) {

				dropItem($(ui.draggable).clone(true).detach(), $(this));

			}
		});
	});
}

function setupSwappables() {

	$('.page-content-area').sortable({
		handle: '.cht-button-move',
		placeholder: "ui-state-highlight",
        receive: function (event, ui) { // add this handler
            ui.item.remove(); // remove original item
        },
        start: function(event, ui) {
            // small-drop
            var dropHolder = "<div class=\"droppable-holder insertedDrop sortable ui-droppable\"></div>";

            $('.section-button').each(function() {
                if ($(this).hasClass('active')) {
                    var section = $(this).data('section');
                    if (section.indexOf('-coverpage') === -1) {
                        var holder = $('#'+section).find('.droppable-holder');
                        var contentHolder = $('#'+section).find('.content-holder');
                        var footer = $('#'+section).find('#footer-drop-area');
                        $(holder).each(function() {
                            $(dropHolder).insertBefore($(contentHolder));
                        });

						$(footer).each(function(footHolder) {
                            $(dropHolder).insertBefore($(footer[footHolder]));
                        });
                    }
                }
            });
            initSortables();

        },
        stop: function(event, ui) {
            $('.droppable-holder').each(function() {
                $(this).hide();
                $('.content-holder').removeClass('linked');
                $('.insertedDrop').remove();
            })
        }
	});
}

function setupCoverPage(data) {
    $.get('/application/include/editorIncludes/coverpage.jsp', function(css){

        // Grab card and load content
        var contentInto = css;

        var sectionTitle = "Cover Page";
        
        if (data != null) {
        	var styleSettings;
            for (var i = 0; i < data.content.length; i++) {
                var sectionContent = data.content[i];
                if (sectionContent.type != null && sectionContent.type.toLowerCase() == "cover") {
                    var content = data.content[i].coverData.html;
                    var cssStyles = data.content[i].coverData.css;
                    if (content != null) {
                        var section = "    <div class=\"doc-page-area-sheet coverPageContent\" id=\"content-section-coverpage\" data-title=\"" + sectionTitle + "\">\n" + content + "</div>";

                        var tab = "  <li class=\" coverTab\">\n" + // <input
																					// type="checkbox"
																					// class="js-switch"
																					// id="coverpageVisible"
																					// checked
																					// />
                            "                <button style=\"width: 100%;text-align: left; display: flex;\" type=\"button\" class=\"section-button\" data-section=\"content-section-coverpage\" data-title=\"" + sectionTitle + "\">\n" +
                            "                    <div class=\"tab-label\" style='position:relative;line-height: 23px;width: 90%;'>" + sectionTitle + "</div>\n" +
                            "                    <div class=\"editButton\"></div>\n" +
                            "                </button>\n" +
                            "            </li>";
                        $('.doc-page-area').prepend(section);
                        $('#document-section-tabs').prepend(tab);
                        $('head').append('<style type="text/css" class="proposalCoverPageStyles">'+cssStyles+'</style>');
                        
                    }
                    if(data.content[i].coverURL) {
                    	$('.cover-page-chooser-button-main').data('bg', data.content[i].coverURL);
                    	$('#cover-page-chooser-button').find('#coverpage-title').text(data.content[i].coverData.metadata.name)
                    }
                    styleSettings = sectionContent.coverData.metadata;
                } else if (sectionContent.id != null && sectionContent.id.toLowerCase() == "con-info") {
                    var content = data.content[i].metadata;

                    $('input#proposalTitleInput').val(content.proposalTitleInput);
                    $("span[tag='travelDocsTitle']").text(content.proposalTitleInput);
                    $('div.travelDocsTitle').text(content.proposalTitleInput);
                    $('input#proposalCusNameInput').val(content.proposalCusNameInput);
                    $('div.travelDocsCusName').text(content.proposalCusNameInput);
                    $('input#proposalExtraInput').val(content.proposalExtraInput);
                    $('div.travelDocExtra').text(content.proposalExtraInput);
                    $('input#proposalSubtitleInput').val(content.proposalSubtitleInput);
                    $('div.travelDocsSubTitle').text(content.proposalSubtitleInput);
                } else {
                    console.log("other");
                }
            }
            var backgroundColor = $('div.proposalTitle').css('background-color');
            var textTopColor = $('div.proposalTitle').css('color');
            var blockColorHolder = $("div#cover-page-visibility-switch").find('.round-replacer-small');
            $(blockColorHolder).css('background-color', backgroundColor);
            $(blockColorHolder).find('.sp-preview-inner').css('background-color', backgroundColor);
            $('[name="coverPageContentBoxColor"]').spectrum('set', backgroundColor);
            var blockTextColor = $("div#cover-page-title-text-color").find('.round-replacer-small');
            $(blockTextColor).css('background-color', textTopColor);
            $(blockTextColor).find('.sp-preview-inner').css('background-color', textTopColor);
            $('[name="coverPageContentBoxTitleColor"]').spectrum('set', textTopColor);
            var textTopColor = $("div.proposalInfo").css('color');
			var blockTextTopColor = $("div#cover-page-content-text-color").find('.round-replacer-small');
			$(blockTextTopColor).css('background-color', textTopColor);
			$('[name="coverPageContentBoxContentColor"]').spectrum('set', textTopColor);
			$(blockTextTopColor).find('.sp-preview-inner').css('background-color', textTopColor);

			if($('.cover').hasClass('content-box-hidden')) {
				$('#coverpageContentBoxSwitch').click();
			}
			
			$('#template-style-container [name]').each(function() {
				var propInput = $(this);
				if(propInput.attr('type')=='checkbox') {
					propInput.prop('checked', styleSettings[propInput.attr('name')]=='true');
				} else {
					propInput.val(styleSettings[propInput.attr('name')]);
				}
				if(propInput.hasClass('color-spectrum')) {
					initSpectrum(propInput);
				}
			});
			
			setupActiveStyles();
			
        } else {

            $('.cover-page-chooser-button-main').data('bg', 'url(https://my.quotecloud.net/stores/GEN_COVERPAGES/6/ATTRFILE_image/cropped_helloworld.jpg)');
            contentInto = contentInto.replace(new RegExp('<:coverpageBGImage>', 'g'), 'url(https://my.quotecloud.net/stores/GEN_COVERPAGES/6/ATTRFILE_image/cropped_helloworld.jpg)');
            contentInto = contentInto.replace(new RegExp('<:coverpageTitleBG>', 'g'), '#f00808');
            contentInto = contentInto.replace(new RegExp('<:coverpageTitleColor>', 'g'), '#fff');
            contentInto = contentInto.replace(new RegExp('<:coverpageInfoBG>', 'g'), '#ffffff');
            contentInto = contentInto.replace(new RegExp('<:coverpageInfoColor>', 'g'), '#f00808');
            contentInto = contentInto.replace(new RegExp('<:coverpageAuthorBG>', 'g'), '#ffffff');
            contentInto = contentInto.replace(new RegExp('<:coverpageAuthorColor>', 'g'), '#f00808');

            var section = "    <div class=\"doc-page-area-sheet coverPageContent\" id=\"content-section-coverpage\" data-title=\""+ sectionTitle +"\">\n" +
                "                        <div class=\"cover coverpage-main-image\"> " +
                "								<div class=\"coverPlaceHolder\"> </div> " +
                "							<div class=\"coverpageSpectrum\" style=\"\"></div> " +
                "							<div class=\"proposalTitle\" style=\"\"> " +
                "								<div class=\"travelDocsTitle\" style=\"\">New Template</div> " +
                "									<div class=\"travelDocsSubTitle\" style=\"\">Sample Subject</div> " +
                "								<div class=\"travelDocExtra\"></div> " +
                "							</div> " +
                "							<div class=\"coverData\" style=\"\"> " +
                "								<div class=\"proposalInfo coverSection\" style=\"\"> " +
                "									<div class=\"client coverTopLeft level1\" style=\"\"> " +
                "										<div class=\"coverTitle\" style=\"\">PREPARED FOR:</div> " +
                "										<div class=\"travelDocsCusName\">${customerName}</div> " +
                "									</div> " +
                "								<div class=\"proposalDate coverTopMiddle level1\" style=\"\"> " +
                "									<div class=\"coverTitle\" style=\"\">DATE:</div> " +
                "									<div>${date}</div> " +
                "								</div> " +
                "								<div class=\"proposalDate coverTopRight level1\" style=\"\"> " +
                "									<div class=\"coverTitle\" style=\"font-size: 21px;\">BOOKING REF:</div> " +
                "									<div id=\"bookingRefNumber\">${referenceNumber}</div> " +
                "								</div> " +
                "							</div> " +
                "							<div class=\"authorInfo coverSection\" style=\"\"> " +
                "								<div class=\"author coverLeft level2\" style=\"\"> " +
				"									<div class=\"coverTitle\" style=\"\"> Travel From: </div> " +
				"									<div class=\"coverTitle\" style=\"\"> Travel To: </div> " +
                "									<div class=\"travelDocsConName\" style=\"\">${bookingAgent}</div> " +
                "									<div class=\"travelDocsConCompany\" style=\"\">${bookingCompany}</div> " +
                "									</div> " +
                "								<div class=\"contact coverRight level2\" style=\"\"> " +
                "									<div class=\"coverTitle\" style=\"\"> &nbsp; </div> " +
                "								<div class=\"coverTitleContainer\" style=\"\"> " +
                "									<i class=\"fas fa-phone\"></i><span class=\"travelDocsConPhone\">&nbsp;${agentPhone}</span> " +
                "								</div> " +
                "								<div class=\"coverTitleContainer\" style=\"\"> " +
                "									<i class=\"fas fa-envelope\"></i>&nbsp;${agentEmail}" +
                "								</div> " +
                "								<div class=\"coverTitleContainer\"><span class=\"travelDocsConEmails\">&nbsp;</span></div> " +
                "							</div> " +
                "						</div> " +
                "					</div> </div>\n" +
                "                    </div>";

            var tab = "  <li class=\" coverTab\">\n" +
                "                <button style=\"width: 100%;display: flex;text-align: left;\" type=\"button\" class=\"section-button\" data-section=\"content-section-coverpage\" data-title=\""+sectionTitle+"\">\n" +
                "                    <div class=\"tab-label\" style='position:relative;line-height: 23px;width: 90%;'>"+sectionTitle+"</div>\n" +
                "                    <div class=\"editButton\"></div>\n" +
                "                </button>\n" +
                "            </li>";
            $('.doc-page-area').append(section);
            $('#document-section-tabs').append(tab);
            $('head').append(contentInto);

            setupSections();
            setupDropZones();
            setupSwappables();

            initSpectrum($('.color-spectrum'));
           
        }
        initSwitchery();
        //setupStyleSettingsCSS();
    });
}

function setupSections(title, data) {
	if(data != null) {
		var unicode = uniqId();
		var $section = $('<div class="doc-page-area-sheet dynaContent" id="content-section-'+unicode+'"  data-title="'+ sectionTitle +'">');
		var $sectionContainer = $('<div class="page-content-area">');
		$section.append($sectionContainer);
		 if (data.type == "COVER") {
			setupCoverPage(data);
		}
		else if (data.type == "DYNAMIC"){
			var sectionTitle = data.title;
            for (var i = 0; i < data.content.length; i++) {
                var contentRaw = data.content[i];
                var colContent = contentRaw.colContent;
                var $contentHolder;
                if (contentRaw.type == "block-pricing-table") {
                	$contentHolder = $("<div class=\"content-holder sortable\" data-id=\"block-pricing-table-itinerary-"+ unicode +"\" data-type=\"" + contentRaw.type + "\" style=\"width: 100%; height: auto;\">");
                } else {
                	$contentHolder = $("<div class=\"content-holder sortable\" data-type=\"" + contentRaw.type + "\" style=\"width: 100%; height: auto;\">");
                }
                $sectionContainer.append($contentHolder);
                if (colContent != null) {
                	if(colContent.elements) {
	                	colContent.elements.forEach(function(component, index){
	                		var $component = $('<div>');
	                		$contentHolder.append($component);
	                		$component[component.type]({
	                            children : component.children,
	                            properties : component.properties,
	                            inner : component.inner
	                        });
	                	});
                	}
				} else {
                	if (contentRaw.type == "block-video") {
                		$contentHolder.append("<div class=\"videoContainer\" style=\"vertical-align: top;display: inline-block;text-align: center;width: 100%;padding: 15px;border: 1px dotted;\"><div class=\"iframe-wrapper\"><iframe src=\""+contentRaw.data+"\" style=\"width: 100%;min-height: 350px;\"></iframe></div></div>");
					} else {
						$contentHolder.append(contentRaw.data);
					}
				}
                
            }
		}
        var footerArea = "<div class=\"droppable-holder sortable ui-droppable\" id=\"footer-drop-area\" ></div>";
        
        $sectionContainer.append(footerArea);

        var tab = "  <li class=\" dynaTab\">\n" +
            "                <button style=\"width: 100%;display: flex;text-align: left;\" type=\"button\" class=\"section-button\" data-section=\"content-section-"+unicode+"\"  data-title=\""+sectionTitle+"\">\n" +
            "                    <div class=\"tab-label\" style=\"width: 90%;\">"+sectionTitle+"</div>\n" +
            "                    <div class=\"editButton\"><i class=\"fas fa-pencil-alt\"></i></div>\n" +
            "                    <div class=\"removeButton\"><i class=\"fas fa-times\"></i></div>\n" +
            "                </button>\n" +
            "                    <div class=\"section-sort-handle\"><i class=\"fas fa-arrows-alt-v\"></i></div>\n" +
            "            </li>";
        if (data.type != "COVER") {
        	$($section).prepend($('<div class="edit-section-title"><input class="section-title-input"></div>'));
        	$('.edit-section-title input', $section).val(sectionTitle).attr('placeholder', 'New Section');
            $('.doc-page-area').append($section);
            $('#document-section-tabs').append(tab);
            $('#document-section-tabs').sortable('refresh');
            initDroppables();
		}


	} else if (addTemplate) {
		var sectionTitle = "New Section";
		var unicode = uniqId();
		// var footerArea = "<div class=\"content-footer
		// droppable-holder\"></div>";
        var footerArea = "<div class=\"droppable-holder sortable ui-droppable\" id=\"footer-drop-area\" ></div>";
		var bodyArea = "";
		var section = "    <div class=\"doc-page-area-sheet dynaContent\" id=\"content-section-"+unicode+"\"  data-title=\"" + sectionTitle + "\">\n" +
			"                        <div class=\"page-content-area\">\n" +
			bodyArea + footerArea +
			"                        </div>\n" +
			"                    </div>";

		var tab = "  <li class=\" dynaTab\">\n" +
			"                <button style=\"width: 100%;text-align: left;display: flex;\" type=\"button\" class=\"section-button\" data-section=\"content-section-"+unicode+"\"  data-title=\""+sectionTitle+"\">\n" +
			"                    <div class=\"tab-label\" style=\"width: 90%;\">"+sectionTitle+"</div>\n" +
			"                    <div class=\"editButton\"><i class=\"fas fa-pencil-alt\"></i></div>\n" +
			"                    <div class=\"removeButton\"><i class=\"fas fa-times\"></i></div>\n" +
			"                </button>\n" +
			"                    <div class=\"section-sort-handle\"><i class=\"fas fa-arrows-alt-v\"></i></div>\n" +
			"            </li>";

		var $section = $(section);
		$($section).prepend($('<div class="edit-section-title"><input class="section-title-input"></div>'));
		$('.edit-section-title input', $section).attr('placeholder', 'New Section');
		$('.doc-page-area').append($section);
		$('#document-section-tabs').append(tab);
		$('#document-section-tabs').sortable('refresh');

		// sectionTitle = "Travel Itinerary";
		// unicode = uniqId();
		// bodyArea = "";
		// section = " <div class=\"doc-page-area-sheet dynaContent\"
		// id=\"content-section-"+unicode+"\" data-title=\"" + sectionTitle +
		// "\">\n" +
		// " <div class=\"page-content-area\">\n" +
		// bodyArea + footerArea +
		// " </div>\n" +
		// " </div>";
		// tab = " <li class=\" dynaTab\">\n" +
		// " <button style=\"width: 100%;text-align: left;\" type=\"button\"
		// class=\"section-button\" data-section=\"content-section-"+unicode+"\"
		// data-title=\""+sectionTitle+"\">\n" +
		// " <div class=\"tab-label\">"+sectionTitle+"</div>\n" +
		// " <div class=\"tab-numbered\"></div>\n" +
		// " </button>\n" +
		// " </li>";
        //
		// $('.doc-page-area').append(section);
		// $('#document-section-tabs').append(tab);

	} else {
		var sectionTitle = "New Section";
		var unicode = uniqId();
		var footerArea = "<div class=\"content-footer droppable-holder\"></div>";
		var bodyArea = "";
		var section = "    <div class=\"doc-page-area-sheet dynaContent\" id=\"content-section-"+unicode+"\" data-title=\"" + sectionTitle + "\">\n" +
			"                        <div class=\"page-content-area\">\n" +
			bodyArea + footerArea +
			"                        </div>\n" +
			"                    </div>";

		var tab = "  <li class=\" dynaTab\">\n" +
			"                <button style=\"width: 100%;text-align: left;display: flex;\" type=\"button\" class=\"section-button\" data-section=\"content-section-"+unicode+"\"  data-title=\""+sectionTitle+"\">\n" +
			"                    <div class=\"tab-label\" style=\"width: 90%;\">"+sectionTitle+"</div>\n" +
			"                    <div class=\"editButton\"><i class=\"fas fa-pencil-alt\"></i></div>\n" +
			"                    <div class=\"removeButton\"><i class=\"fas fa-times\"></i></div>\n" +
			"                </button>\n" +
			"                    <div class=\"section-sort-handle\"><i class=\"fas fa-arrows-alt-v\"></i></div>\n" +
			"            </li>";

		var $section = $(section);
		$($section).prepend($('<div class="edit-section-title"><input class="section-title-input"></div>'));
		$('.edit-section-title input', $section).attr('placeholder', 'New Section');
		$('.doc-page-area').append($section);
		$('#document-section-tabs').append(tab);
		$('#document-section-tabs').sortable('refresh');
	}

	$('#document-section-tabs li:last-of-type button').click();

}

function initDroppables() {
    $('.droppable-holder').each(function() {
        $(this).droppable({
            // activate: function( event, ui ) {
            // $('.droppable-holder').show();
            // },
            over: function(event, ui) {
                //$(this).css("background-color", "green");
            },

            out: function(event, ui) {
                //$(this).css("background-color", "lightblue");
            },
            drop: function( event, ui ) {

            	var dropzone = $(this);
            	insertContentBlock($(ui.draggable), dropzone);
            }
        });
    })
}

function insertContentBlock (contentBlock, dropzone) {
	var blockType = contentBlock.attr('id');
	if(!dropzone.parent().hasClass('ve_element_container') && blockType != 'block-columns' && blockType != 'block-newpage' && blockType != 'block-itinerary' && blockType != 'block-zerorisk') {
		var agContentHolder = $("<div class=\"content-new-holder\" data-type=\"block-columns\"><div class=\"content-new-loader\"></div></div>");
		agContentHolder.insertBefore(dropzone);
		agContentHolder.data('id', ("block-columns-" + uniqId()));
		var div = $('<div>');
		agContentHolder.append(div);
		div.newrow();
		createBlockWithMenu(agContentHolder, "", "", "");
		dropzone = agContentHolder.find('.droppable-holder');
	}
    var clone = contentBlock.clone(true).detach();
    $('#droppable-label').remove();
    dropzone.closest('.ve_col').removeClass('ve_col_empty');
    if ($(clone).attr('id').indexOf('block') < 0) {
        var contentHolder = $('<div class="content-new-holder" data-type="'+ $(clone).attr('id')+'" data-name=\"'+$(clone).data('name')+'">');
        contentHolder.insertBefore(dropzone);
        if($(clone).attr('id') == "content-library-images") {
        	var imageLink = $(clone).find('img').attr('src');
        	var imageName = $(clone).find('img').attr('data-name');
        	loadImageContainer(imageLink, imageName, contentHolder);
        }
        else if($(clone).attr('id') == "content-library-text") {
        	var text = $(clone).data('data');
        	loadTextContainer(text, contentHolder);
        }
        
        else if($(clone).attr('id') == "content-library-videos") {
        	var videolink = $(clone).data('data');
        	loadVideoContainer(videolink, contentHolder);
        }
	} else {
        var contentItem = "<div class=\"content-new-holder\" data-type=\""+ $(clone).attr('id')+"\" data-id=\""+ $(clone).data('id') +"\"><div class=\"content-new-loader\"></div></div>";
        var content =  contentItem;
        $(content).insertBefore(dropzone);
        $('.content-new-holder').animate({height:'50px', width:'100%'},
            400, function() {
                $('.content-new-holder').height('auto');
                setTimeout(function() {
                    loadContent($('.content-new-holder'));
                }, 500);
            });
	}
}

function initSortables() {
    $('.droppable-holder').each(function() {
        $(this).droppable({
            // activate: function( event, ui ) {
            // $('.droppable-holder').show();
            // },
            over: function(event, ui) {
                //$(this).css("background-color", "green");
            },

            out: function(event, ui) {
                //$(this).css("background-color", "lightblue");
            },
            drop: function( event, ui ) {

                var clone = $(ui.draggable).clone(true).detach();
                $('#droppable-label').remove();
                $(clone).removeClass('ui-sortable-helper');
                $(clone).attr('style', 'width: 100%; height: auto;');
                // $(clone).insertBefore($( this ));

            }
        });
    })
}

function restoreBlock(ele, eleType, eleID) {
    $.ajax({
        type: 'POST',
        url: '/api/blocks/getBlockOfType',
        data: {
            travelDocId: travelDocID,
            eleType: eleType,
            eleId: eleID
        },
        success: function (data) {

            var toolbar = loadMainToolbar(ele, null);

            $(ele).html(data).append(toolbar);
        },
        error: function(error) {
            console.log(error);
        }
    });
}

function uniqId() {
	return Math.round(new Date().getTime() + (Math.random() * 100));
}

var saveAndPreview = false;
function doSaveAndPreviewTravelDoc(event)
{
    logger.info("Saving traveldoc");
    // We may need to save fist... so create a callback function.
    var preview = function()
    {
        var proposalId = $("#param-proposal-id").val();
        var vKey = $("#doc-v-key").val();
        // sendProposal(proposalId);
        startLoadingForeground(saving_and_encrypting);
        window.location = "/view?traveldoc=" + proposalId + "&v-key=" + vKey;
    };

    saveAndPreview = true;

    if (addTemplate == true)
    {
        $('#saveTemplateModal').modal();
    }
    else
    {
        processSaveTravelDoc(event, preview);
    }

}

function doSaveTravelDoc(event, callback)
{
	saveAndPreview = false;
	if (addTemplate == true)
	{
		$('#saveTemplateModal').modal();
	}
	else
	{
		processSaveTravelDoc(event, callback);
	}

}

function processSaveTravelDoc(event, callback)
{

	var travelDoc = getCurrentTravelDoc();
	saveTravelDoc(travelDoc, callback);
	// var result = checkSaveRequirements();
	// if (result.ok)
	// {
	// }
	// else
	// {
	// swal({
	// title : "Sorry",
	// text : "Something have to be done before we can save: " + result.message,
	// type : "error",
	// showCancelButton : false,
	// confirmButtonColor : "#BA131A",
	// confirmButtonText : "Ok",
	// closeOnConfirm : true
	// }, function()
	// {
	// if (result.action)
	// result.action();
	// });
	// }
}

function checkSaveRequirements()
{
	var result = {
		ok : true
	};

	return result;
}

function getCurrentTravelDoc()
{
	return {
		version : 1,
		pricings : getTravelDocPricings(),
		sections : getTravelDocSections()
	}
}


function getTravelDocPricings()
{
	var pricings = [];
	$('div[data-type="block-pricing-table"]').each(function()
	{
		pricings.push($(this));
	});
	return pricings;
}

function getTravelDocSections()
{
    var orderedSections = [];
    var domSections = $("#document-section-tabs li button");
    $.each(domSections, function(i, e)
	{
        var section = {};
        var sectionId = $(e).data('section');
        var contentSection = $("div").find("[data-section='" + sectionId + "']");

        if (sectionId == "content-section-coverpage")
		{
            section.type = 'COVER';
            var sectionTitle = contentSection.data('title');
            section.title = sectionTitle;
		}
        else
        {
            section.type = "DYNAMIC";

            var sectionTitle = contentSection.data('title');
            section.title = sectionTitle;
        }

        section.content = [];
        if (sectionId == "content-section-coverpage") {
            var content = getTravelDocContent($('#' + sectionId));
            if (typeof content == 'object')
            {
                section.content.push(content);
            }

            var contentGenerated = getTravelDocContentGenerated();
            if (typeof contentGenerated == 'object')
            {
                section.content.push(contentGenerated);
            }
		} else {
        	var sectionID = '#' + sectionId;
        	var domContents = $(sectionID).find('.page-content-area');
        	var contentKids = $(domContents.children());
        	console.log(contentKids);
            $.each(contentKids, function(i, panel)
            {
                var content = getTravelDocContent(panel);
                if (typeof content == 'object')
                {
                    section.content.push(content);
                }
            });
		}

        orderedSections.push(section);

    });

	return orderedSections;
}

function getTravelDocContent(panel)
{

    var $domPanel = $(panel);
    var content = {};
    var id = $domPanel.attr('id');
    if (id != null && id == "content-section-coverpage") {
        content.type = "cover";
    } else {
        content.type = $domPanel.data('type');
    }
    if (content.type == undefined)
    {
        return true;
    }

    // Load type specifc fields...
    else if (content.type == 'cover')
    {
        var colContent = {};
        colContent.html = $(panel).html();
        colContent.css = $('.proposalCoverPageStyles').html();
		content.coverURL = $('.cover-page-chooser-button-main').data('bg') != null ? $('.cover-page-chooser-button-main').data('bg') : 'url("https://my.quotecloud.net/stores/GEN_COVERPAGES/4/ATTRFILE_image/cropped_bags4optimised.jpg")';

		var coverId = content.coverURL;
		coverId = coverId.substring(coverId.indexOf('GEN_COVERPAGES/') + ('GEN_COVERPAGES/').length, coverId.indexOf('/ATTRFILE_image'))

		var coverMetadata = {};
		coverMetadata.id = coverId;
		coverMetadata.name = $('#cover-page-chooser-button').find('#coverpage-title').text();
		
		$('#template-style-container [name]').each(function(i, prop){
			var propInput = $(prop);
			if(propInput.attr('type')=='checkbox') {
				coverMetadata[propInput.attr('name')] = propInput.is(':checked');
			}
			else {
				coverMetadata[propInput.attr('name')] = propInput.val();
			}
		});
		
		colContent.metadata = coverMetadata;

		content.coverData = colContent;
    }
    else if (content.type == 'block-text' || content.type == 'content-library-text')
    {

        var textContext = getFroalaEditorData($domPanel);
        var toolbar = $(panel).find('.touch-container');
        content.data = "<div class=\"panel-froala\" style=\"width: 100%; min-height: 50px;\">" + textContext + "</div>";

    }
    else if (content.type == 'block-newpage')
    {
        var block = $(panel).clone();
        $(block).find('.touch-container').each(function() {
            $(this).remove();
        });
        content.data = $(block).html();
    }
    else if (content.type == 'block-itinerary')
    {
        // Nothing needs to be returned here just leave blank
		var block = $(panel).remove('.touch-container');
        content.data = $(block).html();

    }
    else if (content.type == 'block-pricing-table')
    {
        var block = $(panel).remove('.touch-container');
        content.data = $(block).html();

    }
    else if (content.type == 'block-columns')
    {
    	var colContent = {};
    	colContent = convertToJSON($(panel));
    	
//    	colContent.dataLeft = [];
//    	colContent.dataRight = [];
//        var block = $(panel).remove('.touch-container');
//        var leftColumn = $(block).find('.left-column');
//        var leftContentKids = $($(leftColumn).children());
//        var rightColumn = $(block).find('.right-column');
//        var rightContentKids = $($(rightColumn).children());
//        $.each(leftContentKids, function(i, panel)
//        {
//            var content = getTravelDocContent(panel);
//            if (typeof content == 'object')
//            {
//                colContent.dataLeft.push(content);
//            }
//        });
//        $.each(rightContentKids, function(i, panel)
//        {
//            var content = getTravelDocContent(panel);
//            if (typeof content == 'object')
//            {
//                colContent.dataRight.push(content);
//            }
//        });
        content.colContent = colContent;
    }
    else if (content.type == 'block-image' || content.type == 'content-library-images')
    {
    	var block = $(panel).clone();
    	$(block).find('.touch-container').each(function() {
    		$(this).remove();
		});
    	$(block).find('.contentImageBlockHolder');
        // var block = $(panel).remove('.touch-container');
        content.data = $(block).html();
    }
    else if (content.type == 'block-video' || content.type == 'content-library-videos')
    {
        var html = $(panel).find('iframe').attr('src');
        content.data = html;
        content.properties = $(panel).find('#edit-content-block').data('custom-videoEdit').options.properties;
    }
    return content;
}

function getTravelDocContentGenerated()
{

    var content = {};
    var id = 'con-info';
    content.type = 'generated';
    content.id = id;
    content.data = "";

    content.metadata = {};
    $.each($('#coverpage-main-container').find('.input-content'), function(i, field)
    {
        content.metadata[$(field).attr("id")] = $(field).val() == null ? "" : $(field).val();
    });

    return content;
}

function saveTravelDoc(proposal_unused, callback)
{
	startLoadingForeground(saving_and_encrypting);

	var go = function()
	{
		var eKey = $("#param-e-key").val();
		var travelDocId = $("#param-proposal-id").val();


		var travelDoc = getCurrentTravelDoc();
		var travelDocJson = LZString.compressToBase64(JSON.stringify(travelDoc));
		// var proposalJson = JSON.stringify(proposal);
		var data = {
			travelDocJson : travelDocJson,
			'e-key' : eKey,
			traveldoc : travelDocId
		};
		if (addTemplate == true)
		{
			data.templateTitle = $('#template-title').val();
			data.templateDescription = $('#template-description').val();
			data.preview = saveAndPreview;
		}
		$.ajax('/api/secure/traveldoc/save', {
			type : "POST",
			data : data
		}).done(function(data, textStatus, jqXHR)
		{
			stopLoadingForeground();

			if (!data.success)
			{
				swal({
					title : "Oops, that's embarrassing",
					text : data.message,
					type : "warning"
				});
				return;
			}


			localStorage.removeItem('unsavedTravelDoc-' + travelDocId);
			lastSave = MD5(JSON.stringify(travelDoc));
			if (data.message)
			{
				swal({
					title : "Oops, that's embarrassing",
					text : data.message,
					type : "warning"
				}, function()
				{
					callback && callback();
				});
			}
			else if (data.data)
			{
				if (data.redirect)
				{
					window.location.replace(data.redirect);
				}
				else
				{
					window.location.replace('');
				}
			}
			else
			{
				callback && callback();
			}
		});
	};

	// Wait for any background tasks to complete.
	var poller = setInterval(function()
	{
		// TODO stop polling.. use events
		if (backgroundProcessing == 0)
		{
			clearInterval(poller);
			go();
		}
	}, 500);

}

function createSection(title, autoclick)
{
    var genId = "content-section-" + unique_id();
    var tab = $('#document-section-tabs .dynaTab').clone();
    tab.find('> button').attr('data-section', '' + genId).find("div.tab-label").text(title);
    tab.find('div.tab-numbered').hide();
    tab.appendTo('#document-section-tabs');


    var content = $('.doc-page-area .dynaContent').clone();
    content.attr('id', genId);
    content.attr('data-title', title);
    content.appendTo('.doc-page-area');

    if (autoclick)
    {
        $('[href="#' + genId + '"]').click();
    }
    logger.info("Created section with ID: " + genId);
    return genId;
}

function doDisableCoverPage()
{
    swal({
        title : "Warning!",
        text : "Disabling the Cover Page, all information in this area will be hidden in the proposal.",
        type : "warning",
        showCancelButton : true,
        cancelButtonText : "No",
        confirmButtonColor : "#FF8700",
        confirmButtonText : "Yes, disable it",
        closeOnConfirm : true
    }, function(confirm)
    {
        if (confirm)
        {
            var $tab = $("#documentSectionBrowser li.active");
            logger.info("Disabling Cover Page");
            $tab.addClass('notenabled');
            // $(".disabledSection", $tab).css('display', 'inline-block');
            // $(".enabledSection", $tab).css('display', 'none');
            var id = $("a", $tab).attr("href");
            $(".editorContent " + id).attr("data-enabled", false);
            $('.pricing-droppable:not(#htmlFragments .pricing-droppable)').droppable("disable");
        }
        else
        {
            changeSwitch("#pricing-tab-check", true);
        }
    });
}

function enableCoverPage()
{
    var $tab = $("#documentSectionBrowser li.active");
    logger.info("Enabling Cover Page");
    $tab.removeClass('notenabled');
    // $(".disabledSection", $tab).css('display', 'none');
    // $(".enabledSection", $tab).css('display', 'inline-block');
    var id = $("a", $tab).attr("href");
    $(".editorContent " + id).attr("data-enabled", true);
    $('.pricing-droppable:not(#htmlFragments .pricing-droppable)').droppable("enable")
}

function reorderSectionToIndex(sectionId, index)
{
    logger.info("Reordering section with ID: " + sectionId + " to index " + index);
    var target = $('#document-section-tabs li')[index];
    $('button[data-section="'+ sectionId +'"]').closest("li").insertBefore(target);
}

function getFroalaEditorData(editor)
{
	var editorPanel = $(editor).find('[data-editor-id]');
    if (editorPanel.length > 0)
    {
    	var froalaEditor = FroalaEditor.INSTANCES.find(x => x.id === parseInt(editorPanel.attr('data-editor-id'))); 
        return froalaEditor.html.get().replace(/\<br>/g, '<br/>');
    }
    else
    {
        return editor.html();
    }
}

function coverpageStyles(name, attr) {
    var coverpageBGImage = $('.cover-page-chooser-button-main').data('bg');
    var coverpageTitleBG = $("#cover-page-content-box-color").val();
    var coverpageTitleColor = $("#cover-page-content-box-title-text-color").val();
    var coverpageInfoBG = "#ffffff";
    var coverpageInfoColor = $("#cover-page-content-box-content-text-color").val();
    var coverpageAuthorBG = "#ffffff";
    var coverpageAuthorColor = $("#cover-page-content-box-content-text-color").val();
    $.get('/application/include/editorIncludes/coverpage.jsp', function(css){
        var contentInto = css;
        contentInto = contentInto.replace(new RegExp('<:coverpageBGImage>', 'g'), name.toLowerCase() != "coverpageBGImage" ? coverpageBGImage : attr);
        contentInto = contentInto.replace(new RegExp('<:coverpageTitleBG>', 'g'), name.toLowerCase() != "coverpageTitleBG" ? coverpageTitleBG : attr);
        contentInto = contentInto.replace(new RegExp('<:coverpageTitleColor>', 'g'), name.toLowerCase() != "coverpageTitleColor" ? coverpageTitleColor : attr);
        contentInto = contentInto.replace(new RegExp('<:coverpageInfoBG>', 'g'), name.toLowerCase() != "coverpageInfoBG" ? coverpageInfoBG : attr);
        contentInto = contentInto.replace(new RegExp('<:coverpageInfoColor>', 'g'), name.toLowerCase() != "coverpageInfoColor" ? coverpageInfoColor : attr);
        contentInto = contentInto.replace(new RegExp('<:coverpageAuthorBG>', 'g'), name.toLowerCase() != "coverpageAuthorBG" ? coverpageAuthorBG : attr);
        contentInto = contentInto.replace(new RegExp('<:coverpageAuthorColor>', 'g'), name.toLowerCase() != "coverpageAuthorColor" ? coverpageAuthorColor : attr);
        $('.proposalCoverPageStyles').remove();
        $('head').append(contentInto);
    });

}

String.prototype.capitalize = function() {
	return this.charAt(0).toUpperCase() + this.slice(1);
}

function initSpectrum(input) {
	input.spectrum({
		preferredFormat : "hex",
		replacerClassName : 'round-replacer-small margin',
		showInput : true,
		hide : function(tinycolor)
		{
			$('.tableStyle .editorArea').css('color', tinycolor.toHexString());
		},
		change : function(tinycolor)
		{
			{
				$('.sp-container .input-border').css('background-color', '#' + tinycolor.toHexString());
			}
		},
		move : function(tinycolor)
		{
			{
				$('.sp-container .input-border').css('background-color', '#' + tinycolor.toHexString());
			}
		},
		show : function(tinycolor)
		{
			{
				$('.sp-container .input-border').css('background-color', '#' + tinycolor.toHexString());
			}
		}
	});
}

function initSwitchery() {
	$('.side-bar-style-container input[type=checkbox]').each(function(){
		new Switchery(this, {
				color : '#64bd63',
				size : 'small'
			});
	});
}

function setupStyleSettingsCSS() {
	$('head').find('#styleSettingsCSS').remove();
	var css = '<style id="styleSettingsCSS">';
	css += '.editor-content {                                                                                                              ';
	css += '	font-family:"'+$('[name=fontFamilyBody]').val()+'" !important;                                                             ';
	css += '	font-size:'+$('[name=fontSizeBody]').val()+'px !important;                                                                 ';
	css += '	color:'+$('[name=fontColorBody]').val()+' !important;                                                                      ';
	css += '}                                                                                                                              ';
	css += '.editor-content p{                                                                                                             ';
	css += '	font-size:'+$('[name=fontSizeBody]').val()+'px;                                                                            ';
	css += '	color:'+$('[name=fontColorBody]').val()+' !important;                                                                      ';
	css += '}                                                                                                                              ';
	css += '.editor-content table{                                                                                                         ';
	css += '	font-size:'+$('[name=fontSizeBody]').val()+'px;                                                                            ';
	css += '	color:'+$('[name=fontColorBody]').val()+' !important;                                                                      ';
	css += '}                                                                                                                              ';
	css += '.editor-content .fr-box.fr-basic .fr-element{                                                                                  ';
	css += '	font-family:"'+$('[name=fontFamilyBody]').val()+'" !important;                                                             ';
	css += '	color:'+$('[name=fontColorBody]').val()+' !important;                                                                      ';
	css += '}                                                                                                                              ';
	css += '.editor-content ol, .editor-content ul {                                                                                     ';
	css += '	font-size:'+$('[name=fontSizeBody]').val()+'px;                                                                            ';
	css += '}                                                                                                                              ';
	css += '.editor-content h1 {                                                                                                           ';
	css += '	font-family:"'+$('[name=fontFamilyH1]').val()+'" !important;                                                               ';
	css += '	font-size:'+$('[name=fontSizeH1]').val()+'px !important;                                                                   ';
	css += $('[name=customHeading1Color]').prop('checked') ?'	color:'+$('[name=fontColorHeading1]').val()+' !important;' : '';
	css += $('[name=boldHeading1]').prop('checked') ? 'font-weight:bold !important;' : '';
	css += $('[name=uppercaseHeading1]').prop('checked') ? 'text-transform:uppercase !important;' : '';
	css += $('[name=underlineHeading1]').prop('checked') ? 'text-decoration:underline !important;' : '';
	css += '}                                                                                                                              ';
	css += '.editor-content h2 {                                                                                                           ';
	css += '	font-family:"'+$('[name=fontFamilyH2]').val()+'" !important;                                                               ';
	css += '	font-size:'+$('[name=fontSizeH2]').val()+'px !important;                                                                   ';
	css += $('[name=customHeading2Color]').prop('checked') ? '	color:'+$('[name=fontColorHeading2]').val()+' !important;' : '';
	css += $('[name=boldHeading2]').prop('checked') ? 'font-weight:bold !important;' : '';
	css += $('[name=uppercaseHeading2]').prop('checked') ? 'text-transform:uppercase !important;' : '';
	css += $('[name=underlineHeading2]').prop('checked') ? 'text-decoration:underline !important;' : '';
	css += '}                                                                                                                              ';
	css += '.editor-content h3 {                                                                                                           ';
	css += '	font-family:"'+$('[name=fontFamilyH3]').val()+'" !important;                                                               ';
	css += '	font-size:'+$('[name=fontSizeH3]').val()+'px !important;                                                                   ';
	css += $('[name=customHeading3Color]').prop('checked') ? '	color:'+$('[name=fontColorHeading3]').val()+' !important;' : '';
	css += $('[name=boldHeading3]').prop('checked') ? 'font-weight:bold !important;' : '';
	css += $('[name=uppercaseHeading3]').prop('checked') ? 'text-transform:uppercase !important;' : '';
	css += $('[name=underlineHeading3]').prop('checked') ? 'text-decoration:underline !important;' : '';
	css += '}                                                                                                                              ';
	css += '.editor-content .edit-section-title input {                                                                                                ';
	css += '	font-family:"'+$('[name=fontFamilySection]').val()+'" !important;                                                          ';
	css += '	font-size:'+$('[name=fontSizeSection]').val()+'px !important;                                                              ';
	css += $('[name=customSectionColor]').prop('checked') ? '	color:'+$('[name=fontColorSectionTitle]').val()+' !important;' : '';
	css += $('[name=boldSectionTitle]').prop('checked') ? 'font-weight:bold !important;' : '';
	css += $('[name=uppercaseSectionTitle]').prop('checked') ? 'text-transform:uppercase !important;' : '';
	css += $('[name=underlineSectionTitle]').prop('checked') ? 'text-decoration:underline !important;' : '';
	css += '}                                                                                                                              ';
	css += '</style>                                                                                                                       ';
	
	$('head').append($(css));
}

function setupActiveStyles (checkbox) {
	if(!checkbox) {
		$('.side-bar-style-container [data-icon=fa-paint-brush]').each(function(){
			setupActiveStyles($(this));
		});
	}
	else {
		var cardTitle = checkbox.closest('.collapse-card').find('.collapse-card__title');
		cardTitle.find('.fa.pull-right').remove();
		checkbox.closest('.section-group').find('input[type=checkbox]').each(function(){
			var input = $(this);
			if(input.prop('checked')) {
				var activeIcon = $('<i class="fa fa-fw pull-right"></i>');
				activeIcon.addClass(input.attr('data-icon'));
			}
			cardTitle.append(activeIcon);
			if(input.hasClass('toggle-custom-color')) {
				input.closest('.collapse-card__body').find('[tag=custom-color]')[input.prop('checked') ? 'show':'hide']();
			}
		});
		
	}
	
}