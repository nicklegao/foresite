var switcheryCoverpageVisibility;
var backgroundProcessing = 0;
var lastSave = "";
var restoredTravelDoc = undefined;





$(document).ready(function() {
    $(document).on("click", ".doSendTraveldocsConfirmedAction", sendProposalConfirmed);

    $(document).on('click', '.get-itinerary-directions-button', function (e) {
        e.preventDefault();
        var name = $(this).attr('data-name');
        var location = $(this).attr('data-location');
        name = name.trim();
        location = location.trim();
        name = name.split(' ').join('+');
        location = location.split(' ').join('+');

        var win = window.open("https://maps.google.com/?q=" + name + "+" + location, '_blank');
        win.focus();
    });

    $(document).on("click", ".doResendProposal", function(e)
    {


        $('body').modalmanager('loading');
        var $modal = $('#send-proposal-modal');
        var traveldocID = $(this).attr('data-id');

        $modal.load('/application/dialog/sendProposal.jsp?proposal=' + traveldocID, '', function() {
            console.log('MODAL LOADED');
            $modal.modal({
                backdrop: 'static',
                keyboard: false
            });
            // $modal.css('left', '42%');
            // $modal.css('top', '30%');
            $modal.css('width', '650px');
        });
    });

});

function sendProposalConfirmed(event)
{
    var proposalId = $(this).attr('traveldocID');

    var emailCc = $("#proposalSendEmail #emailCc").val();
    var emailSubject = $("#proposalSendEmail #emailSubject").val();

    var editor1 = new FroalaEditor('#emailBody', {}, function(){
    });

    var emailBody = editor1.html.get().replace(/\<br>/g, '<br/>').replace(/\%24/g, '$');

    var sendProposalDialog = $("#send-proposal-modal"); //.closest(".modal");
    sendProposalDialog.modal('hide');

    startLoadingForeground();

    setTimeout(function()
    {

        $.ajax({
            type : "POST",
            url : '/api/secure/proposal/send',
            data : {
                traveldoc : proposalId,
                'e-key' : $("#travelDoc-user-e-key").val(),

                emailCc : emailCc,
                emailSubject : emailSubject,
                emailBody : emailBody
            }
        }).done(function(response)
        {
            stopLoadingForeground();

            if (response == "OK")
            {
                $(document).one('foregroundLoadingComplete', function()
                {

                    Swal.fire({
                        title: 'TravelDoc Sent!',
                        text: 'The TravelDoc has been sent to the traveller',
                        type: 'success'
                    });
                });
            }
            else
            {
                Swal.fire({
                    title: 'Error Sending',
                    text: 'Unable to send proposal to the client. Server message:' + response + 'Please try again.',
                    type: 'error'
                });
            }

        });
    }, 1000);
}

var countries = [ {
			"code" : "AUD",
			"name" : "Australia - Australian Dollars (AUD)",
			"locale" : "en-AU"
		}, {
			"code" : "USD",
			"name" : "USA - US Dollar (USD)",
			"locale" : "en-US"
		}, {
			"code" : "EUR",
			"name" : "Europe - Euro (EUR)",
			"locale" : "de-AT"
		}, {
			"code" : "GBP",
			"name" : "United Kingdom - British Pound (GBP)",
			"locale" : "en-GB"
		}, {
			"code" : "CNY",
			"name" : "China - Chinese Renminbi (CNY)",
			"locale" : "zh-CN"
		}, {
			"code" : "CAD",
			"name" : "Canada - Canadian Dollar (CAD)",
			"locale" : "fr-CA"
		}, {
			"code" : "THB",
			"name" : "Thailand - Thai Baht (THB)",
			"locale" : "th-TH"
		}, {
    "code" : "HKD",
    "name" : "Hong Kong - Hong Kong Dollar (HKD)",
    "locale" : "zh-HK"
}, {
    "code" : "HKD",
    "name" : "Hong Kong, PR China - Hong Kong Dollar (HKD)",
    "locale" : "zh-HK"
}, {
			"code" : "SGD",
			"name" : "Singapore - Singapore Dollar (SGD)",
			"locale" : "en-SG"
		}, {
			"code" : "IDR",
			"name" : "Indonesia - Indonesian Rupiah (IDR)",
			"locale" : "id-ID"
		}, {
			"code" : "JPY",
			"name" : "Japan - Japanese Yen (JPY)",
			"locale" : "ja-JP-u-ca-japanese-x-lvariant-JP"
		}, {
			"code" : "FJD",
			"name" : "Fiji - Fijian Dollar (FJD)",
			"locale" : "en-FJ"
		}, {
			"code" : "NZD",
			"name" : "New Zealand - New Zealand Dollar (NZD)",
			"locale" : "en-NZ"
		}, {
			"code" : "BSD",
			"name" : "Bahamas - Bahamian Dollar (BSD)",
			"locale" : "en-BS"
		}, {
			"code" : "BHD",
			"name" : "Bahrain - Bahraini Dinar (BHD)",
			"locale" : "ar-BH"
		}, {
			"code" : "BBD",
			"name" : "Barbados - Barbadian Dollar (BBD)",
			"locale" : "en-BB"
		}, {
			"code" : "BRL",
			"name" : "Brazil - Brazilian Real (BRL)",
			"locale" : "pt-BR"
		}, {
			"code" : "BND",
			"name" : "Brunei Darussalam - Brunei Dollar (BND)",
			"locale" : "ms-BN"
		}, {
			"code" : "CLP",
			"name" : "Chile - Chilean Peso (CLP)",
			"locale" : "es-CL"
		}, {
			"code" : "COP",
			"name" : "Colombia - Colombian Peso (COP)",
			"locale" : "es-CO"
		}, {
			"code" : "HRK",
			"name" : "Croatia - Croatian Kuna (HRK)",
			"locale" : "hr-HR"
		}, {
			"code" : "CZK",
			"name" : "Czech Republic - Czech Koruna (CZK)",
			"locale" : "cs-CZ"
		}, {
			"code" : "DKK",
			"name" : "Denmark - Danish Krone (DKK)",
			"locale" : "da-DK"
		}, {
			"code" : "XPF",
			"name" : "French Polynesia - CFP Franc (XPF)",
			"locale" : "fr-PF"
		}, {
			"code" : "HUF",
			"name" : "Hungary - Hungarian Forint (HUF)",
			"locale" : "hu-HU"
		}, {
			"code" : "INR",
			"name" : "India - Indian Rupee (INR)",
			"locale" : "hi-IN"
		}, {
			"code" : "ILS",
			"name" : "Israel - Israeli New Sheqel (ILS)",
			"locale" : "he-IL"
		}, {
			"code" : "JOD",
			"name" : "Jordan - Jordanian Dinar (JOD)",
			"locale" : "ar-JO"
		}, {
			"code" : "KWD",
			"name" : "Kuwait - Kuwaiti Dinar (KWD)",
			"locale" : "ar-KW"
		}, {
			"code" : "MOP",
			"name" : "Macao - Macanese Pataca (MOP)",
			"locale" : "zh-MO"
		}, {
			"code" : "MYR",
			"name" : "Malaysia - Malaysian Ringgit (MYR)",
			"locale" : "ms-MY"
		}, {
			"code" : "MUR",
			"name" : "Mauritius - Mauritian Rupee (MUR)",
			"locale" : "en-MU"
		}, {
			"code" : "MXN",
			"name" : "Mexico - Mexican Peso (MXN)",
			"locale" : "es-MX"
		}, {
			"code" : "MAD",
			"name" : "Morocco - Moroccan Dirham (MAD)",
			"locale" : "ar-MA"
		}, {
			"code" : "NOK",
			"name" : "Norway - Norwegian Krone (NOK)",
			"locale" : "nn-NO"
		}, {
			"code" : "OMR",
			"name" : "Oman - Omani Rial (OMR)",
			"locale" : "ar-OM"
		}, {
			"code" : "PKR",
			"name" : "Pakistan - Pakistani Rupee (PKR)",
			"locale" : "en-PK"
		}, {
			"code" : "PGK",
			"name" : "Papua New Guinea - Papua New Guinean Kina (PGK)",
			"locale" : "en-PG"
		}, {
			"code" : "PHP",
			"name" : "Philippines - Philippine Peso (PHP)",
			"locale" : "en-PH"
		}, {
			"code" : "PLN",
			"name" : "Poland - Polish Zloty (PLN)",
			"locale" : "pl-PL"
		}, {
			"code" : "QAR",
			"name" : "Qatar - Qatari Riyal (QAR)",
			"locale" : "ar-QA"
		}, {
			"code" : "RUB",
			"name" : "Russian Federation - Russian Ruble (RUB)",
			"locale" : "ru-RU"
		}, {
			"code" : "WST",
			"name" : "Samoa - Samoan Tala (WST)",
			"locale" : "en-AS"
		}, {
			"code" : "SAR",
			"name" : "Saudi Arabia - Saudi Riyal (SAR)",
			"locale" : "ar-SA"
		}, {
			"code" : "SBD",
			"name" : "Solomon Islands - Solomon Islands Dollar (SBD)",
			"locale" : "en-SB"
		}, {
			"code" : "ZAR",
			"name" : "South Africa - South African Rand (ZAR)",
			"locale" : "en-ZA"
		}, {
			"code" : "KRW",
			"name" : "South Korea - South Korean Won (KRW)",
			"locale" : "ko-KR"
		}, {
			"code" : "LKR",
			"name" : "Sri Lanka - Sri Lankan Rupee (LKR)",
			"locale" : "ta-LK"
		}, {
			"code" : "SEK",
			"name" : "Sweden - Swedish Krona (SEK)",
			"locale" : "sv-SE"
		}, {
			"code" : "CHF",
			"name" : "Switzerland - Swiss Franc (CHF)",
			"locale" : "fr-CH"
		}, {
			"code" : "TWD",
			"name" : "Taiwan - New Taiwan Dollar (TWD)",
			"locale" : "zh-TW"
		}, {
			"code" : "TOP",
			"name" : "Tonga - Paanga (TOP)",
			"locale" : "to-TO"
		}, {
			"code" : "AED",
			"name" : "United Arab Emirates - UAE Dirham (AED)",
			"locale" : "ar-AE"
		}, {
			"code" : "VUV",
			"name" : "Vanuatu - Vanuatu Vatu (VUV)",
			"locale" : "en-VU"
		}, {
			"code" : "VND",
			"name" : "Vietnam - Vietnamese Dong (VND)",
			"locale" : "vi-VN"
		} ];
function getCountry(country) {
    if ((country.toLowerCase()).includes("china")) {
        country = "china";
    }
	var result = countries.find(x => x.name.indexOf(country) > (-1));

	if (result == null) {
	    return {
            "code" : "AUD",
            "name" : "Australia - Australian Dollars (AUD)",
            "locale" : "en-AU"
        };
    } else {
        return result;
    }
}
$(document).ready(function() {

    restoreTravelDoc();

    $('#waypointsTable tr').hover(function() {
        $(this).addClass('hover');
    }, function() {
        $(this).removeClass('hover');
    });

    $("ul#document-section-tabs").on("click", "button.section-button", function(){
        var sectionName = $(this).data('section');
        $('.editor-content-scroller').animate({

            scrollTop: 0
        }, 0);
        $('.editor-content-scroller').animate({

            scrollTop: $("#"+sectionName).offset().top - 60
        }, 0);
    });

});

function loadImageContainer(link, name, container) {

    var imageDiv = "<div class=\" width: 100%; height: auto;text-align: center;\"><img src=\""+link+"\" alt=\""+name+"\" style=\"max-width: 100%;\"></div>";

    $(container).empty().html(imageDiv);
}

function restoreTravelDoc() {
    var travelDocID = $("#param-proposal-id").val();
    var vKey = $("#param-v-key").val();

    if (localStorage['unsavedProposal-' + travelDocID])
    {
        swal({
            title : "Unsaved TravelDoc!",
            text : "Your last TravelDoc wasn't saved properly, would you like to restore it?",
            type : "warning",
            showCancelButton : true,
            cancelButtonText : "No, ignore",
            confirmButtonText : "Yes, restore it!",
            closeOnConfirm : true
        }, function(restore)
        {
            if (restore)
            {
                restoreStorageProposal(travelDocID, eKey);
            }
            else
            {
                restoreServerProposal(travelDocID, eKey);
            }
        });
    }
    else
    {
        restoreServerTravelDoc(travelDocID, vKey);
    }
}

function restoreServerTravelDoc(travelDocID, vKey) {
    startLoadingForeground(loading_and_decrypting);
    setTimeout(function()
    {

        $.ajax('/api/secure/traveldoc/loadViewData', {
            type : "POST",
            dataType : 'json',
            data : {
                traveldoc : travelDocID,
                traveldocId : travelDocID,
                vKey : vKey,
                userId : ownerID,
                email : previewEmail
            }
        }).done(function(data, textStatus, jqXHR)
        {
            restoredTravelDoc = data;
            logger.info("Raw data recieved");
            

            restoreSections(data.sections);

            logger.info("Done firing events for rebuilding document.");

            stopLoadingForeground();

            $(document).one('foregroundLoadingComplete', function()
            {
                $('body').find('.content-holder-toolbar').remove();
                $('.content-holder').find('.item').css("border", "");
            });
        });

    }, 750);
}

function restoreSections(sections)
{
    var pricingTableAdded = false;
    var itineraryTableAdded = false;
    $.each(sections, function(i, section)
    {

        var currentSectionId = 0;
        // Create and/or move the section into the correct index.

        if (section.type == 'DYNAMIC')
        {
            logger.info("Restoring dynamic section with title: " + section.title);
            currentSectionId = setupSections(section.title, section);
            // currentSectionId = createSection(section.title, false);
        }
        else if (section.type == 'COVER')
        {
            logger.info("Restoring coverpage");
            currentSectionId = setupSections(section.title, section);
        }
        else
        {
            logger.error("Unknown section type: " + section.type);
            // currentSectionId = createSection(section.title || section.type, false);
        }
        if (section.type != 'generated') {
            reorderSectionToIndex(currentSectionId, i);

            var $currentSection = $("#" + currentSectionId + " .page-content-area");

            // Populate the content
            $.each(section.content, function(j, content)
            {
                if (content.type != 'generated') {
                    restoreContent(j, content, $currentSection);
                }
            });

        }

    });
    return "";
}

function insertContentIntoEditor(targetSection, dataIndex, itemType, itemId, autoscroll, fixedPanel, top)
{
    var sendData = {};
    var proposalId = $("#param-proposal-id").val();
    sendData.type = itemType;
    sendData.proposalId = proposalId;
    if (itemId != undefined)
    {
        sendData.id = itemId;
    }

    return $.ajax({
        type : "POST",
        url : '/api/editor/content',
        data : sendData
    }).done(function(data, textStatus, jqXHR)
    {
        if (!top)
        {
            insertContentIntoPanel(targetSection, dataIndex, autoscroll, fixedPanel, data);
            if (itemType == 'Spreadsheet')
            {
                var $panel = $('.panel[dataOrder="' + dataIndex + '"]', targetSection);
                if (itemId)
                {
                    $.ajax({
                        type : "POST",
                        url : '/api/editor/spreadsheet',
                        data : {
                            itemId : itemId
                        }
                    }).done(function(data, textStatus, jqXHR)
                    {
                        console.log(data);
                        initKendoSpreadsheet($panel, data);
                    });
                }
                else
                {
                    initKendoSpreadsheet($panel);
                }
            }
        }
        if (itemType == "itinerary")
        {
            showHideRemarks();
        }
    });
}

function restoreContent(j, content, $currentSection)
{

    if (content.type == 'generated')
    {
        if (content.id == 'con-info')
        {
            var $docCover = $("#doc-cover");
            $.each(content.metadata, function(key, value)
            {
                var input = $("#" + key, $docCover);
                if (input.is(':checkbox'))
                {
                    input.prop('checked', value == 'on');
                }
                else
                {
                    input.val(value);
                }
            });


            var $tableOfContent = $('#tableOfContent').change(function()
            {
                if ($('#tableOfContent').is(':checked'))
                {
                    $('#tableOfContentLevelGroup').show();
                }
                else
                {
                    $('#tableOfContentLevelGroup').hide();
                }
            }).change();

            var $globalTitleHidden = $('#globalTitleHidden').change(function()
            {
                if ($('#globalTitleHidden').is(':checked'))
                {
                    hideHeadings(true);
                }
                else
                {
                    var all = true;
                    $('.noTitle-switch').each(function()
                    {
                        if (!($(this).is(':checked')))
                        {
                            all = false;
                        }
                    });
                    if (all)
                    {
                        hideHeadings(false);
                    }
                }
            }).change();

            var tableOfContentSwitchery = new Switchery($tableOfContent.get(0), {
                size : 'small'
            });

            var landscapeModeSwitchery = new Switchery($('#landscapeMode').get(0), {
                size : 'small'
            });
            var globalTitleHiddenSwitchery = new Switchery($globalTitleHidden.get(0), {
                color : '#fc0d00',
                size : 'small'
            });

            if ($('#showRemarks').length > 0)
            {
                var remarksSwitchery = new Switchery($('#showRemarks').get(0), {
                    size : 'small'
                });
            }

            // if (content.metadata['globalTitleHidden'] === "on")
            // {
            // $('#globalTitleHidden').click();
            // }


            $(".documentTagSrc", $docCover).keyup();// special
            if (content.metadata['cc-emails'] != undefined)
            {
                $("#cc-emails", $docCover).importTags(content.metadata['cc-emails']);
            }

            $("#displayOrderButton").prop('checked', content.metadata['displayOrderButton'] === "true");
            $("#showOrderChk").addClass(content.metadata['displayOrderButton'] == "true" ? "checked" : "");
            // Restore coverpage and color if possible...
            if (content.metadata['spectrumColor'] != undefined)
            {
                var spectrumColor = content.metadata['spectrumColor'];
                logger.info("Restoring doc color:" + spectrumColor);
                $('#colorPicker .btn-qcloud-design-' + spectrumColor).click();

                var coverPageImage = content.metadata['coverPageImage'];
                logger.info("Restoring coverPageImage ID:" + coverPageImage);
                $('#coverPicker .coverPageOption[pageId="' + coverPageImage + '"]').click();
            }
            else
            {
                $('#colorPicker .btn').first().click();
            }
        }
        else if (content.id == 'con-custom-fields')
        {
            var $docCover = $("#doc-cover");
            $.each(content.metadata, function(key, value)
            {
                if (customFields[key] && customFields[key].type == 'date')
                {
                    var _d = moment(value, 'YYYY-MM-DD');
                    if (_d.isValid())
                    {
                        $("#" + key, $docCover).datepicker('update', _d.toDate());
                    }
                    else
                    {
                        $("#" + key, $docCover).datepicker('update', '');
                    }
                }
                else
                {
                    $("#" + key, $docCover).val(value);
                }
            });
            // $(".documentTagSrc", $docCover).keyup();// special
        }
        else if (content.id.startsWith('con-fnb-'))
        {
            var categoryId = content.id.substring(8);
            var newSectionIndex = $("#doc-solution .contentContainer > .panel").length;
            logger.info('Restoring fNb category at index:' + newSectionIndex + ', with ID:' + categoryId);

            var $fnbPanel = $("#htmlFragments .con-fnb").clone();
            $fnbPanel.attr('dataId', 'con-fnb-' + categoryId);
            $fnbPanel.attr("dataOrder", newSectionIndex);

            $("#doc-solution .contentContainer").append($fnbPanel);
        }
        else if (content.id == 'con-plans')
        {
            addPricingTableAction(j);
            var $panel = $('.panel[dataOrder="' + j + '"]', $currentSection);
            if (content.breakPageBeforeInPDF === "on")
            {
                $panel.find('.panel-heading').find('.js-switch').trigger('click');
            }
            pricingTableAdded = true;
            return;
        }
        else if (content.id == 'con-iti')
        {
            return;
        }
        var $targetContentPane = $('.panel[type="generated"][dataid="' + content.id + '"]');
        $targetContentPane.attr("dataOrder", j);

        var $siblingContent = $targetContentPane.siblings(".panel");
        $.each($siblingContent, function(k, sibling)
        {
            var siblingOrder = parseInt($(sibling).attr('dataOrder'));
            if (siblingOrder < j)
            {
                $targetContentPane.insertAfter($(sibling));
            }
        });
    }
    else
    {
        var dynType = undefined;
        if (content.type == 'block-image')
        {
            dynType = 'Images';
        }
        else if (content.type == 'block-video')
        {
            dynType = 'Videos';
        }
        else if (content.type == 'block-text')
        {
            dynType = 'Text';
        }
        else if (content.type == 'block-destination')
        {
            dynType = 'destination';
        }
        else if (content.type == 'block-attachment')
        {
            dynType = 'fileHandle';
        }
        else if (content.type == 'block-itinerary')
        {
            dynType = 'itinerary';
        }
        else if (content.type == 'block-pricing-table')
        {
            dynType = 'pricing';
        }
        else if (content.type == 'block-columns')
        {
            dynType = 'columns';
        }
        else if (content.type == 'block-zerorisk')
        {
            dynType = 'zerorisk';
        }
        else
        {
            logger.error("Unknown content type: " + content.type);
        }

        if (dynType != undefined)
        {

            // have determined the type so load it.
            // var callback = insertContentIntoEditor($currentSection, j, dynType, content.id, false);

            // do some post-initilisation

            if (dynType == 'columns')
            {
                var colContent = content.colContent;
                $.each(colContent.elements, function(j, contentSub)
                {
                    if (contentSub.type != 'generated') {
                        restoreContent(j, contentSub, $currentSection);
                    }
                });
            }
            if (dynType == 'Text')
            {

            }
            if (dynType == 'itinerary') {

                var itineraryContent = $("div[data-type='block-itinerary']");
                $(itineraryContent).empty();
                restoreBlock($(itineraryContent), "itinerary", "itinerary")
            }
            if (dynType == 'pricing') {
                $("div[data-type='block-pricing-table']").each(function() {
                    var id = $(this).data('id');
                    if (id.indexOf("pricing-table-itinerary") >= 0) {
                        $(this).empty();
                        restoreBlock($(this), "pricing", "itinerary")
                    }
                });
            }
            if (dynType == 'columns') {
                $("div[data-type='block-pricing-table']").each(function() {
                    var id = $(this).data('id');
                    if (id.indexOf("pricing-table-itinerary") >= 0) {
                        $(this).empty();
                        restoreBlock($(this), "pricing", "itinerary")
                    }
                });
            }
            if (dynType == 'zerorisk') {

                var element = $($("div[data-type='block-zerorisk']")[0]);
                restoreZeroRiskBlockWithMenu(element, "<div class=\"zerorisk-container\"></div>", "", "");
            }
        }

    }

}

function restoreZeroRiskBlockWithMenu(ele, blockData, topMenuData, sideMenuData, library) {
    if (isTemplate) {
        $(ele).addClass('content-holder').addClass('sortable').removeClass('content-new-holder');

        if ($(ele).find( ".content-new-loader" ).length > 0) {
            $(ele).find( ".content-new-loader" ).remove();
        }

        blockData = "<div class=\"item\" style=\"vertical-align: top;display: inline-block;text-align: center;width: 100%;padding: 15px;border: 1px dotted;\"><i class=\"fas fa-compass fa-7x\"></i><span class=\"caption\" style=\"display: block; padding-top: 10px;\">This is where the ZeroRisk content will be loaded</span></div>";
        $(ele).append(blockData);

        return;
    }
    $.ajax({
        url : '/api/free/travel-watch/' + travelDocID,
        type : 'GET',
        async : false,
        beforeSend: function( xhr ) {
            console.log('BEFORE');
        },
        success: function (data) {
            // your callback here
            console.log('DONE');

            if (data.length > 0) {
                blockData = setupZeroRiskViewForFeed(blockData, data);
                $(ele).append(blockData);
            }

        },
        error: function (error) {
            // handle error
            console.log('ERROR');
            alert("ERROR");
        }
    });
}
function setupZeroRiskViewForFeed(blockData, data) {

    for (var i=0;i<data.length;i++) {
        var info = data[i];
        var countryInfo = info.country;
        if (countryInfo == null) {
            continue;
        }
        

        var pinActive = false;
        if (info.startdate != null) {
            var pinStartDate = new Date(info.startdate);
            var pinEndDate = info.enddate;
            if (pinEndDate == null) {
                pinEndDate = new Date(info.startDate);
                pinEndDate.setDate(info.startDate + 1);
            } else {
                pinEndDate = new Date(info.enddate);
            }
            var currentDate = new Date();
            if (currentDate > pinStartDate && currentDate < pinEndDate ){
                pinActive = true;
            }
        }


        var countryBlock = $('<div class="country-container" data-country="'+countryInfo.attr_categoryname+'"></div>');

        if (countryInfo.attr_assessmentdescription !== "") {
            countryBlock = $(countryBlock).append('<div class="country-description-container" style="border: 1px solid;border-radius: 4px;margin: 20px 10px;">' +
                '<div class="country-description" style="padding: 10px 20px;">' +
                '<div class="row">' +
                '<div class="col-auto" style="text-align: center;">' +
                '<div class="status-icon"><i class="fas fa-3x fa-exclamation-triangle"></i><span class="status-icon-text" style="display: block; line-height: 1; margin: 0; padding-top: 3px; text-transform: capitalize; letter-spacing: 0;">'+countryInfo.attrdrop_assessmentalertlevel+'</span> </div>' +
                '</div>' +
                '<div class="col"><span style="font-size: 12px;">'+countryInfo.attr_assessmentdescription+'</span></div>' +
                '</div>' +
                '</div>' +
                '</div>');
        }

        if (info.cityAlerts != null) {
            for(var key in info.cityAlerts) {
                if(info.cityAlerts.hasOwnProperty(key)) {
                    var cityAlerts = info.cityAlerts[key];
                    if (cityAlerts.length > 0) {
                        var alertsBlock = $('<div class="alerts-container" style="margin: 20px 10px;"><div class="row"><div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div><div class="col-auto"><div class="alert-section-title"><h6>Recent '+key+' Alerts</h6></div></div><div class="col"><hr style="margin: 12px 0px;"></div> </div></div>');

                        for (var a=0;a < cityAlerts.length; a++) {
                            var alert = cityAlerts[a];
                            var alertString = alert.attrtext_content;
                            if (alertString.length > 400) {
                                alertString = alertString.substring(0,400) +'...';
                            }
                            var date = new Date(alert.attrdate_start_date);
                            var color = "green";
                            if (alert.attr_rating === "Low") {
                                var color = "green";
                            } else if (alert.attr_rating === "Medium") {
                                var color = "orange";
                            } else {
                                var color = "red";
                            }
                            alertsBlock = $(alertsBlock).append('<div class="alert-info-container" style="border: 1px solid;border-radius: 4px;margin: 20px 0px;">' +
                                '<div class="country-description" style="padding: 10px 20px;">' +
                                '<div class="row">' +
                                '<div class="col">' +
                                '<div class="row">' +
                                '<div class="col"><div class="alert-text" style="max-height: 80px"><span style="font-size: 12px;">'+alertString+'...</span></div></div></div>' +
                                '<div class="row">' +
                                '<div class="col"><span style="font-size: 12px;">'+alert.attr_address+' -- '+date.getDay() + '/'+date.getMonth()+'/'+date.getFullYear()+'</span></div><div class="col-auto"><span style="font-size: 12px;">Read More in your ZeroRisk Secapp</span></div> </div>' +
                                ' </div>' +
                                '<div class="col-auto" style="text-align: center;">' +
                                '<div class="status-icon"><i class="fas fa-3x fa-exclamation-triangle" style="color: '+color+';"></i><span class="status-icon-text" style="display: block; line-height: 1; margin: 0; padding-top: 3px; text-transform: capitalize; letter-spacing: 0;">'+alert.attr_rating+'</span> </div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>');
                        }
                        countryBlock = $(countryBlock).append(alertsBlock);

                    } else {

                        var alertsBlock = $('<div class="alerts-container" style="margin: 20px 10px;"><div class="row"><div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div><div class="col-auto"><div class="alert-section-title"><h6>Recent '+key+' Alerts</h6></div></div><div class="col"><hr style="margin: 12px 0px;"></div> </div></div>');

                        alertsBlock = $(alertsBlock).append('<div class="alert-info-container" style="border: 1px solid;border-radius: 4px;margin: 20px 0px;">' +
                            '<div class="country-description" style="padding: 10px 20px;">' +
                            '<div class="row">' +
                            '<div class="col text-center">' +
                            '<span>No ZeroRisk alerts reported in this area at present.</span>' +
                            '</div>' +
                            '</div>' +
                            '</div>');
                        countryBlock = $(countryBlock).append(alertsBlock);
                    }
                }
            }


        } else {
            var alertsBlock = $('<div class="alerts-container" style="margin: 20px 10px;"><div class="row"><div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div><div class="col-auto"><div class="alert-section-title"><h6>Recent '+countryInfo.attr_categoryname.toLowerCase().capitalize()+' Alerts</h6></div></div><div class="col"><hr style="margin: 12px 0px;"></div> </div></div>');

            alertsBlock = $(alertsBlock).append('<div class="alert-info-container" style="border: 1px solid;border-radius: 4px;margin: 20px 0px;">' +
                '<div class="country-description" style="padding: 10px 20px;">' +
                '<div class="row">' +
                '<div class="col text-center">' +
                '<span>No ZeroRisk alerts reported in this area at present.</span>' +
                '</div>' +
                '</div>' +
                '</div>');
            countryBlock = $(countryBlock).append(alertsBlock);
        }

        if (info.travel != null && info.travel.length > 0) {
            var travelBlockInside = '';
            var travelRating = '';
            for (var t=0;t < info.travel.length; t++) {
                var travel = info.travel[t];
                travelRating = travel.attrdrop_assessmentalertlevel;
                var travelElementString = '';

                if ((travel.attr_video != null && travel.attr_video != "")) {
                    var url = '';
                    if (travel.attr_video.includes('youtub')) {
                        var code = travel.attr_video.replace('https://www.youtube.com/watch?v=','').replace('https://youtu.be/', '');
                        url = '<iframe width="420" height="315"\n' +
                            'src="https://www.youtube.com/embed/'+code+'"></iframe>';
                    } else if (travel.attr_video.includes('vimeo')) {
                        var code = travel.attr_video.replace('https://player.vimeo.com/video/','').replace('https://vimeo.com/', '');
                        url = '<iframe width="420" height="315"\n' +
                            'src="https://player.vimeo.com/video/'+code+'"></iframe>';
                    }
                    if (t % 2 == 0) {

                        travelElementString = '<div class="travel-info-container" style="margin: 20px 0px;">' +
                            '<div class="row">' +
                            '<div class="col-auto">'+url+'</div>' +
                            '<div class="col"> ' +
                            '<div class="travel-info-text">'+travel.attrlong_description+'</div> ' +
                            '</div></div>' +
                            '</div>';
                    } else {
                        travelElementString = '<div class="travel-info-container" style="margin: 20px 0px;">' +
                            '<div class="row"><div class="col"> ' +
                            '<div class="travel-info-text">'+travel.attrlong_description+'</div> ' +
                            '</div>' +
                            '<div class="col-auto">'+url+'</div>' +
                            '</div>' +
                            '</div>';
                    }
                } else if (travel.attrfile_image != null && travel.attrfile_image != "") {
                    if (t % 2 == 0) {
                        travelElementString = '<div class="travel-info-container" style="margin: 20px 0px;">' +
                            '<div class="row">' +
                            '<div class="col-auto"><img src="'+travel.attrfile_image+'" alt="'+travel.attr_headline+'"></div>' +
                            '<div class="col"> ' +
                            '<div class="travel-info-text">'+travel.attrlong_description+'</div> ' +
                            '</div></div>' +
                            '</div>';
                    } else {
                        travelElementString = '<div class="travel-info-container" style="margin: 20px 0px;">' +
                            '<div class="row"><div class="col"> ' +
                            '<div class="travel-info-text">'+travel.attrlong_description+'</div> ' +
                            '</div>' +
                            '<div class="col-auto"><img src="'+travel.attrfile_image+'" alt="'+travel.attr_headline+'"></div>' +
                            '</div>' +
                            '</div>';
                    }
                } else {
                    travelElementString = '<div class="travel-info-container" style="margin: 20px 0px;">' +
                        '<div class="row"><div class="col"> ' +
                        '<div class="travel-info-text">'+travel.attrlong_description+'</div> ' +
                        '</div></div>' +
                        '</div>';
                }
                travelBlockInside = travelElementString;
            }
            var travelBlock = $('<div class="travel-container" style="margin: 20px 10px;"><div class="row"><div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div><div class="col-auto"><div class="travel-section-title"><h6>Local Travel Information</h6></div></div><div class="col" style="padding-right: 0px;"><hr style="margin: 12px 0px;"></div><div class="col-auto">'+travelRating+'</div></div></div>');
            travelBlock = $(travelBlock).append(travelBlockInside);
            countryBlock = $(countryBlock).append(travelBlock);
        }

        if (info.terrorism != null && info.terrorism.length > 0) {
            var assetBlockInside = '';
            var assetRating = '';
            for (var t=0;t < info.terrorism.length; t++) {
                var asset = info.terrorism[t];
                assetRating = asset.attrdrop_assessmentalertlevel;
                var assetElementString = '';

                if ((asset.attr_video != null && asset.attr_video != "")) {
                    var url = '';
                    if (asset.attr_video.includes('youtub')) {
                        var code = asset.attr_video.replace('https://www.youtube.com/watch?v=','').replace('https://youtu.be/', '');
                        url = '<iframe width="420" height="315"\n' +
                            'src="https://www.youtube.com/embed/'+code+'"></iframe>';
                    } else if (asset.attr_video.includes('vimeo')) {

                        var code = asset.attr_video.replace('https://player.vimeo.com/video/','').replace('https://vimeo.com/', '');
                        url = '<iframe width="420" height="315"\n' +
                            'src="https://player.vimeo.com/video/'+code+'"></iframe>';
                    }
                    if (t % 2 == 0) {

                        assetElementString = '<div class="terrorism-info-container" style="margin: 20px 0px;">' +
                            '<div class="row">' +
                            '<div class="col-auto">'+url+'</div>' +
                            '<div class="col"> ' +
                            '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                            '</div></div>' +
                            '</div>';
                    } else {
                        assetElementString = '<div class="terrorism-info-container" style="margin: 20px 0px;">' +
                            '<div class="row"><div class="col"> ' +
                            '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                            '</div>' +
                            '<div class="col-auto">'+url+'</div>' +
                            '</div>' +
                            '</div>';
                    }
                } else if (asset.attrfile_image != null && asset.attrfile_image != "") {
                    if (t % 2 == 0) {
                        assetElementString = '<div class="terrorism-info-container" style="margin: 20px 0px;">' +
                            '<div class="row">' +
                            '<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
                            '<div class="col"> ' +
                            '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                            '</div></div>' +
                            '</div>';
                    } else {
                        assetElementString = '<div class="terrorism-info-container" style="margin: 20px 0px;">' +
                            '<div class="row"><div class="col"> ' +
                            '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                            '</div>' +
                            '<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
                            '</div>' +
                            '</div>';
                    }
                } else {
                    assetElementString = '<div class="terrorism-info-container" style="margin: 20px 0px;">' +
                        '<div class="row"><div class="col"> ' +
                        '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                        '</div></div>' +
                        '</div>';
                }
                assetBlockInside = assetElementString;
            }
            var assetBlock = $('<div class="asset-container" style="margin: 20px 10px;">' +
                '<div class="row">' +
                '<div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div>' +
                '<div class="col-auto">' +
                '<div class="asset-section-title">' +
                '<h6>Terrorism Threat</h6>' +
                '</div>' +
                '</div>' +
                '<div class="col"><hr style="margin: 12px 0px;"></div>' +
                '<div class="col-auto">'+assetRating+'</div>' +
                '</div>' +
                '</div>');
            assetBlock = $(assetBlock).append(assetBlockInside);
            countryBlock = $(countryBlock).append(assetBlock);
        }

        if (info.crime != null && info.crime.length > 0) {
            var assetBlockInside = '';
            var assetRating = '';
            for (var t=0;t < info.crime.length; t++) {
                var asset = info.crime[t];
                assetRating = asset.attrdrop_assessmentalertlevel;
                var assetElementString = '';

                if ((asset.attr_video != null && asset.attr_video != "")) {
                    var url = '';
                    if (asset.attr_video.includes('youtub')) {
                        var code = asset.attr_video.replace('https://www.youtube.com/watch?v=','').replace('https://youtu.be/', '');
                        url = '<iframe width="420" height="315"\n' +
                            'src="https://www.youtube.com/embed/'+code+'"></iframe>';
                    } else if (asset.attr_video.includes('vimeo')) {
                        var code = asset.attr_video.replace('https://player.vimeo.com/video/','').replace('https://vimeo.com/', '');
                        url = '<iframe width="420" height="315"\n' +
                            'src="https://player.vimeo.com/video/'+code+'"></iframe>';
                    }
                    if (t % 2 == 0) {

                        assetElementString = '<div class="crime-info-container" style="margin: 20px 0px;">' +
                            '<div class="row">' +
                            '<div class="col-auto">'+url+'</div>' +
                            '<div class="col"> ' +
                            '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                            '</div></div>' +
                            '</div>';
                    } else {
                        assetElementString = '<div class="crime-info-container" style="margin: 20px 0px;">' +
                            '<div class="row"><div class="col"> ' +
                            '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                            '</div>' +
                            '<div class="col-auto">'+url+'</div>' +
                            '</div>' +
                            '</div>';
                    }
                } else if (asset.attrfile_image != null && asset.attrfile_image != "") {
                    if (t % 2 == 0) {
                        assetElementString = '<div class="crime-info-container" style="margin: 20px 0px;">' +
                            '<div class="row">' +
                            '<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
                            '<div class="col"> ' +
                            '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                            '</div></div>' +
                            '</div>';
                    } else {
                        assetElementString = '<div class="crime-info-container" style="margin: 20px 0px;">' +
                            '<div class="row"><div class="col"> ' +
                            '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                            '</div>' +
                            '<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
                            '</div>' +
                            '</div>';
                    }
                } else {
                    assetElementString = '<div class="crime-info-container" style="margin: 20px 0px;">' +
                        '<div class="row"><div class="col"> ' +
                        '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                        '</div></div>' +
                        '</div>';
                }
                assetBlockInside = assetElementString;
            }
            var assetBlock = $('<div class="asset-container" style="margin: 20px 10px;">' +
                '<div class="row">' +
                '<div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div>' +
                '<div class="col-auto">' +
                '<div class="asset-section-title">' +
                '<h6>Local Crime Information</h6>' +
                '</div>' +
                '</div>' +
                '<div class="col"><hr style="margin: 12px 0px;"></div>' +
                '<div class="col-auto">'+assetRating+'</div>' +
                '</div>' +
                '</div>');
            assetBlock = $(assetBlock).append(assetBlockInside);
            countryBlock = $(countryBlock).append(assetBlock);
        }

        if (info.date != null && info.date.length > 0) {
            var assetBlockInside = '';
            var demoBlockInside = '';
            var assetRating = '';
            for (var t=0;t < info.date.length; t++) {
                var asset = info.date[t];
                assetRating = asset.attrdrop_assessmentalertlevel;
                var assetElementString = '';

                if ((asset.attr_video != null && asset.attr_video != "")) {
                    var url = '';
                    if (asset.attr_video.includes('youtub')) {
                        var code = asset.attr_video.replace('https://www.youtube.com/watch?v=','').replace('https://youtu.be/', '');
                        url = '<iframe width="420" height="315"\n' +
                            'src="https://www.youtube.com/embed/'+code+'"></iframe>';
                    } else if (asset.attr_video.includes('vimeo')) {
                        var code = asset.attr_video.replace('https://player.vimeo.com/video/','').replace('https://vimeo.com/', '');
                        url = '<iframe width="420" height="315"\n' +
                            'src="https://player.vimeo.com/video/'+code+'"></iframe>';
                    }
                    if (t % 2 == 0) {

                        assetElementString = '<div class="date-info-container" style="margin: 20px 0px;">' +
                            '<div class="row">' +
                            '<div class="col-auto">'+url+'</div>' +
                            '<div class="col"> ' +
                            '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                            '</div></div>' +
                            '</div>';
                    } else {
                        assetElementString = '<div class="date-info-container" style="margin: 20px 0px;">' +
                            '<div class="row"><div class="col"> ' +
                            '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                            '</div>' +
                            '<div class="col-auto">'+url+'</div>' +
                            '</div>' +
                            '</div>';
                    }
                } else if (asset.attrfile_image != null && asset.attrfile_image != "") {
                    if (t % 2 == 0) {
                        assetElementString = '<div class="date-info-container" style="margin: 20px 0px;">' +
                            '<div class="row">' +
                            '<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
                            '<div class="col"> ' +
                            '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                            '</div></div>' +
                            '</div>';
                    } else {
                        assetElementString = '<div class="date-info-container" style="margin: 20px 0px;">' +
                            '<div class="row"><div class="col"> ' +
                            '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                            '</div>' +
                            '<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
                            '</div>' +
                            '</div>';
                    }
                } else {
                    assetElementString = '<div class="date-info-container" style="margin: 20px 0px;">' +
                        '<div class="row"><div class="col"> ' +
                        '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                        '</div></div>' +
                        '</div>';
                }
                assetBlockInside = assetElementString;
            }
            if (info.demonstrations != null && info.demonstrations.length > 0) {
                for (var t=0;t < info.demonstrations.length; t++) {
                    var asset = info.demonstrations[t];
                    var demoElementString = '';

                    if ((asset.attr_video != null && asset.attr_video != "")) {
                        var url = '';
                        if (asset.attr_video.includes('youtub')) {
                            var code = asset.attr_video.replace('https://www.youtube.com/watch?v=','').replace('https://youtu.be/', '');
                            url = '<iframe width="420" height="315"\n' +
                                'src="https://www.youtube.com/embed/'+code+'"></iframe>';
                        } else if (asset.attr_video.includes('vimeo')) {
                            var code = asset.attr_video.replace('https://player.vimeo.com/video/','').replace('https://vimeo.com/', '');
                            url = '<iframe width="420" height="315"\n' +
                                'src="https://player.vimeo.com/video/'+code+'"></iframe>';
                        }
                        if (t % 2 == 0) {

                            demoElementString = '<div class="demonstrations-info-container" style="margin: 20px 0px;">' +
                                '<div class="row">' +
                                '<div class="col-auto">'+url+'</div>' +
                                '<div class="col"> ' +
                                '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                                '</div></div>' +
                                '</div>';
                        } else {
                            demoElementString = '<div class="demonstrations-info-container" style="margin: 20px 0px;">' +
                                '<div class="row"><div class="col"> ' +
                                '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                                '</div>' +
                                '<div class="col-auto">'+url+'</div>' +
                                '</div>' +
                                '</div>';
                        }
                    } else if (asset.attrfile_image != null && asset.attrfile_image != "") {
                        if (t % 2 == 0) {
                            demoElementString = '<div class="demonstrations-info-container" style="margin: 20px 0px;">' +
                                '<div class="row">' +
                                '<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
                                '<div class="col"> ' +
                                '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                                '</div></div>' +
                                '</div>';
                        } else {
                            demoElementString = '<div class="demonstrations-info-container" style="margin: 20px 0px;">' +
                                '<div class="row"><div class="col"> ' +
                                '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                                '</div>' +
                                '<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
                                '</div>' +
                                '</div>';
                        }
                    } else {
                        demoElementString = '<div class="demonstrations-info-container" style="margin: 20px 0px;">' +
                            '<div class="row"><div class="col"> ' +
                            '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                            '</div></div>' +
                            '</div>';
                    }
                    demoBlockInside = demoElementString;
                }
            }
            var assetBlock = $('<div class="asset-container" style="margin: 20px 10px;">' +
                '<div class="row">' +
                '<div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div>' +
                '<div class="col-auto">' +
                '<div class="asset-section-title">' +
                '<h6>Anniversaries &amp; Demonstrations</h6>' +
                '</div>' +
                '</div>' +
                '<div class="col"><hr style="margin: 12px 0px;"></div>' +
                '<div class="col-auto">'+assetRating+'</div>' +
                '</div>' +
                '</div>');
            assetBlock = $(assetBlock).append(assetBlockInside);
            assetBlock = $(assetBlock).append(demoBlockInside);
            countryBlock = $(countryBlock).append(assetBlock);
        }

        if (info.weather != null && info.weather.length > 0) {
            if (asset.attrlong_description != null && asset.attrlong_description != "") {
                var assetBlockInside = '';
                var assetRating = '';
                for (var t=0;t < info.weather.length; t++) {
                    var asset = info.weather[t];
                    assetRating = asset.attrdrop_assessmentalertlevel;
                    var assetElementString = '';

                    if ((asset.attr_video != null && asset.attr_video != "")) {
                        var url = '';
                        if (asset.attr_video.includes('youtub')) {
                            var code = asset.attr_video.replace('https://www.youtube.com/watch?v=','').replace('https://youtu.be/', '');
                            url = '<iframe width="420" height="315"\n' +
                                'src="https://www.youtube.com/embed/'+code+'"></iframe>';
                        } else if (asset.attr_video.includes('vimeo')) {
                            var code = asset.attr_video.replace('https://player.vimeo.com/video/','').replace('https://vimeo.com/', '');
                            url = '<iframe width="420" height="315"\n' +
                                'src="https://player.vimeo.com/video/'+code+'"></iframe>';
                        }
                        if (t % 2 == 0) {

                            assetElementString = '<div class="weather-info-container" style="margin: 20px 0px;">' +
                                '<div class="row">' +
                                '<div class="col-auto">'+url+'</div>' +
                                '<div class="col"> ' +
                                '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                                '</div></div>' +
                                '</div>';
                        } else {
                            assetElementString = '<div class="weather-info-container" style="margin: 20px 0px;">' +
                                '<div class="row"><div class="col"> ' +
                                '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                                '</div>' +
                                '<div class="col-auto">'+url+'</div>' +
                                '</div>' +
                                '</div>';
                        }
                    } else if (asset.attrfile_image != null && asset.attrfile_image != "") {
                        if (t % 2 == 0) {
                            assetElementString = '<div class="weather-info-container" style="margin: 20px 0px;">' +
                                '<div class="row">' +
                                '<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
                                '<div class="col"> ' +
                                '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                                '</div></div>' +
                                '</div>';
                        } else {
                            assetElementString = '<div class="weather-info-container" style="margin: 20px 0px;">' +
                                '<div class="row"><div class="col"> ' +
                                '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                                '</div>' +
                                '<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
                                '</div>' +
                                '</div>';
                        }
                    } else {
                        assetElementString = '<div class="weather-info-container" style="margin: 20px 0px;">' +
                            '<div class="row"><div class="col"> ' +
                            '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                            '</div></div>' +
                            '</div>';
                    }
                    assetBlockInside = assetElementString;
                }
                var assetBlock = $('<div class="asset-container" style="margin: 20px 10px;">' +
                    '<div class="row">' +
                    '<div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div>' +
                    '<div class="col-auto">' +
                    '<div class="asset-section-title">' +
                    '<h6>Weather &amp; Disaster Information</h6>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col"><hr style="margin: 12px 0px;"></div>' +
                    '<div class="col-auto">'+assetRating+'</div>' +
                    '</div>' +
                    '</div>');
                assetBlock = $(assetBlock).append(assetBlockInside);
                countryBlock = $(countryBlock).append(assetBlock);
            }
        }

        if (info.health != null && info.health.length > 0) {
            if (asset.attrlong_description != null && asset.attrlong_description != "") {
                var assetBlockInside = '';
                var assetRating = '';
                for (var t=0;t < info.health.length; t++) {
                    var asset = info.health[t];
                    assetRating = asset.attrdrop_assessmentalertlevel;
                    var assetElementString = '';

                    if ((asset.attr_video != null && asset.attr_video != "")) {
                        var url = '';
                        if (asset.attr_video.includes('youtub')) {
                            var code = asset.attr_video.replace('https://www.youtube.com/watch?v=','').replace('https://youtu.be/', '');
                            url = '<iframe width="420" height="315"\n' +
                                'src="https://www.youtube.com/embed/'+code+'"></iframe>';
                        } else if (asset.attr_video.includes('vimeo')) {
                            var code = asset.attr_video.replace('https://player.vimeo.com/video/','').replace('https://vimeo.com/', '');
                            url = '<iframe width="420" height="315"\n' +
                                'src="https://player.vimeo.com/video/'+code+'"></iframe>';
                        }
                        if (t % 2 == 0) {

                            assetElementString = '<div class="health-info-container" style="margin: 20px 0px;">' +
                                '<div class="row">' +
                                '<div class="col-auto">'+url+'</div>' +
                                '<div class="col"> ' +
                                '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                                '</div></div>' +
                                '</div>';
                        } else {
                            assetElementString = '<div class="health-info-container" style="margin: 20px 0px;">' +
                                '<div class="row"><div class="col"> ' +
                                '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                                '</div>' +
                                '<div class="col-auto">'+url+'</div>' +
                                '</div>' +
                                '</div>';
                        }
                    } else if (asset.attrfile_image != null && asset.attrfile_image != "") {
                        if (t % 2 == 0) {
                            assetElementString = '<div class="health-info-container" style="margin: 20px 0px;">' +
                                '<div class="row">' +
                                '<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
                                '<div class="col"> ' +
                                '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                                '</div></div>' +
                                '</div>';
                        } else {
                            assetElementString = '<div class="health-info-container" style="margin: 20px 0px;">' +
                                '<div class="row"><div class="col"> ' +
                                '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                                '</div>' +
                                '<div class="col-auto"><img src="'+asset.attrfile_image+'" alt="'+asset.attr_headline+'"></div>' +
                                '</div>' +
                                '</div>';
                        }
                    } else {
                        assetElementString = '<div class="health-info-container" style="margin: 20px 0px;">' +
                            '<div class="row"><div class="col"> ' +
                            '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                            '</div></div>' +
                            '</div>';
                    }
                    assetBlockInside = assetElementString;
                }
                var assetBlock = $('<div class="asset-container" style="margin: 20px 10px;">' +
                    '<div class="row">' +
                    '<div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div>' +
                    '<div class="col-auto">' +
                    '<div class="asset-section-title">' +
                    '<h6>Health &amp; Vaccine Information</h6>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col"><hr style="margin: 12px 0px;"></div>' +
                    '<div class="col-auto">'+assetRating+'</div>' +
                    '</div>' +
                    '</div>');
                assetBlock = $(assetBlock).append(assetBlockInside);
                countryBlock = $(countryBlock).append(assetBlock);
            }
        }

        if (info.emergencyProtocol != null && info.emergencyProtocol.length > 0) {
            if (asset.attrlong_description != null && asset.attrlong_description != "") {
                var assetBlockInside = '';
                var assetRating = '';
                for (var t=0;t < info.emergencyProtocol.length; t++) {
                    var asset = info.emergencyProtocol[t];
                    assetRating = asset.attrdrop_assessmentalertlevel;
                    var assetElementString = '';

                    assetElementString = '<div class="emergencyProtocol-info-container" style="margin: 20px 0px;">' +
                        '<div class="row"><div class="col"> ' +
                        '<div class="travel-info-text">'+asset.attrlong_description+'</div> ' +
                        '</div></div>' +
                        '</div>';
                    assetBlockInside = assetElementString;
                }
                var assetBlock = $('<div class="asset-container" style="margin: 20px 10px;">' +
                    '<div class="row">' +
                    '<div class="col-auto" style="padding-right: 0px;"><hr style="width: 40px;margin: 12px 0px;"></div>' +
                    '<div class="col-auto">' +
                    '<div class="asset-section-title">' +
                    '<h6>Critical Emergency Protocol</h6>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col"><hr style="margin: 12px 0px;"></div>' +
                    '<div class="col-auto">'+assetRating+'</div>' +
                    '</div>' +
                    '</div>');
                assetBlock = $(assetBlock).append(assetBlockInside);
                countryBlock = $(countryBlock).append(assetBlock);
            }
        }

        if (info.emergancyContact != null && info.emergancyContact.length > 0) {
            if (info.emergancyContact[0] != "") {
                var assetBlockInside = '';
                for (var t=0;t < info.emergancyContact.length; t++) {
                    var asset = info.emergancyContact[t];
                    var assetElementString = '';

                    assetElementString = '<div class="emergancy-contact-info-container" style="margin: 20px 0px;">' +
                        '<div class="row"><div class="col"> ' +
                        '<div class="travel-info-text">'+asset+'</div> ' +
                        '</div></div>' +
                        '</div>';

                    assetBlockInside = assetElementString;
                }
                var assetBlock = $('<div class="asset-container" style="margin: 20px 10px;"><div class="row"><div class="col"><div class="asset-section-title" style="padding-left: 40px;"><h6>Emergency Contact Information</h6></div></div><div class="col-auto">Important</div></div></div>');
                assetBlock = $(assetBlock).append(assetBlockInside);
                countryBlock = $(countryBlock).append(assetBlock);
            }
        }
        var card = $('<div class="card" style="box-shadow: none !important;"></div>');

        var header = $('<div class="card-header row ' +(!pinActive ? "collapsed" : "")+ '" data-toggle="collapse" data-target="#collapse_'+countryInfo.category_id+'" aria-expanded="true" aria-controls="collapse_'+countryInfo.category_id+'" style="margin: 0px; cursor: pointer;"><div class="col"><div class="wt-bg"><div class="flag-container"><span class="flag country-img-'+getCountry(countryInfo.attr_categoryname).code+'"></span></div></div> <h1 class="mb-0">\n' +
            '            <button class="btn btn-link" type="button">'+ countryInfo.attr_categoryname +'</button>\n' +
            '</h1></div></div>');

            if (countryInfo.attrdrop_assessmentalertlevel !== "") {
                var rightIndicator = $('<div class="col-auto"><div class="status-icon"><i class="fas fa-2x fa-exclamation-triangle" style="margin: 3px 0px;color: '+getAlertColor(countryInfo.attrdrop_assessmentalertlevel.toLowerCase())+';"></i></div></div>');
                header = $(header).append(rightIndicator);
            }


            var topCevron = $('<div class="col-auto"> <i class="fas fa-chevron-down"></i></div>');
            header = $(header).append(topCevron);

        var topAccordion = $('<div id="collapse_'+countryInfo.category_id+'" class="collapse ' +(pinActive ? "show" : "")+ '" aria-labelledby="headingOne" data-parent="#accordionExample"></div>').append($('<div class="card-body"></div>').append(countryBlock));

        card = $(card).append(header);
        card = $(card).append(topAccordion);

        blockData = $(blockData).append($('<div class="accordion" id="zerorisk-'+countryInfo.category_id+'-accordion"></div>').append(card));
    }

    return blockData;
}



function showMainCoverPageMenu(eleChanged, eleValue) {
    if (eleChanged != null) {
        if (eleChanged == "coverpage") {
            $('#coverpage-title').text(eleValue);
        }
    }
    $('.content-title').text('Cover Page Customizer');
    $('.title-back-button').hide();
    $('.title-back-button').removeData( "id" );
    $('#coverpage-main-container').show();
    $('#coverpage-dyn-loader').hide().empty();
}

function setupCoverPageSubContainer(eleID) {
    var folder = true;
    var folderCount = 3;
    var files = true;
    var filescount = 4;

    var folderhtml = '';
    var fileshtml = '';

    if (folder && folderCount > 0) {
        folderhtml += '<h7>Folders:</h7>';
        for (var i=1;i<=folderCount;i++) {
            folderhtml += '<div class="block-button-container" id="cover-page-chooser-button" data-id="coverpage-chooser" style="margin-bottom: 5px;">' +
                '<div class="button-content">' +
                '<i class="far fa-folder fa-sm"></i>' +
                '<span>Demo Folder '+ i +'</span>' +
                '<i class="fas fa-angle-right fa-sm" style="position:absolute;right:14px;top:11px"></i>' +
                '</div>' +
                '</div>';
        }
    }
    if (files && filescount > 0) {
        fileshtml += "<h7>Files:</h7>";
        fileshtml += "<div class=\"row content-container-row\" style=\"display: flex;margin: 0;padding-bottom:10px;\">";

        fileshtml += "<div class=\"content-files-column coverpage-image\" style=\"background: url('../../images/coverpages/cropped_bags4optimised.jpg');\"><div class=\"content-info\"><p style=\"margin: 0;\">Demo File</p></div></div>" +
            "</div><div class=\"row content-container-row\" style=\"display: flex;margin: 0;padding-bottom:10px;\">";
        fileshtml += "<div class=\"content-files-column coverpage-image\" style=\"background: url('../../images/coverpages/cropped_helloworld.jpg');\"><div class=\"content-info\"><p style=\"margin: 0;\">Demo File</p></div></div>" +
            "<div class=\"column\" style=\"flex: 1;\"></div>";
        fileshtml += "<div class=\"content-files-column coverpage-image\" style=\"background: url('../../images/coverpages/Travel_Solutions.jpg');\"><div class=\"content-info\"><p style=\"margin: 0;\">Demo File</p></div></div>" +
            "</div><div class=\"row content-container-row\" style=\"display: flex;margin: 0;padding-bottom:10px;\">";
        fileshtml += "<div class=\"content-files-column coverpage-image\" style=\"background: url('../../images/coverpages/zerorisk.jpg');\"><div class=\"content-info\"><p style=\"margin: 0;\">Demo File</p></div></div>" +
            "<div class=\"column\" style=\"flex: 1;\"></div>";
        // for (var i=1;i<=filescount;i++)
        // {
        //     if (i == filescount) {
        //         if ((filescount/2) % 1 == 0) {
        //             fileshtml += '<div class="content-files-column coverpage-image" style="background: url(https://my.quotecloud.net/stores/GEN_COVERPAGES/4/ATTRFILE_image/cropped_bags4optimised.jpg);"><div class="content-info"><p style="margin: 0;">File ' + i + '</p></div></div>' +
        //                 '</div>';
        //         } else {
        //             fileshtml += '<div class="content-files-column coverpage-image" style="background: url(https://my.quotecloud.net/stores/GEN_COVERPAGES/6/ATTRFILE_image/cropped_helloworld.jpg);"><div class="content-info"><p style="margin: 0;">File ' + i + '</p></div></div>' +
        //                 '<div class="column" style="flex: 1;"></div><div class="content-files-column coverpage-image" style="background: url(https://my.quotecloud.net/stores/GEN_COVERPAGES/4/ATTRFILE_image/cropped_bags4optimised.jpg);"><div class="content-info"><p style="margin: 0;">File ' + i + '</p></div></div>' +
        //                 '</div></div>';
        //         }
        //     } else {
        //         if (i % 2 === 0)
        //         {
        //             /* we are even */
        //             fileshtml += '<div class="content-files-column coverpage-image" style="background: url(https://my.quotecloud.net/stores/GEN_COVERPAGES/6/ATTRFILE_image/cropped_helloworld.jpg);"><div class="content-info"><p style="margin: 0;">File ' + i + '</p></div></div>' +
        //                 '</div><div class="row content-container-row" style="display: flex;margin: 0;padding-bottom:10px;">';
        //         }
        //         else
        //         {
        //             /* we are odd */
        //             fileshtml += '<div class="content-files-column coverpage-image" style="background: url(https://my.quotecloud.net/stores/GEN_COVERPAGES/4/ATTRFILE_image/cropped_bags4optimised.jpg);"><div class="content-info"><p style="margin: 0;">File ' + i + '</p></div></div>' +
        //                 '<div class="column" style="flex: 1;"></div>';
        //         }
        //     }
        // }
    }
    if (!folder && !files) {

    }
    $('#coverpage-dyn-loader').empty();
    $('#coverpage-dyn-loader').html(folderhtml + fileshtml);
}

function deleteImage( $item ) {
    $item = $item.clone();
    $item.fadeOut(function() {
        var $list = $( "ul", $trash ).length ?
            $( "ul", $trash ) :
            $( "<ul class='gallery ui-helper-reset'/>" ).appendTo( $trash );

        $item.find( "a.ui-icon-trash" ).remove();
        $item.append( recycle_icon ).appendTo( $list ).fadeIn(function() {
            $item
                .animate({ width: "48px" })
                .find( "img" )
                .animate({ height: "36px" });
        });
    });
}

function dropItem(clone, dropElem) {
    $('.subtitle.fancy.drop-label').each(function() {
        $(this).remove();
    });

    if (($(clone).hasClass('block-icon'))) {
        console.log("No class - New element");
        // var headerArea = "<div class=\"droppable-holder sortable ui-droppable\" style=\"\n" +
        //     "width: 100%; height: 40px;border: 1px solid lightblue;background-color: lightblue;margin-bottom: 20px;display: none;\"><p style=\"\n" +
        //     "text-align: center;padding-top: 8;\">Drop your block here</p></div>";
        var contentItem = "<div class=\"content-new-holder\" data-type=\""+ $(clone).attr('id')+"\" data-id=\""+ $(clone).data('id') +"\"><div class=\"content-new-loader\"></div></div>";
        var content = contentItem;
        $(content).insertBefore(dropElem);
        $('.content-new-holder').animate({height:'50px', width:'100%'},
            400, function() {
                $('.content-new-holder').height('auto');
                setTimeout(function() {
                    loadContent($('.content-new-holder'))
                    // loadDummyContent($('.content-new-holder'));
                }, 500);
            });
    }

}

function buildDependancies(eleType) {

    if (eleType.indexOf("itinerary") >= 0) {

        //Build pricing table holder
        var pricingTile = $('#block-pricing-table');

        console.log("No class - New element");
        // var headerArea = "<div class=\"droppable-holder sortable ui-droppable\" style=\"\n" +
        //     "width: 100%; height: 40px;border: 1px solid lightblue;background-color: lightblue;margin-bottom: 20px;display: none;\"><p style=\"\n" +
        //     "text-align: center;padding-top: 8;\">Drop your block here</p></div>";
        var contentItem = "<div class=\"content-new-holder\" data-type=\""+ $(pricingTile).attr('id')+"\" data-id=\""+ "itinerary-" + $(pricingTile).data('id') +"\"><div class=\"content-new-loader\"></div></div>";
        var content = contentItem;
        $(content).insertAfter($("div[data-type='block-itinerary']"));
        $('.content-new-holder').animate({height:'50px', width:'100%'},
            400, function() {
                $('.content-new-holder').height('auto');
                setTimeout(function() {
                    loadContent($('.content-new-holder'))
                }, 500);
            });



        // var itineraryPrcingEle = $("div").find('[data-type="block-pricing-table-itinerary"]');
        // if (itineraryPrcingEle.length == 0) {
        // 	var itineraryEle = $("div").find('[data-type="block-itinerary"]');
        // 	var contentItem = "<div class=\"content-new-holder\" data-type=\"block-pricing-table-itinerary\"><div class=\"content-new-loader\"></div></div>";
        // 	$(contentItem).insertBefore(itineraryEle);
        // 	$('.content-new-holder').animate({height:'50px', width:'100%'},
        // 		400, function() {
        // 			$('.content-new-holder').height('auto');
        // 			setTimeout(function() {
        // 				loadContent($('.content-new-holder'))
        // 			}, 500);
        // 		});
        // }
    }


}


function loadContent(ele, library) {

    var eleType = $(ele).attr('data-type');
    var eleId = $(ele).attr('data-id');

    // if (eleType.indexOf("pricing") >= 0) {
    //
    // 	createBlockWithMenu(ele, pricingtableview, "");
    // 	return;
    // }
    $.ajax({
        type: 'POST',
        url: '/api/blocks/getBlockOfType',
        data: {
            travelDocId: travelDocID,
            eleType: eleType,
            eleId: eleId
        },
        success: function (data) {
            console.log(data);
            if (eleType.indexOf("itinerary") >= 0) {
                $(ele).removeAttr('data-id');
                $(ele).data('id', (eleType + "-" + uniqId()));
                createBlockWithMenu(ele, data, "", "");
                $('#block-itinerary').css('color', 'rgba(0, 0, 0, 0.17)');
                $('#block-itinerary').draggable( 'disable' );
                buildDependancies(eleType);
            } else if (eleType.indexOf("pricing") >= 0) {
                if (eleId.indexOf('itinerary') >= 0) {
                    $(ele).removeAttr('data-id');
                    var id = (eleType + "-itinerary-" + uniqId());
                    ele = $(ele).attr('data-id', id);//data('id', id);
                } else {
                    $(ele).removeAttr('data-id');
                    ele = $(ele).data('id', (eleType + "-" + uniqId()));
                }
                createBlock(ele, data, "");
            } else if (eleType.indexOf("image") >= 0) {
                $(ele).removeAttr('data-id');
                $(ele).data('id', (eleType + "-" + uniqId()));
                var link = $(ele).data('url');
                if (link != null && link != "") {
                    createBlock(ele, data, "", "images");
                } else {
                    createBlock(ele, data, "");
                }

            } else {
                $(ele).removeAttr('data-id');
                $(ele).data('id', (eleType + "-" + uniqId()));

                createBlock(ele, data, "");
            }
        },
        error: function(error) {
            console.log(error);
        }
    });


}


function createBlock(ele, blockData, library) {

    $(ele).addClass('content-holder').addClass('sortable').removeClass('content-new-holder');
    $(ele).append(blockData);

    $(ele).find( ".content-new-loader" ).remove();

    if (library != null) {
        if (library == "images") {

            var link = $(ele).data('url');
            var name = $(ele).data('name');
            var container = $(ele).find('div.item');
            loadImageContainer(link, name, container);
        }
    }
}

function setupCoverPage(data) {
    
    var sectionTitle = "Cover Page";
    if (data != null) {
        for (var i = 0; i < data.content.length; i++) {
            var section = data.content[i];
            if (i==0) {
                var content = data.content["0"].coverData.html;
                var cssStyles = data.content["0"].coverData.css;
                var section = "    <div class=\"doc-page-area-sheet coverPageContent\" id=\"content-section-coverpage\" data-title=\"" + sectionTitle + "\">\n" + content + "</div>";

                var tab = "  <li class=\"ui-sort-disabled coverTab\">\n" + // <input type="checkbox" class="js-switch" id="coverpageVisible" checked />
                    "                <button style=\"width: 100%;text-align: left; display: flex;\" type=\"button\" class=\"section-button\" data-section=\"content-section-coverpage\" data-title=\"" + sectionTitle + "\">\n" +
                    "                    <div class=\"tab-label\" style='position:relative;line-height: 23px;width: 90%;'>" + sectionTitle + "</div>\n" +
                    "                    <div class=\"editButton\" style=\"width: 10%;padding-left: 10px;\"></div>\n" +
                    "                </button>\n" +
                    "            </li>";
                $('.doc-page-area').prepend(section);
                $('#document-section-tabs').prepend(tab);
                $('head').append('<style type="text/css" class="proposalCoverPageStyles">'+cssStyles+'</style>');
                if (restoredTravelDoc != null && restoredTravelDoc.booking != null) {
                    var booking = restoredTravelDoc.booking;

                    if (booking.passengers.passenger.length > 0) {
                        var passenger = booking.passengers.passenger[0];
                        $('.proposalInfo .client.coverTopLeft .travelDocsCusName').html(passenger.lastname + "/" + passenger.firstname);
                    }

                    $('#bookingRefNumber').html(booking.bookingReference);
                    var date = (booking.bookingStatus.lastUpdated != null ? booking.bookingStatus.lastUpdated : booking.bookingStatus.bookDate);
                    var finalDate = moment(date).format(dateFormatPattern);
                    $('.proposalInfo .proposalDate.coverTopMiddle div:not([class])').html(finalDate);
                    var companyName = (booking.company.name !== "" ? booking.company.name : booking.company.code);
                    var agentNumber = "";
                    if (booking.other != null) {
                        companyName = booking.other.agentName;
                        agentNumber = booking.other.agentNumber;
                    } else {
                        while(companyName.charAt(companyName.length-1) === "Z") {
                            companyName = companyName.substring(0, companyName.length-1);
                        }
                        agentNumber = "Unknown Number";
                    }
                    if (agentNumber === "") {
                        agentNumber = "Unknown Number";
                    }
                    if (companyName !== "") {
                        $('.authorInfo .author.coverLeft .travelDocsConCompany').html(companyName);
                    }
                    $($('.authorInfo .contact.coverRight .coverTitleContainer')[0]).html("<i class=\"fas fa-phone\"></i> " + agentNumber);
                    
                    if (booking.bookingEmails.email.length > 0) {
                        var bookingAgent = (booking.bookingEmails.email[0].name !== "" ? booking.bookingEmails.email[0].name : booking.bookingEmails.email[0].address);
                        if (bookingAgent.includes("@")) {
                            bookingAgent = bookingAgent.split('@')[0];
                        }
                        var bookingAgentEmail = booking.bookingEmails.email[0].address;
                        $('.authorInfo .author.coverLeft .travelDocsConName').html(bookingAgent);
                        $($('.authorInfo .contact.coverRight .coverTitleContainer')[1]).html("<i class=\"fas fa-envelope\"></i> " + bookingAgentEmail);
                    }
                }
            } else {
                console.log("other");
            }
        }
    }
}

function setupSections(sectionTitle, data) {
    if(data != null) {
    	var unicode = uniqId();
		var $section = $('<div class="doc-page-area-sheet dynaContent" id="content-section-'+unicode+'"  data-title="'+ sectionTitle +'">');
		var $sectionHead = $('<div>').addClass('section-head').text(sectionTitle);
		var $sectionContainer = $('<div class="page-content-area">');
		$section.append($sectionHead);
		$section.append($sectionContainer);
        if (data.type == "COVER") {
            setupCoverPage(data);
        }
        else if (data.type == "DYNAMIC"){
            var sectionTitle = data.title;
            for (var i = 0; i < data.content.length; i++) {
                var contentRaw = data.content[i];
                if (contentRaw.type != null && contentRaw.type === "block-newpage") {
                    continue;
                }
                var colContent = contentRaw.colContent;
                var $contentHolder;
                if (contentRaw.type == "block-pricing-table") {
                	$contentHolder = $("<div class=\"content-holder sortable\" data-id=\"block-pricing-table-itinerary-"+ unicode +"\" data-type=\"" + contentRaw.type + "\" style=\"width: 100%; height: auto;\">");
                } else {
                	$contentHolder = $("<div class=\"content-holder sortable\" data-type=\"" + contentRaw.type + "\" style=\"width: 100%; height: auto;\">");
                }
                if (colContent != null) {
                	if(colContent.elements) {
                		colContent.elements.forEach(function(component, index){
                			var $component = $('<div>');
	                		$contentHolder.append($component);
	                		$component[component.type]({
	                            children : component.children,
	                            properties : component.properties,
	                            inner : component.inner,
	                            viewer : true
	                        });
                		});
                	}
                } else {
                    if (contentRaw.type == "block-video") {
                    	$contentHolder.append("<div class=\"videoContainer\" style=\"vertical-align: top;display: inline-block;text-align: center;width: 100%;padding: 15px;\"><iframe src=\""+contentRaw.data+"\" style=\"width: 100%;border: none;\"></iframe></div>");
                    } else {
                    	$contentHolder.append(contentRaw.data);
                    }
                }

                $sectionContainer.append($contentHolder);
            }
            // if (contentRaw.type =="block-itinerary") {
            //     $('#block-itinerary').draggable( 'disable' );
            //     // restoreBlock()
            // }
            // if (contentRaw.type =="block-pricing") {
            //
            // }
        }

        var tab = "  <li class=\"ui-sort-disabled dynaTab\">\n" +
            "                <button style=\"width: 100%;display: flex;text-align: left;\" type=\"button\" class=\"section-button\" data-section=\"content-section-"+unicode+"\"  data-title=\""+sectionTitle+"\">\n" +
            "                    <div class=\"tab-label\" style=\"width: 90%;\">"+sectionTitle+"</div>\n" +
            "                </button>\n" +
            "            </li>";
        if (data.type != "COVER") {
            $('.doc-page-area').append($section);
            $('#document-section-tabs').append(tab);
        }


    }

}

function restoreBlock(ele, eleType, eleID) {
    $.ajax({
        type: 'POST',
        url: '/api/blocks/getBlockOfType',
        data: {
            travelDocId: travelDocID,
            eleType: eleType,
            eleId: eleID
        },
        success: function (data) {

            $(ele).html(data);
        },
        error: function(error) {
            console.log(error);
        }
    });
}

function uniqId() {
    return Math.round(new Date().getTime() + (Math.random() * 100));
}

function createSection(title, autoclick)
{
    var genId = "content-section-" + unique_id();
    var tab = $('#document-section-tabs .dynaTab').clone();
    tab.find('> button').attr('data-section', '' + genId).find("div.tab-label").text(title);
    tab.find('div.tab-numbered').hide();
    tab.appendTo('#document-section-tabs');


    var content = $('.doc-page-area .dynaContent').clone();
    content.attr('id', genId);
    content.attr('data-title', title);
    content.appendTo('.doc-page-area');

    if (autoclick)
    {
        $('[href="#' + genId + '"]').click();
    }
    logger.info("Created section with ID: " + genId);
    return genId;
}

function reorderSectionToIndex(sectionId, index)
{
    logger.info("Reordering section with ID: " + sectionId + " to index " + index);
    var target = $('#document-section-tabs li')[index];
    $('button[data-section="'+ sectionId +'"]').closest("li").insertBefore(target);
}

function getAlertColor(level) {
    switch (level) {
        case "l":
            return 'green';
        case "m":
            return 'orange';
        case "h":
            return 'red';
        default:
                return 'green';

    }
}

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}