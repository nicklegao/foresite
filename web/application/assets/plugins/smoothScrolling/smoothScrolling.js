! function() {
    function a() {
        r.keyboardSupport && i("keydown", e)
    }
    function b() {
        if (document.body) {
            var b = document.body,
                c = document.documentElement,
                d = window.innerHeight,
                e = b.scrollHeight;
            if (w = document.compatMode.indexOf("CSS") >= 0 ? c : b, p = b, a(), v = !0, top != self) t = !0;
            else if (e > d && (b.offsetHeight <= d || c.offsetHeight <= d)) {
                var f = !1,
                    g = function() {
                        f || c.scrollHeight == document.height || (f = !0, setTimeout(function() {
                            c.style.height = document.height + "px", f = !1
                        }, 500))
                    };
                if (c.style.height = "auto", setTimeout(g, 10), w.offsetHeight <= d) {
                    var h = document.createElement("div");
                    h.style.clear = "both", b.appendChild(h)
                }
            }
            r.fixedBackground || s || (b.style.backgroundAttachment = "scroll", c.style.backgroundAttachment = "scroll")
        }
    }
    function c(a, b, c, d) {
        if (d || (d = 1e3), k(b, c), 1 != r.accelerationMax) {
            var e = +new Date,
                f = e - B;
            if (f < r.accelerationDelta) {
                var g = (1 + 30 / f) / 2;
                g > 1 && (g = Math.min(g, r.accelerationMax), b *= g, c *= g)
            }
            B = +new Date
        }
        if (z.push({
                x: b,
                y: c,
                lastX: b < 0 ? .99 : -.99,
                lastY: c < 0 ? .99 : -.99,
                start: +new Date
            }), !A) {
            var h = a === document.body,
                i = function(e) {
                    for (var f = +new Date, g = 0, j = 0, k = 0; k < z.length; k++) {
                        var l = z[k],
                            m = f - l.start,
                            n = m >= r.animationTime,
                            p = n ? 1 : m / r.animationTime;
                        r.pulseAlgorithm && (p = o(p));
                        var q = l.x * p - l.lastX >> 0,
                            s = l.y * p - l.lastY >> 0;
                        g += q, j += s, l.lastX += q, l.lastY += s, n && (z.splice(k, 1), k--)
                    }
                    h ? window.scrollBy(g, j) : (g && (a.scrollLeft += g), j && (a.scrollTop += j)), b || c || (z = []), z.length ? F(i, a, d / r.frameRate + 1) : A = !1
                };
            F(i, a, 0), A = !0
        }
    }
    function d(a) {
        v || b();
        var d = a.target,
            e = h(d);
        if (!e || a.defaultPrevented || j(p, "embed") || j(d, "embed") && /\.pdf/i.test(d.src)) return !0;
        var f = a.wheelDeltaX || 0,
            g = a.wheelDeltaY || 0;
        if (f || g || (g = a.wheelDelta || 0), !r.touchpadSupport && l(g)) return !0;
        Math.abs(f) > 1.2 && (f *= r.stepSize / 120), Math.abs(g) > 1.2 && (g *= r.stepSize / 120), c(e, -f, -g), a.preventDefault()
    }
    function e(a) {
        var b = a.target,
            d = a.ctrlKey || a.altKey || a.metaKey || a.shiftKey && a.keyCode !== y.spacebar;
        if (/input|textarea|select|embed/i.test(b.nodeName) || b.isContentEditable || a.defaultPrevented || d) return !0;
        if (j(b, "button") && a.keyCode === y.spacebar) return !0;
        var e, f = 0,
            g = 0,
            i = h(p),
            k = i.clientHeight;
        switch (i == document.body && (k = window.innerHeight), a.keyCode) {
            case y.up:
                g = -r.arrowScroll;
                break;
            case y.down:
                g = r.arrowScroll;
                break;
            case y.spacebar:
                e = a.shiftKey ? 1 : -1, g = -e * k * .9;
                break;
            case y.pageup:
                g = .9 * -k;
                break;
            case y.pagedown:
                g = .9 * k;
                break;
            case y.home:
                g = -i.scrollTop;
                break;
            case y.end:
                var l = i.scrollHeight - i.scrollTop - k;
                g = l > 0 ? l + 10 : 0;
                break;
            case y.left:
                f = -r.arrowScroll;
                break;
            case y.right:
                f = r.arrowScroll;
                break;
            default:
                return !0
        }
        c(i, f, g), a.preventDefault()
    }
    function f(a) {
        p = a.target
    }
    function g(a, b) {
        for (var c = a.length; c--;) C[E(a[c])] = b;
        return b
    }
    function h(a) {
        var b = [],
            c = w.scrollHeight;
        do {
            var d = C[E(a)];
            if (d) return g(b, d);
            if (b.push(a), c === a.scrollHeight) {
                if (!t || w.clientHeight + 10 < c) return g(b, document.body)
            } else if (a.clientHeight + 10 < a.scrollHeight && (overflow = getComputedStyle(a, "").getPropertyValue("overflow-y"), "scroll" === overflow || "auto" === overflow)) return g(b, a)
        } while (a = a.parentNode)
    }
    function i(a, b, c) {
        window.addEventListener(a, b, c || !1)
    }
    function j(a, b) {
        return (a.nodeName || "").toLowerCase() === b.toLowerCase()
    }
    function k(a, b) {
        a = a > 0 ? 1 : -1, b = b > 0 ? 1 : -1, u.x === a && u.y === b || (u.x = a, u.y = b, z = [], B = 0)
    }
    function l(a) {
        if (a) {
            a = Math.abs(a), x.push(a), x.shift(), clearTimeout(D);
            return !(m(x[0], 120) && m(x[1], 120) && m(x[2], 120))
        }
    }
    function m(a, b) {
        return Math.floor(a / b) == a / b
    }
    function n(a) {
        var b, c, d;
        return a *= r.pulseScale, a < 1 ? b = a - (1 - Math.exp(-a)) : (c = Math.exp(-1), a -= 1, d = 1 - Math.exp(-a), b = c + d * (1 - c)), b * r.pulseNormalize
    }
    function o(a) {
        return a >= 1 ? 1 : a <= 0 ? 0 : (1 == r.pulseNormalize && (r.pulseNormalize /= n(1)), n(a))
    }
    var p, q = {
            frameRate: 150,
            animationTime: 600,
            stepSize: 80,
            pulseAlgorithm: !0,
            pulseScale: 8,
            pulseNormalize: 1,
            accelerationDelta: 20,
            accelerationMax: 1,
            keyboardSupport: !0,
            arrowScroll: 50,
            touchpadSupport: !0,
            fixedBackground: !0,
            excluded: ""
        },
        r = q,
        s = !1,
        t = !1,
        u = {
            x: 0,
            y: 0
        },
        v = !1,
        w = document.documentElement,
        x = [120, 120, 120],
        y = {
            left: 37,
            up: 38,
            right: 39,
            down: 40,
            spacebar: 32,
            pageup: 33,
            pagedown: 34,
            end: 35,
            home: 36
        },
        r = q,
        z = [],
        A = !1,
        B = +new Date,
        C = {};
    setInterval(function() {
        C = {}
    }, 1e4);
    var D, E = function() {
            var a = 0;
            return function(b) {
                return b.uniqueID || (b.uniqueID = a++)
            }
        }(),
        F = function() {
            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || function(a, b, c) {
                    window.setTimeout(a, c || 1e3 / 60)
                }
        }(),
        G = /chrome/i.test(window.navigator.userAgent),
        H = null;
    "onwheel" in document.createElement("div") ? H = "wheel" : "onmousewheel" in document.createElement("div") && (H = "mousewheel"), H && G && (i(H, d), i("mousedown", f), i("load", b))
}();