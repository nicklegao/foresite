/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here.
    // For complete reference see:
    // http://docs.ckeditor.com/#!/api/CKEDITOR.config
    config.extraPlugins = 'richcombo,button,panel,panelbutton,floatpanel,colorbutton,colordialog';
    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbar = [
        {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', '-', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
        {name: 'paragraph', items: ['NumberedList', 'BulletedList']},
        {name: 'paragraph', items: ['Outdent', 'Indent']},
        {name: 'editing', items: ['Scayt']},
        {name: 'styles', items: ['Format', 'FontSize']},
        '/',
        {name: 'insert', items: ['Table']},
        {name: 'links', items: ['Link', 'Unlink']},
        {name: 'clipboard', items: ['Undo', 'Redo']},
        {name: 'color', items: ['TextColor', 'BGColor']}
    ];

    // Set the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';
    config.scayt_autoStartup = true;
    config.forcePasteAsPlainText = false;
    config.extraAllowedContent = 'table(*)';
};

CKEDITOR.on( 'dialogDefinition', function( ev )
{
    var editor = ev.editor;
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;

  if ( dialogName == 'table' || dialogName == 'tableProperties' )
  {
      dialogDefinition.minWidth = 150;
      dialogDefinition.minHeight = 100;
      var infoTab = dialogDefinition.getContents('info');

      // Remove unnecessary widgets/elements from the 'Image Info' tab.
      //infoTab.remove( 'txtWidth' );
      infoTab.remove('txtHeight');
      infoTab.remove('txtCaption');
      infoTab.remove('txtSummary');
      //infoTab.remove( 'txtBorder' );

      //infoTab.remove( 'txtCellSpace' );
      //infoTab.remove( 'txtCellPad' );

      infoTab.remove('selHeaders');
      // infoTab.remove( 'cmbAlign' );

      dialogDefinition.onShow = function () {
          $.each($(".cke_dialog label"), function (i, e) {
        if ($(e).html() == "Width")
        {
            var top = $(e).closest(".cke_dialog_ui_text");
            top.find("input").val('100%');
          if (dialogName == 'table')
          {
              top.css('visibility', 'hidden');
          }
        }
        else if ($(e).html() == "Border size")
        {
            var top = $(e).closest(".cke_dialog_ui_text");
            top.find("input").val('1').prop('disabled', true);
            top.css('visibility', 'hidden');
        }
          });

          //TABLE PLUGIN ORIGINAL ONSHOW LISTENER
          // Detect if there's a selected table.
          var selection = editor.getSelection(),
              ranges = selection.getRanges(),
              table;

          var rowsInput = this.getContentElement('info', 'txtRows'),
              colsInput = this.getContentElement('info', 'txtCols'),
              widthInput = this.getContentElement('info', 'txtWidth'),
              heightInput = this.getContentElement('info', 'txtHeight');

          if (dialogName == 'tableProperties') {
              var selected = selection.getSelectedElement();
              if (selected && selected.is('table'))
                  table = selected;
              else if (ranges.length > 0) {
                  // Webkit could report the following range on cell selection (#4948):
                  // <table><tr><td>[&nbsp;</td></tr></table>]
                  if (CKEDITOR.env.webkit)
                      ranges[0].shrink(CKEDITOR.NODE_ELEMENT);

                  table = editor.elementPath(ranges[0].getCommonAncestor(true)).contains('table', 1);
              }

              // Save a reference to the selected table, and push a new set of default values.
              this._.selectedElement = table;
          }

          // Enable or disable the row, cols, width fields.
          if (table) {
              this.setupContent(table);
              rowsInput && rowsInput.disable();
              colsInput && colsInput.disable();
          } else {
              rowsInput && rowsInput.enable();
              colsInput && colsInput.enable();
          }

          // Call the onChange method for the widht and height fields so
          // they get reflected into the Advanced tab.
          widthInput && widthInput.onChange();
          heightInput && heightInput.onChange();
      };
  }
  else if ( dialogName == 'cellProperties' )
  {
      var infoTab = dialogDefinition.getContents('info');

      infoTab.remove('bgColor');
      infoTab.remove('borderColor');
  }

});