$(function()
{

	$.FroalaEditor.DEFAULTS.zIndex = 2000;

	$.FroalaEditor.DEFAULTS.heightMin = 100;
	$.FroalaEditor.DEFAULTS.toolbarSticky = false;
	$.FroalaEditor.DEFAULTS.pastePlain = true;
	$.FroalaEditor.DEFAULTS.wordPasteModal = false;
	// $.FroalaEditor.DEFAULTS.toolbarStickyOffset = 40;
	$.FroalaEditor.DEFAULTS.toolbarButtons = [ "bold", "italic", "underline", "strikeThrough", "subscript", "superscript", "|", "fontSize", "color", "paragraphStyle", "fullscreen", "|", "paragraphFormat", "align", "formatOL", "formatUL", "outdent", "indent", "quote", "insertLink", "insertTable", "insertImage", "insertVideo", "|", "specialCharacters", "insertHR", "selectAll", "clearFormatting", "|", "spellChecker", "|", "undo", "redo", "dataItems" ];
	$.FroalaEditor.DEFAULTS.tableResizerOffset = 10;
	$.FroalaEditor.DEFAULTS.tableResizingLimit = 50;
	$.FroalaEditor.DEFAULTS.htmlAllowedAttrs = [ 'style', 'class', 'colspan', 'href', 'target' ];
	$.FroalaEditor.DEFAULTS.videoUpload = false;
	$.FroalaEditor.DEFAULTS.imageInsertButtons = ['imageBack', '|', 'imageUpload', 'imageByURL'];
	$.FroalaEditor.DEFAULTS.tableStyles = {
		'non-tableStyle' : 'No Formatting',
		'tableStyle0' : 'Table Style 1',
		'tableStyle1' : 'Table Style 2',
		'tableStyle2' : 'Table Style 3',
		'tableStyle3' : 'Table Style 4',
		'tableStyle4' : 'Table Style 5',
		'tableStyle5' : 'Table Style 6'
	};


	$.FroalaEditor.DefineIconTemplate('text fr-selection', '<span style="text-align: center;">[NAME]</span>');

	$.FroalaEditor.DefineIcon('dataItems', {
		NAME : 'Data Items',
		template : 'text fr-selection'
	});

	var dataItems = '';

	$.ajax({
		type : 'GET',
		url : '/api/library/dataitems',
		data : {
			module : 'EMAIL_TEMPLATES'
		}
	}).done(function(data, textStatus, jqXHR)
	{
		$.each(data, function(key, value)
		{
			dataItems += '<h1 role="presentation">' + key + '</h1>'
			$.each(value, function(key, value)
			{
				dataItems += '<ul class="fr-dropdown-list" role="presentation"><li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="dataItems" data-param1="' + value.replace(/\"/g, '&#34;') + '" title="' + key + '" aria-selected="false">' + key + '</a></li></ul>';
			});
		});

		// $.ajax({
		// 	type : 'GET',
		// 	url : '/api/library/customfields'
		// }).done(function(data, textStatus, jqXHR)
		// {
		// 	if (Object.keys(data).length > 0) {
		// 		dataItems += '<h1 role="presentation">Custom Fields</h1>';
		// 		dataItems += '<ul class="fr-dropdown-list" role="presentation">';
		// 		$.each(data, function(key, value)
		// 		{
		// 			dataItems += '<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="dataItems" data-param1="' + value + '" title="' + key + '" aria-selected="false">' + key + '</a></li>';
		// 		});
		// 		dataItems += '</ul>';
		// 	}
		// });

	});
	$.FroalaEditor.RegisterCommand('dataItems', {
		title : 'Insert data items',
		icon : 'dataItems',
		type : 'dropdown',
		focus : false,
		undo : false,
		refreshAfterCallback : true,

		html : function()
		{
			// data-items dropdown
			return dataItems;
		},

		callback : function(cmd, val)
		{
			this.html.insert(val);
			this.undo.saveStep();
		},
		// Callback on refresh.
		refresh : function($btn)
		{
			console.log('do refresh');
		},
		// Callback on dropdown show.
		refreshOnShow : function($btn, $dropdown)
		{
			console.log('do refresh when show');
		}
	});

});

function customizeFroalaEditor(e, editor)
{
	editor.toolbar.hide();

	editor.events.on('focus', function(e)
	{
		this.toolbar.show();
	});
	editor.events.on('blur', function(e)
	{
		this.toolbar.hide();
	});
	editor.events.on('froalaEditor.tableResize', function(e)
	{
		console.log('table resize')
	});
	editor.events.on('froalaEditor.table-resize', function(e)
	{
		console.log('resize')
	});
}

function setDataItems()
{
	// data-items dropdown
	var dataItems = '';

	$.ajax({
		type : 'GET',
		url : '/api/library/dataitems'
	}).done(function(data, textStatus, jqXHR)
	{
		$.each(data, function(key, value)
		{
			dataItems += '<h1 role="presentation">' + key + '</h1>'
			$.each(value, function(key, value)
			{
				dataItems += '<li role="presentation"><a class="fr-command" tabindex="-1" role="option" data-cmd="dataItems" data-param1="' + value + '" title="' + key + '" aria-selected="false">"' + key + '"</a></li>';
			});
		});
		return dataItems;
	});
}
