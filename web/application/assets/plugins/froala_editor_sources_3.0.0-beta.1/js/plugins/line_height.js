/*!
        * Froala Editor v3.0.0-beta.1 (https://www.froala.com/wysiwyg-editor)
        * Copyright 2014-2019 Froala Labs
        * Licensed under Froala Editor Terms (https://www.froala.com/wysiwyg-editor/terms)
        */
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(require('froala-editor')) :
  typeof define === 'function' && define.amd ? define(['froala-editor'], factory) :
  (factory(global.FroalaEditor));
}(this, (function (FE) { 'use strict';

  FE = FE && FE.hasOwnProperty('default') ? FE['default'] : FE;

  Object.assign(FE.DEFAULTS, {
    lineHeights: {
      Default: '',
      Single: '1',
      '1.15': '1.15',
      '1.5': '1.5',
      Double: '2'
    }
  });

  FE.PLUGINS.lineHeight = function (editor) {
    var $ = editor.$;
    /**
     * Apply style.
     */

    function apply(val) {
      editor.selection.save();
      editor.html.wrap(true, true, true, true);
      editor.selection.restore();
      var blocks = editor.selection.blocks(); // Save selection to restore it later.

      editor.selection.save();

      for (var i = 0; i < blocks.length; i++) {
        $(blocks[i]).css('line-height', val);

        if ($(blocks[i]).attr('style') === '') {
          $(blocks[i]).removeAttr('style');
        }
      } // Unwrap temp divs.


      editor.html.unwrap(); // Restore selection.

      editor.selection.restore();
    }

    function refreshOnShow($btn, $dropdown) {
      var blocks = editor.selection.blocks();

      if (blocks.length) {
        var $blk = $(blocks[0]);
        $dropdown.find('.fr-command').each(function () {
          var lineH = $(this).data('param1');
          var active = ($blk.attr('style') || '').indexOf('line-height: ' + lineH + ';') >= 0;
          $(this).toggleClass('fr-active', active).attr('aria-selected', active);
        });
      }
    }

    function _init() {}

    return {
      _init: _init,
      apply: apply,
      refreshOnShow: refreshOnShow
    };
  }; // Register the font size command.


  FE.RegisterCommand('lineHeight', {
    type: 'dropdown',
    html: function html() {
      var c = '<ul class="fr-dropdown-list" role="presentation">';
      var options = this.opts.lineHeights;

      for (var val in options) {
        if (options.hasOwnProperty(val)) {
          c += '<li role="presentation"><a class="fr-command ' + val + '" tabIndex="-1" role="option" data-cmd="lineHeight" data-param1="' + options[val] + '" title="' + this.language.translate(val) + '">' + this.language.translate(val) + '</a></li>';
        }
      }

      c += '</ul>';
      return c;
    },
    title: 'Line Height',
    callback: function callback(cmd, val) {
      this.lineHeight.apply(val);
    },
    refreshOnShow: function refreshOnShow($btn, $dropdown) {
      this.lineHeight.refreshOnShow($btn, $dropdown);
    },
    plugin: 'lineHeight'
  }); // Add the font size icon.

  FE.DefineIcon('lineHeight', {
    NAME: 'arrows-v',
    FA5NAME: 'arrows-alt-v',
    SVG_KEY: 'lineHeight'
  });

})));
//# sourceMappingURL=line_height.js.map
