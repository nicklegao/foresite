/*!
        * Froala Editor v3.0.0-beta.1 (https://www.froala.com/wysiwyg-editor)
        * Copyright 2014-2019 Froala Labs
        * Licensed under Froala Editor Terms (https://www.froala.com/wysiwyg-editor/terms)
        */
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(require('froala-editor')) :
  typeof define === 'function' && define.amd ? define(['froala-editor'], factory) :
  (factory(global.FroalaEditor));
}(this, (function (FE) { 'use strict';

  FE = FE && FE.hasOwnProperty('default') ? FE['default'] : FE;

  Object.assign(FE.DEFAULTS, {
    inlineStyles: {
      'Big Red': 'font-size: 20px; color: red;',
      'Small Blue': 'font-size: 14px; color: blue;'
    }
  });

  FE.PLUGINS.inlineStyle = function (editor) {
    function apply(val) {
      if (editor.selection.text() !== '') {
        var splits = val.split(';');

        for (var i = 0; i < splits.length; i++) {
          var new_split = splits[i].split(':');

          if (splits[i].length && new_split.length == 2) {
            editor.format.applyStyle(new_split[0].trim(), new_split[1].trim());
          }
        }
      } else {
        editor.html.insert('<span style="' + val + '">' + FE.INVISIBLE_SPACE + FE.MARKERS + '</span>');
      }
    }

    return {
      apply: apply
    };
  }; // Register the inline style command.


  FE.RegisterCommand('inlineStyle', {
    type: 'dropdown',
    html: function html() {
      var c = '<ul class="fr-dropdown-list" role="presentation">';
      var options = this.opts.inlineStyles;

      for (var val in options) {
        if (options.hasOwnProperty(val)) {
          var inlineStyle = options[val] + (options[val].indexOf('display:block;') === -1 ? ' display:block;' : '');
          c += '<li role="presentation"><span style="' + inlineStyle + '" role="presentation"><a class="fr-command" tabIndex="-1" role="option" data-cmd="inlineStyle" data-param1="' + options[val] + '" title="' + this.language.translate(val) + '">' + this.language.translate(val) + '</a></span></li>';
        }
      }

      c += '</ul>';
      return c;
    },
    title: 'Inline Style',
    callback: function callback(cmd, val) {
      this.inlineStyle.apply(val);
    },
    plugin: 'inlineStyle'
  }); // Add the font size icon.

  FE.DefineIcon('inlineStyle', {
    NAME: 'paint-brush',
    SVG_KEY: 'inlineStyle'
  });

})));
//# sourceMappingURL=inline_style.js.map
