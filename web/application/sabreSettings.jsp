<!DOCTYPE html>
<!--[if lt IE 9]><html class="bad-ie" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<%@page import="au.com.ci.sbe.util.UrlUtils" %>
<html lang="en">
<!--<![endif]-->
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="false" compressJavaScript="false" compressCss="true">
    <head>
        <script>

        </script>

        <title>TravelDocs - Dashboard</title>
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="shortcut icon" type="image/x-icon" href="/application/assets/new/images/favicon.ico" />

        <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700italic,700,900,900italic" rel="stylesheet">
        <link rel="stylesheet" href="/application/assets/plugins/bootstrap-modal/css/bootstrap-modal.css">
        <link rel="stylesheet" href="/application/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css">
        <link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/font-awesome.min.css" />
        <link type="text/css" rel="stylesheet" href="/application/assets/new/icons/fuse-icon-font/style.css">
        <link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/all.css">

        <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/plugins/table.css",request) %>" />
        <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/froala_style.min.css",request) %>" />
        <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/froala_editor.pkgd.min.css",request) %>" />

        <!-- STYLESHEETS -->
        <style type="text/css">
            .content-wrapper {
                height: 100%;
            }
            .content.custom-scrollbar {
                height: 100%;
            }
            [fuse-cloak],
            .fuse-cloak {
                display: none !important;
            }
            body {
                background-color: white !important;
            }
            .fr-toolbar i {
                font-size: 14px;
            }
        </style>
        <!-- Icons.css -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/icons/fuse-icon-font/style.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/fontawesome-pro-5.8.1-web/css/all.css" />
        <!-- Animate.css -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/animate.css/animate.min.css">
        <!-- PNotify -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/pnotify/pnotify.custom.min.css">
        <!-- Nvd3 - D3 Charts -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/nvd3/build/nv.d3.min.css" />
        <!-- Fuse Html -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/fuse-html/fuse-html.min.css" />
        <!-- Swiper -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/swiper/css/swiper.min.css" />
        <!-- Datatable -->
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css" />
        <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.material.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.2/css/fixedHeader.dataTables.min.css">
        <!-- Main CSS -->
        <link type="text/css" rel="stylesheet" href="/application/assets/new/css/main.css">
        <link type="text/css" rel="stylesheet" href="/application/assets/new/css/sabreSettings.css">
        
        <link type="text/css" rel="stylesheet" href="/application/assets/new/css/dashboard.css">
		<link rel="stylesheet" href="/application/assets/plugins/sweetalert/sweetalert.css" />

            <%--<link type="text/css" rel="stylesheet" href="/application/assets/materialize/css/materialize.min.css"  media="screen,projection"/>--%>
        <!-- / STYLESHEETS -->

        <!-- JAVASCRIPT -->
        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
        <script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.3.min.js"></script>
        <!-- Mobile Detect -->
        <script type="text/javascript" src="/application/assets/new/vendor/mobile-detect/mobile-detect.min.js"></script>
        <!-- Popper.js -->
        <script type="text/javascript" src="/application/assets/new/vendor/popper.js/index.js"></script>
        <!-- Bootstrap -->
        <script type="text/javascript" src="/application/assets/new/vendor/bootstrap/bootstrap.min.js"></script>
        <!-- Nvd3 - D3 Charts -->
        <script type="text/javascript" src="/application/assets/new/vendor/d3/d3.min.js"></script>
        <script type="text/javascript" src="/application/assets/new/vendor/nvd3/build/nv.d3.min.js"></script>
        <!-- Data tables -->
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.material.min.js"></script>  <%--/application/assets/new/vendor/datatables.net/js/jquery.dataTables.min.js--%>
            <%--<script type="text/javascript" src="/application/assets/new/vendor/datatables-responsive/js/dataTables.responsive.js"></script>--%>
        <!-- PNotify -->
        <script type="text/javascript" src="/application/assets/new/vendor/pnotify/pnotify.custom.min.js"></script>
        <!-- Fuse Html -->
        <script type="text/javascript" src="/application/assets/new/vendor/fuse-html/fuse-html.min.js"></script>
        <!-- Main JS -->
        <script type="text/javascript" src="/application/assets/new/js/main.js"></script>
        <script type="text/javascript" src="/application/js/randomIDGenerator.js"></script>

        <!-- / JAVASCRIPT -->

        <link rel="stylesheet" href="/application/assets/plugins/switchery/switchery.css" />
        <link rel="stylesheet" href="/application/assets/plugins/paper-collapse/paper-collapse.min.css" />
        <script src="/application/assets/plugins/switchery/switchery.js"></script>
        <script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>


    </head>

    <body class="layout layout-vertical layout-left-navigation layout-below-toolbar">
    <main>
        <div id="wrapper">

            <jsp:include page="/application/include/common-side-menu.jsp"></jsp:include>

            <div class="content-wrapper">

                <jsp:include page="/application/include/common-top-menu.jsp"></jsp:include>

                <div class="content-wrapper">
                    <div class="content custom-scrollbar">

                        <div id="settings" class="page-layout simple right-sidebar">
                            <aside class="page-sidebar custom-scrollbar" data-fuse-bar="file-manager-info-sidebar" data-fuse-bar-position="right" data-fuse-bar-media-step="lg">
                                <!-- SIDEBAR HEADER -->
                                <div class="header bg-secondary text-auto d-flex flex-column justify-content-between p-6">

                                    <!-- INFO -->
                                    <div>

                                        <div class="title ">System Settings</div>

                                       <!--  <div class="subtitle text-muted">
                                            <span>Queues #</span>
                                            2
                                        </div> -->

                                    </div>
                                    <!-- / INFO-->

                                </div>
                                <!-- / SIDEBAR HEADER -->

                                <!-- SIDENAV CONTENT -->
                                <div class="content">

                                    <ul class="nav flex-column custom-scrollbar" id="sidenav" data-children=".nav-item">

                                        <li class="subheader">
                                            <span>Options</span>
                                        </li>
                                        <li class="nav-item">
                                            <a class="settingsMenu nav-link ripple active" href="#" id="O" data-url="sabreQueues.jsp">

                                                <i class="fas fa-list-ul" style="font-size: 18px;padding: 3px 0px 0px 8px;"></i>

                                                <span>Integrations</span>
                                            </a>
<%--                                            <a class="settingsMenu nav-link ripple" href="#" id="O" data-url="settingsCompany.jsp">--%>

<%--                                                <i class="fas fa-cogs" style="font-size: 18px;padding: 3px 0px 0px 8px;"></i>--%>

<%--                                                <span>System Settings</span>--%>
<%--                                            </a>--%>
                                            <a class="settingsMenu nav-link ripple" href="#" id="O" data-url="accountSettings.jsp">

                                                <i class="fas fa-money-check" style="font-size: 18px;padding: 3px 0px 0px 8px;"></i>

                                                <span>Payment Settings</span>
                                            </a>
                                            <a class="settingsMenu nav-link ripple" href="#" id="O" data-url="showInvoices.jsp">

                                                <i class="fas fa-money-bill" style="font-size: 18px;padding: 3px 0px 0px 8px;"></i>

                                                <span>Invoices</span>
                                            </a>

                                            <a class="settingsMenu nav-link ripple" href="#" id="O" data-url="emailSettings.jsp">

                                                <i class="fas fa-envelope" style="font-size: 18px;padding: 3px 0px 0px 8px;"></i>

                                                <span>Email Settings</span>
                                            </a>

                                            <a class="settingsMenu nav-link ripple" href="#" id="O" data-url="systemSettings.jsp">

                                                <i class="fas fa-sliders-v-square" style="font-size: 18px;padding: 3px 0px 0px 8px;"></i>

                                                <span>System Settings</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- / SIDENAV CONTENT -->
                            </aside>
                            <div class="page-content-wrapper custom-scrollbar" style="overflow-y: auto">

                                <!-- CONTENT -->
                                <div class="page-content custom-scrollbar" id="settingsContentArea" style="padding-bottom: 80px;">
                                        <%--SETTINGS CONTENT GOES HERE--%>
                                </div>
                                <!-- / CONTENT -->
                            </div>
                        </div>

                    </div>
                </div>
                <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
                    <div class="list-group" class="date">

                        <div class="list-group-item subheader">TODAY</div>

                        <div class="list-group-item two-line">

                            <div class="text-muted">

                                <div class="h1"> Friday</div>

                                <div class="h2 row no-gutters align-items-start">
                                    <span> 4</span>
                                    <span class="h6">th</span>
                                    <span> Apr</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="list-group">

                        <div class="list-group-item subheader">Events</div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Group Meeting</h3>
                                <p>In 32 Minutes, Room 1B</p>
                            </div>
                        </div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Public Beta Release</h3>
                                <p>11:00 PM</p>
                            </div>
                        </div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Dinner with David</h3>
                                <p>17:30 PM</p>
                            </div>
                        </div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Q&amp;A Session</h3>
                                <p>20:30 PM</p>
                            </div>
                        </div>

                    </div>

                    <div class="divider"></div>

                    <div class="list-group">

                        <div class="list-group-item subheader">Notes</div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Best songs to listen while working</h3>
                                <p>Last edit: May 8th, 2015</p>
                            </div>
                        </div>

                        <div class="list-group-item two-line">

                            <div class="list-item-content">
                                <h3>Useful subreddits</h3>
                                <p>Last edit: January 12th, 2015</p>
                            </div>
                        </div>

                    </div>

                    <div class="divider"></div>

                    <div class="list-group">

                        <div class="list-group-item subheader">Quick Settings</div>

                        <div class="list-group-item">

                            <div class="list-item-content">
                                <h3>Notifications</h3>
                            </div>

                            <div class="secondary-container">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" />
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </div>

                        </div>

                        <div class="list-group-item">

                            <div class="list-item-content">
                                <h3>Cloud Sync</h3>
                            </div>

                            <div class="secondary-container">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" />
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </div>

                        </div>

                        <div class="list-group-item">

                            <div class="list-item-content">
                                <h3>Retro Thrusters</h3>
                            </div>

                            <div class="secondary-container">

                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" />
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <div id="edit-management-modal" class="modal top-modal fade" tabindex="-1"></div>
    <div id="migrations-modal" class="modal top-modal fade" tabindex="-1"></div>


    <!-- Bootstrap core JavaScript
      ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<%=UrlUtils.autocachedUrl("/application/js/log4javascript.js", request) %>"></script>
        <%--<script src="<%=UrlUtils.autocachedUrl("/application/js/logging.js", request) %>"></script>--%>
    <script src="<%=UrlUtils.autocachedUrl("/application/js/utils.js", request) %>"></script>

    <script type="text/javascript" src="/application/assets/new/js/apps/dashboard/project.js"></script>

    <script src="/application/assets/plugins/froala_editor_3.0.0/js/froala_editor.pkgd.min.js"></script>
    <!-- Fuse Html -->
    <script src="/application/assets/plugins/jquery-placeholder/js/jquery.placeholder.js"></script>
    <script src="/application/assets/plugins/moment/moment.js"></script>
    <script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="/application/assets/plugins/moment-timezone/0.5.10/moment-timezone-with-data.min.js"></script>
    <script type="text/javascript" src="/application/assets/new/vendor/swiper/js/swiper.min.js"></script>
    <script src="/application/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="/application/assets/plugins/DataTables/buttons/js/dataTables.buttons.min.js"></script>
    <script src="/application/assets/plugins/DataTables/dataTables.colReorder.min.js"></script>
    <script src="/application/assets/plugins/DataTables/dataTables.colResize.js"></script>
    <script src="https://cdn.datatables.net/fixedcolumns/3.2.4/js/dataTables.fixedColumns.min.js"></script>
    <script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="/application/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"></script>
    <script src="/application/assets/plugins/timeago/jquery.timeago.js"></script>
    <script src="/application/assets/plugins/dynatable.0.3.1/jquery.dynatable.js"></script>
    <script src="/application/assets/plugins/dropzone/downloads/dropzone.min.js"></script>
    <script src="/application/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="/application/assets/plugins/bootstrap-datepicker/js/date.format.js"></script>
    <script src="/application/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="/application/assets/plugins/nprogress/nprogress.js"></script>
    <script src="/application/assets/js/ui-modals.js"></script>
    <script src="/application/assets/plugins/Chart.js/Chart.min.js"></script>
    <script src="/application/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/application/assets/plugins/paper-collapse/paper-collapse.min.js"></script>
    <script src="/application/assets/plugins/ui.multiselect.js"></script>


    <script src="<%=UrlUtils.autocachedUrl("/application/js/loading.js", request) %>"></script>

    <script src="/application/js/sabreSettings.js"></script>
    <script src="/application/js/integrations.js"></script>


        <%--<script type="text/javascript" src="/application/assets/materialize/js/materialize.min.js"></script>--%>
        <%--MAIN DASH JS--%>
    <script>
    </script>
    </body>
</compress:html>
</html>