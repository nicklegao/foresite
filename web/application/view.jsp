<!DOCTYPE html>
<%@page import="java.util.Map"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Content"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Section"%>
<%@ page import="org.owasp.esapi.ESAPI" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsDataAccess" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.Constants" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.util.TravelDocsStatus" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.json.proposal.Proposal" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.service.SecurityKeyService" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess" %>
<%@ page import="au.net.webdirector.common.datalayer.client.CategoryData" %>
<%@ page import="au.net.webdirector.common.Defaults" %>
<%@ page import="au.net.webdirector.common.datalayer.client.ElementData" %>
<%@ page import="au.com.ci.sbe.util.UrlUtils" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.utils.DateFormatUtils" %>
<%@ page import="au.corporateinteractive.qcloud.proposalbuilder.db.TravelDocsStylesDataAccess" %>
<%@ page import="java.util.Locale" %>
<%@ page import="au.com.ci.system.classfinder.StringUtil" %><%--
  Created by IntelliJ IDEA.
  User: coreybaines
  Date: 10/5/18
  Time: 10:12 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html;
charset=utf-8" pageEncoding="utf-8" isELIgnored="false" %>
<%
    String travelDocsID = request.getParameter("traveldoc");
    String vKey = request.getParameter("v-key");
    String email = request.getParameter("email");
    UserAccountDataAccess uda = new UserAccountDataAccess();
    String userUsername = (String)session.getAttribute(Constants.PORTAL_USERNAME_ATTR);

    Hashtable<String, String> traveldocHT = null;
    String portalUserId = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
    String registeredAccount = (String)session.getAttribute(Constants.PORTAL_USER_ID_ATTR);
    CategoryData companyData = uda.getUserCompanyForUserId(portalUserId);


    boolean isTemplate = false;
    boolean isItineraryProposal = true;

    if(travelDocsID==null){
        traveldocHT = new Hashtable<String, String>();
        traveldocHT.put(TravelDocsDataAccess.E_OWNER, companyData.getId());
        travelDocsID = "0";
        isTemplate = true;
        isItineraryProposal = false;

%>

<script type="text/javascript">
    var addTemplate = true;
    var isTemplate = <%=isTemplate%>;
</script>
<%
}
else {
    traveldocHT = new TravelDocsDataAccess().getTravelDocById(travelDocsID);
    isTemplate = traveldocHT.get(TravelDocsDataAccess.E_PROPOSAL_STATUS).equals(TravelDocsStatus.Template.getDbText());
    Proposal proposal = new TravelDocsDataAccess().loadTravelDoc(travelDocsID);
    if (StringUtil.isNullOrEmpty(portalUserId)) {
        portalUserId = traveldocHT.get("ATTRDROP_ownerId");
    }
    isItineraryProposal = proposal.getBooking() != null;
    if (StringUtil.isNullOrEmpty(vKey)) {
        vKey = new SecurityKeyService().getViewKey(
                (String)session.getAttribute(Constants.PORTAL_COMPANY_ID_ATTR),
                Integer.parseInt(travelDocsID),
                traveldocHT.get(TravelDocsDataAccess.E_KEY));
    }

%>
<script type="text/javascript">
    var addTemplate = false;
    var isTemplate = <%=isTemplate%>;
</script>
<%
	    Map<String, String> styleSettings = null;
		for(Section section : proposal.getSections()) {
			if(StringUtils.equalsIgnoreCase(section.getType(), "cover")) {
				for(Content content : section.getContent()) {
					if(StringUtils.equalsIgnoreCase(content.getType(), "cover")) {
						styleSettings = content.getCoverData().getMetadata();
						break;
					}
				}
				break;
			}
		}
		
		if(styleSettings != null) {
			%>
			<style type="text/css">
				.editor-content {
					font-family:"<%=styleSettings.get("fontFamilyBody")%>" !important;
					font-size:<%=styleSettings.get("fontSizeBody")%>px !important;
					color:<%=styleSettings.get("fontColorBody")%> !important;
				}
				.editor-content p{
					font-size:<%=styleSettings.get("fontSizeBody")%>px;
				}
				.editor-content ol, .editor-content ul{
					font-size:<%=styleSettings.get("fontSizeBody")%>px;
				}
				.editor-content h1 {
					font-family:"<%=styleSettings.get("fontFamilyH1")%>" !important;
					font-size:<%=styleSettings.get("fontSizeH1")%>px !important;
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("customHeading1Color"))){%>color:<%=styleSettings.get("fontColorHeading1")%> !important;<%}%>
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("boldHeading1"))){%>font-weight:bold !important;<%}%>
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("uppercaseHeading1"))){%>text-transform:uppercase !important;<%}%>
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("underlineHeading1"))){%>text-decoration:underline !important;<%}%>
				}
				.editor-content .segment .segment-heading .icon, .zerorisk-container .accordion .card .card-header button, .zerorisk-container .accordion .card .card-header .fa-chevron-down {
					font-size:<%=styleSettings.get("fontSizeH1")%>px !important;
				}
				
				.zerorisk-container .accordion .card .card-header .flag {
					height:26px !important;
					width:26px !important;
				}
				.zerorisk-container .accordion .card .card-header .flag + h1{
					<%if(StringUtils.isNotBlank(styleSettings.get("fontSizeH1"))){%>margin-left:<%=(Double.valueOf(styleSettings.get("fontSizeH1"))+20)%>px;<%}%>
				}
				
				.editor-content h2 {
					font-family:"<%=styleSettings.get("fontFamilyH2")%>" !important;
					font-size:<%=styleSettings.get("fontSizeH2")%>px !important;
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("customHeading2Color"))){%>color:<%=styleSettings.get("fontColorHeading2")%> !important;<%}%>
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("boldHeading2"))){%>font-weight:bold !important;<%}%>
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("uppercaseHeading2"))){%>text-transform:uppercase !important;<%}%>
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("underlineHeading2"))){%>text-decoration:underline !important;<%}%>
				}
				.editor-content h3 {
					font-family:"<%=styleSettings.get("fontFamilyH3")%>" !important;
					font-size:<%=styleSettings.get("fontSizeH3")%>px !important;
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("customHeading3Color"))){%>color:<%=styleSettings.get("fontColorHeading3")%> !important;<%}%>
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("boldHeading3"))){%>font-weight:bold !important;<%}%>
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("uppercaseHeading3"))){%>text-transform:uppercase !important;<%}%>
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("underlineHeading3"))){%>text-decoration:underline !important;<%}%>
				}
				.editor-content .section-head {
					font-family:"<%=styleSettings.get("fontFamilySection")%>" !important;
					font-size:<%=styleSettings.get("fontSizeSection")%>px !important;
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("customSectionColor"))){%>color:<%=styleSettings.get("fontColorSectionTitle")%> !important;<%}%>
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("boldSectionTitle"))){%>font-weight:bold !important;<%}%>
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("uppercaseSectionTitle"))){%>text-transform:uppercase !important;<%}%>
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("underlineSectionTitle"))){%>text-decoration:underline !important;<%}%>
				}
				 #itineraryContent .segment .segment-detail .segment-table .table-heading, #itineraryContent .desc .booking-desc .desc-text .desc-lbl {
			    	font-family:"<%=styleSettings.get("fontFamilyDL")%>" !important;
					font-size:<%=styleSettings.get("fontSizeDL")%>px !important;
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("customDLColor"))){%>color:<%=styleSettings.get("fontColorDL")%> !important;<%}%>
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("boldDL"))){%>font-weight:bold !important;<%}%>
					<%if(StringUtils.equalsIgnoreCase("true", styleSettings.get("uppercaseDL"))){%>text-transform:uppercase !important;<%}%>
			    }
			</style>
			<%
		}
    }

    CategoryData company = uda.getUserCompanyForUserId(portalUserId);
    Hashtable<String, String> user = uda.getUserWithId(portalUserId);

    String corporateCSS = String.format("/%s/%s/Categories/%s/gen/css-document/edit-colours.css",
            Defaults.getInstance().getStoreContext(),
            UserAccountDataAccess.USER_MODULE,
            company.getId());
                TravelDocsStylesDataAccess tds = new TravelDocsStylesDataAccess();
                CategoryData companyStyles = tds.getCompanyStylesWithId(company.getId());

%>

<script type="text/javascript">
    var ConName = "<%=user.get(UserAccountDataAccess.E_NAME)%> <%=user.get(UserAccountDataAccess.E_SURNAME)%>";
    var ConEmail = "<%=user.get(UserAccountDataAccess.E_EMAIL_ADDRESS)%>";
    var ConPhone = "<%=user.get(UserAccountDataAccess.E_MOBILE_NUMBER)%>";
    var ConCompany = "<%=company.getName()%>";
    var dateFormatPattern = '<%= DateFormatUtils.getInstance(companyStyles.getString(TravelDocsStylesDataAccess.C_DATE_FORMAT), Locale.forLanguageTag(company.getString(UserAccountDataAccess.C1_LOCALE))).getJsPattern() %>';
</script>
<html>
<head>

    <title>TravelDocs - Viewer</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="shortcut icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon" />

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700italic,700,900,900italic" rel="stylesheet">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
	
    <!-- STYLESHEETS -->
    <style type="text/css">
        [fuse-cloak],
        .fuse-cloak {
            display: none !important;
        }
    </style>
    <link rel="stylesheet" href="/stores/_fonts/stylesheet.css" type="text/css"/>
    <!-- Icons.css -->
    <link type="text/css" rel="stylesheet" href="/application/assets/new/icons/fuse-icon-font/style.css">
    <!-- Animate.css -->
    <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/animate.css/animate.min.css">
    <!-- PNotify -->
    <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/pnotify/pnotify.custom.min.css">
    <!-- Nvd3 - D3 Charts -->
    <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/nvd3/build/nv.d3.min.css" />
    <!-- Fuse Html -->
    <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/fuse-html/fuse-html.min.css" />
    <!-- Swiper -->
    <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/swiper/css/swiper.min.css" />
    <!-- Datatable -->
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.material.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.2/css/fixedHeader.dataTables.min.css">
    <%--MODAL BOOTSTRAP--%>
    <link rel="stylesheet" href="/application/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" type="text/css"/>
    <link rel="stylesheet" href="/application/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" type="text/css"/>
    <!-- Main CSS -->
    <link type="text/css" rel="stylesheet" href="/application/assets/new/css/main.css">
    <link type="text/css" rel="stylesheet" href="/application/assets/new/css/itinerary.css">
    <link type="text/css" rel="stylesheet" href="/application/assets/new/css/viewer.css">
    <link type="text/css" rel="stylesheet" href="/application/assets/new/css/flags.css">
    <link type="text/css" rel="stylesheet" href="/application/assets/new/css/coverpage.css">
    <link rel="stylesheet" href="/application/assets/plugins/switchery/switchery.css" />
    <link rel='stylesheet' href='/application/assets/plugins/spectrum/spectrum.css' />
    <link rel='stylesheet' href='/application/assets/new/css/zeroRisk.css' />
    <!-- / STYLESHEETS -->

    <!-- JAVASCRIPT -->
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
    <script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.3.min.js"></script>
    <!-- Mobile Detect -->
    <script type="text/javascript" src="/application/assets/new/vendor/mobile-detect/mobile-detect.min.js"></script>
    <!-- Popper.js -->
    <script type="text/javascript" src="/application/assets/new/vendor/popper.js/index.js"></script>
    <!-- Bootstrap -->
    <script type="text/javascript" src="/application/assets/new/vendor/bootstrap/bootstrap.min.js"></script>
    <!-- Nvd3 - D3 Charts -->
    <script type="text/javascript" src="/application/assets/new/vendor/d3/d3.min.js"></script>
    <script type="text/javascript" src="/application/assets/new/vendor/nvd3/build/nv.d3.min.js"></script>
    <!-- Data tables -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.material.min.js"></script>  <%--/application/assets/new/vendor/datatables.net/js/jquery.dataTables.min.js--%>
    <%--<script type="text/javascript" src="/application/assets/new/vendor/datatables-responsive/js/dataTables.responsive.js"></script>--%>
    <!-- PNotify -->
    <script type="text/javascript" src="/application/assets/new/vendor/pnotify/pnotify.custom.min.js"></script>
    <!-- Fuse Html -->
    <script type="text/javascript" src="/application/assets/new/vendor/fuse-html/fuse-html.min.js"></script>
    <!-- Main JS -->
    <script type="text/javascript" src="/application/assets/new/js/main.js"></script>
    <!-- Interact.js -->
    <script src="https://unpkg.com/interactjs@1.3.3/dist/interact.min.js"></script>

    <%
        if (!StringUtil.isNullOrEmpty(registeredAccount)) {
    %>
    <!-- Include Editor style... -->
    <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/plugins/table.css",request) %>" />
	<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/froala_style.min.css",request) %>" />
	<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala_editor_3.0.0/css/froala_editor.pkgd.min.css",request) %>" />
    <%-- <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala-editor/css/plugins/table.css",request) %>" />
    <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala-editor/css/froala_style.min.css",request) %>" />
    <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/assets/plugins/froala-editor/css/froala_editor.pkgd.min.css",request) %>" /> --%>

    <!-- Include JS file... -->
    <!-- <script src="/application/assets/plugins/froala-editor/js/froala_editor.pkgd.min.js"></script> -->
    <script src="/application/assets/plugins/froala_editor_3.0.0/js/froala_editor.pkgd.min.js"></script>
    <script src="/application/assets/plugins/froala_editor_old/custom/config-proposalFreetext.js"></script>

    <%
        }
    %>
    <%--<script src="/application/assets/plugins/froala-editor/js/plugins/table.js"></script>--%>
    <%--<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.1/js/froala_editor.min.js'></script>--%>
    <link type="text/css" rel="stylesheet" href="/application/assets/new/vendor/fontawesome-pro-5.8.1-web/css/all.css">
    <!-- / JAVASCRIPT -->
    <script>
        var travelDocID = <%=travelDocsID%>;
        var vKey = "";
        var ownerID = <%=portalUserId%>;
        var previewEmail = "<%= !StringUtil.isNullOrEmpty(email) ? email : "null" %>";
    </script>
</head>

<!-- start: MAIN CONTAINER -->
<input type="hidden" id="param-proposal-id" value="<%=ESAPI.encoder().encodeForHTMLAttribute(travelDocsID) %>"/>
<input type="hidden" id="param-v-key" value="<%=ESAPI.encoder().encodeForHTMLAttribute(request.getParameter("v-key")) %>"/>
<input type="hidden" id="doc-v-key" value="<%=ESAPI.encoder().encodeForHTMLAttribute(vKey) %>"/>
<input type="hidden" id="proposal-template" value="<%=isTemplate %>"/>
<body>

<div class="editor-header">


    <div id="logo-group">
        <span id="logo" class="logo-text">
            <!-- <img src="/application/images/qCloud.png" alt="QuoteCloud"> -->
            <span>Td</span>
        </span>
<%--        <h3><span class="documentTag header-company" tag="companyName"><%=company.getName()%></span></h3>--%>
    </div>
    <div class="main-editor-buttons">
        <% if(!isTemplate && !StringUtil.isNullOrEmpty(registeredAccount)) { %>
        <button type="button" class="btn btn-default button-green doResendProposal" data-id="<%=travelDocsID%>">
            <i class="fa fa-paper-plane"></i> &nbsp;Send
        </button>
        <% } %>
        <button type="button" class="btn btn-light-grey btn-close" id="closeViewerButton">
            <i class="fa fa-times"></i> &nbsp;Close
        </button>
    </div>
</div>
<div class="froala-tool-holder" style="display: none;top:50px;min-height: 30px;position: absolute;width: calc(100% - 604px);left: 228px;z-index: 1002;background: #fff;color: #43454e;box-shadow: 0 2px 3px rgba(0, 0, 0, 0.15);">
</div>

<div class="editor-section-panel">


    <div class="section-tab-area">
        <ul id="document-section-tabs" class="nav nav-tabs tab-bricky">

        </ul>
    </div>


</div>

<div class="editor-options-panel">
    <div class="editor-blocks-holder">
        <div class="nav-panel">
            <ul class="nav-panel-set">
                <% if(!isTemplate) { %>
                <li class="nav-panel-icon" id="download-nav-icon">
                    <a href="/pdf/<%=ESAPI.encoder().encodeForURL(travelDocsID) %>.pdf?traveldoc=<%=ESAPI.encoder().encodeForURL(travelDocsID) %>&email=<%=ESAPI.encoder().encodeForURL(request.getParameter("email")) %>&v-key=<%=ESAPI.encoder().encodeForURL(request.getParameter("v-key")) %>" target="_blank" class=" nav-icon" id="download-nav-icon-button">
                        <i class="fas fa-download fa-3x"></i>
                        <span class="nav-icon-title">Download</span>
                    </a>
                </li>
                <% } %>
            </ul>
        </div>
    </div>
</div>


<div class="editor-content window-min-height panel-padding-sections panel-padding-blocks">
    <div class="editor-content-scroller">
        <div class="doc-effect-holder">
            <div class="blosck-droppable-area">
                <div class="doc-page-area">

                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="send-proposal-modal" tabindex="-1" role="dialog" style="display: none;width: 650px" aria-hidden="true"></div>


<jsp:include page="dialog/backgroundLoading.jsp"></jsp:include>

<script src="/webdirector/free/csrf-token/init"></script>
<script src="/application/assets/plugins/moment/moment.js"></script>
<script src="/application/assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="/application/assets/plugins/jquery-placeholder/js/jquery.placeholder.js"></script>
<script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
<script src="/application/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script src="/application/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"></script>
<script src="/application/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/application/assets/plugins/bootstrap-datepicker/js/date.format.js"></script>
<script src="/application/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/application/assets/plugins/nprogress/nprogress.js"></script>
<script src="/application/assets/js/ui-modals.js"></script>
<script src='/application/assets/plugins/spectrum/spectrum.js'></script>
<script src="/application/assets/plugins/switchery/switchery.js"></script>
<script src="/application/assets/plugins/lz-string/lz-string.js"></script>
<script src="/application/js/md5.js"></script>


<script src="<%=UrlUtils.autocachedUrl("/application/js/log4javascript.js", request) %>"></script>
<script src="<%=UrlUtils.autocachedUrl("/application/js/logging.js", request) %>"></script>
<script src="<%=UrlUtils.autocachedUrl("/application/js/loading.js", request) %>"></script>
<script type="text/javascript" src="/application/assets/new/js/viewer.js"></script>
<script type="text/javascript" src="/application/js/content-modules/modules.js"></script>
<script>

    $(document).ready(function() {
        $(document).on('click', '#closeViewerButton', function(e) {
            e.preventDefault();
            if (isTemplate) {
                window.history.back();
            } else {
                window.top.close();
            }

        });
    });

</script>
</body>
</html>


