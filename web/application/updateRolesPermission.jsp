<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.transaction.TUserRolesDataAccess"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.model.UserPermissions"%>
<%@page import="java.util.Arrays"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.entity.MultiOptions"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess"%>
<%@page import="au.net.webdirector.common.utils.module.AssetService"%>
<%@page import="au.net.webdirector.common.datatypes.service.DataTypesService"%>
<%@page import="au.net.webdirector.common.datatypes.domain.DataType"%>
<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page import="au.net.webdirector.admin.modules.DynamicModuleController"%>
<%@page import="webdirector.db.UserUtils"%>
<%@page import="au.net.webdirector.admin.modules.privileges.CategoryPrivileges"%>
<%@page import="au.net.webdirector.admin.modules.privileges.PrivilegeUtils"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.net.webdirector.common.datalayer.client.TransactionModuleAccess"%>
<%@page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.TransactionDataAccess"%>
<%@page import="webdirector.db.client.ManagedDataAccess"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.List"%>
<%@page import="au.net.webdirector.common.datalayer.admin.db.DataAccess"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
	TransactionDataAccess tda = TransactionDataAccess.getInstance(false);
	TransactionManager tm = TransactionManager.getInstance(tda);
	TransactionModuleAccess tma = TransactionModuleAccess.getInstance(tm);
	ModuleAccess ma = ModuleAccess.getInstance();
	DataAccess da = DataAccess.getInstance();
	%>
	<%
	String module = "ROLES";
	String userModule="USERS";

	CategoryData args = new CategoryData();
	args.setFolderLevel(1);
	args.setParentId("0");
	List<CategoryData> companies = ma.getCategories(userModule, args);
		
	for(CategoryData company : companies){
		%>
		<div>---------------------------------------------------------------------</div>
		<div><%=company.getName() %></div>
		<%
		CategoryData cat = new CategoryData();
		cat.setFolderLevel(1);
		cat.setParentId("0");
		cat.setLive(true);
		cat.setName(company.getName());
		cat.put("attrdrop_companyId", company.getId());
		
		cat = tma.insertCategory(cat, module);		
		

		String publicCategoryId = ma.getCategoryByName(module, "Default").getId();

		List<ElementData> elementsUnderDefaultCategory = ma.getElementsByParentId(module, publicCategoryId);
		for (ElementData elementToCopyFrom : elementsUnderDefaultCategory)
		{
			List<DataType> columns = (new DataTypesService()).loadGenericDataTypes(module, false, "");
			new AssetService().copyAsset(elementToCopyFrom, cat.getId(), module, tma, columns, false);
			tm.commit();
		}
		
		List<Hashtable<String, String>> wdusers = da.select("SELECT "
				+"u.Element_id "
				+"FROM "
				+"elements_useraccounts u "
				+"JOIN "
				+"categories_users c2 on c2.Category_id = u.Category_id "
				+"JOIN  "
				+"categories_users c1 on c2.Category_Parentid = c1.Category_id "
				+"WHERE "
				+"c1.Category_id = ? ",
				new String[]{company.getId()},
				HashtableMapper.getInstance());
		for(Hashtable<String, String> u : wdusers){
			ElementData user = ma.getElementById(userModule, u.get("Element_id"));
			%>
			<div><%=user.getName() %></div>
			<div><%=user.getString("ATTR_name") %></div>
			<div><%=user.getString("ATTRDROP_userType") %></div>
			<%
			List<ElementData> roles = ma.getElementsByParentId("ROLES", cat.getId());
			Hashtable<String, String> htroles = new Hashtable<String, String>();
			for(ElementData role : roles){
				htroles.put(role.getName(), role.getId());
			}
			MultiOptions mo;
			if(user.getString("attrdrop_usertype").equals("admin")){
				mo = new MultiOptions(user.getId(),UserAccountDataAccess.E_USER_ROLES,userModule, user.getType(),Arrays.asList(htroles.get("Administrator")));
			}else{
				mo = new MultiOptions(user.getId(),UserAccountDataAccess.E_USER_ROLES,userModule, user.getType(),Arrays.asList(htroles.get("Consultant")));
			}
			user.put(UserAccountDataAccess.E_USER_ROLES, mo);
			tma.updateElement(user, userModule);
			tm.commit();
			
		}

	}	
	%>
	
	<%--
	List<ElementData> list = ma.getElements(userModule, new ElementData());
	UserAccountDataAccess uada = new UserAccountDataAccess();
	for(ElementData user : list){
		String companyId = uada.getUserCompanyIdForUserId(user.getId());
		CategoryData company = ma.getCategoryById(userModule, companyId);
		CategoryData rcompany = new CategoryData();
		rcompany.put("attrdrop_companyid", companyId);
		rcompany = ma.getCategory("ROLES", rcompany);
		List<ElementData> roles = ma.getElementsByParentId("ROLES", rcompany.getId());
		Hashtable<String, String> htroles = new Hashtable<String, String>();
		for(ElementData role : roles){
			htroles.put(role.getString("attr_headline"), role.getId());
		}
		MultiOptions mo;
		if(user.getString("attrdrop_usertype").equals("admin")){
			mo = new MultiOptions(user.getId(),UserAccountDataAccess.E_USER_ROLES,userModule, user.getType(),Arrays.asList(htroles.get("Administrator")));
		}else{
			mo = new MultiOptions(user.getId(),UserAccountDataAccess.E_USER_ROLES,userModule, user.getType(),Arrays.asList(htroles.get("Consultant")));
		}
		user.put(UserAccountDataAccess.E_USER_ROLES, mo);
		tma.updateElement(user, userModule);
		tm.commit();
	}
	--%>
</body>
</html>