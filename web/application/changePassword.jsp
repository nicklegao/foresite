<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%
String userId = (String) session.getAttribute(Constants.PORTAL_CHANGE_PASSWORD_ATTR);
if (userId == null)
{
	//response.sendRedirect("/");
	//return;
}
Calendar calendar = GregorianCalendar.getInstance();
int currentYear = calendar.get(calendar.YEAR);
%><!DOCTYPE html>
<!--[if lt IE 9]><html class="bad-ie" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.ClientApplicationProperties"%>
<%@ page import="org.owasp.esapi.ESAPI" %>
<%@ page import="java.util.GregorianCalendar" %>
<%@ page import="java.util.Calendar" %>
<html lang="en">
<!--<![endif]-->
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
<head>
	<title>Change Password</title>
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />


	<link rel="shortcut icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/application/assets/new/images/favicon.ico" type="image/x-icon">

	<link rel="stylesheet" href="/application/assets/plugins/bootstrap-qcloud/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/font-awesome.min.css" />

	<link rel="stylesheet" href="/application/assets/plugins/qcloud-iconfont/style.css" />

	<link rel="stylesheet" href="/application/assets/fonts/style.css" />

	<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/css/login.css", request) %>" type="text/css"/>
	<!--[if IE 7]>
	<link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
	<![endif]-->
</head>
<body class="login">
	<div class="container">
		<div class="row">
			<div class="main-login col-xs-offset-2 col-xs-8">
		
				<!-- start: CHANGE PASSWORD BOX -->
				<div class="box-change-password">
					<div class="box-header"><div class="logo"></div></div>
					<h3>Change Password</h3>
					<div class="alert alert-warning">
						<i class="clip-warning"></i> For security reason, you are requested to change your password. The password requirements are:
						<ul>
							<li>At least 8 characters long</li>
							<li>Contain at least one uppercase letter</li>
							<li>Contain at least one lowercase letter</li>
							<li>Contain at least one digit</li>
						</ul>
					</div>
					<%
					String errorMessage = (String) session.getAttribute(Constants.SESSION_CHANGE_PASSWORD_ERROR_MSG);
					if (errorMessage != null) {
					  session.setAttribute(Constants.SESSION_CHANGE_PASSWORD_ERROR_MSG, null);
					%>
					  <div class="errorMsg">
					  <%=ESAPI.encoder().encodeForHTML(errorMessage) %>
					  </div>
					<%
					}
					%>
					<form class="form-change-password" action="/api/free/changePassword" method="post">
						<div class="errorHandler alert alert-danger" style="display: none;">
							<i class="fa fa-remove-sign"></i> We're missing some information. Please check below.
						</div>
						<fieldset>
						
							<div class="form-group form-actions">
								<span class="input-icon">
									<input type="password" class="form-control password" name="currentPassword" placeholder="Current Password" value="">
									<i class="qc qc-padlock"></i>
								</span>
							</div>
							<div class="form-group form-actions">
								<span class="input-icon">
									<input type="password" class="form-control password" id="newPassword" name="newPassword" placeholder="New Password" value="">
									<i class="qc qc-padlock"></i>
								</span>
							</div>
							<div class="form-group form-actions">
								<span class="input-icon">
									<input type="password" class="form-control password" name="repeatPassword" placeholder="Repeat New Password" value="">
									<i class="qc qc-padlock"></i>
								</span>
							</div>
		
							<div class="form-actions">
								<button type="submit" class="btn btn-success pull-right">Submit <i class="qc qc-next"></i></button>
							</div>
						</fieldset>
					</form>
				</div>
				<!-- end: CHANGE PASSWORD BOX -->
			</div>
					
			<!-- start: COPYRIGHT -->
			<div class="copyright col-xs-offset-2 col-xs-8">
				QuoteCloud version <%=ESAPI.encoder().encodeForHTML(ClientApplicationProperties.sharedInstance().getApplicationVersion()) %> &copy; Copyright QuoteCloud 2015-<%= currentYear %>
			</div>
			<!-- end: COPYRIGHT -->
		</div>
	</div>

	<!--[if lt IE 9]>
	<script src="/application/assets/plugins/respond.js"></script>
	<![endif]-->
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script src="/application/assets/plugins/jquery-placeholder/js/jquery.placeholder.js"></script>
	<script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
	<script src="/application/assets/plugins/bootstrap-qcloud/js/bootstrap.min.js"></script>
	
	<script src="/application/js/three.min.js"></script>
	<script src="/application/js/login.js"></script>
	
<script src="/webdirector/free/csrf-token/init"></script>
	<script>
		jQuery(document).ready(function() {
			Login.initChangePassword();
		});
	</script>
	<jsp:include page="ga-tracking.jsp"></jsp:include>
</body>
</compress:html>
</html>