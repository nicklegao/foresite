<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Woop!</title>
</head>
<body>
<%
session.setAttribute(Constants.PORTAL_ALLOW_SEND_ATTR, Constants.PORTAL_ALLOW_SEND_ATTR);
%>
<script>
	alert("You may send proposals till you logout.");
	window.history.back();
</script>

</body>
</html>