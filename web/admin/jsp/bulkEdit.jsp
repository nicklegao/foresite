<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/strict.dtd"> 
<div class="bulkEditor">
  <h3 class="panelLoadingIndicator" style="text-align:center"><i class="fa fa-spin fa-cog"></i> Preparing bulk editor</h3>
  <!-- stop form submitting by ENTER key, refer to [DC-38] -->
  <form class="dynamic-form" onsubmit="return false;">
    <table class="table table-hover bulkEditorTbl" width="99.9%">
      <thead>
      </thead>
      <tbody>
      </tbody>
    </table>
  </form>
	<script>
	<%=au.net.webdirector.admin.modules.DynamicFormScriptUtil.getAssetFormValidScript((String)request.getAttribute("module"))%>
	<%=au.net.webdirector.admin.modules.DynamicFormScriptUtil.getCategoryFormValidScript((String)request.getAttribute("module"))%>
	</script>

  <div class="button-area row">
    <label class="changeSummary"></label>
    <button type="button" class="btn btn-warning clearDatatable">Reload</button>
    <button type="button" class="btn btn-primary saveDatatable editAction" disabled="disabled">Save</button>
    <% if(!(Boolean)request.getAttribute("recursive")){ %>
    <button type="button" class="btn btn-default backToParentCategory pull-left" style="display:none"><i class="fa fa-level-up fa-rotate-270 fa-lg"></i>&nbsp;&nbsp;Back</button>
    <button type="button" class="btn btn-default insertNewItem pull-left" style="display:none"><i class="fa fa fa-plus-circle fa-lg"></i>&nbsp;&nbsp;Insert</button>
    <% } %>
    <button type="button" class="btn btn-default exportDatatable pull-left" style="display:none">
    	<i class="fa fa-google" aria-hidden="true"></i> Export to Google Sheets
    </button>
    <button type="button" class="btn btn-default cutSelectedItems pull-left" style="display:none"><i class="fa fa fa-scissors fa-lg"></i>&nbsp;&nbsp;Cut</button>
    <button type="button" class="btn btn-default copySelectedItems pull-left" style="display:none"><i class="fa fa fa-files-o fa-lg"></i>&nbsp;&nbsp;Copy</button>
    
    <form id="google-sheets-export-form" action="/webdirector/google-sheets-export" method="post" target="_blank">
    	<textarea class="hidden" name="payload"></textarea>
    </form>
  </div>
</div>
