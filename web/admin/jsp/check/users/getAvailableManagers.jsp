<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.List"%>
<%@page import="au.net.webdirector.common.datalayer.admin.db.DBaccess"%>
<%@page import="webdirector.db.*"%>
<%
String pid = request.getParameter("pid");
String id = request.getParameter("id");
DBaccess db = new DBaccess();
String result = "";
try{
  String cid = db.selectQuerySingleCol("select category_parentId from categories_users where category_id = ?", new String[]{pid}).get(0);
  String query = "select e.element_id from elements_useraccounts e, categories_users c where e.category_id = c.category_id and c.category_parentid = ? and e.element_id <> ?";
  List<String> v = db.selectQuerySingleCol(query, new String[]{cid, id});
  result = StringUtils.join(v, ",");
}catch(Exception e){
    e.printStackTrace();
}
out.clear();
out.print(result);
%>