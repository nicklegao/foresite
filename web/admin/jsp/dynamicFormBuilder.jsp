<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/strict.dtd"> 
<%@page import="webdirector.db.UserUtils"%>
<%@page import="java.util.Vector"%>
<%@page import="au.net.webdirector.admin.security.LoginController"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.mapper.StringMapper"%>
<%@page import="au.net.webdirector.admin.workflow.WorkflowService"%>

<%@page import="java.util.Date"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.List"%>
<%@page import="au.net.webdirector.common.utils.SimpleDateFormat"%>

<%@page import="au.net.webdirector.admin.datalayer.client.Difference"%>
<%@page import="au.net.webdirector.admin.modules.DynamicFormScriptUtil"%>
<%@page import="au.net.webdirector.admin.modules.privileges.CachedCategoryPrivileges"%>
<%@page import="au.net.webdirector.admin.modules.privileges.CategoryPrivileges"%>
<%@page import="au.net.webdirector.admin.modules.privileges.PrivilegeUtils"%>
<%@page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager"%>
<%@page import="au.net.webdirector.admin.version.VersionControlService"%>
<%@page import="au.net.webdirector.common.datalayer.util.text.DefaultDateFormat"%>

<%@page import="au.net.webdirector.admin.workflow.WorkflowAccess"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.DataMap"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleData"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.TransactionDataAccess"%>
<%@page import="au.com.ci.webdirector.user.UserManager"%>
<%@page import="au.net.webdirector.admin.workflow.WorkflowInfo"%>
<%@page import="au.net.webdirector.admin.workflow.WorkflowHelper"%>

<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="org.apache.commons.lang.math.RandomUtils"%>

<%@page import="au.net.webdirector.common.datatypes.domain.DataType"%>
<%@page import="au.net.webdirector.common.datalayer.admin.db.DataAccess"%>

<%@page import="au.net.webdirector.admin.edit.DynamicFormTabs"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@page import="au.net.webdirector.common.datalayer.util.DatabaseValidation"%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" isELIgnored="false" %>
<jsp:useBean id="dataTypes" class="au.net.webdirector.common.datatypes.service.DataTypesService" />
<%
	int rid = RandomUtils.nextInt(99999999);
Defaults d = Defaults.getInstance();
String module = request.getParameter("module");
final String dataId =  request.getParameter("ID");
String parentId = (String) request.getParameter("PID");
String draftId = request.getParameter("DID") == null ? "0" : request.getParameter("DID");
String loadId = dataId;
String tableType = request.getParameter("tableType");
String draft = request.getParameter("draft");
ModuleAccess ma = ModuleAccess.getInstance();
String action = (StringUtils.isBlank(dataId) || StringUtils.equals(dataId, "0")) && (StringUtils.isBlank(draftId) || StringUtils.equals(draftId, "0")) ? "insert" : "update";

String userId = UserManager.getUserId(request);
String [] vals = null;
if (userId != null && !userId.equals("")) {
	Vector v = new UserUtils().getUser(userId);
	vals = (String[]) v.elementAt(0);
}

String access_file_manager = vals != null? vals[8] : "no";
boolean isCategory = StringUtils.equals("Categories", tableType);

boolean workflowStage = StringUtils.equals("true", draft);
boolean isInsertAction = action.equals("insert");
WorkflowHelper workflowHelper = WorkflowHelper.getInstance();
boolean workflowEnabled = workflowHelper.isWorkflowEnabled(module);
WorkflowInfo workflow = StringUtils.isNotBlank(dataId) && !StringUtils.equals(dataId, "0") ? workflowHelper.getStatus(module, tableType, dataId) : workflowHelper.getStatusByDraftId(module, tableType, draftId);


boolean hasDraft = workflow != null;
if(workflowStage){
  ma = WorkflowAccess.getInstance();
  if(workflow == null){
    throw new RuntimeException("Draft not found for ["+ module +", "+ tableType +", "+dataId+"]");
  }else{
    loadId = workflow.getDraftId();
    draftId = workflow.getDraftId();
  }
}

    

    DataMap rawData = new DataMap();
    
    List<DataType> data = null;
    DynamicFormTabs tabs = null;
    if (isInsertAction) {
        data = dataTypes.loadGenericDataTypes(module, isCategory, (String) session.getValue(LoginController.WD_USER));
    } else {
      rawData = isCategory ? ma.getCategoryById(module, loadId) : ma.getElementById(module, loadId);
      if(rawData == null){
        out.clear();
        response.sendRedirect(request.getContextPath() + "/webdirector/widget-error");
        return;
      }
      parentId = ((ModuleData)rawData).getParentId();
      data = dataTypes.loadDataTypesWithContent(module,isCategory,(String) session.getValue(LoginController.WD_USER),rawData);
    }

    int level = 0;
    if(isCategory){
        level = isInsertAction 
      		  ? "0".equals(parentId) 
      				  ? 1 
  					  : ma.getCategoryById(module, parentId).getFolderLevel() + 1 
  			  : ma.getCategoryById(module, loadId).getFolderLevel();
      data = dataTypes.filterOutHiddenDataTypesAndCatLevels(data, String.valueOf(level));
      tabs = new DynamicFormTabs(module, "Categories", String.valueOf(level));
    }else{
      data = dataTypes.filterOutHiddenDataTypes(data);
      tabs = new DynamicFormTabs(module);
    }

    List<String> tabNames = tabs.getOrderedColumnTabNames();
    Map<String, List<Integer>> tabCounts = tabs.getOrderedColumnTabs();
    

    String idForPriviligeCheck = StringUtils.equals("0", dataId) ? parentId : isCategory ? dataId : parentId;
    PrivilegeUtils privilegeUtils = PrivilegeUtils.getCachedInstance(module, session, false);
    CategoryPrivileges priv = privilegeUtils.getPrivilegeForCategory(idForPriviligeCheck);
    boolean canSave = !isInsertAction && priv.isEdit() || isInsertAction && priv.isInsert();
    boolean canDelete = priv.isDelete();
    boolean isApprover = workflowHelper.isDataApprover(module, userId);
    boolean hasVersion = new VersionControlService(request).hasVersion(module, tableType, dataId);
    
    boolean showLoadDraftBtn = workflowEnabled && canSave && !workflowStage && workflow != null && workflow.isDraft();
    
    
    boolean showSaveAsDraftBtn = workflowEnabled && canSave && !isApprover ;
    boolean showSaveAndSubmitBtn = workflowEnabled && canSave && !isApprover;
    
    boolean showReviewBtn = workflowEnabled && canSave && !workflowStage && workflow != null && !workflow.isDraft();
    
    boolean showApproveBtn = workflowEnabled && canSave && isApprover && workflowStage && workflow != null && (workflow.isToBeApproved() || workflow.isToBeDeleted());
    boolean showDeclineBtn = workflowEnabled && canSave && isApprover && workflowStage && workflow != null && (workflow.isToBeApproved() || workflow.isToBeDeleted());
    
    boolean showSaveDirectBtn = canSave && (!workflowEnabled || isApprover);
    boolean showDeleteBtn = !isInsertAction && canDelete && (!workflowStage || !workflowEnabled || isApprover);
    //boolean showDiscardBtn = canDelete && !showDeleteBtn;
    boolean showHistoryBtn = !workflowStage && hasVersion;
    
 	// Get name of draft/changes author, get diff
 	String workflowUser = "";
 	List<Difference> diff = null;
 	boolean currentUserIsDraftAuthor = false;
 	Date draftEditDate = new Date(0);

 	if (hasDraft) {
 		// get draft author's name
 		List al = DataAccess.getInstance().select("select name from users where user_id = ?", new Object[]{ workflow.getWho() }, new StringMapper());
 		if (al.size() > 0){
 			workflowUser = (String) al.get(0);
 		}
 		diff = new WorkflowService().compare(module, tableType, workflow.getDraftId());
 		
 		draftEditDate = workflow.getWhen();
 		
 		// if current user accesses asset containing their own draft
 		if (userId.equals(workflow.getWho()) && !workflowStage)  
 			currentUserIsDraftAuthor = true;
 		else if (userId.equals(workflow.getWho()) && workflowStage)
 			showDeleteBtn = true;
 	}
 	
 	// Display striped background for drafts
 	String draftBackground = "";
 	if (workflowStage)
 		draftBackground = "background: repeating-linear-gradient( 135deg, white, white 15px, #f4f4f4 15px, #F4F4F4 20px );";
 		
%>
<html>
  <script>
  <%=au.net.webdirector.admin.modules.DynamicFormScriptUtil.getAssetFormValidScript(module)%>
  <%=au.net.webdirector.admin.modules.DynamicFormScriptUtil.getCategoryFormValidScript(module)%>
  
  <% if(!access_file_manager.equalsIgnoreCase("yes")) { %>
  $(function(){
    $('.removeSharedFileAnchor').remove();
    $('.open-sharedfile-manager').remove();
  });
  <% } %>
  <%
  if(!canSave){
  %>
  $(function(){
    setTimeout(function(){
      $("#myTabContent<%= rid %> input, #myTabContent<%= rid %> select, #myTabContent<%= rid %> textarea").prop("disabled", true);
      $("#myFormFooter<%= rid %> input, #myFormFooter<%= rid %> select").prop("disabled", true);
      $("#myTabContent<%= rid %> .open-editor").each(function(){
        var $this = $(this);
        var url = '/admin/jsp/a_readATTRLONG.jsp?cache'+(+new Date)+'&srcFile=' + $this.data('src');
        // var url = '/stores' + $this.data('src');
        var $div = $('<div>').addClass('content-box');
        $div.load(url, function(){
          $this.parent().empty().append($div);
        })
      });
      $("#myTabContent<%= rid %> .input-group button").remove();
      $("#myTabContent<%= rid %> .input-multidrop").show().find('option:not(:checked)').remove();
       
    }, 300);
  });
  <%  
  }
  %>
  </script>
 
<form ENCTYPE="multipart/form-data" method="POST" name="formBuilder" class="form-horizontal dynamic-form" style="<%= draftBackground %>">
  
<% if(workflowStage){ %>
<div class="alert alert-info">
  This draft is created/modified by  @<%= workflowUser %>.
  <strong style="float: right;">DRAFT</strong>
</div>  
<% } else if (hasDraft){ %>
<div class="alert alert-warning">
  <strong>Warning!</strong>
  There is a draft created/modified by  @<%= workflowUser %>. &nbsp;
  <% if (showLoadDraftBtn) { %>
        <a class="btn btn-warning load-btn" href="javascript:void(0);" style="color: white;">Load Draft</a>
  <% } 
  if (showReviewBtn) { %>
        <a class="btn btn-warning load-btn" href="javascript:void(0);" style="color: white;">Review</a>
  <% } %>
</div> 
<% } %>  
<div class="dynamic-form-container" data-priv="<%=priv%>">
  <ul class="nav nav-tabs bordered" id="myTab<%= rid %>">
    <% for (int i = 0;i<tabNames.size();i++){ %>
    <li class="<%= (i==0) ? "active" : ""%>">
      <a data-toggle="tab" href="#s_<%= rid %>_<%= i + 1%>"><i class="fa fa-warning text-danger validation-tips"></i> <%= tabNames.get(i) %></a>
    </li>
    <% } %>
  </ul>

	<input type="hidden" name="sendOldDraftInEmail" value="false">
	<input type="hidden" name="currentUserIsDraftAuthor" value="<%= currentUserIsDraftAuthor %>">
	<input type="hidden" name="draftEditDate" value="<%= draftEditDate.getTime() %>">
	<% if (hasDraft) { %>
	<input type="hidden" name="hasDraft" value="true">
	<% } %>
    <input type="hidden" name="action" value="<%=action%>">
    <input type="hidden" name="ID" value="<%=dataId%>">
    <input type="hidden" name="DID" value="<%=draftId%>">
    <input type="hidden" name="PID" value="<%=parentId%>">
    <input type="hidden" name="table" value="<%= tableType %>">
    <input type="hidden" name="module" value="<%= module %>">
    <input type="hidden" name="level" value="<%= level %>">
    <input type="hidden" name="status" value="" class="skip-change-check">
    <input type="hidden" name="declineReason" value="">
    <input type="hidden" name="stage" value="<%= StringUtils.equals("true", draft) ? "workflow" : "live" %>">
    <div class="tab-content padding-10" id="myTabContent<%= rid %>">
      <% for (int i = 0;i<tabNames.size();i++){ %>
      <div id="s_<%= rid %>_<%= i + 1%>" class="tab-pane fade <%= (i==0) ? "in active" : ""%>">
        <fieldset>
        <% 
        for (int idx : tabCounts.get(tabNames.get(i))){
        	
          DataType col = data.get(idx);
          
       	  // if field is different between live and draft, show color
          String contextualBackground = "";
          if (diff != null) {
        	  for (Difference difference : diff) 
        		  if (difference.getFieldName().equalsIgnoreCase(col.getInternalName()))
        			  contextualBackground = "bg-info";
          }
          %>
          <div class="form-group <%= col.isMandatory() ? "required" : ""%> <%=contextualBackground%>">
            <label class="col-md-2 control-label" for="<%=col.getInternalName()%>">
              <%= col.getExternalName() %>
            </label>
            <div class="col-md-10">
              <div class="input-group col-xs-12">
                <%= col.getMarkup() %>
              </div>
                <% if( (StringUtils.isNotBlank(col.getLabelInfo()) || col.isUnique()) && !col.getTypeName().equals("Notes")) { %>
                <p class="note">
                  <%= col.getLabelInfo() %>
                  <%if(col.isUnique()){%>
                  (Should be UNIQUE)
                  <%}%>
                </p>
                <% } %>
              </div>
            </div>
        <% } %>
        </fieldset>  
      </div>
      <% } %>
    </div>
    <hr>
    <div class="form-footer row show-hide-live " id="myFormFooter<%= rid %>">
      <div class="col-md-2">
        <input type="hidden" value="1" name="elementForm"/>
        <label class="">
          <input type="checkbox" class="checkbox style-0" <%= StringUtils.equals(rawData.getString("Live"), "1") ? "checked" : "" %> name="DUMMY_Live">
          <span>Live</span>
          <input type="hidden" name="Live">
        </label>
      </div>  
      <div class="col-md-5">
        <label class="pos-relative">
          <span>Live Date: </span>
          <input type=text name='Live_date' value='<%= DefaultDateFormat.formatDate(rawData.getDate("Live_date"), "yyyy-MM-dd HH:mm") %>' class="short-input input-datetime form-control input-xs" id="live_date_picker">
        </label>
        <label class="pos-relative">
          <span>Expire Date: </span>
          <input type=text name='Expire_date' value='<%= DefaultDateFormat.formatDate(rawData.getDate("Expire_date"), "yyyy-MM-dd HH:mm") %>' class="short-input input-datetime form-control input-xs" id="expire_date_picker">
        </label>
      </div>
      <div class="col-md-5">
        <label class="create-time">
          Created at: 
          <span><%= rawData.getString("Create_date") %></span>
        </label>
        <label class="update-time">
          Updated at: 
          <span><%= rawData.getString("LAST_UPDATE_DATE") %></span>
        </label>
      </div>
    </div>
    <div class="button-area row">
    <%
      if (showSaveDirectBtn) {
        %>
        <button type="button" class="btn btn-primary save-direct-btn">
          Save
          <i class="fa fa-refresh fa-spin" style="display: none;"></i>
        </button>
        <%
      }
      if (showSaveAsDraftBtn) {
        %>
        <button type="button" class="btn btn-default save-draft-btn">
          Save as Draft
          <i class="fa fa-refresh fa-spin" style="display: none;"></i>
        </button>
        <%
      }
      if (showSaveAndSubmitBtn) {
      %>
      <button type="button" class="btn btn-primary save-submit-btn">
        Save and Submit
        <i class="fa fa-refresh fa-spin" style="display: none;"></i>
      </button>
      <%
      }
      if (showApproveBtn) {
      %>
      <button type="button" class="btn btn-primary approve-btn">
        Approve
        <i class="fa fa-refresh fa-spin" style="display: none;"></i>
      </button>
      <%
      }
      if (showDeclineBtn) {
        %>
        <button type="button" class="btn btn-danger decline-btn">
          Decline
          <i class="fa fa-refresh fa-spin" style="display: none;"></i>
        </button>
        <%
      }
      if (showDeleteBtn) {
        %>
        <a class="btn btn-danger delete-btn" href="javascript:void(0);">Delete <%= (workflowStage) ? "Draft" : "" %></a>
        <%
      }
      if (showHistoryBtn) {
        %>
        <a class="btn btn-warning show-history-btn" href="javascript:void(0);">Show History</a>
        <%
      }
    %>
    </div>
  </form>
</div>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/jsp/javaScript/qTip2/jquery.qtip.min.css" />
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/jsp/javaScript/qTip2/jquery.qtip.mod.css" />
<script type="text/javascript" src="<%= request.getContextPath() %>/jsp/javaScript/qTip2/jquery.qtip.min.js"></script -->
</body>
</html>

