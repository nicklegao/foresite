<%@page import="au.net.webdirector.admin.security.LoginController"%>
<%@page import="au.net.webdirector.common.utils.password.PasswordUtils"%>
<%@page import="java.util.*"%>
<%@page import="au.net.webdirector.admin.modules.DynamicModuleController"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<jsp:useBean id="db" class="webdirector.db.UserUtils"/>
<%
String userId = (String) session.getAttribute(LoginController.WD_USER_ID);
String[] vals = null;

if (userId != null && !userId.equals("")) {
  Vector v = db.getUser(userId);
  vals = (String[]) v.elementAt(0);
}

String userType = (vals != null) ? vals[6] : "1";

String admin = "";
String module = "";
String secure = "";
String readOnlyChecked = "";
String dashboard="";
String workbench="";

String default_layout = vals != null ? vals[7] : "";

if(default_layout.equalsIgnoreCase("dashboard")){
	dashboard="checked";
}else if(default_layout.equalsIgnoreCase("workbench")){
	workbench="checked";
} else {
	dashboard = "checked";
}

%>
<style>
#userEditForm .bootstrap-duallistbox-container label {
  margin: 0;
  border: 0;
  font-size: 14px;
}

#userEditForm .bootstrap-duallistbox-container .info-container {
  display: none;
}


</style>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Edit Account</h4>
</div>
<form method="POST" id="userEditForm" class="form-horizontal">
<div class="modal-body">
  <fieldset>
    <div class="form-group required">
      <label class="col-md-2 control-label">Name</label>
      <div class="col-md-8">
        <input type="text" class="form-control" name="name" maxlength="100" value='<%=ESAPI.encoder().encodeForHTMLAttribute(vals != null ? vals[0] : "")%>' required>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label">Company</label>
      <div class="col-md-8">
        <input type="text" class="form-control" name="company" maxlength="50" value='<%=ESAPI.encoder().encodeForHTMLAttribute(vals != null ? vals[1] : "")%>'>
      </div>
    </div>
    <div class="form-group required">
      <label class="col-md-2 control-label">Email</label>
      <div class="col-md-8">
        <input type="text" class="form-control" name="email" maxlength="50" value='<%=ESAPI.encoder().encodeForHTMLAttribute(vals != null ? vals[2] : "")%>' required>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label">Phone Number</label>
      <div class="col-md-3">
        <input type="text" class="form-control" name="phone" maxlength="50" size="50" value='<%=ESAPI.encoder().encodeForHTMLAttribute(vals != null ? vals[3] : "")%>'>
      </div>
    </div>
  </fieldset>
  <hr>
  <fieldset>
    <div class="form-group required">
      <label class="col-md-2 control-label">Login User Name</label>
      <div class="col-md-8">
        <input type="text" class="form-control" name="userName" maxlength="50" value='<%=ESAPI.encoder().encodeForHTMLAttribute(vals != null ? vals[4] : "")%>' required>
      </div>
    </div>
    <!-- 
    <div class="form-group required">
      <label class="col-md-2 control-label">Password</label>
      <div class="col-md-3">
        <input type="password" class="form-control" name="password" maxlength="50" value="<%=PasswordUtils.FAKE_PASSWD%>" required>
      </div>
      <label class="col-md-2 control-label">Repeat Password</label>
      <div class="col-md-3">
        <input type="password" class="form-control" name="password_confirm" maxlength="50" value="<%=PasswordUtils.FAKE_PASSWD%>" required>
      </div>
    </div>    
     -->
    
    <%-- <div class="form-group">
      <label class="col-md-2 control-label">Default Layout</label>
      <div class="col-md-10">
        <label class="radio radio-inline">
          <input type="radio" name="Default_Layout" class="radiobox style-1" value="Dashboard" <%= dashboard %>>
          <span> <i class="fa fa-tachometer"></i> Dashboard</span> 
        </label>
        <label class="radio radio-inline">
          <input type="radio" name="Default_Layout" class="radiobox style-1" value="Workbench" <%= workbench %>>
          <span> <i class="fa fa-archive"></i> Workbench</span>  
        </label>
      </div>
    </div> --%>
    
  </fieldset>
</div>
<div class="modal-footer">
  <button type="submit" class="btn btn-primary">Save</button>
</div>
</form>

<script>
$(function() {
  var $modal = ModalHelper.$modal;

  var $form = $("#userEditForm");

  $form.validate({
    // Rules for form validation
    rules : {
      name : {
        required : true,
      },
      email : {
        required : true,
      },
      userName : {
        required : true
      },
      password : {
        required : true
      },
      password_confirm : {
        required : true,
        equalTo : 'input[name="password"]'
      }
    },

    // Messages for form validation
    messages : {
      password_confirm : {
        equalTo : 'Passwords do not match'
      }
    },

    submitHandler : submitForm,

    // Do not change code below
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }
  });

  function submitForm() {

    $form.find("button").attr("disabled", "disabled");
    $.ajax({
      url : '/webdirector/configureUsers/updateUserNonAdmin',
      data : $form.serializeArray(),
      type : "POST"
    }).done(function(data) {
      $form.find("button").removeAttr("disabled");
      StorageHelper.restoreOpenModules();
      if (data.success) {
        ModalHelper.closeModal();
      } else {
        ModalHelper.showNotification({success: false, content: data.message});
      }
    });
  }

});
</script>