<style>
form .invalid {
	color: red;
}
</style>

<div class="modal-header">
	<h4 class="modal-title">Change Password</h4>
</div>

<div class="modal-body">
	<form id="changePasswordForm">
	  <div class="form-group">
	    <label for="exampleInputEmail1">Current Password</label>
	    <input type="password" class="form-control" name="currentPassword" placeholder="Current Password">
	  </div>
	  <div class="form-group">
	    <label for="exampleInputPassword1">New Password</label>
	    <input type="password" class="form-control" name="newPassword" placeholder="New Password">
	  </div>
	  <div class="form-group">
	    <label for="exampleInputPassword1">Repeat New Password</label>
	    <input type="password" class="form-control" name="repeatNewPassword" placeholder="New Password Again">
	  </div>
	  <button type="submit" class="btn btn-default">Submit</button>
	</form>
</div>

<script>
	$("form#changePasswordForm").validate({
		rules: {
			currentPassword: "required",
			newPassword: {
				required: true,
				minlength: 6,
				maxlength: 30
			},
			repeatNewPassword: {
				required: true,
				equalTo: '[name="newPassword"]'
			}
	
		}
	});

	$("form#changePasswordForm").submit(function(e) {
		e.preventDefault();
		var $form = $(this);
			
		if (!$form.valid())
			return false;
		
		var currentPassword = $('[name="currentPassword"]', $form).val();
		var newPassword = $('[name="newPassword"]', $form).val();
		var repeatNewPassword = $('[name="repeatNewPassword"]', $form).val();
		
		$.ajax({
		      url : '/webdirector/configureUsers/changePassword',
		      data : {
		    	  currentPassword: currentPassword,
		    	  newPassword: newPassword,
		    	  repeatNewPassword: repeatNewPassword
		      }
		    }).done(function(data) {
		      if (data.success) {
			    ModalHelper.showNotification({success: true, content: data.message});
		      } else {
		        ModalHelper.showNotification({success: false, content: data.message});
		      }
		    });
	});
</script>