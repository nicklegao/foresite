<%
String srcFile = request.getParameter("srcFile");
%>
<!doctype html>
<html>
  <body>
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        &times;
      </button>
      <h4 class="modal-title">Delete Content</h4>
    </div>
    <div class="modal-body">
      <p>Are you sure?</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">
        Cancel
      </button>
      <button type="button" class="btn btn-danger" onclick="deleteTemplate();">
        Delete
      </button>
    </div> 
    <script type="text/javascript">
      function deleteTemplate(){
        $.post('/webdirector/template/delete?srcFile=<%=srcFile%>', function(data){
          $('.modal-body').html('<p>Content has been deleted!</p>');
          $('.modal-footer').hide();
          setTimeout(function(){
	      	$('#myModal').modal('hide');
          }, 2000);
        })
      }
    </script>
  </body>
</html>
