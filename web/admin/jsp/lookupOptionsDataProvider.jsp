<%@page import="java.util.Arrays"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="au.net.webdirector.common.datalayer.util.DatabaseValidation"%>
<%@page import="au.net.webdirector.common.datalayer.admin.db.DBaccess"%>
<%@page import="au.net.webdirector.common.datatypes.domain.DropDownData"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Vector"%>
<%@page import="au.net.webdirector.common.utils.module.DropDownBuilder"%>
<%
	String module = request.getParameter("module");
	String field = request.getParameter("field");
	String user = request.getParameter("user");
	String tableType = request.getParameter("tableType");
	String value = request.getParameter("search_text");
	int numberPerPage = 10;
	int maxRows = 100;
	int pageNumber = 1;
	if(request.getParameter("pageNumber")!= null){
		pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
	}
	if(value == null || value.equalsIgnoreCase("filter")){
		value = "";
	}
	
	List<DropDownData> list = new DropDownBuilder().getDropDownDatas(module, field, user, tableType);
	if(list.size() > 0){
		DropDownData data = list.get(0);
		Vector<String[]> columnNames = data.getColumnName();
		int[] showColumnNumbers = data.getShownColumns(request);
		Cookie p = new Cookie("dropdownlookup-"+user+"-"+module+"-"+field, request.getParameter("showCols"));
		p.setPath(request.getContextPath() + "/webdirector/lookup-options");
		p.setMaxAge(60*60*24*365);
		response.addCookie(p);
%>

<div id="demo" onselectstart="return false" style="overflow-x:auto;">
<table class="display table table-striped" id="datatable">
	<thead>
		<tr>
			<th style="display:none;">Stored Value</th>
		<%
		String heading = "Shown Text";
// 		String sql = "select Label_ExternalName from Labels_" + module + " where Label_OnOff = 1 and Label_InternalName = '"+ field +"'";
// 		Vector v = new DBaccess().select(sql, new String[]{"name"});

		String sql = "select Label_ExternalName from Labels_" + DatabaseValidation.encodeParam(module) + " where Label_OnOff = 1 and Label_InternalName = ?";
		Vector v = new DBaccess().selectQuery(sql, new String[]{field});

		if(v!=null && v.size()>0){
			heading = ((String[])v.get(0))[0];
		}
		%>
			<th><%= heading %></th>
		<%
		for(int columnIdx:showColumnNumbers){%>
			<th>
				<table><thead><tr><td style="padding:0;vertical-align:middle">
					<%= columnNames.get(columnIdx)[1] %><input type="hidden" name="show_<%= columnNames.get(columnIdx)[0] %>" value="1">
				</td><td style="padding-left:1px;vertical-align:middle">	
                    <button style="margin-top:-4px" class="close" type="button" onclick="shutOffCol(<%=columnIdx%>)">&times;</button>
				</td></tr></thead></table>
					</th>
		<%}%>	
		</tr>
	</thead>
<%
		list = filterList(list, showColumnNumbers, value);
		if(list.size()>0){	 
%>	
	<tbody>
<%	
		for(int i=(pageNumber-1)*numberPerPage;i<list.size() && i<pageNumber*numberPerPage;i++){
			DropDownData ddd = list.get(i);
			out.println(ddd.printTable(showColumnNumbers));
		}
%>
	</tbody>
	<%}%>
</table>
</div>
<br>
<div class="dataTables_info" id="datatable_info" style="color: rgb(17, 17, 17);">Showing <%= (pageNumber-1)*numberPerPage+1 %> to <%= Math.min(pageNumber*numberPerPage, list.size()) %> of <%= list.size() %> entries</div>
<div class="dataTables_paginate paging_two_button" id="datatable_paginate">
<a role="button" tabindex="0" class="paginate_<%= pageNumber==1?"disabled":"enabled" %>_previous" id="datatable_previous" aria-controls="datatable" <%= pageNumber==1?"":"onclick='goPage(-1)'" %>>Previous</a>
<a role="button" tabindex="0" class="paginate_<%= pageNumber*numberPerPage>=list.size()?"disabled":"enabled" %>_next" id="datatable_next" aria-controls="datatable" <%= pageNumber*numberPerPage>=list.size()?"":"onclick='goPage(1)'" %>>Next</a>
</div>
<%	
	}
%>
<%!
public List<DropDownData> filterList(List<DropDownData> orig, int[] showColumnNumbers, String value){
	List<DropDownData> ret = new ArrayList<DropDownData>();
	for(DropDownData ddd: orig){
		if(ddd.contains(showColumnNumbers, value)){
			ret.add(ddd);
		}
	}
	return ret;
}
%>