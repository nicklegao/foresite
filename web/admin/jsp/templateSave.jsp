<%@page import="webdirector.db.client.ClientFileUtils"%>
<%@page import="java.io.File"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<HTML>
<%
  String colName = (String) request.getParameter("colName");
  String formName = (String) request.getParameter("formName");
  String contents = (String) request.getParameter("contents");
  String srcFile = (String) request.getParameter("srcFile");
  String dynForm = (String) request.getParameter("dynForm");
  
  // read this once

  String redirect = request.getHeader("Referer");
%>
  <HEAD>
  </HEAD>
  <body>
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        &times;
      </button>
      <h4 class="modal-title">Save Content as a Template</h4>
    </div>
    <div class="modal-body">
      <form name="template" method="POST" class="form-horizontal" id="saveTemplate" action="/webdirector/template/save">
        <input type="hidden" name="dynForm" value="<%=dynForm%>">
        <input type="hidden" name="referer_page" value="<%=redirect%>">
        <input type="hidden" name="srcFile" value="<%=srcFile%>">
        <fieldset>
          <div class="form-group required">
            <label class="col-md-3 control-label">
              Template Name
            </label>
            <div class="col-md-9">
              <input type="text" data-max-length="40" id="filename" value="" name="filename" class="form-control input-char">
            </div>
          </div>
        </fieldset>
      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">
        Cancel
      </button>
      <button type="button" class="btn btn-primary" onclick="saveTemplate();">
        Save
      </button>
    </div> 
    <script type="text/javascript">
      function saveTemplate(){
        $('#saveTemplate').ajaxSubmit({
          success: function(data, statusText, xhr, $form){
          	if(data){
          	  $('.modal-body').html('<p>Template has been saved!</p>');
              $('.modal-footer').hide();
              setTimeout(function(){
    	      	$('#myModal').modal('hide');
              }, 2000);
          	}
          }
        });
      }
    </script>
  </body>
</html>
