<%@ page isELIgnored="false" %>
<%@page import="sbe.openlinks.quartz.page.quartz.JobAndSimpleTrigger"%>
<%@page import="org.quartz.Trigger"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Jobs</title>
<!-- 
<script type="text/javascript" src="<%= request.getContextPath() %>/admin/js/jquery1.7.js"></script> 
 -->
<script type="text/javascript" src="<%= request.getContextPath() %>/admin/js/tableSorter/jquery.tablesorter.min.js"></script> 
<link href="<%= request.getContextPath() %>/admin/js/tableSorter/themes/blue/style.css" rel="stylesheet" type="text/css">
<script>
jQuery(document).ready(function($) 
    { 
        $("#listJobs").tablesorter({
			  headers: {
					5: {
						 sorter: false 
					} 
			  }
		  }); 
    } 
); 
</script>
<%

String addr =(request.isSecure()?"https://":"http://")+request.getServerName()+":"+request.getServerPort()+request.getContextPath();
%>
<script type="text/javascript">
jQuery(document).ready(function($){
	   $(".actionLink").click(function(event){
	     var txt = $(this).text();
	     var action = "";
	     if(txt=='Resume'){
	    	 action ="<%=addr%>/webdirector/secure/scheduler/resumeJob?jobName="+$(this).attr("id");
	     }
		 if(txt=='Pause'){
			 action ="<%=addr%>/webdirector/secure/scheduler/pauseJob?jobName="+$(this).attr("id");
	     }
		if(txt=='Interrupt'){
			action ="<%=addr%>/webdirector/secure/scheduler/interruptJob?jobName="+$(this).attr("id");
	     }
		if(txt=='Trigger'){
			action ="<%=addr%>/webdirector/secure/scheduler/triggerJob?jobName="+$(this).attr("id");
	     }
		if(txt=='Delete'){
			action ="<%=addr%>/webdirector/secure/scheduler/deleteJob?jobName="+$(this).attr("id");
	     }
		
		$.ajax({
		      type:'POST',
			  url: action,
			  context: document.body,
			  success: function(data){
				  if(data == 'true' && txt=='Resume')
						  window.location.reload();
				  if(data == 'true' && txt=='Pause')
						  window.location.reload();
				  if(data == 'true' && txt=='Interrupt')
						  window.location.reload();
				  if(data == 'true' && txt=='Trigger')
						  window.location.reload();
				  if(data == 'true' && txt=='Delete')
						  window.location.reload();
			  }
			});
		 event.preventDefault();
		 
	   });		  
		$("#refresh").click(function() {
		    location.reload();
		});
	 })
</script>

</head>
<body class="iframe-body">

<button class="btn btn-default" id="refresh">Refresh</button>
<table width="80%" class="listJobs tablesorter table table-striped" id="listJobs">
<thead>
<tr>
	<th style="width: 200px; ">Job Details </th>
	<th style="width: 200px; ">Trigger state </th>
	<th style="width: 250px; ">Repeat</th>
	<th style="width: 200px; ">Interval</th>
	<th style="width: 200px; ">Date Start</th>
	<th style="width: 200px; ">Date End</th>
	<th style="width: 200px; ">Executing</th>
	<th colspan="5" style="text-align: center;">Controls</th>
</tr>
</thead>
<tbody>
<c:forEach var="current" varStatus="index" items="${ jobList }">
<tr class="${idx.index % 2 == 0 ? 'odd' : 'even'}">
	<td width="100px">${ current.job.name}</td>
	<td>
	<%
	JobAndSimpleTrigger j = (JobAndSimpleTrigger ) pageContext.getAttribute("current");
	switch ( j.getTriggerState() ) {
	     case Trigger.STATE_NONE:
	        out.print("<span style='color:red'>None</span>");
	        break;
	     case Trigger.STATE_NORMAL:
	    	 out.print("<span style='color:blue'>Normal</span>");
	    	 break;
	     case Trigger.STATE_PAUSED:
	    	 out.print("<span style='color:red'>Paused</span>");
	    	 break;
	     case Trigger.STATE_BLOCKED:
	    	 out.print("<span style='color:green'>Running</span>");
	    	 break;
	     case Trigger.STATE_COMPLETE:
	    	 out.print("<span style='color:black'>Complete</span>");
	    	 break;
	     case Trigger.STATE_ERROR:
	    	 out.print("<span style='color:red'>Error</span>");
	    	 break;
 		 }
 	%>
	</td>
	<td>${ current.repeat}</td>
	<td>${ current.interval }</td>
	<td>${ current.trigger.startTime}</td>
	<td>${ current.trigger.endTime}</td>
	<td>${ current.executing }</td>
	<td><a href="" id="${ current.job.name}" class="actionLink">Resume</a></td>
	<td><a href="" id="${ current.job.name}" class="actionLink">Pause</a></td>
	<td><a href="" id="${ current.job.name}" class="actionLink">Interrupt</a></td>
	<td><a href="" id="${ current.job.name}" class="actionLink">Trigger</a></td>
	<td><a href="" id="${ current.job.name}" class="actionLink">Delete</a></td>
</tr>
</c:forEach>
</tbody>
</table>
<br/>
<div id="info">

</div>

</body>
</html>