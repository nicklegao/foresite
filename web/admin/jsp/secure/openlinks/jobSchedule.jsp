<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<link href="<%=request.getContextPath() %>/admin/css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<%=request.getContextPath() %>/admin/js/jquery-ui-timepicker-addon.js"></script>
<script>
$(document).ready( function() {
   $('.datepicker').datetimepicker({ dateFormat: 'yy-mm-dd', timeFormat: 'hh:mm' });
	checkHiddenStuff();
	$('#repeatCount').change( function() {
		checkHiddenStuff();
	});
});

function checkHiddenStuff() {
	var elms = $('.onlyRepeat');
	if ($('#repeatCount').val() == 0) {
		$(elms).hide();
	} else {
		$(elms).show();
	}
	var elms = $('.onlyContinuous');
	if ($('#repeatCount').val() == -1) {
		$(elms).show();
	} else {
		$(elms).hide();
	}
	
	var elms = $('.onlyCron');
	if ($('#repeatCount').val() == -2) {
		$(elms).show();
		$('.notCron').hide();
		$('.onlyRepeat').hide();
	} else {
		$(elms).hide();
		$('.notCron').show();
		$('.onlyRepeat').show();
	}
}
</script>
<style>
.form-horizontal .form-group {
  margin-left: 0;
  margin-right: 0;
}
.btn {
  display: block;
  margin: 0 auto;
}
</style>
</head>
<body class="iframe-body">
<% 
  if(null != request.getAttribute("resultInsert")){
  %>
  <script>
  $(function(){
    setTimeout(function(){
      top.showNotification({
        success: <%=request.getAttribute("resultInsert")%>,
        container: '#modal-setting .modal-dialog',
        content: "<%= (Boolean)request.getAttribute("resultInsert") ? "Job Scheduled." : "Error Scheduling Job. " + request.getAttribute("AdditionalInfo") %>"
      }, function(){
      });
    }, 500);
  });
  </script>
  <%    
  }
%>
<form:form name="SchedulObj" modelAttribute="SchedulObj" action="/webdirector/secure/scheduler/jobScheduleSubmit" method="POST" class="form-horizontal">
<div class="form-group">
  <label class="col-sm-2 control-label">Job Name</label>
  <div class="col-sm-10">
    <form:input path="jobDetail.name" maxlength="100" cssClass="form-control input-sm"/>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-2 control-label">Class Name</label>
  <div class="col-sm-10">
    <form:select path="className" cssClass="form-control select-sm">
      <form:options items="${availableJob}" />
    </form:select>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-2 control-label">Repeat Number</label>
  <div class="col-sm-10">
    <form:select path="repeatCount" cssClass="form-control select-sm">
    	<form:option value="-2" >Use Cron Expression</form:option>
      <form:option value="-1" >Run continuously</form:option>
      <form:option value="0" >Run Once</form:option>
      <form:option value="1" >Repeat 1 time</form:option>
      <form:option value="2" >Repeat 2 times</form:option>
      <form:option value="3" >Repeat 3 times</form:option>
      <form:option value="4" >Repeat 4 times</form:option>
      <form:option value="5" >Repeat 5 times</form:option>
      <form:option value="10" >Repeat 10 times</form:option>
      <form:option value="20" >Repeat 20 times</form:option>
    </form:select>
  </div>
</div>
<div class="form-group onlyRepeat">
  <label class="col-sm-2 control-label">Repeat Interval</label>
  <div class="col-sm-10">
    <form:select path="repeatInterval" cssClass="form-control select-sm">
      <form:option value="1000" >1 second</form:option>
      <form:option value="2000" >2 seconds</form:option>
      <form:option value="3000" >3 seconds</form:option>
      <form:option value="4000" >4 seconds</form:option>
      <form:option value="5000" >5 seconds</form:option>
      <form:option value="10000" >10 seconds</form:option>
      <form:option value="15000" >15 seconds</form:option>
      <form:option value="30000" >30 seconds</form:option>
      <form:option value="60000" >1 minute</form:option>
      <form:option value="120000" >2 minutes</form:option>
      <form:option value="300000" >5 minutes</form:option>
      <form:option value="600000" >10 minutes</form:option>
      <form:option value="900000" >15 minutes</form:option>
      <form:option value="1800000" >30 minutes</form:option>
      <form:option value="3600000" >1 hour</form:option>
      <form:option value="7200000" >2 hours</form:option>
      <form:option value="10800000" >3 hours</form:option>
      <form:option value="21600000" >6 hours</form:option>
      <form:option value="43200000" >12 hours</form:option>
      <form:option value="86400000" >24 hours</form:option>
    </form:select>
  </div>
</div>
<div class="form-group onlyCron">
  <label class="col-sm-2 control-label">Cron Expression</label>
  <div class="col-sm-10">
    <form:input path="cronExpression" maxlength="200" cssClass="form-control input-sm"/>
  </div>
</div>
<div class="form-group notCron">
  <label class="col-sm-2 control-label">Start Date</label>
  <div class="col-sm-10">
    <form:input path="startDate" cssClass="datepicker form-control" />
  </div>
</div>
<div class="form-group onlyContinuous">
  <label class="col-sm-2 control-label">End Date</label>
  <div class="col-sm-10">
    <form:input path="endDate" cssClass="datepicker form-control" />
  </div>
</div>
<div class="form-group">
  <label class="col-sm-2 control-label">Context</label>
  <div class="col-sm-10">
    <form:textarea path="context" cssClass="form-control" />
  </div>
</div>
<button type="submit" class="btn btn-primary">Save</button>
</form:form>
</body>
</html>