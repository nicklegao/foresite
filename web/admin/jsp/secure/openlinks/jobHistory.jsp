<%@page import="au.net.webdirector.common.Defaults"%>
<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Job History</title>
<!-- 
<script type="text/javascript" src="/admin/js/jquery1.7.js"></script> 
 -->
<script type="text/javascript" src="/admin/js/tableSorter/jquery.tablesorter.min.js"></script> 
<link href="<%= request.getContextPath() %>/admin/js/tableSorter/themes/blue/style.css" rel="stylesheet" type="text/css">
<script>
$(document).ready(function(){ 
  $("#jobTable").tablesorter({
    headers: {
      0: { 
      	sorter: false 
      }
    }
  });
  
  $('#selectAllDelete').change( function() {
    if ($(this).is(':checked')) {
      $(".selectDelete").prop('checked', true);
    } else {
      $(".selectDelete").prop('checked', false);
    }
  });
  
  $("#deleteSelected").click(function() {
  	var str = "";
  	$(".selectDelete:checked").each( function() {
      str += $(this).attr('data-elid')+",";
  	});
  	str = str.slice(0, -1);
  	if(str!= ''){
      $.ajax({
        url: "/webdirector/secure/scheduler/deleteJoHistoryRecords?elid="+str,
        context: document.body,
        success: function(){
          top.showNotification({
            success: true,
            container: '#modal-setting .modal-dialog',
            content: "Record(s) have been deleted."
          }, function(){
          });
          $(".selectDelete:checked").each( function() {
            var row = $(this).closest('tr');
            $(row).children('td').each(function() {
              $(this).html('<div>'+$(this).html()+'</div>').children('div').slideUp('fast', function() {
                $(row).remove();
              });
            });
          });
        },
        error: function(){
          top.showNotification({
            success: false,
            container: '#modal-setting .modal-dialog',
            content: "Record(s) cannot be deleted."
          }, function(){
          });
        }
      });
  	}
  });		  
  $("#refresh").click(function() {
      location.reload();
  });
} 
); 
</script>
</head>
<body class="iframe-body">
<button class="btn btn-danger" id="deleteSelected">Delete Selected Records</button>&nbsp;&nbsp;
<button class="btn btn-default" id="refresh">Refresh</button>
<div id="result" style="color:green;font-family:sans-serif;padding-left:10px;font-size:0.8em;display:inline-block;"></div>
<table id="jobTable" class="tablesorter table">
<thead>
<tr>
	<th><input type="checkbox" id="selectAllDelete" /></th>
	<th>Seq</th>
	<th width="100px">Job Name </th>
	<th>Start Date </th>
	<th>End Date </th>
	<th>Nodes</th>
	<th>Succeeded </th>
	<th>Failed </th>
	<th>Log File</th>
</tr>
</thead>
<tbody>
<c:forEach var="current" varStatus="idx" items="${ jobList }">
	<tr class="${idx.index % 2 == 0 ? 'odd' : 'even'} hisElm">
		<td><input type="checkbox" id="selectDelete${idx.index}" class="selectDelete" value="" data-elid="${current.elementId}"> </td>
		<td>${current.elementId}</td>
		<td style="max-width:150px; word-wrap: break-word;">${current.jobName }</td>
		<td>${current.startDate }</td>
		<td>${current.endDate }</td>
		<td>${current.numberOfNodes }</td>
		<td>${current.number_Succeed }</td>
		<td>${current.number_Failed }</td>
		<td><a href="/<%= Defaults.getInstance().getStoreContext()%>/temp/${current.logFile }" target="_blank">Log File </a></td>
	</tr>
</c:forEach>
</tbody>
</table>
</body>
</html>