<%@page import="java.util.List"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.net.webdirector.common.datatypes.domain.*"%>
<jsp:useBean id="dataTypes" class="au.net.webdirector.common.datatypes.service.DataTypesService" />
<%
  String moduleName = request.getParameter("moduleName");
  String mode = request.getParameter("mode");
  boolean isCategory = "Category".equalsIgnoreCase(mode) ? true : false;
  
  String label = isCategory ? "categorylabels" : "labels";
  List<DataType> attributes = dataTypes.loadGenericDataTypes(moduleName, isCategory, null);
%>

<style>
.list-header {
  padding-left: 16px;
  padding-right: 16px;
  font-style: italic;
}
</style>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
  </button>
  <h4 class="modal-title" id="myModalLabel">Sort <%=moduleName%> - <%=isCategory?"Category":"Elements"%></h4>
</div>
<div class="modal-body">
  <div class="list-header">
    <div class="row">
      <div class="col-xs-4">
        Attribute Name
      </div>
      <div class="col-xs-4">
        Data Label
      </div>
      <div class="col-xs-4">
        Tab Name
      </div>
    </div>
  </div>
  <ol class="dd-list" id="sortable">
<%
  for (int i = 0; i < attributes.size(); i++) {
    DataType col = attributes.get(i);
    if (!col.isOn()) {
      continue;   
    }
    boolean isSortable = i != 0;    
%>
    <li class='dd-item <%=isSortable ? "" : "not-sortable" %>' id="<%=col.getLabelId()%>">
      <div class="dd-handle">
        <div class="row">
          <div class="col-xs-4">
            <%=ESAPI.encoder().encodeForHTML(col.getInternalName())%>
          </div>
          <div class="col-xs-4">
            <div class="editLabel_area" id="<%=col.getInternalName()%>"><%=ESAPI.encoder().encodeForHTML(col.getExternalName())%></div>
          </div>
          <div class="col-xs-4">
            <div class="editTab_area" id="<%=col.getInternalName()%>"><%=ESAPI.encoder().encodeForHTML(col.getTabName())%></div>
          </div>
        </div>
      <%=isSortable ? "" : "<p class='note pull right'>This attribute cannot be re-ordered</p>" %>
      </div>
    </li>
<% } %>
  </ol>
  <p class="note"><strong>Note:</strong> Drag and drop is available on this page</p>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" id="btnBack">Back</button>
</div>

<script src="/admin/plugins/jquery.multisortable/multisortable.js"></script>
<script src="/admin/plugins/jquery.jeditable/jquery.jeditable.min.js"></script>
<script>
$(function() {
  $("#btnBack").click(function() {
    ModalHelper.loadContent('/webdirector/secure/module/configure?mode=<%=mode%>&moduleName=<%=moduleName%>');
  });
  
  var $sortable = $('#sortable');
  $sortable.multisortable({
	  axis: "y",
	  containment: "parent",
    items: 'li:not(.not-sortable)',
    stop: function(event, ui) {
      ui.item.parent().find('[style]').removeAttr('style');
      saveArrangableNodes();
    }
  });
  
  function saveArrangableNodes() {
    var string = "";
    var result = $sortable.sortable('toArray');
    var size = $sortable.children().length;
    
    for (var no = 0; no < size; no++) {
      var assetId = result[no];
      if (string.length > 0)
        string = string + ',';
      string = string + "lyr" + no + "T" + assetId;
    }
    var orderString = string.replace(/,/g, '|');
    $.ajax({
      url: '/webdirector/labels/updateSortingOrder',
      data: {
        orderString : orderString,
        moduleName : '<%=moduleName%>',
        moduleType : '<%=label%>'
      },
      type: 'POST'
    });
  }
  
  $('.editLabel_area').editable('/webdirector/labels/editLabelExternalName?type=<%=mode.toLowerCase()%>', { 
    indicator : 'Saving...',
    tooltip   : 'Click to edit...',
    id        : 'labelInternalName',
    name      : 'labelExternalName',
    onblur : 'submit',
  });
  $('.editTab_area').editable('/webdirector/labels/editLabelTab?type=<%=mode.toLowerCase()%>', { 
    indicator: 'Saving...',
    tooltip  : 'Click to edit...',
    id       : 'labelInternalName',
    name     : 'tabName',
    onblur : 'submit',
  });
  
});
</script>