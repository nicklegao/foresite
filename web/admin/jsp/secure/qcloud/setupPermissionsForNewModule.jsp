<%@page import="au.net.webdirector.admin.modules.privileges.CategoryPrivileges"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.DataMap"%>
<%@page import="au.net.webdirector.admin.modules.privileges.PrivilegeUtils"%>
<%@page import="au.net.webdirector.common.datalayer.client.TransactionModuleAccess"%>
<%@page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.TransactionDataAccess"%>
<%@page import="java.util.List"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%
//module
String MODULE = "EMAIL_TEMPLATES";

//get all existing companies

TransactionDataAccess da = TransactionDataAccess.getInstance(false);
TransactionManager tm = TransactionManager.getInstance(da);
TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);

int processed = 0;
int skipped = 0;
int permissionsUpdated = 0;

Exception exception = null;
try{
	/*
select c.attr_categoryname, p.user_id
from categories_users c, user_module_privileges p
where c.category_id = p.category_id
and c.folderLevel =1
and p.module_name = 'users'
	*/
	
	List<DataMap> mapping = da.selectMaps(
			"select c.category_id as company_id, c.attr_categoryname as company_name, p.user_id as user_id"
				+" from categories_users c, user_module_privileges p "
				+" where c.category_id = p.category_id "
				+" and c.folderLevel =1 "
				+" and p.module_name = 'users'");
	
	for(DataMap m:mapping){
		String companyID = m.getString("company_id");
		String companyName = m.getString("company_name");
		String userID = m.getString("user_id");
		
		CategoryData conditions = new CategoryData();
		conditions.setName(companyName);
		conditions.setFolderLevel(1);
		CategoryData found = ma.getCategory(MODULE, conditions);
		
		String catID;
		if (found == null)
		{
			CategoryData category = new CategoryData();
			category.setFolderLevel(1);
			category.setParentId("0");
			category.setLive(true);
			category.setName(companyName);
			category.put("attrdrop_companyId", companyID);
			category = ma.insertCategory(category, MODULE);

			catID = category.getId();
			
			processed++;
		}else{
			catID = found.getId();
			
			skipped++;
		}
		
		new PrivilegeUtils(MODULE, userID).setPrivlageOnCategory(tm, CategoryPrivileges.CREATE, catID);
		permissionsUpdated++;

		//
	}

	tm.commit();
	
}catch(Exception e){
	exception = e;
	tm.rollback();
}

//create the companies in the new module
//set permissions in the new module
%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Updating companies for new modules</title>
</head>
<body>
	<h1>Module: <%=MODULE %></h1>
	<h2>Processed: <%=processed %></h2>
	<h2>Skipped: <%=skipped %></h2>
	<h2>Permissions Updated: <%=permissionsUpdated %></h2>
	<% if (exception != null) { %>
	<p>Exception: <%=exception %></p>
	<% } %>
</body>
</html>