<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.WebdirectorDataAccess"%>
<%@page import="au.net.webdirector.admin.modules.privileges.CategoryPrivileges"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.DataMap"%>
<%@page import="au.net.webdirector.admin.modules.privileges.PrivilegeUtils"%>
<%@page import="au.net.webdirector.common.datalayer.client.TransactionModuleAccess"%>
<%@page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.TransactionDataAccess"%>
<%@page import="java.util.List"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleData"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%

//get all existing companies

TransactionDataAccess da = TransactionDataAccess.getInstance(false);
TransactionManager tm = TransactionManager.getInstance(da);
TransactionModuleAccess ma = TransactionModuleAccess.getInstance(tm);

int deleted = 0;
int users = 0;
int modules = 0;
int inserted = 0;
Exception exception = null;
try{
	/*
select c.attr_categoryname, p.user_id
from categories_users c, user_module_privileges p
where c.category_id = p.category_id
and c.folderLevel =1
and p.module_name = 'users'
	*/
	
	
	List<DataMap> moduleNames = da.selectMaps("select id, internal_module_name from sys_moduleconfig");
	Map<String, String> moduleIDMap = new HashMap<String, String>();
	for (DataMap m : moduleNames)
	{
		moduleIDMap.put(m.getString("internal_module_name"), m.getString("id"));
	}
	
	
	List<String> nonSysUsers = da.selectStrings("select user_id from users where userlevel_id = 4");
	users = nonSysUsers.size();
	modules = WebdirectorDataAccess.MODULES_TO_ASSIGN.length;
	
	for (String userID : nonSysUsers)
	{
		deleted += da.update("DELETE FROM sys_usermodules WHERE user_id=?", userID);
		for (String module : WebdirectorDataAccess.MODULES_TO_ASSIGN)
		{
			int success = da.insert("INSERT INTO sys_usermodules (user_id, module_id) VALUES (?, ?)",
					userID,
					moduleIDMap.get(module));
			if (success>0)
			{
				++inserted;
			}
		}
	}
	
	//tm.rollback();

	tm.commit();
	
}catch(Exception e){
	exception = e;
	tm.rollback();
}

//create the companies in the new module
//set permissions in the new module
%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Updating companies for new modules</title>
</head>
<body>
	<h2>Deleted: <%=deleted %></h2>
	<h2>Users: <%=users %></h2>
	<h2>Modules: <%=modules %></h2>
	<h2>Expected: <%=users * modules %></h2>
	<h2>Inserted: <%=inserted %></h2>
	<% if (exception != null) { %>
	<p>Exception: <%=exception %></p>
	<% } %>
</body>
</html>