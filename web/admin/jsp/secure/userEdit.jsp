<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.DataMap"%>
<%@page import="webdirector.db.UserUtils"%>
<%@page import="webdirector.db.UserUtils.UserModel"%>
<%@page import="au.net.webdirector.common.utils.password.PasswordUtils"%>
<%@page import="java.util.*"%>
<%@page import="au.net.webdirector.admin.modules.DynamicModuleController"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<jsp:useBean id="db" class="webdirector.db.UserUtils"/>
<%
String userId = request.getParameter("userId");
Map<String,String> userModules = null;
DataMap userMap = new DataMap();
if (StringUtils.isNotBlank(userId)) {
  userMap = db.getUserDataMapById(userId);
  userModules = DynamicModuleController.sharedInstance().getModulesForUser(userId);
} else {
  userModules = new HashMap<String,String>();
}

String userType = userMap.containsKey("UserLevel_id") ? userMap.getString("UserLevel_id") : "1";

StringBuilder dualListOptions = new StringBuilder();

Map<String,String> allModules = DynamicModuleController.sharedInstance().getModuleNameLookup();
for (String internalModuleName : allModules.keySet()) {
  String moduleName = allModules.get(internalModuleName);
  String selected = userModules.containsKey(internalModuleName) ? "selected='selected'" : "";
  dualListOptions.append("<OPTION value=\""+internalModuleName+"\" " + selected + ">"+moduleName+"</OPTION>\n");
}

%>
<style>
#userEditForm .bootstrap-duallistbox-container label {
  margin: 0;
  border: 0;
  font-size: 14px;
}

#userEditForm .bootstrap-duallistbox-container .info-container {
  display: none;
}


</style>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title"><%=userId == null ? "Add User" : "Edit User" %></h4>
</div>
<form method="POST" id="userEditForm" class="form-horizontal">
<input type="hidden" name="userId" value="<%=ESAPI.encoder().encodeForHTMLAttribute(userId != null ? userId : "")%>">
<input type="hidden" name="moduleSelected">
<div class="modal-body">
  <fieldset>
    <div class="form-group required">
      <label class="col-md-2 control-label">Name</label>
      <div class="col-md-8">
        <input type="text" class="form-control" name="name" maxlength="100" value='<%=ESAPI.encoder().encodeForHTMLAttribute(userMap.getString("Name"))%>' required>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label">Company</label>
      <div class="col-md-8">
        <input type="text" class="form-control" name="company" maxlength="50" value='<%=ESAPI.encoder().encodeForHTMLAttribute(userMap.getString("Company"))%>'>
      </div>
    </div>
    <div class="form-group required">
      <label class="col-md-2 control-label">Email</label>
      <div class="col-md-8">
        <input type="text" class="form-control" name="email" maxlength="50" value='<%=ESAPI.encoder().encodeForHTMLAttribute(userMap.getString("email"))%>' required>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label">Phone Number</label>
      <div class="col-md-3">
        <input type="text" class="form-control" name="phone" maxlength="50" size="50" value='<%=ESAPI.encoder().encodeForHTMLAttribute(userMap.getString("phone"))%>'>
      </div>
    </div>
  </fieldset>
  <hr>
  <fieldset>
    <div class="form-group required">
      <label class="col-md-2 control-label">Login User Name</label>
      <div class="col-md-8">
        <input type="text" class="form-control" name="userName" maxlength="50" value='<%=ESAPI.encoder().encodeForHTMLAttribute(userMap.getString("user_name"))%>' required <%= StringUtils.isNotBlank(userId)?"readonly":"" %>>
      </div>
    </div>
    <div class="form-group required">
      <label class="col-md-2 control-label">Password</label>
      <div class="col-md-3">
        <input type="password" class="form-control" name="password" maxlength="50" value="<%=PasswordUtils.FAKE_PASSWD%>" required>
      </div>
      <label class="col-md-2 control-label">Repeat Password</label>
      <div class="col-md-3">
        <input type="password" class="form-control" name="password_confirm" maxlength="50" value="<%=PasswordUtils.FAKE_PASSWD%>" required>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label">Access Level</label>
      <div class="col-md-10">
        <label class="radio radio-inline">
          <input type="radio" name="User_Type" class="radiobox style-1" value="<%=UserUtils.UserLevel.ADMIN.getRank() %>" <%= String.valueOf(UserUtils.UserLevel.ADMIN.getRank()).equals(userType)?"checked":"" %>>
          <span>Administrator</span> 
        </label>
        <label class="radio radio-inline">
          <input type="radio" name="User_Type" class="radiobox style-1" value="<%=UserUtils.UserLevel.AUTHOR.getRank() %>" <%= String.valueOf(UserUtils.UserLevel.AUTHOR.getRank()).equals(userType)?"checked":"" %>>
          <span>Content Editor</span>  
        </label>
      </div>
    </div>
    
    
    <div class="form-group">
      <label class="col-md-2 control-label">Default Layout</label>
      <div class="col-md-10">
        <label class="radio radio-inline">
          <input type="radio" name="Default_Layout" class="radiobox style-1" value="Dashboard" <%= "Dashboard".equals(userMap.getString("default_layout")) ? "checked" : "" %>>
          <span> <i class="fa fa-tachometer"></i> Dashboard</span> 
        </label>
        <label class="radio radio-inline">
          <input type="radio" name="Default_Layout" class="radiobox style-1" value="Workbench" <%= !"Dashboard".equals(userMap.getString("default_layout")) ? "checked" : "" %>>
          <span> <i class="fa fa-archive"></i> Workbench</span>  
        </label>
      </div>
    </div>
    
    <div class="form-group">
      <label class="col-md-2 control-label">Access File Manager</label>
      <div class="col-md-10">
        <label class="radio radio-inline">
          <input type="radio" name="Access_File_Manager" class="radiobox style-1" value="yes" <%= "yes".equals(userMap.getString("access_file_manager")) ? "checked" : "" %>>
          <span> Yes</span> 
        </label>
        <label class="radio radio-inline">
          <input type="radio" name="Access_File_Manager" class="radiobox style-1" value="no" <%= !"yes".equals(userMap.getString("access_file_manager")) ? "checked" : "" %>>
          <span> No</span>  
        </label>
      </div>
    </div>
    
    <div class="form-group">
      <label class="col-md-2 control-label">Can Change Password</label>
      <div class="col-md-10">
        <label class="radio radio-inline">
          <input type="radio" name="Can_Change_Password" class="radiobox style-1 author_settings" value="yes" <%= "yes".equals(userMap.getString("can_change_password")) ? "checked" : "" %>>
          <span> Yes</span> 
        </label>
        <label class="radio radio-inline">
          <input type="radio" name="Can_Change_Password" class="radiobox style-1 author_settings" value="no" <%= !"yes".equals(userMap.getString("can_change_password")) ? "checked" : "" %>>
          <span> No</span>  
        </label>
      </div>
    </div>
    
    <div class="form-group">
      <label class="col-md-2 control-label">Can Edit Account</label>
      <div class="col-md-10">
        <label class="radio radio-inline">
          <input type="radio" name="Can_Edit_Account" class="radiobox style-1 author_settings" value="yes" <%= "yes".equals(userMap.getString("can_edit_account")) ? "checked" : "" %>>
          <span> Yes</span> 
        </label>
        <label class="radio radio-inline">
          <input type="radio" name="Can_Edit_Account" class="radiobox style-1 author_settings" value="no" <%= !"yes".equals(userMap.getString("can_edit_account")) ? "checked" : "" %>>
          <span> No</span>  
        </label>
      </div>
    </div>
    
  </fieldset>
  <hr>
  <fieldset class="col-md-10 col-md-offset-1">
    <select multiple="multiple" size="10" name="assignedModulesList" id="assignedModulesList" style="visibility: hidden">
      <%=dualListOptions.toString()%>
    </select>
    <% if (userId != null) { %>
    <div class="pull-right">
      <button type="button" class="btn btn-sm btn-info" id="btnAssignCategories" <%="1".equals(userType) ? "disabled='disabled'" : ""%> >Assign Categories</button>
    </div>
    <% } %>
  </fieldset>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" id="btnBack">Back</button>
  <button type="submit" class="btn btn-primary">Save</button>
</div>
</form>

<div class="modal fade" id="assignCategoriesModal">
  <div class="modal-dialog">
    <div class="modal-content" id="assignCategoriesContent">
    </div>
  </div>
</div>
<div class="modal fade" id="assignApproversModal">
  <div class="modal-dialog">
    <div class="modal-content" id="assignApproversContent">
    </div>
  </div>
</div>


<script src="/admin/js/plugin/bootstrap-duallistbox/jquery.bootstrap-duallistbox.js"></script>
<script>
$(function() {
  var $modal = ModalHelper.$modal;
  $modal.find("#btnBack").click(function() {
    ModalHelper.loadContent('/webdirector/secure/user/list');
  });

  var initializeDuallistbox = $('#assignedModulesList').bootstrapDualListbox({
      nonSelectedListLabel: 'Available Modules',
      selectedListLabel: 'Assigned Modules',
      preserveSelectionOnMove: 'moved',
      moveOnSelect: false,
      showFilterInputs: false,
      infoText: false,
    });

  var $form = $("#userEditForm");
  
  $("#btnAssignCategories").click(function() {
    var select = $form[0].assignedModulesList_helper2;
    var moduleName = select.options[select.selectedIndex].value;
    var moduleValue = select.options[select.selectedIndex].value;
    var userType = $form.find('input[name="User_Type"]:checked').val();

    var modal = $("#assignCategoriesModal");
    var content = $("#assignCategoriesContent");
    content.load("/webdirector/secure/user/categories",
    {
      moduleName : moduleName,
      moduleValue : moduleValue,
      userId : "<%=userId%>",
      userType : userType
    },
    function() {
      content.children(".modal-body").css("height", $(window).height() * 0.4);
      modal.modal();
    });
  });

  $form.find('input[name="User_Type"]').click(function() {
    if ($(this).val() == 1) {
      $("#btnAssignCategories").attr("disabled","disabled");
      $form.find('.author_settings').prop('disabled', true);
    } else {
      $("#btnAssignCategories").removeAttr("disabled");
      $form.find('.author_settings').prop('disabled', false);
    }
  });

  $.validator.addMethod("hasContent", function(value, element) {
      return element.length > 0;
  });

  $form.validate({
    // Rules for form validation
    rules : {
      name : {
        required : true,
      },
      email : {
        required : true,
      },
      userName : {
        required : true
      },
      password : {
        required : true
      },
      password_confirm : {
        required : true,
        equalTo : 'input[name="password"]'
      },
      assignedModulesList_helper2 : {
        hasContent : true
      }
    },

    // Messages for form validation
    messages : {
      password_confirm : {
        equalTo : 'Passwords do not match'
      },
      assignedModulesList_helper2 : {
        hasContent : 'Please choose modules from the left list box and transfer them to the right list box'
      }
    },

    submitHandler : submitForm,

    // Do not change code below
    errorPlacement : function(error, element) {
      error.insertAfter(element.parent());
    }
  });
  
  $form.find('input[name="User_Type"]:checked').click();
  
  function submitForm() {
    var strChosenItems = "";
    var select = $form[0].assignedModulesList_helper2;
    for (var i=0;i<select.options.length;i++) {
      if(strChosenItems === ""){
        strChosenItems = select.options[i].value;
      }else{
        strChosenItems = strChosenItems + "," + select.options[i].value;
      }
    }
    $form.find("button").attr("disabled", "disabled");
    $.ajax({
      url : '/webdirector/configureUsers/updateUser?moduleSelected=' + strChosenItems,
      data : $form.serializeArray(),
      type : "POST"
    }).done(function(data) {
      $form.find("button").removeAttr("disabled");
      StorageHelper.restoreOpenModules();
      if (data.success) {
        ModalHelper.loadContent('/webdirector/secure/user/list', {success: true, content: "User saved"});
      } else {
        ModalHelper.showNotification({success: false, content: data.message});
      }
    });
  }

});
</script>