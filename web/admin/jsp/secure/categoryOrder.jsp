<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="au.net.webdirector.admin.tree.LazyTreeDAO"%>
<%
String moduleName = request.getParameter("moduleName");
String parentId = request.getParameter("parentId");
boolean isCategory = "true".equalsIgnoreCase(request.getParameter("isCategory"));
LazyTreeDAO db = new LazyTreeDAO();
List<Map<String, Object>> items = null;
if (isCategory) {
    items = db.getCategoriesByDisplayOrder(moduleName, parentId);
} else {
    items = db.getElementsByDisplayOrder(moduleName, parentId);
}
%>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Sort <%=isCategory ? "Categories" : "Elements"%></h4>
</div>
<div class="modal-body">
  <ul class="dd-list" id="sortable">
    <% for (Map item : items) { %>
    <li id='<%=item.get("id")%>' class="dd-item">
      <div class="dd-handle"><%=item.get("title")%></div>
    </li>
    <% } %>
  </ul>
</div>
<script src="/admin/plugins/jquery.multisortable/multisortable.js"></script>
<script>
$(function() {
  var $sortable = $('#sortable');
  
  $sortable.multisortable({
	  axis: "y",
	  containment: "parent",
    stop: function(event, ui) {
      ui.item.parent().find('[style]').removeAttr('style');
      saveArrangableNodes();
    }
  });
  
  function saveArrangableNodes() {
    var string = "";
    var result = $sortable.sortable('toArray');
    var size = $sortable.children().length;
    
    for (var no = 0; no < size; no++) {
      var assetId = result[no];
      if (string.length > 0)
        string = string + ',';
      string = string + "lyr" + no + "T" + assetId;
    }
    var orderString = string.replace(/,/g, '|');
    $.ajax({
      url: '/webdirector/folderTree/updateSortingOrder',
      data: {
        orderString : orderString,
        moduleName : '<%=moduleName%>',
        moduleType : '<%=isCategory? "Category" : "ELEMENTS" %>'
      },
      type: 'POST'
    });
  }
  
});
</script>