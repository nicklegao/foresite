<%@page import="webdirector.db.UserUtils"%>
<%@ page import="java.util.*"%>
<jsp:useBean id="db" class="webdirector.db.UserUtils"/>
<%
  // get users
  Vector v = db.getAllUsers();
  //u.User_id, u.Name, u.Company, u.user_name, ul.UserLevel_name
%>
<style>
</style>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Users</h4>
</div>
<div class="modal-body">
  <table class="table table-hover">
    <thead>
      <tr>
        <td>Name</td>
        <td>Company</td>
        <td>User Name</td>
        <td>Access Level</td>
        <td></td>
      </tr>
    </thead>
    <tbody>
<% for (int i=0; i<v.size(); i++)
{
  boolean noDelete = false;
  String[] details = (String[])v.elementAt(i);
  if ( details[4].equals("Administrator") )
    noDelete = true;
%>
      <tr class="user-detail-row" data-user-id="<%=details[0]%>">
        <td><%=details[1]%></td>
        <td><%=details[2]%></td>
        <td><%=details[3]%></td>
        <td><%=UserUtils.UserLevel.withRank(Integer.parseInt(details[4])).getDisplayText()%></td>
        <td><%=noDelete ? "" : "<button type='button' class='btn btn-danger btn-xs btnDeleteUser'>Delete</button>"%></td>
<%
}
%>
    </tbody>
  </table>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-success pull-left" id="btnAddUser">Add User</button>
</div>

<script>
$(function() {
  var $modal = ModalHelper.$modal;

  $modal.find(".user-detail-row").click(function() {
    var userId = $(this).data("user-id");
    ModalHelper.loadContent('/webdirector/secure/user/edit?userId=' + userId);
  });

  $modal.find("#btnAddUser").click(function() {
    ModalHelper.loadContent('/webdirector/secure/user/add');
  });

  $modal.find(".btnDeleteUser").click(function(e) {
    e.stopPropagation();
    var row = $(this).closest("tr");
    var userId = row.data("user-id");
    var name = row.children(":first").text();

    $.SmartMessageBox({
      title : "Delete User",
      content : "Are you sure you want to delete the user: " + name,
      buttons : '[No][Yes]'
    }, function(ButtonPressed) {
      if (ButtonPressed === "Yes") {
        deleteUser(userId);
      }
    });

  });

  function deleteUser(userId) {
    $.ajax({
      url : '/webdirector/configureUsers/deleteUser?userId='+userId,
      type : 'POST'
    }).done(function(data) {
      if (data.success) {
        ModalHelper.loadContent('/webdirector/secure/user/list', {success: true, content: "User deleted"});
      } else {
        ModalHelper.showNotification({success: true, content: data.message});
      }
    });
  }

});

</script>