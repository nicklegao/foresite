<%@page import="au.net.webdirector.admin.triggers.ModuleAction"%>
<%@page import="au.net.webdirector.admin.triggers.ModuleActionSet"%>
<%@page import="au.net.webdirector.admin.triggers.ModuleActionTrigger"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.net.webdirector.admin.modules.ModuleConfiguration"%>
<%@ page import="java.util.*"%>
<%@ page import="webdirector.db.DButils"%>
<%@ page import="webdirector.db.DatabaseTableMetaDataService"%>
<%@ page import="au.net.webdirector.common.Defaults"%>
<%@ page import="au.net.webdirector.admin.modules.domain.*"%>
<%@ page import="au.net.webdirector.common.datalayer.admin.db.DataAccess,au.net.webdirector.common.datatypes.domain.*"%>
<%@ page import="au.net.webdirector.admin.modules.DynamicFormScriptUtil"%>
<jsp:useBean id="userDB" class="webdirector.db.UserUtils"/>
<%
  String moduleName = request.getParameter("moduleName");
  
  ModuleConfiguration moduleConfiguration = ModuleConfiguration.getInstance();
  Module module = moduleConfiguration.getModule(moduleName);
  String moduleDescription = module.getExternalModuleName();
  int modLevels = module.getLevels();
  String moduleColor = module.getColor();
  String moduleIcon = module.getIcon();
  
  ModuleActionSet actions = ModuleActionTrigger.actionsForModule(moduleName);
%>
<style>

.icon-picker {
  float: right;
  z-index: 1;
}

.form-group .onoffswitch{
	top:5px;
}

.enabledisableswitch .onoffswitch{
  width: 85px;
}
.enabledisableswitch .onoffswitch-switch{
	right: 67px;
}
.enabledisableswitch .onoffswitch-checkbox:checked+.onoffswitch-label .onoffswitch-switch
{
	right: 0;
}

</style>
<script>
  $(function() {
    var moduleName = "<%=ESAPI.encoder().encodeForJavaScript(moduleName)%>";
    var $form = $("#configure-module-form");
    
    $("#btnSaveChanges").click(function() {
      
      $("#customisedButtons").val($('a.cke_button.cke_button_on').map(function(){return this.id;}).get().join(','));
      var btn = $(this);
      btn.attr("disabled", "disabled");
      $.ajax({
        url: '/webdirector/configureModule/updateModuleSettings?moduleName=' + moduleName,
        type: "POST",
        data: $("#configure-module-form").serializeArray(),
        success: function(data) {
          if (data.success) {
            reloadModal("Settings", {success: true, content: 'Settings configuration saved', container: "#settingsContent"});
          } else {
            ModalHelper.showNotification({success: false, content: data.message, container: "#settingsContent"});
          }
        },
        complete: function() {
          btn.removeAttr("disabled");
          StorageHelper.restoreOpenModules();
        }
      });
    });

    var $dropdown = $(".icon-picker > .dropdown-menu");
    $("#btnIcon").on('click', function (event) {
      $(this).parent().toggleClass('open');
    });
    $('body').on('click', function (e) {
      if (!$dropdown.is(e.target) && $dropdown.has(e.target).length === 0 && $('.open').has(e.target).length === 0) {
        $("#btnIcon").parent().removeClass('open');
      }
    });

    $("#modal-setting").find(".nav-tabs").find("li").not("active").find("a").click(function(e) {
      e.preventDefault();
      reloadModal($(this).data("mode"));
    });

    $(".modal-footer").find("button").click(function(e) {
      if ($(this).data("mode") != undefined)
      {
        e.preventDefault();
        reloadModal($(this).data("mode"));
      }
    });
    
    function reloadModal(mode, notification) {
      if (mode == "Settings") {
        ModalHelper.loadContent('/webdirector/secure/module/configure/settings?mode=' + mode + '&moduleName=<%=ESAPI.encoder().encodeForURL(moduleName)%>', notification);
      } else if (mode == "Permissions") {
          ModalHelper.loadContent('/webdirector/secure/module/configure/permissions?mode=' + mode + '&moduleName=<%=ESAPI.encoder().encodeForURL(moduleName)%>', notification);
      } else if (mode == "Code") {
          ModalHelper.loadContent('/webdirector/secure/module/configure/developer/generate-code?moduleName=<%=ESAPI.encoder().encodeForURL(moduleName)%>', notification);
      } else {        
        ModalHelper.loadContent('/webdirector/secure/module/configure?mode=' + mode + '&moduleName=<%=ESAPI.encoder().encodeForURL(moduleName)%>', notification);
      }
    }

  });

</script>
<div class="modal-header modal-tabs">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
  </button>
  <h4 class="modal-title" id="myModalLabel">Configure <%=ESAPI.encoder().encodeForHTML(moduleName) %> - Settings</h4>
  <ul class="nav nav-tabs tabs-pull-right" id="myTab3">
    <li class="active">
      <a href="#" data-mode="Settings">Settings</a>
    </li>
    <li>
      <a href="#" data-mode="Permissions">Permissions</a>
    </li>
    <li>
      <a href="#" data-mode="Category">Category</a>
    </li>
    <li>
      <a href="#" data-mode="Elements">Elements</a>
    </li>
  </ul>
</div>
<div class="modal-body">
<section class="icon-picker btn-group">
  <button type="button" class="btn dropdown-toggle <%=moduleColor%>" id="btnIcon">
    <i class="<%=moduleIcon%> fa-2x"></i>
    <i class="fa fa-caret-down"></i>
  </button>
  <%@include file="/admin/jsp/icons.jsp"%>
</section>
<form id="configure-module-form" class="form-horizontal">
  <input type="hidden" name="editModuleColor" id="editModuleColor" value="<%=module.getColor()%>"/>
  <input type="hidden" name="editModuleIcon" id="editModuleIcon" value="<%=module.getIcon()%>"/>

	<fieldset>
		<legend class="col">Module</legend>
		<div class="form-group">
			<label class="col-md-2 control-label">Module Name</label>
			<div class="col-md-10">
				<input placeholder="Required" type="text" class="form-control" id="editAttrModuleName" name="editAttrModuleName" maxlength="50" value="<%=ESAPI.encoder().encodeForHTMLAttribute(moduleDescription) %>"/>
				<p class="note"><strong>Note:</strong> This value is the display name throughout WebDirector. Internally this module is named <%=ESAPI.encoder().encodeForHTML(moduleName) %></p>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">Levels</label>
			<div class="col-md-10">
				<input placeholder="Required" type="number" class="form-control" id="editAttrModuleLevel" name="editAttrModuleLevel" maxlength="2" value="<%=ESAPI.encoder().encodeForHTMLAttribute(Integer.toString(modLevels)) %>"/>
				<p class="note" style="color:#b94a48"><strong>Note:</strong> Data integrity may be affected if you change this value.</p>
			</div>
		</div>
	</fieldset>

	<fieldset id="dataActions" class="enabledisableswitch">
	
      <legend class="col">Data Actions</legend>
      <div class="form-group">
      	<label class="col-md-2 control-label">Sort</label>
      	<div class="col-md-10">
      		<span class="onoffswitch">
      			<input type="checkbox" name="hasSort" id="hasSort" class="onoffswitch-checkbox" <%=module.getHasSort() ? "checked='checked'" : "" %>>
      			<label class="onoffswitch-label" for="hasSort"> 
      				<span class="onoffswitch-inner" data-swchon-text="ENABLED" data-swchoff-text="DISABLED"></span> 
      				<span class="onoffswitch-switch"></span> 
      			</label> 
      		</span>
      	</div>
      </div>
      
      <div class="form-group">
      	<label class="col-md-2 control-label">Cut</label>
      	<div class="col-md-10">
      		<span class="onoffswitch">
      			<input type="checkbox" name="hasCut" id="hasCut" class="onoffswitch-checkbox" <%=module.getHasCut() ? "checked='checked'" : "" %>>
      			<label class="onoffswitch-label" for="hasCut"> 
      				<span class="onoffswitch-inner" data-swchon-text="ENABLED" data-swchoff-text="DISABLED"></span> 
      				<span class="onoffswitch-switch"></span> 
      			</label> 
      		</span>
      	</div>
      </div>
      
      <div class="form-group">
      	<label class="col-md-2 control-label">Copy</label>
      	<div class="col-md-10">
      		<span class="onoffswitch">
      			<input type="checkbox" name="hasCopy" id="hasCopy" class="onoffswitch-checkbox" <%=module.getHasCopy() ? "checked='checked'" : "" %>>
      			<label class="onoffswitch-label" for="hasCopy"> 
      				<span class="onoffswitch-inner" data-swchon-text="ENABLED" data-swchoff-text="DISABLED"></span> 
      				<span class="onoffswitch-switch"></span> 
      			</label> 
      		</span>
      	</div>
      </div>
      
      <div class="form-group">
      	<label class="col-md-2 control-label">Import</label>
      	<div class="col-md-10">
      		<span class="onoffswitch">
      			<input type="checkbox" name="hasImport" id="hasImport" class="onoffswitch-checkbox" <%=module.getHasImport() ? "checked='checked'" : "" %>>
      			<label class="onoffswitch-label" for="hasImport"> 
      				<span class="onoffswitch-inner" data-swchon-text="ENABLED" data-swchoff-text="DISABLED"></span> 
      				<span class="onoffswitch-switch"></span> 
      			</label> 
      		</span>
      	</div>
      </div>
      
      <div class="form-group">
      	<label class="col-md-2 control-label">Export</label>
      	<div class="col-md-10">
      		<span class="onoffswitch">
      			<input type="checkbox" name="hasExport" id="hasExport" class="onoffswitch-checkbox" <%=module.getHasExport() ? "checked='checked'" : "" %>>
      			<label class="onoffswitch-label" for="hasExport"> 
      				<span class="onoffswitch-inner" data-swchon-text="ENABLED" data-swchoff-text="DISABLED"></span> 
      				<span class="onoffswitch-switch"></span> 
      			</label> 
      		</span>
      	</div>
      </div>
	</fieldset>

	<fieldset class="enabledisableswitch">
      <legend class="col">CKEditor</legend>
      <div class="form-group">
        <div class="col-md-12">
          <link href="/adminExtra/ckeditor/skins/bootstrapck/editor_gecko.css" rel="stylesheet" type="text/css">
          <style>
          .cke_button_label{
            display: initial;
          }
          </style>
          <span style="height: auto; background: none !important; border: 0px none;" class="cke_top cke_reset_all">
            <span>
              <span class="cke_toolbar">
                <span class="cke_toolbar_start">
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="1">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1824px;background-size:auto;" class="cke_button_icon cke_button__source_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__source_label">
                      Source
                    </span>
                  </a>
                  <span class="cke_toolbar_separator">
                  </span>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="2">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1704px;background-size:auto;" class="cke_button_icon cke_button__save_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__save_label">
                      Save
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="3">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1440px;background-size:auto;" class="cke_button_icon cke_button__newpage_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__newpage_label">
                      New Page
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="4">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1632px;background-size:auto;" class="cke_button_icon cke_button__preview_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__preview_label">
                      Preview
                    </span>
                  </a>
                  <span class="cke_toolbar_separator">
                  </span>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="5">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -456px;background-size:auto;" class="cke_button_icon cke_button__templates_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__templates_label">
                      Templates
                    </span>
                  </a>
                </span>
                <span class="cke_toolbar_end">
                </span>
              </span>
              <span class="cke_toolbar">
                <span class="cke_toolbar_start">
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="6">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -312px;background-size:auto;" class="cke_button_icon cke_button__cut_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__cut_label">
                      Cut
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="7">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -264px;background-size:auto;" class="cke_button_icon cke_button__copy_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__copy_label">
                      Copy
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="8">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -360px;background-size:auto;" class="cke_button_icon cke_button__paste_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__paste_label">
                      Paste
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="9">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1536px;background-size:auto;" class="cke_button_icon cke_button__pastetext_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__pastetext_label">
                      Paste as plain text
                    </span>
                  </a>
                  <span class="cke_toolbar_separator">
                  </span>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="10">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1872px;background-size:auto;" class="cke_button_icon cke_button__scayt_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__scayt_label">
                      Spell Check As You Type
                    </span>
                    
                  </a>
                </span>
                <span class="cke_toolbar_end">
                </span>
              </span>
              <span class="cke_toolbar">
                <span class="cke_toolbar_start">
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="11">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1992px;background-size:auto;" class="cke_button_icon cke_button__undo_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__undo_label">
                      Undo
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="12">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1944px;background-size:auto;" class="cke_button_icon cke_button__redo_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__redo_label">
                      Redo
                    </span>
                  </a>
                  <span class="cke_toolbar_separator">
                  </span>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="13">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -528px;background-size:auto;" class="cke_button_icon cke_button__find_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__find_label">
                      Find
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="14">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -552px;background-size:auto;" class="cke_button_icon cke_button__replace_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__replace_label">
                      Replace
                    </span>
                  </a>
                  <span class="cke_toolbar_separator">
                  </span>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="15">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1728px;background-size:auto;" class="cke_button_icon cke_button__selectall_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__selectall_label">
                      Select All
                    </span>
                  </a>
                </span>
                <span class="cke_toolbar_end">
                </span>
              </span>
              <span class="cke_toolbar">
                <span class="cke_toolbar_start">
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="16">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -648px;background-size:auto;" class="cke_button_icon cke_button__form_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__form_label">
                      Form
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="17">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -624px;background-size:auto;" class="cke_button_icon cke_button__checkbox_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__checkbox_label">
                      Checkbox
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="18">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -720px;background-size:auto;" class="cke_button_icon cke_button__radio_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__radio_label">
                      Radio Button
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="19">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -864px;background-size:auto;" class="cke_button_icon cke_button__textfield_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__textfield_label">
                      Text Field
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="20">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -816px;background-size:auto;" class="cke_button_icon cke_button__textarea_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__textarea_label">
                      Textarea
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="21">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -768px;background-size:auto;" class="cke_button_icon cke_button__select_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__select_label">
                      Selection Field
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="22">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -600px;background-size:auto;" class="cke_button_icon cke_button__button_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__button_label">
                      Button
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="23">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -696px;background-size:auto;" class="cke_button_icon cke_button__imagebutton_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__imagebutton_label">
                      Image Button
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="24">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -672px;background-size:auto;" class="cke_button_icon cke_button__hiddenfield_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__hiddenfield_label">
                      Hidden Field
                    </span>
                  </a>
                </span>
                <span class="cke_toolbar_end">
                </span>
              </span>
              <span class="cke_toolbar_break">
              </span>
              <span class="cke_toolbar">
                <span class="cke_toolbar_start">
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="25">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -24px;background-size:auto;" class="cke_button_icon cke_button__bold_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__bold_label">
                      Bold
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="26">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -48px;background-size:auto;" class="cke_button_icon cke_button__italic_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__italic_label">
                      Italic
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="27">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -144px;background-size:auto;" class="cke_button_icon cke_button__underline_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__underline_label">
                      Underline
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="28">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -72px;background-size:auto;" class="cke_button_icon cke_button__strike_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__strike_label">
                      Strikethrough
                    </span>
                  </a>
                  <span class="cke_toolbar_separator">
                  </span>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="29">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -96px;background-size:auto;" class="cke_button_icon cke_button__subscript_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__subscript_label">
                      Subscript
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="30">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -120px;background-size:auto;" class="cke_button_icon cke_button__superscript_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__superscript_label">
                      Superscript
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="31">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1680px;background-size:auto;" class="cke_button_icon cke_button__removeformat_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__removeformat_label">
                      Remove Format
                    </span>
                  </a>
                </span>
                <span class="cke_toolbar_end">
                </span>
              </span>
              <span class="cke_toolbar">
                <span class="cke_toolbar_start">
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="32">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1368px;background-size:auto;" class="cke_button_icon cke_button__numberedlist_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__numberedlist_label">
                      Insert/Remove Numbered List
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="33">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1320px;background-size:auto;" class="cke_button_icon cke_button__bulletedlist_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__bulletedlist_label">
                      Insert/Remove Bulleted List
                    </span>
                  </a>
                  <span class="cke_toolbar_separator">
                  </span>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="34">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1032px;background-size:auto;" class="cke_button_icon cke_button__outdent_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__outdent_label">
                      Decrease Indent
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="35">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -984px;background-size:auto;" class="cke_button_icon cke_button__indent_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__indent_label">
                      Increase Indent
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="36">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -216px;background-size:auto;" class="cke_button_icon cke_button__blockquote_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__blockquote_label">
                      Block Quote
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="37">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -480px;background-size:auto;" class="cke_button_icon cke_button__creatediv_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__creatediv_label">
                      Create Div Container
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="60">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/wddiv/icons/createwddiv.png');" class="cke_button_icon cke_button__createwddiv_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__creatediv_label">
                      Create WD Div Container
                    </span>
                  </a>
                </span>
                <span class="cke_toolbar_end">
                </span>
              </span>
              <span class="cke_toolbar">
                <span class="cke_toolbar_start">
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="38">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1128px;background-size:auto;" class="cke_button_icon cke_button__justifyleft_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__justifyleft_label">
                      Align Left
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="39">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1104px;background-size:auto;" class="cke_button_icon cke_button__justifycenter_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__justifycenter_label">
                      Center
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="40">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1152px;background-size:auto;" class="cke_button_icon cke_button__justifyright_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__justifyright_label">
                      Align Right
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="41">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1080px;background-size:auto;" class="cke_button_icon cke_button__justifyblock_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__justifyblock_label">
                      Justify
                    </span>
                  </a>
                </span>
                <span class="cke_toolbar_end">
                </span>
              </span>
              <span class="cke_toolbar">
                <span class="cke_toolbar_start">
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="42">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1248px;background-size:auto;" class="cke_button_icon cke_button__link_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__link_label">
                      Link
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="43">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1272px;background-size:auto;" class="cke_button_icon cke_button__unlink_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__unlink_label">
                      Unlink
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="44">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1224px;background-size:auto;" class="cke_button_icon cke_button__anchor_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__anchor_label">
                      Anchor
                    </span>
                  </a>
                </span>
                <span class="cke_toolbar_end">
                </span>
              </span>
              <span class="cke_toolbar">
                <span class="cke_toolbar_start">
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="45">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -936px;background-size:auto;" class="cke_button_icon cke_button__image_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__image_label">
                      Image
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="46">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/videodetector//icons/videodetector.svg');background-position:0 0px;background-size:16px;" class="cke_button_icon cke_button__videodetector_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__videodetector_label">
                      Insert a Youtube, Vimeo or Dailymotion video
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="47">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1896px;background-size:auto;" class="cke_button_icon cke_button__table_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__table_label">
                      Table
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="48">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -888px;background-size:auto;" class="cke_button_icon cke_button__horizontalrule_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__horizontalrule_label">
                      Insert Horizontal Line
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="49">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1848px;background-size:auto;" class="cke_button_icon cke_button__specialchar_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__specialchar_label">
                      Insert Special Character
                    </span>
                  </a>
                </span>
                <span class="cke_toolbar_end">
                </span>
              </span>
              <span class="cke_toolbar_break">
              </span>
              <span class="cke_toolbar">
                <span class="cke_toolbar_start">
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="50">
                    <span class="cke_button_label">
                      Format
                    </span>
                  </a>
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="51">
                    <span class="cke_button_label">
                      Font
                    </span>
                  </a>
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="52">
                    <span class="cke_button_label">
                      Size
                    </span>
                  </a>
                </span>
                <span class="cke_toolbar_end">
                </span>
              </span>
              <span class="cke_toolbar">
                <span class="cke_toolbar_start">
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="53">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -408px;background-size:auto;" class="cke_button_icon cke_button__textcolor_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__textcolor_label">
                      Text Color
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="54">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -384px;background-size:auto;" class="cke_button_icon cke_button__bgcolor_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__bgcolor_label">
                      Background Color
                    </span>
                  </a>
                </span>
                <span class="cke_toolbar_end">
                </span>
              </span>
              
              <span class="cke_toolbar">
                <span class="cke_toolbar_start">
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="59">
                    <span class="cke_button_label">
                      Data Items
                    </span>
                  </a>
                </span>
                <span class="cke_toolbar_end">
                </span>
              </span>
              <span class="cke_toolbar">
                <span class="cke_toolbar_start">
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="55">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1392px;background-size:auto;" class="cke_button_icon cke_button__maximize_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__maximize_label">
                      Minimize
                    </span>
                  </a>
                  <a class="cke_button cke_button__closewindow cke_button_off" id="56">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 -1776px;background-size:auto;" class="cke_button_icon cke_button__showblocks_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__showblocks_label">
                      Show Blocks
                    </span>
                  </a>
                </span>
                <span class="cke_toolbar_end">
                </span>
              </span>
              <span class="cke_toolbar">
                <span class="cke_toolbar_start">
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="57">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/externalcss/images/css.png');background-position:0 0px;background-size:16px;" class="cke_button_icon cke_button__commoncss_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__commoncss_label">
                      Common CSS
                    </span>
                  </a>
                </span>
                <span class="cke_toolbar_end">
                </span>
              </span>
              <span class="cke_toolbar">
                <span class="cke_toolbar_start">
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="58">
                    <span style="background-image:url('/adminExtra/ckeditor/plugins/icons.png');background-position:0 0px;background-size:auto;" class="cke_button_icon cke_button__about_icon">
                      &nbsp;
                    </span>
                    <span class="cke_button_label cke_button__about_label">
                      About CKEditor
                    </span>
                  </a>
                </span>
                <span class="cke_toolbar_end">
                </span>
              </span>
              <span class="cke_toolbar">
                <span class="cke_toolbar_start">
                </span>
                <span class="cke_toolgroup">
                  <a class="cke_button cke_button__closewindow cke_button_off" id="B">
                    <span class="cke_button_label cke_button__about_label">
                      Browse Server
                    </span>
                  </a>
                </span>
                <span class="cke_toolbar_end">
                </span>
              </span>
            </span>
          </span>
          
          <input type="hidden" id="customisedButtons" name="customisedButtons" maxlength="500" value="<%=module.getCustomisedButtons() %>">
          
          <script>
          $(function(){
            $('.cke_button').click(function(){
              $(this).toggleClass('cke_button_off').toggleClass('cke_button_on');
            });
            $($('#customisedButtons').val().split(',')).each(function(){
              $('#'+this).removeClass('cke_button_off').addClass('cke_button_on');
            })
          });
          </script>
        </div>
      </div>
    </fieldset>
    <fieldset>
		<legend class="col">Data Management</legend>
		
		<div class="form-group">
			<label class="col-md-2 control-label">Version Control</label>
			<div class="col-md-10">
				<span class="onoffswitch">
					<input type="checkbox" name="hasVersionControl" id="hasVersionControl" class="onoffswitch-checkbox" <%=module.getHasVersionControl() ? "checked='checked'" : "" %>>
					<label class="onoffswitch-label" for="hasVersionControl"> 
						<span class="onoffswitch-inner" data-swchon-text="ON" data-swchoff-text="OFF"></span> 
						<span class="onoffswitch-switch"></span> 
					</label> 
				</span>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-md-2 control-label">Workflow</label>
			<div class="col-md-10">
				<span class="onoffswitch">
					<input type="checkbox" name="hasWorkflow" id="hasWorkflow" class="onoffswitch-checkbox" <%=module.getHasWorkflow() ? "checked='checked'" : "" %>>
					<label class="onoffswitch-label" for="hasWorkflow"> 
						<span class="onoffswitch-inner" data-swchon-text="ON" data-swchoff-text="OFF"></span> 
						<span class="onoffswitch-switch"></span> 
					</label> 
				</span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">Show Live Flag &amp; Date(s)</label>
			<div class="col-md-10">
				<span class="onoffswitch">
					<input type="checkbox" name="showLiveFlag" id="showLiveFlag" class="onoffswitch-checkbox" <%=module.hasShowLiveFlag() ? "checked='checked'" : "" %>>
					<label class="onoffswitch-label" for="showLiveFlag"> 
						<span class="onoffswitch-inner" data-swchon-text="ON" data-swchoff-text="OFF"></span> 
						<span class="onoffswitch-switch"></span> 
					</label> 
				</span>
			</div>
		</div>
	</fieldset>
  
    <% if (actions.size() > 0) { %>
    <fieldset>
      <legend class="col">Trigger Actions</legend>
      
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Class</th>
            <th width="100">Executions</th>
            <th width="150">Average execution time</th>
          </tr>
        </thead>
        <tbody>
        <% for (ModuleAction action : actions) { %>
          <tr>
            <td><%=action.getClass().getCanonicalName() %></td>
            <td><%=action.executionCount() %></td>
            <td><%=action.calculateAverageExecutionTime() %> ms</td>
          </tr>
        <% } %>
        </tbody>
        <tfoot>
          <tr>
            <th>Total <%=actions.size() %> data actions.</th>
            <th><%=actions.executionCount() %></th>
            <th><%=actions.calculateAverageExecutionTime() %> ms</th>
          </tr>
          <tr>
            <th colspan="3">Please contact Corporate Interactive to make changes.</th>
          </tr>
        </tfoot>
      </table>
    </fieldset>
    <% } %>
</form>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-mode="Code">Generate Code</button>
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  <button type="button" class="btn btn-primary" id="btnSaveChanges">Save Changes</button>
</div>