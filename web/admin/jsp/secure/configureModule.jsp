<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.net.webdirector.admin.modules.ModuleConfiguration"%>
<%@ page import="java.util.*"%>
<%@ page import="webdirector.db.DButils"%>
<%@ page import="webdirector.db.DatabaseTableMetaDataService"%>
<%@ page import="au.net.webdirector.common.Defaults"%>
<%@ page import="au.net.webdirector.admin.modules.domain.*"%>
<%@ page import="au.net.webdirector.common.datalayer.admin.db.DataAccess,au.net.webdirector.common.datatypes.domain.*"%>
<%@ page import="au.net.webdirector.admin.modules.DynamicFormScriptUtil"%>
<jsp:useBean id="dataTypes" class="au.net.webdirector.common.datatypes.service.DataTypesService" />
<%
  String moduleName = request.getParameter("moduleName");
  
  String tableName = "Elements_" + moduleName;
  String labelTableName = "Labels_" + moduleName;

  //Retrieve straight from DB
  ModuleConfiguration moduleConfiguration = ModuleConfiguration.getInstance();
  Module module = moduleConfiguration.getModule(moduleName);
  String moduleDescription = module.getExternalModuleName();
  int modLevels = module.getLevels();
  
  String mode = request.getParameter("mode"); //either Elements or Category
  boolean isCategory = "Category".equalsIgnoreCase(mode) ? true : false;
  
  String formValidationScripts;
  if (isCategory) {
    formValidationScripts = DynamicFormScriptUtil.getCategoryFormValidScript(moduleName);
  } else {
    formValidationScripts = DynamicFormScriptUtil.getAssetFormValidScript(moduleName);
  }

  //TODO: This sets the session attributes but DBUtils shouldn't be pulling from the session
  request.getSession().setAttribute("moduleSelected", moduleName);
  request.getSession().setAttribute("moduleSelectedName", moduleDescription);
  request.getSession().setAttribute("moduleLevels", Integer.toString(modLevels));

  List<DataType> labels = null;
  if (moduleName != null) {
    // get fields from db
    labels = dataTypes.loadGenericDataTypes(moduleName, isCategory, null);
  }
%>
<style>
#configure-module-form {
  color: #0e0e0e;
}

.vertical-text {
  /*writing-mode: vertical-rl;*/

  -webkit-transform: rotate(90deg);
  -moz-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  -o-transform: rotate(90deg);
  transform: rotate(90deg);

  /* also accepts left, right, top, bottom coordinates; not required, but a good idea for styling */
  -webkit-transform-origin: 50% 50%;
  -moz-transform-origin: 50% 50%;
  -ms-transform-origin: 50% 50%;
  -o-transform-origin: 50% 50%;
  transform-origin: 50% 50%;

  white-space: nowrap;
  display: block;
  width: 20px;
  height: 20px;
  text-shadow: 0 0 1px #FFF;
}

#configure-module-form thead tr {
  height: 100px;
}

#configure-module-form thead tr th {
  font-weight: normal;
  /*font-style: italic;*/
}


#configure-module-form th, #configure-module-form td {
  vertical-align: top;
  padding-left: 2px;
  padding-right: 2px;
}

#configure-module-form td select {
  padding: 0;
  height: 24px;
  width: 40px;
}

#configure-module-form td .select i {
  right: 4px;
  top: 7px;
  box-shadow: none;
}

#configure-module-form input, #configure-module-form textarea {
  padding-left: 3px;
  padding-right: 3px;
}

#configure-module-form .input-short {
  width: 25px;
}

#configure-module-form textarea {
  font-size: 11px;
}

#frmAddColumn {
  float: left;
}

#frmAddColumn > label {
  display: inline-block;
  vertical-align: top;
}

#frmAddColumn > button {
  padding: 6px 12px;
}

#dropDownValueModal .modal-content {
  width: 700px;
  margin: auto;
  top: 50px;
}
table > thead.sticky-table-header{
  left:21px !important;
  top:56px !important;
}
</style>
<script src="/admin/js/plugin/stickytableheaders/jquery.stickytableheaders.js"></script>
<script>
  $(function() {
    var mode = "<%=mode%>";
    var moduleName = "<%=moduleName%>";
    var $form = $("#configure-module-form");
    
    var a = $form.find("input[type='checkbox'][name^='DUMMY']").click(function() {
      var $this = $(this);
      var hiddenName = $this.attr("name").slice(5);
      if ($this.is(":checked")) {
        $form.find("input[name='" + hiddenName + "']").val("1");
        if(hiddenName.indexOf('ATTRCHECK_noChange')!= -1){
          var manditoryName = hiddenName.replace('ATTRCHECK_noChange', 'ATTRCHECK_mandatory')
          $form.find("input[name='DUMMY" + manditoryName + "']:not(:checked)").click();
        }
      } else {
        $form.find("input[name='" + hiddenName + "']").val("0");
      }
    });
    
    $('#frmAddColumn select[name="colType"]').change(function(){
      if($(this).val() == 'varchar'){
        $(this).closest('label').next().show().find('input').val('500');
      }else if($(this).val() == 'char'){
        $(this).closest('label').next().show().find('input').val('50');
      }else if($(this).val() == 'Decimal'){
        $(this).closest('label').next().show().find('input').val('19,9');
      }else{
        $(this).closest('label').next().hide().find('input').val('');
      }
    })
    
    $("#btnSaveChanges").click(function() {
      var btn = $(this);
      btn.attr("disabled", "disabled");
      $.ajax({
        url: '/webdirector/configureModule/updateMetadata?mode=' + mode + '&moduleName=' + moduleName,
        type: "POST",
        data: $("#configure-module-form").serializeArray(),
        success: function(data) {
          if (data.success) {
            reloadModal(mode, {success: true, content: mode + ' configuration saved', container: "#settingsContent"});
          } else {
            ModalHelper.showNotification({success: false, content: data.message, container: "#settingsContent"});
          }
        },
        complete: function() {
          btn.removeAttr("disabled");
          StorageHelper.restoreOpenModules();
        }
      });
    });

    $("#frmAddColumn").on("submit", function(e) {
      e.preventDefault();
      e.stopPropagation();
      var data = $(this).serialize();
      var btn = $(this).find("button");
      btn.attr("disabled", "disabled");
      $.ajax({
        url : '/webdirector/configureModule/addColumn?mode=' + mode + '&moduleName=' + moduleName,
        data : data,
        success : function(data) {
          if (data.success) {
            reloadModal(mode, {success: true, content: data.message});
          } else {
            ModalHelper.showNotification({success: false, content: data.message});
            btn.removeAttr("disabled");
          }
        }
      });
    });

    $("#configure-module-form").on("click", ".btn-delete-col", function() {
      var deleteCol = $(this).data("delete-col");
      $.ajax({
        url : '/webdirector/configureModule/deleteColumn?mode=' + mode + '&moduleName=' + moduleName,
        data : { deleteCol : deleteCol },
        success : function(data) {
          if (data.success) {
            reloadModal(mode, {success: true, content: deleteCol + " deleted from the database"});
          } else {
            ModalHelper.showNotification({success: false, content: data.message});
          }
        }
      });
    });

    $("#configure-module-form").on("click", ".btn-values-col", function() {
      var colName = $(this).data("values-col");
      var type = mode === 'Category' ? 'Category' : 'element';

      var modal = $("#dropDownValueModal");
      var content = $("#dropDownValueContent");
      content.load("/webdirector/secure/module/configure/dropdown/values?type=" + type + "&colName=" + colName,
        function() {
          content.find(".modal-body").css("height", $(window).height() * 0.4);
          modal.modal();
        });
    });

    $("#modal-setting").find(".nav-tabs").find("li").not("active").find("a").click(function(e) {
      e.preventDefault();
      reloadModal($(this).data("mode"));
    });
    
    function reloadModal(mode, notification) {
      if (mode == "Settings") {
          ModalHelper.loadContent('/webdirector/secure/module/configure/settings?mode=' + mode + '&moduleName=<%=ESAPI.encoder().encodeForURL(moduleName)%>', notification);
        } else if (mode == "Permissions") {
            ModalHelper.loadContent('/webdirector/secure/module/configure/permissions?mode=' + mode + '&moduleName=<%=ESAPI.encoder().encodeForURL(moduleName)%>', notification);
        } else if (mode == "Code") {
            ModalHelper.loadContent('/webdirector/secure/module/configure/developer/generate-code?moduleName=<%=ESAPI.encoder().encodeForURL(moduleName)%>', notification);
        } else {        
          ModalHelper.loadContent('/webdirector/secure/module/configure?mode=' + mode + '&moduleName=<%=ESAPI.encoder().encodeForURL(moduleName)%>', notification);
        }
    }
    
    $("#btnSort").click(function() {
      ModalHelper.loadContent('/webdirector/secure/module/configure/sort-attribute?mode=' + mode + '&moduleName=<%=moduleName%>');
    });

  });
  $(function(){
    $('table.table').stickyTableHeaders({
      scrollableArea: $('.modal-body','#modal-setting'),
      // objWindow: $('.modal-body','#modal-setting')
      // fixedOffset: $('.header')
    });
  });
</script>
<div class="modal-header modal-tabs">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
  </button>
  <h4 class="modal-title" id="myModalLabel">Configure <%=moduleName%> - <%=isCategory?"Category":"Elements"%></h4>
  <ul class="nav nav-tabs tabs-pull-right" id="myTab3">
    <li>
      <a href="#" data-mode="Settings">Settings</a>
    </li>
    <li>
      <a href="#" data-mode="Permissions">Permissions</a>
    </li>
    <li class='<%=isCategory ? "active" : "" %>'>
      <a href="#" data-mode="Category">Category</a>
    </li>
    <li class='<%=isCategory ? "" : "active" %>'>
      <a href="#" data-mode="Elements">Elements</a>
    </li>
  </ul>
</div>
<div class="modal-body">
<form id="configure-module-form" class="smart-form">
  <section class="header">
  <label class="label">Form Validation Scripts</label>
  <label class="textarea textarea-resizable">
    <textarea class="custom-scroll" id="formValidationScripts" name="formValidationScripts" rows="8"><%=formValidationScripts%></textarea>
  </label>
  <div class="note"><strong>Note:</strong>
    <%=isCategory ? " function onDeleteCategory(id, module, msgShower), onEditCategory(id, module), onCreateCategory(module) must be implemented"
        : " function onDeleteAsset(id, module, msgShower), onEditAsset(id, module), onCreateAsset(module) must be implemented"
    %>
  </div>
  </section>
<table class="table">
<thead class="sticky-table-header">
<tr>
  <th class="" style="vertical-align: middle">
    Internal (Attribute Name)
  </th>
  <th class="">
    <span class="vertical-text">On/Off</span>
  </th>
  <th class="">
    <span class="vertical-text">Mandatory</span>
  </th>
  <th class="">
    <span class="vertical-text">Unique</span>
  </th>
  <th class="">
    <span class="vertical-text">Index</span>
  </th>
  <th class="">
    <span class="vertical-text">Read-only</span>
  </th>
  <th class="">
    <span class="vertical-text">No Change</span>
  </th>
  <% if (!isCategory) { %>
  <th class="">
    <span class="vertical-text">XML Export</span>
  </th>
  <% } %>
  <% if (isCategory && modLevels > 1) { %>
  <th class="">
    <span class="vertical-text">Category Level</span>
  </th>
  <% } %>
  <th class="">
    <span class="vertical-text">Rank</span>
  </th>
  <th class="">
    <span class="vertical-text">Tokenized</span>
  </th>
  <th class="" style="vertical-align: middle; width: 50%">
    Data Label & Help Text
  </th>
  <th>
  </th>
  <th>
  </th>
</tr>
</thead>
<tbody>
<% for (DataType col : labels) { // set the form field names
    String onOff="ATTRCHECK_onOff" + col.getLabelId();
    String mandatory="ATTRCHECK_mandatory" + col.getLabelId();
    String unique="ATTRCHECK_unique" + col.getLabelId();
    String index="ATTRCHECK_index" + col.getLabelId();
    String tokenized="ATTRCHECK_tokenized" + col.getLabelId();
    String rank="ATTR_rank" + col.getLabelId();
    String external="ATTR_external" + col.getLabelId();
    String info="ATTR_info" + col.getLabelId();
    String defaultValue="ATTR_defaultValue" + col.getLabelId();
    String loginRequired="ATTRCHECK_loginRequired" + col.getLabelId();
    String exportRequired="ATTRCHECK_exportRequired" + col.getLabelId();
    String level = "ATTR_Level"+col.getLabelId();
    String readOnly = "ATTRCHECK_readOnly"+col.getLabelId();
    String noChange = "ATTRCHECK_noChange"+col.getLabelId();
%>
<tr>
  <td class="">
    <%=col.getInternalName()%>
      <input type=hidden name="ATTR_internal<%=col.getLabelId()%>" value="<%=col.getInternalName()%>">
      <br>
      <small><%=col.getTypeName()%></small>
  </td>
  <td>
    <label class="checkbox">
      <input type="checkbox" <%=col.isOn() ? "checked" : ""%> name="DUMMY<%=onOff%>">
      <i title="On/Off"></i>
    </label>
    <input type=hidden name="<%=onOff%>" value="<%=col.isOn() ? " 1 " : "0 "%>">
  </td>
  <td>
    <label class="checkbox">
      <input type="checkbox" <%=col.isMandatory() ? "checked" : ""%> name="DUMMY<%=mandatory%>">
      <i title="Mandatory"></i>
    </label>
    <input type=hidden name="<%=mandatory%>" value="<%=col.isMandatory() ? " 1 " : "0 "%>">
  </td>
  <td>
    <label class="checkbox">
      <input type="checkbox" <%=col.isUnique() ? "checked" : ""%> <%=printUniqueDisabled(col.getInternalName())%> name="DUMMY<%=unique%>">
      <i title="Unique"></i>
    </label>
    <input type=hidden name="<%=unique%>" value="<%=col.isUnique() ? " 1 " : "0 "%>">
  </td>
  <td>
    <label class="checkbox">
      <input type="checkbox" <%=col.isIndex() ? "checked" : ""%> name="DUMMY<%=index%>">
      <i title="Index"></i>
    </label>
    <input type=hidden name="<%=index%>" value="<%=col.isIndex() ? " 1 " : "0 "%>">
  </td>
  <td>
    <label class="checkbox">
      <input type="checkbox" <%=col.isReadOnly() ? "checked" : ""%> name="DUMMY<%=readOnly%>">
      <i title="Read Only"></i>
    </label>
    <input type=hidden name="<%=readOnly %>" value="<%=col.isReadOnly() ? " 1 " : "0 "%>">
  </td>
  <td>
    <label class="checkbox">
      <input type="checkbox" <%=col.isNoChange() ? "checked" : ""%> name="DUMMY<%=noChange%>">
      <i title="No Change"></i>
    </label>
    <input type=hidden name="<%=noChange %>" value="<%=col.isNoChange() ? " 1 " : "0 "%>">
  </td>
  <% if (!isCategory) { %>
  <td>
    <label class="checkbox">
      <input type="checkbox" <%=col.getExport().equals( "1") ? "checked" : ""%> name="DUMMY<%=exportRequired%>">
      <i title="XML Export"></i>
    </label>
    <input type=hidden name="<%=exportRequired%>" value="<%=col.getExport()%>">
  </td>
  <% } %>
  <% if (isCategory && modLevels > 1) { %>
  <td>
    <% if (!col.getInternalName().equals("ATTR_categoryName")) { %>
    <label class="select">
      <select class="" name="<%=level%>"><option value="0">All</option><%for(int i = 1; i <= modLevels; i++){%><option value="<%=i%>" <%=Integer.toString(i).equals(col.getCategoryAttributeLevel())?"SELECTED":""%> ><%=i%></option><%}%></select>
      <i title="Level"></i>
    </label>
    <% } %>
  </td>
  <% } %>
  <td>
    <label class="input">
      <input type="text" class="input-xs input-short" name="<%=rank%>" value="<%=col.getLuceneRank()%>">
    </label>
  </td>
  <td>
    <label class="checkbox">
      <input type="checkbox" <%=col.isLuceneTokenized() ? "checked" : ""%> name="DUMMY<%=tokenized%>">
      <i title="Tokenized"></i>
    </label>
    <input type=hidden name="<%=tokenized%>" value="<%=col.isLuceneTokenized() ? " 1 " : "0 "%>">
  </td>
  <td class="">
    <section>
      <label class="label">DATA LABEL</label>
      <label class="input">
        <input type="text" class="input-sm" name="<%=external%>" maxlength="50" value="<%=col.getExternalName()%>" />
      </label>
      <input type="hidden" name="DUMMY<%=external%>" value="<%=col.getExternalName()%>">
    </section>
    <section>
      <label class="label">DEFAULT VALUE</label>
      <label class="input">
        <input type="text" class="input-sm" name="<%=defaultValue%>" maxlength="100" value="<%=col.getDefaultValue()%>" />
      </label>
      <input type="hidden" name="DUMMY<%=defaultValue%>" value="<%=col.getDefaultValue()%>">
    </section>
    <section>
      <label class="label">USER HELP TEXT</label>
      <label class="textarea textarea-resizable">
        <textarea class="custom-scroll" name="<%=info%>" rows="2" maxlength="255"><%=col.getLabelInfo()%></textarea>
      </label>
      <input type=hidden name="DUMMY<%=info%>" value="<%=col.getLabelInfo()%>">
    </section>
  </td>
  <td style="vertical-align: middle">
    <% if (col.getInternalName() !=null && (col.getInternalName().indexOf( "ATTRDROP_") !=- 1 || col.getInternalName().indexOf( "ATTRMULTI_") !=- 1)) { %>
      <button type="button" class="btn btn-info btn-xs btn-values-col" data-values-col="<%=col.getInternalName()%>">Values</button>
    <% } %>
  </td>
  <td style="vertical-align: middle">
    <% if (!StringUtils.equalsIgnoreCase(col.getInternalName(), "attr_headline") && !StringUtils.equalsIgnoreCase(col.getInternalName(), "attr_categoryname")) { %>
      <button type="button" class="btn btn-danger btn-xs btn-delete-col" data-delete-col="<%=col.getInternalName()%>">Delete</button>
    <% } %>
  </td>
</tr>
<% } %>
</tbody>
</table>
</form>
</div>
<div class="modal-footer">
  <form class="smart-form" id="frmAddColumn">
      <label class="input">
        <input type="text" placeholder="Column Name" name="addCol" maxlength="50">
      </label>
      <label class="select">
        <select name="colType">
          <option disabled="" selected="" value="">Data Type</option>
            <option value="Datetime">Datetime</option>
            <option value="Integer">Integer</option>
            <option value="Float">Float</option>
            <option value="Money">Money</option>
            <option value="Decimal">Decimal</option>
            <option value="DropDown">DropDown</option>
            <option value="DropDownMulti">DropDownMulti</option>
            <option value="Checkbox">Checkbox</option>
            <option value="File">File</option>
            <option value="char20">Char(20)</option>
            <option value="char50">Char(50)</option>
            <option value="char100">Char(100)</option>
            <option value="char255">Char(255)</option>
            <option value="varchar">Varchar</option>
            <option value="Notes">Notes</option>
            <option value="Unlimited">Text (Editor)</option>
            <option value="TextOnly">Text (No editor)</option>
            <option value="password">Password</option>
            <option value="SharedFile">Shared File</option>
        </select> <i></i>
      </label>
      <label class="input" style="width:100px;display:none;">
        <input type="text" name="colSize" placeholder="Size" value=""/>
      </label>
      <button type="submit" class="btn btn-success" id="btnAddColumn">Add Column</button>
  </form>
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-info" id="btnSort">Sort</button>
<button type="button" class="btn btn-primary" id="btnSaveChanges">Save Changes</button>
</div>

<div class="modal fade" id="dropDownValueModal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content" id="dropDownValueContent">
    </div>
  </div>
</div>

<%!public String printUniqueDisabled(String fieldName) {
      if (fieldName.toUpperCase().startsWith("ATTR_") || fieldName.toUpperCase().startsWith("ATTRINTEGER_")) {
         return "";
      }
      return "disabled";
}%>