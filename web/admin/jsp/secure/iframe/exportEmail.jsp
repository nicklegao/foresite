<!DOCTYPE html>
<%@page import="au.net.webdirector.admin.security.LoginController"%>
<%@ page import="java.util.Vector,
				 java.util.Hashtable"%>
<%@ page import="webdirector.db.client.UserDetails,
				    webdirector.db.client.UserDetails,au.net.webdirector.admin.modules.ModuleNameService"%>
<%@ page import="au.net.webdirector.common.Defaults"%>

<jsp:useBean id="CSV" class="webdirector.db.DBexport" scope="session" />
<jsp:setProperty name="CSV" property="*" />
<jsp:useBean id="email" class="au.net.webdirector.common.utils.email.EmailUtils" scope="session" />
<jsp:setProperty name="email" property="*" />

<html>

<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
<style>
.modal-body {
  padding-left: 40px;
  padding-right: 40px;
}
</style>
</head>

<%
	Defaults d = Defaults.getInstance();
    ModuleNameService m = new ModuleNameService();
    // export file

    String noRow;
    String modules[] = m.getAllValidModules();
    String u = (String)session.getValue(LoginController.WD_USER);
	UserDetails user = new UserDetails(u);
    String[] email_name = { (String)request.getParameter("email") };
    String moduleName = request.getParameter("module")!=null ?request.getParameter("module") : "SUBSCRIBE";
    String table =  request.getParameter("table");

   // noRow = CSV.exportElementTableToCSV("SUBSCRIBE",table);


    // send email
    String OSTypeTempDir = d.getOStypeTempDir();
    boolean retInfo = email.sendMail(email_name, null, null, "no-reply@traveldocs.cloud", moduleName+" export", "Please find attached your export details from WebDirector.",
	OSTypeTempDir+table+"_"+moduleName+".csv");
%>
<body class="iframe-body">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Export Data</h4>
</div>
<div class="modal-body">
  <div class="form-group">
    <label>Export data file built & email sent: Status <%= retInfo%></label>
  </div>
</div>
<div class="modal-footer">
</div>
<script>
$(function(){
  $('.modal-header .close').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>

