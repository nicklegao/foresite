$(document).ready(function()
{
	var domHead = "<'navbar' <'navbar-form navbar-left'<'form-group show-page' l>>  <'navbar-form navbar-right'<'form-group greentech-panel-group' f>> >";
	var domBody = "<'responsive-table-wrapper' t>";
	var domFooter = "<'navbar' <'navbar-form navbar-left' i> <'navbar-form navbar-right'<'form-group pagination-bar' p>> >";

	var dashboardTable = $('#paymentTable').DataTable({
		"aaSorting" : [ [ 1, "asc" ] ],
		"autoWidth" : true,
		"processing" : true,
		"serverSide" : true,
		"ajax" : {
			"url" : "/webdirector/secure/app/data/customers",
			"method" : "POST",
		},
		"aoColumns" : [ {
			title : 'Delete',
			data : 'id',
			render : function(data, type, row)
			{
				if (type == 'display')
				{
					var btn = $('<span class="btn btn-danger btn-delete">').attr('data-id', row.id).text('Delete');
					return $("<div>").append(btn).html();
				}
				else
				{
					return row.id;
				}
			}
		}, {
			title : 'ID',
			data : 'id',
			render : function(data, type, row)
			{
				return row.id;
			}
		}, {
			title : 'Company',
			data : 'company',
			render : function(data, type, row)
			{
				return row.company;
			}
		}, {
			title : 'Plan',
			data : 'plan',
			render : function(data, type, row)
			{
				return row.plan;
			}
		}, {
			title : 'Expiry',
			data : 'expiry',
			render : function(data, type, row)
			{
				if (type == 'display')
				{
					return new Date(row.expiry).toLocaleDateString("en-US");
				}
				else
				{
					return row.expiry;
				}
			}
		}, {
			title : 'Status',
			data : 'status',
			render : function(data, type, row)
			{
				return row.status;
			}
		} ],
		"oLanguage" : {
			"sLengthMenu" : '<label>Show</label> <div class="per-page short-select-option"> _MENU_ </div> <span>per page</span>',
			"sSearch" : "",
			"oPaginate" : {
				"sPrevious" : '<i class="fa fa-arrow-left"></i>',
				"sNext" : '<i class="fa fa-arrow-right"></i>'
			},
			"sInfo" : '<i class="fa fa-spin fa-cog dashboardLoadingIndicator"></i> Showing _START_ to _END_ of _TOTAL_ customers',
			"sInfoEmpty" : "No customer found",
		},
		// "aaSorting" : [ [ 15, 'desc' ] ],
		"aLengthMenu" : [ [ 10, 20, 50, 100 ], [ 10, 20, 50, 100 ] ],
		// // set the initial value
		"iDisplayLength" : 10,
		"sDom" : domHead + domBody + domFooter,
	}).on('preXhr.dt', function(e, settings, json, xhr)
	{
		$(".dashboardLoadingIndicator").addClass('fa-spin fa-cog').removeClass('fa-table');
	}).on('xhr.dt', function(e, settings, json, xhr)
	{
	}).on('draw.dt', function(e, settings)
	{
		$(".dashboardLoadingIndicator").removeClass('fa-spin fa-cog').addClass('fa-table');
		bindDeleteAction();
	});

	function bindDeleteAction()
	{
		$('.btn-delete').click(function()
		{
			deleteCustomer($(this));
		})
	}

	function deleteCustomer($btn)
	{
		if ($btn.hasClass('disabled'))
		{
			return;
		}
		var id = prompt("Do you want to delete this customer and all it's folders? This action can not be undone!\nPlease enter the category ID for this customer:", "");
		if (id == $btn.attr('data-id'))
		{
			$('.btn-delete').addClass('disabled');
			$btn.text('Deleting...');
			$.ajax({
				url : '/webdirector/secure/app/delete/customer',
				data : {
					id : $btn.attr('data-id')
				},
				type : 'post'
			}).done(function(data)
			{
				if (data.success)
				{
				}
				else
				{
					alert(data.message);
				}
				dashboardTable.draw();
			})
		}
	}

});
