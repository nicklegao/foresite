<!DOCTYPE html>
<%@page import="au.net.webdirector.admin.modules.ModuleNameService"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Arrays"%>
<%
	ModuleNameService NameService = new ModuleNameService();
    String[] Module = new String[40];

    Module = NameService.getAllValidModules();

    String[] module1 = new String[Module.length];
    Hashtable h = new Hashtable();
    for (int i = 0; i < Module.length; i++) {
        module1[i] = NameService.getModuleName(Module[i]);
        h.put(module1[i],Module[i]);
    }
    Arrays.sort(module1);
%>
<html>
<head> 
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
</head>
<body class="iframe-body">

<!-- START CONTENT AREA -->
<form name="purgeModuleForm" action="purge" method="post">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Purge Module Data</h4>
</div>
<div class="modal-body">
  <div class="alert alert-warning fade in">
    <i class="fa-fw fa fa-warning"></i>
    <strong>Warning</strong> This process will delete ALL CONTENT in the selected module.
  </div>
  <div class="form-group">
    <label class="control-label">Select Module</label>
    <select class="form-control" name="ModuleID" required>
      <option value=""></option>
      <%for(int i=0;i<module1.length;i++){%>
              <option value = "<%=h.get(module1[i])%>"> <%=(module1[i])%>  </option>
        <%}%>
    </select> 
  </div>
</div>
<div class="modal-footer">
  <button type="submit" class="btn btn-default">Next</button>
</div>
</form>
<!-- END CONTENT AREA -->

<script>
$(function(){
  $('.modal-header .close').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>
