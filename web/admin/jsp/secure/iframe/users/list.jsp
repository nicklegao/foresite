<!DOCTYPE html>
<%@page import="au.net.webdirector.admin.modules.DynamicModuleController"%>
<%@page import="java.util.Map"%>
<html>
<head> 
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>

<link rel="stylesheet" type="text/css" media="screen" href="/admin/plugins/jquery.dataTables.bootstrap/css/dataTables.bootstrap.css" />
<link rel="stylesheet" href="/application/assets/plugins/sweetalert/sweetalert.css"
	type="text/css" />
<style>
div.per-page.short-select-option{
  display: inline-block;
}
.pick-date input
{
  min-width: 190px;
  text-align: center;
}
</style>
</head>
<body class="iframe-body">
<!-- START CONTENT AREA -->   
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">User List</h4>
</div>
<div class="modal-body">
  <table id="paymentTable" class="table" cellspacing="0" width="100%">
    <thead>
      <tr>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</div>
<div class="modal-footer">
  <button class="btn btn-default" id="closeBtn">Close</button>
</div>
<!-- END CONTENT AREA -->

<script src="/admin/plugins/jquery.dataTables/js/jquery.dataTables.min.js"></script>
<script src="/admin/plugins/jquery.dataTables.bootstrap/js/dataTables.bootstrap.min.js"></script>
<script src="/application/assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/admin/jsp/iframe/users/list.js"></script>
<script>
$(function(){
  $('.modal-header .close, #closeBtn').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>
