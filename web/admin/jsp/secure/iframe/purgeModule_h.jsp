<!DOCTYPE html>
<%@page import="au.net.webdirector.admin.modules.ModuleNameService"%>
<%@page import="au.net.webdirector.common.datalayer.admin.db.DBaccess"%>
<%@page import="webdirector.db.client.NotesDataAccess"%>
<%@page import="au.net.webdirector.common.datalayer.util.DatabaseValidation"%>
<%@ page import="java.util.*"%>

<html>
<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
</head>
<body class="iframe-body">

<!-- START CONTENT AREA -->
<form action="purgeDo" name="formBuilder" method="post">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Purge Module Data</h4>
</div>
<div class="modal-body">
<%
	DBaccess db = new DBaccess();
    NotesDataAccess nda = new NotesDataAccess();
    Vector v;
    String tablePostFix = request.getParameter("ModuleID");

    System.out.println("Name of Module"+ tablePostFix);
  
    int rowsAffected, rowsAffected1;

//     String query3 = "select * from Categories"+"_"+tablePostFix;
//     String query4 = "select * from Elements"+"_"+tablePostFix;
//     String query5 = "select * from drop_down_multi_selections where MODULE_NAME = '"+tablePostFix+"'";

    String query3 = "select * from Categories"+"_"+ DatabaseValidation.encodeParam(tablePostFix);
    String query4 = "select * from Elements"+"_"+ DatabaseValidation.encodeParam(tablePostFix);
    String query5 = "select * from drop_down_multi_selections where MODULE_NAME = ?";
    
    v = db.selectQuery(query3);

    ModuleNameService NameService = new ModuleNameService();
    String moduleName = NameService.getModuleName(tablePostFix);

    //rowsEffected= db.rowAffected("Categories",tablePostFix);
%>

  You are about to delete all data from <font size="4"><%=moduleName %></font>(<%=tablePostFix %>)
<br>
  Number of <font size="4">CATEGORIES</font> to be deleted: <font size="4"><%= v.size() %></font>
<br>
   <% v = db.selectQuery(query4);%>
  Number of <font size="4">ASSETS</font> to be deleted: <font size="4"><%= v.size()%></font>
<br>
  Number of <font size="4">NOTES</font> to be deleted: <font size="4"><%= nda.countNotesForModule(tablePostFix)%></font>
<br>
   <% 
   // v = db.select(query5);    
   v = db.selectQuery(query5, new String[]{tablePostFix});
   %>
  Number of <font size="4">ATTR_MULTI</font> to be deleted: <font size="4"><%= v.size()%></font>
<br>

        <input type="hidden" name="moduleID" value =<%=tablePostFix%> >
</div>
<div class="modal-footer">
  <a href="javascript:history.go(-1);" class="btn btn-default pull-left">Back</a>
  <a href="javascript:go_there();" class="btn btn-danger">Confirm Purge</a>
</div>
</form>

<!-- END CONTENT AREA -->

<script type="text/javascript">
     function go_there(){
          if(window.confirm("Are you sure, this will delete all data in this module."))
  {
    document.formBuilder.submit();
  }

  }
</script>
<script>
$(function(){
  $('.modal-header .close').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>