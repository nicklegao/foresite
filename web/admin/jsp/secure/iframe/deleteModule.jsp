<!DOCTYPE html>
<%@page import="au.net.webdirector.admin.modules.DynamicModuleController"%>
<%@page import="java.util.Map"%>
<html>
<head> 
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
</head>
<body class="iframe-body">
<!-- START CONTENT AREA -->   
<form name="deleteModuleForm" action="delete" method="post">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Delete Module</h4>
</div>
<div class="modal-body">
  <div class="alert alert-warning fade in">
    <i class="fa-fw fa fa-warning"></i>
    <strong>Warning</strong> This process will delete THE ENTIRE module and its data.
  </div>
  <div class="form-group">
    <label class="control-label">Please select a module to delete</label>
    <select class="form-control" name="deleteTarget">
      <option value=""></option>
      <%
      Map<String, String> moduleNames = DynamicModuleController.sharedInstance().getDeletableModuleNames();
      for (String internalName : moduleNames.keySet()) {%>
          <option value="<%=internalName %>"><%="["+internalName+"] "+moduleNames.get(internalName) %></option>
      <%}%>
    </select> 
  </div>
</div>
<div class="modal-footer">
  <button type="submit" class="btn btn-default">Next</button>
</div>
</form>
<!-- END CONTENT AREA -->
<script>
$(function(){
  $('.modal-header .close').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>
