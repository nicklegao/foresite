<%@page import="au.net.webdirector.common.datalayer.base.db.mapper.StringArrayMapper"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.mapper.StringMapper"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper"%>
<%@page import="au.com.ci.webdirector.audittrail.AuditTrialService"%>
<% response.setHeader("pragma", "no-cache");
response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
response.setHeader("Expires", "01 Apr 1995 01:10:10 GMT"); %>
<%@ page import="java.util.*"%>
<%@ page import="webdirector.db.client.ClientDataAccess"%>
<%@ page import="au.net.webdirector.admin.modules.ModuleNameService"%>
<%@ page import="webdirector.utils.*"%>
<%@ page import="webdirector.db.*"%>
<%@ page import="au.net.webdirector.common.datalayer.admin.db.*"%>
<%@ page import="org.apache.commons.lang.*"%>
<%@page import="au.net.webdirector.common.datalayer.util.DatabaseValidation"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
<%
    String startDate = request.getParameter("startDate");
    startDate = StringUtils.isNotBlank(startDate)?startDate:"";
    String endDate = request.getParameter("endDate");
    endDate = StringUtils.isNotBlank(endDate)?endDate:"";
    String userName = request.getParameter("userName");
    userName = StringUtils.isNotBlank(userName)?userName:"";
    String ip = request.getParameter("ip");
    ip = StringUtils.isNotBlank(ip)?ip:"";
    String module = request.getParameter("module");
    module = StringUtils.isNotBlank(module)?module:"";
    String msg = request.getParameter("msg");
    msg = StringUtils.isNotBlank(msg)?msg:"";
    
    String pageNo = request.getParameter("pageNo");
    pageNo = StringUtils.isNotBlank(pageNo)?pageNo:"1";

    String sPerPage = request.getParameter("perPage");
    sPerPage = StringUtils.isNotBlank(sPerPage)?sPerPage:"25";
    
    
    int perPage = Integer.parseInt(sPerPage);
    int startPos = (Integer.parseInt(pageNo) - 1) * perPage;
    String sql =" select * from SYS_AUDIT_TRIAL where 1=1 ";
    
    
    if(StringUtils.isNotBlank(userName)){
        sql += " and event_who = '" + DatabaseValidation.encodeParam(userName) + "'";   
    }
    if(StringUtils.isNotBlank(startDate)){
        sql += " and EVENT_WHEN >= '"+ DatabaseValidation.encodeParam(startDate) +"'";
    }
    if(StringUtils.isNotBlank(endDate)){
        sql += " and EVENT_WHEN <= '"+ DatabaseValidation.encodeParam(endDate) +"'";
    }
    if(StringUtils.isNotBlank(ip)){
        sql += " and EVENT_IP = '"+ DatabaseValidation.encodeParam(ip) +"'";
    }
    if(StringUtils.isNotBlank(module)){
        sql += " and EVENT_WHERE = '"+ DatabaseValidation.encodeParam(module) +"'";
    }
    if(StringUtils.isNotBlank(msg)){
        sql += " and EVENT_MSG like '%"+ DatabaseValidation.encodeParam(msg) +"%'";
    }
    sql += " order by event_id desc limit "+ DatabaseValidation.encodeParam(startPos) +", " + DatabaseValidation.encodeParam(perPage);
    
    DataAccess da = DataAccess.getInstance();
    Map<String, String> dict = AuditTrialService.getModuleDict();
    List<Hashtable<String, String>> trials = (List<Hashtable<String, String>>)da.select(sql, new HashtableMapper());
    
    List<String> users = da.select("select user_name from users", new StringMapper());
    List<String[]> modules = da.select("select internal_module_name, client_module_name from sys_moduleconfig where client_module_name != 'not used'", new StringArrayMapper());
    modules.add(new String[]{"Security","Security"});
    modules.add(new String[]{"Openlinks","Openlinks"});
    modules.add(new String[]{"Utilities","Utilities"});
    Collections.sort(modules, new Comparator<String[]>(){
        public int compare(String[] a, String[] b){
            return a[1].compareTo(b[1]);
        }
    });
    
    
%>
<script type="text/javascript" src="<%= request.getContextPath() %>/admin/js/jquery-ui-timepicker-addon.js"></script>
<link rel="stylesheet" href="<%= request.getContextPath() %>/admin/css/jquery-ui-timepicker-addon.css" type="text/css">
<script src="/admin/plugins/colResizable/colResizable-1.5.min.js"></script>
<style text="text/css">
.container {
	position:relative;
}
table.categorySummary{
    color: #505050;
    font-size: 8pt;
}
table.categorySummary th, table.categorySummary td {
    padding: 5px !important;
}
</style>
<style>
.title {
    color: #333333;
    font-size: 20px;
    margin-bottom: 10px;
    text-align: center;
}
.content {
	color: #333333;
	font-size: 11px;
	text-align: left;
    margin:0 3px;
}

.fixed-width{
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    font-size:11px;
}


.subject .fixed-width{
    width: 400px;
    display: block;
    cursor: pointer;
}
.categorySummary div.msg_important{
    color: orangered;
}
.categorySummary div.msg_very_important{
    color: red;
}
.categorySummary div.msg_normal{
    color: green;
}
.categorySummary div.msg_not_important{
    color: grey;
}

.filter {    
    color: #333333;
    margin: 10px;
}

.categorySummary td div.message{
    cursor: pointer;
    font-weight: normal;
}

.categorySummary div.full-message {
    border-top: 1px solid #AAAAAA;
    color: #333333;
    font-size: 0.9em;
    margin-top: 5px;
    padding-top: 5px;
}

.categorySummary input , .categorySummary select {
    height: 21px;
    width:100%;
}

input, select {
    font-size:12px;
    -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
    -moz-box-sizing: border-box;    /* Firefox, other Gecko */
    box-sizing: border-box;         /* Opera/IE 8+ */
}

.filter-label {
    display: inline-block;
    margin-left: 10px;
    width: 60px;
    text-align: right;
}

.filter-icon{
    float: right;
    cursor:pointer;
}

.filter-selected{
    color: blue;
}

.pager {
  margin: 0 auto;
  width: 250px;
}

select.form-control {
  width: auto;
  display: inline-block;
}

</style>
</head>
<body class="iframe-body">
<form method="POST">
<input type="hidden" name="pageNo" value="<%=pageNo%>">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Audit Trail</h4>
</div>
<div class="modal-body">
	<table class="categorySummary table table-striped">
		<thead>
			<tr>
                <th class="th-custom"><div style="float:left;">User</div><div class="filter-icon <%= userName.equals("")?"":"filter-selected" %>">&#9660;</div></th>
                <th class="th-custom"><div style="float:left;">IP</div><div class="filter-icon <%= ip.equals("")?"":"filter-selected" %>">&#9660;</div></th>
                <th class="th-custom"><div style="float:left;">Time</div><div class="filter-icon <%= startDate.equals("")&&endDate.equals("")?"":"filter-selected" %>">&#9660;</div></th>
                <th class="th-custom"><div style="float:left;">Module</div><div class="filter-icon <%= module.equals("")?"":"filter-selected" %>">&#9660;</div></th>
				<th class="th-custom" style="width:415px;"><div style="float:left;">Message</div><div class="filter-icon <%= msg.equals("")?"":"filter-selected" %>">&#9660;</div></th>
			</tr>
		</thead>
		<tbody>
            <tr style="display:none;">
                <td><div>
                <select name="userName" class="resetable">
                    <option value=""></option>
<% for(String who: users){ %>
                    <option value="<%= who %>" <%= who.equals(userName)?"selected":"" %>><%= who %></option>
<% } %>                    
                </select></div>
                </td>
                <td><div><input type="text" value="<%= ip %>" name="ip" class="resetable"></div></td>
                <td><div>
                <input type="text" value="<%= startDate %>" name="startDate" class="hasDateTimePicker resetable" placeholder="From">
                <input type="text" value="<%= endDate %>" name="endDate" class="hasDateTimePicker resetable" placeholder="To"></div></td>
                <td><div>
                <select name="module" class="resetable">
                    <option value=""></option>
<% for(String[] m: modules){ %>
                    <option value="<%= m[0] %>" <%= module.equals(m[0])?"selected":"" %>><%= m[1] %></option>
<% } %>                    
                </select></div>
                </td>
                <td><div><input type="text" value="<%= msg %>" name="msg" class="resetable"></div></td>
            </tr>
        <% for(Hashtable<String, String> trial: trials){ %>
            <tr>
                <td>
                    <div class="content fixed-width">
                        <%= trial.get("EVENT_WHO") %>
                    </div>
                </td>
                <td>
                    <div class="content fixed-width">
                        <%= trial.get("EVENT_IP") %>
                    </div>
                </td>
                <td>
                    <div class="content fixed-width">
                        <%= trial.get("EVENT_WHEN").substring(0, 19) %>
                    </div>
                </td>
                <td>
                    <div class="content fixed-width">
                        <%= AuditTrialService.translateModule(trial.get("EVENT_WHERE"), dict) %>
                    </div>
                </td>
                <td>
                    <div class="message fixed-width <%= AuditTrialService.getMsgClassName(trial.get("EVENT_LEVEL")) %>">
                        <%= trial.get("EVENT_MSG") %>
                    </div>
                    <div class="full-message" style="display:none;"><%= trial.get("EVENT_MSG") %></div>
                </td>
            </tr>
        <% } %>
        </tbody>
	</table>
</div>
<div class="modal-footer">
  <div class="pull-left">
    <label class="control-label">Results per page: </label>
    <select name="perPage" class="form-control">
      <option value="25" <%= perPage==25?"selected":"" %>>25</option>
      <option value="50" <%= perPage==50?"selected":"" %>>50</option>
      <option value="100" <%= perPage==100?"selected":"" %>>100</option>
    </select>
  </div>
  <div class="pull-right form-group">
    <span class="control-label">Page <%=pageNo%></span>
  </div>
  <ul class="pager">
    <li class='previous <%=startPos > 0 ? "" : "disabled" %>'>
      <a href="javascript:void(0);" class="turn-page" data-page="-1"><i class="fa fa-arrow-left"></i> Prev Page</a>
    </li>
    <li class='next <%=trials.size() == perPage ? "" : "disabled" %>'>
      <a href="javascript:void(0);" class="turn-page" data-page="1">Next Page <i class="fa fa-arrow-right"></i></a>
    </li>
  </ul>
</form>
<script>
jQuery(document).ready(function($){
	$('.hasDateTimePicker').datetimepicker({ 
		dateFormat: 'yy-mm-dd',
		timeFormat: 'hh:mm'
	});
    
	$('.categorySummary').colResizable();
	
    $('div.filter .resetable').each(function(){
        $(this).data('data-orig', $(this).val());
    });
    
  	$('.turn-page').click(function(){
  	    var pageNoInput = $('input[name=pageNo]');
  	    var d = parseInt($(this).attr('data-page'));
  	    var c = parseInt(pageNoInput.val());
  	 	pageNoInput.val(c+d);
  	 	$('div.filter .resetable').each(function(){
  	        $(this).val($(this).data('data-orig'));
  	    });
  	 	$('form').submit();
  	});
	
  	$('.categorySummary td div.message').click(function(){
  	    var full_msg = $(this).parent().find('.full-message');
  	    if(full_msg.is(':hidden')){
  	        full_msg.show();
  	        return;
  	    }
  	    full_msg.hide();
  	});
  	
  	$('select').change(function(){
  	    $('form').find('input[name=pageNo]').val('1').end().submit();
  	});
  	
  	$('input').keypress(function(event){
  	    if(event.keyCode == 13){
  	      $('form').find('input[name=pageNo]').val('1').end().submit(); 
  	    }
  	});
  	
  	$('.categorySummary th').click(function(){
  	    var index = $(this).parent().children().index($(this));
  	    var tr = $('.categorySummary tbody tr:first');
  	    var div = $(tr.children()[index]).find('div');
  	    if(div.is(':hidden')){
  	        tr.show().find('td div').hide();
  	        div.show();
  	    }else{
  	        tr.hide();
  	    }
  	});
})

</script>
<script>
$(function(){
  $('.modal-header .close').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>