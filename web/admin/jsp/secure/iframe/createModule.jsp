<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict/EN">
<%@page import="au.net.webdirector.admin.modules.DynamicModuleController"%>
<%@page import="java.util.Map"%>
<html>
<head> 
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
</head>
<meta http-equiv="expires" content="0">

<!-- START CONTENT AREA -->   
<body class="iframe-body">
<form name="createModuleFrom" method="POST" class="form-horizontal">
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Create Module</h4>
</div>
<div class="modal-body">
  <div class="form-group required">
    <label class="col-sm-3 control-label">Internal SQL Name</label>
    <div class="col-sm-8">
      <input id="internalName" type="text" class="form-control" name="internalName" maxlength="30" style="text-transform: uppercase;" required>
      <p class="note"><strong>Note:</strong> Should be UNIQUE. Cannot change in the future</p>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label">Display name</label>
    <div class="col-sm-8">
      <input id="displayName" type="text" class="form-control" name="displayName" maxlength="50">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label">Clone existing module</label>
    <div class="col-sm-1 checkbox">
      <label>
        <input type="checkbox" class="checkbox style-0" name="doClone" id="doClone">
        <span></span>
      </label>
    </div>
  </div>
  <div class="form-group required" id="cloneSelector" style="display: none">
    <label class="col-sm-3 control-label">Module to clone</label>
    <div class="col-sm-8">          
      <select class="form-control" name="cloneTarget">
        <%
        Map<String, String> moduleNames = DynamicModuleController.sharedInstance().getModuleNameLookup();
        for (String internalName : moduleNames.keySet()) {%>
            <option value = "<%=internalName %>"><%="["+internalName+"] "+moduleNames.get(internalName) %></option>
        <%}%>
      </select>
      <p class="note"><strong>Note:</strong> Data will not be copied</p>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label">Enable Version Control</label>
    <div class="col-sm-1 checkbox">
      <label>
        <input type="checkbox" class="checkbox style-0" name="versionControlEnabled">
        <span></span>
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label">Enable Workflow</label>
    <div class="col-sm-1 checkbox">
      <label>
        <input type="checkbox" class="checkbox style-0" name="workflowEnabled">
        <span></span>
      </label>
    </div>
  </div>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-primary">Next</button>
</div>
</form>
<!-- END CONTENT AREA -->
<script type="text/javascript">
	$(
	document).ready(function()
	{
		$("#doClone").click(function()
		{
			if ($('#doClone:checked').val() !== undefined)
			{
				$("#cloneSelector").css("display", "block");
			}
			else
			{
				$("#cloneSelector").css("display", "none");
			}
		});

		$("#internalName").on("keyup change", function(e)
		{
			var internalName = $("#internalName").val();
			internalName = internalName.replace(new RegExp("[^a-zA-Z0-9_]+"), "");
			var displayName = toTitleCase(internalName);
			$("#displayName").attr("placeholder", displayName);
		});
	});

	function toTitleCase(str)
	{
		return str.replace(/\w\S*/g, function(txt)
		{
			return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
		});
	}
</script>
<script>
	$(function()
	{
		$('.modal-header .close').click(function()
		{
			top.ModalHelper.closeModal();
		});
	})
</script>
</body>
</html>
