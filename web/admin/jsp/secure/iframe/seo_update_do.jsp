<!DOCTYPE html>
<%@page import="webdirector.seo.UrlRewriteFileUtils"%>
<%@page import="webdirector.seo.CustomizedShortUrlGenerator"%>
<%@page import="java.util.List"%>
<%@page import="webdirector.seo.UrlMapping"%>
<%@page import="java.util.ArrayList"%>
<html>
<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp"%>
</head>
<body class="iframe-body">

<%
String[] froms = request.getParameterValues("mappingfrom");
String[] tos = request.getParameterValues("mappingto");
String[] lasts = request.getParameterValues("mappinglast");
String[] external = request.getParameterValues("mappingexternal");
List<UrlMapping> mappings = new ArrayList<UrlMapping>();
for(int i=0;i<froms.length;i++){
	if(froms[i].equals("")){
		continue;
	}
	UrlMapping mapping = new UrlMapping("customized");
	mapping.setFrom(froms[i]);
	mapping.setTo(tos[i]);
    mapping.setLast(lasts[i]);
    mapping.setPermanentRedirect("true".equals(external[i]));
	mappings.add(mapping);
}
CustomizedShortUrlGenerator.writeFile(mappings, application);
%>
<%
      UrlRewriteFileUtils u = new UrlRewriteFileUtils();
      u.refreshShortUrls();
      u.writeFile();
      String rulesText = u.urlRulesToString(true);
%>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Short URL Rules</h4>
</div>
<div class="modal-body">
  <p><%= u.getCount()%> rules updated:</p>
  <p style="font-family:courier;"><%=rulesText%></p>
</div>
<div class="modal-footer">
  <a href="update" class="btn btn-default pull-left">Back</a>
</div>
<script>
$(function(){
  $('.modal-header .close').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>