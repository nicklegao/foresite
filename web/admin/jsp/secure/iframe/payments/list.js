$(document).ready(function()
{
  var domHead = "<'navbar' <'navbar-form navbar-left'<'form-group show-page' l>> <'navbar-form navbar-left'<'form-group pick-date'>> <'navbar-form navbar-right'<'form-group pick-select'>> >";
	var domBody = "<'responsive-table-wrapper' t>";
	var domFooter = "<'navbar' <'navbar-form navbar-left' i> <'navbar-form navbar-right'<'form-group pagination-bar' p>> >";

	var dateRangePicker = undefined;// Setup later!
	var dashboardTable = $('#paymentTable').DataTable({
		"aaSorting" : [[0, "asc"]],
		"autoWidth" : true,
		"processing" : true,
		"serverSide" : true,
		"ajax" : {
			"url" : "/webdirector/secure/app/data/payments",
			"method" : "POST",
			"data" : function(d)
			{
				if (dateRangePicker != undefined)
				{
					var picker = dateRangePicker.data('daterangepicker');
					d.startDate = picker.startDate.format('YYYY-MM-DD');
					d.endDate = picker.endDate.format('YYYY-MM-DD');
				}
				else
				{
					d.startDate = moment().subtract(1, 'month').format('YYYY-MM-DD');
					d.endDate = moment().format('YYYY-MM-DD');
				}
				var $processed = $('select[name="processed"]');
				if ($processed.length == 0)
        {
          d.processed = 'N';
        }
        else
        {
          d.processed = $processed.val();
        }
			}
		},
		"aoColumns" : [ {
		  title : 'Select',
      data : 'element_id',
			render : function(data, type, row)
			{
				if (type == 'display')
				{
				  if(row.processed == 'Y'){
				    return '';
				  }
					var btn = $('<input type="checkbox" name="id" value="'+data+'" checked>').attr("title", row.attr_headline);
					return $("<div>").append(btn).html();
				}
				else
				{
					return row.element_id;
				}
			}
		}, {
			title : 'Processed Time',
			data : 'attrdate_processedtime',
			render : function(data, type, row)
			{
				return formatDate(row.attrdate_processedtime);
			}
		}, {
		  title : 'Customer',
		  data : 'attr_companyname',
		  render : function(data, type, row)
		  {
		    return row.attr_companyname;
		  }
		}, {
			title : 'Created On',
			data : 'attrdate_created',
			render : function(data, type, row)
			{
				return formatDate(row.attrdate_created);
			}
		}, {
		  title : 'Available On',
		  data : 'attrdate_availableon',
		  render : function(data, type, row)
		  {
		    return formatDate(row.attrdate_availableon);
		  }
    }, {
      title : 'Fee',
      data : 'attrcurrency_fee',
      render : function(data, type, row)
      {
        if (type == 'display')
        {
          return formatMoney(row.attr_currency, row.attrcurrency_fee)
        }
        else
        {
          return row.attrcurrency_fee;
        }
      }
    }, {
      title : 'Received',
      data : 'attrcurrency_net',
      render : function(data, type, row)
      {
        if (type == 'display')
        {
          var span = $('<span class="ppcharge">').attr('data-charge', row.attrcurrency_net).text(formatMoney(row.attr_currency, row.attrcurrency_net));
          return $("<div>").append(span).html();
        }
        else
        {
          return row.attrcurrency_net;
        }
      }
		}, {
		  title : 'GST',
		  data : 'attrcurrency_gst',
		  render : function(data, type, row)
		  {
		    if (type == 'display')
		    {
		      var span = $('<span class="gst">').attr('data-gst', row.attrcurrency_gst).text(formatMoney(row.attr_currency, row.attrcurrency_gst));
		      return $("<div>").append(span).html();
		    }
		    else
		    {
		      return row.attrcurrency_gst;
		    }
		  }
		}],
		"oLanguage" : {
			"sLengthMenu" : '<label>Show</label> <div class="per-page short-select-option"> _MENU_ </div> <span>per page</span>',
			"sSearch" : "Customer: ",
			"oPaginate" : {
				"sPrevious" : '<i class="fa fa-arrow-left"></i>',
				"sNext" : '<i class="fa fa-arrow-right"></i>'
			},
			"sInfo" : '<i class="fa fa-spin fa-cog dashboardLoadingIndicator"></i> Showing _START_ to _END_ of _TOTAL_ payments',
			"sInfoEmpty" : "No payments found",
		},
//		"aaSorting" : [ [ 15, 'desc' ] ],
		"aLengthMenu" : [ [10, 20, 50, 100 ], [10, 20, 50, 100 ] ],
//		// set the initial value
		"iDisplayLength" : 10,
		"sDom" : domHead + domBody + domFooter,
	}).on('preXhr.dt', function ( e, settings, json, xhr ) {
		$(".dashboardLoadingIndicator").addClass('fa-spin fa-cog').removeClass('fa-table');
	}).on('xhr.dt', function ( e, settings, json, xhr ) {
	}).on('draw.dt', function ( e, settings ) {
		$(".dashboardLoadingIndicator").removeClass('fa-spin fa-cog').addClass('fa-table');
	});


	initSelector();
  initDatePicker();
  
  function initSelector(){
    var $processed = $('<select class="form-control" name="processed">').appendTo($(".pick-select"));
    $processed.change(function(){
      dashboardTable.draw();
    })
    $processed.append('<option value="N">Not Processed</option>');
    $processed.append('<option value="Y">Processed</option>');
  }
  function initDatePicker()
  {
    /*
     * Configure the dateRangePicker
     */
    var start = moment().subtract(1, 'month');
    var end = moment();
    var defaultFormat = start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY');
    $(".pick-date").addClass("greentech-panel-group").append('<input type="text" class="form-control" name="daterange" value="'+defaultFormat+'" />');
    
    dateRangePicker = $(".pick-date input").daterangepicker({
      "locale": {
        "format": "DD/MM/YYYY"
      },
      "autoApply": true,
      "autoUpdateInput": true,
      "alwaysShowCalendars": true,
      "ranges": {
        'Today': [moment(), moment()],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        '1 Month': [moment().subtract(1, 'month'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        '3 Months': [moment().subtract(3, 'month'), moment()],
        '6 Months': [moment().subtract(6, 'month'), moment()],
        '1 Year': [moment().subtract(12, 'month'), moment()]
      }
    }).on('apply.daterangepicker', function(ev, picker) {
      dashboardTable.draw();
    });
  }
  
  function formatMoney(code, money){
    return code + " " + money.toFixed(2);
  }
  
  function formatDate(date){
    if(date == null || date == ''){
      return '';
    }
    var d = moment(date).format("DD/MM/YYYY hh:mm:ss");
    if(d == '01/01/1900 01:00:00'){
      return '';
    }
    return d;
  }
	
  $('#saveBtn').click(function(){
    var total = 0.0;
    var gst = 0.0
    var ids = [];
    $('[name="id"]:checked').each(function(){
      ids.push($(this).val());
      total += parseFloat($(this).closest('tr').find('.ppcharge').data('charge'), 10);
      gst += parseFloat($(this).closest('tr').find('.gst').data('gst'), 10);
    })
    if(ids.length == 0){
      alert("Please choose payment first.");
      return;
    }
    top.$.SmartMessageBox({
      title : "Are you sure",
      content : "Do you want to mark these payments as processed? <br> Total amount: <strong style='color:yellow;font-size:1.5em;'>" + total.toFixed(2) + "</strong> <br> Total GST: <strong style='color:yellow;font-size:1.5em;'>" + gst.toFixed(2) + "</strong>",
      buttons : '[No][Yes]'
    }, function(ButtonPressed) {
      if (ButtonPressed === "Yes") {
        $.ajax({
          url: '/webdirector/secure/app/mark/payments',
          type: 'POST',
          data:{ids: ids.join(','), total: total, gst: gst}
        }).done(function(){
          dashboardTable.draw();
        })
      }
    });
  })
  
  
  function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
  }
});
