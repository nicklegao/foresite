<!DOCTYPE html>
<%@page import="au.net.webdirector.admin.security.LoginController"%>
<%@page import="au.net.webdirector.common.utils.password.PasswordUtils"%>
<%@page import="org.apache.commons.lang.WordUtils"%>
<%@page import="au.net.webdirector.admin.modules.DynamicModuleController"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<html>
<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
</head>
<body class="iframe-body">

<%
String error = null;
DynamicModuleController moduleController = DynamicModuleController.sharedInstance();
String internalName = request.getParameter("internalName");
String displayName = request.getParameter("displayName");

String doClone = request.getParameter("doClone");
String cloneTarget = request.getParameter("cloneTarget");

String versionControlEnabled = request.getParameter("versionControlEnabled");
String workflowEnabled = request.getParameter("workflowEnabled");

String confirmed = request.getParameter("confirmed");
String expectedConfirmation = "";
boolean createAttempted = false;

if (internalName == null || internalName.trim().length() == 0)
{
    error = "Missing Internal Name of new module.";
}
else
{
    //clean input
    if (displayName == null)
    {
        displayName = "";
    }
    else
    {
        displayName = WordUtils.capitalizeFully(displayName.replaceAll("[^a-zA-Z0-9_]+", "")).trim();
    }
    
    internalName = internalName.replaceAll("[^a-zA-Z0-9_]+", "").trim();
    
    if (displayName.length() == 0)
    {
        // optional value gets auto generated
      displayName = WordUtils.capitalizeFully(internalName);
    }

    //make internal name db friendly
    internalName = internalName.toUpperCase()
           .replaceAll("\\s+", "_")
               .replaceAll("_+", "_");
    if (internalName.length() == 0)
    {
        error = "Missing Internal Name of new module.";
    }
    
    if (error == null)
    {
        //Check internal data pre-conditions
      error = moduleController.checkPreconditionsForModuleCreation(internalName, displayName, cloneTarget);
    }
    
    expectedConfirmation = PasswordUtils.encrypt(internalName+"-"+displayName);
    // Do the magic!
    if (error == null && confirmed != null && confirmed.equals(expectedConfirmation))
    {
      createAttempted = true;
        if (doClone != null)
        {
            error = moduleController.cloneModuleStructure(internalName, displayName, versionControlEnabled != null, workflowEnabled != null, cloneTarget, (String) session.getAttribute(LoginController.WD_USER_ID));
        }
        else
        {
          error = moduleController.createTemplatedModule(internalName, displayName, versionControlEnabled != null, workflowEnabled != null, (String) session.getAttribute(LoginController.WD_USER_ID));
        }
        
        //Update audit trial
        request.setAttribute("AuditTrial_CreatedSuccessful", error==null);
    }
}

%>

<!-- START CONTENT AREA -->
<div class="modal-header">
  <h4 class="modal-title">Create Module</h4>
</div> 

<% if (error == null && createAttempted == true) { %>

  <div class="modal-body">
    <div class="alert alert-success alert-block">
      <h4 class="alert-heading">Success</h4>
      Module Successfully Created. Internal name: <%=internalName %>, Display name: <%=displayName %>
    </div>
  </div>
  <div class="modal-footer">
  </div>
  <script type="text/javascript">
  	top.moduleCreateDelete = true;
  </script>
    
<% } else { %>

<div class="modal-body">
    <TABLE border="0" align="center" width="90%" cellspacing="0" cellpadding="0" class="tableContainer">
    <tr>
       <td style="padding:20px;">
        <table width="800" border="0" cellspacing="0" cellpadding="0" align="center" class="tableContentContainer">
      <tr>
            <td class="FormCell-L_Align">
            <% if (error != null) { %>
                
                    Unable to create module. Reason: <%=error %>
                <br>
                <br>
                    Press back to try again.
    
            <% } else { %>
            
                <form name="confirmationForm" method="post">
                    <input type="hidden" name="displayName" value="<%=displayName %>"/>
                    <input type="hidden" name="internalName" value="<%=internalName %>"/>
                    <% if (doClone != null){ %>
                        <input type="hidden" name="doClone" value="<%=doClone %>"/>
                        <input type="hidden" name="cloneTarget" value="<%=cloneTarget %>"/>
                    <% } %>
                    <% if (versionControlEnabled != null){ %>
                        <input type="hidden" name="versionControlEnabled" value="<%=versionControlEnabled %>"/>
                    <% } %>
                    <% if (workflowEnabled != null){ %>
                        <input type="hidden" name="workflowEnabled" value="<%=workflowEnabled %>"/>
                    <% } %>
                    <input type="hidden" name="confirmed" value="<%=expectedConfirmation %>"/>
                    
                        You are about to create a module called <font size="4"><%=displayName %></font>
                    
                    <% if (doClone != null){ %>
                        <br>
                            The structure of the <font size="4"><%=cloneTarget %></font> module will be cloned. Data will not be migrated.
                    <% } else { %>
                        <br>
                            The module will be empty.
                    <% } %>
    
                    <br>
                        The module will be internally named <font size="4"><%=internalName %></font>. You can <b>not</b> change this in the future.
                    <br>
                    
                    <span id="countdownAlert">
                        You can press confirm in <font size="4" class="countdownRemaining">3</font> seconds.
                    </span>
                </form>
                    
            <% } %>
            
            </td>
      </tr>
      </table>
      </td>
      </tr>
    </TABLE>
</div>
<div class="modal-footer">
  <a href="create" class="btn btn-default pull-left">Back</a>
    <% if (error == null && !createAttempted) { %>
      <button class="btn btn-success disabled" id="confirmBtn">Confirm (<span class="countdownRemaining">3</span>)</button>
    <% } %>
</div>
<% } %>

<!-- END CONTENT AREA -->

<script type="text/javascript">
$(document).ready(function(){ 
  setTimeout(tick, 1000);
});

function doConfirm()
{
  $('#confirmBtn').addClass('disabled');
  document.forms["confirmationForm"].submit();
}

function tick()
{
  var remaining = parseInt($(".countdownRemaining").first().text()) - 1;
  $(".countdownRemaining").text(remaining)
  if (remaining <= 0)
{
        $('#confirmBtn').click(doConfirm);
        $('#confirmBtn').removeClass('disabled');
        $('#confirmBtn').text("Confirm");
    //$("#countdownAlert").hide(); 
}
  else
{
        setTimeout(tick, 1000);
}
}
</script>
<script>
$(function(){
  $('.modal-header .close').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>
