<!DOCTYPE html>
<%@page import="au.net.webdirector.common.utils.password.PasswordUtils"%>
<%@page import="au.net.webdirector.admin.modules.DynamicModuleController"%>
<html>
<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
</head>

<%
String error = null;
DynamicModuleController moduleController = DynamicModuleController.sharedInstance();
String deleteTarget = request.getParameter("deleteTarget");

String confirmed = request.getParameter("confirmed");
String expectedConfirmation = "";
boolean deleteAttempted = false;

if (deleteTarget == null || deleteTarget.trim().length() == 0)
{
    error = "Missing name of module to delete.";
}
else
{
    //Check internal data pre-conditions
  error = moduleController.checkPreconditionsForModuleDeletion(deleteTarget);
    
    expectedConfirmation = PasswordUtils.encrypt(deleteTarget);
    // Do the magic!
    if (error == null && confirmed != null && confirmed.equals(expectedConfirmation))
    {
      deleteAttempted = true;
      error = moduleController.deleteModule(deleteTarget);
        
        //Update audit trial
        request.setAttribute("AuditTrial_DeletedSuccessful", error==null);
    }
}

%>

<body class="iframe-body">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Delete Module</h4>
</div>

<!-- START CONTENT AREA -->   
<% if (error == null && deleteAttempted == true) { %>

  <div class="modal-body">
    <div class="alert alert-success alert-block">
      <h4 class="alert-heading">Success</h4>
      Module Successfully Deleted. Internal name: <%=deleteTarget %>
    </div>
  </div>
  <div class="modal-footer">
  </div>
    <script type="text/javascript">
  	top.moduleCreateDelete = true;
  </script>
<% } else { %>

  <form name="confirmationForm" method="post">
  <div class="modal-body">
    <% if (error != null) { %>
      <p class="txt-color-red">
        Unable to delete module. Reason: <%=error %>
      </p>
      <p class="txt-color-red">
        Press back to try again.
      </p>
  
    <% } else { %>
    
      <input type="hidden" name="deleteTarget" value="<%=deleteTarget %>"/>
      <input type="hidden" name="confirmed" value="<%=expectedConfirmation %>"/>
      <p>You are about to delete the module with internal name <font size="4"><%=deleteTarget %></font></p>
      <p id="countdownAlert">You can press confirm in <font size="4" class="countdownRemaining">3</font> seconds.
      </p>
        
      <script>
        $(document).ready(function() {
          setTimeout(tick, 1000);
        });
    
        function doConfirm() {
          $('#confirmBtn').addClass('disabled');
          document.forms["confirmationForm"].submit();
        }
    
        function tick() {
          var remaining = parseInt($(".countdownRemaining").first().text()) - 1;
          $(".countdownRemaining").text(remaining)
          if (remaining <= 0) {
            $('#confirmBtn').click(doConfirm);
            $('#confirmBtn').removeClass('disabled');
            $("#confirmBtn").text("Confirm");
            $("#countdownAlert").hide();
          } else {
            setTimeout(tick, 1000);
          }
        }
      </script>
    <% } %>
  </div>
  <div class="modal-footer">
    <a href="delete" class="btn btn-default pull-left">Back</a>
    <% if (error == null && !deleteAttempted) { %>
      <a id="confirmBtn" class="btn btn-danger disabled">Confirm (<span class="countdownRemaining">3</span>)</a>
    <% } %>
  </div>
  </form>

<% } %>
<!-- END CONTENT AREA -->

<script>
$(function(){
  $('.modal-header .close').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>
