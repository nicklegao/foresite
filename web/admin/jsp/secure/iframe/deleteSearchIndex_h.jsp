<!DOCTYPE html>
<%@page import="au.net.webdirector.common.search.SearchUtils"%>
<html>
<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
</head>
<body class="iframe-body">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Index Content</h4>
</div>
<div class="modal-body">

<%
String emailAddress = request.getParameter("Email");
String[] moduleNames = request.getParameterValues("ModuleID");
boolean flag = true;
for(String moduleName : moduleNames){
  flag = new SearchUtils().deleteIndexFile(moduleName,emailAddress) && flag;
}
if(flag){%>
  <div class="result-msg success">
    Content Indexing process has started.<br>
	<br>
	An email will be sent to you on completion.
  </div>
<%}else {%>
  <div class="result-msg error">
    ERROR: There was an error while attempting to start the content indexing process.<br>
	<br>
	If the problem persists please contact Corporate Interactive Technical Support.
  </div>
<%}%>
  <style>
  .success{
    color:#116611;
  }
  .error {
    color:#991111;
  }
  .result-msg{
    background:#fff;
    border:1px solid #999;
    border-radius:10px;
    padding:30px 10px;
    width:320px;
    text-align:center;
    margin:100px auto;
    font-weight:bold;
    font-family:sans-serif;
    opacity:0.9;
    display:none;
  }
  </style>
  <script type="text/javascript">
    $(document).ready( function() {
       $('.result-msg').hide().fadeIn();
    });
  </script>
<!-- END CONTENT AREA -->

</div>
<div class="modal-footer"></div>
<script>
$(function(){
  $('.modal-header .close').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>