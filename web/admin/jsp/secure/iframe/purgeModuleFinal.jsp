<!DOCTYPE html>
<%@page import="au.net.webdirector.admin.modules.DynamicModuleController"%>
<%@page import="au.net.webdirector.common.datalayer.util.DatabaseValidation"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<html>
<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
<%
Defaults d = Defaults.getInstance();

String tablePostFix = (String) request.getParameter("moduleID");
tablePostFix = DatabaseValidation.encodeParam(tablePostFix);

String error = DynamicModuleController.sharedInstance().purgeModule(tablePostFix);

//Update audit trial
request.setAttribute("AuditTrial_PurgeSuccessful", error == null);
%>
</head>
<body class="iframe-body">

<!-- START CONTENT AREA -->
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Purge Module Data</h4>
</div>
<div class="modal-body">
  <% if(error == null){ %>
    <div class="alert alert-success alert-block">
      <h4 class="alert-heading">Success</h4>
      Process Complete for Module: <font size="4"><%=tablePostFix %></font>
    </div>
  <% }else{ %>
    <div class="alert alert-error alert-block">
      <h4 class="alert-heading">Failed</h4>
       Unable to Purge Module: <font size="4"><%=tablePostFix %></font><br>
       <%= error %>
    </div>
  <% } %>  
</div>
<div class="modal-footer">
</div>
<!-- END CONTENT AREA -->

<script>
$(function(){
  $('.modal-header .close').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>
