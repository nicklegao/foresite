<!DOCTYPE html>
<%@page import="java.util.*"%>
<%@ page import="java.util.Vector,webdirector.db.client.UserDetails,au.net.webdirector.common.utils.FileUtils,au.net.webdirector.common.Defaults,java.io.*"%>
<jsp:useBean id="db" class="au.net.webdirector.common.datalayer.admin.db.DBaccess" scope="page"/>
<jsp:setProperty name="db" property="*" />
<jsp:useBean id="DA" class="webdirector.db.client.ClientDataAccess" scope="session" />
<jsp:setProperty name="DA" property="*" />
<jsp:useBean id="ns" class="au.net.webdirector.admin.modules.ModuleNameService" scope="session"/>
<jsp:setProperty name="ns" property="*" />

<%
	String moduleName = request.getParameter("moduleName");
String moduleDescription = request.getParameter("moduleDescription");

Defaults d = Defaults.getInstance();
String mapDir = d.getStoreDir() + "/_importFileMapping";
List<String> mappingFileList = new ArrayList<String>();
try {
  File folder = new File(mapDir);
  for (File file : folder.listFiles()) {
    if (file.isFile())
      mappingFileList.add(file.getName());
  }
} catch (Exception e) {
    e.printStackTrace();
}

int catNos=0,assetNos=0;
Vector vtCols = DA.allCategories(moduleName);
if(vtCols!=null)
  catNos = vtCols.size();
Vector vtAsset = DA.allElements(moduleName);
if(vtAsset!=null)
  assetNos = vtAsset.size();

String importFile = (String)request.getParameter("importFile");
%>

<html>
<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
</head>
<body class="iframe-body">
<form ENCTYPE="multipart/form-data" name=startimport action="/webdirector/secure/data/import/upload" method=POST name=csv_import class="form-horizontal">
<input type=hidden name="formName" value="data_csv_import.jsp">

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Import Data</h4>
</div>
<div class="modal-body">
  <div class="form-group">
    <label class="col-sm-3 control-label">Destination Module</label>
    <div class="col-sm-8">
      <input type="text" class="form-control input-sm" value="<%=moduleDescription%>" readonly>
      <input type="hidden" name="moduleName" value="<%=moduleName%>">
    </div>
  </div>
    <div class="form-group">
    <label class="col-sm-3 control-label">Import File (CSV Format)</label>
    <div class="col-sm-8">
      <input type="file" class="btn btn-default" name="importFile">
      <p class="help-block">
        Please ensure the file selected here is a valid CSV (comma seperated values) file.  No other file format can be processed using this utility.
      </p>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label">Import Type</label>
    <div class="col-sm-8">
      <label class="radio radio-inline">
        <input type="radio" name="importType" class="radiobox style-0" value="Asset" checked>
        <span>Assets</span> 
      </label>
      <label class="radio radio-inline">
        <input type="radio" name="importType" class="radiobox style-0" value="Category">
        <span>Categories</span>  
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label">Upon import make assets LIVE?</label>
    <div class="col-sm-8">
      <label class="radio radio-inline">
        <input type="radio" name="assetLive" class="radiobox style-0" value="1">
        <span>Yes</span> 
      </label>
      <label class="radio radio-inline">
        <input type="radio" name="assetLive" class="radiobox style-0" value="0" checked>
        <span>No</span>  
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label">Delete all content before importing?</label>
    <div class="col-sm-1">
      <label class="checkbox-inline">
        <input type="checkbox" class="checkbox style-3" name="deleteFirst" value="1"><span></span>
      </label>
    </div>
    <p class="note col-sm-7"><strong>Warning:</strong> If you tick this check box, ALL content in the selected module will be deleted prior to this data import file being processed.  Make sure you are happy to delete all data prior to performing this task and also make sure there is a valid backup of this data in case you need to recover from this action.</p>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label">Switch off mandatory attribute validation?</label>
    <div class="col-sm-8">
      <label class="radio radio-inline">
        <input type="radio" name="switchOff" class="radiobox style-0" value="1" checked>
        <span>Yes</span> 
      </label>
      <label class="radio radio-inline">
        <input type="radio" name="switchOff" class="radiobox style-0" value="0">
        <span>No</span>  
      </label>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label" for="select-1">Mapping File</label>
    <div class="col-sm-6">
      <select class="form-control input-sm" name="importFileFormat">
        <option value=""></option>
        <% for(String fileName : mappingFileList){ %>
        <option value="<%= fileName %>"><%= fileName %></option>
        <% } %>
      </select>
      <p class="note">Load an existing Data Mapping file to use for this data import. This will automatically set the column mapping between the import file and the data attributes in the destination module.</p> 
    </div>
    <div class="col-sm-2">
    <button type="button" class="btn btn-danger btn-sm" onclick="deleteFile()">Delete Selected File</button>
    </div>
  </div>
</div>
<div class="modal-footer">
  <a href="javascript:validate();" class="btn btn-primary">Start Import</a>
</div>
<input type="hidden" name="assetNos" value="<%=assetNos%>">
<input type="hidden" name="catNos" value="<%=catNos%>">
</form>
<!-- END CONTENT AREA -->


  <script>
    function validate()
    {
      var form = document.startimport;
      var file = new String(form.importFile.value)
      //alert(file.substring(file.length-4))
      if(form.importFile.value=="")
      {
        alert("Please select CVS file to start import process.");
      }
      else if(file.substring(file.length-4).toLowerCase()!=".csv")
      {
        alert("Please select ONLY CSV file to import");
      }
      else if((form.importType[0].checked==false & form.importType[1].checked==false))
      {
        alert("Please select import type.");
      }
      else if((form.assetLive[0].checked==false & form.assetLive[1].checked==false))
      {
        alert("Please select Asset Live option.");
      }
      else if(form.deleteFirst.checked==true)
      {
        if(form.importType[0].checked==true)
        {
          if(confirm("This will delete "+form.assetNos.value +" assets.\nAre you sure you want to delete all content before importing?"))
            document.startimport.submit();
        }
        else
        {
          if(confirm("This will delete "+form.assetNos.value +" assets and "+form.catNos.value+" categories.\nAre you sure you want to delete all content before importing?"))
            document.startimport.submit();
        }

      }
      else
        document.startimport.submit();
    }

  function deleteFile()
  {
    var $select = $('[name="importFileFormat"]');
    if($select.val()=="")
    {
      alert("Please select file to delete.");
      return;
    }
    $.post("/webdirector/secure/data/import/delete-map-file?importFileFormat=" + encodeURI(document.startimport.importFileFormat.value), function(data){
      if(data){
        $select.find("option:checked").remove();
        alert("Mapping file deleted!");
      }else{
        alert("Could not delete mapping file!");
      }
    });
  }
  </script>
<script>
$(function(){
  $('.modal-header .close').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>