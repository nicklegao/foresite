<!DOCTYPE html>
<%@ page import="java.util.*" %>
<%@ page import="webdirector.db.DButils" %>
<%@ page import="au.net.webdirector.common.Defaults"%>

<jsp:useBean id="CSV" class="webdirector.db.DBexport" scope="session" />
<jsp:setProperty name="CSV" property="*" />
<jsp:useBean id="email" class="au.net.webdirector.common.utils.email.EmailUtils" scope="session" />
<jsp:setProperty name="email" property="*" />
<%@ page import="au.net.webdirector.common.datalayer.admin.db.DataAccess,au.net.webdirector.common.datatypes.domain.*" %>
<jsp:useBean id="dataTypes" class="au.net.webdirector.common.datatypes.service.DataTypesService" />

<html>
<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
<link rel="stylesheet" type="text/css" media="screen" href="/admin/plugins/jqueryui.multiselect/ui.multiselect.css">
<style>
legend {
  margin-bottom: 0;
  padding-top: 18px;
}
label input[type="checkbox"].checkbox:checked + span {
  font-weight: normal;
}
.modal-body {
  padding-left: 40px;
  padding-right: 40px;
}
</style>
</head>

<%
	Defaults d = Defaults.getInstance();

    String moduleName = request.getParameter("module");
    DButils db = new DButils("Categories" + "_" + moduleName);
    if (moduleName == null) {
        moduleName = "SUBSCRIBE";
    }

    List<DataType> labels = dataTypes.loadGenericDataTypes(moduleName,true,null);

    String categoryId = request.getParameter("categoryId");
    String elementId = request.getParameter("elementId");
    String retInfo = "Please select which columns to export...";
    boolean exportedData = "yes".equals(request.getParameter("exportNow"));

    System.out.println("now=" + new Date().toString());
    System.out.println("moduleName=" + moduleName);
    System.out.println("elementId=" + elementId);
    System.out.println("categoryId=" + categoryId);
    System.out.println("exportedData=" + exportedData);

    if (exportedData) {
        String fieldsList[] = null;
        Vector flv = new Vector(labels.size());
        boolean folderLevelQuery = false;
        int folderLevels = d.getModuleLevelsInt(moduleName);
        if ("1".equals(request.getParameter("fieldCatNames"))) {
            folderLevelQuery = true;
            for (int theLevels = 1; theLevels <= folderLevels; theLevels++) {
                flv.addElement("c" + theLevels + ".ATTR_categoryName");
            }
        }
        if ("1".equals(request.getParameter("fieldCatIds"))) {
            folderLevelQuery = true;
            for (int theLevels = 1; theLevels <= folderLevels; theLevels++) {
                flv.addElement("c" + theLevels + ".category_id");
            }
        }
        boolean exportOnlyLive = false;
        if ("1".equals(request.getParameter("fieldExpire_date"))) {
            flv.addElement("c.Expire_date");
        }
        if ("1".equals(request.getParameter("fielddisplay_order"))) {
            flv.addElement("c.display_order");
        }
        if ("1".equals(request.getParameter("fieldCreate_date"))) {
            flv.addElement("c.Create_date");
        }

        /*
        for (int eachField = 0; eachField < labels.size(); eachField++) {
          DataType col = labels.get(eachField);
          if ("1".equals(request.getParameter("f" + col.getLabelId()))) {
              flv.addElement("c." + col.getInternalName());
              System.out.println("exporting field#" + col.getLabelId() + " called " + col.getInternalName());
          }
        }
        */
        for(String dataItem : request.getParameterValues("data_items")){
          flv.addElement("c." + dataItem);
        }

        fieldsList = new String[flv.size()];
        flv.copyInto(fieldsList);
        int recCount = 0;

        // if we want a root export (all catgeories) do it here...
        if ("1".equals(request.getParameter("fieldRootExport"))) {
            categoryId = "0";
        }

        System.out.println("moduleName=" + moduleName + ", category=" + categoryId);

        // this is the main export routine
        recCount = CSV.exportElementFieldsToCSVByTreeTraversal(moduleName,
                fieldsList,
                categoryId,
                "1".equals(request.getParameter("fieldNames")),
                folderLevels,
                exportOnlyLive, true);

        retInfo = "" + recCount + " record(s) exported sucessfully.";
    }
%>
<body class="iframe-body">
<div class="modal-header modal-tabs">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Export Category Data</h4>
  <ul class="nav nav-tabs tabs-pull-right">
    <li>
      <a href="#" onclick="javascript:window.location.href='/webdirector/secure/module/iframe/export-elements?module=<%=moduleName%>&categoryId=<%=categoryId%>'">Export Elements</a>
    </li>
    <li class="active">
      <a href="#">Export Categories</a>
    </li>
  </ul>
</div>

<!-- START CONTENT AREA -->

<%
    if(!exportedData)
    {
%>

<form method=post name=exportnow action="export-categories?module=<%=moduleName%>&exportNow=yes<%= categoryId!=null ? "&categoryId="+categoryId : ""%><%= elementId!=null ? "&elementId="+elementId : ""%>" >
<input type=hidden name='module' value='<%=moduleName%>'>
<input type=hidden name='exportNow' value='yes'>
<div class="modal-body form-horizontal">
  <fieldset>
    <legend style="border-bottom:0"><%=moduleName%> Data Items</legend>
    <select name="data_items" multiple class="multiselect">
      <% for (DataType col : labels){ %>
      <option value="<%= col.getInternalName() %>"><%= col.getExternalName()+" ("+col.getInternalName()+")" %></option>
      <% } %>
    </select>
  </fieldset>
  <fieldset>
    <legend>Category Data</legend>
    <div class="checkbox"><label>
      <input type="checkbox" class="checkbox style-0" name="fieldCatIds" value="1" checked>
      <span>Export all category hierarchy ids</span>
    </label></div>
    <div class="checkbox"><label>
      <input type="checkbox" class="checkbox style-0" name="fieldCatNames" value="1" checked>
      <span>Export all category hierarchy names</span>
    </label></div>
  </fieldset>
  <fieldset>
    <legend>System Data</legend>
    <div class="checkbox"><label>
      <input type="checkbox" class="checkbox style-0" name="fieldExpire_date" value="1">
      <span>Export 'expire date' field for each record</span>
    </label></div>
    <div class="checkbox"><label>
      <input type="checkbox" class="checkbox style-0" name="fielddisplay_order" value="1">
      <span>Export 'display order' field for each record</span>
    </label></div>
    <div class="checkbox"><label>
      <input type="checkbox" class="checkbox style-0" name="fieldCreate_date" value="1">
      <span>Export 'create date' field for each record</span>
    </label></div>
  </fieldset>
  <fieldset>
    <legend>Options</legend>
    <div class="checkbox"><label>
      <input type="checkbox" class="checkbox style-0" name="fieldRootExport" value="1">
      <span>Export all records in this module (not just selected heirarchy)</span>
    </label></div>
    <div class="checkbox"><label>
      <input type="checkbox" class="checkbox style-0" name="fieldNames" value="1" checked>
      <span>Show Column Headings?</span>
    </label></div>
  </fieldset>
</div>
<div class="modal-footer">
  <a class="btn btn-primary" href="javascript:exportData();">Export Now</a>
</div>

<%
    }
    else
    {
%>
<%--Now get an email address to send this to...--%>
<form method=POST name=sendmail action="export-email" >
<div class="modal-body">
  <div class="form-group">
    <label>Please enter your email address to send export file</label>
    <input type=text name=email value='' size=80 class="form-control"></TD>
    <input type=hidden name=module value='<%=moduleName%>'>
    <input type=hidden name=table value='Categories'>
  </div>
</div>
<div class="modal-footer">
  <a class="btn btn-primary" href="javascript:document.sendmail.submit();">Send Email</a>
</div>
</form>

<%
    }
%>
<!-- END CONTENT AREA -->

<script src="/admin/plugins/jqueryui.multiselect/ui.multiselect.js"></script>
<script>
jQuery(document).ready(function($){
  jQuery('.multiselect').css({'height': '400px', 'width': '600px'}).multiselect().next().find('input.search').width(600/2-80);  
  jQuery('input[name="export_type"][value="c"]').click();
});
function exportData(){
    if(jQuery('select[name="data_items"]').val()){
      document.exportnow.submit();
    }else{
      alert('Please select at least one data item!');  
    }
}
</script>
<script>
$(function(){
  $('.modal-header .close').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>

