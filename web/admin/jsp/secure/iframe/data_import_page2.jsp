<!DOCTYPE html>
<%@page import="au.net.webdirector.admin.security.LoginController"%>
<%@ page import="java.util.*,
        java.io.*,
        webdirector.db.client.UserDetails,au.net.webdirector.common.Defaults,au.net.webdirector.admin.imports.ImportCSV"%>
<%@ page import="au.net.webdirector.common.datalayer.admin.db.DataAccess,au.net.webdirector.common.datatypes.domain.*" %>
<jsp:useBean id="dataTypes" class="au.net.webdirector.common.datatypes.service.DataTypesService" />

<html>
<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
<style>
.button-row {
  display: inline-block;
  position: relative;
  left: 50%;
  -moz-transform: translateX(-50%);
  -ms-transform: translateX(-50%);
  -webkit-transform: translateX(-50%);
  transform: translateX(-50%);
}
</style>
</head>
<%
	Defaults d = Defaults.getInstance();

  String moduleName = (String)request.getAttribute("moduleName");
  String deleteMarker = (String)request.getAttribute("arg1");

  String importType = (String)request.getAttribute("importType");
  String assetLive = (String)request.getAttribute("assetLive");
  String switchOff = (String)request.getAttribute("switchOff");
  String mapFileName = (String)request.getAttribute("arg2");

  String u = (String)session.getValue(LoginController.WD_USER);
  UserDetails user = new UserDetails(u);

  String importFile = (String)request.getAttribute("importFile");
  String tableType="";

  List<DataType> labels = dataTypes.loadGenericDataTypes(moduleName,!"Asset".equalsIgnoreCase(importType),null);

  int levels = Integer.parseInt( Defaults.getModuleLevels(moduleName) );

  
  String deleteAll = (String)request.getAttribute("arg1");

  ImportCSV ic = new ImportCSV();
  String[] arrLineData=null;
  try
  {
    arrLineData = ic.readFirstLine(importFile);
  }
  catch(Exception e)
  {
    out.println("<br><font color='#000000'><b>"+e.getMessage());
    if(true)
      return;
  }
%>
<body class="iframe-body">
<form name="Form1" method="post" action="/webdirector/secure/data/import/do" onsubmit="return validate()">

<div class="modal-header modal-tabs">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Import Data</h4>
  <ul class="nav nav-tabs tabs-pull-right">
    <li>
      <a href="#saveMappingFile" data-toggle="tab">Save Mapping File</a>
    </li>
    <li>
      <a href="#primaryKey" data-toggle="tab">Primary Key</a>
    </li>
    <li class="active">
      <a href="#dataMapping" data-toggle="tab">Data Mapping</a>
    </li>
  </ul>
</div>
<div class="modal-body tab-content">
  <div id="dataMapping" class="tab-pane fade active in">
    <div class="form-group clearfix">
      <label class="col-sm-6 control-label"><b style="text-transform: uppercase;"><%=importType%></b> data to be imported into the <b><%= moduleName %></b> module.</label>
      <div class="col-sm-6">
        <label class="checkbox-inline">
          <input type="checkbox" class="checkbox style-0" name="ignoreFirstLine" id="ignoreFirstLine" value="on">
          <span>Ignore the first line</span>
        </label>
      </div>
    </div>
    <div class="form-group clearfix">
      <div class="col-xs-6">
        <label>Source File Data Columns</label>
        <select size="8" name="data" style="width:100%" class="form-control">
  <%
          int index=levels;
            for(int i=levels;i<arrLineData.length;i++)
            {
              String value = arrLineData[i];
              value = value.replaceAll("\"","");
              if(value.trim().equals(""))
                value = "BLANK";
  %>
              <option value="<%=index%>"><%=value%></option>
  <%
            index++;
            }
  %>
          </select>
      </div>
      <div class="col-xs-6">
        <label>Destination Data Table Attributes <%= importFile %></label>
        <select size="8" name="attributes" style="width:100%" class="form-control">
  <%
          if(labels!=null)
          {
            for(int i=0;i<labels.size();i++)
            {
              DataType col = labels.get(i);
              //0 -- Internal Name, 1 -- OnOff, 2 -- Mandatory, 3 -- External
              String internalName = col.getInternalName();
              if(importType.equalsIgnoreCase("Category") && internalName.equalsIgnoreCase("ATTR_categoryName"))
                continue;
              String externalName = col.getExternalName();
              boolean mandatory = col.isMandatory();
              String display = externalName +"("+internalName+")";
              if(mandatory)
                display = "*" + display;
              
  %>
        <option value="<%=internalName%>" alt='<%=mandatory%>'><%=display%></option>
  <%
            }
          }
  %>
        </select>
      </div>
    </div>
    <div>
      <div class="button-row">
        <button type='button' class="btn btn-default" onclick="addMapping()">Add</button>
        <button type='button' class="btn btn-default" onclick="removeMapping()">Remove</button>
        <button type='button' class="btn btn-default" onclick="autoMapping()">Auto Mapping</button>
      </div>
    </div>
    <div class="form-group col-xs-12">
      <label>Mapped Data Attributes</label>
      <select size="8" name="mapping" multiple class="form-control">
<%
      String mapDir = d.getStoreDir()+"/_importFileMapping";
      System.out.println("\n\n--------->fileName:"+mapFileName);
      System.out.println("\n\n--------->Full Path:"+mapDir+"/"+mapFileName);
      String fileContent="";
      if(mapFileName!=null && !mapFileName.equals(""))
      {
        try
        {
          File mapFile = new File(mapDir+"/"+mapFileName);
          FileInputStream fstream = new FileInputStream(mapDir+"/"+mapFileName);
          DataInputStream in = new DataInputStream(fstream);
          BufferedReader br = new BufferedReader(new InputStreamReader(in));
          String strLine;
          //Read File Line By Line
          while ((strLine = br.readLine()) != null)
          {
            // Print the content on the console
            System.out.println (strLine);
            fileContent = fileContent + strLine +"@@@@";
          }
          if(!fileContent.equals(""))
          {
            fileContent = fileContent.substring(0,fileContent.length()-4);
          }
          //Close the input stream
          in.close();
        }
        catch(IOException ioe)
        {
          ioe.printStackTrace();
        }
      }
%>
      </select>
      <input type="hidden" name="fileContent" value="<%=fileContent%>">
    </div>
  </div>
  <div id="primaryKey" class="tab-pane fade form-horizontal">
    <div class="form-group">
      <label class="col-xs-2 control-label">Primary Key</label>
      <div class="col-xs-10">
        <select name="pKey" class="form-control">
        <option value="">Please select a Primary Key</option>
  <%
            if(labels!=null)
            {
              for(int i=0;i<labels.size();i++)
              {
                DataType value = labels.get(i);
  %>
        <option value="<%=value.getInternalName()%>"><%=value.getInternalName()%></option>
  <%
              }
            }
  %>
        </select>
      </div>
    </div>
  </div>
  <div id="saveMappingFile" class="tab-pane fade form-horizontal">
    <div class="form-group">
      <label class="col-xs-2 control-label">Mapping File Name</label>
      <div class="col-xs-10">
        <input type="text" name="mapFile" class="form-control">
        <p class="help-block">
          Save this Data Mapping to use again for future data imports when using the same CSV data file format.  This will allow you to skip the data mapping step and go straight to performing the data import.
        </p>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="submit" class="btn btn-primary">Import Now</button>
</div>

<input type="hidden" name="importFile" value="<%=importFile%>">
<input type="hidden" name="importType" value="<%=importType%>">
<input type="hidden" name="assetLive" value="<%=assetLive%>">
<input type="hidden" name="deleteMarker" value="<%=deleteMarker%>">
<input type="hidden" name="deleteAll" value="<%=deleteAll%>">
<input type="hidden" name="switchOff" value="<%=switchOff%>">
<input type="hidden" name="moduleName" value="<%=moduleName%>">
</form>

<script>

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}


function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

</script>


<script>

function autoMapping(){
  var data = document.Form1.data;
  var attrs = document.Form1.attributes;
  for(var i=0;i<data.options.length;i++){
    data.selectedIndex = i;
    var left = data.options[i].text;
    if(attrs.options.length == 0){
      break;
    }
    for(var j=0;j<attrs.options.length;j++){
      attrs.selectedIndex = j;
      if(left.toLowerCase().indexOf(attrs.options[j].value.toLowerCase()) != -1){
        addMapping();
        i--;
        j--;
        break;
      } 
    }
  }
  data.selectedIndex = -1;
  attrs.selectedIndex = -1;
}

function addMapping()
{
  var selIndex = document.Form1.data.selectedIndex;
  var selIndex1 = document.Form1.attributes.selectedIndex;
  if(selIndex!=-1 && selIndex1!=-1)
  {
    var csvDataId= document.Form1.data.options[document.Form1.data.selectedIndex].value;
    var csvData= document.Form1.data.options[document.Form1.data.selectedIndex].text;
    var colNameVal= document.Form1.attributes.options[document.Form1.attributes.selectedIndex].value;
    var colNameText= document.Form1.attributes.options[document.Form1.attributes.selectedIndex].text;

    var mandatory = document.Form1.attributes.options[document.Form1.attributes.selectedIndex].alt

    if(mandatory=="1" && csvData=="BLANK")
    {
      alert("Mandatory Attributes Can't Be Mapped To 'Blank' Values");
      return;
    }


    var obj = new Option(csvData+"----"+colNameText, csvDataId+"----"+colNameVal, true, true);
    document.Form1.mapping[document.Form1.mapping.length] = obj;
    document.Form1.data.remove(document.Form1.data.options.selectedIndex)
    document.Form1.attributes.remove(document.Form1.attributes.options.selectedIndex)
  }
}

function removeMapping()
{
  var selIndex = document.Form1.mapping.selectedIndex;
  if(selIndex!=-1)
  {
    var id = document.Form1.mapping.options[document.Form1.mapping.selectedIndex].value;
    var text = document.Form1.mapping.options[document.Form1.mapping.selectedIndex].text;

    var val = new String(id);
    var textVal = new String(text);
    var valArray = val.split("----");
    var textArray = textVal.split("----");
    var obj = new Option(textArray[0], valArray[0], true, true);
    var obj1 = new Option(textArray[1], valArray[1], true, true);

    document.Form1.data[document.Form1.data.length] = obj;
    document.Form1.attributes[document.Form1.attributes.length] = obj1;
    document.Form1.mapping.remove(document.Form1.mapping.options.selectedIndex)
  }
}

function moveToBin()
{
  var selIndex = document.Form1.data.selectedIndex;
  if(selIndex!=-1 )
  {
    var id= document.Form1.data.options[document.Form1.data.selectedIndex].value;

    var obj = new Option(id, id, true, true);
    document.Form1.bin[document.Form1.bin.length] = obj;
    document.Form1.data.remove(document.Form1.data.options.selectedIndex)
  }
}
function removeFromBin()
{
  var selIndex = document.Form1.bin.selectedIndex;
  if(selIndex!=-1)
  {
    var id = document.Form1.bin.options[document.Form1.bin.selectedIndex].value;

    var obj = new Option(id, id, true, true);
    document.Form1.data[document.Form1.data.length] = obj;
    document.Form1.bin.remove(document.Form1.bin.options.selectedIndex)
  }
}

function validate()
{
  var form = document.Form1;
  for(i=0;i<form.mapping.options.length;i++)
    form.mapping.options[i].selected=true;

  if(form.mapping.options.length==0)
  {
    alert("Please map data to attributes");
    return false;
  }

  var switchOff = form.switchOff.value

  if(switchOff=="null" || parseInt(switchOff)==0)
  {
    for(i=0;i<form.attributes.options.length;i++)
    {
      //alert("alt:"+form.attributes.options[i].alt)
      if(parseInt(form.attributes.options[i].alt)==1)
      {
        alert("Please map "+form.attributes.options[i].text +" attributes with data");
        return false;
      }
    }
  }
  else if(parseInt(switchOff)==1)
  {

    if(form.pKey.selectedIndex==0)
    {

    }
  }
  
  if(form.pKey.selectedIndex!=0)
  {
    var pkeyVal = new String(form.pKey[form.pKey.selectedIndex].value);
    var pkeyMapped = false;
    for(i=0;i<form.mapping.options.length;i++)
    {
      var value = new String(form.mapping.options[i].value)

      if(value.indexOf(pkeyVal)!=-1)
      {
        pkeyMapped = true;
        break;
      }
    }
    if(!pkeyMapped)
    {
      alert("Please map primary key '"+pkeyVal +"' attributes with data");
      return false;

    }
  }
  return true;
}

function loadMapping()
{
  var fileContent = new String(document.Form1.fileContent.value);

  if(fileContent!="")
  {
    if(fileContent.indexOf("@@@@")!=-1 && fileContent.indexOf("----")!=-1)
    {
      var arrFileContent = fileContent.split("@@@@");
      
      if(arrFileContent[0]!=document.Form1.moduleName.value)
      {
        alert("Mapping file doesn't belong to selected module. \nMapping will not be loaded automatically.");
        //break;
      }
      else
      {

        for(j=1;j<arrFileContent.length;j++)
        {

          var arrLine = arrFileContent[j].split("----");

          if(arrLine[0]!=null)
          {
            var dataIndex = getSelIndex("data",arrLine[0]);
            var colIndex = getSelIndex("attributes",arrLine[1]);


//old code to add mapping
            var csvDataId= document.Form1.data.options[dataIndex].value;
            var csvData= document.Form1.data.options[dataIndex].text;
            var colNameVal= document.Form1.attributes.options[colIndex].value;
            var colNameText= document.Form1.attributes.options[colIndex].text;

            var obj = new Option(csvData+"----"+colNameText, csvDataId+"----"+colNameVal, true, true);
            document.Form1.mapping[document.Form1.mapping.length] = obj;
            document.Form1.data.remove(dataIndex)
            document.Form1.attributes.remove(colIndex)
          }
        }
      }//else part
      
    }
    else
    {
      alert("Incorrect Mapping File Format.");
    }
  }
}

function getSelIndex(selName,value)
{

  var selObj = document.Form1.elements[selName];

  for(i=0;i<selObj.options.length;i++)
  {
    if(value==selObj.options[i].value)
      return i;
  }
}

</script>
<script>
  loadMapping();
</script>
<script>
$(function(){
  $('.modal-header .close').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>