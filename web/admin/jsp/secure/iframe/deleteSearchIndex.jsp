<!DOCTYPE html>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Hashtable"%>
<%@page import="au.net.webdirector.admin.modules.ModuleNameService"%>
<%
	ModuleNameService NameService = new ModuleNameService();
    String[] Module = new String[40];

    Module = NameService.getAllValidModules();

    String[] module1 = new String[Module.length];
    Hashtable h = new Hashtable();
    for (int i = 0; i < Module.length; i++) {
        module1[i] = NameService.getModuleName(Module[i]);
        h.put(module1[i],Module[i]);
    }
    Arrays.sort(module1);
%>
<html>
<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
</head>
<body class="iframe-body">
<!-- START CONTENT AREA -->
<form  Method= post name="detail">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Index Content</h4>
</div>
<div class="modal-body">
  Enter your email Address: <input type="text" size="45" maxlength="50" name="Email" ><br>
    <br>
    WARNING: This process will DELETE the current Search Index File for the selected module, and then create a new Search Index based on the current content of the module.<br>
    <br>
    To proceed please select a Module: <br>
      <SELECT NAME="ModuleID" multiple size="10">
<%
    for(int i=0;i<module1.length;i++){%>
      <option value = "<%=h.get(module1[i])%>"> <%=(module1[i])%>  </option>
      <%}%>
      </SELECT>
</div>
<div class="modal-footer">
  <button type="submit" class="btn btn-primary">Start</button>
</div>
</form>
<!-- END CONTENT AREA -->
<script type="text/javascript">
  $(function() {
    $("form").submit(function(e) {
      e.preventDefault;
      validate();
    })
  });

  function validate() {
    //Now ensure the email is valid
    if ((document.detail.Email.value.indexOf('@') < 1)
        || // '@' cannot be in first position
        (document.detail.Email.value.lastIndexOf('.') <= document.detail.Email.value
            .indexOf('@') + 1)
        || // Must be at least one valid char btwn '@' and '.'
        (document.detail.Email.value.lastIndexOf('.') == document.detail.Email.value.length - 1)
        || // Must be at least one valid char after '.'
        (document.detail.Email.value.indexOf(' ') != -1)) // No empty spaces permitted
    {
      alert("The Email field must contain a valid email address");
      document.detail.Email.focus();
      return false;
    }
    if (confirm("This process will delete the current search indexing, do you want to continue?")) {
      document.detail.submit();
    }
  }
</script>
<script>
$(function(){
  $('.modal-header .close').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>
