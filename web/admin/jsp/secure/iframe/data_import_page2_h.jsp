<!DOCTYPE html>
<html>
<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp" %>
</head>

<%
  String moduleName = (String)request.getAttribute("moduleName");
  String importerId = (String)request.getAttribute("importerId");
%>

<body class="iframe-body">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Import Data</h4>
</div>
<div class="modal-body">
  <table align="center" border="0" width="100%" cellspacing="1" cellpadding="3" id="resultTable">
    <tr>
      <td><font color="#000000">Importing data into <font size="4"><%=moduleName%></font> module ...</td>
    </tr>
    <tr>
      <td><font color="#000000" id="importResult"><div class="demo"><div id="progressbar"></div></div></font></td>
    </tr>
  </table>
</div>
<div class="modal-footer">
</div>
<!-- END CONTENT AREA -->


<script>

var timer;
var refreshInterval = 500;
$(document).ready(function(){
  $( "#progressbar" ).progressbar({
    value: 0
  });
  timer = setTimeout("importPercentage()",refreshInterval);
  
});


function importPercentage(){
  $.get('/webdirector/secure/data/import/percentage?id=<%= importerId %>', function(result){
    var res = eval("("+result+")");
    $( "#progressbar" ).progressbar("option", "value", res.percentage*1);
    if( res.status == "1" || res.status=="0"){
      setTimeout("importPercentage()",refreshInterval);
      return;
    }
    if(res.status == "3"){
      clearTimeout(timer);
      //refreshTree();
      return;
    }
    if(res.status == "2"){
      clearTimeout(timer);
      $("#resultTable").append(res.info);
      //refreshTree();
      return;
    }
  });
}
</script>
<!-- End demo -->
<script>
$(function(){
  $('.modal-header .close').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>
