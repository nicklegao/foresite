<!DOCTYPE html>
<%@page import="webdirector.seo.CustomizedShortUrlGenerator"%>
<%@page import="java.util.List"%>
<%@page import="webdirector.seo.UrlMapping"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	List<UrlMapping> mappings = CustomizedShortUrlGenerator.readFile(application);
%>

<html>
<head>
<%@include file="/admin/jsp/webdirectorTheme.jsp"%>
<style>
.form-horizontal .form-group {
  margin-left: -26px;
  margin-bottom: 6px;
}
.form-horizontal .form-group:last-child {
  margin-bottom: 0px;
}
</style>
</head>
<body class="iframe-body">
<form name="frm1" method="post">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Customized Short URL Rules</h4>
</div>
<div class="modal-body">
  <table class="table table-bordered form-horizontal" id="ruleTable">
  <%for(UrlMapping mapping: mappings){%>
  <tr style="display:table-row;">
    <td>
      <div class="form-group">
        <label class="col-sm-2 control-label">Short URL</label>
        <div class="col-sm-4">
          <input class="form-control input-sm" name="mappingfrom" size="50" value="<%= mapping.getFrom() %>">
        </div>
        <label class="col-sm-2 checkbox-inline">
          <input type="checkbox" class="checkbox cb mappinglast" <%= mapping.getLast().equals("true")?"":"checked" %>>
          <span>To short URL</span>
          <input type="hidden" name="mappinglast" value="<%= mapping.getLast() %>">
        </label>
        <label class="col-sm-2 checkbox-inline">
          <input type="checkbox" class="checkbox cb mappingexternal" <%= mapping.isPermanentRedirect()?"checked":"" %>>
          <span>To external URL</span>
          <input type="hidden" name="mappingexternal" value="<%= mapping.isPermanentRedirect() %>">
        </label>
        <div class="col-sm-1">
          <button onclick="removeRule(this)" class="btn btn-sm btn-danger">Remove</button>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Mapping To:</label>
        <div class="col-sm-10">
          <input class="form-control input-sm" name="mappingto" value="<%= mapping.getTo() %>">
        </div>
      </div>
    </td>
  </tr>
  <%}%>
  <tr style="display:none;">
    <td>
      <div class="form-group">
        <label class="col-sm-2 control-label">Short URL</label>
        <div class="col-sm-4">
          <input class="form-control input-sm" name="mappingfrom" size="50" value="">
        </div>
        <label class="col-sm-2 checkbox-inline">
          <input type="checkbox" class="checkbox cb mappinglast">
          <span>To short URL</span>
          <input type="hidden" name="mappinglast" value="true">
        </label>
        <label class="col-sm-2 checkbox-inline">
          <input type="checkbox" class="checkbox cb mappingexternal">
          <span>To external URL</span>
          <input type="hidden" name="mappingexternal" value="false">
        </label>
        <div class="col-sm-1">
          <button onclick="removeRule(this)" class="btn btn-sm btn-danger">Remove</button>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Mapping To:</label>
        <div class="col-sm-10">
          <input class="form-control input-sm" name="mappingto" value="">
        </div>
      </div>
    </td>
  </tr>
  </table>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-success pull-left" id="AddRule">Add</button>
  <button type="submit" class="btn btn-primary">Next</button>
</div>
</form>

<script>
jQuery(document).ready(function($){
	$('#AddRule').css('cursor', 'pointer');
	$('#AddRule').click(function(){	
		var tr = $('#ruleTable').find('tr:hidden');
		tr.parent().append(tr.clone().show());
		bindClick();
	});
	bindClick();
	jQuery('#ruleTable').find('tbody').sortable();
});

function bindClick(){
  jQuery('input.cb').unbind('click');
  jQuery('input.mappinglast').click(function(){
    var last = jQuery(this).parent().find('input[type="hidden"]');
    if(jQuery(this).prop("checked")){
      last.val("false");
    }else{
      last.val("true");
    }
  });
  jQuery('input.mappingexternal').click(function(){
    var external = jQuery(this).parent().find('input[type="hidden"]');
    if(jQuery(this).prop("checked")){
      external.val("true");
    }else{
      external.val("false");
    }
  });
}

function removeRule(btn){
	jQuery(btn).closest("tr").remove();
}
</script>
<script>
$(function(){
  $('.modal-header .close').click(function(){
    top.ModalHelper.closeModal();
  });
})
</script>
</body>
</html>