<%@page import="au.net.webdirector.admin.workflow.WorkflowHelper"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.net.webdirector.admin.modules.ModuleConfiguration"%>
<%@ page import="java.util.*"%>
<%@ page import="webdirector.db.DButils"%>
<%@ page import="webdirector.db.DatabaseTableMetaDataService"%>
<%@ page import="au.net.webdirector.common.Defaults"%>
<%@ page import="au.net.webdirector.admin.modules.domain.*"%>
<%@ page import="au.net.webdirector.common.datalayer.admin.db.DataAccess,au.net.webdirector.common.datatypes.domain.*"%>
<%@ page import="au.net.webdirector.admin.modules.DynamicFormScriptUtil"%>
<jsp:useBean id="userDB" class="webdirector.db.UserUtils"/>
<%
  String moduleName = request.getParameter("moduleName");
  
  ModuleConfiguration moduleConfiguration = ModuleConfiguration.getInstance();
  Module module = moduleConfiguration.getModule(moduleName);
  String moduleDescription = module.getExternalModuleName();
  
  Vector allUsers = userDB.getAllUsers();
  List<String[]> nonAdminUsers = new LinkedList<String[]>();
  for (int i=0; i<allUsers.size(); i++)
	{
	  String[] details = (String[])allUsers.elementAt(i);
	  // if ( details[4].equals("Administrator") )
		//  continue;
	  nonAdminUsers.add(details);
	}

  Set<String> approvers = WorkflowHelper.getInstance().getModuleApprovers(moduleName);
%>
<style>

.icon-picker {
  float: right;
  z-index: 1;
}

.form-group .onoffswitch{
	top:5px;
}

</style>
<script>
  $(function() {
    var moduleName = "<%=ESAPI.encoder().encodeForJavaScript(moduleName)%>";
    var $form = $("#configure-module-form");
    
    $("#btnSaveChanges").click(function() {
      var btn = $(this);
      btn.attr("disabled", "disabled");
      $.ajax({
        url: '/webdirector/configureModule/assignApprovers?moduleName=' + moduleName,
        type: "POST",
        data: $("#configure-module-form").serializeArray(),
        success: function(data) {
          if (data.success) {
            reloadModal("Permissions", {success: true, content: 'Permissions saved', container: "#settingsContent"});
          } else {
            ModalHelper.showNotification({success: false, content: data.message, container: "#settingsContent"});
          }
        },
        complete: function() {
          btn.removeAttr("disabled");
          StorageHelper.restoreOpenModules();
        }
      });
    });

    $(".editPermissions").click(function(e){
      var modal = $("#assignCategoriesModal");
      var content = $("#assignCategoriesContent");
      content.load("/webdirector/secure/user/categories",
      {
        moduleName : moduleName,
        userId : $(e.currentTarget).closest("tr").attr('data-user-id')
      },
      function() {
        content.children(".modal-body").css("height", $(window).height() * 0.4);
        modal.modal();
      });
    });
    
    var $dropdown = $(".icon-picker > .dropdown-menu");
    $("#btnIcon").on('click', function (event) {
      $(this).parent().toggleClass('open');
    });
    $('body').on('click', function (e) {
      if (!$dropdown.is(e.target) && $dropdown.has(e.target).length === 0 && $('.open').has(e.target).length === 0) {
        $("#btnIcon").parent().removeClass('open');
      }
    });

    $("#modal-setting").find(".nav-tabs").find("li").not("active").find("a").click(function(e) {
      e.preventDefault();
      reloadModal($(this).data("mode"));
    });
    
    function reloadModal(mode, notification) {
      if (mode == "Settings") {
          ModalHelper.loadContent('/webdirector/secure/module/configure/settings?mode=' + mode + '&moduleName=<%=ESAPI.encoder().encodeForURL(moduleName)%>', notification);
      } else if (mode == "Permissions") {
          ModalHelper.loadContent('/webdirector/secure/module/configure/permissions?mode=' + mode + '&moduleName=<%=ESAPI.encoder().encodeForURL(moduleName)%>', notification);
      } else if (mode == "Code") {
          ModalHelper.loadContent('/webdirector/secure/module/configure/developer/generate-code?moduleName=<%=ESAPI.encoder().encodeForURL(moduleName)%>', notification);
      } else {        
        ModalHelper.loadContent('/webdirector/secure/module/configure?mode=' + mode + '&moduleName=<%=ESAPI.encoder().encodeForURL(moduleName)%>', notification);
      }
    }

  });

</script>
<div class="modal-header modal-tabs">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
  </button>
  <h4 class="modal-title" id="myModalLabel">Configure <%=ESAPI.encoder().encodeForHTML(moduleName) %> - Permissions</h4>
  <ul class="nav nav-tabs tabs-pull-right" id="myTab3">
    <li>
      <a href="#" data-mode="Settings">Settings</a>
    </li>
    <li class="active">
      <a href="#" data-mode="Permissions">Permissions</a>
    </li>
    <li>
      <a href="#" data-mode="Category">Category</a>
    </li>
    <li>
      <a href="#" data-mode="Elements">Elements</a>
    </li>
  </ul>
</div>
<div class="modal-body">
<form id="configure-module-form" class="form-horizontal">
	<fieldset>
		<div class="col-md-12">
			<small><strong>Note:</strong> Admin users have full read/write/approve permissions.</small>
			
    <table class="table table-hover">
      <thead>
        <tr>
          <th width="1">#</th>
          <th>Name</th>
          <th>Company</th>
          <th width="100">Username</th>
          <th width="1">Approver</th>
          <th width="1">Permissions</th>
        </tr>
      </thead>
      <tbody>
	<% for (String[] details : nonAdminUsers)
	{
	%>
        <tr data-user-id="<%=ESAPI.encoder().encodeForHTMLAttribute(details[0]) %>">
          <th scope="row"><%=ESAPI.encoder().encodeForHTML(details[0]) %></th>
          <td><%=ESAPI.encoder().encodeForHTML(details[1]) %></td>
          <td><%=ESAPI.encoder().encodeForHTML(details[2]) %></td>
          <td><%=ESAPI.encoder().encodeForHTML(details[3]) %></td>
          <td class="text-center">
						<span class="onoffswitch">
							<input type="checkbox" name="approver_<%=details[0] %>" id="approver_<%=details[0] %>" <%=approvers.contains(details[0])?"checked":"" %> class="onoffswitch-checkbox">
							<label class="onoffswitch-label" for="approver_<%=details[0] %>"> 
								<span class="onoffswitch-inner" data-swchon-text="YES" data-swchoff-text="NO"></span> 
								<span class="onoffswitch-switch"></span> 
							</label> 
						</span>
          </td>
          <td class="text-center">
            <button type="button" class="btn btn-primary btn-xs editPermissions">Edit</button>
          </td>
        </tr>
				<%--
				<a href="#" data-user-id="<%=ESAPI.encoder().encodeForHTMLAttribute(details[0]) %>" class="manage-privilages list-group-item"><%=ESAPI.encoder().encodeForHTMLAttribute(details[1]) %><span class="badge"><%=ESAPI.encoder().encodeForHTMLAttribute(details[3]) %></span></a>
				
				 --%>
	<%
	}
	%>
      </tbody>
      <tfoot>
      	<tr>
      		<th colspan="6">Total <%=nonAdminUsers.size() %> users</th>
      	</tr>
      </tfoot>
    </table>
	<% if (nonAdminUsers.size() == 0){ %>
		<div class="alert alert-warning" role="alert">
      <strong>Oh NO!</strong> There are no users.
    </div>
	<% } %>
		</div>
	</fieldset>
</form>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  <button type="button" class="btn btn-primary" id="btnSaveChanges">Save Changes</button>
</div>
<div class="modal fade" id="assignCategoriesModal">
  <div class="modal-dialog">
    <div class="modal-content" id="assignCategoriesContent">
    </div>
  </div>
</div>
