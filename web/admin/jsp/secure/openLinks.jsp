<style>
.tab-content, .tab-pane {
  height: 100%
}
</style>
<div class="modal-header modal-tabs">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
  </button>
  <h4 class="modal-title" id="myModalLabel">Job Scheduler</h4>
  <ul class="nav nav-tabs tabs-pull-right" id="myTab3">
    <li>
      <a data-toggle="tab" href="#jobSchedule">Job Schedule</a>
    </li>
    <li>
      <a data-toggle="tab" href="#jobHistory">Job History</a>
    </li>
    <li class="active">
      <a data-toggle="tab" href="#newJob">New Job</a>
    </li>
  </ul>
</div>
<div class="modal-body">
  <div class="tab-content">
    <div class="tab-pane fade active in" id="newJob">
      <iframe src="/webdirector/secure/scheduler/jobSchedule" id="jobScheduleFrame" border=0 frameborder=0>Your browser doesn't support Iframes please upgrade it.</iframe>
    </div>
    <div class="tab-pane fade" id="jobHistory">
      <iframe src="/webdirector/secure/scheduler/jobHistory?jobName=" id="aFrmId" border=0 frameborder=0>Your browser doesn't support Iframes please upgrade it.</iframe>
    </div>
    <div class="tab-pane fade" id="jobSchedule">
      <iframe src="/webdirector/secure/scheduler/jobList" id="aFrmId" width="750px" border=0 frameborder=0>Your browser doesn't support Iframes please upgrade it.</iframe>
    </div>
  </div>
</div>
<div class="modal-footer">
</div>