<%@page import="au.net.webdirector.common.datalayer.client.ModuleHelper"%>
<%@page import="au.net.webdirector.admin.modules.privileges.CategoryPrivilegeSummary"%>
<%@page import="au.net.webdirector.admin.modules.privileges.LegacyUserModulePrivilegeService"%>
<%@ page import="java.util.*"%>
<%@ page import="webdirector.db.client.ClientDataAccess"%>
<%@ page import="au.net.webdirector.admin.modules.ModuleNameService" %>
<%@ page import="webdirector.utils.*" %>
<%@ page import="webdirector.db.*" %>
<%
	String userId = request.getParameter("userId");
    String userType = request.getParameter("userType");
    boolean viewOnly = "5".equals(userType);
    String moduleName = request.getParameter("moduleName");
    ModuleNameService NameService = new ModuleNameService();
    LegacyUserModulePrivilegeService service = null;
    String msg = "";
    if("save".equals(request.getParameter("action"))){
        service = new LegacyUserModulePrivilegeService(moduleName, userId);
        List<CategoryPrivilegeSummary> privileges = service.parsePrivileges(request);
        service.saveAssignCategories(privileges);
        msg = "Saved";
    }
    service = new LegacyUserModulePrivilegeService(moduleName, userId);
    CategoryPrivilegeSummary root = service.getAllCategoryPrivileges();
    int folderDepth = ModuleHelper.getInstance().getModuleLevels(moduleName);
%>
<style>

#assignCategoriesContent {
  margin: auto;
}

.ci-tree-item{
  color: #333;
  vertical-align: bottom;
}
.ci-tree-column-grp{

}
.ci-tree-group{

}

.ci-tree-label {
  border-bottom: 1px solid #aaa;
  padding: 5px 0;
}
.ci-tree-label:hover {
  background-color: #e0e0e0;
}

.ci-tree-icon{
  font-family: FontAwesome;
  font-feature-settings: normal;
  font-kerning: auto;
  font-language-override: normal;
  font-size: 10px;
  font-size-adjust: none;
  font-stretch: normal;
  font-style: normal;
  font-synthesis: weight style;
  font-variant: normal;
  font-weight: normal;
  line-height: 1;
  text-rendering: auto;
  vertical-align: middle;
  margin-left: 1px;
  margin-right: 4px;
  cursor: pointer;
  color: #525252;
}

.ci-tree-icon-plus:before {
  content: "\f0fe";
}

.ci-tree-icon-minus:before {
  content: "\f146";
}

.ci-tree-label .ci-tree-item{
  display:inline-block;
}

.ci-tree-column-name{
  width:<%= viewOnly?"275":"230" %>px;
  padding-left: 10px;
}

.ci-tree-column-cb{
  width:80px;
  text-align:center;
}

.ci-tree-space{
  width: 30px;
}

.ci-tree-header{
  background-color: #e0e0e0;
  width: 100%;
  font-weight: bold;
}

.ci-tree-body {
  height: 360px; 
  overflow-y: auto;
}

.result-msg{
  color: green;
  font-weight: bold;
  font-size: 14px;
}

</style>
<div class="modal-header">
  <button type="button" class="close">&times;</button>
  <h4 class="modal-title">Assign Categories for <%=NameService.getModuleName(moduleName)%></h4>
</div>
<form id="assignCategoriesForm">
<div class="modal-body">
<div class="ci-tree-item ci-tree-column-grp ci-tree-header">
  <div class="ci-tree-item ci-tree-label">
    <div class="ci-tree-item ci-tree-column-name">Name</div>
    <div class="ci-tree-item ci-tree-column-cb">View</div>
    <% if(!viewOnly){ %>
    <div class="ci-tree-item ci-tree-column-cb">Insert</div>
    <div class="ci-tree-item ci-tree-column-cb">Edit</div>
    <div class="ci-tree-item ci-tree-column-cb">Delete</div>
    <% } %>
    <div class="ci-tree-item ci-tree-column-cb">Don't<br/>Pass Down</div>
    <div class="ci-tree-item ci-tree-column-cb">Don't<br/>Inherit</div>
  </div>
</div>
<div class="ci-tree-item ci-tree-group ci-tree-body">
<%= service.drawTree(root, viewOnly, folderDepth) %>  
</div>
</div>
<div class="modal-footer">
<span class="result-msg"><%=msg%></span>
<button type="button" class="btn btn-sm btn-warning" id="btnClearCategories">Clear</button>
<button type="button" class="btn btn-sm btn-primary" id="btnSaveAssignCategories">Save Settings</button>
<input type="hidden" name="action" value="save">
<input type="hidden" name="userId" value="<%=userId%>">
<input type="hidden" name="userType" value="<%=userType%>">
<input type="hidden" name="moduleName" value="<%=moduleName%>">
</div>
</form>

<script>
$(document).ready(function(){
  $(".ci-tree-icon").click(function(){
    $($(this).parents(".ci-tree-label")[0]).next().slideToggle();
    if($(this).hasClass("ci-tree-icon-plus")){
      $(this).removeClass("ci-tree-icon-plus").addClass("ci-tree-icon-minus");
    }else{
      $(this).removeClass("ci-tree-icon-minus").addClass("ci-tree-icon-plus");
    }
  });
  
  $('input[name^="C_"], input[name^="R_"], input[name^="U_"], input[name^="D_"]').click(function(){
    tickCheckBox($(this));
  });
  
  setTimeout(function(){
    $('.result-msg').fadeOut();
  }, 1000);

  $("#btnSaveAssignCategories").click(function() {
    var content = $("#assignCategoriesContent");
    content.load("/webdirector/secure/user/categories",
      $("#assignCategoriesForm").serializeArray(),
      function() {
        $(this).closest(".modal").modal("hide");
        ModalHelper.showNotification({success: true, content: "Assigned categories saved"});
      });
  });

  $("#btnClearCategories").click(function() {
    $('#assignCategoriesForm .ci-tree-item>input[type="checkbox"]').attr("checked", false)
  });

  $("#assignCategoriesContent").find("button.close").click(function() {
    $(this).closest(".modal").modal("hide");
  });

});

function tickCheckBox(cb){
/*   var name = cb.attr('name');
  var pref = name.substring(0,2);
  var checked = cb.is(':checked');
  if(checked){
    $(cb.parents('.ci-tree-label')[0]).next().find('input[name^="'+ pref +'"]').prop('checked', true);    
  } else{
    $(cb.parents('.ci-tree-label')[0]).next().find('input[name^="'+ pref +'"]').prop('checked', false);
  }
  if(checked && (pref == 'C_' || pref == 'U_' || pref == 'D_')){
    var cbr = cb.parent().parent().find('input[name^="R_"]');
    if(cbr.is(':checked')){
      return;
    }
    cbr.prop('checked', true);
    tickCheckBox(cbr);
  } */
}

function untickCheckBox(cb){
  
}
</script>