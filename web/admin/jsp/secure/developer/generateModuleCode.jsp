<%@page import="au.net.webdirector.developer.module.CodeGenerator"%>
<%@page import="au.net.webdirector.admin.triggers.ModuleAction"%>
<%@page import="au.net.webdirector.admin.triggers.ModuleActionSet"%>
<%@page import="au.net.webdirector.admin.triggers.ModuleActionTrigger"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.net.webdirector.admin.modules.ModuleConfiguration"%>
<%@ page import="java.util.*"%>
<%@ page import="webdirector.db.DatabaseTableMetaDataService"%>
<%@ page import="au.net.webdirector.common.Defaults"%>
<%@ page import="au.net.webdirector.admin.modules.domain.*"%>
<%@ page import="au.net.webdirector.common.datalayer.admin.db.DataAccess,au.net.webdirector.common.datatypes.domain.*"%>
<%@ page import="au.net.webdirector.admin.modules.DynamicFormScriptUtil"%>
<jsp:useBean id="userDB" class="webdirector.db.UserUtils"/>
<%
  String moduleName = request.getParameter("moduleName");
  
  ModuleConfiguration moduleConfiguration = ModuleConfiguration.getInstance();
  Module module = moduleConfiguration.getModule(moduleName);
  String moduleDescription = module.getExternalModuleName();
  int modLevels = module.getLevels();
  String moduleColor = module.getColor();
  String moduleIcon = module.getIcon();
  
  ModuleActionSet actions = ModuleActionTrigger.actionsForModule(moduleName);
%>
<style>

.icon-picker {
  float: right;
  z-index: 1;
}

.form-group .onoffswitch{
	top:5px;
}

.enabledisableswitch .onoffswitch{
  width: 85px;
}
.enabledisableswitch .onoffswitch-switch{
	right: 67px;
}
.enabledisableswitch .onoffswitch-checkbox:checked+.onoffswitch-label .onoffswitch-switch
{
	right: 0;
}

</style>
<script>
  $(function() {
    var moduleName = "<%=ESAPI.encoder().encodeForJavaScript(moduleName)%>";
    var $form = $("#configure-module-form");
    
    $("#modal-setting").find(".nav-tabs").find("li").not("active").find("a").click(function(e) {
      e.preventDefault();
      reloadModal($(this).data("mode"));
    });

    $(".modal-footer").find("button").click(function(e) {
      if ($(this).data("mode") != undefined)
      {
        e.preventDefault();
        reloadModal($(this).data("mode"));
      }
    });
    
    function reloadModal(mode, notification) {
      if (mode == "Settings") {
        ModalHelper.loadContent('/webdirector/secure/module/configure/settings?mode=' + mode + '&moduleName=<%=ESAPI.encoder().encodeForURL(moduleName)%>', notification);
      } else if (mode == "Permissions") {
          ModalHelper.loadContent('/webdirector/secure/module/configure/permissions?mode=' + mode + '&moduleName=<%=ESAPI.encoder().encodeForURL(moduleName)%>', notification);
      } else if (mode == "Code") {
          ModalHelper.loadContent('/webdirector/secure/module/configure/developer/generate-code?moduleName=<%=ESAPI.encoder().encodeForURL(moduleName)%>', notification);
      } else {        
        ModalHelper.loadContent('/webdirector/secure/module/configure?mode=' + mode + '&moduleName=<%=ESAPI.encoder().encodeForURL(moduleName)%>', notification);
      }
    }

  });

</script>
<div class="modal-header modal-tabs">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
  </button>
  <h4 class="modal-title" id="myModalLabel">Generated code for <%=ESAPI.encoder().encodeForHTML(moduleName) %> module</h4>
  <ul class="nav nav-tabs tabs-pull-right" id="myTab3">
    <li>
      <a href="#" data-mode="Settings">Settings</a>
    </li>
    <li>
      <a href="#" data-mode="Permissions">Permissions</a>
    </li>
    <li>
      <a href="#" data-mode="Category">Category</a>
    </li>
    <li>
      <a href="#" data-mode="Elements">Elements</a>
    </li>
  </ul>
</div>
<div class="modal-body">
  <legend class="col">Java Code</legend>
  <pre><%=ESAPI.encoder().encodeForHTML(new CodeGenerator().generateModule(request.getParameter("moduleName")).toString()) %></pre>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>