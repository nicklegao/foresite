<%@page import="au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper"%>
<%@ page import="java.util.*"%>
<%@ page import="webdirector.db.client.ClientDataAccess"%>
<%@ page import="au.net.webdirector.common.datalayer.admin.db.*"%>
<%@ page import="org.apache.commons.lang.*"%>
<%
	au.net.webdirector.common.utils.module.DropDownBuilder ddb = new au.net.webdirector.common.utils.module.DropDownBuilder();
  String tablePrefix = (String)session.getValue("moduleSelected");
  String type = request.getParameter("type");
  String colName = request.getParameter("colName");

  Map mapOfExistingRecords = ddb.getDropDownValues(tablePrefix, colName, type);
%>

<style>
#dropDownBuilderForm th {
  vertical-align: middle;
}

#dropDownBuilderForm .popover {
  max-width: 400px;
}

</style>

<form method="POST" action="<%= request.getContextPath() %>/webdirector/secure/module/configure/dropdown/values" name="dropDownBuilderForm" id="dropDownBuilderForm" class="">
<div class="modal-header">
  <button type="button" class="close"><span>&times;</span>
  </button>
  <h4 class="modal-title" id="myModalLabel">Configure values for <b><%=request.getParameter("colName")%></b> of <b><%=(String)session.getValue("moduleSelected")%></b></h4>
</div>
<div class="modal-body">
<input type="hidden" name="action" value="save"/>
<input type="hidden" name="colName" value="<%=colName%>"/>
<input type="hidden" name="type" value="<%=type%>"/>

<table class="table table-condensed">
  <thead>
    <tr>
      <th style="width: 26px;">
        <label>
          <input type="checkbox" class="checkbox style-3" value="" title="ALL"><span></span>
        </label>
      </th>
      <th>STORED VALUE</th>
      <th>DROP DOWN LABEL</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
  <tr>
    <td></td>
    <td><input type="text" class="form-control input-xs" name="dropDownName" value="" ></td>
    <td><input type="text" class="form-control input-xs" name="dropDownValue" value=""></td>
    <td><button type="button" class="btn btn-raised btn-xs btn-info btn-macro">Macro</button></td>
  </tr>
<%
  Iterator it =  mapOfExistingRecords.keySet().iterator();
  while(it.hasNext()){
    String idE = (String)it.next();
    String value = (String)mapOfExistingRecords.get(idE);

    StringTokenizer str = new StringTokenizer(value,"|");
%>
  <tr>
    <td>
      <label>
          <input type="checkbox" class="checkbox style-3" value="<%=idE%>"><span></span>
      </label>
    </td>
    <td>
      <input type="hidden" name="id" value="<%=idE%>">
      <input type="text" class="form-control input-xs" name="DD_NAME" value="<%=StringEscapeUtils.escapeHtml(str.nextToken())%>">
    </td>
    <td>
      <input type="text" class="form-control input-xs" name="DD_VALUE" value="<%=StringEscapeUtils.escapeHtml(str.nextToken())%>">
    </td>
    <td><button type="button" class="btn btn-raised btn-xs btn-info btn-macro">Macro</button></td>
  </tr>
<%
  }
%>
</tbody>
</table>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-raised btm-sm btn-danger pull-left" id="btnDelete">Delete</button>
  <button type="button" class="btn btn-raised btm-sm btn-primary" id="btnSave">Save</button>
  <!-- <button type="button" class="btn btn-raised btm-sm btn-info">Help</button> -->
</div>
</form>

<script>
  function reloadModal() {
    var content = $("#dropDownValueContent");
    content.load('<%= request.getContextPath() %>/webdirector/secure/module/configure/dropdown/values',
      {
        type: "<%=type%>",
        colName: "<%=colName%>"
      },
      function() {
        content.find(".modal-body").css("height", $(window).height() * 0.4);
      }
    );
  }

  $(function(){
    var bSaveMacro;
    var valuesForm = $("#dropDownBuilderForm");

    valuesForm.find("button.close").click(function() {
      $(this).closest(".modal").modal("hide");
    });

    valuesForm.find("input:checkbox").not("[title]").click(function() {
      if (!$(this).is(":checked")){  
        valuesForm.find("input:checkbox[title]").prop("checked", false);
      }
    });

    valuesForm.find("input:checkbox[title]").click(function() {
      if ($(this).is(":checked")){  
        valuesForm.find("input:checkbox").not("[title]").prop("checked", true);
      } else {
        valuesForm.find("input:checkbox").not("[title]").prop("checked", false);
      }
    });

    valuesForm.find("#btnDelete").click(function() {
      var checkedID = [];
      valuesForm.find(":checked").each(function(){
        if($(this).attr("title")){
          return;
        }
        checkedID.push($(this).val());      
      });
      if (checkedID.length === 0) {
        alert("Please select at least one record.");
        return;
      }
      var sProp = "these records";
      if (checkedID.length == 1) {
        sProp = "this record";
      }
      if (confirm("Are you sure you want to delete "+ sProp +"?")) {
        $.ajax({
          url: '<%= request.getContextPath() %>/webdirector/configureModule/deleteDropDownValue?dropDownId=' + checkedID.join(","),
        }).done(function(){
          reloadModal();
        });
      }
    });

    valuesForm.find("#btnSave").click(function() {
      var nameValid = true;
      var valueValid = true;
      
      valuesForm.find('input[name="DD_NAME"]').each(function(){
          if($(this).val() === ''){
            nameValid = false;
          }
      });
      valuesForm.find('input[name="DD_VALUE"]').each(function(){
            if($(this).val() === ''){
              valueValid = false;
            }
        });
      if(!nameValid){
        alert("Please enter drop down value");
        return false;
      }
      if(!valueValid){
        alert("Please enter drop down label");
        return false;
      }
      
      if((document.dropDownBuilderForm.dropDownName.value === "" && document.dropDownBuilderForm.dropDownValue.value !== "") ||
         (document.dropDownBuilderForm.dropDownName.value !== "" && document.dropDownBuilderForm.dropDownValue.value === "")){
        if(document.dropDownBuilderForm.dropDownName.value === ""){
            alert("Please enter drop down value");
              document.dropDownBuilderForm.dropDownName.focus();
              return false;
        }
        if(document.dropDownBuilderForm.dropDownValue.value === ""){
          alert("Please enter drop down label");
          document.dropDownBuilderForm.dropDownValue.focus();
          return false;
        }
      }
      $.ajax({
        url: '<%= request.getContextPath() %>/webdirector/configureModule/saveDropDownValue?type=<%=type%>&colName=<%=colName%>&moduleName=<%=tablePrefix%>',
        data: $(valuesForm).serializeArray()
      }).done(function(){
    	  reloadModal();
    	  setTimeout(function(){
    		  ModalHelper.showNotification({success: true, content: "Macro Saved", container: "#dropDownValueContent"});   		  
    	  }, 500);
      });
    });

    valuesForm.find(".btn-macro").popover({
      html : true,
      placement : "left",
      title : "Macro Builder",
      content : $("#macroBuilderPopover").html(),
      container : valuesForm,
    });

    valuesForm.find(".btn-macro").on("shown.bs.popover", function() {
      var $form = $(".popover #macroBuilderForm");
      var $this = $(this);

      $form.find('select[name="modType"]').change(function(){
        if ($(this).val() === 'CATEGORY') {
          $form.find('select[name="catLevel"]').closest(".row").css("visibility", "visible");
        } else {
          $form.find('select[name="catLevel"]').closest(".row").css("visibility", "hidden");
        }
      });
      
      $form.find('select[name="macroType"]').change(function(){
			var popover = $form.closest('.popover');$()
			var top = popover.offset().top;
    	  if ($(this).val() === 'module') {
            $form.find('select[name="modType"]').closest(".row").hide();
            $form.find('input[name="catName"]').closest(".row").hide();
            top += 46;
          } else {
            $form.find('select[name="modType"]').closest(".row").show();
            $form.find('input[name="catName"]').closest(".row").show();
            top -= 46;
          }
    	  popover.offset({top:top})
        });

      $form.find('select[name="modType"], select[name="modName"]').change(function(){
        if ($('select[name="modName"]').val() !== '') {
          $.ajax({
            type: 'POST',
            url: '<%= request.getContextPath() %>/webdirector/configureModule/getMacroColumns',
            data: 'modName=' + $('select[name="modName"]').val() + '&modType=' + $('select[name="modType"]').val(),
            dataType: 'json'
          }).done(function(data){
            $form.find('select[name="optLabel"] option:not(:first)').remove();
            $form.find('select[name="optValue"] option:not(:first)').remove();
            for (var i in data) {
              var opt = data[i];
              $('<option/>').attr('value', opt.Label_InternalName).text(opt.Label_ExternalName + ' (' + opt.Label_InternalName + ')').appendTo('select[name="optLabel"]');
              $('<option/>').attr('value', opt.Label_InternalName).text(opt.Label_ExternalName + ' (' + opt.Label_InternalName + ')').appendTo('select[name="optValue"]');
            }
          });
        }
      });

      var buildMacro = function() {
        var fields = {};
        $.each($form.serializeArray(), function(i, field) {
            fields[field.name] = field.value;
        });

        var macroBuilt = '[[';
        
        if(fields.macroType === 'module') {
        	macroBuilt += fields.optValue + ', ' + fields.optLabel;
        }
        else {
        	if (fields.modType === 'ELEMENT') {
                macroBuilt += 'SELECT e.' + fields.optLabel + ', e.' + fields.optValue + ' FROM elements_' + fields.modName + ' e';
                if (fields.catName !== '') {
                  macroBuilt += ', categories_' + fields.modName + ' c WHERE e.Category_id=c.Category_id AND c.ATTR_categoryName=\'' + fields.catName + '\'';
                }
              }
              else if (fields.modType === 'CATEGORY') {
                macroBuilt += 'SELECT c.' + fields.optLabel + ', c.' + fields.optValue + ' FROM categories_' + fields.modName + ' c';
                if (fields.catName !== '') {
                  macroBuilt += ', categories_' + fields.modName + ' p WHERE c.Category_ParentID=p.Category_id AND p.ATTR_categoryName=\'' + fields.catName + '\'';
                  macroBuilt += ' AND c.folderLevel=' + fields.catLevel;
                } else {
                  macroBuilt += ' WHERE c.folderLevel=' + fields.catLevel;
                }
              }
        }
        
        macroBuilt += ']]';
        $this.closest('tr').find('input[name="DD_NAME"], input[name="dropDownName"]').val('[['+(fields.macroType === 'module'? fields.modName:'MACRO')+']]');
        $this.closest('tr').find('input[name="DD_VALUE"], input[name="dropDownValue"]').val(macroBuilt);
        $this.popover('hide');
      };

      // Validation
      $form.validate({
        // Rules for form validation
        rules : {
          modName : {
            required : true,
          },
          optLabel : {
            required : true,
          },
          optValue : {
            required : true
          }
        },

        // Messages for form validation
        messages : {
          modName : {
            required : 'Module Name cannot be empty',
          },
          optLabel : {
            required : 'Label Field cannot be empty'
          },
          optValue : {
            required : 'Value Field cannot be empty'
          }
        },

        submitHandler : buildMacro,

        // Do not change code below
        errorPlacement : function(error, element) {
          error.insertAfter(element.parent());
        }
      });

    });

    valuesForm.on("click", function (e) {
        valuesForm.find(".btn-macro").each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
    
  });
</script>
<div id="macroBuilderPopover" style="display:none">
<form id="macroBuilderForm" class="smart-form">
  <div class="row">
    <label class="label col col-4">Macro Type</label>
    <section class="col col-8">
      <label class="select">
        <select name="macroType" class="input-sm">
        	<option value="moduleCatEle" selected>Module Category &amp; Element</option>
          	<option value="module">Module</option>
        </select> <i></i> </label>
    </section>
  </div>
  <div class="row">
    <label class="label col col-4">Module Type</label>
    <section class="col col-8">
      <label class="select">
        <select name="modType" class="input-sm">
          <option value="ELEMENT">Element</option>
          <option value="CATEGORY">Category</option>
        </select> <i></i> </label>
    </section>
  </div>
  <div class="row">
    <label class="label col col-4">Module Name</label>
    <section class="col col-8">
      <label class="select">
        <select name="modName" class="input-sm">
        <option value="">Please Choose...</option>
<%
      String sql = "SELECT internal_module_name, client_module_name FROM sys_moduleconfig";
      List<Hashtable<String, String>> moduleNameList = DataAccess.getInstance().select(sql, new HashtableMapper());
      for (Hashtable<String, String> moduleName : moduleNameList) {
        %>
        <option value="<%= moduleName.get("internal_module_name") %>"><%= moduleName.get("client_module_name") + " (" + moduleName.get("internal_module_name") + ")" %></option>
        <%
      }
%>
        </select> <i></i> </label>
    </section>
  </div>
  <div class="row">
    <label class="label col col-4">Display Label</label>
    <section class="col col-8">
      <label class="select">
        <select name="optLabel" class="input-sm">
          <option value=""></option>
        </select> <i></i> </label>
    </section>
  </div>
  <div class="row">
    <label class="label col col-4">Storing Value</label>
    <section class="col col-8">
      <label class="select">
        <select name="optValue" class="input-sm">
          <option value=""></option>
        </select> <i></i> </label>
    </section>
  </div>
  <div class="row">
    <label class="label col col-4">Category Name</label>
    <section class="col col-8">
      <label class="input">
        <input type="text" name="catName">
      </label>
    </section>
  </div>
  <div class="row" style="visibility: hidden;">
    <label class="label col col-4">Category Level</label>
    <section class="col col-8">
      <label class="select">
        <select name="catLevel" class="input-sm">
          <option value="1">1</option>
          <option value="2">2</option>
        </select> <i></i> </label>
    </section>
  </div>
  <div class="form-actions">
    <button type="submit" class="btn btn-raised btn-sm btn-success" id="btnApply">Apply</button>
  </div>
</form>
</div>
</body>
</html>