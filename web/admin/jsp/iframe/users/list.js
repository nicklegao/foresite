$(document).ready(function()
{
	var domHead = "<'navbar' <'navbar-form navbar-left'<'form-group show-page' l>>  <'navbar-form navbar-right'<'form-group greentech-panel-group' f>> >";
	var domBody = "<'responsive-table-wrapper' t>";
	var domFooter = "<'navbar' <'navbar-form navbar-left' i> <'navbar-form navbar-right'<'form-group pagination-bar' p>> >";

	var dashboardTable = $('#paymentTable').DataTable({
		"aaSorting" : [ [ 0, "asc" ] ],
		"autoWidth" : true,
		"processing" : true,
		"serverSide" : true,
		"ajax" : {
			"url" : "/webdirector/app/data/users",
			"method" : "POST",
		},
		"aoColumns" : [ {
			title : 'Login as',
			data : 'element_id',
			render : function(data, type, row)
			{
				if (type == 'display')
				{
					var btn = $('<a class="btn btn-success btn-login-as" target="_blank">').attr('href', '/webdirector/app/loginas/verification?email=' + encodeURIComponent(row.attr_headline)).text('Login');
					return $("<div>").append(btn).html();
				}
				else
				{
					return row.element_id;
				}
			}
		}, {
			title : 'Email',
			data : 'attr_headline',
			render : function(data, type, row)
			{
				return row.attr_headline;
			}
		}, {
			title : 'Company',
			data : 'company',
			render : function(data, type, row)
			{
				return row.company;
			}
		}, {
			title : 'First Name',
			data : 'attr_name',
			render : function(data, type, row)
			{
				return row.attr_name;
			}
		}, {
			title : 'Last Name',
			data : 'attr_surname',
			render : function(data, type, row)
			{
				return row.attr_surname;
			}
		} ],
		"oLanguage" : {
			"sLengthMenu" : '<label>Show</label> <div class="per-page short-select-option"> _MENU_ </div> <span>per page</span>',
			"sSearch" : "",
			"oPaginate" : {
				"sPrevious" : '<i class="fa fa-arrow-left"></i>',
				"sNext" : '<i class="fa fa-arrow-right"></i>'
			},
			"sInfo" : '<i class="fa fa-spin fa-cog dashboardLoadingIndicator"></i> Showing _START_ to _END_ of _TOTAL_ users',
			"sInfoEmpty" : "No users found",
		},
		// "aaSorting" : [ [ 15, 'desc' ] ],
		"aLengthMenu" : [ [ 10, 20, 50, 100 ], [ 10, 20, 50, 100 ] ],
		// // set the initial value
		"iDisplayLength" : 10,
		"sDom" : domHead + domBody + domFooter,
	}).on('preXhr.dt', function(e, settings, json, xhr)
	{
		$(".dashboardLoadingIndicator").addClass('fa-spin fa-cog').removeClass('fa-table');
	}).on('xhr.dt', function(e, settings, json, xhr)
	{
	}).on('draw.dt', function(e, settings)
	{
		$(".dashboardLoadingIndicator").removeClass('fa-spin fa-cog').addClass('fa-table');
	});

});
