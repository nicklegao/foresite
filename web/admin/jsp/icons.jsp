<script>
$(function() {
    var $icons = $(".fontawesome-icon-list > div > a");
    $icons.contents().filter(function() {
        return this.nodeType === 3;
    }).each(function(){
        this.textContent = "";
    });
    $icons.find("span").remove();

    $(".color-select").on("click", "span", function() {
      var moduleColor = $("#editModuleColor");
      var selectedColor = $(this).attr("class");
      $(this).closest(".icon-picker").find("button").removeClass(moduleColor.val()).addClass(selectedColor);
      moduleColor.val(selectedColor);

    });

    $(".icon-select").on("click", "a", function() {
      var moduleIcon = $("#editModuleIcon");
      var selectedIcon = $(this).find("i").attr("class");
      $(this).closest(".icon-picker").find("button i:first").removeClass(moduleIcon.val()).addClass(selectedIcon);
      moduleIcon.val(selectedIcon);
    });

    function hide_divs(search) {
      $(".icon-item").hide(); // hide all divs
      $(".icon-select h5").hide();
      $('.icon-item > a > i[class*="'+search+'"]').parent().parent().show(); // show the ones that match
    }
    
    function show_all() {
      $(".icon-item").show();
      $(".icon-select h5").show();
    }
    
    $("#icon-search").keyup(function() {
      var search = $.trim(this.value);
      if (search === "") {
        show_all();
      }
      else {
        hide_divs(search);
      }
    });

});

</script>

<style>
.row.fontawesome-icon-list {
  margin: 0;
  padding-right: 5px;
}

.icon-select .fa, .icon-select .fal, .icon-select .far, .icon-select .fab {
  font-size: 22px;
}

.icon-select a {
  color: inherit;
}

.icon-select {
  height: 300px;
  overflow-y: auto;
  overflow-x: hidden;
}

.icon-select .page-header {
  border-top: 1px solid #eee;
  margin-top: 10px;
  padding-top: 8px;
  padding-left: 5px;
}

.fontawesome-icon-list .icon-item {
    margin-top: 5px;
}

.color-select {
	max-width: 478px;
}

.icon-picker .dropdown-menu {
	width: 478px;
}

@media (max-width: 768px) {
	.icon-picker .dropdown-menu {
		width: 244px;
	}
}
</style>

<div class="container-fluid dropdown-menu arrow-box-up-right pull-right">

<ul class="color-select clearfix">
  <li><span class="bg-color-green txt-color-white" data-toggle="tooltip" data-placement="left" title="Green Grass"></span>
  </li>
  <li><span class="bg-color-greenDark txt-color-white" data-toggle="tooltip" data-placement="top" title="Dark Green"></span>
  </li>
  <li><span class="bg-color-greenLight txt-color-white" data-toggle="tooltip" data-placement="top" title="Light Green"></span>
  </li>
  <li><span class="bg-color-purple txt-color-white" data-toggle="tooltip" data-placement="top" title="Purple"></span>
  </li>
  <li><span class="bg-color-magenta txt-color-white" data-toggle="tooltip" data-placement="top" title="Magenta"></span>
  </li>
  <li><span class="bg-color-pink txt-color-white" data-toggle="tooltip" data-placement="right" title="Pink"></span>
  </li>
  <li><span class="bg-color-pinkDark txt-color-white" data-toggle="tooltip" data-placement="left" title="Fade Pink"></span>
  </li>
  <li><span class="bg-color-blueLight txt-color-white" data-toggle="tooltip" data-placement="top" title="Light Blue"></span>
  </li>
  <li><span class="bg-color-teal txt-color-white" data-toggle="tooltip" data-placement="top" title="Teal"></span>
  </li>
  <li><span class="bg-color-blue txt-color-white" data-toggle="tooltip" data-placement="top" title="Ocean Blue"></span>
  </li>
  <li><span class="bg-color-blueDark txt-color-white" data-toggle="tooltip" data-placement="top" title="Night Sky"></span>
  </li>
  <li><span class="bg-color-darken txt-color-white" data-toggle="tooltip" data-placement="right" title="Night"></span>
  </li>
  <li><span class="bg-color-yellow txt-color-white" data-toggle="tooltip" data-placement="left" title="Day Light"></span>
  </li>
  <li><span class="bg-color-orange txt-color-white" data-toggle="tooltip" data-placement="bottom" title="Orange"></span>
  </li>
  <li><span class="bg-color-orangeDark txt-color-white" data-toggle="tooltip" data-placement="bottom" title="Dark Orange"></span>
  </li>
  <li><span class="bg-color-red txt-color-white" data-toggle="tooltip" data-placement="bottom" title="Red Rose"></span>
  </li>
  <li><span class="bg-color-redLight txt-color-white" data-toggle="tooltip" data-placement="bottom" title="Light Red"></span>
  </li>
  <li><span class="bg-color-blueNeon txt-color-white" data-toggle="tooltip" data-placement="right" title="Electric Blue"></span>
  </li>
</ul>

<div class="">
    <div class="input-group">
        <input class="form-control input-sm" type="text" id="icon-search" placeholder="Search icons">
        <span class="input-group-addon"><i class="fa fa-fw fa-search"></i></span>
    </div>
</div>

<div class="icon-select">

  <section id="web-application">

  <div class="row fontawesome-icon-list">
    
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-adjust"></i> adjust</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-anchor"></i> anchor</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-archive"></i> archive</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chart-area"></i> chart-area</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrows"></i> arrows</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrows-h"></i> arrows-h</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrows-v"></i> arrows-v</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-asterisk"></i> asterisk</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-at"></i> at</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-car"></i> car<span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-balance-scale"></i> balance-scale</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-ban"></i> ban</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-university"></i> university <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chart-bar"></i> chart-bar</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-barcode"></i> barcode</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bars"></i> bars</a></div>
     <!--  <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-battery-0"></i> battery-0 <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-battery-1"></i> battery-1 <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-battery-2"></i> battery-2 <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-battery-3"></i> battery-3 <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-battery-4"></i> battery-4 <span class="text-muted">(alias)</span></a></div> -->
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-battery-empty"></i> fal battery-empty</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-battery-full"></i> fal battery-full</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-battery-half"></i> fal battery-half</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-battery-quarter"></i> fal battery-quarter</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-battery-empty"></i> battery-empty</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-battery-full"></i> battery-full</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-battery-half"></i> battery-half</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-battery-quarter"></i> battery-quarter</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-battery-three-quarters"></i> battery-three-quarters</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bed"></i> bed</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-beer"></i> beer</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bell"></i> bell</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-bell"></i> fal bell</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bell-slash"></i> bell-slash</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-bell-slash"></i> fal bell-slash</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bicycle"></i> bicycle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-binoculars"></i> binoculars</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-birthday-cake"></i> birthday-cake</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bolt"></i> bolt</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bomb"></i> bomb</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-book"></i> book</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bookmark"></i> bookmark</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-bookmark"></i> fal bookmark</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-briefcase"></i> briefcase</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bug"></i> bug</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-building"></i> building</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-building"></i> fal building</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bullhorn"></i> bullhorn</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bullseye"></i> bullseye</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bus"></i> bus</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-taxi"></i> taxi<span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-calculator"></i> calculator</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-calendar"></i> calendar</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-calendar-check"></i> fal calendar-check</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-calendar-minus"></i> fal calendar-minus</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-calendar"></i> fal calendar</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-calendar-plus"></i> fal calendar-plus</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-calendar-times"></i> fal calendar-times</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-camera"></i> camera</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-camera-retro"></i> camera-retro</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-car"></i> car</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-caret-square-down"></i> caret-square-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-caret-square-left"></i> caret-square-left</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-caret-square-right"></i> caret-square-right</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-caret-square-up"></i> caret-square-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cart-arrow-down"></i> cart-arrow-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cart-plus"></i> cart-plus</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="far fa-closed-captioning"></i> far closed-captioning</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-certificate"></i> certificate</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-check"></i> check</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-check-circle"></i> check-circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-check-circle"></i> fal check-circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-check-square"></i> check-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-check-square"></i> fal check-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-child"></i> child</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-circle"></i> circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="far fa-circle"></i> far circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-circle-notch"></i> circle-notch</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-circle"></i> fal circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="far fa-clock"></i> far clock</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-clone"></i> clone</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-times"></i> close <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cloud"></i> cloud</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cloud-download"></i> cloud-download</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cloud-upload"></i> cloud-upload</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-code"></i> code</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="far fa-code-branch"></i> far code-branch</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-coffee"></i> coffee</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cog"></i> cog</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cogs"></i> cogs</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-comment"></i> comment</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="far fa-comment"></i> far comment</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-comment-dots"></i> comment-dots</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="far fa-comment-dots"></i> far comment-dots</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-comments"></i> comments</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="far fa-comments"></i> far comments</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-compass"></i> compass</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-copyright"></i> copyright</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-creative-commons"></i> fab creative-commons</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-credit-card"></i> credit-card</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-crop"></i> crop</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-crosshairs"></i> crosshairs</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cube"></i> cube</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cubes"></i> cubes</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-utensils"></i> utensils</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-tachometer-alt"></i> tachometer-alt<span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-database"></i> database</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-desktop"></i> desktop</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-diamond"></i> diamond</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="far fa-dot-circle"></i> far dot-circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-download"></i> download</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-edit"></i> edit <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-ellipsis-h"></i> ellipsis-h</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-ellipsis-v"></i> ellipsis-v</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-envelope"></i> envelope</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-envelope"></i> fal envelope</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-envelope-square"></i> envelope-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-eraser"></i> eraser</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-exchange"></i> exchange</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-exclamation"></i> exclamation</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-exclamation-circle"></i> exclamation-circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-exclamation-triangle"></i> exclamation-triangle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-external-link"></i> external-link</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-external-link-square"></i> external-link-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-eye"></i> eye</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-eye-slash"></i> eye-slash</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-eyedropper"></i> eyedropper</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-fax"></i> fax</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-feed"></i> feed <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-female"></i> female</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-fighter-jet"></i> fighter-jet</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-archive-o"></i> file-archive-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-audio-o"></i> file-audio-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-code-o"></i> file-code-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-excel-o"></i> file-excel-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-image-o"></i> file-image-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-movie-o"></i> file-movie-o <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-pdf-o"></i> file-pdf-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-photo-o"></i> file-photo-o <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-picture-o"></i> file-picture-o <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-powerpoint-o"></i> file-powerpoint-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-sound-o"></i> file-sound-o <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-video-o"></i> file-video-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-word-o"></i> file-word-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-zip-o"></i> file-zip-o <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-film"></i> film</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-filter"></i> filter</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-fire"></i> fire</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-fire-extinguisher"></i> fire-extinguisher</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-flag"></i> flag</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-flag-checkered"></i> flag-checkered</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-flag-o"></i> flag-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-flash"></i> flash <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-flask"></i> flask</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-folder"></i> folder</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-folder-o"></i> folder-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-folder-open"></i> folder-open</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-folder-open-o"></i> folder-open-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-frown-o"></i> frown-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-futbol-o"></i> futbol-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-gamepad"></i> gamepad</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-gavel"></i> gavel</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-gear"></i> gear <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-gears"></i> gears <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-gift"></i> gift</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-glass"></i> glass</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-globe"></i> globe</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-graduation-cap"></i> graduation-cap</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-group"></i> group <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-grab-o"></i> hand-grab-o <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-lizard-o"></i> hand-lizard-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-paper-o"></i> hand-paper-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-peace-o"></i> hand-peace-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-pointer-o"></i> hand-pointer-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-rock-o"></i> hand-rock-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-scissors-o"></i> hand-scissors-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-spock-o"></i> hand-spock-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-stop-o"></i> hand-stop-o <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hdd-o"></i> hdd-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-headphones"></i> headphones</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-heart"></i> heart</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-heart-o"></i> heart-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-heartbeat"></i> heartbeat</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-history"></i> history</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-home"></i> home</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hotel"></i> hotel <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hourglass"></i> hourglass</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hourglass-1"></i> hourglass-1 <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hourglass-2"></i> hourglass-2 <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hourglass-3"></i> hourglass-3 <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hourglass-end"></i> hourglass-end</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hourglass-half"></i> hourglass-half</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hourglass-o"></i> hourglass-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hourglass-start"></i> hourglass-start</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-i-cursor"></i> i-cursor</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-image"></i> image <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-inbox"></i> inbox</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-industry"></i> industry</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-info"></i> info</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-info-circle"></i> info-circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-institution"></i> institution <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-key"></i> key</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-keyboard-o"></i> keyboard-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-language"></i> language</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-laptop"></i> laptop</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-leaf"></i> leaf</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-legal"></i> legal <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-lemon-o"></i> lemon-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-level-down"></i> level-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-level-up"></i> level-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-life-bouy"></i> life-bouy <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-life-buoy"></i> life-buoy <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-life-ring"></i> life-ring</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-life-saver"></i> life-saver <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-lightbulb-o"></i> lightbulb-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-line-chart"></i> line-chart</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-location-arrow"></i> location-arrow</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-lock"></i> lock</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-magic"></i> magic</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-magnet"></i> magnet</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-mail-forward"></i> mail-forward <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-mail-reply"></i> mail-reply <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-mail-reply-all"></i> mail-reply-all <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-male"></i> male</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-map"></i> map</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-map-marker"></i> map-marker</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-map-o"></i> map-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-map-pin"></i> map-pin</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-map-signs"></i> map-signs</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-meh-o"></i> meh-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-microphone"></i> microphone</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-microphone-slash"></i> microphone-slash</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-minus"></i> minus</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-minus-circle"></i> minus-circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-minus-square"></i> minus-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-minus-square-o"></i> minus-square-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-mobile"></i> mobile</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-mobile-phone"></i> mobile-phone <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-money"></i> money</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-moon-o"></i> moon-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-mortar-board"></i> mortar-board <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-motorcycle"></i> motorcycle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-mouse-pointer"></i> mouse-pointer</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-music"></i> music</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-navicon"></i> navicon <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-newspaper-o"></i> newspaper-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-object-group"></i> object-group</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-object-ungroup"></i> object-ungroup</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-paint-brush"></i> paint-brush</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-paper-plane"></i> paper-plane</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-paper-plane-o"></i> paper-plane-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-paw"></i> paw</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-pencil"></i> pencil</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-pencil-square"></i> pencil-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-pencil-square-o"></i> pencil-square-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-phone"></i> phone</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-phone-square"></i> phone-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-photo"></i> photo <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-picture-o"></i> picture-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-pie-chart"></i> pie-chart</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-plane"></i> plane</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-plug"></i> plug</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-plus"></i> plus</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-plus-circle"></i> plus-circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-plus-square"></i> plus-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-plus-square-o"></i> plus-square-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-power-off"></i> power-off</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-print"></i> print</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-puzzle-piece"></i> puzzle-piece</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-qrcode"></i> qrcode</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-question"></i> question</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-question-circle"></i> question-circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-quote-left"></i> quote-left</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-quote-right"></i> quote-right</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-random"></i> random</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-recycle"></i> recycle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-refresh"></i> refresh</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-registered"></i> registered</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-remove"></i> remove <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-reorder"></i> reorder <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-reply"></i> reply</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-reply-all"></i> reply-all</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-retweet"></i> retweet</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-road"></i> road</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-rocket"></i> rocket</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-rss"></i> rss</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-rss-square"></i> rss-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-search"></i> search</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-search-minus"></i> search-minus</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-search-plus"></i> search-plus</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-send"></i> send <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-send-o"></i> send-o <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-server"></i> server</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-share"></i> share</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-share-alt"></i> share-alt</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-share-alt-square"></i> share-alt-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-share-square"></i> share-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-share-square-o"></i> share-square-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-shield"></i> shield</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-ship"></i> ship</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-shopping-cart"></i> shopping-cart</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sign-in"></i> sign-in</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sign-out"></i> sign-out</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-signal"></i> signal</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sitemap"></i> sitemap</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sliders"></i> sliders</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-smile-o"></i> smile-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-soccer-ball-o"></i> soccer-ball-o <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sort"></i> sort</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sort-alpha-asc"></i> sort-alpha-asc</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sort-alpha-desc"></i> sort-alpha-desc</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sort-amount-asc"></i> sort-amount-asc</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sort-amount-desc"></i> sort-amount-desc</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sort-asc"></i> sort-asc</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sort-desc"></i> sort-desc</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sort-down"></i> sort-down <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sort-numeric-asc"></i> sort-numeric-asc</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sort-numeric-desc"></i> sort-numeric-desc</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sort-up"></i> sort-up <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-space-shuttle"></i> space-shuttle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-spinner"></i> spinner</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-spoon"></i> spoon</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-square"></i> square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-square-o"></i> square-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-star"></i> star</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-star-half"></i> star-half</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-star-half-empty"></i> star-half-empty <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-star-half-full"></i> star-half-full <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-star-half-o"></i> star-half-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-star-o"></i> star-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sticky-note"></i> sticky-note</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sticky-note-o"></i> sticky-note-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-street-view"></i> street-view</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-suitcase"></i> suitcase</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sun-o"></i> sun-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-support"></i> support <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-tablet"></i> tablet</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-tachometer"></i> tachometer</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-tag"></i> tag</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-tags"></i> tags</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-tasks"></i> tasks</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-taxi"></i> taxi</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-television"></i> television</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-terminal"></i> terminal</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-thumb-tack"></i> thumb-tack</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-thumbs-down"></i> thumbs-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-thumbs-o-down"></i> thumbs-o-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-thumbs-o-up"></i> thumbs-o-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-thumbs-up"></i> thumbs-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-ticket"></i> ticket</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-times"></i> times</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-times-circle"></i> times-circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-times-circle-o"></i> times-circle-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-tint"></i> tint</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-toggle-down"></i> toggle-down <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-toggle-left"></i> toggle-left <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-toggle-off"></i> toggle-off</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-toggle-on"></i> toggle-on</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-toggle-right"></i> toggle-right <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-toggle-up"></i> toggle-up <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-trademark"></i> trademark</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-trash"></i> trash</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-trash-o"></i> trash-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-tree"></i> tree</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-trophy"></i> trophy</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-truck"></i> truck</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-tty"></i> tty</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-tv"></i> tv <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-umbrella"></i> umbrella</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-university"></i> university</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-unlock"></i> unlock</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-unlock-alt"></i> unlock-alt</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-unsorted"></i> unsorted <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-upload"></i> upload</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-user"></i> user</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-user-plus"></i> user-plus</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-user-secret"></i> user-secret</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-user-times"></i> user-times</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-users"></i> users</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-video-camera"></i> video-camera</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-volume-down"></i> volume-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-volume-off"></i> volume-off</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-volume-up"></i> volume-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-warning"></i> warning <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-wheelchair"></i> wheelchair</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-wifi"></i> wifi</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-wrench"></i> wrench</a></div>
    
  </div>

</section>

  <section id="hand">
  <h2 class="page-header">Hand Icons</h2>

  <div class="row fontawesome-icon-list">
    

      <!-- <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-grab-o"></i> hand-grab-o <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-lizard-o"></i> hand-lizard-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-o-down"></i> hand-o-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-o-left"></i> hand-o-left</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-o-right"></i> hand-o-right</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-o-up"></i> hand-o-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-paper-o"></i> hand-paper-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-peace-o"></i> hand-peace-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-pointer-o"></i> hand-pointer-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-rock-o"></i> hand-rock-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-scissors-o"></i> hand-scissors-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-spock-o"></i> hand-spock-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-stop-o"></i> hand-stop-o <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-thumbs-down"></i> thumbs-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-thumbs-o-down"></i> thumbs-o-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-thumbs-o-up"></i> thumbs-o-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-thumbs-up"></i> thumbs-up</a></div> -->

				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-allergies"></i> far fa-allergies</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-allergies"></i> fal fa-allergies</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-fist-raised"></i> far
						fa-fist-raised</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-fist-raised"></i> fal
						fa-fist-raised</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-hand-heart"></i> far fa-hand-heart</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-heart"></i> fal fa-hand-heart</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-hand-holding"></i> far
						fa-hand-holding</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-holding"></i> fal
						fa-hand-holding</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-hand-holding-box"></i> far
						fa-hand-holding-box</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-holding-box"></i> fal
						fa-hand-holding-box</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-hand-holding-heart"></i> far
						fa-hand-holding-heart</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-holding-heart"></i> fal
						fa-hand-holding-heart</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-hand-holding-magic"></i> far
						fa-hand-holding-magic</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-holding-magic"></i> fal
						fa-hand-holding-magic</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-hand-holding-seedling"></i> far
						fa-hand-holding-seedling</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-holding-seedling"></i> fal
						fa-hand-holding-seedling</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-hand-holding-usd"></i> far
						fa-hand-holding-usd</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-holding-usd"></i> fal
						fa-hand-holding-usd</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-hand-holding-water"></i> far
						fa-hand-holding-water</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-holding-water"></i> fal
						fa-hand-holding-water</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-lizard"></i> fal
						fa-hand-lizard</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-hand-middle-finger"></i> far
						fa-hand-middle-finger</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-middle-finger"></i> fal
						fa-hand-middle-finger</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-paper"></i> fal fa-hand-paper</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-peace"></i> fal fa-hand-peace</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-point-down"></i> fal
						fa-hand-point-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-point-left"></i> fal
						fa-hand-point-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-point-right"></i> fal
						fa-hand-point-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-point-up"></i> fal
						fa-hand-point-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-pointer"></i> fal
						fa-hand-pointer</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-hand-receiving"></i> far
						fa-hand-receiving</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-receiving"></i> fal
						fa-hand-receiving</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-rock"></i> fal fa-hand-rock</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-scissors"></i> fal
						fa-hand-scissors</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-spock"></i> fal fa-hand-spock</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-hands"></i> far fa-hands</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hands"></i> fal fa-hands</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-hands-heart"></i> far
						fa-hands-heart</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hands-heart"></i> fal
						fa-hands-heart</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-hands-helping"></i> far
						fa-hands-helping</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hands-helping"></i> fal
						fa-hands-helping</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-hands-usd"></i> far fa-hands-usd</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hands-usd"></i> fal fa-hands-usd</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-handshake"></i> fal fa-handshake</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-handshake-alt"></i> far
						fa-handshake-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-handshake-alt"></i> fal
						fa-handshake-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="far fa-praying-hands"></i> far
						fa-praying-hands</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-praying-hands"></i> fal
						fa-praying-hands</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-thumbs-down"></i> fal
						fa-thumbs-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-thumbs-up"></i> fal fa-thumbs-up</a>
				</div>

			</div>

</section>

  <section id="transportation">
  <h2 class="page-header">Transportation Icons</h2>

  <div class="row fontawesome-icon-list">
    

      <!-- <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-ambulance"></i> ambulance</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-automobile"></i> automobile <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bicycle"></i> bicycle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bus"></i> bus</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cab"></i> cab <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-car"></i> car</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-fighter-jet"></i> fighter-jet</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-motorcycle"></i> motorcycle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-plane"></i> plane</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-rocket"></i> rocket</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-ship"></i> ship</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-space-shuttle"></i> space-shuttle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-subway"></i> subway</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-taxi"></i> taxi</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-train"></i> train</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-truck"></i> truck</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-wheelchair"></i> wheelchair</a></div> -->

				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-archway"></i> fa fa-archway</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-archway"></i> fal fa-archway</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-atlas"></i> fa fa-atlas</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-atlas"></i> fal fa-atlas</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-bed"></i> fa fa-bed</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-bed"></i> fal fa-bed</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-bus"></i> fa fa-bus</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-bus"></i> fal fa-bus</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-bus-alt"></i> fa fa-bus-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-bus-alt"></i> fal fa-bus-alt</a>
				</div>
				<!-- <div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-car-bus"></i> fa fa-car-bus</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-car-bus"></i> fal fa-car-bus</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-cars"></i> fa fa-cars</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-cars"></i> fal fa-cars</a>
				</div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-cocktail"></i> fa fa-cocktail</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-cocktail"></i> fal fa-cocktail</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-concierge-bell"></i> fa
						fa-concierge-bell</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-concierge-bell"></i> fal
						fa-concierge-bell</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-dumbbell"></i> fa fa-dumbbell</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-dumbbell"></i> fal fa-dumbbell</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-glass-martini"></i> fa
						fa-glass-martini</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-glass-martini"></i> fal
						fa-glass-martini</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-glass-martini-alt"></i> fa
						fa-glass-martini-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-glass-martini-alt"></i> fal
						fa-glass-martini-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-globe-africa"></i> fa
						fa-globe-africa</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-globe-africa"></i> fal
						fa-globe-africa</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-globe-americas"></i> fa
						fa-globe-americas</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-globe-americas"></i> fal
						fa-globe-americas</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-globe-asia"></i> fa fa-globe-asia</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-globe-asia"></i> fal fa-globe-asia</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-globe-europe"></i> fa
						fa-globe-europe</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-globe-europe"></i> fal
						fa-globe-europe</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-hot-tub"></i> fa fa-hot-tub</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hot-tub"></i> fal fa-hot-tub</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-hotel"></i> fa fa-hotel</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hotel"></i> fal fa-hotel</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-island-tropical"></i> fa
						fa-island-tropical</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-island-tropical"></i> fal
						fa-island-tropical</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-luggage-cart"></i> fa
						fa-luggage-cart</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-luggage-cart"></i> fal
						fa-luggage-cart</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-map"></i> fa fa-map</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-map"></i> fal fa-map</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-map-marked"></i> fa fa-map-marked</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-map-marked"></i> fal fa-map-marked</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-map-marked-alt"></i> fa
						fa-map-marked-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-map-marked-alt"></i> fal
						fa-map-marked-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-monument"></i> fa fa-monument</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-monument"></i> fal fa-monument</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-passport"></i> fa fa-passport</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-passport"></i> fal fa-passport</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-plane"></i> fa fa-plane</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-plane"></i> fal fa-plane</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-plane-arrival"></i> fa
						fa-plane-arrival</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-plane-arrival"></i> fal
						fa-plane-arrival</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-plane-departure"></i> fa
						fa-plane-departure</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-plane-departure"></i> fal
						fa-plane-departure</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-rings-wedding"></i> fa
						fa-rings-wedding</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-rings-wedding"></i> fal
						fa-rings-wedding</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-shuttle-van"></i> fa fa-shuttle-van</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-shuttle-van"></i> fal
						fa-shuttle-van</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-spa"></i> fa fa-spa</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-spa"></i> fal fa-spa</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-suitcase"></i> fa fa-suitcase</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-suitcase"></i> fal fa-suitcase</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-suitcase-rolling"></i> fa
						fa-suitcase-rolling</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-suitcase-rolling"></i> fal
						fa-suitcase-rolling</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-swimmer"></i> fa fa-swimmer</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-swimmer"></i> fal fa-swimmer</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-swimming-pool"></i> fa
						fa-swimming-pool</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-swimming-pool"></i> fal
						fa-swimming-pool</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-taxi"></i> fa fa-taxi</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-taxi"></i> fal fa-taxi</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-tram"></i> fa fa-tram</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-tram"></i> fal fa-tram</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-tree-palm"></i> fa fa-tree-palm</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-tree-palm"></i> fal fa-tree-palm</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-umbrella-beach"></i> fa
						fa-umbrella-beach</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-umbrella-beach"></i> fal
						fa-umbrella-beach</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-wine-glass"></i> fa fa-wine-glass</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-wine-glass"></i> fal fa-wine-glass</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-wine-glass-alt"></i> fa
						fa-wine-glass-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-wine-glass-alt"></i> fal
						fa-wine-glass-alt</a>
				</div>

			</div>

</section>

  <section id="gender">
  <h2 class="page-header">Gender Icons</h2>

  <div class="row fontawesome-icon-list">
    

      <!-- <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-genderless"></i> genderless</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-intersex"></i> intersex <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-mars"></i> mars</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-mars-double"></i> mars-double</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-mars-stroke"></i> mars-stroke</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-mars-stroke-h"></i> mars-stroke-h</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-mars-stroke-v"></i> mars-stroke-v</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-mercury"></i> mercury</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-neuter"></i> neuter</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-transgender"></i> transgender</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-transgender-alt"></i> transgender-alt</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-venus"></i> venus</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-venus-double"></i> venus-double</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-venus-mars"></i> venus-mars</a></div> -->

				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-genderless"></i> fa fa-genderless</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-genderless"></i> fal fa-genderless</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-mars"></i> fa fa-mars</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-mars"></i> fal fa-mars</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-mars-double"></i> fa fa-mars-double</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-mars-double"></i> fal
						fa-mars-double</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-mars-stroke"></i> fa fa-mars-stroke</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-mars-stroke"></i> fal
						fa-mars-stroke</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-mars-stroke-h"></i> fa
						fa-mars-stroke-h</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-mars-stroke-h"></i> fal
						fa-mars-stroke-h</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-mars-stroke-v"></i> fa
						fa-mars-stroke-v</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-mars-stroke-v"></i> fal
						fa-mars-stroke-v</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-mercury"></i> fa fa-mercury</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-mercury"></i> fal fa-mercury</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-neuter"></i> fa fa-neuter</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-neuter"></i> fal fa-neuter</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-transgender"></i> fa fa-transgender</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-transgender"></i> fal
						fa-transgender</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-transgender-alt"></i> fa
						fa-transgender-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-transgender-alt"></i> fal
						fa-transgender-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-venus"></i> fa fa-venus</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-venus"></i> fal fa-venus</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-venus-double"></i> fa
						fa-venus-double</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-venus-double"></i> fal
						fa-venus-double</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-venus-mars"></i> fa fa-venus-mars</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-venus-mars"></i> fal fa-venus-mars</a>
				</div>

			</div>

</section>

  <section id="file-type">
  <h2 class="page-header">File Type Icons</h2>

  <div class="row fontawesome-icon-list">
    

      <!-- <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file"></i> file</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-archive-o"></i> file-archive-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-audio-o"></i> file-audio-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-code-o"></i> file-code-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-excel-o"></i> file-excel-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-image-o"></i> file-image-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-movie-o"></i> file-movie-o <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-o"></i> file-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-pdf-o"></i> file-pdf-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-photo-o"></i> file-photo-o <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-picture-o"></i> file-picture-o <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-powerpoint-o"></i> file-powerpoint-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-sound-o"></i> file-sound-o <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-text"></i> file-text</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-text-o"></i> file-text-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-video-o"></i> file-video-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-word-o"></i> file-word-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-zip-o"></i> file-zip-o <span class="text-muted">(alias)</span></a></div> -->

				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-archive"></i> fa fa-archive</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-archive"></i> fal fa-archive</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-clone"></i> fa fa-clone</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-clone"></i> fal fa-clone</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-copy"></i> fa fa-copy</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-copy"></i> fal fa-copy</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-cut"></i> fa fa-cut</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-cut"></i> fal fa-cut</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file"></i> fa fa-file</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file"></i> fal fa-file</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-alt"></i> fa fa-file-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-alt"></i> fal fa-file-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-archive"></i> fa
						fa-file-archive</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-archive"></i> fal
						fa-file-archive</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-audio"></i> fa fa-file-audio</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-audio"></i> fal fa-file-audio</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-check"></i> fa fa-file-check</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-check"></i> fal fa-file-check</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-code"></i> fa fa-file-code</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-code"></i> fal fa-file-code</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-edit"></i> fa fa-file-edit</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-edit"></i> fal fa-file-edit</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-excel"></i> fa fa-file-excel</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-excel"></i> fal fa-file-excel</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-exclamation"></i> fa
						fa-file-exclamation</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-exclamation"></i> fal
						fa-file-exclamation</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-image"></i> fa fa-file-image</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-image"></i> fal fa-file-image</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-minus"></i> fa fa-file-minus</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-minus"></i> fal fa-file-minus</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-pdf"></i> fa fa-file-pdf</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-pdf"></i> fal fa-file-pdf</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-plus"></i> fa fa-file-plus</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-plus"></i> fal fa-file-plus</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-powerpoint"></i> fa
						fa-file-powerpoint</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-powerpoint"></i> fal
						fa-file-powerpoint</a>
				</div>
				<!-- <div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-search"></i> fa fa-file-search</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-search"></i> fal
						fa-file-search</a>
				</div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-times"></i> fa fa-file-times</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-times"></i> fal fa-file-times</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-video"></i> fa fa-file-video</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-video"></i> fal fa-file-video</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-word"></i> fa fa-file-word</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-word"></i> fal fa-file-word</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-folder"></i> fa fa-folder</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-folder"></i> fal fa-folder</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-folder-open"></i> fa fa-folder-open</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-folder-open"></i> fal
						fa-folder-open</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-paste"></i> fa fa-paste</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-paste"></i> fal fa-paste</a>
				</div>
				<!-- <div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-photo-video"></i> fa fa-photo-video</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-photo-video"></i> fal
						fa-photo-video</a>
				</div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-save"></i> fa fa-save</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-save"></i> fal fa-save</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sticky-note"></i> fa fa-sticky-note</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sticky-note"></i> fal
						fa-sticky-note</a>
				</div>

			</div>

</section>

  <section id="spinner">
  <h2 class="page-header">Spinner Icons</h2>

  <div class="alert alert-success">
    <ul class="fa-ul">
      <li>
        <i class="fa fa-info-circle fa-lg fa-li"></i>
        These icons work great with the <code>fa-spin</code> class. Check out the
        <a href="../examples/#animated" class="alert-link">spinning icons example</a>.
      </li>
    </ul>
  </div>

  <div class="row fontawesome-icon-list">
    

      <!-- <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-circle-o-notch"></i> circle-o-notch</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cog"></i> cog</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-gear"></i> gear <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-refresh"></i> refresh</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-spinner"></i> spinner</a></div> -->

				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-asterisk"></i> fa fa-asterisk</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-asterisk"></i> fal fa-asterisk</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-atom"></i> fa fa-atom</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-atom"></i> fal fa-atom</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-atom-alt"></i> fa fa-atom-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-atom-alt"></i> fal fa-atom-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-badge"></i> fa fa-badge</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-badge"></i> fal fa-badge</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-bullseye-arrow"></i> fa
						fa-bullseye-arrow</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-bullseye-arrow"></i> fal
						fa-bullseye-arrow</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-certificate"></i> fa fa-certificate</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-certificate"></i> fal
						fa-certificate</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-circle-notch"></i> fa
						fa-circle-notch</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-circle-notch"></i> fal
						fa-circle-notch</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-cog"></i> fa fa-cog</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-cog"></i> fal fa-cog</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-compact-disc"></i> fa
						fa-compact-disc</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-compact-disc"></i> fal
						fa-compact-disc</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-compass"></i> fa fa-compass</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-compass"></i> fal fa-compass</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-crosshairs"></i> fa fa-crosshairs</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-crosshairs"></i> fal fa-crosshairs</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-dharmachakra"></i> fa
						fa-dharmachakra</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-dharmachakra"></i> fal
						fa-dharmachakra</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-eclipse-alt"></i> fa fa-eclipse-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-eclipse-alt"></i> fal
						fa-eclipse-alt</a>
				</div>
				<!-- <div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-fan"></i> fa fa-fan</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-fan"></i> fal fa-fan</a>
				</div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-haykal"></i> fa fa-haykal</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-haykal"></i> fal fa-haykal</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-hurricane"></i> fa fa-hurricane</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hurricane"></i> fal fa-hurricane</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-life-ring"></i> fa fa-life-ring</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-life-ring"></i> fal fa-life-ring</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-palette"></i> fa fa-palette</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-palette"></i> fal fa-palette</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-ring"></i> fa fa-ring</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-ring"></i> fal fa-ring</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-slash"></i> fa fa-slash</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-slash"></i> fal fa-slash</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-snowflake"></i> fa fa-snowflake</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-snowflake"></i> fal fa-snowflake</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-spider-web"></i> fa fa-spider-web</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-spider-web"></i> fal fa-spider-web</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-spinner"></i> fa fa-spinner</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-spinner"></i> fal fa-spinner</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-spinner-third"></i> fa
						fa-spinner-third</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-spinner-third"></i> fal
						fa-spinner-third</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-star-christmas"></i> fa
						fa-star-christmas</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-star-christmas"></i> fal
						fa-star-christmas</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-steering-wheel"></i> fa
						fa-steering-wheel</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-steering-wheel"></i> fal
						fa-steering-wheel</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-stroopwafel"></i> fa fa-stroopwafel</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-stroopwafel"></i> fal
						fa-stroopwafel</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sun"></i> fa fa-sun</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sun"></i> fal fa-sun</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sync"></i> fa fa-sync</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sync"></i> fal fa-sync</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sync-alt"></i> fa fa-sync-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sync-alt"></i> fal fa-sync-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-tire"></i> fa fa-tire</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-tire"></i> fal fa-tire</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-tire-rugged"></i> fa fa-tire-rugged</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-tire-rugged"></i> fal
						fa-tire-rugged</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-wreath"></i> fa fa-wreath</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-wreath"></i> fal fa-wreath</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-yin-yang"></i> fa fa-yin-yang</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-yin-yang"></i> fal fa-yin-yang</a>
				</div>

			</div>
</section>

  <section id="form-control">
  <h2 class="page-header">Form Control Icons</h2>

  <div class="row fontawesome-icon-list">
    

      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-check-square"></i> check-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-check-square-o"></i> check-square-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-circle"></i> circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-circle-o"></i> circle-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-dot-circle-o"></i> dot-circle-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-minus-square"></i> minus-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-minus-square-o"></i> minus-square-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-plus-square"></i> plus-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-plus-square-o"></i> plus-square-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-square"></i> square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-square-o"></i> square-o</a></div>
    
  </div>
</section>

  <section id="payment">
  <h2 class="page-header">Payment Icons</h2>

  <div class="row fontawesome-icon-list">
    

     <!--  <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cc-amex"></i> cc-amex</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cc-diners-club"></i> cc-diners-club</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cc-discover"></i> cc-discover</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cc-jcb"></i> cc-jcb</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cc-mastercard"></i> cc-mastercard</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cc-paypal"></i> cc-paypal</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cc-stripe"></i> cc-stripe</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cc-visa"></i> cc-visa</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-credit-card"></i> credit-card</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-google-wallet"></i> google-wallet</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-paypal"></i> paypal</a></div> -->
      
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-credit-card-front"></i> fa fa-credit-card-front</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-credit-card-front"></i> fal fa-credit-card-front</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-credit-card-blank"></i> fa 		fa-credit-card-blank</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-credit-card-blank"></i> fal fa-credit-card-blank</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-credit-card"></i> fa 		fa-credit-card</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-credit-card"></i> fal fa-credit-card</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-visa"></i> fab fa-cc-visa</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-stripe"></i> fab fa-cc-stripe</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-paypal"></i> fab fa-cc-paypal</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-mastercard"></i> fab fa-cc-mastercard</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-jcb"></i> fab fa-cc-jcb</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-discover"></i> fab fa-cc-discover</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-diners-club"></i> fab fa-cc-diners-club</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-apple-pay"></i> fab fa-cc-apple-pay</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-amex"></i> fab fa-cc-amex</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-amazon-pay"></i> fab fa-cc-amazon-pay</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-paypal"></i> fab fa-paypal</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-apple-pay"></i> fab fa-apple-pay</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-amazon-pay"></i> fab fa-amazon-pay</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-paypal"></i> fab fa-cc-paypal</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-apple-pay"></i> fab fa-cc-apple-pay</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-amazon-pay"></i> fab fa-cc-amazon-pay</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-alipay"></i> fab fa-alipay</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-stripe-s"></i> fab fa-stripe-s</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-stripe"></i> fab fa-stripe</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-google-wallet"></i> fab fa-google-wallet</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-visa"></i> fab fa-cc-visa</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-stripe"></i> fab fa-cc-stripe</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-mastercard"></i> fab fa-cc-mastercard</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-jcb"></i> fab fa-cc-jcb</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-discover"></i> fab fa-cc-discover</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-diners-club"></i> fab fa-cc-diners-club</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-amex"></i> fab fa-cc-amex</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-ethereum"></i> fab fa-ethereum</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-btc"></i> fab fa-btc</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-bitcoin"></i> fab fa-bitcoin</a></div>
    
  </div>

</section>

  <section id="chart">
  <h2 class="page-header">Chart Icons</h2>

  <div class="row fontawesome-icon-list">
    

      <!-- <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-area-chart"></i> area-chart</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bar-chart"></i> bar-chart</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bar-chart-o"></i> bar-chart-o <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-line-chart"></i> line-chart</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-pie-chart"></i> pie-chart</a></div> -->
      
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-analytics"></i> fa fa-analytics</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-analytics"></i> fal fa-analytics</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-chart-pie"></i> fa fa-file-chart-pie</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-file-chart-pie"></i> fal fa-file-chart-pie</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-chart-line"></i> fa fa-file-chart-line</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-file-chart-line"></i> fal fa-file-chart-line</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chart-scatter"></i> fa fa-chart-scatter</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-chart-scatter"></i> fal fa-chart-scatter</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chart-pie-alt"></i> fa fa-chart-pie-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-chart-pie-alt"></i> fal fa-chart-pie-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chart-pie"></i> fa fa-chart-pie</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-chart-pie"></i> fal fa-chart-pie</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chart-line-down"></i> fa fa-chart-line-down</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-chart-line-down"></i> fal fa-chart-line-down</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chart-bar"></i> fa fa-chart-bar</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-chart-bar"></i> fal fa-chart-bar</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chart-area"></i> fa fa-chart-area</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-chart-area"></i> fal fa-chart-area</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chart-network"></i> fa fa-chart-network</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-chart-network"></i> fal fa-chart-network</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chart-line"></i> fa fa-chart-line</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-chart-line"></i> fal fa-chart-line</a></div>
    
  </div>

</section>

  <section id="currency">
  <h2 class="page-header">Currency Icons</h2>

  <div class="row fontawesome-icon-list">
    

      <!-- <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bitcoin"></i> bitcoin <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-btc"></i> btc</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cny"></i> cny <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-dollar"></i> dollar <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-eur"></i> eur</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-euro"></i> euro <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-gbp"></i> gbp</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-gg"></i> gg</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-gg-circle"></i> gg-circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-ils"></i> ils</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-inr"></i> inr</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-jpy"></i> jpy</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-krw"></i> krw</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-money"></i> money</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-rmb"></i> rmb <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-rouble"></i> rouble <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-rub"></i> rub</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-ruble"></i> ruble <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-rupee"></i> rupee <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-shekel"></i> shekel <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sheqel"></i> sheqel <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-try"></i> try</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-turkish-lira"></i> turkish-lira <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-usd"></i> usd</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-won"></i> won <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-yen"></i> yen <span class="text-muted">(alias)</span></a></div> -->

				<!-- <div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-coin"></i> fa fa-coin</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-coin"></i> fal fa-coin</a>
				</div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-dollar-sign"></i> fa fa-dollar-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-dollar-sign"></i> fal
						fa-dollar-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-euro-sign"></i> fa fa-euro-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-euro-sign"></i> fal fa-euro-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-hryvnia"></i> fa fa-hryvnia</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hryvnia"></i> fal fa-hryvnia</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-lira-sign"></i> fa fa-lira-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-lira-sign"></i> fal fa-lira-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-money-bill"></i> fa fa-money-bill</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-money-bill"></i> fal fa-money-bill</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-money-bill-alt"></i> fa
						fa-money-bill-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-money-bill-alt"></i> fal
						fa-money-bill-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-money-bill-wave"></i> fa
						fa-money-bill-wave</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-money-bill-wave"></i> fal
						fa-money-bill-wave</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-money-bill-wave-alt"></i> fa
						fa-money-bill-wave-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-money-bill-wave-alt"></i> fal
						fa-money-bill-wave-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-money-check"></i> fa fa-money-check</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-money-check"></i> fal
						fa-money-check</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-money-check-alt"></i> fa
						fa-money-check-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-money-check-alt"></i> fal
						fa-money-check-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-pound-sign"></i> fa fa-pound-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-pound-sign"></i> fal fa-pound-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-ruble-sign"></i> fa fa-ruble-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-ruble-sign"></i> fal fa-ruble-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-rupee-sign"></i> fa fa-rupee-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-rupee-sign"></i> fal fa-rupee-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sack"></i> fa fa-sack</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sack"></i> fal fa-sack</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sack-dollar"></i> fa fa-sack-dollar</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sack-dollar"></i> fal
						fa-sack-dollar</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-shekel-sign"></i> fa fa-shekel-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-shekel-sign"></i> fal
						fa-shekel-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-tenge"></i> fa fa-tenge</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-tenge"></i> fal fa-tenge</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-usd-circle"></i> fa fa-usd-circle</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-usd-circle"></i> fal fa-usd-circle</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-usd-square"></i> fa fa-usd-square</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-usd-square"></i> fal fa-usd-square</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-won-sign"></i> fa fa-won-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-won-sign"></i> fal fa-won-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-yen-sign"></i> fa fa-yen-sign</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-yen-sign"></i> fal fa-yen-sign</a>
				</div>

			</div>

</section>

  <section id="text-editor">
  <h2 class="page-header">Text Editor Icons</h2>

  <div class="row fontawesome-icon-list">
    

      <!-- <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-align-center"></i> align-center</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-align-justify"></i> align-justify</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-align-left"></i> align-left</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-align-right"></i> align-right</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bold"></i> bold</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chain"></i> chain <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chain-broken"></i> chain-broken</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-clipboard"></i> clipboard</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-columns"></i> columns</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-copy"></i> copy <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cut"></i> cut <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-dedent"></i> dedent <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-eraser"></i> eraser</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file"></i> file</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-o"></i> file-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-text"></i> file-text</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-text-o"></i> file-text-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-files-o"></i> files-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-floppy-o"></i> floppy-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-font"></i> font</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-header"></i> header</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-indent"></i> indent</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-italic"></i> italic</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-link"></i> link</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-list"></i> list</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-list-alt"></i> list-alt</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-list-ol"></i> list-ol</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-list-ul"></i> list-ul</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-outdent"></i> outdent</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-paperclip"></i> paperclip</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-paragraph"></i> paragraph</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-paste"></i> paste <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-repeat"></i> repeat</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-rotate-left"></i> rotate-left <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-rotate-right"></i> rotate-right <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-save"></i> save <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-scissors"></i> scissors</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-strikethrough"></i> strikethrough</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-subscript"></i> subscript</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-superscript"></i> superscript</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-table"></i> table</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-text-height"></i> text-height</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-text-width"></i> text-width</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-th"></i> th</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-th-large"></i> th-large</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-th-list"></i> th-list</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-underline"></i> underline</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-undo"></i> undo</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-unlink"></i> unlink <span class="text-muted">(alias)</span></a></div> -->

				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-align-center"></i> fa
						fa-align-center</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-align-center"></i> fal
						fa-align-center</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-align-justify"></i> fa
						fa-align-justify</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-align-justify"></i> fal
						fa-align-justify</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-align-left"></i> fa fa-align-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-align-left"></i> fal fa-align-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-align-right"></i> fa fa-align-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-align-right"></i> fal
						fa-align-right</a>
				</div>
				<!-- <div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-align-slash"></i> fa fa-align-slash</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-align-slash"></i> fal
						fa-align-slash</a>
				</div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-bold"></i> fa fa-bold</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-bold"></i> fal fa-bold</a>
				</div>
				<!-- <div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-border-all"></i> fa fa-border-all</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-border-all"></i> fal fa-border-all</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-border-bottom"></i> fa
						fa-border-bottom</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-border-bottom"></i> fal
						fa-border-bottom</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-border-center-h"></i> fa
						fa-border-center-h</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-border-center-h"></i> fal
						fa-border-center-h</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-border-center-v"></i> fa
						fa-border-center-v</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-border-center-v"></i> fal
						fa-border-center-v</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-border-inner"></i> fa
						fa-border-inner</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-border-inner"></i> fal
						fa-border-inner</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-border-left"></i> fa fa-border-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-border-left"></i> fal
						fa-border-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-border-none"></i> fa fa-border-none</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-border-none"></i> fal
						fa-border-none</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-border-outer"></i> fa
						fa-border-outer</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-border-outer"></i> fal
						fa-border-outer</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-border-right"></i> fa
						fa-border-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-border-right"></i> fal
						fa-border-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-border-style"></i> fa
						fa-border-style</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-border-style"></i> fal
						fa-border-style</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-border-style-alt"></i> fa
						fa-border-style-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-border-style-alt"></i> fal
						fa-border-style-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-border-top"></i> fa fa-border-top</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-border-top"></i> fal fa-border-top</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-bring-forward"></i> fa
						fa-bring-forward</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-bring-forward"></i> fal
						fa-bring-forward</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-bring-front"></i> fa fa-bring-front</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-bring-front"></i> fal
						fa-bring-front</a>
				</div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-clipboard"></i> fa fa-clipboard</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-clipboard"></i> fal fa-clipboard</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-clone"></i> fa fa-clone</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-clone"></i> fal fa-clone</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-columns"></i> fa fa-columns</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-columns"></i> fal fa-columns</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-copy"></i> fa fa-copy</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-copy"></i> fal fa-copy</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-cut"></i> fa fa-cut</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-cut"></i> fal fa-cut</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-edit"></i> fa fa-edit</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-edit"></i> fal fa-edit</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-eraser"></i> fa fa-eraser</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-eraser"></i> fal fa-eraser</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file"></i> fa fa-file</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file"></i> fal fa-file</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-file-alt"></i> fa fa-file-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-file-alt"></i> fal fa-file-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-font"></i> fa fa-font</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-font"></i> fal fa-font</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-font-case"></i> fa fa-font-case</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-font-case"></i> fal fa-font-case</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-glasses"></i> fa fa-glasses</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-glasses"></i> fal fa-glasses</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-h1"></i> fa fa-h1</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-h1"></i> fal fa-h1</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-h2"></i> fa fa-h2</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-h2"></i> fal fa-h2</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-h3"></i> fa fa-h3</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-h3"></i> fal fa-h3</a>
				</div>
				<!-- <div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-h4"></i> fa fa-h4</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-h4"></i> fal fa-h4</a>
				</div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-heading"></i> fa fa-heading</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-heading"></i> fal fa-heading</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-highlighter"></i> fa fa-highlighter</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-highlighter"></i> fal
						fa-highlighter</a>
				</div>
				<!-- <div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-horizontal-rule"></i> fa
						fa-horizontal-rule</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-horizontal-rule"></i> fal
						fa-horizontal-rule</a>
				</div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-i-cursor"></i> fa fa-i-cursor</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-i-cursor"></i> fal fa-i-cursor</a>
				</div>
				<!-- <div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-icons"></i> fa fa-icons</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-icons"></i> fal fa-icons</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-icons-alt"></i> fa fa-icons-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-icons-alt"></i> fal fa-icons-alt</a>
				</div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-indent"></i> fa fa-indent</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-indent"></i> fal fa-indent</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-italic"></i> fa fa-italic</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-italic"></i> fal fa-italic</a>
				</div>
				<!-- <div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-kerning"></i> fa fa-kerning</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-kerning"></i> fal fa-kerning</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-line-columns"></i> fa
						fa-line-columns</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-line-columns"></i> fal
						fa-line-columns</a>
				</div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-link"></i> fa fa-link</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-link"></i> fal fa-link</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-list"></i> fa fa-list</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-list"></i> fal fa-list</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-list-alt"></i> fa fa-list-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-list-alt"></i> fal fa-list-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-list-ol"></i> fa fa-list-ol</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-list-ol"></i> fal fa-list-ol</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-list-ul"></i> fa fa-list-ul</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-list-ul"></i> fal fa-list-ul</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-marker"></i> fa fa-marker</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-marker"></i> fal fa-marker</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-outdent"></i> fa fa-outdent</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-outdent"></i> fal fa-outdent</a>
				</div>
				<!-- <div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-overline"></i> fa fa-overline</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-overline"></i> fal fa-overline</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-page-break"></i> fa fa-page-break</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-page-break"></i> fal fa-page-break</a>
				</div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-paper-plane"></i> fa fa-paper-plane</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-paper-plane"></i> fal
						fa-paper-plane</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-paperclip"></i> fa fa-paperclip</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-paperclip"></i> fal fa-paperclip</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-paragraph"></i> fa fa-paragraph</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-paragraph"></i> fal fa-paragraph</a>
				</div>
				<!-- <div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-paragraph-rtl"></i> fa
						fa-paragraph-rtl</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-paragraph-rtl"></i> fal
						fa-paragraph-rtl</a>
				</div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-paste"></i> fa fa-paste</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-paste"></i> fal fa-paste</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-pen"></i> fa fa-pen</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-pen"></i> fal fa-pen</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-pen-alt"></i> fa fa-pen-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-pen-alt"></i> fal fa-pen-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-pen-fancy"></i> fa fa-pen-fancy</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-pen-fancy"></i> fal fa-pen-fancy</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-pen-nib"></i> fa fa-pen-nib</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-pen-nib"></i> fal fa-pen-nib</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-pencil"></i> fa fa-pencil</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-pencil"></i> fal fa-pencil</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-pencil-alt"></i> fa fa-pencil-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-pencil-alt"></i> fal fa-pencil-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-print"></i> fa fa-print</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-print"></i> fal fa-print</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-quote-left"></i> fa fa-quote-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-quote-left"></i> fal fa-quote-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-quote-right"></i> fa fa-quote-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-quote-right"></i> fal
						fa-quote-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-redo"></i> fa fa-redo</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-redo"></i> fal fa-redo</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-redo-alt"></i> fa fa-redo-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-redo-alt"></i> fal fa-redo-alt</a>
				</div>
				<!-- <div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-remove-format"></i> fa
						fa-remove-format</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-remove-format"></i> fal
						fa-remove-format</a>
				</div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-reply"></i> fa fa-reply</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-reply"></i> fal fa-reply</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-reply-all"></i> fa fa-reply-all</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-reply-all"></i> fal fa-reply-all</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-screwdriver"></i> fa fa-screwdriver</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-screwdriver"></i> fal
						fa-screwdriver</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-send-back"></i> fa fa-send-back</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-send-back"></i> fal fa-send-back</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-send-backward"></i> fa
						fa-send-backward</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-send-backward"></i> fal
						fa-send-backward</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-share"></i> fa fa-share</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-share"></i> fal fa-share</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-share-all"></i> fa fa-share-all</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-share-all"></i> fal fa-share-all</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-spell-check"></i> fa fa-spell-check</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-spell-check"></i> fal
						fa-spell-check</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-strikethrough"></i> fa
						fa-strikethrough</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-strikethrough"></i> fal
						fa-strikethrough</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-subscript"></i> fa fa-subscript</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-subscript"></i> fal fa-subscript</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-superscript"></i> fa fa-superscript</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-superscript"></i> fal
						fa-superscript</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sync"></i> fa fa-sync</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sync"></i> fal fa-sync</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sync-alt"></i> fa fa-sync-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sync-alt"></i> fal fa-sync-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-table"></i> fa fa-table</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-table"></i> fal fa-table</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-tasks"></i> fa fa-tasks</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-tasks"></i> fal fa-tasks</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-tasks-alt"></i> fa fa-tasks-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-tasks-alt"></i> fal fa-tasks-alt</a>
				</div>
				<!-- <div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-text"></i> fa fa-text</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-text"></i> fal fa-text</a>
				</div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-text-height"></i> fa fa-text-height</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-text-height"></i> fal
						fa-text-height</a>
				</div>
				<!-- <div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-text-size"></i> fa fa-text-size</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-text-size"></i> fal fa-text-size</a>
				</div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-text-width"></i> fa fa-text-width</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-text-width"></i> fal fa-text-width</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-th"></i> fa fa-th</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-th"></i> fal fa-th</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-th-large"></i> fa fa-th-large</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-th-large"></i> fal fa-th-large</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-th-list"></i> fa fa-th-list</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-th-list"></i> fal fa-th-list</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-tools"></i> fa fa-tools</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-tools"></i> fal fa-tools</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-trash"></i> fa fa-trash</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-trash"></i> fal fa-trash</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-trash-alt"></i> fa fa-trash-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-trash-alt"></i> fal fa-trash-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-trash-restore"></i> fa
						fa-trash-restore</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-trash-restore"></i> fal
						fa-trash-restore</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-trash-restore-alt"></i> fa
						fa-trash-restore-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-trash-restore-alt"></i> fal
						fa-trash-restore-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-underline"></i> fa fa-underline</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-underline"></i> fal fa-underline</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-undo"></i> fa fa-undo</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-undo"></i> fal fa-undo</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-undo-alt"></i> fa fa-undo-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-undo-alt"></i> fal fa-undo-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-unlink"></i> fa fa-unlink</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-unlink"></i> fal fa-unlink</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-wrench"></i> fa fa-wrench</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-wrench"></i> fal fa-wrench</a>
				</div>

			</div>

</section>

  <section id="directional">
  <h2 class="page-header">Directional Icons</h2>

  <div class="row fontawesome-icon-list">
    

      <!-- <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-angle-double-down"></i> angle-double-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-angle-double-left"></i> angle-double-left</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-angle-double-right"></i> angle-double-right</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-angle-double-up"></i> angle-double-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-angle-down"></i> angle-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-angle-left"></i> angle-left</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-angle-right"></i> angle-right</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-angle-up"></i> angle-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrow-circle-down"></i> arrow-circle-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrow-circle-left"></i> arrow-circle-left</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrow-circle-o-down"></i> arrow-circle-o-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrow-circle-o-left"></i> arrow-circle-o-left</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrow-circle-o-right"></i> arrow-circle-o-right</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrow-circle-o-up"></i> arrow-circle-o-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrow-circle-right"></i> arrow-circle-right</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrow-circle-up"></i> arrow-circle-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrow-down"></i> arrow-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrow-left"></i> arrow-left</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrow-right"></i> arrow-right</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrow-up"></i> arrow-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrows"></i> arrows</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrows-alt"></i> arrows-alt</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrows-h"></i> arrows-h</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrows-v"></i> arrows-v</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-caret-down"></i> caret-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-caret-left"></i> caret-left</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-caret-right"></i> caret-right</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-caret-square-o-down"></i> caret-square-o-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-caret-square-o-left"></i> caret-square-o-left</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-caret-square-o-right"></i> caret-square-o-right</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-caret-square-o-up"></i> caret-square-o-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-caret-up"></i> caret-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chevron-circle-down"></i> chevron-circle-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chevron-circle-left"></i> chevron-circle-left</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chevron-circle-right"></i> chevron-circle-right</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chevron-circle-up"></i> chevron-circle-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chevron-down"></i> chevron-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chevron-left"></i> chevron-left</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chevron-right"></i> chevron-right</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chevron-up"></i> chevron-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-exchange"></i> exchange</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-o-down"></i> hand-o-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-o-left"></i> hand-o-left</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-o-right"></i> hand-o-right</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hand-o-up"></i> hand-o-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-long-arrow-down"></i> long-arrow-down</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-long-arrow-left"></i> long-arrow-left</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-long-arrow-right"></i> long-arrow-right</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-long-arrow-up"></i> long-arrow-up</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-toggle-down"></i> toggle-down <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-toggle-left"></i> toggle-left <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-toggle-right"></i> toggle-right <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-toggle-up"></i> toggle-up <span class="text-muted">(alias)</span></a></div> -->
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-angle-double-down"></i> fa
						fa-angle-double-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-angle-double-down"></i> fal
						fa-angle-double-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-angle-double-left"></i> fa
						fa-angle-double-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-angle-double-left"></i> fal
						fa-angle-double-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-angle-double-right"></i> fa
						fa-angle-double-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-angle-double-right"></i> fal
						fa-angle-double-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-angle-double-up"></i> fa
						fa-angle-double-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-angle-double-up"></i> fal
						fa-angle-double-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-angle-down"></i> fa fa-angle-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-angle-down"></i> fal fa-angle-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-angle-left"></i> fa fa-angle-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-angle-left"></i> fal fa-angle-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-angle-right"></i> fa fa-angle-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-angle-right"></i> fal
						fa-angle-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-angle-up"></i> fa fa-angle-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-angle-up"></i> fal fa-angle-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-circle-down"></i> fa
						fa-arrow-alt-circle-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-circle-down"></i> fal
						fa-arrow-alt-circle-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-circle-left"></i> fa
						fa-arrow-alt-circle-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-circle-left"></i> fal
						fa-arrow-alt-circle-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-circle-right"></i> fa
						fa-arrow-alt-circle-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-circle-right"></i> fal
						fa-arrow-alt-circle-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-circle-up"></i> fa
						fa-arrow-alt-circle-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-circle-up"></i> fal
						fa-arrow-alt-circle-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-down"></i> fa
						fa-arrow-alt-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-down"></i> fal
						fa-arrow-alt-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-from-bottom"></i> fa
						fa-arrow-alt-from-bottom</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-from-bottom"></i> fal
						fa-arrow-alt-from-bottom</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-from-left"></i> fa
						fa-arrow-alt-from-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-from-left"></i> fal
						fa-arrow-alt-from-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-from-right"></i> fa
						fa-arrow-alt-from-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-from-right"></i> fal
						fa-arrow-alt-from-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-from-top"></i> fa
						fa-arrow-alt-from-top</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-from-top"></i> fal
						fa-arrow-alt-from-top</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-left"></i> fa
						fa-arrow-alt-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-left"></i> fal
						fa-arrow-alt-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-right"></i> fa
						fa-arrow-alt-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-right"></i> fal
						fa-arrow-alt-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-square-down"></i> fa
						fa-arrow-alt-square-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-square-down"></i> fal
						fa-arrow-alt-square-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-square-left"></i> fa
						fa-arrow-alt-square-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-square-left"></i> fal
						fa-arrow-alt-square-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-square-right"></i> fa
						fa-arrow-alt-square-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-square-right"></i> fal
						fa-arrow-alt-square-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-square-up"></i> fa
						fa-arrow-alt-square-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-square-up"></i> fal
						fa-arrow-alt-square-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-to-bottom"></i> fa
						fa-arrow-alt-to-bottom</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-to-bottom"></i> fal
						fa-arrow-alt-to-bottom</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-to-left"></i> fa
						fa-arrow-alt-to-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-to-left"></i> fal
						fa-arrow-alt-to-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-to-right"></i> fa
						fa-arrow-alt-to-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-to-right"></i> fal
						fa-arrow-alt-to-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-to-top"></i> fa
						fa-arrow-alt-to-top</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-to-top"></i> fal
						fa-arrow-alt-to-top</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-alt-up"></i> fa
						fa-arrow-alt-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-alt-up"></i> fal
						fa-arrow-alt-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-circle-down"></i> fa
						fa-arrow-circle-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-circle-down"></i> fal
						fa-arrow-circle-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-circle-left"></i> fa
						fa-arrow-circle-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-circle-left"></i> fal
						fa-arrow-circle-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-circle-right"></i> fa
						fa-arrow-circle-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-circle-right"></i> fal
						fa-arrow-circle-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-circle-up"></i> fa
						fa-arrow-circle-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-circle-up"></i> fal
						fa-arrow-circle-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-down"></i> fa fa-arrow-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-down"></i> fal fa-arrow-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-from-bottom"></i> fa
						fa-arrow-from-bottom</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-from-bottom"></i> fal
						fa-arrow-from-bottom</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-from-left"></i> fa
						fa-arrow-from-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-from-left"></i> fal
						fa-arrow-from-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-from-right"></i> fa
						fa-arrow-from-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-from-right"></i> fal
						fa-arrow-from-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-from-top"></i> fa
						fa-arrow-from-top</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-from-top"></i> fal
						fa-arrow-from-top</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-left"></i> fa fa-arrow-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-left"></i> fal fa-arrow-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-right"></i> fa fa-arrow-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-right"></i> fal
						fa-arrow-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-square-down"></i> fa
						fa-arrow-square-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-square-down"></i> fal
						fa-arrow-square-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-square-left"></i> fa
						fa-arrow-square-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-square-left"></i> fal
						fa-arrow-square-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-square-right"></i> fa
						fa-arrow-square-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-square-right"></i> fal
						fa-arrow-square-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-square-up"></i> fa
						fa-arrow-square-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-square-up"></i> fal
						fa-arrow-square-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-to-bottom"></i> fa
						fa-arrow-to-bottom</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-to-bottom"></i> fal
						fa-arrow-to-bottom</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-to-left"></i> fa
						fa-arrow-to-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-to-left"></i> fal
						fa-arrow-to-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-to-right"></i> fa
						fa-arrow-to-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-to-right"></i> fal
						fa-arrow-to-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-to-top"></i> fa
						fa-arrow-to-top</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-to-top"></i> fal
						fa-arrow-to-top</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrow-up"></i> fa fa-arrow-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrow-up"></i> fal fa-arrow-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrows"></i> fa fa-arrows</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrows"></i> fal fa-arrows</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrows-alt"></i> fa fa-arrows-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrows-alt"></i> fal fa-arrows-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrows-alt-h"></i> fa
						fa-arrows-alt-h</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrows-alt-h"></i> fal
						fa-arrows-alt-h</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrows-alt-v"></i> fa
						fa-arrows-alt-v</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrows-alt-v"></i> fal
						fa-arrows-alt-v</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrows-h"></i> fa fa-arrows-h</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrows-h"></i> fal fa-arrows-h</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-arrows-v"></i> fa fa-arrows-v</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-arrows-v"></i> fal fa-arrows-v</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-caret-circle-down"></i> fa
						fa-caret-circle-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-caret-circle-down"></i> fal
						fa-caret-circle-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-caret-circle-left"></i> fa
						fa-caret-circle-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-caret-circle-left"></i> fal
						fa-caret-circle-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-caret-circle-right"></i> fa
						fa-caret-circle-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-caret-circle-right"></i> fal
						fa-caret-circle-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-caret-circle-up"></i> fa
						fa-caret-circle-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-caret-circle-up"></i> fal
						fa-caret-circle-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-caret-down"></i> fa fa-caret-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-caret-down"></i> fal fa-caret-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-caret-left"></i> fa fa-caret-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-caret-left"></i> fal fa-caret-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-caret-right"></i> fa fa-caret-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-caret-right"></i> fal
						fa-caret-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-caret-square-down"></i> fa
						fa-caret-square-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-caret-square-down"></i> fal
						fa-caret-square-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-caret-square-left"></i> fa
						fa-caret-square-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-caret-square-left"></i> fal
						fa-caret-square-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-caret-square-right"></i> fa
						fa-caret-square-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-caret-square-right"></i> fal
						fa-caret-square-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-caret-square-up"></i> fa
						fa-caret-square-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-caret-square-up"></i> fal
						fa-caret-square-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-caret-up"></i> fa fa-caret-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-caret-up"></i> fal fa-caret-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-cart-arrow-down"></i> fa
						fa-cart-arrow-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-cart-arrow-down"></i> fal
						fa-cart-arrow-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chart-line"></i> fa fa-chart-line</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chart-line"></i> fal fa-chart-line</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chevron-circle-down"></i> fa
						fa-chevron-circle-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chevron-circle-down"></i> fal
						fa-chevron-circle-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chevron-circle-left"></i> fa
						fa-chevron-circle-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chevron-circle-left"></i> fal
						fa-chevron-circle-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chevron-circle-right"></i> fa
						fa-chevron-circle-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chevron-circle-right"></i> fal
						fa-chevron-circle-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chevron-circle-up"></i> fa
						fa-chevron-circle-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chevron-circle-up"></i> fal
						fa-chevron-circle-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chevron-double-down"></i> fa
						fa-chevron-double-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chevron-double-down"></i> fal
						fa-chevron-double-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chevron-double-left"></i> fa
						fa-chevron-double-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chevron-double-left"></i> fal
						fa-chevron-double-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chevron-double-right"></i> fa
						fa-chevron-double-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chevron-double-right"></i> fal
						fa-chevron-double-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chevron-double-up"></i> fa
						fa-chevron-double-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chevron-double-up"></i> fal
						fa-chevron-double-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chevron-down"></i> fa
						fa-chevron-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chevron-down"></i> fal
						fa-chevron-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chevron-left"></i> fa
						fa-chevron-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chevron-left"></i> fal
						fa-chevron-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chevron-right"></i> fa
						fa-chevron-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chevron-right"></i> fal
						fa-chevron-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chevron-square-down"></i> fa
						fa-chevron-square-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chevron-square-down"></i> fal
						fa-chevron-square-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chevron-square-left"></i> fa
						fa-chevron-square-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chevron-square-left"></i> fal
						fa-chevron-square-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chevron-square-right"></i> fa
						fa-chevron-square-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chevron-square-right"></i> fal
						fa-chevron-square-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chevron-square-up"></i> fa
						fa-chevron-square-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chevron-square-up"></i> fal
						fa-chevron-square-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-chevron-up"></i> fa fa-chevron-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-chevron-up"></i> fal fa-chevron-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-cloud-download"></i> fa
						fa-cloud-download</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-cloud-download"></i> fal
						fa-cloud-download</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-cloud-download-alt"></i> fa
						fa-cloud-download-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-cloud-download-alt"></i> fal
						fa-cloud-download-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-cloud-upload"></i> fa
						fa-cloud-upload</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-cloud-upload"></i> fal
						fa-cloud-upload</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-cloud-upload-alt"></i> fa
						fa-cloud-upload-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-cloud-upload-alt"></i> fal
						fa-cloud-upload-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-compress-alt"></i> fa
						fa-compress-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-compress-alt"></i> fal
						fa-compress-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-compress-arrows-alt"></i> fa
						fa-compress-arrows-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-compress-arrows-alt"></i> fal
						fa-compress-arrows-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-download"></i> fa fa-download</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-download"></i> fal fa-download</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-exchange"></i> fa fa-exchange</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-exchange"></i> fal fa-exchange</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-exchange-alt"></i> fa
						fa-exchange-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-exchange-alt"></i> fal
						fa-exchange-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-expand-alt"></i> fa fa-expand-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-expand-alt"></i> fal fa-expand-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-expand-arrows"></i> fa
						fa-expand-arrows</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-expand-arrows"></i> fal
						fa-expand-arrows</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-expand-arrows-alt"></i> fa
						fa-expand-arrows-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-expand-arrows-alt"></i> fal
						fa-expand-arrows-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-external-link"></i> fa
						fa-external-link</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-external-link"></i> fal
						fa-external-link</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-external-link-alt"></i> fa
						fa-external-link-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-external-link-alt"></i> fal
						fa-external-link-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-external-link-square"></i> fa
						fa-external-link-square</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-external-link-square"></i> fal
						fa-external-link-square</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-external-link-square-alt"></i> fa
						fa-external-link-square-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-external-link-square-alt"></i> fal
						fa-external-link-square-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-hand-point-down"></i> fa
						fa-hand-point-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-point-down"></i> fal
						fa-hand-point-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-hand-point-left"></i> fa
						fa-hand-point-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-point-left"></i> fal
						fa-hand-point-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-hand-point-right"></i> fa
						fa-hand-point-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-point-right"></i> fal
						fa-hand-point-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-hand-point-up"></i> fa
						fa-hand-point-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-point-up"></i> fal
						fa-hand-point-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-hand-pointer"></i> fa
						fa-hand-pointer</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-hand-pointer"></i> fal
						fa-hand-pointer</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-history"></i> fa fa-history</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-history"></i> fal fa-history</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-inbox-in"></i> fa fa-inbox-in</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-inbox-in"></i> fal fa-inbox-in</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-inbox-out"></i> fa fa-inbox-out</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-inbox-out"></i> fal fa-inbox-out</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-level-down"></i> fa fa-level-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-level-down"></i> fal fa-level-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-level-down-alt"></i> fa
						fa-level-down-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-level-down-alt"></i> fal
						fa-level-down-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-level-up"></i> fa fa-level-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-level-up"></i> fal fa-level-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-level-up-alt"></i> fa
						fa-level-up-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-level-up-alt"></i> fal
						fa-level-up-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-location-arrow"></i> fa
						fa-location-arrow</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-location-arrow"></i> fal
						fa-location-arrow</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-long-arrow-alt-down"></i> fa
						fa-long-arrow-alt-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-long-arrow-alt-down"></i> fal
						fa-long-arrow-alt-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-long-arrow-alt-left"></i> fa
						fa-long-arrow-alt-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-long-arrow-alt-left"></i> fal
						fa-long-arrow-alt-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-long-arrow-alt-right"></i> fa
						fa-long-arrow-alt-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-long-arrow-alt-right"></i> fal
						fa-long-arrow-alt-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-long-arrow-alt-up"></i> fa
						fa-long-arrow-alt-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-long-arrow-alt-up"></i> fal
						fa-long-arrow-alt-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-long-arrow-down"></i> fa
						fa-long-arrow-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-long-arrow-down"></i> fal
						fa-long-arrow-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-long-arrow-left"></i> fa
						fa-long-arrow-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-long-arrow-left"></i> fal
						fa-long-arrow-left</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-long-arrow-right"></i> fa
						fa-long-arrow-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-long-arrow-right"></i> fal
						fa-long-arrow-right</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-long-arrow-up"></i> fa
						fa-long-arrow-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-long-arrow-up"></i> fal
						fa-long-arrow-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-mouse-pointer"></i> fa
						fa-mouse-pointer</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-mouse-pointer"></i> fal
						fa-mouse-pointer</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-play"></i> fa fa-play</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-play"></i> fal fa-play</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-random"></i> fa fa-random</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-random"></i> fal fa-random</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-recycle"></i> fa fa-recycle</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-recycle"></i> fal fa-recycle</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-redo"></i> fa fa-redo</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-redo"></i> fal fa-redo</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-redo-alt"></i> fa fa-redo-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-redo-alt"></i> fal fa-redo-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-repeat"></i> fa fa-repeat</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-repeat"></i> fal fa-repeat</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-repeat-1"></i> fa fa-repeat-1</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-repeat-1"></i> fal fa-repeat-1</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-repeat-1-alt"></i> fa
						fa-repeat-1-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-repeat-1-alt"></i> fal
						fa-repeat-1-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-repeat-alt"></i> fa fa-repeat-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-repeat-alt"></i> fal fa-repeat-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-reply"></i> fa fa-reply</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-reply"></i> fal fa-reply</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-reply-all"></i> fa fa-reply-all</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-reply-all"></i> fal fa-reply-all</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-retweet"></i> fa fa-retweet</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-retweet"></i> fal fa-retweet</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-retweet-alt"></i> fa fa-retweet-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-retweet-alt"></i> fal
						fa-retweet-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-share"></i> fa fa-share</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-share"></i> fal fa-share</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-share-all"></i> fa fa-share-all</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-share-all"></i> fal fa-share-all</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-share-square"></i> fa
						fa-share-square</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-share-square"></i> fal
						fa-share-square</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sign-in"></i> fa fa-sign-in</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sign-in"></i> fal fa-sign-in</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sign-in-alt"></i> fa fa-sign-in-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sign-in-alt"></i> fal
						fa-sign-in-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sign-out"></i> fa fa-sign-out</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sign-out"></i> fal fa-sign-out</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sign-out-alt"></i> fa
						fa-sign-out-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sign-out-alt"></i> fal
						fa-sign-out-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort"></i> fa fa-sort</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort"></i> fal fa-sort</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-alpha-down"></i> fa
						fa-sort-alpha-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-alpha-down"></i> fal
						fa-sort-alpha-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-alpha-down-alt"></i> fa
						fa-sort-alpha-down-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-alpha-down-alt"></i> fal
						fa-sort-alpha-down-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-alpha-up"></i> fa
						fa-sort-alpha-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-alpha-up"></i> fal
						fa-sort-alpha-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-alpha-up-alt"></i> fa
						fa-sort-alpha-up-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-alpha-up-alt"></i> fal
						fa-sort-alpha-up-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-alt"></i> fa fa-sort-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-alt"></i> fal fa-sort-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-amount-down"></i> fa
						fa-sort-amount-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-amount-down"></i> fal
						fa-sort-amount-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-amount-down-alt"></i> fa
						fa-sort-amount-down-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-amount-down-alt"></i> fal
						fa-sort-amount-down-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-amount-up"></i> fa
						fa-sort-amount-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-amount-up"></i> fal
						fa-sort-amount-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-amount-up-alt"></i> fa
						fa-sort-amount-up-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-amount-up-alt"></i> fal
						fa-sort-amount-up-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-down"></i> fa fa-sort-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-down"></i> fal fa-sort-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-numeric-down"></i> fa
						fa-sort-numeric-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-numeric-down"></i> fal
						fa-sort-numeric-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-numeric-down-alt"></i> fa
						fa-sort-numeric-down-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-numeric-down-alt"></i> fal
						fa-sort-numeric-down-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-numeric-up"></i> fa
						fa-sort-numeric-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-numeric-up"></i> fal
						fa-sort-numeric-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-numeric-up-alt"></i> fa
						fa-sort-numeric-up-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-numeric-up-alt"></i> fal
						fa-sort-numeric-up-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-shapes-down"></i> fa
						fa-sort-shapes-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-shapes-down"></i> fal
						fa-sort-shapes-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-shapes-down-alt"></i> fa
						fa-sort-shapes-down-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-shapes-down-alt"></i> fal
						fa-sort-shapes-down-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-shapes-up"></i> fa
						fa-sort-shapes-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-shapes-up"></i> fal
						fa-sort-shapes-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-shapes-up-alt"></i> fa
						fa-sort-shapes-up-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-shapes-up-alt"></i> fal
						fa-sort-shapes-up-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-size-down"></i> fa
						fa-sort-size-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-size-down"></i> fal
						fa-sort-size-down</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-size-down-alt"></i> fa
						fa-sort-size-down-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-size-down-alt"></i> fal
						fa-sort-size-down-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-size-up"></i> fa
						fa-sort-size-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-size-up"></i> fal
						fa-sort-size-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-size-up-alt"></i> fa
						fa-sort-size-up-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-size-up-alt"></i> fal
						fa-sort-size-up-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sort-up"></i> fa fa-sort-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sort-up"></i> fal fa-sort-up</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sync"></i> fa fa-sync</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sync"></i> fal fa-sync</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-sync-alt"></i> fa fa-sync-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-sync-alt"></i> fal fa-sync-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-text-height"></i> fa fa-text-height</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-text-height"></i> fal
						fa-text-height</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-text-width"></i> fa fa-text-width</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-text-width"></i> fal fa-text-width</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-triangle"></i> fa fa-triangle</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-triangle"></i> fal fa-triangle</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-undo"></i> fa fa-undo</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-undo"></i> fal fa-undo</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-undo-alt"></i> fa fa-undo-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-undo-alt"></i> fal fa-undo-alt</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fa fa-upload"></i> fa fa-upload</a>
				</div>
				<div class="col-xs-2 col-sm-1 icon-item">
					<a href="#"><i class="fal fa-upload"></i> fal fa-upload</a>
				</div>

			</div>

</section>

  <section id="video-player">
  <h2 class="page-header">Audio and Video Player Icons</h2>

  <div class="row fontawesome-icon-list">
    

      <!-- <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-arrows-alt"></i> arrows-alt</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-backward"></i> backward</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-compress"></i> compress</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-eject"></i> eject</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-expand"></i> expand</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-fast-backward"></i> fast-backward</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-fast-forward"></i> fast-forward</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-forward"></i> forward</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-pause"></i> pause</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-play"></i> play</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-play-circle"></i> play-circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-play-circle-o"></i> play-circle-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-random"></i> random</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-step-backward"></i> step-backward</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-step-forward"></i> step-forward</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-stop"></i> stop</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-youtube-play"></i> youtube-play</a></div> -->
      
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-audio-description"></i> fa fa-audio-description</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-audio-description"></i> fal fa-audio-description</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-backward"></i> fa fa-backward</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-backward"></i> fal fa-backward</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-broadcast-tower"></i> fa fa-broadcast-tower</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-broadcast-tower"></i> fal fa-broadcast-tower</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-circle"></i> fa fa-circle</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-circle"></i> fal fa-circle</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-closed-captioning"></i> fa fa-closed-captioning</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-closed-captioning"></i> fal fa-closed-captioning</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-compress"></i> fa fa-compress</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-compress"></i> fal fa-compress</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-compress-alt"></i> fa fa-compress-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-compress-alt"></i> fal fa-compress-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-compress-arrows-alt"></i> fa fa-compress-arrows-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-compress-arrows-alt"></i> fal fa-compress-arrows-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-compress-wide"></i> fa fa-compress-wide</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-compress-wide"></i> fal fa-compress-wide</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-drone"></i> fa fa-drone</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-drone"></i> fal fa-drone</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-drone-alt"></i> fa fa-drone-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-drone-alt"></i> fal fa-drone-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-eject"></i> fa fa-eject</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-eject"></i> fal fa-eject</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-expand"></i> fa fa-expand</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-expand"></i> fal fa-expand</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-expand-alt"></i> fa fa-expand-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-expand-alt"></i> fal fa-expand-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-expand-arrows"></i> fa fa-expand-arrows</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-expand-arrows"></i> fal fa-expand-arrows</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-expand-arrows-alt"></i> fa fa-expand-arrows-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-expand-arrows-alt"></i> fal fa-expand-arrows-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-expand-wide"></i> fa fa-expand-wide</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-expand-wide"></i> fal fa-expand-wide</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-fast-backward"></i> fa fa-fast-backward</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-fast-backward"></i> fal fa-fast-backward</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-fast-forward"></i> fa fa-fast-forward</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-fast-forward"></i> fal fa-fast-forward</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-audio"></i> fa fa-file-audio</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-file-audio"></i> fal fa-file-audio</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-video"></i> fa fa-file-video</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-file-video"></i> fal fa-file-video</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-film"></i> fa fa-film</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-film"></i> fal fa-film</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-film-alt"></i> fa fa-film-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-film-alt"></i> fal fa-film-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-forward"></i> fa fa-forward</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-forward"></i> fal fa-forward</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-headphones"></i> fa fa-headphones</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-headphones"></i> fal fa-headphones</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-microphone"></i> fa fa-microphone</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-microphone"></i> fal fa-microphone</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-microphone-alt"></i> fa fa-microphone-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-microphone-alt"></i> fal fa-microphone-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-microphone-alt-slash"></i> fa fa-microphone-alt-slash</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-microphone-alt-slash"></i> fal fa-microphone-alt-slash</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-microphone-slash"></i> fa fa-microphone-slash</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-microphone-slash"></i> fal fa-microphone-slash</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-music"></i> fa fa-music</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-music"></i> fal fa-music</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-pause"></i> fa fa-pause</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-pause"></i> fal fa-pause</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-pause-circle"></i> fa fa-pause-circle</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-pause-circle"></i> fal fa-pause-circle</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-phone-volume"></i> fa fa-phone-volume</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-phone-volume"></i> fal fa-phone-volume</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-photo-video"></i> fa fa-photo-video</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-photo-video"></i> fal fa-photo-video</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-play"></i> fa fa-play</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-play"></i> fal fa-play</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-play-circle"></i> fa fa-play-circle</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-play-circle"></i> fal fa-play-circle</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-podcast"></i> fa fa-podcast</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-podcast"></i> fal fa-podcast</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-random"></i> fa fa-random</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-random"></i> fal fa-random</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-rectangle-landscape"></i> fa fa-rectangle-landscape</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-rectangle-landscape"></i> fal fa-rectangle-landscape</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-rectangle-portrait"></i> fa fa-rectangle-portrait</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-rectangle-portrait"></i> fal fa-rectangle-portrait</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-rectangle-wide"></i> fa fa-rectangle-wide</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-rectangle-wide"></i> fal fa-rectangle-wide</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-redo"></i> fa fa-redo</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-redo"></i> fal fa-redo</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-redo-alt"></i> fa fa-redo-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-redo-alt"></i> fal fa-redo-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-repeat"></i> fa fa-repeat</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-repeat"></i> fal fa-repeat</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-repeat-1"></i> fa fa-repeat-1</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-repeat-1"></i> fal fa-repeat-1</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-repeat-1-alt"></i> fa fa-repeat-1-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-repeat-1-alt"></i> fal fa-repeat-1-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-repeat-alt"></i> fa fa-repeat-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-repeat-alt"></i> fal fa-repeat-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-rss"></i> fa fa-rss</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-rss"></i> fal fa-rss</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-rss-square"></i> fa fa-rss-square</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-rss-square"></i> fal fa-rss-square</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-scrubber"></i> fa fa-scrubber</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-scrubber"></i> fal fa-scrubber</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-step-backward"></i> fa fa-step-backward</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-step-backward"></i> fal fa-step-backward</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-step-forward"></i> fa fa-step-forward</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-step-forward"></i> fal fa-step-forward</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-stop"></i> fa fa-stop</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-stop"></i> fal fa-stop</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-stop-circle"></i> fa fa-stop-circle</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-stop-circle"></i> fal fa-stop-circle</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sync"></i> fa fa-sync</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-sync"></i> fal fa-sync</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sync-alt"></i> fa fa-sync-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-sync-alt"></i> fal fa-sync-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-undo"></i> fa fa-undo</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-undo"></i> fal fa-undo</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-undo-alt"></i> fa fa-undo-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-undo-alt"></i> fal fa-undo-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-video"></i> fa fa-video</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-video"></i> fal fa-video</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-volume"></i> fa fa-volume</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-volume"></i> fal fa-volume</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-volume-down"></i> fa fa-volume-down</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-volume-down"></i> fal fa-volume-down</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-volume-mute"></i> fa fa-volume-mute</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-volume-mute"></i> fal fa-volume-mute</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-volume-off"></i> fa fa-volume-off</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-volume-off"></i> fal fa-volume-off</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-volume-slash"></i> fa fa-volume-slash</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-volume-slash"></i> fal fa-volume-slash</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-volume-up"></i> fa fa-volume-up</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-volume-up"></i> fal fa-volume-up</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-webcam"></i> fa fa-webcam</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-webcam"></i> fal fa-webcam</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-webcam-slash"></i> fa fa-webcam-slash</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-webcam-slash"></i> fal fa-webcam-slash</a></div>
    
  </div>

</section>

  <section id="brand">
  <h2 class="page-header">Brand Icons</h2>

  <div class="row fontawesome-icon-list margin-bottom-lg">
    

      <!-- <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-500px"></i> 500px</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-adn"></i> adn</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-amazon"></i> amazon</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-android"></i> android</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-angellist"></i> angellist</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-apple"></i> apple</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-behance"></i> behance</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-behance-square"></i> behance-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bitbucket"></i> bitbucket</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bitbucket-square"></i> bitbucket-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bitcoin"></i> bitcoin <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-black-tie"></i> black-tie</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-btc"></i> btc</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-buysellads"></i> buysellads</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cc-amex"></i> cc-amex</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cc-diners-club"></i> cc-diners-club</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cc-discover"></i> cc-discover</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cc-jcb"></i> cc-jcb</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cc-mastercard"></i> cc-mastercard</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cc-paypal"></i> cc-paypal</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cc-stripe"></i> cc-stripe</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cc-visa"></i> cc-visa</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-chrome"></i> chrome</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-codepen"></i> codepen</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-connectdevelop"></i> connectdevelop</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-contao"></i> contao</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-css3"></i> css3</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-dashcube"></i> dashcube</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-delicious"></i> delicious</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-deviantart"></i> deviantart</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-digg"></i> digg</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-dribbble"></i> dribbble</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-dropbox"></i> dropbox</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-drupal"></i> drupal</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-empire"></i> empire</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-expeditedssl"></i> expeditedssl</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-facebook"></i> facebook</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-facebook-f"></i> facebook-f <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-facebook-official"></i> facebook-official</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-facebook-square"></i> facebook-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-firefox"></i> firefox</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-flickr"></i> flickr</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-fonticons"></i> fonticons</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-forumbee"></i> forumbee</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-foursquare"></i> foursquare</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-ge"></i> ge <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-get-pocket"></i> get-pocket</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-gg"></i> gg</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-gg-circle"></i> gg-circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-git"></i> git</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-git-square"></i> git-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-github"></i> github</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-github-alt"></i> github-alt</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-github-square"></i> github-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-gittip"></i> gittip <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-google"></i> google</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-google-plus"></i> google-plus</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-google-plus-square"></i> google-plus-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-google-wallet"></i> google-wallet</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-gratipay"></i> gratipay</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hacker-news"></i> hacker-news</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-houzz"></i> houzz</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-html5"></i> html5</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-instagram"></i> instagram</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-internet-explorer"></i> internet-explorer</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-ioxhost"></i> ioxhost</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-joomla"></i> joomla</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-jsfiddle"></i> jsfiddle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-lastfm"></i> lastfm</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-lastfm-square"></i> lastfm-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-leanpub"></i> leanpub</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-linkedin"></i> linkedin</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-linkedin-square"></i> linkedin-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-linux"></i> linux</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-maxcdn"></i> maxcdn</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-meanpath"></i> meanpath</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-medium"></i> medium</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-odnoklassniki"></i> odnoklassniki</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-odnoklassniki-square"></i> odnoklassniki-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-opencart"></i> opencart</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-openid"></i> openid</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-opera"></i> opera</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-optin-monster"></i> optin-monster</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-pagelines"></i> pagelines</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-paypal"></i> paypal</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-pied-piper"></i> pied-piper</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-pied-piper-alt"></i> pied-piper-alt</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-pinterest"></i> pinterest</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-pinterest-p"></i> pinterest-p</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-pinterest-square"></i> pinterest-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-qq"></i> qq</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-ra"></i> ra <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-rebel"></i> rebel</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-reddit"></i> reddit</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-reddit-square"></i> reddit-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-renren"></i> renren</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-safari"></i> safari</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-sellsy"></i> sellsy</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-share-alt"></i> share-alt</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-share-alt-square"></i> share-alt-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-shirtsinbulk"></i> shirtsinbulk</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-simplybuilt"></i> simplybuilt</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-skyatlas"></i> skyatlas</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-skype"></i> skype</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-slack"></i> slack</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-slideshare"></i> slideshare</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-soundcloud"></i> soundcloud</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-spotify"></i> spotify</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-stack-exchange"></i> stack-exchange</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-stack-overflow"></i> stack-overflow</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-steam"></i> steam</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-steam-square"></i> steam-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-stumbleupon"></i> stumbleupon</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-stumbleupon-circle"></i> stumbleupon-circle</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-tencent-weibo"></i> tencent-weibo</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-trello"></i> trello</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-tripadvisor"></i> tripadvisor</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-tumblr"></i> tumblr</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-tumblr-square"></i> tumblr-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-twitch"></i> twitch</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-twitter"></i> twitter</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-twitter-square"></i> twitter-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-viacoin"></i> viacoin</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-vimeo"></i> vimeo</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-vimeo-square"></i> vimeo-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-vine"></i> vine</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-vk"></i> vk</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-wechat"></i> wechat <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-weibo"></i> weibo</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-weixin"></i> weixin</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-whatsapp"></i> whatsapp</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-wikipedia-w"></i> wikipedia-w</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-windows"></i> windows</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-wordpress"></i> wordpress</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-xing"></i> xing</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-xing-square"></i> xing-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-y-combinator"></i> y-combinator</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-y-combinator-square"></i> y-combinator-square <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-yahoo"></i> yahoo</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-yc"></i> yc <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-yc-square"></i> yc-square <span class="text-muted">(alias)</span></a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-yelp"></i> yelp</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-youtube"></i> youtube</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-youtube-play"></i> youtube-play</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-youtube-square"></i> youtube-square</a></div> -->
      
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-500px"></i> fab fa-500px</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-accessible-icon"></i> fab fa-accessible-icon</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-accusoft"></i> fab fa-accusoft</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-acquisitions-incorporated"></i> fab fa-acquisitions-incorporated</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-adn"></i> fab fa-adn</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-adobe"></i> fab fa-adobe</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-adversal"></i> fab fa-adversal</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-affiliatetheme"></i> fab fa-affiliatetheme</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-airbnb"></i> fab fa-airbnb</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-algolia"></i> fab fa-algolia</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-alipay"></i> fab fa-alipay</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-amazon"></i> fab fa-amazon</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-amazon-pay"></i> fab fa-amazon-pay</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-amilia"></i> fab fa-amilia</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-android"></i> fab fa-android</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-angellist"></i> fab fa-angellist</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-angrycreative"></i> fab fa-angrycreative</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-angular"></i> fab fa-angular</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-app-store"></i> fab fa-app-store</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-app-store-ios"></i> fab fa-app-store-ios</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-apper"></i> fab fa-apper</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-apple"></i> fab fa-apple</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-apple-pay"></i> fab fa-apple-pay</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-artstation"></i> fab fa-artstation</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-asymmetrik"></i> fab fa-asymmetrik</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-atlassian"></i> fab fa-atlassian</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-audible"></i> fab fa-audible</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-autoprefixer"></i> fab fa-autoprefixer</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-avianex"></i> fab fa-avianex</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-aviato"></i> fab fa-aviato</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-aws"></i> fab fa-aws</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-bandcamp"></i> fab fa-bandcamp</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-battle-net"></i> fab fa-battle-net</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-behance"></i> fab fa-behance</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-behance-square"></i> fab fa-behance-square</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-bimobject"></i> fab fa-bimobject</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-bitbucket"></i> fab fa-bitbucket</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-bitcoin"></i> fab fa-bitcoin</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-bity"></i> fab fa-bity</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-black-tie"></i> fab fa-black-tie</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-blackberry"></i> fab fa-blackberry</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-blogger"></i> fab fa-blogger</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-blogger-b"></i> fab fa-blogger-b</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-bluetooth"></i> fab fa-bluetooth</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-bluetooth-b"></i> fab fa-bluetooth-b</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-bootstrap"></i> fab fa-bootstrap</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-btc"></i> fab fa-btc</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-buffer"></i> fab fa-buffer</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-buromobelexperte"></i> fab fa-buromobelexperte</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-buysellads"></i> fab fa-buysellads</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-canadian-maple-leaf"></i> fab fa-canadian-maple-leaf</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-amazon-pay"></i> fab fa-cc-amazon-pay</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-amex"></i> fab fa-cc-amex</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-apple-pay"></i> fab fa-cc-apple-pay</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-diners-club"></i> fab fa-cc-diners-club</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-discover"></i> fab fa-cc-discover</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-jcb"></i> fab fa-cc-jcb</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-mastercard"></i> fab fa-cc-mastercard</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-paypal"></i> fab fa-cc-paypal</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-stripe"></i> fab fa-cc-stripe</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cc-visa"></i> fab fa-cc-visa</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-centercode"></i> fab fa-centercode</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-centos"></i> fab fa-centos</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-chrome"></i> fab fa-chrome</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-chromecast"></i> fab fa-chromecast</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cloudscale"></i> fab fa-cloudscale</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cloudsmith"></i> fab fa-cloudsmith</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cloudversify"></i> fab fa-cloudversify</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-codepen"></i> fab fa-codepen</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-codiepie"></i> fab fa-codiepie</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-confluence"></i> fab fa-confluence</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-connectdevelop"></i> fab fa-connectdevelop</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-contao"></i> fab fa-contao</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cotton-bureau"></i> fab fa-cotton-bureau</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cpanel"></i> fab fa-cpanel</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-creative-commons"></i> fab fa-creative-commons</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-creative-commons-by"></i> fab fa-creative-commons-by</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-creative-commons-nc"></i> fab fa-creative-commons-nc</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-creative-commons-nc-eu"></i> fab fa-creative-commons-nc-eu</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-creative-commons-nc-jp"></i> fab fa-creative-commons-nc-jp</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-creative-commons-nd"></i> fab fa-creative-commons-nd</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-creative-commons-pd"></i> fab fa-creative-commons-pd</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-creative-commons-pd-alt"></i> fab fa-creative-commons-pd-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-creative-commons-remix"></i> fab fa-creative-commons-remix</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-creative-commons-sa"></i> fab fa-creative-commons-sa</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-creative-commons-sampling"></i> fab fa-creative-commons-sampling</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-creative-commons-sampling-plus"></i> fab fa-creative-commons-sampling-plus</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-creative-commons-share"></i> fab fa-creative-commons-share</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-creative-commons-zero"></i> fab fa-creative-commons-zero</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-critical-role"></i> fab fa-critical-role</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-css3"></i> fab fa-css3</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-css3-alt"></i> fab fa-css3-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-cuttlefish"></i> fab fa-cuttlefish</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-d-and-d"></i> fab fa-d-and-d</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-d-and-d-beyond"></i> fab fa-d-and-d-beyond</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-dashcube"></i> fab fa-dashcube</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-delicious"></i> fab fa-delicious</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-deploydog"></i> fab fa-deploydog</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-deskpro"></i> fab fa-deskpro</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-dev"></i> fab fa-dev</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-deviantart"></i> fab fa-deviantart</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-dhl"></i> fab fa-dhl</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-diaspora"></i> fab fa-diaspora</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-digg"></i> fab fa-digg</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-digital-ocean"></i> fab fa-digital-ocean</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-discord"></i> fab fa-discord</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-discourse"></i> fab fa-discourse</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-dochub"></i> fab fa-dochub</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-docker"></i> fab fa-docker</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-draft2digital"></i> fab fa-draft2digital</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-dribbble"></i> fab fa-dribbble</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-dribbble-square"></i> fab fa-dribbble-square</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-dropbox"></i> fab fa-dropbox</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-drupal"></i> fab fa-drupal</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-dyalog"></i> fab fa-dyalog</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-earlybirds"></i> fab fa-earlybirds</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-ebay"></i> fab fa-ebay</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-edge"></i> fab fa-edge</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-elementor"></i> fab fa-elementor</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-ello"></i> fab fa-ello</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-ember"></i> fab fa-ember</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-empire"></i> fab fa-empire</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-envira"></i> fab fa-envira</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-erlang"></i> fab fa-erlang</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-ethereum"></i> fab fa-ethereum</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-etsy"></i> fab fa-etsy</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-evernote"></i> fab fa-evernote</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-expeditedssl"></i> fab fa-expeditedssl</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-facebook"></i> fab fa-facebook</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-facebook-f"></i> fab fa-facebook-f</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-facebook-messenger"></i> fab fa-facebook-messenger</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-facebook-square"></i> fab fa-facebook-square</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-fantasy-flight-games"></i> fab fa-fantasy-flight-games</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-fedex"></i> fab fa-fedex</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-fedora"></i> fab fa-fedora</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-figma"></i> fab fa-figma</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-firefox"></i> fab fa-firefox</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-first-order"></i> fab fa-first-order</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-first-order-alt"></i> fab fa-first-order-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-firstdraft"></i> fab fa-firstdraft</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-flickr"></i> fab fa-flickr</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-flipboard"></i> fab fa-flipboard</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-fly"></i> fab fa-fly</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-font-awesome"></i> fab fa-font-awesome</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-font-awesome-alt"></i> fab fa-font-awesome-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-font-awesome-flag"></i> fab fa-font-awesome-flag</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-fonticons"></i> fab fa-fonticons</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-fonticons-fi"></i> fab fa-fonticons-fi</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-fort-awesome"></i> fab fa-fort-awesome</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-fort-awesome-alt"></i> fab fa-fort-awesome-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-forumbee"></i> fab fa-forumbee</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-foursquare"></i> fab fa-foursquare</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-free-code-camp"></i> fab fa-free-code-camp</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-freebsd"></i> fab fa-freebsd</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-fulcrum"></i> fab fa-fulcrum</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-galactic-republic"></i> fab fa-galactic-republic</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-galactic-senate"></i> fab fa-galactic-senate</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-get-pocket"></i> fab fa-get-pocket</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-gg"></i> fab fa-gg</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-gg-circle"></i> fab fa-gg-circle</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-git"></i> fab fa-git</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-git-alt"></i> fab fa-git-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-git-square"></i> fab fa-git-square</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-github"></i> fab fa-github</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-github-alt"></i> fab fa-github-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-github-square"></i> fab fa-github-square</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-gitkraken"></i> fab fa-gitkraken</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-gitlab"></i> fab fa-gitlab</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-gitter"></i> fab fa-gitter</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-glide"></i> fab fa-glide</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-glide-g"></i> fab fa-glide-g</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-gofore"></i> fab fa-gofore</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-goodreads"></i> fab fa-goodreads</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-goodreads-g"></i> fab fa-goodreads-g</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-google"></i> fab fa-google</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-google-drive"></i> fab fa-google-drive</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-google-play"></i> fab fa-google-play</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-google-plus"></i> fab fa-google-plus</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-google-plus-g"></i> fab fa-google-plus-g</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-google-plus-square"></i> fab fa-google-plus-square</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-google-wallet"></i> fab fa-google-wallet</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-gratipay"></i> fab fa-gratipay</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-grav"></i> fab fa-grav</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-gripfire"></i> fab fa-gripfire</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-grunt"></i> fab fa-grunt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-gulp"></i> fab fa-gulp</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-hacker-news"></i> fab fa-hacker-news</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-hacker-news-square"></i> fab fa-hacker-news-square</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-hackerrank"></i> fab fa-hackerrank</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-hips"></i> fab fa-hips</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-hire-a-helper"></i> fab fa-hire-a-helper</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-hooli"></i> fab fa-hooli</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-hornbill"></i> fab fa-hornbill</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-hotjar"></i> fab fa-hotjar</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-houzz"></i> fab fa-houzz</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-html5"></i> fab fa-html5</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-hubspot"></i> fab fa-hubspot</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-imdb"></i> fab fa-imdb</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-instagram"></i> fab fa-instagram</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-intercom"></i> fab fa-intercom</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-internet-explorer"></i> fab fa-internet-explorer</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-invision"></i> fab fa-invision</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-ioxhost"></i> fab fa-ioxhost</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-itch-io"></i> fab fa-itch-io</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-itunes"></i> fab fa-itunes</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-itunes-note"></i> fab fa-itunes-note</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-java"></i> fab fa-java</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-jedi-order"></i> fab fa-jedi-order</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-jenkins"></i> fab fa-jenkins</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-jira"></i> fab fa-jira</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-joget"></i> fab fa-joget</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-joomla"></i> fab fa-joomla</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-js"></i> fab fa-js</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-js-square"></i> fab fa-js-square</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-jsfiddle"></i> fab fa-jsfiddle</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-kaggle"></i> fab fa-kaggle</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-keybase"></i> fab fa-keybase</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-keycdn"></i> fab fa-keycdn</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-kickstarter"></i> fab fa-kickstarter</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-kickstarter-k"></i> fab fa-kickstarter-k</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-korvue"></i> fab fa-korvue</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-laravel"></i> fab fa-laravel</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-lastfm"></i> fab fa-lastfm</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-lastfm-square"></i> fab fa-lastfm-square</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-leanpub"></i> fab fa-leanpub</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-less"></i> fab fa-less</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-line"></i> fab fa-line</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-linkedin"></i> fab fa-linkedin</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-linkedin-in"></i> fab fa-linkedin-in</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-linode"></i> fab fa-linode</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-linux"></i> fab fa-linux</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-lyft"></i> fab fa-lyft</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-magento"></i> fab fa-magento</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-mailchimp"></i> fab fa-mailchimp</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-mandalorian"></i> fab fa-mandalorian</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-markdown"></i> fab fa-markdown</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-mastodon"></i> fab fa-mastodon</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-maxcdn"></i> fab fa-maxcdn</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-medapps"></i> fab fa-medapps</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-medium"></i> fab fa-medium</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-medium-m"></i> fab fa-medium-m</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-medrt"></i> fab fa-medrt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-meetup"></i> fab fa-meetup</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-megaport"></i> fab fa-megaport</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-mendeley"></i> fab fa-mendeley</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-microsoft"></i> fab fa-microsoft</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-mix"></i> fab fa-mix</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-mixcloud"></i> fab fa-mixcloud</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-mizuni"></i> fab fa-mizuni</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-modx"></i> fab fa-modx</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-monero"></i> fab fa-monero</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fab fa-napster"></i> fab fa-napster</a></div>
    
  </div>

</section>

  <section id="medical">
  <h2 class="page-header">Medical Icons</h2>

  <div class="row fontawesome-icon-list">
    

      <!-- <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-ambulance"></i> ambulance</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-h-square"></i> h-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-heart"></i> heart</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-heart-o"></i> heart-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-heartbeat"></i> heartbeat</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hospital-o"></i> hospital-o</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-medkit"></i> medkit</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-plus-square"></i> plus-square</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-stethoscope"></i> stethoscope</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-user-md"></i> user-md</a></div>
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-wheelchair"></i> wheelchair</a></div> -->
      
      <div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-allergies"></i> fa fa-allergies</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-allergies"></i> fal fa-allergies</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-ambulance"></i> fa fa-ambulance</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-ambulance"></i> fal fa-ambulance</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-band-aid"></i> fa fa-band-aid</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-band-aid"></i> fal fa-band-aid</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-biohazard"></i> fa fa-biohazard</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-biohazard"></i> fal fa-biohazard</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bone"></i> fa fa-bone</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-bone"></i> fal fa-bone</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bone-break"></i> fa fa-bone-break</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-bone-break"></i> fal fa-bone-break</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-bong"></i> fa fa-bong</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-bong"></i> fal fa-bong</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-book-medical"></i> fa fa-book-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-book-medical"></i> fal fa-book-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-book-user"></i> fa fa-book-user</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-book-user"></i> fal fa-book-user</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-books-medical"></i> fa fa-books-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-books-medical"></i> fal fa-books-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-brain"></i> fa fa-brain</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-brain"></i> fal fa-brain</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-briefcase-medical"></i> fa fa-briefcase-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-briefcase-medical"></i> fal fa-briefcase-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-burn"></i> fa fa-burn</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-burn"></i> fal fa-burn</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-cannabis"></i> fa fa-cannabis</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-cannabis"></i> fal fa-cannabis</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-capsules"></i> fa fa-capsules</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-capsules"></i> fal fa-capsules</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-clinic-medical"></i> fa fa-clinic-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-clinic-medical"></i> fal fa-clinic-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-clipboard-prescription"></i> fa fa-clipboard-prescription</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-clipboard-prescription"></i> fal fa-clipboard-prescription</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-clipboard-user"></i> fa fa-clipboard-user</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-clipboard-user"></i> fal fa-clipboard-user</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-comment-alt-medical"></i> fa fa-comment-alt-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-comment-alt-medical"></i> fal fa-comment-alt-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-comment-medical"></i> fa fa-comment-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-comment-medical"></i> fal fa-comment-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-crutch"></i> fa fa-crutch</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-crutch"></i> fal fa-crutch</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-crutches"></i> fa fa-crutches</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-crutches"></i> fal fa-crutches</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-diagnoses"></i> fa fa-diagnoses</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-diagnoses"></i> fal fa-diagnoses</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-disease"></i> fa fa-disease</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-disease"></i> fal fa-disease</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-dna"></i> fa fa-dna</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-dna"></i> fal fa-dna</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-ear"></i> fa fa-ear</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-ear"></i> fal fa-ear</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-medical"></i> fa fa-file-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-file-medical"></i> fal fa-file-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-medical-alt"></i> fa fa-file-medical-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-file-medical-alt"></i> fal fa-file-medical-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-file-prescription"></i> fa fa-file-prescription</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-file-prescription"></i> fal fa-file-prescription</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-files-medical"></i> fa fa-files-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-files-medical"></i> fal fa-files-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-first-aid"></i> fa fa-first-aid</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-first-aid"></i> fal fa-first-aid</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-head-side-brain"></i> fa fa-head-side-brain</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-head-side-brain"></i> fal fa-head-side-brain</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-head-side-medical"></i> fa fa-head-side-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-head-side-medical"></i> fal fa-head-side-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-heart"></i> fa fa-heart</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-heart"></i> fal fa-heart</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-heart-rate"></i> fa fa-heart-rate</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-heart-rate"></i> fal fa-heart-rate</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-heartbeat"></i> fa fa-heartbeat</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-heartbeat"></i> fal fa-heartbeat</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hospital"></i> fa fa-hospital</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-hospital"></i> fal fa-hospital</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hospital-alt"></i> fa fa-hospital-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-hospital-alt"></i> fal fa-hospital-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hospital-symbol"></i> fa fa-hospital-symbol</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-hospital-symbol"></i> fal fa-hospital-symbol</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hospital-user"></i> fa fa-hospital-user</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-hospital-user"></i> fal fa-hospital-user</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-hospitals"></i> fa fa-hospitals</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-hospitals"></i> fal fa-hospitals</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-id-card-alt"></i> fa fa-id-card-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-id-card-alt"></i> fal fa-id-card-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-inhaler"></i> fa fa-inhaler</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-inhaler"></i> fal fa-inhaler</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-joint"></i> fa fa-joint</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-joint"></i> fal fa-joint</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-kidneys"></i> fa fa-kidneys</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-kidneys"></i> fal fa-kidneys</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-laptop-medical"></i> fa fa-laptop-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-laptop-medical"></i> fal fa-laptop-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-lips"></i> fa fa-lips</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-lips"></i> fal fa-lips</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-lungs"></i> fa fa-lungs</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-lungs"></i> fal fa-lungs</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-microscope"></i> fa fa-microscope</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-microscope"></i> fal fa-microscope</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-monitor-heart-rate"></i> fa fa-monitor-heart-rate</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-monitor-heart-rate"></i> fal fa-monitor-heart-rate</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-mortar-pestle"></i> fa fa-mortar-pestle</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-mortar-pestle"></i> fal fa-mortar-pestle</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-notes-medical"></i> fa fa-notes-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-notes-medical"></i> fal fa-notes-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-pager"></i> fa fa-pager</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-pager"></i> fal fa-pager</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-pills"></i> fa fa-pills</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-pills"></i> fal fa-pills</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-plus"></i> fa fa-plus</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-plus"></i> fal fa-plus</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-poop"></i> fa fa-poop</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-poop"></i> fal fa-poop</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-prescription"></i> fa fa-prescription</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-prescription"></i> fal fa-prescription</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-prescription-bottle"></i> fa fa-prescription-bottle</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-prescription-bottle"></i> fal fa-prescription-bottle</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-prescription-bottle-alt"></i> fa fa-prescription-bottle-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-prescription-bottle-alt"></i> fal fa-prescription-bottle-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-procedures"></i> fa fa-procedures</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-procedures"></i> fal fa-procedures</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-radiation"></i> fa fa-radiation</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-radiation"></i> fal fa-radiation</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-radiation-alt"></i> fa fa-radiation-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-radiation-alt"></i> fal fa-radiation-alt</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-scalpel"></i> fa fa-scalpel</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-scalpel"></i> fal fa-scalpel</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-scalpel-path"></i> fa fa-scalpel-path</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-scalpel-path"></i> fal fa-scalpel-path</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-skeleton"></i> fa fa-skeleton</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-skeleton"></i> fal fa-skeleton</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-smoking"></i> fa fa-smoking</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-smoking"></i> fal fa-smoking</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-smoking-ban"></i> fa fa-smoking-ban</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-smoking-ban"></i> fal fa-smoking-ban</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-star-of-life"></i> fa fa-star-of-life</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-star-of-life"></i> fal fa-star-of-life</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-stethoscope"></i> fa fa-stethoscope</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-stethoscope"></i> fal fa-stethoscope</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-stomach"></i> fa fa-stomach</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-stomach"></i> fal fa-stomach</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-stretcher"></i> fa fa-stretcher</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-stretcher"></i> fal fa-stretcher</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-syringe"></i> fa fa-syringe</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-syringe"></i> fal fa-syringe</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-tablets"></i> fa fa-tablets</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-tablets"></i> fal fa-tablets</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-teeth"></i> fa fa-teeth</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-teeth"></i> fal fa-teeth</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-teeth-open"></i> fa fa-teeth-open</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-teeth-open"></i> fal fa-teeth-open</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-thermometer"></i> fa fa-thermometer</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-thermometer"></i> fal fa-thermometer</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-tooth"></i> fa fa-tooth</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-tooth"></i> fal fa-tooth</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-toothbrush"></i> fa fa-toothbrush</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-toothbrush"></i> fal fa-toothbrush</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-user-md"></i> fa fa-user-md</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-user-md"></i> fal fa-user-md</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-user-md-chat"></i> fa fa-user-md-chat</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-user-md-chat"></i> fal fa-user-md-chat</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-user-nurse"></i> fa fa-user-nurse</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-user-nurse"></i> fal fa-user-nurse</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-users-medical"></i> fa fa-users-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-users-medical"></i> fal fa-users-medical</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-vial"></i> fa fa-vial</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-vial"></i> fal fa-vial</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-vials"></i> fa fa-vials</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-vials"></i> fal fa-vials</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-walker"></i> fa fa-walker</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-walker"></i> fal fa-walker</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-watch-fitness"></i> fa fa-watch-fitness</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-watch-fitness"></i> fal fa-watch-fitness</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-weight"></i> fa fa-weight</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-weight"></i> fal fa-weight</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fa fa-x-ray"></i> fa fa-x-ray</a></div><div class="col-xs-2 col-sm-1 icon-item"><a href="#"><i class="fal fa-x-ray"></i> fal fa-x-ray</a></div>
    
  </div>

</section>

</div>

</div>