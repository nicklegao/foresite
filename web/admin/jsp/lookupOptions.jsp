<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.Vector"%>
<%@page import="au.net.webdirector.common.utils.module.DropDownBuilder"%>
<%@page import="au.net.webdirector.common.datatypes.domain.DropDownData"%>
<%@page import="java.util.List"%>
<%@page import="au.com.ci.webdirector.user.UserManager"%>
<%
String module = request.getParameter("module");
String field = request.getParameter("field");
String user = UserManager.getUserName(request);
String tableType = request.getParameter("tableType");
String widget = request.getParameter("widget");
List<DropDownData> list = new DropDownBuilder().getDropDownDatas(module, field, user, tableType);

String savedlookup = "";
Cookie[] cookies = request.getCookies();
if (cookies != null) {
 for (Cookie cookie : cookies) {
   if (cookie.getName().equals("dropdownlookup-"+user+"-"+module+"-"+field)) {
     savedlookup = cookie.getValue();
    }
  }
}
if(list.size() > 0)
{
	DropDownData data = list.get(0);
	Vector<String[]> columnNames = data.getColumnName();
	if(columnNames == null){
		columnNames = new Vector<String[]>();
	}
%>
		<script type="text/javascript" charset="utf-8">
			var currentFilterText;
			var asInitVals = new Array();
			$(document).ready(function() {

				$("#search_text").keydown( function () {
					currentFilterText = this.value;
				} );
				$("#search_text").keyup( function () {
					if(currentFilterText != this.value){
						$("#dataForm input[name='pageNumber']").val("1");
						loadData();
					}
				} );

				/*
				 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in 
				 * the footer
				 */
				$("#search_text").each( function (i) {
					asInitVals[i] = this.value;
				} );

				$("#search_text").focus( function () {
					if ( this.className == "search_init" )
					{
						this.className = "";
						this.value = "";
					}
				} );

				$("#search_text").blur( function (i) {
					if ( this.value == "" )
					{
						this.className = "search_init";
						this.value = asInitVals[$("#search_text").index(this)];
					}
				} );

			} );
 
			/* Get the rows which are currently selected */
			function fnGetSelected()
			{
				var row = $('#datatable tbody tr.row_selected');
				var nTds = $('td', row);
				setValue('<%= widget %>', '<%= field %>', $(nTds[0]).text());
				ModalHelper.closeModal();

			}
			
			function closeWidget(){
				ModalHelper.closeModal();
			}

			function initTable(){
				$("#datatable tbody tr").click( function( e ) {
					if ( $(this).hasClass('row_selected') ) {
						$(this).removeClass('row_selected');
					}
					else {
						$('#datatable tbody tr.row_selected').removeClass('row_selected');
						$(this).addClass('row_selected');
					}
				});

				$("#datatable tbody tr").dblclick( function( e ) {
					$('#datatable tbody tr.row_selected').removeClass('row_selected');
					$(this).addClass('row_selected');
					fnGetSelected();
				});
				$("#datatable tbody td").each( function( ) {
					$(this).attr("nowrap", "1");
				});
				$("#datatable thead th").each( function( ) {
					$(this).attr("nowrap", "1");
				});
			}

			function loadData(){
				var url = "/webdirector/lookup-options/data?" + $("#dataForm").serialize();
				$("#container").load(url, initTable);
			}

			function goPage(num){
				$("#dataForm input[name='pageNumber']").val(num*1+$("#dataForm input[name='pageNumber']").val()*1);
				loadData();
			}
		</script>

		<SCRIPT type="text/javascript">
		$(document).ready(function(){
			loadData();
		});

		function addColumn(){
			var selector = document.getElementById("columnSelector");
			var showCols = $("#dataForm input[name='showCols']");
			var selected = selector.value;
			if(selected == -1){
				return;
			}
			if(showCols.val().indexOf(","+selected+",") == -1){
				showCols.val(showCols.val()+ selected + ",");
				loadData();
			}
		}

		function shutOffCol(num){
			var showCols = $("#dataForm input[name='showCols']");
			if(showCols.val().indexOf(","+num+",")!= -1){
				showCols.val(showCols.val().replace(","+num+",", ","));
				loadData();
			}
		}

		function setValue(widget, field, value){
		  $('[name="'+field+'"]', '#' + widget).val(value);
		}
		</SCRIPT>
<style>
#datatable tr.row_selected {
	background: none repeat scroll 0 0 #88deff !important;
}
</style>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title"> Lookup Dropdown Values </h4>
</div>
<div class="modal-body">
  <div style="color:#111">
  <form name="dataForm1" id="dataForm">
  Add Columns:&nbsp;&nbsp;
  <select name="columnSelector" id="columnSelector">
  <option value="-1">--Please Select One Column--</option>
  <%
  int colCount = 0;
  for(String[] columnName:columnNames){
  %>
  <option value="<%= colCount++ %>"><%= columnName[1] %></option>
  <%}%>
  </select>
  <span class="button" onclick="javascript:addColumn();" style="cursor:pointer;">Add</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Search:&nbsp;&nbsp;<input type="text" class="search_init" value="Filter" id="search_text" name="search_text"></span>
  <input type="hidden" name="module" value="<%= module %>">
  <input type="hidden" name="field" value="<%= field %>">
  <input type="hidden" name="tableType" value="<%= tableType %>">
  <input type="hidden" name="user" value="<%= user %>">
  <input type="hidden" name="pageNumber" value="1">
  <input type="hidden" name="showCols" value="<%= (StringUtils.isNotBlank(savedlookup)? savedlookup : ",")%>">
  </div>
  <br>
  <div id="container" style="width:98%;" onselectstart="return false" style="color:#111;">
  </div>
  </form>
  <%
  }else{
  %>
  <div>no values found!</div>
  <%
  }
  %>
  			
</div>
<div class="modal-footer">
  <div align="center" width="100%">
     <span class="btn btn-success" onclick="javascript:fnGetSelected();" style="cursor:pointer;">OK</span>&nbsp;&nbsp;<span class="btn btn-default" onclick="javascript:closeWidget();" style="cursor:pointer;">Cancel</span>
  </div>
</div>