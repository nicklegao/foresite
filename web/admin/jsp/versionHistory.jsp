<%@page import="au.net.webdirector.admin.version.VersionInfo"%>
<%@page import="au.net.webdirector.admin.version.VersionControlService"%>
<%@page import="java.util.*"%>
<%@page import="au.net.webdirector.common.utils.SimpleDateFormat"%>
<%@page import="au.net.webdirector.common.datalayer.base.transaction.TransactionManager"%>

<%
String module = request.getParameter("module");
String tableType = request.getParameter("tableType");
String dataId = request.getParameter("dataId");

VersionControlService svc = new VersionControlService(request);
List<VersionInfo> versionInfo = svc.getVersionList(module, tableType, dataId);
String currentVersionNo = versionInfo.get(0).getVersionNo();
String currentVersionId = versionInfo.get(0).getSysId();
%>

<%!
public String getFormatted(Date date) {
    return new SimpleDateFormat("dd/MM/yyyy'<br>'hh:mm aa").format(date);
}
%>

<link rel="stylesheet" href="/admin/css/versionHistory.css" />
<!-- <link rel="stylesheet/less" type="text/css" href="/admin/css/less/versionHistory.less" />
<script src="/admin/js/less/less.min.js" data-poll="1000" data-relative-urls="false"></script> -->

<form id="versionHistory" class="version-history">
  <header>
    <h4>Version History</h4>
  </header>
  <main>
    <!-- Timeline Content -->
    <div class="smart-timeline col-xs-6 col-lg-5">
      <ul class="smart-timeline-list">
        <% for (int i=0; i < versionInfo.size(); i++) {
            VersionInfo version = versionInfo.get(i);
            String leftChecked = i == 0 ? "checked='checked'" : "";
        %>
        <li class="highlight-ready">
          <div class="smart-timeline-time">
            <small>
              <%=i == 0 ? "Current Version<br>" : "" %>
              <%=getFormatted(version.getWhen())%>
            </small>
          </div>
          <div class="smart-timeline-content">
            <p>
              User: <%=version.getWho() %>
            </p>
            <p>
              Action: <%=version.getAction() %>
            </p>
          </div>
          <div class="smart-timeline-icon">
            <label class="compare-left">
              <input type="radio" name="leftCompare" value="<%=version.getSysId()%>" <%=leftChecked%> >
              <span></span>
            </label>
            <i><%=version.getVersionNo()%></i>
            <label class="compare-right">
              <input type="radio" name="rightCompare" value="<%=version.getSysId()%>">
              <span></span>
            </label>
          </div>
        </li>
        <% } %>
      </ul>
    </div>
    <!-- END Timeline Content -->
    <div class="version-comparison col-xs-6 col-lg-7">
      <table class="table-comparison table-condensed">
        <thead>
          <tr>
            <th class="version-left"></th>
            <th class="version-right"></th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
      <div class="message">
        Please select a version to compare
      </div>
    </div>
  </main>
  <footer>
    <button type="submit" class="btn btn-primary btn-emboss">Revert Version <i class="fa fa-refresh fa-spin" style="display:none"></i></button>
  </footer>
</form>

<script>
$(function() {
  var module = "<%=module%>";
  var tableType = "<%=tableType%>";
  var dataId = "<%=dataId%>";
  var currentVersionId = "<%=currentVersionId%>";
  
  $("input[name='leftCompare']").click(function(e) {
    e.stopPropagation();
    e.preventDefault();
  });
  
  $("input[name='rightCompare']").click(function(e) {
    e.stopPropagation();
    e.preventDefault();
  });
  
  $(".compare-left").hover(toggleHighlightReady, toggleHighlightReady).click(function(e) {
    e.stopPropagation();
    $(this).find('input[name="leftCompare"]').prop("checked", "checked");
    compareVersions(this);
  });
  
  $(".highlight-ready").click(function(e) {
    var $this = $(this);
    $this.find('input[name="rightCompare"]').prop("checked", "checked");
    $this.closest("ul").find(".selected").removeClass("selected");
    $this.addClass("selected");
    compareVersions(this);
  });
  
  $("#versionHistory").submit(function(e) {
    e.preventDefault();
    var $this = $(this);
    var $selected = $this.find('input[name="rightCompare"]:checked');
    if ($selected.length !== 1 || $selected.val() === currentVersionId) {
      alert("Please select a past version to revert to");
      return;
    }
    if ($this.find("tbody tr").length === 0) {
      alert("The selected version is identical to the current one");
      return;
    }
    $this.find("button").addClass("disabled").find("i").show();
    revertVersion($selected.val());
  });
  
  function toggleHighlightReady() {
    $(this).closest("li").toggleClass("highlight-ready");
  }
  
  function compareVersions(that) {
    var $ul = $(that).closest("ul");
    var $leftChecked = $ul.find('input[name="leftCompare"]:checked');
    var $rightChecked = $ul.find('input[name="rightCompare"]:checked');
    
    if ($leftChecked.length === 1 && $rightChecked.length === 1) {
      $.ajax({
        url: '/webdirector/versionControl/compare',
        data: {
          module: module,
          tableType: tableType,
          versionId1: $leftChecked.val(),
          versionId2: $rightChecked.val()
        }
      }).done(function(data) {       
        if (data.length > 0) {
          var versionNo1 = $leftChecked.closest("div").find("i").text();
          var versionNo2 = $rightChecked.closest("div").find("i").text();
          displayComparison(data, versionNo1, versionNo2);
        } else {
          displayIdentical();
        }
      });
    }
  }
  
  function displayComparison(diffList, versionNo1, versionNo2) {
    $(".version-comparison .message").hide();
    $(".table-comparison").hide().fadeIn();
    $(".table-comparison .version-left").text("Version " + versionNo1);
    $(".table-comparison .version-right").text("Version " + versionNo2);
    
    var $tbody = $(".table-comparison tbody");
    $tbody.empty();
    for (var i=0; i < diffList.length; i++) {
      var diff = diffList[i];
      $('<tr class="field"><td colspan="2"><i>'+diff.name+'</i></td></tr>').appendTo($tbody);
      $('<tr class="value"><td>'+diff.first+'</td><td>'+diff.second+'</td></tr>').appendTo($tbody);
    }
  }
  
  function displayIdentical() {
    $(".table-comparison").hide();
    $(".table-comparison .version-left").empty();
    $(".table-comparison .version-right").empty();
    $(".table-comparison tbody").empty();
    $(".version-comparison .message").text("The selected versions are identical").fadeIn();
  }
  
  function revertVersion(versionId) {
    $.ajax({
      url: '/webdirector/versionControl/revert',
      data: {
        module: module,
        tableType: tableType,
        versionId: versionId
      },
      type: 'POST'
    }).done(function() {
        $("#versionHistory").closest(".modal-panel-content").load('/webdirector/versionControl/history', {
          module: module,
          tableType: tableType,
          dataId: dataId
      });
    });
  }
  
});
</script>