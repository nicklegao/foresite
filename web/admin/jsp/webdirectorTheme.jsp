<!-- Basic Styles -->
<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/font-awesome.min.css">

<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/smartadmin-production-plugins.css">
<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/smartadmin-production.css">
<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/smartadmin-skins.css">

<!-- WebDirector Styles -->
<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/webdirector.css">
<!-- <link rel="stylesheet/less" type="text/css" href="/admin/css/less/webdirector.less" /> -->
<!-- <script src="/admin/js/less/less.min.js" data-poll="1000" data-relative-urls="false"></script> -->

<!-- GOOGLE FONT -->
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="/admin/js/upgradeJquery.js"></script>

<script src="/admin/js/libs/jquery-ui-1.10.3.min.js"></script>
<script src="/admin/js/bootstrap/bootstrap.min.js"></script>

<script src="/webdirector/free/csrf-token/init"></script>