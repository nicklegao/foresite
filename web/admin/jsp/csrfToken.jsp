<%@page import="au.net.webdirector.admin.security.filter.CheckCSRFTokenFilter"%>
<%response.setContentType("application/javascript"); %>
var csrfTokenValue = '<%= CheckCSRFTokenFilter.getCSRFTokenToSession(session) %>';
var csrfTokenName = '<%= CheckCSRFTokenFilter.TokenFieldName %>'
// always set token to header in ajax call
$.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
  jqXHR.setRequestHeader(csrfTokenName, csrfTokenValue);
});
$(function(){ 
  var injectTokenField = function(){
    injectTokenToOneFrame(window.document);
  }
  
  var injectTokenToOneFrame = function(document){
    $('form', document).each(function(){
      if($('input[name="'+ csrfTokenName +'"]',$(this)).length == 0){
        $('<input type="hidden" name="'+ csrfTokenName +'">').val(csrfTokenValue).addClass('skip-change-check').prependTo($(this));
      }
    });
  }
 
  $(document).ajaxSuccess(function(event, xhr, settings){
    injectTokenField();
  });
  
  injectTokenField();
  
  
});
