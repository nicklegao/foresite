<%@page import="org.springframework.web.util.UrlPathHelper"%>
<%@page import="au.com.ci.webdirector.user.UserManager"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.LinkedHashMap"%>
<jsp:useBean id="db" class="webdirector.db.UserUtils"/>
<%
String userId = UserManager.getUserId(request);
String [] vals = null;
if (userId != null && !userId.equals("")) {
	Vector v = db.getUser(userId);
	vals = (String[]) v.elementAt(0);
}

String access_file_manager = vals != null? vals[8] : "no";

LinkedHashMap<String, String> links = new LinkedHashMap<String, String>();
LinkedHashMap<String, String> icons = new LinkedHashMap<String, String>();
/* links.put("Dashboard", "/webdirector/dashboard");
icons.put("Dashboard", "fa fa-tachometer"); */
links.put("Workbench", "/webdirector/workbench");
icons.put("Workbench", "fa fa-archive");
if(access_file_manager.equalsIgnoreCase("yes")){
	links.put("File Manager", "javascript:void openFileManager($(this))");
	icons.put("File Manager", "fa fa-folders");
}
String uri = new UrlPathHelper().getOriginatingRequestUri(request);
%>
<ul class="nav navbar-nav ci-navbar-collapsing">
<% for (String link : links.keySet()){ 
	String liClass = "";
	if (uri.startsWith(links.get(link))) liClass = "active";
%>
	<li id="<%= "nav" + link.replaceAll(" ", "") %>" class="<%=liClass%>"><a href="<%=links.get(link)%>"><i class="<%=icons.get(link)%>"></i><span> <%=link %></span></a></li>
<% } %>
</ul>
