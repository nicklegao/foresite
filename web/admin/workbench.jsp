<!DOCTYPE html>
<%@page import="java.util.Date"%>
<%@page import="au.net.webdirector.common.utils.SimpleDateFormat"%>
<%@page import="au.net.webdirector.common.datalayer.base.db.DataMap"%>
<%@page import="webdirector.db.UserUtils"%>
<%@page import="au.net.webdirector.admin.security.LoginController"%>
<%
String prefixTitle = "["+request.getServerName() + request.getContextPath()+"] - "; 
LoginController.setFirstScreen(request, response);
%>
<html lang="en-us">
  <head>
  <script>
  var context = '<%= request.getContextPath() %>';
  </script>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    
    <title><%=prefixTitle%> WebDirector 10 - Workbench</title>
    <meta name="description" content="">
    <meta name="author" content="">
      
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/css/bootstrap.min.css">
    
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/fonts/segoe-ui-4-cufonfonts-webfont/style.css">

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/css/smartadmin-production-plugins.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/css/smartadmin-skins.min.css">

    <!-- SmartAdmin RTL Support  -->
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/css/smartadmin-rtl.min.css">

    <link rel="stylesheet" href="<%= request.getContextPath() %>/admin/plugins/spectrum/spectrum.css">

    <!-- WebDirector Styles -->
    <%
    boolean useLess = false; 
    String[] files = new String[]{"webdirector","dynamicFormBuilder","bulkEditor"};
    
    for (String f : files)
    {
    	%>
      <link rel="stylesheet<%=useLess?"/less":"" %>" type="text/css" href="<%= request.getContextPath() %>/admin/css<%=useLess?"/less":"" %>/<%=f %>.<%=useLess?"less":"min.css" %>" />
      <%
    }
    if (useLess) { %>
      <script src="<%= request.getContextPath() %>/admin/js/less/less.min.js" data-poll="1000" data-relative-urls="false"></script>
    <% } %>

    <!-- Plugin CSS, New CSS should be put beneath this -->
    
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/plugins/jqueryui.multiselect/ui.multiselect.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/plugins/bootstrap-datetimepicker/4.0.0/bootstrap-datetimepicker.css">

  
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/plugins/jquery.dataTables.bootstrap/css/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/plugins/jquery.dataTables/extensions/ColReorder/css/dataTables.colReorder.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/plugins/jquery.dataTables/extensions/FixedHeader/css/dataTables.fixedHeader.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/plugins/jquery.dataTables/extensions/TableTools/css/dataTables.tableTools.min.css">

    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/css/demo.min.css">

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<%= request.getContextPath() %>/admin/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<%= request.getContextPath() %>/admin/img/favicon/favicon.ico" type="image/x-icon">

    <!-- GOOGLE FONT -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto+Slab:700,300,300italic,700normal,700italic,400">

    <!-- Specifying a Webpage Icon for Web Clip 
       Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="<%= request.getContextPath() %>/admin/img/splash/sptouch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<%= request.getContextPath() %>/admin/img/splash/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<%= request.getContextPath() %>/admin/img/splash/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<%= request.getContextPath() %>/admin/img/splash/touch-icon-ipad-retina.png">
    
    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    
    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="<%= request.getContextPath() %>/admin/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="<%= request.getContextPath() %>/admin/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="<%= request.getContextPath() %>/admin/img/splash/iphone.png" media="screen and (max-device-width: 320px)">
    
    <link rel="stylesheet" href="<%= request.getContextPath() %>/admin/css/ui.fancytree.css">
    <link rel="stylesheet" href="<%= request.getContextPath() %>/admin/plugins/jquery.contextMenu/css/jquery.contextMenu.css">
    <%-- <link rel="stylesheet" href="<%= request.getContextPath() %>/admin/css/bootstrap-material-design.css"> --%>
    <link rel="stylesheet" href="<%= request.getContextPath() %>/admin/css/ripples.css">
    <link rel="stylesheet" href="<%= request.getContextPath() %>/admin/css/workbench.css">
    
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/plugins/fontawesome-pro-5.8.1-web/css/all.css">
  </head>

  <%
  Boolean isAdmin = (Boolean)session.getAttribute(LoginController.WD_USER_ADMIN);
  String userId = (String)session.getAttribute(LoginController.WD_USER_ID);
  UserUtils userUtils = new UserUtils();
  DataMap userMap = userUtils.getUserDataMapById(userId);
  boolean canChangePassword = "yes".equals(userMap.getString("can_change_password"));
  boolean canEditAccount = "yes".equals(userMap.getString("can_edit_account"));
  %>
  <body class="fixed-header fixed-navigation fixed-ribbon fixed-page-footer <%= isAdmin?"admin-user":"nonadmin-user" %> module-inactive xs-module-selector" data-user="<%=session.getAttribute(LoginController.WD_USER) %>">
    <div class="blurred">
    <!-- HEADER -->
    <header id="header">
      <div id="logo-group">

        <span id="logo"> <img src="<%= request.getContextPath() %>/admin/img/custom/ci-logo.png" alt="Corporate Interactive "> </span>

      </div>

	<jsp:include page="include/i_navbar.jsp" />
	<div class="activity-container">
		<span id="activity" class="activity-dropdown"> <i class="fa fa-user"></i> <b class="badge">:(</b> </span>
        <div class="ajax-dropdown" style="display: none">
        	<div class="content">
        		<!-- content goes here -->
        		<h5 class="todo-group-title"><i class="far fa-dot-circle"></i> Awaiting Approvals (<small class="pendingCount">1</small>)</h5>
        		<ul id="pending" class="todo ui-sortable">
        			<li>
        				<span class="handle"> 
        						<button type="button" class="btn btn-raised bg-color-magenta txt-color-white btn-xs openEditor" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="ID:13"><i class="far fa-dot-circle-o"></i></button>
        				</span>
        				<p>
        					<strong>Element #1</strong> - Product Catalog <span class="text-muted">Author: </span>
        				</p>
        			</li>
        		</ul>
        		<h5 class="todo-group-title"><i class="far fa-circle"></i> Drafts (<small class="draftCount">3</small>)</h5>
        		<ul id="draft" class="todo ui-sortable">
        		</ul>
        
        		<!-- end content -->
        	</div>
        	<span style="bottom:2px;">Last Update: <span id="statusText"></span> 
        		<button type="button" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading..." class="btn btn-raised btn-xs btn-default pull-right">
        			<i class="fa fa-sync"></i>
        		</button> 
        	</span>
        </div>
	</div>		

      <!-- pulled right: nav area -->
      <div class="pull-right">
        
        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
          <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-bars"></i></a> </span>
        </div>
        <!-- end collapse menu -->

        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
          <span> <a href="<%= request.getContextPath() %>/webdirector" title="Sign Out" data-action="userLogout" data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i class="far fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->

        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
          <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="far fa-expand-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->

		<div class="dropdown btn-header transparent pull-right" id="configMenu">
			<a class="dropdown-toggle" role="button" href="javascript:void(0);" id="dropdownConfig" title="Configuration" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          		<i class="far fa-cogs"></i>
       		</a>
			<div class="dropdown-menu demo" id="settingsMenu" aria-labelledby="dropdownConfig">
		        <form class="settings-dropdown" style="padding: 5px 10px 10px">
		           <a href="javascript:void(0);" style="display:none" id="configure-module"><i></i><span>Configure Module</span></a>
		           <% if (!isAdmin) { %>
                     <% if (canChangePassword || canEditAccount){ %> 
  		             <h6 class="margin-top-10 bold margin-bottom-5">Settings</h6>
                     <% } %>
                     <% if (canChangePassword){ %>   
  		               <a href="<%= request.getContextPath() %>/webdirector/user/changePassword" class="dropdown-item">Change Password</a>
                     <% } %>
                     <% if (canEditAccount){ %>   
  		               <a href="<%= request.getContextPath() %>/webdirector/user/edit" class="dropdown-item">Edit Account</a>
  		             <% } %>
		           <% } %>
		           
		           	<h6 class="margin-top-10 bold margin-bottom-5 admin-func">Settings</h6>
		              <a href="<%= request.getContextPath() %>/webdirector/secure/user/add" class="dropdown-item">Add Users</a>
		              <a href="<%= request.getContextPath() %>/webdirector/secure/user/list" class="dropdown-item">Edit User</a>
		              <a href="<%= request.getContextPath() %>/webdirector/secure/rewrite/iframe/update" class="dropdown-item">Update Short URLs</a>
		              <a href="<%= request.getContextPath() %>/webdirector/secure/lucene/iframe/rebuild" class="dropdown-item">Build Lucene Index</a>
		              
		            <h6 class="margin-top-10 bold margin-bottom-5 admin-func">Utilities</h6>
		              <a href="<%= request.getContextPath() %>/webdirector/secure/scheduler/" class="dropdown-item">Job Scheduler</a>
		              <a href="javascript:deleteTemporaryFiles();" class="dropdown-item">Flush Temp Files</a>
		              <a href="<%= request.getContextPath() %>/webdirector/secure/audittrail/iframe/report" class="dropdown-item">Audit Trail Report</a>
		              <a href="<%= request.getContextPath() %>/webdirector/secure/module/iframe/purge" class="dropdown-item">Purge Module Data</a>
		              <a href="<%= request.getContextPath() %>/webdirector/secure/module/iframe/create" class="dropdown-item">Create Module</a>
		              <a href="<%= request.getContextPath() %>/webdirector/secure/module/iframe/delete" class="dropdown-item">Delete Module</a>
                  
                    <%@ include file="/admin/project/extraCogsButtons.jsp" %>
		        </form>
		      </div>
		</div>
      </div>
      <!-- end pulled right: nav area -->

    </header>
    <!-- END HEADER -->
		<div id="body">
		    <!-- Left panel : Navigation area -->
		    <!-- Note: This width of the aside area can be adjusted through LESS variables -->
		    <aside id="left-panel">
		
		      <!-- User info -->
		      <!-- end user info -->
		
		      <!-- NAVIGATION : This navigation is also responsive-->
		      <nav>
		        <ul class="categories-panel">
                  <li class="no-context-menu always-show pull-right-on-top">
                    <a id="show-shortcut" href="javascript:void(0);">
                      <!-- <img src="<%= request.getContextPath() %>/admin/img/avatars/sunny.png" alt="me" class="online" /> -->
                      <i class="fa fa-fw fa-lg fa-plus-square"></i>
                      <span class="menu-item-parent">Select<span class="hide-on-top"> Module</span></span>
                    </a>
                    
                  </li>
                </ul>
		        <span class="minifyme" data-action="minifyMenu"> 
		          <i class="fa fa-arrow-circle-left hit"></i>
		        </span>
		      </nav>
		
		    </aside>
		    <!-- END NAVIGATION -->
		
		    <!-- MAIN PANEL -->
		    <div id="main" role="main">
		
		      <!-- RIBBON -->
		      <div id="ribbon">
		
		        <span class="ribbon-button-alignment"> 
		          <span id="refresh" class="btn btn-raised btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true" data-viewport="#main">
		            <i class="fa fa-refresh"></i>
		          </span> 
		        </span>
		
						<span class="ribbon-text" rel="tooltip" data-toggle="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-info-circle'></i> <%=((Boolean)session.getAttribute(LoginController.WD_USER_ADMIN))?"Admin":"Author" %>" data-html="true">Logged in as <%=session.getAttribute(LoginController.WD_USER) %></span>
		        <!-- You can also add more buttons to the
		        ribbon for further usability
		
		        Example below:
		
		        <span class="ribbon-button-alignment pull-right">
		        <span id="search" class="btn btn-raised btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
		        <span id="add" class="btn btn-raised btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
		        <span id="search" class="btn btn-raised btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
		        </span> -->
		
		      </div>
		      <!-- END RIBBON -->
		
		      <!-- MAIN CONTENT -->
		      <div id="content">
		        <section id="widget-grid">
		          <!-- row -->
		          <div class="row">
		            <!-- SINGLE GRID -->
		            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		            </article><!-- GRID END -->
		          </div><!-- end row -->
		        </section>
		      </div>
		      <!-- END MAIN CONTENT -->
		
		    </div>
		    <!-- END MAIN PANEL -->
		</div>
    <!-- PAGE FOOTER -->
    <div class="page-footer">
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <span>WebDirector 10<span class="hidden-xs"> - Data Management</span> &copy; 2002-<%= new SimpleDateFormat("yyyy").format(new Date()) %></span>
        </div>

        <div class="col-xs-6 col-sm-6 text-right hidden-xs">
          <div class="txt-color-white inline-block">
            <i class="txt-color-blueLight hidden-mobile">You will be logged out at <i class="fa fa-clock-o fa-spin"></i> <strong class="autologoutTime"></strong> </i>
          </div>
        </div>
        
      </div>
    </div>
    <!-- END PAGE FOOTER -->
    </div>
    
    
    
          <div class="modal fade" id="modal-setting" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content" id="settingsContent"></div>
            </div>
          </div>
          
          <div class="modal fade" id="modal-fullwidth" tabindex="-2">
            <div class="modal-dialog">
              <div class="modal-content" id="fullWidthContent"></div>
            </div>
          </div>
          
          
    <div class="htmlFragments">
      <ul>
      <!-- 
        <li class="module bounceInDown animated">
       -->
        <li class="module">
          <a href="javascript:void(0);">
            <i class="fa-lg fa-fw"></i>
            <span class="menu-item-parent"></span>
            <b class="tree-search-icon"><em class="fa fa-lg fa-search"></em></b>
          </a>
          <section style="display:none">
            <div class="tree-search-box" style="display:none" data-search-mode="element">
              <button class="do-search"><i class="fa fa-search"></i></button>
              <input type="text">
              <!-- <label class="clear-search"><i class="fa fa-times"></i></label> -->
              <button type="button" class="clear-search"><i class="fa fa-times"></i></button>
              <div class="form-horizontal search-options">
                <!-- <span>Search</span> -->
                <label class="radio radio-inline">
                  <input type="radio" checked="checked" class="radiobox style-1" data-search-mode="element">
                  <span>Items</span> 
                </label>
                <label class="radio radio-inline">
                  <input type="radio" class="radiobox style-1" data-search-mode="category">
                  <span>Categories</span> 
                </label>
              </div>
            </div>
            <div class="lazytree"></div>
            <div class="search-tree"></div>
          </section>
        </li>
      </ul>
      <div id="-1" style="display:none" class="jarviswidget widget-unlocked stdWidgetContainer" data-widget-colorbutton="false" data-widget-editbutton="false">
        <header>
          <div class="jarviswidget-ctrls">
            <a data-placement="bottom" title="" rel="tooltip" class="button-icon widget-lock-btn"
            href="javascript:void(0);" data-original-title="Lock"><i class="fa fa-unlock-alt"></i></a>
          </div>
          <h2 class="widgetTitle">.</h2>
        </header>
      <div><div class="widget-body ci-body"></div></div>
      </div>
      <div class="modal-panel">
        <div class="modal-panel-backdrop fade"></div>
        <div class="modal-panel-content slide"></div>
      </div>
    </div>
    
    <!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
    Note: These tiles are completely responsive,
    you can add as many as you like
    -->
    <div id="shortcut">
			<!-- <h2 class="text-center">MODULE PICKER</h2>  -->
			<button type="button" class="btn btn-raised btn-danger closeButton"><i class="fa fa-times"></i> Close</button>
      <ul></ul>
    </div>
    <!-- END SHORTCUT AREA -->

    <!--================================================== -->

    <!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
    <!-- 
    <script data-pace-options='{ "restartOnRequestAfter": true }' src="<%= request.getContextPath() %>/admin/js/plugin/pace/pace.min.js"></script>
     -->

	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script><script src="/admin/js/upgradeJquery.js"></script>

    <script src="<%= request.getContextPath() %>/admin/js/libs/jquery-ui-1.12.1.min.js"></script>

    <script src="<%= request.getContextPath() %>/webdirector/free/csrf-token/init"></script>
    <script src="<%= request.getContextPath() %>/admin/js/log4javascript/log4javascript.js"></script>
    <script src="<%= request.getContextPath() %>/admin/js/logging.js"></script>

    <!-- IMPORTANT: APP CONFIG -->
    <script src="<%= request.getContextPath() %>/admin/js/app.config.js"></script>

    <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
    <script src="<%= request.getContextPath() %>/admin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

    <!-- BOOTSTRAP JS -->
    <script src="<%= request.getContextPath() %>/admin/js/bootstrap/bootstrap.min.js"></script>

    <!-- MATERIAL DESIGN JS -->
    <script src="<%= request.getContextPath() %>/admin/js/material/material.js"></script>
    <script src="<%= request.getContextPath() %>/admin/js/material/ripples.js"></script>
    
    <!-- CUSTOM NOTIFICATION -->
    <script src="<%= request.getContextPath() %>/admin/js/notification/SmartNotification.js"></script>

    <!-- JARVIS WIDGETS -->
    <script src="<%= request.getContextPath() %>/admin/js/smartwidgets/jarvis.widget.js"></script>

    <!-- JQUERY VALIDATE -->
    <script src="<%= request.getContextPath() %>/admin/js/plugin/jquery-validate/jquery.validate.new.js"></script>

    <!-- JQUERY MASKED INPUT -->
    <script src="<%= request.getContextPath() %>/admin/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

    <!-- JQUERY SELECT2 INPUT -->
    <script src="<%= request.getContextPath() %>/admin/js/plugin/select2/select2.min.js"></script>

    <!-- JQUERY UI + Bootstrap Slider -->
    <script src="<%= request.getContextPath() %>/admin/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

    <!-- browser msie issue fix -->
    <script src="<%= request.getContextPath() %>/admin/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

    <!-- FastClick: For mobile devices -->
    <script src="<%= request.getContextPath() %>/admin/js/plugin/fastclick/fastclick.min.js"></script>

    <!--[if IE 8]>

    <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

    <![endif]-->
    
    <!-- Demo purpose only -->
    <script src="<%= request.getContextPath() %>/admin/js/wdLocalStorage.js"></script>
    <script src="<%= request.getContextPath() %>/admin/js/demo.js"></script>
    
    <script src="<%= request.getContextPath() %>/admin/js/error.js"></script>
    
    <!-- MAIN APP JS FILE -->
    <script src="<%= request.getContextPath() %>/admin/js/app.js"></script>
    <script src="<%= request.getContextPath() %>/admin/js/main.js"></script>
    <script src="<%= request.getContextPath() %>/admin/js/moduleTree.js"></script>
    <script src="<%= request.getContextPath() %>/admin/js/widgetHelper.js"></script>
    <script src="<%= request.getContextPath() %>/admin/js/widgetsDesktop.js"></script>
    <script src="<%= request.getContextPath() %>/admin/js/modalHelper.js"></script>
    <script src="<%= request.getContextPath() %>/admin/js/fullWidthHelper.js"></script>
    <script src="<%= request.getContextPath() %>/admin/js/treeContextMenu.js"></script>
    <script src="<%= request.getContextPath() %>/admin/js/settingsMenu.js"></script>
    <script src="<%= request.getContextPath() %>/admin/js/todoList.js"></script>

    <!-- Utils -->
    <script src="<%= request.getContextPath() %>/admin/js/utils.js"></script>
    <script src="<%= request.getContextPath() %>/admin/js/dataType.js"></script>
    <script src="<%= request.getContextPath() %>/admin/js/permissions.js"></script>

    <!-- Bulk edit functionality -->
    <script src="<%= request.getContextPath() %>/admin/js/bulkEditAjax.js"></script>

    <!-- Entity edit functionality -->
    <%-- <script src="<%= request.getContextPath() %>/admin/js/entityEditAjax.js"></script> --%>
    
    <!-- Dynamic Form Builder functionality -->
    <script src="<%= request.getContextPath() %>/admin/js/dynamicFormValidator.js"></script>
    <script src="<%= request.getContextPath() %>/admin/js/dynamicFormBuilder.js"></script>

    <!-- PAGE RELATED PLUGIN(S), New JS should be put beneath this -->
    

    <script src="<%= request.getContextPath() %>/admin/plugins/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="<%= request.getContextPath() %>/admin/plugins/bootstrap-datetimepicker/4.0.0/bootstrap-datetimepicker.js"></script>

    <script src="<%= request.getContextPath() %>/admin/plugins/jquery.form/3.51.0/jquery.form.min.js"></script>
    <script src="<%= request.getContextPath() %>/admin/plugins/jqueryui.multiselect/ui.multiselect.js"></script>
    
    <script src="<%= request.getContextPath() %>/admin/plugins/jquery.fancytree/js/jquery.fancytree.js"></script>
    <script src="<%= request.getContextPath() %>/admin/plugins/jquery.contextMenu/js/jquery.contextMenu.js"></script>

    <script src="<%= request.getContextPath() %>/admin/plugins/jquery.dataTables/js/jquery.dataTables.min.js"></script>
    <script src="<%= request.getContextPath() %>/admin/plugins/jquery.dataTables.bootstrap/js/dataTables.bootstrap.min.js"></script>
    <script src="<%= request.getContextPath() %>/admin/plugins/jquery.dataTables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
    <script src="<%= request.getContextPath() %>/admin/plugins/jquery.dataTable.ColResize/js/dataTables.colResize.js"></script>
    <script src="<%= request.getContextPath() %>/admin/plugins/jquery.dataTables/extensions/FixedHeader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<%= request.getContextPath() %>/admin/plugins/jquery.dataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>

    <script src="<%= request.getContextPath() %>/admin/plugins/spectrum/spectrum.js"></script>
    <script src="<%= request.getContextPath() %>/admin/plugins/jquery.slimScroll/jquery.slimscroll.min.js"></script>

    <script>
      UserDataHelper.userLevelId = <%=request.getSession().getAttribute("userLevelId")%>;
      $(document).ready(function(){
    	  $.material.init();
    	  $.material.ripples();
      })
    </script>
    <% if(!au.net.webdirector.common.sso.service.SingleSignOnHelper.isDisabled("webdirector")){ %>
    <script src="<%= au.net.webdirector.common.sso.service.SingleSignOnHelper.getAuthenticateUrl("webdirector", session) %>"></script>
    <% } %>
    
  </body>

</html>