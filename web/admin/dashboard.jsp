<!DOCTYPE html>
<%@page import="java.util.Date"%>
<%@page import="au.net.webdirector.common.utils.SimpleDateFormat"%>
<%@page import="au.net.webdirector.admin.security.LoginController"%>
<%
String prefixTitle = "["+request.getServerName() + request.getContextPath()+"] - ";
LoginController.setFirstScreen(request, response);
%>
<html lang="en-us">
	<head>
  <script>
  var context = '<%= request.getContextPath() %>';
  </script>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title><%=prefixTitle%> WebDirector 10 - Dashboard</title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/plugins/fontawesome-pro-5.8.1-web/css/all.css">

		<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
		<link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/css/smartadmin-production-plugins.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/css/smartadmin-skins.min.css">

		<!-- SmartAdmin RTL Support  -->
		<link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath() %>/admin/css/smartadmin-rtl.min.css">

		<!-- WebDirector Styles -->
		<%
		boolean useLess = false; 
		
		if (useLess) { %>
			<script src="<%= request.getContextPath() %>/admin/js/less/less.min.js" data-poll="1000" data-relative-urls="false"></script>
		<% } %>
		<link rel="stylesheet" href="<%= request.getContextPath() %>/admin/css/dashboard.css">
		
		
		<!-- Plugin CSS, New CSS should be put beneath this -->
		
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<%= request.getContextPath() %>/admin/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<%= request.getContextPath() %>/admin/img/favicon/favicon.ico" type="image/x-icon">
		
		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
		
		<!-- Specifying a Webpage Icon for Web Clip 
				Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
		<link rel="apple-touch-icon" href="<%= request.getContextPath() %>/admin/img/splash/sptouch-icon-iphone.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<%= request.getContextPath() %>/admin/img/splash/touch-icon-ipad.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<%= request.getContextPath() %>/admin/img/splash/touch-icon-iphone-retina.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<%= request.getContextPath() %>/admin/img/splash/touch-icon-ipad-retina.png">
		
		<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		
		<!-- Startup image for web apps -->
		<link rel="apple-touch-startup-image" href="<%= request.getContextPath() %>/admin/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
		<link rel="apple-touch-startup-image" href="<%= request.getContextPath() %>/admin/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
		<link rel="apple-touch-startup-image" href="<%= request.getContextPath() %>/admin/img/splash/iphone.png" media="screen and (max-device-width: 320px)">
        <style>
        .menu-on-top #main {
            margin-top: 0px !important;
        }
        </style>
	</head>
	
	<body class="fixed-header fixed-page-footer fixed-navigation" data-user="<%=session.getAttribute(LoginController.WD_USER) %>">

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
			  <span id="logo"> <img src="<%= request.getContextPath() %>/admin/img/custom/ci-logo.png" alt="Corporate Interactive "> </span>
			</div>

			<jsp:include page="include/i_navbar.jsp" />
			
			<div class="activity-container">
				<span id="activity" class="activity-dropdown"> <i class="fa fa-user"></i> <b class="badge">:(</b> </span>
              <div class="ajax-dropdown" style="display: none">
                <div class="content">
                  <!-- content goes here -->
                  <h5 class="todo-group-title"><i class="far fa-dot-circle"></i> Awaiting Approvals (<small class="pendingCount">1</small>)</h5>
                  <ul id="pending" class="todo ui-sortable">
                    <li>
                      <span class="handle"> 
                          <button type="button" class="btn btn-raised bg-color-magenta txt-color-white btn-xs openEditor" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="ID:13"><i class="far fa-dot-circle"></i></button>
                      </span>
                      <p>
                        <strong>Element #1</strong> - Product Catalog <span class="text-muted">Author: </span>
                      </p>
                    </li>
                  </ul>
                  <h5 class="todo-group-title"><i class="far fa-circle"></i> Drafts (<small class="draftCount">3</small>)</h5>
                  <ul id="draft" class="todo ui-sortable">
                  </ul>
              
                  <!-- end content -->
                </div>
                <span>Last Update: <span id="statusText"></span> 
                  <button type="button" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading..." class="btn btn-raised btn-xs btn-default pull-right">
                    <i class="fa fa-refresh"></i>
                  </button> 
                </span>
              </div>
			</div>

			
			<!-- pulled right: nav area -->
			<div class="pull-right">

				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<%= request.getContextPath() %>/webdirector" title="Sign Out" data-action="userLogout" data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i class="far fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->

				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="far fa-expand-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->

			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->

		<!-- MAIN PANEL -->
		<div id="body">
			<div id="main" role="main">
	
				<!-- RIBBON -->
				<div id="ribbon">
	
					<span class="ribbon-button-alignment"> 
						<span id="refresh" class="btn btn-raised btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
							<i class="fa fa-refresh"></i>
						</span> 
					</span>
	
						<span class="ribbon-text" rel="tooltip" data-toggle="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-info-circle'></i> <%=((Boolean)session.getAttribute(LoginController.WD_USER_ADMIN))?"Admin":"Author" %>" data-html="true">Logged in as <%=session.getAttribute(LoginController.WD_USER) %></span>
				</div>
				<!-- END RIBBON -->
	
				<!-- MAIN CONTENT -->
				<div id="content">
	
					<div class="row">
						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
							<h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-home"></i> Dashboard <span>> My Dashboard</span></h1>
						</div>
						<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
							<ul id="sparks" class="">
								<li class="sparks-info">
									<h5> My Income <span class="txt-color-blue">$47,171</span></h5>
									<div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
										1300, 1877, 2500, 2577, 2000, 2100, 3000, 2700, 3631, 2471, 2700, 3631, 2471
									</div>
								</li>
								<li class="sparks-info">
									<h5> Site Traffic <span class="txt-color-purple"><i class="fa fa-arrow-circle-up"></i>&nbsp;45%</span></h5>
									<div class="sparkline txt-color-purple hidden-mobile hidden-md hidden-sm">
										110,150,300,130,400,240,220,310,220,300, 270, 210
									</div>
								</li>
								<li class="sparks-info">
									<h5> Site Orders <span class="txt-color-greenDark"><i class="fa fa-shopping-cart"></i>&nbsp;2447</span></h5>
									<div class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm">
										110,150,300,130,400,240,220,310,220,300, 270, 210
									</div>
								</li>
							</ul>
						</div>
					</div>
					<!-- widget grid -->
					<section id="widget-grid" class="">
	
						<!-- row -->
						<div class="row">
							<article class="col-sm-12">
								<!-- new widget -->
								<div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false">
									<!-- widget options:
									usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
									data-widget-colorbutton="false"
									data-widget-editbutton="false"
									data-widget-togglebutton="false"
									data-widget-deletebutton="false"
									data-widget-fullscreenbutton="false"
									data-widget-custombutton="false"
									data-widget-collapsed="true"
									data-widget-sortable="false"
	
									-->
									<header>
										<span class="widget-icon"> <i class="glyphicon glyphicon-stats txt-color-darken"></i> </span>
										<h2>Live Feeds </h2>
	
										<ul class="nav nav-tabs pull-right in" id="myTab">
											<li class="active">
												<a data-toggle="tab" href="#s1"><i class="far fa-clock"></i> <span class="hidden-mobile hidden-tablet">Live Stats</span></a>
											</li>
	
											<li>
												<a data-toggle="tab" href="#s2"><i class="fab fa-facebook"></i> <span class="hidden-mobile hidden-tablet">Social Network</span></a>
											</li>
	
											<li>
												<a data-toggle="tab" href="#s3"><i class="fa fa-dollar-sign"></i> <span class="hidden-mobile hidden-tablet">Revenue</span></a>
											</li>
										</ul>
	
									</header>
	
									<!-- widget div-->
									<div class="no-padding">
										<!-- widget edit box -->
										<div class="jarviswidget-editbox">
	
											test
										</div>
										<!-- end widget edit box -->
	
										<div class="widget-body">
											<!-- content -->
											<div id="myTabContent" class="tab-content">
												<div class="tab-pane fade active in padding-10 no-padding-bottom" id="s1">
													<div class="row no-space">
														<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
															<span class="demo-liveupdate-1"> <span class="onoffswitch-title">Live switch</span> <span class="onoffswitch">
																	<input type="checkbox" name="start_interval" class="onoffswitch-checkbox" id="start_interval">
																	<label class="onoffswitch-label" for="start_interval"> 
																		<span class="onoffswitch-inner" data-swchon-text="ON" data-swchoff-text="OFF"></span> 
																		<span class="onoffswitch-switch"></span> </label> </span> </span>
															<div id="updating-chart" class="chart-large txt-color-blue"></div>
	
														</div>
														<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 show-stats">
	
															<div class="row">
																<div class="col-xs-6 col-sm-6 col-md-12 col-lg-12"> <span class="text"> My Tasks <span class="pull-right">130/200</span> </span>
																	<div class="progress">
																		<div class="progress-bar bg-color-blueDark" style="width: 65%;"></div>
																	</div> </div>
																<div class="col-xs-6 col-sm-6 col-md-12 col-lg-12"> <span class="text"> Transfered <span class="pull-right">440 GB</span> </span>
																	<div class="progress">
																		<div class="progress-bar bg-color-blue" style="width: 34%;"></div>
																	</div> </div>
																<div class="col-xs-6 col-sm-6 col-md-12 col-lg-12"> <span class="text"> Bugs Squashed<span class="pull-right">77%</span> </span>
																	<div class="progress">
																		<div class="progress-bar bg-color-blue" style="width: 77%;"></div>
																	</div> </div>
																<div class="col-xs-6 col-sm-6 col-md-12 col-lg-12"> <span class="text"> User Testing <span class="pull-right">7 Days</span> </span>
																	<div class="progress">
																		<div class="progress-bar bg-color-greenLight" style="width: 84%;"></div>
																	</div> </div>
	
																<span class="show-stat-buttons"> <span class="col-xs-12 col-sm-6 col-md-6 col-lg-6"> <a href="javascript:void(0);" class="btn btn-raised btn-default btn-block hidden-xs">Generate PDF</a> </span> <span class="col-xs-12 col-sm-6 col-md-6 col-lg-6"> <a href="javascript:void(0);" class="btn btn-raised btn-default btn-block hidden-xs">Report a bug</a> </span> </span>
	
															</div>
	
														</div>
													</div>
	
													<div class="show-stat-microcharts">
														<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
	
															<div class="easy-pie-chart txt-color-orangeDark" data-percent="33" data-pie-size="50">
																<span class="percent percent-sign">35</span>
															</div>
															<span class="easy-pie-title"> Server Load <i class="fa fa-caret-up icon-color-bad"></i> </span>
															<ul class="smaller-stat hidden-sm pull-right">
																<li>
																	<span class="label bg-color-greenLight"><i class="fa fa-caret-up"></i> 97%</span>
																</li>
																<li>
																	<span class="label bg-color-blueLight"><i class="fa fa-caret-down"></i> 44%</span>
																</li>
															</ul>
															<div class="sparkline txt-color-greenLight hidden-sm hidden-md pull-right" data-sparkline-type="line" data-sparkline-height="33px" data-sparkline-width="70px" data-fill-color="transparent">
																130, 187, 250, 257, 200, 210, 300, 270, 363, 247, 270, 363, 247
															</div>
														</div>
														<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
															<div class="easy-pie-chart txt-color-greenLight" data-percent="78.9" data-pie-size="50">
																<span class="percent percent-sign">78.9 </span>
															</div>
															<span class="easy-pie-title"> Disk Space <i class="fa fa-caret-down icon-color-good"></i></span>
															<ul class="smaller-stat hidden-sm pull-right">
																<li>
																	<span class="label bg-color-blueDark"><i class="fa fa-caret-up"></i> 76%</span>
																</li>
																<li>
																	<span class="label bg-color-blue"><i class="fa fa-caret-down"></i> 3%</span>
																</li>
															</ul>
															<div class="sparkline txt-color-blue hidden-sm hidden-md pull-right" data-sparkline-type="line" data-sparkline-height="33px" data-sparkline-width="70px" data-fill-color="transparent">
																257, 200, 210, 300, 270, 363, 130, 187, 250, 247, 270, 363, 247
															</div>
														</div>
														<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
															<div class="easy-pie-chart txt-color-blue" data-percent="23" data-pie-size="50">
																<span class="percent percent-sign">23 </span>
															</div>
															<span class="easy-pie-title"> Transfered <i class="fa fa-caret-up icon-color-good"></i></span>
															<ul class="smaller-stat hidden-sm pull-right">
																<li>
																	<span class="label bg-color-darken">10GB</span>
																</li>
																<li>
																	<span class="label bg-color-blueDark"><i class="fa fa-caret-up"></i> 10%</span>
																</li>
															</ul>
															<div class="sparkline txt-color-darken hidden-sm hidden-md pull-right" data-sparkline-type="line" data-sparkline-height="33px" data-sparkline-width="70px" data-fill-color="transparent">
																200, 210, 363, 247, 300, 270, 130, 187, 250, 257, 363, 247, 270
															</div>
														</div>
														<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
															<div class="easy-pie-chart txt-color-darken" data-percent="36" data-pie-size="50">
																<span class="percent degree-sign">36 <i class="fa fa-caret-up"></i></span>
															</div>
															<span class="easy-pie-title"> Temperature <i class="fa fa-caret-down icon-color-good"></i></span>
															<ul class="smaller-stat hidden-sm pull-right">
																<li>
																	<span class="label bg-color-red"><i class="fa fa-caret-up"></i> 124</span>
																</li>
																<li>
																	<span class="label bg-color-blue"><i class="fa fa-caret-down"></i> 40 F</span>
																</li>
															</ul>
															<div class="sparkline txt-color-red hidden-sm hidden-md pull-right" data-sparkline-type="line" data-sparkline-height="33px" data-sparkline-width="70px" data-fill-color="transparent">
																2700, 3631, 2471, 2700, 3631, 2471, 1300, 1877, 2500, 2577, 2000, 2100, 3000
															</div>
														</div>
													</div>
	
												</div>
												<!-- end s1 tab pane -->
	
												<div class="tab-pane fade" id="s2">
													<div class="widget-body-toolbar bg-color-white">
	
														<form class="form-inline" role="form">
	
															<div class="form-group">
																<label class="sr-only" for="s123">Show From</label>
																<input type="email" class="form-control input-sm" id="s123" placeholder="Show From">
															</div>
															<div class="form-group">
																<input type="email" class="form-control input-sm" id="s124" placeholder="To">
															</div>
	
															<div class="btn-group hidden-phone pull-right">
																<a class="btn btn-raised dropdown-toggle btn-xs btn-default" data-toggle="dropdown"><i class="fa fa-cog"></i> More <span class="caret"> </span> </a>
																<ul class="dropdown-menu pull-right">
																	<li>
																		<a href="javascript:void(0);"><i class="fa fa-file-text-alt"></i> Export to PDF</a>
																	</li>
																	<li>
																		<a href="javascript:void(0);"><i class="fa fa-question-sign"></i> Help</a>
																	</li>
																</ul>
															</div>
	
														</form>
	
													</div>
													<div class="padding-10">
														<div id="statsChart" class="chart-large has-legend-unique"></div>
													</div>
	
												</div>
												<!-- end s2 tab pane -->
	
												<div class="tab-pane fade" id="s3">
	
													<div class="widget-body-toolbar bg-color-white smart-form" id="rev-toggles">
	
														<div class="inline-group">
	
															<label for="gra-0" class="checkbox">
																<input type="checkbox" name="gra-0" id="gra-0" checked="checked">
																<i></i> Target </label>
															<label for="gra-1" class="checkbox">
																<input type="checkbox" name="gra-1" id="gra-1" checked="checked">
																<i></i> Actual </label>
															<label for="gra-2" class="checkbox">
																<input type="checkbox" name="gra-2" id="gra-2" checked="checked">
																<i></i> Signups </label>
														</div>
	
														<div class="btn-group hidden-phone pull-right">
															<a class="btn btn-raised dropdown-toggle btn-xs btn-default" data-toggle="dropdown"><i class="fa fa-cog"></i> More <span class="caret"> </span> </a>
															<ul class="dropdown-menu pull-right">
																<li>
																	<a href="javascript:void(0);"><i class="fa fa-file-text-alt"></i> Export to PDF</a>
																</li>
																<li>
																	<a href="javascript:void(0);"><i class="fa fa-question-sign"></i> Help</a>
																</li>
															</ul>
														</div>
	
													</div>
	
													<div class="padding-10">
														<div id="flotcontainer" class="chart-large has-legend-unique"></div>
													</div>
												</div>
												<!-- end s3 tab pane -->
											</div>
	
											<!-- end content -->
										</div>
	
									</div>
									<!-- end widget div -->
								</div>
								<!-- end widget -->
	
								<!-- new widget -->
								<div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false" data-widget-colorbutton="false">
	
									<!-- widget options:
									usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
	
									data-widget-colorbutton="false"
									data-widget-editbutton="false"
									data-widget-togglebutton="false"
									data-widget-deletebutton="false"
									data-widget-fullscreenbutton="false"
									data-widget-custombutton="false"
									data-widget-collapsed="true"
									data-widget-sortable="false"
	
									-->
	
									<header>
										<span class="widget-icon"> <i class="fa fa-check txt-color-white"></i> </span>
										<h2> ToDo's </h2>
										<!-- <div class="widget-toolbar">
										add: non-hidden - to disable auto hide
	
										</div>-->
									</header>
	
									<!-- widget div-->
									<div>
										<!-- widget edit box -->
										<div class="jarviswidget-editbox">
											<div>
												<label>Title:</label>
												<input type="text" />
											</div>
										</div>
										<!-- end widget edit box -->
	
										<div class="widget-body no-padding smart-form">
											<!-- content goes here -->
											<h5 class="todo-group-title"><i class="fa fa-exclamation-triangle"></i> Critical Tasks (<small class="num-of-tasks">1</small>)</h5>
											<ul id="sortable1" class="todo">
												<li>
													<span class="handle"> <label class="checkbox">
															<input type="checkbox" name="checkbox-inline">
															<i></i> </label> </span>
													<p>
														<strong>Ticket #17643</strong> - Hotfix for WebApp interface issue [<a href="javascript:void(0);" class="font-xs">More Details</a>] <span class="text-muted">Sea deep blessed bearing under darkness from God air living isn't. </span>
														<span class="date">Jan 1, 2014</span>
													</p>
												</li>
											</ul>
											<h5 class="todo-group-title"><i class="fa fa-exclamation"></i> Important Tasks (<small class="num-of-tasks">3</small>)</h5>
											<ul id="sortable2" class="todo">
												<li>
													<span class="handle"> <label class="checkbox">
															<input type="checkbox" name="checkbox-inline">
															<i></i> </label> </span>
													<p>
														<strong>Ticket #1347</strong> - Inbox email is being sent twice <small>(bug fix)</small> [<a href="javascript:void(0);" class="font-xs">More Details</a>] <span class="date">Nov 22, 2013</span>
													</p>
												</li>
												<li>
													<span class="handle"> <label class="checkbox">
															<input type="checkbox" name="checkbox-inline">
															<i></i> </label> </span>
													<p>
														<strong>Ticket #1314</strong> - Call customer support re: Issue <a href="javascript:void(0);" class="font-xs">#6134</a><small>(code review)</small>
														<span class="date">Nov 22, 2013</span>
													</p>
												</li>
												<li>
													<span class="handle"> <label class="checkbox">
															<input type="checkbox" name="checkbox-inline">
															<i></i> </label> </span>
													<p>
														<strong>Ticket #17643</strong> - Hotfix for WebApp interface issue [<a href="javascript:void(0);" class="font-xs">More Details</a>] <span class="text-muted">Sea deep blessed bearing under darkness from God air living isn't. </span>
														<span class="date">Jan 1, 2014</span>
													</p>
												</li>
											</ul>
	
											<h5 class="todo-group-title"><i class="fa fa-check"></i> Completed Tasks (<small class="num-of-tasks">1</small>)</h5>
											<ul id="sortable3" class="todo">
												<li class="complete">
													<span class="handle" style="display:none"> <label class="checkbox state-disabled">
															<input type="checkbox" name="checkbox-inline" checked="checked" disabled="disabled">
															<i></i> </label> </span>
													<p>
														<strong>Ticket #17643</strong> - Hotfix for WebApp interface issue [<a href="javascript:void(0);" class="font-xs">More Details</a>] <span class="text-muted">Sea deep blessed bearing under darkness from God air living isn't. </span>
														<span class="date">Jan 1, 2014</span>
													</p>
												</li>
											</ul>
	
											<!-- end content -->
										</div>
	
									</div>
									<!-- end widget div -->
								</div>
								<!-- end widget -->
	
                <!-- new widget -->
                <div class="jarviswidget jarviswidget-color-yellow" id="wid-id-2" data-widget-editbutton="false" data-widget-colorbutton="false">
  
                  <!-- widget options:
                  usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
  
                  data-widget-colorbutton="false"
                  data-widget-editbutton="false"
                  data-widget-togglebutton="false"
                  data-widget-deletebutton="false"
                  data-widget-fullscreenbutton="false"
                  data-widget-custombutton="false"
                  data-widget-collapsed="true"
                  data-widget-sortable="false"
  
                  -->
  
                  <header>
                    <span class="widget-icon"> <i class="fa fa-link txt-color-white"></i> </span>
                    <h2> Job Scheduler </h2>
                    <ul class="nav nav-tabs pull-right">
                      <li class="active">
                        <a data-toggle="tab" href="#newJob">New Job</a>
                      </li>
                      <li>
                        <a data-toggle="tab" href="#jobHistory">Job History</a>
                      </li>
                      <li>
                        <a data-toggle="tab" href="#jobSchedule">Job Schedule</a>
                      </li>
                    </ul>
                  </header>
  
                  <!-- widget div-->
                  <div>
                    <div class="tab-content">
                      <div class="tab-pane fade active in" id="newJob">
                        <iframe src="<%= request.getContextPath() %>/webdirector/secure/scheduler/jobSchedule" id="jobScheduleFrame" border=0 frameborder=0>Your browser doesn't support Iframes please upgrade it.</iframe>
                      </div>
                      <div class="tab-pane fade" id="jobHistory">
                        <iframe src="<%= request.getContextPath() %>/webdirector/secure/scheduler/jobHistory?jobName=" id="aFrmId" border=0 frameborder=0>Your browser doesn't support Iframes please upgrade it.</iframe>
                      </div>
                      <div class="tab-pane fade" id="jobSchedule">
                        <iframe src="<%= request.getContextPath() %>/webdirector/secure/scheduler/jobList" id="aFrmId" width="750px" border=0 frameborder=0>Your browser doesn't support Iframes please upgrade it.</iframe>
                      </div>
                    </div>
                  </div>
                  <!-- end widget div -->
                </div>
                <!-- end widget -->
  
              </article>
  
            </div>
  
            <!-- end row -->
	
					</section>
					<!-- end widget grid -->
	
				</div>
				<!-- END MAIN CONTENT -->
				<div class="modal fade" id="modal-fullwidth" tabindex="-2">
		      		<div class="modal-dialog">
		      			<div class="modal-content" id="fullWidthContent"></div>
		      		</div>
		      	</div>
			</div>
			<!-- END MAIN PANEL -->
		</div>

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
          <span class="txt-color-white">WebDirector 10<span class="hidden-xs"> - Data Management</span> &copy; 2002-<%= new SimpleDateFormat("yyyy").format(new Date()) %></span>
				</div>

				<div class="col-xs-6 col-sm-6 text-right hidden-xs">
					<div class="txt-color-white inline-block">
						<i class="txt-color-blueLight hidden-mobile">You will be logged out at <i class="fa fa-clock-o fa-spin"></i> <strong class="autologoutTime"></strong> </i>
						<div class="btn-group dropup">
							<button class="btn btn-raised btn-xs dropdown-toggle bg-color-blue txt-color-white" data-toggle="dropdown">
								<i class="fa fa-link"></i> <span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left">
								<li>
									<div class="padding-5">
										<p class="txt-color-darken font-sm no-margin">Download Progress</p>
										<div class="progress progress-micro no-margin">
											<div class="progress-bar progress-bar-success" style="width: 50%;"></div>
										</div>
									</div>
								</li>
								<li class="divider"></li>
								<li>
									<div class="padding-5">
										<p class="txt-color-darken font-sm no-margin">Server Load</p>
										<div class="progress progress-micro no-margin">
											<div class="progress-bar progress-bar-success" style="width: 20%;"></div>
										</div>
									</div>
								</li>
								<li class="divider"></li>
								<li>
									<div class="padding-5">
										<p class="txt-color-darken font-sm no-margin">Memory Load <span class="text-danger">*critical*</span></p>
										<div class="progress progress-micro no-margin">
											<div class="progress-bar progress-bar-danger" style="width: 70%;"></div>
										</div>
									</div>
								</li>
								<li class="divider"></li>
								<li>
									<div class="padding-5">
										<button class="btn btn-raised btn-block btn-default">refresh</button>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
		Note: These tiles are completely responsive,
		you can add as many as you like
		-->
		<div id="shortcut" class="wtf">
			<ul>
			</ul>
		</div>
		<!-- END SHORTCUT AREA -->

		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
    <!-- 
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<%= request.getContextPath() %>/admin/js/plugin/pace/pace.min.js"></script>
     -->

		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script><script src="/admin/js/upgradeJquery.js"></script>
		<script src="/admin/js/upgradeJquery.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<%= request.getContextPath() %>/admin/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

        <script src="<%= request.getContextPath() %>/webdirector/free/csrf-token/init"></script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<%= request.getContextPath() %>/admin/js/app.config.js"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<%= request.getContextPath() %>/admin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

		<!-- BOOTSTRAP JS -->
		<script src="<%= request.getContextPath() %>/admin/js/bootstrap/bootstrap.min.js"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="<%= request.getContextPath() %>/admin/js/notification/SmartNotification.js"></script>

		<!-- JARVIS WIDGETS -->
		<script src="<%= request.getContextPath() %>/admin/js/smartwidgets/jarvis.widget.min.js"></script>

		<!-- EASY PIE CHARTS -->
		<script src="<%= request.getContextPath() %>/admin/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

		<!-- SPARKLINES -->
		<script src="<%= request.getContextPath() %>/admin/js/plugin/sparkline/jquery.sparkline.min.js"></script>

		<!-- JQUERY VALIDATE -->
		<script src="<%= request.getContextPath() %>/admin/js/plugin/jquery-validate/jquery.validate.min.js"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="<%= request.getContextPath() %>/admin/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="<%= request.getContextPath() %>/admin/js/plugin/select2/select2.min.js"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<%= request.getContextPath() %>/admin/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

		<!-- browser msie issue fix -->
		<script src="<%= request.getContextPath() %>/admin/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

		<!-- FastClick: For mobile devices -->
		<script src="<%= request.getContextPath() %>/admin/js/plugin/fastclick/fastclick.min.js"></script>

		<!--[if IE 8]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->
        <script src="<%= request.getContextPath() %>/admin/js/wdLocalStorage.js"></script>
    	<script src="<%= request.getContextPath() %>/admin/js/error.js"></script>

		<!-- MAIN APP JS FILE -->
		<script src="<%= request.getContextPath() %>/admin/js/app.js"></script>
        <script src="<%= request.getContextPath() %>/admin/js/widgetsDesktop.js"></script>
        <script src="<%= request.getContextPath() %>/admin/plugins/moment.js/2.9.0/moment-with-locales.js"></script>
        <script src="<%= request.getContextPath() %>/admin/js/todoList.js"></script>

		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<%= request.getContextPath() %>/admin/js/speech/voicecommand.min.js"></script>

		<!-- SmartChat UI : plugin -->
		<script src="<%= request.getContextPath() %>/admin/js/smart-chat-ui/smart.chat.ui.min.js"></script>
		<script src="<%= request.getContextPath() %>/admin/js/smart-chat-ui/smart.chat.manager.min.js"></script>
		
		<!-- PAGE RELATED PLUGIN(S) -->
		
		<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
		<script src="<%= request.getContextPath() %>/admin/js/plugin/flot/jquery.flot.cust.min.js"></script>
		<script src="<%= request.getContextPath() %>/admin/js/plugin/flot/jquery.flot.resize.min.js"></script>
		<script src="<%= request.getContextPath() %>/admin/js/plugin/flot/jquery.flot.time.min.js"></script>
		<script src="<%= request.getContextPath() %>/admin/js/plugin/flot/jquery.flot.tooltip.min.js"></script>
		
		<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
		<script src="<%= request.getContextPath() %>/admin/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
		<script src="<%= request.getContextPath() %>/admin/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
		
		<!-- Full Calendar -->
		<script src="<%= request.getContextPath() %>/admin/js/plugin/moment/moment.min.js"></script>
		<script src="<%= request.getContextPath() %>/admin/js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>

		<script src="<%= request.getContextPath() %>/admin/js/fullWidthHelper.js"></script>

		<script src="<%= request.getContextPath() %>/admin/js/dashboard.js"></script>
		
		<script type="text/javascript">
			$.ajax("<%= request.getContextPath() %>/webdirector/module/assignedModules")
		</script>
        <% if(!au.net.webdirector.common.sso.service.SingleSignOnHelper.isDisabled("webdirector")){ %>
        <script src="<%= au.net.webdirector.common.sso.service.SingleSignOnHelper.getAuthenticateUrl("webdirector", session) %>"></script>
        <% } %>

	</body>

</html>