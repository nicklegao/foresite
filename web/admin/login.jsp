<!DOCTYPE html>
<html lang="en-us" id="extr-page">
<head>
<script>
	if (window != top)
	{
		top.document.location.href = '/webdirector';
	}
</script>
<meta charset="utf-8">
<title>SmartAdmin</title>
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- #CSS Links -->
<!-- Basic Styles -->
<link rel="stylesheet" type="text/css" media="screen"
	href="/admin/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="/admin/css/font-awesome.min.css">

<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
<link rel="stylesheet" type="text/css" media="screen"
	href="/admin/css/smartadmin-production-plugins.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="/admin/css/smartadmin-production.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="/admin/css/smartadmin-skins.min.css">

<!-- SmartAdmin RTL Support -->
<link rel="stylesheet" type="text/css" media="screen"
	href="/admin/css/smartadmin-rtl.min.css">

<!-- WebDirector Styles -->
<link rel="stylesheet" type="text/css" media="screen"
	href="/admin/css/webdirector.css">

<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
<link rel="stylesheet" type="text/css" media="screen"
	href="/admin/css/demo.min.css">

<!-- #FAVICONS -->
<link rel="shortcut icon" href="/admin/img/favicon/favicon.ico"
	type="image/x-icon">
<link rel="icon" href="/admin/img/favicon/favicon.ico"
	type="image/x-icon">

<!-- #GOOGLE FONT -->
<link rel="stylesheet"
	href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

<!-- #APP SCREEN / ICONS -->
<!-- Specifying a Webpage Icon for Web Clip 
       Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
<link rel="apple-touch-icon"
	href="/admin/img/splash/sptouch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76"
	href="/admin/img/splash/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120"
	href="/admin/img/splash/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152"
	href="/admin/img/splash/touch-icon-ipad-retina.png">

<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<!-- Startup image for web apps -->
<link rel="apple-touch-startup-image"
	href="/admin/img/splash/ipad-landscape.png"
	media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
<link rel="apple-touch-startup-image"
	href="/admin/img/splash/ipad-portrait.png"
	media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
<link rel="apple-touch-startup-image"
	href="/admin/img/splash/iphone.png"
	media="screen and (max-device-width: 320px)">

</head>

<body class="animated fadeInDown">

	<header id="header">

		<div id="logo-group">
			<span id="logo"> <img src="/admin/img/custom/logo.png"
				alt="Corporate Interactive">
			</span>
		</div>

	</header>

	<div id="main" role="main">

		<!-- MAIN CONTENT -->
		<div id="content" class="container">

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-4 col-centered">

					<div class="well no-padding">
						<div id="login">
							<form id="login-form" class="smart-form client-form">

								<header> Sign In </header>

								<fieldset class="needUserDetails">
									<div role="alert"
										class="alert alert-danger alert-dismissible fade in"
										id="errorMsgBox" style="display: none;">
										<h4>Error</h4>
										<p class="msg-content"></p>
									</div>
									<div role="alert"
										class="alert alert-success alert-dismissible fade in"
										id="successMsgBox" style="display: none;">
										<h4>Success</h4>
										<p class="msg-content"></p>
									</div>

									<section>
										<label class="label">Username</label> <label class="input">
											<i class="icon-append fa fa-user"></i> <input
											autocorrect="off" autocapitalize="off" type="text"
											name="username" id="usernameInput"> <b
											class="tooltip tooltip-top-right"><i
												class="fa fa-user txt-color-teal"></i> Please enter username</b>
										</label>
									</section>

									<section>
										<label class="label">Password</label> <label class="input">
											<i class="icon-append fa fa-lock"></i> <input type="password"
											name="password"> <b class="tooltip tooltip-top-right"><i
												class="fa fa-lock txt-color-teal"></i> Enter your password</b>
										</label>
										<div class="note">
											<a href="/webdirector/forgot-password" id="forgot-passwd">Forgot
												password?</a>
										</div>
									</section>

								</fieldset>
								<fieldset class="serverLoading" style="display: none;">
									<h2 class="text-center">
										<i class="fa fa-clock-o fa-3x fa-spin"></i>
									</h2>
									<p class="text-center">
										Logging in: <strong class="username text-danger">...</strong>
									</p>
								</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary needUserDetails">
										Sign in</button>
								</footer>
							</form>
						</div>
						<div id="verify" style="display: none;">
							<form id="verification-form" class="smart-form client-form">
								<header> Verify </header>

								<fieldset class="needVerificationDetails">
									<div role="alert"
										class="alert alert-danger alert-dismissible fade in"
										id="verificationErrorMsgBox" style="display: none;">
										<h4>Error</h4>
										<p class="msg-content"></p>
									</div>
									<div role="alert"
										class="alert alert-success alert-dismissible fade in"
										id="successMsgBox" style="display: none;">
										<h4>Success</h4>
										<p class="msg-content"></p>
									</div>

									<input type="hidden" id="verificationUsername" name="username" />
									
									<section>
										<label class="label">Please check your email for verification code</label> <label class="input">
											<i class="icon-append fa fa-lock"></i> <input placeholder="Verification Code"
											autocorrect="off" autocapitalize="off" type="text"
											name="verificationCode" id="verificationCode"> <b
											class="tooltip tooltip-top-right"><i
												class="fa fa-lock txt-color-teal"></i> Please enter verification code</b>
										</label>
									</section>

								</fieldset>
								<fieldset class="serverVerificationLoading" style="display: none;">
									<h2 class="text-center">
										<i class="fa fa-clock-o fa-3x fa-spin"></i>
									</h2>
									<p class="text-center">
										Verifying: <strong class="username text-danger">...</strong>
									</p>
								</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary needVerificationDetails">
										Verify</button>
								</footer>
							</form>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>

	<!--================================================== -->

	<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
	<script src="/admin/js/plugin/pace/pace.min.js"></script>

	<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script><script src="/admin/js/upgradeJquery.js"></script>

	<script
		src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script>
		if (!window.jQuery.ui)
		{
			document.write('<script src="/admin/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
		}
	</script>

	<script src="/webdirector/free/csrf-token/init"></script>
	<!-- IMPORTANT: APP CONFIG -->
	<script src="/admin/js/app.config.js"></script>

	<!-- JS TOUCH : include this plugin for mobile drag / drop touch events     
    <script src="/admin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

	<!-- BOOTSTRAP JS -->
	<script src="/admin/js/bootstrap/bootstrap.min.js"></script>

	<!-- JQUERY VALIDATE -->
	<script src="/admin/js/plugin/jquery-validate/jquery.validate.min.js"></script>

	<!-- JQUERY MASKED INPUT -->
	<script src="/admin/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

	<!--[if IE 8]>
      
      <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
      
    <![endif]-->

	<script src="/admin/js/error.js"></script>

	<!-- MAIN APP JS FILE -->
	<script src="/admin/js/app.js"></script>

	<script type="text/javascript">
		runAllForms();

		$(function()
		{
			console.log('login');
			// Validation
			$("#login-form").validate({
				// Rules for form validation
				rules : {
					username : {
						required : true,
					},
					password : {
						required : true,
						minlength : 5,
						maxlength : 30
					}
				},

				// Messages for form validation
				messages : {
					username : {
						required : 'Please enter your username',
					},
					password : {
						required : 'Please enter your password'
					}
				},

				// Do not change code below
				errorPlacement : function(error, element)
				{
					error.insertAfter(element.parent());
				}
			});
			$("#verification-form").validate({
				// Rules for form validation
				rules : {
					verificationCode : {
						required : true,
					}
				},

				// Messages for form validation
				messages : {
					verificationCode : {
						required : 'Please enter verificationCode',
					}
				},

				errorPlacement : function(error, element)
				{
					error.insertAfter(element.parent());
				}
			});
		});

		$('#login-form').on('submit', function(e)
		{
			e.preventDefault();
			var $errorMsgBox = $('#errorMsgBox');
			var $successMsgBox = $('#successMsgBox');
			$errorMsgBox.hide();
			$successMsgBox.hide();
			if (!$("#login-form").valid())
			{
				return;
			}
			var formData = $(this).serialize();
			$('form input, form footer .btn').prop('disabled', true);

			$(".needUserDetails").hide();
			$(".serverLoading .username").text($("#usernameInput").val());
			$(".serverLoading").show();

			$.ajax({
				url : '/webdirector/free/login',
				data : formData,
				type : 'post'
			}).done(function(data)
			{
				if (data.success === true)
				{
					if(data.message != 'Credentials valid.'){
						window.location.href = data.message;
					}else{
						$('#login-form').hide();
						$('#verify').show();
						$('#verificationUsername').val($('#usernameInput').val())
					}
				}
				else
				{
					$('form .state-success').removeClass('state-success').addClass('state-error');
					$errorMsgBox.find('.msg-content').text(data.message).end().fadeIn();
					$(".needUserDetails").slideDown();
					$(".serverLoading").slideUp();
					$('#unlock-account').remove();
					if (data.message.indexOf(' locked') != -1)
					{
						var $unlock = $('<a href="#" id="unlock-account">Unlock my account</a>').css({
							display : 'block',
							float : 'right'
						}).click(function(e)
						{
							e.preventDefault();
							$errorMsgBox.hide();
							$successMsgBox.hide();
							$('#unlock-account').remove();
							$.ajax({
								url : '/webdirector/free/unlock-account',
								data : formData,
								type : 'post'
							}).done(function(data)
							{
								if (data.success === true)
								{
									$('form .state-error').removeClass('state-error').addClass('state-success');
									$successMsgBox.find('.msg-content').text(data.message).end().fadeIn();
								}
								else
								{
									$('form .state-success').removeClass('state-success').addClass('state-error');
									$errorMsgBox.find('.msg-content').text(data.message).end().fadeIn();
								}
							}).fail(function(jqXHR, textStatus, e)
							{
								console.log(e);
							}).always(function()
							{
								$('form input, form footer .btn').prop('disabled', false);
							});
						});
						$('#forgot-passwd').after($unlock);
					}
				}
			}).fail(function(jqXHR, textStatus, e)
			{
				console.log(e);

				$('form .state-success').removeClass('state-success').addClass('state-error');
				$errorMsgBox.find('.msg-content').text("Unable to sign you in. Please try again later.").end().fadeIn();
				$(".needUserDetails").slideDown();
				$(".serverLoading").slideUp();

			}).always(function()
			{
				$('form input, form footer .btn').prop('disabled', false);
			});
		});
		
		$('#verification-form').on('submit', function(e)
				{
					e.preventDefault();
					var $errorMsgBox = $('#verificationErrorMsgBox');
					var $successMsgBox = $('#successMsgBox');
					$errorMsgBox.hide();
					$successMsgBox.hide();
					if (!$("#verification-form").valid())
					{
						return;
					}
					var formData = $(this).serialize();
					$('form input, form footer .btn').prop('disabled', true);

					$(".needVerificationDetails").hide();
					$(".serverVerificationLoading .username").text($("#usernameInput").val());
					$(".serverVerificationLoading").show();

					$.ajax({
						url : '/webdirector/free/verifyLogin',
						data : formData,
						type : 'post'
					}).done(function(data)
					{
						if (data.success === true)
						{
							window.location.href = data.message;
						}
						else
						{
							$('form .state-success').removeClass('state-success').addClass('state-error');
							$errorMsgBox.find('.msg-content').text(data.message).end().fadeIn();
							$(".needVerificationDetails").slideDown();
							$(".serverVerificationLoading").slideUp();
						}
					}).fail(function(jqXHR, textStatus, e)
					{
						console.log(e);

						$('form .state-success').removeClass('state-success').addClass('state-error');
						$errorMsgBox.find('.msg-content').text("Unable to verify. Please try again later.").end().fadeIn();
						$(".needVerificationDetails").slideDown();
						$(".serverVerificationLoading").slideUp();

					}).always(function()
					{
						$('form input, form footer .btn').prop('disabled', false);
					});
				});
	</script>
	<script src="/admin/js/plugin/moment/moment.min.js"></script>
	<%
		if (!au.net.webdirector.common.sso.service.SingleSignOnHelper.isDisabled("webdirector"))
		{
	%>
	<script
		src="<%=au.net.webdirector.common.sso.service.SingleSignOnHelper.getAuthenticateUrl("webdirector", session)%>"></script>
	<%
		}
	%>
</body>
</html>