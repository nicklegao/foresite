<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<!--[if lt IE 9]><html class="bad-ie" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<html lang="en">
<!--<![endif]-->
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
    <head>
        <title>TravelDocs - Login Verification</title>
        <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />

        <link rel="stylesheet" href="/application/assets/plugins/bootstrap-qcloud/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/font-awesome.min.css" />

        <link rel="stylesheet" href="/application/assets/plugins/qcloud-iconfont/style.css" />

        <link rel="stylesheet" href="/application/assets/plugins/sweetalert/sweetalert.css" />

        <link rel="stylesheet" href="/application/assets/fonts/style.css" />

        <link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/css/login.css", request) %>" type="text/css"/>
        <!--[if IE 7]>
        <link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
        <![endif]-->
    </head>
    <body class="login">
    <div class="main-login col-md-6 col-md-offset-3 col-xs-8 col-xs-offset-2">

        <!-- start: COPYRIGHT -->
        <div class="copyright">
            <!-- &copy; QuoteCloud  -->
        </div>
        <!-- end: COPYRIGHT -->
    </div>

    <!--[if lt IE 9]>
    <script src="/application/assets/plugins/respond.js"></script>
    <![endif]-->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script><script src="/admin/js/upgradeJquery.js"></script>
    <script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="/application/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
    <script src="/application/assets/plugins/bootstrap-qcloud/js/bootstrap.min.js"></script>
    <script src="/application/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <script>
		$(document).ready(function(){
			if($(window).width() < 768) {
				swal({
					title: "Veritication email sent!",
					text: "A verification email has been sent to the user your trying to log into, when they approve your access you will receive a verification code via email.",
					type: "success",
					showConfirmButton: false
				});
			} else {
				swal({
					title: "Veritication email sent!",
					text: "A verification email has been sent to the user your trying to log into, when they approve your access you will receive a verification code via email.",
					type: "success",
					showConfirmButton: false
				});
			}
		});
    </script>
    </body>
</compress:html>
</html>