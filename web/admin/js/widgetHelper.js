$(document).ready(function(){
  $(document).on("click", ".jarviswidget-delete-btn", function(){
    WidgetHelper.saveWidgetState();
  });
  
  $("#widget-grid .row article").on("sortupdate", function(){
    WidgetHelper.saveWidgetState();
  });
  
  
});

var WidgetHelper = {
    widgetIdForElement : function(module, elementId)
    {
      return module + "_element_" + elementId;
    },

    widgetIdForCategory : function(module, categoryId)
    {
      return module + "_category_" + categoryId;
    },

    widgetIdForElementList : function(module, parentCategoryId, search)
    {
      var searchSuffix = (search != undefined && search.length>0)? "-"+search.replace(/[^a-zA-Z0-9]/g, "") : "";
      return module + "_element_list_" + parentCategoryId + searchSuffix;
    },

    widgetIdForCategoryList : function(module, parentCategoryId, search, show)
    {
      var searchSuffix = (search != undefined && search.length>0)? "-"+search.replace(/[^a-zA-Z0-9]/g, "") : "";
      var showSuffix = (show != undefined && show.length>0)? "-"+show : "";
      return module + "_category_list_" + parentCategoryId + showSuffix + searchSuffix;
    },
    
    saveWidgetState : function()
    {
      //save in 0.5 seconds... allow for multiple calls to this functions in the meantime.
      if (WidgetHelper.delaySave != undefined)
      {
        clearTimeout(WidgetHelper.delaySave);
      }
      WidgetHelper.delaySave = setTimeout(function(){
        console.log("Saving widget state");
        var $allWidgets = $("#widget-grid").find("article .jarviswidget");
        var saveState = [];
        $allWidgets.each(function(i, e){
          var $widget = $(e);
          var requestData = $widget.data("requestData");
          if (requestData != undefined)
          {
            var locked = WidgetHelper.widgetIsLocked($widget);
            var restoreParams = JSON.parse(requestData);
            
            var saveInfo = {
                state: {
                  locked: locked
                },
                restoreParams: restoreParams
              };
            saveState.push(saveInfo);
          }
          else
          {
            console.log("Unable to save widget. Unsupported type: "+type);
          }
          
        });
        
        console.log("Saved "+saveState.length+" widgets.");
        wdLocalStorage.setItem("savedWidgetState", JSON.stringify(saveState));
      }, 500);
    },
    
    restoreWidgetState : function()
    {
      var restoreFunctions = {
          "element" : WidgetHelper.addWidgetForModuleElementId,
          "category" : WidgetHelper.addWidgetForModuleCategoryId,
          "elementList" : WidgetHelper.addWidgetForBulkEditElements,
          "categoryList" : WidgetHelper.addWidgetForBulkEditCategories
      }
      //TODO: Clear out all existing widgets.
      var savedWidgetState = JSON.parse(wdLocalStorage.getItem("savedWidgetState"));
      if (savedWidgetState != undefined)
      {
        console.log("Restoring "+savedWidgetState.length+" widgets.");
        var toUnlock = [];
        // as we always add widget on the top, and we save widget from top to bottom, we need to restore reversely
        $.each(savedWidgetState.reverse(), function(i, saveInfo){
          var restoreParams = saveInfo.restoreParams;
          
          if (restoreFunctions[restoreParams.widgetType] != undefined)
          {
            var state = saveInfo.state;
            
            var $widget = restoreFunctions[restoreParams.widgetType](restoreParams);
            
            if ($widget!=undefined)
            {
              WidgetHelper.lockWidget($widget);
              //FIXME: not sure why this doesnt always work
              if (state.locked == false)
              {
                toUnlock.push($widget);
              }
            }
          }
        });
        $.each(toUnlock, function(i, $widget){
          WidgetHelper.unlockWidget($widget);
        });
      }
    },

    widgetIsLocked : function($widget)
    {
      return !$widget.hasClass("widget-unlocked");
    },

    unlockWidget : function($widget)
    {
      var $icon = $(".widget-lock-btn i", $widget);
      $("#widget-grid").find(".widget-unlocked").removeClass("widget-unlocked")
      .find(".widget-lock-btn").find("i").removeClass("fa-unlock-alt").addClass("fa-lock");
      $widget.addClass("widget-unlocked");
      $icon.removeClass("fa-lock").addClass("fa-unlock-alt");
    },

    lockWidget : function($widget)
    {
      var $icon = $(".widget-lock-btn i", $widget);
      $widget.removeClass("widget-unlocked");
      $icon.removeClass("fa-unlock-alt").addClass("fa-lock");
    },

    addNewActiveWidget : function()
    {
      var $widget = $('.htmlFragments .stdWidgetContainer').clone();
      $widget.addClass('bounceInLeft animated');
      $("#widget-grid").find("article").prepend($widget);
      $widget.find(".widget-lock-btn").click(toggleWidgetLock);
      $widget.fadeIn(100);
      $widget.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass('bounceInLeft animated');
      });

      $('#widget-grid').jarvisWidgets("destroy");
      setup_widgets_desktop();

      return $widget;
    },

    widgetExists : function(selector)
    {
      return this.getWidget(selector).length > 0;
    },

    activeWidgetExists : function()
    {
      return this.getActiveWidget().length > 0;
    },

    getActiveWidget : function()
    {
      return this.getWidget(".widget-unlocked");
    },

    getWidget : function(selector)
    {
      return $("#widget-grid").find(selector);
    },

    configureAsLoading : function($widget)
    {
      //TODO: Maybe make this look nicer or clean up the code inside here?
      $widget.find(".widgetTitle").html('<i class="fa fa-spin fa-cog"></i> Loading');
      $widget.find(".widget-body").html('<h3 style="text-align:center"><i class="fa fa-spin fa-cog"></i> Loading</h3>');
      $widget.data("autoclose", false);
      
      if ($("article .stdWidgetContainer").length > 1)
      {
        if ($widget.hasClass('animated') == false)
        {
          $widget.addClass('bounce animated');
          $widget.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $(this).removeClass('bounce animated');
          });
        }
      }
    },
    
    updateDisplayMetadata : function($widget, displayMetadata, iconClass, title)
    {
      var $widgetTitle = $(".widgetTitle", $widget)
        .empty()
        .text(displayMetadata.breadcrumb)
        .prepend( $("<i>").attr("class", iconClass) )
        .attr("title", title + " ("+ $widget.data("module") +")")
        .attr("data-placement", "bottom");
      var $header = $(">header", $widget).attr('class', displayMetadata.colorClass);

      var headerTextColor = $header.css("color");
      $(".jarviswidget-ctrls a", $widget).css("color", headerTextColor);

      $widgetTitle.tooltip().tooltip('fixTitle');
      if(displayMetadata.showLiveFlag != '1') {
    	  $widget.addClass('hide-live-flag');
      }
    },
    
    blockUI : function($widget, callback)
    {
      var unblockDelay = $widget.data("unblockDelay");
      if (unblockDelay != undefined)
      {
        clearTimeout(unblockDelay);
        $widget.removeData("unblockDelay");
        
        if (callback != undefined)
        {
          callback();
        }
      }
      else if ($widget.data('uiBlocked') == undefined)
      {
        var $header = $(">header", $widget);
        
        var $background = $('<div class="loading-background" style="display: none"><span><i class="fa fa-spinner fa-pulse fa-3x"></i></span></div>');
        $background.css("background-color", $header.css("background-color").replace("rgb(", "rgba(").replace(")", ",0.5)"));
        $background.css("color", $header.css("color"));
        
        $(".widget-body", $widget).append($background);
        $widget.data('uiBlocked', true);
        $background.fadeIn(200, callback);
      }
    },
    
    unblockUI : function($widget, callback)
    {
      var unblockCallback = $widget.data("unblockCallback");
      if (unblockCallback != undefined)
      {
        unblockCallback = [];
      }
      if (callback != undefined)
      {
        unblockCallback.push(callback);
      }
      $widget.data("unblockCallback", unblockCallback);
      
      var unblockDelay = setTimeout(function(){
        $(".widget-body > .loading-background", $widget).fadeOut(200, function(){
          var unblockCallback = $widget.data("unblockCallback");
          $(unblockCallback).each(function(i,done){
            done();
          });
          $widget.removeData("unblockCallback");
          $widget.removeData("uiBlocked");
        }).remove();
        $widget.removeData("unblockDelay");
      }, 100);
      $widget.data("unblockDelay", unblockDelay);
    },

    addWidgetForModuleElementId : function(params, internalName, elementID, parentID)
    {
      var doingRestore = internalName?false:true;
      params = $.extend({
        widgetType : "element",
        module : internalName,
        tableType : 'Elements',
        ID : elementID,
        PID : parentID,
        action: 'update'
      }, params);
      internalName = params.module;
      elementID = params.ID;
      
      var widgetID = WidgetHelper.widgetIdForElement(internalName, elementID);
      var widgetSelector = "#"+widgetID;
      if (WidgetHelper.widgetExists(widgetSelector))
      {
        if(!doingRestore){
          WidgetHelper.scrollToWidget(widgetSelector);
        }
        return WidgetHelper.getWidget(widgetSelector);
      }

      var $widget = WidgetHelper.getActiveWidget();
      if (WidgetHelper.activeWidgetExists() == false) {
        $widget = WidgetHelper.addNewActiveWidget();
      }
      $widget.attr("id", widgetID);
      WidgetHelper.configureAsLoading($widget);
      
      $widget.data("requestData", JSON.stringify(params));
      $widget.data("module", internalName);
      $widget.data("id", elementID);
      var request = $widget.data("request");
      if (request != undefined)
      {
        request.abort();
      }

      request = $.ajax({
        url: '/webdirector/edit',
        data: params
      }).done(function( data, textStatus, jqXHR ) {
        $widget.find(".widget-body").html(data);
      }).done(function( data, textStatus, jqXHR ) {
        DynamicFormBuilder.init($widget);
      }).done(function(){
        if(!doingRestore){
          WidgetHelper.scrollToWidget($widget);
        }
      });
      $widget.data("request", request)
      
      var breadcrumb = $widget.data("breadcrumb");
      if (breadcrumb != undefined)
      {
        breadcrumb.abort();
      }
      breadcrumb = $.ajax({
        url: '/webdirector/module/elementDisplayMetadata',
        data: {
          tablePrefix: internalName,
          elementID : elementID
        }
      }).done(function( data, textStatus, jqXHR ) {
        WidgetHelper.updateDisplayMetadata($widget, data, "fa fa-file-o", "Element ID:"+elementID);
        WidgetHelper.hideMenuOnMobile();
      });

      WidgetHelper.saveWidgetState();
      return $widget;
    },
    
    addWidgetForModuleCategoryId : function(params, internalName, categoryID, parentID)
    {
      var doingRestore = internalName?false:true;
      params = $.extend({
        widgetType : "category",
        module : internalName,
        tableType : 'Categories',
        ID : categoryID,
        PID : parentID,
        action: 'update'
      }, params);
      internalName = params.module;
      categoryID = params.ID;
      
      var widgetID = WidgetHelper.widgetIdForCategory(internalName, categoryID);
      var widgetSelector = "#"+widgetID;
      if (WidgetHelper.widgetExists(widgetSelector))
      {
        if(!doingRestore){
          WidgetHelper.scrollToWidget(widgetSelector);
        }
        return WidgetHelper.getWidget(widgetSelector);
      }

      var $widget = WidgetHelper.getActiveWidget();
      if (WidgetHelper.activeWidgetExists() == false) {
        $widget = WidgetHelper.addNewActiveWidget();
      }
      $widget.attr("id", widgetID);
      WidgetHelper.configureAsLoading($widget);
      
      $widget.data("requestData", JSON.stringify(params));
      $widget.data("module", internalName);
      $widget.data("id", categoryID);
      var request = $widget.data("request");
      if (request != undefined)
      {
        request.abort();
      }

      request = $.ajax({
        url: '/webdirector/edit',
        data: params
      }).done(function( data, textStatus, jqXHR ) {
        $widget.find(".widget-body").html(data);
      }).done(function( data, textStatus, jqXHR ) {
        DynamicFormBuilder.init($widget);
      }).done(function(){
        if(!doingRestore){
          WidgetHelper.scrollToWidget($widget);
        }
      });
      $widget.data("request", request)
      
      var breadcrumb = $widget.data("breadcrumb");
      if (breadcrumb != undefined)
      {
        breadcrumb.abort();
      }
      breadcrumb = $.ajax({
        url: '/webdirector/module/categoryDisplayMetadata',
        data: {
          tablePrefix: internalName,
          categoryID : categoryID
        }
      }).done(function( data, textStatus, jqXHR ) {
        WidgetHelper.updateDisplayMetadata($widget, data, "fa fa-folder-open", "Category ID:"+categoryID);
        WidgetHelper.hideMenuOnMobile();
      });

      WidgetHelper.saveWidgetState();
      return $widget;
    },
    
    addWidgetForBulkEditElements : function(params, internalName, categoryParentID)
    {
      var doingRestore = internalName?false:true;
      params = $.extend({
        widgetType : "elementList",
        tablePrefix : internalName,
        ID : categoryParentID
      }, params);
      internalName = params.tablePrefix;
      categoryParentID = params.ID;

      var widgetID = WidgetHelper.widgetIdForElementList(internalName, categoryParentID);
      var widgetSelector = "#"+widgetID;
      if (WidgetHelper.widgetExists(widgetSelector))
      {
        if(!doingRestore){
          WidgetHelper.scrollToWidget(widgetSelector);
        }
        return WidgetHelper.getWidget(widgetSelector);
      }

      var $widget = WidgetHelper.getActiveWidget();
      if (WidgetHelper.activeWidgetExists() == false) {
        $widget = WidgetHelper.addNewActiveWidget();
      }
      $widget.attr("id", widgetID);
      WidgetHelper.configureAsLoading($widget);
      
      $widget.data("requestData", JSON.stringify(params));
      $widget.data("module", internalName);
      $widget.data("id", categoryParentID);
      var request = $widget.data("request");
      if (request != undefined)
      {
        request.abort();
      }

      request = $.ajax({
        url: '/webdirector/bulkEdit',
        data: params
      }).done(function( data, textStatus, jqXHR ) {
        $widget.find(".widget-body").html(data);
      }).done(function( data, textStatus, jqXHR ) {
        BulkEditor.init($widget, 'elementList', internalName, categoryParentID);
      }).done(function(){
        if(!doingRestore){
          WidgetHelper.scrollToWidget($widget);
        }
      });
      $widget.data("request", request)
      
      var breadcrumb = $widget.data("breadcrumb");
      if (breadcrumb != undefined)
      {
        breadcrumb.abort();
      }
      breadcrumb = $.ajax({
        url: '/webdirector/module/categoryDisplayMetadata',
        data: {
          tablePrefix: internalName,
          categoryID : categoryParentID
        }
      }).done(function( data, textStatus, jqXHR ) {
        WidgetHelper.updateDisplayMetadata($widget, data, "fa fa-files-o", "Category Parent ID:"+categoryParentID);
        WidgetHelper.hideMenuOnMobile();
      });

      WidgetHelper.saveWidgetState();
      return $widget;
    },
    
    addWidgetForBulkEditCategories : function(params, internalName, categoryParentID)
    {
      var doingRestore = internalName?false:true;
      params = $.extend({
        widgetType : "categoryList",
        tablePrefix : internalName,
        ID : categoryParentID
      }, params);
      internalName = params.tablePrefix;
      categoryParentID = params.ID;

      var widgetID = WidgetHelper.widgetIdForCategoryList(internalName, categoryParentID, "", params.show);
      var widgetSelector = "#"+widgetID;
      if (WidgetHelper.widgetExists(widgetSelector))
      {
        if(!doingRestore){
          WidgetHelper.scrollToWidget(widgetSelector);
        }
        return WidgetHelper.getWidget(widgetSelector);
      }

      var $widget = WidgetHelper.getActiveWidget();
      if (WidgetHelper.activeWidgetExists() == false) {
        $widget = WidgetHelper.addNewActiveWidget();
      }
      $widget.attr("id", widgetID);
      WidgetHelper.configureAsLoading($widget);
      
      $widget.data("requestData", JSON.stringify(params));
      $widget.data("module", internalName);
      $widget.data("id", categoryParentID);
      var request = $widget.data("request");
      if (request != undefined)
      {
        request.abort();
      }

      request = $.ajax({
        url: '/webdirector/bulkEdit',
        data: params
      }).done(function( data, textStatus, jqXHR ) {
        $widget.find(".widget-body").html(data);
      }).done(function( data, textStatus, jqXHR ) {
        BulkEditor.init($widget, 'categoryList', internalName, categoryParentID);
      }).done(function(){
        if(!doingRestore){
          WidgetHelper.scrollToWidget($widget);
        }
      });
      $widget.data("request", request)
      
      var breadcrumb = $widget.data("breadcrumb");
      if (breadcrumb != undefined)
      {
        breadcrumb.abort();
      }
      breadcrumb = $.ajax({
        url: '/webdirector/module/categoryDisplayMetadata',
        data: {
          tablePrefix: internalName,
          categoryID : categoryParentID
        }
      }).done(function( data, textStatus, jqXHR ) {
        WidgetHelper.updateDisplayMetadata($widget, data, "fa fa-table", "Category Parent ID:"+categoryParentID);
        WidgetHelper.hideMenuOnMobile();
      });

      WidgetHelper.saveWidgetState();
      return $widget;
    },
    
    addWidgetForInsertElement : function(params, internalName, parentID)
    {
      var doingRestore = internalName?false:true;
      params = $.extend({
        widgetType : "newElement",
        module : internalName,
        tableType : 'Elements',
        ID : "0",
        PID : parentID,
        action: 'insert'
      }, params);
      internalName = params.module;
      parentID = params.PID;

      var $widget = WidgetHelper.getActiveWidget();
      if (WidgetHelper.activeWidgetExists() == false) {
        $widget = WidgetHelper.addNewActiveWidget();
      }
      // WidgetHelper.lockWidget($widget);
      WidgetHelper.configureAsLoading($widget);
      
      $widget.data("requestData", JSON.stringify(params));
      $widget.data("module", internalName);
      var request = $widget.data("request");
      if (request != undefined)
      {
        request.abort();
      }
      request = $.ajax({
        url: '/webdirector/edit',
        data: params
      }).done(function( data, textStatus, jqXHR ) {
        $widget.find(".widget-body").html(data);
      }).done(function( data, textStatus, jqXHR ) {
        DynamicFormBuilder.init($widget);
      }).done(function(){
        if(!doingRestore){
          WidgetHelper.scrollToWidget($widget);
        }
      });
      $widget.data("request", request);
      
      var breadcrumb = $widget.data("breadcrumb");
      if (breadcrumb != undefined)
      {
        breadcrumb.abort();
      }
      breadcrumb = $.ajax({
        url: '/webdirector/module/categoryDisplayMetadata',
        data: {
          tablePrefix: internalName,
          categoryID : parentID
        }
      }).done(function( data, textStatus, jqXHR ) {
        data.breadcrumb = data.breadcrumb + " | New Element";
        WidgetHelper.updateDisplayMetadata($widget, data, "fa fa-file-o", "New Element");
        WidgetHelper.hideMenuOnMobile();
      });
      
      return $widget;
    },
    
    addWidgetForInsertCategory : function(params, internalName, parentID, level)
    {
      var doingRestore = internalName?false:true;
      params = $.extend({
        widgetType : "newCategory",
        module : internalName,
        tableType : 'Categories',
        ID : "0",
        PID : parentID,
        action: 'insert'
      }, params);
      internalName = params.module;
      parentID = params.PID;

      var $widget = WidgetHelper.getActiveWidget();
      if (WidgetHelper.activeWidgetExists() == false) {
        $widget = WidgetHelper.addNewActiveWidget();
      }
      // WidgetHelper.lockWidget($widget);
      WidgetHelper.configureAsLoading($widget);
      
      $widget.data("requestData", JSON.stringify(params));
      $widget.data("module", internalName);
      var request = $widget.data("request");
      if (request != undefined)
      {
        request.abort();
      }
      request = $.ajax({
        url: '/webdirector/edit',
        data: params
      }).done(function( data, textStatus, jqXHR ) {
        $widget.find(".widget-body").html(data);
      }).done(function( data, textStatus, jqXHR ) {
        DynamicFormBuilder.init($widget);
      }).done(function(){
        if(!doingRestore){
          WidgetHelper.scrollToWidget($widget);
        }
      });
      $widget.data("request", request);
      
      var breadcrumb = $widget.data("breadcrumb");
      if (breadcrumb != undefined)
      {
        breadcrumb.abort();
      }
      breadcrumb = $.ajax({
        url: '/webdirector/module/categoryDisplayMetadata',
        data: {
          tablePrefix: internalName,
          categoryID : parentID
        }
      }).done(function( data, textStatus, jqXHR ) {
        data.breadcrumb = data.breadcrumb + " | New Category";
        WidgetHelper.updateDisplayMetadata($widget, data, "fa fa-folder-open", "New Category");
        WidgetHelper.hideMenuOnMobile();
      });
      
      return $widget;
    },
    
    closeUnlockedWidget : function($widget){
      if(WidgetHelper.widgetIsLocked($widget) || $widget.data("autoclose") != true){
        return;
      }
      $widget.remove();
    },
    
    smartDialogBox: function ($widget, settings, callback) {
        var SmartMSG, Content;
        settings = $.extend({
            title: "",
            content: "",
            NormalButton: undefined,
            ActiveButton: undefined,
            buttons: undefined,
            input: undefined,
            inputValue: undefined,
            placeholder: "",
            options: undefined
        }, settings);

        var PlaySound = 0;
        var id = $widget.selector.substring(1, $widget.selector.length)
        var dialogId =  id + "_dialog";

        PlaySound = 1;
        //Messagebox Sound

        // SmallBox Sound
        if (isIE8orlower() == 0 && $.sound_on) {
            var audioElement = document.createElement('audio');
            audioElement.setAttribute('src', $.sound_path + 'messagebox.mp3');
            //$.get();
            audioElement.addEventListener("load", function () {
                audioElement.play();
            }, true);
    		
    		audioElement.pause();
        	audioElement.play();

        }

        if ($widget.closest(".jarviswidget").find(".divDialogBox").size() == 0) {
            SmartMSG = "<div class='divDialogBox animated fadeIn fast' id='" + dialogId + "'></div>";
            $widget.closest(".jarviswidget").append(SmartMSG);

            if (isIE8orlower() == 1) {
                $("#" + dialogId).addClass("MessageIE");
            }
        }

        var InputType = "";
        var HasInput = 0;
        if (settings.input != undefined) {
            HasInput = 1;
            settings.input = settings.input.toLowerCase();

            switch (settings.input) {
            case "text":
                settings.inputValue = $.type(settings.inputValue) === 'string' ? settings.inputValue.replace(/'/g, "&#x27;") : settings.inputValue;
                InputType = "<input class='form-control' type='" + settings.input + "' id='" + id +
                    "_txt' placeholder='" + settings.placeholder + "' value='" + settings.inputValue + "'/><br/><br/>";
                break;
            case "password":
                InputType = "<input class='form-control' type='" + settings.input + "' id='" + id +
                    "_txt' placeholder='" + settings.placeholder + "'/><br/><br/>";
                break;

            case "select":
                if (settings.options == undefined) {
                    alert("For this type of input, the options parameter is required.");
                } else {
                    InputType = "<select class='form-control' id='" + id + "_txt'>";
                    for (var i = 0; i <= settings.options.length - 1; i++) {
                        if (settings.options[i] == "[") {
                            Name = "";
                        } else {
                            if (settings.options[i] == "]") {
                                NumBottons = NumBottons + 1;
                                Name = "<option>" + Name + "</option>";
                                InputType += Name;
                            } else {
                                Name += settings.options[i];
                            }
                        }
                    };
                    InputType += "</select>"
                }

                break;
            default:
                alert("That type of input is not handled yet");
            }

        }

        Content = "<div class='MessageBoxContainer animated fadeIn fast' id='" + dialogId + "_Msg'>";
        Content += "<div class='MessageBoxMiddle'>";
        Content += "<span class='MsgTitle'>" + settings.title + "</span class='MsgTitle'>";
        Content += "<p class='pText'>" + settings.content + "</p>";
        Content += InputType;
        Content += "<div class='MessageBoxButtonSection'>";

        if (settings.buttons == undefined) {
            settings.buttons = "[Accept]";
        }

        settings.buttons = $.trim(settings.buttons);
        settings.buttons = settings.buttons.split('');

        var Name = "";
        var NumBottons = 0;
        if (settings.NormalButton == undefined) {
            settings.NormalButton = "#232323";
        }

        if (settings.ActiveButton == undefined) {
            settings.ActiveButton = "#ed145b";
        }

        for (var i = 0; i <= settings.buttons.length - 1; i++) {

            if (settings.buttons[i] == "[") {
                Name = "";
            } else {
                if (settings.buttons[i] == "]") {
                    NumBottons = NumBottons + 1;
                    Name = "<button id='bot" + NumBottons + "-" + dialogId + "_Msg" +
                        "' class='btn btn-default btn-sm botTempo'> " + Name + "</button>";
                    Content += Name;
                } else {
                    Name += settings.buttons[i];
                }
            }
        };

        Content += "</div>";
        //MessageBoxButtonSection
        Content += "</div>";
        //MessageBoxMiddle
        Content += "</div>";
        //MessageBoxContainer

        // alert(SmartMSGboxCount);
        /*
        if (SmartMSGboxCount > 1) {
            $(".MessageBoxContainer").hide();
            $(".MessageBoxContainer").css("z-index", 99999);
        }
        */

        $("#" + dialogId).append(Content);

        // Focus
        if (HasInput == 1) {
            $("#" + id +"_txt").focus();
        }

        $('.botTempo').hover(function () {
            var ThisID = $(this).attr('id');
            // alert(ThisID);
            // $("#"+ThisID).css("background-color", settings.ActiveButton);
        }, function () {
            var ThisID = $(this).attr('id');
            //$("#"+ThisID).css("background-color", settings.NormalButton);
        });

        // Callback and button Pressed
        $("#" + dialogId + " .botTempo").click(function () {
            // Closing Method
            var ThisID = $(this).attr('id');
            var MsgBoxID = ThisID.substr(ThisID.indexOf("-") + 1);
            var Press = $.trim($(this).text());

            if (HasInput == 1) {
                if (typeof callback == "function") {
                    var IDNumber = MsgBoxID.replace("Msg", "");
                    var Value = $("#txt" + IDNumber).val();
                    if (callback)
                        callback(Press, Value);
                }
            } else {
                if (typeof callback == "function") {
                    if (callback)
                        callback(Press);
                }
            }

            $("#" + MsgBoxID).addClass("animated fadeOut fast");
            //SmartMSGboxCount = SmartMSGboxCount - 1;

            
            $("#" + dialogId).removeClass("fadeIn").addClass("fadeOut").delay(300).queue(function () {
                $(this).remove();
            });
        });

    },
    
    hideMenuOnMobile : function(){
      if(!$.root_.hasClass("hidden-menu")){
        return;
      }
      if ($.root_.hasClass("container") && !$.root_.hasClass("menu-on-top")){
        $('span.minifyme').trigger("click");
      } else {
        $('#hide-menu > span > a').trigger("click"); 
      }
    },
    
    scrollToWidget: function($widget){
      var offset = $('body').hasClass('menu-on-top') ? 170 : 100;
      var headerBgColor = $(">header", $widget).css("background-color");
      
      var boxShadow = headerBgColor+" 0px 0px 20px";
      $('html,body').stop().animate({
        scrollTop: $($widget).offset().top - offset},
      'fast', function(){
        $($widget).css('box-shadow', boxShadow);
        setTimeout(function(){
          $($widget).css('box-shadow', 'none');
        }, 400);
        setTimeout(function(){
          $($widget).css('box-shadow', boxShadow);
        }, 800);
        setTimeout(function(){
          $($widget).css('box-shadow', 'none');
        }, 1200);
      });
    }
};
