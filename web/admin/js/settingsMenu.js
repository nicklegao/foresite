$(function() {
  var $settings = $("#settingsMenu");
  $settings.find("a").click(function(e) {
    var url = $(this).attr("href");
    if (url.indexOf("javascript") >= 0) {
      //Don't prevent default if javascript
    } else if (url.indexOf("iframe") >= 0) {
      ModalHelper.loadInIframe(url);
      e.preventDefault();
    } else {
      ModalHelper.launchModal(url);
      e.preventDefault();
    }
    $settings.removeClass("activate");
  });
});

function deleteTemporaryFiles() {
  $.post('/webdirector/deleteTempFiles').done(function(success) {
    if (success) {
      showNotification({success: success, content: "Temporary files deleted"});
    } else {
      showNotification({success: success, content: "There was an error while deleting temporary files. If this problem persists, please contact your site administrator."});
    }
  });
}