var debugObject;
var DynamicFormBuilder = {
    init: function($widget){
      var priv = parseInt($('.dynamic-form-container').data('priv'), 10);
      var request = JSON.parse($widget.data('requestData'));
      console.log(request);
      debugObject = $widget;
      var $form = $('form.form-horizontal', $widget);
      
      var clearCustomisedErrors = function($form){
        $('.hasInvalidField', $form).removeClass('hasInvalidField');
      };
      
      var initAjaxForm = function($form, type){
        
        $form.ajaxForm({
          url: '/webdirector/form/' + type, 
          beforeSerialize: function($form, options) { 
            // do validation here
            var action = $('input[name="action"]', $form).val();
            clearCustomisedErrors($form);
            var errors = {};
            if(type == 'save' && !$form.valid()){
              errors = {
                'cancel': true,
                'string': 'There are validation errors, please correct and try again.'
              };
            }
            if(type == 'delete'){
              errors = $.extend(errors, onDelete($widget));
            }else if(action == 'update'){
              errors = $.extend(errors, onUpdate($widget));
            }else if(action == 'insert'){
              errors = $.extend(errors, onCreate($widget));
            }
            if(errors.cancel){
              DynamicFormValidator.showErrors(errors, type, validator, $widget);
              return false;
            }
            
            
            // return false to stop submit
            if(type != 'review' && type != 'delete' && $form.data('orig') == DynamicFormBuilder.serialiseForm($form)){
              console.log('No change');
              showNotification({
                success: false,
                title: 'Nothing Changed',
                content: 'Please change something first',
                container: $('.widget-body', $widget)
              })
              $('.fa-refresh.fa-spin', $form).hide();
              $('.btn.disabled', $form).removeClass('disabled');
              return false;
            }
            WidgetHelper.blockUI($widget);
          },
          success:    function(response,status,xhr,$form ) { 
            WidgetHelper.unblockUI($widget);
            if (response.success)
            {
              var action = $('input[name="action"]', $form).val();
              if(type == 'delete'){
                afterDelete($widget);
              }else if(action == 'update'){
                afterUpdate($widget);
              }else if(action == 'insert'){
                afterCreate($widget);
              }
              $form.data('orig',DynamicFormBuilder.serialiseForm($form));
              $widget.data("autoclose", true);
              showNotification({
                success: true,
                title: 'Success',
                content: 'Data '+type+'d successfully',
                container:  $('.widget-body', $widget)
                }, function(){
                  WidgetHelper.closeUnlockedWidget($widget);
                });
            }
            else
            {
              showNotification({
                success: false,
                title: 'Error',
                content: response.message,
                container:  $('.widget-body', $widget)
              });
            }
            TreeHelper.reloadChildren(request.module, request.PID);
            $('.fa-refresh.fa-spin', $form).hide();
            $('.btn.disabled', $form).removeClass('disabled');
          }
        });
      };

      var createValidatorErrors = function(message){
        if(typeof message == 'undefined'){
          return {};
        }
        if(typeof message == 'string'){
          return {'cancel': true, 'string': 'Reason: ' + message};
        }
        if(typeof message == 'object'){
          return {
            'cancel': true,
            'string': 'There are validation errors, please correct and try again.', 
            'object': message
          };
        }
      }
      
      var onCreate = function($widget){
        if(DynamicFormValidator 
          && DynamicFormValidator[request.tableType]
          && DynamicFormValidator[request.tableType][request.module]) {
          var foo = DynamicFormValidator[request.tableType][request.module]($form);
          if(foo.onCreate){
            return createValidatorErrors(foo.onCreate());
          }
        }
        return {};
      }
      
      var onUpdate = function($widget){
        if(DynamicFormValidator 
          && DynamicFormValidator[request.tableType]
          && DynamicFormValidator[request.tableType][request.module]) {
          var foo = DynamicFormValidator[request.tableType][request.module]($form);
          if(foo.onUpdate){
            return createValidatorErrors(foo.onUpdate());
          }
        }
        return {};
      }

      var onDelete = function($widget){
        if(DynamicFormValidator 
          && DynamicFormValidator[request.tableType]
          && DynamicFormValidator[request.tableType][request.module])  {
          var foo = DynamicFormValidator[request.tableType][request.module]($form);
          if(foo.onDelete){
            return createValidatorErrors(foo.onDelete());
          }
        }
        return {};
      }

      var afterCreate = function($widget){
        if(DynamicFormValidator 
          && DynamicFormValidator[request.tableType]
          && DynamicFormValidator[request.tableType][request.module]) {
          var foo = DynamicFormValidator[request.tableType][request.module]($form);
          foo.afterCreate && foo.afterCreate()
        }
      }

      var afterUpdate = function($widget){
        if(DynamicFormValidator 
          && DynamicFormValidator[request.tableType]
          && DynamicFormValidator[request.tableType][request.module]) {
          var foo = DynamicFormValidator[request.tableType][request.module]($form);
          foo.afterUpdate && foo.afterUpdate()
        }
      }

      var afterDelete = function($widget){
        if(DynamicFormValidator 
            && DynamicFormValidator[request.tableType]
        && DynamicFormValidator[request.tableType][request.module]) {
          var foo = DynamicFormValidator[request.tableType][request.module]($form);
          foo.afterDelete && foo.afterDelete()
        }
      }
      
      var showHistory = function() {
        var module = $('input[name="module"]', $form).val();
        var dataId = $('input[name="ID"]', $form).val();
        var tableType = $('input[name="table"]', $form).val();
        
        ModalHelper.slideOpenPanel('/webdirector/versionControl/history', {
          module : module,
          dataId : dataId,
          tableType : tableType
        });
      }
      
      var initMultiSelector = function(selector){
        selector.show();
        var _width = selector.width(); 
        selector.before('<div class="loading" style="width:'+_width+'px;position:absolute;height:200px;background:#000;opacity:0.7;color:#fff;font-face:sans-serif;"><div style="margin-top:80px;text-align:center;font-size:1.4em;font-weight:bold;">Loading</div></div>');
        setTimeout(function() {
          selector.multiselect().next().find('input.search').width('70%');
        },100)
      };
      
      var loadDraft = function(div){
        var $div = $(div);
        var internalName = $('input[name="module"]', $form).val();
        var elementID = $('input[name="ID"]', $form).val();
        var parentID = $('input[name="PID"]', $form).val();
        var tableType = $('input[name="table"]', $form).val();
        
        var request = $widget.data("request");
        if (request != undefined)
        {
          request.abort();
        }

        request = $.ajax({
          url: '/webdirector/edit',
          data: {
            module : internalName,
            tableType : tableType,
            ID : elementID,
            PID : parentID,
            action: 'update',
            draft : true
          }
        }).done(function( data, textStatus, jqXHR ) {
          $widget.find(".widget-body").html(data);
        }).done(function( data, textStatus, jqXHR ) {
          DynamicFormBuilder.init($widget);
        });
        $widget.data("request", request)
      };
      
      if(DynamicFormValidator 
          && DynamicFormValidator[request.tableType]
          && DynamicFormValidator[request.tableType][request.module] 
          && DynamicFormValidator[request.tableType][request.module]($form).onInit){
        DynamicFormValidator[request.tableType][request.module]($form).onInit();
      }

      var validator = DynamicFormValidator.jQueryValidate($form);
      
      if($('input[name="ID"]').val() != '0'){
        $('.wd-no-change', $form).each(function(){
          if($(this).hasClass('btn')){
            $(this).prop("disabled", true);
            return;
          }
          if($(this).hasClass('input-checkbox')){
            $(this).prop("disabled", true);
            return;
          }
          $(this).prop("readonly", true);
        });
      }
      
      $("a.removeNoteAnchor", $form).click(function(event){
        event.preventDefault();
        var $a = $(this);
        $.SmartMessageBox({
          title : '<i class="fa fa-warning text-warning"></i> You are about to delete this note.',
          content : "Do you want to continue? ",
          buttons : '[No][Yes]'
        }, function(ButtonPressed) {
          if (ButtonPressed === "No") {
            return false;
          } else if (ButtonPressed === "Yes") {
            $.ajax({
              url: '/webdirector/note/delete',
              data: { id: $a.attr("id") }
            }).done(function( data, textStatus, jqXHR ) {
              if (data) {
                $a.closest('.note').remove();
              }
            });
            return true;
          }
        });
      });
      
      $("a.removeFileAnchor", $form).click(function(event){
        event.preventDefault();
        var $a = $(this);
        $.SmartMessageBox({
          title : '<i class="fa fa-warning text-warning"></i> You are about to delete this file.',
          content : "Do you want to continue? ",
          buttons : '[No][Yes]'
        }, function(ButtonPressed) {
          if (ButtonPressed === "No") {
            return false;
          } else if (ButtonPressed === "Yes") {
            $.ajax({
              url: '/webdirector/file/delete',
              data: { 
                id: $a.data("id"),
                type: request.tableType,
                col: $a.data('field'),
                file: $a.data('file'),
                module: request.module
              }
            }).done(function( data, textStatus, jqXHR ) {
              if (data) {
                $a.closest('.thumb-container').remove();
              }
            });
            return true;
          }
        });
      });
      
      $("a.removeSharedFileAnchor", $form).click(function(event){
        event.preventDefault();
        var $a = $(this);
        $.SmartMessageBox({
          title : '<i class="fa fa-warning text-warning"></i> You are about to clear this field.',
          content : "Do you want to continue? ",
          buttons : '[No][Yes]'
        }, function(ButtonPressed) {
          if (ButtonPressed === "No") {
            return false;
          } else if (ButtonPressed === "Yes") {
            $a.parents('.input-group')
            .find('.thumb-container')
              .find('a.thumb').attr('href', '')
                .empty()
              .end()
              .find('.desc')
                .find('.filename').text('')
                .end()
                .find('.filesize').text('')  
                .end()
              .end()
              .hide()  
            .end()
            .find('input.input-sharedfile').val('');
            return true;
          }
        });
      });
      
      $('input:checkbox[name^="DUMMY_"]', $form).change(function(){
        var $input = $(this).siblings('input:hidden');
        if($(this).is(':checked')){
          $input.val('1');
        }else{
          $input.val('0');
        }
      }).change();
      
      $('input.open-sharedfile-manager', $form).click(function(e){
		// e.preventDefault();
        var index = DynamicFormBuilder.indexOf($widget);
        var urlString = '/webdirector/adminExtra/filemanager/index?fileRoot=/&WDWidget='+ index +'&langCode=en&WDField=' + this.id + '&time=' + (+new Date)
        changeTab($("#navFileManager"));
        FullWidthHelper.loadInIframe(urlString);
      })
      
      $('input.open-editor', $form).click(function(){
        var urlString = '/webdirector/adminExtra/ckeditor/edit?colNum=0&id='+request.ID+'&module='+request.module+'&action='+request.action+'&colName=' + $(this).attr('id') + '&tableType='+request.tableType+'&draft=' + request.draft+'&draftId=' + request.DID;
        FullWidthHelper.loadInStaticIframe(urlString);
      })
      
      $('.save-direct-btn', $form).click(function(){
    	  var btn = $(this);
    	  
        if(btn.hasClass('disabled')){
          return false;
        }
        if ($('input[name="hasDraft"]', $form).val() === "true" && $('input[name="ID"]', $form).val() !== "0") {
        	$.SmartMessageBox({
      	      title : "You are about to overwrite a draft.",
      	      content : "Do you want to continue? ",
      	      buttons : '[No][Yes]'
      	    }, function(ButtonPressed) {
      	      if (ButtonPressed === "No") {
      	    	return false;
      	      } else if (ButtonPressed === "Yes") {
      	    	$('input[name="sendOldDraftInEmail"]', $form).val("true");
      	    	initAjaxForm($form, 'save');
      	    	btn.closest('form').find('input[name="status"]').val("").end().submit();
      	      }
      	   });
        	
        } else {
        	initAjaxForm($form, 'save');
        	btn.closest('form').find('input[name="status"]').val("").end().submit();
        }
        
      });
      
      
      
      $('.save-draft-btn', $form).click(function(){
    	var $btn = $(this);
    	  
        if($btn.hasClass('disabled')){
          return false;
        }
        
        DynamicFormBuilder.checkConcurrentDraftOverwriteThenSave("DRAFT", $form, $btn, initAjaxForm);
      });

      $('.save-submit-btn', $form).click(function(){
    	  var $btn = $(this);
    	  
        if($(this).hasClass('disabled')){
          return false;
        }

        DynamicFormBuilder.checkConcurrentDraftOverwriteThenSave("TO_BE_APPROVED", $form, $btn, initAjaxForm);
      });

      $('.delete-btn', $form).click(function(){
        var $this = $(this);
        if($this.hasClass('disabled')){
          return false;
        }
        $.SmartMessageBox({
          title : '<i class="fa fa-warning text-warning"></i> You are about to delete this data.',
          content : "Do you want to continue? ",
          buttons : '[No][Yes]'
        }, function(ButtonPressed) {
          if (ButtonPressed === "No") {
            return false;
          } else if (ButtonPressed === "Yes") {
            initAjaxForm($form, 'delete');
            $this.closest('form').find('input[name="status"]').val("").end().submit();
            return true;
          }
        });
      });
      
      $('.show-history-btn', $form).click(function(){
        showHistory();
      });
      
      $('.load-btn', $form).click(function(){
        loadDraft();
      });
      
      $('.approve-btn', $form).click(function(){
        if($(this).hasClass('disabled')){
          return false;
        }
        initAjaxForm($form, 'review');
        $(this).closest('form').find('input[name="status"]').val("APPROVED").end().submit();
      });
      
      $('.decline-btn', $form).click(function(){
        if($(this).hasClass('disabled')){
          return false;
        }
        $.SmartMessageBox({
          title : '<i class="fa fa-warning text-warning"></i> Please enter the reason.',
          content : "<textarea id='decline-reason-input' style='width: 100%; height: 100px; color: black; padding: 3px 5px; margin: 3px 0px;'></textarea>",
          buttons : '[Cancel][Continue]'
        }, function(ButtonPressed) {
          if (ButtonPressed === "Cancel") {
            return false;
          } else if (ButtonPressed === "Continue") {
            if($('#decline-reason-input').val() != ''){
              initAjaxForm($form, 'review');
              $form.find('input[name="status"]').val("DECLINED").end()
              .find('input[name="declineReason"]').val($('#decline-reason-input').val()).end()
              .submit();
              return true;
            }
          }
        });
      });

      $(".input-multidrop", $form).each(function(){
        if($(this).children("option").length > 20){
          $button = $("<button type='button' class='btn btn-default' id='"+ $(this).attr("name") +"'/>").text('Choose');
          $button.click(function(){
            var selector = $(this).hide().prev(".input-multidrop");
            initMultiSelector(selector);
          });
          $(this).hide().after($button)
        }else{
          initMultiSelector($(this));
        }
      });
      
      $('.input-datetime', $form)
        .filter('.short-input')
        .css({
          'width':'115px',
          'display':'inline-block'
        }).end().datetimepicker({
        format: 'YYYY-MM-DD HH:mm', 
        useCurrent: false
      });
      
      //enable tab key in textarea, refer to http://pallieter.org/Projects/insertTab/
      $("textarea[id^='note-ATTRNOTES']", $form).keydown(
        function(e){
          var o = this;
          var kC = e.keyCode ? e.keyCode : e.charCode ? e.charCode : e.which;
          if (kC == 9 && !e.shiftKey && !e.ctrlKey && !e.altKey)
          {
              var oS = o.scrollTop;
              if (o.setSelectionRange)
              {
                  var sS = o.selectionStart;
                  var sE = o.selectionEnd;
                  o.value = o.value.substring(0, sS) + "\t" + o.value.substr(sE);
                  o.setSelectionRange(sS + 1, sS + 1);
                  o.focus();
              }
              else if (o.createTextRange)
              {
                  document.selection.createRange().text = "\t";
                  e.returnValue = false;
              }
              o.scrollTop = oS;
              if (e.preventDefault)
              {
                  e.preventDefault();
              }
              return false;
          }
          return true;
        }
      );
      
      $('.lookup-btn', $form).click(function(){
        var url = '/webdirector/lookup-options?module='+request.module+'&field='+ $(this).prev().attr('name') +'&tableType='+request.tableType + '&widget=' + $widget.attr('id');
        ModalHelper.launchModal(url);
      });
      
      $(".nav-tabs", $widget).on("click", "a", function() {
        var $li = $(this).closest("li");
        DynamicFormBuilder.setActiveTab(request.module, request.tableType, $li.index())
      });
      
      var activeTabIndex = DynamicFormBuilder.getActiveTab(request.module, request.tableType);
      if (activeTabIndex && activeTabIndex > -1 && request.action != "insert") {
        var $items =  $(".nav-tabs > li", $widget);
        if($items.length <= activeTabIndex){
          activeTabIndex = $items.length - 1
          DynamicFormBuilder.setActiveTab(request.module, request.tableType, activeTabIndex);
        }
        var $li = $items.eq(activeTabIndex);
        $items.removeClass("active");
        var tabPaneId = $li.find("a").attr("href");
        $li.addClass("active");
        var $tabPanes = $items.closest("div").find(".tab-pane");
        $tabPanes.removeClass("in active");
        $tabPanes.filter(tabPaneId).addClass("in active");
      }
      
      // Author of draft is accessing asset
      var internalName = $('input[name="currentUserIsDraftAuthor"]', $form).val();
      if (internalName === "true") {
    	  $.SmartMessageBox({
    	      title : "A draft of this asset exists.",
    	      content : "Would you like to view the live asset or your draft? ",
    	      buttons : '[Draft Asset][Live Asset]'
    	    }, function(ButtonPressed) {
    	      if (ButtonPressed === "Draft Asset") {
    			loadDraft();
    	      }
    	   });
      }

      $form.data('orig', DynamicFormBuilder.serialiseForm($form));
    },
    val: function($widget, input, data){
      var $input = $('[name="'+input+'"]', $widget);
      if(typeof data == 'string'){
        $input.val(data);
        return;
      }
      var formatBytes = function(bytes) {
        if(typeof bytes == 'undefined'){
          return '';
        }
        var n = parseFloat(bytes);
        var d = parseFloat(1024);
        var c = 0;
        var u = ['bytes','kb','mb','gb'];
        
        while(true){
          if(n < d){
            n = Math.round(n * 100) / 100;
            return '(' + n + u[c] + ')';
          } else {
            n /= d;
            c += 1;
          }
        }
      }
      var preview = data['Preview'] ? $('<img>').attr('src', data['Preview']) : $('<i class="fa fa-file-o">');
      var storeContext = data['StoreContext'];
      var value = data['Path'];
      var filename = data['Filename'];
      var size = formatBytes(data['Properties']['Size']);
      $input.val(value);
      if(input.indexOf("ATTRSHAREDFILE_") != -1){
        $input.parent().find('.thumb-container')
          .find('a.thumb').attr('href', storeContext + value)
            .empty().append(preview)
          .end()
          .find('.desc')
            .find('.filename').text(filename)
            .end()
            .find('.filesize').text(size)  
            .end()
          .end()  
          .show();
      }
    },
    get: function(index){
      return $($('.jarviswidget')[index]);
    },
    indexOf: function($widget) {
      return $('.jarviswidget').index($widget);
    },
    setActiveTab : function(module, tableType, tabNumber)
    {
      var activeTabs = JSON.parse(wdLocalStorage.getItem("activeTabs")) || {};
      activeTabs[module + "_" + tableType] = tabNumber;
      wdLocalStorage.setItem("activeTabs", JSON.stringify(activeTabs));
    },
    getActiveTab : function(module, tableType)
    {
      var activeTabs = JSON.parse(wdLocalStorage.getItem("activeTabs")) || {};
      return activeTabs[module + "_" + tableType];
    },
    
    checkConcurrentDraftOverwriteThenSave : function(draftState, $form, $btn, initAjaxForm)
    {
        var draftEditDate = $('input[name="draftEditDate"]', $form).val();
        var ID = $('input[name="ID"]', $form).val();
        var DID = $('input[name="DID"]', $form).val();
        var module = $('input[name="module"]', $form).val();
        var table = $('input[name="table"]', $form).val();

        $.ajax({
            url: '/webdirector/form/getDraftSaveDate',
            data: {
            	draftEditDate: draftEditDate,
            	DataID: ID,
            	DraftID: DID,
            	module: module,
            	table: table
            }
          }).done(function( data, textStatus, jqXHR ) {
        	  if (data) {
              	$.SmartMessageBox({
            	      title : "You are about to overwrite a draft belonging to someone else.",
            	      content : "Do you want to continue?",
            	      buttons : '[No][Yes]'
            	    }, function(ButtonPressed) {
            	      if (ButtonPressed === "No") {
            	    	return false;
            	      } else if (ButtonPressed === "Yes") {
            	    	$('input[name="sendOldDraftInEmail"]', $form).val("true");
            	    	initAjaxForm($form, 'save');
            	    	$btn.closest('form').find('input[name="status"]').val(draftState).end().submit();
            	      }
            	   });
              	
              } else {
              	initAjaxForm($form, 'save');
              	$btn.closest('form').find('input[name="status"]').val(draftState).end().submit();
              }
          });
    },
    serialiseForm: function($form) {
      return $(':input:not(.skip-change-check)', $form).fieldSerialize();
    }
  }
