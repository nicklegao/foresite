if(!localStorage.getItem('header') || localStorage.getItem('header') === "on") {
	$('#smart-fixed-header').prop('checked', true);
} else {
	$('#smart-fixed-header').prop('checked', false);
}

if(!localStorage.getItem('navigation') || localStorage.getItem('navigation') === "on") {
	$('#smart-fixed-navigation').prop('checked', true);
} else {
	$('#smart-fixed-navigation').prop('checked', false);
}

if(!localStorage.getItem('ribbon') || localStorage.getItem('ribbon') === "on") {
	$('#smart-fixed-ribbon').prop('checked', true);
} else {
	$('#smart-fixed-ribbon').prop('checked', false);
}