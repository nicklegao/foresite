var wdLocalStorage = {
    
    _user: $('body').data('user'),
    
    _generateKey: function(key){
      return this._user + "_" + key;
    },
    
    _isCurrentUser: function(key){
      return key.indexOf(this._user) > -1;
    },
    
    getItem: function(key){
      return localStorage.getItem(this._generateKey(key));
    },
    
    setItem: function(key, value){
      return localStorage.setItem(this._generateKey(key), value);
    },
    
    removeItem: function(key){
      localStorage.removeItem(this._generateKey(key))
    },
    
    clear: function(){
      for(var i=0;i<localStorage.length;i++){
        var key = localStorage.key(i);
        if(this._isCurrentUser(key)){
          localStorage.removeItem(key);
          i--;
        }
      }
    }
}