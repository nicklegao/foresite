jQuery.validator.addMethod("integer", function(value, element) {
  return value === '' + parseInt(value, 10);
}, "Please enter a valid integer");
jQuery.validator.addMethod("float", function(value, element) {
  return /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test( value )
}, "Please enter a valid number");
      
var DynamicFormValidator = {
    Categories:{},
    Elements:{},
    _loadFormValidatiorSettings: function ($form){
      var settings = {
        onsubmit: false,
        ignore: [],
        ignoreTitle: true,
        rules:{
        },
        errorPlacement : function(error, element) {
          element.parent().append(error);
        }
      };
      $('.form-control.required',$form).each(function(){
        settings.rules[this.name] = settings.rules[this.name] || {};
        settings.rules[this.name]['required'] = true;
      });
      $('.form-control[data-max-length]',$form).each(function(){
        settings.rules[this.name] = settings.rules[this.name] || {};
        settings.rules[this.name]['maxlength'] = parseInt($(this).attr('data-max-length'), 10);
      });
      $('.form-control.input-money, .form-control.input-float',$form).each(function(){
        settings.rules[this.name] = settings.rules[this.name] || {};
        settings.rules[this.name]['float'] = true;
      });
      $('.form-control.input-integer',$form).each(function(){
        settings.rules[this.name] = settings.rules[this.name] || {};
        settings.rules[this.name]['integer'] = true;
      });
      $('.form-control.input-email',$form).each(function(){
        settings.rules[this.name] = settings.rules[this.name] || {};
        settings.rules[this.name]['email'] = true;
      });
      return settings;
    },
    _showErrorsInTab: function(input){
      var $pane = $(input).closest('div.tab-pane');
      var $tab = $pane.closest('div.dynamic-form-container').find('ul.nav-tabs > li:eq('+ $pane.index() +')');
      $tab.addClass('hasInvalidField');
    },
    jQueryValidate: function($form){
      return $form.validate(DynamicFormValidator._loadFormValidatiorSettings($form));
    },
    showErrors: function(errors, type, validator, $widget){
      if(!errors.string){
        return;
      }
      var object = $.extend({}, errors.object, validator.errorMap);
      validator.showErrors(object);
      $.each(validator.errorMap, function(name, value){
        DynamicFormValidator._showErrorsInTab($('[name="'+ name +'"]', $widget));
      });
      showNotification({
        success: false,
        title: 'Cannot ' + type + ' your data',
        content: errors.string,
        container: $('.widget-body', $widget)
      });
    }
};
