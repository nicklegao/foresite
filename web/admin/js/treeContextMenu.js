var lastAction = null;

function setLastAction(action)
{
	lastAction = action;
}

function getLastAction()
{
	return lastAction;
}

$(function()
{
	var moduleMenuItems = function($trigger, e)
	{

		var options = {
			$trigger : $trigger
		};
		var module = getModuleSettings(options);
		var node = getNode(options);
		var items = {};

		if (!isModuleConfigureHidden('configure', options))
		{
			items.configure = {
				icon : 'fa-cogs',
				name : "Configure",
				callback : configureModule
			};
			items.separator1 = "-";
		}

		if (!isBulkEditHidden('bulkEdit', options) && node)
		{
			items.bulkEdit = {
				icon : 'fa-table',
				name : "Edit All",
				items : {}
			};
			for (var i = node.data.folderLevel; i < module.levels; i++)
			{
				items.bulkEdit.items['c' + (i + 1)] = {
					icon : 'fa-folder',
					name : "All Level " + (i + 1) + " Categories",
					callback : bulkEdit
				};
			}
			items.bulkEdit.items.e = {
				icon : 'fa-file',
				name : "All Elements",
				callback : bulkEdit
			};
		}

		if (!isModuleInsertHidden('insert', options))
		{
			items.insert = {
				icon : 'fa-plus-circle',
				name : "Insert",
				callback : insertFirstLevelCategory
			};
		}
		if (!isPasteHidden('paste', options))
		{
			items.paste = {
				icon : 'fa-clipboard',
				name : "Paste",
				disabled : isPasteDisabled,
				callback : pasteCategoryOrElement
			};
			items.separator2 = "-";
		}
		// if(!isImportHidden('import', options)){
		// items.import = {
		// icon: 'fa-download',
		// name: "Import",
		// callback: importData
		// };
		// items.separator3 = "-";
		// }
		if (!isCollapseAllHidden('collapse', options))
		{
			items.collapse = {
				icon : 'fa-minus-square',
				name : "Collapse All",
				callback : collapseAll
			};
		}
		if (!isModuleRefreshHidden('refresh', options))
		{
			items.refresh = {
				icon : 'fa-sync',
				name : "Refresh",
				callback : refreshModule
			};
		}
		/*
		 * items.close = { icon: 'fa-times-circle', name: "Close", callback:
		 * closeModule };
		 */
		return items;
	};


	var categoryMenuItems = function($trigger, e)
	{
		var options = {
			$trigger : $trigger
		};
		var module = getModuleSettings(options);
		var node = getNode(options);
		var items = {};

		if (!isEditHidden('edit', options))
		{
			items.edit = {
				icon : 'fa-pencil',
				name : "Edit",
				callback : viewOrEdit
			};
		}
		if (!isInsertHidden('insert', options))
		{
			items.insert = {
				icon : 'fa-plus-circle',
				name : "Insert",
				callback : insertCategoryOrElement
			};
		}

		if (!isViewHidden('view', options))
		{
			items.view = {
				icon : 'fa-eye',
				name : "View",
				callback : viewOrEdit
			};
		}
		if (!isBulkEditHidden('bulkEdit', options) && node)
		{
			items.bulkEdit = {
				icon : 'fa-table',
				name : "Edit All",
				items : {}
			};
			for (var i = node.data.folderLevel; i < module.levels; i++)
			{
				items.bulkEdit.items['c' + (i + 1)] = {
					icon : 'fa-folder',
					name : "All Level " + (i + 1) + " Categories",
					callback : bulkEdit
				};
			}
			items.bulkEdit.items.e = {
				icon : 'fa-file',
				name : "All Elements",
				callback : bulkEdit
			};
		}
		if (!isDeleteHidden('del', options))
		{
			items.del = {
				icon : 'fa-trash-alt',
				name : "Delete",
				callback : deleteCategoryOrElement
			};
			items.separator1 = "-";
		}
		if (!isSortHidden('sort', options))
		{
			items.sort = {
				icon : 'fa-sort',
				name : "Sort",
				callback : sortCategoryOrElement
			};
			items.separator2 = "-";
		}
		if (!isCancelHidden('cancel', options))
		{
			items.cancel = {
				icon : 'fa-times',
				name : "Cancel",
				callback : cancelLastAction
			};
		}
		if (!isPasteHidden('paste', options))
		{
			items.paste = {
				icon : 'fa-clipboard',
				name : "Paste",
				disabled : isPasteDisabled,
				callback : pasteCategoryOrElement
			};
		}
		if (!isCopyHidden('copy', options))
		{
			items.copy = {
				icon : 'fa-copy',
				name : "Copy",
				callback : cutOrCopy
			};
		}
		if (!isCopyHidden('copyAll', options))
		{
			items.copyAll = {
				icon : 'fa-copy',
				name : "Copy All",
				callback : cutOrCopyAll
			};
		}
		if (!isCutHidden('cut', options))
		{
			items.cut = {
				icon : 'fa-cut',
				name : "Cut",
				callback : cutOrCopy
			};
		}
		if (!isCutHidden('cutAll', options))
		{
			items.cutAll = {
				icon : 'fa-cut',
				name : "Cut All",
				callback : cutOrCopyAll
			};
			items.separator3 = "-";
		}
		// if(!isImportHidden('import', options)){
		// items.import = {
		// icon: 'fa-download',
		// name: "Import",
		// callback: importData
		// };
		// }
		// if(!isExportHidden('export', options)){
		// items.export = {
		// icon: 'fa-upload',
		// name: "Export",
		// callback: exportData
		// };
		// items.separator4 = "-";
		// }
		if (!isCollapseAllHidden('collapse', options))
		{
			items.collapse = {
				icon : 'fa-minus-square',
				name : "Collapse All",
				callback : collapseAll
			};
		}
		if (!isRefreshHidden('refresh', options))
		{
			items.refresh = {
				icon : 'fa-sync',
				name : "Refresh",
				callback : refresh
			}
		}
		return items;
	};

	var elementMenuItems = function($trigger, e)
	{
		var options = {
			$trigger : $trigger
		};
		var module = getModuleSettings(options);
		var items = {};

		if (!isViewHidden('view', options))
		{
			items.view = {
				icon : 'fa-eye',
				name : "View",
				callback : viewOrEdit
			};
		}
		if (!isEditHidden('edit', options))
		{
			items.edit = {
				icon : 'fa-pencil',
				name : "Edit",
				callback : viewOrEdit
			};
		}
		if (!isDeleteHidden('del', options))
		{
			items.del = {
				icon : 'fa-trash-alt',
				name : "Delete",
				callback : deleteCategoryOrElement
			};
			items.separator1 = "-";
		}
		if (!isSortHidden('sort', options))
		{
			items.sort = {
				icon : 'fa-sort',
				name : "Sort",
				callback : sortCategoryOrElement
			};
			items.separator2 = "-";
		}
		if (!isCancelHidden('cancel', options))
		{
			items.cancel = {
				icon : 'fa-times',
				name : "Cancel",
				callback : cancelLastAction
			};
		}
		if (!isCopyHidden('copy', options))
		{
			items.copy = {
				icon : 'fa-copy',
				name : "Copy",
				callback : cutOrCopy
			};
		}
		if (!isCopyHidden('copyAll', options))
		{
			items.copyAll = {
				icon : 'fa-copy',
				name : "Copy All",
				callback : cutOrCopyAll
			};
		}
		if (!isCutHidden('cut', options))
		{
			items.cut = {
				icon : 'fa-cut',
				name : "Cut",
				callback : cutOrCopy
			};
		}
		if (!isCutHidden('cutAll', options))
		{
			items.cutAll = {
				icon : 'fa-cut',
				name : "Cut All",
				callback : cutOrCopyAll
			};
			items.separator3 = "-";
		}
		// if(!isImportHidden('import', options)){
		// items.import = {
		// icon: 'fa-download',
		// name: "Import",
		// callback: importData
		// };
		// items.separator4 = "-";
		// }
		items.refresh = {
			icon : 'fa-sync',
			name : "Refresh",
			callback : refresh
		}
		return items;
	};

	if (wdLocalStorage.getItem("sm-setmenu") != "top")
	{
		var $aside = $("#left-panel")

		$aside.contextMenu({
			selector : "nav > ul > li:not('.no-context-menu') > a",
			build : function($trigger, e)
			{
				return {
					items : moduleMenuItems($trigger, e)
				};
			}
		});

		$aside.contextMenu({
			selector : ".fancytree-folder > .fancytree-icon, .fancytree-folder > .fancytree-title",
			build : function($trigger, e)
			{
				return {
					items : categoryMenuItems($trigger, e)
				};
			}
		});

		$aside.contextMenu({
			selector : ".fancytree-node:not(.fancytree-folder) > .fancytree-icon, .fancytree-node:not(.fancytree-folder) > .fancytree-title",
			build : function($trigger, e)
			{
				return {
					items : elementMenuItems($trigger, e)
				};
			}
		});

		$aside.on('mouseover', function()
		{
			if ($('body').hasClass('hidden-menu'))
			{
				$aside.addClass('hover');
			}
		})

		$aside.on('mouseleave', function()
		{
			if ($('.context-menu-root').length == 0)
			{
				$aside.removeClass("hover");
			}
		})

		$('#main').on('click', function()
		{
			if ($('.context-menu-root').length == 0)
			{
				$aside.removeClass("hover");
			}
		})
	}


	function getNode(options)
	{
		var $el = options.$trigger.parent();
		var node = $.ui.fancytree.getNode($el);
		if (node == null)
		{
			var root = $el.find('span.fancytree-node:eq(0)');
			node = $.ui.fancytree.getNode(root);
		}
		return node;
	}

	function getPrivileges(options)
	{
		var node = getNode(options);
		if (node == null)
			return null;
		return node.folder ? node.data.privileges : node.parent.data.privileges;
	}

	function isWorkflow(options)
	{
		var node = getNode(options);
		// if it's workflow, cannot insert
		if (node != null && node.data.workflow)
		{
			return true;
		}
		return false;
	}

	function getModuleSettings(options)
	{
		var $el = options.$trigger.parent();
		return $el.closest(".module").data("module");
	}

	function isSortHidden(key, options)
	{
		if (isWorkflow(options))
			return true;
		var module = getModuleSettings(options);
		var priv = $.ui.fancytree.getNode(options.$trigger.parent()).parent.data.privileges;
		if (!module.has_sort)
		{
			return true;
		}
		if (priv == null)
		{
			return false;
		}
		return !(priv.insert);
	}

	function isViewHidden(key, options)
	{
		var privileges = getPrivileges(options);
		return privileges === null || privileges.edit || (!privileges.view && !privileges.edit);
	}

	function isEditHidden(key, options)
	{
		var privileges = getPrivileges(options);
		return privileges !== null && !privileges.edit;
	}

	function isInsertHidden(key, options)
	{
		if (isWorkflow(options))
			return true;
		var privileges = getPrivileges(options);
		return privileges !== null && !privileges.insert;
	}

	function isModuleInsertHidden(key, options)
	{
		var $el = options.$trigger.parent();
		if (!$el.hasClass("open"))
		{
			return true;
		}
		var node = $.ui.fancytree.getNode($el.find(".root-category"));
		return !node || (node.data.privileges && !node.data.privileges.insert);
	}

	function isCollapseAllHidden(key, options)
	{
		// var node = getNode(options);
		// if(node.key != '0' && !node.expended){
		// return false;
		// }
		// var expanded = false;
		// node.visit(function(child){
		// expanded = expanded || node.expended;
		// });
		// return !expanded;
		if (isWorkflow(options))
			return true;
		return false;
	}

	function isRefreshHidden(key, options)
	{
		if (isWorkflow(options))
			return true;
		return false;
	}

	function isModuleRefreshHidden(key, options)
	{
		if (isWorkflow(options))
			return true;
		var $el = options.$trigger.parent();
		if (!$el.hasClass("open"))
		{
			return true;
		}
	}

	function isDeleteHidden(key, options)
	{
		var module = getModuleSettings(options);
		if (module.workflow_enabled && !UserDataHelper.isAdmin())
		{
			return true;
		}

		var node = getNode(options);
		if (node.data.workflow)
		{
			return true;
		}

		var privileges = getPrivileges(options);
		return privileges !== null && !privileges["delete"];
	}

	function isCutHidden(key, options)
	{
		if (isWorkflow(options))
			return true;
		var module = getModuleSettings(options);
		if (!module.has_cut)
		{
			return true;
		}

		if (module.workflow_enabled && !UserDataHelper.isAdmin())
		{
			return true;
		}

		var node = getNode(options);

		if (node.getLevel() <= 2)
		{
			return true;
		}
		if (node.data.workflow)
		{
			return true;
		}

		var privileges = getPrivileges(options);
		return privileges !== null && !privileges["delete"];
	}

	function isCopyHidden(key, options)
	{
		if (isWorkflow(options))
			return true;
		var module = getModuleSettings(options);
		if (!module.has_copy)
		{
			return true;
		}

		if (module.workflow_enabled && !UserDataHelper.isAdmin())
		{
			return true;
		}

		var node = getNode(options);
		if (node.data.workflow)
		{
			return true;
		}
		return false;
	}

	function isModuleConfigureHidden(key, options)
	{
		return !UserDataHelper.isAdmin();
	}

	function isImportHidden(key, options)
	{
		if (isWorkflow(options))
			return true;
		var module = getModuleSettings(options);
		if (!module.has_import)
		{
			return true;
		}
		return !UserDataHelper.isAdmin();
	}

	function isBulkEditHidden(key, options)
	{
		var privileges = getPrivileges(options);
		return privileges !== null && !privileges.edit;
	}

	function isExportHidden(key, options)
	{
		if (isWorkflow(options))
			return true;
		var module = getModuleSettings(options);
		if (!module.has_export)
		{
			return true;
		}
		return !UserDataHelper.isAdmin();
	}

	function viewOrEdit(key, options)
	{
		var $el = options.$trigger.parent();
		var node = getNode(options);
		var module = $el.closest(".module").data("module");
		var params = {};
		var key = node.key;
		if (node.data.workflow)
		{
			key = 0;
			params = {
				draft : true,
				DID : node.data.draftId
			};
		}
		if (node.folder)
		{
			WidgetHelper.addWidgetForModuleCategoryId(params, module.module_name, key, node.parent.key);
		}
		else
		{
			WidgetHelper.addWidgetForModuleElementId(params, module.module_name, key, node.parent.key);
		}
	}

	function configureModule(key, options)
	{
		var $el = options.$trigger.parent();
		var module = $el.closest(".module").data("module");

		ModalHelper.launchModal(context + '/webdirector/secure/module/configure/settings?moduleName=' + module.module_name);
	}

	function bulkEdit(key, options)
	{
		var $el = options.$trigger.parent();
		var node = getNode(options);
		var module = $el.closest(".module").data("module");
		if (key != 'e')
		{
			WidgetHelper.addWidgetForBulkEditCategories({
				all : true,
				show : key
			}, module.module_name, node.key);
		}
		else
		{
			WidgetHelper.addWidgetForBulkEditElements({
				all : true,
				show : key
			}, module.module_name, node.key);
		}
	}

	function insertCategoryOrElement(key, options)
	{
		var $el = options.$trigger.parent();
		var node = getNode(options);
		var module = $el.closest(".module").data("module");
		if (node.data.folderLevel < module.levels)
		{
			WidgetHelper.addWidgetForInsertCategory({}, module.module_name, node.key);
		}
		else
		{
			WidgetHelper.addWidgetForInsertElement({}, module.module_name, node.key);
		}
	}

	function insertFirstLevelCategory(key, options)
	{
		var $el = options.$trigger.parent().find(".root-category");
		var node = getNode(options);
		var module = $el.closest(".module").data("module");
		WidgetHelper.addWidgetForInsertCategory({}, module.module_name, node.key);
	}

	function deleteCategoryOrElement(key, options)
	{
		var $el = options.$trigger.parent();
		var node = getNode(options);
		var module = $el.closest(".module").data("module");
		var isCategory = node.folder;


		$.SmartMessageBox({
			title : "<i class=\"fa fa-trash-alt text-warning\" style=\"font-size:1.2em;\"></i> Delete " + (isCategory ? "Category" : "Asset"),
			content : "Are you sure you want to delete " + node.title + (isCategory ? " and all its children" : "") + "... permanently?",
			buttons : '[No][Yes]'
		}, function(ButtonPressed)
		{
			if (ButtonPressed !== "Yes")
			{
				return;
			}
			if (DynamicFormValidator && DynamicFormValidator[(isCategory ? 'Categories' : 'Elements')] && DynamicFormValidator[(isCategory ? 'Categories' : 'Elements')][module.module_name])
			{
				var foo = DynamicFormValidator[(isCategory ? 'Categories' : 'Elements')][module.module_name](node.key);
				if (foo.onDelete)
				{
					var result = foo.onDelete();
					if (typeof result == 'string')
					{
						showNotification({
							success : false,
							content : "Could not delete " + node.title + "<br>Reason: " + result
						});
						return;
					}
					if (typeof result == 'object')
					{
						showNotification({
							success : false,
							content : "Could not delete " + node.title
						});
						return;
					}
				}
			}
			var openLazyTree = $(".open .lazytree");
			showLoadingBackground(openLazyTree);
			$.ajax({
				url : isCategory ? context + "/webdirector/folderTree/deleteCategory" : context + "/webdirector/folderTree/deleteElement",
				data : {
					moduleName : module.module_name,
					id : node.key
				},
				type : "POST"
			}).done(function(success)
			{
				if (success)
				{
					showNotification({
						success : success,
						content : node.title + " deleted... permanently."
					});
				}
				else
				{
					showNotification({
						success : false,
						content : "Could not delete " + node.title
					});
				}
				hideLoadingBackground(openLazyTree, function()
				{
					TreeHelper.reloadNode(node.parent, false);
				})
			});
		});
	}

	function sortCategoryOrElement(key, options)
	{
		var $el = options.$trigger.parent();
		var node = getNode(options);
		var module = $el.closest(".module").data("module");
		ModalHelper.launchModal(context + '/webdirector/folderTree/sort-data?moduleName=' + module.module_name + '&parentId=' + node.parent.key + '&isCategory=' + (node.folder === true));
	}

	function cutOrCopy(key, options)
	{
		resetCheckboxes();
		var $el = options.$trigger.parent();
		var node = getNode(options);
		var module = $el.closest(".module").data("module");

		$(node.li).parent().addClass("show-checkboxes");
		node.setSelected();
		lastAction = {
			type : key,
			module : module.module_name,
			parentNode : node.parent,
			isCategory : node.folder
		};
		setLastAction(lastAction);
	}

	function cutOrCopyAll(key, options)
	{
		resetCheckboxes();
		var $el = options.$trigger.parent();
		var node = getNode(options);
		var module = $el.closest(".module").data("module");

		$(node.li).parent().addClass("show-checkboxes");
		$.each(node.parent.getChildren(), function(index, item)
		{
			item.setSelected();
		});

		lastAction = {
			type : key,
			module : module.module_name,
			parentNode : node.parent,
			isCategory : node.folder
		};
		setLastAction(lastAction);
	}

	function isCancelHidden(key, options)
	{
		var $el = options.$trigger.parent();
		var module = $el.closest(".module").data("module");
		var lastAction = getLastAction();
		return !lastAction || lastAction.module !== module.module_name;
	}

	function isPasteHidden(key, options)
	{
		var privileges = getPrivileges(options);
		var $el = options.$trigger.parent();
		var module = $el.closest(".module").data("module");
		var lastAction = getLastAction();
		return !lastAction || lastAction.module !== module.module_name || (privileges !== null && !privileges.insert);
	}

	function isPasteDisabled(key, options)
	{
		console.log(key, options);
		var node = getNode(options);
		var lastAction = getLastAction();
		return !lastAction || lastAction.parentNode.data.folderLevel !== node.data.folderLevel || !hasSelected(lastAction.parentNode);
	}

	function cancelLastAction(key, options)
	{
		resetCheckboxes();
	}

	function pasteCategoryOrElement(key, options)
	{
		var $el = options.$trigger.parent();
		var node = getNode(options);
		var module = $el.closest(".module").data("module");
		var lastAction = getLastAction();
		var lastNodeId = lastAction.parentNode.key;
		var parentId = node.key;
		var urlPath;
		if (lastAction.type === "copy" || lastAction.type === "copyAll")
		{
			urlPath = lastAction.isCategory ? "copyCategories" : "copyElements";
		}
		else if (lastAction.type === "cut" || lastAction.type === "cutAll")
		{
			urlPath = lastAction.isCategory ? "moveCategories" : "moveElements";
		}

		var openLazyTree = $(".open .lazytree");
		showLoadingBackground(openLazyTree);
		$.ajax({
			url : context + '/webdirector/folderTree/' + urlPath,
			data : {
				items : getSelectedItems(lastAction.parentNode),
				parentId : node.key,
				moduleName : module.module_name
			},
			type : "POST"
		}).done(function(response)
		{
			if (response.success)
			{
				showNotification({
					success : response.success,
					title : 'Success',
					content : response.message + "<br>" + "Target: " + node.title
				});
			}
			else
			{
				showNotification({
					success : response.success,
					title : 'Error',
					content : response.message + "<br>" + "Target: " + node.title
				});
			}
			hideLoadingBackground(openLazyTree, function()
			{
				TreeHelper.reloadNode(lastAction.parentNode, false);
				TreeHelper.reloadNode(node, false);
				// RELOADING AFFECTED BULK EDITORS.
				if (lastAction.isCategory)
				{
					var oldWidgetId = WidgetHelper.widgetIdForCategoryList(lastAction.module, lastNodeId);
					var newWidgetId = WidgetHelper.widgetIdForCategoryList(lastAction.module, parentId);
				}
				else
				{
					var oldWidgetId = WidgetHelper.widgetIdForElementList(lastAction.module, lastNodeId);
					var newWidgetId = WidgetHelper.widgetIdForElementList(lastAction.module, parentId);
				}
				var $oldWidget = $('[id^="' + oldWidgetId + '"]');
				var $newWidget = $('[id^="' + newWidgetId + '"]');
				if ($oldWidget.length > 0)
				{
					BulkEditor.reloadBulkEditor($oldWidget);
				}
				if ($newWidget.length > 0)
				{
					BulkEditor.reloadBulkEditor($newWidget);
				}
				resetCheckboxes();

				// /WIDGETHELPER DIALOGBOX
				// /
			});
		});
	}

	function collapseAll(key, options)
	{
		var node = getNode(options);
		node.visit(function(child)
		{
			child.setExpanded(false);
		});
		if (node.key != '0')
		{
			node.setExpanded(false);
		}
	}

	function refreshModule(key, options)
	{
		var $el = options.$trigger;
		var module = $el.closest(".module").data("module");
		TreeHelper.reloadChildren(module.module_name, "0", true);
	}

	function refresh(key, options)
	{
		var node = getNode(options);
		if (node.folder)
		{
			TreeHelper.reloadNode(node, true);
		}
		else
		{
			TreeHelper.reloadNode(node.parent, true);
		}

	}

	function importData(key, options)
	{
		var $el = options.$trigger;
		var module = $el.closest(".module").data("module");
		ModalHelper.loadInIframe(context + '/webdirector/secure/module/iframe/import-data?moduleName=' + module.module_name + '&moduleDescription=' + module.module_description);
	}

	function exportData(key, options)
	{
		var $el = options.$trigger.parent();
		var node = getNode(options);
		var module = $el.closest(".module").data("module");
		ModalHelper.loadInIframe(context + '/webdirector/secure/module/iframe/export-elements?module=' + module.module_name + '&categoryId=' + node.key);
	}

	function closeModule(key, options)
	{
		var $el = options.$trigger;
		var module = $el.parent().data("module");
		ModuleHelper.removeModule(module.module_name);
	}

	function resetCheckboxes()
	{
		var $parent = $(".show-checkboxes");
		$parent.each(function(index, parent)
		{
			var node = $.ui.fancytree.getNode(parent);
			$.each(node.getChildren(), function(index, item)
			{
				item.setSelected(false);
			});
			$(parent).removeClass("show-checkboxes");
		});
		setLastAction(null);
	}

	function hasSelected(node)
	{
		var items = node.getChildren();
		for (var i = 0; i < items.length; i++)
		{
			if (items[i].selected)
				return true;
		}
		return false;
	}

	function getSelectedItems(node)
	{
		var items = node.getChildren();
		var selectedItems = [];
		for (var i = 0; i < items.length; i++)
		{
			if (items[i].selected)
				selectedItems.push(items[i].key);
		}
		return selectedItems;
	}

	function showLoadingBackground(openLazyTree)
	{
		var $background = $('<div class="loading-background" style="display: none"><span><i class="fa fa-spinner fa-pulse fa-3x"></i></span></div>');
		openLazyTree.append($background);
		$background.fadeIn();
	}

	function hideLoadingBackground(openLazyTree, callback)
	{
		$(".loading-background", openLazyTree).fadeOut(200, function()
		{
			$(this).remove();
			if (callback && $.isFunction(callback))
			{
				callback();
			}
		});
	}

});
