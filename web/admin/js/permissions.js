/*
 * C - 8
 * R - 4
 * U - 2
 * D - 1
 */
var Permissions = {
    has: function(value, check){
      return (value&check)==check
    },

    hasCreate: function(value){
      return Permissions.has(value, 8);
    },

    hasRead: function(value){
      return Permissions.has(value, 4);
    },

    hasUpdate: function(value){
      return Permissions.has(value, 2);
    },

    hasDelete: function(value){
      return Permissions.has(value, 1);
    }
};

