function showNotification(settings, callback) {
  if (settings.success) {
    $.smallBox({
      title : settings.title || "Success",
      content : settings.content || "",
      color : "#659265",
      iconSmall : "fa fa-check fa-2x pulse animated infinite",
      timeout : 4000,
      container : settings.container || "#content",
      extraClasses : "animated fadeInRight fast"
    }, callback);
  } else {
    $.smallBox({
      title : settings.title || "Error",
      content : settings.content || "",
      color : "#C46A69",
      iconSmall : "fa fa-times fa-2x pulse animated infinite",
      timeout : 6000,
      container : settings.container || "#content",
      extraClasses : "animated fadeInRight fast"
    }, callback);
  }
}
