var previousActive;

var FullWidthHelper = {
  initModal : function($modal, height) {
    this.$modal = $modal;
    this.$content = $modal.children('.modal-dialog').children(".modal-content");
    this.height = height;
  },
  launchModal : function(url, notification, callback) {
    var $modal = this.$modal;
    this.$content.css("height", this.height);
    this.loadContent(url, notification, function() {
      $modal.modal();
      if ($.isFunction(callback)) {
        callback();
      }
    });
  },
  closeModal : function(callback) {
    var $modal = this.$modal;
    $modal.modal("hide");
    if ($.isFunction(callback)) {
      callback();
    }
  },
  loadContent : function(url, notification, callback) {
    var $content = this.$content;
    $.get(url).done(function(html) {
      var $html = $(html);
      var $body = $html.filter(".modal-body");
      if ($body.length === 0) {
        $body = $html.find(".modal-body");
      }
      $body.css("height", $content.height() - 131);
      $content.empty().append($html);
      if (notification)
        ModalHelper.showNotification(notification);

      if ($.isFunction(callback)) {
        callback();
      }
    });
  },
  loadInIframe : function(url) {
    var $content = this.$content;
    var $iframe = $('<iframe>');
    this.$modal.find('.modal-dialog').css('top', $("#body").offset().top - 1);
    $content.css("height", "100%");
    $content.empty().append($iframe);
    this.$modal.modal({backdrop:true,keyboard: true});
    this.$modal.on('shown.bs.modal', function(e){
    	$iframe.attr('src', url);
    });
    this.$modal.on('hidden.bs.modal', function(e){
    	$('#header .nav .active').removeClass('active');
    	previousActive.addClass('active');
    })
    $iframe.load(function() {
      $('.modal-body', $iframe.contents()).css("height", $content.height());
    });
  },
  loadInStaticIframe : function(url) {
    var $content = this.$content;
    var $iframe = $('<iframe src="' + url + '"></iframe>');
    this.$modal.find('.modal-dialog').css('top', $("#body").offset().top - 1);
    $content.css("height", "100%");
    $content.empty().append($iframe);
    $iframe.load(function() {
      $('.modal-body', $iframe.contents()).css("height", $content.height() - 131);
    });
    this.$modal.modal({backdrop:'static',keyboard: false});
  },
  slideOpenPanel : function(url, data) {
    var $body = $(document.body);
    var $panel = $(".htmlFragments .modal-panel").clone();
    var $backdrop = $panel.find(".modal-panel-backdrop");
    var $content = $panel.find(".modal-panel-content");
    $body.append($panel);
    $content.load(url, data, function() {
      $backdrop.addClass("in");
      $panel.find(".modal-panel-content").addClass("open");
      $backdrop.on("mousedown", function() {
        ModalHelper.slideClosePanel($panel);
      });
    });
  },
  slideClosePanel : function($panel) {
    var $backdrop = $panel.find(".modal-panel-backdrop");
    var $content = $panel.find(".modal-panel-content");
    $backdrop.removeClass("in");
    $content.removeClass("open");
    $panel.one($.support.transition.end, function() {
      $panel.remove();
    }).emulateTransitionEnd(300);
  },
  showNotification : function(settings) {
    settings = $.extend({
      container: ".modal-body"
    }, settings);
    
    if (settings.success) {
      $.smallBox({
        title : settings.title || "Success",
        content : settings.content,
        color : "#659265",
        iconSmall : "fa fa-check fa-2x fadeInRight animated",
        timeout : 4000,
        container: settings.container
      });
    } else {
      $.smallBox({
        title : settings.title || "Error",
        content : settings.content,
        color : "#C46A69",
        iconSmall : "fa fa-times fa-2x fadeInRight animated",
        timeout : 4000,
        container: settings.container
      });
    }
  }
};

$(document).ready(function(){
	FullWidthHelper.initModal($("#modal-fullwidth"), $(window).height() * 0.9, window.innerWidth);
	
	$('#header .nav li').click(function(){
		changeTab($(this));
	})
})

function changeTab(element) {
	previousActive = $("#header .nav").find('.active');
	previousActive.removeClass('active');
	element.addClass('active');
}

function openFileManager() {
	var urlString = '/webdirector/adminExtra/filemanager/index?fileRoot=/&langCode=en&time=' + (+new Date)
    FullWidthHelper.loadInIframe(urlString);
}