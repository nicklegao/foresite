var moduleCreateDelete = false;
$(document).ready(function()
{

	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();

	ModalHelper.initModal($("#modal-setting"), $(window).height() * 0.9);
	$('#modal-setting').on('hidden.bs.modal', function()
	{
		console.log('modal closed');
		if (moduleCreateDelete)
		{
			location.reload();
		}
	});
	StorageHelper.initStorage();

	$('#demo-setting').click(function()
	{
		$('.demo').toggleClass('activate');
	});

	var leftPanel = $("#left-panel");
	var mainPanel = $("#main");
	var fixedRibbon = $(".fixed-ribbon #ribbon")

	if (wdLocalStorage.getItem("sm-setmenu") != "top")
	{
		leftPanel.resizable({
			minWidth : 220,
			start : function()
			{
				leftPanel.addClass("no-transition");
			},
			resize : function(event, ui)
			{
				mainPanel.css("margin-left", (ui.size.width + 156) + "px");
				fixedRibbon.css("left", ui.size.width + "px");
				$(this).find('.slimScrollDiv, .slimScrollDiv section').css('width', ui.size.width);
			},
			stop : function()
			{
				leftPanel.removeClass("no-transition");
			}
		});
	}


	var shortcut_dropdown = $("#shortcut");
	$("#show-shortcut").on("click", function()
	{
		// if (shortcut_dropdown.is(":visible")) {
		// shortcut_buttons_hide();
		// } else {
		// shortcut_buttons_show();
		// }
		shortcut_buttons_show();
	});

	$("#favourite-modules-btn").on("click", function()
	{
		if ($(this).find('i').hasClass('fa-arrow-up'))
		{
			$('#shortcut ul li:not(.favourite)').fadeOut();
		}
		else
		{
			$('#shortcut ul li:not(.favourite)').fadeIn();
		}
		$(this).find('i').toggleClass('fa-arrow-up').toggleClass('fa-arrow-down');
	});

	// shortcut_dropdown.on("click", function(e) {
	// e.preventDefault();
	// shortcut_buttons_hide();
	// });

	shortcut_dropdown.on("click", "a", function(e)
	{
		e.preventDefault();
		var target = $(e.currentTarget);
		target.parent().siblings().find('a.selected').removeClass('selected');
		if (target.hasClass("selected"))
		{
			target.removeClass('selected');
			$('#main').removeAttr('style');
			$('body').addClass('module-inactive');
			ModuleHelper.removeModule($(this).data("module").module_name);
			target.closest('li').removeClass('favourite');

		}
		else
		{
			target.addClass("selected");
			$('body').removeClass('module-inactive');
			$('body').removeClass('xs-module-selector');
			$('body').addClass('xs-category-selector');
			ModuleHelper.addModule($(this).data("module"));

			target.closest('li').addClass('favourite');

			if ($(window).width() > 979)
				$('#main').attr('style', 'margin-left:' + ($('#left-panel').outerWidth() + $('#shortcut').outerWidth()) + 'px !important');
		}
		// shortcut_buttons_hide();
	});

	// SHORTCUT ANIMATE HIDE
	function shortcut_buttons_hide()
	{
		shortcut_dropdown.animate({
			height : "hide"
		}, 300, "easeOutCirc");
		$.root_.removeClass('shortcut-on');
	}

	$("#left-panel").find("nav").children("ul").on("click", ".module > a", function(e)
	{
		e.preventDefault();
		var li = $(this).closest(".module");
		var data = li.data("module");

		if (li.hasClass("open"))
		{
			li.removeClass("open");
			li.find("section").slideUp();
			StorageHelper.setActiveModule(false);
		}
		else
		{
			$(this).closest("ul").children("li").each(function(index, item)
			{
				if ($(this).hasClass("open"))
				{
					$(this).removeClass("open");
					$(this).find("section").slideUp();
				}
			});
			if (li.hasClass("initialised"))
			{
				// existing lazy tree
				setConfigureModule(data);
				li.addClass("open");
				li.find("section").slideDown();
			}
			else
			{
				// existing new tree
				setConfigureModule(data);
				TreeHelper.createTree(data);
			}
		}

		if ($(e.target).is("i"))
		{
			return;
		}
		// icon not clicked... do other stuff
		// WidgetHelper.addWidgetForBulkEditCategories({}, data.module_name, 0);
	});

	// restore state
	WidgetHelper.restoreWidgetState();

	setTimeout(function()
	{
		shortcut_buttons_show();
	}, 200);

	$(document).on('click', '.back-to-module', function()
	{
		$('body').addClass('xs-module-selector');
		$('body').removeClass('xs-category-selector');
	});
	$('.fixed-header #shortcut').slimScroll({
		height : ($(this).outerHeight() - 64),
		width : '156px',
		railVisible : true,
		railColor : '#ededed',
	});
	$('.fixed-header #shortcut').mouseover();
});

// SHORTCUT ANIMATE SHOW
function shortcut_buttons_show()
{
	var storage = wdLocalStorage.getItem('openModules');
	var openModules = (storage == undefined) ? [] : JSON.parse(storage);
	$.getJSON(context + "/webdirector/module/assignedModules", function(data)
	{
		var ul = shortcut_dropdown.find("ul");
		ul.empty();
		$.each(data, function(index, item)
		{
			var li = $("<li></li>");
			var a = $('<a class=""> <span class="iconbox"> <i class="fa-lg"></i> <span></span> </span> </a>');
			a.find(".iconbox span").text(item.module_description);
			a.attr("href", "#");
			a.addClass(item.color);
			a.find("i").addClass(item.icon);
			a.data("module", item);
			if ($.inArray(item.module_name, openModules) >= 0)
			{
				li.addClass('favourite');
			}
			var activeModule = StorageHelper.getActiveModule();
			if (activeModule == item.module_name)
			{
				a.addClass("selected");
			}
			/* a.data("module-desc", item.module_description); */
			li.append(a).appendTo(ul);
		});

		shortcut_dropdown.animate({
			height : "show"
		}, 300, "easeOutCirc");
		$.root_.addClass('shortcut-on');
	});

}

// Kept to make new configureModule function work
function setConfigureModule(module)
{
	var link = $("#configure-module");
	link.attr("href", context + '/webdirector/secure/module/configure/settings?moduleName=' + module.module_name);
	link.find("i").removeClass().addClass(module.icon);
	link.find("span").text("Configure " + module.module_description);
	/*
	 * if (module) { link.css("display", "block"); } else { link.css("display",
	 * "none"); }
	 */
	StorageHelper.setActiveModule(module.module_name);
}

function resetCheckboxes()
{
	var $parent = $(".show-checkboxes");
	$parent.each(function(index, parent)
	{
		var node = $.ui.fancytree.getNode(parent);
		$.each(node.getChildren(), function(index, item)
		{
			item.setSelected(false);
		});
		$(parent).removeClass("show-checkboxes");
	});
	lastAction = null;
}

function initTree($element, data, source, callback)
{
	var internalName = data.module_name;
	var displayName = data.module_description;
	var levels = data.levels;
	var filterByPrivileges = data.filter_by_privileges == true;// will always
	// be false for
	// admins

	var expandedNodes = null;
	if (!$element.hasClass("search-tree"))
	{
		expandedNodes = StorageHelper.getExpandedNodes(internalName);
		StorageHelper.resetExpandedNodes(internalName);
	}

	$element.fancytree({
		init : callback,
		extensions : [ "glyph" ],
		glyph : {
			map : {
				doc : "fa fa-file",
				docOpen : "fa fa-file",
				checkbox : "fal fa-square",
				checkboxSelected : "fal fa-check-square",
				checkboxUnknown : "fal fa-share-square",
				error : "fa fa-exclamation-triangle",
				expanderClosed : "fa fa-plus",
				expanderLazy : "fa fa-plus",
				expanderOpen : "fa fa-minus",
				folder : "fa fa-folder",
				folderOpen : "fa fa-folder-open",
				loading : "fa fa-spinner fa-pulse"
			}
		},
		checkbox : true,
		source : source || $.ajax(context + '/webdirector/folderTree/initTree', {
			dataType : "json",
			data : {
				moduleName : internalName,
				levels : levels,
				filterByPrivileges : filterByPrivileges,
				expandedNodes : JSON.stringify(expandedNodes)
			}
		}),
		lazyLoad : function(event, data)
		{ // when expanded
			var node = data.node;
			var expandedNodes = StorageHelper.getExpandedNodes(internalName);
			StorageHelper.resetExpandedChildren(internalName, node.key);
			// Get the page where we are at
			var lastPageNum = node.data.pageNum;
			data.result = $.ajax(context + "/webdirector/folderTree/lazyload", {
				dataType : "json",
				data : {
					moduleName : internalName,
					levels : levels,
					curLevel : node.data.folderLevel,
					parentId : node.key,
					pageNum : 0,
					pageTo : lastPageNum,
					filterByPrivileges : filterByPrivileges,
					expandedNodes : JSON.stringify(expandedNodes)
				}
			});
		},
		createNode : $element.hasClass("search-tree") ? null : function(event, data)
		{
			var node = data.node;
			if (node.folder && node.expanded && !node.lazy && node.data.parentId !== null)
			{
				StorageHelper.expandNode(internalName, node.data.parentId, node.key);
			}
		},
		expand : $element.hasClass("search-tree") ? null : function(event, data)
		{
			var node = data.node;
			StorageHelper.expandNode(internalName, node.data.parentId, node.key);
		},
		collapse : $element.hasClass("search-tree") ? null : function(event, data)
		{
			var node = data.node;
			StorageHelper.collapseNode(internalName, node.data.parentId, node.key);
		},
		click : function(event, data)
		{ // when a item clicked
			var node = data.node;
			var parent = node.getParent();

			if (data.targetType == "icon" && node.folder)
			{
				node.toggleExpanded();
				return false;
			}

			if (data.targetType == "expander" || data.targetType == "checkbox")
			{
				return true;
			}

			if (node.key === parent.key + '-load')
			{// Load more
				if (parent.data.pageNum)
				{
					parent.data.pageNum++;
				}
				else
				{
					parent.data.pageNum = 1;
				}

				$.ajax(context + '/webdirector/folderTree/lazyload', {
					dataType : "json",
					data : {
						moduleName : internalName,
						levels : levels,
						curLevel : node.parent.data.folderLevel,
						parentId : parent.key,
						pageNum : parent.data.pageNum,
						filterByPrivileges : filterByPrivileges
					},
					success : function(treeData)
					{
						$.map(treeData, function(item, index)
						{
							item.extraClasses = item.extraClasses + " fade-in";
						});
						var ul = $(parent.ul);
						var curHeight = ul.css("height");
						ul.css("maxHeight", curHeight);

						node.remove();
						parent.addChildren(treeData);

						ul.css("height", curHeight);
						ul.css("maxHeight", "none");

						ul.animate({
							"height" : ul[0].scrollHeight
						}, 200, function()
						{
							ul.css("height", "auto");
						});
						ul.find(".fade-in").animate({
							"opacity" : "1"
						}, 200, function()
						{
							$(this).removeClass("fade-in");
						});
					}
				});
			}
			else if (node.folder === false)
			{ // Element clicked
				if (node.data.workflow)
				{
					var params = {
						draft : true,
						DID : node.data.draftId
					};
					WidgetHelper.addWidgetForModuleElementId(params, internalName, "0", node.parent.key);
				}
				else
				{
					WidgetHelper.addWidgetForModuleElementId({}, internalName, node.key, node.parent.key);
				}
			}
			else
			{ // Category clicked
				// var search =
				// $(event.currentTarget).closest(".module").find(".tree-search-box
				// input").val();
				// if (levels == node.data.folderLevel)
				// {
				// //Display list of elements.
				// var $widget = WidgetHelper.addWidgetForBulkEditElements({
				// search: search
				// }, internalName, data.node.key);
				// }
				// else
				// {
				// //Display list of categories.
				// var $widget = WidgetHelper.addWidgetForBulkEditCategories({
				// search: search
				// }, internalName, data.node.key);
				// }
				// Edit category by default;
				if (node.data.workflow)
				{
					var params = {
						draft : true,
						DID : node.data.draftId
					};
					WidgetHelper.addWidgetForModuleCategoryId(params, internalName, "0", node.parent.key);
				}
				else
				{
					WidgetHelper.addWidgetForModuleCategoryId({}, internalName, node.key, node.parent.key);
				}
				// var $widget =
				// WidgetHelper.addWidgetForModuleCategoryId(params,
				// internalName, categoryID, parentID)
			}
		}
	});
	$element.fancytree("getTree").setFocus(true);
}

function toggleWidgetLock()
{
	var $widget = $(this).closest(".jarviswidget");
	if (WidgetHelper.widgetIsLocked($widget))
	{
		WidgetHelper.unlockWidget($widget);
	}
	else
	{
		WidgetHelper.lockWidget($widget);
	}
	WidgetHelper.saveWidgetState();
}

var TreeHelper = {
	createTree : function(module, source)
	{
		var $li = $("#left-panel").find("#li_" + module.module_name);
		$li.addClass("open initialised");

		var $treeDiv = $li.find(".lazytree");
		$treeDiv.attr("id", "tree_" + module.module_name);

		initTree($treeDiv, module, source, function()
		{
			$li.find("section").slideDown();
		});
		// $treeDiv.parent().slideDown();
	},
	breadcrumbForNode : function(node)
	{
		var currentNode = node;
		var breadcrumb = currentNode.title; // default
		while (currentNode != undefined && currentNode.data.folderLevel != undefined)
		{
			if (currentNode == node)
			{
				breadcrumb = currentNode.title;
			}
			else
			{
				breadcrumb = currentNode.title + " | " + breadcrumb;
			}
			currentNode = currentNode.parent;
		}
		return breadcrumb;
	},
	reloadChildren : function(moduleName, categoryId, animate)
	{
		var $tree = $("#tree_" + moduleName);
		var node = $tree.fancytree("getFolderByKey", categoryId.toString());
		TreeHelper.reloadNode(node, animate);
	},
	reloadNode : function(node, animate, scroll)
	{
		var expanded = node.expanded;
		var top = $('#left-panel>nav ul.categories-panel').scrollTop();
		if (animate !== false)
		{
			node.resetLazy();
			node.setExpanded(expanded);
		}
		else
		{
			node.removeChildren();
			node.lazy = true;
			node.children = undefined;
			node.setExpanded(expanded);
			setTimeout(function()
			{
				node.load();
			}, 500);
		}
		setTimeout(function()
		{
			$('#left-panel>nav ul.categories-panel').scrollTop(top);
		}, 1000);
	}
};

var ModuleHelper = {
	fetchAssignedModules : function()
	{
		return $.getJSON(context + "/webdirector/module/assignedModules");
	},
	addModule : function(module, restore)
	{
		$('aside ul li.module').remove();
		/*
		 * $('aside ul li.module').each(function(){ var moduleName =
		 * $(this).data('module').module_name; $("#li_" + moduleName).remove();
		 * StorageHelper.removeModule(moduleName); });
		 */
		var getSubMenuItemWidth = function(text)
		{
			var $span = $('<span style="white-space:nowrap;">').text(text);
			var width = $span.appendTo('body').width();
			$span.remove();
			return width;
		}

		var marquee = function(a, margin)
		{
			// var time = Math.min(3000, 3000/90*-1*margin);
			var time = 3000;
			$(a).mouseover(function()
			{
				var that = $(this).find(".marquee");
				that.finish().animate({
					'margin-left' : margin + "px"
				}, time, function()
				{
					that.animate({
						'margin-left' : '0'
					}, time);
				});
				var interval = setInterval(function()
				{
					that.finish().animate({
						'margin-left' : margin + "px"
					}, time, function()
					{
						that.animate({
							'margin-left' : '0'
						}, time);
					});
				}, 6100)
				that.data('interval', interval);
			})

			$(a).mouseout(function()
			{
				var that = $(this).find(".marquee");
				if (that.data('interval'))
				{
					clearInterval(that.data('interval'));
				}
				that.removeData('interval');
				that.finish();
			})

		}
		var getSubMenuWidth = function()
		{
			if ($('body').hasClass('mobile-view-activated'))
			{
				return $('#left-panel').width() - 43;
			}
			var dummyLi = $('<li>')
			var dummyUl = $('<ul id="newUl">').appendTo(dummyLi);
			dummyLi.appendTo($('#left-panel > nav ul.categories-panel'))
			var width = dummyUl.outerWidth();
			dummyLi.remove();
			return width - 28;
		};

		var subMenuWidth = getSubMenuWidth();
		if ($("#left-panel").find("#li_" + module.module_name).length === 0)
		{
			var menuOnTop = wdLocalStorage.getItem("sm-setmenu") == "top";
			var $li = $(".htmlFragments .module").clone();
			$li.prepend('<li class="fa-lg fa-fw fa fa-long-arrow-left back-to-module">');
			$li.attr("id", "li_" + module.module_name)
			if (!menuOnTop)
			{
				$li.addClass(module.color);
			}
			$li.find(".menu-item-parent").text(module.module_description);
			$li.data("module", module);

			var $a = $li.children("a");
			if (!menuOnTop)
			{
				$a.addClass(module.color);
			}
			$a.find("i").addClass(module.icon);

			$("#left-panel").find("nav").find("ul.categories-panel").append($li);

			setTimeout(function()
			{
				var width = $li.closest('aside#left-panel').outerWidth();
				$li.find("section").slimScroll({
					height : ($(this).outerHeight() - 72),
					width : width,
					railVisible : true,
					railColor : '#ededed',
				});
				$li.find("section").mouseover();
			}, 1000);

			if (!menuOnTop)
			{
				ModuleHelper.initSearchBox($li, module);
			}
			StorageHelper.addModule(module, restore);

			if (menuOnTop)
			{
				// remove a's siblings in li
				$('.tree-search-icon', $li).remove();
				$('section', $li).remove();

				// load first category as sub menu
				$.ajax(context + '/webdirector/folderTree/initTree', {
					dataType : "json",
					data : {
						moduleName : module.module_name,
						levels : module.levels,
						filterByPrivileges : module.filter_by_privileges == true
					}
				}).done(function(data)
				{
					var $ul = $('<ul>').appendTo($li);
					if (data[0] && data[0].children && data[0].children.length > 0)
					{
						for (var i = 0; i < data[0].children.length; i++)
						{
							var child = data[0].children[i];
							if (child.workflow)
							{
								continue;
							}
							var $submenu = $('<li>').appendTo($ul);
							var $a = $('<a>').attr('href', 'javascript:void(0);').appendTo($submenu);
							var $span = $('<span>').addClass('submenu-title').text(child.title).appendTo($a);
							var itemWidth = getSubMenuItemWidth(child.title)
							if (itemWidth > subMenuWidth)
							{
								$span.addClass('marquee');
								marquee($span.parent().closest('a'), (subMenuWidth - itemWidth));
							}
							$a.data('cid', child.key).data('last-folder-level', module.levels == child.folderLevel).click(function()
							{
								if ($(this).data('last-folder-level'))
									WidgetHelper.addWidgetForBulkEditElements({}, module.module_name, $(this).data('cid'));
								else
									WidgetHelper.addWidgetForBulkEditCategories({}, module.module_name, $(this).data('cid'));
							})
						}
					}
					var $submenu = $('<li>').appendTo($ul);
					var $a = $('<a>').addClass('close-module btn btn-raised btn-default btn-block').attr('href', 'javascript:void(0);').html('<span class="pull-right">close <i class="fa fa-times"></i></span>').appendTo($submenu);
					$a.click(function()
					{
						ModuleHelper.removeModule(module.module_name);
					})
				})
			}
		}
	},
	removeModule : function(moduleName)
	{
		$("#li_" + moduleName).remove();
		StorageHelper.removeModule(moduleName);
	},
	initSearchBox : function($li, module)
	{
		$li.find(".tree-search-icon").click(function(e)
		{
			e.stopPropagation();
			$li.find(".tree-search-box").slideToggle();
			if ($li.hasClass("search-active"))
			{
				$li.find(".tree-search-box input").val("");
				$li.removeClass("search-active");
			}
		});
		var radioName = "search-option-" + module.module_name;
		$li.find("input[type='radio']").attr("name", radioName).change(function()
		{
			var $this = $(this);
			$this.closest(".tree-search-box").data("search-mode", $this.data("search-mode"));
		});
		$li.find(".clear-search").click(function()
		{
			$li.find(".tree-search-box input").val("");
			$li.removeClass("search-active");
		});
		$li.find(".tree-search-box input").keypress(function(e)
		{
			var $section = $(this).closest("section");
			var key = e.which;
			if (key == 13)
			{
				var filterText = $(this).val();
				if (!filterText)
				{
					$li.find(".tree-search-box input").val("");
					$li.removeClass("search-active");
					return;
				}
				$.ajax({
					url : context + '/webdirector/folderTree/filterTree',
					type : 'GET',
					data : {
						moduleName : module.module_name,
						levels : module.levels,
						filterByPrivileges : module.filter_by_privileges == true,
						filterText : filterText,
						searchMode : $(this).parent().data("search-mode")
					}
				}).done(function(data)
				{
					var $searchTree = $section.find(".search-tree");
					if ($searchTree.children().length > 0)
					{
						$searchTree.empty().fancytree("destroy");
					}
					$section.parent().addClass("search-active");
					initTree($searchTree, module, data);
				});
			}
		});
	},
	expandModuleTo : function($widget, module, id, expandMode, callback)
	{
		var moduleName = module.module_name;
		var expandedNodes = null;
		expandedNodes = StorageHelper.getExpandedNodes(moduleName);
		$.ajax({
			url : context + '/webdirector/folderTree/expandTree',
			type : 'GET',
			data : {
				moduleName : moduleName,
				id : id,
				levels : module.levels,
				expandedNodes : JSON.stringify(expandedNodes),
				filterByPrivileges : module.filter_by_privileges == true,
				expandMode : expandMode
			}
		}).done(function(data)
		{
			var $module = $("#li_" + moduleName);
			StorageHelper.setActiveModule(moduleName);
			// Removing open class so it doesn't slide up
			$module.removeClass("open");
			// Closing all other open modules
			$module.closest("ul").children("li").each(function(index, item)
			{
				if ($(this).hasClass("open"))
				{
					$(this).removeClass("open");
					$(this).find("section").slideUp();
				}
			});
			$module.find("section").slideDown();
			// Adding open class back
			$module.addClass("open");
			var $lazyTree = $module.find(".lazytree");
			$lazyTree.attr("id", "tree_" + moduleName);
			if ($lazyTree.children().length > 0)
			{
				$lazyTree.empty().fancytree("destroy");
			}
			initTree($lazyTree, module, data)
			callback($widget, module, id, expandMode);
		});
	},
	selectCategoriesForAction : function(module, key, selectedItems, parentId)
	{
		resetCheckboxes();
		var node = $("#tree_" + module.module_name).fancytree("getTree").getNodeByKey("0");
		if (selectedItems.length > 0)
		{
			var category = ModuleHelper.findInModule(node, parentId, selectedItems[0].category_id);
		}
		if (category != null)
		{
			$(category.li).parent().addClass("show-checkboxes");

			selectedItems.forEach(function(item)
			{
				var element = ModuleHelper.findInModule(node, parentId, item.category_id);
				element.setSelected();
			});

			lastAction = {
				type : key,
				module : module.module_name,
				parentNode : category.parent,
				isCategory : category.folder
			};
			setLastAction(lastAction);
		}
	},
	selectElementsForAction : function(module, key, selectedItems, parentId)
	{
		resetCheckboxes();
		var node = $("#tree_" + module.module_name).fancytree("getTree").getNodeByKey("0");
		if (selectedItems.length > 0)
		{
			var category = ModuleHelper.findInModule(node, parentId, selectedItems[0].element_id);
		}
		if (category != null)
		{
			$(category.li).parent().addClass("show-checkboxes");

			selectedItems.forEach(function(item)
			{
				var element = ModuleHelper.findInModule(node, parentId, item.element_id);
				element.setSelected();
			});

			lastAction = {
				type : key,
				module : module.module_name,
				parentNode : category.parent,
				isCategory : category.folder
			};
			setLastAction(lastAction);
		}
	},
	findInModule : function(node, parentId, elementId)
	{
		if (node.key == elementId && node.getParent().key == parentId)
		{
			// WE FOUND IT
			return node;
		}
		if (node.getFirstChild() != null)
		{
			// Loop through first child
			var nextChild = node.getFirstChild();
			var result = ModuleHelper.findInModule(nextChild, parentId, elementId);
		}
		if (result == null)
		{
			// Loop through siblings
			if (node.getNextSibling() != null)
			{
				var nextChild = node.getNextSibling();
				result = ModuleHelper.findInModule(nextChild, parentId, elementId);
			}
			// If nothing is found return null
			return result;
		}
		else
		{
			// Returns something not null
			return result;
		}
	},
	moduleIsOpen : function(module)
	{
		return $(".module#li_" + module).size() > 0
	},
	nodeIsExpanded : function(module)
	{
		$(".fancytree-expanded").find(".fancytree-title").each(function()
		{
		})
	}
};

var StorageHelper = {
	initStorage : function()
	{
		this.treeKey = "treeExpandedNodes";
		this.openModulesKey = "openModules";
		this.activeModuleKey = "activeModule";
		if (!wdLocalStorage.getItem(this.treeKey))
		{
			wdLocalStorage.setItem(this.treeKey, JSON.stringify({}));
		}
		this.restoreOpenModules();
	},
	addModule : function(module, restore)
	{
		var openModules = JSON.parse(wdLocalStorage.getItem(this.openModulesKey)) || [];
		// if ($.inArray(module.module_name, openModules) < 0) {
		openModules.push(module.module_name);
		wdLocalStorage.setItem(this.openModulesKey, JSON.stringify(openModules));
		if (!restore)
		{
			StorageHelper.restoreActiveModuleTree(module);
		}
		// }
	},
	removeModule : function(moduleName)
	{
		if (this.getActiveModule() === moduleName)
			this.setActiveModule(false);
		var openModules = JSON.parse(wdLocalStorage.getItem(this.openModulesKey)) || [];
		var index = openModules.indexOf(moduleName);
		openModules.splice(index, 1);
		wdLocalStorage.setItem(this.openModulesKey, JSON.stringify(openModules));
	},
	setActiveModule : function(moduleName)
	{
		if (moduleName)
			wdLocalStorage.setItem(this.activeModuleKey, moduleName);
		else
			wdLocalStorage.removeItem(this.activeModuleKey);
	},
	getActiveModule : function()
	{
		return wdLocalStorage.getItem(this.activeModuleKey);
	},
	expandNode : function(moduleName, parentId, categoryId)
	{
		var expandedNodes = JSON.parse(wdLocalStorage.getItem(this.treeKey));
		expandedNodes[moduleName] = expandedNodes[moduleName] || {};
		expandedNodes[moduleName][parentId] = expandedNodes[moduleName][parentId] || {};
		expandedNodes[moduleName][parentId][categoryId] = true;
		wdLocalStorage.setItem(this.treeKey, JSON.stringify(expandedNodes));
	},
	collapseNode : function(moduleName, parentId, categoryId)
	{
		var expandedNodes = JSON.parse(wdLocalStorage.getItem(this.treeKey));
		var parent = expandedNodes[moduleName] && expandedNodes[moduleName][parentId];
		if (parent)
		{
			delete parent[categoryId];
		}
		wdLocalStorage.setItem(this.treeKey, JSON.stringify(expandedNodes));
	},
	restoreOpenModules : function()
	{
		$("#left-panel > nav ul.categories-panel > li:not('.always-show')").remove();
		var menuOnTop = wdLocalStorage.getItem("sm-setmenu") == "top";
		var openModules = JSON.parse(wdLocalStorage.getItem(this.openModulesKey)) || [];
		wdLocalStorage.removeItem(this.openModulesKey);
		var activeModule = wdLocalStorage.getItem(this.activeModuleKey);
		ModuleHelper.fetchAssignedModules().done(function(assignedModules)
		{
			for (var i = 0; i < openModules.length; i++)
			{
				for (var j = 0; j < assignedModules.length; j++)
				{
					if (openModules[i] === assignedModules[j].module_name)
					{
						if ($('#shortcut ul li a.selected').data('module'))
						{
							$('#shortcut ul li a.selected').data('module').module_description = assignedModules[j].module_description;
							$('#shortcut ul li a.selected .iconbox span').text(assignedModules[j].module_description);
						}
						ModuleHelper.addModule(assignedModules[j], true);
						$('body').removeClass('module-inactive');
						if (!menuOnTop && openModules[i] === activeModule)
						{
							StorageHelper.restoreActiveModuleTree(assignedModules[j]);
						}
						// return;
					}
				}
			}
		});
	},
	restoreActiveModuleTree : function(module)
	{
		setConfigureModule(module);
		TreeHelper.createTree(module);
	},
	getExpandedNodes : function(moduleName)
	{
		var expandedNodes = JSON.parse(wdLocalStorage.getItem(this.treeKey));
		return expandedNodes && expandedNodes[moduleName];
	},
	resetExpandedNodes : function(moduleName)
	{
		var expandedNodes = JSON.parse(wdLocalStorage.getItem(this.treeKey));
		if (expandedNodes && expandedNodes[moduleName])
		{
			delete expandedNodes[moduleName];
			wdLocalStorage.setItem(this.treeKey, JSON.stringify(expandedNodes));
		}
	},
	resetExpandedChildren : function(moduleName, parentId)
	{
		var expandedNodes = JSON.parse(wdLocalStorage.getItem(this.treeKey));
		var moduleExpandedNodes = expandedNodes && expandedNodes[moduleName];
		if (moduleExpandedNodes)
		{
			(function deleteChildNodes(parentId)
			{
				if (!moduleExpandedNodes[parentId])
				{
					return;
				}
				var children = Object.keys(moduleExpandedNodes[parentId]);
				for (var i = 0; i < children.length; i++)
				{
					deleteChildNodes(children[i]);
				}
				delete moduleExpandedNodes[parentId];
			})(parentId);
			wdLocalStorage.setItem(this.treeKey, JSON.stringify(expandedNodes));
		}
	}

};

var UserDataHelper = {
	isAdmin : function()
	{
		return this.userLevelId === 1;
	},
	reloadUserData : function()
	{
		// Ajax to reload user data
	}
};
