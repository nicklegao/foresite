var logger = undefined;
logger = log4javascript.getLogger();
logger.addAppender(new log4javascript.BrowserConsoleAppender());

// var ajaxAppender = new
// log4javascript.AjaxAppender('/webdirector/free/logging');
// ajaxAppender.setThreshold(log4javascript.Level.ALL);
// logger.addAppender(ajaxAppender);

logger.info("Logger ready " + location.href + " at " + Date());

var delayedEmail = undefined;
window.onerror = function(msg, url, line, col, error)
{
	if (url != "" && url.indexOf("/ckeditor/") < 0)
	{
		logger.fatal(url + ":" + line + (col != undefined ? "," + col : "") + "\n -- MSG: " + msg);
		if (error != undefined)
		{
			logger.fatal("STACK TRACE:\n" + error.stack);
		}

		if (delayedEmail != undefined)
		{
			clearInterval(delayedEmail);
		}
		delayedEmail = setTimeout(function()
		{
			emailLogs("Client page error.");
		}, 1000);
	}
	return false;
};

function emailLogs(description)
{
	if (window.jQuery)
	{
		$.ajax({
			url : '/webdirector/free/logging/report',
			data : {
				description : description
			}
		});
	}
	else
	{
		logger.fatal("Unable to email error logs due to missing jQuery");
	}
}
