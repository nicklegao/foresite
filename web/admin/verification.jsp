<%@page import="au.corporateinteractive.qcloud.proposalbuilder.db.UserAccountDataAccess.AuthenticationStatus"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.ClientApplicationProperties"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="au.com.ci.sbe.util.UrlUtils"%>
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.utils.BrowserChecker"%>
<%
String loginAsEmail = (String) session.getAttribute(Constants.LOGIN_AS);

if (loginAsEmail == null)
{
	response.sendRedirect("/dashboard");
	return;
}

java.util.Calendar calendar = java.util.Calendar.getInstance();
int currentYear = calendar.get(java.util.Calendar.YEAR);
%><!DOCTYPE html>
<!--[if lt IE 9]><html class="bad-ie" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<%@page import="au.corporateinteractive.qcloud.proposalbuilder.Constants"%>
<html lang="en">
<!--<![endif]-->
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %><compress:html enabled="true" compressJavaScript="true" compressCss="true">
<head>
	<title>Proposal Builder</title>
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />

	<link rel="stylesheet" href="/application/assets/plugins/bootstrap-qcloud/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" href="/application/assets/plugins/qcloud-iconfont/style.css" />
	<link rel="stylesheet" href="/application/assets/fonts/style.css" />
	<link rel="stylesheet" href="/application/assets/plugins/sweetalert/sweetalert.css" />
	<link rel="stylesheet" href="/application/assets/plugins/nprogress/nprogress.css" />

	<link rel="stylesheet" href="<%=UrlUtils.autocachedUrl("/application/css/login.css", request) %>" type="text/css"/>
	<!--[if IE 7]>
	<link rel="stylesheet" href="/application/assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
	<![endif]-->
</head>
<body class="login">
<div class="background" style="width: 100%;height: 100%;position: absolute;-webkit-background-size: cover;background-size: cover;"></div>
	<div class="container">
		<div class="row">
			<div class="main-login col-xs-offset-2 col-xs-8">
		
				<div class="box-login">
					<div class="box-header"><div class="logo"></div></div>
					<div><p>Please check your email for the verification code.</p></div>
						<div class="alert alert-block alert-warning fade in device-warning" style="display: none">
							<h4 class="alert-heading"><i class="fa fa-exclamation-circle"></i> Warning</h4>
							<p><strong>Oh No!...</strong> It looks like you are using an unsupported device. This application works best on devices with larger screens.</p>
						</div>
					<% if (session.getAttribute(Constants.VERIFICATION_ERROR) != null) {
							session.removeAttribute(Constants.VERIFICATION_ERROR);
						%>
						<div class="alert alert-block alert-danger fade in">
							<button type="button" class="close" data-dismiss="alert">
								&times;
							</button>
							<h4 class="alert-heading"><i class="fa fa-times-circle"></i> Error</h4>
							<p>Verification code incorrect. Please try again.</p>
						</div>				
					<% } %>
					<form class="form-verify" action="/api/free/loginas/verification-submit" method="post">
						<fieldset>
							<input type="hidden" name="email" value="<%=loginAsEmail%>" />
							<div class="form-group">
								<span class="input-icon">
									<input type="text" class="form-control" name="verificationCode" placeholder="Verification Code" autofocus="autofocus" required>
									<i class="qc qc-padlock"></i>
									<a class="forgot" href="/webdirector/secure/app/loginas/verification?email=<%=loginAsEmail%>">Resend verification code</a> </span>
							</div>
							<div class="form-actions">
							  <button class="btn btn-success pull-right submitButton">Verify <i class="qc qc-next"></i></button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
			<!-- start: COPYRIGHT -->
			<div class="copyright col-xs-offset-2 col-xs-8">
				QuoteCloud version <%=ESAPI.encoder().encodeForHTML(ClientApplicationProperties.sharedInstance().getApplicationVersion()) %> &copy; Copyright QuoteCloud 2015-<%= currentYear %>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script><script src="/admin/js/upgradeJquery.js"></script>
	<script src="/application/assets/plugins/jquery-placeholder/js/jquery.placeholder.js"></script>
	<script src="/application/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="/application/assets/plugins/bootstrap-qcloud/js/bootstrap.min.js"></script>
	<script src="/application/assets/plugins/select2/select2.js"></script>
	<script src="/application/js/login.js"></script>
	<script src="/application/assets/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="/application/assets/plugins/nprogress/nprogress.js"></script>
	
	<script src="/application/js/three.min.js"></script>
	<script>
		jQuery(document).ready(function() {

			$.validator.setDefaults({
				errorElement : "span", // contain the error msg in a small tag
				errorClass : 'help-block',
				errorPlacement : function(error, element)
				{
					error.insertAfter(element);
				},
				ignore : ':hidden'
			});
			
			var form = $('.form-verify');
			
			var errorHandler = $('.errorHandler', form);
			
			form.validate({
				rules : {
					username : {
						required : true
					}
				},
				highlight : function(element)
				{
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
				},
				unhighlight : function(element)
				{
					$(element).closest('.form-group').removeClass('has-error');
				},
				success : function(label, element)
				{
					$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
				},
				submitHandler : function(form)
				{
					errorHandler.hide();
					form.submit();
				},
				invalidHandler : function(event, validator)
				{ // display error alert on form submit
					errorHandler.show();
				}
				
			});
            $(document).on("click", ".submitButton", function() {
                event.preventDefault();

                NProgress.configure({ showSpinner: false,
										parent: '.box-login',
                    					trickleSpeed: 50});
                NProgress.start();
                $('.form-verify').submit();
            });

            $('div.background').css({'background-image': 'url(../application/assets/images/backgrounds/' + Math.floor(Math.random() * 19) + '.jpg)'});


            if($(window).width() < 768) {
				$(".device-warning").show();
				$("input, button").prop("disabled", true);
			}
			$(window).resize(function(){
				if($(window).width() < 768) {
					$(".device-warning").show();
					$("input, button").prop("disabled", true);
				} else {
					$(".device-warning").hide();
					$("input, button").prop("disabled", false);
				}	
			})
			Login.initLogin();
			
		});
	</script>
	
</body>
</compress:html>
</html> 