<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

    <title> SmartAdmin </title>
    <meta name="description" content="">
    <meta name="author" content="">
      
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-production-plugins.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-skins.min.css">

    <!-- SmartAdmin RTL Support -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.min.css"> 

    <!-- WebDirector Styles -->
    <%
    boolean useLess = false; 
    String[] files = new String[]{"webdirector", "bulkEditor"};
    
    for (String f : files)
    {
      %>
      <link rel="stylesheet<%=useLess?"/less":"" %>" type="text/css" href="/admin/css<%=useLess?"/less":"" %>/<%=f %>.<%=useLess?"less":"min.css" %>" />
      <%
    }
    if (useLess) { %>
      <script src="/admin/js/less/less.min.js" data-poll="1000" data-relative-urls="false"></script>
    <% } %>

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">

    <!-- GOOGLE FONT -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

    <!-- Specifying a Webpage Icon for Web Clip 
       Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="img/splash/sptouch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/splash/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/splash/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/splash/touch-icon-ipad-retina.png">
    
    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    
    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="img/splash/iphone.png" media="screen and (max-device-width: 320px)">

    <!--[if IE 9]>
      <style>
        .error-text {
          color: #333 !important;
        }
      </style>
    <![endif]-->
    
    <style>
      .page-footer {
        padding-left: 13px;
      }
    </style>

  </head>
  
  <!--

  TABLE OF CONTENTS.
  
  Use search to find needed section.
  
  ===================================================================
  
  |  01. #CSS Links                |  all CSS links and file paths  |
  |  02. #FAVICONS                 |  Favicon links and file paths  |
  |  03. #GOOGLE FONT              |  Google font link              |
  |  04. #APP SCREEN / ICONS       |  app icons, screen backdrops   |
  |  05. #BODY                     |  body tag                      |
  |  06. #HEADER                   |  header tag                    |
  |  07. #PROJECTS                 |  project lists                 |
  |  08. #TOGGLE LAYOUT BUTTONS    |  layout buttons and actions    |
  |  09. #MOBILE                   |  mobile view dropdown          |
  |  10. #SEARCH                   |  search field                  |
  |  11. #NAVIGATION               |  left panel & navigation       |
  |  12. #RIGHT PANEL              |  right panel userlist          |
  |  13. #MAIN PANEL               |  main panel                    |
  |  14. #MAIN CONTENT             |  content holder                |
  |  15. #PAGE FOOTER              |  page footer                   |
  |  16. #SHORTCUT AREA            |  dropdown shortcuts area       |
  |  17. #PLUGINS                  |  all scripts and plugins       |
  
  ===================================================================
  
  -->
  
  <!-- #BODY -->
  <!-- Possible Classes

    * 'smart-style-{SKIN#}'
    * 'smart-rtl'         - Switch theme mode to RTL
    * 'menu-on-top'       - Switch to top navigation (no DOM change required)
    * 'no-menu'       - Hides the menu completely
    * 'hidden-menu'       - Hides the main menu but still accessable by hovering over left edge
    * 'fixed-header'      - Fixes the header
    * 'fixed-navigation'  - Fixes the main menu
    * 'fixed-ribbon'      - Fixes breadcrumb
    * 'fixed-page-footer' - Fixes footer
    * 'container'         - boxed layout mode (non-responsive: will not work with fixed-navigation & fixed-ribbon)
  -->
  <body class="">

    <!-- HEADER -->
    <header id="header">
      <div id="logo-group">
        <span id="logo"> <img src="/admin/img/custom/logo.png" alt="Corporate Interactive "> </span>
      </div>

      <!-- pulled right: nav area -->
      <div class="pull-right">
        
        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
          <span> <a href="/webdirector" title="Sign Out" data-action="userLogout" data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->

      </div>
      <!-- end pulled right: nav area -->

    </header>
    <!-- END HEADER -->
    
    <!-- MAIN CONTENT -->
    <div id="content">

      <!-- row -->
      <div class="row">
      
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      
          <div class="row">
            <div class="col-sm-12">
              <div class="text-center error-box">
                <h1 class="error-text tada animated"><i class="fa fa-times-circle text-danger error-icon-shadow tada animated infinite"></i> Access Denied</h1>
                <h2 class="font-xl"><strong> </strong></h2>
                <br />
                <p class="lead semi-bold">
                  <strong>You have attempted to access content for which you are not authorised</strong><br><br>
                  <small>
                    
                  </small>
                </p>
                <ul class="error-search text-left font-md">
                  <li><a href="javascript:window.top.location.href = '/webdirector/dashboard';"><small>Go to Dashboard <i class="fa fa-arrow-right"></i></small></a></li>
                  <li><a href="javascript:window.top.location.href = '/webdirector/workbench';"><small>Go to Workbench <i class="fa fa-arrow-right"></i></small></a></li>
                </ul>
              </div>
      
            </div>
      
          </div>
      
        </div>
        
      </div>
      <!-- end row -->

    </div>
    <!-- END MAIN CONTENT -->

    <!-- PAGE FOOTER -->
    <div class="page-footer">
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <span class="txt-color-white">SmartAdmin 1.5 <span class="hidden-xs"> - Web Application Framework</span> &copy; 2014-2015</span>
        </div>
      </div>
    </div>
    <!-- END PAGE FOOTER -->

    <!--================================================== -->

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script><script src="/admin/js/upgradeJquery.js"></script>

    <script src="/admin/js/libs/jquery-ui-1.10.3.min.js"></script>

    <script src="/admin/js/log4javascript/log4javascript.js"></script>
    <script src="/admin/js/logging.js"></script>

    <!-- IMPORTANT: APP CONFIG -->
    <script src="/admin/js/app.config.js"></script>

    <!-- CUSTOM NOTIFICATION -->
    <script src="/admin/js/notification/SmartNotification.js"></script>
    
    <!-- MAIN APP JS FILE -->
    <script src="js/app.min.js"></script>

    <!--[if IE 8]>

    <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

    <![endif]-->

  </body>

</html>