<%
String token = (String)request.getAttribute("token");
String username = (String)request.getAttribute("user");
%>
<!DOCTYPE html>
<html lang="en-us" id="extr-page">
<head>
<meta charset="utf-8">
<title>SmartAdmin</title>
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport"
  content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- #CSS Links -->
<!-- Basic Styles -->
<link rel="stylesheet" type="text/css" media="screen"
  href="/admin/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen"
  href="/admin/css/font-awesome.min.css">

<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
<link rel="stylesheet" type="text/css" media="screen"
  href="/admin/css/smartadmin-production-plugins.css">
<link rel="stylesheet" type="text/css" media="screen"
  href="/admin/css/smartadmin-production.css">
<link rel="stylesheet" type="text/css" media="screen"
  href="/admin/css/smartadmin-skins.min.css">

<!-- SmartAdmin RTL Support -->
<link rel="stylesheet" type="text/css" media="screen"
  href="/admin/css/smartadmin-rtl.min.css">

<!-- WebDirector Styles -->
<link rel="stylesheet" type="text/css" media="screen" href="/admin/css/webdirector.css">

<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
<link rel="stylesheet" type="text/css" media="screen"
  href="/admin/css/demo.min.css">

<!-- #FAVICONS -->
<link rel="shortcut icon" href="/admin/img/favicon/favicon.ico"
  type="image/x-icon">
<link rel="icon" href="/admin/img/favicon/favicon.ico" type="image/x-icon">

<!-- #GOOGLE FONT -->
<link rel="stylesheet"
  href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

<!-- #APP SCREEN / ICONS -->
<!-- Specifying a Webpage Icon for Web Clip 
			 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
<link rel="apple-touch-icon" href="/admin/img/splash/sptouch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76"
  href="/admin/img/splash/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120"
  href="/admin/img/splash/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152"
  href="/admin/img/splash/touch-icon-ipad-retina.png">

<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<!-- Startup image for web apps -->
<link rel="apple-touch-startup-image"
  href="/admin/img/splash/ipad-landscape.png"
  media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
<link rel="apple-touch-startup-image"
  href="/admin/img/splash/ipad-portrait.png"
  media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
<link rel="apple-touch-startup-image" href="/admin/img/splash/iphone.png"
  media="screen and (max-device-width: 320px)">

</head>

<body class="animated fadeInDown">

  <header id="header">

    <div id="logo-group">
      <span id="logo"> <img src="/admin/img/custom/logo.png"
        alt="Corporate Interactive">
      </span>
    </div>

  </header>

  <div id="main" role="main">

    <!-- MAIN CONTENT -->
    <div id="content" class="container">

      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4 col-centered">
        
          <div class="well no-padding">
            <form id="submit-form" class="smart-form client-form">
              <header> Unlock your account </header>

              <fieldset>
                <% if(null != request.getAttribute("ErrorMsg")){ %>
                <div role="alert" class="alert alert-danger alert-dismissible fade in" id="errorMsgBox">
                  <h4>Error</h4>
                  <p class="msg-content"><%= request.getAttribute("ErrorMsg") %></p>
                </div>
                <% } %>
                <% if (null != request.getAttribute("Success")){ %>
                <div role="alert" class="alert alert-success alert-dismissible fade in" id="successMsgBox">
                  <h4>Success</h4>
                  <p class="msg-content">Your account has been unlocked successfully. Please login again.</p>
                </div>
                <% } %>
              </fieldset>
            </form>
          </div>

        </div>
      </div>
    </div>

  </div>

  <!--================================================== -->

  <!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
  <script src="/admin/js/plugin/pace/pace.min.js"></script>

  <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script><script src="/admin/js/upgradeJquery.js"></script>


  <script
    src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
  <script>
			if (!window.jQuery.ui) {
				document
						.write('<script src="/admin/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

  <script src="/webdirector/free/csrf-token/init"></script>
  <!-- IMPORTANT: APP CONFIG -->
  <script src="/admin/js/app.config.js"></script>

  <!-- JS TOUCH : include this plugin for mobile drag / drop touch events 		
		<script src="/admin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

  <!-- BOOTSTRAP JS -->
  <script src="/admin/js/bootstrap/bootstrap.min.js"></script>

  <!-- JQUERY VALIDATE -->
  <script src="/admin/js/plugin/jquery-validate/jquery.validate.min.js"></script>

  <!-- JQUERY MASKED INPUT -->
  <script src="/admin/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

  <!--[if IE 8]>
			
			<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
			
		<![endif]-->

  <script src="/admin/js/error.js"></script>

  <!-- MAIN APP JS FILE -->
  <script src="/admin/js/app.js"></script>

  <script type="text/javascript">
runAllForms();

$(function() {
  setTimeout(function(){
    window.document.location.href="/webdirector";
  }, 3000);
});
    </script>
	<script src="/admin/js/plugin/moment/moment.min.js"></script>

</body>
</html>