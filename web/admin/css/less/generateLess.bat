@echo on
cd /d %~dp0

rem requires:
rem
rem npm install -g less
rem npm install -g clean-css@3.4.23
rem

cmd /C lessc webdirector.less > ../webdirector.css
cmd /C cleancss -o ../webdirector.min.css ../webdirector.css

cmd /C lessc dashboard.less > ../dashboard.css
cmd /C cleancss -o ../dashboard.min.css ../dashboard.css

cmd /C lessc workbench.less > ../workbench.css
cmd /C cleancss -o ../workbench.min.css ../workbench.css

cmd /C lessc bulkEditor.less > ../bulkEditor.css
cmd /C cleancss -o ../bulkEditor.min.css ../bulkEditor.css

cmd /C lessc dynamicFormBuilder.less > ../dynamicFormBuilder.css
cmd /C cleancss -o ../dynamicFormBuilder.min.css ../dynamicFormBuilder.css

cmd /C lessc ui.fancytree.less > ../ui.fancytree.css
cmd /C cleancss -o ../ui.fancytree.min.css ../ui.fancytree.css

cmd /C lessc versionHistory.less > ../versionHistory.css
cmd /C cleancss -o ../versionHistory.min.css ../versionHistory.css

cmd /C lessc smartadmin-production-plugins.less > ../smartadmin-production-plugins.css
cmd /C cleancss -o ../smartadmin-production-plugins.min.css ../smartadmin-production-plugins.css

cmd /C lessc smartadmin-production.less > ../smartadmin-production.css
cmd /C cleancss -o ../smartadmin-production.min.css ../smartadmin-production.css

cmd /C lessc smartadmin-skin\smartadmin-skins.less > ../smartadmin-skins.css
cmd /C cleancss -o ../smartadmin-skins.min.css ../smartadmin-skins.css

