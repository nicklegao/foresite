<%@page import="org.apache.commons.lang.StringUtils"%>
<%@ page import="java.util.Enumeration"%><!--
 * FCKeditor - The text editor for internet
 * Copyright (C) 2003-2004 Frederico Caldeira Knabben
 *
 * Licensed under the terms of the GNU Lesser General Public License:
 * 		http://www.opensource.org/licenses/lgpl-license.php
 *
 * For further information visit:
 * 		http://www.fckeditor.net/
 *
 * File Name: sample01.html
 * 	Sample page.
 *
 * Version:  2.0 RC2
 * Modified: 2004-05-31 23:35:28
 *
 * File Authors:
 * 		Frederico Caldeira Knabben (fredck@fckeditor.net)
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@ page import="webdirector.db.DButils"%>
<html>
<%
	String colName = (String)request.getParameter("colName");
	String content = (String)request.getParameter("HTMLContent");
	String id = (String)request.getParameter("id");
	String module = (String)request.getParameter("module");
	String tableType = (String)request.getParameter("tableType");
    String template = (String)request.getParameter("template");
    String action = request.getParameter("action");
    String draft = request.getParameter("draft");
    String draftId = request.getParameter("draftId");
    boolean isWorkflow = StringUtils.equalsIgnoreCase("true", draft);
    DButils db = new DButils(tableType+"_"+module);

    String keyCol = "Element_id";
	if ( tableType.equals("Categories") )
        keyCol = "Category_id";
	if(isWorkflow)
	{
		id = draftId;
	}
%>
	<head>
		<title>FCKeditor - Content</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="robots" content="noindex, nofollow">
	</head>
	<body>
    <form name="frm">
        <textarea name="content" style="display:none;"><%= content %></textarea>
    </form>    

<%
if ( !action.equals("insert")) {
    String encodedContent = db.encode(content, "'", "&#39;");

    boolean b = db.updateLongTextFile(colName, encodedContent, module, Integer.parseInt(id), tableType, isWorkflow);
} else {
    %>
<script type="text/javascript">
	if(top.document.formBuilder.insertID){
	  top.document.formBuilder.insertID.value = '<%= id %>';  
	}
	if(top.jQuery('[name="<%= colName %>"]').length > 0){
	  top.jQuery('[name="<%= colName %>"]').val(document.frm.content.value);
	}
    
</script>
<%
}
%>
<script type="text/javascript">
	if(window.parent && window.parent.FullWidthHelper && window.parent.FullWidthHelper.closeModal){
        window.parent.FullWidthHelper.closeModal();
    }else{
	    window.close();
	}
</script>
	</body>
</html>
