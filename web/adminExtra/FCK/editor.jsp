
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.net.webdirector.admin.security.filter.CheckCSRFTokenFilter"%>
<%@page import="webdirector.db.UserUtils"%>
<%@page import="java.util.Vector"%>
<%@page import="au.com.ci.webdirector.user.UserManager"%>
<%@page import="au.net.webdirector.admin.modules.domain.Module"%>
<%@page import="au.net.webdirector.admin.modules.ModuleConfiguration"%>
<%@page import="au.net.webdirector.common.Defaults"%>
<%@ page import="webdirector.db.client.ClientFileUtils" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!doctype HTML>
<html>
<head>
  <title>WebDirector : Content Editor</title>
    <meta charset="UTF-8">
	<script src="/adminExtra/FCK/plugin-configuration.js"></script>
    <link rel="stylesheet" type="text/css" href="/admin/js/plugin/select2/ckeditor-select2.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/adminExtra/ckeditor/ckeditor.js"></script>
    <!-- JQUERY SELECT2 INPUT -->
	<script src="/admin/js/plugin/select2/select2.full.min.js"></script>
    <style type="text/css">
    .fm-modal {
        z-index: 10011; /** Because CKEditor image dialog was at 10010 */
        width:100%;
        height:100%;
        top: 0%;
        left:0%;
        border:0;
        position:fixed;
        -moz-box-shadow: 0px 1px 5px 0px #656565;
        -webkit-box-shadow: 0px 1px 5px 0px #656565;
        -o-box-shadow: 0px 1px 5px 0px #656565;
        box-shadow: 0px 1px 5px 0px #656565;
        filter:progid:DXImageTransform.Microsoft.Shadow(color=#656565, Direction=180, Strength=5);
    }
    </style>
</head>
<%
    StringBuffer field = new StringBuffer();
    String colName = (String) request.getParameter("colName");
    String colNum = (String) request.getParameter("colNum");
    String id = (String) request.getParameter("id");
    String module = (String) request.getParameter("module");
    String tableType = (String) request.getParameter("tableType");
    String template = (String) request.getParameter("template");
    String name = (String) request.getParameter("name");
    String action = (String) request.getParameter("action");
    String draft = (String) request.getParameter("draft");
    String draftId = (String) request.getParameter("draftId");
    String dontShowCss = request.getParameter("dontShowCss");
    if(dontShowCss != null && "true".equals(dontShowCss)){
     session.setAttribute("dontShowCommonCss","true");
    }else if(dontShowCss != null && "false".equals(dontShowCss)){
     session.setAttribute("dontShowCommonCss","false");
    }
    Module moduleObj = ModuleConfiguration.getInstance().getModule(module);
    ClientFileUtils cfu = new ClientFileUtils();
    String cat = "";
    if (tableType.equals("Categories"))
        cat = "Categories/";
    String storeName = "/" + module + "/" + cat + id + "/" + colName + "/" + colName + ".txt";
    if (template != null)
        storeName = "/_templates/" + name;
    if (StringUtils.equalsIgnoreCase(draft, "true")) 
    	storeName = "/_draft/" + module + "/" + "" + cat + draftId + "/" + colName + "/" + colName + ".txt";

    // don't read file contents in if we're inserting ... this was the old bug of reading in parent ID element data.
    if (!action.startsWith("insert")) {
        field = cfu.readTextContents(cfu.getFilePathFromStoreName(storeName), true);
        String text = StringEscapeUtils.escapeHtml(field.toString());
        field = new StringBuffer(text);        
    } else {
        // if we're inserting then only set the ID if we actually use it
        // which then reserves its usage and forces the dynamicForm code to also use it
        // rather than getting its own.
        id = "";
    }
    String userId = UserManager.getUserId(request);
    String [] vals = null;
    if (userId != null && !userId.equals("")) {
    	Vector v = new UserUtils().getUser(userId);
    	vals = (String[]) v.elementAt(0);
    }

    boolean access_file_manager = vals != null && "yes".equalsIgnoreCase(vals[8]);
%>
<body>
  <form method="post" action="/webdirector/adminExtra/ckeditor/save" name="frm">
    <input type="hidden" name="<%= CheckCSRFTokenFilter.TokenFieldName %>" value="<%= CheckCSRFTokenFilter.getCSRFTokenToSession(session) %>" class="skip-change-check">
    <p>
      <textarea name="HTMLContent" id="HTMLContent"><%= field.toString() %></textarea>
    </p>
     <input type="hidden" name="colNum" value="<%= colNum %>">
    <input type="hidden" name="colName" value="<%= colName %>">
    <input type="hidden" name="module" value="<%= module %>">
    <input type="hidden" name="id" value="<%= id %>">
    <input type="hidden" name="tableType" value="<%= tableType %>">
    <input type="hidden" name="name" value="<%= name %>">
    <input type="hidden" name="action" value="<%= action %>">
    <input type="hidden" name="draft" value="<%= draft %>">
    <input type="hidden" name="draftId" value="<%= draftId %>">
    </form>
</body>

<script type="text/javascript">
  window.onload = function()
  {
<%
if (action.startsWith("insert")) {
%>
    document.frm.HTMLContent.value = top.jQuery('[name="<%= colName %>"]').val();
<%    
}
%>

  var showBrowseServerBtn = <%= ("," + moduleObj.getCustomisedButtons()+",").indexOf(",B,") > -1 && access_file_manager %>;
        //    CKEDITOR.replace( 'HTMLContent' );
        CKEDITOR.replace( 'HTMLContent',
        {
            extraAllowedContent : '*(*);*{*};iframe[*]',
            customConfig: '/webdirector/adminExtra/ckeditor/config?module=<%= module %>',
            extraPlugins : 'externalcss,close,strinsert,wddiv',
            filebrowserSharedFileBrowseUrl : '/webdirector/adminExtra/filemanager/index?fileRoot=/',
            filebrowserImageBrowseUrl : '/webdirector/adminExtra/filemanager/index?fileRoot=/_images/&type=images',
            filebrowserFlashBrowseUrl : '/webdirector/adminExtra/filemanager/index?fileRoot=/_Flashs/&type=flashes',
            uploadUrl : '/webdirector/ckeditor/upload-image?type=image',
            // filebrowserImageBrowseUrl : '../FCKeditor/editor/filemanager/browser/default/browser.html?Type=_images&Connector=connectors/jsp/connector',
            // filebrowserImageUploadUrl : '../FCKeditor/editor/filemanager/browser/default/connectors/jsp/connector?Command=FileUpload&Type=_images&CurrentFolder=/',
            // filebrowserFlashBrowseUrl : '../FCKeditor/editor/filemanager/browser/default/browser.html?Type=_Flashs&Connector=connectors/jsp/connector',
            // filebrowserFlashUploadUrl : '../FCKeditor/editor/filemanager/browser/default/connectors/jsp/connector?Command=FileUpload&Type=_Flashs&CurrentFolder=/',
            //skin:'office2003',
            skin:'moono',
            //skin:'bootstrapck',    
          // uiColor: '#C6C6C6',
            height:'460',
            width:'880',
        font_names: 'Arial/Arial, Helvetica, sans-serif;Courier New/Courier New, Courier, monospace;Georgia/Georgia, serif;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;Tahoma/Tahoma, Geneva, sans-serif;Times New Roman/Times New Roman, Times, serif;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;Verdana/Verdana, Geneva, sans-serif'
    //        filebrowserImageUploadUrl : '../FCKssfseditor/editor/filemanager/browser/default/connectors/jsp/connector?Command=FileUpload&Type=_images&CurrentFolder=/'
        });
        CKEDITOR.on('dialogDefinition', function (event)
        {
          var editor = event.editor;
          var dialogDefinition = event.data.definition;
          var dialogName = event.data.name;

          var cleanUpFuncRef = CKEDITOR.tools.addFunction(function ()
          {
            // Do the clean-up of filemanager here (called when an image was selected or cancel was clicked)
            $('#filemanager_iframe').remove();
            $("body").css("overflow-y", "scroll");
          });

          var tabCount = dialogDefinition.contents.length;
          for (var i = 0; i < tabCount; i++) {
            var browseButton = dialogDefinition.contents[i].get('browse');
            
            if (browseButton !== null) {
              browseButton.hidden = !showBrowseServerBtn;
              browseButton.onClick = function (dialog, i)
              {
                editor._.filebrowserSe = this;
                var iframe = $("<iframe id='filemanager_iframe' class='fm-modal'/>").attr({
                  src: this.filebrowser.url + // Change it to wherever  Filemanager is stored.
                      '&CKEditorFuncNum=' + CKEDITOR.instances[event.editor.name]._.filebrowserFn +
                      '&CKEditorCleanUpFuncNum=' + cleanUpFuncRef +
                      '&langCode=en' +
                      '&CKEditor=' + event.editor.name
                });

                $("body").append(iframe);
                $("body").css("overflow-y", "hidden");  // Get rid of possible scrollbars in containing document
              }
            }
          }
        }); // dialogDefinition
        CKEDITOR.on('instanceReady',
            function( evt )
            {
               var editor = evt.editor;
               
               var restoreIframe = function(){
                 console.log("restore iframe");
                 var CKEIframes = jQuery('.cke_iframe', window.frames[0].document);
                 var CKEIframesL = CKEIframes.length;
                 for (i=0;i<CKEIframesL;i++) {
                    $iframe = jQuery(decodeURIComponent(jQuery(CKEIframes[i]).data('cke-realelement')));
                    $iframe.addClass('ck-restored-iframe');
                    jQuery(CKEIframes[i]).before($iframe);
                 }
               }
               
               var removeIframe = function(){
                 $('.ck-restored-iframe', window.frames[0].document).remove();
                 editor.updateElement();
               }
               
               editor.execCommand('maximize');
               
               restoreIframe();
               
               editor.on('beforeCommandExec', function (evt){
                 console.log(evt);
                 if(evt.data.name=='save'){
                   removeIframe();
                   editor.fire( 'saveSnapshot' );
                 }
               });
               
               CKEDITOR.plugins.sourcearea.commands.source.exec=function(editor){
                  if ( editor.mode == 'wysiwyg' ){
                    removeIframe();
                    editor.fire( 'saveSnapshot' );
                  }
                  editor.getCommand( 'source' ).setState( CKEDITOR.TRISTATE_DISABLED );
                  editor.setMode( editor.mode == 'source' ? 'wysiwyg' : 'source', editor.mode == 'source'? restoreIframe : null);
                }
            });
        javascript:void(0);
    };

    function popup_window_withargs(url, args)
    {
      var newWindow;
      var urlstring = url;
      var argstring = args;
      newWindow = window.open(urlstring,'',argstring);
    }
</script>
</html>
