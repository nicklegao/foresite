<%@ page import="java.util.*" %>
<%
response.setContentType("text/javascript");
%>

    CKEDITOR.addTemplates("webdirector", {
        imagesPath: CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/"),
        templates: <%= new com.ci.webdirector.filesystem.TemplateUtils().getJsonTemplates() %>
    });