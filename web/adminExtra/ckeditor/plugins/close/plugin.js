﻿/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.plugins.add( 'close', {
    // lang: 'af,ar,bg,bn,bs,ca,cs,cy,da,de,el,en-au,en-ca,en-gb,en,eo,es,et,eu,fa,fi,fo,fr-ca,fr,gl,gu,he,hi,hr,hu,is,it,ja,ka,km,ko,ku,lt,lv,mk,mn,ms,nb,nl,no,pl,pt-br,pt,ro,ru,sk,sl,sr-latn,sr,sv,th,tr,ug,uk,vi,zh-cn,zh', // %REMOVE_LINE_CORE%
    icons: 'closewindow', // %REMOVE_LINE_CORE%
    init: function( editor ) {
        var buttonName = 'CloseWindow';
        var buttonLabel = 'Close Window';
        var commandName = 'closewindow';
        var command = editor.addCommand( commandName,
            {
                exec : function( editor )
                {   
                    var preState = command.state;
                    if(window.parent && window.parent.FullWidthHelper && window.parent.FullWidthHelper.closeModal){
                        window.parent.FullWidthHelper.closeModal();
                    }else{
                        window.close();
                    }
                }
            });

        editor.ui.addButton( buttonName,
            {
                label: buttonLabel,
                command: commandName,
                icon: this.path + 'images/close.png'
            } );

    }
});


