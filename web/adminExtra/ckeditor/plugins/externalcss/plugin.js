﻿/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.plugins.add( 'externalcss', {
    // lang: 'af,ar,bg,bn,bs,ca,cs,cy,da,de,el,en-au,en-ca,en-gb,en,eo,es,et,eu,fa,fi,fo,fr-ca,fr,gl,gu,he,hi,hr,hu,is,it,ja,ka,km,ko,ku,lt,lv,mk,mn,ms,nb,nl,no,pl,pt-br,pt,ro,ru,sk,sl,sr-latn,sr,sv,th,tr,ug,uk,vi,zh-cn,zh', // %REMOVE_LINE_CORE%
    icons: 'commoncss', // %REMOVE_LINE_CORE%
    init: function( editor ) {
        var buttonName = 'CommonCSS';
        var buttonLabel = 'Common CSS';
        var commandName = 'commoncss';
        var switchCss = function(cssId, url){
            var doc = window.frames[0].document;
            if (!doc.getElementById(cssId))
            {
                var head  = doc.getElementsByTagName('head')[0];
                var link  = doc.createElement('link');
                link.id   = cssId;
                link.rel  = 'stylesheet';
                link.type = 'text/css';
                link.href = url;
                link.media = 'all';
                head.appendChild(link);
            }else{
                var link = doc.getElementById(cssId);
                link.parentNode.removeChild(link);
            }
        };
        var command = editor.addCommand( commandName,
            {
                exec : function( editor )
                {   
                    var preState = command.state;
                    command.setState(preState == CKEDITOR.TRISTATE_ON ? CKEDITOR.TRISTATE_OFF : CKEDITOR.TRISTATE_ON);
                    for(x = 0; x < plugin_config.commonCss.cssFiles.length; x++) {
                    	switchCss(plugin_config.commonCss.cssFiles[x].cssId, plugin_config.commonCss.cssFiles[x].cssPath)
                    }
                }
            });

        editor.ui.addButton( buttonName,
            {
                label: buttonLabel,
                command: commandName,
                icon: this.path + 'images/css.png'
            } );

    }
});


