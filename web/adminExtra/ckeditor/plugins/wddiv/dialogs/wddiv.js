/*
 * Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

( function() {

	// Add to collection with DUP examination.
	// @param {Object} collection
	// @param {Object} element
	// @param {Object} database
	function addSafely( collection, element, database ) {
		// 1. IE doesn't support customData on text nodes;
		// 2. Text nodes never get chance to appear twice;
		if ( !element.is || !element.getCustomData( 'block_processed' ) ) {
			element.is && CKEDITOR.dom.element.setMarker( database, element, 'block_processed', true );
			collection.push( element );
		}
	}

	// Dialog reused by both 'creatediv' and 'editdiv' commands.
	// @param {Object} editor
	// @param {String} command	The command name which indicate what the current command is.
	function divDialog( editor, command ) {
		// Definition of elements at which div operation should stopped.
		var divLimitDefinition = ( function() {

			// Customzie from specialize blockLimit elements
			var definition = CKEDITOR.tools.extend( {}, CKEDITOR.dtd.$blockLimit );

			if ( editor.config.div_wrapTable ) {
				delete definition.td;
				delete definition.th;
			}
			return definition;
		} )();

		// DTD of 'div' element
		var dtd = CKEDITOR.dtd.div;
		
		// Get the first div limit element on the element's path.
		// @param {Object} element
		function getDivContainer( element ) {
			var container = editor.elementPath( element ).blockLimit;

			// Never consider read-only (i.e. contenteditable=false) element as
			// a first div limit (#11083).
			if ( container.isReadOnly() )
				container = container.getParent();

			// Dont stop at 'td' and 'th' when div should wrap entire table.
			if ( editor.config.div_wrapTable && container.is( [ 'td', 'th' ] ) ) {
				var parentPath = editor.elementPath( container.getParent() );
				container = parentPath.blockLimit;
			}

			return container;
		}

		// Init all fields' setup/commit function.
		// @memberof divDialog
		function setupFields() {
			this.foreach( function( field ) {
				// Exclude layout container elements
				if ( /^(?!vbox|hbox)/.test( field.type ) ) {
					if ( !field.setup ) {
						// Read the dialog fields values from the specified
						// element attributes.
						field.setup = function( element ) {
							field.setValue( element.getAttribute( field.id ) || '', 1 );
						};
					}
					if ( !field.commit ) {
						// Set element attributes assigned by the dialog
						// fields.
						field.commit = function( element ) {
							var fieldValue = this.getValue();
							// ignore default element attribute values
							if ( field.id == 'dir' && element.getComputedStyle( 'direction' ) == fieldValue ) {
								return;
							}

							if ( fieldValue ) {
								element.setAttribute( field.id, fieldValue );
							} else {
								element.removeAttribute( field.id );
							}
						};
					}
				}
			} );
		}

		// Wrapping 'div' element around appropriate blocks among the selected ranges.
		// @param {Object} editor
		function createwdDiv( editor ) {
			// new adding containers OR detected pre-existed containers.
			var containers = [];
			// node markers store.
			var database = {};
			// All block level elements which contained by the ranges.
			var containedBlocks = [],
				block;

			// Get all ranges from the selection.
			var selection = editor.getSelection(),
				ranges = selection.getRanges();
			var bookmarks = selection.createBookmarks();
			var i, iterator;

			// collect all included elements from dom-iterator
			for ( i = 0; i < ranges.length; i++ ) {
				iterator = ranges[ i ].createIterator();
				while ( ( block = iterator.getNextParagraph() ) ) {
					// include contents of blockLimit elements.
					if ( block.getName() in divLimitDefinition && !block.isReadOnly() ) {
						var j,
							childNodes = block.getChildren();
						for ( j = 0; j < childNodes.count(); j++ )
							addSafely( containedBlocks, childNodes.getItem( j ), database );
					} else {
						while ( !dtd[ block.getName() ] && !block.equals( ranges[ i ].root ) )
							block = block.getParent();
						addSafely( containedBlocks, block, database );
					}
				}
			}

			CKEDITOR.dom.element.clearAllMarkers( database );

			var blockGroups = groupByDivLimit( containedBlocks );
			var ancestor, divElement;

			for ( i = 0; i < blockGroups.length; i++ ) {
				var currentNode = blockGroups[ i ][ 0 ];

				// Calculate the common parent node of all contained elements.
				ancestor = currentNode.getParent();
				for ( j = 1; j < blockGroups[ i ].length; j++ )
					ancestor = ancestor.getCommonAncestor( blockGroups[ i ][ j ] );

				divElement = new CKEDITOR.dom.element( 'div', editor.document );

				// Normalize the blocks in each group to a common parent.
				for ( j = 0; j < blockGroups[ i ].length; j++ ) {
					currentNode = blockGroups[ i ][ j ];

					while ( !currentNode.getParent().equals( ancestor ) )
						currentNode = currentNode.getParent();

					// This could introduce some duplicated elements in array.
					blockGroups[ i ][ j ] = currentNode;
				}

				// Wrapped blocks counting
				for ( j = 0; j < blockGroups[ i ].length; j++ ) {
					currentNode = blockGroups[ i ][ j ];

					// Avoid DUP elements introduced by grouping.
					if ( !( currentNode.getCustomData && currentNode.getCustomData( 'block_processed' ) ) ) {
						currentNode.is && CKEDITOR.dom.element.setMarker( database, currentNode, 'block_processed', true );

						// Establish new container, wrapping all elements in this group.
						if ( !j )
							divElement.insertBefore( currentNode );

						divElement.append( currentNode );
					}
				}
				divElement.setAttribute('class', 'wdDiv');
				CKEDITOR.dom.element.clearAllMarkers( database );
				containers.push( divElement );
			}

			selection.selectBookmarks( bookmarks );
			return containers;
		}

		// Divide a set of nodes to different groups by their path's blocklimit element.
		// Note: the specified nodes should be in source order naturally, which mean they are supposed to producea by following class:
		//  * CKEDITOR.dom.range.Iterator
		//  * CKEDITOR.dom.domWalker
		// @returns {Array[]} the grouped nodes
		function groupByDivLimit( nodes ) {
			var groups = [],
				lastDivLimit = null,
				block;

			for ( var i = 0; i < nodes.length; i++ ) {
				block = nodes[ i ];
				var limit = getDivContainer( block );
				if ( !limit.equals( lastDivLimit ) ) {
					lastDivLimit = limit;
					groups.push( [] );
				}
				groups[ groups.length - 1 ].push( block );
			}
			return groups;
		}

		// Synchronous field values to other impacted fields is required, e.g. div styles
		// change should also alter inline-style text.
		function commitInternally( targetFields ) {
			var dialog = this.getDialog(),
				element = dialog._element && dialog._element.clone() || new CKEDITOR.dom.element( 'div', editor.document );

			// Commit this field and broadcast to target fields.
			this.commit( element, true );

			targetFields = [].concat( targetFields );
			var length = targetFields.length,
				field;
			for ( var i = 0; i < length; i++ ) {
				field = dialog.getContentElement.apply( dialog, targetFields[ i ].split( ':' ) );
				field && field.setup && field.setup( element, true );
			}
		}


		// Registered 'CKEDITOR.style' instances.
		var styles = {};

		// Hold a collection of created block container elements.
		var containers = [];
		
		var customClasses = {};

		// @type divDialog
		return {
			title: editor.lang.div.title,
			minWidth: 400,
			minHeight: 165,
			contents: [
			{
				id: 'styling',
				label: 'Styling',
				title: 'Styling',
				elements: [ {
					type: 'vbox',
					padding: 1,
					children: [ {
						type: 'hbox',
						widths: [ '50%', '50%' ],
						children: [ {
							type: 'select',
							className: 'select2',
							id: 'contentSelect',
							style: 'width:100%',
							//TODO: Labels
							label: 'Content Boxing',
							items: plugin_config.wddiv.containerClasses,
							setup: function( element ) {
								var dialog = this.getDialog();
								var select = dialog.getContentElement.apply(dialog, ['styling', 'contentSelect']);
								var classNames = element.$.classList;
								var items = select.items;
								for(x = 0; x < items.length; x++) {
									for(y = 0; y < classNames.length; y++) {
										if(items[x][1] == classNames[y]) {
											select.setValue(classNames[y]);
										}
									}
								}
								$(select.getElement().$).find('select.select2').trigger("change");
							},
							commit: function( element ) {
								if(element.hasClass('background-child')) {
									element.setAttribute('class', 'wdDiv background-child');
								} else { 
									element.setAttribute('class', 'wdDiv')
								}
								if(this.getValue() != "") {
									element.addClass(this.getValue());
								}
							}
						},
						{
							type: 'select',
							className: 'select2',
							id: 'colorSelect',
							style: 'width:100%',
							//TODO: Labels
							label: 'Background Color',
							items: plugin_config.wddiv.colourClasses,
							setup: function( element ) {
								var dialog = this.getDialog();
								var select = dialog.getContentElement.apply(dialog, ['styling', 'colorSelect']);
								var classNames = element.$.classList;
								var items = select.items;
								for(x = 0; x < items.length; x++) {
									for(y = 0; y < classNames.length; y++) {
										if(items[x][1] == classNames[y]) {
											select.setValue(classNames[y]);
										}
									}
								}
								$(select.getElement().$).find('select.select2').trigger("change");
							},
							commit: function( element ) {
								if(this.getValue() != "") {
									element.addClass(this.getValue());
								}
							}
						}]
					},
					{
						type: 'hbox',
						widths: [ '50%', '50%' ],
						children: [ {
							type: 'select',
							className: 'select2',
							id: 'textAlignSelect',
							style: 'width:100%',
							//TODO: Labels
							label: 'Text Alignment',
							items:  plugin_config.wddiv.textAlignmentClasses,
							setup: function( element ) {
								var dialog = this.getDialog();
								var select = dialog.getContentElement.apply(dialog, ['styling', 'textAlignSelect']);
								var classNames = element.$.classList;
								var items = select.items;
								for(x = 0; x < items.length; x++) {
									for(y = 0; y < classNames.length; y++) {
										if(items[x][1] == classNames[y]) {
											select.setValue(classNames[y]);
										}
									}
								}
								$(select.getElement().$).find('select.select2').trigger("change");
							},
							commit: function( element ) {
								if(this.getValue() != "") {
									element.addClass(this.getValue());
								}
							}
						},
						{
							type: 'select',
							className: 'select2',
							id: 'linkSelect',
							style: 'width:100%',
							//TODO: Labels
							label: 'Content Link',
							items: plugin_config.wddiv.contentLinkClasses,
							setup: function( element ) {
								var dialog = this.getDialog();
								var select = dialog.getContentElement.apply(dialog, ['styling', 'linkSelect']);
								var classNames = element.$.classList;
								var items = select.items;
								for(x = 0; x < items.length; x++) {
									for(y = 0; y < classNames.length; y++) {
										if(items[x][1] == classNames[y]) {
											select.setValue(classNames[y]);
										}
									}
								}
								$(select.getElement().$).find('select.select2').trigger("change");
							},
							commit: function( element ) {
								if(this.getValue() != "") {
									element.addClass(this.getValue());
								}
							}
						}]
					},
					{
						type: 'hbox',
						widths: [ '50%', '50%' ],
						children: [{
							type: 'select',
							className: 'select2',
							id: 'verticalAlignmentSelect',
							style: 'width:100%',
							//TODO: Labels
							label: 'Vertical Alignment',
							items: plugin_config.wddiv.verticalAlignmentClasses,
							setup: function( element ) {
								var dialog = this.getDialog();
								var select = dialog.getContentElement.apply(dialog, ['styling', 'verticalAlignmentSelect']);
								var classNames = element.$.classList;
								var items = select.items;
								for(x = 0; x < items.length; x++) {
									for(y = 0; y < classNames.length; y++) {
										if(items[x][1] == classNames[y]) {
											select.setValue(classNames[y]);
										}
									}
								}
								$(select.getElement().$).find('select.select2').trigger("change");
							},
							commit: function( element ) {
								if(this.getValue() != "") {
									element.addClass(this.getValue());
								}
							}
						},
						{
							type: 'select',
							className: 'select2',
							id: 'hiddenSelect',
							style: 'width:100%',
							multiple: 'true',
							//TODO: Labels
							label: 'Hide On',
							items: plugin_config.wddiv.hideOnClasses,
							setup: function( element ) {
								var dialog = this.getDialog();
								var select = dialog.getContentElement.apply(dialog, ['styling', 'hiddenSelect']);
								var classNames = element.$.classList;
								var items = select.items;
								var multiple = [];
								for(x = 0; x < items.length; x++) {
									for(y = 0; y < classNames.length; y++) {
										if(items[x][1] == classNames[y]) {
											multiple.push(classNames[y]);
										}
									}
								}
								$(select.getInputElement().$).val(multiple);
								$(select.getElement().$).find('select.select2').trigger("change");
							},
							commit: function( element ) {
								var input = $(this.getInputElement().$);
								if(input.val() != null) {
									var values = input.val();
									for(x = 0; x < values.length; x++) {
										element.addClass(values[x]);
									}
								}
							}
						}]
					}]
				} ]
			},
			{
				id: 'animations',
				label: 'Animations',
				title: 'Animations',
				elements: [ {
					type: 'vbox',
					padding: 1,
					children: [ {
						type: 'hbox',
						children: [ {
							type: 'select',
							className: 'select2',
							id: 'animationSelect',
							style: 'width:100%',
							//TODO: Labels
							label: 'Animation',
							items: plugin_config.wddiv.animationClasses,
							setup: function( element ) {
								var dialog = this.getDialog();
								var select = dialog.getContentElement.apply(dialog, ['animations', 'animationSelect']);
								var classNames = element.$.classList;
								var items = select.items;
								for(x = 0; x < items.length; x++) {
									for(y = 0; y < classNames.length; y++) {
										if(items[x][1] == classNames[y]) {
											select.setValue(classNames[y]);
										}
									}
								}
								$(select.getElement().$).find('select.select2').trigger("change");
							},
							commit: function( element ) {
								if(this.getValue() != "") {									
									element.addClass('animated');
									element.addClass(this.getValue());
								}
							}
						}
						]
					},
					{
						type: 'hbox',
						children: [ {
							type: 'checkbox',
							id: 'animationLoop',
							label: 'Animation loop',
							setup: function( element ) {
								var dialog = this.getDialog();
								var checkbox = dialog.getContentElement.apply(dialog, ['animations', 'animationLoop']);
								var classNames = element.$.classList;
								for(y = 0; y < classNames.length; y++) {
									if(classNames[y] == "infinite") {
										checkbox.setValue(true);
									}
								}
							},
							commit: function( element ) {
								if(this.getValue()) {
									element.addClass('infinite');
								} else {
									element.removeClass('infinite');
								}
							}
						} ]
					} ]
				} ]
			},
			//TODO: Move Parallax into its own plugin
			{
				id: 'background',
				label: 'Background',
				title: 'Background',
				elements: [ {
					type: 'vbox',
					padding: 1,
					children: [ {
						type: 'hbox',
						widths: [ '70%', '30%' ],
						children: [
						{
							id: 'backgroundUrl',
							type: 'text',
							label: editor.lang.common.url,
							required: true,
							'default': '',
							onChange: function() {

							},
							setup: function( element ) {
								var style = element.getAttribute( 'style' );
								var field = this;
								if(style != null && style.indexOf('background:') != -1) {
									var background = style.substring(style.indexOf('background:url'));
									var url = background.substring((background.indexOf('(') + 2), (background.indexOf(')') - 1))
						
									this.getDialog().dontResetSize = true;
						
									field.setValue( url.trim() ); // And call this.onChange()
									// Manually set the initial value.(#4191)
									field.setInitValue();
								}
							},
							commit: function( element ) {
								if ( ( this.getValue() != "" ) ) {
									var background = 'url("' + this.getValue() + '") no-repeat center';
									element.data( 'cke-image-background', this.getValue() );
									element.getParent().addClass('background-child');
									element.setStyle('background',  background);
									element.setStyle('background-size', 'cover');
									element.setStyle('background-clip', 'content-box');
								} else {
									element.setStyle( 'background', '' ); // If removeAttribute doesn't work.
									element.removeStyle( 'background' );
									element.removeStyle('background-clip');
								}
							},
						},
						{
							type: 'button',
							id: 'browse',
							// v-align with the 'txtUrl' field.
							// TODO: We need something better than a fixed size here.
							style: 'display:inline-block;margin-top:14px;',
							align: 'center',
							label: editor.lang.common.browseServer,
							hidden: true,
							filebrowser: { 
								action: 'Browse',
								target: 'background:backgroundUrl',
								url: editor.config.filebrowserImageBrowseUrl,
							}
						}
						]
					},
					{
						type: 'hbox',
						children: [{
							type: 'checkbox',
							id: 'parallaxLoop',
							label: 'Enable Parallax',
							setup: function( element ) {
								var dialog = this.getDialog();
								var checkbox = dialog.getContentElement.apply(dialog, ['background', 'parallaxLoop']);
								var classNames = element.$.classList;
								for(y = 0; y < classNames.length; y++) {
									if(classNames[y] == "bg-parallax") {
										checkbox.setValue(true);
									}
								}
							},
							commit: function( element ) {
								if(this.getValue()) {
									element.addClass('bg-parallax');
									element.setAttribute('data-top-bottom', "transform: translateY(300px);")
									element.setAttribute('data-bottom-top', "transform: translateY(-300px);")
								} else {
									element.removeClass('bg-parallax');
									element.removeAttribute('data-top-bottom');
									element.removeAttribute('data-bottom-top');
								}
							}
						}]
					}]
				} ]
			},
			{
				id: 'sizes',
				label: 'Sizes',
				title: 'Sizes',
				elements: [ {
					type: 'vbox',
					padding: 1,
					children: [ {
						type: 'hbox',
						widths: [ '25%', '25%', '25%', '25%' ],
						children: [ {
							id: 'paddingTop',
							type: 'text',
							label: 'Padding Top',
							'default': '',
							setup: function( element ) {
								var style = element.getAttribute( 'style' );
								var field = this;
								if(style != null) {
									var padding = "";
									var size = "";
									if(style.indexOf('padding-top:') != -1) {
										padding = style.substring(style.indexOf('padding-top:'));
										size = padding.substring((padding.indexOf(':') + 1), (padding.indexOf('px;')));
									}
									if(style.indexOf('padding:') != -1) {
										padding = style.substring(style.indexOf('padding: '));
										size = padding.substring((padding.indexOf(':') + 1), (padding.indexOf('px;'))).trim();
										var sizeArray = size.split('px ');
										size = sizeArray[0];
									}
						
									this.getDialog().dontResetSize = true;
						
									field.setValue( size.trim() ); // And call this.onChange()
									// Manually set the initial value.(#4191)
									field.setInitValue();
								}
							},
							commit: function( element ) {
								if(this.getValue() != "") {
									element.setStyle('padding-top', this.getValue() + 'px');
								} else {
									element.removeStyle( 'padding-top' );
								}
							}
						},
						{
							id: 'paddingLeft',
							type: 'text',
							label: 'Padding Left',
							'default': '',
							setup: function( element ) {
								var style = element.getAttribute( 'style' );
								var field = this;
								if(style != null) {
									var padding = "";
									var size = "";
									if(style.indexOf('padding-left:') != -1) {
										padding = style.substring(style.indexOf('padding-left:'));
										size = padding.substring((padding.indexOf(':') + 1), (padding.indexOf('px;')));
									}
									if(style.indexOf('padding:') != -1) {
										padding = style.substring(style.indexOf('padding: '));
										size = padding.substring((padding.indexOf(':') + 1), (padding.indexOf('px;'))).trim();
										var sizeArray = size.split('px ');
										size = sizeArray[3] || sizeArray[1] || sizeArray[0];
									}
						
									this.getDialog().dontResetSize = true;
						
									field.setValue( size.trim() ); // And call this.onChange()
									// Manually set the initial value.(#4191)
									field.setInitValue();
								}
							},
							commit: function( element ) {
								if(this.getValue() != "") {
									element.setStyle('padding-left', this.getValue() + 'px');
								} else {
									element.removeStyle( 'padding-left' );
								}
							}
						},
						{
							id: 'paddingRight',
							type: 'text',
							label: 'Padding Right',
							'default': '',
							setup: function( element ) {
								var style = element.getAttribute( 'style' );
								var field = this;
								if(style != null) {
									var padding = "";
									var size = "";
									if(style.indexOf('padding-right:') != -1) {
										padding = style.substring(style.indexOf('padding-right:'));
										size = padding.substring((padding.indexOf(':') + 1), (padding.indexOf('px;')));
									}
									if(style.indexOf('padding:') != -1) {
										padding = style.substring(style.indexOf('padding: '));
										size = padding.substring((padding.indexOf(':') + 1), (padding.indexOf('px;'))).trim();
										var sizeArray = size.split('px ');
										size = sizeArray[1] || sizeArray[0];
									}
						
						
									this.getDialog().dontResetSize = true;
						
									field.setValue( size.trim() ); // And call this.onChange()
									// Manually set the initial value.(#4191)
									field.setInitValue();
								}
							},
							commit: function( element ) {
								if(this.getValue() != "") {
									element.setStyle('padding-right', this.getValue() + 'px');
								} else {
									element.removeStyle( 'padding-right' );
								}
							}
						},
						{
							id: 'paddingBottom',
							type: 'text',
							label: 'Padding Bottom',
							'default': '',
							setup: function( element ) {
								var style = element.getAttribute( 'style' );
								var field = this;
								if(style != null) {
									var padding = "";
									var size = "";
									if(style.indexOf('padding-bottom:') != -1) {
										padding = style.substring(style.indexOf('padding-bottom:'));
										size = padding.substring((padding.indexOf(':') + 1), (padding.indexOf('px;')));
									}
									if(style.indexOf('padding:') != -1) {
										padding = style.substring(style.indexOf('padding: '));
										size = padding.substring((padding.indexOf(':') + 1), (padding.indexOf('px;'))).trim();
										var sizeArray = size.split('px ');
										size = sizeArray[2] || sizeArray[0];
									}
						
									this.getDialog().dontResetSize = true;
						
									field.setValue( size.trim() ); // And call this.onChange()
									// Manually set the initial value.(#4191)
									field.setInitValue();
								}
							},
							commit: function( element ) {
								if(this.getValue() != "") {									
									element.setStyle('padding-bottom', this.getValue() + 'px');
								} else {
									element.removeStyle( 'padding-bottom' );
								}
							}
						}]
					},
					{
						type: 'hbox',
						widths: [ '25%', '25%', '25%', '25%' ],
						children: [ {
							id: 'marginTop',
							type: 'text',
							label: 'Margin Top',
							'default': '',
							setup: function( element ) {
								var style = element.getAttribute( 'style' );
								var field = this;
								if(style != null) {
									var margin = "";
									var size = "";
									if(style.indexOf('margin-top:') != -1) {
										margin = style.substring(style.indexOf('margin-top:'));
										size = margin.substring((margin.indexOf(':') + 1), (margin.indexOf('px;')));
									}
									if(style.indexOf('margin:') != -1) {
										margin = style.substring(style.indexOf('margin: '));
										size = margin.substring((margin.indexOf(':') + 1), (margin.indexOf('px;'))).trim();
										var sizeArray = size.split('px ');
										size = sizeArray[0];
									}
						
									this.getDialog().dontResetSize = true;
						
									field.setValue( size.trim() ); // And call this.onChange()
									// Manually set the initial value.(#4191)
									field.setInitValue();
								}
							},
							commit: function( element ) {
								if(this.getValue() != "") {
									element.setStyle('margin-top', this.getValue() + 'px');
								} else {
									element.removeStyle( 'margin-top' );
								} 
							}
						},
						{
							id: 'marginLeft',
							type: 'text',
							label: 'Margin Left',
							'default': '',
							setup: function( element ) {
								var style = element.getAttribute( 'style' );
								var field = this;
								if(style != null) {
									var margin = "";
									var size = "";
									if(style.indexOf('margin-left:') != -1) {
										margin = style.substring(style.indexOf('margin-left:'));
										size = margin.substring((margin.indexOf(':') + 1), (margin.indexOf('px;')));
									}
									if(style.indexOf('margin:') != -1) {
										margin = style.substring(style.indexOf('margin: '));
										size = margin.substring((margin.indexOf(':') + 1), (margin.indexOf('px;'))).trim();
										var sizeArray = size.split('px ');
										size = sizeArray[3] || sizeArray[1] || sizeArray[0];
									}
						
									this.getDialog().dontResetSize = true;
						
									field.setValue( size.trim() ); // And call this.onChange()
									// Manually set the initial value.(#4191)
									field.setInitValue();
								}
							},
							commit: function( element ) {
								if(this.getValue() != "") {
									element.removeStyle( 'margin-left' );
									element.setStyle('margin-left', this.getValue() + 'px');
								}
							}
						},
						{
							id: 'marginRight',
							type: 'text',
							label: 'Margin Right',
							'default': '',
							setup: function( element ) {
								var style = element.getAttribute( 'style' );
								var field = this;
								if(style != null) {
									var margin = "";
									var size = "";
									if(style.indexOf('margin-right:') != -1) {
										margin = style.substring(style.indexOf('margin-right:'));
										size = margin.substring((margin.indexOf(':') + 1), (margin.indexOf('px;')));
									}
									if(style.indexOf('margin:') != -1) {
										margin = style.substring(style.indexOf('margin: '));
										size = margin.substring((margin.indexOf(':') + 1), (margin.indexOf('px;'))).trim();
										var sizeArray = size.split('px ');
										size = sizeArray[2] || sizeArray[0];
									}
						
									this.getDialog().dontResetSize = true;
						
									field.setValue( size.trim() ); // And call this.onChange()
									// Manually set the initial value.(#4191)
									field.setInitValue();
								}
							},
							commit: function( element ) {
								if(this.getValue() != "") {
									element.removeStyle( 'margin-right' );
									element.setStyle('margin-right', this.getValue() + 'px');
								}
							}
						},
						{
							id: 'marginBottom',
							type: 'text',
							label: 'Margin Bottom',
							'default': '',
							setup: function( element ) {
								var style = element.getAttribute( 'style' );
								var field = this;
								if(style != null) {
									var margin = "";
									var size = "";
									if(style.indexOf('margin-bottom:') != -1) {
										margin = style.substring(style.indexOf('margin-bottom:'));
										size = margin.substring((margin.indexOf(':') + 1), (margin.indexOf('px;')));
									}
									if(style.indexOf('margin:') != -1) {
										margin = style.substring(style.indexOf('margin: '));
										size = margin.substring((margin.indexOf(':') + 1), (margin.indexOf('px;'))).trim();
										var sizeArray = size.split('px ');
										size = sizeArray[1] || sizeArray[0];
									}
						
									this.getDialog().dontResetSize = true;
						
									field.setValue( size.trim() ); // And call this.onChange()
									// Manually set the initial value.(#4191)
									field.setInitValue();
								}
							},
							commit: function( element ) {
								if(this.getValue() != "") {
									element.removeStyle( 'margin-bottom' );
									element.setStyle('margin-bottom', this.getValue() + 'px');
								}
							}
						}]
					},
					{
						type: 'hbox',
						width: ['25%', '25%', '25%', '25%'],
						children: [ {
							id: 'height',
							type: 'text',
							label: 'Height',
							'default': '',
							setup: function( element ) {
								var style = element.getAttribute( 'style' );
								var field = this;
								if(style != null && style.indexOf('height:') != -1) {
									var height = style.substring(style.indexOf('height:'));
									var size = height.substring((height.indexOf(':') + 1), (height.indexOf('px')));
						
									this.getDialog().dontResetSize = true;
						
									field.setValue( size.trim() ); // And call this.onChange()
									// Manually set the initial value.(#4191)
									field.setInitValue();
								}
							},
							commit: function( element ) {
								if(this.getValue() != "") {
									element.removeAttribute( 'height' );
									element.setStyle('height', this.getValue() + 'px');
								}
							}
						},
						{
							id: 'width',
							type: 'text',
							label: 'Max Width',
							'default': '',
							setup: function( element ) {
								var style = element.getAttribute( 'style' );
								var field = this;
								if(style != null && style.indexOf('max-width:') != -1) {
									var width = style.substring(style.indexOf('max-width:'));
									var size = width.substring((width.indexOf(':') + 1), (width.indexOf('px')));
						
									this.getDialog().dontResetSize = true;
						
									field.setValue( size.trim() ); // And call this.onChange()
									// Manually set the initial value.(#4191)
									field.setInitValue();
								}
							},
							commit: function( element ) {
								if(this.getValue() != "") {
									element.removeAttribute( 'max-width' );
									element.setStyle('max-width', this.getValue() + 'px');
								}
							}
						},
						{
							type: 'select',
							id: 'columnSelection',
							style: 'width:100%',
							//TODO: Labels
							label: 'Column Size',
							items: plugin_config.wddiv.columnSizeClasses,
							setup: function( element ) {
								var dialog = this.getDialog();
								var select = dialog.getContentElement.apply(dialog, ['sizes', 'columnSelection']);
								var classNames = element.$.classList;
								var items = select.items;
								for(x = 0; x < items.length; x++) {
									for(y = 0; y < classNames.length; y++) {
										if(items[x][1] == classNames[y]) {
											select.setValue(classNames[y]);
										}
									}
								}
							},
							commit: function( element ) {
								if(this.getValue() != "") {
									element.addClass(this.getValue());
								}
							}
						},
						{
							id: 'borderspacing',
							type: 'text',
							label: 'Border Spacing',
							'default': '',
							setup: function( element ) {
								var style = element.getAttribute( 'style' );
								var field = this;
								if(style != null && style.indexOf('border-spacing:') != -1) {
									var width = style.substring(style.indexOf('border-spacing:'));
									var size = width.substring((width.indexOf(':') + 1), (width.indexOf('px')));
						
									this.getDialog().dontResetSize = true;
						
									field.setValue( size.trim() ); // And call this.onChange()
									// Manually set the initial value.(#4191)
									field.setInitValue();
								}
							},
							commit: function( element ) {
								if(this.getValue() != "") {
									element.removeAttribute( 'border-spacing' );
									element.setStyle('border-spacing', this.getValue() + 'px');
								}
							}
						},]
					}] }
				]
			} ],
			onLoad: function() {
				setupFields.call( this );
				$('select.select2').select2();
			},
			onShow: function() {
				// Whether always create new container regardless of existed
				// ones.
				$('select.select2').trigger("change");
				
				if ( command == 'editwddiv' ) {
					// Try to discover the containers that already existed in
					// ranges
					// update dialog field values
					this.setupContent( this._element = CKEDITOR.plugins.div.getSurroundDiv( editor ) );
				}
			},
			onOk: function() {
				if ( command == 'editwddiv' )
					containers = [ this._element ];
				else
					containers = createwdDiv( editor, true );

				// Update elements attributes
				var size = containers.length;
				for ( var i = 0; i < size; i++ ) {
					this.commitContent( containers[ i ] );

					// Remove empty 'style' attribute.
					!containers[ i ].getAttribute( 'style' ) && containers[ i ].removeAttribute( 'style' );
				}

				this.hide();
			},
			onHide: function() {
				// Remove style only when editing existing DIV. (#6315)
				if ( command == 'editwddiv' )
					this._element.removeCustomData( 'elementStyle' );
				delete this._element;
			}
		};
	}

	CKEDITOR.dialog.add( 'createwddiv', function( editor ) {
		return divDialog( editor, 'createwddiv' );
	} );

	CKEDITOR.dialog.add( 'editwddiv', function( editor ) {
		return divDialog( editor, 'editwddiv' );
	} );
	
} )();

/**
 * Whether to wrap the entire table instead of individual cells when creating a `<div>` in a table cell.
 *
 *		config.div_wrapTable = true;
 *
 * @cfg {Boolean} [div_wrapTable=false]
 * @member CKEDITOR.config
 */
