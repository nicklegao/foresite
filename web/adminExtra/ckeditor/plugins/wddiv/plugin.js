﻿/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

/**
 * @fileOverview The "div" plugin. It wraps the selected block level elements with a 'div' element with specified styles and attributes.
 *
 */

(function() {
	CKEDITOR.plugins.add( 'wddiv', {
		requires: 'dialog',
		icons: 'createwddiv',
		init: function( editor ) {
			if ( editor.blockless )
				return;

			editor.addMenuGroup('wddiv');
			
			editor.addCommand( 'createwddiv', new CKEDITOR.dialogCommand( 'createwddiv', {
				contextSensitive: true,
				refresh: function( editor, path ) {
					var context = editor.config.wddiv_wrapTable ? path.root : path.blockLimit;
					this.setState( 'div' in context.getDtd() ? CKEDITOR.TRISTATE_OFF : CKEDITOR.TRISTATE_DISABLED );
				}
			}));

			editor.addCommand( 'editwddiv', new CKEDITOR.dialogCommand( 'editwddiv' ) );
			editor.addCommand( 'removewddiv', {
				exec: function( editor ) {
					var selection = editor.getSelection(),
						ranges = selection && selection.getRanges(),
						range,
						bookmarks = selection.createBookmarks(),
						walker,
						toRemove = [];

					function findDiv( node ) {
						var div = CKEDITOR.plugins.wddiv.getSurroundDiv( editor, node );
						if ( div && !div.data( 'cke-div-added' ) ) {
							toRemove.push( div );
							div.data( 'cke-div-added' );
						}
					}

					for ( var i = 0; i < ranges.length; i++ ) {
						range = ranges[ i ];
						if ( range.collapsed )
							findDiv( selection.getStartElement() );
						else {
							walker = new CKEDITOR.dom.walker( range );
							walker.evaluator = findDiv;
							walker.lastForward();
						}
					}

					for ( i = 0; i < toRemove.length; i++ )
						toRemove[ i ].remove( true );

					selection.selectBookmarks( bookmarks );
				}
			});

			editor.ui.addButton && editor.ui.addButton( 'CreateWDDiv', {
				label: 'Create WD Div',
				command: 'createwddiv',
				icons: this.path + 'icons/creatediv.png',
				toolbar: 'blocks,50'
			});

			if ( editor.addMenuItems ) {
				editor.addMenuItems({
					editdiv: {
						label: 'Edit WD Div',
						command: 'editwddiv',
						group: 'wddiv',
						order: 1
					},

					removediv: {
						label: 'Remove WD Div',
						command: 'removewddiv',
						group: 'wddiv',
						order: 5
					}
				});

				if ( editor.contextMenu ) {
					editor.contextMenu.addListener( function( element ) {
						if ( !element || element.isReadOnly() )
							return null;


						if ( CKEDITOR.plugins.wddiv.getSurroundDiv( editor ) ) {
							return {
								editwddiv: CKEDITOR.TRISTATE_OFF,
								removewddiv: CKEDITOR.TRISTATE_OFF
							};
						}

						return null;
					});
				}
			}

			CKEDITOR.dialog.add( 'createwddiv', this.path + 'dialogs/wddiv.js' );
			CKEDITOR.dialog.add( 'editwddiv', this.path + 'dialogs/wddiv.js' );
		}
	});

	CKEDITOR.plugins.wddiv = {
		getSurroundDiv: function( editor, start ) {
			var path = editor.elementPath( start );
			return editor.elementPath( path.blockLimit ).contains( 'div', 1 );
		}
	};
})();
