<%@page import="au.net.webdirector.common.datalayer.client.ElementData"%>
<%@page import="java.util.List"%>
<%@page import="au.net.webdirector.common.datalayer.client.CategoryData"%>
<%@page import="au.net.webdirector.common.datalayer.client.conf.ModuleAccessConfig"%>
<%@page import="au.net.webdirector.common.datalayer.client.ModuleAccess"%>
<%@page import="au.net.webdirector.admin.modules.domain.Module"%>
<%@page import="au.net.webdirector.admin.modules.ModuleConfiguration"%>
<%
    response.setContentType("text/javascript");
    Module module = ModuleConfiguration.getInstance().getModule(request.getParameter("module"));
    String buttons = ","+module.getCustomisedButtons()+",";
%>
/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
  // Define changes to default configuration here. For example:
  // config.language = 'fr';
  // config.uiColor = '#AADC6E';
  	// extraAllowedContent:  'td[*];' //Enable all td attributes like width, height etc
    config.allowedContent = true; //Enable extra contents
    config.toolbar = 'MyToolbar';


	config.strinsert_strings = [
  <% 
    try{
      String dataItemModuleName = "DataItems";
      ModuleAccess ma = ModuleAccess.getInstance();
      ma.setConfig(ModuleAccessConfig.logicalLive().merge(ModuleAccessConfig.orderByDisplay()));
      CategoryData condition = new CategoryData();
      condition.put("attrdrop_module", request.getParameter("module"));
      List<CategoryData> dataModules = ma.getCategories(dataItemModuleName, condition);
      for(CategoryData dataModule: dataModules){
        List<CategoryData> groups = ma.getCategoriesByParentId(dataItemModuleName, dataModule.getId());
        for(CategoryData group : groups){
          List<ElementData> items = ma.getElementsByParentId(dataItemModuleName, group.getId());
          if(items.size()>0){
            %>
            {'name': '<%= group.getName() %>'},
            <%
          }
          for(ElementData item : items){
            %>
            {'name': '<%= item.getName() %>', 'value': '<%= item.getString("ATTR_Value") %>'},
            <%
          }
        }
      }
    }catch(Exception e){
  
    }
  %>
	];
	config.strinsert_button_label = 'DataItems';
    config.strinsert_button_title = config.strinsert_button_voice = 'Insert data items';

    config.toolbar_MyToolbar =
[
    ['CloseWindow'],
    [
  <% if(buttons.indexOf(",1,") >-1){ %>
      'Source',
  <% } %>  
      '-',
  <% if(buttons.indexOf(",2,") >-1){ %>
      'Save',
  <% } %>  
  <% if(buttons.indexOf(",3,") >-1){ %>
      'NewPage',
  <% } %>  
  <% if(buttons.indexOf(",4,") >-1){ %>
      'Preview',
  <% } %>  
      '-',
  <% if(buttons.indexOf(",5,") >-1){ %>
      'Templates'
  <% } %>  
    ],
    [
  <% if(buttons.indexOf(",6,") >-1){ %>
      'Cut',
  <% } %>  
  <% if(buttons.indexOf(",7,") >-1){ %>
      'Copy',
  <% } %>  
  <% if(buttons.indexOf(",8,") >-1){ %>
      'Paste',
  <% } %>  
  <% if(buttons.indexOf(",9,") >-1){ %>
      'PasteText',
  <% } %>  
      '-',
  <% if(buttons.indexOf(",10,") >-1){ %>
      'SpellChecker', 
  <% } %>  
    ],
    [
  <% if(buttons.indexOf(",11,") >-1){ %>
      'Undo',
  <% } %>  
  <% if(buttons.indexOf(",12,") >-1){ %>
      'Redo',
  <% } %>  
      '-',
  <% if(buttons.indexOf(",13,") >-1){ %>
      'Find',
  <% } %>  
  <% if(buttons.indexOf(",14,") >-1){ %>
      'Replace',
  <% } %>  
      '-',
  <% if(buttons.indexOf(",15,") >-1){ %>
      'SelectAll'
  <% } %>  
    ],
    [
  <% if(buttons.indexOf(",16,") >-1){ %>
      'Form',
  <% } %>  
  <% if(buttons.indexOf(",17,") >-1){ %>
      'Checkbox',
  <% } %>  
  <% if(buttons.indexOf(",18,") >-1){ %>
      'Radio',
  <% } %>  
  <% if(buttons.indexOf(",19,") >-1){ %>
      'TextField',
  <% } %>  
  <% if(buttons.indexOf(",20,") >-1){ %>
      'Textarea',
  <% } %>  
  <% if(buttons.indexOf(",21,") >-1){ %>
      'Select',
  <% } %>  
  <% if(buttons.indexOf(",22,") >-1){ %>
      'Button',
  <% } %>  
  <% if(buttons.indexOf(",23,") >-1){ %>
      'ImageButton',
  <% } %>  
  <% if(buttons.indexOf(",24,") >-1){ %>
      'HiddenField'
  <% } %>  
    ],
    '/',
    [
  <% if(buttons.indexOf(",25,") >-1){ %>
      'Bold',
  <% } %>  
  <% if(buttons.indexOf(",26,") >-1){ %>
      'Italic',
  <% } %>  
  <% if(buttons.indexOf(",27,") >-1){ %>
      'Underline',
  <% } %>  
  <% if(buttons.indexOf(",28,") >-1){ %>
      'Strike',
  <% } %>  
      '-',
  <% if(buttons.indexOf(",29,") >-1){ %>
      'Subscript',
  <% } %>  
  <% if(buttons.indexOf(",30,") >-1){ %>
      'Superscript',
  <% } %>  
  <% if(buttons.indexOf(",31,") >-1){ %>
      'RemoveFormat'
  <% } %>  
    ],
    [
  <% if(buttons.indexOf(",32,") >-1){ %>
      'NumberedList',
  <% } %>  
  <% if(buttons.indexOf(",33,") >-1){ %>
      'BulletedList',
  <% } %>  
      '-',
  <% if(buttons.indexOf(",34,") >-1){ %>
      'Outdent',
  <% } %>  
  <% if(buttons.indexOf(",35,") >-1){ %>
      'Indent',
  <% } %>  
  <% if(buttons.indexOf(",36,") >-1){ %>
      'Blockquote',
  <% } %>  
  <% if(buttons.indexOf(",37,") >-1){ %>
      'CreateDiv',
  <% } %>  
  <% if(buttons.indexOf(",60,") >-1){ %>
  	  'CreateWDDiv'
  <% } %>
    ],
    [
  <% if(buttons.indexOf(",38,") >-1){ %>
      'JustifyLeft',
  <% } %>  
  <% if(buttons.indexOf(",39,") >-1){ %>
      'JustifyCenter',
  <% } %>  
  <% if(buttons.indexOf(",40,") >-1){ %>
      'JustifyRight',
  <% } %>  
  <% if(buttons.indexOf(",41,") >-1){ %>
      'JustifyBlock'
  <% } %>  
    ],
    [
  <% if(buttons.indexOf(",42,") >-1){ %>
      'Link',
  <% } %>  
  <% if(buttons.indexOf(",43,") >-1){ %>
      'Unlink',
  <% } %>  
  <% if(buttons.indexOf(",44,") >-1){ %>
      'Anchor'
  <% } %>  
    ],
    [
  <% if(buttons.indexOf(",45,") >-1){ %>
      'Image',
  <% } %>  
  <% if(buttons.indexOf(",46,") >-1){ %>
      'VideoDetector',
  <% } %>  
  <% if(buttons.indexOf(",47,") >-1){ %>
      'Table',
  <% } %>  
  <% if(buttons.indexOf(",48,") >-1){ %>
      'HorizontalRule',
  <% } %>  
  <% if(buttons.indexOf(",49,") >-1){ %>
      'SpecialChar'
  <% } %>  
    ],
    '/',
    [
  <% if(buttons.indexOf(",50,") >-1){ %>
      'Format',
  <% } %>  
  <% if(buttons.indexOf(",51,") >-1){ %>
      'Font',
  <% } %>  
  <% if(buttons.indexOf(",52,") >-1){ %>
      'FontSize'
  <% } %>  
    ],
    [
  <% if(buttons.indexOf(",53,") >-1){ %>
      'TextColor',
  <% } %>  
  <% if(buttons.indexOf(",54,") >-1){ %>
      'BGColor'
  <% } %>  
    ],
  <% if(buttons.indexOf(",59,") >-1){ %>
    ['strinsert'],
  <% } %>
    [
  <% if(buttons.indexOf(",55,") >-1){ %>
      'Maximize',
  <% } %>  
  <% if(buttons.indexOf(",56,") >-1){ %>
      'ShowBlocks'
  <% } %>  
    ],
  <% if(buttons.indexOf(",57,") >-1){ %>
    ['CommonCSS'],
  <% } %>  
  <% if(buttons.indexOf(",58,") >-1){ %>
    ['About'],
  <% } %>  
];

    config.keystrokes = [
    [ CKEDITOR.ALT + 121 /*F10*/, 'toolbarFocus' ],
    [ CKEDITOR.ALT + 122 /*F11*/, 'elementsPathFocus' ],
    
    [ CKEDITOR.SHIFT + 121 /*F10*/, 'contextMenu' ],

    [ CKEDITOR.CTRL + 83 /*S*/, 'save' ],
    
    // [ CKEDITOR.CTRL + 90 /*Z*/, 'undo' ],
    [ CKEDITOR.CTRL + 89 /*Y*/, 'redo' ],
    [ CKEDITOR.CTRL + CKEDITOR.SHIFT + 90 /*Z*/, 'redo' ],

    [ CKEDITOR.CTRL + 76 /*L*/, 'link' ],

    [ CKEDITOR.CTRL + 66 /*B*/, 'bold' ],
    [ CKEDITOR.CTRL + 73 /*I*/, 'italic' ],
    [ CKEDITOR.CTRL + 85 /*U*/, 'underline' ],

    [ CKEDITOR.ALT + 109 /*-*/, 'toolbarCollapse' ]
    ];
    
    config.templates = 'webdirector';
    
    config.templates_files =
    [
        // '/jsp/ckeditor/plugins/templates/templates/default.js',
        '/webdirector/adminExtra/ckeditor/templates'
    ];
};
