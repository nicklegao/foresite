<%@page import="au.com.ci.webdirector.user.UserManager"%>
<%@page import="java.util.Vector"%>
<jsp:useBean id="db" class="webdirector.db.UserUtils"/>

<!DOCTYPE html>
<html>
<%
String userId = UserManager.getUserId(request);
String [] vals = null;
if (userId != null && !userId.equals("")) {
	Vector v = db.getUser(userId);
	vals = (String[]) v.elementAt(0);
}

String access_file_manager = vals != null? vals[8] : "no";
%>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>File Manager</title>
<BASE HREF="/adminExtra/filemanager/index.jsp">
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
<link rel="stylesheet" type="text/css" href="styles/reset.css" />
<link rel="stylesheet" type="text/css" href="styles/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="scripts/jquery.filetree/jqueryFileTreeWD.css" />
<link rel="stylesheet" type="text/css" href="scripts/jquery.contextmenu/jquery.contextMenu-1.01.css" />
<link rel="stylesheet" type="text/css" href="scripts/custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" />
<style type="text/css">
	#loading-wrap {
		position:fixed;
		height:100%;
		width:100%;
		overflow:hidden;
		top:0;
		left:0;
		display:block;
		background: white url(./images/wait30trans.gif) no-repeat center center;
		z-index:999;
	}
</style>

<!-- CSS dynamically added using 'config.options.theme' defined in config file -->
</head>
<body>
<div id="loading-wrap"><!-- loading wrapper / removed when loaded --></div>
<div>
<form id="uploader" method="post">
	<h1></h1>
	<div id="uploadresponse"></div>
	<button id="level-up" name="level-up" type="button" value="LevelUp"><i class="fa fa-level-up fa-rotate-270"></i></button>
	<button id="home" name="home" type="button" value="Home"><i class="fa fa-home"></i></button>
	<input id="mode" name="mode" type="hidden" value="add" /> 
	<input id="currentpath" name="currentpath" type="hidden" />
	<div id="file-input-container">
		<div id="alt-fileinput">
			<input id="filepath" name="filepath" type="text" /><button id="browse" name="browse" type="button" value="Browse"></button>
		</div>
		<input	id="newfile" name="newfile" type="file" />
	</div>
	<button id="upload" name="upload" type="submit" value="Upload" class="em"><i class="fa fa-upload"></i> </button>
	<button id="newfolder" name="newfolder" type="button" value="New Folder" class="em"><i class="fa fa-folder"></i> </button>
	<button id="grid" class="ON" type="button"><i class="fa fa-th"></i></button>
	<button id="list" type="button"><i class="fa fa-list"></i></button>
</form>
<div>
<form name="search" id="search" method="get">
		<div>
			<input type="text" value="" name="q" id="q" autocomplete="off" />
			<a id="reset" href="#" class="q-reset"></a>
			<span class="q-inactive"></span>
		</div> 
</form>
<div id="folder-info"></div>
</div>
<div id="splitter">
<div id="filetree"></div>
	<div id="fileinfo">
	<h1></h1>
	</div>
</div>
<div id="footer">

</div>
<div id="drop-overlay" style="width:100%;height:100%;top:0;right:0;bottom:0;left:0;z-index:1000;"></div>
<ul id="itemOptions" class="contextMenu">
	<li class="select"><a href="#select"></a></li>
	<li class="download"><a href="#download"></a></li>
	<li class="rename"><a href="#rename"></a></li>
	<li class="move"><a href="#move"></a></li>
	<li class="replace"><a href="#replace"></a></li>
	<li class="delete separator"><a href="#delete"></a></li>
</ul>

<script type="text/javascript" src="scripts/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="scripts/jquery.form-3.24.js"></script>
<script type="text/javascript" src="scripts/jquery.splitter/jquery.splitter-1.5.1.js"></script>
<script type="text/javascript" src="scripts/jquery.filetree/jqueryFileTree.js"></script>
<script type="text/javascript" src="scripts/jquery.contextmenu/jquery.contextMenu-1.01.js"></script>
<script type="text/javascript" src="scripts/jquery.impromptu-3.2.min.js"></script>
<script type="text/javascript" src="scripts/jquery.tablesorter-2.7.2.min.js"></script>
<script type="text/javascript" src="scripts/filemanager.js"></script>
<script src="/webdirector/free/csrf-token/init"></script>
<script>
	var accessFileManager = <%= access_file_manager.equalsIgnoreCase("yes") %>
</script>
</div>
</body>
</html>