

/**
 * Here is is the code snippet to initialize Firebase Messaging in the Service
 * Worker when your app is not hosted on Firebase Hosting.


 // Retrieve an instance of Firebase Messaging so that it can handle background
 // messages.
 const messaging = firebase.messaging();
 // [END initialize_firebase_in_sw]
 **/

// [START initialize_firebase_in_sw]
// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-messaging.js');

var config = {
	apiKey: "AIzaSyCjGIC8Jg0d1rEececEMEJW0QMHi0N5P2o",
	authDomain: "quotecloud-55f9a.firebaseapp.com",
	databaseURL: "https://quotecloud-55f9a.firebaseio.com",
	projectId: "quotecloud-55f9a",
	storageBucket: "quotecloud-55f9a.appspot.com",
	messagingSenderId: "250598880955"
};

firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
	console.log('[firebase-messaging-sw.js] Received background message ', payload);
	// Customize notification here
	const notificationTitle = 'Background Message Title';
	const notificationOptions = {
		body: 'Background Message body.'
	};

	return self.registration.showNotification(notificationTitle,
		notificationOptions);
});

// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]

// [END background_handler]
