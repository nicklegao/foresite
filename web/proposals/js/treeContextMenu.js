var lastAction = null;
  
function setLastAction(action) {
	lastAction = action;
}

function getLastAction() {
	return lastAction;
}

$(function() {
  var moduleMenuItems = {
	  configure: {
        name: "<i class=\"fa fa-cogs fa-fw\"></i> &nbsp; Configure",
        hidden: isModuleConfigureHidden,
        callback: configureModule
      },
      separator1: "-",
      
      insert: {
        name: "<i class=\"fa fa-plus-circle fa-fw\"></i> &nbsp; Insert",
        hidden: isModuleInsertHidden,
        callback: insertFirstLevelCategory
      },
      import: {
        name: "<i class=\"fa fa-download fa-fw\"></i> &nbsp; Import",
        hidden: isImportHidden,
        callback: importData
      },
      separator3: "-",
      refresh: {
        name: "<i class=\"fa fa-refresh fa-fw\"></i> &nbsp; Refresh",
        hidden: isModuleRefreshHidden,
        callback: refreshModule
      },
      close: {
        name: "<i class=\"fa fa-times-circle fa-fw\"></i> &nbsp; Close",
        callback: closeModule
      }
  }
  
  var categoryMenuItems = {
      insert: {
        name: "<i class=\"fa fa-plus-circle fa-fw\"></i> &nbsp; Insert",
        hidden: isInsertHidden,
        callback: insertCategoryOrElement
      },
      view: {
        name: "<i class=\"fa fa-eye fa-fw\"></i> &nbsp; View",
        hidden: isViewHidden,
        callback: viewOrEdit
      },
      edit: {
        name: "<i class=\"fa fa-pencil fa-fw\"></i> &nbsp; Edit",
        hidden: isEditHidden,
        callback: viewOrEdit
      },
      del: {
        name: "<i class=\"fa fa-trash-o fa-fw\"></i> &nbsp; Delete",
        hidden: isDeleteHidden,
        callback: deleteCategoryOrElement
      },
      separator1: "-",
      sort: {
        name: "<i class=\"fa fa-sort fa-fw\"></i> &nbsp; Sort",
        hidden: isSortHidden,
        callback: sortCategoryOrElement
      },
      separator2: "-",
      cancel: {
        name: "<i class=\"fa fa-times fa-fw\"></i> &nbsp; Cancel",
        hidden: isCancelHidden,
        callback: cancelLastAction
      },
      paste: {
        name: "<i class=\"fa fa-clipboard fa-fw\"></i> &nbsp; Paste",
        hidden: isPasteHidden,
        disabled: isPasteDisabled,
        callback: pasteCategoryOrElement
      },
      copy: {
        name: "<i class=\"fa fa-files-o fa-fw\"></i> &nbsp; Copy",
        hidden: isCopyHidden,
        callback: cutOrCopy
      },
      copyAll: {
        name: "<i class=\"fa fa-files-o fa-fw\"></i> &nbsp; Copy All",
        hidden: isCopyHidden,
        callback: cutOrCopyAll
      },
      cut: {
        name: "<i class=\"fa fa-scissors fa-fw\"></i> &nbsp; Cut",
        hidden: isCutHidden,
        callback: cutOrCopy
      },
      cutAll: {
        name: "<i class=\"fa fa-scissors fa-fw\"></i> &nbsp; Cut All",
        hidden: isCutHidden,
        callback: cutOrCopyAll
      },
      separator3: "-",
      import: {
        name: "<i class=\"fa fa-download fa-fw\"></i> &nbsp; Import",
        hidden: isImportHidden,
        callback: importData
      },
      export: {
        name: "<i class=\"fa fa-upload fa-fw\"></i> &nbsp; Export",
        hidden: isExportHidden,
        callback: exportData
      },
      separator4: "-",
      refresh: {
        name: "<i class=\"fa fa-refresh fa-fw\"></i> &nbsp; Refresh",
        callback: refresh
      }
  };
  
  var elementMenuItems = {
/*      view: {
        name: "View",
        hidden: isViewHidden,
        callback: viewOrEdit
      },
      edit: {
        name: "Edit",
        hidden: isEditHidden,
        callback: viewOrEdit
      },*/
      del: {
        name: "<i class=\"fa fa-trash-o fa-fw\"></i> &nbsp; Delete",
        hidden: isDeleteHidden,
        callback: deleteCategoryOrElement
      },
      separator1: "-",
      sort: {
        name: "<i class=\"fa fa-sort fa-fw\"></i> &nbsp; Sort",
        hidden: isSortHidden,
        callback: sortCategoryOrElement
      },
      separator2: "-",
      cancel: {
        name: "<i class=\"fa fa-times fa-fw\"></i> &nbsp; Cancel",
        hidden: isCancelHidden,
        callback: cancelLastAction
      },
      copy: {
        name: "<i class=\"fa fa-files-o fa-fw\"></i> &nbsp; Copy",
        hidden: isCopyHidden,
        callback: cutOrCopy
      },
      copyAll: {
        name: "<i class=\"fa fa-files-o fa-fw\"></i> &nbsp; Copy All",
        hidden: isCopyHidden,
        callback: cutOrCopyAll
      },
      cut: {
        name: "<i class=\"fa fa-scissors fa-fw\"></i> &nbsp; Cut",
        hidden: isCutHidden,
        callback: cutOrCopy
      },
      cutAll: {
        name: "<i class=\"fa fa-scissors fa-fw\"></i> &nbsp; Cut All",
        hidden: isCutHidden,
        callback: cutOrCopyAll
      },
      separator3: "-",
      import: {
        name: "<i class=\"fa fa-download fa-fw\"></i> &nbsp; Import",
        hidden: isImportHidden,
        callback: importData
      },
      separator4: "-",
      refresh: {
        name: "<i class=\"fa fa-refresh fa-fw\"></i> &nbsp; Refresh",
        callback: refresh
      }
  };
  
  var $aside = $("#left-panel")
  
  $aside.contextMenu({
    selector: "nav > ul > li > a",
    items: moduleMenuItems
  });
  
  $aside.contextMenu({
    selector: ".fancytree-folder > .fancytree-icon, .fancytree-folder > .fancytree-title",
    items: categoryMenuItems
  });
  
  $aside.contextMenu({
    selector: ".fancytree-node:not(.fancytree-folder) > .fancytree-icon, .fancytree-node:not(.fancytree-folder) > .fancytree-title",
    items: elementMenuItems
  });

  function getNode(options) {
    var $el = options.$trigger.parent();
    return $.ui.fancytree.getNode($el);
  }
  
  function getPrivileges(options) {
    var $el = options.$trigger.parent();
    var node = $.ui.fancytree.getNode($el);
    return node.folder ? node.data.privileges : node.parent.data.privileges;
  }
  
  function getModuleSettings(options) {
    var $el = options.$trigger.parent();
    return $el.closest(".module").data("module");
  }
  
  function isSortHidden(key, options) {  
    var module = getModuleSettings(options);
    return !module.has_sort;
  }

  function isViewHidden(key, options) {
    var privileges = getPrivileges(options);
    return privileges === null
      || privileges.edit
      || (!privileges.view && !privileges.edit);
  }

  function isEditHidden(key, options) {
    var privileges = getPrivileges(options);
    return privileges !== null
      && !privileges.edit;
  }

  function isInsertHidden(key, options) {
    var privileges = getPrivileges(options);
    return privileges !== null
      && !privileges.insert;
  }
  
  function isModuleInsertHidden(key, options) {
    var $el = options.$trigger.parent();
    if (!$el.hasClass("open")) {
      return true;
    }
    var node = $.ui.fancytree.getNode($el.find(".root-category"));
    return !node || (node.data.privileges && !node.data.privileges.insert);
  }
  function isModuleRefreshHidden(key, options) {
    var $el = options.$trigger.parent();
    if (!$el.hasClass("open")) {
      return true;
    }
  }

  function isDeleteHidden(key, options) {
    var module = getModuleSettings(options);
    if (module.workflow_enabled && !UserDataHelper.isAdmin()) {
      return true;
    }
    
    var node = getNode(options);
    if (node.data.workflow) {
      return true;
    }
    
    var privileges = getPrivileges(options);
    return privileges !== null
      && !privileges.del;
    }
  
  function isCutHidden(key, options) {
    var module = getModuleSettings(options);
    if (!module.has_cut) {
      return true;
    }
    
    if (module.workflow_enabled && !UserDataHelper.isAdmin()) {
      return true;
    }
    
    var node = getNode(options);
    if (node.data.workflow) {
      return true;
    }
    
    var privileges = getPrivileges(options);
    return privileges !== null
      && !privileges.del;
  }
  
  function isCopyHidden(key, options) {
    var module = getModuleSettings(options);
    if (!module.has_copy) {
      return true;
    }
    
    if (module.workflow_enabled && !UserDataHelper.isAdmin()) {
      return true;
    }
    
    var node = getNode(options);
    if (node.data.workflow) {
      return true;
    }
    
    return false;
  }
  
  function isModuleConfigureHidden(key, options) {
	return !UserDataHelper.isAdmin();
  }
  
  function isImportHidden(key, options) {
    var module = getModuleSettings(options);
    if (!module.has_import) {
      return true;
    }
    return !UserDataHelper.isAdmin();
  }
  
  function isExportHidden(key, options) {
    var module = getModuleSettings(options);
    if (!module.has_export) {
      return true;
    }
    return !UserDataHelper.isAdmin();
  }

  function viewOrEdit(key, options) {
    var $el = options.$trigger.parent();
    var node = $.ui.fancytree.getNode($el);
    var module = $el.closest(".module").data("module");
    var params = {};
    var key = node.key;
    if (node.data.workflow) {
      key = 0;
      params = {
        draft : true,
        DID : node.data.draftId
      };
    }
    if (node.folder) {
      WidgetHelper.addWidgetForModuleCategoryId(params, module.module_name, key, node.parent.key);
    } else {
      WidgetHelper.addWidgetForModuleElementId(params, module.module_name, key, node.parent.key);
    }
  }
  
  function configureModule(key, options) {
	var $el = options.$trigger.parent();
    var node = $.ui.fancytree.getNode($el);
    var module = $el.closest(".module").data("module");
    
    ModalHelper.launchModal('/admin/jsp/configureModuleSettings.jsp?moduleName=' + module.module_name);
  }

  function insertCategoryOrElement(key, options) {
    var $el = options.$trigger.parent();
    var node = $.ui.fancytree.getNode($el);
    var module = $el.closest(".module").data("module");
    if (node.data.folderLevel < module.levels) {
      WidgetHelper.addWidgetForInsertCategory({}, module.module_name, node.key);
    } else {
      WidgetHelper.addWidgetForInsertElement({}, module.module_name, node.key);
    }
  }
  
  function insertFirstLevelCategory(key, options) {
    var $el = options.$trigger.parent().find(".root-category");
    var node = $.ui.fancytree.getNode($el);
    var module = $el.closest(".module").data("module");
    WidgetHelper.addWidgetForInsertCategory({}, module.module_name, node.key);
  }

  function deleteCategoryOrElement(key, options) {
    var $el = options.$trigger.parent();
    var node = $.ui.fancytree.getNode($el);
    var module = $el.closest(".module").data("module");
    var isCategory = node.folder;
    
    $.SmartMessageBox({
      title : "Delete " + (isCategory? "Category" : "Asset"),
      content : "Are you sure you want to delete " + node.title + (isCategory? " and all its children" : "") + "... permanently?",
      buttons : '[No][Yes]'
    }, function(ButtonPressed) {
      if (ButtonPressed === "Yes") {
        var openLazyTree = $(".open .lazytree");
        showLoadingBackground(openLazyTree);
        $.ajax({
          url: isCategory ? "/wd-api/folderTree/deleteCategory" : "/wd-api/folderTree/deleteElement",
          data: {
            moduleName : module.module_name,
            id : node.key
          },
          type: "POST"
        }).done(function(success) {
          if (success) {
            showNotification({success: success, content: node.title + " deleted... permanently."});
          } else {
            showNotification({success: success, content: "Could not delete " + node.title});
          }
          hideLoadingBackground(openLazyTree, function() {
            TreeHelper.reloadNode(node.parent, false);
          })
        });
      }
    });
  }

  function sortCategoryOrElement(key, options) {
    var $el = options.$trigger.parent();
    var node = $.ui.fancytree.getNode($el);
    var module = $el.closest(".module").data("module");
    ModalHelper.launchModal('/admin/jsp/categoryOrder.jsp?moduleName=' + module.module_name + '&parentId=' + node.parent.key + '&isCategory=' + (node.folder === true));
  }

  function cutOrCopy(key, options) {
    resetCheckboxes();
    var $el = options.$trigger.parent();
    var node = $.ui.fancytree.getNode($el);
    var module = $el.closest(".module").data("module");
    
    $(node.li).parent().addClass("show-checkboxes");
    node.setSelected();
    lastAction = {
      type: key,
      module: module.module_name,
      parentNode: node.parent,
      isCategory: node.folder
    };
    setLastAction(lastAction);
  }

  function cutOrCopyAll(key, options) {
    resetCheckboxes();
    var $el = options.$trigger.parent();
    var node = $.ui.fancytree.getNode($el);
    var module = $el.closest(".module").data("module");
    
    $(node.li).parent().addClass("show-checkboxes");
    $.each(node.parent.getChildren(), function(index, item) {
      item.setSelected();
    });
    
    lastAction = {
      type: key,
      module: module.module_name,
      parentNode: node.parent,
      isCategory: node.folder
    };
    setLastAction(lastAction);
  }
  
  function isCancelHidden(key, options) {
    var $el = options.$trigger.parent();
    var module = $el.closest(".module").data("module");
    var lastAction = getLastAction();
    return !lastAction ||
           lastAction.module !== module.module_name;
  }
  
  function isPasteHidden(key, options) {
    var privileges = getPrivileges(options);
    var $el = options.$trigger.parent();
    var module = $el.closest(".module").data("module");
    var lastAction = getLastAction();
    return !lastAction ||
           lastAction.module !== module.module_name ||
           (privileges !== null && !privileges.insert);
  }
  
  function isPasteDisabled(key, options) {
    var $el = options.$trigger.parent();
    var node = $.ui.fancytree.getNode($el);
    var module = $el.closest(".module").data("module");
    var lastAction = getLastAction();
    return !lastAction ||
           lastAction.parentNode.data.folderLevel !== node.data.folderLevel ||
           !hasSelected(lastAction.parentNode);
  }
  
  function cancelLastAction(key, options) {
    resetCheckboxes();
  }
  
  function pasteCategoryOrElement(key, options) { 
    var $el = options.$trigger.parent();
    var node = $.ui.fancytree.getNode($el);
    var module = $el.closest(".module").data("module");
    var lastAction = getLastAction();
    var lastNodeId = lastAction.parentNode.key;
    var parentId = node.key;
    var urlPath;
    if (lastAction.type === "copy" || lastAction.type === "copyAll") {
      urlPath = lastAction.isCategory ? "copyCategories" : "copyElements";
    } else if (lastAction.type === "cut" || lastAction.type === "cutAll") {
      urlPath = lastAction.isCategory ? "moveCategories" : "moveElements";
    }
    
    var openLazyTree = $(".open .lazytree");
    showLoadingBackground(openLazyTree);
    $.ajax({
      url: '/wd-api/folderTree/' + urlPath,
      data: {
        items: getSelectedItems(lastAction.parentNode),
        parentId: node.key,
        moduleName: module.module_name
      },
      type: "POST"
    }).done(function(success) {
      if (success) {
        //showNotification({success: success, content: node.title + " deleted"});
      } else {
        //showNotification({success: success, content: "Could not delete " + node.title});
      }
      hideLoadingBackground(openLazyTree, function() {
        TreeHelper.reloadNode(lastAction.parentNode, false);
        TreeHelper.reloadNode(node, false);
        //RELOADING AFFECTED BULK EDITORS.
        if(lastAction.isCategory) {
        	var oldWidgetId = WidgetHelper.widgetIdForCategoryList(lastAction.module, lastNodeId);
        	var newWidgetId = WidgetHelper.widgetIdForCategoryList(lastAction.module, parentId);
        } else {
        	var oldWidgetId = WidgetHelper.widgetIdForElementList(lastAction.module, lastNodeId);
        	var newWidgetId = WidgetHelper.widgetIdForElementList(lastAction.module, parentId);
        }
        var $oldWidget = $("#" + oldWidgetId);
    	var $newWidget = $("#" + newWidgetId);
    	BulkEditor.reloadBulkEditor($oldWidget);
    	BulkEditor.reloadBulkEditor($newWidget);
        resetCheckboxes();
        
        ///WIDGETHELPER DIALOGBOX
        ///
      });
    });
  }
  
  function refreshModule(key, options) {
    var $el = options.$trigger;
    var module = $el.closest(".module").data("module");
    TreeHelper.reloadChildren(module.module_name, "0", true);
  }
  
  function refresh(key, options) {
    var $el = options.$trigger;
    var node = $.ui.fancytree.getNode($el);
    if (node.folder) {
      TreeHelper.reloadNode(node, true);
    } else {
      TreeHelper.reloadNode(node.parent, true);
    }
    
  }
  
  function importData(key, options) {
    var $el = options.$trigger;
    var module = $el.closest(".module").data("module");
    ModalHelper.loadInIframe('/admin/jsp/secure/iframe/data_import_page1.jsp?moduleName=' + module.module_name + '&moduleDescription=' + module.module_description);
  }
  
  function exportData(key, options) {
    var $el = options.$trigger.parent();
    var node = $.ui.fancytree.getNode($el);
    var module = $el.closest(".module").data("module");
    ModalHelper.loadInIframe('/admin/jsp/secure/iframe/exportElements.jsp?module=' + module.module_name + '&categoryId=' + node.key);
  }
  
  function closeModule(key, options) {
    var $el = options.$trigger;
    var module = $el.parent().data("module");
    ModuleHelper.removeModule(module.module_name);
  }

  function resetCheckboxes() {
    var $parent = $(".show-checkboxes");
    $parent.each(function(index, parent) {
      var node = $.ui.fancytree.getNode(parent);
      $.each(node.getChildren(), function(index, item) {
        item.setSelected(false);
      });
      $(parent).removeClass("show-checkboxes");
    });
    setLastAction(null);
  }
  
  function hasSelected(node) {
    var items = node.getChildren();
    for (var i=0; i < items.length ; i++) {
      if (items[i].selected)
        return true;
    }
    return false;
  }
  
  function getSelectedItems(node) {
    var items = node.getChildren();
    var selectedItems = [];
    for (var i=0; i < items.length ; i++) {
      if (items[i].selected)
        selectedItems.push(items[i].key);
    }
    return selectedItems;
  }
  
  function showLoadingBackground(openLazyTree) {
    var $background = $('<div class="loading-background" style="display: none"><span><i class="fa fa-spinner fa-pulse fa-3x"></i></span></div>');
    openLazyTree.append($background);
    $background.fadeIn();
  }
  
  function hideLoadingBackground(openLazyTree, callback) {
    $(".loading-background", openLazyTree).fadeOut(200, function() {
      $(this).remove();
      if (callback && $.isFunction(callback)) {
        callback();
      }
    });
  }
  
});