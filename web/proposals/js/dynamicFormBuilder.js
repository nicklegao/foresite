var DynamicFormValidator = {
    Categories:{},
    Elements:{}
}

var debugObject;
var DynamicFormBuilder = {
    init: function($widget){
      var priv = parseInt($('.dynamic-form-container').data('priv'), 10);
      var request = JSON.parse($widget.data('requestData'));
      console.log(request);
      debugObject = $widget;
      var $form = $('form.form-horizontal', $widget);
      $form.data('orig', $form.formSerialize());
      
      jQuery.validator.addMethod("integer", function(value, element) {
        return this.optional(element) || value === '' + parseInt(value, 10);
      }, "Please enter a valid integer");

      
      var loadFormValidationSettings = function ($form){
        var settings = {
          rules:{
          },
          errorPlacement : function(error, element) {
            error.insertAfter(element);
          }
        };
        $('.form-group.required .form-control',$form).each(function(){
          settings.rules[this.name] = settings.rules[this.name] || {};
          settings.rules[this.name]['required'] = true;
        });
        $('.form-control[data-max-length]',$form).each(function(){
          settings.rules[this.name] = settings.rules[this.name] || {};
          settings.rules[this.name]['maxlength'] = parseInt($(this).attr('data-max-length'), 10);
        });
        $('.form-control.input-money, .form-control.input-float',$form).each(function(){
          settings.rules[this.name] = settings.rules[this.name] || {};
          settings.rules[this.name]['number'] = true;
        });
        $('.form-control.input-integer',$form).each(function(){
          settings.rules[this.name] = settings.rules[this.name] || {};
          settings.rules[this.name]['integer'] = true;
        });
        console.log(settings)
        return settings;
      };
      
      var validator = $form.validate(loadFormValidationSettings($form));
      var initAjaxForm = function($form, type){
        
        $form.ajaxForm({
          url: '/wd-api/form/' + type, 
          beforeSerialize: function($form, options) { 
            // do validation here
            var action = $('input[name="action"]', $form).val();
            if(type == 'save' && !$form.valid()){
              return false;
            }
            if(action == 'update' && !onUpdate($widget)){
              return false;
            }
            if(action == 'insert' && !onCreate($widget)){
              return false;
            }
            // return false to stop submit
            if(type != 'review' && $form.data('orig') == $form.formSerialize()){
              console.log('No change');
              showNotification({
                success: false,
                title: 'Nothing Changed',
                content: 'Please change something first',
                container: $('.widget-body', $widget)
              })
              $('.fa-refresh.fa-spin', $form).hide();
              $('.btn.disabled', $form).removeClass('disabled');
              return false;
            }
            WidgetHelper.blockUI($widget);
          },
          success:    function(response,status,xhr,$form ) { 
            WidgetHelper.unblockUI($widget);
            if (response.success)
            {
              $widget.data("autoclose", true);
              showNotification({
                success: true,
                title: 'Success',
                content: 'Data saved successfully',
                container:  $('.widget-body', $widget)
                }, function(){
                  WidgetHelper.closeUnlockedWidget($widget);
                });
            }
            else
            {
              showNotification({
                success: false,
                title: 'Error',
                content: response.message,
                container:  $('.widget-body', $widget)
              });
            }
            TreeHelper.reloadChildren(request.module, request.PID);
            $('.fa-refresh.fa-spin', $form).hide();
            $('.btn.disabled', $form).removeClass('disabled');
          }
        });
      };

      
      var onCreate = function($widget){
        if(DynamicFormValidator 
          && DynamicFormValidator[request.tableType]
          && DynamicFormValidator[request.tableType][request.module] 
          && DynamicFormValidator[request.tableType][request.module]($form).onCreate){
            var result = DynamicFormValidator[request.tableType][request.module]($form).onCreate();
            if(typeof result == 'undefined'){
              return true;
            }
            if(typeof result == 'string'){
              showNotification({
                success: false,
                title: 'Cannot create',
                content: 'Reason: ' + result,
                container: $('.widget-body', $widget)
              })
              return false;
            }
            if(typeof result == 'object'){
              validator.showErrors(result);
              showNotification({
                success: false,
                title: 'Cannot create',
                content: 'Please refer the tips in the form',
                container: $('.widget-body', $widget)
              })
              return false;
            }
            return false;
              
        }
        return true;
      }
      
      var onUpdate = function($widget){
        if(DynamicFormValidator 
          && DynamicFormValidator[request.tableType]
          && DynamicFormValidator[request.tableType][request.module] 
          && DynamicFormValidator[request.tableType][request.module]($form).onUpdate){
            var result = DynamicFormValidator[request.tableType][request.module]($form).onUpdate();
            if(typeof result == 'undefined'){
              return true;
            }
            if(typeof result == 'string'){
              showNotification({
                success: false,
                title: 'Cannot update',
                content: 'Reason: ' + result,
                container: $('.widget-body', $widget)
              })
              return false;
            }
            if(typeof result == 'object'){
              validator.showErrors(result);
              showNotification({
                success: false,
                title: 'Cannot update',
                content: 'Please refer the tips in the form',
                container: $('.widget-body', $widget)
              })
              return false;
            }
            return false;
              
        }
        return true;
      }

      var onDelete = function($widget){
        if(!confirm('Confirm to delete?')){
          return false;
        }
        if(DynamicFormValidator 
          && DynamicFormValidator[request.tableType]
          && DynamicFormValidator[request.tableType][request.module] 
          && DynamicFormValidator[request.tableType][request.module]($form).onDelete){
            var result = DynamicFormValidator[request.tableType][request.module]($form).onDelete();
            if(typeof result == 'undefined'){
              return true;
            }
            if(typeof result == 'string'){
              showNotification({
                success: false,
                title: 'Cannot delete',
                content: 'Reason: ' + result,
                container: $('.widget-body', $widget)
              })
              return false;
            }
            if(typeof result == 'object'){
              validator.showErrors(result);
              showNotification({
                success: false,
                title: 'Cannot delete',
                content: 'Please refer the tips in the form',
                container: $('.widget-body', $widget)
              })
              return false;
            }
            return false;
              
        }
        return true;
      }

      var showHistory = function() {
        var module = $('input[name="module"]', $form).val();
        var dataId = $('input[name="ID"]', $form).val();
        var tableType = $('input[name="table"]', $form).val();
        
        ModalHelper.slideOpenPanel('/admin/jsp/versionHistory.jsp', {
          module : module,
          dataId : dataId,
          tableType : tableType
        });
      }
      
      var initMultiSelector = function(selector){
        selector.show();
        var _width = selector.width(); 
        selector.before('<div class="loading" style="width:'+_width+'px;position:absolute;height:200px;background:#000;opacity:0.7;color:#fff;font-face:sans-serif;"><div style="margin-top:80px;text-align:center;font-size:1.4em;font-weight:bold;">Loading</div></div>');
        setTimeout(function() {
          selector.multiselect().next().find('input.search').width('70%');
        },100)
      };
      
      var loadDraft = function(div){
        var $div = $(div);
        var internalName = $('input[name="module"]', $form).val();
        var elementID = $('input[name="ID"]', $form).val();
        var parentID = $('input[name="PID"]', $form).val();
        var tableType = $('input[name="table"]', $form).val();
        
        var request = $widget.data("request");
        if (request != undefined)
        {
          request.abort();
        }

        request = $.ajax({
          url: '/wd-api/edit',
          data: {
            module : internalName,
            tableType : tableType,
            ID : elementID,
            PID : parentID,
            action: 'update',
            draft : true
          }
        }).done(function( data, textStatus, jqXHR ) {
          $widget.find(".widget-body").html(data);
        }).done(function( data, textStatus, jqXHR ) {
          DynamicFormBuilder.init($widget);
        });
        $widget.data("request", request)
      };
      
      $("a.removeNoteAnchor", $form).click(function(event){
        event.preventDefault();
        var $a = $(this);
        $a.hide();
        var $span = $('<span>').insertAfter($a);
        $('<span>Confirm?</span>').appendTo($span);
        $('<a href="#"><i class="fa fa-check"></i></a>').appendTo($span).click(function(event){
          event.preventDefault();
          $.ajax({
            url: '/wd-api/note/delete',
            data: { id: $span.prev().attr("id") }
          }).done(function( data, textStatus, jqXHR ) {
            if (data) {
              $a.closest('.note').remove();
            }else{
              $span.remove();
              $a.show();
            }
          });
        });
        $('<a href="#"><i class="fa fa-times"></i></a>').appendTo($span).click(function(event){
          event.preventDefault();
          $span.remove();
          $a.show();
        });
        setTimeout(function(){
          $span.remove();
          $a.show()
        }, 2000);
      });
      
      $("a.removeFileAnchor", $form).click(function(event){
        event.preventDefault();
        var $a = $(this);
        $a.hide();
        var $span = $('<span>').insertAfter($a);
        $('<span>Confirm?</span>').appendTo($span);
        $('<a href="#"><i class="fa fa-check"></i></a>').appendTo($span).click(function(event){
          event.preventDefault();
          $.ajax({
            url: '/wd-api/file/delete',
            data: { 
              id: $a.data("id"),
              type: 'Elements',
              col: $a.data('field'),
              file: $a.data('file'),
              module: request.module
            }
          }).done(function( data, textStatus, jqXHR ) {
            if (data) {
              $a.closest('.desc').parent().remove();
            }else{
              $span.remove();
              $a.show();
            }
          });
        });
        $('<a href="#"><i class="fa fa-times"></i></a>').appendTo($span).click(function(event){
          event.preventDefault();
          $span.remove();
          $a.show();
        });
        setTimeout(function(){
          $span.remove();
          $a.show()
        }, 2000);
      });
      
      $('input:checkbox[name^="DUMMY_"]', $form).change(function(){
        var $input = $(this).prev('input:hidden');
        if($(this).is(':checked')){
          $input.val('1');
        }else{
          $input.val('0');
        }
      }).change();
      
      $('input.open-sharedfile-manager', $form).click(function(){
        var index = DynamicFormBuilder.indexOf($widget);
        var urlString = '/adminExtra/filemanager/index.html?fileRoot=/_sharedfiles/&WDWidget='+ index +'&langCode=en&WDField=' + this.id
        ModalHelper.loadInIframe(urlString);
      })
      
      $('input.open-editor', $form).click(function(){
        var urlString = '/adminExtra/FCK/editor.jsp?colNum=0&id='+request.ID+'&module='+request.module+'&action='+request.action+'&colName=' + $(this).attr('id') + '&tableType=Elements';
        ModalHelper.loadInIframe(urlString);
      })
      
      $('.save-direct-btn', $form).click(function(){
    	  var btn = $(this);
    	  
        if(btn.hasClass('disabled')){
          return false;
        }
        if ($('input[name="hasDraft"]', $form).val() === "true" && $('input[name="ID"]', $form).val() !== "0") {
        	$.SmartMessageBox({
      	      title : "You are about to overwrite a draft.",
      	      content : "Do you want to continue? ",
      	      buttons : '[No][Yes]'
      	    }, function(ButtonPressed) {
      	      if (ButtonPressed === "No") {
      	    	return false;
      	      } else if (ButtonPressed === "Yes") {
      	    	$('input[name="sendOldDraftInEmail"]', $form).val("true");
      	    	initAjaxForm($form, 'save');
      	    	btn.closest('form').find('input[name="status"]').val("").end().submit();
      	      }
      	   });
        	
        } else {
        	initAjaxForm($form, 'save');
        	btn.closest('form').find('input[name="status"]').val("").end().submit();
        }
        
      });
      
      
      
      $('.save-draft-btn', $form).click(function(){
    	var $btn = $(this);
    	  
        if($btn.hasClass('disabled')){
          return false;
        }
        
        DynamicFormBuilder.checkConcurrentDraftOverwriteThenSave("DRAFT", $form, $btn, initAjaxForm);
      });

      $('.save-submit-btn', $form).click(function(){
    	  var $btn = $(this);
    	  
        if($(this).hasClass('disabled')){
          return false;
        }

        DynamicFormBuilder.checkConcurrentDraftOverwriteThenSave("TO_BE_APPROVED", $form, $btn, initAjaxForm);
      });

      $('.delete-btn', $form).click(function(){
        if($(this).hasClass('disabled')){
          return false;
        }
        if(!onDelete($widget)){
          return false;
        }
        initAjaxForm($form, 'delete');
        $(this).closest('form').find('input[name="status"]').val("").end().submit();
      });
      
      $('.show-history-btn', $form).click(function(){
        showHistory();
      });
      
      $('.load-btn', $form).click(function(){
        loadDraft();
      });
      
      $('.approve-btn', $form).click(function(){
        if($(this).hasClass('disabled')){
          return false;
        }
        initAjaxForm($form, 'review');
        $(this).closest('form').find('input[name="status"]').val("APPROVED").end().submit();
      });
      
      $('.decline-btn', $form).click(function(){
        if($(this).hasClass('disabled')){
          return false;
        }
        initAjaxForm($form, 'review');
        $(this).closest('form').find('input[name="status"]').val("DECLINED").end().submit();
      });

      $(".input-multidrop", $form).each(function(){
        if($(this).children("option").length > 20){
          $button = $("<input type='button' class='btn btn-default' value='Choose' id='"+ $(this).attr("name") +"'/>")
          $button.click(function(){
            var selector = $(this).hide().prev(".input-multidrop");
            initMultiSelector(selector);
          });
          $(this).hide().after($button)
        }else{
          initMultiSelector($(this));
        }
      });
      
      $('.input-datetime', $form)
        .filter('.short-input')
        .css({
          'width':'115px',
          'display':'inline-block'
        }).end().datetimepicker({
        format: 'YYYY-MM-DD HH:mm', 
        useCurrent: false
      });
      
      //enable tab key in textarea, refer to http://pallieter.org/Projects/insertTab/
      $("textarea[id^='note-ATTRNOTES']", $form).keydown(
        function(e){
          var o = this;
          var kC = e.keyCode ? e.keyCode : e.charCode ? e.charCode : e.which;
          if (kC == 9 && !e.shiftKey && !e.ctrlKey && !e.altKey)
          {
              var oS = o.scrollTop;
              if (o.setSelectionRange)
              {
                  var sS = o.selectionStart;
                  var sE = o.selectionEnd;
                  o.value = o.value.substring(0, sS) + "\t" + o.value.substr(sE);
                  o.setSelectionRange(sS + 1, sS + 1);
                  o.focus();
              }
              else if (o.createTextRange)
              {
                  document.selection.createRange().text = "\t";
                  e.returnValue = false;
              }
              o.scrollTop = oS;
              if (e.preventDefault)
              {
                  e.preventDefault();
              }
              return false;
          }
          return true;
        }
      );
      
      $('.lookup-btn', $form).click(function(){
        var url = '/admin/jsp/lookupOptions.jsp?module='+request.module+'&field='+ $(this).prev().attr('name') +'&tableType='+request.tableType + '&widget=' + $widget.attr('id');
        ModalHelper.launchModal(url);
      });
      
      $(".nav-tabs", $widget).on("click", "a", function() {
        var $li = $(this).closest("li");
        DynamicFormBuilder.setActiveTab(request.module, request.tableType, $li.index())
      });
      
      var activeTabIndex = DynamicFormBuilder.getActiveTab(request.module, request.tableType);
      if (activeTabIndex && activeTabIndex > -1 && request.action != "insert") {
        var $items =  $(".nav-tabs > li", $widget);
        $items.removeClass("active");
        var $li = $items.eq(activeTabIndex);
        var tabPaneId = $li.find("a").attr("href");
        $li.addClass("active");
        var $tabPanes = $items.closest("div").find(".tab-pane");
        $tabPanes.removeClass("in active");
        $tabPanes.filter(tabPaneId).addClass("in active");
      }
      
      // Author of draft is accessing asset
      var internalName = $('input[name="currentUserIsDraftAuthor"]', $form).val();
      if (internalName === "true") {
    	  $.SmartMessageBox({
    	      title : "A draft of this asset exists.",
    	      content : "Would you like to view the live asset or your draft? ",
    	      buttons : '[Draft Asset][Live Asset]'
    	    }, function(ButtonPressed) {
    	      if (ButtonPressed === "Draft Asset") {
    			loadDraft();
    	      }
    	   });
      }
      
    },
    val: function($widget, input, value){
      $('[name="'+input+'"]', $widget).val(value);
    },
    get: function(index){
      return $($('.jarviswidget')[index]);
    },
    indexOf: function($widget) {
      return $('.jarviswidget').index($widget);
    },
    setActiveTab : function(module, tableType, tabNumber)
    {
      var activeTabs = JSON.parse(localStorage.getItem("activeTabs")) || {};
      activeTabs[module + "_" + tableType] = tabNumber;
      localStorage.setItem("activeTabs", JSON.stringify(activeTabs));
    },
    getActiveTab : function(module, tableType)
    {
      var activeTabs = JSON.parse(localStorage.getItem("activeTabs")) || {};
      return activeTabs[module + "_" + tableType];
    },
    
    checkConcurrentDraftOverwriteThenSave : function(draftState, $form, $btn, initAjaxForm)
    {
        var draftEditDate = $('input[name="draftEditDate"]', $form).val();
        var ID = $('input[name="ID"]', $form).val();
        var DID = $('input[name="DID"]', $form).val();
        var module = $('input[name="module"]', $form).val();

        $.ajax({
            url: '/wd-api/form/getDraftSaveDate',
            data: {
            	draftEditDate: draftEditDate,
            	DataID: ID,
            	DraftID: DID,
            	module: module
            }
          }).done(function( data, textStatus, jqXHR ) {
        	  if (data) {
              	$.SmartMessageBox({
            	      title : "You are about to overwrite a draft belonging to someone else.",
            	      content : "Do you want to continue?",
            	      buttons : '[No][Yes]'
            	    }, function(ButtonPressed) {
            	      if (ButtonPressed === "No") {
            	    	return false;
            	      } else if (ButtonPressed === "Yes") {
            	    	$('input[name="sendOldDraftInEmail"]', $form).val("true");
            	    	initAjaxForm($form, 'save');
            	    	$btn.closest('form').find('input[name="status"]').val(draftState).end().submit();
            	      }
            	   });
              	
              } else {
              	initAjaxForm($form, 'save');
              	$btn.closest('form').find('input[name="status"]').val(draftState).end().submit();
              }
          });
    	
    }
  }
