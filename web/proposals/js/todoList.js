var TodoList = {
  initDropdown: function(){

    // ACTIVITY
    // ajax drop
    $('#activity').click(function(e) {
        var $this = $(this);

        if (!$this.next('.ajax-dropdown').is(':visible')) {
            $this.next('.ajax-dropdown').show();
            $this.addClass('active');
        } else {
            $this.next('.ajax-dropdown').hide();
            $this.removeClass('active');
        }

        var theUrlVal = $this.next('.ajax-dropdown').find('.btn-group > .active > input').attr('id');
        
        //clear memory reference
        $this = null;
        theUrlVal = null;
                
        e.preventDefault();
    });

    $('input[name="activity"]').change(function() {
        var $this = $(this);

        url = $this.attr('id');
        container = $('.ajax-notifications');
        
        loadURL(url, container);
        
        //clear memory reference
        $this = null;       
    });

    // close dropdown if mouse is not inside the area of .ajax-dropdown
    $(document).mouseup(function(e) {
        if (!$('.ajax-dropdown').is(e.target) && $('.ajax-dropdown').has(e.target).length === 0) {
            $('.ajax-dropdown').fadeOut(150);
            $('.ajax-dropdown').prev().removeClass("active");
        }
    });
    
    /*
     * AJAX BUTTON LOADING TEXT
     * Usage: <button type="button" data-loading-text="Loading..." class="btn btn-xs btn-default ajax-refresh"> .. </button>
     */
    $('button[data-loading-text]').on('click', function() {
        TodoList.loadData();
    });

    // NOTIFICATION IS PRESENT
    // Change color of lable once notification button is clicked

    $this = $('#activity > .badge');

    if (parseInt($this.text()) > 0) {
        $this.addClass("bg-color-red bounceIn animated");
        
        //clear memory reference
        $this = null;
    }

    TodoList.loadData();
  },
  
  createRow: function(workflowItem, data){
    var isNew = (parseInt(workflowItem.dataId) == 0);
    var isDelete = (workflowItem.status == "TO_BE_DELETED");
    var isWorkflow = (isNew || isDelete);
    
    var typeForDisplay = workflowItem.type=="Elements"?"Element":"Category";
    var actionForDisplay = (isDelete?"Delete":(isNew?"New":"Edit"));
    var idForDisplay = isNew?"":", ID = "+workflowItem.draftId;
    
    var $handle = $("<span>").attr("class", "handle");
    var $p = $("<p>").append($("<strong>").text(data.moduleToName[workflowItem.module]), ": ", data.itemToHeadline[workflowItem.sysId])
                    .append($("<span>").attr("class", "text-muted").text(actionForDisplay+" "+typeForDisplay+" "+idForDisplay))
                    .append($("<span>").attr("class", "text-muted").text("Author: "+data.userToName[workflowItem.who]))
                    .append($("<span>").attr("class", "date").text(moment(workflowItem.when).fromNow()));
    return $("<li>").append($handle, $p).attr("data-workflow", isWorkflow?"true":"false");
  },
  
  openPanel: function(workflowItem){
    if (workflowItem.type == "Elements")
    {
      WidgetHelper.addWidgetForModuleElementId({
        draft: true,
        DID: workflowItem.draftId
      }, workflowItem.module, workflowItem.dataId);
    }
    else
    {
      WidgetHelper.addWidgetForModuleCategoryId({
        draft: true,
        DID: workflowItem.draftId
      }, workflowItem.module, workflowItem.dataId);
    }
  },
  
  addPending: function(workflowItem, data){
    var $row = TodoList.createRow(workflowItem, data);
    var $actionButton = $("<button>")
                            .attr("type", "button")
                            .attr("class", "btn bg-color-magenta txt-color-white btn-xs")
                            .attr("title", "Review Changes")
                            .attr("data-toggle", "tooltip")
                            .attr("data-placement", "right")
                            .append($("<i>").attr("class", "fa fa-dot-circle-o"));
    $(".handle",$row).append($actionButton);
    $(".ajax-dropdown ul#pending").append($row);
    $actionButton.click(function(){
      TodoList.openPanel(workflowItem);
    });
  },
  
  addDraft: function(workflowItem, data){
    var $row = TodoList.createRow(workflowItem, data);
    var $actionButton = $("<button>")
                            .attr("type", "button")
                            .attr("class", "btn bg-color-magenta txt-color-white btn-xs")
                            .attr("title", "Edit")
                            .attr("data-toggle", "tooltip")
                            .attr("data-placement", "right")
                            .append($("<i>").attr("class", "fa fa-circle-o"));
    $(".handle",$row).append($actionButton);
    $(".ajax-dropdown ul#draft").append($row);
    $actionButton.click(function(){
      TodoList.openPanel(workflowItem);
    });
  },
  
  loadData: function(){
    var currentActivityBadge = $("#activity .badge").text();
    currentActivityBadge = isNaN(parseInt(currentActivityBadge)) ? 0 : parseInt(currentActivityBadge);

    //reload button
    var reloadDataBtn = $(".ajax-dropdown button[data-loading-text]");
    reloadDataBtn.button('loading');
    
    $("#activity .badge").empty().append($("<i>").attr("class", "fa fa-refresh fa-spin"));

    $.ajax({
      url : '/wd-api/todoList',
      type : 'GET'
    }).done(function( data, textStatus, jqXHR ) {
      $(".ajax-dropdown ul#pending").empty();
      $(".ajax-dropdown ul#draft").empty();
      $.each(data.workflowItems, function(i,e){
        if (e.status == "TO_BE_APPROVED" || e.status == "TO_BE_DELETED")
        {
          TodoList.addPending(e, data);
        }
        else
        {
          TodoList.addDraft(e, data);
        }
      });
      
      //Update badge
      var pending = $(".ajax-dropdown ul#pending").children().length;
      var draft = $(".ajax-dropdown ul#draft").children().length;
      var total = pending + draft;
      $(".ajax-dropdown .pendingCount").text(pending);
      $(".ajax-dropdown .draftCount").text(draft);
      
      var animateChange = setInterval(function(){
        $("#activity .badge").text(currentActivityBadge);

        if (currentActivityBadge > total) --currentActivityBadge;
        else if (currentActivityBadge < total) ++currentActivityBadge;
        else {
          clearInterval(animateChange);

          $("#activity .badge").addClass('rubberBand animated');
          $("#activity .badge").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $(this).removeClass('rubberBand animated');
          });
        };
      }, Math.floor(750/Math.abs(currentActivityBadge-total)));
      
      if (pending > 0)
      {
        $("#activity .badge").addClass("bg-color-red");
      }
      else
      {
        $("#activity .badge").removeClass("bg-color-red");
      }
      
      $('.ajax-dropdown [data-toggle="tooltip"]').tooltip();
      $(".ajax-dropdown #statusText").text( moment().format('LLL') );
      reloadDataBtn.button('reset');
    });
  }
};