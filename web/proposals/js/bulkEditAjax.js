const BULK_EDITOR_LAZY_LOAD_THRESHOLD = 20;

var BulkEditor = {
    init: function($widget, type, module, categoryParentID){
      var type = JSON.parse($widget.data("requestData")).widgetType;
      var module = $widget.data("module");
      var insertNewItem = $(".insertNewItem", $widget);

      $.ajax({
          url: '/wd-api/bulkEdit/columns',
          data: {
              module: module,
              type: type,
              id : categoryParentID,
          }
      }).done(function( data, textStatus, jqXHR ) {
          $(".panelLoadingIndicator", $widget).remove();
          $(".bulkEditor", $widget).data('tableMetadata', data);
          $(".bulkEditor", $widget).data('level', data.level);
          
          if (data.insert)
          {
            insertNewItem.show().click(function(){
              var wasLocked = WidgetHelper.widgetIsLocked($widget);
              WidgetHelper.lockWidget($widget);
              if (type == 'elementList')
              {
                WidgetHelper.addWidgetForInsertElement({}, module, categoryParentID);
              }
              else
              {
                WidgetHelper.addWidgetForInsertCategory({}, module, categoryParentID);
              }
              if (!wasLocked)
              {
                WidgetHelper.unlockWidget($widget);
              }
            });
          }
          if (data.level != 0 && categoryParentID) {
            $(".backToParentCategory", $widget).click(function() {
              WidgetHelper.addWidgetForBulkEditCategories({}, module, categoryParentID);
            });
          } else {
            $(".backToParentCategory", $widget).hide();
          }
          
          BulkEditor.buildTable($widget);
      });
    },
    
    buildTable: function($widget){
      var tableMetadata = $(".bulkEditor", $widget).data('tableMetadata');
      BulkEditor.configureTable($widget, tableMetadata.columns);
      BulkEditor.configureDropdown($widget, tableMetadata.columns);
    },
    
    configureTable: function($widget, allColumns){
      var domHead = "<'navbar' <'navbar-form navbar-left' <'form-group' l>> <'navbar-form navbar-right'<'form-group' f><'form-group' <'#columnPicker'>>> >";
      var domBody = "t";
      var domFooter = "<'navbar' <'navbar-form navbar-left'<'form-group' p>> <'navbar-form navbar-right' i> >";

      var requestData = JSON.parse($widget.data("requestData"));
      var type = requestData.widgetType;
      var module = $widget.data("module");
      var dataId = $widget.data("id");
      
      var tableConfiguration = BulkEditor.restoreTableConfigurationSnapshot($widget);

      var columnInformation = [];
      var columnRenderers = [];

      $.each(tableConfiguration.visibleColumns, function(i,e){
        var columnMetadata = BulkEditor.columnInformationForType($widget, e.name);
        if (columnMetadata != undefined)
        {
          columnInformation.push(columnMetadata);
          columnRenderers.push(BulkEditorDataDisplay.rendererForType($widget, columnMetadata));
          
        }
      });
      $(".bulkEditor", $widget).data('columnInformation', columnInformation);

      var table = $(".bulkEditorTbl", $widget).DataTable( {
          "dom": "ZT<'tableWrapper' "+domHead+domBody+domFooter+" >",
          tableTools: {
            "sRowSelect": "os",
            "aButtons": []
          },
          "processing": true,
          "serverSide": true,
          "ajax": {
            "url": "/wd-api/bulkEdit/data",
            "data": function(d){
              d.type = type;
              d.dataId = dataId;
              d.module = module;
            }
          },
          "search": {
            "search": tableConfiguration.search
          },
          "pageLength": tableConfiguration.pageLength,
          "order": tableConfiguration.order,
          "lengthMenu": [
                          [5, 10, 50, 100],
                          [5, 10, 50, 100] // change per page values here
                      ],
          "columns": columnRenderers,
          "createdRow": function(row, data, index){
            $(row).data('originalData', JSON.stringify(data));
          },
          "drawCallback": function( settings ) {
            BulkEditor.updateSummary($widget);
          },
          "colResize": {
            "resizeCallback": function(column) {
              BulkEditor.snapshotState($widget);
            },
            "exclude": [0]//cant resize first column.
        },
      });
      $(".bulkEditor", $widget).data('table', table);
      
      $(".dataTables_length", $widget).closest(".navbar-form").addClass("navbar-length-form")
      
      new $.fn.dataTable.ColReorder( table , {
        "reorderCallback": function(){
          BulkEditor.snapshotState($widget);
          table.draw();
        },
        "fixedColumns": 1
      });
      
      table.on( 'length', function () {
        BulkEditor.snapshotState($widget);
      });
      table.on( 'page', function () {
        BulkEditor.snapshotState($widget);
      });
      table.on( 'search', function () {
        BulkEditor.snapshotState($widget);
      });
      table.on( 'draw.dt', function () {
        BulkEditor.updateWidgetID($widget);
      });

      $(".clearDatatable", $widget).click(function(e){;
      	BulkEditor.reloadBulkEditor($widget);
      });
      $(".saveDatatable", $widget).click(function(e){;
        BulkEditor.submitChanges($widget);
      });
      $(".cutSelectedItems", $widget).click(function(e){;
      	BulkEditor.cutOrCopySelectedItems($widget, "cut");
      });
      $(".copySelectedItems", $widget).click(function(e){;
    	BulkEditor.cutOrCopySelectedItems($widget, "copy");
      });
      $("tbody").on('click', 'tr', function(){
    	  if(BulkEditor.getSelectedRows($widget).length > 0) {
    	    	$widget.find(".cutSelectedItems").show();
    	    	$widget.find(".copySelectedItems").show();
    	    } else {
    	    	$widget.find(".cutSelectedItems").hide();
    	    	$widget.find(".copySelectedItems").hide();
    	  }
      });
    },
    
    cutOrCopySelectedItems: function($widget, method){
    	var requestData = JSON.parse($widget.data("requestData"));
    	var widgetType = requestData.widgetType;
    	var module = requestData.tablePrefix;
    	var id = requestData.ID;
    	var expandTo = "";
    	
    	if(requestData.widgetType == "elementList") {
    		expandTo = "elements";
    	} else {
    		expandTo = "category";
    	}
    	
    	ModuleHelper.fetchAssignedModules().done(function(assignedModules) {
    		for(var i=0; i < assignedModules.length; i++) {
    			if(module == assignedModules[i].module_name) {
    				if(!ModuleHelper.moduleIsOpen(module)) { 
    					ModuleHelper.addModule(assignedModules[i]);
    					StorageHelper.restoreActiveModuleTree(assignedModules[i])
    				}
    				ModuleHelper.expandModuleTo($widget, assignedModules[i], id, expandTo, function($widget, assignedModule, id, type) {
    					var selectedItems = BulkEditor.getSelectedRows($widget);    
    					if(type == "elements") {
    						ModuleHelper.selectElementsForAction(assignedModule, method, selectedItems, id);
    					}
    					if(type == "category") {
    						ModuleHelper.selectCategoriesForAction(assignedModule, method, selectedItems, id);
    					}
    				});
    			}
    		}
    	});
    },
    
    updateWidgetID: function($widget){
      var requestData = JSON.parse($widget.data("requestData"));
      var table = $(".bulkEditor", $widget).data('table');
      
      var widgetType = requestData.widgetType;
      var widgetID = undefined;
      if (widgetType == "elementList")
      {
        widgetID = WidgetHelper.widgetIdForElementList(requestData.tablePrefix, requestData.ID, table.search());
      }
      else
      {
        widgetID = WidgetHelper.widgetIdForCategoryList(requestData.tablePrefix, requestData.ID, table.search());
      }
      $widget.attr("id", widgetID);
    },
    
    configureDropdown: function($widget, allColumns){
      var $columnPicker = $("#columnPicker", $widget);
      
      var columnInformation = $(".bulkEditor", $widget).data('columnInformation');
      var currentColumnNames = {};
      $.each(columnInformation, function(i,e){
        currentColumnNames[e.internal] = true;
      });
      
      //Add a dropdown button
      $columnPicker
        .addClass("btn-group")
        .addClass("hidden-xs")
        .append($("<button>")
                    .attr("class", "btn btn-default dropdown-toggle")
                    .attr("data-toggle", "dropdown")
                    .append('Manage Columns ')
                    .append($("<i>").attr('class','fa fa-caret-down')))
        .append($("<ul>").attr("class", "dropdown-menu pull-right columnList").attr('role', "menu"))
      
      var $columnList = $(".columnList", $columnPicker);
      var currentTab = "";
      $.each(allColumns, function(i,e){
        if (i<1) return true;//skip
        
        if (currentTab != e.tab)
        {
          var $item = $("<li>")
              .attr("role", "presentation")
              .attr("class", "dropdown-header")
              .text(e.tab);
          $columnList.append($item);
          currentTab = e.tab;
        }
        var $icon = DataType.getTypeIcon(e.internal);
        if (DataType.isTypeEditable(e.internal))
        {
          $icon = $("<span>").attr("class", "label label-success").append($icon);
        }
        else
        {
          $icon = $("<span>").attr("class", "label label-danger").append($icon);
        }
        var label = " "+e.external;
        var selectedIndicator = $("<i>").attr("id", "selectedIndicator");
        if (e.internal in currentColumnNames)
        {
          selectedIndicator.attr("class", "fa fa-check-square-o");
        }
        else
        {
          selectedIndicator.attr("class", "fa fa-square-o")
        }
        if (e.internal == "attr_headline"
          || e.internal == "attr_categoryname")
        {
          selectedIndicator.addClass("fallbackOption");
        }
        var $item = $("<li>").append($("<a>").attr("class", "columnValue")
                                            .attr("title", e.external)
                                            .attr("href", "#"+e.internal)
                                            .append(selectedIndicator)
                                            .append($icon)
                                            .append(label));
        $columnList.append($item);
      });
      
      $(".dropdown-toggle", $columnPicker).click(function(e){
        $("#selectedIndicator", $columnPicker).first().focus();
      });
      $(".columnValue", $columnList).click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var clicked = $(e.currentTarget);
        var internalName = clicked.attr('href').substring(1);
        
        var selectedIndicator = $("#selectedIndicator", clicked);
        selectedIndicator.toggleClass("fa-square-o fa-check-square-o changeToggle");
        if ($(".fa-check-square-o", $columnList).size()==0)
        {
          $(".fallbackOption", $columnList).toggleClass("fa-square-o fa-check-square-o changeToggle");
        }
      });
      
      $columnPicker.on('show.bs.dropdown', function () {
        var toggleBtn = $("#columnPicker .dropdown-toggle", $widget);
        var tableNode = $(".bulkEditorTbl ", $widget);
        
        var btnBottom = toggleBtn.offset().top + toggleBtn.height();
        var tableBottom = tableNode.offset().top + tableNode.height();
        
        $(".bulkEditor .columnList", $widget).css('max-height', tableBottom-btnBottom);
      });
      
      $columnPicker.on('hidden.bs.dropdown', function () {
        if ($(".changeToggle", $columnPicker).size()>0)
        {
          var sortedColumnInformation = BulkEditor.sortedColumnInformation($widget);
          
          //find out what columns were selected and their sequence.
          var selectedColumnNames = [sortedColumnInformation[0].internal];
          var selectedColumnNameSet = {};
          selectedColumnNameSet[selectedColumnNames[0]] = true;
          $(".fa-check-square-o").closest("a").each(function(i,e){
            var selectedInternalName = $(e).attr("href").substring(1);
            selectedColumnNames.push(selectedInternalName);
            selectedColumnNameSet[selectedInternalName] = true;
          });
          
          //find out what columns are currently displayed.
          // - If the column is still selected, store its column information.
          var updatedColumnInformation = [];
          var updatedColumnInformationNameSet = [];
          $.each(sortedColumnInformation, function(i,columnMetadata){//sortedColumnMetadata
            if (columnMetadata.internal in selectedColumnNameSet)
            {
              updatedColumnInformationNameSet[columnMetadata.internal] = true;
              updatedColumnInformation.push(columnMetadata);
            }
          });
          
          //Find which columns were newly seleted. Add these to the right of the current columns.
          $.each(selectedColumnNames, function(i, selectedName){
            if (selectedName in updatedColumnInformationNameSet == false)
            {
              updatedColumnInformationNameSet[selectedName] = true;
              updatedColumnInformation.push(BulkEditor.columnInformationForType($widget, selectedName));
            }
          });
          
          //DONE!
          $(".bulkEditor", $widget).data('columnInformation', updatedColumnInformation);
          BulkEditor.snapshotNow($widget, true);

          //clear out the table...
          var table = $(".bulkEditor", $widget).data('table');
          var tableNode = table.table().node();
          table.destroy();//BOOM!
          $("tbody, thead, tfoot", tableNode).html('');//remove data

          BulkEditor.buildTable($widget);
        }
      });
    },
    
    columnInformationForType: function($widget, columnName){
      var colInfo = undefined;
      var tableMetadata = $(".bulkEditor", $widget).data('tableMetadata');
      $.each(tableMetadata.columns, function(j,e){
        if (columnName == e.internal)
        {
          colInfo = e;//found
          //early exit
          return false;
        }
      });

      return colInfo;
    },
    
    changeSummary: function($widget){
      var table = $(".bulkEditor", $widget).data('table');
      var columnInformation = $(".bulkEditor", $widget).data('columnInformation');
      var columnOrder = table.colReorder.order();

      var type = JSON.parse($widget.data("requestData")).widgetType;
      
      var dataType = "categories";
      var indexField = "category_id";
      if (type.startsWith("element"))
      {
        dataType = "elements";
        indexField = "element_id";
      }
      var module = $widget.data("module");

      var tableData = table.data();

      //Start by finding what IDs have been deleted
      var deleteIDs = {};
      $.each(table.rows(".deleteFlagged").data(), function(i,data){
        deleteIDs[data[indexField]] = true;
      });

      //Now find column data for modified rows
      var changedCol = {}; 
      var changedRows = {};
      var deltaData = [];

      //collect rows first
      var modifiedCells = 0;
      var changes = table.cells(".cellChanged").indexes();
      $.each(changes, function(i,cellDef){
        var dataID = tableData[cellDef.row][indexField];
        if(dataID in deleteIDs == false)
        {
          ++modifiedCells;
          changedCol[columnInformation[columnOrder[cellDef.column]].internal] = true;
          changedRows[cellDef.row] = tableData[cellDef.row];
        }
      });

      //now remove columns that havent changed
      $.each(changedRows, function(i, dataItem){
        var dataID = dataItem[indexField];
        if(dataID in deleteIDs == false)
        {
          var deltaRow = [dataID];
          $.each(changedCol, function(dataIndex, e){
            deltaRow.push(dataItem[dataIndex]);
          });
          deltaData.push(deltaRow);
        }
      });

      //Generate the internal column names for modified columns
      var columnNames = [indexField];
      $.each(changedCol, function(dataIndex, e){
        columnNames.push(dataIndex);
      });

      return {
        moduleName : module,
        dataType : dataType,
        deletedRows : Object.keys(deleteIDs),
        modifiedColumns : columnNames,
        modifiedRows : deltaData,
        modifiedCells : modifiedCells
      }
    },
    
    isDirty: function($widget){
    	var changes = BulkEditor.changeSummary($widget);
    	return changes.modifiedRows.length > 0 || changes.deletedRows.length > 0
    },
    
    getSelectedRows: function($widget){
    	var selectedItems = [];
    	var $table = $widget.find(".bulkEditor").data('table');
    	$table.rows(".DTTT_selected").data().each(function(data){ 
    		selectedItems.push(data);
    	});
    	return selectedItems;
    },
    
    valueChanged: function($widget, td, rowData, row, col, field){
      var $tr = $(td).closest("tr");
      var originalData = $tr.data('originalData');
      var tableTools = TableTools.fnGetInstance($(".bulkEditorTbl",$widget).attr("id"));
      if (tableTools.fnGetSelected().length > 0)
      {
        //Make sure the row that is being edited is selected.
        tableTools.fnSelect($tr[0]);
      }

      var currentData = JSON.stringify(rowData);
      if (originalData == currentData)
      {
        $tr.removeClass("rowChanged");
      }
      else
      {
        $tr.addClass("rowChanged");
      }
      
      var original = JSON.parse(originalData)[field];
      var current = rowData[field];
      if (current instanceof Array)//attr_multi
      {
        var valueUnchanged = $(original).not(current).length === 0
                    && $(current).not(original).length === 0;
        if (valueUnchanged)
        {
          $(td).removeClass("cellChanged");
        }
        else
        {
          $(td).addClass("cellChanged");
        }
      }
      else
      {
        if (original == current)
        {
          $(td).removeClass("cellChanged");
        }
        else
        {
          $(td).addClass("cellChanged");
        }
      }
      
      BulkEditor.updateSummary($widget);
    },
    
    updateSummary: function($widget){
      if ($widget == undefined) $widget = $(".bulkEditor").closest(".jarviswidget");
      
      var singleUpdate = $(".bulkEditor", $widget).data('singleUpdate');
      if (singleUpdate != undefined)
      {
        clearTimeout(singleUpdate);
      }
      singleUpdate = setTimeout(function(){
        BulkEditor.updateSummaryDelay($widget);
        singleUpdate = undefined
        $(".bulkEditor", $widget).removeData('singleUpdate');
      }, 500);
      $(".bulkEditor", $widget).data('singleUpdate', singleUpdate);
    },
    
    updateSummaryDelay: function($widget){
      
      var table = $(".bulkEditor", $widget).data('table');
      var columnInformation = $(".bulkEditor", $widget).data('columnInformation');
      var changes = BulkEditor.changeSummary($widget);

      var deletedRows = changes.deletedRows.length;
      var modifiedRows = changes.modifiedRows.length;
      var modifiedColumns = changes.modifiedColumns.length - 1;
      
      var summary = [];
      if (deletedRows > 0 || modifiedRows > 0)
      {
        if (deletedRows > 0)
        {
          var rows = deletedRows == 1 ? "row" : "rows";
          summary.push(deletedRows+" "+rows+" marked for delete");
        }
        if (modifiedRows > 0)
        {
          var cols = modifiedColumns == 1 ? "column" : "columns";
          var rows = modifiedRows == 1 ? "row" : "rows";
          var fields = changes.modifiedCells == 1 ? "cell" : "cells";
          if (changes.modifiedCells == 1)
          {
            summary.push(changes.modifiedCells+" "+fields+" modified");
          }
          else
          {
            var rowCol = [];
            if (modifiedRows > 1)
            {
              rowCol.push(modifiedRows+" "+rows);
            }
            if (modifiedColumns > 1)
            {
              var c = $("<div>").append($("<span>")
                                          .attr("title", changes.modifiedColumns.join(", "))
                                          .attr("data-toggle", "tooltip")
                                          .attr("data-placement", "top")
                                          .append(modifiedColumns+" "+cols)).html();
              rowCol.push(c);
            }
            summary.push(changes.modifiedCells+" "+fields+" modified in "+rowCol.join(" and ")+".");
          }
        }
      }
      
      var changeSummary = $(".changeSummary", $widget)
        .html(summary.join(" and "));
      $('[data-toggle="tooltip"]', changeSummary).tooltip();
      if (summary.length == 0)
      {
        //Not editing
        $(".editAction", $widget).prop('disabled', true);
        $(".button-area button.clearDatatable", $widget).text('Reload');
        $(".dataTables_paginate .paginate_button", $widget).removeClass('disabled');
        $("#columnPicker button.dropdown-toggle", $widget).removeClass('disabled');
        $(".dataTables_length select", $widget).prop('disabled', false);
        $(".dataTables_filter input", $widget).prop('disabled', false);
        $(table.table().node()).removeClass('disable-reorder');
      }
      else
      {
        //Editing
        $(".editAction", $widget).prop('disabled', false); //action buttons in dialog
        $(".button-area button.clearDatatable", $widget).text('Discard Changes');
        $(".dataTables_paginate .paginate_button", $widget).addClass('disabled'); //Page picker
        $("#columnPicker button.dropdown-toggle", $widget).addClass('disabled'); //Column picker
        $(".dataTables_length select", $widget).prop('disabled', true); //Page size
        $(".dataTables_filter input", $widget).prop('disabled', true); //Search box
        $(table.table().node()).addClass('disable-reorder'); //Column reordering
      }
    },
    
    bindButtonInSelection: function($widget, button, col, eClass){
    	var $table = $(".bulkEditor", $widget).data('table');
        var cellsDT = $table.cells(".DTTT_selected", col).nodes().to$();
        var action = $(button).attr("data-action");
        var flag = $(button).closest('tr').hasClass(eClass);
        
        cellsDT.find(">button")
        .not(".recursiveBind")
        .addClass("recursiveBind")
        .each(function(e){
        	if($(this).attr("data-action") == action && $(this).closest('tr').hasClass(eClass) == flag) {
        		$(this).trigger("wd.button.clicked");
        	}
        })
        .removeClass("recursiveBind")
    },
    
    bindTextValueInSelection: function($widget, inputField, col){
      var $table = $(".bulkEditor", $widget).data('table');
      var cellsDT = $table.cells(".DTTT_selected", col).nodes().to$();
      
      cellsDT.find(">input")
              .not(inputField)
              .not(".recursiveBind")
              .addClass("recursiveBind")
              .val($(inputField).val())
              .change()
              .removeClass("recursiveBind")
    },
    
    bindCheckboxValueInSelection: function($widget, checkbox, col){
      var $table = $(".bulkEditor", $widget).data('table');
      var cellsDT = $table.cells(".DTTT_selected", col).nodes().to$();
      
      var setChecked = $(checkbox).is(":checked");
      cellsDT.find("input")
              .not(checkbox)
              .not(".recursiveBind")
              .addClass("recursiveBind")
              .each(function(i,ch){
                if (setChecked)
                  $(ch).attr("checked", "checked");
                else
                  $(ch).removeAttr("checked");
              })
              .change()
              .removeClass("recursiveBind")
    },
    
    bindDateValueInSelection: function($widget, inputField, col){
      var $table = $(".bulkEditor", $widget).data('table');
      var cellsDT = $table.cells(".DTTT_selected", col).nodes().to$();
      
      var updateTo = $(inputField).data("DateTimePicker").date();
      
      cellsDT.find(">input")
              .not(inputField)
              .not(".recursiveBind")
              .addClass("recursiveBind")
              .each(function(i,f){
                $(f).data("DateTimePicker").date(updateTo);
              })
              .removeClass("recursiveBind");
    },
    
    bindSelectValueInSelection: function($widget, selectField, col){
      var $table = $(".bulkEditor", $widget).data('table');
      var cellsDT = $table.cells(".DTTT_selected", col).nodes().to$();
      
      cellsDT.find(".lazyLoadAction").click();
      
      // SUSHANT: This is a enhanced version of recursive bind.... It doesnt retrigger the change on 
      // the first |item. I think its worthy of being applied to the rest once we know it works fine.
      if (!$(selectField).hasClass("recursiveBind"))
      {
        $(selectField).addClass("recursiveBind").trigger('wd.select.changed');
        cellsDT.find(">select")
        .not(".recursiveBind")
        .addClass("recursiveBind")
        .val($(selectField).val())
        .trigger('wd.select.changed')
        .trigger('wd.miltiselect.regroup')
        .removeClass("recursiveBind");
        $(selectField).removeClass("recursiveBind")
      }
    },
    
    restoreTableConfigurationSnapshot: function($widget)
    {
      var requestData = JSON.parse($widget.data("requestData"));
      var type = requestData.widgetType;
      var module = $widget.data("module");
      var level = $(".bulkEditor", $widget).data('level');
      
      var visibleColumns = [];
      if (type == "elementList")
      {
        visibleColumns.push({
          "name":"element_id"
        });
        visibleColumns.push({
          "name":"attr_headline"
        });
      }
      else
      {
        visibleColumns.push({
          "name":"category_id"
        });
        visibleColumns.push({
          "name":"attr_categoryname"
        });
      }
      var config = {
        "pageLength": 10,
        "order": [1, 'asc'],
        "visibleColumns" : visibleColumns,
        "search" : requestData.search
      }
      
      //Override with localstorage
      var stored = localStorage.getItem("bulkEdit-"+module+"-"+type+"-"+level);
      if (stored != undefined)
      {
        var restored = JSON.parse(stored);
        if (restored.search == "") delete restored.search;
        $.extend(config, restored);
      }

      return config;
    },
    
    /**
     * This function doesnt work if the internal columnInformation has been changed.
     * @param $widget
     * @returns {Array}
     */
    sortedColumnInformation: function($widget)
    {
      var table = $(".bulkEditor", $widget).data('table');
      var columnInformation = $(".bulkEditor", $widget).data('columnInformation');

      var res = [];
      $.each(table.colReorder.order(), function(i, columnIndex){
        res.push(columnInformation[columnIndex]);
      });
      return res;
    },
    
    snapshotNow: function($widget, addingColumn)
    {
      var type = JSON.parse($widget.data("requestData")).widgetType;
      var module = $widget.data("module");
      var level = $(".bulkEditor", $widget).data('level');
      
      var table = $(".bulkEditor", $widget).data('table');
      var columnInformation = $(".bulkEditor", $widget).data('columnInformation');

      var visibleColumns = [];
      if (addingColumn)
      {
        $.each(columnInformation, function(i,col){
          visibleColumns.push({
            "name": col.internal
          });
        });
      }
      else
      {
        var tableWidth = $(table.table().node()).width();
        $.each(table.colReorder.order(), function(i, columnIndex){
          var cellWidth = $(table.column(i).header()).width();
          var info = {
              "name": columnInformation[columnIndex].internal
            };
          if (i > 0)
          {
            info.ratio = cellWidth / tableWidth;
          }
          else
          {
            tableWidth -= cellWidth;//Exclude the first column from ratio calculation
          }
          visibleColumns.push(info);
        });
      }
      

      var snapshotState = {
          "pageLength" : table.page.len(),
          "order" : table.order(),
          "search" : table.search(),
          "visibleColumns" : visibleColumns
      }
//      console.log(JSON.stringify(snapshotState));
      localStorage.setItem("bulkEdit-"+module+"-"+type+"-"+level, JSON.stringify(snapshotState));
    },
    
    snapshotState: function($widget)
    {
      if (BulkEditor.widgetsToSave == undefined)
      {
        BulkEditor.widgetsToSave = {};
      }
      //save in 0.5 seconds... allow for multiple calls to this functions in the meantime.
      if (BulkEditor.delaySave != undefined)
      {
        clearTimeout(BulkEditor.delaySave);
      }
      
      BulkEditor.widgetsToSave[$widget.attr("id")] = $widget;
      BulkEditor.delaySave = setTimeout(function(){
        $.each(BulkEditor.widgetsToSave, function(id, $w){
          BulkEditor.snapshotNow($w, false);
        });
        BulkEditor.widgetsToSave = {};
      }, 500);
    },
    
    reloadBulkEditor: function($widget) {
    	var table = $(".bulkEditor", $widget).data('table');
    	
    	if(BulkEditor.isDirty($widget)) {
    		WidgetHelper.smartDialogBox($widget, {
                title : "<i class='fa fa-refresh txt-color-orangeDark'></i> You have unsaved changes <span class='txt-color-orangeDark'></span>",
                content : "You have unsaved changes. Would you like to discard your changes and reload?",
                buttons : '[No][Yes]'

            }, function(ButtonPressed) {
                if (ButtonPressed == "Yes") {
                	setTimeout(table.ajax.reload(undefined, false), 1000);
                }
            });
    	} else {
    		table.ajax.reload(undefined, false);
    	}
    },
    
    runJSValidation: function($widget){
      return true;
    },
    
    submitChanges: function($widget){
      var isValid = BulkEditor.runJSValidation();
      if (isValid == false)
        return;

      var table = $(".bulkEditor", $widget).data('table');

      var changes = BulkEditor.changeSummary($widget);
      WidgetHelper.blockUI($widget);
      var widgetWasLocked = WidgetHelper.widgetIsLocked($widget);
      WidgetHelper.lockWidget($widget);
      
      $.ajax({
          url : '/wd-api/bulkEdit/save',
          type : 'POST',
          contentType: "application/json;",
          data : JSON.stringify(changes)
        }).done(function( response, textStatus, jqXHR ) {
          widgetWasLocked ? WidgetHelper.lockWidget($widget) : WidgetHelper.unlockWidget($widget);

          if (response.success)
          {
            var summary = response.warnings ? response.message : $(".changeSummary", $widget).first().text();
            table.one( 'draw.dt', function () {
              WidgetHelper.unblockUI($widget);
            
              if(!response.warnings){
                $widget.data("autoclose", true);
              }
              showNotification({
                success: !response.warnings,
                container: $('.bulkEditor', $widget),
                content: summary
              }, function(){
                if(!response.warnings){
                  WidgetHelper.closeUnlockedWidget($widget);
                }
              });
            });
            table.ajax.reload(undefined, false);
            var rowData = table.data();
            $(".bulkEditor", $widget).data('rowData', rowData);
            BulkEditor.updateSummary($widget);
            
            var module = $widget.data("module");
            var categoryParentID = $widget.data("id");
            TreeHelper.reloadChildren(module, categoryParentID, false);
          }
          else
          {
            WidgetHelper.unblockUI($widget);

            $.SmartMessageBox({
              title : "<i class='fa fa-exclamation-triangle' style='color:yellow'></i> Unable to perform bulk update.",
              content : response.message,
              buttons : '[OK]' 
            });
          }
        });
    }
};

var BulkEditorDataDisplay = {
    rendererForType: function($widget, columnInfo){
      if (columnInfo.internal == "element_id"
        || columnInfo.internal == "category_id")
      {
        return BulkEditorDataDisplay.rendereForTypeID($widget, columnInfo);
      }
      else if (columnInfo.internal.startsWith('attr_'))
      {
        return BulkEditorDataDisplay.rendereForTypeText($widget, columnInfo);
      }
      else if (columnInfo.internal.startsWith('attrpass_'))
      {
        return BulkEditorDataDisplay.rendereForTypePassword($widget, columnInfo);
      }
      else if (columnInfo.internal == "live"
        || columnInfo.internal.startsWith('attrcheck_'))
      {
        return BulkEditorDataDisplay.rendereForTypeCheckbox($widget, columnInfo);
      }
      else if (columnInfo.internal == "live_date"
        || columnInfo.internal == "expire_date"
          || columnInfo.internal.startsWith('attrdate_'))
      {
        return BulkEditorDataDisplay.rendereForTypeDate($widget, columnInfo);
      }
      else if (columnInfo.internal.startsWith("attrtext_")
          || columnInfo.internal.startsWith("attrlong_")
          || columnInfo.internal.startsWith('attrfile_'))
      {
        return BulkEditorDataDisplay.rendereForTypeFile($widget, columnInfo);
      }
      else if(columnInfo.internal.startsWith("attrcurrency_")
          || columnInfo.internal.startsWith("attrfloat_"))
      {
        return BulkEditorDataDisplay.rendereForTypeFloat($widget, columnInfo);
      }
      else if(columnInfo.internal.startsWith("attrinteger_"))
      {
        return BulkEditorDataDisplay.rendereForTypeInteger($widget, columnInfo);
      }
      else if (columnInfo.internal.startsWith('attrdrop_'))
      {
        return BulkEditorDataDisplay.rendereForTypeDropdown($widget, columnInfo);
      }
      else if (columnInfo.internal.startsWith('attrmulti_'))
      {
        return BulkEditorDataDisplay.rendereForTypeDropdownMulti($widget, columnInfo);
      }
      else
      {
        return BulkEditorDataDisplay.rendereForTypeUnknown($widget, columnInfo);
      }
    },
    
    rendereForTypeID : function($widget, columnInfo){
      var widgetType = JSON.parse($widget.data("requestData")).widgetType;
      var widgetDataModule = $widget.data("module");
      var widgetDataId = $widget.data("id");

      return {
        title:'Actions',
        data:columnInfo.internal,
        name:columnInfo.external,
        width: '60px',
        orderable: true,
        visible: true,
        render: function(data, type, row){
          if($widget.parent().length == 0){
            return "";
          }
          if (type == "display")
          {
            var drillDown = $("<button>")
              .attr("type", "button")
              .attr("class", "btn bg-color-green txt-color-white btn-xs drillDown")
              .attr("title", "Open")
              .attr("data-toggle", "tooltip")
              .attr("data-placement", "bottom")
              .append($("<i>").attr("class", "fa fa-level-down"));
            var openEditor = $("<button>")
              .attr("type", "button")
              .attr("class", "btn btn-primary btn-xs openEditor")
              .attr("title", "ID:"+data)
              .attr("data-toggle", "tooltip")
              .attr("data-placement", "bottom")
              .append($("<i>").attr("class", "fa fa-edit"));
            //WORKFLOW...
            if (row.workflow == 'TO_BE_APPROVED' || row.workflow == 'TO_BE_DELETED')
            {
              openEditor.attr("title", openEditor.attr("title") + " - Waiting for approval");
              openEditor.removeClass("btn-primary").addClass("bg-color-magenta").addClass("txt-color-white");
              openEditor.empty().append($("<i>").attr("class", "fa fa-dot-circle-o"));
            }
            else if (row.workflow == 'DRAFT'  || row.workflow == 'DECLINED')
            {
              openEditor.attr("title", openEditor.attr("title") + " - Changes in draft");
              openEditor.removeClass("btn-primary").addClass("bg-color-magenta").addClass("txt-color-white");
              openEditor.empty().append($("<i>").attr("class", "fa fa-circle-o"));
            }
            var flagDelete = $("<button>")
              .attr("type", "button")
              .attr("class", "btn btn-default btn-xs flagDelete")
              .attr("title", "Flag for delete")
              .attr("data-toggle", "tooltip")
              .attr("data-placement", "bottom")
              .attr("data-action", "delete")
              .append($("<i>").attr("class", "fa fa-trash-o"));
            
            var payload = $("<div>");
            
            if (widgetType != 'elementList') payload.append(drillDown);
            if (Permissions.hasRead(row.privlage)) payload.append(openEditor);
            if (Permissions.hasDelete(row.privlage)) payload.append(flagDelete);
            if (payload.children().length == 0) payload.append("None");
            
            return payload.html();
          }
          else if (type=='filter')
          {
            return data;
          }
          return null;
        },
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).attr('class','statusCell text-center');
          $('[data-toggle="tooltip"]', td).tooltip();
          
          $('.drillDown', td).click(function(){
            if (widgetType != 'elementList')
            {
              var tableMetadata = $(".bulkEditor", $widget).data('tableMetadata');
              if (tableMetadata.level+1 == tableMetadata.depth)
              {
                WidgetHelper.addWidgetForBulkEditElements({}, widgetDataModule, cellData);
              }
              else
              {
                WidgetHelper.addWidgetForBulkEditCategories({}, widgetDataModule, cellData);
              }
            }
          });
          
          $('.openEditor', td).click(function(){
            WidgetHelper.lockWidget($widget);
            if (widgetType == 'elementList')
            {
              WidgetHelper.addWidgetForModuleElementId({}, widgetDataModule, cellData, widgetDataId);
            }
            else
            {
              WidgetHelper.addWidgetForModuleCategoryId({}, widgetDataModule, cellData, widgetDataId);
            }
          });
          
          $('.flagDelete', td).click(function(e){
          	var $button = $(e.currentTarget);
            var $tr = $button.closest("tr");   
            $tr.addClass("DTTT_selected");       	
            BulkEditor.bindButtonInSelection($widget, e.currentTarget, col, "deleteFlagged");
            e.stopPropagation();
          }).on('wd.button.clicked', function(e){
			var $button = $(e.currentTarget);
            var $tr = $button.closest("tr");
            if ($tr.hasClass('deleteFlagged'))
            {
              $button.removeClass('btn-danger').addClass('btn-default');
              $tr.removeClass('deleteFlagged');
            }
            else
            {
              $button.removeClass('btn-default').addClass('btn-danger');
              $tr.addClass('deleteFlagged');
            }
            BulkEditor.updateSummary($widget);
          });
        }
      };
    },

    rendereForTypeText : function($widget, columnInfo){
      var widgetType = JSON.parse($widget.data("requestData")).widgetType;
      var widgetDataModule = $widget.data("module");
      var widgetDataId = $widget.data("id");
      
      return {
        title: columnInfo.external,
        data:columnInfo.internal,
        name:columnInfo.external,
        orderable: true,
        visible: true,
        render: function(data, type, row){
          if (type == "display" && Permissions.hasUpdate(row.privlage) && !columnInfo.readonly)
          {
            var field = $("<input>")
                                .attr("type", "text")
                                .attr("class", "form-control input-xs")
                                .attr("value", data)
                                .attr("placeholder", columnInfo.external);
            
            return $("<div>").append(field).html();
          }
          return data;
        },
        createdCell: function (td, cellData, rowData, row, col) {
          $("input", td).on('change keyup',function(e){
            var currentValue = $(e.currentTarget).val();
            var modifiedTooltip = (cellData.length == 0)? "Was empty" : "Was: "+cellData;
            var tooltip = (currentValue == cellData)? "" : modifiedTooltip;
            $(e.currentTarget).attr("title", tooltip);
            
            rowData[columnInfo.internal] = currentValue;
            BulkEditor.valueChanged($widget, td, rowData, row, col, columnInfo.internal);
            
            BulkEditor.bindTextValueInSelection($widget, e.currentTarget, col);
          }).click(function(e){
            event.stopPropagation();
          });
        }
      };
    },

    rendereForTypePassword : function($widget, columnInfo){
      var widgetType = JSON.parse($widget.data("requestData")).widgetType;
      var widgetDataModule = $widget.data("module");
      var widgetDataId = $widget.data("id");
      
      return {
        title: columnInfo.external,
        data:columnInfo.internal,
        name:columnInfo.external,
        orderable: false,
        visible: true,
        render: function(data, type, row){
          if (type == "display")
          {
            var container = $("<div>");
            if (Permissions.hasUpdate(row.privlage) && !columnInfo.readonly)
            {
              var field = $("<input>")
                              .attr("type", "password")
                              .attr("class", "form-control input-xs")
                              .attr("value", data)
                              .attr("placeholder", columnInfo.external);
              container.append(field);
            }
            else
            {
              container.append('<i class="fa fa-asterisk fa-spin"></i>');
              container.append('<i class="fa fa-asterisk fa-spin"></i>');
              container.append('<i class="fa fa-asterisk fa-spin"></i>');
            }

            return container.html();
          }
          return "";
        },
        createdCell: function (td, cellData, rowData, row, col) {
          $("input", td).on('change keyup',function(e){
            var currentValue = $(e.currentTarget).val();
            var modifiedTooltip = (cellData.length == 0)? "Was empty" : "Was: "+cellData;
            var tooltip = (currentValue == cellData)? "" : modifiedTooltip;
            $(e.currentTarget).attr("title", tooltip);

            rowData[columnInfo.internal] = $(e.currentTarget).val();
            BulkEditor.valueChanged($widget, td, rowData, row, col, columnInfo.internal);
            
            BulkEditor.bindTextValueInSelection($widget, e.currentTarget, col);
          }).click(function(e){
            event.stopPropagation();
          });
        }
      };
    },

    rendereForTypeCheckbox : function($widget, columnInfo){
      var widgetType = JSON.parse($widget.data("requestData")).widgetType;
      var widgetDataModule = $widget.data("module");
      var widgetDataId = $widget.data("id");
      
      return {
        title: columnInfo.external,
        data:columnInfo.internal,
        name:columnInfo.external,
        orderable: true,
        visible: true,
        render: function(data, type, row){
          if (type == "display")
          {
            var container = $("<div>");
            if (Permissions.hasUpdate(row.privlage) && !columnInfo.readonly)
            {
              var $input = $("<input>")
                                .attr("type", "checkbox")
                                .attr("class", "checkbox");
              if (data == "1")
              {
                $input.attr("checked", "checked");
              }
              container.append($("<label>")
                                  .append($input)
                                  .append($("<span>")))
            }
            else
            {
              if (data == "1") container.append('<i class="fa fa-check"></i>');
              else container.append('<i class="fa fa-times-circle"></i>');
            }
            return container.html();
          }
          else if (type=='filter')
          {
            return data;
          }
          return data == "1" ? "T":"F";
        },
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).addClass("text-center");
          
          $("input", td).on('change keyup',function(e){
            rowData[columnInfo.internal] = $(e.currentTarget).is(":checked")?"1":"0";
            BulkEditor.valueChanged($widget, td, rowData, row, col, columnInfo.internal);
            BulkEditor.bindCheckboxValueInSelection($widget, e.currentTarget, col);
          }).click(function(e){
            e.stopPropagation();//for checkbox 
          });
          
          $("label span", td).on('click', function(e){
            e.stopPropagation();//for smartadmin checkbox
          });
        }
      };
    },

    rendereForTypeDate : function($widget, columnInfo){
      var widgetType = JSON.parse($widget.data("requestData")).widgetType;
      var widgetDataModule = $widget.data("module");
      var widgetDataId = $widget.data("id");
      
      return {
        title: columnInfo.external,
        data:columnInfo.internal,
        name:columnInfo.external,
        orderable: true,
        visible: true,
        render: function(data, type, row){
          if (type == "display")
          {
            var container = $("<div>");
            if (Permissions.hasUpdate(row.privlage) && !columnInfo.readonly)
            {
              var field = $("<input>")
                                .attr("type", "text")
                                .attr("class", "form-control input-xs dateField")
                                .attr("value", "")
                                .attr("placeholder", columnInfo.external);
              container.append(field);
            }
            else
            {
              container.append(moment(parseInt(data)).format('YYYY-MM-DD HH:mm'));
            }
            
            return container.html();
          }
          return data;
        },
        createdCell: function (td, cellData, rowData, row, col) {
          if (!Permissions.hasUpdate(rowData.privlage))
          {
            return;
          }
          
          var datePicker = $(".dateField", td).datetimepicker({
            format: 'YYYY-MM-DD HH:mm', 
            useCurrent: false
          });
          
          var initalDate = new Date(parseInt(cellData));
          if ( false == isNaN( initalDate.getTime()) )
          {
            datePicker.data("DateTimePicker").date(initalDate);
          }
          
          datePicker.on("dp.change",function (e) {
            //IMPORTANT... bind change event after setting date
            var momentDate = datePicker.data("DateTimePicker").date();
            if (momentDate != undefined)
            {
              rowData[columnInfo.internal] = (momentDate.unix()*1000).toString();
            }
            else
            {
              rowData[columnInfo.internal] = "";
            }
            
            BulkEditor.valueChanged($widget, td, rowData, row, col, columnInfo.internal);
            BulkEditor.bindDateValueInSelection($widget, e.currentTarget, col);
          }).click(function(e){
            event.stopPropagation();
          });
        }
      };
    },
    
    rendereForTypeFile : function($widget, columnInfo){
      var widgetType = JSON.parse($widget.data("requestData")).widgetType;
      var widgetDataModule = $widget.data("module");
      var widgetDataId = $widget.data("id");
      
      return {
        title: columnInfo.external,
        data:columnInfo.internal,
        name:columnInfo.external,
        orderable: false,
        visible: true,
        render: function(data, type, row){
          if (type == "display" && !columnInfo.readonly)
          {
            var anchor = $("<button>")
            .attr("type", "button")
            .attr("class", "btn btn-xs")
            .attr("target", "_blank")
            .attr("disabled", "disabled")
            .append($("<i>").attr("class", "fa"));
            return $("<div>").append(anchor).html();
          }
          return null;
        },
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).addClass("text-center");
          var $button = $("button",td);
          if (cellData != "")
          {
            var pathComponents = cellData.split("/");
            $button.removeAttr("disabled")
            .attr("href", cellData)
            .attr("title", pathComponents[pathComponents.length-1])
            .attr("data-toggle", "tooltip")
            .attr("data-placement", "bottom")
            .addClass("btn-info")
            .click(function(e){
              ModalHelper.loadInIframe(cellData);
            });
            $button.tooltip();
            
            $("i", $button).addClass("fa-eye");
          }
          else
          {
            $button.addClass("btn-link");
            $("i", $button).addClass("fa-ban");
          }
        }
      };
    },
    
    rendereForTypeFloat : function($widget, columnInfo){
      var widgetType = JSON.parse($widget.data("requestData")).widgetType;
      var widgetDataModule = $widget.data("module");
      var widgetDataId = $widget.data("id");
      
      return {
        title: columnInfo.external,
        data:columnInfo.internal,
        name:columnInfo.external,
        orderable: true,
        visible: true,
        render: function(data, type, row){
          if (type == "display" && !columnInfo.readonly)
          {
            var container = $("<div>");
            if (Permissions.hasUpdate(row.privlage))
            {
              var field = $("<input>")
                                .attr("type", "number")
                                .attr("step", "0.01")
                                .attr("class", "form-control input-xs")
                                .attr("value", data)
                                .attr("placeholder", columnInfo.external);
              container.append(field)
            }
            else
            {
              container.append(parseFloat(data).toFixed(2))
            }
            return container.html();
          }
          return data;
        },
        createdCell: function (td, cellData, rowData, row, col) {
          $("input", td).on('change keyup',function(e){
            var currentValue = $(e.currentTarget).val();
            var modifiedTooltip = (cellData.length == 0)? "Was empty" : "Was: "+cellData;
            var tooltip = (currentValue == cellData)? "" : modifiedTooltip;
            $(e.currentTarget).attr("title", tooltip);

            rowData[columnInfo.internal] = $(e.currentTarget).val();
            BulkEditor.valueChanged($widget, td, rowData, row, col, columnInfo.internal);
            
            BulkEditor.bindTextValueInSelection($widget, e.currentTarget, col);
          }).click(function(e){
            event.stopPropagation();
          });
        }
      };
    },
    
    rendereForTypeInteger : function($widget, columnInfo){
      var widgetType = JSON.parse($widget.data("requestData")).widgetType;
      var widgetDataModule = $widget.data("module");
      var widgetDataId = $widget.data("id");
      
      return {
        title: columnInfo.external,
        data:columnInfo.internal,
        name:columnInfo.external,
        orderable: true,
        visible: true,
        render: function(data, type, row){
          if (type == "display")
          {
            var container = $("<div>");
            if (Permissions.hasUpdate(row.privlage) && !columnInfo.readonly)
            {
              var field = $("<input>")
                                .attr("type", "number")
                                .attr("step", "1")
                                .attr("class", "form-control input-xs")
                                .attr("value", data)
                                .attr("placeholder", columnInfo.external);
              container.append(field)
            }
            else
            {
              container.append(parseInt(data))
            }
            return container.html();
          }
          return data;
        },
        createdCell: function (td, cellData, rowData, row, col) {
          $("input", td).on('change keyup',function(e){
            var currentValue = $(e.currentTarget).val();
            var modifiedTooltip = (cellData.length == 0)? "Was empty" : "Was: "+cellData;
            var tooltip = (currentValue == cellData)? "" : modifiedTooltip;
            $(e.currentTarget).attr("title", tooltip);

            rowData[columnInfo.internal] = $(e.currentTarget).val();
            BulkEditor.valueChanged($widget, td, rowData, row, col, columnInfo.internal);
            
            BulkEditor.bindTextValueInSelection($widget, e.currentTarget, col);
          }).click(function(e){
            event.stopPropagation();
          });
        }
      };
    },
    
    rendereForTypeDropdown : function($widget, columnInfo){
      var widgetType = JSON.parse($widget.data("requestData")).widgetType;
      var widgetDataModule = $widget.data("module");
      var widgetDataId = $widget.data("id");
      
      return {
        title: columnInfo.external,
        data:columnInfo.internal,
        name:columnInfo.external,
        orderable: true,
        visible: true,
        render: function(data, type, row){
          if (type == "display")
          {
            var container = $("<div>");
            if (Permissions.hasUpdate(row.privlage) && !columnInfo.readonly)
            {
              var initWithLazyLoad = (columnInfo.options.length > BULK_EDITOR_LAZY_LOAD_THRESHOLD);
              if (initWithLazyLoad)
              {
                BulkEditorLazyLoading.initDropWithLazyLoad($widget, container, data, columnInfo.options);
              }
              else
              {
                BulkEditorLazyLoading.initDropWithData($widget, container, data, columnInfo.options);
              }
            }
            else
            {
              var found = undefined;
              $.each(columnInfo.options, function(i, e){
                if (e.value == data)
                {
                  found = e.text;
                  return false;
                }
              });
              if (found == undefined)
              {
                found = data;
              }
              container.append(found);
            }
            return container.html();

          }
          return data;
        },
        createdCell: function (td, cellData, rowData, row, col) {
          var initDropdownCell = function(){
            $("select", td).on("change blur", function(e){
              BulkEditor.bindSelectValueInSelection($widget, e.currentTarget, col);
            }).on("wd.select.changed", function(e){
              var currentValue = $(e.currentTarget).val();
              var modifiedTooltip = (cellData.length == 0)? "Was empty" : "Was: "+cellData;
              var tooltip = (currentValue == cellData)? "" : modifiedTooltip;
              $(e.currentTarget).attr("title", tooltip);

              rowData[columnInfo.internal] = currentValue;
              BulkEditor.valueChanged($widget, td, rowData, row, col, columnInfo.internal);
            }).click(function(e){
              event.stopPropagation();
            });
          };//end function
          
          var initWithLazyLoad = (columnInfo.options.length > BULK_EDITOR_LAZY_LOAD_THRESHOLD);
          if (initWithLazyLoad)
          {
            $(td).addClass("preLazyLoadCell");
            $(".lazyLoadAction", td).one('click', function(){
              var container = $(td).empty().removeClass("preLazyLoadCell");
              BulkEditorLazyLoading.initDropWithData($widget, container, cellData, columnInfo.options);
              
              initDropdownCell();
            });
          }
          else
          {
            initDropdownCell();
          }
        }
      };
    },
    
    rendereForTypeDropdownMulti : function($widget, columnInfo){
      var widgetType = JSON.parse($widget.data("requestData")).widgetType;
      var widgetDataModule = $widget.data("module");
      var widgetDataId = $widget.data("id");
      
      return {
        title: columnInfo.external,
        data:columnInfo.internal,
        name:columnInfo.external,
        orderable: true,
        visible: true,
        render: function(data, type, row){
          if (type == "display")
          {
            var container = $("<div>");
            if (Permissions.hasUpdate(row.privlage) && !columnInfo.readonly)
            {
              var initWithLazyLoad = (columnInfo.options.length > BULK_EDITOR_LAZY_LOAD_THRESHOLD);
              if (initWithLazyLoad)
              {
                BulkEditorLazyLoading.initDropMultiWithLazyLoad($widget, container, data, columnInfo.options);
              }
              else
              {
                BulkEditorLazyLoading.initDropMultiWithData($widget, container, data, columnInfo.options);
              }
            }
            else
            {
              var dataMap = {};
              $.each(columnInfo.options, function(i,e){
                dataMap[e.value] = e.text;
              });

              var $ol = $("<ul>");
              $.each(data, function(i, d){
                
                if (dataMap[d] != undefined)
                {
                  $ol.append($("<li>")
                                  .attr('title', d+" | "+dataMap[d])
                                  .text(dataMap[d]));
                }
              });

              container.append($ol);
            }
            
            return container.html();
          }
          return data;
        },
        createdCell: function (td, cellData, rowData, row, col) {
          var initDropdownMultiCell = function(){
            $("select", td).on("wd.miltiselect.regroup", function(e){
              var selectedOptions = $('option:selected', e.currentTarget);
              var overflowOptions = $('option:not(:selected)', e.currentTarget);
              
              var selectedOptionGroup = $("#selectedOptionGroup", e.currentTarget);
              selectedOptionGroup.prepend(selectedOptions);
              var overflowOptionGroup = $("#overflowOptionGroup", e.currentTarget);
              overflowOptionGroup.prepend(overflowOptions);
              
              selectedOptionGroup.css("display", (selectedOptionGroup.children("option").length == 0) ? "none" : "block");
              overflowOptionGroup.css("display", (overflowOptionGroup.children("option").length == 0) ? "none" : "block");
            });
            
            $("select", td).on("blur", function(e){
              //FOCUS LOST:
              $("select", td).trigger("wd.miltiselect.regroup");
              $("select", td).animate({ scrollTop: "0px" });
              
              //Update hint
              var selectedOptions = $('option:selected', e.currentTarget);
              $(".multiSelectedCounter", td).text(selectedOptions.length);
            });
            
            $("input#filter", td).on("change keyup", function(e){
              //FILTER CHANGED:
              var filter = $(e.currentTarget).val().toLowerCase();
              var options = $("option", td);
              if (filter == "")
              {
                //NO FILTER
                options.css("display", "block");
                $("select", td).blur();
              }
              else
              {
                //DO BASIC FILTERING
                options.each(function(i, e){
                  var text = $(e).text().toLowerCase();
                  var value = $(e).attr("value").toLowerCase();
                  
                  if (text.indexOf(filter) < 0
                      && value.indexOf(filter) < 0)
                  {
                    $(e).css("display", "none");
                  }
                  else
                  {
                    $(e).css("display", "block");
                  }
                });
              }
            });
            
            $("select", td).on("change blur", function(e){
              BulkEditor.bindSelectValueInSelection($widget, e.currentTarget, col);
            }).on("wd.select.changed", function(e){
              //VALUE CHANGED:
              var currentValue = $(e.currentTarget).val();
              if (currentValue == undefined)
                currentValue = [];
              else
                currentValue = currentValue.sort();//we sort for the cell change detection algorithm.
              
              var valueUnchanged = $(currentValue).not(cellData).length === 0
              && $(cellData).not(currentValue).length === 0;
              var modifiedTooltip = (cellData.length == 0)? "Was empty" : "Was: "+cellData;
              var tooltip = (valueUnchanged)? "" : modifiedTooltip;
              $(e.currentTarget).attr("title", tooltip);
              
              rowData[columnInfo.internal] = currentValue;
              BulkEditor.valueChanged($widget, td, rowData, row, col, columnInfo.internal);
              
              //Update hint
              var selectedOptions = $('option:selected', e.currentTarget);
              $(".multiSelectedCounter", td).text(selectedOptions.length);
            }).click(function(e){
              event.stopPropagation();
            });
            
            $("input", td).on('change keyup',function(e){});
          };//End function
          
          var initWithLazyLoad = (columnInfo.options.length > BULK_EDITOR_LAZY_LOAD_THRESHOLD);
          if (initWithLazyLoad)
          {
            $(td).addClass("preLazyLoadCell");
            $(".lazyLoadAction", td).one('click', function(){
              var container = $(td).empty().removeClass("preLazyLoadCell");
              BulkEditorLazyLoading.initDropMultiWithData($widget, container, cellData, columnInfo.options);
              
              initDropdownMultiCell();
            });
          }
          else
          {
            initDropdownMultiCell();
          }
          
        }
      };
    },
    
    rendereForTypeUnknown : function($widget, columnInfo){
      var widgetType = JSON.parse($widget.data("requestData")).widgetType;
      var widgetDataModule = $widget.data("module");
      var widgetDataId = $widget.data("id");
      
      return {
        title: columnInfo.external,
        data:columnInfo.internal,
        name:columnInfo.external,
        orderable: false,
        visible: true,
        render: function(data, type, row){
          if (type == "display" && !columnInfo.readonly)
          {
            return data;
          }
          return null;
        },
        createdCell: function (td, cellData, rowData, row, col) {
        }
      };
    },
};

var BulkEditorLazyLoading = {
    initDropWithLazyLoad: function($widget, container, data, options){
      var displayLabel = undefined;
      
      $.each(options, function(i, e){
        if (e.value == data)
        {
          //found the friendly text
          displayLabel = e.text;
          return false;
        }
      });
      if (displayLabel == undefined)
      {
        //fallback
        if (data == undefined || data == "")
        {
          displayLabel = $("<small>").attr("class", "hint")
                            .append($("<i>").attr("class", "fa fa-info-circle"))
                            .append(" Nothing selected");
        }
        else
        {
          displayLabel = data;
          //logger.trace("Data not found in dropdown. Using fallback: "+displayLabel);
        }
      }
      var moreButton = $('<button>')
                            .attr("type", "button")
                            .attr("class", "btn btn-xs btn-default lazyLoadAction")
                            .append($("<i>").attr("class", "fa fa-pencil"));
      
      $(container).append(displayLabel, moreButton);
    },
    
    initDropMultiWithLazyLoad: function($widget, container, data, options){
      
      var moreButton = $('<button>')
                            .attr("type", "button")
                            .attr("class", "btn btn-xs btn-default lazyLoadAction")
                            .append($("<i>").attr("class", "fa fa-pencil"));
      var displayData = "";
      var hint = $("<small>").attr("class", "hint")
                    .append($("<i>").attr("class", "fa fa-info-circle"))
                    .append(" ")
                    .append($("<span>").attr("class", "multiSelectedCounter").text(data.length))
                    .append(" of "+options.length+" selected");

      if (data.length > 0)
      {
        var selectedOption = {};
        $.each(data, function(i,e){
          selectedOption[e] = true;
        });
        
        displayData = $("<ul>");
        $.each(options, function(i, e){
          if (selectedOption[e.value] == true)
          {
            displayData.append($("<li>").text(e.text));
          }
        });
      }

      $(container).append(displayData, hint, moreButton);
    },
    
    initDropWithData: function($widget, container, data, options){
      var select = $("<select>").attr("class", "form-control input-xs");
      var blank = $("<option>");
      select.append(blank);
      
      var found = false;
      $.each(options, function(i, e){
        var opt = $("<option>")
                        .attr('value', e.value)
                        .attr('title', e.value+" | "+e.text)
                        .text(e.text);
        if (e.value == data)
        {
          found = true;
          opt.attr('selected', 'selected');
          opt.addClass("original");
        }
        select.append(opt);
      });
      if (!found && data!="")
        blank.after($("<option>")
            .attr('value', data)
            .attr('selected', 'selected')
            .attr('title', data+" | (Unknown Friendly Text)")
            .addClass("original")
            .text(data));
      
      container.append(select);
    },
    
    initDropMultiWithData: function($widget, container, data, options){
      var search = $("<input>")
            .attr("id", "filter")
            .attr("type", "text")
            .attr("class", "form-control input-xs")
            .attr("placeholder", "Filter");
      
      var hint = $("<small>").attr("class", "hint")
                .append($("<i>").attr("class", "fa fa-info-circle"))
                .append(" ")
                .append($("<span>").attr("class", "multiSelectedCounter").text(data.length))
                .append(" of "+options.length+" selected");
      
      var selectedOption = {};
      $.each(data, function(i,e){
        selectedOption[e] = true;
      });
      
      var select = $("<select>")
          .attr("size", "7")
          .attr("class", "form-control input-xs")
          .attr("multiple", "multiple");
      var selectedOptionGroup = $("<optgroup>")
                          .attr("id", "selectedOptionGroup")
                          .attr("label", "Selected");
      var overflowOptionGroup = $("<optgroup>")
                            .attr("id", "overflowOptionGroup")
                            .attr("label", "Options");
      
      $.each(options, function(i, e){
        var opt = $("<option>")
              .attr('value', e.value)
              .attr('title', e.value+" | "+e.text)
              .text(e.text);
        if (selectedOption[e.value] == true)
        {
          opt.attr('selected', 'selected');
          opt.addClass("original");
          selectedOptionGroup.append(opt);
        }
        else
        {
          overflowOptionGroup.append(opt);
        }
      });
      selectedOptionGroup.css("display", (selectedOptionGroup.children("option").length == 0) ? "none" : "block");
      overflowOptionGroup.css("display", (overflowOptionGroup.children("option").length == 0) ? "none" : "block");
      
      select.append(selectedOptionGroup);
      select.append(overflowOptionGroup);
      
      container.append(search).append(select).append(hint);
    }
};