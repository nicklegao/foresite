var DataType = {
    getTypeIcon: function(internalName){
      internalName = internalName.toLowerCase();
      if (internalName == "attr_headline")
        return $("<i>").attr("class", "fa fa-tag")
      if (internalName.startsWith("attr_")
          || internalName.startsWith("attrfloat_")
          || internalName.startsWith("attrinteger_"))
        return $("<i>").attr("class", "fa fa-font")
      if (internalName.startsWith("attrdate_") 
          || internalName == "live_date"
          || internalName == "expire_date")
        return $("<i>").attr("class", "fa fa-calendar")
      if (internalName.startsWith("attrcheck_")
          || internalName == "live")
        return $("<i>").attr("class", "fa fa-check")
      if (internalName.startsWith("attrtext_"))
        return $("<i>").attr("class", "fa fa-paragraph")
      if (internalName.startsWith("attrlong_"))
        return $("<i>").attr("class", "fa fa-pencil")
      if (internalName.startsWith("attrfile_"))
        return $("<i>").attr("class", "fa fa-file-o")
      if (internalName.startsWith("attrcurrency_"))
        return $("<i>").attr("class", "fa fa-dollar")
      if (internalName.startsWith("attrpass_"))
        return $("<i>").attr("class", "fa fa-asterisk fa-spin")
      if (internalName.startsWith("attrnotes_"))
        return $("<i>").attr("class", "fa fa-th-list")
      if (internalName.startsWith("attrdrop_"))
        return $("<i>").attr("class", "fa fa-th-list")
      if (internalName.startsWith("attrmulti_"))
        return $("<i>").attr("class", "fa fa-list-ol")
      return $("<i>").attr("class", "fa fa-question")
    },

    isTypeEditable: function(internalName){
      if (internalName.startsWith("attr_")
          || internalName.startsWith("attrdate_")
          || internalName.startsWith("attrcheck_")
          || internalName.startsWith("attrcurrency_")
          || internalName.startsWith("attrfloat_")
          || internalName.startsWith("attrdecimal_")
          || internalName.startsWith("attrinteger_")
          || internalName.startsWith("attrpass_")
          || internalName.startsWith("attrdrop_")
          || internalName.startsWith("attrmulti_")
          || internalName == "live_date"
          || internalName == "expire_date"
          || internalName == "live")
        return true;
      else return false;
    }
};
