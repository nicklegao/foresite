$(document).ready(function() {

  // DO NOT REMOVE : GLOBAL FUNCTIONS!
  pageSetUp();

  ModalHelper.initModal($("#modal-setting"), $(window).height() * 0.9);
  StorageHelper.initStorage();

  $('#demo-setting').click(function () {
      $('.demo').toggleClass('activate');
  });

  var leftPanel = $("#left-panel");
  var mainPanel = $("#main");
  var fixedRibbon = $(".fixed-ribbon #ribbon")

  leftPanel.resizable({
    minWidth: 220,
    start: function() {
      leftPanel.addClass("no-transition");
    },
    resize: function(event, ui) {
      mainPanel.css("margin-left", ui.size.width + "px");
      fixedRibbon.css("left", ui.size.width + "px");
    },
    stop: function() {
      leftPanel.removeClass("no-transition");
    }
  });
  
  var shortcut_dropdown = $("#shortcut");
  $("#show-shortcut").on("click", function() {
    if (shortcut_dropdown.is(":visible")) {
      shortcut_buttons_hide();
    } else {
      shortcut_buttons_show();
    }
    
    // SHORTCUT ANIMATE SHOW
    function shortcut_buttons_show() {
      var storage = localStorage['openModules'];
      var openModules = (storage==undefined)?[]:JSON.parse(storage);
      $.getJSON("/wd-api/module/assignedModules", function(data) {
        var ul = shortcut_dropdown.find("ul");
        ul.empty();
        $.each(data, function(index, item) {
          var li = $("<li></li>");
          var a = $('<a class="jarvismetro-tile big-cubes"> <span class="iconbox"> <i class="fa-4x"></i> <span></span> </span> </a>');
          a.find(".iconbox span").text(item.module_description);
          a.attr("href", "#");
          a.addClass(item.color);
          a.find("i").addClass(item.icon);
          a.data("module", item);
          if ($.inArray(item.module_name, openModules) >= 0)
          {
            a.addClass("selected");
          }
          /* a.data("module-desc", item.module_description); */
          li.append(a).appendTo(ul);
        });
        
        shortcut_dropdown.animate({
          height : "show"
        }, 300, "easeOutCirc");
        $.root_.addClass('shortcut-on');
      });
      
    }
  });

  shortcut_dropdown.on("click", function(e) {
    e.preventDefault();
    shortcut_buttons_hide();
  });
  
  shortcut_dropdown.on("click", "a", function(e) {
    e.preventDefault();
    if ($(e.currentTarget).hasClass("selected"))
    {
      ModuleHelper.removeModule($(this).data("module").module_name);
    }
    else
    {
      ModuleHelper.addModule($(this).data("module"));
    }
    shortcut_buttons_hide();
  });
  
  // SHORTCUT ANIMATE HIDE
  function shortcut_buttons_hide() {
    shortcut_dropdown.animate({
      height : "hide"
    }, 300, "easeOutCirc");
    $.root_.removeClass('shortcut-on');
  }
  
  $("#left-panel").find("nav").children("ul").on("click", ".module > a", function(e) {
    e.preventDefault();
    var li = $(this).closest(".module"); 
    var data = li.data("module");

    if (li.hasClass("open")) {
      li.removeClass("open");
      li.find("section").slideUp();
      StorageHelper.setActiveModule(false);
    } else {
      $(this).closest("ul").children("li").each(function(index, item) {
        if($(this).hasClass("open")) {
          $(this).removeClass("open");
          $(this).find("section").slideUp();
        }
      });
      if (li.hasClass("initialised")) {
        //existing lazy tree
        setConfigureModule(data);
        li.addClass("open");
        li.find("section").slideDown();
      } else {
        //existing new tree
        setConfigureModule(data);
        TreeHelper.createTree(data);
      }
    }
    
    if ($(e.target).is("i"))
    {
      return;
    }
    //icon not clicked... do other stuff
    WidgetHelper.addWidgetForBulkEditCategories({}, data.module_name, 0);
  });
  
  //restore state
  WidgetHelper.restoreWidgetState();
  
  //Load the todo list
  TodoList.initDropdown();
});

// Kept to make new configureModule function work
function setConfigureModule(module) {
  var link = $("#configure-module");
  link.attr("href", '/admin/jsp/configureModuleSettings.jsp?moduleName=' + module.module_name);
  link.find("i").removeClass().addClass(module.icon);
  link.find("span").text("Configure " + module.module_description);
  /*if (module) {
    link.css("display", "block");
  } else {
    link.css("display", "none");
  }*/
  StorageHelper.setActiveModule(module.module_name);
}

function resetCheckboxes() {
    var $parent = $(".show-checkboxes");
    $parent.each(function(index, parent) {
      var node = $.ui.fancytree.getNode(parent);
      $.each(node.getChildren(), function(index, item) {
        item.setSelected(false);
      });
      $(parent).removeClass("show-checkboxes");
    });
    lastAction = null;
  }

function initTree($element, data, source, callback) {
  var internalName = data.module_name;
  var displayName = data.module_description;
  var levels = data.levels;
  var filterByPrivileges = data.filter_by_privileges == true;//will always be false for admins

  var expandedNodes = null;
  if (!$element.hasClass("search-tree")) {
    expandedNodes = StorageHelper.getExpandedNodes(internalName);
    StorageHelper.resetExpandedNodes(internalName);    
  }
  
  $element.fancytree({
    init: callback,
    extensions: ["glyph"],
    glyph: {
      map: {
        doc: "fa fa-file",
        docOpen: "fa fa-file",
        checkbox: "fa fa-square-o",
        checkboxSelected: "fa fa-check-square-o",
        checkboxUnknown: "fa fa-share-square-o",
        error: "fa fa-exclamation-triangle",
        expanderClosed: "fa fa-plus",
        expanderLazy: "fa fa-plus",
        expanderOpen: "fa fa-minus",
        folder: "fa fa-folder",
        folderOpen: "fa fa-folder-open",
        loading: "fa fa-spinner fa-pulse"
      }
    },
    checkbox: true,
    source: source || $.ajax('/wd-api/folderTree/initTree', {
      dataType: "json",
      data: {
        moduleName: internalName,
        levels: levels,
        filterByPrivileges: filterByPrivileges,
        expandedNodes: JSON.stringify(expandedNodes)
      }
    }),
    lazyLoad: function(event, data) { //when expanded
      var node = data.node;
      var expandedNodes = StorageHelper.getExpandedNodes(internalName);
      StorageHelper.resetExpandedChildren(internalName, node.key);
      data.result = $.ajax("/wd-api/folderTree/lazyload", {
        dataType: "json",
        data: {
          moduleName: internalName,
          levels: levels,
          curLevel: node.data.folderLevel,
          parentId: node.key,
          pageNum: 0,
          filterByPrivileges: filterByPrivileges,
          expandedNodes: JSON.stringify(expandedNodes)
        }
      });
    },
    createNode: $element.hasClass("search-tree") ? null : function(event, data) {
      var node = data.node;
      if (node.folder && node.expanded && !node.lazy && node.data.parentId !== null) {
        StorageHelper.expandNode(internalName, node.data.parentId, node.key);  
      }
    },
    expand: $element.hasClass("search-tree") ? null : function(event, data) {
      var node = data.node;
      StorageHelper.expandNode(internalName, node.data.parentId, node.key);
    },
    collapse: $element.hasClass("search-tree") ? null : function(event, data) {
      var node = data.node;
      StorageHelper.collapseNode(internalName, node.data.parentId, node.key);
    },
    click: function(event, data) { //when a item clicked
      var node = data.node;
      var parent = node.getParent();
      
      if (data.targetType == "icon" && node.folder) {
        node.toggleExpanded();
        return false;
      }
      
      if (data.targetType == "expander") {
        return true;
      }

      if (node.key === parent.key + '-load') {//Load more
        if (parent.data.pageNum) {
          parent.data.pageNum++;
        } else {
          parent.data.pageNum = 1;
        }

        $.ajax('/wd-api/folderTree/lazyload', {
          dataType: "json",
          data: {
            moduleName: internalName,
            levels: levels,
            curLevel: node.parent.data.folderLevel,
            parentId: parent.key,
            pageNum: parent.data.pageNum,
            filterByPrivileges: filterByPrivileges
          },
          success: function(treeData) {
            $.map(treeData, function(item, index) {
              item.extraClasses = item.extraClasses + " fade-in"; 
            });
            var ul = $(parent.ul);
            var curHeight = ul.css("height");             
            ul.css("maxHeight",curHeight);

            node.remove();
            parent.addChildren(treeData);

            ul.css("height",curHeight);
            ul.css("maxHeight","none");

            ul.animate({
              "height": ul[0].scrollHeight
            }, 200, function() {
              ul.css("height","auto");
            });
            ul.find(".fade-in").animate({
              "opacity":"1"
            }, 200, function() {
              $(this).removeClass("fade-in");
            });
          }
        });
      } else if (node.folder === false) { //Element clicked
        if (node.data.workflow) {
          var params = {
            draft : true,
            DID : node.data.draftId
          };
          WidgetHelper.addWidgetForModuleElementId(params, internalName, "0", node.parent.key);
        } else {          
          WidgetHelper.addWidgetForModuleElementId({}, internalName, node.key, node.parent.key);
        }
      } else { //Category clicked
        var search = $(event.currentTarget).closest(".module").find(".tree-search-box input").val();
        if (levels == node.data.folderLevel)
        {
          //Display list of elements.
          var $widget = WidgetHelper.addWidgetForBulkEditElements({
            search: search
          }, internalName, data.node.key);
        }
        else
        {
          //Display list of categories.
          var $widget = WidgetHelper.addWidgetForBulkEditCategories({
            search: search
          }, internalName, data.node.key);
        }
      }
    }
  });
  $element.fancytree("getTree").setFocus(true);
}

function toggleWidgetLock() {
  var $widget = $(this).closest(".jarviswidget");
  if (WidgetHelper.widgetIsLocked($widget)) {
    WidgetHelper.unlockWidget($widget);
  } else {
    WidgetHelper.lockWidget($widget);
  }
  WidgetHelper.saveWidgetState();
}

var TreeHelper = {
    createTree : function(module, source) {
      var $li = $("#left-panel").find("#li_" + module.module_name);
      $li.addClass("open initialised");
      
      var $treeDiv = $li.find(".lazytree");
      $treeDiv.attr("id", "tree_" + module.module_name);
      
      initTree($treeDiv, module, source, function() {
        $li.find("section").slideDown();
      });
      //$treeDiv.parent().slideDown();      
    },
    breadcrumbForNode : function(node)
    {
      var currentNode = node;
      var breadcrumb = currentNode.title; // default
      while (currentNode != undefined && currentNode.data.folderLevel != undefined)
      {
        if (currentNode == node)
        {
          breadcrumb = currentNode.title;
        }
        else
        {
          breadcrumb = currentNode.title + " | " + breadcrumb;
        }
        currentNode = currentNode.parent;
      }
      return breadcrumb;
    },
    reloadChildren : function(moduleName, categoryId, animate) {
      var $tree = $("#tree_" + moduleName);
      var node = $tree.fancytree("getNodeByKey", categoryId.toString());
      TreeHelper.reloadNode(node, animate);
    },
    reloadNode : function(node, animate) {
      if (animate !== false) {
        node.resetLazy();
        node.setExpanded();  
      } else {
        node.removeChildren();
        node.lazy = true;
        node.children = undefined;
        node.load();
      }
    }
};

var ModuleHelper = {
  fetchAssignedModules : function() {
    return $.getJSON("/wd-api/module/assignedModules");
  },
  addModule : function(module) {
    if ($("#left-panel").find("#li_" + module.module_name).length === 0) {
      var $li = $(".htmlFragments .module").clone();
      $li.attr("id", "li_" + module.module_name)      
      $li.addClass(module.color);
      $li.find(".menu-item-parent").text(module.module_description);
      $li.data("module", module);
      
      var $a = $li.children("a");
      $a.addClass(module.color);
      $a.find("i").addClass(module.icon);
      
      $("#left-panel").find("nav").children("ul").append($li);
      ModuleHelper.initSearchBox($li, module);
      StorageHelper.addModule(module.module_name);
    }
  },
  removeModule : function(moduleName) {
    $("#li_" + moduleName).remove();
    StorageHelper.removeModule(moduleName);
  },
  initSearchBox : function($li, module) {
    $li.find(".tree-search-icon").click(function(e) {
      e.stopPropagation();
      $li.find(".tree-search-box").slideToggle();
      if ($li.hasClass("search-active")) {
        $li.find(".tree-search-box input").val("");
        $li.removeClass("search-active");
      }
    });
    var radioName = "search-option-" + module.module_name;
    $li.find("input[type='radio']").attr("name", radioName).change(function() {
      var $this = $(this);
      $this.closest(".tree-search-box").data("search-mode", $this.data("search-mode"));
    });
    $li.find(".clear-search").click(function() {
      $li.find(".tree-search-box input").val("");
      $li.removeClass("search-active");
    });
    $li.find(".tree-search-box input").keypress(function(e) {
      var $section = $(this).closest("section");
      var key = e.which;
      if (key == 13) {
        var filterText = $(this).val();
        if (!filterText) {
          $li.find(".tree-search-box input").val("");
          $li.removeClass("search-active");
          return;
        }
        $.ajax({
          url : '/wd-api/folderTree/filterTree',
          type : 'GET',
          data : {
            moduleName: module.module_name,
            levels: module.levels,
            filterByPrivileges: module.filter_by_privileges == true,
            filterText: filterText,
            searchMode: $(this).parent().data("search-mode")
          }
        }).done(function(data) {
          var $searchTree = $section.find(".search-tree"); 
          if ($searchTree.children().length > 0) {
            $searchTree.empty().fancytree("destroy");
          }
          $section.parent().addClass("search-active");
          initTree($searchTree, module, data);
        });
      }
    });
  },
  expandModuleTo: function($widget, module, id, expandMode, callback) {
	  var moduleName = module.module_name;
	  var expandedNodes = null;
	  expandedNodes = StorageHelper.getExpandedNodes(moduleName);
	  $.ajax({
          url : '/wd-api/folderTree/expandTree',
          type : 'GET',
          data : {
            moduleName: moduleName,
            id: id,
            levels: module.levels,
            expandedNodes: JSON.stringify(expandedNodes),
            filterByPrivileges: module.filter_by_privileges == true,
            expandMode: expandMode
          }
        }).done(function(data) {
        	var $module = $("#li_" + moduleName);
        	StorageHelper.setActiveModule(moduleName);
        	//Removing open class so it doesn't slide up
        	$module.removeClass("open");
        	//Closing all other open modules
       	 	$module.closest("ul").children("li").each(function(index, item) {
       	 		if($(this).hasClass("open")) {
       	 				$(this).removeClass("open");
	        	    	$(this).find("section").slideUp();
	            }
        	}); 
        	$module.find("section").slideDown();
        	//Adding open class back
        	$module.addClass("open");
        	var $lazyTree = $module.find(".lazytree");
        	$lazyTree.attr("id", "tree_" + moduleName);
        	if ($lazyTree.children().length > 0) {
                $lazyTree.empty().fancytree("destroy");
            }
        	initTree($lazyTree, module, data)
        	callback($widget, module, id, expandMode);
        });
  },
  selectCategoriesForAction: function(module, key, selectedItems, parentId) {
	  resetCheckboxes();
	  var node = $("#tree_" + module.module_name).fancytree("getTree").getNodeByKey("0");
	  if(selectedItems.length > 0) {
		  var category = ModuleHelper.findInModule(node, parentId, selectedItems[0].category_id);
	  }
	  if(category != null) {
		  $(category.li).parent().addClass("show-checkboxes");
		  
		  selectedItems.forEach(function(item){
			  var element = ModuleHelper.findInModule(node, parentId, item.category_id);
			  element.setSelected();
	  	  });
		  
		  lastAction = {
				  type: key,
				  module: module.module_name,
				  parentNode: category.parent,
				  isCategory: category.folder
		  };
		  setLastAction(lastAction);
	  }
  },
  selectElementsForAction: function(module, key, selectedItems, parentId) {
	  resetCheckboxes();
	  var node = $("#tree_" + module.module_name).fancytree("getTree").getNodeByKey("0");
	  if(selectedItems.length > 0) {
		  var category = ModuleHelper.findInModule(node, parentId, selectedItems[0].element_id);
	  }
	  if(category != null) {
		  $(category.li).parent().addClass("show-checkboxes");
		  
		  selectedItems.forEach(function(item){
			  var element = ModuleHelper.findInModule(node, parentId, item.element_id);
			  element.setSelected();
	  	  });
		  
		  lastAction = {
				  type: key,
				  module: module.module_name,
				  parentNode: category.parent,
				  isCategory: category.folder
		  };
		  setLastAction(lastAction);
	  }
  },
  findInModule: function(node, parentId, elementId) {
	  if(node.key == elementId && node.getParent().key == parentId) {
		  //WE FOUND IT
		  return node;
	  }
	  if(node.getFirstChild() != null) {
		  //Loop through first child
		  var nextChild = node.getFirstChild();
		  var result = ModuleHelper.findInModule(nextChild, parentId, elementId);
	  }
	  if(result == null) {
		  //Loop through siblings
		  if(node.getNextSibling() != null) {
			  var nextChild = node.getNextSibling();
			  result = ModuleHelper.findInModule(nextChild, parentId, elementId);
		  }
		  //If nothing is found return null
		  return result;
	  } else {
		  //Returns something not null
		  return result;
	  }
  },
  moduleIsOpen: function(module) {
	  return $(".module#li_" + module).size() > 0
  },
  nodeIsExpanded: function(module) {
	  $(".fancytree-expanded").find(".fancytree-title").each(function() { 
		  console.log($(this).text()) 
	  })
  }
};

var StorageHelper = {
  initStorage : function() {
    this.treeKey = "treeExpandedNodes";
    this.openModulesKey = "openModules";
    this.activeModuleKey = "activeModule";
    if (!localStorage.getItem(this.treeKey)) {
      localStorage.setItem(this.treeKey, JSON.stringify({}));
    }
    this.restoreOpenModules();
  },
  addModule : function(moduleName) {
    var openModules = JSON.parse(localStorage.getItem(this.openModulesKey)) || [];
    openModules.push(moduleName);
    localStorage.setItem(this.openModulesKey, JSON.stringify(openModules));
  },
  removeModule : function(moduleName) {
    if (this.activeModuleKey === moduleName)
      this.setActiveModule(false);
    var openModules = JSON.parse(localStorage.getItem(this.openModulesKey)) || [];
    var index = openModules.indexOf(moduleName);
    openModules.splice(index, 1);
    localStorage.setItem(this.openModulesKey, JSON.stringify(openModules));
  },
  setActiveModule : function(moduleName) {
    if (moduleName)
      localStorage.setItem(this.activeModuleKey, moduleName);
    else
      localStorage.removeItem(this.activeModuleKey);
  },
  expandNode : function(moduleName, parentId, categoryId) {
    var expandedNodes = JSON.parse(localStorage.getItem(this.treeKey));
    expandedNodes[moduleName] = expandedNodes[moduleName] || {};
    expandedNodes[moduleName][parentId] = expandedNodes[moduleName][parentId] || {};
    expandedNodes[moduleName][parentId][categoryId] = true;
    localStorage.setItem(this.treeKey, JSON.stringify(expandedNodes));
  },
  collapseNode : function(moduleName, parentId, categoryId) {
    var expandedNodes = JSON.parse(localStorage.getItem(this.treeKey));
    var parent = expandedNodes[moduleName] && expandedNodes[moduleName][parentId];
    if (parent) {
      delete parent[categoryId];
    }
    localStorage.setItem(this.treeKey, JSON.stringify(expandedNodes));
  },
  restoreOpenModules : function() {
    $("#left-panel > nav > ul").empty();
    var openModules = JSON.parse(localStorage.getItem(this.openModulesKey)) || [];
    localStorage.removeItem(this.openModulesKey);
    var activeModule = localStorage.getItem(this.activeModuleKey);
    ModuleHelper.fetchAssignedModules().done(function(assignedModules) {
      for (var i=0; i < openModules.length; i++) {
        for (var j=0; j < assignedModules.length; j++) {
          if (openModules[i] === assignedModules[j].module_name) {
            ModuleHelper.addModule(assignedModules[j]);
            if (openModules[i] === activeModule) {
              StorageHelper.restoreActiveModuleTree(assignedModules[j]);
            }
          }
        }
      }
    });
  },
  restoreActiveModuleTree : function(module) {
    setConfigureModule(module);
    TreeHelper.createTree(module);
  },
  getExpandedNodes : function(moduleName) {
    var expandedNodes = JSON.parse(localStorage.getItem(this.treeKey));
    return expandedNodes && expandedNodes[moduleName];
  },
  resetExpandedNodes : function(moduleName) {
    var expandedNodes = JSON.parse(localStorage.getItem(this.treeKey));
    if (expandedNodes && expandedNodes[moduleName]) {
      delete expandedNodes[moduleName];
      localStorage.setItem(this.treeKey, JSON.stringify(expandedNodes));
    }
  },
  resetExpandedChildren : function(moduleName, parentId) {
    var expandedNodes = JSON.parse(localStorage.getItem(this.treeKey));
    var moduleExpandedNodes = expandedNodes && expandedNodes[moduleName];
    if (moduleExpandedNodes) {
      (function deleteChildNodes(parentId) {
        if (!moduleExpandedNodes[parentId]) {
          return;
        }
        var children = Object.keys(moduleExpandedNodes[parentId]);
        for (var i = 0; i < children.length; i++) {
          deleteChildNodes(children[i]);
        }
        delete moduleExpandedNodes[parentId];
      })(parentId);
      localStorage.setItem(this.treeKey, JSON.stringify(expandedNodes));
    }
  }
  
};

var UserDataHelper = {
    isAdmin : function() {
      return this.userLevelId === 1;
    },
    reloadUserData : function() {
      //Ajax to reload user data
    }
};