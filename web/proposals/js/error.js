var autoLogoutWarning = undefined;

$(document).ready(function(){
  $(document).ajaxComplete(function(event, xhr, ajaxOptions) {
    if (xhr.status == 401)
    {
      $.SmartMessageBox({
        title : "<i class='fa fa-exclamation-triangle' style='color:yellow'></i> You have been logged out due to inactivity.",
        content : "You have been logged out due to inactivity. Please login again.",
        buttons : '[OK]'
      }, function(ButtonPressed) {
        if (ButtonPressed == "OK") {
          window.location = "/webdirector";
        }
      });
    }

    var sessionLife = xhr.getResponseHeader("session-lifetime");
    if (sessionLife != undefined)
    {
      var seconds = parseInt(sessionLife);
      var logoutAt = moment().add(seconds, 'seconds');
      
      var warningText = $(".autologoutTime").text(logoutAt.format('h:mm:ss a')).css("color", "inherit");
      if (autoLogoutWarning != undefined)
      {
        clearTimeout(autoLogoutWarning);
      }
      autoLogoutWarning = setTimeout(function(){
        warningText.css("color", "orange");

        autoLogoutWarning = setTimeout(function(){
          warningText.css("color", "red");
        },(2000*60));
      },(seconds*1000)-(2000*60));
    }
    
    if (xhr.status > 0 && xhr.status != 200 && xhr.status != 418)
    {
      console.log("HTTP "+xhr.status+" recieved for url..." + ajaxOptions.url + " with data..."+ajaxOptions.data);
    }
  });
});
