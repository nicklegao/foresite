package test;

import java.net.MalformedURLException;
import java.time.Instant;
import java.util.UUID;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.ebxml.namespaces.messageheader.From;
import org.ebxml.namespaces.messageheader.MessageData;
import org.ebxml.namespaces.messageheader.MessageHeader;
import org.ebxml.namespaces.messageheader.PartyId;
import org.ebxml.namespaces.messageheader.Service;
import org.ebxml.namespaces.messageheader.To;
import org.opentravel.ota._2002._11.SessionCloseRQ;
import org.opentravel.ota._2002._11.SessionCloseRS;
import org.opentravel.ota._2002._11.SessionCreateRQ;
import org.opentravel.ota._2002._11.SessionCreateRS;
import org.xmlsoap.schemas.ws._2002._12.secext.Security;
import org.xmlsoap.schemas.ws._2002._12.secext.Security.UsernameToken;

import com.google.gson.Gson;
import com.sabre.webservices.pnrbuilder.getres.GetReservationPortType;
import com.sabre.webservices.pnrbuilder.getres.GetReservationService;
import com.sabre.webservices.pnrbuilder.v1_19.GetReservationRQ;
import com.sabre.webservices.pnrbuilder.v1_19.GetReservationRS;
import com.sabre.webservices.sabrexml._2003._07.SabreCommandLLSRQ;
import com.sabre.webservices.sabrexml._2003._07.SabreCommandLLSRS;
import com.sabre.webservices.sabrexml._2011._10.EncodeDecodeRQ;
import com.sabre.webservices.sabrexml._2011._10.EncodeDecodeRQ.Decode.Address.CityName;
import com.sabre.webservices.sabrexml._2011._10.EncodeDecodeRS;
import com.sabre.webservices.sabrexml._2011._10.QueueAccessRQ;
import com.sabre.webservices.sabrexml._2011._10.QueueAccessRS;
import com.sabre.webservices.sabrexml._2011._10.QueueCountRQ;
import com.sabre.webservices.sabrexml._2011._10.QueueCountRS;

import https.webservices_sabre_com.websvc.EncodeDecodePortType;
import https.webservices_sabre_com.websvc.EncodeDecodeService;
import https.webservices_sabre_com.websvc.QueueAccessPortType;
import https.webservices_sabre_com.websvc.QueueAccessService;
import https.webservices_sabre_com.websvc.QueueCountPortType;
import https.webservices_sabre_com.websvc.QueueCountService;
import https.webservices_sabre_com.websvc.SabreCommandLLSPortType;
import https.webservices_sabre_com.websvc.SabreCommandLLSService;
import https.webservices_sabre_com.websvc.SessionClosePortType;
import https.webservices_sabre_com.websvc.SessionCloseRQService;
import https.webservices_sabre_com.websvc.SessionCreatePortType;
import https.webservices_sabre_com.websvc.SessionCreateRQService;

public class Test
{
	private static Logger logger = Logger.getLogger(Test.class);
	private static String from = "traveldocs.cloud";
	private static String to = "webservices.sabre.com";

	private String pcc;
	private String userid;
	private String password;
	private String domain;
	private String endpoint;


	private String conversationId;
	private Holder<Security> securityHeader;

	private Test() throws MalformedURLException, DatatypeConfigurationException
	{
//		this("AA", "ZG9J", "415263", "WS493182", "https://sws-crt.cert.havail.sabre.com");
		this("AA", "ZG9J", "415263", "WS493182");
	}

	public Test(String domain, String pcc, String userid, String password) throws MalformedURLException, DatatypeConfigurationException
	{
		this(domain, pcc, userid, password, null);
	}

	public Test(String domain, String pcc, String userid, String password, String endpoint) throws MalformedURLException, DatatypeConfigurationException
	{
		super();
		this.pcc = pcc;
		this.userid = userid;
		this.password = password;
		this.domain = domain;
		this.endpoint = endpoint;
		init();
	}


	private void init() throws MalformedURLException, DatatypeConfigurationException
	{
		conversationId = UUID.randomUUID().toString();
		securityHeader = createSecurityHeader();
		createSession(securityHeader, conversationId);
	}

	public static void main(String[] args) throws DatatypeConfigurationException, MalformedURLException
	{
		System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "99999999");
		new Test().test();

	}

	public void test() throws DatatypeConfigurationException, MalformedURLException
	{
		try
		{
//			countQueue();
//			firstPNRCode("500", "");
//			nextPNRCode(false);
			retrieveItinerary("BGVTRM");
//			checkIata();
			// placeQueue(securityHeader, conversationId, "400", "11");
//		command("SI9");
//		command("QN/500/A-TRAVELDOCS");
		}
		finally
		{
			closeSession();
		}
	}

	private boolean createSession(Holder<Security> securityHeader, String conversationId) throws DatatypeConfigurationException, MalformedURLException
	{
		SessionCreateRQ body = new SessionCreateRQ();

		SessionCreateRQ.POS pos = new SessionCreateRQ.POS();
		SessionCreateRQ.POS.Source source = new SessionCreateRQ.POS.Source();
		source.setPseudoCityCode(pcc);
		pos.setSource(source);

		body.setPOS(pos);
		Holder<MessageHeader> messageHeader = createMessageHeader("SessionCreateRQ", conversationId);
		SessionCreatePortType port = new SessionCreateRQService().getSessionCreatePortType();
		bindEndpoint((BindingProvider) port);
		SessionCreateRS res = port.sessionCreateRQ(messageHeader, securityHeader, body);
		return StringUtils.equalsIgnoreCase("Approved", res.getStatus());
	}

	public boolean closeSession() throws DatatypeConfigurationException
	{
		SessionCloseRQ body = new SessionCloseRQ();
		SessionCloseRQ.POS pos = new SessionCloseRQ.POS();
		SessionCloseRQ.POS.Source source = new SessionCloseRQ.POS.Source();
		source.setPseudoCityCode(pcc);
		pos.setSource(source);
		body.setPOS(pos);
		Holder<MessageHeader> messageHeader = createMessageHeader("SessionCloseRQ", conversationId);
		SessionClosePortType port = new SessionCloseRQService().getSessionClosePortType();
		bindEndpoint((BindingProvider) port);
		SessionCloseRS res = port.sessionCloseRQ(messageHeader, securityHeader, body);
		return StringUtils.equalsIgnoreCase("Approved", res.getStatus());
	}

	private void bindEndpoint(BindingProvider bp)
	{
		if (null != endpoint)
		{
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);
		}
	}

	private Holder<MessageHeader> createMessageHeader(String action, String conversationId) throws DatatypeConfigurationException
	{
		MessageHeader header1 = new MessageHeader();

		header1.setAction(action);
		header1.setCPAId(pcc);

		From fromHeader = new From();
		PartyId fromPartyId = new PartyId();
		fromPartyId.setValue(from);
		fromHeader.getPartyId().add(fromPartyId);
		header1.setFrom(fromHeader);

		To toHeader = new To();
		PartyId toPartyId = new PartyId();
		toPartyId.setValue(to);
		toHeader.getPartyId().add(toPartyId);
		header1.setTo(toHeader);

		Service service = new Service();
		service.setValue(action);
		header1.setService(service);

		MessageData messageData = new MessageData();
		messageData.setMessageId(UUID.randomUUID().toString());

		String timestamp = Instant.now().toString();
		messageData.setTimestamp(timestamp);
		header1.setMessageData(messageData);


		header1.setConversationId(conversationId);

		return new Holder<>(header1);
	}

	private Holder<Security> createSecurityHeader()
	{
		Security security = new Security();
		UsernameToken usernameToken = new UsernameToken();
		usernameToken.setDomain(domain);
		usernameToken.setOrganization(pcc);
		usernameToken.setPassword(password);
		usernameToken.setUsername(userid);
		security.setUsernameToken(usernameToken);
		return new Holder<Security>(security);
	}

	public void retrieveItinerary(String locator) throws DatatypeConfigurationException
	{
		GetReservationRQ body = new GetReservationRQ();
		body.setVersion("1.19.0");
		body.setLocator(locator);
//		body.setRequestType("Stateful");
//		ReturnOptions option = new ReturnOptions();
//		option.setViewName("VaDefaultWithPq");
//		option.setResponseFormat("STL");
//		body.setReturnOptions(option);
		Holder<MessageHeader> messageHeader = createMessageHeader("GetReservationRQ", conversationId);
		GetReservationPortType port = new GetReservationService().getGetReservationPortType();
		bindEndpoint((BindingProvider) port);
		GetReservationRS res = port.getReservationOperation(messageHeader, securityHeader, body);
//		SabreAPIToCoreConverter scc = new SabreAPIToCoreConverter();
//		scc.convertToCore("", res.getReservation());
//		res.getReservation();

		System.out.println(new Gson().toJson(res));
	}

	public void countQueue() throws DatatypeConfigurationException
	{
		QueueCountRQ body = new QueueCountRQ();
		body.setVersion("2.2.1");
		QueueCountRQ.QueueInfo queueInfo = new QueueCountRQ.QueueInfo();
		QueueCountRQ.QueueInfo.QueueIdentifier qid = new QueueCountRQ.QueueInfo.QueueIdentifier();
		qid.setPseudoCityCode(pcc);
		queueInfo.setQueueIdentifier(qid);
		body.setQueueInfo(queueInfo);
		Holder<MessageHeader> messageHeader = createMessageHeader("QueueCountLLSRQ", conversationId);
		QueueCountPortType port = new QueueCountService().getQueueCountPortType();
		bindEndpoint((BindingProvider) port);
		QueueCountRS res = port.queueCountRQ(messageHeader, securityHeader, body);
		System.out.println(res.getQueueInfo().getQueueIdentifier());
	}

	public String firstPNRCode(String queueNumber, String queueName) throws DatatypeConfigurationException
	{
		QueueAccessRQ body = new QueueAccessRQ();
		body.setVersion("2.0.9");
		QueueAccessRQ.QueueIdentifier qid = new QueueAccessRQ.QueueIdentifier();
		qid.setPseudoCityCode(pcc);
		if (StringUtils.isNotBlank(queueNumber))
			qid.setNumber(queueNumber);
		if (StringUtils.isNotBlank(queueName))
			qid.setName(queueName);
		body.setQueueIdentifier(qid);
		Holder<MessageHeader> messageHeader = createMessageHeader("QueueAccessLLSRQ", conversationId);
		QueueAccessPortType port = new QueueAccessService().getQueueAccessPortType();
		bindEndpoint((BindingProvider) port);
		QueueAccessRS res = port.queueAccessRQ(messageHeader, securityHeader, body);
		if (res.getLine() == null || res.getLine().size() == 0)
		{
			return null;
		}
		return res.getLine().get(0).getUniqueID().getID();
	}

	public String nextPNRCode(boolean deleteCurrentPNR) throws DatatypeConfigurationException
	{
		QueueAccessRQ body = new QueueAccessRQ();
		body.setVersion("2.0.9");
		body.setReturnHostCommand(true);
		QueueAccessRQ.Navigation nav = new QueueAccessRQ.Navigation();
		nav.setAction(deleteCurrentPNR ? "QR" : "I");
		body.setNavigation(nav);

		Holder<MessageHeader> messageHeader = createMessageHeader("QueueAccessLLSRQ", conversationId);
		QueueAccessPortType port = new QueueAccessService().getQueueAccessPortType();
		bindEndpoint((BindingProvider) port);
		QueueAccessRS res = port.queueAccessRQ(messageHeader, securityHeader, body);
		if (res.getLine() == null || res.getLine().size() == 0)
		{
			return null;
		}
		return res.getLine().get(0).getUniqueID().getID();
	}

	private void command(String command) throws DatatypeConfigurationException
	{
		SabreCommandLLSRQ body = new SabreCommandLLSRQ();
		body.setVersion("1.8.1");
		SabreCommandLLSRQ.Request request = new SabreCommandLLSRQ.Request();
		request.setHostCommand(command);
		request.setCDATA(true);
		request.setOutput("SCREEN");
		body.setRequest(request);
		Holder<MessageHeader> messageHeader = createMessageHeader("SabreCommandLLSRQ", conversationId);
		SabreCommandLLSPortType port = new SabreCommandLLSService().getSabreCommandLLSPortType();
		bindEndpoint((BindingProvider) port);
		SabreCommandLLSRS res = port.sabreCommandLLSRQ(messageHeader, securityHeader, body);
		System.out.println(res.getResponse());
	}

	public void checkAddressIata(String locationCode) throws DatatypeConfigurationException
	{
		EncodeDecodeRQ body = new EncodeDecodeRQ();
		body.setVersion("2.0.0");
		EncodeDecodeRQ.Decode decode = new EncodeDecodeRQ.Decode();
		EncodeDecodeRQ.Decode.Address address = new EncodeDecodeRQ.Decode.Address();
		CityName cityName = new CityName();
		cityName.setLocationCode(locationCode);
		address.setCityName(cityName);
		decode.setAddress(address);
		body.setDecode(decode);

		Holder<MessageHeader> messageHeader = createMessageHeader("EncodeDecodeLLSRQ", conversationId);
		EncodeDecodePortType port = new EncodeDecodeService().getEncodeDecodePortType();
		bindEndpoint((BindingProvider) port);


		EncodeDecodeRS res = port.encodeDecodeRQ(messageHeader, securityHeader, body);
		System.out.println(res);
	}

	public void checkVehicleIata(String locationCode) throws DatatypeConfigurationException
	{
		EncodeDecodeRQ body = new EncodeDecodeRQ();
		body.setVersion("2.0.0");
		EncodeDecodeRQ.Decode decode = new EncodeDecodeRQ.Decode();
		EncodeDecodeRQ.Decode.Address address = new EncodeDecodeRQ.Decode.Address();
		CityName cityName = new CityName();
		cityName.setLocationCode(locationCode);
		address.setCityName(cityName);
		decode.setAddress(address);
		body.setDecode(decode);

		Holder<MessageHeader> messageHeader = createMessageHeader("EncodeDecodeLLSRQ", conversationId);
		EncodeDecodePortType port = new EncodeDecodeService().getEncodeDecodePortType();
		bindEndpoint((BindingProvider) port);


		EncodeDecodeRS res = port.encodeDecodeRQ(messageHeader, securityHeader, body);
		System.out.println(res);
	}
}
