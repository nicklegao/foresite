
package com.sabre.webservices.pnrbuilder.getres;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Holder;
import com.sabre.webservices.pnrbuilder.v1_19.GetReservationRQ;
import com.sabre.webservices.pnrbuilder.v1_19.GetReservationRS;
import org.ebxml.namespaces.messageheader.MessageHeader;
import org.xmlsoap.schemas.ws._2002._12.secext.Security;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebService(name = "GetReservationPortType", targetNamespace = "http://webservices.sabre.com/pnrbuilder/getres")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    org.xmlsoap.schemas.ws._2002._12.secext.ObjectFactory.class,
    com.sabre.webservices.pnrbuilder.v1_19.ObjectFactory.class,
    com.sabre.services.res.or.v1_14.ObjectFactory.class,
    org.ebxml.namespaces.messageheader.ObjectFactory.class,
    org.w3._1999.xlink.ObjectFactory.class,
    org.w3._2000._09.xmldsig.ObjectFactory.class,
    org.xmlsoap.schemas.soap.envelope.ObjectFactory.class
})
public interface GetReservationPortType {


    /**
     * 
     * @param body
     * @param header2
     * @param header
     * @return
     *     returns com.sabre.webservices.pnrbuilder.v1_19.GetReservationRS
     */
    @WebMethod(operationName = "GetReservationOperation", action = "GetReservationOperation")
    @WebResult(name = "GetReservationRS", targetNamespace = "http://webservices.sabre.com/pnrbuilder/v1_19", partName = "body")
    public GetReservationRS getReservationOperation(
        @WebParam(name = "MessageHeader", targetNamespace = "http://www.ebxml.org/namespaces/messageHeader", header = true, mode = WebParam.Mode.INOUT, partName = "header")
        Holder<MessageHeader> header,
        @WebParam(name = "Security", targetNamespace = "http://schemas.xmlsoap.org/ws/2002/12/secext", header = true, mode = WebParam.Mode.INOUT, partName = "header2")
        Holder<Security> header2,
        @WebParam(name = "GetReservationRQ", targetNamespace = "http://webservices.sabre.com/pnrbuilder/v1_19", partName = "body")
        GetReservationRQ body);

}
