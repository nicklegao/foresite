
package com.sabre.webservices.pnrbuilder.v1_19;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AncillaryServiceIATAIndicatorPartialUpdate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AncillaryServiceIATAIndicatorPartialUpdate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RefundIndicator" type="{http://webservices.sabre.com/pnrbuilder/v1_19}CommonString" minOccurs="0"/>
 *         &lt;element name="CommisionIndicator" type="{http://webservices.sabre.com/pnrbuilder/v1_19}CommonString" minOccurs="0"/>
 *         &lt;element name="InterlineIndicator" type="{http://webservices.sabre.com/pnrbuilder/v1_19}CommonString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AncillaryServiceIATAIndicatorPartialUpdate.PNRB", propOrder = {
    "refundIndicator",
    "commisionIndicator",
    "interlineIndicator"
})
public class AncillaryServiceIATAIndicatorPartialUpdatePNRB {

    @XmlElement(name = "RefundIndicator")
    protected String refundIndicator;
    @XmlElement(name = "CommisionIndicator")
    protected String commisionIndicator;
    @XmlElement(name = "InterlineIndicator")
    protected String interlineIndicator;

    /**
     * Gets the value of the refundIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundIndicator() {
        return refundIndicator;
    }

    /**
     * Sets the value of the refundIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundIndicator(String value) {
        this.refundIndicator = value;
    }

    /**
     * Gets the value of the commisionIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommisionIndicator() {
        return commisionIndicator;
    }

    /**
     * Sets the value of the commisionIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommisionIndicator(String value) {
        this.commisionIndicator = value;
    }

    /**
     * Gets the value of the interlineIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterlineIndicator() {
        return interlineIndicator;
    }

    /**
     * Sets the value of the interlineIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterlineIndicator(String value) {
        this.interlineIndicator = value;
    }

}
