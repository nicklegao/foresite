
package com.sabre.webservices.pnrbuilder.v1_19;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FrequentFlyer.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FrequentFlyer.PNRB">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservices.sabre.com/pnrbuilder/v1_19}Loyalty.PNRB">
 *       &lt;sequence>
 *         &lt;element name="TravelingCarrierCodes" type="{http://webservices.sabre.com/pnrbuilder/v1_19}TravelingCarrierCodes.PNRB" minOccurs="0"/>
 *         &lt;element name="PartnershipAirlineCodes" type="{http://webservices.sabre.com/pnrbuilder/v1_19}PartnershipAirlineCodes" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="RPH" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="NameNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FrequentFlyer.PNRB", propOrder = {
    "travelingCarrierCodes",
    "partnershipAirlineCodes"
})
public class FrequentFlyerPNRB
    extends LoyaltyPNRB
{

    @XmlElement(name = "TravelingCarrierCodes")
    protected TravelingCarrierCodesPNRB travelingCarrierCodes;
    @XmlElement(name = "PartnershipAirlineCodes")
    protected PartnershipAirlineCodes partnershipAirlineCodes;
    @XmlAttribute(name = "RPH")
    protected String rph;
    @XmlAttribute(name = "NameNumber")
    protected String nameNumber;

    /**
     * Gets the value of the travelingCarrierCodes property.
     * 
     * @return
     *     possible object is
     *     {@link TravelingCarrierCodesPNRB }
     *     
     */
    public TravelingCarrierCodesPNRB getTravelingCarrierCodes() {
        return travelingCarrierCodes;
    }

    /**
     * Sets the value of the travelingCarrierCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link TravelingCarrierCodesPNRB }
     *     
     */
    public void setTravelingCarrierCodes(TravelingCarrierCodesPNRB value) {
        this.travelingCarrierCodes = value;
    }

    /**
     * Gets the value of the partnershipAirlineCodes property.
     * 
     * @return
     *     possible object is
     *     {@link PartnershipAirlineCodes }
     *     
     */
    public PartnershipAirlineCodes getPartnershipAirlineCodes() {
        return partnershipAirlineCodes;
    }

    /**
     * Sets the value of the partnershipAirlineCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnershipAirlineCodes }
     *     
     */
    public void setPartnershipAirlineCodes(PartnershipAirlineCodes value) {
        this.partnershipAirlineCodes = value;
    }

    /**
     * Gets the value of the rph property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRPH() {
        return rph;
    }

    /**
     * Sets the value of the rph property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRPH(String value) {
        this.rph = value;
    }

    /**
     * Gets the value of the nameNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameNumber() {
        return nameNumber;
    }

    /**
     * Sets the value of the nameNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameNumber(String value) {
        this.nameNumber = value;
    }

}
