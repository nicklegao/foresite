
package com.sabre.webservices.pnrbuilder.v1_19;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LinearEntryRequestType.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LinearEntryRequestType.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LinearEntry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LinearEntryRequestType.PNRB", propOrder = {
    "linearEntry"
})
public class LinearEntryRequestTypePNRB {

    @XmlElement(name = "LinearEntry", required = true)
    protected String linearEntry;

    /**
     * Gets the value of the linearEntry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinearEntry() {
        return linearEntry;
    }

    /**
     * Sets the value of the linearEntry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinearEntry(String value) {
        this.linearEntry = value;
    }

}
