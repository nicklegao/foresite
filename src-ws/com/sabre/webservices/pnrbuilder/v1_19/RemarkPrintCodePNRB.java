
package com.sabre.webservices.pnrbuilder.v1_19;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RemarkPrintCode.PNRB.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RemarkPrintCode.PNRB">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ITIN"/>
 *     &lt;enumeration value="INV"/>
 *     &lt;enumeration value="PAY"/>
 *     &lt;enumeration value="NAME"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RemarkPrintCode.PNRB")
@XmlEnum
public enum RemarkPrintCodePNRB {

    ITIN,
    INV,
    PAY,
    NAME;

    public String value() {
        return name();
    }

    public static RemarkPrintCodePNRB fromValue(String v) {
        return valueOf(v);
    }

}
