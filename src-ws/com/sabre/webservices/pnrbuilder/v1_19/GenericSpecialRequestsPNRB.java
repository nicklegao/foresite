
package com.sabre.webservices.pnrbuilder.v1_19;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GenericSpecialRequests.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenericSpecialRequests.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GenericSpecialRequest" type="{http://webservices.sabre.com/pnrbuilder/v1_19}GenericSpecialRequest.PNRB" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericSpecialRequests.PNRB", propOrder = {
    "genericSpecialRequest"
})
public class GenericSpecialRequestsPNRB {

    @XmlElement(name = "GenericSpecialRequest")
    protected GenericSpecialRequestPNRB genericSpecialRequest;

    /**
     * Gets the value of the genericSpecialRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GenericSpecialRequestPNRB }
     *     
     */
    public GenericSpecialRequestPNRB getGenericSpecialRequest() {
        return genericSpecialRequest;
    }

    /**
     * Sets the value of the genericSpecialRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenericSpecialRequestPNRB }
     *     
     */
    public void setGenericSpecialRequest(GenericSpecialRequestPNRB value) {
        this.genericSpecialRequest = value;
    }

}
