
package com.sabre.webservices.pnrbuilder.v1_19;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssociationMatrices.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AssociationMatrices.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AssociationMatrix" type="{http://webservices.sabre.com/pnrbuilder/v1_19}AssociationMatrix.PNRB" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssociationMatrices.PNRB", propOrder = {
    "associationMatrix"
})
public class AssociationMatricesPNRB {

    @XmlElement(name = "AssociationMatrix", required = true)
    protected List<AssociationMatrixPNRB> associationMatrix;

    /**
     * Gets the value of the associationMatrix property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the associationMatrix property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssociationMatrix().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssociationMatrixPNRB }
     * 
     * 
     */
    public List<AssociationMatrixPNRB> getAssociationMatrix() {
        if (associationMatrix == null) {
            associationMatrix = new ArrayList<AssociationMatrixPNRB>();
        }
        return this.associationMatrix;
    }

}
