
package com.sabre.webservices.pnrbuilder.v1_19;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Point of Sale (POS) is the details identifying the party or connection channel making the
 *                 request.
 *             
 * 
 * <p>Java class for POS_Type.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="POS_Type.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Source" type="{http://webservices.sabre.com/pnrbuilder/v1_19}SourceType.PNRB" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="AirExtras" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="InhibitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "POS_Type.PNRB", propOrder = {
    "source"
})
public class POSTypePNRB {

    @XmlElement(name = "Source")
    protected SourceTypePNRB source;
    @XmlAttribute(name = "AirExtras")
    protected Boolean airExtras;
    @XmlAttribute(name = "InhibitCode")
    protected String inhibitCode;

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link SourceTypePNRB }
     *     
     */
    public SourceTypePNRB getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link SourceTypePNRB }
     *     
     */
    public void setSource(SourceTypePNRB value) {
        this.source = value;
    }

    /**
     * Gets the value of the airExtras property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAirExtras() {
        return airExtras;
    }

    /**
     * Sets the value of the airExtras property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAirExtras(Boolean value) {
        this.airExtras = value;
    }

    /**
     * Gets the value of the inhibitCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInhibitCode() {
        return inhibitCode;
    }

    /**
     * Sets the value of the inhibitCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInhibitCode(String value) {
        this.inhibitCode = value;
    }

}
