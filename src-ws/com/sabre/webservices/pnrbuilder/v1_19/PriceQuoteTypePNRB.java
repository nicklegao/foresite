
package com.sabre.webservices.pnrbuilder.v1_19;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PriceQuoteType.PNRB.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PriceQuoteType.PNRB">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PQ"/>
 *     &lt;enumeration value="PQR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PriceQuoteType.PNRB")
@XmlEnum
public enum PriceQuoteTypePNRB {

    PQ,
    PQR;

    public String value() {
        return name();
    }

    public static PriceQuoteTypePNRB fromValue(String v) {
        return valueOf(v);
    }

}
