
package com.sabre.webservices.sabrexml._2011._10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Decode">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="Address">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;choice>
 *                             &lt;element name="CityName">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StateCountyProv">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;attribute name="StateCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/choice>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Airline">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="Code" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="CruiseLine">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Equipment">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="AirEquipType" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="TravelAgency">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="PseudoCityCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="UniversalAssociate">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Encode">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="Address">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;choice>
 *                             &lt;element name="CityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StateCountyProv" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/choice>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="AirEquipmentManufacturer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Airline" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Airport" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CruiseLine" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TravelAgency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="UniversalAssociate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *       &lt;attribute name="ReturnHostCommand" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="TimeStamp" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Version" use="required" type="{http://www.w3.org/2001/XMLSchema}string" fixed="2.0.0" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "decode",
    "encode"
})
@XmlRootElement(name = "EncodeDecodeRQ")
public class EncodeDecodeRQ {

    @XmlElement(name = "Decode")
    protected EncodeDecodeRQ.Decode decode;
    @XmlElement(name = "Encode")
    protected EncodeDecodeRQ.Encode encode;
    @XmlAttribute(name = "ReturnHostCommand")
    protected String returnHostCommand;
    @XmlAttribute(name = "TimeStamp")
    protected String timeStamp;
    @XmlAttribute(name = "Version", required = true)
    protected String version;

    /**
     * Gets the value of the decode property.
     * 
     * @return
     *     possible object is
     *     {@link EncodeDecodeRQ.Decode }
     *     
     */
    public EncodeDecodeRQ.Decode getDecode() {
        return decode;
    }

    /**
     * Sets the value of the decode property.
     * 
     * @param value
     *     allowed object is
     *     {@link EncodeDecodeRQ.Decode }
     *     
     */
    public void setDecode(EncodeDecodeRQ.Decode value) {
        this.decode = value;
    }

    /**
     * Gets the value of the encode property.
     * 
     * @return
     *     possible object is
     *     {@link EncodeDecodeRQ.Encode }
     *     
     */
    public EncodeDecodeRQ.Encode getEncode() {
        return encode;
    }

    /**
     * Sets the value of the encode property.
     * 
     * @param value
     *     allowed object is
     *     {@link EncodeDecodeRQ.Encode }
     *     
     */
    public void setEncode(EncodeDecodeRQ.Encode value) {
        this.encode = value;
    }

    /**
     * Gets the value of the returnHostCommand property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnHostCommand() {
        return returnHostCommand;
    }

    /**
     * Sets the value of the returnHostCommand property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnHostCommand(String value) {
        this.returnHostCommand = value;
    }

    /**
     * Gets the value of the timeStamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     * Sets the value of the timeStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeStamp(String value) {
        this.timeStamp = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        if (version == null) {
            return "2.0.0";
        } else {
            return version;
        }
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="Address">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;choice>
     *                   &lt;element name="CityName">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StateCountyProv">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;attribute name="StateCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/choice>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Airline">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;attribute name="Code" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="CruiseLine">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Equipment">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;attribute name="AirEquipType" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="TravelAgency">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;attribute name="PseudoCityCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="UniversalAssociate">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "address",
        "airline",
        "cruiseLine",
        "equipment",
        "travelAgency",
        "universalAssociate"
    })
    public static class Decode {

        @XmlElement(name = "Address")
        protected EncodeDecodeRQ.Decode.Address address;
        @XmlElement(name = "Airline")
        protected EncodeDecodeRQ.Decode.Airline airline;
        @XmlElement(name = "CruiseLine")
        protected EncodeDecodeRQ.Decode.CruiseLine cruiseLine;
        @XmlElement(name = "Equipment")
        protected EncodeDecodeRQ.Decode.Equipment equipment;
        @XmlElement(name = "TravelAgency")
        protected EncodeDecodeRQ.Decode.TravelAgency travelAgency;
        @XmlElement(name = "UniversalAssociate")
        protected EncodeDecodeRQ.Decode.UniversalAssociate universalAssociate;

        /**
         * Gets the value of the address property.
         * 
         * @return
         *     possible object is
         *     {@link EncodeDecodeRQ.Decode.Address }
         *     
         */
        public EncodeDecodeRQ.Decode.Address getAddress() {
            return address;
        }

        /**
         * Sets the value of the address property.
         * 
         * @param value
         *     allowed object is
         *     {@link EncodeDecodeRQ.Decode.Address }
         *     
         */
        public void setAddress(EncodeDecodeRQ.Decode.Address value) {
            this.address = value;
        }

        /**
         * Gets the value of the airline property.
         * 
         * @return
         *     possible object is
         *     {@link EncodeDecodeRQ.Decode.Airline }
         *     
         */
        public EncodeDecodeRQ.Decode.Airline getAirline() {
            return airline;
        }

        /**
         * Sets the value of the airline property.
         * 
         * @param value
         *     allowed object is
         *     {@link EncodeDecodeRQ.Decode.Airline }
         *     
         */
        public void setAirline(EncodeDecodeRQ.Decode.Airline value) {
            this.airline = value;
        }

        /**
         * Gets the value of the cruiseLine property.
         * 
         * @return
         *     possible object is
         *     {@link EncodeDecodeRQ.Decode.CruiseLine }
         *     
         */
        public EncodeDecodeRQ.Decode.CruiseLine getCruiseLine() {
            return cruiseLine;
        }

        /**
         * Sets the value of the cruiseLine property.
         * 
         * @param value
         *     allowed object is
         *     {@link EncodeDecodeRQ.Decode.CruiseLine }
         *     
         */
        public void setCruiseLine(EncodeDecodeRQ.Decode.CruiseLine value) {
            this.cruiseLine = value;
        }

        /**
         * Gets the value of the equipment property.
         * 
         * @return
         *     possible object is
         *     {@link EncodeDecodeRQ.Decode.Equipment }
         *     
         */
        public EncodeDecodeRQ.Decode.Equipment getEquipment() {
            return equipment;
        }

        /**
         * Sets the value of the equipment property.
         * 
         * @param value
         *     allowed object is
         *     {@link EncodeDecodeRQ.Decode.Equipment }
         *     
         */
        public void setEquipment(EncodeDecodeRQ.Decode.Equipment value) {
            this.equipment = value;
        }

        /**
         * Gets the value of the travelAgency property.
         * 
         * @return
         *     possible object is
         *     {@link EncodeDecodeRQ.Decode.TravelAgency }
         *     
         */
        public EncodeDecodeRQ.Decode.TravelAgency getTravelAgency() {
            return travelAgency;
        }

        /**
         * Sets the value of the travelAgency property.
         * 
         * @param value
         *     allowed object is
         *     {@link EncodeDecodeRQ.Decode.TravelAgency }
         *     
         */
        public void setTravelAgency(EncodeDecodeRQ.Decode.TravelAgency value) {
            this.travelAgency = value;
        }

        /**
         * Gets the value of the universalAssociate property.
         * 
         * @return
         *     possible object is
         *     {@link EncodeDecodeRQ.Decode.UniversalAssociate }
         *     
         */
        public EncodeDecodeRQ.Decode.UniversalAssociate getUniversalAssociate() {
            return universalAssociate;
        }

        /**
         * Sets the value of the universalAssociate property.
         * 
         * @param value
         *     allowed object is
         *     {@link EncodeDecodeRQ.Decode.UniversalAssociate }
         *     
         */
        public void setUniversalAssociate(EncodeDecodeRQ.Decode.UniversalAssociate value) {
            this.universalAssociate = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;choice>
         *         &lt;element name="CityName">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StateCountyProv">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;attribute name="StateCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/choice>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cityName",
            "countryCode",
            "stateCountyProv"
        })
        public static class Address {

            @XmlElement(name = "CityName")
            protected EncodeDecodeRQ.Decode.Address.CityName cityName;
            @XmlElement(name = "CountryCode")
            protected String countryCode;
            @XmlElement(name = "StateCountyProv")
            protected EncodeDecodeRQ.Decode.Address.StateCountyProv stateCountyProv;

            /**
             * Gets the value of the cityName property.
             * 
             * @return
             *     possible object is
             *     {@link EncodeDecodeRQ.Decode.Address.CityName }
             *     
             */
            public EncodeDecodeRQ.Decode.Address.CityName getCityName() {
                return cityName;
            }

            /**
             * Sets the value of the cityName property.
             * 
             * @param value
             *     allowed object is
             *     {@link EncodeDecodeRQ.Decode.Address.CityName }
             *     
             */
            public void setCityName(EncodeDecodeRQ.Decode.Address.CityName value) {
                this.cityName = value;
            }

            /**
             * Gets the value of the countryCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryCode() {
                return countryCode;
            }

            /**
             * Sets the value of the countryCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryCode(String value) {
                this.countryCode = value;
            }

            /**
             * Gets the value of the stateCountyProv property.
             * 
             * @return
             *     possible object is
             *     {@link EncodeDecodeRQ.Decode.Address.StateCountyProv }
             *     
             */
            public EncodeDecodeRQ.Decode.Address.StateCountyProv getStateCountyProv() {
                return stateCountyProv;
            }

            /**
             * Sets the value of the stateCountyProv property.
             * 
             * @param value
             *     allowed object is
             *     {@link EncodeDecodeRQ.Decode.Address.StateCountyProv }
             *     
             */
            public void setStateCountyProv(EncodeDecodeRQ.Decode.Address.StateCountyProv value) {
                this.stateCountyProv = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class CityName {

                @XmlAttribute(name = "LocationCode")
                protected String locationCode;

                /**
                 * Gets the value of the locationCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocationCode() {
                    return locationCode;
                }

                /**
                 * Sets the value of the locationCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocationCode(String value) {
                    this.locationCode = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;attribute name="StateCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class StateCountyProv {

                @XmlAttribute(name = "StateCode")
                protected String stateCode;

                /**
                 * Gets the value of the stateCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStateCode() {
                    return stateCode;
                }

                /**
                 * Sets the value of the stateCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStateCode(String value) {
                    this.stateCode = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;attribute name="Code" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Airline {

            @XmlAttribute(name = "Code", required = true)
            protected String code;

            /**
             * Gets the value of the code property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Sets the value of the code property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class CruiseLine {

            @XmlAttribute(name = "Code")
            protected String code;

            /**
             * Gets the value of the code property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Sets the value of the code property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;attribute name="AirEquipType" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Equipment {

            @XmlAttribute(name = "AirEquipType")
            protected String airEquipType;

            /**
             * Gets the value of the airEquipType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAirEquipType() {
                return airEquipType;
            }

            /**
             * Sets the value of the airEquipType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAirEquipType(String value) {
                this.airEquipType = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;attribute name="PseudoCityCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class TravelAgency {

            @XmlAttribute(name = "PseudoCityCode")
            protected String pseudoCityCode;

            /**
             * Gets the value of the pseudoCityCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPseudoCityCode() {
                return pseudoCityCode;
            }

            /**
             * Sets the value of the pseudoCityCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPseudoCityCode(String value) {
                this.pseudoCityCode = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class UniversalAssociate {

            @XmlAttribute(name = "Code")
            protected String code;

            /**
             * Gets the value of the code property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Sets the value of the code property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="Address">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;choice>
     *                   &lt;element name="CityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StateCountyProv" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/choice>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="AirEquipmentManufacturer" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Airline" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Airport" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CruiseLine" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TravelAgency" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="UniversalAssociate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "address",
        "airEquipmentManufacturer",
        "airline",
        "airport",
        "cruiseLine",
        "travelAgency",
        "universalAssociate"
    })
    public static class Encode {

        @XmlElement(name = "Address")
        protected EncodeDecodeRQ.Encode.Address address;
        @XmlElement(name = "AirEquipmentManufacturer")
        protected String airEquipmentManufacturer;
        @XmlElement(name = "Airline")
        protected String airline;
        @XmlElement(name = "Airport")
        protected String airport;
        @XmlElement(name = "CruiseLine")
        protected String cruiseLine;
        @XmlElement(name = "TravelAgency")
        protected String travelAgency;
        @XmlElement(name = "UniversalAssociate")
        protected String universalAssociate;

        /**
         * Gets the value of the address property.
         * 
         * @return
         *     possible object is
         *     {@link EncodeDecodeRQ.Encode.Address }
         *     
         */
        public EncodeDecodeRQ.Encode.Address getAddress() {
            return address;
        }

        /**
         * Sets the value of the address property.
         * 
         * @param value
         *     allowed object is
         *     {@link EncodeDecodeRQ.Encode.Address }
         *     
         */
        public void setAddress(EncodeDecodeRQ.Encode.Address value) {
            this.address = value;
        }

        /**
         * Gets the value of the airEquipmentManufacturer property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAirEquipmentManufacturer() {
            return airEquipmentManufacturer;
        }

        /**
         * Sets the value of the airEquipmentManufacturer property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAirEquipmentManufacturer(String value) {
            this.airEquipmentManufacturer = value;
        }

        /**
         * Gets the value of the airline property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAirline() {
            return airline;
        }

        /**
         * Sets the value of the airline property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAirline(String value) {
            this.airline = value;
        }

        /**
         * Gets the value of the airport property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAirport() {
            return airport;
        }

        /**
         * Sets the value of the airport property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAirport(String value) {
            this.airport = value;
        }

        /**
         * Gets the value of the cruiseLine property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCruiseLine() {
            return cruiseLine;
        }

        /**
         * Sets the value of the cruiseLine property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCruiseLine(String value) {
            this.cruiseLine = value;
        }

        /**
         * Gets the value of the travelAgency property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelAgency() {
            return travelAgency;
        }

        /**
         * Sets the value of the travelAgency property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelAgency(String value) {
            this.travelAgency = value;
        }

        /**
         * Gets the value of the universalAssociate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUniversalAssociate() {
            return universalAssociate;
        }

        /**
         * Sets the value of the universalAssociate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUniversalAssociate(String value) {
            this.universalAssociate = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;choice>
         *         &lt;element name="CityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StateCountyProv" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/choice>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cityName",
            "country",
            "stateCountyProv"
        })
        public static class Address {

            @XmlElement(name = "CityName")
            protected String cityName;
            @XmlElement(name = "Country")
            protected String country;
            @XmlElement(name = "StateCountyProv")
            protected String stateCountyProv;

            /**
             * Gets the value of the cityName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCityName() {
                return cityName;
            }

            /**
             * Sets the value of the cityName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCityName(String value) {
                this.cityName = value;
            }

            /**
             * Gets the value of the country property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountry() {
                return country;
            }

            /**
             * Sets the value of the country property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountry(String value) {
                this.country = value;
            }

            /**
             * Gets the value of the stateCountyProv property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStateCountyProv() {
                return stateCountyProv;
            }

            /**
             * Sets the value of the stateCountyProv property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStateCountyProv(String value) {
                this.stateCountyProv = value;
            }

        }

    }

}
