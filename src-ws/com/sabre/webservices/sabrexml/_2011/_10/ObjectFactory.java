
package com.sabre.webservices.sabrexml._2011._10;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sabre.webservices.sabrexml._2011._10 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sabre.webservices.sabrexml._2011._10
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link VendorCodesRS }
     * 
     */
    public VendorCodesRS createVendorCodesRS() {
        return new VendorCodesRS();
    }

    /**
     * Create an instance of {@link VendorCodesRQ }
     * 
     */
    public VendorCodesRQ createVendorCodesRQ() {
        return new VendorCodesRQ();
    }

    /**
     * Create an instance of {@link VendorCodesRQ.Vendor }
     * 
     */
    public VendorCodesRQ.Vendor createVendorCodesRQVendor() {
        return new VendorCodesRQ.Vendor();
    }

    /**
     * Create an instance of {@link VendorCodesRS.Table }
     * 
     */
    public VendorCodesRS.Table createVendorCodesRSTable() {
        return new VendorCodesRS.Table();
    }

    /**
     * Create an instance of {@link VendorCodesRS.Table.SubTable }
     * 
     */
    public VendorCodesRS.Table.SubTable createVendorCodesRSTableSubTable() {
        return new VendorCodesRS.Table.SubTable();
    }

    /**
     * Create an instance of {@link VendorCodesRS.Table.SubTable.Row }
     * 
     */
    public VendorCodesRS.Table.SubTable.Row createVendorCodesRSTableSubTableRow() {
        return new VendorCodesRS.Table.SubTable.Row();
    }

    /**
     * Create an instance of {@link VendorCodesRS.Table.Row }
     * 
     */
    public VendorCodesRS.Table.Row createVendorCodesRSTableRow() {
        return new VendorCodesRS.Table.Row();
    }

    /**
     * Create an instance of {@link VendorCodesRQ.Vendor.SubType }
     * 
     */
    public VendorCodesRQ.Vendor.SubType createVendorCodesRQVendorSubType() {
        return new VendorCodesRQ.Vendor.SubType();
    }

    /**
     * Create an instance of {@link VendorCodesRS.Table.SubTable.Row.Column }
     * 
     */
    public VendorCodesRS.Table.SubTable.Row.Column createVendorCodesRSTableSubTableRowColumn() {
        return new VendorCodesRS.Table.SubTable.Row.Column();
    }

    /**
     * Create an instance of {@link VendorCodesRS.Table.Row.Column }
     * 
     */
    public VendorCodesRS.Table.Row.Column createVendorCodesRSTableRowColumn() {
        return new VendorCodesRS.Table.Row.Column();
    }

}
