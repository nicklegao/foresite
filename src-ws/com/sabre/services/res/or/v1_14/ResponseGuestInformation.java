
package com.sabre.services.res.or.v1_14;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Added Optional Full Name to Guest Information because sometimes first name/last name is not returned
 * 
 * <p>Java class for ResponseGuestInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseGuestInformation">
 *   &lt;complexContent>
 *     &lt;extension base="{http://services.sabre.com/res/or/v1_14}ExtendedGuestInformation">
 *       &lt;attribute name="FullName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseGuestInformation")
@XmlSeeAlso({
    com.sabre.services.res.or.v1_14.Guests.Guest.class
})
public class ResponseGuestInformation
    extends ExtendedGuestInformation
{

    @XmlAttribute(name = "FullName")
    protected String fullName;

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFullName(String value) {
        this.fullName = value;
    }

}
