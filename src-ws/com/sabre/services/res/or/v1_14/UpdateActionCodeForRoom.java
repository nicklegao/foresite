
package com.sabre.services.res.or.v1_14;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateActionCodeForRoom.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="UpdateActionCodeForRoom">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MODIFY"/>
 *     &lt;enumeration value="CANCEL"/>
 *     &lt;enumeration value="ADD"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "UpdateActionCodeForRoom")
@XmlEnum
public enum UpdateActionCodeForRoom {

    MODIFY,
    CANCEL,
    ADD;

    public String value() {
        return name();
    }

    public static UpdateActionCodeForRoom fromValue(String v) {
        return valueOf(v);
    }

}
