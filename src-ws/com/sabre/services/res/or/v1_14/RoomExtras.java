
package com.sabre.services.res.or.v1_14;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * List of Room Extras
 * 
 * <p>Java class for RoomExtras complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoomExtras">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RoomExtra" type="{http://services.sabre.com/res/or/v1_14}RoomExtra" maxOccurs="3"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoomExtras", propOrder = {
    "roomExtra"
})
public class RoomExtras {

    @XmlElement(name = "RoomExtra", required = true)
    protected List<RoomExtra> roomExtra;

    /**
     * Gets the value of the roomExtra property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the roomExtra property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRoomExtra().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoomExtra }
     * 
     * 
     */
    public List<RoomExtra> getRoomExtra() {
        if (roomExtra == null) {
            roomExtra = new ArrayList<RoomExtra>();
        }
        return this.roomExtra;
    }

}
