
package com.sabre.services.res.or.v1_14;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Additional information includes contact
 * 
 * <p>Java class for ExtendedGuestInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExtendedGuestInformation">
 *   &lt;complexContent>
 *     &lt;extension base="{http://services.sabre.com/res/or/v1_14}BasicGuestInformation">
 *       &lt;sequence>
 *         &lt;element name="Contact" type="{http://services.sabre.com/res/or/v1_14}ContactType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtendedGuestInformation", propOrder = {
    "contact"
})
@XmlSeeAlso({
    ResponseGuestInformation.class
})
public class ExtendedGuestInformation
    extends BasicGuestInformation
{

    @XmlElement(name = "Contact")
    protected ContactType contact;

    /**
     * Gets the value of the contact property.
     * 
     * @return
     *     possible object is
     *     {@link ContactType }
     *     
     */
    public ContactType getContact() {
        return contact;
    }

    /**
     * Sets the value of the contact property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactType }
     *     
     */
    public void setContact(ContactType value) {
        this.contact = value;
    }

}
