
package com.sabre.services.res.or.v1_14;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A List of Rate Plan Inclusions
 * 
 * <p>Java class for RatePlanInclusionsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RatePlanInclusionsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RatePlanInclusionDescription" type="{http://services.sabre.com/res/or/v1_14}RatePlanInclusionDescriptionType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RatePlanInclusionsType", propOrder = {
    "ratePlanInclusionDescription"
})
public class RatePlanInclusionsType {

    @XmlElement(name = "RatePlanInclusionDescription")
    protected List<RatePlanInclusionDescriptionType> ratePlanInclusionDescription;

    /**
     * Gets the value of the ratePlanInclusionDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ratePlanInclusionDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRatePlanInclusionDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RatePlanInclusionDescriptionType }
     * 
     * 
     */
    public List<RatePlanInclusionDescriptionType> getRatePlanInclusionDescription() {
        if (ratePlanInclusionDescription == null) {
            ratePlanInclusionDescription = new ArrayList<RatePlanInclusionDescriptionType>();
        }
        return this.ratePlanInclusionDescription;
    }

}
