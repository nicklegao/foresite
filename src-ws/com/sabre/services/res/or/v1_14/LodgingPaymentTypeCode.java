
package com.sabre.services.res.or.v1_14;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LodgingPaymentTypeCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LodgingPaymentTypeCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DEPOSIT"/>
 *     &lt;enumeration value="GUARANTEE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LodgingPaymentTypeCode")
@XmlEnum
public enum LodgingPaymentTypeCode {

    DEPOSIT,
    GUARANTEE;

    public String value() {
        return name();
    }

    public static LodgingPaymentTypeCode fromValue(String v) {
        return valueOf(v);
    }

}
