
package com.sabre.services.res.or.v1_14;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateActionCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="UpdateActionCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CREATE"/>
 *     &lt;enumeration value="CANCEL"/>
 *     &lt;enumeration value="MODIFY"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "UpdateActionCode")
@XmlEnum
public enum UpdateActionCode {

    CREATE,
    CANCEL,
    MODIFY;

    public String value() {
        return name();
    }

    public static UpdateActionCode fromValue(String v) {
        return valueOf(v);
    }

}
