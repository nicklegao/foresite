
package com.sabre.services.res.or.v1_14;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for BookingInfoRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BookingInfoRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BookingKey" type="{http://services.sabre.com/res/or/v1_14}SabreKeyType" minOccurs="0"/>
 *         &lt;element name="BookingRef" type="{http://services.sabre.com/res/or/v1_14}LodgingBookingSearchRef" minOccurs="0"/>
 *         &lt;element name="CancellationReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HotelReservationIds" type="{http://services.sabre.com/res/or/v1_14}HotelReservationIds" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="RequestorID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CorpDiscount" type="{http://services.sabre.com/res/or/v1_14}MembershipIdType" />
 *       &lt;attribute name="PromotionalCode" type="{http://services.sabre.com/res/or/v1_14}MembershipIdType" />
 *       &lt;attribute name="CreationDate" type="{http://www.w3.org/2001/XMLSchema}date" />
 *       &lt;attribute name="ExistingItinerary" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="InfoSource" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="RateKey" type="{http://services.sabre.com/res/or/v1_14}SabreKeyType" />
 *       &lt;attribute name="TransactionId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="RetransmissionIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="Email" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="FrequentFlyerNumber" type="{http://services.sabre.com/res/or/v1_14}MembershipIdType" />
 *       &lt;attribute name="LoyaltyId" type="{http://services.sabre.com/res/or/v1_14}MembershipIdType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BookingInfoRefType", propOrder = {
    "bookingKey",
    "bookingRef",
    "cancellationReason",
    "hotelReservationIds"
})
public class BookingInfoRefType {

    @XmlElement(name = "BookingKey")
    protected String bookingKey;
    @XmlElement(name = "BookingRef")
    protected LodgingBookingSearchRef bookingRef;
    @XmlElement(name = "CancellationReason")
    protected String cancellationReason;
    @XmlElement(name = "HotelReservationIds")
    protected HotelReservationIds hotelReservationIds;
    @XmlAttribute(name = "Status")
    protected String status;
    @XmlAttribute(name = "RequestorID")
    protected String requestorID;
    @XmlAttribute(name = "CorpDiscount")
    protected String corpDiscount;
    @XmlAttribute(name = "PromotionalCode")
    protected String promotionalCode;
    @XmlAttribute(name = "CreationDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar creationDate;
    @XmlAttribute(name = "ExistingItinerary")
    protected Boolean existingItinerary;
    @XmlAttribute(name = "InfoSource")
    protected String infoSource;
    @XmlAttribute(name = "RateKey")
    protected String rateKey;
    @XmlAttribute(name = "TransactionId")
    protected String transactionId;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "Email")
    protected String email;
    @XmlAttribute(name = "FrequentFlyerNumber")
    protected String frequentFlyerNumber;
    @XmlAttribute(name = "LoyaltyId")
    protected String loyaltyId;

    /**
     * Gets the value of the bookingKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingKey() {
        return bookingKey;
    }

    /**
     * Sets the value of the bookingKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingKey(String value) {
        this.bookingKey = value;
    }

    /**
     * Gets the value of the bookingRef property.
     * 
     * @return
     *     possible object is
     *     {@link LodgingBookingSearchRef }
     *     
     */
    public LodgingBookingSearchRef getBookingRef() {
        return bookingRef;
    }

    /**
     * Sets the value of the bookingRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link LodgingBookingSearchRef }
     *     
     */
    public void setBookingRef(LodgingBookingSearchRef value) {
        this.bookingRef = value;
    }

    /**
     * Gets the value of the cancellationReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancellationReason() {
        return cancellationReason;
    }

    /**
     * Sets the value of the cancellationReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancellationReason(String value) {
        this.cancellationReason = value;
    }

    /**
     * Gets the value of the hotelReservationIds property.
     * 
     * @return
     *     possible object is
     *     {@link HotelReservationIds }
     *     
     */
    public HotelReservationIds getHotelReservationIds() {
        return hotelReservationIds;
    }

    /**
     * Sets the value of the hotelReservationIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelReservationIds }
     *     
     */
    public void setHotelReservationIds(HotelReservationIds value) {
        this.hotelReservationIds = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the requestorID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestorID() {
        return requestorID;
    }

    /**
     * Sets the value of the requestorID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestorID(String value) {
        this.requestorID = value;
    }

    /**
     * Gets the value of the corpDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorpDiscount() {
        return corpDiscount;
    }

    /**
     * Sets the value of the corpDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorpDiscount(String value) {
        this.corpDiscount = value;
    }

    /**
     * Gets the value of the promotionalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromotionalCode() {
        return promotionalCode;
    }

    /**
     * Sets the value of the promotionalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromotionalCode(String value) {
        this.promotionalCode = value;
    }

    /**
     * Gets the value of the creationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the value of the creationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationDate(XMLGregorianCalendar value) {
        this.creationDate = value;
    }

    /**
     * Gets the value of the existingItinerary property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExistingItinerary() {
        return existingItinerary;
    }

    /**
     * Sets the value of the existingItinerary property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExistingItinerary(Boolean value) {
        this.existingItinerary = value;
    }

    /**
     * Gets the value of the infoSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfoSource() {
        return infoSource;
    }

    /**
     * Sets the value of the infoSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfoSource(String value) {
        this.infoSource = value;
    }

    /**
     * Gets the value of the rateKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateKey() {
        return rateKey;
    }

    /**
     * Sets the value of the rateKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateKey(String value) {
        this.rateKey = value;
    }

    /**
     * Gets the value of the transactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

    /**
     * Gets the value of the retransmissionIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Sets the value of the retransmissionIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the frequentFlyerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrequentFlyerNumber() {
        return frequentFlyerNumber;
    }

    /**
     * Sets the value of the frequentFlyerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrequentFlyerNumber(String value) {
        this.frequentFlyerNumber = value;
    }

    /**
     * Gets the value of the loyaltyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoyaltyId() {
        return loyaltyId;
    }

    /**
     * Sets the value of the loyaltyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoyaltyId(String value) {
        this.loyaltyId = value;
    }

}
