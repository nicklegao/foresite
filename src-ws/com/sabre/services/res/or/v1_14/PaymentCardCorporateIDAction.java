
package com.sabre.services.res.or.v1_14;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentCardCorporateIDAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentCardCorporateIDAction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CorporateIDNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="fopElementId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="op" use="required" type="{http://services.sabre.com/res/or/v1_14}OpenReservationOperation" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentCardCorporateIDAction", propOrder = {
    "corporateIDNumber"
})
public class PaymentCardCorporateIDAction {

    @XmlElement(name = "CorporateIDNumber")
    protected String corporateIDNumber;
    @XmlAttribute(name = "fopElementId", required = true)
    protected String fopElementId;
    @XmlAttribute(name = "op", required = true)
    protected OpenReservationOperation op;

    /**
     * Gets the value of the corporateIDNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorporateIDNumber() {
        return corporateIDNumber;
    }

    /**
     * Sets the value of the corporateIDNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorporateIDNumber(String value) {
        this.corporateIDNumber = value;
    }

    /**
     * Gets the value of the fopElementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFopElementId() {
        return fopElementId;
    }

    /**
     * Sets the value of the fopElementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFopElementId(String value) {
        this.fopElementId = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OpenReservationOperation }
     *     
     */
    public OpenReservationOperation getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpenReservationOperation }
     *     
     */
    public void setOp(OpenReservationOperation value) {
        this.op = value;
    }

}
