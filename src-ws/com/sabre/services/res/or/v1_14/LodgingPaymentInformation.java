
package com.sabre.services.res.or.v1_14;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LodgingPaymentInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LodgingPaymentInformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormOfPaymentReference">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="newRef" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="deploymentId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="Type" type="{http://services.sabre.com/res/or/v1_14}LodgingPaymentTypeCode" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LodgingPaymentInformation", propOrder = {
    "formOfPaymentReference"
})
public class LodgingPaymentInformation {

    @XmlElement(name = "FormOfPaymentReference", required = true)
    protected LodgingPaymentInformation.FormOfPaymentReference formOfPaymentReference;
    @XmlAttribute(name = "Type")
    protected LodgingPaymentTypeCode type;

    /**
     * Gets the value of the formOfPaymentReference property.
     * 
     * @return
     *     possible object is
     *     {@link LodgingPaymentInformation.FormOfPaymentReference }
     *     
     */
    public LodgingPaymentInformation.FormOfPaymentReference getFormOfPaymentReference() {
        return formOfPaymentReference;
    }

    /**
     * Sets the value of the formOfPaymentReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link LodgingPaymentInformation.FormOfPaymentReference }
     *     
     */
    public void setFormOfPaymentReference(LodgingPaymentInformation.FormOfPaymentReference value) {
        this.formOfPaymentReference = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link LodgingPaymentTypeCode }
     *     
     */
    public LodgingPaymentTypeCode getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link LodgingPaymentTypeCode }
     *     
     */
    public void setType(LodgingPaymentTypeCode value) {
        this.type = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="newRef" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="deploymentId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class FormOfPaymentReference {

        @XmlAttribute(name = "ref")
        protected String ref;
        @XmlAttribute(name = "newRef")
        protected String newRef;
        @XmlAttribute(name = "deploymentId")
        protected String deploymentId;

        /**
         * Gets the value of the ref property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRef() {
            return ref;
        }

        /**
         * Sets the value of the ref property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRef(String value) {
            this.ref = value;
        }

        /**
         * Gets the value of the newRef property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNewRef() {
            return newRef;
        }

        /**
         * Sets the value of the newRef property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNewRef(String value) {
            this.newRef = value;
        }

        /**
         * Gets the value of the deploymentId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeploymentId() {
            return deploymentId;
        }

        /**
         * Sets the value of the deploymentId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeploymentId(String value) {
            this.deploymentId = value;
        }

    }

}
