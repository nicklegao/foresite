
package com.sabre.services.res.or.v1_14;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PriceBreakDownType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PriceBreakDownType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PassengerType" type="{http://services.sabre.com/res/or/v1_14}PassengerTypeCode"/>
 *         &lt;element name="NumberOfPassengers" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="BasePrice" type="{http://services.sabre.com/res/or/v1_14}DecimalPrice" minOccurs="0"/>
 *         &lt;element name="EquivPrice" type="{http://services.sabre.com/res/or/v1_14}DecimalPrice" minOccurs="0"/>
 *         &lt;element name="Taxes" type="{http://services.sabre.com/res/or/v1_14}TaxesType" minOccurs="0"/>
 *         &lt;element name="Price" type="{http://services.sabre.com/res/or/v1_14}DecimalPrice"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceBreakDownType", propOrder = {
    "passengerType",
    "numberOfPassengers",
    "basePrice",
    "equivPrice",
    "taxes",
    "price"
})
public class PriceBreakDownType {

    @XmlElement(name = "PassengerType", required = true)
    protected PassengerTypeCode passengerType;
    @XmlElement(name = "NumberOfPassengers", defaultValue = "1")
    protected int numberOfPassengers;
    @XmlElement(name = "BasePrice")
    protected DecimalPrice basePrice;
    @XmlElement(name = "EquivPrice")
    protected DecimalPrice equivPrice;
    @XmlElement(name = "Taxes")
    protected TaxesType taxes;
    @XmlElement(name = "Price", required = true)
    protected DecimalPrice price;

    /**
     * Gets the value of the passengerType property.
     * 
     * @return
     *     possible object is
     *     {@link PassengerTypeCode }
     *     
     */
    public PassengerTypeCode getPassengerType() {
        return passengerType;
    }

    /**
     * Sets the value of the passengerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PassengerTypeCode }
     *     
     */
    public void setPassengerType(PassengerTypeCode value) {
        this.passengerType = value;
    }

    /**
     * Gets the value of the numberOfPassengers property.
     * 
     */
    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    /**
     * Sets the value of the numberOfPassengers property.
     * 
     */
    public void setNumberOfPassengers(int value) {
        this.numberOfPassengers = value;
    }

    /**
     * Gets the value of the basePrice property.
     * 
     * @return
     *     possible object is
     *     {@link DecimalPrice }
     *     
     */
    public DecimalPrice getBasePrice() {
        return basePrice;
    }

    /**
     * Sets the value of the basePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link DecimalPrice }
     *     
     */
    public void setBasePrice(DecimalPrice value) {
        this.basePrice = value;
    }

    /**
     * Gets the value of the equivPrice property.
     * 
     * @return
     *     possible object is
     *     {@link DecimalPrice }
     *     
     */
    public DecimalPrice getEquivPrice() {
        return equivPrice;
    }

    /**
     * Sets the value of the equivPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link DecimalPrice }
     *     
     */
    public void setEquivPrice(DecimalPrice value) {
        this.equivPrice = value;
    }

    /**
     * Gets the value of the taxes property.
     * 
     * @return
     *     possible object is
     *     {@link TaxesType }
     *     
     */
    public TaxesType getTaxes() {
        return taxes;
    }

    /**
     * Sets the value of the taxes property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxesType }
     *     
     */
    public void setTaxes(TaxesType value) {
        this.taxes = value;
    }

    /**
     * Gets the value of the price property.
     * 
     * @return
     *     possible object is
     *     {@link DecimalPrice }
     *     
     */
    public DecimalPrice getPrice() {
        return price;
    }

    /**
     * Sets the value of the price property.
     * 
     * @param value
     *     allowed object is
     *     {@link DecimalPrice }
     *     
     */
    public void setPrice(DecimalPrice value) {
        this.price = value;
    }

}
