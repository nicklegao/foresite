
package com.sabre.services.res.or.v1_14;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OpenReservationAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OpenReservationAction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="PaymentCardApprovalAction" type="{http://services.sabre.com/res/or/v1_14}PaymentCardApprovalAction" minOccurs="0"/>
 *           &lt;element name="PaymentCardCorporateIDAction" type="{http://services.sabre.com/res/or/v1_14}PaymentCardCorporateIDAction" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpenReservationAction", propOrder = {
    "paymentCardApprovalAction",
    "paymentCardCorporateIDAction"
})
public class OpenReservationAction {

    @XmlElement(name = "PaymentCardApprovalAction")
    protected PaymentCardApprovalAction paymentCardApprovalAction;
    @XmlElement(name = "PaymentCardCorporateIDAction")
    protected PaymentCardCorporateIDAction paymentCardCorporateIDAction;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "name")
    protected String name;

    /**
     * Gets the value of the paymentCardApprovalAction property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentCardApprovalAction }
     *     
     */
    public PaymentCardApprovalAction getPaymentCardApprovalAction() {
        return paymentCardApprovalAction;
    }

    /**
     * Sets the value of the paymentCardApprovalAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentCardApprovalAction }
     *     
     */
    public void setPaymentCardApprovalAction(PaymentCardApprovalAction value) {
        this.paymentCardApprovalAction = value;
    }

    /**
     * Gets the value of the paymentCardCorporateIDAction property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentCardCorporateIDAction }
     *     
     */
    public PaymentCardCorporateIDAction getPaymentCardCorporateIDAction() {
        return paymentCardCorporateIDAction;
    }

    /**
     * Sets the value of the paymentCardCorporateIDAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentCardCorporateIDAction }
     *     
     */
    public void setPaymentCardCorporateIDAction(PaymentCardCorporateIDAction value) {
        this.paymentCardCorporateIDAction = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

}
