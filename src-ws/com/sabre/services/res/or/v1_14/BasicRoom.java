
package com.sabre.services.res.or.v1_14;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Represent a Basic Room Which Can be used in Request/Response
 * 
 * <p>Java class for BasicRoom complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BasicRoom">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="NonSmoking" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="BedTypeCode" type="{http://services.sabre.com/res/or/v1_14}OTACodeType" />
 *       &lt;attribute name="AccessibilityInformation" type="{http://services.sabre.com/res/or/v1_14}OTACodeType" />
 *       &lt;attribute name="RoomIndex" type="{http://services.sabre.com/res/or/v1_14}IndexType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BasicRoom")
@XmlSeeAlso({
    RoomInformation.class
})
public class BasicRoom {

    @XmlAttribute(name = "NonSmoking")
    protected Boolean nonSmoking;
    @XmlAttribute(name = "BedTypeCode")
    protected Integer bedTypeCode;
    @XmlAttribute(name = "AccessibilityInformation")
    protected Integer accessibilityInformation;
    @XmlAttribute(name = "RoomIndex")
    protected Integer roomIndex;

    /**
     * Gets the value of the nonSmoking property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isNonSmoking() {
        if (nonSmoking == null) {
            return false;
        } else {
            return nonSmoking;
        }
    }

    /**
     * Sets the value of the nonSmoking property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNonSmoking(Boolean value) {
        this.nonSmoking = value;
    }

    /**
     * Gets the value of the bedTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBedTypeCode() {
        return bedTypeCode;
    }

    /**
     * Sets the value of the bedTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBedTypeCode(Integer value) {
        this.bedTypeCode = value;
    }

    /**
     * Gets the value of the accessibilityInformation property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAccessibilityInformation() {
        return accessibilityInformation;
    }

    /**
     * Sets the value of the accessibilityInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAccessibilityInformation(Integer value) {
        this.accessibilityInformation = value;
    }

    /**
     * Gets the value of the roomIndex property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRoomIndex() {
        return roomIndex;
    }

    /**
     * Sets the value of the roomIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRoomIndex(Integer value) {
        this.roomIndex = value;
    }

}
