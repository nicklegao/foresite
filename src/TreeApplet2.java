//package webdirector.tree;

import javax.swing.*;
import javax.swing.plaf.metal.MetalIconFactory;
import java.awt.event.*;
import java.awt.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import java.util.*;
import java.net.*;
import java.io.*;
import java.net.URL;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;

/**
 * <APPLET code="TreeApplet2.class" width=500 height=400></APPLET>
 **/

public class TreeApplet2 extends JApplet
		implements ActionListener, TreeSelectionListener, TreeExpansionListener
{
	private JTree tree;
	private DefaultTreeModel treeModel;

	protected DefaultMutableTreeNode root;
	protected static int folderLvl = 0;
	// Hash of tree nodes using category_id as the index
	private Hashtable treeNodes = new Hashtable(10);
	private Hashtable leafNodes = new Hashtable(10);
	private String str = "nothing";
	JPopupMenu popup;
	TreePath selPath;
	private DefaultMutableTreeNode rightClickNode;
	public String module;
	private String war;
	public int moduleLevels = 0;
	private String moduleLevel;
	public boolean isExportable = false;
	private double popupHeight = 157;
	private double popupWidth = 78;

	public void init()
	{
		//loadModule("PRODUCTS","2","newCategory","SBE","0");
	}

	public void update()
	{
		System.out.println("@@@@@@@@@@@@@@@@@@Done@@@@@@@@@@@@@2");
	}

	public void updatePopupMenu()
	{
		createNewPopupMenu();
	}

	private void createNewPopupMenu()
	{
		//Create the popup menu.
		popup = new JPopupMenu();

		JMenuItem menuItem = new JMenuItem("Insert");
		menuItem.addActionListener(this);
		popup.add(menuItem);

		menuItem = new JMenuItem("Edit");
		menuItem.addActionListener(this);
		popup.add(menuItem);

		menuItem = new JMenuItem("Delete");
		menuItem.addActionListener(this);
		popup.add(menuItem);

		menuItem = new JMenuItem("Sort");
		menuItem.addActionListener(this);
		popup.add(menuItem);

		menuItem = new JMenuItem("Move");
		menuItem.addActionListener(this);
		popup.add(menuItem);

		menuItem = new JMenuItem("Copy");
		menuItem.addActionListener(this);
		popup.add(menuItem);

		/**
		 * if you add more menu than change size of popupMenu using popupHeight
		 * and popupWidth variable
		 */

		if (isExportable)
		{
			menuItem = new JMenuItem("Export");
			menuItem.addActionListener(this);
			popup.add(menuItem);
		}

		menuItem = new JMenuItem("Refresh");
		menuItem.addActionListener(this);
		popup.add(menuItem);

		popup.setLabel("");
	}

	public TreeApplet2()
	{

		try
		{
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		}
		catch (ClassNotFoundException e)
		{
			str = "e";
		}
		catch (InstantiationException i)
		{
			str = "i";
		}
		catch (IllegalAccessException f)
		{
			str = "f";
		}
		catch (UnsupportedLookAndFeelException b)
		{
			str = "b";
		}
		JMenuItem item;
		JFrame jf = new JFrame();
		//create a window adapter that shuts things down when x is selected
		jf.addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				System.exit(0);
			}
		});
		//set up a file menu with one submenu and an exit menuItem
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		// JMenu menu = new JMenu("Folder List");
		// menuBar.add(menu);
		menuBar.invalidate();
		// create the popup
		createNewPopupMenu();
		//add a sample Jtree in a scroll pane that includes a sample JTree
		Stack stack = new Stack();
		DefaultMutableTreeNode top = null;
		initTree();
	}

	private void initTree()
	{
		root = new DefaultMutableTreeNode("Please select a module");
		treeModel = new DefaultTreeModel(root, true);

		tree = new JTree(treeModel);

		tree.setCellRenderer(new treeRender());
		tree.setShowsRootHandles(true);
		tree.putClientProperty("JTree.lineStyle", "Angled");
		tree.setEditable(false);
		//The tree allows one node selection at a time.
		tree.getSelectionModel().setSelectionMode
				(TreeSelectionModel.SINGLE_TREE_SELECTION);
		//Listen for when the selection changes.
		tree.addTreeSelectionListener(this);
		//put tree in a scroll pane and add it to the frames content pane
		JScrollPane sourceScrollPane = new JScrollPane(tree);
		getContentPane().add(sourceScrollPane);

		// Add listener to components that can bring up popup menus.
		MouseListener popupListener = new PopupListener();
		tree.addMouseListener(popupListener);
		tree.addTreeExpansionListener(this);
	}

	public InputStream servletCall(String cmd)
	{
		return servletCall(cmd, "0");
	}

	public InputStream servletCall(String cmd, String parentCatId)
	{
		System.out.println("servletCall cmd=" + cmd + " module" + module);
		StringBuffer sb = new StringBuffer();
		URLConnection urlConn = null;
		String S_url = getCodeBase() + "../treeServlet?cmd=" + cmd + "&module=" + module + "&pCatID=" + parentCatId;
		;
		//String S_url = "http://localhost:8080/SBE/treeServlet?cmd="+cmd+"&module="+module+"&pCatID="+parentCatId;
		//String S_url="http://www.corporateinteractive.com.au/treeServlet?cmd="+cmd+"&module="+module;
		try
		{
			URL url = new URL(S_url);
			System.out.println("trying to openConnection to this URL");
			urlConn = url.openConnection();
			System.out.println("urlConn has been set ... about to return urlConn.getInputStream()");
			return (urlConn.getInputStream());
		}
		catch (MalformedURLException e)
		{
			System.out.println("MalformedURLException " + e.getMessage());
		}
		catch (IOException i)
		{
			System.out.println("IOException " + i.getMessage() + " when calling '" + S_url + "'");
		}
		return (null);
	}

	public void clearTree()
	{
		System.out.println("CLEARING TREE DOWN: V5.1 ");
		// called when switching modules ... clear down existing contents if any.
		root.removeAllChildren();
		treeModel.reload();
		treeNodes = new Hashtable(10);
		leafNodes = new Hashtable(10);
	}

	int[] selectedRows;

	public void getRowSelected()
	{
		selectedRows = tree.getSelectionRows();
		System.out.println(">>> size " + selectedRows.length);
		System.out.println(">>> size1 " + selectedRows[0]);
	}

	public void setRowSelected()
	{
		System.out.println(">>>>>> sel " + selectedRows[0]);
		tree.expandRow(0);
		if (selectedRows != null && selectedRows.length != 0)
			tree.expandRow(selectedRows[0]);
	}

	public void removeNodeFromTree(String element_id, String nodeType)
	{
		System.out.println("nodeType " + nodeType);
		System.out.println("element_id " + element_id);
		if (nodeType.equals("Category"))
		{
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeNodes.get(element_id);
			treeModel.removeNodeFromParent(node);
			treeNodes.remove(element_id);
		}
		else
		{
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) leafNodes.get(element_id);
			treeModel.removeNodeFromParent(node);
			leafNodes.remove(element_id);
		}
	}

	public void moveNodeInTree(String element_id, String newCategory, String nodeType)
	{
		if (nodeType.equals("Category"))
		{
			System.out.println("Not really sure what to do for Categories ... shouldn't be here!");
		}
		else
		{
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) leafNodes.get(element_id);
			NodeInfo nodeInfo = (NodeInfo) node.getUserObject();
			MutableTreeNode p = (MutableTreeNode) node.getParent();
			MutableTreeNode newParent = (MutableTreeNode) treeNodes.get(newCategory);

			// take node out of existing position
			treeModel.removeNodeFromParent(node);

			// put the node into the tree model object into the correct place
			DefaultMutableTreeNode n = addObject((DefaultMutableTreeNode) treeNodes.get(newCategory), nodeInfo, false);

			// store the new leaf in the hashtable for future retrieval
			leafNodes.put(element_id, n);
		}
	}

	public void copyNodeInTree(String elementName, String elementID, String newCategory, String nodeType)
	{
		if (nodeType.equals("Category"))
		{
			System.out.println("Not really sure what to do for Categories ... shouldn't be here!");
		}
		else
		{
			// create the new leaf (element) node
			NodeInfo ni = new NodeInfo(elementName, elementID, "LEAF", newCategory);

			// adds it into the tree structure itself and returns the new node
			DefaultMutableTreeNode node = addObject((DefaultMutableTreeNode) treeNodes.get(newCategory), ni, false);

			// store the new leaf in the hashtable for future retrieval
			leafNodes.put(elementID, node);
		}
	}

	/**
	 * Adds a leaf (element) node to the tree dynamically. Avoids a refresh of
	 * the whole tree.
	 * 
	 * @param element_id
	 * @param parentCategoryId
	 * @param elementName
	 */
	public void insertNewNode(String element_id, String parentCategoryId, String elementName)
	{
		System.out.println("ADDING new node v7");

		NodeInfo ni = new NodeInfo(elementName, element_id, "LEAF", parentCategoryId);

		DefaultMutableTreeNode node = addObject((DefaultMutableTreeNode) treeNodes.get(parentCategoryId), ni, false);
		leafNodes.put(element_id, node);
	}

	public void insertNewCategory(String element_id, String parentCategoryId, String elementName, String level)
	{
		System.out.println("ADDING new category v4");

		// need to work out what level we're inserting at, get parent level and add one
		int insertLevel = 1;
		if (!parentCategoryId.equals("0"))
		{
			DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) treeNodes.get(parentCategoryId);
			NodeInfo nodeInfo = (NodeInfo) parentNode.getUserObject();
			insertLevel = Integer.valueOf(nodeInfo.level);
			insertLevel++;
		}

		NodeInfo ni = new NodeInfo(elementName, element_id, String.valueOf(insertLevel), parentCategoryId);

		DefaultMutableTreeNode node = addObject((DefaultMutableTreeNode) treeNodes.get(parentCategoryId), ni, false);
		treeNodes.put(element_id, node);
	}

	public void renameLabelForTreeNode(String id, String nodeType, String newLabel)
	{
		System.out.println("Calling renameLabelForTreeNode");
		DefaultMutableTreeNode node = null;
		if (nodeType.equals("Category"))
		{
			node = (DefaultMutableTreeNode) treeNodes.get(id);
		}
		else if (nodeType.equals("Element"))
		{
			node = (DefaultMutableTreeNode) leafNodes.get(id);
		}
		NodeInfo nodeInfo = (NodeInfo) node.getUserObject();
		nodeInfo.setNodeLabel(newLabel);
		System.out.println("End Calling renameLabelForTreeNode");
		treeModel.nodeChanged(node);
	}

	public void loadModule(String m, String levels, String changeType, String warName)
	{
		loadModule(m, levels, changeType, warName, "0");
	}

	public void loadModule(String m, String levels, String changeType, String warName, String parentId)
	{
		loadModule(m, levels, changeType, warName, parentId, true);
	}

	private void loadModule(String m, String levels, String changeType, String warName, String parentId, boolean setModuleLevel)
	{
		System.out.println("Sanjiv Loading module v6");
		war = warName;
		if (setModuleLevel)
		{
			this.moduleLevel = levels;
		}
		boolean newModule = false;
		boolean newElement = false;
		boolean newCategory = false;
		if (changeType.equals("newElement"))
			newElement = true;
		if (changeType.equals("newCategory"))
			newCategory = true;
		// are we refreshing or changing modules ?
		if (m.equals(module))
			newModule = false;
		else
		{
			// switching to a new module ... potentially from none selected
			newModule = true;
			module = m;
			moduleLevels = Integer.parseInt(levels);
		}
		// get all products
		Vector prodVector = new Vector();//getAllProducts();
		// get all folders (all levels)
		int urlRetry = 0;
		InputStream xml = servletCall("getAll", parentId);
		while (urlRetry < 5 && xml == null)
		{
			System.out.println("URL connect failed, retry(" + urlRetry + ") ...");
			xml = servletCall("getAll", parentId);
			urlRetry++;
		}
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
//            builder.setEntityResolver(new MyEntityResolver());
			Document doc = builder.parse(xml);
			Vector v = getAllFolders("entry", doc);
			renderTree(v, newModule, newElement, newCategory, prodVector);
		}
		catch (ParserConfigurationException e)
		{
			System.out.println("Parsing of ctmediastore.xml error");
			e.printStackTrace();
		}
		catch (SAXException ex)
		{
			System.out.println("SAX exception in ctmediastore.xml");
			ex.printStackTrace();
		}
		catch (IOException i)
		{
			System.out.println("IO exception when reading ctmediastore.xml");
			i.printStackTrace();
		}

	}

	private Vector getAllLeafs(String nodeName, Document doc)
	{
		String propVal = null;
		NodeList nlist = doc.getElementsByTagName(nodeName);
		Vector v = new Vector(100);
		for (int i = 0; i < nlist.getLength(); i++)
		{
			String[] cols = new String[3];
			Node n = nlist.item(i);
			NamedNodeMap attr = n.getAttributes();
			Node attrNode = attr.getNamedItem("name");
			cols[0] = attrNode.getNodeValue();
			attrNode = attr.getNamedItem("id");
			cols[1] = attrNode.getNodeValue();
			attrNode = attr.getNamedItem("c");
			cols[2] = attrNode.getNodeValue();
			v.insertElementAt(cols, i);
		}
		return (v);
	}

	private Vector getAllFolders(String nodeName, Document doc)
	{
		NodeList nlist = doc.getElementsByTagName(nodeName);
		Vector v = new Vector(10);
		for (int i = 0; i < nlist.getLength(); i++)
		{
			String[] cols = new String[5];
			Node n = nlist.item(i);
			NamedNodeMap attr = n.getAttributes();

			Node attrNode = attr.getNamedItem("name");
			cols[0] = attrNode.getNodeValue();
			attrNode = attr.getNamedItem("id");
			cols[1] = attrNode.getNodeValue();
			attrNode = attr.getNamedItem("parent");
			cols[2] = attrNode.getNodeValue();
			attrNode = attr.getNamedItem("level");
			cols[3] = attrNode.getNodeValue();
			attrNode = attr.getNamedItem("childCount");
			cols[4] = attrNode.getNodeValue();

			v.insertElementAt(cols, i);
		}
		return (v);
	}

	public void setSize(int width, int height)
	{
		super.setSize(new Dimension(width, height));
		System.exit(1);
		validate();
	}

	public DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent,
			Object child,
			boolean shouldBeVisible)
	{
		NodeInfo ni = (NodeInfo) child;
		DefaultMutableTreeNode childNode;
		System.out.println("Adding name " + ni.getNodeLabel() + " which has children = " + ni.getChildCount());
		if ("LEAF".equalsIgnoreCase(ni.level) || ni.getChildCount() == 0)
		{
			childNode =
					new DefaultMutableTreeNode(child, false);
		}
		else
		{
			childNode =
					new DefaultMutableTreeNode(child);
		}


		if (parent == null)
		{
			parent = root;
		}

		System.out.println("Is Leaf : " + treeModel.isLeaf(childNode));
		parent.setAllowsChildren(true);
		treeModel.insertNodeInto(childNode, parent,
				parent.getChildCount());


		// Make sure the user can see the lovely new node.
		if (shouldBeVisible)
		{
			tree.scrollPathToVisible(new TreePath(childNode.getPath()));
		}
		return childNode;
	}

	private String getParentID(DefaultMutableTreeNode child)
	{
		DefaultMutableTreeNode p = (DefaultMutableTreeNode) child.getParent();
		// working on root node itself... this isn't normal ?
		if (p == null)
			return ("0");
		else
		{
			p.getUserObject();
			NodeInfo n = null;
			Object ni = p.getUserObject();
//System.out.println(ni.getClass().getName());
			// top level nodes will return ROOT node as parent which doesn't have a NodeInfo object in it
			if (ni.getClass().getName().equals("java.lang.String"))
				return ("0");

			n = (NodeInfo) ni;

			return (n.id);
		}
	}

//  Required by TreeExpansionListener interface.
	public void treeExpanded(TreeExpansionEvent e)
	{
		System.out.println("Tree-expanded event detected");
		System.out.println(e.getPath().getLastPathComponent().getClass().getName());
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) (e.getPath().getLastPathComponent());
		System.out.println("Tree expanded event: " + node);
		if (node != null)
		{
			// find parent node
			Object obj = node.getUserObject();

			if (obj.getClass().getName().equals("java.lang.String"))
			{
			}
			else
			{
				NodeInfo ni = (NodeInfo) node.getUserObject();
				System.out.println("Level: " + ni.level);
				System.out.println("Done: " + ni.getNodeLabel());
				System.out.println("ID: " + ni.id);
				System.out.println("Module Level: " + moduleLevel);
				if (moduleLevel.equalsIgnoreCase(ni.level))
				{
					System.out.println("$$$$$$$$ Inside Leaf $$$$$$$$$");
					renderLeafNodes(ni.id, ni.level, node, new Vector());
				}
				else
				{
					String nodeLabel = ni.getNodeLabel();
					ni.setNodeLabel(nodeLabel + " (Loading...)");
					System.out.println("End Calling renameLabelForTreeNode");
					treeModel.nodeChanged(node);

					loadModule(module, ni.level, ni.getNodeLabel(), war, ni.id, false);
					ni.setNodeLabel(nodeLabel);
				}
			}
		}
	}

	public void actionPerformed(ActionEvent e)
	{
		JMenuItem source = (JMenuItem) (e.getSource());
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
		System.out.println("actionPerformed: ummm");
		if (node != null)
		{
			// find parent node
			String parentID = "0"; // getParentID(node);

			NodeInfo n = null;
			Object ni = node.getUserObject();

			if (ni.getClass().getName().equals("java.lang.String"))
			{
				// root node selected and performing right click
				System.out.println("1. NODE RIGHT CLICKED : action(ROOT, 1, ROOT, " + source.getText());
				// the only valid menu option here is insert
				if (source.getText().equals("Insert"))
				{
					NodeInfo rootN = new NodeInfo("ROOT", "0", "0", "0");
					processMenuClick(rootN, "0", source.getText());
				}
			}
			else
			{
				n = (NodeInfo) ni;
				System.out.println(">>>>>>>>>>>>> node clicked on " + n.level);
				processMenuClick(n, parentID, source.getText());
			}
		}
		else
		{
			System.out.println("2. NODE RIGHT CLICKED : action(ROOT, 1, ROOT, " + source.getText());
		}

		if (e.getActionCommand().equals("Exit"))
			System.exit(0);
	}

	private void processMenuClick(NodeInfo ni, String parentID, String itemSelected)
	{
		String nodeLevel = ni.level;
		String nodeLabel = ni.nodeLabel;
		String nodeID = ni.id;

		String jsp = "/" + war + "/jsp/";
		boolean insertOK = true;

		if (nodeLevel.equals("LEAF"))
			insertOK = false;
		// special case for inserts when not already on the leaf
		int x = 0;
		System.out.println("nodeLevel " + nodeLevel);
		if (itemSelected.equals("Insert") && !nodeLevel.equals("LEAF"))
		{
			x = Integer.parseInt(nodeLevel) + 1;
			if (x > getModuleLevels())
				nodeLevel = "LEAF";
			else
				nodeLevel = Integer.toString(x);
		}
		System.out.println("nodeLevel " + nodeLevel);

		System.out.println("NODE RIGHT CLICKED : action(" + nodeLabel + ", " + nodeLevel + ", " + nodeID + ", "
				+ parentID + ", " + itemSelected + ", " + ni.parentID + ")");

		if (itemSelected.equals("Refresh"))
		{
			clearTree();
			loadModule(module, String.valueOf(moduleLevels), "none", war, "0", false);
		}
		if (!nodeLevel.equals("LEAF") && itemSelected.equals("Export") && isExportable)
		{
			callURL(jsp + "csv_export_fields.jsp?module=" + module + "&categoryId=" + nodeID, "body");
		}
		else if (nodeLevel.equals("LEAF") && itemSelected.equals("Export") && isExportable)
		{
			callURL(jsp + "csv_export_fields.jsp?module=" + module + "&elementId=" + nodeID, "body");
		}
		else if (nodeLevel.equals("LEAF") && itemSelected.equals("Edit"))
		{
			// editing a element
			callURL(jsp + "dynamicFormBuilder.jsp?elementID=" + nodeID, "body");
		}
		else if (nodeLevel.equals("LEAF") && itemSelected.equals("Insert"))
		{
			// inserting a new element hence id = 0
			if (insertOK)
				callURL(jsp + "dynamicFormBuilder.jsp?elementID=0&parentID=" + nodeID, "body");
		}
		else if (nodeLevel.equals("LEAF") && itemSelected.equals("Delete"))
		{
			// Deleting an element
			callURL(jsp + "dynamicFormBuilderDelete.jsp?elementID=" + nodeID, "body");
		}
		else if (!nodeLevel.equals("LEAF") && itemSelected.equals("Insert"))
		{
			// inserting a new category
			callURL(jsp + "dynamicFormBuilder_category.jsp?elementID=0&level=" + nodeLevel + "&parentID=" + nodeID, "body");
		}
		else if (!nodeLevel.equals("LEAF") && itemSelected.equals("Edit"))
		{
			// editing a category
			callURL(jsp + "dynamicFormBuilder_category.jsp?elementID=" + nodeID, "body");
		}
		else if (!nodeLevel.equals("LEAF") && itemSelected.equals("Delete"))
		{
			// deleting a category
			callURL(jsp + "dynamicFormBuilder_categoryDelete.jsp?elementID=" + nodeID, "body");
		}
		else if (nodeLevel.equals("LEAF") && itemSelected.equals("Move"))
		{
			// deleting a category
			callURL(jsp + "moveAsset.jsp?elementID=" + nodeID, "body");
		}
		else if (nodeLevel.equals("LEAF") && itemSelected.equals("Copy"))
		{
			// deleting a category
			callURL(jsp + "copyAsset.jsp?elementID=" + nodeID, "body");
		}
		else if (nodeLevel.equals("LEAF") && itemSelected.equals("Sort"))
		{
			// Sorting assets under a category
			callURL(jsp + "categorySummaryOrder.jsp?categoryID=" + ni.parentID, "body");
		}
		else if (!nodeLevel.equals("LEAF") && itemSelected.equals("Sort"))
		{
			// sorting categories
			callURL(jsp + "categoryOrder.jsp?parentCategory_id=" + ni.parentID, "body");
		}
	}

	private void callURL(String url, String frameName)
	{
		URL link = null;
		try
		{
			link = new URL(getCodeBase(), url);
			getAppletContext().showDocument(link, frameName);
		}
		catch (MalformedURLException ex)
		{
			showStatus("Bad URL:" + link.toString());
		}
	}

	public void valueChanged(TreeSelectionEvent e)
	{
		tree.removeTreeSelectionListener(this); //don't want to call myself
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
		DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) treeModel.getRoot();
		String text = "";
		System.out.println("valueChanged: Something happened");
		if (node == null)
		{
			rootNode.setUserObject(text);
		}
		else
		{
			NodeInfo n = null;
			Object ni = node.getUserObject();
			if (ni.getClass().getName().equals("java.lang.String"))
			{
				rootNode.setUserObject(text);
			}
			else
			{
				n = (NodeInfo) ni;
				// Have we selected a bottom level node
				if (n.level.equals("LEAF"))
				{
					// yes we have so get the id and then invoke the frame on the right to edit this leaf
					System.out.println("LEAF LEVEL ...");
					String link = "/" + war + "/jsp/dynamicFormBuilder.jsp?elementID=" + n.id;
					callURL(link, "body");
				}

				// have we selected on level above the leaf node ...
				int x = getModuleLevels();
				if (n.level.equals(Integer.toString(x)))
				{
					// at the lowest category level
					System.out.println("You just clicked a lowest level category ... you fool !");
					String link = "/" + war + "/jsp/categorySummary.jsp?categoryID=" + n.id;
					callURL(link, "body");
				}


				if (n == null)
					System.out.println("n == null !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				else
				{
					rootNode.setUserObject(text);
				}
			}
		}
		treeModel.nodeChanged(rootNode);
		tree.addTreeSelectionListener(this); //done changing, listen again
	}

	private void renderTree(Vector v, boolean newModule, boolean newElement, boolean newCategory, Vector prodVector)
	{
		DefaultMutableTreeNode[] nodes = new DefaultMutableTreeNode[v.size()];
		for (int i = 0; i < v.size(); i++)
		{
			String[] resultItem = (String[]) v.elementAt(i);
			String category_name = resultItem[0];
			String category_id = resultItem[1];
			String parent_id = resultItem[2];
			String folderLevel = resultItem[3];
			String cCount = resultItem[4];

			folderLvl = Integer.parseInt(folderLevel);

			NodeInfo ni = new NodeInfo(category_name, category_id, folderLevel, parent_id, Integer.parseInt(cCount));

			if (parent_id.equals("0"))
			{
				if (!newModule && nodeAlreadyExists(ni))
				{
					System.out.println("skipping node which already exists");
					nodes[i] = (DefaultMutableTreeNode) treeNodes.get(category_id);
				}
				else
				{
					nodes[i] = addObject(root, ni, false);
					treeNodes.put(category_id, nodes[i]);
				}
				//renderChildren(nodes[i], category_id, folderLevel, prodVector );
			}
			else
			{
				if (!newModule && nodeAlreadyExists(ni))
				{
					nodes[i] = (DefaultMutableTreeNode) treeNodes.get(category_id);
				}
				else
				{
					nodes[i] = addObject((DefaultMutableTreeNode) treeNodes.get(parent_id), ni, false);
					treeNodes.put(category_id, nodes[i]);
				}
				//renderChildren(nodes[i], category_id, folderLevel, prodVector );
			}
		}
	}

	private boolean leafAlreadyExists(NodeInfo ni)
	{
		// go through all existing nodes
		Enumeration e = leafNodes.elements();
		NodeInfo loop = null;
		while (e.hasMoreElements())
		{
			DefaultMutableTreeNode n = (DefaultMutableTreeNode) e.nextElement();
			Object o = (Object) n.getUserObject();
			loop = (NodeInfo) o;

			if (ni.equals(loop))
			{
				//System.out.println("LEAF TRUE: Comparing "+loop.nodeLabel+","+loop.id+","+loop.level+" with "+ni.nodeLabel+","+ni.id+","+ni.level);
				return true;
			}
		}
		return false;
	}

	private boolean nodeAlreadyExists(NodeInfo ni)
	{
		// go through all existing nodes
		Enumeration e = treeNodes.elements();
		NodeInfo loop = null;
		while (e.hasMoreElements())
		{
			DefaultMutableTreeNode n = (DefaultMutableTreeNode) e.nextElement();
			Object o = (Object) n.getUserObject();
			loop = (NodeInfo) o;

			if (ni.equals(loop))
			{
				return true;
			}
		}
		return false;
	}

	private void renderChildren(DefaultMutableTreeNode treeNode, String category_id, String folderLevel, Vector prodVector)
	{
		// how many levels does this module have .... before you hit physical products
		int levels = getModuleLevels();
		System.out.println("$$$$$ Levels $$$$$" + levels + " : " + folderLevel);
		if (levels == Integer.parseInt(folderLevel))
		{
			renderLeafNodes(category_id, folderLevel, treeNode, prodVector);
		}
	}

	private void renderLeafNodes(String category_id, String folderLevel, DefaultMutableTreeNode treeNode, Vector prodVector)
	{
		prodVector = getAllProducts(category_id);
		Vector v = getProductsForCategory(category_id, prodVector);
		for (int i = 0; i < v.size(); i++)
		{
			String[] resultItem = (String[]) v.elementAt(i);

			String product_name = resultItem[0];
			String product_id = resultItem[1];
			NodeInfo ni = new NodeInfo(product_name, product_id, "LEAF", category_id);
			if (!leafAlreadyExists(ni))
			{
				DefaultMutableTreeNode newLeaf = addObject(treeNode, ni, false);
				leafNodes.put(product_id, newLeaf);
			}
		}
	}

	private int getModuleLevels()
	{
		return (moduleLevels);
	}

	private Vector getAllProducts(String categoryId)
	{
//		System.out.println("getProductsForCategory category_id"+category_id);
		int urlRetry = 0;
		InputStream xml = servletCall("getCat", categoryId);
		while (urlRetry < 5 && xml == null)
		{
			System.out.println("URL connect failed, retry(" + urlRetry + ") ...");
			xml = servletCall("getCat");
			urlRetry++;
		}

		Vector v = null;
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(xml);
			v = getAllLeafs("entry", doc);
		}
		catch (ParserConfigurationException e)
		{
			System.out.println("Parsing of ctmediastore.xml error");
			e.printStackTrace();
		}
		catch (SAXException ex)
		{
			System.out.println("SAX exception in ctmediastore.xml");
			ex.printStackTrace();
		}
		catch (IOException i)
		{
			System.out.println("IO exception when reading ctmediastore.xml");
			i.printStackTrace();
		}
		return (v);
	}

	private Vector getProductsForCategory(String category_id, Vector prodVector)
	{
		// transpose prodVector into v where entries in prodVector have the cat id passed in.
		Vector v = new Vector(15);

		System.out.println("Vector in " + prodVector.size());
		Enumeration e = prodVector.elements();
		while (e.hasMoreElements())
		{
			String[] o = (String[]) e.nextElement();
			if (o[2].equals(category_id))
				v.add(o);
		}
		System.out.println("Vector out " + v.size());

		return (v);
	}

	// Required by TreeExpansionListener interface.
	public void treeCollapsed(TreeExpansionEvent e)
	{
		System.out.println("Tree-collapsed event detected");
	}

	public static void main(String args[])
	{
		try
		{
			UIManager.LookAndFeelInfo lfs[] = UIManager.getInstalledLookAndFeels();
			for (int i = 0; i < lfs.length; i++)
			{
				System.out.println(lfs[i].getName() + " " + lfs[i].getClassName());
				if (args.length >= 1)
					if (args[0].equals(lfs[i].getName()))
					{
						// UIManager.setLookAndFeel(lfs[i].getClassName());
						UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
					}
			}

			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.xxWindowsLookAndFeel");
			TreeApplet2 window = new TreeApplet2();
			window.setSize(350, 200);
			window.setVisible(true);
			if (args.length >= 1)
				JOptionPane.showMessageDialog(window, "Attempted to use " + args[0] +
						" Look And Feel");
		}
		catch (Exception e)
		{
			System.out.println("Exception: " + e.getMessage());
		}
	}




	private class NodeInfo
	{
		public String nodeLabel;
		public String id;
		public String level;
		public String parentID;
		public int childCount;

		public NodeInfo(String name, String dbID, String lvl, String pID)
		{
			System.out.println("Initializing Node Label");
			nodeLabel = name;
			id = dbID;
			level = lvl;
			parentID = pID;
		}

		public NodeInfo(String name, String dbID, String lvl, String pID, int cCount)
		{
			System.out.println("Initializing Node Label");
			nodeLabel = name;
			id = dbID;
			level = lvl;
			parentID = pID;
			childCount = cCount;
		}

		public boolean equals(NodeInfo ni)
		{
//			System.out.println("EQUALS nodeLabel,"+this.nodeLabel+",id,"+this.id+",level,"+this.level);
//			System.out.println("=	   nodeLabel,"+ni.nodeLabel+",id,"+ni.id+",level,"+ni.level);
			if (this.nodeLabel.equals(ni.nodeLabel) &&
					this.id.equals(ni.id) &&
					this.level.equals(ni.level) &&
					this.parentID.equals(ni.parentID))
				return true;
			else
				return false;
		}

		public void setNodeLabel(String nodeLabel)
		{
			this.nodeLabel = nodeLabel;
		}

		public String getNodeLabel()
		{
			return this.nodeLabel;
		}

		public String toString()
		{
			return nodeLabel;
		}

		public int getChildCount()
		{
			return this.childCount;
		}
	}


	private class treeRender extends DefaultTreeCellRenderer
	{

		public Component getTreeCellRendererComponent
				(JTree tree,
						Object value,
						boolean sel,
						boolean expanded,
						boolean leaf,
						int row,
						boolean hasFocus)
		{

			super.getTreeCellRendererComponent(
					tree, value, sel,
					expanded, leaf, row,
					hasFocus);
			Icon icon = null;

			if (leaf && isTrueLeaf(value))
				icon = new MetalIconFactory.FileIcon16();

			else
				icon = new MetalIconFactory.FolderIcon16();

			setIcon(icon);

			return this;
		}

		protected boolean isTrueLeaf(Object value)
		{
			DefaultMutableTreeNode node =
					(DefaultMutableTreeNode) value;
			NodeInfo nodeInfo = null;
			Object ni = node.getUserObject();
//        if ( ni.getClass().getName().equals("java.lang.String") )
//            return false;

			nodeInfo = (NodeInfo) ni;

			if (nodeInfo.level.equals("LEAF"))
			{
				return true;
			}

			return false;
		}

	}

	class PopupListener extends MouseAdapter
	{
		public void mousePressed(MouseEvent e)
		{
			maybeShowPopup(e);
		}

		public void mouseReleased(MouseEvent e)
		{
			maybeShowPopup(e);
		}

		private void maybeShowPopup(MouseEvent e)
		{
			if (e.isPopupTrigger())
			{
				System.out.println("in Popup trigger version 0.1");
				Point p = e.getPoint();

				double xPosition = p.getX();
				System.out.println("Point X" + xPosition);
				double yPosition = p.getY();
				System.out.println("Point Y" + yPosition);
				double x = popupWidth;
				System.out.println("width of popup" + x);
				double y = popupHeight;
				System.out.println("width of popup" + y);
				double X = tree.getVisibleRect().getWidth();
				double Y = tree.getVisibleRect().getHeight();

				double maxX = tree.getBounds().getMaxX();
				double minX = tree.getBounds().getMinX();
				double maxY = tree.getBounds().getMaxY();
				double minY = tree.getBounds().getMinY();
				int xRePosition = (int) xPosition, yRePosition = (int) yPosition;

				if (xPosition > X || yPosition > Y)
				{
					if (xPosition > X)
						X = X + (xPosition - X);
					if (yPosition > y)
						Y = Y + (yPosition - Y);
				}

				if (xPosition > X - x || yPosition > Y - y)
				{

					if (xPosition > (X - x))
					{
						xRePosition = (int) (X - x - 1);
					}
					if (yPosition > Y - y)
					{
						yRePosition = (int) (Y - y - 1);
					}
					//popup.setLocation(xRePosition,yRePosition);
					//popup.show();
				}
				if ((-minY) > Y - y)
				{
					yRePosition = yRePosition + (int) y;
				}
				if ((-minX) > X - x)
				{
					xRePosition = xRePosition + (int) x;
				}
				System.out.println("Frame width" + tree.getVisibleRect().getWidth());
				System.out.println("Frame Height" + tree.getVisibleRect().getHeight());
				System.out.println("After Setting location");
				System.out.println("X postion of popup" + xRePosition);
				System.out.println("Y postion of popup" + yRePosition);
				System.out.println("Xmin" + minX);
				System.out.println("Ymin" + minY);
				System.out.println("Xmax" + maxX);
				System.out.println("Ymax" + maxY);

				popup.show(e.getComponent(),
						xRePosition, yRePosition);

				selPath = tree.getPathForLocation(e.getX(), e.getY());
				if (selPath != null)
				{
					rightClickNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
					tree.setSelectionPath(selPath);
				}
			}
		}
	}


	public class MyEntityResolver implements EntityResolver
	{
		public MyEntityResolver()
		{
		}

//  public InputSource resolveEntity(String publicId, String systemId) {
//    return new InputSource(this.getClass().getClassLoader().getResource("MyDTD.dtd").toString());
//  }
		public InputSource resolveEntity(String publicId, String systemId)
		{
			System.out.println("p " + publicId);
			System.out.println("s " + systemId);
			if (systemId.endsWith("MyDTD.dtd"))
			{
				return new InputSource(this.getClass().getClassLoader().getResource("MyDTD.dtd").toString());
			}

			return new InputSource(systemId);
		}

	}
}
