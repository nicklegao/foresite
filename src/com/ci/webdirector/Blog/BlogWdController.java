/**
 * 12:06:02 PM 18/01/2012
 * @author Vito Lefemine
 */
package com.ci.webdirector.Blog;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import webdirector.utils.Blog.BlogCategoryUtil;
import webdirector.utils.Blog.BlogMapping;

/**
 * @author Vito Lefemine
 *
 */
@Controller
public class BlogWdController
{

	static Logger logger = Logger.getLogger(BlogWdController.class);

	private String DEFAULT_ERROR_MESSAGE = "An Error Occours!";


	@RequestMapping("/blog/addCategory")
	@ResponseBody
	public String addCategory(@RequestParam("moduleName") String moduleName, @RequestParam("category") String category,
			@RequestParam("blogId") String blogId, HttpServletRequest request)
	{
		try
		{
			BlogCategoryUtil bcu = new BlogCategoryUtil();
			int id = bcu.addCategory(moduleName, category, blogId);
			if (id > 0)
				return "" + id;
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return DEFAULT_ERROR_MESSAGE;
	}

	@RequestMapping("/blog/assignCategory")
	@ResponseBody
	public String assignCategory(@RequestParam("postId") String postId, @RequestParam("assignedCategory") String assignedCategory,
			@RequestParam("moduleSelected") String moduleSelected, HttpServletRequest request)
	{
		try
		{

			BlogMapping blogMapping = new BlogMapping();

			String cols[] = null;
			if (moduleSelected != null && moduleSelected.length() > 0)
			{
				cols = moduleSelected.split(",");
			}
			logger.info("BlogWdController postId:" + postId + " assignedCategory:" + assignedCategory + " moduleSelected:" + moduleSelected);
			boolean status = blogMapping.assignCategories(postId, cols);
			if (status)
				return "OK";
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return DEFAULT_ERROR_MESSAGE;
	}

	@RequestMapping("/blog/deleteCategories")
	@ResponseBody
	public String deleteCategories(@RequestParam("toDelete") String toDelete)
	{
		try
		{
			BlogCategoryUtil bcu = new BlogCategoryUtil();
			bcu.deleteCategories(toDelete);
			return "OK";
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return DEFAULT_ERROR_MESSAGE;

	}
}
