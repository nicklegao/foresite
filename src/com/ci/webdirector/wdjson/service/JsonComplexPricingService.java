package com.ci.webdirector.wdjson.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.search.TopDocs;

import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
import au.net.webdirector.common.search.Search;

import com.ci.webdirector.wdjson.model.JsonComplexPricingRequest;
import com.ci.webdirector.wdjson.model.JsonComplexPricingResponse;
import com.ci.webdirector.wdjson.model.JsonComplexPricingSaveRequest;
import com.ci.webdirector.wdjson.model.JsonPricingTypeSaveRequest;

public class JsonComplexPricingService
{

	private static Logger logger = Logger.getLogger(JsonComplexPricingService.class);

	public JsonComplexPricingResponse getComplexPrices(JsonComplexPricingRequest input) throws IOException
	{

		List<String> ids = new ArrayList<String>();
		Search s = new Search(input.getModuleName());
		if (StringUtils.isNotBlank(input.getFilter()))
		{
			try
			{
				s.openSearchIndex();
				TopDocs hits = s.performSearchbyType(input.getFilter(), "element", false);
				if (hits != null)
				{
					for (int i = 0; i < hits.totalHits; i++)
					{
						ids.add(s.getIndexSearcher().doc(hits.scoreDocs[i].doc).get("id"));
					}
				}
			}
			catch (Exception e)
			{
				logger.error("Error", e);
			}
			finally
			{
				s.closeSearchIndex();
			}
		}
		//Object [] paramsQuery1 = new Object[]{};
		List<String> paramsQuery1 = new ArrayList<String>();

		//Object [] paramsQuery2 = new Object[]{};
		List<String> paramsQuery2 = new ArrayList<String>();

		//paramsQuery2[0] = input.getPricingType();
		paramsQuery2.add(input.getPricingType());


		String sqlProduct = "select e.Element_id as e_id, e.ATTR_ModelNumber as model_number, e.ATTRCURRENCY_Price as p_price, '' as u_price, e.ATTR_Headline as p_name from elements_products e where 1 = 1 ";

		if (StringUtils.isNotBlank(input.getCategoryId()))
		{
			sqlProduct += " and e.category_id = ?";
			//paramsQuery1[0] = input.getCategoryId();
			paramsQuery1.add(input.getCategoryId());
			//paramsQuery2[1] = input.getCategoryId();
			paramsQuery2.add(input.getCategoryId());

		}
		else
		{
			sqlProduct += " and e.category_id in (select category_id from categories_products where folderLevel = '2' and ATTRCHECK_multiplePricing = '1') ";
		}
		if (ids.size() > 0)
		{
			sqlProduct += " and e.element_id in (" + StringUtils.join(ids, ",") + ") ";
		}
		sqlProduct += " order by p_name";
		List<Hashtable<String, String>> result = DataAccess.getInstance().select(sqlProduct, paramsQuery1.toArray(), new HashtableMapper());

		String sqlUom = "select u.Element_id as u_id, u.ATTR_ModelNumber as model_number, u.ATTR_Price as u_price from elements_flex15 u where live=1 and (Live_date is null or live_date <= now()) and (Expire_date is null or expire_date >= now()) ";
		sqlUom += " and u.category_id = ? ";
		sqlUom += " and u.ATTR_ModelNumber in (select ATTR_ModelNumber from (" + sqlProduct + ") as p)";


		if ((null == input.getMemberGrps() || input.getMemberGrps().size() == 0) && (null == input.getMembers() || input.getMembers().size() == 0))
		{
			String sqlUomDef = sqlUom;
			sqlUomDef += " and u.ATTRCHECK_DefaultPriceSetting = '1'";
			List<Hashtable<String, String>> result1 = DataAccess.getInstance().select(sqlUomDef, paramsQuery2.toArray(), new HashtableMapper());
			mergeResult(result, result1);
		}
		if (null != input.getMemberGrps() && input.getMemberGrps().size() > 0)
		{
			String sqlUomMemberGroup = sqlUom;
			sqlUomMemberGroup += " and u.Element_id in (select ELEMENT_ID from drop_down_multi_selections where module_name = 'FLEX15' and module_type = 'element' and ATTRMULTI_NAME = 'ATTRMULTI_MemberGroups' and SELECTED_VALUE IN (" + StringUtils.join(input.getMemberGrps(), ",") + ") ) ";
			List<Hashtable<String, String>> result2 = DataAccess.getInstance().select(sqlUomMemberGroup, paramsQuery2.toArray(), new HashtableMapper());
			mergeResult(result, result2);
		}
		if (null != input.getMembers() && input.getMembers().size() > 0)
		{
			String sqlUomMember = sqlUom;
			sqlUomMember += " and u.Element_id in (select ELEMENT_ID from drop_down_multi_selections where module_name = 'FLEX15' and module_type = 'element' and ATTRMULTI_NAME = 'ATTRMULTI_Members' and SELECTED_VALUE IN (" + StringUtils.join(input.getMembers(), ",") + ") ) ";
			List<Hashtable<String, String>> result3 = DataAccess.getInstance().select(sqlUomMember, paramsQuery2.toArray(), new HashtableMapper());
			mergeResult(result, result3);
		}

		// TODO Auto-generated method stub
		JsonComplexPricingResponse response = new JsonComplexPricingResponse();
		response.setListElements(result);
		return response;
	}

	private void mergeResult(List<Hashtable<String, String>> result, List<Hashtable<String, String>> result1)
	{
		for (Hashtable<String, String> res : result)
		{
			for (Hashtable<String, String> res1 : result1)
			{
				if (res1.get("model_number").equals(res.get("model_number")))
				{
					res.putAll(res1);
				}
			}
		}
	}

	public boolean saveComplexPrices(JsonComplexPricingSaveRequest input)
	{
		if ((null == input.getMembers() || input.getMembers().size() == 0) && (null == input.getMemberGrps() || input.getMemberGrps().size() == 0))
		{
			return saveComplexPricesDefault(input);
		}
		return saveComplexPricesForMembersAndGroups(input);
	}

	private boolean saveComplexPricesForMembersAndGroups(JsonComplexPricingSaveRequest input)
	{
		DataAccess db = DataAccess.getInstance();
		List<String> subcondition = new ArrayList<String>();
		if (input.getMemberGrps() != null && input.getMemberGrps().size() > 0)
		{
			subcondition.add("(ATTRMULTI_NAME = 'ATTRMULTI_MemberGroups' and SELECTED_VALUE in (" + StringUtils.join(input.getMemberGrps(), ",") + "))");
		}
		if (input.getMembers() != null && input.getMembers().size() > 0)
		{
			subcondition.add("(ATTRMULTI_NAME = 'ATTRMULTI_Members' and SELECTED_VALUE in (" + StringUtils.join(input.getMembers(), ",") + "))");
		}
		for (Map<String, String> prices : input.getPrices())
		{

			try
			{
				// unallocate existing settings for member and groups
				String unallocate = "delete from drop_down_multi_selections where module_name = 'FLEX15' and module_type = 'element' and element_id in (select element_id from elements_flex15 where category_id = ? and attr_modelnumber = ?) ";
				unallocate += " and (" + StringUtils.join(subcondition, " OR ") + " )";
				db.updateData(unallocate, new String[] { input.getPricingType(), prices.get("model_number") });

				// find settings (not default) with same prices, create one if
				// there
				// is not
				// allocate members and groups to these settings
				if (StringUtils.isBlank(prices.get("u_price")))
				{
					continue;
				}
				String sql = "select Element_id from elements_flex15 where category_id = ? and ATTRCHECK_DefaultPriceSetting = '0' and ATTR_ModelNumber = ? and live = 1 and (live_date is null or live_date <= now()) and (expire_date is null or expire_date >= now())";
				sql += " and ATTR_PRICE = ? ";
				List<String> ids = db.select(sql, new Object[] { input.getPricingType(), prices.get("model_number"), prices.get("u_price") }, new StringMapper());
				String id = null;
				if (ids.size() == 0)
				{
					String name = (String) db.select("select ATTR_Headline from elements_products where attr_modelnumber = ? ", new Object[] { prices.get("model_number") }, new StringMapper()).get(0);
					String[] values = new String[] { input.getPricingType(), name, prices.get("u_price"), prices.get("model_number") };
					String insert = "insert into elements_flex15 (Language_id,Status_id,Lock_id,Version,display_order,Live,Category_id,ATTR_Headline,ATTR_Price,ATTR_ModelNumber,ATTRCHECK_DefaultPriceSetting) values (1,1,1,1,0,1, ?,?,?,?,'0')";
					id = String.valueOf(db.insertDataQuery(insert, values));
				}
				else
				{
					id = ids.get(0);
				}
				String allocate = "insert into drop_down_multi_selections (SELECTED_VALUE, ELEMENT_ID, ATTRMULTI_NAME, MODULE_NAME, module_type) values ";
				List<String> values = new ArrayList<String>();
				if (input.getMembers() != null)
				{
					for (String member : input.getMembers())
					{
						values.add("('" + member + "', '" + id + "', 'ATTRMULTI_Members', 'FLEX15', 'element')");
					}
				}
				if (input.getMemberGrps() != null)
				{
					for (String group : input.getMemberGrps())
					{
						values.add("('" + group + "', '" + id + "', 'ATTRMULTI_MemberGroups', 'FLEX15', 'element')");
					}
				}
				allocate += StringUtils.join(values, ", ");
				db.updateData(allocate, new String[] {});
			}
			catch (Exception e)
			{
				logger.error("Error", e);
			}

		}

		// remove empty customised settings
		String empty = "delete from elements_flex15 where live = 1 and (live_date is null or live_date <= now()) and (expire_date is null or expire_date >= now()) and ATTRCHECK_DefaultPriceSetting = '0' and category_id = ? and element_id not in (select element_id from drop_down_multi_selections where MODULE_NAME = 'FLEX15' and module_type = 'element' and ATTRMULTI_NAME in ('ATTRMULTI_MemberGroups', 'ATTRMULTI_Members'))";
		db.updateData(empty, new String[] { input.getPricingType() });
		return true;
	}

	private boolean saveComplexPricesDefault(JsonComplexPricingSaveRequest input)
	{
		DataAccess db = DataAccess.getInstance();
		for (Map<String, String> prices : input.getPrices())
		{
			try
			{
				if (StringUtils.isBlank(prices.get("u_price")))
				{
					db.updateData("delete from elements_flex15 where category_id = ? and ATTRCHECK_DefaultPriceSetting = '1' and ATTR_ModelNumber = ? and live = 1 and (live_date is null or live_date <= now()) and (expire_date is null or expire_date >= now())", new String[] { input.getPricingType(), prices.get("model_number") });
					continue;
				}
				String sql = "select Element_id from elements_flex15 where category_id = ? and ATTRCHECK_DefaultPriceSetting = '1' and ATTR_ModelNumber = ? and live = 1 and (live_date is null or live_date <= now()) and (expire_date is null or expire_date >= now())";
				List<String> ids = db.select(sql, new Object[] { input.getPricingType(), prices.get("model_number") }, new StringMapper());
				if (ids.size() == 0)
				{
					String name = (String) db.select("select ATTR_Headline from elements_products where attr_modelnumber = ? ", new Object[] { prices.get("model_number") }, new StringMapper()).get(0);
					String[] values = new String[] { input.getPricingType(), name, prices.get("u_price"), prices.get("model_number") };
					String insert = "insert into elements_flex15 (Language_id,Status_id,Lock_id,Version,display_order,Live,Category_id,ATTR_Headline,ATTR_Price,ATTR_ModelNumber,ATTRCHECK_DefaultPriceSetting) values (1,1,1,1,0,1,?,?,?,?,'1')";
					db.insertDataQuery(insert, values);
					continue;
				}
				String[] values = new String[] { prices.get("u_price"), ids.get(0) };
				String update = "update elements_flex15 set ATTR_Price = ? where Element_id = ?";
				db.updateData(update, values);
			}
			catch (Exception e)
			{
				logger.error("Error", e);
			}
		}
		return true;
	}

	public Map<String, String> createPricingType(JsonPricingTypeSaveRequest input)
	{
		DataAccess db = DataAccess.getInstance();
		String sql = "select category_id from categories_flex15 where ATTRDROP_CountryCode = ? and ATTRDROP_UOM = ?";
		List<String> list = db.select(sql, new String[] { input.getCountry(), input.getUom() }, new StringMapper());
		Map<String, String> result = new HashMap<String, String>();
		if (list.size() == 0)
		{
			String insertSql = "insert into categories_flex15 (Category_ParentID,folderLevel,display_order,Create_date,ATTR_categoryName,ATTRCHECK_Live,ATTRDROP_UOM,ATTRDROP_CountryCode) values (0,1,0,now(),?,1,?,?)";
			result.put("id", String.valueOf(db.insertDataQuery(insertSql, new String[] { input.getName(), input.getUom(), input.getCountry() })));
		}
		else
		{
			result.put("id", list.get(0));
		}
		result.put("name", input.getName());
		result.put("uom", input.getUom());
		return result;
	}

}
