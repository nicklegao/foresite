package com.ci.webdirector.wdjson.model;

import java.util.List;
import java.util.Map;

public class JsonSalesOrdersResponse
{
	List<Map<String, String>> result;
	Map<String, String> sum;
	int count;

	public List<Map<String, String>> getResult()
	{
		return result;
	}

	public void setResult(List<Map<String, String>> result)
	{
		this.result = result;
	}

	public int getCount()
	{
		return count;
	}

	public void setCount(int count)
	{
		this.count = count;
	}

	public Map<String, String> getSum()
	{
		return sum;
	}

	public void setSum(Map<String, String> sum)
	{
		this.sum = sum;
	}

}
