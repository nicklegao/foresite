package com.ci.webdirector.wdjson.model;

import java.util.Vector;

public class JsonReviewResponse
{

	String[] review;
	Vector<String[]> catReview;

	public JsonReviewResponse()
	{
		super();
	}

	public JsonReviewResponse(String[] review, Vector<String[]> catReview)
	{
		super();
		this.review = review;
		this.catReview = catReview;
	}

	public String[] getReview()
	{
		return review;
	}

	public void setReview(String[] review)
	{
		this.review = review;
	}

	public Vector<String[]> getCatReview()
	{
		return catReview;
	}

	public void setCatReview(Vector<String[]> catReview)
	{
		this.catReview = catReview;
	}




}
