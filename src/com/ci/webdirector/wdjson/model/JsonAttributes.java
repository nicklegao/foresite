package com.ci.webdirector.wdjson.model;

import java.util.List;
import java.util.Map;

/**
 * @author Vito Lefemine
 *
 */
public class JsonAttributes
{

	String name;
	String internalName;
	String type;
	int size;
	List<Map<String, String>> options;

	public List<Map<String, String>> getOptions()
	{
		return options;
	}

	public void setOptions(List<Map<String, String>> options)
	{
		this.options = options;
	}

	public int getSize()
	{
		return size;
	}

	public void setSize(int size)
	{
		this.size = size;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getInternalName()
	{
		return internalName;
	}

	public void setInternalName(String internalName)
	{
		this.internalName = internalName;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public JsonAttributes(String name, String internalName, String type)
	{
		super();
		this.name = name;
		this.internalName = internalName;
		this.type = type;
	}

	public JsonAttributes()
	{
		super();
	}




}
