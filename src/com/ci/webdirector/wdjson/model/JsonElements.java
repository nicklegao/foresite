package com.ci.webdirector.wdjson.model;

import java.util.Hashtable;

public class JsonElements
{

	Hashtable<String, String> elements;
	String moduleName;

	public Hashtable<String, String> getElements()
	{
		return elements;
	}

	public void setElements(Hashtable<String, String> elements)
	{
		this.elements = elements;
	}

	public String getModuleName()
	{
		return moduleName;
	}

	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}

	public JsonElements(Hashtable<String, String> elements, String moduleName)
	{
		super();
		this.elements = elements;
		this.moduleName = moduleName;
	}

}
