package com.ci.webdirector.wdjson.model;

import java.util.List;


public class JsonSalesOrdersRequest
{
	String orderNo;
	String email;
	String postcode;
	String paymentRef;
	String dateStart;
	String dateEnd;
	int pageNo;
	int recordPerPage;
	String order;
	List<String> fields;

	public String getOrderNo()
	{
		return orderNo;
	}

	public String getEmail()
	{
		return email;
	}

	public String getPostcode()
	{
		return postcode;
	}

	public String getPaymentRef()
	{
		return paymentRef;
	}

	public String getDateStart()
	{
		return dateStart;
	}

	public String getDateEnd()
	{
		return dateEnd;
	}

	public int getPageNo()
	{
		return pageNo;
	}

	public int getRecordPerPage()
	{
		return recordPerPage;
	}

	public String getOrder()
	{
		return order;
	}

	public void setOrderNo(String orderNo)
	{
		this.orderNo = orderNo;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public void setPostcode(String postcode)
	{
		this.postcode = postcode;
	}

	public void setPaymentRef(String paymentRef)
	{
		this.paymentRef = paymentRef;
	}

	public void setDateStart(String dateStart)
	{
		this.dateStart = dateStart;
	}

	public void setDateEnd(String dateEnd)
	{
		this.dateEnd = dateEnd;
	}

	public void setPageNo(int pageNo)
	{
		this.pageNo = pageNo;
	}

	public void setRecordPerPage(int recordPerPage)
	{
		this.recordPerPage = recordPerPage;
	}

	public void setOrder(String order)
	{
		this.order = order;
	}

	public List<String> getFields()
	{
		return fields;
	}

	public void setFields(List<String> fields)
	{
		this.fields = fields;
	}

}
