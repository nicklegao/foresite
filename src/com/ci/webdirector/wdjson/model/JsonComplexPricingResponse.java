package com.ci.webdirector.wdjson.model;

import java.util.Hashtable;
import java.util.List;

public class JsonComplexPricingResponse
{

	List<Hashtable<String, String>> listElements;

	public List<Hashtable<String, String>> getListElements()
	{
		return listElements;
	}

	public void setListElements(List<Hashtable<String, String>> listElements)
	{
		this.listElements = listElements;
	}



}
