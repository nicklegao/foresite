package com.ci.webdirector.wdjson.model;


public class JsonRequestAttributes
{

	private String moduleName;
	private String categoryId;


	public JsonRequestAttributes()
	{
		super();
	}

	public String getCategoryId()
	{
		return categoryId;
	}

	public void setCategoryId(String categoryId)
	{
		this.categoryId = categoryId;
	}

	public String getModuleName()
	{
		return moduleName;
	}

	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}

	public JsonRequestAttributes(String moduleName)
	{
		super();
		this.moduleName = moduleName;
	}



}
