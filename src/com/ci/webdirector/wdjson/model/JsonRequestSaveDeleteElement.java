package com.ci.webdirector.wdjson.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Hashtable;


public class JsonRequestSaveDeleteElement
{

	Hashtable<String, Hashtable<String, String>> delete;
	String moduleName;
	Hashtable<String, Hashtable<String, String>> save;

	public JsonRequestSaveDeleteElement()
	{
		super();
	}


	public Hashtable<String, Hashtable<String, String>> getSave()
	{
		return save;
	}

	public void setSave(Hashtable<String, Hashtable<String, String>> save)
	{
		this.save = save;
	}

	public Hashtable<String, Hashtable<String, String>> getDelete()
	{
		return delete;
	}

	public void setDelete(Hashtable<String, Hashtable<String, String>> delete)
	{
		this.delete = delete;
	}

	public String getModuleName()
	{
		return moduleName;
	}

	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}

	public List<String> savedValues(String fieldName)
	{
		List<String> values = new ArrayList<String>();
		for (Hashtable<String, String> d : save.values())
		{
			if (d.containsKey(fieldName))
			{
				values.add(d.get(fieldName));
			}
		}
		return values;
	}

	public List<String> deletedValues(String fieldName)
	{
		List<String> values = new ArrayList<String>();
		for (Hashtable<String, String> d : delete.values())
		{
			if (d.containsKey(fieldName))
			{
				values.add(d.get(fieldName));
			}
		}
		return values;
	}

	public List<String> deletedIDs()
	{
		return new ArrayList<String>(delete.keySet());
	}
}
