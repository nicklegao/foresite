package com.ci.webdirector.wdjson.model;

import java.util.List;
import java.util.Map;

public class JsonComplexPricingSaveRequest extends JsonComplexPricingRequest
{
	List<Map<String, String>> prices;

	public List<Map<String, String>> getPrices()
	{
		return prices;
	}

	public void setPrices(List<Map<String, String>> prices)
	{
		this.prices = prices;
	}


}
