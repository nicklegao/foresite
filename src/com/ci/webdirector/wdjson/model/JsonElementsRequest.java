package com.ci.webdirector.wdjson.model;

import java.util.List;

public class JsonElementsRequest
{

	String moduleName;
	List<String> attributes;
	String categoryId;
	String filter;

	public String getFilter()
	{
		return filter;
	}

	public void setFilter(String filter)
	{
		this.filter = filter;
	}

	public String getModuleName()
	{
		return moduleName;
	}

	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}

	public List<String> getAttributes()
	{
		return attributes;
	}

	public void setAttributes(List<String> attributes)
	{
		this.attributes = attributes;
	}

	public JsonElementsRequest()
	{
		super();
	}

	public JsonElementsRequest(String moduleName, List<String> attributes)
	{
		super();
		this.moduleName = moduleName;
		this.attributes = attributes;
	}

	public String getCategoryId()
	{
		return categoryId;
	}

	public void setCategoryId(String categoryId)
	{
		this.categoryId = categoryId;
	}


}
