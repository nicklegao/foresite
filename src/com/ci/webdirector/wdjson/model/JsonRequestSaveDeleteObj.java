package com.ci.webdirector.wdjson.model;

import java.util.Hashtable;

public class JsonRequestSaveDeleteObj
{

	String elementId;
	Hashtable<String, String> attributes;

	public JsonRequestSaveDeleteObj()
	{
		super();
	}

	public JsonRequestSaveDeleteObj(String elementId,
			Hashtable<String, String> attributes)
	{
		super();
		this.elementId = elementId;
		this.attributes = attributes;
	}

	public String getElementId()
	{
		return elementId;
	}

	public void setElementId(String elementId)
	{
		this.elementId = elementId;
	}

	public Hashtable<String, String> getAttributes()
	{
		return attributes;
	}

	public void setAttributes(Hashtable<String, String> attributes)
	{
		this.attributes = attributes;
	}



}
