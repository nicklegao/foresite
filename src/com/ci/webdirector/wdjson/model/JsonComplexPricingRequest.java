package com.ci.webdirector.wdjson.model;

import java.util.List;

public class JsonComplexPricingRequest
{
	String categoryId;
	String filter;
	List<String> memberGrps;
	List<String> members;
	String moduleName;
	String pricingType;

	public String getCategoryId()
	{
		return categoryId;
	}

	public void setCategoryId(String categoryId)
	{
		this.categoryId = categoryId;
	}

	public String getFilter()
	{
		return filter;
	}

	public void setFilter(String filter)
	{
		this.filter = filter;
	}

	public List<String> getMemberGrps()
	{
		return memberGrps;
	}

	public void setMemberGrps(List<String> memberGrps)
	{
		this.memberGrps = memberGrps;
	}

	public List<String> getMembers()
	{
		return members;
	}

	public void setMembers(List<String> members)
	{
		this.members = members;
	}

	public String getModuleName()
	{
		return moduleName;
	}

	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}

	public String getPricingType()
	{
		return pricingType;
	}

	public void setPricingType(String pricingType)
	{
		this.pricingType = pricingType;
	}


}
