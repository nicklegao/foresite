package com.ci.webdirector.wdjson.model;

/**
 * @author Vito Lefemine
 *
 */
import java.util.Hashtable;

public class JsonElementsResponse
{

	Hashtable<String, Hashtable<String, String>> listElements;

	public Hashtable<String, Hashtable<String, String>> getListElements()
	{
		return listElements;
	}

	public void setListElements(Hashtable<String, Hashtable<String, String>> listElements)
	{
		this.listElements = listElements;
	}

	public JsonElementsResponse(Hashtable<String, Hashtable<String, String>> listElements)
	{
		super();
		this.listElements = listElements;
	}

	public JsonElementsResponse()
	{
		super();
	}


}
