package com.ci.webdirector.wdjson.model;

import java.util.Map;


/**
 * @author Vito Lefemine
 *
 */
public class JsonAttributesResponse
{
	Map<String, JsonAttributes> attributes;

	public Map<String, JsonAttributes> getAttributes()
	{
		return attributes;
	}

	public void setAttributes(Map<String, JsonAttributes> attributes)
	{
		this.attributes = attributes;
	}

	public JsonAttributesResponse()
	{
		super();
	}

	public JsonAttributesResponse(Map<String, JsonAttributes> attributes)
	{
		super();
		this.attributes = attributes;
	}

}
