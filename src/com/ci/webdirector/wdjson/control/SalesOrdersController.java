package com.ci.webdirector.wdjson.control;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;

import com.ci.webdirector.wdjson.model.JsonSalesOrdersRequest;
import com.ci.webdirector.wdjson.model.JsonSalesOrdersResponse;

@Controller
public class SalesOrdersController
{
	Logger logger = Logger.getLogger(SalesOrdersController.class);

	@RequestMapping("/exportSalesOrders")
	@ResponseBody
	public String exportElements(@RequestBody JsonSalesOrdersRequest input, HttpServletRequest request) throws IOException
	{
		String token = RandomStringUtils.randomAlphanumeric(9);
		request.getSession().setAttribute("export_token_" + token, input);
		return token;
	}

	/**
	 * TODO: FIXME: Sushant/Nick Removed in WD10
	 * 
	 * @param output
	 * @param request
	 * @throws SQLException
	 * @throws IOException
	 */
	@Deprecated
	public void writeCsv(OutputStream output, HttpServletRequest request) throws SQLException, IOException
	{
//		String token = request.getParameter("token");
//		JsonSalesOrdersRequest input = (JsonSalesOrdersRequest) request.getSession().getAttribute("export_token_" + token);
//		if (input == null)
//		{
//			throw new InvalidParameterException("token not valid");
//		}
//		request.getSession().removeAttribute("export_token_" + token);
//		Map<String, JsonAttributes> attrs = new JsonWdService().getAttributes("SALESORDERS", "categories").getAttributes();
//		List<String> fields = new ArrayList<String>();
//		List<String> headers = new ArrayList<String>();
//		for (String field : input.getFields())
//		{
//			if (!attrs.containsKey(field))
//			{
//				continue;
//			}
//			headers.add(attrs.get(field).getName());
//			fields.add(field);
//		}
//		DataAccess da = DataAccess.getInstance();
//		Connection conn = null;
//		Statement stat = null;
//		ResultSet rs = null;
//		CsvWriter writer = null;
//		try
//		{
//			writer = new CsvWriter(output, ',', Charset.defaultCharset());
//			writer.writeRecord(headers.toArray(new String[] {}));
//			conn = da.getConnection();
//			String sql = getExportQuery(fields, input);
//			stat = conn.createStatement();
//			rs = stat.executeQuery(sql);
//			int length = fields.size();
//			while (rs.next())
//			{
//				String[] row = new String[length];
//				for (int i = 0; i < row.length; i++)
//				{
//					row[i] = (rs.getString(i + 1) == null ? "" : rs.getString(i + 1));
//				}
//				writer.writeRecord(row);
//			}
//		}
//		finally
//		{
//			if (rs != null)
//			{
//				rs.close();
//			}
//			if (stat != null)
//			{
//				stat.close();
//			}
//			if (conn != null)
//			{
//				conn.close();
//			}
//			if (writer != null)
//			{
//				writer.flush();
//				writer.close();
//			}
//		}
//
	}


	private String getExportQuery(List<String> fields, JsonSalesOrdersRequest input)
	{
		String sql = "select " + StringUtils.join(fields, ",") + " from categories_salesorders ";
		sql += getWhereClause(input);
		return sql;
	}

	@RequestMapping("/getSalesOrders")
	@ResponseBody
	public JsonSalesOrdersResponse getElements(@RequestBody JsonSalesOrdersRequest input, HttpServletRequest request) throws IOException
	{
		DataAccess da = DataAccess.getInstance();
		JsonSalesOrdersResponse response = new JsonSalesOrdersResponse();
		response.setResult(da.select(getQuery(input), new HashtableMapper()));
		response.setCount(Integer.parseInt((String) da.select(getCountQuery(input), new StringMapper()).get(0)));
		response.setSum((Map<String, String>) da.select(getSumQuery(input), new HashtableMapper()).get(0));
		return response;
	}

	private String getSumQuery(JsonSalesOrdersRequest input)
	{
		List<String> summableFields = getSummableFields(input);
		if (summableFields.size() == 0)
		{
			return "select 0 as dummy";
		}
		String sql = "select " + StringUtils.join(summableFields, ",") + " from categories_salesorders ";
		sql += getWhereClause(input);
		return sql;
	}

	/**
	 * TODO: FIXME: Sushant/Nick This code has been disabled in WD10
	 * 
	 * @param input
	 * @return
	 */
	private List<String> getSummableFields(JsonSalesOrdersRequest input)
	{
		List<String> summableFields = new ArrayList<String>();
		logger.fatal("getSummableFields(...) HAS BEEN DISABLED!!!");
		logger.fatal("getSummableFields(...) HAS BEEN DISABLED!!!");
		logger.fatal("getSummableFields(...) HAS BEEN DISABLED!!!");
//		Map<String, JsonAttributes> attrs = new JsonWdService().getAttributes("SALESORDERS", "categories").getAttributes();
//		for (String field : attrs.keySet())
//		{
//			if (field.startsWith("ATTRCURRENCY_"))
//			{
//				summableFields.add("ifnull(sum(" + field + "), 0) as " + field);
//			}
//		}
		return summableFields;
	}

	private String getCountQuery(JsonSalesOrdersRequest input)
	{
		String sql = "select count(1) from categories_salesorders ";
		sql += getWhereClause(input);
		return sql;
	}

	private String getQuery(JsonSalesOrdersRequest input)
	{
		String sql = "select * from categories_salesorders ";
		sql += getWhereClause(input);
		int recordPerPage = input.getRecordPerPage() > 0 ? input.getRecordPerPage() : 20;
		int start = ((input.getPageNo() > 0 ? input.getPageNo() : 1) - 1) * recordPerPage;
		sql += " limit " + start + ", " + recordPerPage;
		return sql;
	}

	private String getWhereClause(JsonSalesOrdersRequest input)
	{
		String sql = " where 1= 1 ";
		if (StringUtils.isNotBlank(input.getOrderNo()))
		{
			sql += " and ATTR_OrderNumber = '" + DatabaseValidation.encodeParam(input.getOrderNo()) + "'";
		}
		if (StringUtils.isNotBlank(input.getEmail()))
		{
			sql += " and ATTR_billtoEmail = '" + DatabaseValidation.encodeParam(input.getEmail()) + "'";
		}
		if (StringUtils.isNotBlank(input.getPostcode()))
		{
			sql += " and ATTR_billtoPostCode = '" + DatabaseValidation.encodeParam(input.getPostcode()) + "'";
		}
		if (StringUtils.isNotBlank(input.getPaymentRef()))
		{
			sql += " and ATTR_PaymentReference = '" + DatabaseValidation.encodeParam(input.getPaymentRef()) + "'";
		}
		if (StringUtils.isNotBlank(input.getDateStart()))
		{
			sql += " and date(ATTRDATE_OrderDate) >= '" + DatabaseValidation.encodeParam(input.getDateStart()) + "'";
		}
		if (StringUtils.isNotBlank(input.getDateEnd()))
		{
			sql += " and date(ATTRDATE_OrderDate) <= '" + DatabaseValidation.encodeParam(input.getDateEnd()) + "'";
		}
		if (StringUtils.isNotBlank(input.getOrder()))
		{
			sql += " order by " + input.getOrder();
		}
		return sql;
	}
}
