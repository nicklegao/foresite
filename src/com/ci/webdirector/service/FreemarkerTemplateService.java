package com.ci.webdirector.service;

import java.io.StringReader;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import webdirector.db.client.ClientFileUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * A utility class to process contents in store
 * using Freemarker - this allows us to have
 * dynamic content in stores.
 * 
 * @author asif
 *
 */

public class FreemarkerTemplateService
{

	private static Logger logger = Logger.getLogger(FreemarkerTemplateService.class);

	private static FreemarkerTemplateService inst = new FreemarkerTemplateService();

	private ClientFileUtils clientFileUtils = new ClientFileUtils();

	private Configuration freemarkerConfig = new Configuration();

	private FreemarkerTemplateService()
	{

	}

	public static FreemarkerTemplateService getInstance()
	{
		return inst;
	}

	/**
	 * Reads the given template from Store and then processes it
	 * using Freemarker.
	 * 
	 * @param templatePath
	 * @param model
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public String getContent(String templatePath, Map model)
	{

		String template = clientFileUtils.readTextContents(clientFileUtils.getFilePathFromStoreName(templatePath)).toString();

		String content = "";
		try
		{
			Template t = new Template("t", new StringReader(template), freemarkerConfig);
			content = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
		}
		catch (Exception e)
		{
			logger.error(e);
		}

		return content;
	}
}