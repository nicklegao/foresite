package com.ci.webdirector.ui;

import webdirector.db.client.ClientFileUtils;

public class UIUtils
{

	private static ClientFileUtils clientFileUtils = new ClientFileUtils();

	public static boolean notEmpy(String stringToCheck)
	{
		return stringToCheck != null && stringToCheck.length() > 0;
	}

	public static boolean fileNotEmpy(String s)
	{
		if (s != null && !("").equals(s))
		{
			StringBuffer contents = clientFileUtils.readTextContents(clientFileUtils.getFilePathFromStoreName(s));
			if (contents != null && contents.length() > 0)
			{
				return true;
			}
		}
		return false;
	}
}
