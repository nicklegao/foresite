package com.ci.webdirector.ui;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.log4j.Logger;

import sbe.ShortUrls.ShortUrlManager;
import webdirector.db.client.ClientDataAccess;
import au.net.webdirector.common.Defaults;

/**
 * Creates a list of <code>MenuSettings</code> for an asset at Runtime.
 * 
 * @author asif
 * 
 */

@SuppressWarnings("unchecked")
public class MenuSettingsService
{

	private static Logger logger = Logger.getLogger(MenuSettingsService.class);

	private ShortUrlManager shorturlManager = sbe.ShortUrls.ShortUrlManager.getInstance();
	private ClientDataAccess cda = new ClientDataAccess();
	private List<MenuSettings> allMenus = new CopyOnWriteArrayList<MenuSettings>();
	private Defaults DEFAUTLS = Defaults.getInstance();

	/**
	 * Returns the correct list of <code>MenuSettings</code> for a given
	 * <code>WDSettings</code>
	 * 
	 * @param settings
	 * @return
	 */
	public List<MenuSettings> getMenus(WDSettings settings)
	{

		PageSettings pageSettings = settings.getPageSettings();

		List<MenuSettings> menus = new ArrayList<MenuSettings>();

		allMenus.clear();
		if (allMenus.isEmpty())
		{
			loadAllMenus();

			for (MenuSettings menu : allMenus)
			{
				if ((menu != null) && (menu.getItems() != null) && (!menu.getItems().isEmpty()))
				{
					for (MenuItemSettings item : menu.getItems())
					{
						item.setUrl(getMenuItemURL(item));

						if ((item.getSubMenuItems() != null) && (!item.getSubMenuItems().isEmpty()))
						{
							for (OpenModuleMenuItem subMenuItem : item.getSubMenuItems())
							{
								subMenuItem.setUrl(getSubMenuItemURL(subMenuItem, item.getOpenModule()));
							}
						}
					}

					menus.add(menu);
				}
			}
		}

		List<String> menuNames = new ArrayList<String>();
		menuNames.add(pageSettings.get("ATTR_menuName_TopNav1"));
		menuNames.add(pageSettings.get("ATTR_menuName_TopNav2"));
		menuNames.add(pageSettings.get("ATTR_menuName_VNav"));
		menuNames.add(pageSettings.get("ATTR_menuName_FooterNav"));

		for (MenuSettings menu : allMenus)
		{
			if (menuNames.contains(menu.getName()))
			{
				menus.add(menu);
			}
		}

		return menus;
	}

	/**
	 * Loads all the menus and it's sub menu items into memory/cache.
	 */
	private void loadAllMenus()
	{
		try
		{
			logger.debug("Loading all menu items.");

			//Load Menu
			Vector<Hashtable<String, String>> menuV = cda.getAllCategories("FLEX3");
			if (menuV.size() > 0)
			{
				Enumeration<Hashtable<String, String>> e = menuV.elements();
				while (e.hasMoreElements())
				{
					Hashtable<String, String> h = e.nextElement();
					MenuSettings menu = new MenuSettings(h);

					//Load Menu Items
					Vector<Hashtable<String, String>> menuItemV = cda.getCategoryElements("FLEX3", menu.getId(), true);
					if (menuItemV.size() > 0)
					{
						Enumeration<Hashtable<String, String>> menuItemE = menuItemV.elements();
						while (menuItemE.hasMoreElements())
						{
							Hashtable<String, String> ih = menuItemE.nextElement();
							MenuItemSettings menuItem = new MenuItemSettings(ih);
							menu.getItems().add(menuItem);

							//Load Sub-menu
							if (menuItem.getOpenModule() != null && (menuItem.getOpenModule().length() > 0))
							{

								Vector<Hashtable<String, String>> openModuleItemV;
								if ("INFORMATION".equals(menuItem.getOpenModule()))
								{
									openModuleItemV = cda.getCategoryElementsByName(menuItem.getOpenModule(), menuItem.getOpenCategory(), true);
								}
								else
								{
									openModuleItemV = cda.getLevelCategoriesWithControl(menuItem.getOpenModule(), "1", false, false, true);
								}

								if ((openModuleItemV != null) && (openModuleItemV.size() > 0))
								{

									Enumeration<Hashtable<String, String>> openModuleItemE = openModuleItemV.elements();
									menuItem.setSubMenuItems(new ArrayList<OpenModuleMenuItem>());

									while (openModuleItemE.hasMoreElements())
									{
										Hashtable<String, String> sih = openModuleItemE.nextElement();
										OpenModuleMenuItem subMenuItem = new OpenModuleMenuItem(sih);
										menuItem.getSubMenuItems().add(subMenuItem);
									}
								}
							}
						}
					}

					allMenus.add(menu);
				}

			}
		}
		catch (Exception e)
		{
			logger.error("Error", e);
		}
	}

	public void clearCache()
	{
		allMenus.clear();
	}

	private String getMenuItemURL(MenuItemSettings item)
	{

		String webModuleURL = item.get("ATTRDROP_webModuleURL");
		String rawURL = item.get("ATTR_URL");
		String siteURL = DEFAUTLS.getWebSiteURL();
		String url = "";

		if ((webModuleURL == null) || (webModuleURL.equals("")))
		{
			if (rawURL == null || rawURL.equals(""))
			{
				url = "#";
			}
			else
			{
				if (rawURL.toUpperCase().startsWith("HTTP") || rawURL.toUpperCase().startsWith("FTP") || rawURL.toUpperCase().startsWith("MAILTO"))
				{
					url = item.get("ATTR_URL");
				}
				else if (rawURL.toUpperCase().startsWith("^"))
				{
					url = siteURL + (item.get("ATTR_URL")).substring(1);
				}
				else
				{
					url = item.get("ATTR_URL");
				}
			}
		}
		else
		{
			url = webModuleURL;
		}

		return url;
	}

	private String getSubMenuItemURL(OpenModuleMenuItem item, String moduleName)
	{

		if ("1".equals(item.get("ATTRCHECK_MakeURL")))
		{
			return item.get("ATTR_URL");
		}
		else if (UIUtils.notEmpy(item.get("ATTR_ShortUrl")))
		{
			return item.get("ATTR_ShortUrl");
		}
		else
		{
			String shortUrl = "/";

			if ("INFORMATION".equals(moduleName))
			{
				shortUrl += "page/";
				String categoryName = shorturlManager.getCateName(moduleName, item.get("Category_id"));
				if (categoryName != null)
				{
					shortUrl += categoryName + "/";
				}

				shortUrl += ShortUrlManager.friendly(item.getName());
			}
			else if ("FLEX11".equals(moduleName))
			{
				shortUrl += "blog/" + item.getId() + "/" + shorturlManager.getCateName(moduleName, item.getId());
			}
			else if ("referencelib".equalsIgnoreCase(moduleName))
			{
				shortUrl += "document-lib/" + shorturlManager.getCateName(moduleName, item.getId());
			}
			else if ("FLEX17".equals(moduleName))
			{
				shortUrl += "surveys/" + shorturlManager.getCateName(moduleName, item.getId());
			}
			else
			{
				shortUrl += ShortUrlManager.friendly(moduleName) + "/" + shorturlManager.getCateName(moduleName, item.getId());
			}

			return shortUrl;
		}

	}
}