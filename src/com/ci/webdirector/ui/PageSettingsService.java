package com.ci.webdirector.ui;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.log4j.Logger;

import webdirector.db.client.ClientDataAccess;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;

/**
 * Generates <code>PageSettings</code> from WebDirector
 * for the view layer.
 * 
 * @author asif
 * 
 */

@SuppressWarnings("unchecked")
public class PageSettingsService
{

	private static Logger logger = Logger.getLogger(PageSettingsService.class);

	private List<PageSettings> allPageSettings = new CopyOnWriteArrayList<PageSettings>();
	private PageSettings DEFAULT_PAGE_SETTINGS;

	private ClientDataAccess cda = new ClientDataAccess();

	private DBaccess dba = new DBaccess();

	/**
	 * Get <code>PageSettings</code> for the given asset.
	 * 
	 * @param assetName
	 * @param settings
	 * @return
	 */
	public PageSettings getPageSettings(String assetName, WDSettings settings)
	{

		if (allPageSettings.isEmpty())
		{
			loadAllPageSettings(settings);
		}

		for (PageSettings pageSettins : allPageSettings)
		{
			if (pageSettins.getName().equalsIgnoreCase(assetName))
			{
				return pageSettins;
			}
		}

		return DEFAULT_PAGE_SETTINGS;
	}

	/**
	 * Loads all PageSettings into memory/cache.
	 * Overrides empty asset level values with default and global settings.
	 * 
	 * @param settings
	 */
	private void loadAllPageSettings(WDSettings settings)
	{
		try
		{
			Hashtable<String, String> globalVariables = settings.getGlobalVariables();

			logger.info("Loading all PageSettings from WebDirector..");

			allPageSettings = new CopyOnWriteArrayList<PageSettings>();

			/** Load Page Settings **/

			Vector<Hashtable<String, String>> defaultPageSettingsV = cda.getElementByName("SMALLBUSINESS", "Default Page Settings");
			Hashtable<String, String> defaultPageSettingsH = defaultPageSettingsV.get(0);
			DEFAULT_PAGE_SETTINGS = new PageSettings(defaultPageSettingsH);

			Vector<Hashtable<String, String>> pageSettingsV = cda.getAllElements("SMALLBUSINESS");
			if (pageSettingsV.size() > 0)
			{
				Enumeration<Hashtable<String, String>> e = pageSettingsV.elements();
				while (e.hasMoreElements())
				{
					Hashtable<String, String> h = e.nextElement();
					PageSettings pageSettings = new PageSettings(h);

					logger.debug(pageSettings.getName());

					//load with default settings where needed
					Enumeration<String> defaultKeys = defaultPageSettingsH.keys();
					while (defaultKeys.hasMoreElements())
					{
						String key = defaultKeys.nextElement();
						String value = defaultPageSettingsH.get(key);
						String assetValue = h.get(key);

						if (!UIUtils.notEmpy(assetValue))
						{
							pageSettings.getAttributes().put(key, value);
						}
						else if ((key != null) && (key.startsWith("ATTRLONG")) && (!UIUtils.fileNotEmpy(assetValue)))
						{
							pageSettings.getAttributes().put(key, value);
						}
					}

					//Override according to global variable settings
					if (!"YES".equals(globalVariables.get("SetContentAreaBackgroundfromDefaultPageSettingsOnly")))
					{
						if (UIUtils.fileNotEmpy(h.get("ATTRFILE_File6")))
						{
							pageSettings.getAttributes().put("ATTRFILE_File6", h.get("ATTRFILE_File6"));
						}
					}
					if (!"YES".equals(globalVariables.get("UseBackgroundImageFromDefaultPageSettings")))
					{
						if (UIUtils.fileNotEmpy(h.get("ATTRFILE_File2")))
						{
							pageSettings.getAttributes().put("ATTRFILE_File2", h.get("ATTRFILE_File2"));
						}
					}
					if (!"YES".equals(globalVariables.get("UseBackgroundImageFromDefaultPageSettings")))
					{
						if (UIUtils.notEmpy(h.get("ATTR_BG_Repeat")))
						{
							pageSettings.getAttributes().put("ATTR_BG_Repeat", h.get("ATTR_BG_Repeat"));
						}
					}
					if (!"YES".equals(globalVariables.get("UseBackgroundImageFromDefaultPageSettings")))
					{
						if (UIUtils.notEmpy(h.get("ATTR_BG_Position")))
						{
							pageSettings.getAttributes().put("ATTR_BG_Position", h.get("ATTR_BG_Position"));
						}
					}
					if (!"YES".equals(globalVariables.get("SetContentAreaBackgroundfromDefaultPageSettingsOnly")))
					{
						if (UIUtils.notEmpy(h.get("ATTR_ContentAreaImageBG_Repeat")))
						{
							pageSettings.getAttributes().put("ATTR_ContentAreaImageBG_Repeat", h.get("ATTR_ContentAreaImageBG_Repeat"));
						}
					}
					if (!"YES".equals(globalVariables.get("SetContentAreaBackgroundfromDefaultPageSettingsOnly")))
					{
						if (UIUtils.notEmpy(h.get("ATTR_ContentAreaImageBG_Position")))
						{
							pageSettings.getAttributes().put("ATTR_ContentAreaImageBG_Position", h.get("ATTR_ContentAreaImageBG_Position"));
						}
					}

					/** Load Head UCI **/
					Vector<String> headNotesV = (Vector<String>) dba.selectQuerySingleCol("select content from NOTES where fieldName like '%ATTRNOTES_UCI_HEAD%' and foriegnKey = (select Element_id from elements_smallbusiness where ATTR_Headline = 'Default Page Settings' and live='1' and (live_date is null or live_date <= now()) and (expire_date is null or expire_date >= now() ) limit 1)");
					if (headNotesV != null)
					{
						for (String note : headNotesV)
						{
							if (note != null)
							{
								pageSettings.getHeadUCI().add(note);
							}
						}
					}

					headNotesV = (Vector<String>) dba.selectQuerySingleCol("select content from NOTES where fieldName like '%ATTRNOTES_UCI_HEAD%' and foriegnKey = (select Element_id from elements_smallbusiness where ATTR_Headline = "
							+ "? and live='1' and (live_date is null or live_date <= now()) and (expire_date is null or expire_date >= now() ) limit 1)", new String[] { pageSettings.getName() });

					if (headNotesV != null)
					{
						for (String note : headNotesV)
						{
							if (note != null)
							{
								pageSettings.getHeadUCI().add(note);
							}
						}
					}

					/** Load Body UCI **/
					Vector<String> defaultBodyNotesV = (Vector<String>) dba.selectQuerySingleCol("select content from NOTES where fieldName like '%ATTRNOTES_UCI_BODY%' and foriegnKey = (select Element_id from elements_smallbusiness where ATTR_Headline = 'Default Page Settings' and live='1' and (live_date is null or live_date <= now()) and (expire_date is null or expire_date >= now() ) limit 1)");
					if (defaultBodyNotesV != null)
					{
						for (String note : defaultBodyNotesV)
						{
							if (note != null)
							{
								pageSettings.getBodyUCI().add(note);
							}
						}
					}

					Vector<String> assetNotesV = (Vector<String>) dba.selectQuerySingleCol("select content from NOTES where fieldName like '%ATTRNOTES_UCI_BODY%' and foriegnKey = (select Element_id from elements_smallbusiness where ATTR_Headline = "
							+ "? and live='1' and (live_date is null or live_date <= now()) and (expire_date is null or expire_date >= now() ) limit 1)", new String[] { pageSettings.getName() });

					if (assetNotesV != null)
					{
						for (String note : assetNotesV)
						{
							if (note != null)
							{
								pageSettings.getBodyUCI().add(note);
							}
						}
					}

					allPageSettings.add(pageSettings);
				}
			}

			logger.info("Total PageSettings Loaded: " + allPageSettings.size());
		}
		catch (Exception e)
		{
			logger.error("Error", e);
		}
	}

	public void clearCache()
	{
		allPageSettings.clear();
	}

}
