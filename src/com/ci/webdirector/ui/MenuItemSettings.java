package com.ci.webdirector.ui;

import java.util.Hashtable;
import java.util.List;

public class MenuItemSettings extends AbstractElementSettings
{

	public MenuItemSettings(Hashtable<String, String> attributes)
	{
		super(attributes);

		this.image = get("ATTRFILE_File2");
		this.rolloverImage = get("ATTRFILE_File1");
		this.openInNewWindow = "1".equals(get("ATTRCHECK_OpenNewWindow")) ? true : false;
		this.hideIfNotLoggedOn = "1".equals(get("ATTRCHECK_HideIfNotLoggedOn")) ? true : false;
		this.hideWhenLoggedOn = "1".equals(get("ATTRCHECK_HideWhenLoggedOn")) ? true : false;
		this.hideMenuName = "1".equals(get("ATTRCHECK_hideMenuName")) ? true : false;
		this.doNotHyperlink = "1".equals(get("ATTRCHECK_YesNo1")) ? true : false;
		this.noFollow = "1".equals(get("ATTRCHECK_NoFollow")) ? true : false;
		this.title = get("ATTR_LinkTitle");
		this.altText = get("ATTR_AltTag");
		this.openModule = get("ATTRDROP_webModule");
		this.openCategory = get("ATTR_openCategoryName");
		this.displayMode = get("ATTRDROP_displayMode");
	}

	private String url;
	private String image;
	private String rolloverImage;
	private boolean hideMenuName;
	private boolean doNotHyperlink;
	private boolean openInNewWindow;
	private String title;
	private boolean noFollow;
	private String altText;
	private boolean hideIfNotLoggedOn;
	private boolean hideWhenLoggedOn;
	private String openModule;
	private String openCategory;
	private String displayMode;
	private boolean active;

	private List<OpenModuleMenuItem> subMenuItems;

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getImage()
	{
		return image;
	}

	public void setImage(String image)
	{
		this.image = image;
	}

	public String getRolloverImage()
	{
		return rolloverImage;
	}

	public void setRolloverImage(String rolloverImage)
	{
		this.rolloverImage = rolloverImage;
	}

	public boolean isHideMenuName()
	{
		return hideMenuName;
	}

	public void setHideMenuName(boolean hideMenuName)
	{
		this.hideMenuName = hideMenuName;
	}

	public boolean isDoNotHyperlink()
	{
		return doNotHyperlink;
	}

	public void setDoNotHyperlink(boolean doNotHyperlink)
	{
		this.doNotHyperlink = doNotHyperlink;
	}

	public boolean isOpenInNewWindow()
	{
		return openInNewWindow;
	}

	public void setOpenInNewWindow(boolean openInNewWindow)
	{
		this.openInNewWindow = openInNewWindow;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public boolean isNoFollow()
	{
		return noFollow;
	}

	public void setNoFollow(boolean noFollow)
	{
		this.noFollow = noFollow;
	}

	public String getAltText()
	{
		return altText;
	}

	public void setAltText(String altText)
	{
		this.altText = altText;
	}

	public boolean isHideIfNotLoggedOn()
	{
		return hideIfNotLoggedOn;
	}

	public void setHideIfNotLoggedOn(boolean hideIfNotLoggedOn)
	{
		this.hideIfNotLoggedOn = hideIfNotLoggedOn;
	}

	public boolean isHideWhenLoggedOn()
	{
		return hideWhenLoggedOn;
	}

	public void setHideWhenLoggedOn(boolean hideWhenLoggedOn)
	{
		this.hideWhenLoggedOn = hideWhenLoggedOn;
	}

	public List<OpenModuleMenuItem> getSubMenuItems()
	{
		return subMenuItems;
	}

	public void setSubMenuItems(List<OpenModuleMenuItem> subMenuItems)
	{
		this.subMenuItems = subMenuItems;
	}

	public String getOpenModule()
	{
		return openModule;
	}

	public void setOpenModule(String openModule)
	{
		this.openModule = openModule;
	}

	public String getOpenCategory()
	{
		return openCategory;
	}

	public void setOpenCategory(String openCategory)
	{
		this.openCategory = openCategory;
	}

	public String getDisplayMode()
	{
		return displayMode;
	}

	public void setDisplayMode(String displayMode)
	{
		this.displayMode = displayMode;
	}
}
