package com.ci.webdirector.ui;

import java.util.Hashtable;
import java.util.List;

public class WDSettings
{

	private String assetType;
	private String assetId;
	private String moduleName;
	private String categoryId;

	private String pageTitle;
	private String pageDescription;

	private Hashtable<String, String> globalVariables;
	private Hashtable<String, String> moduleVariables;
	private PageSettings pageSettings;
	private List<MenuSettings> menus;

	public PageSettings getPageSettings()
	{
		return pageSettings;
	}

	public void setPageSettings(PageSettings pageSettings)
	{
		this.pageSettings = pageSettings;
	}

	public Hashtable<String, String> getGlobalVariables()
	{
		return globalVariables;
	}

	public void setGlobalVariables(Hashtable<String, String> globalVariables)
	{
		this.globalVariables = globalVariables;
	}

	public Hashtable<String, String> getModuleVariables()
	{
		return moduleVariables;
	}

	public void setModuleVariables(Hashtable<String, String> moduleVariables)
	{
		this.moduleVariables = moduleVariables;
	}

	public String getAssetType()
	{
		return assetType;
	}

	public void setAssetType(String assetType)
	{
		this.assetType = assetType;
	}

	public String getAssetId()
	{
		return assetId;
	}

	public void setAssetId(String assetId)
	{
		this.assetId = assetId;
	}

	public String getModuleName()
	{
		return moduleName;
	}

	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}

	public String getCategoryId()
	{
		return categoryId;
	}

	public void setCategoryId(String categoryId)
	{
		this.categoryId = categoryId;
	}

	public String getPageTitle()
	{
		return pageTitle;
	}

	public void setPageTitle(String pageTitle)
	{
		this.pageTitle = pageTitle;
	}

	public String getPageDescription()
	{
		return pageDescription;
	}

	public void setPageDescription(String pageDescription)
	{
		this.pageDescription = pageDescription;
	}

	public MenuSettings getMenu(String menuName)
	{

		if ((menus != null) && (!menus.isEmpty()))
		{
			for (MenuSettings menu : menus)
			{
				if (menu.getName().equals(menuName))
				{
					return menu;
				}
			}
		}

		return null;
	}

	public void setMenus(List<MenuSettings> menus)
	{
		this.menus = menus;
	}

}
