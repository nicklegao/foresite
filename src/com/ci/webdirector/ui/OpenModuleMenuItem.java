package com.ci.webdirector.ui;

import java.util.Hashtable;

public class OpenModuleMenuItem extends AbstractPersistentSettings
{

	private String id;
	private String name;
	private String url;
	private boolean hideIfNotLoggedOn;

	public OpenModuleMenuItem(Hashtable<String, String> attributes)
	{
		super(attributes);

		if (attributes.get("Element_id") != null)
		{
			this.id = attributes.get("Element_id");
			this.name = attributes.get("ATTR_Headline");
		}
		else
		{
			this.id = attributes.get("Category_id");
			this.name = attributes.get("ATTR_categoryName");
		}
		this.hideIfNotLoggedOn = "1".equals(attributes.get("ATTRCHECK_Private"));
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public boolean isHideIfNotLoggedOn()
	{
		return hideIfNotLoggedOn;
	}

}
