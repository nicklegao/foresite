package com.ci.webdirector.ui;

import java.util.Hashtable;

import au.net.webdirector.common.Defaults;
import webdirector.db.client.ClientFileUtils;

public abstract class AbstractPersistentSettings
{

	private ClientFileUtils clientFileUtils = new ClientFileUtils();
	private Defaults DEFAUTLS = Defaults.getInstance();
	private Hashtable<String, String> attributes;

	public AbstractPersistentSettings(Hashtable<String, String> attributes)
	{
		this.attributes = attributes;
	}

	public Hashtable<String, String> getAttributes()
	{
		return attributes;
	}

	public void setAttributes(Hashtable<String, String> attributes)
	{
		this.attributes = attributes;
	}

	public String get(String key)
	{

		if (this.attributes != null)
		{
			return this.attributes.get(key);
		}

		return null;
	}

	public String getFileContent(String key)
	{
		String content = "";

		String filePath = this.get(key);
		if (filePath != null)
		{
			content = clientFileUtils.readTextContents(clientFileUtils.getFilePathFromStoreName(filePath)).toString();
		}

		return content;
	}

	public String getStorePath(String key)
	{
		String path = "";

		String filePath = this.get(key);
		if (filePath != null)
		{
			path = "/" + DEFAUTLS.getStoreContext() + filePath;
		}

		return path;
	}
}
