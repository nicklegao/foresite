package com.ci.webdirector.ui;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class PageSettings extends AbstractElementSettings
{

	private List<String> headUCI = new ArrayList<String>();
	private List<String> bodyUCI = new ArrayList<String>();

	public PageSettings(Hashtable<String, String> attributes)
	{
		super(attributes);
	}

	public List<String> getHeadUCI()
	{
		return headUCI;
	}

	public void setHeadUCI(List<String> headUCI)
	{
		this.headUCI = headUCI;
	}

	public List<String> getBodyUCI()
	{
		return bodyUCI;
	}

	public void setBodyUCI(List<String> bodyUCI)
	{
		this.bodyUCI = bodyUCI;
	}

	@Override
	public String toString()
	{

		if (getAttributes() != null)
		{
			return getAttributes().toString();
		}

		return super.toString();
	}

}
