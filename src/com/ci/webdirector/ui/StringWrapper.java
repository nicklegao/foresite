package com.ci.webdirector.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

public class StringWrapper
{

	private int delta = 10;

	private int length = 70;

	private String origin;

	private String wordSeperator = " ";

	public static void main(String[] args)
	{
		String attr_description = "01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789";
		// StringWrapper sw = new StringWrapper(attr_description);
		// System.out.println(sw.wrapLine("\n"));

		StringBuffer showDesc = new StringBuffer();
		StringTokenizer st = new StringTokenizer("attr_description", "\n");
		while (st.hasMoreTokens())
		{
			String line = st.nextToken();
			com.ci.webdirector.ui.StringWrapper sw = new com.ci.webdirector.ui.StringWrapper(
					attr_description);
			sw.setLength(75);
			sw.setDelta(5);
			showDesc.append(sw.wrapLine("<br>")).append("<br>");
		}
		showDesc.delete(showDesc.length() - 4, showDesc.length());

		test();


	}

	public void setLength(int length)
	{
		this.length = length;
	}

	public void setDelta(int delta)
	{
		this.delta = delta;
	}

	public void setWordSeperator(String wordSeperator)
	{
		this.wordSeperator = wordSeperator;
	}

	public StringWrapper(String origin)
	{
		super();
		this.origin = origin;
	}

	public String wrapLine(String lineSeperator)
	{
		StringBuffer sb = new StringBuffer();
		StringTokenizer st = new StringTokenizer(origin, wordSeperator);
		int currentLength = 0;
		while (st.hasMoreTokens())
		{
			String word = st.nextToken();
			if ((currentLength + word.length() + wordSeperator.length()) < length)
			{
				currentLength = currentLength + word.length()
						+ wordSeperator.length();
				sb.append(word).append(wordSeperator);
				continue;
			}
			if ((currentLength + word.length()) <= (length + delta))
			{
				sb.append(word).append(lineSeperator);
				currentLength = 0;
				continue;
			}
			int breakPos = 0;
			StringBuffer wordSB = new StringBuffer(word);
			while ((breakPos += length + delta - currentLength) < word.length())
			{
				System.out.println(breakPos);
				System.out.println(word.length());
				wordSB.insert(breakPos, lineSeperator);
				breakPos += lineSeperator.length();
				currentLength = 0;
			}
			sb.append(wordSB);
			currentLength = wordSB.length() - breakPos + lineSeperator.length();
		}
		return sb.toString();
	}

	public static void test()
	{
		try
		{
			Date eventStart = new SimpleDateFormat("yyyy-MM-dd").parse("2012-02-05");
			Date eventEnd = new SimpleDateFormat("yyyy-MM-dd").parse("2012-02-27");
			Calendar c = Calendar.getInstance();
			c.setTime(eventStart);
			Calendar e = Calendar.getInstance();
			e.setTime(eventEnd);

			while (!c.after(e))
			{
				System.out.println(c.getTime());
//				c.add(Calendar.WEEK_OF_YEAR, 1);
				c.add(Calendar.DATE, 1);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		/**
		 * String start = "2010-03-03";
		 * String end = "2010-04-09";
		 * List result = new ArrayList();
		 * 
		 * String[] s = start.split("-");
		 * String[] e = end.split("-");
		 * Calendar sc = Calendar.getInstance();
		 * 
		 * sc.set(Calendar.YEAR, Integer.parseInt(s[0]));
		 * sc.set(Calendar.MONTH, Integer.parseInt(s[1]) - 1);
		 * sc.set(Calendar.DAY_OF_MONTH, Integer.parseInt(s[2]));
		 * sc.set(Calendar.HOUR, 0);
		 * sc.set(Calendar.MINUTE, 0);
		 * sc.set(Calendar.SECOND, 0);
		 * sc.set(Calendar.MILLISECOND, 0);
		 * 
		 * Calendar ec = Calendar.getInstance();
		 * ec.set(Calendar.YEAR, Integer.parseInt(e[0]));
		 * ec.set(Calendar.MONTH, Integer.parseInt(e[1]) - 1);
		 * ec.set(Calendar.DAY_OF_MONTH, Integer.parseInt(e[2]));
		 * ec.set(Calendar.HOUR, 0);
		 * ec.set(Calendar.MINUTE, 0);
		 * ec.set(Calendar.SECOND, 0);
		 * ec.set(Calendar.MILLISECOND, 0);
		 * 
		 * while (!sc.after(ec)) {
		 * System.out.println(sc.getTime());
		 * sc.add(Calendar.DAY_OF_MONTH, 1);
		 * for (int i = 0; i < result.size(); i++) {
		 * Hashtable item = (Hashtable) result.get(i);
		 * String[] as = ((String) item.get("attrdate_start")).split("-");
		 * String[] ae = ((String) item.get("attrdate_end")).split("-");
		 * Calendar asc = Calendar.getInstance();
		 * 
		 * asc.set(Calendar.YEAR, Integer.parseInt(as[0]));
		 * asc.set(Calendar.MONTH, Integer.parseInt(as[1]) - 1);
		 * asc.set(Calendar.DAY_OF_MONTH, Integer.parseInt(as[2]));
		 * asc.set(Calendar.HOUR, 0);
		 * asc.set(Calendar.MINUTE, 0);
		 * asc.set(Calendar.SECOND, 0);
		 * asc.set(Calendar.MILLISECOND, 0);
		 * 
		 * Calendar aec = Calendar.getInstance();
		 * aec.set(Calendar.YEAR, Integer.parseInt(ae[0]));
		 * aec.set(Calendar.MONTH, Integer.parseInt(ae[1]) - 1);
		 * aec.set(Calendar.DAY_OF_MONTH, Integer.parseInt(ae[2]));
		 * aec.set(Calendar.HOUR, 0);
		 * aec.set(Calendar.MINUTE, 0);
		 * aec.set(Calendar.SECOND, 0);
		 * aec.set(Calendar.MILLISECOND, 0);
		 * if (!sc.before(asc) && !sc.after(aec)) {
		 * System.out.println(sc.getTime());
		 * break;
		 * }
		 * }
		 * }
		 */
	}

	public String getMonthName(String d)
	{

		String[] fullMonthNameArray = new String[] { "January", "Feburary",
				"March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };
		int month = Integer.parseInt(d.split("-")[1]);
		return fullMonthNameArray[month - 1];
	}

}
