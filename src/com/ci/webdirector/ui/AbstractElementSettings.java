package com.ci.webdirector.ui;

import java.util.Hashtable;

public class AbstractElementSettings extends AbstractPersistentSettings
{

	private String id;
	private String name;
	private String categoryId;

	public AbstractElementSettings(Hashtable<String, String> attributes)
	{
		super(attributes);
		this.id = attributes.get("Element_id");
		this.name = attributes.get("ATTR_Headline");
		this.categoryId = attributes.get("Category_id");
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getCategoryId()
	{
		return categoryId;
	}

	public void setCategoryId(String categoryId)
	{
		this.categoryId = categoryId;
	}

}
