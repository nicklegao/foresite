package com.ci.webdirector.ui;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class MenuSettings extends AbstractCategorySettings
{

	public MenuSettings(Hashtable<String, String> attributes)
	{
		super(attributes);
	}

	private List<MenuItemSettings> items = new ArrayList<MenuItemSettings>();

	public List<MenuItemSettings> getItems()
	{
		return items;
	}

	public void setItems(List<MenuItemSettings> items)
	{
		this.items = items;
	}

}
