package com.ci.webdirector.ui;

import java.util.Hashtable;

public class AbstractCategorySettings extends AbstractPersistentSettings
{

	private String id;
	private String name;

	public AbstractCategorySettings(Hashtable<String, String> attributes)
	{
		super(attributes);
		this.id = attributes.get("Category_id");
		this.name = attributes.get("ATTR_categoryName");
	}

	public String getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}
}
