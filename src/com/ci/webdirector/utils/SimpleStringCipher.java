package com.ci.webdirector.utils;

import java.net.URLEncoder;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

/**
 * A simple text cipher to encrypt/decrypt a string.
 * 
 * @author asif
 *
 */

public class SimpleStringCipher
{

	private static Logger logger = Logger.getLogger(SimpleStringCipher.class);

	private static String secret = "cibw63sdg9gh9275"; // secret key length must be 16
	private static SecretKey key;
	private static Cipher cipher;
	private static Base64 coder;

	static
	{
		try
		{
			key = new SecretKeySpec(secret.getBytes(), "AES");
			cipher = Cipher.getInstance("AES/ECB/PKCS5Padding", "SunJCE");
			coder = new Base64();
		}
		catch (Throwable t)
		{
			t.printStackTrace();
			logger.error(t);
		}
	}

	/**
	 * Encrypt the given text and optionally apply URL encoding.
	 * 
	 * @param plainText
	 * @param urlEncode
	 * @return
	 */
	public static synchronized String encrypt(String plainText, boolean urlEncode)
	{
		byte[] cipherText;
		try
		{
			cipher.init(Cipher.ENCRYPT_MODE, key);
			cipherText = cipher.doFinal(plainText.getBytes());
			String enctryptedText = new String(coder.encode(cipherText));

			if (urlEncode)
			{
				enctryptedText = URLEncoder.encode(enctryptedText, "UTF-8");
			}

			return enctryptedText;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e);
		}

		return null;
	}

	/**
	 * Decrypt the given text.
	 * 
	 * @param codedText
	 * @return
	 */
	public static synchronized String decrypt(String codedText)
	{
		byte[] encypted = coder.decode(codedText.getBytes());
		try
		{
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] decrypted = cipher.doFinal(encypted);
			return new String(decrypted);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e);
		}
		return null;
	}

}