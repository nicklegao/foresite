/**
 * 2:42:11 PM 08/02/2012
 * @author Vito Lefemine
 */
package com.ci.webdirector.usersUtils;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Vito Lefemine
 *
 */
@Controller
public class UserChecks
{
	static Logger logger = Logger.getLogger(UserChecks.class);

	@RequestMapping("/userChecks/allowConfig")
	@ResponseBody
	public String isUserAllowedToConfigModule(HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		logger.info("isUserAllowedToConfigModule " + session.getAttribute("userAdmin"));
		if (null != session.getAttribute("userAdmin") && ((Boolean) session.getAttribute("userAdmin")))
		{
			return "" + true;
		}
		else
			return "" + false;

	}

	@RequestMapping("/userChecks/allowCreateCategories")
	@ResponseBody
	public String isUserAllowedToCreateCategories(HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		Enumeration e = session.getAttributeNames();
		while (e.hasMoreElements())
		{
			String name = (String) e.nextElement();
			logger.info(name + "=" + session.getAttribute(name));
		}
		if (null != session.getAttribute("userAdmin") && ((Boolean) session.getAttribute("userAdmin")))
		{
			return "" + true;
		}
		else
			return "" + false;
	}
}
