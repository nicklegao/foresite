package com.ci.webdirector.jsonUtils;

/**
 * @author Vito Lefemine
 *
 */
import java.security.InvalidParameterException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.utils.module.DropDownBuilder;

import com.ci.webdirector.wdjson.model.JsonAttributes;
import com.ci.webdirector.wdjson.model.JsonAttributesResponse;

public class JsonDbUtils
{
	static Logger logger = Logger.getLogger(JsonDbUtils.class);

	public static Hashtable<String, String> getColumnsForModule(String moduleName)
	{
		return getColumnsForModule(moduleName, "Elements");
	}

	public static Hashtable<String, String> getColumnsForModule(String moduleName, String tableType)
	{
		Hashtable<String, String> ret = new Hashtable<String, String>();
		try
		{

			DBaccess dba = new DBaccess();
			Vector<String[]> res = dba.getColsMetaData(tableType + "_" + moduleName);

			/*
			 * dbStruct[0] = rsmd.getColumnName(i); dbStruct[1] =
			 * rsmd.getColumnTypeName(i); dbStruct[2] =
			 * String.valueOf(rsmd.getColumnDisplaySize(i));
			 */
			for (String[] record : res)
			{
				ret.put(record[0], record[1]);
				logger.debug("Adding column " + record[0] + " type:: " + record[1]);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return ret;
	}

	public static JsonAttributesResponse getColumnsInternalExtenalForModule(String moduleName)
	{
		return getColumnsInternalExtenalForModule(moduleName, "elements");
	}

	public static JsonAttributesResponse getColumnsInternalExtenalForModule(String moduleName, String tableType)
	{
		JsonAttributesResponse ret = new JsonAttributesResponse();
		String labelTablePrefix = "labels";
		String tablePrefix = "elements";
		if ("categories".equals(tableType))
		{
			labelTablePrefix = "categorylabels";
			tablePrefix = "categories";
		}
		try
		{

			DBaccess dba = new DBaccess();
			Connection conn = dba.getDBConnection();
			String sqlCheck = "select * from " + labelTablePrefix + "_" + moduleName + " where Label_onOff=1 order by Label_ExternalName";

			PreparedStatement prst = conn.prepareStatement(sqlCheck);
			ResultSet rst = prst.executeQuery();
			LinkedHashMap<String, JsonAttributes> columns = new LinkedHashMap<String, JsonAttributes>();
			/*
			 * Get columns meta data
			 */
			int idx = 0;
			while (rst.next())
			{
				JsonAttributes ja = new JsonAttributes();
				ja.setName(rst.getString("Label_ExternalName"));
				ja.setInternalName(rst.getString("Label_InternalName"));
				if (ja.getInternalName().startsWith("ATTRDROP_") || ja.getInternalName().startsWith("ATTRMULTI_"))
				{
					Iterator<Entry<String, String>> entries = new DropDownBuilder().getDropDownValuesWithoutId(moduleName, ja.getInternalName(), tablePrefix).entrySet().iterator();
					List<Pair> pairs = new ArrayList<Pair>();
					while (entries.hasNext())
					{
						Map.Entry<String, String> entry = (Map.Entry<String, String>) entries.next();
						pairs.add(new Pair(entry));
					}
					Collections.sort(pairs);
					List<Map<String, String>> options = new ArrayList<Map<String, String>>();

					for (int i = 0; i < pairs.size(); i++)
					{
						Map.Entry<String, String> entry = pairs.get(i).getEntry();
						Map<String, String> option = new HashMap<String, String>();
						option.put("value", entry.getKey());
						option.put("text", entry.getValue());
						options.add(option);
					}
					ja.setOptions(options);
				}
				columns.put(rst.getString("Label_InternalName"), ja);
			}

			ret.setAttributes(columns);
			if (null != rst)
				rst.close();
			String sqlDataMeta = "select " + StringUtils.join(columns.keySet(), ",") + " from " + tablePrefix + "_" + moduleName + " where 1!=1";
			prst = conn.prepareStatement(sqlDataMeta);
			rst = prst.executeQuery();
			ResultSetMetaData meta = rst.getMetaData();
			for (int i = 0; i < meta.getColumnCount(); i++)
			{
				String name = meta.getColumnName(i + 1);
				String type = meta.getColumnTypeName(i + 1);
				int size = meta.getColumnDisplaySize(i + 1);

				if (columns.containsKey(name))
				{
					columns.get(name).setType(type);
					columns.get(name).setSize(size);
				}
			}
			if (null != rst)
				rst.close();
			if (null != conn)
				conn.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return ret;
	}

	public static String getSelectAttributeWithAttrList(List<String> attributes, boolean elements, String moduleName)
	{
		String select = "";
		try
		{
			if (null != attributes)
			{
				int idx = 0;
				boolean attributeAdded = false;
				for (String attribute : JsonDbUtils.getExistingAttribute(Collections.enumeration(attributes), getColumnsForModule(moduleName)))
				{
					idx += 1;
					attributeAdded = true;
					select += " " + attribute + (idx <= attributes.size() - 1 ? ", " : " ");
				}
				select = select.trim();
				if (select.charAt(select.length() - 1) == ',')
					select = select.substring(0, select.length() - 1);

				if (!attributeAdded && elements)
					select += " Attr_Headline ";
				if (!attributeAdded && !elements)
					select += " Attr_CategoryName ";
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		return select;

	}

	/**
	 * @param requested
	 * @param columns
	 *            the columns in the module
	 * @return
	 */
	public static Vector<String> getExistingAttribute(Enumeration<String> requested, Hashtable<String, String> columns)
	{
		Vector<String> existingAttributes = new Vector<String>();
		while (requested.hasMoreElements())
		{
			String sortKey = requested.nextElement();
			if (columns.containsKey(sortKey))
			{
				existingAttributes.add(sortKey);
			}
		}
		return existingAttributes;
	}

}

class Pair implements Comparable
{
	private Map.Entry<String, String> entry;

	public Pair(Map.Entry<String, String> e)
	{
		this.entry = e;
	}

	public Map.Entry<String, String> getEntry()
	{
		return this.entry;
	}

	public int compareTo(Object o)
	{
		if (!(o instanceof Pair))
		{
			throw new InvalidParameterException();
		}
		String value = (String) getEntry().getValue();
		String another = (String) ((Pair) o).getEntry().getValue();
		return value.compareTo(another);
	}

}