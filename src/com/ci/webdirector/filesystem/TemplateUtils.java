package com.ci.webdirector.filesystem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.net.webdirector.common.Defaults;
import webdirector.db.client.ClientFileUtils;

public class TemplateUtils
{
	Defaults d = Defaults.getInstance();

	public List<Template> getTemplates()
	{
		List<Template> templates = new ArrayList<Template>();
		File templatesDir = new File(d.getStoreDir(), "_templates");
		if (!templatesDir.exists())
		{
			return templates;
		}
		File[] fileList = templatesDir.listFiles();
		ClientFileUtils cfu = new ClientFileUtils();
		for (File file : fileList)
		{
			Template t = new Template();
			t.setDescription("");
			t.setHtml(cfu.readTextContents(file.getAbsolutePath()).toString());
			t.setTitle(file.getName());
			templates.add(t);
		}
		return templates;
	}

	public String getJsonTemplates()
	{
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			return mapper.writeValueAsString(getTemplates());
		}
		catch (Exception e)
		{
			throw new RuntimeException("Cannot convert to JSON", e);
		}
	}

	public boolean deleteTemplate(String fileName)
	{
		File templatesDir = new File(d.getStoreDir(), "_templates");
		if (!templatesDir.exists())
		{
			return true;
		}
		File template = new File(templatesDir, fileName);
		if (!template.exists())
		{
			return true;
		}
		return template.delete();
	}
}
