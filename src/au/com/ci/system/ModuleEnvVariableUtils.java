package au.com.ci.system;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import sbe.ShortUrls.ShortUrlManager;
import webdirector.db.client.ClientDataAccess;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;

public class ModuleEnvVariableUtils
{

	protected static final String ENV_PREFIX = "au.com.ci.sbe.env";
	public static final String ENV_MODULE_NAME = "au.com.ci.sbe.env.module";
	public static final String ENV_CATEGORY_ID = "au.com.ci.sbe.env.categoryID";
	public static final String ENV_CHILD_CATEGORY_ID = "au.com.ci.sbe.env.childCategoryID";
	public static final String ENV_CATEGORY_FRIENDLY_NAME = "au.com.ci.sbe.env.categoryFriendlyName";
	public static final String ENV_CHILD_CATEGORY_FRIENDLY_NAME = "au.com.ci.sbe.env.childCategoryFriendlyName";
	public static final String ENV_CATEGORY_NAME = "au.com.ci.sbe.env.categoryName";
	public static final String ENV_CHILD_CATEGORY_NAME = "au.com.ci.sbe.env.childCategoryName";

	public static final String ENV_ELEMENT_ID = "au.com.ci.sbe.env.elementID";
	public static final String ENV_ELEMENT_FRIENDLY_NAME = "au.com.ci.sbe.env.elementFriendlyName";
	public static final String ENV_ELEMENT_NAME = "au.com.ci.sbe.env.elementName";

	private static Map<String, String> moduleUrls;
	private static Logger logger = Logger.getLogger(ModuleEnvVariableUtils.class);

	static
	{
		refresh();
	}

	public static void refresh()
	{
		moduleUrls = new HashMap<String, String>();
		List<String[]> modules = new DBaccess().selectQuery("select c.attr_categoryName, e.attr_value from categories_flex2 c, elements_flex2 e where c.category_id = e.category_id and e.ATTR_Headline = 'Module - Short Mapping' and e.live='1' and (e.live_date is null or e.live_date <= now() and (e.expire_date is null or e.expire_date >= now()))");
		for (String[] module : modules)
		{
			moduleUrls.put(module[0], module[1]);
		}
	}

	public static void setVariables(HttpServletRequest request)
	{
		String uri = request.getRequestURI();
		if (StringUtils.isNotBlank((String) request.getAttribute("javax.servlet.forward.request_uri")))
		{
			setIdAndNames(request);
			return;
		}
		Defaults d = Defaults.getInstance();
		for (Entry<String, String> module : moduleUrls.entrySet())
		{
			String pattern = module.getValue();
			if (uri.startsWith(pattern) || uri.startsWith("/" + d.getWarName() + pattern))
			{
				request.setAttribute(ENV_MODULE_NAME, module.getKey());
				return;
			}
		}
	}

	public static void printVariables(HttpServletRequest request)
	{
		Enumeration<String> names = (Enumeration<String>) request.getAttributeNames();
		while (names.hasMoreElements())
		{
			String name = names.nextElement();
			if (name.startsWith(ENV_PREFIX))
			{
				logger.info(name + " = " + request.getAttribute(name));
			}
		}
	}

	private static void setIdAndNames(HttpServletRequest request)
	{
		String module = (String) request.getAttribute(ENV_MODULE_NAME);
		if (StringUtils.isBlank(module))
		{
			return;
		}
		IdAndNamesSetter setter = null;
		if (StringUtils.equals("PRODUCTS", module))
		{
			setter = new ProductsIdAndNamesSetter();
		}
		else
		{
			setter = new IdAndNamesSetter(module);
		}

		setter.setCategoryParameter(request);
		setter.setAssetParameter(request);

	}

	private static class IdAndNamesSetter
	{
		String module;
		ShortUrlManager sum;
		ClientDataAccess da;

		IdAndNamesSetter(String module)
		{
			this.module = module;
			this.sum = ShortUrlManager.getInstance();
			this.da = new ClientDataAccess();
		}

		void setCategoryParameter(HttpServletRequest request)
		{
			String _cateName = request.getParameter("categoryName");
			String _childCategoryName = request.getParameter("childCategoryName");

			String categoryID = request.getParameter("categoryID");
			String childCategoryID = request.getParameter("childCategoryID");

			if (null != categoryID)
			{
				_cateName = sum.getCateName(module, categoryID);
			}
			else if (null != _cateName)
			{
				categoryID = sum.getCateID(module, _cateName);
			}
			if (null != childCategoryID)
			{
				_childCategoryName = sum.getCateName(module, childCategoryID);
			}
			else if (null != _childCategoryName)
			{
				childCategoryID = sum.getCateID(module, _cateName, _childCategoryName);
			}

			String cateName = da.getCategoryFromId(module, categoryID);
			String childCategoryName = da.getCategoryFromId(module, childCategoryID);

			request.setAttribute(ENV_CATEGORY_ID, categoryID);
			request.setAttribute(ENV_CHILD_CATEGORY_ID, childCategoryID);
			request.setAttribute(ENV_CATEGORY_FRIENDLY_NAME, _cateName);
			request.setAttribute(ENV_CHILD_CATEGORY_FRIENDLY_NAME, _childCategoryName);
			request.setAttribute(ENV_CATEGORY_NAME, cateName);
			request.setAttribute(ENV_CHILD_CATEGORY_NAME, childCategoryName);


		}

		void setAssetParameter(HttpServletRequest request)
		{
			String elementID = request.getParameter("elementID");
			String _elementName = (String) request.getParameter("elementName");
			if (null != elementID)
			{
				_elementName = sum.getAssetName(module, elementID);
			}
			else if (null != _elementName)
			{
				elementID = sum.getAssetID(module, (String) request.getAttribute(ENV_CATEGORY_FRIENDLY_NAME), (String) request.getAttribute(ENV_CHILD_CATEGORY_FRIENDLY_NAME));
			}

			String elementName = da.getElementName(elementID, module);

			request.setAttribute(ENV_ELEMENT_ID, elementID);
			request.setAttribute(ENV_ELEMENT_FRIENDLY_NAME, _elementName);
			request.setAttribute(ENV_ELEMENT_NAME, elementName);
		}

	}

	private static class ProductsIdAndNamesSetter extends IdAndNamesSetter
	{
		ProductsIdAndNamesSetter()
		{
			super("PRODUCTS");
		}

		@Override
		void setAssetParameter(HttpServletRequest request)
		{
			String elementID = request.getParameter("elementID");
			String _elementName = (String) request.getParameter("productName");
			String modelNumber = request.getParameter("modelNumber");
			if (null != elementID)
			{
				_elementName = sum.getAssetName(module, elementID);
			}
//			else if (null != modelNumber)
//			{
//				elementID = sum.getOtherID(module, "asset", "ATTR_ModelNumber", modelNumber);
//			}

			String elementName = da.getElementName(elementID, module);

			request.setAttribute(ENV_ELEMENT_ID, elementID);
			request.setAttribute(ENV_ELEMENT_FRIENDLY_NAME, _elementName);
			request.setAttribute(ENV_ELEMENT_NAME, elementName);
		}
	}
}
