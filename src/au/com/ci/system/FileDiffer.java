package au.com.ci.system;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FileDiffer
{
	public static boolean binaryEquals(File first, File second) throws IOException
	{
		if (!first.exists() || !second.exists() || !first.isFile() || !second.isFile())
		{
			return false;
		}
		if (first.length() != second.length())
		{
			return false;
		}
		if (first.getCanonicalPath().equals(second.getCanonicalPath()))
		{
			return true;
		}

		final int BUFFER_SIZE = 65536;
		boolean retval = false;

		BufferedInputStream bufFirstInput = null;
		BufferedInputStream bufSecondInput = null;
		try
		{
			bufFirstInput = new BufferedInputStream(new FileInputStream(first), BUFFER_SIZE);
			bufSecondInput = new BufferedInputStream(new FileInputStream(second), BUFFER_SIZE);

			int firstByte;
			int secondByte;

			while (true)
			{
				firstByte = bufFirstInput.read();
				secondByte = bufSecondInput.read();
				if (firstByte != secondByte)
				{
					break;
				}
				if ((firstByte < 0) && (secondByte < 0))
				{
					retval = true;
					break;
				}
			}
		}
		finally
		{
			try
			{
				if (bufFirstInput != null)
				{
					bufFirstInput.close();
				}
			}
			finally
			{
				if (bufSecondInput != null)
				{
					bufSecondInput.close();
				}
			}
		}
		return retval;
	}
}
