package au.com.ci.system;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;


public class ModuleEnvVariablesFilter implements Filter
{


	public void destroy()
	{
		// TODO Auto-generated method stub

	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException
	{
		// TODO Auto-generated method stub
		HttpServletRequest request = (HttpServletRequest) req;
		ModuleEnvVariableUtils.setVariables(request);
		ModuleEnvVariableUtils.printVariables(request);
		chain.doFilter(req, res);
	}

	public void init(FilterConfig arg0) throws ServletException
	{
	}

}
