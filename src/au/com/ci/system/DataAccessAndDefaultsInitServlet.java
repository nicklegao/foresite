package au.com.ci.system;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;

public class DataAccessAndDefaultsInitServlet extends HttpServlet
{

	private static final long serialVersionUID = 1387896026423420004L;

	@Override
	public void init(ServletConfig config) throws ServletException
	{
		try
		{
			DataAccess.setUpInstance(config.getServletContext());
			Defaults.getInstance().setProperties(config.getServletContext());
			System.out.println(">>>>>>>>>> " + config.getServletContext().getServerInfo() + ": DataAccessAndDefaultsInitServlet SUCCEED.");
		}
		catch (Throwable e)
		{
			System.out.println(">>>>>>>>>> " + config.getServletContext().getServerInfo() + ": DataAccessAndDefaultsInitServlet FAILED.");
			e.printStackTrace();
		}
	}
}
