package au.com.ci.system.classfinder;

import java.io.File;

/**
 * Convert a File object to a Class
 * 
 * @author Sam
 * 
 */
public class FileToClassConverter
{

	private String classPathRoot;

	public FileToClassConverter(String classPathRoot)
	{
		setClassPathRoot(classPathRoot);
	}

	/**
	 * @param classPathRoot
	 */
	public void setClassPathRoot(String classPathRoot)
	{
		if (classPathRoot == null)
		{
			throw new RuntimeException("Class path root must not be null");
		}
		this.classPathRoot = classPathRoot;
	}

	public Class convertToClass(File classFile)
	{
		Class classInstance = null;
		String filePath = classFile.getAbsolutePath().replaceAll("\\\\", "/");
		if (filePath.startsWith(classPathRoot) && filePath.endsWith(".class"))
		{
			classInstance = getClassFromName(classFile.getAbsolutePath());
		}
		return classInstance;
	}

	private Class getClassFromName(String fileName)
	{
		try
		{
			String className = removeClassPathBase(fileName);
			className = FileUtils.removeExtension(className);
			return Class.forName(className);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * @param fileName
	 * @return
	 */
	private String removeClassPathBase(String fileName)
	{
		String classPart = fileName.substring(classPathRoot.length());
		String className = classPart.replace(File.separatorChar, '.');
		return className;
	}



}
