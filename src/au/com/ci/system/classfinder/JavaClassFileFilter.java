/**
 * 
 */
package au.com.ci.system.classfinder;

public class JavaClassFileFilter extends ExtensionMatchFileFilter
{
	public JavaClassFileFilter()
	{
		super("class");
	}
}