package au.com.ci.webdirector.user;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.admin.security.LoginController;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;

public class UserManager
{

	private static Logger logger = Logger.getLogger(UserManager.class);

	public static String ADMIN = "ADMIN";

	public static String CONTENT_MANAGER = "content_manager";
	public static String CONTENT_VIEWER = "content_viewer";

	public static String getUserId(HttpServletRequest request)
	{
		return getUserId(request.getSession());
	}

	public static String getUserId(HttpSession session)
	{
		return (String) session.getAttribute(LoginController.WD_USER_ID);
	}

	public static String getUserName(HttpServletRequest request)
	{
		return getUserName(request.getSession());
	}

	public static String getUserName(HttpSession session)
	{
		return (String) session.getAttribute(LoginController.WD_USER);
	}

	private static DataMap getUser(String userId)
	{
		if (userId == null)
		{
			return null;
		}
		try
		{
			return TransactionDataAccess.getInstance().selectMap("select * from users where user_id = ?", new String[] { userId });
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return null;
		}
	}

	public static String getUserType(String userId)
	{
		DataMap user = getUser(userId);
		if (user == null)
		{
			return null;
		}

		if ("1".equals(user.getString("userLevel_Id")))
		{
			return ADMIN;
		}
		if ("4".equals(user.getString("userLevel_Id")))
		{
			return CONTENT_MANAGER;
		}
		return CONTENT_MANAGER;
	}

	public static boolean isAdmin(String userId)
	{
		return StringUtils.equalsIgnoreCase(getUserType(userId), ADMIN);
	}

	public static String getUserType(HttpServletRequest request)
	{

		String userId = getUserId(request);
		if (userId == null)
		{
			throw new RuntimeException("User not logged in");
		}
		return getUserType(userId);
	}

	public static boolean isAdmin(HttpServletRequest request)
	{
		String userId = getUserId(request);
		if (userId == null)
		{
			throw new RuntimeException("User not logged in");
		}
		return isAdmin(userId);
	}

	public static String getUserEmail(HttpServletRequest request)
	{
		String userId = getUserId(request);
		if (userId == null)
		{
			throw new RuntimeException("User not logged in");
		}

		DataMap user = getUser(userId);

		return user.getString("email");
	}
}
