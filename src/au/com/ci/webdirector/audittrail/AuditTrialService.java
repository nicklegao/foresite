package au.com.ci.webdirector.audittrail;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import au.net.webdirector.admin.security.LoginController;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;

public class AuditTrialService
{

	public static final String CREATE = "Create";
	public static final String COPY = "Copy";
	public static final String MOVE = "Move";
	public static final String READ = "Read";
	public static final String SORT = "Sort";
	public static final String UPDATE = "Update";
	public static final String DELETE = "Delete";
	public static final String LOGIN = "Login";
	public static final String LOGOUT = "Logout";
	public static final String IMPORT = "Import";
	public static final String EXPORT = "Export";
	public static final String SEND = "Send Email";
	public static final String CONFIG = "Configure";
	public static final String EXEC = "Execute";

	public static final int VERY_IMPORTANT = 0;
	public static final int IMPORTANT = 10;
	public static final int NORMAL = 20;
	public static final int NOT_IMPORTANT = 30;

	private String userName;
	private String module;
	private String ip;

	public static String getMsgClassName(int level)
	{
		switch (level)
		{
			case VERY_IMPORTANT:
				return "msg_very_important";
			case IMPORTANT:
				return "msg_important";
			case NORMAL:
				return "msg_normal";
			case NOT_IMPORTANT:
				return "msg_not_important";
			default:
				return "";
		}
	}

	public static String getMsgClassName(String level)
	{
		return getMsgClassName(Integer.parseInt(level));
	}

	public static String getRemoteAddr(HttpServletRequest request)
	{
		String ip = request.getHeader("x-forwarded-for");
		if (StringUtils.isBlank(ip))
		{
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	private AuditTrialService(String ip, String userName, String module)
	{
		this.userName = userName;
		this.module = module;
		this.ip = ip;
	}

	public static AuditTrialService getInstance(String ip, String userName, String module)
	{
		return new AuditTrialService(ip, userName, module);
	}

	public static AuditTrialService getInstance(HttpServletRequest request, String module)
	{
		String ip = getRemoteAddr(request);
		HttpSession session = request.getSession();
		String userName = session.getAttribute(LoginController.WD_USER) != null ? (String) session.getAttribute(LoginController.WD_USER) : "Unknown";
		return getInstance(ip, userName, module);
	}

	public static AuditTrialService getInstance(HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		String module = session.getAttribute("moduleSelected") != null ? (String) session.getAttribute("moduleSelected") : "Unknown";
		return getInstance(request, module);
	}


	public String getModule()
	{
		return module;
	}

	public void setModule(String module)
	{
		this.module = module;
	}

	public void trial(String type, int level, String extraMessage)
	{
		// when, where, who, what(type, level, msg)
		Hashtable<String, String> data = new Hashtable<String, String>();
		data.put("EVENT_WHEN", "NOW()");
		data.put("EVENT_WHERE", module);
		data.put("EVENT_WHO", userName);
		data.put("EVENT_TYPE", type);
		data.put("EVENT_LEVEL", String.valueOf(level));
		data.put("EVENT_MSG", extraMessage);
		data.put("EVENT_IP", ip);
		new DBaccess().insertData(data, "", "SYS_AUDIT_TRIAL");

	}

	public static String translateModule(String module, Map<String, String> dict)
	{
		if (!dict.containsKey(module))
		{
			return module;
		}
		return dict.get(module);
	}

	public static Map<String, String> getModuleDict()
	{
		List<String[]> list = new DBaccess().selectQuery("select internal_module_name, client_module_name from sys_moduleconfig");
		Map<String, String> dict = new HashMap<String, String>();
		for (String[] module : list)
		{
			dict.put(module[0], module[1]);
		}
		return dict;
	}
}
