package au.com.ci.webdirector.audittrail.customized.security;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;
import au.com.ci.webdirector.audittrail.customized.ModuleTrialHelper;

public class MemberManagementTrialer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{

	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		if (!"yes".equals(request.getParameter("save")))
		{
			return;
		}
		List<String> name = ModuleTrialHelper.lookupAssetName(request, request.getParameter("ELEMENTID"));
		String moduleName = AuditTrialService.translateModule(request.getParameter("moduleName"), AuditTrialService.getModuleDict());
		AuditTrialService ats = AuditTrialService.getInstance(request, "User Management");
		ats.trial(AuditTrialService.UPDATE, AuditTrialService.NORMAL, "Assigned Category Access of [" + moduleName + "] to Member [" + StringUtils.join(name, ",") + "]");
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/jsp/assignMemberCategories.jsp" };
	}

}
