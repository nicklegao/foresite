package au.com.ci.webdirector.audittrail.customized.security;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;
import au.net.webdirector.admin.security.LoginController;

public class SecurityLoginTrialer implements AuditTrialer
{

	String user = null;

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
		user = (String) request.getSession().getAttribute(LoginController.WD_USER);
	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		if (request.getAttribute("AuditTrial_LoginSuccessful") != null)
		{
			AuditTrialService ats = AuditTrialService.getInstance(AuditTrialService.getRemoteAddr(request), request.getParameter("username"), "Security");
			boolean loginSuccessful = (Boolean) request.getAttribute("AuditTrial_LoginSuccessful");
			if (loginSuccessful)
			{
				ats.trial(AuditTrialService.LOGIN, AuditTrialService.NORMAL, "Login Successful");
			}
			else
			{
				ats.trial(AuditTrialService.LOGIN, AuditTrialService.IMPORTANT, "Login Failed");
			}
			return;
		}
		if (request.getAttribute("AuditTrial_LogoutSuccessful") != null && user != null)
		{
			AuditTrialService ats = AuditTrialService.getInstance(AuditTrialService.getRemoteAddr(request), user, "Security");
			ats.trial(AuditTrialService.LOGOUT, AuditTrialService.NORMAL, "Logout");
			return;
		}
	}

	public String[] getMappedUrls()
	{
		return new String[] {
				"/webdirector/free/login",
				"/webdirector/free/logout"
		};
	}
}
