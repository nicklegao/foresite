package au.com.ci.webdirector.audittrail.customized.security;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;

public class UserManagementTrialer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		if (request.getAttribute("AuditTrial_UserAdded") != null)
		{
			boolean addSuccessful = (Boolean) request.getAttribute("AuditTrial_UserAdded");
			if (addSuccessful)
			{
				AuditTrialService ats = AuditTrialService.getInstance(request, "User Management");
				ats.trial(AuditTrialService.CREATE, AuditTrialService.NORMAL, "Added User [" + request.getParameter("userName") + "]");
			}
			return;
		}
		if (request.getAttribute("AuditTrial_UserUpdated") != null)
		{

			boolean successful = (Boolean) request.getAttribute("AuditTrial_UserUpdated");
			if (successful)
			{
				AuditTrialService ats = AuditTrialService.getInstance(request, "User Management");
				ats.trial(AuditTrialService.UPDATE, AuditTrialService.NORMAL, "Updated User [" + request.getParameter("userName") + "]");
			}
			return;
		}
		if (request.getAttribute("AuditTrial_UserDeleted") != null)
		{

			boolean successful = (Boolean) request.getAttribute("AuditTrial_UserDeleted");
			if (successful)
			{
				AuditTrialService ats = AuditTrialService.getInstance(request, "User Management");
				ats.trial(AuditTrialService.DELETE, AuditTrialService.NORMAL, "Deleted User [" + request.getParameter("userName") + "]");
			}
			return;
		}
		if ("save".equals(request.getParameter("action")))
		{
			DBaccess db = new DBaccess();
			//String userName = (String)db.select("select user_name from users where user_id = '" +request.getParameter("userID") + "'").get(0);
			String userName = (String) db.selectQuerySingleCol("select user_name from users where user_id = ?", new String[] { request.getParameter("userId") }).get(0);
			String moduleName = AuditTrialService.translateModule(request.getParameter("moduleName"), AuditTrialService.getModuleDict());
			AuditTrialService ats = AuditTrialService.getInstance(request, "User Management");
			ats.trial(AuditTrialService.UPDATE, AuditTrialService.NORMAL, "Assigned Category Access of [" + moduleName + "] to User [" + userName + "]");
		}
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/webdirector/secure/user/edit", "/webdirector/secure/user/add", "/webdirector/secure/user/categories" };
	}

}
