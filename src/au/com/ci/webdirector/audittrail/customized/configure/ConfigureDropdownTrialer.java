package au.com.ci.webdirector.audittrail.customized.configure;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;

public class ConfigureDropdownTrialer implements AuditTrialer
{

	protected List<String> deletedDropDown;

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
		if ("delete".equals(request.getParameter("action")) && request.getParameter("deleted_id") != null)
		{

			String params = request.getParameter("deleted_id");
			String[] paramList = StringUtils.split(params, ",");
			DatabaseValidation.encodeParam(paramList);
			String sql = " ? ";
			for (int i = 1; i < paramList.length; i++)
			{
				sql += ", ? ";
			}

			//deletedDropDown = new DBaccess().select("select concat(DROPDOWN_NAME, '--', DROPDOWN_VALUE) from drop_down where id in (" + request.getParameter("deleted_id") + ")");
			deletedDropDown = new DBaccess().selectQuerySingleCol("select concat(DROPDOWN_NAME, '--', DROPDOWN_VALUE) from drop_down where id in (" + sql + ")", paramList);
			return;
		}
	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		String colName = request.getParameter("colName");
		if ("save".equals(request.getParameter("action")))
		{
			AuditTrialService ats = AuditTrialService.getInstance(request);
			if (StringUtils.isNotBlank(request.getParameter("dropDownName")) && StringUtils.isNotBlank(request.getParameter("dropDownValue")))
			{
				ats.trial(AuditTrialService.CONFIG, AuditTrialService.NORMAL, "Added Dropdown Value (" + request.getParameter("dropDownName") + "--" + request.getParameter("dropDownValue") + ") to " + colName);
			}
			String[] ids = request.getParameterValues("id");
			String[] dd_names = request.getParameterValues("DD_NAME");
			String[] dd_values = request.getParameterValues("DD_VALUE");
			if (ids != null)
			{
				for (int i = 0; i < ids.length; i++)
				{
					ats.trial(AuditTrialService.CONFIG, AuditTrialService.NORMAL, "Changed Dropdown Value (" + dd_names[i] + "--" + dd_values[i] + ") to " + colName);
				}
			}
			return;
		}
		if ("delete".equals(request.getParameter("action")))
		{
			AuditTrialService ats = AuditTrialService.getInstance(request);
			for (String name : deletedDropDown)
			{
				ats.trial(AuditTrialService.CONFIG, AuditTrialService.IMPORTANT, "Deleted Dropdown Value (" + name + ") from " + colName);
			}
			return;
		}
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/webdirector/secure/module/configure/dropdown/values" };
	}

}
