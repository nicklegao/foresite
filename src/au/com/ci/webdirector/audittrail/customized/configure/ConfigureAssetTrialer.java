package au.com.ci.webdirector.audittrail.customized.configure;


public class ConfigureAssetTrialer extends ConfigureModuleTrialer
{

	public ConfigureAssetTrialer()
	{
		super("Asset");
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/jsp/editAttributes.jsp" };
	}

}
