package au.com.ci.webdirector.audittrail.customized.configure;

import org.apache.commons.lang.StringUtils;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;
import au.com.ci.webdirector.audittrail.customized.ModuleTrialHelper;

public class SortCategoryTrialer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		if (request.getParameter("down") == null && request.getParameter("up") == null)
		{
			return;
		}
		AuditTrialService ats = AuditTrialService.getInstance(request);
		if (request.getParameter("down") != null)
		{
			ats.trial(AuditTrialService.SORT, AuditTrialService.NORMAL, "Moved Category [" + StringUtils.join(ModuleTrialHelper.lookupCategoryName(request, request.getParameter("category_id")), ",") + "] down");
			return;
		}
		if (request.getParameter("up") != null)
		{
			ats.trial(AuditTrialService.SORT, AuditTrialService.NORMAL, "Moved Category [" + StringUtils.join(ModuleTrialHelper.lookupCategoryName(request, request.getParameter("category_id")), ",") + "] up");
			return;
		}
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/jsp/categoryOrder.jsp" };
	}

}
