package au.com.ci.webdirector.audittrail.customized.configure;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;

public class ModuleManagementTrialer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		if (request.getAttribute("AuditTrial_CreatedSuccessful") != null)
		{
			AuditTrialService ats = AuditTrialService.getInstance(request, "Module Management");

			String internalName = request.getParameter("internalName");
			if (internalName == null)
			{
				internalName = "<<No Name Provided>>";
			}

			boolean createdSuccessful = (Boolean) request.getAttribute("AuditTrial_CreatedSuccessful");
			if (createdSuccessful)
			{
				ats.trial(AuditTrialService.CREATE, AuditTrialService.NORMAL, "Created New Module [" + internalName + "]");
			}
			else
			{
				ats.trial(AuditTrialService.CREATE, AuditTrialService.IMPORTANT, "Failed to create new module: " + internalName);
			}
			return;
		}

		if (request.getAttribute("AuditTrial_DeletedSuccessful") != null)
		{
			AuditTrialService ats = AuditTrialService.getInstance(request, "Module Management");

			String deleteTarget = request.getParameter("deleteTarget");
			if (deleteTarget == null)
			{
				deleteTarget = "<<No Name Provided>>";
			}

			boolean createdSuccessful = (Boolean) request.getAttribute("AuditTrial_DeletedSuccessful");
			if (createdSuccessful)
			{
				ats.trial(AuditTrialService.CREATE, AuditTrialService.NORMAL, "Delete Module [" + deleteTarget + "]");
			}
			else
			{
				ats.trial(AuditTrialService.DELETE, AuditTrialService.IMPORTANT, "Failed to delete module: " + deleteTarget);
			}
			return;
		}

		if (request.getAttribute("AuditTrial_PurgeSuccessful") != null)
		{
			AuditTrialService ats = AuditTrialService.getInstance(request, "Module Management");

			String purgeTarget = request.getParameter("moduleID");
			if (purgeTarget == null)
			{
				purgeTarget = "<<No Name Provided>>";
			}

			boolean createdSuccessful = (Boolean) request.getAttribute("AuditTrial_PurgeSuccessful");
			if (createdSuccessful)
			{
				ats.trial(AuditTrialService.CREATE, AuditTrialService.NORMAL, "Purged Module [" + purgeTarget + "]");
			}
			else
			{
				ats.trial(AuditTrialService.DELETE, AuditTrialService.IMPORTANT, "Failed to purge module: " + purgeTarget);
			}
			return;
		}
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/webdirector/secure/module/iframe/create",
				"/webdirector/secure/module/iframe/delete",
				"/webdirector/secure/module/iframe/purge" };
	}

}
