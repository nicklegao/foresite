package au.com.ci.webdirector.audittrail.customized.configure;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;

public class SortingTrialer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
	}


	/**
	 * TODO: FIXME: Sushant/Nick: This code needs to be updated for WD10
	 * (#20150701)
	 */
	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
//		Map params = ParseUtil.parsePost(request);
//		String orderString = URLDecoder.decode(ModuleTrialHelper.getDwrParameter(0, params).get(0), "UTF-8");
//		String type = ModuleTrialHelper.getDwrParameter(2, params).get(0);
//		String typeName = type;
//		Map<String, String> order = new SortItemDAO().parseTheOrderString(orderString);
//		List<Entry<String, String>> orders = new ArrayList<Entry<String, String>>(order.entrySet());
//		Collections.sort(orders, new Comparator<Entry<String, String>>()
//		{
//			public int compare(Entry<String, String> o1, Entry<String, String> o2)
//			{
//				// TODO Auto-generated method stub
//				return new Integer(o1.getValue()).compareTo(new Integer(o2.getValue())) * -1;
//			}
//		});
//		if (type.equalsIgnoreCase("labels"))
//		{
//			typeName = "Asset Label";
//		}
//		else if (type.equalsIgnoreCase("categorylabels"))
//		{
//			typeName = "Category Label";
//		}
//		else if (type.equalsIgnoreCase("elements"))
//		{
//			typeName = "Asset";
//		}
//		else if (type.equalsIgnoreCase("categories"))
//		{
//			typeName = "Category";
//		}
//		AuditTrialService ats = AuditTrialService.getInstance(request);
//		for (Entry<String, String> e : orders)
//		{
//			String id = e.getKey();
//			String name = "";
//			if (type.equalsIgnoreCase("labels"))
//			{
//
//				name = StringUtils.join(ModuleTrialHelper.lookupLabelName(request, id), ",");
//			}
//			else if (type.equalsIgnoreCase("categorylabels"))
//			{
//				name = StringUtils.join(ModuleTrialHelper.lookupCategoryLabelName(request, id), ",");
//			}
//			else if (type.equalsIgnoreCase("elements"))
//			{
//				name = StringUtils.join(ModuleTrialHelper.lookupAssetName(request, id), ",");
//			}
//			else
//			{
//				name = StringUtils.join(ModuleTrialHelper.lookupCategoryName(request, id), ",");
//			}
//
//			ats.trial(AuditTrialService.SORT, AuditTrialService.NORMAL, "Sorted " + typeName + " [" + name + "] Order = " + e.getValue());
//		}
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/SortItemDAO.updateSortingOrder.dwr" };
	}

}
