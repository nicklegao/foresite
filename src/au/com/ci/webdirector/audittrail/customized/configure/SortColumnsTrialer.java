package au.com.ci.webdirector.audittrail.customized.configure;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;

public class SortColumnsTrialer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
		// TODO Auto-generated method stub

	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		// TODO Auto-generated method stub
		String type = request.getParameter("type");
		String labelExternalName = request.getParameter("labelExternalName");
		String labelInternalName = request.getParameter("labelInternalName");
		String tabName = request.getParameter("tabName");
		AuditTrialService ats = AuditTrialService.getInstance(request);
		if (labelExternalName != null)
		{
			ats.trial(AuditTrialService.CONFIG, AuditTrialService.NORMAL, "Changed Name [" + labelExternalName + "] of " + type + " Column [" + labelInternalName + "]");
			return;
		}
		if (tabName != null)
		{
			ats.trial(AuditTrialService.CONFIG, AuditTrialService.NORMAL, "Changed Tab [" + tabName + "] of " + type + " Column [" + labelInternalName + "]");
			return;
		}
	}

	public String[] getMappedUrls()
	{
		// TODO Auto-generated method stub
		return new String[] { "/wd/editLabelExternalName", "/wd/editLabelTab" };
	}

}
