package au.com.ci.webdirector.audittrail.customized.configure;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;

public abstract class ConfigureModuleTrialer implements AuditTrialer
{

	protected String type;

	public ConfigureModuleTrialer(String type)
	{
		this.type = type;
	}

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{

	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		if (request.getParameter("updateNow") != null)
		{
			AuditTrialService ats = AuditTrialService.getInstance(request);
			ats.trial(AuditTrialService.CONFIG, AuditTrialService.NORMAL, "Changed " + type + " Settings");
			return;
		}
		if (request.getParameter("addCol") != null)
		{
			String colName = (String) request.getParameter("addCol");
			String colType = (String) request.getParameter("colType");
			AuditTrialService ats = AuditTrialService.getInstance(request);
			ats.trial(AuditTrialService.CONFIG, AuditTrialService.NORMAL, "Added Column [" + colName + ":" + colType + "] to " + type);
			return;
		}
		if (request.getParameter("removeCol") != null)
		{
			// delete column action needed ?
			String removeCol = (String) request.getParameter("removeCol");
			AuditTrialService ats = AuditTrialService.getInstance(request);
			ats.trial(AuditTrialService.CONFIG, AuditTrialService.IMPORTANT, "Deleted Column [" + removeCol + "] from " + type);
			return;
		}
	}
}
