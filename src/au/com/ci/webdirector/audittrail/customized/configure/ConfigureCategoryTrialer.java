package au.com.ci.webdirector.audittrail.customized.configure;


public class ConfigureCategoryTrialer extends ConfigureModuleTrialer
{

	public ConfigureCategoryTrialer()
	{
		super("Category");
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/jsp/editAttributes_category.jsp" };
	}

}
