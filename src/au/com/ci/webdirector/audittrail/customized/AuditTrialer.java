package au.com.ci.webdirector.audittrail.customized;

import au.com.ci.system.ResettableStreamHttpServletRequest;

/**
 * @author msytest
 * 
 */
public interface AuditTrialer
{

	/**
	 * Do something before your business logic handler. For example, when you
	 * delete something, what you can get from request is id only, but you might
	 * want to get the name of it, and you have to lookup the name before it
	 * being deleted.
	 * 
	 * @param request
	 * @throws Exception
	 */

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception;

	/**
	 * Log the event. You may have to put some request attribute in you business
	 * logic handler. In the example above, you need to put an attribute into
	 * request to indicate if the process is successful. so you can get the
	 * result here then log it.
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void trial(ResettableStreamHttpServletRequest request) throws Exception;

	/**
	 * Map URLs to the trialer. One trialer can have more than one URL. Please
	 * be noted that do NOT add the RequestQueryString part behind the URLs, in
	 * another word, they are technically URIs.
	 * 
	 * @return
	 */
	public String[] getMappedUrls();
}
