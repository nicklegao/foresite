package au.com.ci.webdirector.audittrail.customized;

import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;

public class ModuleTrialHelper
{


	public static List<String> lookupLabelName(ResettableStreamHttpServletRequest request, String id)
	{
		List<String> ids = new ArrayList<String>();
		ids.add(id);
		return lookupLabelName(request, ids);
	}

	public static List<String> lookupLabelName(ResettableStreamHttpServletRequest request, List<String> ids)
	{
		String module = (String) request.getSession().getAttribute("moduleSelected");
		ids = DatabaseValidation.encodeParam(ids);
		module = DatabaseValidation.encodeParam(module);
		//return new ArrayList<String>(new DBaccess().select("select concat(Label_id, ':', Label_ExternalName) from labels_" + module + " where Label_id in ('" + StringUtils.join(ids, "','") + "')"));
		return new ArrayList<String>(new DBaccess().selectQuerySingleCol("select concat(Label_id, ':', Label_ExternalName) from labels_" + module + " where Label_id in ('" + StringUtils.join(ids, "','") + "')"));
	}

	public static List<String> lookupCategoryLabelName(ResettableStreamHttpServletRequest request, String id)
	{
		List<String> ids = new ArrayList<String>();
		ids.add(id);
		return lookupCategoryLabelName(request, ids);
	}

	public static List<String> lookupCategoryLabelName(ResettableStreamHttpServletRequest request, List<String> ids)
	{
		String module = (String) request.getSession().getAttribute("moduleSelected");
		module = DatabaseValidation.encodeParam(module);
		ids = DatabaseValidation.encodeParam(ids);
		//return new ArrayList<String>(new DBaccess().select("select concat(Label_id, ':', Label_ExternalName) from categorylabels_" + module + " where Label_id in ('" + StringUtils.join(ids, "','") + "')"));
		return new ArrayList<String>(new DBaccess().selectQuerySingleCol("select concat(Label_id, ':', Label_ExternalName) from categorylabels_" + module + " where Label_id in ('" + StringUtils.join(ids, "','") + "')"));
	}

	public static List<String> lookupAssetName(ResettableStreamHttpServletRequest request, String id)
	{
		List<String> ids = new ArrayList<String>();
		ids.add(id);
		return lookupAssetName(request, ids);
	}

	public static List<String> lookupAssetName(ResettableStreamHttpServletRequest request, List<String> ids)
	{
		String module = (String) request.getSession().getAttribute("moduleSelected");
		module = DatabaseValidation.encodeParam(module);
		ids = DatabaseValidation.encodeParam(ids);
		//return new ArrayList<String>(new DBaccess().select("select concat(Element_id, ':', attr_headline) from elements_" + module + " where element_id in ('" + StringUtils.join(ids, "','") + "')"));
		return new ArrayList<String>(new DBaccess().selectQuerySingleCol("select concat(Element_id, ':', attr_headline) from elements_" + module + " where element_id in ('" + StringUtils.join(ids, "','") + "')"));
	}

	public static List<String> lookupCategoryName(ResettableStreamHttpServletRequest request, String id)
	{
		List<String> ids = new ArrayList<String>();
		ids.add(id);
		return lookupCategoryName(request, ids);
	}

	public static List<String> lookupCategoryName(ResettableStreamHttpServletRequest request, List<String> ids)
	{
		List<String> ret = new ArrayList<String>();
		if (ids.contains("0"))
		{
			ret.add("ROOT");
		}
		String module = (String) request.getSession().getAttribute("moduleSelected");
		module = DatabaseValidation.encodeParam(module);
		ids = DatabaseValidation.encodeParam(ids);
		//ret.addAll(new DBaccess().select("select concat(Category_id, ':', attr_categoryname) from categories_" + module + " where category_id in ('" + StringUtils.join(ids, "','") + "')"));
		ret.addAll(new DBaccess().selectQuerySingleCol("select concat(Category_id, ':', attr_categoryname) from categories_" + module + " where category_id in ('" + StringUtils.join(ids, "','") + "')"));
		return ret;
	}

	public static List<String> getDwrParameter(int pos, Map params) throws ServerException
	{

		// There must be provided APIs from DWR to do this.

		if (!params.containsKey("c0-param" + pos))
		{
			return new ArrayList<String>();
		}
		String value = (String) params.get("c0-param" + pos);
		String type = value.split(":")[0];
		String v = value.substring(type.length() + 1);
		List<String> ret = new ArrayList<String>();
		if (!type.equalsIgnoreCase("Array"))
		{
			ret.add(v);
			return ret;
		}
		value = v.substring(1, v.length() - 1);
		for (String s : value.split(","))
		{
			String t = (String) params.get(s.split(":")[1]);
			ret.add(t.split(":")[1]);
		}
		return ret;
	}
}
