package au.com.ci.webdirector.audittrail.customized.dms;

import javax.servlet.http.HttpServletRequest;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;
import au.net.webdirector.common.Defaults;

import com.oreilly.servlet.MultipartRequest;

public class UploadServletTrialer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{

	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		MultipartRequest multi = new MultipartRequest(request, Defaults.getInstance().getStoreDir() + "/_tmp", "UTF-8");
		String formName = multi.getParameter("formName");
		if ("updateForm.jsp".equalsIgnoreCase(formName))
		{
			processUpdateAsset(request, multi);
			return;
		}
		if ("insertForm.jsp".equalsIgnoreCase(formName))
		{
			processInsertAsset(request, multi);
			return;
		}
		if ("updateForm_category.jsp".equalsIgnoreCase(formName))
		{
			processUpdateCategory(request, multi);
			return;
		}
		if ("insertForm_category.jsp".equalsIgnoreCase(formName))
		{
			processInsertCategory(request, multi);
			return;
		}
	}


	private void processInsertCategory(HttpServletRequest request, MultipartRequest multi)
	{
		AuditTrialService ats = AuditTrialService.getInstance(request);
		ats.trial(AuditTrialService.CREATE, AuditTrialService.NORMAL, "Created Category [" + multi.getParameter("ATTR_categoryName") + "]");
	}

	private void processUpdateCategory(HttpServletRequest request, MultipartRequest multi)
	{
		AuditTrialService ats = AuditTrialService.getInstance(request);
		ats.trial(AuditTrialService.UPDATE, AuditTrialService.NORMAL, "Updated Category [" + multi.getParameter("ATTR_categoryName") + "]");
	}

	private void processInsertAsset(HttpServletRequest request, MultipartRequest multi)
	{
		AuditTrialService ats = AuditTrialService.getInstance(request);
		ats.trial(AuditTrialService.CREATE, AuditTrialService.NORMAL, "Created Asset [" + multi.getParameter("ATTR_Headline") + "]");
	}

	private void processUpdateAsset(HttpServletRequest request, MultipartRequest multi)
	{
		AuditTrialService ats = AuditTrialService.getInstance(request);
		ats.trial(AuditTrialService.UPDATE, AuditTrialService.NORMAL, "Updated Asset [" + multi.getParameter("ATTR_Headline") + "]");
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/uploadServlet" };
	}

}
