package au.com.ci.webdirector.audittrail.customized.dms;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;

import com.ci.webdirector.wdjson.model.JsonRequestSaveDeleteElement;
import com.google.gson.Gson;

public abstract class SummaryTableTrialer implements AuditTrialer
{

	String fieldName;
	String type;

	public SummaryTableTrialer(String type, String fieldName)
	{
		super();
		this.type = type;
		this.fieldName = fieldName;
	}

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{

	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		String body = IOUtils.toString(request.getReader());
		JsonRequestSaveDeleteElement input = (JsonRequestSaveDeleteElement) new Gson().fromJson(body, JsonRequestSaveDeleteElement.class);
		AuditTrialService ats = AuditTrialService.getInstance(request);
		if (input.getDelete().size() > 0)
		{
			for (String sName : input.deletedValues(fieldName))
			{
				ats.trial(AuditTrialService.DELETE, AuditTrialService.NORMAL, "Deleted " + type + " [" + sName + "]");
			}
		}
		if (input.getSave().size() > 0)
		{
			for (String sName : input.savedValues(fieldName))
			{
				ats.trial(AuditTrialService.UPDATE, AuditTrialService.NORMAL, "Updated " + type + " [" + sName + "]");
			}
		}
	}

}
