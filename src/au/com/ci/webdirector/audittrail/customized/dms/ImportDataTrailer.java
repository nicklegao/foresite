package au.com.ci.webdirector.audittrail.customized.dms;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;
import au.net.webdirector.admin.imports.CVSImporter;
import au.net.webdirector.admin.imports.ImportCSV;
import au.net.webdirector.admin.imports.Procedure;

public class ImportDataTrailer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		String url = request.getRequestURI();
		if (url.endsWith("/admin/jsp/secure/iframe/data_import_page2_h.jsp"))
		{
			String importFile = request.getParameter("importFile");
			String importType = request.getParameter("importType");
			String assetLive = request.getParameter("assetLive");
			String deleteAll = request.getParameter("deleteAll");
			String ignoreFirstLine = request.getParameter("ignoreFirstLine");
			if (ignoreFirstLine == null)
			{
				ignoreFirstLine = "off";
			}
			String pKey = request.getParameter("pKey");
			String[] mapping = request.getParameterValues("mapping");

			CVSImporter imp = (CVSImporter) request.getAttribute("AuditTrial_Importer");
			int total = ((ImportCSV) imp.getOperator()).getTotalRecord();
			AuditTrialService ats = AuditTrialService.getInstance(request);
			ats.trial(AuditTrialService.IMPORT, AuditTrialService.NORMAL, "Started Importing " + total + " " + importType + " Records from [" + importFile + "] (DeleteExisting=" + deleteAll + ", MakeAssetLive=" + assetLive + ", IgnoreFirstLine=" + ignoreFirstLine + ", ImporterID=" + imp.getID() + ")");
			return;
		}
		if (url.endsWith("/admin/jsp/secure/iframe/importPercentage.jsp"))
		{
			CVSImporter imp = (CVSImporter) request.getAttribute("AuditTrial_Importer");
			if (imp != null && imp.getStatus() != Procedure.COMPLISHED)
			{
				return;
			}
			AuditTrialService ats = AuditTrialService.getInstance(request);
			ats.trial(AuditTrialService.IMPORT, AuditTrialService.NORMAL, "Finished Importing (ImporterID=" + request.getParameter("id") + ")");
			return;
		}
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/admin/jsp/secure/iframe/data_import_page2_h.jsp", "/admin/jsp/secure/iframe/importPercentage.jsp" };
	}

}
