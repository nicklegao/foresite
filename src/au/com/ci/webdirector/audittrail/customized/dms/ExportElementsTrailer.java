package au.com.ci.webdirector.audittrail.customized.dms;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;
import au.com.ci.webdirector.audittrail.customized.ModuleTrialHelper;

public class ExportElementsTrailer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		if ("yes".equals(request.getParameter("exportNow")))
		{
			AuditTrialService ats = AuditTrialService.getInstance(request);
			List<String> name = ModuleTrialHelper.lookupCategoryName(request, request.getParameter("categoryId"));
			ats.trial(AuditTrialService.EXPORT, AuditTrialService.NORMAL, "Exported Asset Data from [" + StringUtils.join(name, ",") + "]");
			return;
		}
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/jsp/csv_export_fields.jsp" };
	}

}
