package au.com.ci.webdirector.audittrail.customized.dms;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;

public class MoveCategoryTrialer implements AuditTrialer
{

	List<String> name;
	List<String> to;

	/**
	 * TODO: FIXME: Sushant/Nick: This code needs to be updated for WD10
	 * (#20150701)
	 */
	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
//		Map params = ParseUtil.parsePost(request);
//
//		List<String> id = ModuleTrialHelper.getDwrParameter(0, params);
//		List<String> toid = ModuleTrialHelper.getDwrParameter(1, params);
//		name = ModuleTrialHelper.lookupCategoryName(request, id);
//		to = ModuleTrialHelper.lookupCategoryName(request, toid);
	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		AuditTrialService ats = AuditTrialService.getInstance(request);
		String sTo = StringUtils.join(to, ",");
		for (String sName : name)
		{
			ats.trial(AuditTrialService.MOVE, AuditTrialService.NORMAL, "Moved Category [" + sName + "] to [" + sTo + "]");
		}
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/AjaxTree.moveCategoryInTree.dwr" };
	}

}
