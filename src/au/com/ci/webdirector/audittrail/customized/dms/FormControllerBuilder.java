package au.com.ci.webdirector.audittrail.customized.dms;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;
import au.net.webdirector.common.Defaults;

import com.oreilly.servlet.MultipartRequest;

public class FormControllerBuilder implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{

	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		MultipartRequest multi = new MultipartRequest(request, Defaults.getInstance().getStoreDir() + "/_tmp", "UTF-8");
		String action = multi.getParameter("action");
		String table = multi.getParameter("table");
		String module = multi.getParameter("module");

		AuditTrialService ats = AuditTrialService.getInstance(request);
		ats.setModule(module);

		if ("update".equalsIgnoreCase(action) && "Elements".equals(table))
		{
			processUpdateAsset(ats, multi);
			return;
		}
		if ("insert".equalsIgnoreCase(action) && "Elements".equals(table))
		{
			processInsertAsset(ats, multi);
			return;
		}
		if ("update".equalsIgnoreCase(action) && "Categories".equals(table))
		{
			processUpdateCategory(ats, multi);
			return;
		}
		if ("insert".equalsIgnoreCase(action) && "Categories".equals(table))
		{
			processInsertCategory(ats, multi);
			return;
		}
	}


	private void processInsertCategory(AuditTrialService ats, MultipartRequest multi)
	{
		ats.trial(AuditTrialService.CREATE, AuditTrialService.NORMAL, "Created Category [" + multi.getParameter("ATTR_categoryName") + "]");
	}

	private void processUpdateCategory(AuditTrialService ats, MultipartRequest multi)
	{
		ats.trial(AuditTrialService.UPDATE, AuditTrialService.NORMAL, "Updated Category [" + multi.getParameter("ATTR_categoryName") + "]");
	}

	private void processInsertAsset(AuditTrialService ats, MultipartRequest multi)
	{
		ats.trial(AuditTrialService.CREATE, AuditTrialService.NORMAL, "Created Asset [" + multi.getParameter("ATTR_Headline") + "]");
	}

	private void processUpdateAsset(AuditTrialService ats, MultipartRequest multi)
	{
		ats.trial(AuditTrialService.UPDATE, AuditTrialService.NORMAL, "Updated Asset [" + multi.getParameter("ATTR_Headline") + "]");
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/webdirector/form/save" };
	}

}
