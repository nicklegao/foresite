package au.com.ci.webdirector.audittrail.customized.dms;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;

public class CopyAssetTrialer implements AuditTrialer
{

	List<String> name;
	List<String> to;

	/**
	 * TODO: FIXME: Sushant/Nick: This code needs to be updated for WD10
	 * (#20150701)
	 */
	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
//		Map params = ParseUtil.parsePost(request);
//		List<String> id = ModuleTrialHelper.getDwrParameter(0, params);
//		List<String> toid = ModuleTrialHelper.getDwrParameter(1, params);
//		name = ModuleTrialHelper.lookupAssetName(request, id);
//		to = ModuleTrialHelper.lookupCategoryName(request, toid);
	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		AuditTrialService ats = AuditTrialService.getInstance(request);
		for (String sName : name)
		{
			ats.trial(AuditTrialService.COPY, AuditTrialService.NORMAL, "Copied Asset [" + sName + "] to [" + StringUtils.join(to, ",") + "]");
		}
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/AjaxTree.copyAssetInTree.dwr" };
	}

}
