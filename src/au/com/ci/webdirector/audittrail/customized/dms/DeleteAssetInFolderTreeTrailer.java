package au.com.ci.webdirector.audittrail.customized.dms;

import au.com.ci.system.ResettableStreamHttpServletRequest;

public class DeleteAssetInFolderTreeTrailer extends DeleteAssetTrialer
{

	/**
	 * TODO: FIXME: Sushant/Nick: This code needs to be updated for WD10
	 * (#20150701)
	 */
	@Override
	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
//		Map params = ParseUtil.parsePost(request);
//		List<String> id = ModuleTrialHelper.getDwrParameter(1, params);
//		name = ModuleTrialHelper.lookupAssetName(request, id);
	}

	@Override
	public String[] getMappedUrls()
	{
		return new String[] { "/AjaxTree.deleteAsset.dwr" };
	}

}
