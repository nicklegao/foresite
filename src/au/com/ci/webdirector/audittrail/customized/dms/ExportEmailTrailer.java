package au.com.ci.webdirector.audittrail.customized.dms;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;

public class ExportEmailTrailer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		if (request.getParameter("email") != null)
		{
			String table = request.getParameter("table");
			AuditTrialService ats = AuditTrialService.getInstance(request);
			ats.trial(AuditTrialService.SEND, AuditTrialService.NORMAL, "Sent " + table + " Export Data to [" + request.getParameter("email") + "]");
			return;
		}
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/webdirector/secure/module/iframe/export-email" };
	}

}
