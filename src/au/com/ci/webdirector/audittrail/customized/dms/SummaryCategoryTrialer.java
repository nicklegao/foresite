package au.com.ci.webdirector.audittrail.customized.dms;

public class SummaryCategoryTrialer extends SummaryTableTrialer
{
	public SummaryCategoryTrialer()
	{
		super("Category", "ATTR_categoryName");
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/json/saveDeleteCategory" };
	}
}
