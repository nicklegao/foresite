package au.com.ci.webdirector.audittrail.customized.dms;

import java.util.List;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;
import au.com.ci.webdirector.audittrail.customized.ModuleTrialHelper;

public class DeleteCategoryTrialer implements AuditTrialer
{

	List<String> name;

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{
		String id = request.getParameter("elementID");
		name = ModuleTrialHelper.lookupCategoryName(request, id);
	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		if (request.getAttribute("AuditTrial_DoNotTrial") != null)
		{
			return;
		}
		AuditTrialService ats = AuditTrialService.getInstance(request);
		for (String sName : name)
		{
			ats.trial(AuditTrialService.DELETE, AuditTrialService.NORMAL, "Deleted Category [" + sName + "]");
		}
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/jsp/dynamicFormBuilder_categoryDelete.jsp" };
	}

}
