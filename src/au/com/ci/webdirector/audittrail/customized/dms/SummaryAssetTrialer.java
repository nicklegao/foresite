package au.com.ci.webdirector.audittrail.customized.dms;

public class SummaryAssetTrialer extends SummaryTableTrialer
{
	public SummaryAssetTrialer()
	{
		super("Asset", "ATTR_Headline");
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/json/saveDeleteElement" };
	}
}
