package au.com.ci.webdirector.audittrail.customized.utilities.openlinks;

public class JobPauseTrialer extends JobManipulateTrialer
{

	public JobPauseTrialer()
	{
		super("Pause");
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/wd/openlinks/pauseJob" };
	}

}
