package au.com.ci.webdirector.audittrail.customized.utilities.openlinks;

import org.apache.commons.lang.StringUtils;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;

public class NewJobTrialer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{

	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		String name = request.getParameter("jobDetail.name");
		String className = request.getParameter("className");
		String repeatInterval = request.getParameter("repeatInterval");
		String repeatCount = request.getParameter("repeatCount");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String context = request.getParameter("context");
		AuditTrialService ats = AuditTrialService.getInstance(request, "Openlinks");
		ats.trial(AuditTrialService.EXEC, AuditTrialService.NORMAL, "Scheduled New Job [name=" + name + ", className=" + className + ", repeatCount=" + repeatCount + ", repeatInterval=" + repeatInterval + ",startDate=" + startDate + ",endDate=" + endDate + ", context=" + context + "]");
		return;
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/wd/openlinks/jobScheduleSubmit" };
	}

}
