package au.com.ci.webdirector.audittrail.customized.utilities.openlinks;

public class JobDeleteTrialer extends JobManipulateTrialer
{

	public JobDeleteTrialer()
	{
		super("Delete");
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/wd/openlinks/deleteJob" };
	}

}
