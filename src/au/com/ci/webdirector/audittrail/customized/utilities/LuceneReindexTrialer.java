package au.com.ci.webdirector.audittrail.customized.utilities;

import java.util.Map;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;

public class LuceneReindexTrialer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{

	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		String emailAddress = request.getParameter("Email");
		String moduleName = request.getParameter("ModuleID");
		AuditTrialService ats = AuditTrialService.getInstance(request, "Utilities");
		Map<String, String> dict = AuditTrialService.getModuleDict();
		ats.trial(AuditTrialService.EXEC, AuditTrialService.NORMAL, "Re-generated Lucene Indexes for Module [" + AuditTrialService.translateModule(moduleName, dict) + "], Sent Email to [" + emailAddress + "]");
		return;
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/admin/jsp/secure/iframe/deleteSearchIndex_h.jsp" };
	}

}
