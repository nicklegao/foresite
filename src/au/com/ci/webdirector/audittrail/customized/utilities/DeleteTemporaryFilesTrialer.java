package au.com.ci.webdirector.audittrail.customized.utilities;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;

public class DeleteTemporaryFilesTrialer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{

	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		AuditTrialService ats = AuditTrialService.getInstance(request, "Utilities");
		ats.trial(AuditTrialService.EXEC, AuditTrialService.NORMAL, "Deleted Temporary Files");
		return;
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/jsp/houseKeepingTools/deleteTemporaryFiles.jsp" };
	}

}
