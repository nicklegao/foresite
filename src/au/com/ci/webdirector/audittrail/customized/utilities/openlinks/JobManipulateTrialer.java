package au.com.ci.webdirector.audittrail.customized.utilities.openlinks;

import org.apache.commons.lang.StringUtils;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;

public class JobManipulateTrialer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{

	}

	protected String operation;

	public JobManipulateTrialer(String operation)
	{
		this.operation = operation;
	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		String jobName = request.getParameter("jobName");
		if (StringUtils.isBlank(jobName))
		{
			return;
		}
		String url = request.getRequestURI();
		AuditTrialService ats = AuditTrialService.getInstance(request, "Openlinks");
		ats.trial(AuditTrialService.EXEC, AuditTrialService.NORMAL, operation + " Job [" + jobName + "]");
		return;
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/wd/openlinks/resumeJob", "/wd/openlinks/triggerJob", "/wd/openlinks/deleteJob", "/wd/openlinks/interruptJob", "/wd/openlinks/pauseJob" };
	}

}
