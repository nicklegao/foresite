package au.com.ci.webdirector.audittrail.customized.utilities.openlinks;

public class JobTriggerTrialer extends JobManipulateTrialer
{

	public JobTriggerTrialer()
	{
		super("Trigger");
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/wd/openlinks/triggerJob" };
	}

}
