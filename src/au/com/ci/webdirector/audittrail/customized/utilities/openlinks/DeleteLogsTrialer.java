package au.com.ci.webdirector.audittrail.customized.utilities.openlinks;

import org.apache.commons.lang.StringUtils;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;

public class DeleteLogsTrialer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{

	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		String elid = request.getParameter("elid");
		if (StringUtils.isBlank(elid))
		{
			return;
		}
		AuditTrialService ats = AuditTrialService.getInstance(request, "Openlinks");
		ats.trial(AuditTrialService.EXEC, AuditTrialService.NORMAL, "Deleted Openlinks Logs " + StringUtils.split(elid, ",").length + " record(s)");
		return;
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/wd/openlinks/deleteJoHistoryRecords" };
	}

}
