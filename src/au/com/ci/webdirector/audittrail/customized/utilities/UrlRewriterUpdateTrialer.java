package au.com.ci.webdirector.audittrail.customized.utilities;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;

public class UrlRewriterUpdateTrialer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{

	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		AuditTrialService ats = AuditTrialService.getInstance(request, "Utilities");
		ats.trial(AuditTrialService.EXEC, AuditTrialService.NORMAL, "Updated SEO settings");
		return;
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/admin/jsp/secure/iframe/seo_update_do.jsp" };
	}

}
