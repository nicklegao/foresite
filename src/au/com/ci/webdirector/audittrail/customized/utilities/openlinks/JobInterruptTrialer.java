package au.com.ci.webdirector.audittrail.customized.utilities.openlinks;

public class JobInterruptTrialer extends JobManipulateTrialer
{

	public JobInterruptTrialer()
	{
		super("Interrupt");
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/wd/openlinks/interruptJob" };
	}

}
