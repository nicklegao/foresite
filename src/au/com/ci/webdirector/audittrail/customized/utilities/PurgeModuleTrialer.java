package au.com.ci.webdirector.audittrail.customized.utilities;

import java.util.Map;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.webdirector.audittrail.AuditTrialService;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;

public class PurgeModuleTrialer implements AuditTrialer
{

	public void beforeProcess(ResettableStreamHttpServletRequest request) throws Exception
	{

	}

	public void trial(ResettableStreamHttpServletRequest request) throws Exception
	{
		AuditTrialService ats = AuditTrialService.getInstance(request, "Utilities");
		Map<String, String> dict = AuditTrialService.getModuleDict();
		ats.trial(AuditTrialService.EXEC, AuditTrialService.NORMAL, "Purged Module [" + AuditTrialService.translateModule(request.getParameter("moduleID"), dict) + "] Content");
		return;
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/admin/jsp/secure/iframe/purgeModuleFinal.jsp" };
	}

}
