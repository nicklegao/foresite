package au.com.ci.webdirector.audittrail.customized.utilities.openlinks;

public class JobResumeTrialer extends JobManipulateTrialer
{

	public JobResumeTrialer()
	{
		super("Resume");
	}

	public String[] getMappedUrls()
	{
		return new String[] { "/wd/openlinks/resumeJob" };
	}

}
