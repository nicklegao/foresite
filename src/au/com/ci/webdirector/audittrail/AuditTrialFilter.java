package au.com.ci.webdirector.audittrail;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import au.com.ci.system.ResettableStreamHttpServletRequest;
import au.com.ci.system.classfinder.JavaClassFinder;
import au.com.ci.webdirector.audittrail.customized.AuditTrialer;

public class AuditTrialFilter implements Filter
{

	private static Logger logger = Logger.getLogger(AuditTrialFilter.class);

	private Map<String, Class<? extends AuditTrialer>> trialers = new HashMap<String, Class<? extends AuditTrialer>>();

	public void init(FilterConfig arg0) throws ServletException
	{
		JavaClassFinder classFinder = new JavaClassFinder(AuditTrialer.class);
		List<Class<? extends AuditTrialer>> classes = classFinder.findAllMatchingTypes("au.com.ci.webdirector.audittrail.customized");

		for (Class<? extends AuditTrialer> clazz : classes)
		{
			try
			{
				AuditTrialer t = clazz.newInstance();
				for (String url : t.getMappedUrls())
				{
					trialers.put(url, clazz);
				}
			}
			catch (Exception e)
			{
				logger.info("Skip " + clazz.getCanonicalName());
			}
		}
	}

	public void destroy()
	{

	}

	private AuditTrialer initAuditTrialer(HttpServletRequest request)
	{
		String url = request.getRequestURI();
		for (Entry<String, Class<? extends AuditTrialer>> e : trialers.entrySet())
		{
			if (url.endsWith(e.getKey()))
			{
				try
				{
					return e.getValue().newInstance();
				}
				catch (Exception e1)
				{
					logger.error("Skip url = [" + url + "] trialer = [" + e.getValue().getCanonicalName() + "]");
				}
			}
		}

		return null;
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException
	{
		ResettableStreamHttpServletRequest request = new ResettableStreamHttpServletRequest((HttpServletRequest) req);
		AuditTrialer trialer = initAuditTrialer((HttpServletRequest) req);
		try
		{
			if (trialer != null)
			{
				trialer.beforeProcess(request);
				request.resetInputStream();
			}
		}
		catch (Exception e)
		{
			logger.error("Error", e);
		}
		chain.doFilter(request, res);
		try
		{
			if (trialer != null)
			{
				request.resetInputStream();
				trialer.trial(request);
				request.resetInputStream();
			}
		}
		catch (Exception e)
		{
			logger.error("Error", e);
		}
	}

}
