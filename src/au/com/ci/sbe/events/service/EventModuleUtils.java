package au.com.ci.sbe.events.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import au.com.ci.sbe.events.model.Event;
import webdirector.db.client.ClientDataAccess;

/**
 * CUSTOM UTILITY FUNCTIONS FOR LOADING EVENTS MODULE DATA
 *
 * @date 11 November 2013
 * @author Sushant
 *
 */
public class EventModuleUtils
{
	private static final String EVENT_MODULE = "EVENTS";

	public static final SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat calendrioFormat = new SimpleDateFormat("MM-dd-yyyy");

	private LinkedList<Event> repeatingEvents;
	private LinkedList<Event> singleEvents;

	public EventModuleUtils()
	{
		repeatingEvents = new LinkedList<Event>();
		singleEvents = new LinkedList<Event>();
	}


	public Event getEventDetails(String eventId)
	{
		ClientDataAccess cda = new ClientDataAccess();

		try
		{
			Vector<Hashtable<String, String>> v = cda.getElement(EVENT_MODULE, eventId);
			Hashtable<String, String> hEvent = v.get(0);
			return new Event(hEvent);
		}
		catch (Exception e)
		{
			return null;
		}
	}

	public int loadData(String categoryId)
	{
		ClientDataAccess cda = new ClientDataAccess();
		Vector<Hashtable<String, String>> lEvents = null;
		if (StringUtils.isBlank(categoryId))
		{
			lEvents = cda.getAllElements(EVENT_MODULE);
		}
		else
		{
			lEvents = cda.getCategoryElementsByName(EVENT_MODULE, cda.getCategoryFromId(EVENT_MODULE, categoryId));
		}
		for (int i = 0; i < lEvents.size(); i++)
		{
			Hashtable<String, String> dbRow = lEvents.get(i);
			Event event = new Event(dbRow);
			if (event.isRepeating())
			{
				repeatingEvents.add(event);
			}
			else
			{
				singleEvents.add(event);
			}
		}

		return lEvents.size();
	}

	public Map<String, LinkedList<Event>> eventsInRange(Date startDate, Date endDate)
	{
		Map<String, LinkedList<Event>> aMap = new HashMap<String, LinkedList<Event>>();

		processEventsToMap(aMap,
				startDate,
				endDate,
				singleEvents);
		processEventsToMap(aMap,
				startDate,
				endDate,
				repeatingEvents);

		return aMap;
	}

	private void processEventsToMap(Map<String, LinkedList<Event>> aMap,
			Date startDate,
			Date endDate,
			LinkedList<Event> events)
	{
		Calendar calendar = GregorianCalendar.getInstance();

		for (Event event : events)
		{
			Date eventDate = event.firstDateInRange(startDate, endDate);
			if (eventDate != null)
			{
				if (!event.isRepeating())
				{
					addEventForDate(event, eventDate, aMap);
				}
				else
				{
					calendar.setTime(eventDate);

					while (eventDate.before(endDate) && event.dateIsInRepeatCycle(eventDate))
					{
						addEventForDate(event, eventDate, aMap);

						calendar.add(Calendar.DAY_OF_MONTH, 7);
						eventDate = calendar.getTime();
					}
				}
			}
		}
	}

	private void addEventForDate(Event event, Date date, Map<String, LinkedList<Event>> aMap)
	{
		String dateKey = calendrioFormat.format(date);

		LinkedList<Event> currentEvents = aMap.get(dateKey);
		if (currentEvents == null)
		{
			currentEvents = new LinkedList<Event>();
		}

		currentEvents.add(event);
		aMap.put(dateKey, currentEvents);
	}
}
