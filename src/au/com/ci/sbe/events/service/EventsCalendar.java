package au.com.ci.sbe.events.service;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import au.com.ci.sbe.events.model.Event;

import com.google.gson.Gson;

/**
 * Servlet implementation class EventsCalendar
 */
public class EventsCalendar extends HttpServlet
{
	private static final Logger logger = Logger.getLogger(EventsCalendar.class);

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String categoryId = request.getParameter("id");
		EventModuleUtils eventModule = new EventModuleUtils();

		int eventObjCount = eventModule.loadData(categoryId);
		logger.info(String.format("Loaded %d event objects", eventObjCount));

		Calendar cal = GregorianCalendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.add(Calendar.MONTH, -6);
		Date startDate = cal.getTime();

		cal.add(Calendar.MONTH, 12);
		Date endDate = cal.getTime();
		Map<String, LinkedList<Event>> data = eventModule.eventsInRange(startDate, endDate);
		logger.info(String.format("Loaded %d event objects", eventObjCount));

		Map<String, String> content = new HashMap<String, String>();

		for (String key : data.keySet())
		{
			StringBuffer sb = new StringBuffer();
			for (Event event : data.get(key))
			{
				sb.append("<a class=\"calendarEventItem\" onclick=\"displayEvent(" + event.getEventId() + ", '" + key + "')\">");
				sb.append(StringEscapeUtils.escapeHtml(event.getEventName()));
				sb.append("</a>");
			}
			content.put(key, sb.toString());
		}

		Gson gson = new Gson();
		response.getWriter().write(gson.toJson(content));
	}

}
