package au.com.ci.sbe.events.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import org.apache.commons.lang.StringUtils;


public class Event
{
	protected static final SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private int eventId;
	private String eventName;
	private String description;
	private String location;
	private Date startDate;
	private Date endDate;
	private boolean allDay;
	private boolean repeating;

	public Event(Hashtable<String, String> dbRow)
	{
		this.eventId = Integer.parseInt(dbRow.get("Element_id"));
		this.eventName = dbRow.get("ATTR_Headline");
		this.description = dbRow.get("ATTR_Description");
		this.location = dbRow.get("ATTR_Location");
		try
		{
			if (!StringUtils.isEmpty(dbRow.get("ATTRDATE_start")))
				this.startDate = dbDateFormat.parse(dbRow.get("ATTRDATE_start"));
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}
		try
		{
			if (!StringUtils.isEmpty(dbRow.get("ATTRDATE_end")))
				this.endDate = dbDateFormat.parse(dbRow.get("ATTRDATE_end"));
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}
		this.allDay = "1".equals(dbRow.get("ATTRCHECK_allDayEvent"));
		this.repeating = "1".equals(dbRow.get("ATTRCHECK_reoccurWeekly"));
	}

	public Date firstDateInRange(Date startRange, Date endRange)
	{
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(startDate);
		Date d = calendar.getTime();
		while (d.before(startRange))
		{

			calendar.add(Calendar.DAY_OF_MONTH, 7);
			d = calendar.getTime();
		}

		return d.before(endRange) ? d : null;
	}

	public boolean dateIsInRepeatCycle(Date eventDate)
	{
		if (!repeating)
			return false;

		return (eventDate.after(startDate) || eventDate.equals(startDate))
				&& (eventDate.before(endDate) || eventDate.equals(endDate));
	}

	/* Auto generated getters and setters */

	public int getEventId()
	{
		return eventId;
	}

	public void setEventId(int eventId)
	{
		this.eventId = eventId;
	}

	public String getEventName()
	{
		return eventName;
	}

	public void setEventName(String eventName)
	{
		this.eventName = eventName;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getLocation()
	{
		return location;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	public boolean isAllDay()
	{
		return allDay;
	}

	public void setAllDay(boolean allDay)
	{
		this.allDay = allDay;
	}

	public boolean isRepeating()
	{
		return repeating;
	}

	public void setRepeating(boolean repeating)
	{
		this.repeating = repeating;
	}

	public static SimpleDateFormat getDbdateformat()
	{
		return dbDateFormat;
	}
}
