package au.com.ci.sbe.util;

import java.text.DecimalFormat;

public class Formator
{
	public static String formatCurrency(String prefix, double amount)
	{
		DecimalFormat df = new DecimalFormat(prefix + "#,##0.00");
		return df.format(amount);
	}

	public static void main(String[] args)
	{
		System.out.println(Formator.formatCurrency("$", -99.9));
	}
}
