package au.com.ci.sbe.util;

public interface Matcher<T>
{
	public boolean match(T o1);
}
