package au.com.ci.sbe.util;

import java.io.File;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

public class UrlUtils
{
	public static String autocachedUrl(String filePath)
	{
		return autocachedUrl(filePath, null, false);
	}

	public static String autocachedUrl(String filePath, HttpServletRequest request)
	{
		return autocachedUrl(filePath, request, false);
	}

	public static String autocachedUrl(String filePath, HttpServletRequest request, boolean allowNull)
	{
		// filePath needs to be absolute
		// filePath must not contain other parameters
		String contextPath = "";
		if (request != null)
		{
			contextPath = request.getContextPath();
		}

		if (filePath.startsWith("/") == false)
		{
			String path = request.getRequestURI();
			path.toString();

			filePath = path.substring(0, path.lastIndexOf("/") + 1) + filePath;
		}
		File dynamicModuleStylesheet = new File(request.getSession().getServletContext().getRealPath(filePath));
		if (dynamicModuleStylesheet.exists())
		{
			return contextPath + filePath + "?cache=" + dynamicModuleStylesheet.lastModified();
		}
		return allowNull ? null : filePath;
	}

	public static String autocachedUrl(String filePath, HttpServletRequest request, boolean allowNull, Boolean date)
	{
		// filePath needs to be absolute
		// filePath must not contain other parameters
		String contextPath = "";
		if (request != null)
		{
			contextPath = request.getContextPath();
		}

		if (filePath.startsWith("/") == false)
		{
			String path = request.getRequestURI();
			path.toString();

			filePath = path.substring(0, path.lastIndexOf("/") + 1) + filePath;
		}
		File dynamicModuleStylesheet = new File(request.getSession().getServletContext().getRealPath(filePath));
		if (dynamicModuleStylesheet.exists() && !date)
		{
			return contextPath + filePath + "?cache=" + dynamicModuleStylesheet.lastModified();
		} else if (date) {
			return contextPath + filePath + "?cache=" + new Date().toString();
		}
		return allowNull ? null : filePath;
	}
}
