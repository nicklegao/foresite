package au.com.ci.sbe.util;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionManager implements HttpSessionListener
{
	private static final Map<String, HttpSession> sessions = new ConcurrentHashMap<String, HttpSession>();

	public void sessionCreated(HttpSessionEvent event)
	{
		HttpSession session = event.getSession();
		sessions.put(session.getId(), session);
	}

	public void sessionDestroyed(HttpSessionEvent event)
	{
		sessions.remove(event.getSession().getId());
	}

	public static HttpSession find(String sessionId)
	{
		return sessions.get(sessionId);
	}

	public static HttpSession findSessionWithAttributeValue(String attributeKey, Object attributeValue)
	{
		for (HttpSession s : getAllSessions())
		{
			Object testValue = s.getAttribute(attributeKey);
			if (attributeValue.equals(testValue))
			{
				return s;
			}
		}
		return null;
	}

	public static Set<HttpSession> getAllSessions()
	{
		return new HashSet<HttpSession>(sessions.values());
	}

}