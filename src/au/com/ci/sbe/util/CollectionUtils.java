package au.com.ci.sbe.util;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionUtils
{
	public static <T> boolean contains(Collection<T> collection, T obj, Equalor<? super T> e)
	{
		for (T t : collection)
		{
			if (e.equals(t, obj))
			{
				return true;
			}
		}
		return false;
	}

	public static <T> boolean addWhenNotExist(Collection<T> collection, T obj)
	{
		if (collection.contains(obj))
		{
			return false;
		}
		return collection.add(obj);
	}

	public static <T> boolean addWhenNotExist(Collection<T> collection, Collection<T> collection2)
	{
		boolean ret = false;
		for (T obj : collection2)
		{
			ret = addWhenNotExist(collection, obj) || ret;
		}
		return ret;
	}

	public static <T> boolean addWhenNotExist(Collection<T> collection, Collection<T>... collections)
	{
		boolean ret = false;
		for (Collection<T> c : collections)
		{
			ret = addWhenNotExist(collection, c) || ret;
		}
		return ret;
	}

	public static <T> Collection<T> filter(Collection<T> collection, Matcher<? super T> m)
	{
		Collection<T> ret = new ArrayList<T>();
		for (T t : collection)
		{
			if (m.match(t))
			{
				ret.add(t);
			}
		}
		return ret;
	}
}
