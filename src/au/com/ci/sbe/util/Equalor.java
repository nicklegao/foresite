package au.com.ci.sbe.util;

public interface Equalor<T>
{
	public boolean equals(T o1, T o2);
}
