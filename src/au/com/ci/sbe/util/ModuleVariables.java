package au.com.ci.sbe.util;

import java.util.List;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;


public class ModuleVariables
{


	static String module = "flex2";

	public static String getString(String name)
	{
		String sql = "select ATTR_Value from elements_" + module + " where ATTR_JspVariableName = ?";
		//List<String> list = (List<String>)new DBaccess().select(sql);
		List<String> list = (List<String>) new DBaccess().selectQuerySingleCol(sql, new String[] { name });
		if (list.size() == 0)
		{
			throw new RuntimeException("No such name : " + name + " in " + module);
		}
		return list.get(0);
	}

	public static double getDouble(String name)
	{
		String value = getString(name);
		try
		{
			return Double.parseDouble(value);
		}
		catch (Exception e)
		{
			throw new RuntimeException(name + " is not a double : " + value);
		}
	}

	public static int getInt(String name)
	{
		String value = getString(name);
		try
		{
			return Integer.parseInt(value);
		}
		catch (Exception e)
		{
			throw new RuntimeException(name + " is not an integer : " + value);
		}
	}
}
