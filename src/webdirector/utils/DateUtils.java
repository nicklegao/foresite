package webdirector.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by IntelliJ IDEA.
 * User: CorpInteractive
 * Date: 27/01/2009
 * Time: 16:40:05
 * To change this template use File | Settings | File Templates.
 */
/**
 * TODO: SUSHANT: Please stop using this class and remove it
 */
@Deprecated
public class DateUtils
{
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT_RFC = "EEE, dd MMM yyyy HH:mm:ss z";

	public static String getNow()
	{
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());

	}

	public static String getNowForDB()
	{
		// returns timestamp with the right now
		TimeZone tz = TimeZone.getTimeZone("Australia/Sydney");
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTimeZone(tz);

		int d = rightNow.get(Calendar.DAY_OF_MONTH);
		int m = rightNow.get(Calendar.MONTH);
		int y = rightNow.get(Calendar.YEAR);
		int hh = rightNow.get(Calendar.HOUR_OF_DAY);
		int mm = rightNow.get(Calendar.MINUTE);
		int ss = rightNow.get(Calendar.SECOND);

		String dateTime = String.valueOf(y) + "/" + String.valueOf(m + 1) + "/" + String.valueOf(d) + " " +
				String.valueOf(hh) + ":" + String.valueOf(mm) + ":" + String.valueOf(ss);

		return dateTime;
	}

	public static String covertDateRFCFormate(String date)
	{
		String formattedDate = "";
		try
		{
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
			Date srcDate = sdf.parse(date);
			formattedDate = new SimpleDateFormat(DATE_FORMAT_RFC).format(srcDate);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return formattedDate;
	}

	public static String covertDateRFCFormate(String date, String formate)
	{
		String formattedDate = "";
		try
		{
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat(formate);
			Date srcDate = sdf.parse(date);
			formattedDate = new SimpleDateFormat(DATE_FORMAT_RFC).format(srcDate);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return formattedDate;
	}

	public static String changeDateStyleAsNeed(String date, String sorceFormate, String desformate)
	{
		String strOutDt = "";
		DateFormat df = DateFormat.getDateInstance();
		df = new java.text.SimpleDateFormat(sorceFormate);
		try
		{
			Date date1 = df.parse(date);
			strOutDt = new SimpleDateFormat(desformate).format(date1);

		}
		catch (Exception e)
		{
			e.printStackTrace();
			strOutDt = date;
		}
		return strOutDt;
	}

	public static String getDateAfterAddDuration(String str_date, int Duration, String dateFormate)
	{
		String strOutDt = "";
		Calendar cal = Calendar.getInstance();
		try
		{

			DateFormat formatter = new SimpleDateFormat(dateFormate);
			Date date = (Date) formatter.parse(str_date);
			cal.setTime(date);
			cal.add(Calendar.DATE, Duration);
			Date date1 = new Date(cal.getTimeInMillis());
			strOutDt = new SimpleDateFormat(dateFormate).format(date1);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return strOutDt;
	}
}
