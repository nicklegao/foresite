package webdirector.utils;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import webdirector.db.DButils;
import webdirector.db.client.ClientDataAccess;

public class EnhancedAssignCategories
{
	private static Logger logger = Logger.getLogger(EnhancedAssignCategories.class);
	ClientDataAccess cda = new ClientDataAccess();
	DButils db = new DButils();

	public Map getUnassignedLevelOneCategories(String moduleName, String id)
	{
		return getUnassignedLevelOneCategories(moduleName, id, "user");
	}

	public Map getUnassignedLevelOneCategories(String moduleName, String id, String type)
	{
		Map mapOfPrivateCategories = getPrivateLevelOneCategories(moduleName);
		Vector vtAssigned = getAssignedCategoryIDs(moduleName, id, type);
		for (int k = 0; k < vtAssigned.size(); k++)
		{
			String s[] = (String[]) vtAssigned.get(k);
			String categoryId = s[0];
			mapOfPrivateCategories.remove(categoryId);
		}
		Map mapOfCategories = getSortedMap(mapOfPrivateCategories);
		return mapOfCategories;
	}

	public Map getAssignedLevelOneCategories(String moduleName, String id)
	{
		return getAssignedLevelOneCategories(moduleName, id, "user");
	}

	public Map getAssignedLevelOneCategories(String moduleName, String id, String type)
	{
		Vector vt = getAssignedCategoryIDs(moduleName, id, type);
		Map mapOfPrivateCategories = getPrivateLevelOneCategories(moduleName);
		Map mapOfCategories = new LinkedHashMap();
		for (int k = 0; k < vt.size(); k++)
		{
			String s[] = (String[]) vt.get(k);
			String categoryId = s[0];
			if (!mapOfPrivateCategories.containsKey(categoryId))
			{
				continue;
			}
			mapOfCategories.put(categoryId, mapOfPrivateCategories.get(categoryId));
		}
		Map mapOfCategories1 = getSortedMap(mapOfCategories);
		return mapOfCategories1;
	}

	public Map getAllLevelOneCategories(String moduleName)
	{
		Vector vt = cda.getLevelCategories(moduleName, "1");
		Map mapOfCategories = new LinkedHashMap();
		for (int k = 0; k < vt.size(); k++)
		{
			Hashtable ht = (Hashtable) vt.get(k);
			String categoryId = (String) ht.get("Category_id");
			String categoryName = (String) ht.get("ATTR_categoryName");
			mapOfCategories.put(categoryId, categoryName);
		}
		Map mapOfCategories1 = getSortedMap(mapOfCategories);
		return mapOfCategories1;
	}


	public Map getPrivateLevelOneCategories(String moduleName)
	{
		Vector vt = cda.getLevelCategories(moduleName, "1");
		Map mapOfCategories = new LinkedHashMap();
		for (int k = 0; k < vt.size(); k++)
		{
			Hashtable ht = (Hashtable) vt.get(k);
			String categoryId = (String) ht.get("Category_id");
			String categoryName = (String) ht.get("ATTR_categoryName");
			String isPrivate = (String) ht.get("ATTRCHECK_Private");
			if (!StringUtils.equals("1", isPrivate))
			{
				continue;
			}
			mapOfCategories.put(categoryId, categoryName);
		}
		Map mapOfCategories1 = getSortedMap(mapOfCategories);
		return mapOfCategories1;
	}

	public Map getPublicLevelOneCategories(String moduleName)
	{
		Vector vt = cda.getLevelCategories(moduleName, "1");
		Map mapOfCategories = new LinkedHashMap();
		for (int k = 0; k < vt.size(); k++)
		{
			Hashtable ht = (Hashtable) vt.get(k);
			String categoryId = (String) ht.get("Category_id");
			String categoryName = (String) ht.get("ATTR_categoryName");
			String isPrivate = (String) ht.get("ATTRCHECK_Private");
			if (StringUtils.equals("1", isPrivate))
			{
				continue;
			}
			mapOfCategories.put(categoryId, categoryName);
		}
		Map mapOfCategories1 = getSortedMap(mapOfCategories);
		return mapOfCategories1;
	}

	public Vector getAssignedCategoryIDs(String moduleName, String id)
	{
		return getAssignedCategoryIDs(moduleName, id, "user");
	}

	public Vector getAssignedCategoryIDs(String moduleName, String id, String type)
	{
		String cols[] = { "category_id" };
		type = DatabaseValidation.encodeParam(type);
		String SQL_QUERY = "select category_id from " + type + "_module_access where " + type + "_id=? and module_name=? ";
		logger.info("sql ========= " + SQL_QUERY);
		//Vector vt = db.select(SQL_QUERY, cols);
		Vector vt = db.selectQuery(SQL_QUERY, new String[] { id, moduleName });
		return vt;
	}

	public Vector getExistingElements(String moduleName, Vector categories)
	{
		Vector existingElements = new Vector();
		Vector elementIds = new Vector();
		String query = "Select Element_id from Elements_" + moduleName + " where Category_id= ?";
		for (int i = 0; i < categories.size(); i++)
		{
			//Vector ids = db.select(query);
			Vector ids = db.selectQuerySingleCol(query, new String[] { (String) categories.get(i) });
			for (int j = 0; j < ids.size(); j++)
			{
				elementIds.add(ids.get(j));
			}
		}
		return elementIds;
	}

	public boolean saveAssignCategories(String moduleName, String id, String categoriesSelected)
	{
		return saveAssignCategories(moduleName, id, categoriesSelected, "user");
	}

	public boolean saveAssignCategories(String moduleName, String id, String categoriesSelected, String type)
	{
		System.out.println("In SaveAssignCategory Method =======>>>>>>>>>>>>");
		StringTokenizer str = new StringTokenizer(categoriesSelected, ",");
		db.updateData("DELETE FROM " + type + "_MODULE_ACCESS where MODULE_NAME=? and " + type + "_ID=? ", new Object[] { moduleName, id });
		int check;
		while (str.hasMoreTokens())
		{
			String categoryId = str.nextToken();
			Hashtable ht = new Hashtable();
			ht.put("MODULE_NAME", moduleName);
			ht.put(type + "_ID", id);
			ht.put("CATEGORY_ID", categoryId);
			check = db.insertData(ht, "ID", type + "_MODULE_ACCESS");
			if (check < 0)
				return false;
		}
		return true;
	}

	// **
	public boolean checkUserAssignCategory(String module, String category_id, String user_id)
	{
		if (StringUtils.isBlank(module) || StringUtils.isBlank(category_id) || StringUtils.equals(category_id, "-1"))
		{
			return true;
		}
		String tempID = category_id;
		String levelOneID = category_id;
		Vector result = null;
		int count = 0;
		while (!tempID.equals("0"))
		{
			String sql = "select category_parentid from categories_" + DatabaseValidation.encodeParam(module) + " where category_id = ?";
			logger.info(sql);
			//result = new DBaccess().select(sql);
			result = new DBaccess().selectQuerySingleCol(sql, new String[] { tempID });
			if (result.size() == 0)
			{
				return false;
			}
			if (count++ > 100)
			{
				return false;
			}
			levelOneID = tempID;
			tempID = (String) result.get(0);
		}
		Map publicCategories = getPublicLevelOneCategories(module);
		if (publicCategories.containsKey(levelOneID))
		{
			return true;
		}
		if (!isLoginNeeded(module))
		{
			return true;
		}
		if (user_id == null)
		{
			return false;
		}
		if (!isCategoryControlled())
		{
			return true;
		}
		// get user group id
		String group = "select category_id from elements_members where element_id = ?";
		//result = new DBaccess().select(group);
		result = new DBaccess().selectQuerySingleCol(group, new String[] { user_id });
		if (result.size() == 0)
		{
			return false;
		}
		String groupId = (String) result.get(0);

		// check group - category allocation
		try
		{
			String query = "Select distinct Module_Name from usergroup_module_access where Usergroup_id = 'Elements" + groupId + "' " + "and Category_id = " + levelOneID + " and Module_Name = '" + module + "'";
			logger.info(query);
			Vector v = db.select(query);
			if (v.size() > 0)
			{
				return true;
			}
		}
		catch (Exception e)
		{

		}
		String query = "Select distinct Module_Name from user_module_access where User_id = 'Elements" + user_id + "' " + "and Category_id = " + levelOneID + " and Module_Name = '" + module + "'";
		logger.info(query);
		Vector v = db.select(query);
		return v.size() > 0;
	}

	public boolean deleteAssignCategories(String moduleName, String modulePrefix, String Id)
	{
		return deleteAssignCategories(moduleName, modulePrefix, Id, "user");
	}

	public boolean deleteAssignCategories(String moduleName, String modulePrefix, String Id, String type)
	{

		String whereClause = "";
		Object[] params;
		int index = 0;
		// If modulePrefix is ellements than we are going to delete all entry
		// where
		// USER_ID = Elements+Id
		if (modulePrefix.equalsIgnoreCase("Elements"))
		{
			whereClause = type + "_ID = ?";
			//params[index++] = "Elements" + Id ;
			params = new Object[] { "Elements" + Id };
		}
		// Else we have deleted categoryID so need to delete all entries where
		// ModuleName and categor_id matched with arguments
		else
		{
			whereClause = "CATEGORY_ID = ? and MODULE_NAME=?";
			//params[index++] = Id;
			//params[index++] = moduleName;
			params = new Object[] { Id, moduleName };
		}
		String query = "delete from " + type + "_MODULE_ACCESS where " + whereClause;
		System.out.println("Querry====>>>>" + query);
		db.updateData(query, params);
		return true;
	}

	private Map getSortedMap(Map hmap)
	{

		Map map = new LinkedHashMap();
		List mapKeys = new ArrayList(hmap.keySet());
		List mapValues = new ArrayList(hmap.values());
		hmap.clear();
		TreeSet sortedSet = new TreeSet(mapValues);
		Object[] sortedArray = sortedSet.toArray();
		int size = sortedArray.length;
		// a) Ascending sort

		for (int i = 0; i < size; i++)
		{

			map.put(mapKeys.get(mapValues.indexOf(sortedArray[i])), sortedArray[i]);
		}
		return map;
	}

	public boolean isLoginNeeded(String moduleName)
	{
		if (StringUtils.isBlank(moduleName))
		{
			return false;
		}
		String moduleforuser = "";
		String sql = "select attr_value from elements_flex9 where attr_jspvariablename = 'EnableUserAccessControlModuleList' and live = 1";
		//Vector v = new DBaccess().select(sql);
		Vector v = new DBaccess().selectQuerySingleCol(sql);
		if (v.size() > 0)
		{
			moduleforuser = "," + (String) v.get(0) + ",";
		}
		return moduleforuser.indexOf(moduleName) != -1;
	}

	public boolean isCategoryControlled()
	{
		String check = "select attr_value from elements_flex9 where attr_jspvariablename = 'EnableCategoryLevelUserAccessControl' and live = 1";
		Vector v = new DBaccess().selectQuerySingleCol(check);
		return v.size() > 0 && "YES".equals(v.get(0));
	}
}
