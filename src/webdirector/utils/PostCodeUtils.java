package webdirector.utils;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import webdirector.db.client.ClientDataAccess;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 11/04/2007
 * Time: 21:10:52
 * To change this template use File | Settings | File Templates.
 */

public class PostCodeUtils
{
	boolean useCommonDB = true;

	Logger logger = Logger.getLogger(PostCodeUtils.class);

	private DBaccess dbCommon = new DBaccess(useCommonDB);
	private DBaccess db = new DBaccess();

	private String postCodeTable = "pc_postcodedistancetable";

	/**
	 * Uses Austrlaian postcodes by default
	 */
	public PostCodeUtils()
	{
		// uses Australian postcodes by default
	}

	/**
	 * If the useNZpostCodes is set to true then use the NZ post code tables
	 * instead of Australian
	 * 
	 * @param useNZpostCodes
	 */
	public PostCodeUtils(boolean useNZpostCodes)
	{
		postCodeTable = "nz_pc_postcodedistancetable";
	}

	/**
	 * AJAX function originally written for Strandbags to enable 5 closest
	 * stores lookup
	 * Takes the origin location (postcode and from this searches in ever
	 * increasing radius until
	 * 5 stores have been found
	 * 
	 * @param originPostCode
	 * @return list of location addresses, single address/string per item
	 */
	public List<String> closestLocations(String originPostCode)
	{
		//List<String> returnList = new ArrayList<String>();
		List<String> storeList = new ArrayList<String>();

		// first check if we know the originPostCode as this is a non-starter problem
		//String query = "SELECT locationapostcode FROM "+postCodeTable+" p " +
		//"where locationapostcode = "+originPostCode;
		//Vector resultRows = dbCommon.select(query);
		// ok postcode exists so let's find the nearest stores
		for (int radius = 5; radius < 200 && storeList.size() < 5; radius = radius + 5)
		{
			Vector<String> matchingPostCodes = new Vector<String>();
			matchingPostCodes.add(originPostCode);
			matchingPostCodes.addAll(findPostCodesWithinProximity(Integer.valueOf(originPostCode), radius, true));
			if (matchingPostCodes != null && matchingPostCodes.size() > 0)
			{
				// we've found some postcodes within the radius being checked
				// find if we have any valid locations in these postcodes
				logger.debug("SYS matching postcode " + matchingPostCodes.size());
				List stores = getLocationsAtPostcode(matchingPostCodes);
				logger.debug("SYS matching store " + stores.size());
				storeList.addAll(addStoresWhichDontExist(storeList, stores));
			}
		}

		// could get here with no stores if we matches postcodes but they had no exists stores in them
		if (storeList.size() == 0)
		{
			storeList.add("ERROR: Your postcode does not exist or no nearby stores were found.");
		}

		// now get the stores in these postCodes
		if (storeList.size() > 5)
			return storeList.subList(0, 5);
		else
			return storeList;
	}

	private List<String> addStoresWhichDontExist(List<String> closestStores, List<String> newStores)
	{
		List<String> tmpStores = new ArrayList<String>();
		for (int i = 0; i < newStores.size(); i++)
		{
			String s = newStores.get(i);
			logger.debug("SYS NEW store " + s);
			if (!closestStores.contains(s))
			{
				logger.debug("SYS does not exists so add it");
				tmpStores.add(s);
			}
		}
		return tmpStores;
	}

	/**
	 * Get all stores in these postCodes
	 * 
	 * @param postCodes
	 * @return
	 */
	private List<String> getLocationsAtPostcode(List<String> postCodes)
	{
		List<String> addresses = new ArrayList<String>();
		ClientDataAccess cda = new ClientDataAccess();

		for (int i = 0; i < postCodes.size() && addresses.size() <= 5; i++)
		{
			String s = postCodes.get(i);

			// if postCode have less than 4 digits, e.g. Darwin in Northern Territory 0800 and this postcode is stored as 800 in the GEO table
			// as the datatype for postcode is INTEGER, we have to pre-fill 0 at the front of postcode for doing search inside LOCATIONS table
			for (int k = s.length(); k < 4; k++)
			{
				s = "0" + s;
			}

			Vector locations = cda.getElementByValue("LOCATIONS", "ATTR_PostCode", "'" + s + "' AND ATTR_PickupStatus='Y' AND ATTR_VisibleStatus='O'", false);
			if (locations != null && locations.size() > 0)
			{
				for (int j = 0; j < locations.size(); j++)
				{
					Hashtable<String, String> location = (Hashtable) locations.elementAt(j);
					String address = location.get("Element_id") + ".";
					if (!isEmpty(location.get("ATTR_Headline")))
						address += "<strong><span class=\"pickupStoreHeadline\">" + location.get("ATTR_Headline") + "</span></strong><br />";

					if (!isEmpty(location.get("ATTR_Address1")))
						address += "<strong><span class=\"pickupStoreAddress1\">" + location.get("ATTR_Address1") + "</span></strong><br />";

					if (!isEmpty(location.get("ATTR_Address2")))
						address += "<span class=\"pickupStoreAddress2\">" + location.get("ATTR_Address2") + "</span>";

					if (!isEmpty(location.get("ATTR_Address2")) && !isEmpty(location.get("ATTR_Address3")))
					{
						address += ", ";
					}

					if (!isEmpty(location.get("ATTR_Address3")))
						address += "<span class=\"pickupStoreAddress3\">" + location.get("ATTR_Address3") + "</span> ";

					address += "<br />";

					if (!isEmpty(location.get("ATTR_Suburb")))
						address += "<span class=\"pickupStoreSuburb\">" + location.get("ATTR_Suburb") + "</span>, ";

					if (!isEmpty(location.get("ATTR_State")))
						address += "<span class=\"pickupStoreState\">" + location.get("ATTR_State") + "</span>, ";

					if (!isEmpty(location.get("ATTR_PostCode")))
						address += "<span class=\"pickupStorePostcode\">" + location.get("ATTR_PostCode") + "</span>";

					addresses.add(address);
				}
			}
		}
		return addresses;
	}

	/**
	 *
	 * @param origin
	 *            - postcode location to search from
	 * @param radiusKM
	 *            - distance for the radius to search around.
	 * @return an int array of postcodes within that radius from tbe origin
	 *         postcode
	 */
	public Vector findPostCodesWithinProximity(int origin, int radiusKM)
	{
		dbCommon = new DBaccess(true);
		radiusKM = radiusKM * 1000; // use metres
		String query = "SELECT locationbpostcode FROM " + postCodeTable + " p " +
				"where locationapostcode = ? and distanceinmetres < ?";
		//return ( dbCommon.select(query) );
		return (dbCommon.selectQuerySingleCol(query, new Object[] { origin, radiusKM }));
	}

	/**
	 *
	 * @param origin
	 *            - postcode location to search from
	 * @param radiusKM
	 *            - distance for the radius to search around.
	 * @return an int array of postcodes within that radius from tbe origin
	 *         postcode
	 */
	public Vector findPostCodesWithinProximity(int origin, int radiusKM, boolean order)
	{
		radiusKM = radiusKM * 1000; // use metres
		String query = "SELECT locationbpostcode FROM " + postCodeTable + " p " +
				" where locationapostcode = ? and distanceinmetres < ? order by distanceinmetres asc ";
		//return ( dbCommon.select(query) );
		return (dbCommon.selectQuerySingleCol(query, new Object[] { origin, radiusKM }));
	}

	/**
	 *
	 * @param postCodeList
	 *            - vector of postcodes (as Strings) to find matches
	 * @param moduleName
	 *            - full prefixed module name to search within (i.e. already
	 *            with element_ or categories prepended
	 * @param internalPostCodeAttrName
	 *            - the attribute in this module thats stores the postcode
	 * @return element_ID or Category_id of all assets in this module which
	 *         match any of the incoming vector list
	 */
	public Vector findAssetsWithMatchingPostCodes(Vector postCodeList, String moduleName, String internalPostCodeAttrName,
			boolean liveAssetsOnly)
	{
		String postcodeATTR = "ATTR_postcode";
		List<String> params = new ArrayList<String>();
		if (internalPostCodeAttrName != null && !internalPostCodeAttrName.equals(""))
			postcodeATTR = internalPostCodeAttrName;
		String idField = "element_id";
		if (!moduleName.startsWith("element"))
			idField = "Category_id";
		String liveAssetsOnlyString = " and Live = 1 ";
		if (!liveAssetsOnly)
			liveAssetsOnlyString = "";

		if (postCodeList == null || postCodeList.size() < 1)
			return null;
		else
		{
			String query = "SELECT " + idField + " FROM " + DatabaseValidation.encodeParam(moduleName) + " " +
					"where " + postcodeATTR + " in ( ? ";
			params.add((String) postCodeList.elementAt(0));

			for (int i = 1; i < postCodeList.size(); i++)
			{
				String s = (String) postCodeList.elementAt(i);
				params.add(s);
				query += ", ?";
			}

			query += ")" + liveAssetsOnlyString;
			//return ( db.select(query) );
			return (db.selectQuery(query, params.toArray()));
		}
	}

	/**
	 *
	 * @param sourcePostCode
	 *            - point A
	 * @param destPostCode
	 *            - point B
	 * @return straight line distance in km (rounded) between the two points
	 */
	public int getPostCodeAPostCodeBDistance(int sourcePostCode, int destPostCode)
	{
		String query = "SELECT distanceInMetres FROM " + postCodeTable + " p " +
				"where locationapostcode = ? and locationbpostcode = ? ";
		//Vector v = dbCommon.select(query);
		Vector v = dbCommon.selectQuerySingleCol(query, new Object[] { sourcePostCode, destPostCode });
		String distanceS = (String) v.elementAt(0);
		return (Integer.parseInt(distanceS) / 1000);
	}

	/**
	 *
	 * @param
	 * @return
	 */
//    public int getPostCodeFromSuburbName(String suburbName, String state) {
//        String query = "SELECT postCode FROM "+postCodeTable+" p " +
//                "where locationapostcode = "+sourcePostCode+" and locationapostcode = "+destPostCode;
//        DBaccess db = new DBaccess();
//        Vector v = db.select(query);
//        String distanceS = (String)v.elementAt(0);
//        return ( Integer.parseInt(distanceS) / 1000 );
//    }
//
//    /**
//     *
//     * @param postCode
//     * @return
//     */
//    public Vector getSuburbsWithinAPostCode(int postCode) {
//
//    }
//
//    /**
//     *
//     * @param postCode
//     * @param suburbName
//     * @return
//     */
//    public boolean isSuburbWithinPostCode(int postCode, String suburbName) {
//         use getSuburbsWithinAPostCode
//
//    }

	/*
	* Calculate geodesic distance (in m) between two points specified by latitude/longitude
	* using Vincenty inverse formula for ellipsoids
	*/
	public static double distVincenty(double p1lat, double p1lon, double p2lat, double p2lon)
	{
		double a = 6378137, b = 6356752.3142, f = 1 / 298.257223563;  // WGS-84 ellipsiod
		double L = p2lon - p1lon;
		double U1 = Math.atan((1 - f) * Math.tan(p1lat));
		double U2 = Math.atan((1 - f) * Math.tan(p2lat));
		double sinU1 = Math.sin(U1), cosU1 = Math.cos(U1);
		double sinU2 = Math.sin(U2), cosU2 = Math.cos(U2);
		double cosSqAlpha = 0;
		double sinSigma = 0;
		double cos2SigmaM = 0;
		double cosSigma = 0;
		double sigma = 0;

		double lambda = L, lambdaP = 2 * Math.PI;
		double iterLimit = 20;
		while (Math.abs(lambda - lambdaP) > 1e-12 && --iterLimit > 0)
		{
			double sinLambda = Math.sin(lambda), cosLambda = Math.cos(lambda);
			sinSigma = Math.sqrt((cosU2 * sinLambda) * (cosU2 * sinLambda) +
					(cosU1 * sinU2 - sinU1 * cosU2 * cosLambda) * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda));
			if (sinSigma == 0)
				return 0;  // co-incident points
			cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
			sigma = Math.atan2(sinSigma, cosSigma);
			double sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
			cosSqAlpha = 1 - sinAlpha * sinAlpha;
			cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;
			if (Double.isNaN(cos2SigmaM))
				cos2SigmaM = 0;  // equatorial line: cosSqAlpha=0 (6)
			double C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
			lambdaP = lambda;
			lambda = L + (1 - C) * f * sinAlpha *
					(sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));
		}
		if (iterLimit == 0)
			return 0;  // formula failed to converge

		double uSq = cosSqAlpha * (a * a - b * b) / (b * b);
		double A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
		double B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
		double deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) -
				B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
		double s = b * A * (sigma - deltaSigma);

//  s = s.toFixed(3); // round to 1mm precision
		return s;
	}

/*
 * convert lat/long in degrees to radians, for handling input values
 *
 */
	public static double llToRad(double llDeg)
	{
		return llDeg * Math.PI / 180;  // signed decimal degrees without NSEW
	}


	public static double PostCodeDistCalc(double long1, double lat1, double long2, double lat2)
	{

		return (distVincenty(llToRad(lat1), llToRad(long1), llToRad(lat2), llToRad(long2)));

	}

	private boolean isEmpty(String value)
	{
		if (value != null && !value.equals(""))
		{
			return false;
		}
		return true;
	}

}
