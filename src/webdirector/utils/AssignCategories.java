package webdirector.utils;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.Vector;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import webdirector.db.DButils;
import webdirector.db.client.ClientDataAccess;

public class AssignCategories
{
	private static Logger logger = Logger.getLogger(AssignCategories.class);
	ClientDataAccess cda = new ClientDataAccess();
	DButils db = new DButils();

	public Map getLevelOneCategories(String moduleName, String userId)
	{
		Vector vt = cda.getLevelCategories(moduleName, "1");
		Map mapOfCategories = new LinkedHashMap();
		for (int k = 0; k < vt.size(); k++)
		{
			Hashtable ht = (Hashtable) vt.get(k);
			String categoryId = (String) ht.get("Category_id");
			String categoryName = (String) ht.get("ATTR_categoryName");
			mapOfCategories.put(categoryId, categoryName);
		}
		Vector vtExist = getExistingCategories(moduleName, userId);
		for (int k = 0; k < vtExist.size(); k++)
		{
			String categoryId = (String) vtExist.get(k);
			mapOfCategories.remove(categoryId);
		}
		Map mapOfCategories1 = getSortedMap(mapOfCategories);
		return mapOfCategories1;
	}

	public Map getLevelOneCategoriesForMember(String moduleName, String userId)
	{
		Vector vt = cda.getLevelCategories(moduleName, "1");
		Map mapOfCategories = new LinkedHashMap();
		for (int k = 0; k < vt.size(); k++)
		{
			Hashtable ht = (Hashtable) vt.get(k);
			String categoryId = (String) ht.get("Category_id");
			String categoryName = (String) ht.get("ATTR_categoryName");
			mapOfCategories.put(categoryId, categoryName);
		}
		Map mapOfCategories1 = getSortedMap(mapOfCategories);
		return mapOfCategories1;
	}

	public Vector getExistingCategories(String moduleName, String userId)
	{
		String SQL_QUERY = "select category_id from user_module_access where user_id= ? and module_name= ?";
		//return db.select(SQL_QUERY);
		return db.selectQuerySingleCol(SQL_QUERY, new String[] { userId, moduleName });

	}

	public Vector getExistingElements(String moduleName, Vector categories)
	{
		Vector existingElements = new Vector();
		Vector elementIds = new Vector();
		String query = "Select Element_id from Elements_" + moduleName + " where Category_id= ?";

		for (int i = 0; i < categories.size(); i++)
		{
			//query = query.concat(" ?");
			//Vector ids = db.select(query);
			Vector ids = db.selectQuerySingleCol(query, new String[] { (String) categories.get(i) });
			for (int j = 0; j < ids.size(); j++)
			{
				elementIds.add(ids.get(j));
			}
		}
		return elementIds;
	}

	public Map getExistingCategoriesMap(String moduleName, String userId)
	{
		Vector vt = getExistingCategories(moduleName, userId);
		Map mapOfCategories = new LinkedHashMap();
		for (int k = 0; k < vt.size(); k++)
		{
			String categoryId = (String) vt.get(k);
			System.out.println("CategoryID" + categoryId);
			String categoryName = cda.getCategoryFromId(moduleName, categoryId);

			// if category no longer exists then skip putting it in the map
			if (categoryName == null)
				continue;

			mapOfCategories.put(categoryId, categoryName);
		}
		Map mapOfCategories1 = getSortedMap(mapOfCategories);
		return mapOfCategories1;
	}

	public boolean saveAssignCategories(String moduleName, String userId, String categoriesSelected)
	{
		System.out.println("In SaveAssignCategory Method =======>>>>>>>>>>>>");
		StringTokenizer str = new StringTokenizer(categoriesSelected, ",");
		db.updateData("DELETE FROM USER_MODULE_ACCESS where MODULE_NAME=?  and USER_ID=? ", new Object[] { moduleName, userId });
		int check;
		while (str.hasMoreTokens())
		{
			String categoryId = str.nextToken();
			Hashtable ht = new Hashtable();
			ht.put("MODULE_NAME", moduleName);
			ht.put("USER_ID", userId);
			ht.put("CATEGORY_ID", categoryId);
			check = db.insertData(ht, "ID", "USER_MODULE_ACCESS");
			if (check < 0)
				return false;
		}
		return true;
	}

	// **
	public boolean checkUserAssignCategory(String module, String category_id, String user_id)
	{
		logger.info("================in checkUserAssignCategory=================");
		String tempID = category_id;
		String levelOneID = category_id;
		Vector result = null;
		int count = 0;
		while (!tempID.equals("0"))
		{
			String sql = "select category_parentid from categories_" + DatabaseValidation.encodeParam(module) + " where category_id = ?";
			logger.info(sql);
			//result = new DBaccess().select(sql);
			result = new DBaccess().selectQuerySingleCol(sql, new String[] { tempID });
			if (result.size() == 0)
			{
				return false;
			}
			if (count++ > 100)
			{
				return false;
			}
			levelOneID = tempID;
			tempID = (String) result.get(0);
		}

		String query = "Select distinct Module_Name from user_module_access where User_id = ? " + "and Category_id = ? and Module_Name = ?";
		logger.info(query);
		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query, new String[] { "Elements" + user_id, levelOneID, module });

		logger.info("================out checkUserAssignCategory=================");
		if (v.size() == 0)
			return false;
		else
			return true;
	}

	public boolean deleteAssignCategories(String moduleName, String modulePrefix, String Id)
	{

		String whereClause = "";
		Object[] params;

		// If modulePrefix is ellements than we are going to delete all entry
		// where
		// USER_ID = Elements+Id
		if (modulePrefix.equalsIgnoreCase("Elements"))
		{
			whereClause = "USER_ID = ?";
			//params[index++] = "Elements" + Id;
			params = new Object[] { "Elements" + Id };
		}
		// Else we have deleted categoryID so need to delete all entries where
		// ModuleName and categor_id matched with arguments
		else
		{
			whereClause = "CATEGORY_ID = ? and MODULE_NAME=?";
			//params[index++] = Id;
			//params[index++] = moduleName;
			params = new Object[] { Id, moduleName };

		}
		String query = "delete from USER_MODULE_ACCESS where " + whereClause;
		System.out.println("Querry====>>>>" + query);
		db.updateData(query, params);
		return true;
	}

	public Map getSortedMap(Map hmap)
	{

		Map map = new LinkedHashMap();
		List mapKeys = new ArrayList(hmap.keySet());
		List mapValues = new ArrayList(hmap.values());
		hmap.clear();
		TreeSet sortedSet = new TreeSet(mapValues);
		Object[] sortedArray = sortedSet.toArray();
		int size = sortedArray.length;
		// a) Ascending sort

		for (int i = 0; i < size; i++)
		{

			map.put(mapKeys.get(mapValues.indexOf(sortedArray[i])), sortedArray[i]);
		}
		return map;
	}

	public Map getAvailableCategoriesMap(Map allMap, Map existingMap)
	{

		Map map = new LinkedHashMap();

		Iterator alliterator = allMap.entrySet().iterator();
		Hashtable existHash = convertMapToHashtable(existingMap);
		String key = "";
		String categoryName = "";
		while (alliterator.hasNext())
		{
			Map.Entry entry = (Map.Entry) alliterator.next();
			key = (String) entry.getKey();
			categoryName = (String) entry.getValue();
			if (!existHash.containsKey(key))
			{
				map.put(key, categoryName);
			}
		}
		return map;
	}

	private Hashtable convertMapToHashtable(Map allMap)
	{
		Hashtable h = new Hashtable();
		Iterator alliterator = allMap.entrySet().iterator();
		Object key = new Object();
		Object value = new Object();
		while (alliterator.hasNext())
		{
			Map.Entry entry = (Map.Entry) alliterator.next();
			key = entry.getKey();
			value = entry.getValue();
			h.put(key, value);
		}
		return h;
	}

	public boolean isLoginNeeded(String moduleName)
	{
		String moduleforuser = "";
		String sql = "select attr_value from elements_flex9 where attr_jspvariablename = 'EnableUserAccessControlModuleList' and live = 1";
		//Vector v = new DBaccess().select(sql);
		Vector v = new DBaccess().selectQuerySingleCol(sql);
		if (v.size() > 0)
		{
			moduleforuser = "," + (String) v.get(0) + ",";
		}
		return moduleforuser.indexOf(moduleName) != -1;
	}

	public boolean isCategoryControlled()
	{
		String check = "select attr_value from elements_flex9 where attr_jspvariablename = 'EnableCategoryLevelUserAccessControl' and live = 1";
		//Vector v = new DBaccess().select(check);
		Vector v = new DBaccess().selectQuerySingleCol(check);
		if (v.size() == 0)
		{
			return false;
		}
		if ("YES".equals(v.get(0)))
		{
			return true;
		}
		return false;
	}
}
