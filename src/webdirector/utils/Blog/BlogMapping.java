package webdirector.utils.Blog;

import webdirector.db.client.ClientDataAccess;

import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;

/**
 * Created by IntelliJ IDEA.
 * User: CorpInteractive
 * Date: 4/08/2009
 * Time: 14:33:48
 * To change this template use File | Settings | File Templates.
 */
public class BlogMapping
{

	DBaccess du = new DBaccess();
	ClientDataAccess cda = new ClientDataAccess();

	Logger logger = Logger.getLogger(BlogMapping.class);

	public Vector getAssignedCategoriesForPost(String postId)
	{

		String select = "Select catid from categorypostmapping where PostId = ?";

		//return du.select(select);
		return du.selectQuerySingleCol(select, new String[] { postId });
	}

	public Hashtable getAssignedCategoriesForPostC(String postId)
	{

		Hashtable hReturn = new Hashtable();
		String select = "Select cp.catid,c.Name from categorypostmapping cp, category c where " +
				"cp.PostId = ? and cp.catid = c.id ";
		String cols[] = { "", "" };
		//Vector v = du.select(select,cols);
		Vector v = du.selectQuery(select, new String[] { postId });
		if (v != null && v.size() > 0)
		{
			for (int i = 0; i < v.size(); i++)
			{
				String data[] = (String[]) v.elementAt(i);
				hReturn.put(data[0], data[1]);
			}
		}
		return hReturn;
	}

	public boolean assignCategories(String postId, String[] catId)
	{
		logger.info("********** insde assigned Categories ***** " + postId + " catId *** " + catId);

		boolean status = true;
		String select = "delete from categorypostmapping where PostId = ?";
		du.updateData(select, new Object[] { postId });


		if (catId != null && catId.length > 0)
		{
			for (int i = 0; i < catId.length; i++)
			{

				String insertQ = "insert into categorypostmapping set catId = ?, PostId = ?";
				logger.info(insertQ);
				int insert = du.insertQuery(insertQ, new Object[] { catId[i], postId });
				status = (insert > 0);
				logger.info(" insert " + insert);
				if (!status)
				{
					break;
				}
			}
		}
		return status;

	}
}
