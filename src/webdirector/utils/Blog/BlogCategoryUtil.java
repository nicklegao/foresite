package webdirector.utils.Blog;

import webdirector.db.client.ClientDataAccess;

import java.util.Hashtable;
import java.util.Vector;
import java.util.Iterator;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;

/**
 * Created by IntelliJ IDEA.
 * User: CorpInteractive
 * Date: 4/08/2009
 * Time: 13:47:44
 * To change this template use File | Settings | File Templates.
 */
public class BlogCategoryUtil
{

	DBaccess du = new DBaccess();
	ClientDataAccess cda = new ClientDataAccess();
	Logger logger = Logger.getLogger(BlogCategoryUtil.class);

	public boolean copyCategoryFromBlog(String elementID, String idBlogTo, String toCategoryId, String moduleName)
	{
		try
		{
			ClientDataAccess cda = new ClientDataAccess();
			int categoryFrom = Integer.parseInt(cda.getCategoryFromElement(moduleName, elementID));
			//Vector catAlreadyExists  = du.select("select * from Category where blogId='"+toCategoryId+"' ");
			Vector catAlreadyExists = du.selectQuery("select * from Category where blogId=? ", new String[] { toCategoryId });
			int res = 0;
			if (null == catAlreadyExists || catAlreadyExists.size() == 0)
			{
				res = du.insertQuery("insert into Category(Name,Module,blogId) select Name, Module,'" + toCategoryId + "' from Category where blogId=?", new Object[] { categoryFrom });
				logger.info("BLOG insert into Category(Name,Module,blogId) select Name, Module,'" + toCategoryId + "' from Category where blogId='" + categoryFrom + "'" + "'" + res);
			}
			String sql = " insert into categorypostmapping(catid,postId) select id," + idBlogTo + " from category where Name in ( " +
					" select Name from category where id in ( " +
					" select catid from categorypostmapping " +
					" where postId = ? )) and blogid=? ";
			du.insertQuery(sql, new Object[] { elementID, toCategoryId });
			logger.error(sql);
			return res > 0;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}

	public int addCategory(String moduleName, String name, String blogId)
	{
		int retId = 0;
		if (!checkCategoryExist(moduleName, name, blogId))
		{
			Hashtable ht = new Hashtable();
			ht.put("Module", moduleName);
			ht.put("Name", name);
			ht.put("blogId", blogId);
			retId = du.insertData(ht, "id", "Category");
		}
		else
		{
			retId = -1000;
		}
		return retId;
	}

	public int editCategory(String moduleName, String id, String name, String blogId)
	{
		Hashtable ht = new Hashtable();
		ht.put("Module", moduleName);
		ht.put("Name", name);
		ht.put("id", id);
		ht.put("blogId", blogId);
		return du.insertData(ht, "id", "Category");
	}

	public int deleteCategory(String id)
	{
		String SQL_QUERY = "delete from Category where id=?";
		String sql = "delete from categorypostmapping where catId = ?";
		du.updateData(sql, new Object[] { id });
		return du.updateData(SQL_QUERY, new Object[] { id });
	}

	public int deleteCategories(String ids)
	{
		String SQL_QUERY = "delete from Category where id in (" + ids + ")";
		String sql = "delete from categorypostmapping where catId in (" + ids + ")";
		du.updateData(sql);
		return du.updateData(SQL_QUERY);
	}

	/**
	 * This method is used when we delete category from webdirector using
	 * foldertree delete categories funtions.
	 * called from dynamicFormBuilder_categoryDelete.jsp page
	 * 
	 * @param id
	 * @return
	 */
	public int deleteBlogCategories(String id)
	{
		String sql = "delete from categorypostmapping where catId= ?";
		du.updateData(sql, new Object[] { id });
		String SQL_QUERY = "delete from Category where blogId = ?";
		return du.updateData(SQL_QUERY, new Object[] { id });

	}

	/**
	 * This method is used when we delete category from webdirector using
	 * foldertree delete categories funtions.
	 * called from dynamicFormBuilder_categoryDelete.jsp page
	 * 
	 * @param id
	 * @return
	 */
	public int deleteBlogCategoriesMapping(String id)
	{

		String sql = "delete from categorypostmapping where postId = ?";
		return du.updateData(sql, new Object[] { id });
	}

	public Hashtable getAvailCategoryForBlog(String blogId)
	{
		Hashtable h = new Hashtable();
		String select = "Select id,Name from Category where blogId = ?";
		String[] cols = { "id", "name" };
		//Vector v = du.select(select,cols);
		Vector v = du.selectQuery(select, new String[] { blogId });
		if (v != null && v.size() > 0)
		{
			for (int i = 0; i < v.size(); i++)
			{
				String[] data = (String[]) v.get(i);
				h.put(data[0], data[1]);
			}
		}
		return h;
	}

	/**
	 * This method will remove assigned categories from available hash and
	 * return available hashtable it back
	 * 
	 * @param available
	 * @param assigned
	 * @return
	 */
	public Hashtable getAvailableCategoriesWithoutAssigned(Hashtable available, Hashtable assigned)
	{
		if (assigned != null && assigned.size() > 0)
		{
			for (Iterator<String> iterator = assigned.keySet().iterator(); iterator.hasNext();)
			{
				String key = iterator.next();
				if (available.containsKey(key))
				{
					available.remove(key);
				}
			}
		}
		return available;
	}

	private boolean checkCategoryExist(String moduleName, String name, String blogId)
	{
		String select = "Select id from Category where name=? and blogId=? and module = ?";
		//Vector v = du.select(select);
		Vector v = du.selectQuerySingleCol(select, new String[] { name, blogId, moduleName });
		return (v != null && v.size() > 0);
	}
}
