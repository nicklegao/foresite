// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   DBexport.java

package webdirector.db;

import java.io.*;
import java.sql.*;
import java.util.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import webdirector.db.client.ClientDataAccess;

// Referenced classes of package webdirector.db:
//            DButils

public class DBContractExport extends DBaccess
{

	public DBContractExport()
	{
	}

	public int exportElementFieldsToCSVByTreeTraversal(String table, String fields[], String categoryId, boolean writeHeader, int folderLevelsInModule, boolean exportOnlyLive)
	{
		ClientDataAccess cda = new ClientDataAccess();
		int exportRecordCount = 0;
		Vector catInList = new Vector();
		Vector catOutList = new Vector();
		if ("0".equals(categoryId))
		{
			Vector v = cda.getLevelCategories(table, "1");
			Hashtable h = null;
			Enumeration e = v.elements();
			do
			{
				if (!e.hasMoreElements())
					break;
				h = (Hashtable) e.nextElement();
				if (h.get("Category_id") != null)
					catInList.addElement(h.get("Category_id"));
			} while (true);
		}
		else
		{
			catInList.addElement(categoryId);
		}
		label0: for (int ndx = 0; ndx < catInList.size(); ndx++)
		{
			String catId = (String) catInList.elementAt(ndx);
			Vector v = cda.getCategoryChildren(table, catId);
			if (v.size() == 0)
			{
				catOutList.addElement(catId);
				continue;
			}
			Hashtable h = null;
			Enumeration e = v.elements();
			do
			{
				do
				{
					if (!e.hasMoreElements())
						continue label0;
					h = (Hashtable) e.nextElement();
				} while (h.get("Category_id") == null);
				catInList.addElement(h.get("Category_id"));
			} while (true);
		}

		for (int ndx = 0; ndx < catOutList.size(); ndx++)
			exportRecordCount += exportElementFieldsToCSVForOneCategory(table, fields, (String) catOutList.elementAt(ndx), ndx != 0, ndx != 0 ? false : writeHeader, folderLevelsInModule, exportOnlyLive);

		return exportRecordCount;
	}

	public int exportElementFieldsToCSVForOneCategory(String table, String fields[], String categoryId, boolean append, boolean writeHeader, int folderLevelsInModule, boolean exportOnlyLive)
	{
		String tableCols[];
		if (folderLevelsInModule > 0)
		{
			tableCols = fields;
			StringBuffer catFromTables = new StringBuffer("Categories_" + table + " c1 ");
			StringBuffer catWhereTables = new StringBuffer();
			for (int catNdx = 2; catNdx <= folderLevelsInModule; catNdx++)
			{
				catFromTables.append(", Categories_" + table + " c" + catNdx + " ");
				catWhereTables.append("and c" + catNdx + ".Category_ParentID = c" + (catNdx - 1) + ".Category_id ");
			}

			String query = "";
			for (int i = 0; i < tableCols.length; i++)
				query = query + tableCols[i] + ", ";

			query = query.substring(0, query.length() - 2);
			String cols = query;
			query = "select " + query + " " + "from Elements_" + table + " e ," + catFromTables.toString() + "where e.Category_id = '" + categoryId + "'" + " and c" + folderLevelsInModule + ".Category_id = e.Category_id " + catWhereTables.toString() + (exportOnlyLive ? " and Live = 1 " : "");
			System.out.println(query);
			int fileWrite = selectDataAndExport(query, tableCols, "Elements_" + table, append, writeHeader, false, false);
			return fileWrite;
		}
		tableCols = fields;
		String query = "";
		for (int i = 0; i < tableCols.length; i++)
			query = query + tableCols[i] + ", ";

		query = query.substring(0, query.length() - 2);
		String cols = query;
		query = "select " + query + " from Elements_" + table + " where Category_id = '" + categoryId + "'";
		System.out.println(query);
		int fileWrite = selectDataAndExport(query, tableCols, "Elements_" + table, append, writeHeader, false, false);
		return fileWrite;
	}

	public String exportElementTableToCSV(String table)
	{
		String retInfo = "CSV file exported for table : " + table;
		ClientDataAccess cda = new ClientDataAccess();
		String tableCols[] = cda.getElementCols(table);
		String query = "";
		for (int i = 0; i < tableCols.length; i++)
			query = query + tableCols[i] + ", ";

		query = query.substring(0, query.length() - 2);
		String cols = query;
		query = "select " + query + " from Elements_" + table;
		System.out.println(query);
		int fileWrite = selectDataAndExport(query, tableCols, "Elements_" + table, false, false, false, false);
		return retInfo + " (" + fileWrite + ")";
	}

	private String writeVectorToFile(Vector v, String table, String cols, boolean append, boolean writeHeader)
	{

		String OSTypeTempDir = d.getOStypeTempDir();
		File f = new File(OSTypeTempDir + table + ".csv");
		try
		{
			FileWriter fw = new FileWriter(f, append);
			if (writeHeader)
				fw.write(cols + "\r\n");
			for (int i = 0; i < v.size(); i++)
			{
				StringBuffer sb = new StringBuffer();
				String row[] = (String[]) v.elementAt(i);
				for (int x = 0; x < row.length; x++)
				{
					sb.append(transformTextFieldForExport(row[x]));
					if (x + 1 < row.length)
						sb.append(",");
				}

				fw.write(sb.toString() + "\r\n");
			}

			fw.close();
		}
		catch (IOException io)
		{
			System.out.println("DBexport.writeVectorToFile exception: " + io.toString());
			System.out.println("ERROR: Writing to " + f.getName());
			return "File write failed";
		}
		return "OK - " + f.toString();
	}

	public static String transformTextFieldForExport(String input)
	{
		if (input.indexOf(",") > -1 || input.indexOf("\"") > -1 || input.indexOf("'") > -1 || input.indexOf("\\") > -1 || input.indexOf("\t") > -1 || input.indexOf("\n") > -1 || input.indexOf("\f") > -1 || input.indexOf("\r") > -1)
		{
			StringBuffer sb = new StringBuffer(input);
			int where;
			for (where = -2; (where = sb.indexOf("\\", where + 2)) > -1;)
				sb.replace(where, where + 1, "\\\\");

			while ((where = sb.indexOf(",")) > -1)
				sb.replace(where, where + 1, "\\u002c");
			while ((where = sb.indexOf("\"")) > -1)
				sb.replace(where, where + 1, "\\u0022");
			while ((where = sb.indexOf("'")) > -1)
				sb.replace(where, where + 1, "\\u0027");
			while ((where = sb.indexOf("\t")) > -1)
				sb.replace(where, where + 1, "\\t");
			while ((where = sb.indexOf("\n")) > -1)
				sb.replace(where, where + 1, "\\n");
			while ((where = sb.indexOf("\r")) > -1)
				sb.replace(where, where + 1, "\\r");
			while ((where = sb.indexOf("\f")) > -1)
				sb.replace(where, where + 1, "\\f");
			return "\"" + sb.toString() + "\"";
		}
		else
		{
			return input;
		}
	}

	public int selectDataAndExport(String query, String cols[], String table, boolean append, boolean writeHeader, boolean getMetaData, boolean useCache)
	{
		int rowsExported;
		label0:
		{
			Connection conn = null;
			Statement stmt = null;
			ResultSet rs = null;
			String OSTypeTempDir = d.getOStypeTempDir();
			File f = new File(OSTypeTempDir + table + ".csv");
			rowsExported = 0;
			try
			{
				byte byte0;
				try
				{
					Context ctx = new InitialContext();
					if (ctx == null)
						throw new Exception("Boom - No Context");
					String dbName = d.getOdbcConString();
					int index = dbName.lastIndexOf(":") + 1;
					DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/" + dbName.substring(index));
					if (ds != null)
					{
						synchronized (ds)
						{
							conn = ds.getConnection();
						}
						if (conn != null)
						{
							stmt = conn.createStatement();
							rs = stmt.executeQuery(query);
							if (getMetaData)
								getMetaData(rs);
							FileWriter fw = new FileWriter(f, append);
							if (writeHeader)
							{
								for (int i = 0; i < cols.length; i++)
									fw.write(cols[i] + ",");

								fw.write("\r\n");
							}
							while (rs.next())
							{
								StringBuffer sb = new StringBuffer();
								for (int i = 0; i < cols.length; i++)
								{
									String data[] = new String[cols.length];
									Object x = rs.getString(i + 1);
									if ((String) x == null)
									{
										data[i] = "";
									}
									else
									{
										String s = (String) x;
										data[i] = s.trim();
									}
									sb.append(transformTextFieldForExport(data[i]));
									if (i + 1 < cols.length)
										sb.append(",");
								}

								sb.append("\r\n");
								fw.write(sb.toString());
								rowsExported++;
							}
							fw.close();
						}
					}
					break label0;
				}
				catch (IOException io)
				{
					System.out.println("DBexport.writeVectorToFile exception: " + io.toString());
					System.out.println("ERROR: Writing to " + f.getName());
					byte0 = -99;
				}
				catch (Exception e)
				{
					e.printStackTrace();
					break label0;
				}
				return byte0;
			}
			finally
			{
				if (rs != null)
				{
					try
					{
						rs.close();
					}
					catch (SQLException e)
					{
					}
					rs = null;
				}
				if (stmt != null)
				{
					try
					{
						stmt.close();
					}
					catch (SQLException e)
					{
					}
					stmt = null;
				}
				if (conn != null)
				{
					try
					{
						conn.close();
					}
					catch (SQLException e)
					{
					}
					conn = null;
				}
			}
		}
		return rowsExported;
	}

	public int selectDataAndExport(String query, String cols[], String table, boolean append, boolean writeHeader, boolean getMetaData, boolean useCache, String filePath)
	{
		int rowsExported;
		label0:
		{
			Connection conn = null;
			Statement stmt = null;
			ResultSet rs = null;
			File f = new File(filePath);
			rowsExported = 0;
			try
			{
				byte byte0;
				try
				{
					Context ctx = new InitialContext();
					if (ctx == null)
						throw new Exception("Boom - No Context");
					String dbName = d.getOdbcConString();
					int index = dbName.lastIndexOf(":") + 1;
					DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/" + dbName.substring(index));
					if (ds != null)
					{
						synchronized (ds)
						{
							conn = ds.getConnection();
						}
						if (conn != null)
						{
							stmt = conn.createStatement();
							rs = stmt.executeQuery(query);
							if (getMetaData)
								getMetaData(rs);
							FileWriter fw = new FileWriter(f, append);
							if (writeHeader)
							{
								for (int i = 0; i < cols.length; i++)
									fw.write(cols[i] + ",");

								fw.write("\r\n");
							}
							while (rs.next())
							{
								StringBuffer sb = new StringBuffer();
								for (int i = 0; i < cols.length; i++)
								{
									String data[] = new String[cols.length];
									Object x = rs.getString(i + 1);
									if ((String) x == null)
									{
										data[i] = "";
									}
									else
									{
										String s = (String) x;
										data[i] = s.trim();
									}
									sb.append(transformTextFieldForExport(data[i]));
									if (i + 1 < cols.length)
										sb.append(",");
								}

								sb.append("\r\n");
								fw.write(sb.toString());
								rowsExported++;
							}
							fw.close();
						}
					}
					break label0;
				}
				catch (IOException io)
				{
					System.out.println("DBexport.writeVectorToFile exception: " + io.toString());
					System.out.println("ERROR: Writing to " + f.getName());
					byte0 = -99;
				}
				catch (Exception e)
				{
					e.printStackTrace();
					break label0;
				}
				return byte0;
			}
			finally
			{
				if (rs != null)
				{
					try
					{
						rs.close();
					}
					catch (SQLException e)
					{
					}
					rs = null;
				}
				if (stmt != null)
				{
					try
					{
						stmt.close();
					}
					catch (SQLException e)
					{
					}
					stmt = null;
				}
				if (conn != null)
				{
					try
					{
						conn.close();
					}
					catch (SQLException e)
					{
					}
					conn = null;
				}
			}
		}
		return rowsExported;
	}

	private static final String newLine = "\n";
	private static final String carriageReturn = "\r";
	private static final String formFeed = "\f";
}
