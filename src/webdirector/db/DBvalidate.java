package webdirector.db;

import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 14/01/2007
 * Time: 11:49:34
 * $Id: DBvalidate.java,v 1.1 2011-10-27 03:32:35 dan Exp $
 * Class used for validating the schema and ensuring creation of anthing
 * necessary
 */
public class DBvalidate extends DBmanage
{

	/**
	 * uses sysobjects table to check if the table is listed and is a U (User
	 * owned) table
	 * 
	 * @param tableName
	 *            to check exists
	 * @return
	 */
	public boolean tableExists(String tableName)
	{
		String sql = "SELECT name FROM sysobjects WHERE name = ? AND type = 'U'";
		//Vector v = select(sql);
		Vector v = selectQuerySingleCol(sql, new String[] { tableName });

		if (v.size() != 1)
			return false;
		else
			return true;
	}


}
