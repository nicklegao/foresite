package webdirector.db;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import webdirector.db.client.ClientDataAccess;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import au.net.webdirector.common.utils.email.EmailUtils;

import com.csvreader.CsvWriter;

public class DBexport
{
	// private static final Logger LOGGER = Logger.getLogger( ExportUtils.class );
	private static DBaccess _dba = new DBaccess();
	private static ClientDataAccess _cda = new ClientDataAccess();
	private static Defaults _d = Defaults.getInstance();

	private static final String NEW_LINE = "\n";
	private static final String CARRIAGE_RETURN = "\r";

	/**
	 * Gets the specified fields from the category and writes them to a file.
	 * 
	 * @param table
	 * @param fields
	 * @param categoryId
	 * @param append
	 * @param writeHeader
	 * @param folderLevelsInModule
	 * @param exportOnlyLive
	 * @return number of rows exported
	 */
	public int exportCategoryFieldsToCSVForOneCategory(String table, String fields[], String categoryId, boolean append, boolean writeHeader, int folderLevelsInModule, boolean exportOnlyLive)
	{

		if (folderLevelsInModule > 0)
		{
			String[] tableCols = fields;

			StringBuffer catFromTables = new StringBuffer("Categories_" + table + " c1 ");
			StringBuffer catWhereTables = new StringBuffer();
			// append with comma start, space end

			for (int catNdx = 2; catNdx <= folderLevelsInModule; catNdx++)
			{
				catFromTables.append(", Categories_" + table + " c" + catNdx + " ");
				// catWhereTables.append(" and c"+catNdx+".Category_id =  c"+(catNdx-1)+".Category_id");
				catWhereTables.append("and c" + (catNdx) + ".Category_ParentID = c" + (catNdx - 1) + ".Category_id ");
			}

			// build query
			String query = "";
			for (int i = 0; i < tableCols.length; i++)
				query += tableCols[i] + ", ";

			query = query.substring(0, query.length() - 2);
			String cols = query;
			query = "select " + query + " " + "from Categories_" + table + " c ," + catFromTables.toString() + "where c.Category_id = '" + categoryId + "'" + " and c" + folderLevelsInModule + ".Category_id = c.Category_id " + catWhereTables.toString() + (exportOnlyLive ? " and Live = 1 " : "");
			System.out.println("Queery where error occures" + query);
			int fileWrite = selectDataAndExport(query, tableCols, "Categories_" + table, append, writeHeader, false, false);

			System.out.println("File write variable" + fileWrite);
			return fileWrite;
		}

		else
		{
			// ClientDataAccess cda = new ClientDataAccess();
			String[] tableCols = fields;

			// build query
			String query = "";
			for (int i = 0; i < tableCols.length; i++)
				query += tableCols[i] + ", ";

			query = query.substring(0, query.length() - 2);
			String cols = query;
			query = "select " + query + " from Categories_" + table + " c" + " where c.Category_id = '" + categoryId + "'";
			System.out.println(query);

			int fileWrite = selectDataAndExport(query, tableCols, "Categories_" + table, append, writeHeader, false, false);

			return fileWrite;
		}
	}

	/**
	 * Return how many element records were exported File export to is always
	 * C:\temp\{MODULE_NAME}.csv
	 * 
	 * @param table
	 *            Name of table (aka module)
	 * @param fields
	 *            Array of fields to export (in order)
	 * @param categoryId
	 *            Parent category to export them from (0 is all tree)
	 * @return indicating how many element records where exported
	 */
	public int exportElementFieldsToCSVByTreeTraversal(String table, String fields[], String categoryId, boolean writeHeader, int folderLevelsInModule, boolean exportOnlyLive)
	{
		return exportElementFieldsToCSVByTreeTraversal(table, fields, categoryId, writeHeader, folderLevelsInModule, exportOnlyLive, false);
	}

	/**
	 * Return how many element records where exported File export to is always
	 * C:\temp\{MODULE_NAME}.csv
	 * 
	 * @param table
	 * @param fields
	 *            Array of fields to export (in order)
	 * @param categoryId
	 *            parent category to export them from (0 is all tree)
	 * @param writeHeader
	 * @param folderLevelsInModule
	 * @param exportOnlyLive
	 * @param type
	 *            type is for selecting export from element table or category
	 *            table type true = Category table type false = Elements table
	 * @return number of rows exported
	 */
	public int exportElementFieldsToCSVByTreeTraversal(String table, String fields[], String categoryId, boolean writeHeader, int folderLevelsInModule, boolean exportOnlyLive, boolean type)
	{
		System.out.println("In exportElementFieldsToCSVByTreeTraversal");
		webdirector.db.client.ClientDataAccess cda = new webdirector.db.client.ClientDataAccess();
		int exportRecordCount = 0;

		Vector catInList = new Vector();
		Vector catOutList = new Vector();

		// special case for exporting all records - using id 0
		if ("0".equals(categoryId))
		{
			Vector v = cda.getLevelCategories(table, "1");
			Hashtable h = null;
			for (Enumeration e = v.elements(); e.hasMoreElements();)
			{
				h = (Hashtable) e.nextElement();
				// System.out.println(h);
				if (h.get("Category_id") != null)
					catInList.addElement(h.get("Category_id"));
			}
		}
		else
		{
			catInList.addElement(categoryId);
		}

		for (int ndx = 0; ndx < catInList.size(); ndx++)
		{
			String catId = (String) catInList.elementAt(ndx);

			Vector v = cda.getCategoryChildren(table, catId);

			// it's a category with no category children - could be a bottom most
			// category with real elements in it - try to export it...
			if (v.size() == 0)
			{
				catOutList.addElement(catId);
			}
			else
			{
				// it's a category with real category children - could be a middle level category
				// lets explore each of it's children...
				Hashtable h = null;
				for (Enumeration e = v.elements(); e.hasMoreElements();)
				{
					h = (Hashtable) e.nextElement();
					// System.out.println(h);
					if (h.get("Category_id") != null)
						catInList.addElement(h.get("Category_id"));
				}
			}
		}

		if (type)
		{
			// visit each possible bottom most category to do append-export
			// it's fine if we have empty category (i.e. no element children)
			for (int ndx = 0; ndx < catOutList.size(); ndx++)
			{
				exportRecordCount += exportCategoryFieldsToCSVForOneCategory(table, fields, (String) catOutList.elementAt(ndx), ndx != 0 /* overwrite file on first write */, ndx == 0 ? writeHeader : false /* only write header first time */, folderLevelsInModule, exportOnlyLive);
			}
		}
		else
		{
			for (int ndx = 0; ndx < catOutList.size(); ndx++)
			{
				exportRecordCount += exportElementFieldsToCSVForOneCategory(table, fields, (String) catOutList.elementAt(ndx), ndx != 0 /* overwrite file on first write */, ndx == 0 ? writeHeader : false /* only write header first time */, folderLevelsInModule, exportOnlyLive);
			}

		}
		return exportRecordCount;
	}

	/**
	 * Gets the specified fields from the category and writes them to a file.
	 * 
	 * @param table
	 * @param fields
	 * @param categoryId
	 * @param append
	 * @param writeHeader
	 * @param folderLevelsInModule
	 * @param exportOnlyLive
	 * @return number of rows exported
	 */
	public int exportElementFieldsToCSVForOneCategory(String table, String fields[], String categoryId, boolean append, boolean writeHeader, int folderLevelsInModule, boolean exportOnlyLive)
	{
		if (folderLevelsInModule > 0)
		{
			// ClientDataAccess cda = new ClientDataAccess();
			String[] tableCols = fields;

			StringBuffer catFromTables = new StringBuffer("Categories_" + table + " c1 ");
			StringBuffer catWhereTables = new StringBuffer();
			// append with comma start, space end

			for (int catNdx = 2; catNdx <= folderLevelsInModule; catNdx++)
			{
				catFromTables.append(", Categories_" + table + " c" + catNdx + " ");
				// catWhereTables.append(" and c"+catNdx+".Category_id =  c"+(catNdx-1)+".Category_id");
				catWhereTables.append("and c" + (catNdx) + ".Category_ParentID = c" + (catNdx - 1) + ".Category_id ");
			}

			// build query
			String query = "";
			DatabaseValidation.encodeParam(tableCols);
			for (int i = 0; i < tableCols.length; i++)
				query += tableCols[i] + ", ";
			// LOGGER.info( "query" + query );
			System.out.println("query: " + query);
			query = query.substring(0, query.length() - 2);
			String cols = query;
			query = "select " + query + " " + "from Elements_" + DatabaseValidation.encodeParam(table) + " e ," + catFromTables.toString() + "where e.Category_id = '" + DatabaseValidation.encodeParam(categoryId) + "'" + " and c" + folderLevelsInModule + ".Category_id = e.Category_id " + catWhereTables.toString() + (exportOnlyLive ? " and Live = 1 " : "");
			System.out.println("Query in Elements for Table" + query);
			int fileWrite = selectDataAndExport(query, tableCols, "Elements_" + table, append, writeHeader, false, false);
			// System.out.println("File write variable" + fileWrite);
			return fileWrite;
		}
		else
		{
			// ClientDataAccess cda = new ClientDataAccess();
			String[] tableCols = fields;

			// build query
			String query = "";
			for (int i = 0; i < tableCols.length; i++)
				query += tableCols[i] + ", ";

			query = query.substring(0, query.length() - 2);
			String cols = query;
			query = "select " + query + " from Elements_" + table + " where Category_id = '" + DatabaseValidation.encodeParam(categoryId) + "'";
			System.out.println(query);

			int fileWrite = selectDataAndExport(query, tableCols, "Elements_" + table, append, writeHeader, false, false);

			return fileWrite;
		}
	}

	/**
	 * Exports all of the element table specified to a CSV file. Each column in
	 * the table is a a field in the CSV file.
	 * 
	 * @param table
	 * @return
	 */
	public String exportElementTableToCSV(String table)
	{
		String retInfo = "CSV file exported for table : " + table;
		String[] tableCols = _cda.getElementCols(table);
		DatabaseValidation.encodeParam(tableCols);
		// build query
		String query = "";
		for (int i = 0; i < tableCols.length; i++)
			query += tableCols[i] + ", ";
		query = query.substring(0, query.length() - 2);
		String cols = query;
		query = "select " + query + " from Elements_" + table;
		System.out.println(query);

		int fileWrite = selectDataAndExport(query, tableCols, "Elements_" + table, false, false, false, false);

		return retInfo + " (" + fileWrite + ")";
	}

	public int selectDataAndExport(String query, String[] cols, String table, boolean append, boolean writeHeader, boolean getMetaData, boolean useCache)
	{
		System.out.println("In selectDataAndExportNew");
		// LOGGER.info( "New Export starts" );
		System.out.println("New Export starts");

		//Vector v = _dba.select( query, cols );
		// TODO validated
		Vector v = _dba.selectQuery(query);

		writeVectorToFile(v, table, cols, append, writeHeader);

		// LOGGER.info( "New Export ends" );
		System.out.println("New Export ends");
		return v.size();
	}

	/**
	 * Internal method to write the physical file given a vector of contents.
	 * Allows you to append to the csv and write column headings if required.
	 * 
	 * @param v
	 * @param table
	 * @param cols
	 * @param append
	 * @param writeHeader
	 * @return
	 */
	private String writeVectorToFile(Vector v, String table, String[] cols, boolean append, boolean writeHeader)
	{
		String type = table.toLowerCase().startsWith("categories") ? "category" : "element";
		String module = StringUtils.split(table, "_")[1];
		String tempDir = _d.getOStypeTempDir();
		File f = new File(tempDir + table + ".csv");

		try
		{
			FileWriter fw = new FileWriter(f, append);

			if (writeHeader)
			{
				// fw.write( cols + "\r\n" );
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < cols.length; i++)
				{
					sb.append(cols[i]);
					if (i + 1 < cols.length)
					{
						sb.append(",");
					}
				}
				fw.write(sb.toString() + "\r\n");
			}

			for (int i = 0; i < v.size(); i++)
			{
				StringBuffer sb = new StringBuffer();
				String[] row = (String[]) v.elementAt(i);

				for (int x = 0; x < row.length; x++)
				{
					if (cols[x].toLowerCase().indexOf(".attrmulti_") != -1)
					{
						String[] aliasAndFieldName = StringUtils.split(cols[x], ".");
						int idFieldPos = getIdFieldPos(cols, aliasAndFieldName[0], module);
						if (idFieldPos != -1)
						{
							List<String> value = _dba.selectQuerySingleCol("select selected_value from drop_down_multi_selections where module_name= ? and element_id = ? and ATTRMULTI_Name = ? and module_type = ?", new String[] { module, row[idFieldPos], aliasAndFieldName[1], type });
							sb.append(transformTextFieldForExport(StringUtils.join(value, "|")));
						}
					}
					else
					{
						// sb.append( transformTextFieldForExport( StringEscapeUtils.escapeXml( row[x] ) ) );
						sb.append(transformTextFieldForExport(row[x]));
					}
					if (x + 1 < row.length)
						sb.append(",");
				}
				fw.write(sb.toString() + "\r\n");
			}
			fw.close();
		}
		catch (IOException io)
		{
			System.out.println("DBexport.writeVectorToFile exception: " + io.toString());
			System.out.println("ERROR: Writing to " + f.getName());
			return "File write failed";
		}
		return "OK - " + f.toString();
	}

	private int getIdFieldPos(String[] cols, String type, String module)
	{
		if (!"e".equals(type) && !"c".equals(type))
		{
			return -1;
		}
		String name = "e".equals(type) ? "e.Element_id" : "c" + _d.getModuleLevels(module) + ".category_id";
		return ArrayUtils.indexOf(cols, name);
	}

	/**
	 * Translates many special characters like tab, carriag retun, quotes etc.
	 * into escaped versions.
	 * 
	 * @param input
	 * @return
	 */
	public static String transformTextFieldForExport(String input)
	{
		System.out.println("In TransforTextField");

		if (input.indexOf(",") > -1 ||
				input.indexOf(NEW_LINE) > -1 ||
				input.indexOf(CARRIAGE_RETURN) > -1)
		{
			System.out.println("Inside if case ::::: >>>>>>>>>>");
			StringBuffer sb = new StringBuffer(input);

			int where = -2;

			while ((where = sb.indexOf("\"")) > -1)
			{
				sb.replace(where, where + 1, "\"\"");
			}

			return "\"" + sb.toString() + "\"";
		}
		return input;
	}

	public String exportAllModuleData(String module, Hashtable categoryCols, Hashtable elementCols)
	{
		// get all categories
		Vector v = _cda.allCategories(module);

		//get all category columns and element colums
		String[] catHeaders = _cda.getCategoryCols(module);
		catHeaders = filterArrays(catHeaders, categoryCols);
		String[] eleHeaders = _cda.getElementCols(module);
		eleHeaders = filterArrays(eleHeaders, elementCols);

		//set the location of the temporry file 
		String location = _d.getStoreDir() + "\\" + "_tmp\\" + module + "Data.csv";

		File f = new File(location);
		if (f.exists())
		{
			f.delete();
		}


		CsvWriter csvOut = null;
		try
		{
			//create the output file
			csvOut = new CsvWriter(new FileWriter(location, true), ',');
			// write headers at the top of the file
			csvOut.writeRecord(combineArrays(catHeaders, eleHeaders));
			// for every category
			for (int i = 0; i < v.size(); i++)
			{
				Hashtable h = (Hashtable) v.get(i);
				String category_id = (String) h.get("Category_id");
				String[] catData = hashtableToStringArray(h, catHeaders);
				// get all category assets
				Vector elements = _cda.getCategoryElements(module, category_id);
				for (int u = 0; u < elements.size(); u++)
				{
					Hashtable asset = (Hashtable) elements.get(u);
					String[] assetData = hashtableToStringArray(asset, eleHeaders);
					csvOut.writeRecord(combineArrays(catData, assetData));
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (csvOut != null)
			{
				csvOut.close();
			}
			else
			{
				return "";
			}
		}
		return location;
	}


	/**
	 * 
	 * Takes the data in the vector, stores it in a tempory file
	 * and emails to the email address
	 * 
	 * @param data
	 * @param location
	 * @param emailAddress
	 * @param emailSubject
	 * @return
	 */
	public boolean exportDataToEmailAddress(Vector<String[]> data, String location, String emailAddress, String emailSubject)
	{

		boolean ret = false;
		//delete the file is it existed
		File f = new File(location);
		if (f.exists())
		{
			f.delete();
		}

		CsvWriter csvOut = null;
		try
		{
			//create the output file
			csvOut = new CsvWriter(new FileWriter(location, true), ',');
			for (int i = 0; i < data.size(); i++)
			{
				String[] line = data.get(i);
				// write the line to the file
				csvOut.writeRecord(line);
			}

		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (csvOut != null)
			{
				// close the pointer to the file
				csvOut.close();
				EmailUtils email = new EmailUtils();
				// send the email
				ret = email.sendMail(new String[] { emailAddress }, null, null, "no-reply@traveldocs.cloud", emailSubject, "Please find your export attatched", location);
			}
		}

		return ret;
	}

	private String[] combineArrays(String[] first, String[] second)
	{
		List<String> both = new ArrayList<String>(first.length + second.length);
		Collections.addAll(both, first);
		Collections.addAll(both, second);
		return both.toArray(new String[] {});
	}

	private String[] filterArrays(String[] original, Hashtable<String, String> filter)
	{
		List<String> newList = new ArrayList<String>();
		for (int i = 0; i < original.length; i++)
		{
			System.out.println(original[i] + " = " + filter.get(original[i]));
			if (original[i].equals("Element_id") || original[i].equals("Category_id") || (filter.get(original[i]) != null && (filter.get(original[i]).equals("1") || filter.get(original[i]).equals("on") || filter.get(original[i]).equals("yes"))))
			{
				newList.add(original[i]);
			}
		}
		return newList.toArray(new String[] {});
	}

	private String[] hashtableToStringArray(Hashtable h, String[] headers)
	{
		String[] ret = new String[headers.length];
		for (int i = 0; i < headers.length; i++)
		{
			ret[i] = (String) h.get(headers[i]);
		}
		return ret;
	}


}
