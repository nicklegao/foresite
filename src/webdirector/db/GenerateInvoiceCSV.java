package webdirector.db;

import java.io.*;
import java.util.*;
import java.text.*;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import au.net.webdirector.common.utils.email.EmailUtils;
import webdirector.utils.*;
import webdirector.db.client.ClientDataAccess;

/**
 *
 * @author amitksh
 */
public class GenerateInvoiceCSV
{

	DButils db = new DButils();
	protected static Defaults d = Defaults.getInstance();
	static ClientDataAccess cda = new ClientDataAccess();
	private String moduleName = "FLEX20";


	public void generateCSV(String batchId) throws Exception
	{
		String sqlSalesOrder = "select Category_id,ATTR_AccountReference,ATTR_OrderNumber FROM CATEGORIES_" + DatabaseValidation.encodeParam(moduleName) + " WHERE ATTR_BATCHID = ?";
		String colsSalesOrder[] = { "Category_id", "ATTR_AccountReference", "ATTR_OrderNumber" };
		System.out.println("\n\n\n\n***********************************************");
		System.out.println(sqlSalesOrder);
		System.out.println("***********************************************");

		//Vector vtSalesOrder = db.select(sqlSalesOrder, colsSalesOrder);
		Vector vtSalesOrder = db.selectQuery(sqlSalesOrder, new String[] { batchId });

		String storeDir = d.getStoreDir();
		String fileName = "invoices-" + getTimeStamp() + ".csv";
		String filePath = storeDir + "/" + fileName;
		FileWriter fstream = new FileWriter(filePath);
		BufferedWriter out = new BufferedWriter(fstream);

		if (vtSalesOrder != null && vtSalesOrder.size() > 0)
		{
			for (int i = 0; i < vtSalesOrder.size(); i++)
			{
				String[] arrSalesOrder = (String[]) vtSalesOrder.get(i);
				String salesCategoryId = arrSalesOrder[0];
				String accountReference = arrSalesOrder[1];
				String orderNumber = arrSalesOrder[2];
				writeLineInCSV(out, salesCategoryId, accountReference, orderNumber);
			}
		}

		out.close();
		emailPDFReport(filePath, d.getContactUsEmail());
	}

	private boolean writeLineInCSV(BufferedWriter out, String categoryId, String accountReference, String orderNumber
			) throws IOException
	{
		//cat cols
		String customerAccountNumber = accountReference;

		//elements cols
		Vector vtElement = cda.getCategoryElements(moduleName, categoryId, true);

		//handle case of all elements, get data and write in CSV.
		String invoiceText = "";
		float netAmount = 0.0f;
		float totalTax = 0.0f;
		String t1ort9 = "";
		String nominalCode = "";
		if (vtElement != null)
		{
			for (int i = 0; i < vtElement.size(); i++)
			{
				Hashtable htElement0 = (Hashtable) vtElement.get(0);
				invoiceText = (String) htElement0.get("ATTR_Headline");
				try
				{
					float tax = Float.parseFloat((String) htElement0.get("ATTRCURRENCY_Tax"));
					if (tax == 0.00)
						t1ort9 = "T9";
					else
						t1ort9 = "T1";
				}
				catch (NumberFormatException ne)
				{
					t1ort9 = "T9";
				}

				Hashtable htElement = (Hashtable) vtElement.get(i);
				int orderQty = Integer.parseInt((String) htElement.get("ATTR_OrderQty"));
				float unitPrice = Float.parseFloat((String) htElement.get("ATTRCURRENCY_UnitPrice"));
				float tax = 0.0f;
				try
				{
					tax = Float.parseFloat((String) htElement.get("ATTRCURRENCY_Tax")) * orderQty;
				}
				catch (NumberFormatException ne)
				{
					tax = 0.0f;
				}

				netAmount += unitPrice * orderQty;
				System.out.println("==========>> i:" + i);
				System.out.println("==========>> Net Amount:" + netAmount);
				totalTax += tax;
				System.out.println("==========>> TotalTax:" + totalTax);
				nominalCode = (String) htElement0.get("ATTR_nominalCode");
			}
		}

		//Get today's date
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		java.util.Date date = new java.util.Date();
		String datetime = dateFormat.format(date);

		StringBuffer invLine = new StringBuffer();
		invLine.append("SI,");
		invLine.append(customerAccountNumber);
		invLine.append(",");
		invLine.append(nominalCode);
		invLine.append(",");
		invLine.append("0");
		invLine.append(",");
		//invLine.append("3,");
		invLine.append(datetime);
		invLine.append(",");
		invLine.append("C" + orderNumber);
		invLine.append(",");
		invLine.append(invoiceText);
		invLine.append(",");
		invLine.append(netAmount);
		invLine.append(",");
		invLine.append(t1ort9);
		invLine.append(",");
		invLine.append(totalTax);
		invLine.append(",");
		invLine.append(",");
		invLine.append("\n");

		System.out.println("writing to file");
		out.write(invLine.toString());

		return true;
	}

	public Vector getContractElementInformation(String elementId)
	{
		Vector vtElement = cda.getElement("CONTRACTS", elementId);
		return vtElement;
	}

	public Vector getContractBillingInformation(String categoryId)
	{
		System.out.println("categoryId " + categoryId);
		Vector vtCategory = cda.getCategory("CONTRACTS", categoryId, true);
		return vtCategory;
	}

	private String getTimeStamp()
	{
		Calendar cal = new GregorianCalendar();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int hr = cal.get(Calendar.HOUR_OF_DAY);
		int min = cal.get(Calendar.MINUTE);
		int sec = cal.get(Calendar.SECOND);
		return year + "-" + month + "-" + day + "-" + hr + "-" + min + "-" + sec;
	}

	public boolean emailPDFReport(String fileName, String userEmail)
	{
		boolean flg = false;
		EmailUtils eu = new EmailUtils();
		String[] to = { userEmail };
//        String[] bcc={"amitshakya1@gmail.com"};
		String from = d.getContactUsEmail();
		String body = "Please find the attached Invoice CSV Report";
		System.out.println("to " + to[0]);
		try
		{
			flg = eu.sendMail(to, null, null, from, "Invoice - CSV Report", body, fileName);
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
		eu = null;
		return flg;
	}

	/*public void generateCSV(String[] elementAndCategoryIds)
	{
	   try
	   {
	       String storeDir=d.getStoreDir();
	       String fileName = "invoicesForSage-"+getTimeStamp()+".csv";
	       String filePath = storeDir+"/"+fileName;
	       FileWriter fstream = new FileWriter(filePath);
	       BufferedWriter out = new BufferedWriter(fstream);

	       String[] elementsAndCategoryIds = elementAndCategoryIds;
	       for (int i = 0; i < elementsAndCategoryIds.length; ++i)
	       {
	           String elementAndCategoryId = elementsAndCategoryIds[i];
	           String[] elementAndCatId = elementAndCategoryId.split(":");
	           String elementId = elementAndCatId[0];
	           String categoryId = elementAndCatId[1];
	           String innerNumberOfPdfs = elementAndCatId[2];

	           double dPdfs = Double.parseDouble(innerNumberOfPdfs);
	           int intPdfs = (int)Math.ceil(dPdfs);

	           Vector vtElement = getContractElementInformation(elementId);
	           Vector vtCategory = getContractBillingInformation(categoryId);
	           Hashtable htElement = null;
	           if (vtElement.size() != 0)
	               htElement = (Hashtable)vtElement.get(0);
	           else
	               htElement = new Hashtable();

	           Hashtable htCategory = null;
	           if (vtCategory.size() != 0)
	               htCategory = (Hashtable)vtCategory.get(0);
	           else
	               htCategory = new Hashtable();

	           for (int ctr = 1; ctr <= intPdfs; ++ctr)
	           {
	               boolean status = writeLineInCSV(out,categoryId, htCategory, htElement);
	               //vtSalesCategory.add(salesCategoryId);
	           }
	       }
	       out.close();

	       emailPDFReport(filePath, "sales@traveldocs.cloud");
	   }
	   catch(Exception e)
	   {
	       e.printStackTrace();
	   }
	}*/


}
