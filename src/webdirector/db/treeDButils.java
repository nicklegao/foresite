package webdirector.db;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.RMISecurityManager;
import java.rmi.server.UnicastRemoteObject;

import au.net.webdirector.common.Defaults;

// import javax.servlet.*;

public class treeDButils extends UnicastRemoteObject implements interfaceDButils
{
	private Defaults d = Defaults.getInstance();
	protected String brand_id = null;
	private Vector cols = new Vector(20);
	private Vector labelsTable = new Vector(20);

	public treeDButils() throws RemoteException
	{
		super();
	}

	/**
	 * UNUSED.
	 * 
	 * @param args
	 */
	public static void main(String args[])
	{
		// Create and install a security manager
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager(new RMISecurityManager());
		}

		try
		{
			treeDButils obj = new treeDButils();

			// Bind this object instance to the name "HelloServer"
			Naming.rebind("//localhost/select", obj);

			System.out.println("select bound in registry");
		}
		catch (Exception e)
		{
			System.out.println("DButils err: " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Checks if a given column (identified by index within the cols[]) should
	 * be rendered or not.
	 * This is basically determined by working out if the column starts with
	 * ATTR (otherwise its a system column) and then if the
	 * OnOff flag in the labels table is on.
	 * 
	 * @param tableName
	 * @param column
	 * @return
	 */
	public boolean columnNeedsRendering(String tableName, int column)
	{
		String[] columnData = (String[]) cols.elementAt(column);
		String colName = columnData[0]; // the name

		// if its a system column then don't bother even checking OnOff
		if (!colName.startsWith("ATTR"))
			return false;

		String OnOff = "";
		for (int i = 0; i < labelsTable.size(); i++)
		{
			String[] labelsTableRow = (String[]) labelsTable.elementAt(i);
			if (labelsTableRow[0].equals(colName))
			{
				OnOff = labelsTableRow[3];
				break;
			}
		}

		if (OnOff.equals("1"))
			return true;
		else
			return false;
	}

	/**
	 * Returns the chr size of a given column in the cols[] array
	 * 
	 * @param column
	 * @return
	 */
	public String getColumnSize(int column)
	{
		String[] columnData = (String[]) cols.elementAt(column);
		String columnSize = columnData[2]; // the size

		return (columnSize);
	}

	/**
	 * Returns the info label from the labels table for the given column
	 * 
	 * @param tableName
	 * @param column
	 * @return
	 */
	public String getColumnInfo(String tableName, int column)
	{
		String[] columnData = (String[]) cols.elementAt(column);
		String columnName = columnData[0]; // the name

		String info = "";
		for (int i = 0; i < labelsTable.size(); i++)
		{
			String[] labelsTableRow = (String[]) labelsTable.elementAt(i);
			if (labelsTableRow[0].equals(columnName))
			{
				info = labelsTableRow[4];
				break;
			}
		}

		return (info);
	}

	/**
	 * Gives you the internal column name
	 * 
	 * @param column
	 * @return
	 */
	public String getInternalColumnName(int column)
	{
		String[] columnData = (String[]) cols.elementAt(column);
		String columnName = columnData[0]; // the name

		return (columnName);
	}

	/**
	 * Gives you the external column name from the labelsTable object for a
	 * given column index.
	 * 
	 * @param tableName
	 * @param column
	 * @return
	 */
	public String getColumnName(String tableName, int column)
	{
		String[] columnData = (String[]) cols.elementAt(column);
		String columnName = columnData[0]; // the name

		String colName = "";
		for (int i = 0; i < labelsTable.size(); i++)
		{
			String[] labelsTableRow = (String[]) labelsTable.elementAt(i);
			if (labelsTableRow[0].equals(columnName))
			{
				colName = labelsTableRow[1];
				break;
			}
		}

		return (colName);
	}

	/**
	 * Returns if a column input is mandatory or not.
	 * 
	 * @param tableName
	 * @param column
	 * @return "*" for true otherwise "" is returned
	 */
	public String getColumnMandatory(String tableName, int column)
	{
		String[] columnData = (String[]) cols.elementAt(column);
		String columnName = columnData[0]; // the name

		String mandatory = "";
		for (int i = 0; i < labelsTable.size(); i++)
		{
			String[] labelsTableRow = (String[]) labelsTable.elementAt(i);
			if (labelsTableRow[0].equals(columnName))
			{
				mandatory = labelsTableRow[2];
				break;
			}
		}

		if (mandatory.equals("1"))
			return "*";
		else
			return "";
	}

//	public Vector getRowFromTable(String tableName, String element_id, String tableType)
//	{
//		String query = "select * from "+tableType+"_"+tableName+" where element_id = "+element_id;
//		Vector v = new Vector(20);
//		v = select(query, true, true);
//
//		String[] columns = new String[cols.size()];
//
//		for (int i = 0; i < cols.size(); i++)
//		{
//			String[] tmpArray = (String[])cols.elementAt(i);
//			columns[i] = tmpArray[0];
//		}
//
//		v = select(query, columns, false, false);
//
//		 now get Labels table
//		query = "select Label_InternalName, Label_ExternalName, Label_Mandatory, Label_OnOff, Label_Info from Labels_"+tableName;
//		String[] labelCols= { "Label_InternalName", "Label_ExternalName", "Label_Mandatory", "Label_OnOff", "Label_Info" };
//		labelsTable = select(query, labelCols, false, false);
//
//		return v;
//	}

//	private void getMetaData(ResultSet rs)
//	{
//		try
//		{
//			 get table structure and store in object
//			ResultSetMetaData rsmd = rs.getMetaData();
//			int numCols = rsmd.getColumnCount();
//
//			for (int i = 1; i <= numCols; i++)
//			{
//				 dbStruct holds columnName, ColumnType, ColumnSize
//				String[] dbStruct = new String[3];
//
//				/* System.out.println("[" + i + "]" +
//					rsmd.getColumnName(i) + " {" +
//					rsmd.getColumnTypeName(i) + "-" + rsmd.getColumnDisplaySize(i) + "}");*/
//
//				dbStruct[0] = rsmd.getColumnName(i);
//				dbStruct[1] = rsmd.getColumnTypeName(i);
//				dbStruct[2] = String.valueOf(rsmd.getColumnDisplaySize(i));
//
//				cols.addElement(dbStruct);
//			}
//		}
//		catch (Exception e)
//		{
//			System.out.println(e);
//		}
//	}

//	public int updateData(String query)
//	{
//		int rowsAffected=0;
//
//		try
//		{
//			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
//			String url=d.getOdbcConString();
//			Connection con=DriverManager.getConnection(url, d.DBuser(), d.DBpass());
//			Statement stmt = con.createStatement();
//			rowsAffected = stmt.executeUpdate(query);
//			stmt.close();
//			con.close();
//		}
//		catch (Exception e) {
//		System.out.println(e);
//		}
//
//		return rowsAffected;
//	}

//	public Vector RMIselect(String query) throws RemoteException
//	{
//		return ( select(query, false, false) );
//	}

//	public Vector select(String query)
//	{
//		return ( select(query, false, false) );
//	}

	// simple single string column return
//	public Vector select(String query, boolean getMetaData, boolean useCache)
//	{
//		System.out.println("QUERY = "+query);
//		Vector v = new Vector(10);
//		try
//		{
//			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
//			String url=d.getOdbcConString();
//			Connection con=DriverManager.getConnection(url, d.DBuser(), d.DBpass());
//			String url="jdbc:odbc:webdirector";
//			Connection con=DriverManager.getConnection(url, "Andrew Davidson", "rollerblade");
//			Statement stmt = con.createStatement();
//			ResultSet rs = stmt.executeQuery(query);
//
//			 get meta data about table
//			if ( getMetaData )
//				getMetaData(rs);
//
//			while (rs.next())
//				v.addElement(rs.getString(1).trim());
//
//			stmt.close();
//			con.close();
//		}
//		catch (Exception e) {
//		System.out.println(e);
//		}
//		System.out.println("ROWS "+v.size());
//
//		return v;
//	}

//	public Vector select(String query, String[] cols)
//	{
//		return ( select(query, cols, false, false) );
//	}

	// slightly more complex multiple columns but all strings return
//	public Vector select(String query, String[] cols, boolean getMetaData, boolean useCache)
//	{
//		Vector v = new Vector(10);
//		try
//		{
//			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
	//String url=d.getOdbcConString();
	//Connection con=DriverManager.getConnection(url, d.DBuser(), d.DBpass());
//			String url="jdbc:odbc:webdirector";
//			Connection con=DriverManager.getConnection(url, "Andrew Davidson", "rollerblade");
//			Statement stmt = con.createStatement();
//			ResultSet rs = stmt.executeQuery(query);
//
//			String[] data = new String[cols.length];
//
	// get meta data about table
//			if ( getMetaData )
//				getMetaData(rs);
//
//			while (rs.next())
//			{
//				for (int i=0; i<cols.length; i++)
//				{
//					Object x = rs.getString(i+1);
//					if ( (String)x == null )
//						data[i] = "";
//					else
//					{
//						String s = (String)x;
//						data[i] = s.trim();
//					}
//				}
//
//				v.addElement((String[])data.clone());
//			}
//
//			stmt.close();
//			con.close();
//		}
//		catch (Exception e) {
//		System.out.println(e);
//		}
//
//		return (v);
//	}

	/**
	 * MediaStore, please ignore.
	 * 
	 * @return
	 */
	public String getBrand()
	{
		return (d.getName());
	}


//	public String getAllCategorySelect(String cat_name)
//	{
//		StringBuffer out = new StringBuffer("<SELECT name=\"category\">");
//
//		String query = "select category_id, category_name, brand_name from category c, brand b where b.brand_id = c.brand_id order by brand_name";
//		String[] cols = { "category_id", "category_name", "brand_name" };
//
//		Vector v = select(query, cols, false, false);
//
//		String[] res = null;
//
//		out.append("<OPTION value=\"pleaseSelect\">Please select ...</OPTION>");
//
//		for (int i=0; i<v.size(); i++)
//		{
//			out.append("<OPTION");
//
//			res = (String[])v.elementAt(i);
//			if ( cat_name.equals(res[1]) )
//				out.append(" SELECTED");
//
//			out.append(" value=\""+res[0]+"\">"+res[2]+" - "+res[1]+"</OPTION>");
//		}
//
//		out.append("</SELECT>");
//
//		return ( out.toString() );
//	}

//	public String getCategorySelect(String cat_name)
//	{
//		StringBuffer out = new StringBuffer("<SELECT name=\"category\">");
//
//		String query = "select category_name from category where brand_id = "+d.getID();
//		Vector v = select(query, false, false);
//
//		out.append("<OPTION value=\"pleaseSelect\">Please select ...</OPTION>");
//
//		for (int i=0; i<v.size(); i++)
//		{
//			out.append("<OPTION");
//
//			if ( cat_name.equals(v.elementAt(i)) )
//				out.append(" SELECTED");
//
//			out.append(">"+v.elementAt(i)+"</OPTION>");
//		}
//
//		out.append("</SELECT>");
//
//		return ( out.toString() );
//	}

//	public String getAllBrandSelect(String brand)
//	{
//		StringBuffer out = new StringBuffer("<SELECT name=\"brand\">");
//
//		String query = "select brand_name from brand";
//		String[] cols = { "brand_name" };
//
//		Vector v = select(query, false, false);
//
//		out.append("<OPTION value=\"pleaseSelect\">Please select ...</OPTION>");
//
//		for (int i=0; i<v.size(); i++)
//		{
//			out.append("<OPTION");
//
//			if ( brand.equals((String)v.elementAt(i)) )
//				out.append(" SELECTED");
//
//			out.append(">"+(String)v.elementAt(i)+"</OPTION>");
//		}
//
//		out.append("</SELECT>");
//
//		return ( out.toString() );
//	}

//	public String getAllCategorySelect()
//	{
//		StringBuffer out = new StringBuffer("<SELECT name=\"category\">");
//
//		String query = "select category_id, category_name, brand_name from category c, brand b where b.brand_id = c.brand_id order by brand_name";
//		String[] cols = { "category_id", "category_name", "brand_name" };
//
//		Vector v = select(query, cols, false, false);
//
//		String[] res = null;
//
//		out.append("<OPTION value=\"pleaseSelect\">Please select ...</OPTION>");
//
//		for (int i=0; i<v.size(); i++)
//		{
//			res = (String[])v.elementAt(i);
//
//			out.append("<OPTION value=\""+res[0]+"\"");
//			if ( res[1].equals(d.getDirCat()) && res[2].equals(d.getDirHier()) )
//				out.append(" SELECTED");
//
//			out.append(">"+res[2]+" - "+res[1]+"</OPTION>");
//		}
//
//		out.append("</SELECT>");
//
//		return ( out.toString() );
//	}

//	public String getCategorySelect()
//	{
//		StringBuffer out = new StringBuffer("<SELECT name=\"category\">");
//
//		String query = "select category_name from category where brand_id = "+d.getID();
//		Vector v = select(query, false, false);
//
//		out.append("<OPTION value=\"pleaseSelect\">Please select ...</OPTION>");
//
//		for (int i=0; i<v.size(); i++)
//			out.append("<OPTION>"+v.elementAt(i)+"</OPTION>");
//
//		out.append("</SELECT>");
//
//		return ( out.toString() );
//	}


	/**
	 * Doubles up the ' (apos) characters so the database can cope
	 * 
	 * //@param String the text to check for an apos.
	 * 
	 * @return String, the update string with doubled up apos.
	 */
	public final String sq(String str)
	{
		String SQ = "'";
		if (d.getDEBUG())
			System.out.println(str);

		//if( str.equals("") )
		//return SQ + SQ;

		String token = null;
		StringBuffer returnString = new StringBuffer();
		// returnString.append(SQ);
		if (str != null)
		{
			StringTokenizer st = new StringTokenizer(str, SQ, true);
			while (st.hasMoreTokens())
			{
				token = st.nextToken();
				if (token.equals(SQ))
				{
					returnString.append(token);
					returnString.append(SQ);
				}
				else
					returnString.append(token);
			}
		}
		// returnString.append(SQ);

		return returnString.toString();
	}

	/**
	 * MediaStore do not use
	 * 
	 * @param x
	 */
	public void setBrand_id(String x)
	{
		brand_id = x;
	}

	/**
	 * MediaStore do not use
	 * 
	 * @return
	 */
	public String getBrand_id()
	{
		if (brand_id == null)
			return ("");
		else
			return (brand_id);
	}

//	public String getBrands(String multiple)
//	{
//		StringBuffer sb = new StringBuffer(100);
//
//		if ( multiple.equals("single") )
//			sb.append("<SELECT name=brand_id>");
//		else
//			sb.append("<SELECT name='brand_id' size='4' multiple>");
//
//		String query = "select brand_id, brand_name from brand";
//		String[] cols = {"brand_id", "brand_name"};
//
//		Vector v = select(query, cols, false, false);
//
//		if ( ! multiple.equals("multipleUser") )
//			sb.append("<OPTION value=\"pleaseSelect\">Please select ...</OPTION>");
//
//		for (int i=0; i<v.size(); i++)
//		{
//			cols = (String[])v.elementAt(i);
//
//			sb.append("<OPTION value='"+cols[0]+"'");
//
//			if ( cols[1].equals( d.getDirHier() ) )
//				sb.append(" SELECTED");
//
//			sb.append(">"+cols[1]+"</OPTION>");
//		}
//
//		sb.append("</SELECT>");
//
//		return (sb.toString() );
//	}




}
