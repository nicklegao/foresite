package webdirector.db;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew Davidson
 * Date: 21-Mar-2004
 * Time: 00:54:07
 * To change this template use File | Settings | File Templates.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import webdirector.db.client.ClientDataAccess;
import au.net.webdirector.admin.imports.CSV;
import au.net.webdirector.admin.imports.FileToImport;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.utils.module.StoreUtils;

public class DBimport extends DBaccess
{
	protected static Defaults d = Defaults.getInstance();
	int lineNo;
	String Line;
	boolean processATTRLONGdirectly = false;

	/**
	 * Essentially a wrapper method around the createCategoriesAndRowData()
	 * method.
	 * 
	 * @param moduleToImport
	 * @param importFile
	 * @return
	 */
	public String insertData(String moduleToImport, String importFile)
	{
		if (d.getDEBUG())
			System.out.println("DBimport:insertData(): module " + moduleToImport + " file " + importFile);
		// return string to include number of rows imported
		String retStr = "CSV data import: ";

		int levels = Integer.parseInt(Defaults.getModuleLevels(moduleToImport));

		// loop through rows first checking each category level exists (creating
		// it where it doesn't) and then inserting the actual row
		return (createCategoriesAndRowData(levels, moduleToImport, importFile));
	}

	/**
	 * This method reads from the import file and if the file exists calls the
	 * process(...) method
	 * 
	 * @param levels
	 *            number of levels in the module.
	 * @param moduleToImport
	 * @param importFile
	 * @return
	 */
	protected String createCategoriesAndRowData(int levels, String moduleToImport, String importFile)
	{
		int rowsInserted = 0;
		File f = new File(d.getStoreDir() + "/_tmp/" + importFile);
		CSV csv = new CSV();

		if (d.getDEBUG())
			System.out.println("DBimport:createCategoriesAndRowData(levels " + levels + ", module " + moduleToImport + ", importFile " + d.getStoreDir() + "/_tmp/" + importFile);

		if (f.exists())
		{
			try
			{
				FileReader fr = new FileReader(f);
				BufferedReader is = new BufferedReader(fr);
				rowsInserted = process(csv, is, levels, moduleToImport);
			}
			catch (Exception e)
			{
				ClientDataAccess cda = new ClientDataAccess();
				System.out.println("Exception thrown " + e.getMessage());
				String row[] = cda.getElementCols(moduleToImport);
				String data = "";
				if (Line != null)
				{
					String lineValue[] = Line.split(",");
					int linelength = lineValue.length, rowlength = row.length;
					int i = 0;
					data = data.concat("<table>");
					data = data.concat("<tr><td> <b>Table Attribute in DB</B> </td><td><b>Value in Imported File</b></td></tr>");
					while ((i < linelength) || i < rowlength)
					{
						data = data.concat("<tr>");
						if (i < rowlength && row[i] != null)
						{
							data = data.concat("<td>" + row[i] + "</td>");
						}
						else
						{
							data = data.concat("<td> </td>");
						}
						if (i < linelength && lineValue[i] != null)
						{
							data = data.concat("<td>" + lineValue[i] + "</td>");
						}
						else
						{
							data = data.concat("<td> </td>");
						}
						data = data.concat("</tr>");
						i++;
					}
					data = data.concat("</table>");
				}
				return ("An error has occurred while importing file<br><br>" + "on line no: " + lineNo + " which is as below: <br><br> <b>Error Message:</b> <br>" + data + e.getMessage());
			}

			// remove file from _tmp dir
			boolean b = f.delete();
			if (d.getDEBUG())
				System.out.println("Removing temp CSV file from:" + f.getPath() + " removed ok ? " + b);
		}
		else
		{
			System.out.println("File DOES NOT exist ??? " + d.getStoreDir() + "/_tmp/" + importFile);
		}

		return String.valueOf(rowsInserted) + " rows inserted Ok.";
	}

	/**
	 * Reads through the BufferedReader stream and for each line of the CSV file
	 * parses the contents to split the CSV line into its individual fields and
	 * then calls the insertCategoryAndRowData(...) method to insert the CSV
	 * file contents row by row. If a problem occurs at any insert of any line
	 * an exception is thrown.
	 * 
	 * @param csv
	 * @param is
	 * @param levels
	 * @param moduleToImport
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */

	protected int process(CSV csv, BufferedReader is, int levels, String moduleToImport) throws IOException, Exception
	{
		ClientDataAccess cda = new ClientDataAccess();
		Defaults def = Defaults.getInstance();
		String line;
		int rowsInserted = 0;
		String[] cols = cda.getElementCols(moduleToImport);
		lineNo = 0;
		while ((line = is.readLine()) != null)
		{
			// if ( def.getDEBUG() ) System.out.println("line = `" + line +
			// "'");
			Line = line;
			lineNo++;
			Iterator e = csv.parse(line);

			// we now have a line from the CSV file, convert to SQL and insert
			try
			{
				insertCategoryAndRowData(e, levels, moduleToImport, cols);

			}
			catch (Exception io)
			{
				io.printStackTrace();
				throw io;
			}
			rowsInserted++;
		}

		return rowsInserted;
	}

	/**
	 * This is the main Method for inserting all data into the DB. Taking all
	 * the content to insert from the Iterator it works out using the 'levels'
	 * which are category values and then calls processCategories() to create
	 * the non-existing categories. Once done it then processes the rest of the
	 * column values which have to tie up with the internal column names stored
	 * in cols[]. This is used to create insert statements to insert the
	 * appropriate attribute values into the right columns into the specified
	 * module (moduleToImport)
	 * 
	 * @param e
	 * @param levels
	 * @param moduleToImport
	 * @param cols
	 * @throws Exception
	 *             if any inserts fail
	 */
	protected void insertCategoryAndRowData(Iterator e, int levels, String moduleToImport, String[] cols) throws Exception
	{
		ClientDataAccess cda = new ClientDataAccess();
		int field = levels - 1;
		String baseQuery = "insert into Elements_" + moduleToImport + " (";
		StringBuffer keys = new StringBuffer();
		StringBuffer vals = new StringBuffer();
		int colNum = 1;
		String[] categories = new String[levels];
		int id = 0;
		String hold;
		// sort categories out.
		categories = processCategories(moduleToImport, levels, e);

		Vector imageImports = new Vector();
		Vector textImports = new Vector();
		Hashtable<String, String> multiDropImports = new Hashtable<String, String>();
		while (e.hasNext())
		{
			try
			{
				String data = (String) e.next();

				if (d.getDEBUG())
					System.out.println("field[" + field + "] = `" + data + "'");

				// dealing with row data now
				String key = cols[colNum++];
				System.out.println(key);

				// special conditions
				if (key.equals("Element_id"))
					continue;
				// hold = java.net.URLDecoder.decode(data,"UTF-8");
				// System.out.println("hold"+hold);
				// System.out.println("data"+data);
				// check data for special chrs and handle.
				// data = StringEscapeUtils.escapeXml(data);
				// data = encode(hold, "&", "&amp;");

				data = sq(data);

				// otherwise copy attribute into sql statement
				keys.append(key + ",");
				if (key.startsWith("ATTRCURRENCY") || key.startsWith("ATTRINTEGER") || key.startsWith("ATTRFLOAT"))
				{
					if (data.equals(""))
					{
						data = "0";
					}
					vals.append(data + ",");
				}
				else if (key.startsWith("ATTRLONG") && !data.equals("") && !data.equals(" "))
				{
					String storePath = "/" + moduleToImport + "/8888/" + key + "/" + data.substring(data.lastIndexOf("/") + 1);
					if (processATTRLONGdirectly)
					{
						storePath = "/" + moduleToImport + "/8888/" + key + "/null";
					}
					vals.append("'" + storePath + "',");
					textImports.add(new FileToImport(key, data, storePath, moduleToImport));
				}
				else if (key.startsWith("ATTRFILE") && !data.equals("") && !data.equals(" "))
				{
					String storePath = "/" + moduleToImport + "/8888/" + key + "/" + data.substring(data.lastIndexOf("/") + 1).substring(data.lastIndexOf("\\") + 1);
					vals.append("'" + storePath + "',");
					imageImports.add(new FileToImport(key, data, storePath, moduleToImport));
				}
				else if (key.startsWith("ATTRMULIT") && !data.equals("") && !data.equals(" "))
				{
					vals.append("'',");
				}
				else
					vals.append("'" + data + "',");

				field++;
			}
			catch (Exception q)
			{
				throw q;
			}
		}

		// Now Categories should all exist and we just need to do the insert
		String k = keys.substring(0, keys.length() - 1);
		String s = vals.substring(0, vals.length() - 1);

		baseQuery = baseQuery + k + ", Language_id,Status_id,Lock_id,Version,Live,Category_id) values (" + s + ",1,1,1,1,1," + categories[levels - 1] + ")";

		// Complex insert
		id = insertData(baseQuery, "", "Elements_" + moduleToImport);
		Exception x = new Exception("Could not insert one or more rows into the database: " + baseQuery);
		if (id == 0)
			throw x;

		// now load and handle the files
		if (!loadAllFileImports(imageImports, id))
		{
			x = new Exception("Could not upload one of more files for row = " + id);
			throw x;
		}

		if (processATTRLONGdirectly)
		{
			if (!createNewStores(textImports, id))
			{
				x = new Exception("Could not upload one of more files for row = " + id);
				throw x;
			}
		}
		else
		{
			if (!loadAllFileImports(textImports, id))
			{
				x = new Exception("Could not upload one of more files for row = " + id);
				throw x;
			}
		}
	}

	private boolean createNewStores(Vector<FileToImport> fileImports, int elementId)
	{
		DButils dbA = new DButils();
		boolean ret = true;
		for (int i = 0; i < fileImports.size(); i++)
		{
			FileToImport fileToImport = (FileToImport) fileImports.elementAt(i);

			if (!dbA.updateLongTextFile(fileToImport.getColumnName(), fileToImport.getLocalPath(), fileToImport.getModule(), elementId, "Elements", false))
			{
				ret = false;
			}
		}
		return ret;

	}

	private boolean loadAllFileImports(Vector fileImports, int elementId)
	{
		StoreUtils storeUtils = new StoreUtils();
		String colName = "";
		String destination = "";
		String updateDestination = "";
		// loop through all attrs in this row which are of type ATTRFILE
		for (int i = 0; i < fileImports.size(); i++)
		{
			FileToImport fileToImport = (FileToImport) fileImports.elementAt(i);
			// expectation is that these files will already live on the server
			// in the tmp location
			// e.g. c:\temp\...path in csv...\file.jpg
			String srcFile = d.getOStypeTempDir() + d.getOSTypeDirSep() + fileToImport.getLocalPath();

			// update the storePath to contain the real elementId
			fileToImport.setStorePath(fileToImport.getStorePath().replace("/8888/", "/" + elementId + "/"));
			colName = fileToImport.getColumnName();
			boolean generateThumb = true;
			destination = d.getStoreDir() + fileToImport.getStorePath().substring(0, fileToImport.getStorePath().lastIndexOf("/"));

			if (colName.startsWith("ATTRLONG"))
			{
				generateThumb = false;
				updateDestination = fileToImport.getStorePath().substring(0, fileToImport.getStorePath().lastIndexOf("/")) + "/" + "ATTRLONG_PageText.txt";
			}
			else
			{
				generateThumb = true;
				updateDestination = fileToImport.getStorePath();
			}
			// move the files to the appropriate place in the stores

			if (!storeUtils.copyToStores(srcFile, destination, generateThumb))
			{
				System.out.println("***************************************************");
				System.out.println("********* FILE IMPORT FAILED FOR FILE .************");
				System.out.println("******** SRC " + srcFile);
				System.out.println("********                              *************");
				System.out.println("***************************************************");
			}

			// update the DB path to reflect the real elementId
			String query = "update elements_" + fileToImport.getModule() + " set " + fileToImport.getColumnName() + " = " + "? where element_id = ?";
			if (updateData(query, new Object[] { updateDestination, elementId }) != 1)
				return false;
		}

		return true; // if we get here all must be well.....
	}

	/**
	 * Main method() for dealing with categories. If categories exist then the
	 * ID is retrieved from the DB otherwise they are created and then the ID is
	 * also retrieved.
	 * 
	 * @param moduleToImport
	 * @param levels
	 * @param e
	 * @return String array of all parent ID's (some will be newly created and
	 *         some will already exist)
	 */
	protected String[] processCategories(String moduleToImport, int levels, Iterator e)
	{
		ClientDataAccess cda = new ClientDataAccess();
		String[] categories = new String[levels];
		String parentID = null;

		for (int i = 0; i < levels; i++)
		{
			String data = (String) e.next();
			// check data for special chrs and handle.
			data = encode(data, "&", "&amp;");
			data = sq(data);

			if (!cda.doesCategoryExist(moduleToImport, String.valueOf(i + 1), data, parentID))
			{
				parentID = insertCategory(moduleToImport, String.valueOf(i + 1), data, parentID);
				if (d.getDEBUG())
					System.out.println("insertCategoryAndRowData(): category " + data + " created, id " + parentID);
			}
			else
			{
				parentID = cda.getCategoryIdUsingParent(moduleToImport, String.valueOf(i + 1), data, parentID);
				if (d.getDEBUG())
					System.out.println("insertCategoryAndRowData(): category " + data + " exists, id " + parentID);
			}

			categories[i] = parentID;
		}

		return categories;
	}

	/**
	 * Physically carries out a category insert. All arguments are mandatory
	 * except for parentID which may be given as null in which case it is reset
	 * to 0.
	 * 
	 * @param moduleToImport
	 * @param categorylevel
	 * @param categoryName
	 * @param parentID
	 * @return ID of the newly created category.
	 */
	protected String insertCategory(String moduleToImport, String categorylevel, String categoryName, String parentID)
	{
		if (parentID == null)
			parentID = "0";

		if (d.getDEBUG())
			System.out.println("insertCategory(): Creating category " + categoryName + " level " + categorylevel);

		// now do the insert
		String query = "insert into Categories_" + moduleToImport + " (ATTR_categoryName,folderLevel,Category_ParentID) " + "values (?, ?, ?)";

		// String id = String.valueOf(
		// insertData(query,"Category_id","Categories_"+moduleToImport) );
		String id = String.valueOf(insertQuery(query, new Object[] { categoryName, categorylevel, parentID }));

		return id;
	}

	// public static void main(String[] args) throws IOException {
	//
	// Construct a new CSV parser.
	// CSV csv = new CSV();
	//
	// if (args.length == 0) { // read standard input
	// BufferedReader is;
	// is = new BufferedReader(
	// new InputStreamReader(System.in));
	// process(csv, is);
	// } else {
	// for (int i=0; i<args.length; i++) {
	// process(csv, new BufferedReader(new FileReader(args[i])));
	// }
	// }
	// }
}