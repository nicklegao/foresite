package webdirector.db;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 14/01/2007
 * Time: 13:05:59
 * $Id: DBcolumn.java,v 1.1 2011-10-27 03:32:35 dan Exp $
 * To change this template use File | Settings | File Templates.
 */
public class DBcolumn
{

	// simple object to define a DB column
	private String columnName;
	private String columnType;
	private boolean columnIsNull;
	private boolean columnIsIdentity;
	private int columnIdentitySeed;
	private int columnIdentityInc;


	public DBcolumn(String cName, String cType, boolean cIsNull, boolean cIsIdentity, int cIdentitySeed, int cIdentityInc)
	{
		columnName = cName;
		columnType = cType;
		columnIsNull = cIsNull;
		columnIsIdentity = cIsIdentity;
		columnIdentitySeed = cIdentitySeed;
		columnIdentityInc = cIdentityInc;
	}

	public String getColumnName()
	{
		return columnName;
	}

	public String getColumnType()
	{
		return columnType;
	}

	public boolean isColumnIsNull()
	{
		return columnIsNull;
	}

	public boolean isColumnIsIdentity()
	{
		return columnIsIdentity;
	}

	public int getColumnIdentitySeed()
	{
		return columnIdentitySeed;
	}

	public int getColumnIdentityInc()
	{
		return columnIdentityInc;
	}

}
