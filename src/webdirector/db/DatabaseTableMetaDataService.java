package webdirector.db;

import java.util.Vector;
import java.util.Hashtable;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import static java.lang.System.*;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 9/06/2009
 * Time: 16:00:04
 * To change this template use File | Settings | File Templates.
 */
public class DatabaseTableMetaDataService
{

	private static boolean DEBUG = false;
	private static Hashtable tableCache = new Hashtable();
	private static Hashtable labelsTableCache = new Hashtable();

	/**
	 * Labels table service, cache hit is based on the query
	 * 
	 * @param query
	 * @return
	 */
	public static String[] labelsTableService(String query)
	{

		if (labelsTableCache.containsKey(query))
		{
			// cache hit
			if (DEBUG)
				out.println("CACHE HIT " + query);

			return (String[]) labelsTableCache.get(query);
		}

		return null;
	}

	public static void labelsTableAddToCache(String query, String[] colList)
	{
		// check if it exists and remove it
		labelsTableCache.remove(query);

		if (DEBUG)
			out.println("CACHE ADD " + query);

		labelsTableCache.put(query, colList);
	}

	/**
	 * Returns either from the cache or puts into the cache and then returns the
	 * DB meta data 0 = column name, 1 = column type, 2 = column size.
	 * (NOT THE LABELS TABLE META DATA)
	 * Works for both categories and elements
	 * The cache is reset when editAttributes or editAttributes_category is
	 * called
	 *
	 * @param tableName
	 *            - full table name including post and prefix of the table you
	 *            are interested in
	 * @return Returns a Vector of string arrays. Each array has 3 values, 0 =
	 *         column name, 1 = column type, 2 = column size.
	 */
	public static Vector columnService(String tableName)
	{
		if (DEBUG)
			out.println("CACHE ACCESS " + tableName);

		if (tableCache.containsKey(tableName))
		{
			// cache hit
			if (DEBUG)
				out.println("CACHE HIT " + tableName);

			Vector cols = (Vector) tableCache.get(tableName);
			return cols;
		}
		else
		{
			// cache miss
			if (DEBUG)
				out.println("CACHE MISS " + tableName);

			DBaccess db = new DBaccess();
			Vector cols = db.getColsMetaData(tableName);

			tableCache.put(tableName, cols);

			return cols;
		}
	}

	public static boolean isTableInCache(String tableName)
	{
		if (tableCache.containsKey(tableName))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 *
	 * @param tableName
	 *            - full table name including post and prefix of the table you
	 *            are interested in
	 * @param column
	 *            - index number (in raw DB order) of the column you require
	 *            meta data for
	 * @return Returns a string array. Has 3 values, 0 = column name, 1 = column
	 *         type, 2 = column size.
	 */
	public static String[] columnService(String tableName, int column)
	{
		Vector cols = columnService(tableName);

		return (String[]) cols.elementAt(column);

	}

	public static void clearCache(String tableName)
	{
		if (DEBUG)
			out.println("CACHE CLEAR " + tableName);

		tableCache.remove(tableName);

		// lazy cache clear - only need to remove the single query but that would mean having to run through hash and find it...
		labelsTableCache = new Hashtable();
	}

	public static void clearCache()
	{
		if (DEBUG)
			out.println("CACHE CLEAR ALL");

		tableCache = new Hashtable();
		labelsTableCache = new Hashtable();
	}
}
