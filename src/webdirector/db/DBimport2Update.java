package webdirector.db;

/**
 * Created by IntelliJ IDEA.
 * User: Stuart McMillan
 * Date: 05-May-2004
 * Time: 16:57:00
 * To change this template use File | Settings | File Templates.
 */

import webdirector.db.DBimport;
import webdirector.db.client.ClientDataAccess;

import java.util.Vector;
import java.util.Iterator;
import java.util.Hashtable;
import java.util.Enumeration;
import java.io.*;

import au.net.webdirector.admin.imports.CSV;
import au.net.webdirector.common.Defaults;

public class DBimport2Update extends DBimport
{
	public int expectedKeyCount = 1;
	public int expectedDataCount = 2;
	public String defaultColNames[] = { "ATTR_ModelNumber", "ATTR_PriceRRP", "ATTR_GeneralPurpose1" };
	private Vector warningResults = new Vector();

	/**
	 * Entry to import class.
	 * 
	 * @param moduleToImport
	 * @param importFile
	 * @return
	 */
	public String insertData(String moduleToImport, String importFile)
	{
		if (d.getDEBUG())
			System.out.println("DBimport2Update:insertData(): for module " + moduleToImport +
					" file " + importFile);
		// return string to include number of rows imported
		String retStr = "CSV data import: ";

		int levels = Integer.parseInt(Defaults.getModuleLevels(moduleToImport));

		// loop through rows first checking each category level exists (creating it where it doesn't) and then inserting the actual row
		return (updateRowData(levels, moduleToImport, importFile));
	}

	/**
	 * Reads in a line from the file and then calls the process() method to
	 * actually do the work. Once done it works out how many rows have been
	 * updated
	 * and outputs any warnings to the user which were generated during the
	 * inserts.
	 * 
	 * @param levels
	 *            The number of levels this module has
	 * @param moduleToImport
	 *            name of module to import into.
	 * @param importFile
	 *            This is the file expected to live in the stores under the _tmp
	 *            folder.
	 * @return Markup for the results of the import to be displayed to the
	 *         client.
	 */
	protected String updateRowData(int levels, String moduleToImport, String importFile)
	{
		int rowsUpdated = 0;
		warningResults = new Vector();

		File f = preImportCheck(d.getStoreDir() + "/_tmp/" + importFile);

		CSV csv = new CSV();

		if (d.getDEBUG())
			System.out.println("DBimport2Update:updateRowData(levels " + levels + ", module " + moduleToImport + ", importFile " + d.getStoreDir() + "/_tmp/" + importFile);

		if (f.exists())
		{
			try
			{
				FileReader fr = new FileReader(f);
				BufferedReader is = new BufferedReader(fr);
				rowsUpdated = process(csv, is, levels, moduleToImport);
			}
			catch (Exception e)
			{
				System.out.println("Exception thrown " + e.getMessage());
				return ("An error has occurred. Import has stopped. Please mail the output below to Corporate " +
						"Interactive or correct the error in your import data:" +
						"<br><br>DBimport2Update::process()<br>" + e.getMessage());
			}

			// remove file from _tmp dir
			boolean b = f.delete();
			if (d.getDEBUG())
				System.out.println("Removing temp CSV file from:" + f.getPath() + " removed ok ? " + b);
		}
		else
		{
			System.out.println("File DOES NOT exist ??? " + d.getStoreDir() + "/_tmp/" + importFile);
		}

		StringBuffer sb = new StringBuffer();

		if (warningResults.size() != 0)
		{
			sb.append("<b>" + String.valueOf(warningResults.size()) + " warning(s) detected during import processing. (see below)</b><br>\n");

			for (int eachWarning = 0; eachWarning < warningResults.size(); eachWarning++)
			{
				sb.append((String) warningResults.elementAt(eachWarning) + "<br>\n");
			}
		}

		sb.append("<b>" + String.valueOf(rowsUpdated) + " rows updated OK.</b>");
		warningResults = null;

		return sb.toString();
	}

	private File preImportCheck(String importSourceFile)
	{
		File f = new File(importSourceFile);

		// need to check for dodgy chrs and strip them out.
		// need to check for the number of fields and error potentially.

		//return         File newImportFile = new File();

		return f;

	}

	/**
	 * main engine of this class. Reads in a line from the csv buffered reader,
	 * then parses it using csv.parse() then calls updateRowData to insert or
	 * update the
	 * row.
	 * 
	 * @param csv
	 * @param is
	 * @param levels
	 * @param moduleToImport
	 * @return the number of rows successfully inserted.
	 * @throws IOException
	 *             if csv file cannot be read
	 * @throws Exception
	 */
	protected int process(CSV csv, BufferedReader is, int levels, String moduleToImport) throws IOException, Exception
	{
		ClientDataAccess cda = new ClientDataAccess();
		Defaults def = Defaults.getInstance();
		String line;
		int lineCount = 0;
		int rowsProcessed = 0;
		String[] cols = defaultColNames;//cda.getElementCols(moduleToImport);

		while ((line = is.readLine()) != null)
		{
			// parse the line into an interator.
			Iterator e = csv.parse(line);
			lineCount++;
			boolean wasSuccessful = false;

			// we now have a line from the CSV file, convert to SQL and insert
			try
			{
				wasSuccessful = updateRowData(e, levels, moduleToImport, cols, lineCount);
			}
			catch (Exception io)
			{
				io.printStackTrace();
				throw io;
			}

			if (wasSuccessful)
			{
				// keep track of the good work...
				rowsProcessed++;
			}
		}

		return rowsProcessed;
	}

	/**
	 * Works out if the row already exists and therefore needs updating or if
	 * we're looking at an insert.
	 * Builds lists of columns and their values using the colNames[] array and
	 * the Iterator e respectively.
	 * Once built it then inserts or updates the rows in the table
	 * 
	 * @param e
	 * @param levels
	 * @param moduleToImport
	 * @param colNames
	 * @param lineNumber
	 * @return true if all inserts/updates worked ok, otherwise false or an
	 *         exception will be thrown
	 * @throws Exception
	 */
	private boolean updateRowData(Iterator e, int levels, String moduleToImport, String[] colNames, int lineNumber)
			throws Exception
	{
		ClientDataAccess cda = new ClientDataAccess();
		DButils db = new DButils();

		StringBuffer baseSelect = new StringBuffer("select count(*) from Elements_" + moduleToImport + " where ");
		StringBuffer baseUpdate = new StringBuffer("update Elements_" + moduleToImport + " set ");
		StringBuffer whereClause = new StringBuffer("where ");
		int colNum = 0;

		try
		{
			int nKey = 0;

			// enum the keys
			for (nKey = 0; e.hasNext() && nKey < expectedKeyCount; nKey++)
			{
				// key data coming in
				String data = (String) e.next();
				System.out.println("Found key#" + nKey + data);

				// check data for special chrs and handle.
				data = encode(data, "&", "&amp;");
				data = sq(data);

				// dealing with row data now
				String key = colNames[colNum++];

				// what do we do with the data - build some queries...
				baseSelect.append(key + " = '" + data + "' ");
				whereClause.append(key + " = '" + data + "' ");
			}

			// throw a wobbly if theres not enough keys
			if (nKey != expectedKeyCount)
			{
				throw new Exception("Found " + nKey + " fields, expected " +
						(expectedDataCount + expectedKeyCount) + " at line# " + lineNumber);
			}

			int nData = 0;
			// enum the data items
			for (nData = 0; e.hasNext() && nData < expectedDataCount; nData++)
			{
				// key data coming in
				String data = (String) e.next();
				System.out.println("Found data#" + nData + data);

				// check data for special chrs and handle.
				data = encode(data, "&", "&amp;");
				data = sq(data);

				// dealing with row data now
				String key = colNames[colNum++];

				// what do we do with the data - build some queries..."'"+
				baseUpdate.append(key + " = '" + data + "' ");
				if (e.hasNext())
				{
					baseUpdate.append(" , ");
				}

			}

			// throw a wobbly if theres not enough data
			if (nData != expectedDataCount)
			{
				throw new Exception("Found " + (nData + expectedKeyCount) + " fields, expected " +
						(expectedDataCount + expectedKeyCount) + " at line# " + lineNumber);
			}

			// throw a wobbly if theres too many fields
			if (e.hasNext())
			{
				throw new Exception("Found more than " + (expectedDataCount + expectedKeyCount) +
						" data fields at line# " + lineNumber);
			}

		}
		catch (Exception q)
		{
			throw q;
		}

		// test the select
		System.out.println("select does " + baseSelect.toString());
		// TODO - Not implementing the Prepared statement as this may break the code logic.
		Vector result = db.selectQuerySingleCol(baseSelect.toString());
		if (!result.elementAt(0).toString().equals("1"))
		{
			String warning = "Found " + result.elementAt(0).toString() +
					"  records " + whereClause.toString() + " at line# " + lineNumber +
					" (code" + result.elementAt(0).toString() + ")";
			//Exception x = new Exception();
			//throw x;
			warningResults.addElement(warning);
			return false;
		}

		// do the update
		baseUpdate.append(whereClause);

		// count the rows updated
		// complex to be upgraded
		int id = db.updateData(baseUpdate.toString(), new Object[] {});
		System.out.println("updateData does " + baseUpdate.toString());
		if (id != 1)
		{
			Exception x = new Exception("Failed during update of database record " +
					whereClause.toString() + " at line# " + lineNumber + " (code" + id + ")");
			throw x;
		}

		return true;

	}

}
