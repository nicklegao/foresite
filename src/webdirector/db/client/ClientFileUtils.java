package webdirector.db.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;

import au.net.webdirector.common.Defaults;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew Davidson
 * Date: 31-May-2004
 * Time: 11:34:48 <BR>
 * <B>Utility methods for manipulating files in the stores.</B><BR>
 * There are two types of paths used in this method. The first one is a relative
 * path which is the path from below the stores directory:<BR>
 * .../PRODUCTS/31/ATTRFILE_File1/CI_logo.gif<BR>
 * The second type of path is a full file system path:<BR>
 * D:/WebDirector/stores/PRODUCTS/31/ATTRFILE_File1/CI_logo.gif<BR>
 *
 */
public class ClientFileUtils
{
	private Defaults d;

	public ClientFileUtils()
	{
		d = Defaults.getInstance();
	}

	/**
	 * Returns a formatted string which is the file size rounded to the nearest
	 * K.
	 * 
	 * @param storeName
	 *            this is a partial path to the file in the stores directory.
	 * @return
	 */
	public String getFileSizeInStore(String storeName)
	{
		// returns the file size of a specified file which lives in the stores
		// path is relative to the stores.
		float fileSize = 0;

		String storeDir = d.getStoreDir();
		File fname = new File(storeDir + storeName);
		float x = 0;

		try
		{
			fileSize = (float) fname.length();
			x = fileSize / 1024;
		}
		catch (Exception e)
		{
			System.out.println("Exception " + e.getMessage());
		}

		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(2);

		return nf.format(x) + "Kb";
	}

	/**
	 * Given a atores relative path this method returns an equivalent File
	 * object.
	 * 
	 * @param filename
	 * @return
	 */
	private File getFileObjectFromStoreName(String filename)
	{
		String storeDir = d.getStoreDir();
		File fname = new File(storeDir + filename);

		return fname;
	}

	/**
	 * Given the relative path to a file in the stores this method returns just
	 * the physical name of the file itself. <BR>
	 * i.e. pass in PRODUCTS/31/ATTRFILE_File1/CI_logo.gif ... this method will
	 * return CI_logo.gif
	 * 
	 * @param storeName
	 *            store path to phyiscal media file
	 * @return
	 */
	public String getFileNameFromStoreName(String storeName)
	{
		File fname = getFileObjectFromStoreName(storeName);

		return fname.getName();
	}

	/**
	 * Returns full absolute path to the given file in the stores.
	 * 
	 * @param storeName
	 *            store path to phyiscal media file
	 * @return
	 */
	public String getFilePathFromStoreName(String storeName)
	{
		File fname = getFileObjectFromStoreName(storeName);

		return fname.getAbsolutePath();
	}

	/**
	 * Given a full absolute path (not a relative store path) to a file in the
	 * stores, this method opens the file and reads the contents into
	 * a StringBuffer which is then returned when the end of the file is
	 * reached.
	 * 
	 * @param filename
	 * @return
	 */
	public StringBuffer readTextContents(String filename, boolean preserverCarriageReturns)
	{
		StringBuffer sb = new StringBuffer();
		try
		{
			BufferedReader in = new BufferedReader(new FileReader(filename));
			String s;
			while ((s = in.readLine()) != null)
			{
				sb.append(s);
				if (preserverCarriageReturns)
					sb.append("\n");
			}

			in.close();
		}
		catch (FileNotFoundException e)
		{
			// should be ok... do nothing
		}
		catch (IOException e)
		{
			System.out.println("exception readTextContents(): no file found (" + filename + ") " + e.getMessage());
		}
		return sb;
	}

	public StringBuffer readTextContents(String filename)
	{
		return readTextContents(filename, false);
	}

	/**
	 * Opens the given file (must be a full path not a store path) and writes
	 * the new contents into the file overwriting any existing content.
	 * 
	 * @param filename
	 *            to overwrite
	 * @param contents
	 *            new content of the file
	 * @throws IOException
	 *             is thrown if the file cannot be written to.
	 */
	public void overwriteTextContents(String filename, String contents) throws IOException
	{
		BufferedWriter out = new BufferedWriter(new FileWriter(filename, false));
		out.write(contents, 0, contents.length());
		out.close();
	}

	/**
	 * Removes a file (templtae) from the given full path
	 * 
	 * @param fullPathTemplate
	 */
	public void removeTemplate(String fullPathTemplate)
	{
		File f = new File(fullPathTemplate);
		if (f.exists())
		{
			boolean b = f.delete();
			System.out.println("ClientFileUtils:removeTemplate(" + fullPathTemplate + " Result : " + b);
		}
	}

	/**
	 * Creates a new file in the given dir
	 * 
	 * @param dir
	 * @param filename
	 * @return returns true if the file does not exist and gets created ok.
	 *         Returns false if the file cannot be
	 *         created or already exists.
	 */
	public boolean createNewFile(String dir, String filename)
	{
		// does file already exist ? If not create it
		File f = new File(filename);
		if (!f.exists())
		{
			try
			{
				File d = new File(dir);
				d.mkdirs();
				f.createNewFile();
			}
			catch (Exception ex)
			{
				System.out.println(ex.getMessage());
				return false;
			}
			return true;
		}
		return false;
	}
}
