package webdirector.db.client;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import au.net.webdirector.common.Defaults;

import java.sql.*;
import java.util.Vector;
import java.util.Hashtable;
import java.util.HashMap;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 8/04/2007
 * Time: 15:36:36
 * To change this template use File | Settings | File Templates.
 */
public class DBConnection
{
	private Defaults d;

	public DBConnection()
	{
		d = Defaults.getInstance();
	}

	public Connection getConnection()
	{
		if (d.getDEBUG())
			System.out.println("DBConnection:getConnection(): ");

		Connection conn = null;
		Statement stmt = null;  // Or PreparedStatement if needed

		try
		{
			Context ctx = new InitialContext();
			if (ctx == null)
				throw new Exception("Boom - No Context");

			String dbName = d.getOdbcConString();
			int index = dbName.lastIndexOf(":") + 1;
			DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/" + dbName.substring(index));

			if (ds != null)
			{
				synchronized (ds)
				{
					conn = ds.getConnection();
				}
				if (conn == null)
				{
					throw new Exception("Connection is null");
				}
			}
		}
		catch (Exception e)
		{
			System.out.println("DBConnection:getConnection(): EXCEPTION THROWN >>>");
			e.printStackTrace();
		}

		return conn;
	}

	/**
	 * New method to get connetion for different database hosted on different
	 * servers , this is specially for DataMigration Process.........
	 * 
	 * @param host
	 * @param userName
	 * @param password
	 * @return
	 */
	public Connection getConnection(String host, String userName, String password)
	{
		System.out.println("Host:::" + host);
		System.out.println("Username:::" + userName);
		System.out.println("Password:::" + password);
		if (d.getDEBUG())
			System.out.println("DBConnection:getConnection(): ");

		Connection conn = null;
		Statement stmt = null;  // Or PreparedStatement if needed
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://" + host + ":3306/" + userName + "?user=" + userName + "&password=" + password);
			//       conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/RMA?user=RMA&password=rollerblade2004");
		}
		catch (Exception e)
		{
			System.out.println("EXCEPTION THROWN >>> " + e.getMessage());
			e.printStackTrace();
		}
		return conn;
	}

	/**
	 * This is same as above but this is special case for Cambramata where
	 * username and database schemaname were different.
	 * 
	 * @param host
	 * @param userName
	 * @param password
	 * @param context
	 * @return
	 */
	public Connection getConnection(String host, String userName, String password, String context)
	{
		System.out.println("IN GetConnection method:::>>>>");
		System.out.println("Host:::" + host);
		System.out.println("Username:::" + userName);
		System.out.println("Password:::" + password);
		System.out.println("context:::" + context);
		if (d.getDEBUG())
			System.out.println("DBConnection:getConnection(): ");

		Connection conn = null;
		Statement stmt = null;  // Or PreparedStatement if needed
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://" + host + ":3306/" + context + "?user=" + userName + "&password=" + password);
			//       conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/RMA?user=RMA&password=rollerblade2004");
		}
		catch (Exception e)
		{
			System.out.println("EXCEPTION THROWN >>> " + e.getMessage());
			e.printStackTrace();
		}
		return conn;
	}

	/**
	 * Simple select query which can only return one data column
	 * 
	 * @param conn
	 * @param query
	 * @return Vector containing a String objects which matched the results
	 */
	public Vector select(Connection conn, String query)
	{
		Vector resultVector = new Vector();
		try
		{
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			ResultSetMetaData rs1 = rs.getMetaData();
			while (rs.next())
			{
				String val = rs.getString(1);
				if (val == null)
					val = "";
				System.out.println("colname:::>>>>>>" + val);
				resultVector.add(val);

			}
			for (int i = 1; i <= rs1.getColumnCount(); i++)
			{
				System.out.println("MetaData column name" + rs1.getColumnName(i));
				System.out.println("MetaData column Type" + rs1.getColumnType(i));
				System.out.println("MetaDate clumn Size" + String.valueOf(rs1.getColumnDisplaySize(i)));
			}

		}
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		return resultVector;
	}

	/**
	 * Returns a Vector of string arrays. Each array has 3 values, 0 = column
	 * name, 1 = column type, 2 = column size.
	 * 
	 * @param conn
	 * @param query
	 * @return Hashtable containing a String objects which matched the results
	 */
	public Hashtable getMetaData(Connection conn, String query)
	{
		Hashtable h = new Hashtable();
		try
		{
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			ResultSetMetaData rs1 = rs.getMetaData();

			for (int i = 1; i <= rs1.getColumnCount(); i++)
			{
				// dbStruct holds columnName, ColumnType, ColumnSize
				String[] dbStruct = new String[3];
				System.out.println("MetaData column name" + rs1.getColumnName(i));
//                    System.out.println("MetaData column Type"+rs1.getColumnType(i));
				System.out.println("MetaDate clumn Size" + String.valueOf(rs1.getColumnDisplaySize(i)));
				dbStruct[0] = rs1.getColumnName(i);
				dbStruct[1] = rs1.getColumnTypeName(i);
				dbStruct[2] = String.valueOf(rs1.getColumnDisplaySize(i));
				h.put(dbStruct[0], dbStruct);
			}

		}
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		return h;
	}

	/**
	 * select query that can return more than a single column
	 * 
	 * @param conn
	 * @param query
	 * @return Vector of Hashtables. Each hash represents a single row returned.
	 *         Column names are the hash index
	 */
	public Vector selectMultipleColumns(Connection conn, String query)
	{
		Vector resultVector = new Vector();
		try
		{
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);


			while (rs.next())
			{
				HashMap columns = new HashMap();
				ResultSetMetaData rsmd = rs.getMetaData();
				for (int i = 1; i <= rsmd.getColumnCount(); i++)
				{
					Object x = rs.getString(i);
					if ((String) x == null)
						columns.put(rsmd.getColumnName(i), null);
					//  columns.put(rsmd.getColumnName(i), "NULL");
					else
					{
						String s = (String) x;
						// System.out.println(rsmd.getColumnName(i)+":::::"+s);
//                            String temp = s.trim();
//                           if(s.trim().equals("")){
//                                temp = "NULL";
						//}
						columns.put(rsmd.getColumnName(i), s.trim());
						//System.out.println("Get columnName SelectMultipleColumns:::>>> ::::>>>>>>>>"+rsmd.getColumnName(i));
					}

				}
				resultVector.addElement(columns);
			}
		}
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		return resultVector;
	}

	public void closeConnection(Connection conn)
	{
		// Always make sure the connection is returned to the pool
		if (conn != null)
		{
			try
			{
				conn.close();
			}
			catch (SQLException e)
			{
				System.out.println("DBConnection:closeConnection(): EXCEPTION THROWN >>>");
				e.printStackTrace();
			}
			conn = null;
		}
	}


}
