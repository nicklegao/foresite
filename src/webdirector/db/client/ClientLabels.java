/**
 * Created by IntelliJ IDEA.
 * User: Andrew Davidson
 * Date: Oct 19, 2003
 * Time: 6:40:36 AM
 * To change this template use Options | File Templates.
 */
package webdirector.db.client;

import webdirector.db.DButils;

import java.util.Vector;
import java.util.Hashtable;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;

public class ClientLabels
{

	private DButils db = new DButils();
	private Defaults d;

	// constructor
	public ClientLabels()
	{
		d = Defaults.getInstance();
	}

	/**
	 * Returns all the attribute labels for a particular module as a hash
	 * hash key is the internal column name in the DB and the hash value is the
	 * external column name<BR>
	 * So essentially this method returns a hashtable which maps internal column
	 * names (hash key) to external column names (hash value)
	 *
	 * @param module
	 * @return
	 */

	public Hashtable getElementLabels(String module)
	{
		return getElementLabels(module, false);
	}

	public Hashtable getElementLabels(String module, boolean onOnly)
	{
		String[] cols = { "Label_InternalName", "Label_ExternalName" };
		String query = "select Label_InternalName, Label_ExternalName from Labels_" + DatabaseValidation.encodeParam(module);

		if (onOnly)
		{
			query += " where Label_OnOff = 1";
		}

		//Vector v = db.select(query, cols);
		Vector v = db.selectQuery(query);

		Hashtable h = new Hashtable(20);

		for (int i = 0; i < v.size(); i++)
		{
			String[] data = (String[]) v.elementAt(i);
			h.put(data[0], data[1]);
		}

		return h;
	}


	public Hashtable getCategoryLabels(String module)
	{
		return getCategoryLabels(module, false);
	}

	public Hashtable getCategoryLabels(String module, boolean onOnly)
	{
		String[] cols = { "Label_InternalName", "Label_ExternalName" };
		String query = "select Label_InternalName, Label_ExternalName from Categorylabels_" + DatabaseValidation.encodeParam(module);

		if (onOnly)
		{
			query += " where Label_OnOff = 1";
		}

		//Vector v = db.select(query, cols);
		Vector v = db.selectQuery(query); // TODO validated

		Hashtable h = new Hashtable(20);

		for (int i = 0; i < v.size(); i++)
		{
			String[] data = (String[]) v.elementAt(i);
			h.put(data[0], data[1]);
		}

		return h;
	}
}
