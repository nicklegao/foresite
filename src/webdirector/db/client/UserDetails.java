/**
 * Created by IntelliJ IDEA.
 * User: Andrew Davidson
 * Date: Jan 22, 2004
 * Time: 9:36:22 AM
 * To change this template use Options | File Templates.
 */
package webdirector.db.client;

import java.util.Vector;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;

public class UserDetails
{
	private DBaccess db = new DBaccess();
	private String user_name = null;
	private String user_id = null;
	private String password = null;
	private String Name = null;
	private String Company = null;
	private String Email = null;
	private String Phone = null;
	private String UserLevel_id = null;

	/**
	 * Basic constructor if the userID is known.
	 */
	public UserDetails(int userID)
	{
		boolean b = getUser(userID);
		System.out.println("User " + userID + " getUser " + b);
	}

	/**
	 * Second constructor to be used if the userName is only known and not the
	 * ID. UserNames should be unique :_( !
	 * 
	 * @param userName
	 */
	public UserDetails(String userName)
	{
		boolean b = getUser(userName);
		System.out.println("User " + userName + " getUser " + b);
	}

	/**
	 * Examines the Users table to find any matches for the given User Name.
	 * If a match is found the assignAttr() method is called instantiating all
	 * the private user variables in this object.
	 * 
	 * @param userName
	 * @return true if any match is found, otherwise false
	 */
	private boolean getUser(String userName)
	{
		String query = "select user_name, user_id, password, Name, Company, Email, Phone, UserLevel_id from Users where user_name = ?";
		//Vector v = db.select(query, cols);
		Vector v = db.selectQuery(query, new String[] { userName });

		if (v.size() > 0)
		{
			String[] attrs = (String[]) v.elementAt(0);
			assignAttrs(attrs);
			return true;
		}
		else
			return false;
	}

	/**
	 * Examines the Users table to find any matches for the given User ID.
	 * If a match is found the assignAttr() method is called instantiating all
	 * the private user variables in this object.
	 * 
	 * @param userID
	 * @return true if any match is found, otherwise false
	 */
	private boolean getUser(int userID)
	{
		String query = "select user_name, user_id, password, Name, Company, Email, Phone, UserLevel_id from Users where user_id = ?";
		//Vector v = db.select(query, cols);
		Vector v = db.selectQuery(query, new Object[] { userID });

		if (v.size() > 0)
		{
			String[] attrs = (String[]) v.elementAt(0);
			assignAttrs(attrs);
			return true;
		}
		else
			return false;
	}

	/**
	 * Used to assign all the private user variables from the String[] argument
	 * given.
	 * 
	 * @param attrs
	 */
	private void assignAttrs(String[] attrs)
	{
		user_name = attrs[0];
		user_id = attrs[1];
		password = attrs[2];
		Name = attrs[3];
		Company = attrs[4];
		Email = attrs[5];
		Phone = attrs[6];
		UserLevel_id = attrs[7];
	}

	/**
	 * Getter method to retrieve private user variable. Only works if the
	 * constructor found a matching user in the Users table, if not null will be
	 * returned.
	 * 
	 * @return
	 */
	public String getUser_name()
	{
		return user_name;
	}

	/**
	 * Getter method to retrieve private user variable. Only works if the
	 * constructor found a matching user in the Users table, if not null will be
	 * returned.
	 * 
	 * @return
	 */
	public String getUser_id()
	{
		return user_id;
	}

	/**
	 * Getter method to retrieve private user variable. Only works if the
	 * constructor found a matching user in the Users table, if not null will be
	 * returned.
	 * 
	 * @deprecated Use PasswordUtils
	 * @return
	 */
	public String getPassword()
	{
		System.err.println("WARN: Fetching password. See PasswordUtils to decrypt.");
		return password;
	}

	/**
	 * Getter method to retrieve private user variable. Only works if the
	 * constructor found a matching user in the Users table, if not null will be
	 * returned.
	 * 
	 * @return
	 */
	public String getName()
	{
		return Name;
	}

	/**
	 * Getter method to retrieve private user variable. Only works if the
	 * constructor found a matching user in the Users table, if not null will be
	 * returned.
	 * 
	 * @return
	 */
	public String getCompany()
	{
		return Company;
	}

	/**
	 * Getter method to retrieve private user variable. Only works if the
	 * constructor found a matching user in the Users table, if not null will be
	 * returned.
	 * 
	 * @return
	 */
	public String getEmail()
	{
		return Email;
	}

	/**
	 * Getter method to retrieve private user variable. Only works if the
	 * constructor found a matching user in the Users table, if not null will be
	 * returned.
	 * 
	 * @return
	 */
	public String getPhone()
	{
		return Phone;
	}

	/**
	 * Getter method to retrieve private user variable. Only works if the
	 * constructor found a matching user in the Users table, if not null will be
	 * returned.
	 * 
	 * @return
	 */
	public String getUserLevel_id()
	{
		return UserLevel_id;
	}
}
