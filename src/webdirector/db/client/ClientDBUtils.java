package webdirector.db.client;

import webdirector.db.DButils;

import java.util.Vector;

import au.net.webdirector.common.Defaults;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew Davidson
 * Date: 31-May-2004
 * Time: 09:19:48 <BR>
 * Utility class to hold data access methods for use from the client side.
 */
public class ClientDBUtils
{
	private DButils db = new DButils();
	private Defaults d;


	public ClientDBUtils()
	{
		d = Defaults.getInstance();
	}

	/**
	 * Method to increment yes or no vote count for a particular question in the
	 * FAQ module.
	 * 
	 * @param elementID
	 *            This argument identifies the FAQ questions we're updating
	 * @param columnYes
	 *            the yes column name (holds total vote count for yes)
	 * @param columnNo
	 *            the no column name (holds total vote count for no)
	 * @param value
	 *            holds the word 'yes' or 'no' to denote what the actual vote
	 *            is.
	 */
	public void writeFAQresponse(String elementID, String columnYes, String columnNo, String value)
	{
		String column = columnNo;

		// is the FAQ response a no or a yes
		if (value.equals("yes"))
			column = columnYes;

		// get current tally in this column
		String query = "select " + column + " from Elements_FAQ where Element_id = ?";
		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query, new String[] { elementID });
		int countIncremented = 0;
		if (v != null)
		{
			String val = (String) v.elementAt(0);
			try
			{
				countIncremented = Integer.parseInt(val);
			}
			catch (Exception e)
			{
				System.out.println("Exception thrown " + e.getMessage());
			}
			countIncremented++;
		}

		// write incremented value back to DB.
		query = "update Elements_FAQ set " + column + " = ? where Element_id  = ? ";
		int i = db.updateData(query, new Object[] { countIncremented, elementID });

		if (d.getDEBUG())
			System.out.println(query + " " + i);
	}

	public void clearAttributeValueInDB(String module, String id, String colName)
	{
		String query = "update Elements_" + module + " set " + colName + " = '' where Element_id = ?";
		int rows = db.updateData(query, new Object[] { id });

		System.out.println("query: " + query);
		System.out.println("rows effected : " + rows);
	}
}
