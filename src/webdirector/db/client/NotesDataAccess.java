package webdirector.db.client;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;

public class NotesDataAccess
{
	public List<Hashtable<String, String>> getNotes(String module, String foriegnKey, String field, boolean isElement)
	{
		String type = isElement ? "Elements" : "Categories";
		String sql = "select id, content, moduleName, foriegnKey, elementOrCategory, fieldName from notes" +
				" where moduleName = ?" +
				" and foriegnKey = ?" +
				" and elementOrCategory = ?" +
				" and fieldName like ?";

		List<Hashtable<String, String>> dbNotes = DataAccess.getInstance().select(sql,
				new String[] { module, foriegnKey, type, field + "%" },
				new HashtableMapper());

		return dbNotes;
	}

	public int countNotesForModule(String module)
	{
		String sql = "select count(1) from notes" +
				" where moduleName = ?";

		String size = (String) DataAccess.getInstance().select(sql, new String[] { module }, new StringMapper()).get(0);

		return Integer.parseInt(size);
	}

	public int deleteNotesForModule(String module)
	{
		return DataAccess.getInstance().updateData("delete from notes" +
				" where moduleName = ?",
				new String[] { module });
	}

	public void addNoteForCategory(String module, String foriegnKey, String field, String contents)
	{
		String id = (String) DataAccess.getInstance().select("select max(id)+1 from notes", new StringMapper()).get(0);
		String sql = "insert into notes (id," +
				"moduleName, " +
				"foriegnKey, " +
				"elementorCategory, " +
				"content, " +
				"createDate, " +
				"who, " +
				"fieldName)" +
				" values ( ?,?,?,?,?,?,?,? )";

		DataAccess.getInstance().updateData(sql,
				new String[] { id,
						module,
						foriegnKey,
						"Categories",
						contents,
						new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()),
						"NotesDataAccess",
						field + "-" + id });

	}

	public List<Hashtable<String, String>> getNotesForCategory(String module, String foriegnKey, String field)
	{
		return getNotes(module, foriegnKey, field, false);
	}

	public List<Hashtable<String, String>> getNotesForElement(String module, String foriegnKey, String field)
	{
		return getNotes(module, foriegnKey, field, true);
	}

}
