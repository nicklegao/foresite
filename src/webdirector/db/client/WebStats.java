/**
 * Created by IntelliJ IDEA.
 * User: Andrew Davidson
 * Date: Oct 12, 2003
 * Time: 9:02:49 AM
 * To change this template use Options | File Templates.
 */
package webdirector.db.client;

import webdirector.db.DButils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.TimeZone;
import java.util.Calendar;
import java.sql.*;

public class WebStats
{
	private DButils db = new DButils();

	/**
	 * Used by all the client side pages to write into the WebStats table
	 * recording all client side page activity.
	 * The table contents are then used to produce all the WebStats reporting.
	 * 
	 * @param AccessingIPAddress
	 * @param HostName
	 * @param Protocol
	 * @param ReferingPage
	 * @param CountryOfOrigin
	 * @param AssetID
	 * @param AssetType
	 * @param browserUsed
	 * @return true if the webstats table was updated correctly, false
	 *         otherwise.
	 */
	public boolean writeAStatOLD(String AccessingIPAddress, String HostName, String Protocol,
			String ReferingPage, String CountryOfOrigin, String AssetID, String AssetType, String browserUsed)
	{
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
		Date now = new Date(System.currentTimeMillis());

		String DateOfAccess = fmt.format(now);
		String updateQuery = "insert into WebStats (AccessingIPAddress, HostName, Protocol, ReferringPage, " +
				"CountryOfOrigin, AssetID, AssetType, DateOfAccess, BrowserUsed ) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

		Object[] params = new Object[] { AccessingIPAddress, HostName, Protocol, ReferingPage, CountryOfOrigin, AssetID, AssetType, DateOfAccess, browserUsed };

		if (excludeBot(browserUsed))
		{
			System.out.println("Excluded Bot Link " + browserUsed);
			return true;
		}

		// Comparing with 1 means only checking if the record inserted. Not using InsertData() as that returns element_id
		if (db.updateData(updateQuery, params) == 1)
			return true;
		else
			return false;
	}

	public boolean writeAStat(String AccessingIPAddress, String HostName, String Protocol,
			String ReferingPage, String CountryOfOrigin, String AssetID, String AssetType, String browserUsed)
	{

		DBConnection DBConn = new DBConnection();
		Connection con = DBConn.getConnection();
		int rows = 0;
		try
		{

			if (excludeBot(browserUsed))
			{
				System.out.println("Excluded Bot Link " + browserUsed);
				return true;
			}

			if (ReferingPage == null)
				ReferingPage = "";
			if (AssetID == null)
				AssetID = "0";
			if (AssetType == null)
				AssetType = "";



			Timestamp t = new Timestamp(System.currentTimeMillis());

			PreparedStatement insertStat = null;
			if (null != con)
			{
				insertStat = con.prepareStatement("insert into WebStats (AccessingIPAddress, HostName, " +
						"Protocol, ReferringPage, CountryOfOrigin, AssetID, AssetType, DateOfAccess, BrowserUsed ) " +
						"values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
				insertStat.setString(1, AccessingIPAddress);
				insertStat.setString(2, HostName);
				insertStat.setString(3, Protocol);
				insertStat.setString(4, (ReferingPage.length() < 255) ? ReferingPage : ReferingPage.substring(0, 254));
				insertStat.setString(5, CountryOfOrigin);
				insertStat.setInt(6, Integer.parseInt(AssetID));
				insertStat.setString(7, (AssetType.length() < 50) ? AssetType : AssetType.substring(0, 49));
				insertStat.setTimestamp(8, t);
				insertStat.setString(9, browserUsed);

				rows = insertStat.executeUpdate();
				insertStat.close();
			}
			else
				rows = 0;
		}
		catch (SQLException e)
		{
			System.out.println("WebStats:writeAStatPrepared(): EXCEPTION THROWN >>>");
			e.printStackTrace();
		}
		finally
		{
			DBConn.closeConnection(con);
		}

		return (rows == 1);
	}

	/**
	 *
	 * @param browserUsed
	 * @return - returns true if the browser should be excluded from being
	 *         written to the webstats table
	 */
	private boolean excludeBot(String browserUsed)
	{
		Vector v = new Vector();

		// Robot exclusion list...
		v.add("msnbot");
		v.add("LinkWalker");
		v.add("Googlebot");
		v.add("FAST MetaWeb Crawler");
		v.add("Gigabot");
		v.add("sensis");
		v.add("Sensis.com.au");
		v.add("ScanAlert");

		if (browserUsed == null || browserUsed.equals(""))
			return false;

		for (int i = 0; i < v.size(); i++)
		{
			String bot = (String) v.elementAt(i);
			if (browserUsed.toLowerCase().indexOf(bot.toLowerCase()) != -1)
				return true;
		}

		return false;
	}

	/**
	 * Part of the reporting side of the WebStats module. This method returns
	 * the top 5 accessed Products in the Elements_PRODUCTS table.
	 * This method only considers entries which have 'Products Details' as the
	 * AssetType which basically means we're only looking at actual product
	 * views, not category level usage or any other pages.
	 *
	 * So this method will return to you a Vector containing the top 5 most
	 * viewed products from your Elements_PRODUCTS table.
	 *
	 * The period used is a calendar month.
	 *
	 * @return A vector of 5 (or less if you've got less than 5 products being
	 *         viewd on your site) String[] containg the following columns in
	 *         each of the 5
	 *         arrays: "ATTR_Headline", "ATTR_ModelNumber", "ATTR_Headline",
	 *         "Category_id", "count" - count is the number of hits on that
	 *         product. The assets in the array
	 *         are in descending order, so the first position in the array is
	 *         your most viewed product.
	 */
	public Vector Top5()
	{
		TimeZone tz = TimeZone.getTimeZone("Australia/Sydney");
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTimeZone(tz);

		int d = rightNow.get(Calendar.DAY_OF_MONTH);
		int m = rightNow.get(Calendar.MONTH);
		int y = rightNow.get(Calendar.YEAR);

		String now = String.valueOf(y) + "-" + String.valueOf(++m) + "-" + String.valueOf(d);

		rightNow.add(Calendar.MONTH, -1);
		d = rightNow.get(Calendar.DAY_OF_MONTH);
		m = rightNow.get(Calendar.MONTH);
		y = rightNow.get(Calendar.YEAR);

		String minusOneMonth = String.valueOf(y) + "-" + String.valueOf(++m) + "-" + String.valueOf(d);

		// get all IPaddresses and then cross reference them against IP mapping table
		Vector v = new Vector();

		String[] cols = { "ATTR_Headline", "ATTR_ModelNumber", "ATTR_Headline", "Category_id", "count" };
		String query = "SELECT Element_id, ATTR_ModelNumber, ATTR_Headline, Category_id, count(*) as expr1 FROM Elements_PRODUCTS ep, WebStats w " +
				"WHERE DateOfAccess >= ? and DateOfAccess <= '" + now + " 23:59' and w.AssetType = 'Products Details' " +
				"and w.AssetID = ep.Element_id and live = 1 group by Element_id, ATTR_ModelNumber, ATTR_Headline, Category_id order by expr1 desc limit 5";

		//v = db.select(query, cols);
		v = db.selectQuery(query, new String[] { minusOneMonth });


		return v;
	}
}
