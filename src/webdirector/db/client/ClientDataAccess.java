/**
 * Created by IntelliJ IDEA.
 * User: Andrew Davidson
 * Date: Feb 9, 2003
 * Time: 6:43:20 AM
 * To change this template use Options | File Templates.
 */
package webdirector.db.client;

import java.io.File;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import webdirector.db.DatabaseTableMetaDataService;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;

public class ClientDataAccess implements ClientDataAccessRemoteInterface
{
	private DBaccess db = new DBaccess();
	private Defaults d;
	private String context;
	Logger logger = Logger.getLogger(ClientDataAccess.class);

	/**
	 * This class is the set of methods for accessing category and element level
	 * data from the database. For example methods exist in this class for
	 * getting all child assets of a paticular category parent, getting assets
	 * by ID, etc. Most methods in this class have two variations. One which has
	 * a control flag and one which doesn't. The difference being one method can
	 * specify if the content returned from the database should be live content
	 * only, instead of live and non-live content combined.
	 */
	public ClientDataAccess()
	{
		d = Defaults.getInstance();
	}

	// this is code for remot interface
	public ClientDataAccess(String context)
	{
		this.context = context;

	}

	public void setContext(String context)
	{
		this.context = context;

		d.setOdbcConString(context);
	}

	public String[] getCategoryCols(String module)
	{
		String[] colList = null;

		String query = "select Label_InternalName from CategoryLabels_" + DatabaseValidation.encodeParam(module) + " order by Label_id";

		// check for a cache hit
		colList = DatabaseTableMetaDataService.labelsTableService(query);
		if (colList != null)
		{
			return colList;
		}

		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query);

		if (v.size() > 0)
		{
			colList = new String[v.size() + 2];
			colList[0] = "Category_id";
			colList[1] = "Category_ParentID";

			for (int i = 0; i < v.size(); i++)
			{
				String colName = (String) v.elementAt(i);
				colList[i + 2] = colName;
			}
		}

		// add it into the cache
		DatabaseTableMetaDataService.labelsTableAddToCache(query, colList);

		return colList;
	}

	/**
	 * Accesses the database for a list of internal column names for the module
	 * specified.
	 * 
	 * @param module
	 *            to get internal column names for.
	 * @return String[] of the column names ordered by label_id.
	 */
	public String[] getElementCols(String module)
	{
		String[] colList = null;
		String query = "select Label_InternalName from Labels_" + DatabaseValidation.encodeParam(module) + " order by Label_id";

		// check for a cache hit
		colList = DatabaseTableMetaDataService.labelsTableService(query);
		if (colList != null)
		{
			return colList;
		}

		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query);

		if (v.size() > 0)
		{
			colList = new String[v.size() + 2];
			colList[0] = "Element_id";
			colList[1] = "Category_id";

			for (int i = 0; i < v.size(); i++)
			{
				String colName = (String) v.elementAt(i);
				colList[i + 2] = colName;
			}
		}

		// add it into the cache
		DatabaseTableMetaDataService.labelsTableAddToCache(query, colList);

		return colList;
	}

	/**
	 * Returns the category name for the specified arguments supplied. If more
	 * than one Category is returned from the database the first one only is
	 * returned from this method. (more than one should not exist if database
	 * IDs are intact and unique)
	 * 
	 * @param module
	 *            Category name
	 * @param categoryId
	 *            numeric id of the category to get name for.
	 * @return String Category name and empty stirng if it didn't find any thing
	 */
	public String getCategoryFromId(String module, String categoryId)
	{
		String query = "select ATTR_categoryName from Categories_" + DatabaseValidation.encodeParam(module) + " where Category_id = ?";
		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query, new String[] { categoryId });

		if (v == null || v.size() == 0)
			return null;
		else
			return (String) v.elementAt(0);
	}

	/**
	 * ***** DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER
	 * ***** Do not use - special for Minolta site which has fixed top level
	 * category names ***** DANGER DANGER DANGER DANGER DANGER DANGER DANGER
	 * DANGER DANGER *****
	 * 
	 * Gets the category ID for the category name and module passed in. Is very
	 * dangerous because system does allow duplicate category names which in
	 * this methods case will then cause random results. In Minolta's case its
	 * fine as the top level categories are uniquely named.
	 * 
	 * @param module
	 * @param categoryName
	 * @return String Category ID or "" if not found.
	 */
	public String getIdFromCategory(String module, String categoryName)
	{
		// ***** DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER
		// *****
		// ***** DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER
		// *****
		// Do not use - special for Minolta site which has fixed top level
		// category names
		// ***** DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER
		// *****
		// ***** DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER
		// *****
		String query = "select Category_id from Categories_" + DatabaseValidation.encodeParam(module) + " where ATTR_categoryName = ?";
		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query, new String[] { categoryName });

		if (v.size() == 0)
			return "";
		else
			return (String) v.elementAt(0);
	}

	/**
	 * Special method to cut down db access when need to get an elements
	 * category parent then that categories parent and its Category name -
	 * obviously only works for product catalogues with 2 levels before you hit
	 * the elements.
	 * 
	 * @param module
	 *            name
	 * @param elementID
	 * @return Vector of String[]
	 */
	public Vector get2LevelDetails(String module, String elementID)
	{
		String[] cols = { "Category_id", "Category_ParentID", "ATTR_categoryName" };
		String query = "SELECT ele.Category_id, cat1.Category_ParentID, cat2.ATTR_categoryName " + "FROM Elements_PRODUCTS ele, Categories_PRODUCTS cat1, Categories_PRODUCTS cat2 " + "where ele.Category_id = cat1.Category_id and cat1.Category_ParentID = cat2.Category_id and " + "ele.Element_id = ?";

		//Vector v = db.select(query, cols);
		Vector v = db.selectQuery(query, new String[] { elementID });
		return v;
	}

	public String getCategoryFromElement(String module, String elementID, boolean live)
	{
		String query = "select Category_id from Elements_" + DatabaseValidation.encodeParam(module) + " where Element_id = ? and live = ?";
		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query, new Object[] { elementID, live });
		if (v.size() > 0)
			return (String) v.elementAt(0);
		else
			return "";
	}

	/**
	 * Gets the category ID which is the parent of the element identified in the
	 * arguments supplied.
	 * 
	 * @param module
	 *            name
	 * @param elementID
	 * @return String Category ID or possibly null if nothing is matched
	 */
	public String getCategoryFromElement(String module, String elementID)
	{
		String query = "select Category_id from Elements_" + DatabaseValidation.encodeParam(module) + " where Element_id = ?";
		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query, new String[] { elementID });

		return (String) v.elementAt(0);
	}

	/**
	 * Returns the category parent ID for the category and module arguments
	 * supplied.
	 * 
	 * @param module
	 * @param categoryId
	 * @return String or possibly null if nothing is matched
	 */
	public String getCategoryParent(String module, String categoryId)
	{
		String query = "select Category_ParentID from Categories_" + DatabaseValidation.encodeParam(module) + " where Category_id = ?";
		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query, new String[] { categoryId });

		return (String) v.elementAt(0);
	}

	public String getCategoryParentName(String module, String categoryId)
	{
		String query = "select p.attr_categoryName from Categories_" + DatabaseValidation.encodeParam(module) + " c, Categories_" + DatabaseValidation.encodeParam(module) + " p where c.category_parentid = p.category_id and c.Category_id = ?";
		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query, new String[] { categoryId });

		return v.size() == 0 ? null : (String) v.elementAt(0);
	}

	/**
	 * Gets a list of all entries in the Categories_SUBSCRIBE table. Fields
	 * returned are: "ATTR_categoryName", "Category_id", "Category_ParentID",
	 * "ATTR_categoryDesc", "folderLevel", "ATTRFILE_image"
	 * 
	 * @return Vector of String[] or possibly null if nothing is matched
	 */
	public Vector getSubscribeTopics()
	{
		return getSubscribeTopics(null);
	}

	public Vector getSubscribeTopics(String userId)
	{
		String[] cols = getCategoryCols("SUBSCRIBE");

		String extraSQL = "";
		List<String> params = new ArrayList<String>();

		if (userId != null)
		{
			extraSQL = " where category_id in (select category_id from user_module_access where user_id=? and module_name='SUBSCRIBE') ";
			params.add(userId);
		}
		String query = "select ATTR_categoryName, Category_id, Category_ParentID, ATTR_categoryDesc, folderLevel, " + "ATTRFILE_image from Categories_SUBSCRIBE" + extraSQL + " order by ATTR_categoryName";

		//Vector returnVector = db.select(query, cols);
		Vector returnVector = db.selectQuery(query, params.toArray());

		return returnVector;
	}

	public Vector getLevelCategoriesWithUserControl(String module, String userID, boolean liveContentOnly, boolean stringManipulation)
	{
		String[] cols = getCategoryCols(module);
		String query = "Select c." + cols[0];
		for (int i = 1; i < cols.length; i++)
		{
			query = query.concat(",c." + cols[i]);
		}
		query = query.concat(" from  categories_" + DatabaseValidation.encodeParam(module) + " c");
		query = query.concat(" where c.folderLevel = 1 and c.live = 1 and (c.live_date < now() or c.live_date is null) and (c.expire_date > now() or c.expire_date is null)");
		query = query.concat(" and ((c.attrcheck_private = '0') or (c.attrcheck_private = '1' and c.Category_id in (select CATEGORY_ID from user_module_access u where u.MODULE_NAME = ? and u.USER_ID = ?)))");
		System.out.println("N11Query" + query);
		Vector returnVector = new Vector();
		//Vector v = db.select(query, cols);
		Vector v = db.selectQuery(query, new String[] { module, userID });

		Vector colsData = DatabaseTableMetaDataService.columnService("Categories_" + module);

		logger.debug("getColumns: rows(" + v.size() + ")\n " + query);

		// switch vector into hash array
		for (int i = 0; i < v.size(); i++)
		{
			// System.out.println("XXXX "+i);

			Hashtable h = new Hashtable();
			String[] vals = (String[]) v.elementAt(i);
			// inner loop - through columns in a row
			for (int x = 0; x < cols.length; x++)
			{
				boolean bigCol = false;
				String[] c = (String[]) colsData.elementAt(x);
				int colSize = Integer.parseInt(c[2]);

				if (colSize > 255)
					bigCol = true;
				String tmp = "";
				if (cols[x].startsWith("ATTR_") && bigCol == false && stringManipulation)
				{
					tmp = replaceCarriageReturns(vals[x]);
				}
				else
					tmp = vals[x];

				h.put(cols[x], tmp);
			}
			returnVector.addElement(h);
		}

		return returnVector;
	}

	/**
	 * See getColumns method as this is essentially just a wrapper method to it.
	 * Returns all categories at a given level in the tree hierarchy.
	 * 
	 * @param module
	 *            name to get categories for
	 * @param theLevel
	 *            category level 1,2,3,...
	 * @param isString
	 *            should be set to false for this method as we're using 1,2,3
	 *            ... an integer value so no quotes needed.
	 * @param getAll
	 *            if set to true, the constraint in the query is not added, i.e.
	 *            no constraints so all categories are returned
	 * @param liveContentOnly
	 *            only retrieve categories which are marked as live
	 * @return Vector of Hashes. Each Hash is a name/value pair containing <col
	 *         name> and <value>
	 */
	public Vector getLevelCategoriesWithControl(String module, String theLevel, boolean isString, boolean getAll, boolean liveContentOnly)
	{
		String[] cols = getCategoryCols(module);

		Vector returnVector = getColumns(cols, module, "Categories", "folderLevel", theLevel, isString, getAll, liveContentOnly);

		return returnVector;
	}

	/**
	 * See getColumns method as this is essentially just a wrapper method to it.
	 * Returns all categories at a given level in the tree hierarchy.
	 * 
	 * @param module
	 *            name to get categories for
	 * @param theLevel
	 *            category level 1,2,3,...
	 * @param isString
	 *            should be set to false for this method as we're using 1,2,3
	 *            ... an integer value so no quotes needed.
	 * @param getAll
	 *            if set to true, the constraint in the query is not added, i.e.
	 *            no constraints so all categories are returned
	 * @param liveContentOnly
	 *            only retrieve categories which are marked as live
	 * @param distinctKey
	 *            specifies a column name which will be used in the query to
	 *            retrieve categories where this column is distinct in the
	 *            result set.
	 * @return Vector of Hashes. Each Hash is a name/value pair containing <col
	 *         name> and <value>
	 */
	public Vector getLevelCategoriesWithControl(String module, String theLevel, boolean isString, boolean getAll, boolean liveContentOnly, String distinctKey)
	{
		String[] cols = distinctKey == null ? getCategoryCols(module) : new String[] { distinctKey };

		Vector returnVector = getColumns(cols, module, "Categories", "folderLevel", theLevel, isString, getAll, liveContentOnly, distinctKey, null, true);

		return returnVector;
	}

	/**
	 * Get all categories for a particular level (in a module) without any
	 * restrictions.
	 * 
	 * @param module
	 * @param theLevel
	 * @return Vector of Categories (in a hash) within the module at a
	 *         particular level
	 */
	public Vector getLevelCategories(String module, String theLevel)
	{
		String[] cols = getCategoryCols(module);

		Vector returnVector = getColumns(cols, module, "Categories", "folderLevel", theLevel, false, false, false);

		return returnVector;
	}

	/**
	 * Essentially a wrapper method to getCategoryIdUsingParent. Does the same
	 * as this method but instead of returning the Category ID (if a match is
	 * found) this method returns true or false, depending on the existance of
	 * the category. Not a straight forward Category check though as the method
	 * arguments specify a particular parent category and a level other than the
	 * category name itself.
	 * 
	 * @param module
	 * @param theLevel
	 *            in the tree to get categories from
	 * @param category
	 *            name of category
	 * @param parent
	 *            category ID
	 * @return boolean true if Category exists, false otherwise
	 */
	public boolean doesCategoryExist(String module, String theLevel, String category, String parent)
	{
		String id = getCategoryIdUsingParent(module, theLevel, category, parent);

		if (id.equals("0"))
			return false;
		else
			return true;
	}

	/**
	 * Returns the Category ID depending on the existance of the category. Not a
	 * straight forward Category retrieval though as the method arguments
	 * specify a particular parent category and a level in the folder tree. So
	 * to get a category ID returned it must exist with the name specified
	 * underneath the parent category ID given and at the level in the folder
	 * tree specified.
	 * 
	 * @param module
	 * @param theLevel
	 *            in the tree to get categories from
	 * @param category
	 *            name of category
	 * @param parent
	 *            category ID
	 * @return String of the category ID if found, 0 otherwise
	 */
	public String getCategoryIdUsingParent(String module, String theLevel, String category, String parent)
	{

		List<String> params = new ArrayList<String>();
		String query = "select Category_id from Categories_" + DatabaseValidation.encodeParam(module) + " where " + "ATTR_categoryName = ? and folderLevel = ?";
		params.add(category);
		params.add(theLevel);

		if (parent != null)
		{
			// also add a parent check if a parent name is supplied
			query = "select Category_id from Categories_" + DatabaseValidation.encodeParam(module) + " where " + "ATTR_categoryName = ? and folderLevel = ? and Category_ParentID = ?";
			params.add(parent);
		}

		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query, params.toArray());

		if (v.size() == 0)
			return "0";
		else
			return (String) v.elementAt(0);
	}

	/**
	 * This is special method for RIL for now but we can extend this method to
	 * make it more generic by adding extra argument to specifyl level
	 * =========================== This method will give you Element id of asset
	 * matching asset in elements_table and it aslo match ParentID of parent of
	 * this asset.
	 * 
	 * This is only works for 2 level categories but we can extend this method.
	 * 
	 * @param module
	 * @param parentId
	 *            : ParentID of top level Parent of Asset
	 * @param assetName
	 *            - name of asset not ID.
	 * @return
	 */
	public String getElementIdbyCategoryParentId(String module, String parentId, String assetName)
	{

		String query = "Select Element_id from Elements_" + DatabaseValidation.encodeParam(module) + " e1,  Categories_" + DatabaseValidation.encodeParam(module) + " c1, Categories_" + DatabaseValidation.encodeParam(module) + " c2 "
				+ " where c1.Category_id = ? and e1.ATTR_Headline = ? and e1.Category_Id = c2.Category_Id and c2.Category_ParentID = c1.Category_id";
		System.out.println("query========>>>>>>>>>" + query);
		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query, new String[] { parentId, assetName });

		if (v.size() > 0)
			return (String) v.get(0);
		else
			return null;
	}

	public String getElementIdForElementInCategory(String module, String parentId, String elementName)
	{
		String query = "Select Element_id from Elements_" + DatabaseValidation.encodeParam(module) + " e1,  Categories_" + DatabaseValidation.encodeParam(module) + " c1 where " + "c1.Category_id = ? and e1.ATTR_Headline = '" + sq(elementName) + "' " + "and e1.Category_Id = c1.Category_Id";
		System.out.println("query========>>>>>>>>>" + query);
		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query, new String[] { parentId });
		if (v.size() > 0)
			return (String) v.get(0);
		else
			return null;
	}

	/**
	 * Get all the Categories for a given parent ID
	 * 
	 * @param module
	 *            to get categories from
	 * @param parentID
	 *            the parent category ID
	 * @return Vector of Categories (in a hash) or null
	 */
	public Vector getCategoryChildren(String module, String parentID)
	{
		String[] cols = getCategoryCols(module);

		Vector returnVector = getColumns(cols, module, "Categories", "Category_ParentID", parentID, false, false, false);

		return returnVector;
	}

	/**
	 * Same as getCategoryChildren but returns only categories marked as live in
	 * the database.
	 * 
	 * @param module
	 *            to get categories from
	 * @param parentID
	 *            the parent category ID
	 * @return Vector of Categories (in a hash) or null if none found.
	 */
	public Vector getLiveCategoryChildren(String module, String parentID)
	{
		Vector returnVector = getCategoryChildrenWithControl(module, parentID, true);

		return returnVector;
	}

	/**
	 * Same as getCategoryChildren but returns only categories marked as live in
	 * the database.
	 * 
	 * @param module
	 *            to get categories from
	 * @param parentID
	 *            the parent category ID
	 * @param liveContentOnly
	 *            boolean denoting retrieval of live only content (true), if set
	 *            to false then all categories are returned irrespective of the
	 *            live flag
	 * @return Vector of Categories (in a hash) or null if none found.
	 */
	public Vector getCategoryChildrenWithControl(String module, String parentID, boolean liveContentOnly)
	{
		String[] cols = getCategoryCols(module);

		Vector returnVector = getColumns(cols, module, "Categories", "Category_ParentID", parentID, false, false, liveContentOnly);

		return returnVector;
	}

	/**
	 * Same as getCategoryChildrenWithControl but this method has the extra
	 * distinctKey argument which allows you to further refine the search for
	 * categories to include the 'distinct' directive on a particular column in
	 * the query when retrieving categories.
	 * 
	 * @param module
	 *            to get categories from
	 * @param parentID
	 *            the parent category ID
	 * @param liveContentOnly
	 *            boolean denoting retrieval of live only content (true), if set
	 *            to false then all categories are returned irrespective of the
	 *            live flag
	 * @param distinctKey
	 *            column name in the category table which needs refines query as
	 *            unique (distinct)
	 * @return Vector of Categories (in a hash) or null if none found.
	 */
	public Vector getCategoryChildrenWithControl(String module, String parentID, boolean liveContentOnly, String distinctKey)
	{
		String[] cols = distinctKey == null ? getCategoryCols(module) : new String[] { distinctKey };

		Vector returnVector = getColumns(cols, module, "Categories", "Category_ParentID", parentID, false, false, liveContentOnly, distinctKey, null, true);

		return returnVector;
	}

	/**
	 * Returns all the elements in a bottom level category. For example all the
	 * news assets in the Latest category. Dangerous method as it relies on the
	 * CategoryName being unique which it may not be. i.e. if the site has
	 * non-unique category names this method will give strange results. SHOULD
	 * NOT BE USED
	 * 
	 * @param module
	 * @param categoryName
	 *            name of bottom level category
	 * @return Vector of Assets (in a hash) or null if none found.
	 */
	public Vector getCategoryElementsByName(String module, String categoryName)
	{
		return getCategoryElementsByName(module, categoryName, true);
	}

	/**
	 * Returns all the elements in a bottom level category. For example all the
	 * news assets in the Latest category. Dangerous method as it relies on the
	 * CategoryName being unique which it may not be. i.e. if the site has
	 * non-unique category names this method will give strange results. SHOULD
	 * NOT BE USED
	 * 
	 * @param module
	 * @param categoryName
	 *            name of bottom level category
	 * @return Vector of Assets (in a hash) or null if none found.
	 */
	public Vector getCategoryElementsByName(String module, String categoryName, boolean live)
	{
		// dangerous method as we could have multiple categories with the same
		// name !
		String[] cols = getElementCols(module);

		String category_id = getIdFromCategory(module, categoryName);

		if (category_id == null || category_id.equals(""))
		{
			System.out.println("Category " + categoryName + " does not exist, and it should.");
			return null;
		}

		Vector returnVector = getColumns(cols, module, "Elements", "Category_id", category_id, false, false, live);

		return returnVector;
	}

	/*
	 * Find elements within a category in a module with specified element
	 * headline
	 */
	public Vector getCategoryElementsByCategoryNameAndElementName(String module, String categoryName, String elementName)
	{
		Vector elements = getCategoryElementsByName(module, categoryName);
		Vector results = new Vector();
		for (Object object : elements)
		{
			Hashtable table = (Hashtable) object;
			if (elementName.equals((String) table.get("ATTR_Headline")))
			{
				results.add(table);
			}
		}
		return results;
	}

	/**
	 * Used to retrieve all elements/assets within a bottom level category by
	 * giving the category id This is a safe version of the
	 * getCategoryElementsByName method Returns live content only
	 * 
	 * @param module
	 * @param category_id
	 *            of bottom level category
	 * @return Vector of (live only) Assets (in a hash) or null if none found.
	 */
	public Vector getCategoryElementsWithSpecifiedOrder(String module, String category_id, String orderColumn)
	{
		String[] cols = getElementCols(module);

		Vector returnVector = getColumns(cols, module, "Elements", "Category_id", category_id, false, false, true, null, orderColumn, true);

		return returnVector;
	}

	/**
	 * Used to retrieve all elements/assets within a bottom level category by
	 * giving the category id This is a safe version of the
	 * getCategoryElementsByName method Returns live content only
	 * 
	 * @param module
	 * @param category_id
	 *            of bottom level category
	 * @return Vector of (live only) Assets (in a hash) or null if none found.
	 */
	public Vector getCategoryElements(String module, String category_id)
	{
		return (getCategoryElements(module, category_id, true));
	}

	/**
	 * Used to retrieve all elements/assets within a bottom level category by
	 * giving the category id Also allows you to specify if you want only want
	 * live assets (liveContentOnly=true) or all assets (liveContentOnly=false)
	 * 
	 * @param module
	 * @param category_id
	 *            of bottom level category
	 * @param liveContentOnly
	 *            boolean denoting retrieval of live only content (true),
	 *            otherwise get all irrespective of live flag
	 * @return Vector of Assets (in a hash) or null if none found.
	 */
	public Vector getCategoryElements(String module, String category_id, boolean liveContentOnly)
	{
		String[] cols = getElementCols(module);

		Vector returnVector = getColumns(cols, module, "Elements", "Category_id", category_id, false, false, liveContentOnly);

		return returnVector;
	}

	public String getElementName(String element_id, String Module)
	{
		String query = "Select ATTR_Headline from elements_" + DatabaseValidation.encodeParam(Module) + " where Element_id = ?";

		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query, new String[] { element_id });
		if (v.size() > 0)
		{
			return (String) v.get(0);
		}
		else
			return "";
	}

	/**
	 * Returns all the category data for the ID given. Allows you to specify if
	 * the content should only be returned if its live flag is set.
	 * 
	 * @param module
	 *            to get category from
	 * @param category_id
	 *            of any category in the above module
	 * @param liveOnly
	 *            boolean denoting retrieval of live only content (true),
	 *            otherwise get all irrespective of live flag
	 * @return Vector containing a single category (in a hash) or null if none
	 *         found. Should only ever return a single category hash in the
	 *         vector
	 */
	public Vector getCategory(String module, String category_id, boolean liveOnly)
	{
		String[] cols = getCategoryCols(module);

		Vector returnVector = getColumns(cols, module, "Categories", "Category_id", category_id, false, false, liveOnly);

		return returnVector;
	}

	public Vector getCategoryFromElementName(String module, String elementName)
	{
		return getCategoryFromElementName(module, elementName, true);
	}

	public Vector getCategoryFromElementName(String module, String elementName, boolean liveOnly)
	{
		Vector elements = getElementByName(module, elementName, liveOnly);
		if (elements.size() > 0)
		{
			Hashtable ht = (Hashtable) elements.get(0);
			String category_id = (String) ht.get("Category_id");
			return getCategory(module, category_id, liveOnly);
		}
		return null;
	}

	/**
	 * Returns the element/asset in the given module for the asset ID specified.
	 * If the live flag is set to true then the asset will only be returned if
	 * its live flag is set to true.
	 * 
	 * @param module
	 *            to get asset from
	 * @param element_id
	 *            in the above module
	 * @param liveContentOnly
	 *            boolean denoting retrieval of live only content (true),
	 *            otherwise get all irrespective of live flag
	 * @return Vector of Assets (in a hash) or null if none found. Should only
	 *         ever return a vector with a single hashtable in it.
	 */
	public Vector getElementWithControl(String module, String element_id, boolean liveContentOnly)
	{
		String[] cols = getElementCols(module);

		Vector returnVector = getColumns(cols, module, "Elements", "Element_id", element_id, false, false, liveContentOnly);

		return returnVector;
	}

	/**
	 * Returns a single hash (within a vector wrapper) for the element ID
	 * argument given. Only returns the element if the live flag is true.
	 * 
	 * @param module
	 * @param element_id
	 * @return Vector with a hash in it of the element or null if not found.
	 */
	public Vector getElement(String module, String element_id)
	{
		// get live content only by default
		return (getElement(module, element_id, true));

	}

	public Vector<String> getMultiSelectionValuesForElement(String moduleName, String element_id, String multiValName)
	{
		String sql_getSelectedValues = "select SELECTED_VALUE from drop_down_multi_selections where element_id = ? and attrmulti_name =? and module_name =? and module_type = 'element'";

		@SuppressWarnings("unchecked")
		//Vector<String> v_selectedValues = db.select(sql_getSelectedValues);
		Vector v_selectedValues = db.selectQuerySingleCol(sql_getSelectedValues, new String[] { element_id, multiValName, moduleName });

		return v_selectedValues;
	}

	public int getRandom(int no)
	{
		Random rand = new Random();
		return Math.abs(rand.nextInt(no));
	}

	/**
	 * Returns a single hash (within a vector wrapper) for the element ID
	 * argument given. The live flag can be set true to return the element only
	 * its live flag is also true.
	 * 
	 * @param module
	 * @param element_id
	 * @param liveContentOnly
	 * @return
	 */
	public Vector getElement(String module, String element_id, boolean liveContentOnly)
	{
		Vector returnVector = getElementWithControl(module, element_id, liveContentOnly);

		return returnVector;
	}

	/**
	 * Returns all elements in a module with no restrictions other than elements
	 * have to be live.
	 * 
	 * @param module
	 *            name of module to get elements from
	 * @return Vector of hashes
	 */
	public Vector getAllElements(String module)
	{
		return (allElements(module));
	}

	/**
	 * Returns all elements in a module with no restrictions other than elements
	 * have to be live with order by specified order.
	 * 
	 * @param module
	 *            name of module to get elements from
	 * @return Vector of hashes
	 */
	public Vector getAllElementsBySpecifiedOrder(String module, String orderColumn)
	{
		String[] cols = getElementCols(module);
		Vector returnVector = getColumns(cols, module, "Elements", "", "", false, false, true, null, orderColumn, true);

		return returnVector;
	}

	public String[] getElementNames(String module, boolean liveContentOnly)
	{
		String query = "select ATTR_Headline from Elements_" + DatabaseValidation.encodeParam(module);
		if (liveContentOnly)
			query += " where live = 1";

		String elementName = null;
		//Vector tmpResults = db.select(query);
		Vector tmpResults = db.selectQuerySingleCol(query);

		String[] resultArray = new String[tmpResults.size()];
		if (tmpResults != null && tmpResults.size() > 0)
		{
			for (int i = 0; i < tmpResults.size(); i++)
			{
				resultArray[i] = (String) tmpResults.elementAt(i);
			}
		}

		return (resultArray);
	}

	/**
	 * returns the element name (ATTR_Headline)
	 * 
	 */
	public String getElementName(String module, String element_id, boolean liveContentOnly)
	{
		String query = "select ATTR_Headline from Elements_" + DatabaseValidation.encodeParam(module) + " where element_id = ?";
		if (liveContentOnly)
			query += " and live = 1";

		String elementName = null;
		//Vector tmpResults = db.select(query);
		Vector tmpResults = db.selectQuerySingleCol(query, new String[] { element_id });

		if (tmpResults != null && tmpResults.size() > 0)
			elementName = (String) tmpResults.elementAt(0);

		return (elementName);
	}

	public String getCategoryName(String module, String element_id)
	{
		String query = "select ATTR_categoryName from Categories_" + DatabaseValidation.encodeParam(module) + " where category_id = ?";

		String elementName = null;
		//Vector tmpResults = db.select(query);
		Vector tmpResults = db.selectQuerySingleCol(query, new String[] { element_id });

		if (tmpResults != null && tmpResults.size() > 0)
			elementName = (String) tmpResults.elementAt(0);

		return (elementName);
	}

	public String getElementIdUsingName(String module, String name)
	{
		String query = "select Element_id from Elements_" + DatabaseValidation.encodeParam(module) + " where ATTR_Headline = ?";

		String elementName = "";
		System.out.println("N11 querry for userID===>>>>>" + query);
		//Vector tmpResults = db.select(query);
		Vector tmpResults = db.selectQuerySingleCol(query, new String[] { name });

		if (tmpResults != null && tmpResults.size() > 0)
			elementName = (String) tmpResults.elementAt(0);

		return (elementName);
	}

	public String getCategoryIdByCategoryName(String module, String categoryName)
	{
		String query = "select Category_id from Categories_" + DatabaseValidation.encodeParam(module) + " where ATTR_categoryName = ?";

		String categoryId = null;
		//Vector tmpResults = db.select(query);
		Vector tmpResults = db.selectQuerySingleCol(query, new String[] { categoryName });

		if (tmpResults != null && tmpResults.size() > 0)
			categoryId = (String) tmpResults.elementAt(0);

		return (categoryId);
	}

	public String getCategoryIdByCategoryNameWithParent(String module, String categoryName, String parentId)
	{
		String query = "select Category_id from Categories_" + DatabaseValidation.encodeParam(module) + " where ATTR_categoryName = ? and Category_ParentID = ?";

		String categoryId = null;
		//Vector tmpResults = db.select(query);
		Vector tmpResults = db.selectQuerySingleCol(query, new String[] { categoryName, parentId });

		if (tmpResults != null && tmpResults.size() > 0)
			categoryId = (String) tmpResults.elementAt(0);

		return (categoryId);
	}

	/**
	 * Returns all elements in a module with no restrictions other than elements
	 * have to be live.
	 * 
	 * @param module
	 *            name of module to get elements from
	 * @return Vector of hashes
	 */
	public Vector allElements(String module)
	{
		String[] cols = getElementCols(module);

		Vector returnVector = getColumns(cols, module, "Elements", "", "", false, true, true);

		return returnVector;
	}

	/**
	 * Returns all Categories in a module with no restrictions other than
	 * categories have to be live.
	 * 
	 * @param module
	 *            name of module to get categories from
	 * @return Vector of hashes
	 */
	public Vector getAllCategories(String module)
	{
		return (allCategories(module));
	}

	/**
	 * Returns all categories in a module with no restrictions other than
	 * categories have to be live.
	 * 
	 * @param module
	 *            name of module to get categories from
	 * @return Vector of hashes
	 */
	public Vector allCategories(String module)
	{
		String[] cols = getCategoryCols(module);

		Vector returnVector = getColumns(cols, module, "Categories", "", "", false, true, true);

		return returnVector;
	}

	/**
	 * Returns all element attributes in the hash, if a match is found between
	 * the value in the elementName argument and in the ATTR_Headline field in
	 * the database. A live asset will only be selected. This method should only
	 * ever return a single asset in the Vector. Dangerous method as element
	 * names are not unique BEWARE !
	 * 
	 * @param module
	 * @param elementName
	 * @return Vector with a hash in it or null
	 */
	public Vector getElementByName(String module, String elementName)
	{
		return (getElementByName(module, elementName, true));
	}

	/**
	 * Returns all element attributes in the hash, if a match is found between
	 * the value in the elementName argument and in the ATTR_Headline field in
	 * the database. You can select if live assets only (or not) will be
	 * selected. This method should only ever return a single asset in the
	 * Vector. Dangerous method as element names are not unique BEWARE !
	 * 
	 * @param module
	 * @param elementName
	 * @return Vector with a hash in it or null
	 */
	public Vector getElementByName(String module, String elementName, boolean liveContentOnly)
	{
		String[] cols = getElementCols(module);

		Vector returnVector = getColumns(cols, module, "Elements", "ATTR_Headline", elementName, true, false, liveContentOnly);

		return returnVector;
	}

	/**
	 * Given a column name and a value this method tries to find that asset in
	 * the element table. You must tell the method if the search column is a
	 * string value so the method can quote the argument or not in the SQL
	 * query. Can select multiple assets if the columnValue is common across
	 * many assets.
	 * 
	 * @param module
	 * @param columnName
	 * @param columnValue
	 * @param isColumnString
	 * @return Vector of hashes or null
	 */
	public Vector getElementByValue(String module, String columnName, String columnValue, boolean isColumnString)
	{
		String[] cols = getElementCols(module);

		Vector returnVector = getColumns(cols, module, "Elements", columnName, columnValue, isColumnString, false, true);

		return returnVector;
	}

	/**
	 * Given a column name and a value this method tries to find that asset in
	 * the element table. You must tell the method if the search column is a
	 * string value so the method can quote the argument or not in the SQL
	 * query. Can select multiple assets if the columnValue is common across
	 * many assets.
	 * 
	 * @param module
	 * @param columnName
	 * @param columnValue
	 * @param isColumnString
	 * @param isStringManipulation
	 *            if this is false than there is no string manipulation
	 * @return Vector of hashes or null
	 */
	public Vector getElementByValue(String module, String columnName, String columnValue, boolean isColumnString, boolean isStringManipulation)
	{
		String[] cols = getElementCols(module);

		Vector returnVector = getColumns(cols, module, "Elements", columnName, columnValue, isColumnString, false, true, isStringManipulation);

		return returnVector;
	}

	/**
	 * This is the same method as the other getColumns method but a null is
	 * passed into the additional 'distinctKey' argument Please see this method
	 * description for a full explanantion.
	 * 
	 * @param cols
	 * @param module
	 * @param tablePrefix
	 * @param keyCol
	 * @param keyVal
	 * @param isString
	 * @param getAll
	 * @param liveContentOnly
	 * @param isStringManipulation
	 *            :: if false there is not string manipulation
	 * @return
	 */
	public Vector getColumns(String[] cols, String module, String tablePrefix, String keyCol, String keyVal, boolean isString, boolean getAll, boolean liveContentOnly, boolean isStringManipulation)
	{
		return getColumns(cols, module, tablePrefix, keyCol, keyVal, isString, getAll, liveContentOnly, null, null, isStringManipulation);
	}

	/**
	 * This is the same method as the other getColumns method but a null is
	 * passed into the additional 'distinctKey' argument Please see this method
	 * description for a full explanantion.
	 * 
	 * @param cols
	 * @param module
	 * @param tablePrefix
	 * @param keyCol
	 * @param keyVal
	 * @param isString
	 * @param getAll
	 * @param liveContentOnly
	 * @return
	 */
	public Vector getColumns(String[] cols, String module, String tablePrefix, String keyCol, String keyVal, boolean isString, boolean getAll, boolean liveContentOnly)
	{
		return getColumns(cols, module, tablePrefix, keyCol, keyVal, isString, getAll, liveContentOnly, null, null, true);
	}

	/**
	 * Low level method which builds a query and returns the set of columns
	 * specified within the module given. You can use this method to access
	 * content in Element_* and Category_* tables. You can specify a column and
	 * assocaited value to constrain the query to return only rows which meet
	 * this condition. Generally this column/value pair specifies an ID column,
	 * keyCol - (Element_ID or Category_ID) and the actual ID value as the
	 * keyVal. Therefore this will find a particular row with this ID. Of course
	 * you can use this method with any column. Most getter methods in this
	 * class use this method underneath.
	 * 
	 * @param cols
	 *            array of columns to return
	 * @param module
	 *            table to look in
	 * @param tablePrefix
	 *            Element or Category
	 * @param keyCol
	 *            the column name to add to the query
	 * @param keyVal
	 *            the value of the above column which must be matched in order
	 *            to be returned
	 * @param isString
	 *            must be set to true if keyVal is a string and therefore needs
	 *            quoting when used in a SQL query.
	 * @param getAll
	 *            if this is only flag set to true then it returns all live
	 *            elements
	 * @param liveContentOnly
	 *            get only live content (true) or get all content (false)
	 * @param distinctKey
	 *            specifies a column name to add a 'disctint( <distinctKey> )'
	 *            section to the SQL clause
	 * @param stringManipulation
	 *            if this is false than there is no string manipulation on data
	 * @return Vector with hashtables inside it. One hashtable for each set of
	 *         columns (essentially each row returned) from the query generated.
	 */
	public Vector getColumns(String[] cols, String module, String tablePrefix, String keyCol, String keyVal, boolean isString, boolean getAll, boolean liveContentOnly, String distinctKey, String orderColumn, boolean stringManipulation)
	{
		String quote = isString ? "'" : "";

		// standard where clause
		keyVal = sq(keyVal);
		String whereClause = " where 1 = 1 ";

		if (liveContentOnly)
		{
			whereClause += " and Live = 1 and (Live_date <= now() or Live_date IS NULL ) and (Expire_date >= now() or Expire_date IS NULL ) ";
		}
		if (keyCol != null && !keyCol.equals(""))
		{
			whereClause += " and " + keyCol + " = " + quote + keyVal + quote;
		}
		// build query
		StringBuffer colList = new StringBuffer();
		if (cols[0].equals(distinctKey))
		{
			colList.append(" distinct(" + cols[0] + ")");
		}
		else
		{
			colList.append(cols[0]);
		}

		for (int y = 1; y < cols.length; y++)
		{
			if (cols[y].equals(distinctKey))
			{
				colList.append(", distinct (" + cols[y] + ")");
			}
			else
			{
				colList.append("," + cols[y]);
			}
		}

		String orderBy = " order by display_order";// elements listed in
													// whatever order you like
													// Fingers crossed fix for selecting ordered categories!
													// Below we grab the Categories out by selecting them in display_order
													// then ATTR_categoryName
													// which means if the display_order is set (not null) then it will be in
													// that order
													// but if it's not - i.e. not ordered then it'll be in order of
													// ATTR_categoryName
		if (tablePrefix.equals("Categories"))
		{
			// below code is to work around a SQLserver syntax issue with 'order
			// by'
			// it can only handle single columns in the order by when a distinct
			// is ued
			// whereas mysql doesn't care.
			String dbProvider = d.getDBProvider();
			if (dbProvider.equals("SQLSERVER"))
				orderBy = " order by ATTR_categoryName";// category elements
														// listed in
														// alphabetical
														// order
			else
			{
				// ********************* NEED to test the below and above on
				// each DB type ******************
				orderBy = " order by display_order, ATTR_categoryName";// category
																		// elements
																		// listed
																		// in
																		// alphabetical
																		// order
																		// orderBy = " order by ATTR_categoryName"; // category elements
																		// listed in alphabetical order
			}
		}
		else if (orderColumn != null)
			orderBy = " order by " + orderColumn;// elements listed in the
													// specified order

		String query = "select " + colList.toString() + " from " + tablePrefix + "_" + DatabaseValidation.encodeParam(module) + whereClause + orderBy;
		if (d.getDEBUG())
		{
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + query);
		}
		// Previously the below used the true 3rd arg to use get meta data as
		// the query is fired
		// this was a dangerous situation because concurrent usage could cause
		// unexpected meta
		// data to be returned from the
		// DatabaseTableMetaDataService.columnService line below. Usin this
		// revised mechanism
		// the meta data is explicitly re-requested which is not as optimal but
		// safer.
		//Vector v = db.select(query, cols, false, false);
		Vector v = db.selectQuery(query);


		Vector colsData = DatabaseTableMetaDataService.columnService(tablePrefix + "_" + DatabaseValidation.encodeParam(module));

		logger.debug("getColumns: rows(" + v.size() + ")\n " + query);

		Vector returnVector = new Vector(20);

		// switch vector into hash array
		for (int i = 0; i < v.size(); i++)
		{
			// System.out.println("XXXX "+i);

			Hashtable h = new Hashtable();
			String[] vals = (String[]) v.elementAt(i);
			// inner loop - through columns in a row
			for (int x = 0; x < cols.length; x++)
			{
				boolean bigCol = false;
				String[] c = (String[]) colsData.elementAt(x);
				try
				{
					int colSize = Integer.parseInt(c[2]);

					if (colSize > 255)
						bigCol = true;
				}
				catch (Exception e)
				{

				}
				String tmp = "";
				if (cols[x].startsWith("ATTR_") && bigCol == false && stringManipulation)
				{
//					tmp = StringEscapeUtils.unescapeXml(replaceCarriageReturns(vals[x]));
					tmp = replaceCarriageReturns(vals[x]);
				}
				else
					tmp = vals[x];

				h.put(cols[x], tmp);
			}
			returnVector.addElement(h);
		}

		return returnVector;
	}

	/**
	 * Tries to intelligently replace carriage returns in a text string (passed
	 * in) with <br/>
	 * tags If a carriage return already has
	 * <P>
	 * or <br>
	 * markup around it then it doesn't add an additional <br/>
	 * tag (i.e. to try and avoid double encoding)
	 * 
	 * @param value
	 * @return Encoded string version of the value argument passed in with all
	 *         carriage returns converted.
	 */
	private String replaceCarriageReturns(String value)
	{
		int s = value.indexOf("\n");
		while (s != -1)
		{
			try
			{
				String start = value.substring(0, s);
				String end = value.substring(++s);
				// if statment missing here to check length before s
				String otherCR = value.substring(s - 6, s - 2);

				/* if there are <br> around then don't insert another one */
				if (otherCR.equalsIgnoreCase("</P>") || otherCR.equalsIgnoreCase("<br>") || otherCR.matches(".<[pP]>"))
				{
					value = start + end;
				}
				else
					value = start + "<br/>" + end;

				s = value.indexOf("\n");

			}
			catch (Exception e)
			{
				e.printStackTrace();
				s = -1;
			}

		}

		return value;
	}

	/**
	 * headlineImg is a full URL to an item of media (see method 'img'). This
	 * method (using the 3 chr file extension) tries to work out what the best
	 * way of rendering the content is. So for images it uses the <img ..> tag
	 * and for avi it uses the <object ... > tah and for mov the <embed ... >
	 * etc etc. Using the other arguments you can control the size of the
	 * rendered content. You can also pass through additional markup (in the
	 * extraArgs argument) to add inside the eventual markup used. So if you
	 * have something special/specific for this rendered item then pass it
	 * through using the extraArg argument and it will be be passed through into
	 * the markup. The showControls argument is for avi/mpg/mpeg file extensions
	 * only. If set to true it renders the media player controls within the
	 * window so the user can stop, fast forward etc. If set to false the
	 * controls are not shown and the movie auto plays on rendering. This method
	 * handles the following file extensions:
	 * swf,avi,mpg,mpeg,mov,mp4,gif,jpg,jpeg,png
	 * 
	 * @param headlineImg
	 * @param width
	 * @param height
	 * @param extraArgs
	 * @param showControls
	 * @return String
	 */
	public String renderRichContent(String headlineImg, String width, String height, String extraArgs, boolean showControls)
	{
		// show video controls ?
		String controls = "0";
		String autoPlay = "<param name=\"AutoStart\" value=\"-1\"/>";
		if (showControls)
		{
			controls = "1";
			// and autoplay off
			autoPlay = "";
		}
		// deal with option width and height args
		String movWidth = "";
		String movHeight = "";
		if (StringUtils.isNotBlank(width))
		{
			movWidth = "<param name=\"MovieWindowWidth\" value=\"" + width + "\"/>";
			width = "width=\"" + width + "\"";
		}
		if (StringUtils.isNotBlank(height))
		{
			movHeight = "<param name=\"MovieWindowHeight\" value=\"" + height + "\"/>";
			height = "height=\"" + height + "\"";
		}

		// deal with rich content
		String mainContent = "";
		if (headlineImg.toLowerCase().endsWith(".swf"))
			mainContent = "<EMBED src=\"" + headlineImg + "\" quality=high TYPE=\"application/x-shockwave-flash\" " + width + " " + height + " " + extraArgs + " PLUGINSPAGE=\"http://www.macromedia.com/shockwave.download/index.cgi?P1_Prod_Version=ShockwaveFlash\"></EMBED>";
		else if (headlineImg.toLowerCase().endsWith(".avi") || headlineImg.toLowerCase().endsWith(".mpg") || headlineImg.toLowerCase().endsWith(".mpeg"))
		{
			mainContent = "<OBJECT classid=\"CLSID:05589FA1-C356-11CE-BF01-00AA0055595A\" " + width + " " + height + ">" + "<param name=\"FileName\" value=\"" + headlineImg + "\"/>" + "<param name=\"ShowDisplay\" value=\"0\"/>" + "<param name=\"ShowControls\" value=\"" + controls + "\"/>" + autoPlay + "<param name=\"PlayCount\" value=\"1\"/>" + movWidth + movHeight + "<embed src=\"" + headlineImg + "\" " + extraArgs + " " + width + " " + height + "></EMBED></OBJECT>";
		}
		else if (headlineImg.toLowerCase().endsWith(".mov") || headlineImg.toLowerCase().endsWith(".3gp"))
			mainContent = "<EMBED src=\"" + headlineImg + "\" " + extraArgs + " quality=high " + width + " " + height + "></EMBED>";
		else if (headlineImg.toLowerCase().endsWith(".mp4"))
			mainContent = "<EMBED src=\"" + headlineImg + "\" " + extraArgs + " quality=high " + width + " " + height + "></EMBED>";
		else if (headlineImg.toLowerCase().endsWith(".gif") || headlineImg.toLowerCase().endsWith(".jpg") || headlineImg.toLowerCase().endsWith(".jpeg") || headlineImg.toLowerCase().endsWith(".png") || headlineImg.toLowerCase().endsWith(".bmp"))
			mainContent = "<img src=\"" + headlineImg + "\" " + extraArgs + " " + width + " " + height + " >";
		else if (headlineImg.toLowerCase().endsWith(".wmv"))
		{
			mainContent = "<OBJECT ID=\"MediaPlayer\" " + width + " " + height + " " + "CLASSID=\"CLSID:22D6f312-B0F6-11D0-94AB-0080C74C7E95\" " + "STANDBY=\"Loading Windows Media Player components...\" " + "TYPE=\"application/x-oleobject\" " + "CODEBASE=\"http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,7,1112\"> " + "<param name=\"FileName\" value=\"" + headlineImg + "\"/>" + "<param name=\"autoStart\" value=\"True\"/>" + "<param name=\"PlayCount\" value=\"1\"/>" + "<embed type=\"application/x-mplayer2\" SRC=\"" + headlineImg + "\" NAME = \"MediaPlayer\" " + width + " " + height + "></EMBED></OBJECT>";
		}
		else
		{
			// render image by default if no extension is matched
			mainContent = "<img src=\"" + headlineImg + "\" " + extraArgs + " " + width + " " + height + " >";
		}

		return mainContent;
	}

	/**
	 * Given the path to a media file in the stores, this method pre-pends the
	 * physical stores context, creating a complete URL to the media file.
	 * 
	 * @param imgSrc
	 * @return String full URL
	 */
	public String img(String imgSrc)
	{
		if (imgSrc == null || imgSrc.equals(""))
			return "";
		// System.out.println("STORE CONTEXT"+d.getStoreContext());
		String s = "/" + d.getStoreContext() + imgSrc;

		// convert any backslahes to forward slashes
		s = s.replace('\\', '/');

		return s;
	}

	public String imgThumb(String imgSrc)
	{
		if (imgSrc == null || imgSrc.equals(""))
			return "";

		// check for a thumb
		File p = new File("/" + d.getStoreContext() + imgSrc);

		String parent = p.getParent();
		String name = p.getName();
		String s = parent + "/_thumb/" + name;

		// convert any backslahes to forward slashes
		s = s.replace('\\', '/');

		return s;
	}

	/**
	 * Given a full date in yyyy-MM-dd hh:mm:ss.SSS format, this method will
	 * convert it to the supplied format in the formatStr argument Therefore
	 * formatDate(dateFromDB, "dd/MM/yyyy"); will convert a db date to the more
	 * commonly used display version.
	 * 
	 * @param fullDate
	 * @param formatStr
	 * @return
	 */
	public String formatDate(String fullDate, String formatStr)
	{
		// Need to extend this sometime so it handles real date formatting.
		// Date d = new GregorianCalendar()
		// SimpleDateFormat sdf = new SimpleDateFormat(formatStr);
		// System.out.println("full DATE "+fullDate);
		if (fullDate == null || fullDate.equals(""))
			return ("");

		DateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

		SimpleDateFormat screenFormat = new SimpleDateFormat(formatStr);// can
																		// place
																		// format
																		// string
																		// here
		Date currentDate = null;
		try
		{
			currentDate = dbDateFormat.parse(fullDate);
			// System.out.println("current DATE "+currentDate.toString());
		}
		catch (ParseException pe)
		{
			System.out.println("DB date format incorrect");
		}
		String startDate = screenFormat.format(currentDate);
		// System.out.println("DATE "+startDate);
		return (startDate);
	}

	/**
	 * Given a real number (as a string) this method converts it to have a set
	 * number of digits on either side of the decimal point, both being
	 * seperately configurable.
	 * 
	 * @param fullNumber
	 * @param maxIntegerDigits
	 * @param maxFractionDigits
	 * @return
	 */
	public String formatNumber(String fullNumber, int maxIntegerDigits, int maxFractionDigits)
	{
		return (formatNumber(fullNumber, maxIntegerDigits, maxFractionDigits, 0));
	}

	public String formatNumber(String fullNumber, int maxIntegerDigits, int maxFractionDigits, int minFractionDigits)
	{
		if (fullNumber == null || fullNumber.equals(""))
			return ("");

		NumberFormat dbNumberFormat = NumberFormat.getNumberInstance();
		Number theNumber;
		try
		{
			theNumber = dbNumberFormat.parse(fullNumber);
		}
		catch (java.text.ParseException jtpe)
		{
			jtpe.printStackTrace();
			return fullNumber;
		}

		if (minFractionDigits != 0)
			dbNumberFormat.setMinimumFractionDigits(minFractionDigits);

		dbNumberFormat.setMaximumIntegerDigits(maxIntegerDigits);

		dbNumberFormat.setMaximumFractionDigits(maxFractionDigits);

		StringBuffer theBuffer = new StringBuffer();

		dbNumberFormat.format(theNumber, theBuffer, new FieldPosition(NumberFormat.FRACTION_FIELD));

		return (theBuffer.toString());
	}

	public final String sq(String str)
	{
		String SQ = "'";
		if (this.d.getDEBUG())
		{
			logger.debug(str);
		}

		String token = null;
		StringBuffer returnString = new StringBuffer();

		if (str != null)
		{
			StringTokenizer st = new StringTokenizer(str, SQ, true);
			while (st.hasMoreTokens())
			{
				token = st.nextToken();
				if (token.equals(SQ))
				{
					returnString.append(token);
					returnString.append(SQ);
				}
				else
				{
					returnString.append(token);
				}
			}
		}

		return returnString.toString();
	}

	public String parseValue(String value)
	{
		java.util.StringTokenizer str = new java.util.StringTokenizer(value, "|");
		StringBuffer sb = new StringBuffer();
		while (str.hasMoreTokens())
		{
			String tokenItem = str.nextToken();
			String tk = "<option value='" + tokenItem;
			sb.append(tk).append("'>" + tokenItem + "</option>");

		}
		return sb.toString();
	}

}
