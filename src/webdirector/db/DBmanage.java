package webdirector.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;

/**
 * Created by IntelliJ IDEA. User: Administrator Date: 19/06/2005 Time: 17:27:10
 * To change this template use File | Settings | File Templates.
 * 
 * This class is used to manipulate the core database itself. For example
 * removing columns, adding colunms.
 */
public class DBmanage extends DBaccess
{
	static Logger logger = Logger.getLogger(DBmanage.class);

	private String DBProvider = d.getDBProvider();
	private int OK = 0;

	public DBmanage()
	{
		if (DBProvider.equals("MYSQL"))
		{
			// OK = 1;
			// why should we set this to 1???
			OK = 0;
		}
	}

	public boolean createTable(String tableName, HashMap columns)
	{
		String sql = "CREATE TABLE " + tableName + " ( ";

		// process columns
		ArrayList alist = new ArrayList(columns.keySet());

		for (int i = 0; i < alist.size(); i++)
		{
			String key = (String) alist.get(i);
			DBcolumn col = (DBcolumn) columns.get(key);

			String name = col.getColumnName();
			String type = col.getColumnType();
			int identitySeed = col.getColumnIdentitySeed();
			int identityInc = col.getColumnIdentityInc();

			sql += name + " " + type + " ";
			if (col.isColumnIsNull())
				sql += "NULL ";
			else
				sql += "NOT NULL ";
			if (col.isColumnIsIdentity())
				sql += "PRIMARY KEY ";
			if (i + 1 != alist.size())
				sql += ", ";
		}
		sql += ")";

		// -99 is failure
		int retVal = updateData(sql);

		logger.info("Creating table\n" + sql + "\nreturned " + retVal);

		if (retVal == -99)
			return false;
		else
			return true;
	}

	public boolean doesTableExist(String tableName)
	{
		String query = "show tables like '" + tableName + "'";
		//Vector rows = select(query);
		Vector rows = selectQuerySingleCol(query);
		if (rows != null && rows.size() > 0)
			return true;
		else
			return false;
	}

	public boolean doesColumnExist(String colName, String tableName)
	{
		String query = "Select " + colName + " from " + tableName;

		Connection conn = null;
		Statement stmt = null; // Or PreparedStatement if needed
		ResultSet rs = null;
		Vector v = new Vector();
		String coloumns[] = null;
		boolean return_flag = false;
		//v = select(query);
		v = selectQuerySingleCol(query);
		if (v.size() > 0)
			return_flag = true;

		return return_flag;
	}

	public boolean removeColumn(String colToRemove, String tableName, String labelTableName)
	{
		// remove this column from the table
		String type = tableName.toLowerCase().startsWith("categories") ? "category" : "element";
		String module = tableName.substring(tableName.indexOf("_") + 1);
		// do not allow main column to be removed.
		if (colToRemove.equals("ATTR_Headline") || colToRemove.equals("ATTR_categoryName"))
			return false;
		long start = System.currentTimeMillis();
		// first delete main element table reference
		int rowsEffectedMainTable = updateData("ALTER TABLE " + tableName + " DROP COLUMN " + colToRemove);
		updateData("ALTER TABLE " + tableName + "_draft DROP COLUMN " + colToRemove);
		updateData("ALTER TABLE " + tableName + "_ver DROP COLUMN " + colToRemove);
		logger.info("alter table done in " + (System.currentTimeMillis() - start) + " ms");

		// now remove labels table entry also
		start = System.currentTimeMillis();
		int rowsEffectedLabelsTabel = updateData("delete from " + labelTableName + " where Label_InternalName = ?", new Object[] { colToRemove });
		logger.info("delete from labels done in " + (System.currentTimeMillis() - start) + " ms");

		// Delete Drop down attributes data from dropdown tables.
		if (colToRemove.startsWith("ATTRDROP"))
		{
			start = System.currentTimeMillis();
			updateData("delete from  drop_down where MODULE_NAME = ? and ATTRDROP_NAME = ? and MODULE_TYPE = ?", new Object[] { module, colToRemove, type });
			logger.info("delete from drop_down done in " + (System.currentTimeMillis() - start) + " ms");
		}

		// Delete MultiDrop attributes data from drop down tables, and values in
		// multi_selections table.
		if (colToRemove.startsWith("ATTRMULTI"))
		{
			start = System.currentTimeMillis();
			updateData("delete from  drop_down where MODULE_NAME = ? and ATTRDROP_NAME = ? and MODULE_TYPE = ?", new Object[] { module, colToRemove, type });
			logger.info("delete from drop_down done in " + (System.currentTimeMillis() - start) + " ms");
			start = System.currentTimeMillis();
			updateData("delete from  drop_down_multi_selections where MODULE_NAME = ? and ATTRMULTI_NAME = ? and MODULE_TYPE = ?", new Object[] { module, colToRemove, type });
			updateData("delete from  drop_down_multi_selections_draft where MODULE_NAME = ? and ATTRMULTI_NAME = ? and MODULE_TYPE = ?", new Object[] { module, colToRemove, type });
			updateData("delete from  drop_down_multi_selections_ver where MODULE_NAME = ? and ATTRMULTI_NAME = ? and MODULE_TYPE = ?", new Object[] { module, colToRemove, type });
			logger.info("delete from drop_down_multi_selections done in " + (System.currentTimeMillis() - start) + " ms");
		}

		DatabaseTableMetaDataService dtmts = new DatabaseTableMetaDataService();
		if (dtmts.isTableInCache(tableName))
		{
			start = System.currentTimeMillis();
			dtmts.clearCache();
			logger.info("clearCache done in " + (System.currentTimeMillis() - start) + " ms");
		}
		if (rowsEffectedMainTable >= OK && rowsEffectedLabelsTabel == 1)
			return true;
		else
			return false;
	}

	public String addNewColumn(String colName, String colType, String tableName, String labelTableName, String colSize)
	{
		// name is empty
		if (StringUtils.isBlank(colName))
		{
			return "Add Column: No column NAME supplied, please try again.";
		}
		// type is null or empty
		if (StringUtils.isBlank(colType))
		{
			return "Add Column: No column TYPE supplied, please try again.";
		}
		// type is varchar and size is not a valid integer
		if (StringUtils.equalsIgnoreCase(colName, "varchar"))
		{
			try
			{
				if (Integer.parseInt(colSize) > 9999)
				{
					return "Add Column: Column SIZE cannot be greater than 9999, please try again.";
				}
			}
			catch (Exception e)
			{
				return "Add Column: Column SIZE is invalid, please try again.";
			}
		}
		// type is char and size is not a valid integer
		if (StringUtils.equalsIgnoreCase(colName, "char"))
		{
			try
			{
				if (Integer.parseInt(colSize) > 255)
				{
					return "Add Column: Column SIZE cannot be greater than 255, please try again.";
				}
			}
			catch (Exception e)
			{
				return "Add Column: Column SIZE is invalid, please try again.";
			}
		}
		// type is char and size is not a valid integer
		if (StringUtils.equalsIgnoreCase(colName, "decimal"))
		{
			try
			{
				String[] arr = StringUtils.split(colSize, ",");
				if (arr.length != 2)
				{
					return "Add Column: Column SIZE is invalid, please try again.";
				}
				int m = Integer.parseInt(arr[0].trim());
				int d = Integer.parseInt(arr[1].trim());
				if (m < d)
				{
					return "Add Column: Column SIZE is invalid. For decimal(M,D), M must be >= D. please try again.";
				}
			}
			// decimal(M,D), M must be >= D
			catch (Exception e)
			{
				return "Add Column: Column SIZE is invalid, please try again.";
			}
		}
		// name already starts with ATTR... ?
		if (colName.startsWith("ATTR"))
		{
			return "Add Column: Column NAME cannot start with ATTR..., please rename the column.";
		}

		// produce the column name
		String newColName = getFullColName(colName, colType);
		// produce the column type
		String newColType = getColType(colType, colSize);

		// what happens if you try and add an existing column name ?
		String query = "select Label_InternalName from " + labelTableName + " where Label_InternalName=? ";
		//Vector rows = select(query);
		Vector rows = selectQuerySingleCol(query, new String[] { newColName });

		if (rows.size() != 0)
		{
			return "Add Column: This column name (" + colName + ") already exists, please rename and try again. (" + rows.size() + ")";
		}

		// ok all well, lets add it, first the main table
		int rowsEffectedMainTable = updateData("ALTER TABLE " + tableName + " ADD " + DatabaseValidation.encodeParam(newColName) + " " + newColType);
		updateData("ALTER TABLE " + tableName + "_draft ADD " + DatabaseValidation.encodeParam(newColName) + " " + newColType);
		updateData("ALTER TABLE " + tableName + "_ver ADD " + DatabaseValidation.encodeParam(newColName) + " " + newColType);
		// set the column to something useful
		long start = System.currentTimeMillis();
		updateData("update " + tableName + " set " + newColName + " = " + getColTypeDefault(colType));
		updateData("update " + tableName + "_draft set " + newColName + " = " + getColTypeDefault(colType));
		updateData("update " + tableName + "_ver set " + newColName + " = " + getColTypeDefault(colType));
		//updateData("update " + tableName + " set " + newColName + " = ?" , new Object[]{getColTypeDefault(colType)});
		logger.info("Update with default value ended in " + (System.currentTimeMillis() - start) + " ms");
		int rowsEffectedLabelsTabel = 0;
		String key = "";
		// and then the labels table but only if the above alter table sql has
		// worked
		// the above sql will return -99 if an exception is thrown, 0 otherwise.
		if (rowsEffectedMainTable != -99)
		{
			query = "select case isnull(max(Label_id)) when 1 then 1 else max(Label_id)+1 end from " + labelTableName;

			//rows = select(query);
			rows = selectQuerySingleCol(query);

			key = (String) rows.elementAt(0);
			String labelTableFields = "Label_InternalName, Label_id, Label_ExternalName, Label_Mandatory, Label_OnOff, Label_Info, display_order, Label_TabName, Label_LoginRequired, Label_Unique, Label_DefaultValue, Label_Index, Label_rank, Label_tokenized, Label_Export";
			String extraArg = ", 0, 1, '', 99, '', 0, 0, '', 0, 0, 0 , 0";
			if (labelTableName.startsWith("Category"))
			{
				extraArg = ", 0, 1, '', 99, '', 0, 0, '', 0, 0, 0 , 0";
				labelTableFields = "Label_InternalName, Label_id, Label_ExternalName, Label_Mandatory, Label_OnOff, Label_Info, display_order, Label_TabName, Label_LoginRequired, Label_Unique, Label_DefaultValue, Label_Index, Label_rank, Label_tokenized, Attribute_Level";
			}

			String tableDisplayName = WordUtils.capitalizeFully(StringUtils.join(StringUtils.splitByCharacterTypeCamelCase(colName), " "));
			String sql = "insert into " + labelTableName + " (" + labelTableFields + ") values ('" + newColName + "', " + key + ", " + "'" + tableDisplayName + "'" + extraArg + ")";
			logger.info(" SQL " + sql);
			rowsEffectedLabelsTabel = updateData(sql);
		}

		if (rowsEffectedMainTable >= OK && rowsEffectedLabelsTabel == 1)
			return "Column " + newColName + " has been successfully added.";
		else
			return "ERROR: Please contact CI as this error could cause database inconsistencies and needs addressing immediately. (" + newColName + "," + newColType + "," + rowsEffectedMainTable + "," + rowsEffectedLabelsTabel + ")";
	}

	// this is method to add new label columns and used for sorting of
	// webdirector elements as well as categories for dynamic form builder jsp
	public String addNewLabelColumn(String colName, String labelTableName, String colType)
	{
		// what happens if you try and add an existing column name ?
		String query = "select " + colName + " from " + labelTableName;
		//Vector rows = select(query);
		Vector rows = selectQuerySingleCol(query);
		if (rows.size() != 0)
		{
			return "Add Column: This column name (" + colName + ") already exists, please rename and try again. (" + rows.size() + ")";
		}

		// ok alls well, lets add it, first the main table
		int rowsEffectedLabelTable = updateData("ALTER TABLE " + labelTableName + " ADD " + DatabaseValidation.encodeParam(colName) + " " + DatabaseValidation.encodeParam(colType));
		updateData("update " + labelTableName + " set " + DatabaseValidation.encodeParam(colName) + " = NULL");

		System.out.println("rows affected label Table" + rowsEffectedLabelTable);
		if (rowsEffectedLabelTable >= OK)
			return "Column " + colName + " has been successfully added.";
		else
			return "ERROR: Please contact CI as this error could cause database inconsistencies and needs addressing immediately. (" + colName + "," + colType + "," + rowsEffectedLabelTable + ")";
	}

	public String getFullColName(String colName, String colType)
	{
		String newColName = "";

		if (colType.equals("Datetime"))
			newColName = "ATTRDATE_" + colName;
		else if (colType.equals("Integer"))
			newColName = "ATTRINTEGER_" + colName;
		else if (colType.equals("Float"))
			newColName = "ATTRFLOAT_" + colName;
		else if (colType.equals("Money"))
			newColName = "ATTRCURRENCY_" + colName;
		else if (colType.equals("Decimal"))
			newColName = "ATTRDECIMAL_" + colName;
		else if (colType.equals("DropDown"))
			newColName = "ATTRDROP_" + colName;
		else if (colType.equals("DropDownMulti"))
			newColName = "ATTRMULTI_" + colName;
		else if (colType.equals("Checkbox"))
			newColName = "ATTRCHECK_" + colName;
		else if (colType.equalsIgnoreCase("File"))
			newColName = "ATTRFILE_" + colName;
		else if (colType.startsWith("char"))
			newColName = "ATTR_" + colName;
		else if (colType.equals("varchar"))
			newColName = "ATTR_" + colName;
		else if (colType.equals("Unlimited"))
			newColName = "ATTRLONG_" + colName;
		else if (colType.equals("TextOnly"))
			newColName = "ATTRTEXT_" + colName;
		else if (colType.equals("Notes"))
			newColName = "ATTRNOTES_" + colName;
		else if (colType.equals("password"))
			newColName = "ATTRPASS_" + colName;
		else if (colType.equals("SharedFile"))
			newColName = "ATTRSHAREDFILE_" + colName;
		return newColName;
	}

	private String getColTypeDefault(String colType)
	{
		String colTypeDefault = "";

		// special case
		if (colType.equals("Integer") || colType.equals("Checkbox") || colType.equals("Notes"))
			colTypeDefault = " 0";
		else if (colType.equals("Money") || colType.equals("Float"))
			colTypeDefault = " 0.00";
		else if (colType.equals("Decimal"))
			colTypeDefault = " 0.00";
		else if (colType.equals("Datetime"))
		{
			if (DBProvider.equals("MYSQL"))
				colTypeDefault = " '1900-01-01 00:00:00'";
			else
				colTypeDefault = " 1900-01-01 00:00:00";
		}
		else
			colTypeDefault = "''";

		return colTypeDefault;
	}

	private String getColType(String colType, String colSize)
	{
		// if SQLSERVER then the 'money' type is money, if MYSQL then use a
		// 'decimal'
		String moneyType = "money";
		if (DBProvider.equals("MYSQL"))
			moneyType = "decimal(19,2) default 0.00";

		String newColType = "";

		if (colType.equals("Datetime"))
			newColType = "datetime";
		else if (colType.equals("Integer"))
			newColType = "int";
		else if (colType.equals("Float"))
			newColType = "float";
		else if (colType.equals("Money"))
			newColType = moneyType;
		else if (colType.equals("Decimal"))
			newColType = "decimal(" + colSize + ")";
		else if (colType.equals("DropDown"))
			newColType = "char(100)";
		else if (colType.equals("DropDownMulti"))
			newColType = "char(1)";
		else if (colType.equals("Checkbox"))
			newColType = "int(1)";
		else if (colType.equalsIgnoreCase("File"))
			newColType = "varchar(500)";
		else if (colType.equals("char20"))
			newColType = "char(20)";
		else if (colType.equals("char50"))
			newColType = "char(50)";
		else if (colType.equals("char100"))
			newColType = "char(100)";
		else if (colType.equals("char255"))
			newColType = "char(255)";
		else if (colType.equals("varchar"))
			newColType = "varchar(" + colSize + ")";
		else if (colType.equals("Notes"))
			newColType = "char(255)";
		else if (colType.equals("Unlimited"))
			newColType = "varchar(500)";
		else if (colType.equals("TextOnly"))
			newColType = "varchar(500)";
		else if (colType.equals("password"))
			newColType = "char(50)";
		else if (colType.equals("SharedFile"))
			newColType = "varchar(500)";

		return newColType;
	}

	public boolean indexExist(String colName, String tableName, String indexType)
	{
		logger.info("=======================================");
		String sql = "show index from " + tableName;
		String indexName = getIndexName(colName, tableName, indexType);
		logger.info(sql);
		List<Hashtable> results = DataAccess.getInstance().select(sql, new HashtableMapper());
		for (Hashtable index : results)
		{
			String keyName = (String) index.get("Key_name");
			if (indexName.equalsIgnoreCase(keyName))
			{
				logger.info("return true");
				logger.info("=======================================");
				return true;
			}
		}
		logger.info("return false");
		logger.info("=======================================");
		return false;
	}

	public boolean addIndex(String colName, String tableName, String indexType)
	{
		logger.info("=======================================");
		String sql = "ALTER TABLE " + tableName + " ADD " + indexType + " INDEX " + getIndexName(colName, tableName, indexType) + " (" + colName + ")";
		logger.info(sql);
		int rowsEffectedLabelTable = updateData(sql);
		logger.info("result = " + rowsEffectedLabelTable);
		logger.info("=======================================");
		return rowsEffectedLabelTable > -1;
	}

	public boolean removeIndex(String colName, String tableName, String indexType)
	{
		logger.info("=======================================");
		String sql = "ALTER TABLE " + tableName + " DROP INDEX " + getIndexName(colName, tableName, indexType);
		logger.info(sql);
		int rowsEffectedLabelTable = updateData(sql);
		logger.info("result = " + rowsEffectedLabelTable);
		logger.info("=======================================");
		return rowsEffectedLabelTable > -1;
	}

	private String getIndexName(String colName, String tableName, String indexType)
	{
		String indexName = indexType + "INDEX_" + tableName + "_" + colName;
		logger.info("IndexName(" + colName + "," + tableName + "," + indexType + ") = " + indexName);
		return indexName;
	}

	public boolean setDefaultValue(String colName, String tableName, String value)
	{
		logger.info("=======================================");
		String sql = "";
		Object[] args = new Object[1];

		if (value == null || value.equals(""))
		{
			sql = "ALTER TABLE " + tableName + " ALTER " + colName + " DROP DEFAULT";
			args = new Object[0];
		}
		else
		{
			sql = "ALTER TABLE " + tableName + " ALTER " + colName + " SET DEFAULT ?";
			args[0] = value;
		}
		logger.info(sql);
		int rowsEffectedLabelTable = updateData(sql, args);
		logger.info("result = " + rowsEffectedLabelTable);
		logger.info("=======================================");
		return rowsEffectedLabelTable > -1;
	}
}
