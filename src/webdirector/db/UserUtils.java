package webdirector.db;

import java.sql.SQLException;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.admin.modules.DynamicModuleController;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.base.db.DataMap;
import au.net.webdirector.common.datalayer.base.db.TransactionDataAccess;
import au.net.webdirector.common.datalayer.base.transaction.TransactionManager;
import au.net.webdirector.common.utils.password.PasswordUtils;

public class UserUtils
{
	private static Logger logger = Logger.getLogger(UserUtils.class);

	private DBaccess db = new DBaccess();

	/**
	 * Checks passwords match and then updates them for this user.
	 * 
	 * @param user
	 *            user name in Users table
	 * @param oldPass
	 * @param newPass
	 * @param newPass1
	 * @return String message on how the operation went, "worked ok" or
	 *         "Could not find user" or "ERROR ... " Etc etc.
	 */
	public String updateAdminPasswordDetails(String userId, String oldPass, String newPass, String newPass1)
	{
		String query = "";
		int nameRows = 0;

		if (newPass != null && !newPass.equals(""))
		{
			if (!newPass.equals(newPass1))
			{
				return ("Passwords do not match!");
			}
			else if (oldPass.equals(newPass))
			{
				return ("New Password cannot be the same as the old password.");
			}
			else
			{
				// first check old password
				query = "select password from users where user_id = ?";
				System.out.println("QUERY: " + query);
				//Vector v = select(query);
				Vector v = db.selectQuerySingleCol(query, new String[] { userId });
				if (v == null || v.size() == 0)
				{
					return ("ERROR: Could not verify old Password");
				}
				if (!v.get(0).equals(PasswordUtils.encrypt(oldPass)))
				{
					return "Current password does not match.";
				}

				// now change it
				query = "update users set password = ? where user_id = ?";
				nameRows = db.updateData(query, new Object[] { PasswordUtils.encrypt(newPass), userId });
				System.out.println("QUERY: " + query + " = " + nameRows);

				if (nameRows != 1)
					return ("ERROR: The Password could not be updated");
			}
			return ("Password updated OK");
		}
		return ("You have not entered a password");
	}

	/**
	 * Allows you to change the username
	 * 
	 * @param oldName
	 * @param newName
	 * @return
	 */
	public String updateAdminLoginDetails(String oldName, String newName)
	{
		String query = "";
		int nameRows = 0;

		if (newName != null && !newName.equals(""))
		{
			query = "update users set user_name = ? where user_name = ? and UserLevel_id = 1";
			nameRows = db.updateData(query, new Object[] { newName, oldName });
			System.out.println("QUERY: " + query + " = " + nameRows);

			if (nameRows == 1)
				return ("User-Name updated OK");
			else
				return ("ERROR: The User-Name could not be updated");
		}
		return ("");
	}

	/**
	 * returns all users in a vector Vector is a String[] containing
	 * "User_id","Name","Company","user_name","UserLevel_name"
	 * 
	 * @return
	 */
	public Vector getAllUsers()
	{
		// get all User details
		String query = "select u.User_id, u.Name, u.Company, u.user_name, u.UserLevel_id from users u order by u.Name";
		System.out.println("QUERY: " + query);
		Vector v = db.selectQuery(query);// TODO - validated

		return v;
	}

	public boolean addUser(String userName, String name, String company, String email, String phone, String password, String[] moduleSelected, String administrator, String secureUser, String moduleUser, String readOnlyUser, String defaultLayout, String accessFileManager, String canChangePassword, String canEditAccount)
	{
		TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false));
		try
		{
			UserModel user = new UserModel(userName, name, company, email, phone, password, moduleSelected, administrator, secureUser, moduleUser, readOnlyUser, defaultLayout, accessFileManager, canChangePassword, canEditAccount);
			return addUser(user, txManager) > 0;
		}
		catch (Exception e)
		{
			txManager.rollback();
			return false;
		}
		finally
		{
			txManager.commit();
		}
	}

	/**
	 * Add new user.
	 * 
	 * @param userName
	 * @param name
	 * @param company
	 * @param email
	 * @param phone
	 * @param password
	 * @param moduleSelected
	 * @param administrator
	 * @param secureUser
	 * @param moduleUser
	 * @return
	 * @throws Exception
	 */
	public int addUser(UserModel user, TransactionManager txManager) throws SQLException
	{
		String admin = "3";

		if (user.administrator == null)
		{
			System.out.println("admin is null");
		}
		else if (user.administrator.equals("on"))
		{
			admin = "1";
			System.out.println("admin " + user.administrator);
		}

		if (user.secureUser == null)
		{
			System.out.println("secure is null");
		}
		else if (user.secureUser.equals("on"))
		{
			admin = "2";
			System.out.println("secure " + user.secureUser);
		}

		if (user.moduleUser == null)
		{
			System.out.println("module is null");
		}
		else if (user.moduleUser.equals("on"))
		{
			admin = "4";
			System.out.println("module " + user.moduleUser);
		}

		if (user.readOnlyUser == null)
		{
			System.out.println("module is null");
		}
		else if (user.readOnlyUser.equals("on"))
		{
			admin = "5";
			System.out.println("module " + user.readOnlyUser);
		}

		DataMap values = new DataMap();
		values.put("user_name", user.userName);
		values.put("name", user.name);
		values.put("company", user.company);
		values.put("email", user.email);
		values.put("phone", user.phone);
		values.put("password", user.password);
		values.put("UserLevel_id", admin);
		values.put("default_layout", user.defaultLayout);
		values.put("can_change_password", user.canChangePassword);
		values.put("can_edit_account", user.canEditAccount);

		int userId = txManager.getDataAccess().insert(values, "users");
		if (userId > 0)
		{
			DynamicModuleController.sharedInstance().assignModulesToUser(user.moduleSelected, Integer.toString(userId), false, txManager);
		}
		return userId;
	}

	/**
	 * deletes a user. form the user table.
	 * 
	 * @param userID
	 * @return
	 */
	public boolean deleteUser(String userID)
	{
		String query1 = "delete from user_module_privileges where user_id = ?";
		db.updateData(query1, new Object[] { userID });
		String query = "delete from users where user_id = ?";
		int rows = db.updateData(query, new Object[] { userID });

		if (rows == 1)
			return true;
		else
			return false;
	}

	public boolean editUser(String userID, String userName, String name, String company, String email, String phone, String password, String[] moduleSelected, String administrator, String secureUser, String moduleUser, String readOnlyUser, String defaultLayout, String accessFileManager, String canChangePassword, String canEditAccount)
	{
		TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false));
		try
		{
			UserModel user = new UserModel(userName, name, company, email, phone, password, moduleSelected, administrator, secureUser, moduleUser, readOnlyUser, defaultLayout, accessFileManager, canChangePassword, canEditAccount);
			user.userID = userID;
			return editUser(user, txManager);
		}
		catch (Exception e)
		{
			txManager.rollback();
			return false;
		}
		finally
		{
			txManager.commit();
		}
	}

	/**
	 * Allows you to change any of the user details.
	 * 
	 * @param userID
	 * @param userName
	 * @param name
	 * @param company
	 * @param email
	 * @param phone
	 * @param password
	 * @param moduleSelected
	 * @param administrator
	 * @param secureUser
	 * @param moduleUser
	 * @return
	 * @throws SQLException
	 */
	public boolean editUser(UserModel user, TransactionManager txManager) throws SQLException
	{
		String admin = "3";

		if (user.administrator == null)
		{
			System.out.println("admin is null");
		}
		else if (user.administrator.equals("on"))
		{
			admin = "1";
			System.out.println("admin " + user.administrator);
		}

		if (user.secureUser == null)
		{
			System.out.println("secure is null");
		}
		else if (user.secureUser.equals("on"))
		{
			admin = "2";
			System.out.println("secure " + user.secureUser);
		}

		if (user.moduleUser == null)
		{
			System.out.println("module is null");
		}
		else if (user.moduleUser.equals("on"))
		{
			admin = "4";
			System.out.println("module " + user.moduleUser);
		}

		if (user.readOnlyUser == null)
		{
			System.out.println("module is null");
		}
		else if (user.readOnlyUser.equals("on"))
		{
			admin = "5";
			System.out.println("module " + user.readOnlyUser);
		}

		DataMap values = new DataMap();
		// values.put("user_name", user.userName);
		values.put("name", user.name);
		values.put("company", user.company);
		values.put("email", user.email);
		values.put("phone", user.phone);
		values.put("UserLevel_id", admin);
		values.put("default_layout", user.defaultLayout);
		values.put("can_change_password", user.canChangePassword);
		values.put("can_edit_account", user.canEditAccount);

		if (!StringUtils.equals(user.password, PasswordUtils.encrypt(PasswordUtils.FAKE_PASSWD)))
		{
			values.put("password", user.password);
		}

		DataMap condition = new DataMap();
		condition.put("user_id", user.userID);

		int rows = txManager.getDataAccess().update(values, "users", condition);

		DynamicModuleController.sharedInstance().assignModulesToUser(user.moduleSelected, user.userID, true, txManager);

		if (rows == 1)
			return true;
		else
			return false;
	}

	/**
	 * retrieves the user details.
	 * 
	 * @param userID
	 * @return single entry Vector which contains a String[] of the following
	 *         values: "Name","Company","email","phone","user_name", "password",
	 *         "UserLevel_id", "Modules"
	 */
	public Vector getUser(String userID) {
		// get all User details
		String query = "select u.Name, u.Company, u.email, u.phone, u.user_name, u.password, u.UserLevel_id, u.default_layout, u.access_file_manager from users u where u.User_id = ?";
		//Vector v = select(query, cols);
		Vector v = db.selectQuery(query, new String[] { userID });
		String[] vals = (String[]) v.elementAt(0);

		return v;
	}

	public DataMap getUserDataMapById(String userID)
	{
		try
		{
			return TransactionDataAccess.getInstance().selectMap("select * from users where user_id = ?", userID);
		}
		catch (SQLException e)
		{
			logger.error("Error:", e);
			return null;
		}
	}

	/**
	 * Retrieve user details by username or email
	 * 
	 * @param usernameOrEmail
	 * @return
	 */
	public Vector getUserByUsernameOrEmail(String usernameOrEmail)
	{

		String query = "select u.user_id, u.Name, u.email, u.user_name, u.password from users u where u.user_name = ? or u.email= ?";

		String[] cols = { "Id", "Name", "email", "user_name", "password" };
		//Vector v = select(query, cols);
		Vector v = db.selectQuery(query, new String[] { usernameOrEmail, usernameOrEmail });

		if ((v == null) || (v.size() == 0))
		{
			return null;
		}

		return v;

	}

	public String[] getUserByEmail(String email)
	{
		String query = "select u.user_id, u.Name, u.email, u.user_name, u.password from users u where u.email= ?";
		Vector v = db.selectQuery(query, new String[] { email });
		if ((v == null) || (v.size() == 0))
		{
			return null;
		}
		return (String[]) v.get(0);

	}

	public String[] getUserByUsername(String username)
	{
		String query = "select u.user_id, u.Name, u.email, u.user_name, u.password from users u where u.user_name = ?";
		Vector v = db.selectQuery(query, new String[] { username });

		if ((v == null) || (v.size() == 0))
		{
			return null;
		}
		return (String[]) v.get(0);
	}

	/**
	 * Retrieve user by id and password.
	 * This function is used to retrieve user details from a unique link
	 * 
	 * @param id
	 * @param password
	 * @return
	 */
	public Vector getUserByIdAndPassword(String id, String password)
	{
		String query = "select u.Name, u.user_name from users u " + "where u.user_id = ? AND u.password = ?";

		String[] cols = { "Name", "user_name" };
		//Vector v = select(query, cols);
		Vector v = db.selectQuery(query, new String[] { id, password });

		if ((v == null) || (v.size() == 0))
		{
			return null;
		}

		return v;
	}

	public int getUserAccessLevel(String userName)
	{

		// get all User details
		String query = "select u.UserLevel_id from users u where user_name = ?";
		//Vector v = select(query);
		Vector v = db.selectQuerySingleCol(query, new String[] { userName });

		if (v == null || v.size() != 1)
			return USER_DOES_NOT_EXIST;
		else
			return Integer.valueOf((String) v.elementAt(0));

	}

	public static int USER_DOES_NOT_EXIST = 0;
	public static int ADMIN_USER = 1;
	public static int SECURE_USER = 2;
	public static int SUSPENDED_USER = 3;
	public static int MODULE_USER = 4;
	public static int READONLY_USER = 5;

	/**
	 * @param who
	 * @return
	 */
	public String getUsernameByID(String userID)
	{
		// get all User details
		String query = "select user_name from users u where u.user_id = ?";
		//Vector v = select(query);
		Vector v = db.selectQuerySingleCol(query, new String[] { userID });

		if (v == null || v.size() != 1)
			return null;
		else
			return (String) v.elementAt(0);
	}

	public static enum UserLevel
	{
		//@formatter:off
		//        Rank      display
		ADMIN(1, "Administrator"),

		//Bucket: Creating
		AUTHOR(4, "Content Author"),

		//Bucket: Sent
		READ_ONLY(5, "Read Only");
		//@formatter:on

		private final int rank;
		private final String displayText;

		private UserLevel(int rank, String displayText)
		{
			this.rank = rank;
			this.displayText = displayText;
		}

		public int getRank()
		{
			return rank;
		}

		public String getDisplayText()
		{
			return displayText;
		}

		public static UserLevel withValue(String value)
		{
			for (UserLevel ul : UserLevel.values())
			{
				if (ul.displayText.equalsIgnoreCase(value))
				{
					return ul;
				}
			}
			return null;
		}

		public static UserLevel withRank(int rank)
		{
			for (UserLevel ul : UserLevel.values())
			{
				if (ul.rank == rank)
				{
					return ul;
				}
			}
			return null;
		}
	}

	public class UserModel
	{
		String userName = "";
		String name = "";
		String company = "";
		String email = "";
		String phone = "";
		String password = "";
		String[] moduleSelected = new String[] {};
		String administrator = "";
		String secureUser = "";
		String moduleUser = "";
		String readOnlyUser = "";
		String defaultLayout = "";
		String userID = "";
		String canChangePassword = "";
		String canEditAccount = "";


		public UserModel()
		{
		}

		public UserModel(String userName, String name, String company, String email, String phone, String password, String[] moduleSelected, String administrator, String secureUser, String moduleUser, String readOnlyUser, String defaultLayout, String accessFileManager, String canChangePassword, String canEditAccount)
		{
			this.userName = userName;
			this.name = name;
			this.company = company;
			this.email = email;
			this.phone = phone;
			this.password = password;
			this.moduleSelected = moduleSelected;
			this.administrator = administrator;
			this.secureUser = secureUser;
			this.moduleUser = moduleUser;
			this.readOnlyUser = readOnlyUser;
			this.defaultLayout = defaultLayout;
			this.canChangePassword = canChangePassword;
			this.canEditAccount = canEditAccount;
		}

		public String getUserName()
		{
			return userName;
		}

		public void setUserName(String userName)
		{
			this.userName = userName;
		}

		public String getName()
		{
			return name;
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public String getCompany()
		{
			return company;
		}

		public void setCompany(String company)
		{
			this.company = company;
		}

		public String getEmail()
		{
			return email;
		}

		public void setEmail(String email)
		{
			this.email = email;
		}

		public String getPhone()
		{
			return phone;
		}

		public void setPhone(String phone)
		{
			this.phone = phone;
		}

		public String getPassword()
		{
			return password;
		}

		public void setPassword(String password)
		{
			this.password = password;
		}

		public String[] getModuleSelected()
		{
			return moduleSelected;
		}

		public void setModuleSelected(String[] moduleSelected)
		{
			this.moduleSelected = moduleSelected;
		}

		public String getAdministrator()
		{
			return administrator;
		}

		public void setAdministrator(String administrator)
		{
			this.administrator = administrator;
		}

		public String getSecureUser()
		{
			return secureUser;
		}

		public void setSecureUser(String secureUser)
		{
			this.secureUser = secureUser;
		}

		public String getModuleUser()
		{
			return moduleUser;
		}

		public void setModuleUser(String moduleUser)
		{
			this.moduleUser = moduleUser;
		}

		public String getReadOnlyUser()
		{
			return readOnlyUser;
		}

		public void setReadOnlyUser(String readOnlyUser)
		{
			this.readOnlyUser = readOnlyUser;
		}

		public String getDefaultLayout()
		{
			return defaultLayout;
		}

		public void setDefaultLayout(String defaultLayout)
		{
			this.defaultLayout = defaultLayout;
		}

		public String getUserID()
		{
			return userID;
		}

		public void setUserID(String userID)
		{
			this.userID = userID;
		}

		public String getCanChangePassword()
		{
			return canChangePassword;
		}

		public void setCanChangePassword(String canChangePassword)
		{
			this.canChangePassword = canChangePassword;
		}

		public String getCanEditAccount()
		{
			return canEditAccount;
		}

		public void setCanEditAccount(String canEditAccount)
		{
			this.canEditAccount = canEditAccount;
		}


	}

	/**
	 * @param userId
	 * @param userName
	 * @param name
	 * @param company
	 * @param email
	 * @param phone
	 * @param password
	 * @param default_layout
	 * @return
	 * @throws SQLException
	 */
	public boolean updateUser(String userId, String userName, String name, String company, String email, String phone, String default_layout)
	{
		TransactionManager txManager = TransactionManager.getInstance(TransactionDataAccess.getInstance(false));

		DataMap values = new DataMap();
		// values.put("user_name", userName);
		values.put("name", name);
		values.put("company", company);
		values.put("email", email);
		values.put("phone", phone);
		values.put("default_layout", default_layout);

		DataMap condition = new DataMap();
		condition.put("user_id", userId);

		int rows = 0;
		try
		{
			rows = txManager.getDataAccess().update(values, "users", condition);
		}
		catch (SQLException e)
		{
			txManager.rollback();
		}
		finally
		{
			txManager.commit();
		}

		if (rows == 1)
			return true;
		else
			return false;
	}
}