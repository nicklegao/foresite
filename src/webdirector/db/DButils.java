package webdirector.db;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import com.oreilly.servlet.MultipartRequest;

import au.net.webdirector.admin.modules.ModuleConfiguration;
import au.net.webdirector.admin.modules.domain.Module;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import au.net.webdirector.common.search.SearchContent;
import au.net.webdirector.common.utils.password.PasswordUtils;
import webdirector.db.client.ClientDataAccess;
import webdirector.db.client.ClientFileUtils;

/**
 * Really this class is a mis-match catch all of utility method for messing with
 * the Database hence it extends DBaccess Has long since needed splitting out
 * into several sub-classes but haven't had the time and now its out of hand !
 * 
 * Sushant/Nick Avoid using DB UTILS. DO NOT DELETE. USE DBaccess Instead
 */
@Deprecated
public class DButils extends DBaccess
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9213725070272207565L;
	Logger logger = Logger.getLogger(DButils.class);
	protected String brand_id = null;
	private Vector labelsTable = new Vector(20);
	private String fullTableName = null;

	/**
	 * irrelevant constructor, this class should really be a bunch of static
	 * methods not an instantiatable object, oh well all in good time.
	 */
	public DButils()
	{
		// System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		// System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		// System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		// System.out.println("XX                  IN HERE                 XX");
		// System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		// System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		// System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
	}

	// Constructor which should be used and all code transitioned to so it does
	// not rely on global variables
	// which cause problems when concurrent queries on different tables occur.
	public DButils(String fullTableName)
	{
		this.fullTableName = fullTableName;
	}

	/**
	 * old DB locking mechanism which is unused now we use connection pooling.
	 * 
	 * @return
	 */
	public boolean getKeyUpdateLockStatus()
	{
		return keyUpdateLock;
	}

	/**
	 * old DB locking mechanism which is unused now we use connection pooling.
	 * 
	 * @return
	 */
	public boolean resetKeyUpdateLockStatus()
	{
		keyUpdateLock = false;

		return keyUpdateLock;
	}

	/**
	 * This method is called when you're switching an element to live or unlive.
	 * It does all the appropriate checks to ensure integrity of sort orders etc
	 * and updates the live flag.
	 * 
	 * @param request
	 * @param module
	 * @param assets
	 * @return
	 */
	public boolean updateDBfromForm(HttpServletRequest request, String module, String assets)
	{
		// if we're switching assets from live to unlive or vice-versa we
		// need to be careful about keeping the remaining assets correctly
		// ordered
		// without any gaps in the order (i.e. sequentially numbered) otherwise
		// the
		// reorder mechanism will fail.

		// so first we need to go through the args in the request and create
		// update statements.
		String query = "";
		for (int i = 0; i < Integer.parseInt(assets); i++)
		{
			String elementID = request.getParameter("elementID_" + i);
			String live = request.getParameter("Live_" + i);
			String live_date = request.getParameter("Live_date_" + i);
			String expire_date = request.getParameter("Expire_date_" + i);

			Object[] params = new Object[4];
			int index = 0;
			// validate
			if (live == null || live.equals(""))
				live = "0";
			else
				live = "1";
			params[index++] = live;

			if (live_date == null || live_date.equals(""))
				live_date = "";
			else
			{
				live_date = live_date.length() == "2015-05-21 12:00".length() ? live_date + ":00" : live_date;
				params[index++] = "{ts '" + live_date + "'}";
				live_date = ", live_date = ?";
			}
			if (expire_date == null || expire_date.equals(""))
				expire_date = "";
			else
			{
				expire_date = expire_date.length() == "2015-05-21 12:00".length() ? expire_date + ":00" : expire_date;
				params[index++] = "{ts '" + expire_date + "'}";
				expire_date = ", Expire_date = ?";
			}

			query = "select live from Elements_" + DatabaseValidation.encodeParam(module) + " where element_id = ?";
			// Vector v = select( query );
			Vector v = selectQuerySingleCol(query, new String[] { elementID });
			String currentLive = (String) v.elementAt(0);
			params[index++] = elementID;
			query = "update Elements_" + module + " set live = ?" + live_date + expire_date + " where element_id = ?";
			logger.error(query);
			updateData(query, params);

			if (currentLive.equals("0") && live.equals("1"))
				switchLiveOrderOn(elementID, module);
			if (currentLive.equals("1") && live.equals("0"))
				switchLiveOrderOff(elementID, module);
		}

		return true;
	}

	/**
	 * physically switches the live flag on ... should not be called directly
	 * hence it is private ... for use via updateDBfromForm
	 * 
	 * @param elementID
	 * @param module
	 */
	private void switchLiveOrderOn(String elementID, String module)
	{
		// check what the order is set to and if its blank then set the order of
		// this element to the max val.
		String query = "select display_order, category_id from Elements_" + DatabaseValidation.encodeParam(module) + " where element_id = ?";
		String[] cols = { "display_order", "category_id" };
		// Vector v = select( query, cols );
		Vector v = selectQuery(query, new String[] { elementID });

		String val = null;
		String[] results = null;
		if (v != null && v.size() != 0)
		{
			results = (String[]) v.elementAt(0);
			val = results[0];
		}

		if (val == null || val.equals("") || val.equals("0"))
		{
			query = "select max(display_order) from Elements_" + DatabaseValidation.encodeParam(module) + " where category_id = ? ";
			// v = select( query );
			v = selectQuerySingleCol(query, new String[] { results[1] });
			int max = 0;
			try
			{
				max = Integer.parseInt((String) v.elementAt(0));
			}
			catch (NumberFormatException nfe)
			{
				nfe.printStackTrace();
			}
			max++;
			query = "update Elements_" + module + " set display_order = ? where element_id = ? ";
			updateData(query, new Object[] { max, elementID });
		}
	}

	/**
	 * physically switches the live flag off ... should not be called directly
	 * hence it is private ... for use via updateDBfromForm
	 * 
	 * @param elementID
	 * @param module
	 */
	private void switchLiveOrderOff(String elementID, String module)
	{
		// find the order element was set at
		String query = "select display_order, category_id from Elements_" + DatabaseValidation.encodeParam(module) + " where element_id = ?";
		String[] cols = { "display_order", "category_id" };
		// Vector v = select( query, cols );
		Vector v = selectQuery(query, new String[] { elementID });
		String[] results = (String[]) v.elementAt(0);

		// now reduce all elements order which were above it in rank
		query = "update Elements_" + module + " set display_order = display_order-1 where display_order > ? and category_id = ?";
		logger.info(query);
		updateData(query, new Object[] { results[0], results[1] });

		// udpate element itself to order of 0 - never will be picked up whilst
		// its not live anyway
		query = "update Elements_" + module + " set display_order = 0 where element_id = ?";
		updateData(query, new Object[] { elementID });
	}

	/**
	 * will chnage live flag for asset depending upon status value supplied
	 * 
	 * @param elementID
	 *            : elementId of Asset
	 * @param module
	 *            : Module Nae
	 * @param Status
	 *            : true/false .. True to make asset live and false to make
	 *            asset non-live
	 * @return
	 */
	public boolean switchElementOnOff(String elementID, String module, boolean Status)
	{
		String live = "1";
		if (!Status)
		{
			live = "0";
		}
		String query = "update Elements_" + module + " set Live = ? where element_id = ?";
		int i = updateData(query, new Object[] { live, elementID });
		if (i > 0)
			return true;
		else
			return false;
	}

	/**
	 * will change live flag for category depending upon status value supplied
	 * 
	 * @param categoryId
	 *            : CategoryID of category
	 * @param module
	 *            : Module Nae
	 * @param Status
	 *            : true/false .. True to make asset live and false to make
	 *            asset non-live
	 * @return
	 */
	public boolean switchCategoryOnOff(String categoryId, String module, boolean Status)
	{
		String live = "1";
		if (!Status)
		{
			live = "0";
		}
		String query = "update Categories_" + module + " set  ATTRCHECK_Live = ? where category_id = ?";
		int i = updateData(query, new Object[] { live, categoryId });
		if (i > 0)
			return true;
		else
			return false;
	}

	private String getHeadline(String tableType, String tableName, String id)
	{
		String keyColumn = "ATTR_Headline";
		String keyColumn2 = "Element_id";
		if (tableType.equals("Categories"))
		{
			keyColumn = "ATTR_categoryName";
			keyColumn2 = "Category_id";
		}

		String sql = "select " + keyColumn + " from " + tableType + "_" + DatabaseValidation.encodeParam(tableName) + " where " + keyColumn2 + " = ?";
		// Vector v = select( sql );
		Vector v = selectQuerySingleCol(sql, new String[] { id });
		return (String) v.elementAt(0);
	}

	/**
	 * THis method is used to change the external label names on the dynamic
	 * form. Again used via the servlet it takes the form contents in the
	 * request extracts the various values for each label and updates the
	 * database appropriately.
	 * 
	 * @param request
	 * @param moduleName
	 */
	public boolean updateLabelsTableFromForm(HttpServletRequest request, String moduleName, String tableType)
	{
		boolean allRowsUpdatedOK = true;
		/*
		 * To change module external name and level
		 */
		try
		{
			if (null != request.getParameter("editAttrModuleLevel") && null != request.getParameter("editAttrModuleName"))
			{
				//FIXME:MOVE THIS CODE

				String newModuleName = request.getParameter("editAttrModuleName");
				String currentModuleName = (String) request.getSession().getAttribute("moduleSelectedName");
				String internalModuleName = (String) request.getSession().getAttribute("moduleSelected");
				int newModuleLevel = Integer.parseInt(request.getParameter("editAttrModuleLevel"));
				int currentModuleLevel = Integer.valueOf(((String) request.getSession().getAttribute("moduleLevels")));

				String currentModuleColor = request.getParameter("currentModuleColor") != null ? request.getParameter("currentModuleColor") : "";
				String currentModuleIcon = request.getParameter("currentModuleIcon") != null ? request.getParameter("currentModuleIcon") : "";
				String newModuleColor = request.getParameter("newModuleColor") != null ? request.getParameter("newModuleColor") : "";
				String newModuleIcon = request.getParameter("newModuleIcon") != null ? request.getParameter("newModuleIcon") : "";

				if (currentModuleLevel != newModuleLevel ||
						!newModuleName.equalsIgnoreCase(currentModuleName) ||
						!currentModuleColor.equalsIgnoreCase(newModuleColor) ||
						!currentModuleIcon.equalsIgnoreCase(newModuleIcon))
				{
					Module module = new Module();
					module = ModuleConfiguration.getInstance(request.getSession().getServletContext()).getModule(internalModuleName);
					module.setLevels(newModuleLevel);
					module.setExternalModuleName(newModuleName);
					module.setColor(newModuleColor);
					module.setIcon(newModuleIcon);
//					if (ModuleConfiguration.getInstance(request.getSession().getServletContext()).updateModule(module))
//					{
//						Defaults.getInstance().setModuleExternalNameAndLevel(internalModuleName, newModuleName, newModuleLevel);
//						request.getSession().setAttribute("moduleLevels", String.valueOf(newModuleLevel));
//						request.getSession().setAttribute("moduleSelectedName", newModuleName);
//					}
				}
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		String eleOrCat = "Category";
		String tableName = "Categories_" + moduleName;
		if (tableType.equals("Elements"))
		{
			eleOrCat = "";
			tableName = "Elements_" + moduleName;
		}

		// get label count
		String query = "select Label_id,Label_Unique,Label_Index,Label_DefaultValue  from " + eleOrCat + "labels_" + DatabaseValidation.encodeParam(moduleName) + " order by label_id";
		// Vector labels = select( query,new String[]{"","","",""} );
		Vector labels = selectQuery(query);
		DBmanage dm = new DBmanage();
		for (int x = 0; x < labels.size(); x++)
		{
			String[] label = (String[]) labels.get(x);
			String id = label[0];
			String origUnique = label[1];
			String origIndex = label[2];
			String origDefaultValue = label[3];
			StringBuffer sb = new StringBuffer(200);
			String onOff = request.getParameter("ATTRCHECK_onOff" + id);
			String mandatory = request.getParameter("ATTRCHECK_mandatory" + id);
			String unique = request.getParameter("ATTRCHECK_unique" + id);
			String index = request.getParameter("ATTRCHECK_index" + id);
			String tokenized = request.getParameter("ATTRCHECK_tokenized" + id);
			String rank = request.getParameter("ATTR_rank" + id);
			String loginRequired = request.getParameter("ATTRCHECK_loginRequired" + id);
			String internal = request.getParameter("ATTR_internal" + id);
			String external = request.getParameter("ATTR_external" + id);
			String defaultValue = request.getParameter("ATTR_defaultValue" + id);
			String info = request.getParameter("ATTR_info" + id);
			String attributeLevel = request.getParameter("ATTR_Level" + id) != null ? request.getParameter("ATTR_Level" + id) : "0";
			String exportRequired = request.getParameter("ATTRCHECK_exportRequired" + id);
			String readOnly = request.getParameter("ATTRCHECK_readOnly" + id);
			String noChange = request.getParameter("ATTRCHECK_noChange" + id);
			if (!unique.equals(origUnique))
			{
				if ("1".equals(unique) && !dm.indexExist(internal, tableName, "UNIQUE"))
				{
					if (!dm.addIndex(internal, tableName, "UNIQUE"))
					{
						unique = "0";
					}
				}
				else if ("0".equals(unique) && dm.indexExist(internal, tableName, "UNIQUE"))
				{
					if (!dm.removeIndex(internal, tableName, "UNIQUE"))
					{
						unique = "1";
					}
				}
			}
			if (!index.equals(origIndex))
			{
				if ("1".equals(index) && !dm.indexExist(internal, tableName, ""))
				{
					if (!dm.addIndex(internal, tableName, ""))
					{
						index = "0";
					}
				}
				else if ("0".equals(index) && dm.indexExist(internal, tableName, ""))
				{
					if (!dm.removeIndex(internal, tableName, ""))
					{
						index = "1";
					}
				}
			}
			if (!defaultValue.equals(origDefaultValue))
			{
				if (!dm.setDefaultValue(internal, tableName, defaultValue))
				{
					defaultValue = origDefaultValue;
				}
			}

			ArrayList<Object> args = new ArrayList<Object>();

			sb.append("update " + eleOrCat + "Labels_" + moduleName + " set ");
			sb.append("Label_onOff = ?, ");
			args.add(onOff);
			sb.append(" Label_mandatory = ?");
			args.add(mandatory);
			sb.append(", Label_Unique = ?");
			args.add(unique);
			sb.append(", Label_Index = ?");
			args.add(index);
			sb.append(", Label_tokenized = ?");
			args.add(tokenized);
			sb.append(", Label_ReadOnly = ?");
			args.add(readOnly);
			sb.append(", Label_NoChange = ?");
			args.add(noChange);
			try
			{
				sb.append(", Label_rank = ?");
				args.add(Integer.parseInt(rank));
			}
			catch (Exception e)
			{

			}
			sb.append(", Label_externalName = ?");
			args.add(external);
			sb.append(", Label_DefaultValue = ?");
			args.add(defaultValue);
			sb.append(", Label_info = ?");
			args.add(info);
			if (eleOrCat.equals(""))
			{
				sb.append(", Label_LoginRequired = ?");
				args.add(loginRequired);
				sb.append(", Label_Export = ?");
				args.add(exportRequired);
			}
			else
			{
				sb.append(", Attribute_Level = ?");
				args.add(attributeLevel);
			}

			sb.append(" where label_id = ?");
			args.add(id);


			int rows = updateData(sb.toString(), args.toArray());
			if (rows != 1)
				allRowsUpdatedOK = false;
		}

		return (allRowsUpdatedOK);
	}

	public boolean updateLongTextFile(String colName, String colValue, String module, int id, String tableType, boolean isWorkflow)
	{
		boolean longTextFileExists = true;
		boolean b = true;
		String storeDir = d.getStoreDir();

		String categories = "Categories";
		String sep1 = d.getOSTypeDirSep();
		String sep2 = d.getOSTypeDirSep();
		if (tableType.equals("Elements"))
		{
			sep1 = "";
			sep2 = "";
			categories = "";
		}

		// build filename for long text field
		String dir = "";
		String dest = "";
		if (isWorkflow)
		{
			dir = storeDir + d.getOSTypeDirSep() + "_DRAFT" + d.getOSTypeDirSep() + module + sep1 + categories + d.getOSTypeDirSep() + id + d.getOSTypeDirSep() + colName;
			dest = dir + d.getOSTypeDirSep() + colName + ".txt";
		}
		else
		{
			dir = storeDir + d.getOSTypeDirSep() + module + sep1 + categories + d.getOSTypeDirSep() + id + d.getOSTypeDirSep() + colName;
			dest = dir + d.getOSTypeDirSep() + colName + ".txt";
		}

		// does file already exist ? If not create it
		File f = new File(dest);
		if (!f.exists())
		{
			longTextFileExists = false;
			try
			{
				File d = new File(dir);
				d.mkdirs();
				f.createNewFile();
			}
			catch (Exception ex)
			{
				System.out.println(ex.getMessage());
			}
		}

		// update contents of file with text
		updateDB(tableType, id, module, colName, "/" + module + sep2 + categories + "/" + id + "/" + colName + "/" + colName + ".txt");
		ClientFileUtils cfu = new ClientFileUtils();
		try
		{
			cfu.overwriteTextContents(dest, colValue);

			// now update the search index
			if (SearchContent.luceneSearchOn())
			{
				boolean isElement = true;
				if ("Categories".equalsIgnoreCase(tableType))
					isElement = false;
				SearchContent searchContent = new SearchContent(isElement, module, String.valueOf(id));
				searchContent.updateAsset();
			}
		}
		catch (Exception ex)
		{
			System.out.println(ex.getMessage());
		}

		return b;
	}

	public boolean updateFileStores(String colName, String File, String module, int id, String tableType)
	{
		boolean b = false;

		String path = storeImageForAll(id, File, colName, module, tableType);
		System.out.println("Store Path" + path);
		// update contents of file with text
		updateDB(tableType, id, module, colName, path);

		return b;
	}

	public String storeImageForAll(int Id, String image, String columnName, String table, String tabletype)
	{
		try
		{
			System.out.println("ElementID " + Id);
			System.out.println("image " + image);
			System.out.println("columnName " + columnName);
			if (tabletype.equals("category"))
			{
				table = "Categories/" + table;
			}
			if (image != null && !"".equals(image))
			{
				String storeDirectory = d.getStoreDir();
				String inputSourceFile = storeDirectory + File.separator + "_tmp" + File.separator + image;
				String directoryPath = storeDirectory + File.separator + table + File.separator + Id + File.separator + columnName;

				System.out.println("inputSourceFile " + inputSourceFile);
				System.out.println("directoryPath " + directoryPath);

				org.apache.commons.io.FileUtils.copyFileToDirectory(new File(inputSourceFile), new File(directoryPath));
				// d.generateThumb(new File(directoryPath + File.separator +
				// image));
				String path = "/" + table + "/" + Id + "/" + columnName + "/" + image;
				return path;
			}
			return null;
		}
		catch (Exception e)
		{
			System.out.println("Exception " + e.getMessage());
			e.printStackTrace(System.out);
			return null;
		}
	}

	/**
	 * New method which deals with store files containing the LONGTEXT attribute
	 * values. These are not stored in the DB but in physical files in the
	 * stores. Works for both Elements and Categories. Once the file has been
	 * updated in the stores with its new contents the database is updated to
	 * reflect the store path name in the ATTRLONG_... attribute. This is needed
	 * so the dynamic forms and client side pages can then later find the right
	 * file, extract the large text contents and do something useful with it.
	 * When updating the physical files complete overwrites are used, i.e. no
	 * clever merge, extend, update or otherwise is attempted just a complete
	 * overwrite of all contents. Although this methods name is update... it
	 * actually works for inserts too.
	 * 
	 * @param request
	 * @param module
	 * @param id
	 * @param tableType
	 * @return
	 */
	private boolean updateLongTextFile(MultipartRequest request, String module, int id, String tableType)
	{
		boolean longTextFileExists = true;
		boolean b = true;
		String storeDir = d.getStoreDir();

		String categories = "Categories";
		String sep1 = d.getOSTypeDirSep();
		String sep2 = "/";
		if (tableType.equals("Elements"))
		{
			sep1 = "";
			sep2 = "";
			categories = "";
		}

		// process all long attributes in the request
		Enumeration e = request.getParameterNames();
		while (e.hasMoreElements())
		{
			String name = (String) e.nextElement();
			String value = request.getParameter(name);

			if (name.startsWith("ATTRLONG") || name.startsWith("ATTRTEXT"))
			{
				// build filename for long text field
				String dir = storeDir + d.getOSTypeDirSep() + module + sep1 + categories + d.getOSTypeDirSep() + id + d.getOSTypeDirSep() + name;
				String dest = dir + d.getOSTypeDirSep() + name + ".txt";

				// does file already exist ? If not create it
				File f = new File(dest);
				if (!f.exists())
				{
					longTextFileExists = false;
					try
					{
						File d = new File(dir);
						d.mkdirs();
						f.createNewFile();
					}
					catch (Exception ex)
					{
						System.out.println(ex.getMessage());
					}
				}

				// update contents of file with text
				updateDB(tableType, id, module, name, "/" + module + sep2 + categories + "/" + id + "/" + name + "/" + name + ".txt");
				ClientFileUtils cfu = new ClientFileUtils();
				try
				{
					cfu.overwriteTextContents(dest, value);
				}
				catch (Exception ex)
				{
					System.out.println(ex.getMessage());
				}
			}
		}

		return b;
	}

	/**
	 * Utility method for updating a single attribute in the database. Is
	 * actually used specifically for updating file paths in category or element
	 * tables but there is no reason you couldn't use this method for updating a
	 * single attribute in any table... however its private so using the generic
	 * updataData() method is better.
	 * 
	 * @param tablePrefix
	 * @param id
	 * @param module
	 * @param attr
	 * @param file
	 * @return
	 */
	public boolean updateDB(String tablePrefix, int id, String module, String attr, String file)
	{
		String query = "";

		if (tablePrefix.equals("Categories"))
			query = "update " + DatabaseValidation.encodeParam(tablePrefix + "_" + module) + " set " + attr + " = ? where category_id = ?";
		else
			query = "update " + DatabaseValidation.encodeParam(tablePrefix + "_" + module) + " set " + attr + " = ? where element_id = ?";

		int rows = updateData(query, new Object[] { file, id });
		System.out.println(query + " rows " + rows);
		if (rows == 1)
			return (true);
		else
			return (false);
	}

	/**
	 * Internal method for going through form data (in the request argument) and
	 * creating the body of an SQL statement i.e. all the columnName = value,
	 * sections for each form item. Some form items are skipped: ATTRFILE,
	 * ATTRLONG, ATTRDATE(when the value is empty) and any internal columns
	 * which don't begin with ATTR. These fields listed are dealt with
	 * differently because they do not write all the form contents directly into
	 * the DB as other normal fields are. ATTRFILE and ATTRLONG need file
	 * manipulation and then DB updates which occur elsewhere. are dealt with in
	 * a special way. Also using the addExtraArgs boolean the correct system
	 * columns are added to the SQL statement. If this argument is true then
	 * folderLevel,Category_ParentID are added otherwise
	 * Category_id,Language_id,Status_id,Lock_id,Version,Live are added.
	 * 
	 * @param request
	 * @param updateSQL
	 * @param level
	 * @param parentID
	 * @param addExtraArgs
	 * @param meta
	 * @return the body of the update statement but not the whole thing.
	 */
	private String processAttributes(MultipartRequest request, boolean updateSQL, String level, String parentID, boolean addExtraArgs, Vector meta, String tableName, String tableType)
	{
		StringBuffer sb = new StringBuffer(200);
		StringBuffer values = new StringBuffer(200);
		String liveFlag = "0";
		boolean includeLive = false;
		boolean updateSortOrder = true;
		String liveDate = null;
		String expireDate = null;

		String categoryId = parentID;
		int displayOrder = 0;

		// Should not execute while update element and not sorting
		if (updateSQL && "Elements".equals(tableType))
		{
			Vector v = new ClientDataAccess().getElement(tableName, parentID, true);
			if (v != null && v.size() > 0)
			{
				updateSortOrder = false;
			}
			else
			{
				categoryId = new ClientDataAccess().getCategoryFromElement(tableName, parentID);
				displayOrder = getElementCount(tableName, new Integer(categoryId).intValue(), "elements");
			}
		}

		Enumeration e = request.getParameterNames();
		while (e.hasMoreElements())
		{

			String name = (String) e.nextElement();
			String value = request.getParameter(name);

			if (name.startsWith("ATTR"))
			{
				if (name.startsWith("ATTRFILE") || name.startsWith("ATTRMULTI") || name.startsWith("ATTRNOTES"))
					continue;
				// deal with large text fields - actually post-pone handling
				// them till a little later
				if (name.startsWith("ATTRLONG") || name.startsWith("ATTRTEXT"))
					continue;
				// special case for null dates or files ... don't write nulls
				if (name.startsWith("ATTRDATE") && (value == null || value.equals("")))
					continue;
				if (name.startsWith("ATTRPASS") && PasswordUtils.FAKE_PASSWD.equals(value))
					continue;
				// special cases ...
				if (name.startsWith("ATTRCHECK") || name.startsWith("ATTRCURRENCY") || name.startsWith("ATTRINTEGER"))
				{
					if (updateSQL)
						sb.append(name + " = " + value + ",");
					else
					{
						sb.append(name + ",");
						values.append(value + ",");
					}
				}
				else if (name.startsWith("ATTRDATE"))
				{
					if (updateSQL)
						sb.append(name + " = '" + value + "',");
					else
					{
						sb.append(name + ",");
						values.append("'" + value + "',");
					}
				}
				else if (name.startsWith("ATTRPASS"))
				{
					if (updateSQL)
						sb.append(name + " = '" + PasswordUtils.encrypt(value) + "',");
					else
					{
						sb.append(name + ",");
						values.append("'" + PasswordUtils.encrypt(value) + "',");
					}
				}
				else
				{
					// parse value for special chrs to encode
					value = parseAndEncode(value, meta, name);
					if (updateSQL)
						sb.append(name + " = '" + value + "',");
					else
					{
						sb.append(name + ",");
						values.append("'" + value + "',");
					}
				}
			}
			else if (name.equals("elementForm"))
			{
				includeLive = true;
			}
			else if (name.equals("Live"))
			{
				liveFlag = "1";
			}
			else if (name.equals("Live_date"))
			{
				liveDate = value;
			}
			else if (name.equals("Expire_date"))
			{
				expireDate = value;
			}
		}

		String query = "";
		if (updateSQL)
		{
			if (!tableName.equalsIgnoreCase("SUBSCRIBE") && includeLive)
			{
				sb.append("Live = '" + liveFlag + "',");

				if ("1".equals(liveFlag) && updateSortOrder)
				{
					sb.append("display_order = " + displayOrder + ",");
				}
				else if ("0".equals(liveFlag))
				{
					sb.append("display_order = null,");
				}
				if (liveDate != null)
				{
					if ("".equals(liveDate))
					{
						sb.append("Live_date = NULL,");
					}
					else
					{
						sb.append("Live_date = '" + liveDate + "',");
					}
				}
				if (expireDate != null)
				{
					if ("".equals(expireDate))
					{
						sb.append("Expire_date = NULL,");
					}
					else
					{
						sb.append("Expire_date = '" + expireDate + "',");
					}
				}
			}
			query = sb.substring(0, sb.length() - 1);
		}
		else
		{
			String query2 = "";
			if (tableName.equalsIgnoreCase("SUBSCRIBE"))
			{
				liveFlag = "1";
			}
			if (includeLive)
			{
				if (!tableName.equalsIgnoreCase("SUBSCRIBE"))
				{
					sb.append("Live" + ",");
					values.append("'" + liveFlag + "',");

					if ("1".equals(liveFlag))
					{
						sb.append("display_order" + ",");
						values.append(displayOrder + ",");
					}

					if (liveDate != null && !"".equals(liveDate))
					{
						sb.append("Live_date" + ",");
						values.append("{d '" + liveDate + "'},");
					}
					if (expireDate != null && !"".equals(expireDate))
					{
						sb.append("Expire_date,");
						values.append("{d '" + expireDate + "'},");
					}
				}
			}

			if (addExtraArgs)
			{// this check only works for category
				String s = "select max(display_order)+1 from Categories_" + DatabaseValidation.encodeParam(tableName) + " where Category_ParentID = ?";
				// Vector v = select( s );
				Vector v = selectQuerySingleCol(s, new String[] { parentID });
				String display_order = "";
				if (v.size() > 0)
					display_order = (String) v.elementAt(0);
				// special case for adding the first category
				if (display_order == null || display_order == "")
					display_order = "1";

				query = sb.toString() + "folderLevel,Category_ParentID,display_order";
				query2 = values.toString() + level + "," + parentID + "," + display_order;
			}
			else
			{
				// special condition to send all subscribe data live immediately
				// TODO against front end access code
				query = sb.toString() + "Category_id,Language_id,Status_id,Lock_id,Version";
				query2 = values.toString() + parentID + ",1,1,1,1";
			}
			query = "(" + query + ",Create_date) VALUES (" + query2 + ",now())";
		}

		return (query);
	}

	public boolean deleteNote(String idNote)
	{
		boolean ret = false;
		DBaccess dba = new DBaccess();
		try
		{
			int changes = dba.updateData("delete from notes where id=?", new String[] { idNote });
			ret = (changes == 1);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return ret;
	}

	private void processMultiSelect(MultipartRequest request, String tableName, boolean isUpdate, int assetID, String tableType)
	{
		String type = tableType.toLowerCase().startsWith("categories") ? "category" : "element";
		Enumeration e = request.getParameterNames();
		while (e.hasMoreElements())
		{
			String name = (String) e.nextElement();
			if (name.startsWith("ATTRMULTI"))
			{
				String[] values = request.getParameterValues(name);

				// if we have nothing selected then just return
				if (values == null || values.length == 0)
					return;

				// if we're in update mode then delete all entries first..
				String sql = "delete from drop_down_multi_selections where ELEMENT_ID = ? and ATTRMULTI_NAME = ? and MODULE_NAME = ? and module_type = ? ";
				if (isUpdate)
				{
					updateData(sql, new String[] { String.valueOf(assetID), name, tableName, type });
				}

				// insert each of the multi rows.
				sql = "insert into drop_down_multi_selections values('" + escape(values[0]) + "'," + assetID + ",'" + name + "','" + tableName + "', '" + type + "')";

				for (int i = 1; i < values.length; i++)
				{
					sql += ",('" + escape(values[i]) + "'," + assetID + ",'" + name + "','" + tableName + "', '" + type + "')";
				}

				updateData(sql);
			}

			if (name.startsWith("DUMMYATTRMULTI"))
			{
				String realMulti = request.getParameter(name.substring(5));
				if (realMulti == null)
				{
					String sql = "delete from drop_down_multi_selections where ELEMENT_ID = ? and ATTRMULTI_NAME = ? and MODULE_NAME = ? and module_type = ? ";
					updateData(sql, new String[] { String.valueOf(assetID), name.substring(5), tableName, type });
				}
			}
		}
	}

	public String processAttribute(String name, String value)
	{

		if (name.startsWith("ATTR"))
		{
			// special cases ...
			if (name.startsWith("ATTRCHECK") || name.startsWith("ATTRCURRENCY") || name.startsWith("ATTRINTEGER"))
			{
				// value = "+value+";
			}
			else if (name.startsWith("ATTRDATE"))
			{
				// special case for null dates or files ... don't write nulls
				if (value == null || value.equals(""))
				{
					value = null;
				}
				else
				{
					value = "{d '" + value + "'}";
				}
			}
			else
			{

				value = StringEscapeUtils.escapeXml(value);
				value = "'" + value + "'";

			}
		}

		return value;
	}

	/**
	 * Returns the external column name for a given internalName.
	 * 
	 * @param module
	 * @param internalName
	 * @return
	 */
	public String getColumnName(String module, String internalName)
	{
		String query = "select Label_ExternalName from Labels_" + DatabaseValidation.encodeParam(module) + " where Label_InternalName = ?";
		// Vector v = select( query );
		Vector v = selectQuerySingleCol(query, new String[] { internalName });
		String s = "";
		try
		{
			if (null != v && v.size() > 0)
				s = (String) v.elementAt(0);
		}
		catch (RuntimeException e)
		{
			logger.error(" module=" + module + ",internalName=" + internalName + ",select= " + query);
			e.printStackTrace();
			throw e;
		}

		return s;
	}

	/**
	 * Returns the external column name for a given internalName.
	 * 
	 * @param module
	 * @param internalName
	 * @return
	 */
	public Vector getColumnsInternalName(String module)
	{
		String query = "select Label_InternalName from Labels_" + DatabaseValidation.encodeParam(module);
		return selectQuerySingleCol(query);
	}

	public Vector getColumnsExternalName(String module)
	{
		String query = "select Label_ExternalName from Labels_" + DatabaseValidation.encodeParam(module);
		return selectQuerySingleCol(query);
	}

	/**
	 * Returns internal column name for the given column index number. Uses the
	 * labelsTable array to get column names from.
	 * 
	 * @param tableName
	 * @param column
	 * @return
	 */
	public String getColumnName(String tableName, int column)
	{
		String[] columnData;
		if (fullTableName != null)
			columnData = DatabaseTableMetaDataService.columnService(fullTableName, column);
		else
			columnData = DatabaseTableMetaDataService.columnService(tableName, column);

		String columnName = columnData[0];// the name

		String colName = "";
		for (int i = 0; i < labelsTable.size(); i++)
		{
			String[] labelsTableRow = (String[]) labelsTable.elementAt(i);
			if (labelsTableRow[0].equals(columnName))
			{
				colName = labelsTableRow[1];
				break;
			}
		}

		return (colName);
	}

	/**
	 * forces a query on the specified table which refreshes column meta data
	 * variables in this class.
	 * 
	 * @param tablePostFix
	 *            module name
	 * @param tableName
	 *            should be Categories or Elements
	 */
	public void getMetaDataOnly(String tablePostFix, String tableName)
	{
		String key = "Element_id";
		String tablePrefix = "";
		if (tableName.equals("Categories"))
		{
			tablePrefix = "Category";
			key = "Category_id";
		}

		String query = "select * from " + DatabaseValidation.encodeParam(tableName) + "_" + DatabaseValidation.encodeParam(tablePostFix) + " where " + key + " = 0";
		Vector v = new Vector(20);
		// v = select( query, true, false );
		// TODO validated
		v = selectQuery(query);
	}

	public static int getRandom(int no)
	{
		Random rand = new Random();
		return Math.abs(rand.nextInt()) % no + 1;
	}

	public static int numberOfSystemColumns_Asset = 11;
	public static int numberOfSystemColumns_Category = 11;

	// compares a vector of labels to a single label given in the arguments and
	// finds the index number
	// where that label exists or return 0 if it doesn't exist.
	// used for comparing ordered labels with unordered lists
	public int getInteralIndex(String label, Vector labels)
	{
		for (int i = 0; i < labels.size(); i++)
		{
			String[] cols = (String[]) labels.elementAt(i);
			if (cols[0].equals(label))
				return i + DButils.numberOfSystemColumns_Category;
		}
		return 0;
	}

	/* Wrapper for below */
	public Vector getRowFromTable(String tablePostFix, String element_id, String tableName)
	{
		return getRowFromTable(tablePostFix, element_id, tableName, true);
	}

	/**
	 * Works with both Category and Element tables. sets the labelsTable global
	 * with all the label info for the table given. Actually returns the content
	 * from the table itself though so the method really performs two functions;
	 * priming the labelsTable array and returning actual row data
	 * 
	 * @param tablePostFix
	 * @param element_id
	 * @param tableName
	 *            Should be either Categories or Elements
	 * @param userSortOrder
	 *            true for normal element requests (as used by various things
	 *            like tabbed views) with column sort on, false for usage via
	 *            export
	 * @return
	 */
	public Vector getRowFromTable(String tablePostFix, String element_id, String tableName, boolean userSortOrder)
	{
		String elementExtraCategoryCol = "Live,Element_id,";
		String key = "Element_id";
		String tablePrefix = "";
		if (tableName.equals("Categories"))
		{
			tablePrefix = "Category";
			key = "Category_id";
			elementExtraCategoryCol = "Category_ParentID,folderLevel,";
		}
		// New code for sorting of Attributes
		String userSortOrderStatment = " order by display_order";
		if (!userSortOrder)
			userSortOrderStatment = "";
		// now get Labels table

		tablePostFix = DatabaseValidation.encodeParam(tablePostFix);

		String query = "select Label_InternalName, Label_ExternalName, Label_DefaultValue, Label_Mandatory, Label_Unique, Label_Index, Label_OnOff, Label_Info from " + tablePrefix + "Labels_" + tablePostFix + userSortOrderStatment;
		String[] labelCols = { "Label_InternalName", "Label_ExternalName", "Label_DefaultValue", "Label_Mandatory", "Label_Unique", "Label_Index", "Label_OnOff", "Label_Info" };
		labelsTable = selectQuery(query);

		String x[];
		StringBuffer internalOrderedColList = new StringBuffer("");
		x = (String[]) labelsTable.elementAt(0);
		internalOrderedColList = internalOrderedColList.append(x[0]);
		for (int i = 1; i < labelsTable.size(); i++)
		{
			x = (String[]) labelsTable.elementAt(i);
			internalOrderedColList = internalOrderedColList.append(", " + x[0]);
		}

		String systemCols = "Language_id,Status_id,Lock_id,Version,display_order,Create_date,Live_date,Expire_date" + "," + elementExtraCategoryCol + "Category_id,";
		internalOrderedColList = new StringBuffer(systemCols).append(internalOrderedColList);
		element_id = DatabaseValidation.encodeParam(element_id);
		query = "select " + internalOrderedColList.toString() + " from " + DatabaseValidation.encodeParam(tableName) + "_" + tablePostFix + " where " + key + " = ?";
		// query is needed to get all the meta-data
		String[] columns = internalOrderedColList.toString().split(",");

		Vector v = selectQuery(query, new String[] { element_id });
		return v;
	}

	/**
	 * returns true if category has children ... could be converted but
	 * currently for category use only.
	 * 
	 * @param element_id
	 * @param tablePrefix
	 * @return
	 */
	public boolean checkForChildren(String element_id, String tablePrefix)
	{
		// first check for category children
		String query = "select Category_parentID from Categories_" + DatabaseValidation.encodeParam(tablePrefix) + " where Category_parentID = ?";
		// Vector v = select( query );
		Vector v = selectQuerySingleCol(query, new String[] { element_id });
		if (v != null && v.size() > 0)
			return true;

		// now check for element children
		query = "select category_id from Elements_" + DatabaseValidation.encodeParam(tablePrefix) + " where category_id = ?";
		// v = select( query );
		v = selectQuerySingleCol(query, new String[] { element_id });
		if (v != null && v.size() > 0)
			return true;

		return false;
	}

	/**
	 * As below but for categories
	 * 
	 * @param categoryID
	 * @param module
	 * @return
	 */
	private boolean reorderCategories(String categoryID, String module)
	{
		// find the order element was set at
		String query = "select display_order, Category_ParentID from Categories_" + DatabaseValidation.encodeParam(module) + " where Category_id = ?";
		String[] cols = { "display_order", "Category_ParentID" };
		// Vector v = select( query, cols );
		Vector v = selectQuery(query, new String[] { categoryID });
		String[] results = (String[]) v.elementAt(0);

		// now reduce all elements order which were above it in rank
		// but only do it if the element previously had an order value ... i.e.
		// it was live otherwise
		// reordering makes no sense and will cause issues in the DB integrity.
		int rows = 0;
		if (results[0] != null && !results[0].equals("0") && !results[0].equals(""))
		{
			query = "update Categories_" + module + " set display_order = display_order-1 where display_order > ? and Category_ParentID = ?";
			System.out.println(query);
			rows = updateData(query, new Object[] { results[0], results[1] });
		}
		if (rows > 0)
			return true;
		else
			return false;
	}

	/**
	 * Within a particular parent category this method reorders all the other
	 * elements around the one specified ensuring there are no gaps and the
	 * display_order number is kept consequtive.
	 * 
	 * @param elementID
	 * @param module
	 * @return
	 */
	private boolean reorderElements(String elementID, String module)
	{
		// find the order element was set at
		String query = "select display_order, category_id from Elements_" + DatabaseValidation.encodeParam(module) + " where element_id = ?";
		String[] cols = { "display_order", "category_id" };
		// Vector v = select( query, cols );
		Vector v = selectQuery(query, new String[] { elementID });
		String[] results = (String[]) v.elementAt(0);

		// now reduce all elements order which were above it in rank
		// but only do it if the element previously had an order value ... i.e.
		// it was live otherwise
		// reordering makes no sense and will cause issues in the DB integrity.
		int rows = 0;
		if (results[0] != null && !results[0].equals("0") && !results[0].equals(""))
		{
			query = "update Elements_" + module + " set display_order = display_order-1 where display_order > ? and Live = 1 and category_id = ?";
			System.out.println(query);
			rows = updateData(query, new Object[] { results[0], results[1] });
		}
		if (rows > 0)
			return true;
		else
			return false;
	}

	/**
	 * Allows the user to remove an asset from the CMS (Element or Category).
	 * This method takes care of removing physical files in the stores
	 * directory, reorder elements around the deleted item to ensure gaps don't
	 * appear and also the actual SQL delete from the appropriate asset table.
	 * 
	 * @param element_id
	 *            item to delete
	 * @param tableName
	 *            Categories or Elements
	 * @param tablePostFix
	 *            module name
	 * @return
	 */
	public boolean deleteRow(String element_id, String tableName, String tablePostFix)
	{
		String extraDir = "";
		String key = "Element_id";
		if (tableName.equals("Categories"))
		{
			extraDir = "Categories/";
			key = "Category_id";
		}

		// TODO: below 3 sections need to be wrapped in a transaction

		// reorder elements remaining
		boolean r = false;
		if (!tableName.equals("Categories"))
			r = reorderElements(element_id, tablePostFix);
		else
			r = reorderCategories(element_id, tablePostFix);
		if (d.getDEBUG())
			System.out.println("DButils:deleteRow() Re-ordered elements = " + r);
		// remove db entry
		String query = "delete from " + DatabaseValidation.encodeParam(tableName + "_" + tablePostFix) + " where " + key + " = ?";
		int rowsEffected = updateData(query, new Object[] { element_id });

		if (rowsEffected != 1)
			return false;
		else
		{
			// remove physical file(s)
			boolean success = d.nuke(new File(d.getStoreDir() + "/" + tablePostFix + "/" + extraDir + element_id));
			if (d.getDEBUG())
				System.out.println("DButils:deleteRow(" + element_id + "," + tableName + "," + tablePostFix + ") NUKE " + d.getStoreDir() + "/" + tablePostFix + "/" + extraDir + element_id + " = " + success);

			if (SearchContent.luceneSearchOn())
			{
				boolean isElement = true;
				if ("Categories".equalsIgnoreCase(tableName))
					isElement = false;
				// now update the search index
				SearchContent searchContent = new SearchContent(isElement, tablePostFix, element_id);
				searchContent.deleteAsset();
			}

			// now remove any attrnotes
			query = "delete from notes where foriegnKey = ? and moduleName = ?";
			updateData(query, new Object[] { element_id, tablePostFix });

			return true;
		}
	}

	/**
	 * MediaStore method PLEASE IGNORE. returns the defaults getName() method
	 * value
	 * 
	 * @return
	 */
	public String getBrand()
	{
		return (d.getName());
	}

	/**
	 * MediaStore method PLEASE IGNORE.
	 * 
	 * @param cat_name
	 * @return
	 */
	public String getAllCategorySelect(String cat_name)
	{
		StringBuffer out = new StringBuffer("<SELECT name=\"category\">");

		String query = "select category_id, category_name, brand_name from category c, brand b where b.brand_id = c.brand_id order by brand_name";
		String[] cols = { "category_id", "category_name", "brand_name" };

		// Vector v = select( query, cols, false, false );
		Vector v = selectQuery(query);

		String[] res = null;

		out.append("<OPTION value=\"pleaseSelect\">Please select ...</OPTION>");

		for (int i = 0; i < v.size(); i++)
		{
			out.append("<OPTION");

			res = (String[]) v.elementAt(i);
			if (cat_name.equals(res[1]))
				out.append(" SELECTED");

			out.append(" value=\"" + res[0] + "\">" + res[2] + " - " + res[1] + "</OPTION>");
		}

		out.append("</SELECT>");

		return (out.toString());
	}

	/**
	 * MediaStore method PLEASE IGNORE.
	 * 
	 * @param cat_name
	 * @return
	 */
	public String getCategorySelect(String cat_name)
	{
		StringBuffer out = new StringBuffer("<SELECT name=\"category\">");

		String query = "select category_name from category where brand_id = ?";
		// Vector v = select( query, false, false );
		Vector v = selectQuerySingleCol(query, new String[] { d.getID() });

		out.append("<OPTION value=\"pleaseSelect\">Please select ...</OPTION>");

		for (int i = 0; i < v.size(); i++)
		{
			out.append("<OPTION");

			if (cat_name.equals(v.elementAt(i)))
				out.append(" SELECTED");

			out.append(">" + v.elementAt(i) + "</OPTION>");
		}

		out.append("</SELECT>");

		return (out.toString());
	}

	/**
	 * MediaStore method PLEASE IGNORE.
	 * 
	 * @param brand
	 * @return
	 */
	public String getAllBrandSelect(String brand)
	{
		StringBuffer out = new StringBuffer("<SELECT name=\"brand\">");

		String query = "select brand_name from brand";
		String[] cols = { "brand_name" };

		// Vector v = select( query, false, false );
		// TODO validated
		Vector v = selectQuerySingleCol(query);

		out.append("<OPTION value=\"pleaseSelect\">Please select ...</OPTION>");

		for (int i = 0; i < v.size(); i++)
		{
			out.append("<OPTION");

			if (brand.equals((String) v.elementAt(i)))
				out.append(" SELECTED");

			out.append(">" + (String) v.elementAt(i) + "</OPTION>");
		}

		out.append("</SELECT>");

		return (out.toString());
	}

	/**
	 * MediaStore method PLEASE IGNORE.
	 * 
	 * @return
	 */
	public String getAllCategorySelect()
	{
		StringBuffer out = new StringBuffer("<SELECT name=\"category\">");

		String query = "select category_id, category_name, brand_name from category c, brand b where b.brand_id = c.brand_id order by brand_name";
		String[] cols = { "category_id", "category_name", "brand_name" };

		// Vector v = select( query, cols, false, false );
		Vector v = selectQuery(query);

		String[] res = null;

		out.append("<OPTION value=\"pleaseSelect\">Please select ...</OPTION>");

		for (int i = 0; i < v.size(); i++)
		{
			res = (String[]) v.elementAt(i);

			out.append("<OPTION value=\"" + res[0] + "\"");
			if (res[1].equals(d.getDirCat()) && res[2].equals(d.getDirHier()))
				out.append(" SELECTED");

			out.append(">" + res[2] + " - " + res[1] + "</OPTION>");
		}

		out.append("</SELECT>");

		return (out.toString());
	}

	/**
	 * MediaStore method PLEASE IGNORE.
	 * 
	 * @return
	 */
	public String getCategorySelect()
	{
		StringBuffer out = new StringBuffer("<SELECT name=\"category\">");

		String query = "select category_name from category where brand_id = ?";
		// Vector v = select( query, false, false );
		Vector v = selectQuerySingleCol(query, new String[] { d.getID() });

		out.append("<OPTION value=\"pleaseSelect\">Please select ...</OPTION>");

		for (int i = 0; i < v.size(); i++)
			out.append("<OPTION>" + v.elementAt(i) + "</OPTION>");

		out.append("</SELECT>");

		return (out.toString());
	}

	/**
	 * Unused method to substring through the in filed and encode &'s - this
	 * method was an exercise in double encoding but never finished and the 255
	 * chr limit was then put in place to work out if text fields should be
	 * encoded or not.
	 * 
	 * @param in
	 * @return
	 */
	private String carefulEncode(String in)
	{
		StringBuffer out = new StringBuffer();

		int size = in.length();

		int nextOcc = in.indexOf("&");
		System.out.print("where " + nextOcc);
		while (nextOcc != -1)
		{
			// so first work out if we really need to perform the encoding
			if (nextOcc + 5 <= size && in.substring(nextOcc, nextOcc + 5).equals("&amp;"))
			{
				out.append(in.substring(0, nextOcc + 5));
				in = in.substring(nextOcc + 5, in.length());

				// System.out.println("already encoded");
				// System.out.println(" in so far: "+in);
				// System.out.println("out so far: "+out);
			}
			else
			{
				out.append(in.substring(0, ++nextOcc) + "amp;");
				in = in.substring(nextOcc, size);

				// System.out.println("found one at "+nextOcc);
				// System.out.println(" in so far: "+in);
				// System.out.println("out so far: "+out);
			}

			nextOcc = in.indexOf("&");
			size = in.length();
		}

		// System.out.println("size "+size);
		// System.out.println("size "+in.length());
		// System.out.println("out so far: "+out.toString());
		// System.out.println("in so far: "+in);

		out.append(in);

		return out.toString();

	}

	/**
	 * MediaStore method PLEASE IGNORE.
	 * 
	 * @param x
	 */
	public void setBrand_id(String x)
	{
		brand_id = x;
	}

	/**
	 * MediaStore method PLEASE IGNORE.
	 * 
	 * @return
	 */
	public String getBrand_id()
	{
		if (brand_id == null)
			return ("");
		else
			return (brand_id);
	}

	/**
	 * MediaStore method PLEASE IGNORE.
	 * 
	 * @param multiple
	 * @return
	 */
	public String getBrands(String multiple)
	{
		StringBuffer sb = new StringBuffer(100);

		if (multiple.equals("single"))
			sb.append("<SELECT name=brand_id>");
		else
			sb.append("<SELECT name='brand_id' size='4' multiple>");

		String query = "select brand_id, brand_name from brand";
		String[] cols = { "brand_id", "brand_name" };

		// Vector v = select( query, cols, false, false );
		Vector v = selectQuery(query);

		if (!multiple.equals("multipleUser"))
			sb.append("<OPTION value=\"pleaseSelect\">Please select ...</OPTION>");

		for (int i = 0; i < v.size(); i++)
		{
			cols = (String[]) v.elementAt(i);

			sb.append("<OPTION value='" + cols[0] + "'");

			if (cols[1].equals(d.getDirHier()))
				sb.append(" SELECTED");

			sb.append(">" + cols[1] + "</OPTION>");
		}

		sb.append("</SELECT>");

		return (sb.toString());
	}

	public int setSortOrderElements(String module, String categoryID)
	{
		int returnID = 0;

		System.out.println("setSortOrderElements");
		String query = "select Element_id from elements_" + DatabaseValidation.encodeParam(module) + " where Category_ID = ? and live=1 order by " + " (CASE WHEN display_order IS NULL then 1 ELSE 0 END), display_order";
		// Vector v = select( query );
		Vector v = selectQuerySingleCol(query, new String[] { categoryID });

		for (int i = 1; i <= v.size(); i++)
		{
			String id = (String) v.elementAt(i - 1);
			query = "update elements_" + module + " set display_order = ? where element_id = ?";
			updateData(query, new Object[] { i, id });
		}

		query = "update elements_" + module + " set display_order = null where Category_ID =? and live=0";
		updateData(query, new Object[] { categoryID });

		return returnID;
	}

	public boolean verifySortOrderElements(String module, String categoryID)
	{
		try
		{
			String dataCheck = " select count(*) as data_check from elements_" + DatabaseValidation.encodeParam(module) + " group by display_order, category_ID, live having category_ID = ? " + "  and live=1 and (count(display_order) > 1  OR display_order =0 )";
			// Vector check = select( dataCheck );
			Vector check = selectQuerySingleCol(dataCheck, new String[] { categoryID });

			if (check == null || check.size() == 0)
			{
				String dataCheck2 = " select max(display_order)=count(element_id) from elements_" + DatabaseValidation.encodeParam(module) + " where category_ID = ? and live=1";
				// Vector check2 = select( dataCheck2 );
				Vector check2 = selectQuerySingleCol(dataCheck, new String[] { categoryID });
				int count = 0;
				if (check2 != null && check2.size() > 0)
					count = Integer.parseInt((String) check2.elementAt(0));
				return count == 0;
			}
			else
				return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return true;
	}

	public boolean verifySortOrderCategories(String module, String parentID)
	{

		int count = 0;
		try
		{
			String dataCheck = " select count(*) as data_check from categories_" + DatabaseValidation.encodeParam(module) + " group by display_order, category_parentID having category_parentID = ? " + " and (count(display_order) > 1 OR display_order is null OR display_order =0)";
			// Vector check = select( dataCheck );
			Vector check = selectQuerySingleCol(dataCheck, new String[] { parentID });
			// if ( check == null || check.elementAt(0) == null ||
			// check.elementAt(0).equals( "" ) || check.elementAt(0).equals( "0"
			// ) )
			if (check == null || check.size() == 0)
			{
				String dataCheck2 = " select max(display_order)=count(category_id) from categories_" + DatabaseValidation.encodeParam(module) + " where category_parentid = ?";
				// Vector check2 = select( dataCheck2 );
				Vector check2 = selectQuerySingleCol(dataCheck, new String[] { parentID });
				if (check2 != null && check2.size() > 0)
					count = Integer.parseInt((String) check2.elementAt(0));
				return count == 0;
			}
			else
				return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * There is a 0, blank or null in the ordering so reset all the ordering to
	 * avoid issue
	 * 
	 * @param module
	 * @param parentID
	 * @return
	 */
	public int setSortOrderCategories(String categoryID, String module, String parentID)
	{
		int returnID = 0;

		System.out.println("setSortOrder");
		String query = "select Category_id from Categories_" + DatabaseValidation.encodeParam(module) + " where Category_ParentID = ? order by (CASE WHEN display_order IS NULL then 1 ELSE 0 END), display_order";
		// Vector v = select( query );
		Vector v = selectQuerySingleCol(query, new String[] { parentID });

		for (int i = 1; i <= v.size(); i++)
		{
			String id = (String) v.elementAt(i - 1);
			query = "update Categories_" + module + " set display_order = ? where Category_id = ?";
			updateData(query, new Object[] { i, id });
			if (categoryID.equals(id))
			{
				returnID = i;
			}
		}

		return returnID;
	}

	/**
	 * As below but for Categories instead
	 * 
	 * @param module
	 * @param parentCategoryID
	 * @param categoryID
	 * @param up
	 */

	public void reorderCategory(String module, String parentCategoryID, String categoryID, boolean up)
	{

		if (verifySortOrderCategories(module, parentCategoryID))
		{
			setSortOrderCategories(categoryID, module, parentCategoryID);
		}

		// get the number of sub categories for subsequent calculations
		String query = "select display_order from Categories_" + DatabaseValidation.encodeParam(module) + " where Category_id = ?";
		System.out.println(query);
		// Vector v = select( query );
		Vector v = selectQuerySingleCol(query, new String[] { categoryID });
		int oldOrder = 0;
		if (v.elementAt(0) != null && !v.elementAt(0).equals("") && !v.elementAt(0).equals("0"))
			oldOrder = Integer.parseInt((String) v.elementAt(0));
		else
			oldOrder = setSortOrderCategories(categoryID, module, parentCategoryID);

		query = "select Category_id from Categories_" + DatabaseValidation.encodeParam(module) + " where Category_ParentID = ?";
		// v = select( query );
		v = selectQuerySingleCol(query, new String[] { parentCategoryID });
		int totalSize = v.size();

		int newOrder = oldOrder;
		System.out.println("newOrder " + newOrder + " size " + totalSize + " up " + up);
		if (up && newOrder != 1)
			newOrder--;
		if (!up && newOrder != totalSize)
			newOrder++;

		query = "select Category_id from Categories_" + DatabaseValidation.encodeParam(module) + " where display_order = ? and Category_ParentID = ?";
		// v = select( query );
		v = selectQuerySingleCol(query, new Object[] { newOrder, parentCategoryID });

		String elementIDtoMove = "";
		try
		{
			elementIDtoMove = (String) v.elementAt(0);

		}
		catch (Exception e)
		{
			logger.error("Error occurred while Sorting Elements.  Module:" + module + "  ParentCategoryId :" + parentCategoryID + "  Total Size: " + totalSize);
			e.printStackTrace();
			return;
		}

		System.out.println("Category to move " + elementIDtoMove + " from " + categoryID);
		// now update each of the above with switch order values
		query = "update Categories_" + module + " set display_order = ? where Category_id = ?";
		int rows = updateData(query, new Object[] { oldOrder, elementIDtoMove });
		System.out.println("update 1 " + rows);
		query = "update Categories_" + module + " set display_order = ? where Category_id = ?";
		rows = updateData(query, new Object[] { newOrder, categoryID });
		System.out.println("update 2 " + rows);
	}

	/**
	 * Handles the moving of Elements up and down the display order. It
	 * basically adds or subtracts one from the order of the element specified
	 * and then re-orders the other assets appropriately around it. Only
	 * re-orders within a category, because you don't want to reorder the whole
	 * table. 1 is top of list so order numbers are descending.
	 * 
	 * @param module
	 * @param categoryID
	 *            parent categoryID
	 * @param elementID
	 *            item to reoder
	 * @param up
	 *            true if moving up false if moving down
	 */
	public void reorder(String module, String categoryID, String elementID, boolean up)
	{
		String query = "select live from Elements_" + DatabaseValidation.encodeParam(module) + " where live = 1 and Category_id = ?";
		System.out.println(query);
		// Vector x = select( query );
		Vector x = selectQuerySingleCol(query, new String[] { categoryID });

		int totalSize = x.size();

		query = "select display_order from Elements_" + DatabaseValidation.encodeParam(module) + " where Element_id = ?";
		System.out.println(query);
		// Vector v = select( query );
		Vector v = selectQuerySingleCol(query, new String[] { elementID });
		int oldOrder = Integer.parseInt((String) v.elementAt(0));

		int newOrder = oldOrder;
		System.out.println("newOrder " + newOrder + " size " + totalSize + " up " + up);
		if (up && newOrder != 1)
			newOrder--;
		if (!up && newOrder != totalSize)
			newOrder++;

		query = "select Element_id from Elements_" + DatabaseValidation.encodeParam(module) + " where display_order = ? and Category_id = ?";
		// v = select( query );
		v = selectQuerySingleCol(query, new Object[] { newOrder, categoryID });

		String elementIDtoMove = (String) v.elementAt(0);

		System.out.println("element to move " + elementIDtoMove + " from " + elementID);
		// now update each of the above with switch order values
		query = "update Elements_" + module + " set display_order = ? where Element_id = ?";
		int rows = updateData(query, new Object[] { oldOrder, elementIDtoMove });
		System.out.println("update 1 " + rows);
		query = "update Elements_" + module + " set display_order = ? where Element_id = ?";
		rows = updateData(query, new Object[] { newOrder, elementID });
		System.out.println("update 2 " + rows);
	}

	/**
	 * As below but for category level sorting
	 * 
	 * @param module
	 * @param category_id
	 * @param sortCol
	 * @return
	 */
	public Vector getCategorySummaryData(String module, String category_id, String sortCol)
	{
		System.out.println("sortCol " + sortCol);
		String orderCol = "";
		if (sortCol == null || sortCol.equals(""))
			orderCol = "display_order";
		else if (sortCol.equals("1"))
			orderCol = "ATTR_categoryName";

		String[] cols = { "ATTR_categoryName", "Category_id", "Category_ParentID", "display_order" };
		String query = "select ATTR_categoryName, Category_id, Category_ParentID, display_order from Categories_" + DatabaseValidation.encodeParam(module) + " where Category_ParentID = ? order by " + orderCol;

		System.out.println(query);

		// Vector v = select( query, cols );
		Vector v = selectQuery(query, new String[] { category_id });

		return v;
	}

	/**
	 * Returns the summary (3 cols) data for the module specified. The result is
	 * a vector which can then be used to show all the summary elements within a
	 * particular category.
	 * 
	 * @param module
	 * @param category_id
	 * @param col2
	 * @param col3
	 * @param sortCol
	 * @param liveOnly
	 * @return
	 */
	public Vector getSummaryData(String module, String category_id, List<String> colunms, String sortCol, boolean liveOnly)
	{

		String col2 = colunms.size() >= 2 ? colunms.get(1) : "";
		String col3 = colunms.size() >= 3 ? colunms.get(2) : "";

		col2 = DatabaseValidation.encodeParam(col2);
		col3 = DatabaseValidation.encodeParam(col3);

		System.out.println("sortCol " + sortCol);
		String orderCol = "";
		if (sortCol == null || sortCol.equals(""))
			orderCol = "display_order";
		else if (sortCol.equals("1"))
			orderCol = "ATTR_Headline";
		else if (sortCol.equals("2"))
			orderCol = col2;
		else if (sortCol.equals("3"))
			orderCol = col3;
		else
			orderCol = sortCol;

		String extraClause = "";
		if (liveOnly)
			extraClause = " and live = 1 ";

		if (col2 == null || col2.equals("") || col2.equals("null"))
			col2 = "'null'";
		if (col3 == null || col3.equals("") || col3.equals("null"))
			col3 = "'null'";

		String[] cols = { "live", "live_date", "ATTR_Headline", col2, col3, "Element_id", "display_order", "Expire_date" };
		String query = "select Live, Live_date, ATTR_Headline, " + col2 + ", " + col3 + ", Element_id, display_order, Expire_date from Elements_" + DatabaseValidation.encodeParam(module) + " where Category_id = ? " + extraClause + " order by " + orderCol;

		System.out.println(query);

		// Vector v = select( query, cols );
		Vector v = selectQuery(query, new String[] { category_id });

		return v;
	}

	/**
	 * Return HTML markup for a &lt;SELECT ...> dropdown. The contents of the
	 * dropdown are all the categories within a given parent category. Only
	 * works for Categories. If Parent is null then it will return all
	 * categories at the given level. If selected is specified and matched in
	 * the result set of categories then it will be pre-selected in the
	 * dropdown.
	 * 
	 * @param level
	 * @param module
	 * @param selected
	 * @param parent
	 * @return
	 */
	public String getTreeFolders(int level, String module, String selected, String parent)
	{
		StringBuffer out = new StringBuffer("<SELECT onChange=\"javascript:submit()\" name=\"level" + level + "\">");

		List<String> params = new ArrayList<String>();
		params.add("" + level);

		String extQuery = "";
		if (parent != null)
		{
			extQuery = " and Category_ParentID = ?";
			params.add(parent);
		}
		String query = "select ATTR_categoryName, Category_id from Categories_" + DatabaseValidation.encodeParam(module) + " where folderLevel = ?" + extQuery + " order by ATTR_categoryName";
		String[] cols = { "ATTR_categoryName", "Category_id" };
		// Vector v = select( query, cols );
		Vector v = selectQuery(query, params.toArray());

		out.append("<OPTION value=\"pleaseSelect\">Please select ...</OPTION>");

		for (int i = 0; i < v.size(); i++)
		{
			String[] vals = (String[]) v.elementAt(i);

			out.append("<OPTION value=" + vals[1]);

			if (selected.equals(vals[0]))
				out.append(" SELECTED");

			out.append(">" + vals[0] + "</OPTION>");
		}

		out.append("</SELECT>");

		return (out.toString());
	}

	public int getElementCount(String tableName, int categoryId, String type)
	{

		String s = "";
		if ("categories".equalsIgnoreCase(type))
			s = "select max(display_order) +1 from categories_" + DatabaseValidation.encodeParam(tableName) + " where category_parentid = ?";
		else
		{
			s = "select max(display_order) +1 from elements_" + DatabaseValidation.encodeParam(tableName) + " where category_id = ? and live= 1";
		}

		// Vector v = select( s );
		Vector v = selectQuerySingleCol(s, new Object[] { categoryId });
		int count = 1;
		try
		{
			String order = (String) v.elementAt(0);
			count = (order == null ? 1 : Integer.parseInt(order));

		}
		catch (NumberFormatException nfe)
		{

		}
		return count;
	}

	private String escape(String str)
	{
		String encodedStr = StringEscapeUtils.escapeHtml(str);
		encodedStr = encode(encodedStr, "\\", "\\\\");
		encodedStr = encode(encodedStr, "'", "&#39;");
		return encodedStr;
	}
}
