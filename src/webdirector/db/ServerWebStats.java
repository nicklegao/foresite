/**
 * Created by IntelliJ IDEA.
 * User: Andrew Davidson
 * Date: Oct 24, 2003
 * Time: 12:09:47 PM
 * To change this template use Options | File Templates.
 */
package webdirector.db;

import java.util.Vector;

import au.net.webdirector.admin.modules.ModuleNameService;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;

public class ServerWebStats extends DBaccess
{
	private ModuleNameService NameService = new ModuleNameService();

	/**
	 * Controller method to invoke one of the various web stat reports. The
	 * report to run is specified by the 'col' value.
	 * Current reports are Top10, ProductReport, CountOfOrigin, TotalHits
	 * 
	 * @param start
	 *            date for report
	 * @param end
	 *            date for report
	 * @param col
	 *            report name to run
	 * @return Vector of results, each one is a String[2] with the first col
	 *         being the name of the result and the second being the count.
	 */
	public Vector reportByWebBrowser(String start, String end, String col)
	{
		start = DatabaseValidation.encodeParam(start);
		end = DatabaseValidation.encodeParam(end);
		col = DatabaseValidation.encodeParam(col);

		String query = "select " + col + ", count(*) as expr1 from WebStats where DateOfAccess >= '" + start + "' and " +
				"DateOfAccess <= '" + end + " 23:59' group by " + col + " order by expr1 desc";
		String[] cols = { col, "count" };

		if (col.equals("TotalHits"))
		{
			query = "select 'Total Hits', count(*) from WebStats where DateOfAccess >= '" + start + "' and " +
					"DateOfAccess <= '" + end + " 23:59'";
		}

		Vector v = new Vector();

		if (col.equals("Top10"))
		{
			v = Top10(start, end);
		}
		else if (col.equals("ProductReport"))
		{
			v = ProductReport(start, end);
		}
		else if (col.equals("CountryOfOrigin"))
		{
			v = CountryOfOrigin(start, end);
		}
		else
			v = selectQuery(query);// TODO validated
		return v;
	}

	/**
	 * Internal huge UNION statement of queries to run the report across Product
	 * types in the webstats table.
	 * Looks for AssetType = 'Product Details' or 'Product Popup' or 'News Item'
	 * or 'FAQ Item' or 'Employment Item Page' or 'Offer Item'
	 * or 'Locations Item' or 'Reference Library Item' or 'General Content
	 * Item' 'Special Offers Popup Page'
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	private Vector ProductReport(String start, String end)
	{
		// get all IPaddresses and then cross reference them against IP mapping table
		Vector v = new Vector();


		String[] cols = { "ATTR_Headline", "count" };
		String query = "SELECT '" + NameService.getModuleName("PRODUCTS") + ": '+ATTR_Headline, count(*) as expr1 FROM Elements_PRODUCTS ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = 'Products Details' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("PRODUCTS") + ": '+ATTR_Headline, count(*) FROM Elements_PRODUCTS ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = 'Product Popup' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("NEWS") + ": '+ATTR_Headline, count(*) FROM Elements_NEWS ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = 'News Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("PROJECTGALLERY") + ": '+ATTR_Headline, count(*) FROM Elements_PROJECTGALLERY ep, WebStats w " +
				"WHERE DateOfAccess >= '" + start + "' and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = 'Gallery Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("FAQ") + ": '+ATTR_Headline, count(*) FROM Elements_FAQ ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = 'FAQ Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("JOBADVERTISING") + ": '+ATTR_Headline, count(*) FROM Elements_JOBADVERTISING ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = 'Employment Item Page' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("SPECIALOFFERS") + ": '+ATTR_Headline, count(*) FROM Elements_SPECIALOFFERS ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = 'Offer Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("SPECIALOFFERS") + ": '+ATTR_Headline, count(*) FROM Elements_SPECIALOFFERS ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = 'Special Offers Popup Page' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("GENERALCONTENT") + ": '+ATTR_Headline, count(*) FROM Elements_GENERALCONTENT ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = 'General Content Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("REFERENCELIB") + ": '+ATTR_Headline, count(*) FROM Elements_REFERENCELIB ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = 'Reference Library Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("REALESTATE") + ": '+ATTR_Headline, count(*) FROM Elements_REALESTATE ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = 'Real Estate Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("MEMBERS") + ": '+ATTR_Headline, count(*) FROM Elements_MEMBERS ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = 'Members Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("SERVICES") + ": '+ATTR_Headline, count(*) FROM Elements_SERVICES ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = 'Professional Services' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("LOCATIONS") + ": '+ATTR_Headline, count(*) FROM Elements_LOCATIONS ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = 'Locations Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +


				"SELECT '" + NameService.getModuleName("FLEX1") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX1 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX1") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("FLEX2") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX2 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX2") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("FLEX3") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX3 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX3") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline" +

				"SELECT '" + NameService.getModuleName("FLEX4") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX4 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX4") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("FLEX5") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX5 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX5") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("FLEX6") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX6 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX6") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("FLEX7") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX7 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX7") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("FLEX8") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX8 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX8") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("FLEX9") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX9 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX9") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

				"SELECT '" + NameService.getModuleName("FLEX10") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX10 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX10") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +

// flex 11 - 20
				"SELECT '" + NameService.getModuleName("FLEX11") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX11 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX11") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +
				"SELECT '" + NameService.getModuleName("FLEX12") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX12 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX12") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +
				"SELECT '" + NameService.getModuleName("FLEX13") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX13 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX13") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +
				"SELECT '" + NameService.getModuleName("FLEX14") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX14 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX14") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +
				"SELECT '" + NameService.getModuleName("FLEX15") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX15 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX15") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +
				"SELECT '" + NameService.getModuleName("FLEX16") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX16 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX16") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +
				"SELECT '" + NameService.getModuleName("FLEX17") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX17 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX17") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +
				"SELECT '" + NameService.getModuleName("FLEX18") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX18 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX18") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +
				"SELECT '" + NameService.getModuleName("FLEX19") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX19 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX19") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline union " +
				"SELECT '" + NameService.getModuleName("FLEX20") + ": '+ATTR_Headline, count(*) FROM Elements_FLEX20 ep, WebStats w " +
				"WHERE DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) and w.AssetType = '" + NameService.getModuleName("FLEX20") + " Item' and " +
				"w.AssetID = ep.Element_id group by ATTR_Headline " +


				"order by expr1 desc";

		//v = select(query, cols);
		// TODO - validated
		v = selectQuery(query);

		return v;
	}

	/**
	 * Tries to convert IP address to country of origin using a IP look up table
	 * which may be a little out of date.
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	private Vector CountryOfOrigin(String start, String end)
	{
		// get all IPaddresses and then cross reference them against IP mapping table
		Vector v = new Vector();

		String query = "select AccessingIPAddress from WebStats where DateOfAccess >= CONVERT(DATETIME, ?, 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, ?, 102) group by AccessingIPAddress";
		//v = select(query);
		v = selectQuerySingleCol(query, new String[] { start, end + " 23:59" });

		// for each ip check what country it came from and build a return vector
		for (int i = 0; i < v.size(); i++)
		{
			String ip = (String) v.elementAt(i);
//			int largeIP = convIP(ip);
			query = "select country_name from ip_to_country where IP_TO > largeIP and largeIP > IP_FROM";
		}
//		SELECT     country_name, COUNT(*)
//FROM         iptocountry ip, WebStats w
//WHERE     inet_aton(w.AccessingIPAddress) >= ip.ip_from AND inet_aton(w.AccessingIPAddress) <= ip.ip_to
//GROUP BY AccessingIPAddress;

		return v;
	}

	private int convIP(String ip)
	{

		return 3;
	}

	/**
	 * Returns the top 10 accessed assets over the given period.
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	private Vector Top10(String start, String end)
	{
		Vector internal = new Vector(10);
		Vector x = new Vector();

		String[] cols2 = { "AssetType", "AssetID", "count" };
		String query = "select AssetType, AssetID, count(*) as expr1 from WebStats where DateOfAccess >= CONVERT(DATETIME, '" + start + "', 102) and " +
				"DateOfAccess <= CONVERT(DATETIME, '" + end + " 23:59', 102) group by AssetType, AssetID order by expr1 desc";
		//x = select(query, cols2);
		x = selectQuery(query);// TODO validated

		// need to retain only 10 and also look up asset names where possible
		if (x != null && x.size() != 0)
		{
			for (int i = 0; i < 10; i++)
			{
				Vector title = new Vector();
				String[] c = (String[]) x.elementAt(i);
				if (c[0].equals("News Item"))
				{
					query = "select ATTR_Headline from Elements_NEWS where Element_id = ?";
					//title = select(query);
					title = selectQuerySingleCol(query, new String[] { c[1] });

					if (title != null && title.size() > 0)
						c[0] = c[0] + " (" + (String) title.elementAt(0) + ")";
					else
						continue;
				}
				if (c[0].equals("Products Details"))
				{
					query = "select ATTR_Headline from Elements_PRODUCTS where Element_id = ?";
					//title = select(query);
					title = selectQuerySingleCol(query, new String[] { c[1] });
					if (title != null && title.size() > 0)
						c[0] = c[0] + " (" + (String) title.elementAt(0) + ")";
					else
						continue;
				}
				String[] newCols = { c[0], c[2] };
				internal.insertElementAt(newCols, i);
			}
		}
		return internal;
	}
}
