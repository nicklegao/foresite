package webdirector.seo;


/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 16/01/2009
 * Time: 13:59:06
 * To change this template use File | Settings | File Templates.
 */
public class ShortUrl
{
	/**
	 * <rule>
	 * <from>^/SBE/(.*)$</from>
	 * <to last="true">/$1</to>
	 * </rule>
	 **/
	private String fromUrl;
	private String toUrl;
	private boolean lastRule;

	public String getFromUrl()
	{
		return fromUrl;
	}

	public void setFromUrl(String fromUrl)
	{
		this.fromUrl = fromUrl;
	}

	public String getToUrl()
	{
		return toUrl;
	}

	public void setToUrl(String toUrl)
	{
		this.toUrl = toUrl;
	}

	public boolean isLastRule()
	{
		return lastRule;
	}

	public void setLastRule(boolean lastRule)
	{
		this.lastRule = lastRule;
	}

	public ShortUrl(String fromUrl, String toUrl, boolean lastRule)
	{
		this.fromUrl = fromUrl;
		this.toUrl = toUrl;
		this.lastRule = lastRule;
	}

	public boolean equals(Object another)
	{
		if (another instanceof ShortUrl)
		{
			return fromUrl.equals(((ShortUrl) another).getFromUrl())
					&& toUrl.equals(((ShortUrl) another).getToUrl())
					&& lastRule == ((ShortUrl) another).isLastRule();
		}
		return false;
	}


	public String toString(boolean useHTMLFormatting)
	{
		String carriageReturn = "\n";
		String tabChar = "\t";
		String lt = "<";
		String gt = ">";
		if (useHTMLFormatting)
		{
			carriageReturn = "<br/>";
			tabChar = "&nbsp;&nbsp;&nbsp;&nbsp;";
			lt = "&lt;";
			gt = "&gt;";
		}
		StringBuffer sb = new StringBuffer();
		sb.append(carriageReturn + lt + "rule" + gt + carriageReturn + tabChar + lt + "from" + gt + "^" + getFromUrl() + "$" + lt + "/from" + gt + carriageReturn + tabChar + lt + "to last=\"");
		if (isLastRule())
			sb.append("true");
		else
			sb.append("false");
		sb.append("\"" + gt + getToUrl() + "" + lt + "/to" + gt + carriageReturn + lt + "/rule" + gt);

		return sb.toString();
	}

}
