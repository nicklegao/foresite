package webdirector.seo;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * Created by IntelliJ IDEA. User: Administrator Date: 16/01/2009 Time: 14:00:07
 * To change this template use File | Settings | File Templates.
 */
public class UrlRewriteFileUtils
{

	private final static Logger logger = Logger.getLogger(UrlRewriteFileUtils.class);
	private static final String urlRewriteFileName = "/WEB-INF/urlrewrite.xml";

	private static ServletContext ctx = null;
	private String newlyGenerated = "";
	private int count = 0;

	public int getCount()
	{
		return count;
	}

	public UrlRewriteFileUtils()
	{
		super();
	}

	public static void setContext(ServletContext ctx1)
	{
		ctx = ctx1;
	}

	private String generateNewUrlString(String existingUrlRewriteFile)
	{
		String fileStart = "";
		String fileEnd = "";

		// find section which is dynamic denoted by
		String startComment = "<!-- ** DYNAMIC DO NOT CHANGE START ** -->";
		String endComment = "<!-- ** DYNAMIC DO NOT CHANGE END ** -->";
		int startingIndex = existingUrlRewriteFile.indexOf(startComment);
		fileStart = existingUrlRewriteFile.substring(0, startingIndex + 42);
		int endingIndex = existingUrlRewriteFile.indexOf(endComment);
		fileEnd = existingUrlRewriteFile.substring(endingIndex);

		return fileStart + newlyGenerated + fileEnd;
	}

	private String readInTheFile() throws Exception
	{
		String urlRewriteText = "";
		InputStream inputStream = ctx.getResourceAsStream(urlRewriteFileName);

		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
		String s = br.readLine();
		while (s != null)
		{
			// Process the line
			urlRewriteText += s + "\n";
			s = br.readLine();
		}

		inputStream.close();

		return urlRewriteText;
	}

	public void refreshShortUrls()
	{
		ShortUrlGenerator inst = ShortUrlGenerator.getInstance();
		newlyGenerated = inst.generate(inst.getModules());
		count = StringUtils.countMatches(newlyGenerated, "<rule>");
	}

	public void writeFile()
	{
		String urlRewriteText = "";
		try
		{
			urlRewriteText = readInTheFile();
			String newUrlRewriteText = generateNewUrlString(urlRewriteText);
			// get old customized rules from urlRewrite.xml

			String fileStart = "";
			String fileEnd = "";

			// find section which is dynamic denoted by
			String startComment = CustomizedShortUrlGenerator.SIGN_START;
			String endComment = CustomizedShortUrlGenerator.SIGN_END;
			int startingIndex = newUrlRewriteText.indexOf(startComment);
			fileStart = newUrlRewriteText.substring(0, startingIndex + startComment.length());
			int endingIndex = newUrlRewriteText.indexOf(endComment);
			fileEnd = newUrlRewriteText.substring(endingIndex);
			List<UrlMapping> newList = CustomizedShortUrlGenerator.readFile(ctx);
			newUrlRewriteText = fileStart + "\n" + CustomizedShortUrlGenerator.convertToString(newList) + fileEnd;
			count += newList.size();
//			logger.info("========================");
//			logger.info(newUrlRewriteText);
//			logger.info("========================");
			writeOutTheFile(urlRewriteFileName, newUrlRewriteText);
		}
		catch (Exception e)
		{
			logger.error("Tried to rewrite urlrewrite.xml file but access was denied: ", e);
			System.out.println("Tried to rewrite urlrewrite.xml file but access was denied: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private void writeOutTheFile(String urlRewriteFileName, String newUrlRewriteText) throws Exception
	{
		String tmpFileName = ctx.getRealPath(urlRewriteFileName);
		OutputStream outstream = new FileOutputStream(tmpFileName);

		PrintWriter pw = new PrintWriter(new BufferedOutputStream(outstream, 1024), true);
		pw.print(newUrlRewriteText);
		pw.flush();
		pw.close();
		outstream.close();
	}

	public String urlRulesToString(boolean useHTMLFormatting)
	{
		if (!useHTMLFormatting)
		{
			return CustomizedShortUrlGenerator.convertToString(CustomizedShortUrlGenerator.readFile(ctx)) + "\n" + newlyGenerated;
		}
		String ret = CustomizedShortUrlGenerator.convertToString(CustomizedShortUrlGenerator.readFile(ctx)) + "\n" + newlyGenerated;
		ret = StringUtils.replace(ret, "<", "&lt;");
		ret = StringUtils.replace(ret, ">", "&gt;");
		ret = StringUtils.replace(ret, " ", "&nbsp;");
		ret = StringUtils.replace(ret, "\n", "<br/>");
		return ret;
	}
}
