package webdirector.seo;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

public class CustomizedShortUrlGenerator
{

	private final static Logger logger = Logger.getLogger(CustomizedShortUrlGenerator.class);

	public final static String SIGN_START = "<!-- Dynamic change for customized url, do NOT change START -->";
	public final static String SIGN_END = "<!-- Dynamic change for customized url, do NOT change END -->";

	private static final String customizedFileName = "/WEB-INF/customizedUrls.xml";

	public static List<UrlMapping> readFile(ServletContext ctx)
	{
		List<UrlMapping> list = new ArrayList<UrlMapping>();
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new InputStreamReader(ctx.getResourceAsStream(customizedFileName)));
			StringBuffer sb = new StringBuffer();
			String line = "";
			for (line = br.readLine(); line != null; line = br.readLine())
			{
				sb.append(line + "\n");
			}
			List rules = DocumentHelper.parseText(sb.toString()).getRootElement().elements("rule");
			for (int i = 0; i < rules.size(); i++)
			{
				Element rule = (Element) rules.get(i);
				UrlMapping mapping = new UrlMapping("customized");
				mapping.setFrom(rule.elementText("from").substring(1, rule.elementText("from").length() - 1));
				mapping.setTo(rule.elementText("to"));
				mapping.setLast(rule.element("to").attributeValue("last"));
				mapping.setPermanentRedirect("permanent-redirect".equals(rule.element("to").attributeValue("type")));
				list.add(mapping);
			}
		}
		catch (Exception e)
		{
			logger.error("Error", e);
		}
		finally
		{
			if (br != null)
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
				}
			}
		}
		return list;
	}

	public static void writeFile(List<UrlMapping> mappings, ServletContext ctx)
	{
		PrintWriter pw = null;
		try
		{
			pw = new PrintWriter(new FileOutputStream(ctx.getRealPath(customizedFileName)));
			pw.print("<rules>\n");
			pw.print(convertToString(mappings));
			pw.print("</rules>\n");
			pw.flush();
		}
		catch (Exception e)
		{
			logger.error("Error", e);
		}
		finally
		{
			if (pw != null)
			{
				pw.close();
			}
		}

	}

	public static String convertToString(List<UrlMapping> mappings)
	{
		StringBuffer sb = new StringBuffer();
		for (UrlMapping mapping : mappings)
		{
			sb.append(mapping.toString());
		}
		return sb.toString();
	}
}
