package webdirector.seo;

import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import sbe.ShortUrls.ShortUrlManager;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;

public class ShortUrlGenerator
{

	private static final Logger logger = Logger.getLogger(ShortUrlGenerator.class);
	private static ShortUrlGenerator inst = new ShortUrlGenerator();

	private DataAccess da = DataAccess.getInstance();
	private DBaccess db = new DBaccess();

	private ShortUrlGenerator()
	{

	}

	public static ShortUrlGenerator getInstance()
	{
		return inst;
	}

	public Vector<String[]> getModules()
	{
		String sql1 = "select Category_id, ATTR_categoryName from categories_flex2 where category_parentid in (select category_id from categories_flex2 where attr_categoryname = 'SEO Settings')";
		//Vector<String[]> modules = db.select(sql1, new String[] { "a", "b" });
		// TODO validated
		Vector<String[]> modules = db.selectQuery(sql1);
		return modules;
	}

	public String generate(Vector<String[]> modules)
	{
		StringBuffer sb = new StringBuffer();
		for (String[] module : modules)
		{
			String configID = module[0];
			String moduleName = module[1];
			logger.info("update short url for " + moduleName);
			//Vector<String> usingSU = db.select("select ATTR_Value from elements_flex2 where category_id = " + configID + " and attr_headline = 'Short URLs - " + moduleName + "'");
			Vector<String> usingSU = db.selectQuerySingleCol("select ATTR_Value from elements_flex2 where category_id = ? and attr_headline = ?", new String[] { configID, "Short URLs - " + moduleName });
			if (usingSU.size() == 0 || !usingSU.get(0).equalsIgnoreCase("YES"))
			{
				logger.info("'Short URLs - " + moduleName + "' is OFF");
				continue;
			}
			sb.append("<!-- " + moduleName + " START -->\n");
			sb.append("<!-- " + moduleName + " GENERAL START -->\n");
			sb.append(getXml(generalShortUrl(configID, moduleName)));
			sb.append("<!-- " + moduleName + " GENERAL END -->\n");
			sb.append("<!-- " + moduleName + " SPECIFIC START -->\n");
			sb.append(getXml(specificShortUrl(configID, moduleName)));
			sb.append("<!-- " + moduleName + " SPECIFIC END -->\n");
			sb.append("<!-- " + moduleName + " END -->\n");
		}

		// after all the modules have been processed, append the last resort
		// short url mapping
		// thus if a general content asset name is "products" its generated
		// short url "/products"
		// will not overwrite the "/products" used by product catalogue
		for (String[] module : modules)
		{
			String configID = module[0];
			String moduleName = module[1];
			//Vector<String> usingSU = db.select("select ATTR_Value from elements_flex2 where category_id = " + configID + " and attr_headline = 'Short URLs - " + moduleName + "'");
			Vector<String> usingSU = db.selectQuerySingleCol("select ATTR_Value from elements_flex2 where category_id = ? and attr_headline = ? ", new String[] { configID, "Short URLs - " + moduleName });
			if (usingSU.size() == 0 || !usingSU.get(0).equalsIgnoreCase("YES"))
			{
				logger.info("'Short URLs - " + moduleName + "' is OFF");
				continue;
			}
			sb.append("<!-- " + moduleName + " LAST RESORT START -->\n");
			sb.append(getXml(lastResortShortUrl(configID, moduleName)));
			sb.append("<!-- " + moduleName + " LAST RESORT END -->\n");
		}
		return sb.toString();
	}

	public Map<String, UrlMapping> generalShortUrl(String configID, String moduleName)
	{
		String sql2 = "select ATTR_Headline, ATTR_Value from elements_flex2 where category_id = ? and live = 1 and (attr_headline like '%Long Mapping' or attr_headline like '%Short Mapping') order by display_order";
		//Vector<String[]> urls = db.select(sql2, new String[] { "a", "b" });
		Vector<String[]> urls = db.selectQuery(sql2, new String[] { configID });
		Map<String, UrlMapping> mappings = new LinkedHashMap<String, UrlMapping>();
		for (String[] url : urls)
		{
			url[1] = StringUtils.replace(url[1], "&amp;", "&");
			UrlMapping mapping = null;
			String name = url[0].substring(0, url[0].indexOf(" - "));
			if (mappings.containsKey(name))
			{
				mapping = mappings.get(name);
			}
			else
			{
				mapping = new UrlMapping(name);
				mappings.put(name, mapping);
			}
			if (url[0].indexOf("Long") != -1)
			{
				mapping.setTo(url[1]);
			}
			else
			{
				mapping.setFrom(url[1]);
			}
		}
		return mappings;
	}

	public void getShortUrlMappings(ServletContext ctx)
	{

		ShortUrlGenerator inst = ShortUrlGenerator.getInstance();
		Vector<String[]> modules = inst.getModules();
		for (String[] module : modules)
		{
			Map<String, UrlMapping> mappings = inst.generalShortUrl(module[0], module[1]);
			int depth = mappings.size();
			for (UrlMapping mapping : mappings.values())
			{
				ShortUrl rule = new ShortUrl(mapping.getFrom(), mapping.getTo(), true);
				ctx.setAttribute(module[1] + " - Map" + depth, rule);
				depth--;
			}
		}
	}

	private String getXml(Map<String, UrlMapping> mappings)
	{
		StringBuffer sb = new StringBuffer();
		for (UrlMapping mapping : mappings.values())
		{
			sb.append(mapping.toString());
		}
		return sb.toString();
	}

	private Map<String, UrlMapping> specificShortUrl(String configID, String moduleName)
	{
		String sql = "select ATTR_Headline, ATTR_Value from elements_flex2 where category_id = ? and live = 1 and (attr_headline like '%Specific Mapping') order by display_order";

		// only support level in 1,2.
		int moduleLevel = Math.min(Defaults.getInstance().getModuleLevelsInt(moduleName), 2);
		// only support level in 1,2.

		//Vector<String[]> urls = db.select(sql, new String[] { "a", "b" });
		Vector<String[]> urls = db.selectQuery(sql, new String[] { configID });
		Map<String, UrlMapping> mappings = new LinkedHashMap<String, UrlMapping>();
		for (String[] url : urls)
		{
			String name = url[0].substring(0, url[0].indexOf(" - "));
			url[1] = StringUtils.replace(url[1], "&amp;", "&");
			String[] variables = StringUtils.substringsBetween(url[1], "{", "}");
			if (variables == null)
			{
				continue;
			}
			List<Hashtable<String, String>> list = getData(name, moduleName, variables, moduleLevel);
			if (list == null)
			{
				continue;
			}
			for (Hashtable<String, String> ele : list)
			{
				String to = url[1];
				for (String variable : variables)
				{
					to = StringUtils.replace(to, "{" + variable + "}", ele.get(variable).replace("&", "%26"));
				}
				String[] urlFrom = new String[moduleLevel + 1];
				for (int i = 0; i < moduleLevel; i++)
				{
					urlFrom[i] = formatUrl(ele.get("c" + (i + 1) + "url"));
				}
				urlFrom[moduleLevel] = formatUrl(ele.get("ATTR_shortUrl"));
				String from = StringUtils.join(urlFrom);
				{
					UrlMapping mapping = new UrlMapping(from);
					mapping.setFrom(from);
					mapping.setTo(to);
					mappings.put(mapping.getName(), mapping);
				}
				if (name.equalsIgnoreCase("asset"))
				{
					UrlMapping mapping = new UrlMapping(urlFrom[moduleLevel]);
					mapping.setFrom(urlFrom[moduleLevel]);
					mapping.setTo(to);
					mappings.put(mapping.getName(), mapping);
				}
				else if (name.equalsIgnoreCase("category 1"))
				{
					UrlMapping mapping = new UrlMapping(urlFrom[0]);
					mapping.setFrom(urlFrom[0]);
					mapping.setTo(to);
					mappings.put(mapping.getName(), mapping);
				}
				else if (name.equalsIgnoreCase("category 2"))
				{
					UrlMapping mapping = new UrlMapping(urlFrom[1]);
					mapping.setFrom(urlFrom[1]);
					mapping.setTo(to);
					mappings.put(mapping.getName(), mapping);
				}
			}
		}
		return mappings;
	}

	private List<Hashtable<String, String>> getData(String name, String moduleName, String[] variables, int moduleLevel)
	{
		boolean cateSUAvailable = shortUrlAvailable(moduleName, "categories");
		boolean elementSUAvailable = shortUrlAvailable(moduleName, "elements");

		String sqlDetail = null;
		if (name.equalsIgnoreCase("asset") && elementSUAvailable)
		{
			if (moduleLevel == 1)
			{
				sqlDetail = " select c1.Category_id as categoryID, e.Element_id as elementID, e.ATTR_shortUrl";
				if (cateSUAvailable)
				{
					sqlDetail += " , c1.attr_shorturl as c1url";
				}
				for (String variable : variables)
				{
					if (variable.equalsIgnoreCase("categoryID") || variable.equalsIgnoreCase("childCategoryID") || variable.equalsIgnoreCase("elementID"))
					{
						continue;
					}
					sqlDetail += " , e." + variable;
				}
				sqlDetail += " from categories_" + moduleName + " c1, elements_" + moduleName + " e ";
				sqlDetail += " where c1.category_id = e.category_id and e.attr_shorturl is not null and e.attr_shorturl != ''";
			}
			else
			{
				sqlDetail = " select c1.Category_id as categoryID, c2.Category_id as childCategoryID, e.Element_id as elementID, e.ATTR_shortUrl";
				if (cateSUAvailable)
				{
					sqlDetail += " , c1.attr_shorturl as c1url, c2.attr_shorturl as c2url";
				}
				for (String variable : variables)
				{
					if (variable.equalsIgnoreCase("categoryID") || variable.equalsIgnoreCase("childCategoryID") || variable.equalsIgnoreCase("elementID"))
					{
						continue;
					}
					sqlDetail += " , e." + variable;
				}
				sqlDetail += " from categories_" + moduleName + " c1, categories_" + moduleName + " c2, elements_" + moduleName + " e ";
				sqlDetail += " where c2.category_id = e.category_id and c1.category_id = c2.category_parentid and e.attr_shorturl is not null and e.attr_shorturl != ''";
			}
		}
		else if (name.equalsIgnoreCase("category 1") && cateSUAvailable)
		{
			sqlDetail = " select c1.Category_id as categoryID, c1.attr_shorturl as c1url";
			for (String variable : variables)
			{
				if (variable.equalsIgnoreCase("categoryID"))
				{
					continue;
				}
				sqlDetail += " , c1." + variable;
			}
			sqlDetail += " from categories_" + moduleName + " c1 ";
			sqlDetail += " where c1.folderLevel = 1 and c1.attr_shorturl is not null and c1.attr_shorturl != ''";
		}
		else if (name.equalsIgnoreCase("category 2") && cateSUAvailable)
		{
			sqlDetail = " select c1.Category_id as categoryID, c2.Category_id as childCategoryID , c1.attr_shorturl as c1url, c2.attr_shorturl as c2url";
			for (String variable : variables)
			{
				if (variable.equals("categoryID") || variable.equals("childCategoryID"))
				{
					continue;
				}
				sqlDetail += " , c2." + variable;
			}
			sqlDetail += " from categories_" + moduleName + " c1, categories_" + moduleName + " c2 ";
			sqlDetail += " where c1.category_id = c2.category_parentid and c1.folderLevel = 1 and c2.folderLevel = 2 and c2.attr_shorturl is not null and c2.attr_shorturl != ''";
		}
		logger.info("specificShortUrl sql:");
		logger.info(sqlDetail);
		if (sqlDetail != null)
		{
			// Called during server startup
			return da.select(sqlDetail, new HashtableMapper());
		}
		return null;
	}

	private Map<String, UrlMapping> lastResortShortUrl(String configID, String moduleName)
	{
		Map<String, UrlMapping> result = new LinkedHashMap<String, UrlMapping>();

		// only support level in 1,2.
		int moduleLevel = Math.min(Defaults.getInstance().getModuleLevelsInt(moduleName), 2);
		// only support level in 1,2.

		String sql = "select ATTR_Headline, ATTR_Value from elements_flex2 where category_id = ? and live = 1 and (attr_headline like '%Last Resort Mapping') order by display_order";
		//Vector<String[]> urls = db.select(sql, new String[2]);
		Vector<String[]> urls = db.selectQuery(sql, new String[] { configID });
		for (String[] url : urls)
		{
			String name = url[0].substring(0, url[0].indexOf(" - "));
			url[1] = StringUtils.replace(url[1], "&amp;", "&");
			String[] variables = StringUtils.substringsBetween(url[1], "{", "}");
			if (variables == null)
			{
				continue;
			}

			String sqlDetail = null;

			if (name.equalsIgnoreCase("asset"))
			{
				if (moduleLevel == 1)
				{
					sqlDetail = "select c1.Category_id as categoryID, e.Element_id as elementID, e.ATTR_headline, c1.ATTR_categoryName as categoryName";

					for (String variable : variables)
					{
						if (variable.equalsIgnoreCase("categoryID") || variable.equalsIgnoreCase("childCategoryID") || variable.equalsIgnoreCase("elementID") || variable.equalsIgnoreCase("ATTR_headline"))
						{
							continue;
						}
						sqlDetail += " , e." + variable;
					}

					sqlDetail += " from categories_" + moduleName + " c1, elements_" + moduleName + " e ";
					sqlDetail += " where c1.category_id = e.category_id";
				}
				else
				{
					sqlDetail = " select c1.Category_id as categoryID, c2.Category_id as childCategoryID, e.Element_id as elementID, e.ATTR_headline as ATTR_headline, c1.ATTR_categoryName as categoryName, c2.ATTR_categoryName as childCategoryName";

					for (String variable : variables)
					{
						if (variable.equalsIgnoreCase("categoryID") || variable.equalsIgnoreCase("childCategoryID") || variable.equalsIgnoreCase("elementID") || variable.equalsIgnoreCase("ATTR_headline"))
						{
							continue;
						}
						sqlDetail += " , e." + variable;
					}

					sqlDetail += " from categories_" + moduleName + " c1, categories_" + moduleName + " c2, elements_" + moduleName + " e ";
					sqlDetail += " where c2.category_id = e.category_id and c1.category_id = c2.category_parentid";
				}
			}
			else if (name.equalsIgnoreCase("category 1"))
			{
				sqlDetail = " select c1.Category_id as categoryID, c1.ATTR_categoryName as categoryName";

				for (String variable : variables)
				{
					if (variable.equalsIgnoreCase("categoryID") || variable.equalsIgnoreCase("categoryName"))
					{
						continue;
					}
					sqlDetail += " , c1." + variable;
				}

				sqlDetail += " from categories_" + moduleName + " c1 ";
				sqlDetail += " where c1.folderLevel = 1";
			}
			else if (name.equalsIgnoreCase("category 2"))
			{
				sqlDetail = " select c1.Category_id as categoryID, c2.Category_id as childCategoryID, c1.ATTR_categoryName as categoryName, c2.ATTR_categoryName as childCategoryName";

				for (String variable : variables)
				{
					if (variable.equalsIgnoreCase("categoryID") || variable.equalsIgnoreCase("childCategoryID") || variable.equalsIgnoreCase("categoryName") || variable.equalsIgnoreCase("childCategoryName"))
					{
						continue;
					}
					sqlDetail += " , c2." + variable;
				}

				sqlDetail += " from categories_" + moduleName + " c1, categories_" + moduleName + " c2 ";
				sqlDetail += " where c1.category_id = c2.category_parentid and c1.folderLevel = 1 and c2.folderLevel = 2";
			}

			if (sqlDetail != null)
			{
				logger.info("lastResortShortUrl sql:");
				logger.info(sqlDetail);
				List<Hashtable<String, String>> list = da.select(sqlDetail, new HashtableMapper());

				if (list == null)
				{
					continue;
				}

				for (Hashtable<String, String> ele : list)
				{
					String to = url[1];
					for (String variable : variables)
					{
						to = StringUtils.replace(to, "{" + variable + "}", ele.get(variable));
					}
					String[] urlFrom = new String[moduleLevel + 1];
					for (int i = 0; i < moduleLevel; i++)
					{
						if (i == 0)
						{
							urlFrom[0] = formatUrl(ShortUrlManager.getInstance().friendly(ele.get("categoryName")));
						}
						else if (i == 1)
						{
							urlFrom[1] = formatUrl(ShortUrlManager.getInstance().friendly(ele.get("childCategoryName")));
						}
					}
					urlFrom[moduleLevel] = formatUrl(ShortUrlManager.getInstance().friendly(ele.get("ATTR_headline")));
					String from = StringUtils.join(urlFrom);
					// always build from full path to last resort
					{
						UrlMapping mapping = new UrlMapping(from);
						mapping.setFrom(from);
						mapping.setTo(to);
						result.put(mapping.getName(), mapping);
					}
					// build /[FRIENDLY_ASSET_NAME] to last resort
					if (name.equalsIgnoreCase("asset"))
					{
						UrlMapping mapping = new UrlMapping(urlFrom[moduleLevel]);
						mapping.setFrom(urlFrom[moduleLevel]);
						mapping.setTo(to);
						result.put(mapping.getName(), mapping);
					}
					else if (name.equalsIgnoreCase("category 1"))
					{
						// build /[FRIENDLY_CATEGORY_NAME]/[FRIENDLY_ASSET_NAME]
						// to last resort
						UrlMapping mapping = new UrlMapping(urlFrom[0]);
						mapping.setFrom(urlFrom[0]);
						mapping.setTo(to);
						result.put(mapping.getName(), mapping);
					}
					else if (name.equalsIgnoreCase("category 2"))
					{
						// build
						// /[FRIENDLY_CATEGORY_NAME]/[FRIENDLY_CHILD_CATEGORY_NAME]/[FRIENDLY_ASSET_NAME]
						// to last resort
						UrlMapping mapping = new UrlMapping(urlFrom[1]);
						mapping.setFrom(urlFrom[1]);
						mapping.setTo(to);
						result.put(mapping.getName(), mapping);
					}
				}
			}
		}

		return result;
	}

	private String formatUrl(String url)
	{
		if (url == null || url.equals("/"))
		{
			return "";
		}
		String ret = url;
		if (!ret.startsWith("/"))
		{
			ret = "/" + ret;
		}
		if (ret.endsWith("/"))
		{
			ret = ret.substring(0, ret.length() - 1);
		}
		return ret;
	}

	private boolean shortUrlAvailable(String moduleName, String type)
	{

		String check = "select count(1) from " + DatabaseValidation.encodeParam(type) + "_" + DatabaseValidation.encodeParam(moduleName) + " where attr_shorturl != ''";
		try
		{
			//Vector v = db.select(check);
			Vector v = db.selectQuerySingleCol(check);
			return (v != null && v.size() > 0);
		}
		catch (Exception e)
		{
			return false;
		}
	}

	public static void main(String[] args)
	{

		System.out.println(StringUtils.join(StringUtils.substringsBetween("abc[d]ef[g]hi[j]klmn", "[", "]"), ","));
		System.out.println(StringUtils.substringsBetween("abcklmn", "[", "]"));
	}
}
