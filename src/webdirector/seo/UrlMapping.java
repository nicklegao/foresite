package webdirector.seo;

public class UrlMapping
{
	String name;
	String to;
	String from;
	String last = "true";
	boolean permanentRedirect = false;

	public boolean isPermanentRedirect()
	{
		return permanentRedirect;
	}

	public void setPermanentRedirect(boolean permanentRedirect)
	{
		this.permanentRedirect = permanentRedirect;
	}

	public String getLast()
	{
		return last;
	}

	public void setLast(String last)
	{
		this.last = last;
	}

	public UrlMapping(String name)
	{
		super();
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getTo()
	{
		return to;
	}

	public void setTo(String to)
	{
		this.to = to;
	}

	public String getFrom()
	{
		return from;
	}

	public void setFrom(String from)
	{
		this.from = from;
	}

	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		try
		{
			sb.append("<rule>\n");
			sb.append("    <from>^" + getFrom().replace("&", "&amp;").replace("&amp;amp;", "&amp;") + "$</from>\n");
			sb.append("    <to " + (isPermanentRedirect() ? "type=\"permanent-redirect\" " : "") + "last=\"" + getLast() + "\">" + getTo().replace("&", "&amp;").replace("&amp;amp;", "&amp;") + "</to>\n");
			sb.append("</rule>\n");
			return sb.toString();
		}
		catch (Exception e)
		{
			return "";
		}
	}
}