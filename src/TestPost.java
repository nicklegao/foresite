import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 21/08/2008
 * Time: 10:52:29
 * To change this template use File | Settings | File Templates.
 */
public class TestPost extends HttpServlet
{

	/**
	 * Simple catch all wrapper to ensure either POST or GET are received and
	 * processed
	 * 
	 * @param req
	 *            HttpServletRequest
	 * @param resp
	 *            HttpServletResponse
	 * @throws java.io.IOException
	 * @throws javax.servlet.ServletException
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException
	{
		InputStream is = req.getInputStream();

		System.out.println("Receive POST FIRING >>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
		processScanResultXML(is);
		is.close();
		System.out.println("Receive POST FIRED >>>>>>>>>>>>>>>>>>>>>>>>>>>> ");

		return;
	}

	/**
	 * Receives the XML payload from XY and passes it on for processing
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException
	{
		InputStream is = req.getInputStream();

		System.out.println("Receive GET FIRING >>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
		processScanResultXML(is);
		is.close();
		System.out.println("Receive GET FIRED >>>>>>>>>>>>>>>>>>>>>>>>>>>> ");

		return;
	}

	/**
	 * Reads the XML payload into a XML document and processes the nodes placing
	 * them into a Hashtable
	 * If a bad score (zero,blank,null) is given then 2 is hardcoded to avoid
	 * users going offline if XY service responds with these type of scan
	 * responses
	 * Next an appropriate method is called to update the relevant table based
	 * on the incoming XML
	 * 
	 * @param is
	 */
	private void processScanResultXML(InputStream is) throws IOException
	{
		java.io.InputStreamReader ins = new java.io.InputStreamReader(is);
		java.io.BufferedReader bufReader = new java.io.BufferedReader(ins);
		String strOutput = null;
		do
		{
			strOutput = bufReader.readLine();
			if (strOutput != null)
			{
				System.out.println(">> " + strOutput + "\n");
			}
		} while (strOutput != null);
	}


}
