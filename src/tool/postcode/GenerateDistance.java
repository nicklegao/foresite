package tool.postcode;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import webdirector.utils.PostCodeUtils;

public class GenerateDistance
{

	static DBaccess db = new DBaccess(true);

	public static void calc()
	{
		int count = 0;
		long start = System.currentTimeMillis();
		List<String> values = new ArrayList<String>();
		List<String[]> pcs = getPostcodes();
		int total = pcs.size() * (pcs.size() - 1);
		for (int i = 0; i < pcs.size(); i++)
		{
			String[] pca = pcs.get(i);
			for (int j = i + 1; j < pcs.size(); j++)
			{
				String[] pcb = pcs.get(j);
				double distance = PostCodeUtils.PostCodeDistCalc(Double.parseDouble(pca[1]), Double.parseDouble(pca[0]), Double.parseDouble(pcb[1]), Double.parseDouble(pcb[0]));
				// System.out.println(pca[2] + " - " + pcb[2] + " = " +
				// distance);
				values.add("('" + pca[2] + "', '" + pcb[2] + "', '" + (int) distance + "')");
				values.add("('" + pcb[2] + "', '" + pca[2] + "', '" + (int) distance + "')");
				count += 2;
				if (values.size() > 2000)
				{
					String sql = "insert into new_pc_postcodedistancetable ( LocationAPostCode, LocationBPostCode, DistanceInMetres) values " + StringUtils.join(values, ",");
					db.updateData(sql);
					values.clear();
					System.out.println(count + " / " + total + " spent " + ((System.currentTimeMillis() - start) / 1000) + "s");
				}
			}
		}
		if (values.size() > 0)
		{
			String sql = "insert into new_pc_postcodedistancetable ( LocationAPostCode, LocationBPostCode, DistanceInMetres) values " + StringUtils.join(values, ",");
			db.updateData(sql);
			values.clear();
			System.out.println(count + " / " + total + " spent " + ((System.currentTimeMillis() - start) / 1000) + "s");
		}
	}

	public static void calc2()
	{
		PrintWriter pw = null;
		try
		{
			pw = new PrintWriter(new FileWriter(new File("/temp/distance.txt")));
			int count = 0;
			long start = System.currentTimeMillis();
			List<String> values = new ArrayList<String>();
			List<String[]> pcs = getPostcodes();
			int total = pcs.size() * (pcs.size() - 1);
			for (int i = 0; i < pcs.size(); i++)
			{
				String[] pca = pcs.get(i);
				for (int j = i + 1; j < pcs.size(); j++)
				{
					String[] pcb = pcs.get(j);
					double distance = PostCodeUtils.PostCodeDistCalc(Double.parseDouble(pca[1]), Double.parseDouble(pca[0]), Double.parseDouble(pcb[1]), Double.parseDouble(pcb[0]));
					// System.out.println(pca[2] + " - " + pcb[2] + " = " +
					// distance);
					pw.println((++count) + "\t" + pca[2] + "\t" + pcb[2] + "\t" + (int) distance);
					pw.println((++count) + "\t" + pcb[2] + "\t" + pca[2] + "\t" + (int) distance);
					if (count % 2000 == 0)
					{
						System.out.println(count + " / " + total + " spent " + ((System.currentTimeMillis() - start) / 1000) + "s");
					}
				}
			}
			System.out.println(count + " / " + total + " spent " + ((System.currentTimeMillis() - start) / 1000) + "s");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (pw != null)
			{
				pw.close();
			}
		}
	}

	private static List<String[]> getPostcodes()
	{
		String sql = "select lat, `long`, postCode from new_pc_postcode";
		return db.select(sql, new String[] {});
	}
}
