import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import webdirector.db.client.UserDetails;
import au.net.webdirector.admin.security.LoginController;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;


public class treeServlet extends HttpServlet
{
	String name = null;
	String model = null;
	String category = null;
	String category_id = null;
	String storeDir = null;
	private au.net.webdirector.common.Defaults d = au.net.webdirector.common.Defaults.getInstance();
	String brand = null;
	private DBaccess db = new DBaccess();


	public void init()
	{
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String cmd = (String) request.getParameter("cmd");
		String module = (String) request.getParameter("module");
		String pCatId = (String) request.getParameter("pCatID");
		String u = (String) request.getSession().getAttribute(LoginController.WD_USER);
		UserDetails user = new UserDetails(u);
		String userId = user.getUser_id();
		String userLevelId = user.getUserLevel_id();
		String moduleName = (String) request.getSession().getAttribute("moduleSelected");
		String ret = null;
		if (cmd.equals("getAll"))
		{
			ret = getFullTreeDetails(module, pCatId, userId, userLevelId, moduleName);
		}
		else if (cmd.equals("getCat"))
		{
			ret = getProductDetails(module, pCatId);
		}
		response.setContentType("text/html; charset=utf-8");
		try
		{
			OutputStream ostr = response.getOutputStream();
			ostr.write(ret.getBytes());
			ostr.flush();
		}
		catch (Exception ex)
		{
			System.out.println("Exception occured writing back from treeServlet");
			ex.printStackTrace();
		}
	}

	private String getProductDetails(String module, String pCatId)
	{
		System.out.println("XML for PRODUCTS is being built");
		String query = "select Element_id, ATTR_Headline, Category_id from Elements_" + DatabaseValidation.encodeParam(module) + " where Category_id=? order by ATTR_Headline";
		String[] cols = { "Element_id", "ATTR_Headline", "Category_id" };
		//Vector v = db.select(query, cols);
		Vector v = db.selectQuery(query, new String[] { pCatId });

		StringBuffer sb = new StringBuffer();
		if (v != null)
		{
//            sb.append("<!DOCTYPE tree SYSTEM \"http://localhost:8080/SBE/MyDTD.dtd\">");

//			System.out.println("not null"+v.size());
			sb.append("<tree>\n");
			for (int i = 0; i < v.size(); i++)
			{
				String[] data = (String[]) v.elementAt(i);

				// the c attribute is the category id of the product
				sb.append("<entry name=\"" + data[1] + "\" id='" + data[0] + "' c='" + data[2] + "' />\n");
//				System.out.println("1"+data[0]);
//				System.out.println("1"+data[1]);
			}
			sb.append("</tree>\n");
		}
		else
			System.out.println("possibly null");

		System.out.println("XML for PRODUCTS has been built: " + sb.length());

		return (sb.toString());
	}

	private String getFullTreeDetails(String module, String pCatId, String userId, String userLevelId, String moduleName)
	{
		module = DatabaseValidation.encodeParam(module);
		pCatId = DatabaseValidation.encodeParam(pCatId);

		String query = null;
		String extraQuery = "select count(*) from categories_" + module + " c1 where c1.Category_parentID = ";

		String maxLevels = d.getModuleLevels(module);
		boolean atMaxLevel = false;
		// are we dealing with the bottom level category, i.e. check the level of the category we're looking at, against the
		// max levels for this module
		query = "select folderLevel from categories_" + module + " c2 where Category_parentID= ?";
		//Vector folder = db.select(query, new String[]{pCatId});
		Vector folder = db.selectQuery(query, new String[] { pCatId });
		if (folder != null && folder.size() > 0)
		{
			if (((String) folder.elementAt(0)).equals(maxLevels))
				atMaxLevel = true;
		}
		if (atMaxLevel)
			extraQuery = "select count(*) from elements_" + module + " c1 where c1.Category_id = ";

		System.out.println("XML TREE is being built");
		String[] cols = { "ATTR_categoryName", "category_id", "category_parentID", "ATTR_categoryDesc", "folderLevel", "childCount" };
		if ("0".equals(pCatId) && !"1".equals(userLevelId))
		{
			query = "select ATTR_categoryName, category_id, category_parentID, ATTR_categoryDesc, " +
					"folderLevel, " +
					"( " + extraQuery + "c2.category_id)" +
					" from categories_" + module + " c2 where Category_parentID=? " +
					"and category_id in (select category_id from user_module_access where user_id= ? and module_name=?)" +
					"order by folderLevel, ATTR_categoryName";
			//Vector v1 = db.select(query, cols);
			Vector v1 = db.selectQuery(query, new String[] { pCatId, userId, moduleName });
			if (v1 != null && v1.size() == 0)
			{
				query = "select ATTR_categoryName, category_id, category_parentID, ATTR_categoryDesc, " +
						"folderLevel, (" + extraQuery + " c2.category_id)" +
						" from categories_" + module + " c2 where Category_parentID='" + pCatId + "' " +
						"order by folderLevel, ATTR_categoryName";
			}
		}
		else
		{
			query = "select ATTR_categoryName, category_id, category_parentID, ATTR_categoryDesc, " +
					"folderLevel, (" + extraQuery + " c2.category_id)" +
					" from categories_" + module + " c2 where Category_parentID='" + pCatId + "' " +
					"order by folderLevel, ATTR_categoryName";
		}


		//Vector v = db.select(query, cols);
		Vector v = db.selectQuery(query);

		StringBuffer sb = new StringBuffer();
		if (v != null)
		{
//            sb.append("<!DOCTYPE tree SYSTEM \"http://localhost:8080/SBE/MyDTD.dtd\">");
			sb.append("<tree>\n");
			for (int i = 0; i < v.size(); i++)
			{
				String[] data = (String[]) v.elementAt(i);
				sb.append("<entry name=\"" + data[0] + "\" id='" + data[1] + "' parent='" + data[2] + "' level='" + data[4] + "' childCount='" + data[5] + "' />\n");
			}
			sb.append("</tree>\n");
		}

		System.out.println("XML TREE has been built: " + sb.length());
		System.out.println("XML : " + sb.toString());
		return (sb.toString());
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
//		if ( d.getDEBUG() )
//			System.out.println("Calling GET in treeServlet");

		doPost(request, response);
	}
}
