import javax.servlet.http.HttpServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import webdirector.seo.ShortUrlGenerator;
import webdirector.seo.UrlRewriteFileUtils;
import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;

/**
 * Created by IntelliJ IDEA. User: CorpInteractive Date: 28/04/2008 Time:
 * 10:20:35 To change this template use File | Settings | File Templates.
 */
public class startupUtility extends HttpServlet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4355067656249608332L;

	public void init(ServletConfig config) throws ServletException
	{
		DataAccess.setUpInstance(config.getServletContext());
		Defaults.getInstance().setProperties(config.getServletContext());
		ShortUrlGenerator.getInstance().getShortUrlMappings(config.getServletContext());
		UrlRewriteFileUtils.setContext(config.getServletContext());
		UrlRewriteFileUtils urfu = new UrlRewriteFileUtils();
		urfu.refreshShortUrls();
		urfu.writeFile();
	}

}
