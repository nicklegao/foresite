import webdirector.db.DButils;
import webdirector.db.client.ClientDataAccess;
import webdirector.utils.DateUtils;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import java.util.*;
import java.io.IOException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.apache.commons.lang.StringEscapeUtils;

import au.net.webdirector.common.datalayer.util.DatabaseValidation;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

/**
 * Created by IntelliJ IDEA.
 * User: CorpInteractive
 * Date: 27/01/2009
 * Time: 15:26:54
 * To change this template use File | Settings | File Templates.
 */
public class RSSFeed extends HttpServlet
{
	private DButils db = new DButils();

	//Hash of attributes from labelstable  which will contains internal and external name base upon export selection
	// on editAttributes from web director.
	private LinkedHashMap hAttributes;
	private ClientDataAccess clientDataAccess = new ClientDataAccess();
	private DButils dButils = new DButils();
	private String module = "NEWS";
	private String version = "2.0";
	private au.net.webdirector.common.Defaults d = au.net.webdirector.common.Defaults.getInstance();
	private webdirector.db.client.ClientFileUtils CF = new webdirector.db.client.ClientFileUtils();
	Document dom;

	public void init()
	{


	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("in Doget Method");
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In dopost Method:::>>>>>>");


		//Get a DOM object
		createDocument();

		// initialize the list
		Vector data = loadData(module);

		//Create XML file....
		createDOMTree(data, module);

		// Crate response
		printToFile(response);

		// response back to caller using iostream


	}

	/**
	 * load data into vector which we need to dispaly in Items of RSS feed
	 * 
	 * @param module
	 * @return
	 */
	private Vector loadData(String module)
	{
		Vector vXMLData = new Vector();
		//vXMLData = clientDataAccess.getAllElements(module);
		String cols[] = { "Element_id", "Category_id", "ATTR_Headline", "Live_date", "ATTR_IssueNumber", "ATTRLONG_Teaser", "ATTRFILE_Attachment" };
		String query = "Select Element_id, Category_id, ATTR_Headline,Live_date,ATTR_IssueNumber,ATTRLONG_Teaser,ATTRFILE_Attachment " +
				" from Elements_" + DatabaseValidation.encodeParam(module) + " where live = 1";

		//vXMLData = dButils.select(query,cols);
		vXMLData = dButils.selectQuery(query);

		return vXMLData;
	}

	/**
	 * Using JAXP in implementation independent manner create a document object
	 * using which we create a xml tree in memory
	 */
	private void createDocument()
	{

		//get an instance of factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try
		{
			//get an instance of builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			//create an instance of DOM
			dom = db.newDocument();

		}
		catch (ParserConfigurationException pce)
		{
			//dump it
			System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
			System.exit(1);
		}

	}

	/**
	 * This method will return latest publish date for news article which is to
	 * find latest category in news module.
	 * 
	 * @return
	 */
	private String getPubDate()
	{
		Vector vCategory = clientDataAccess.getAllCategories(module);
		Hashtable hCategory = new Hashtable();
		List list = new ArrayList();
		String categoryName = "";
		if (vCategory != null && vCategory.size() > 0)
		{
			for (int i = 0; i < vCategory.size(); i++)
			{
				hCategory = (Hashtable) vCategory.get(i);
				categoryName = (String) hCategory.get("ATTR_categoryName");
				System.out.println("CategoryName:::" + categoryName);
				if (checkForStartWithNumber(categoryName))
				{
					list.add(categoryName);
				}
			}
		}

		Collections.sort(list);
		System.out.println("Final publish date :::" + list.get(0));
		for (int i = 0; i < list.size(); i++)
		{
			System.out.println("List " + i + " value ::" + list.get(i));
		}

		return (list.size() > 1) ? (String) list.get(list.size() - 1) : "";
	}


	private boolean checkForStartWithNumber(String data)
	{
		String number = "0123456789";
		String startChar = data.substring(0, 1);

		return number.contains(startChar);
	}

	/**
	 * The real workhorse which creates the XML structure
	 */
	private void createDOMTree(Vector myData, String module)
	{

		//create the root element <Root>
		Element rootEle = dom.createElement("rss");
		rootEle.setAttribute("version", version);
		rootEle.setAttribute("encoding", "UTF-8");
		Text text;
		dom.appendChild(rootEle);

		// create channel tag
		Element channel = dom.createElement("channel");

		// create title
		Element title = dom.createElement("title");
		text = dom.createTextNode("Daily Addict");
		title.appendChild(text);

		// create Link
		Element link = dom.createElement("link");
		text = dom.createTextNode("http://www.dailyaddict.com.au/");
		link.appendChild(text);

		// create description
		Element description = dom.createElement("description");
		text = dom.createTextNode("Daily Addict RSS feed");
		description.appendChild(text);

		// pubDate description
		Element pubDateC = dom.createElement("pubDate");
		text = dom.createTextNode(DateUtils.covertDateRFCFormate(getPubDate(), "yyyy-MM-dd"));
		pubDateC.appendChild(text);

		channel.appendChild(title);
		channel.appendChild(link);
		channel.appendChild(description);
		channel.appendChild(pubDateC);

		// Run thorugh mydata vector which  essentially contains all informations including system coloumns of assets for XML .....
		for (int i = 0; i < myData.size(); i++)
		{
			String data[] = (String[]) myData.get(i);

			// create item element
			Element asset = dom.createElement("item");
			String id = data[0];
			System.out.println("Element_ID in crateDomTree " + id);
			String createDate = data[3]; // live date
			String headline = data[2]; //
			headline = StringEscapeUtils.unescapeXml(headline);
			headline = StringEscapeUtils.unescapeXml(headline);
			System.out.println("BMDA headline::::" + headline + "row ::::" + data[2]);
			String issueId = data[4];
			String itemSrc = data[5];
			String itemImage = data[6];
			String catId = data[1];
			System.out.println("Create_date in crateDomTree " + createDate);

			// create Headline
			Element subTitle = dom.createElement("title");
			text = dom.createTextNode(headline);
			subTitle.appendChild(text);

			// create pubDate
			Element pubDate = dom.createElement("pubDate");
			text = dom.createTextNode(DateUtils.covertDateRFCFormate(createDate));
			pubDate.appendChild(text);

			// create pubDate
			Element issue = dom.createElement("guid");
			text = dom.createTextNode(id + issueId);
			issue.appendChild(text);

			String itemDescripion = CF.readTextContents(CF.getFilePathFromStoreName(itemSrc)).toString();

			/*if(itemDescripion!=null && itemDescripion.length()>1000){
			     itemDescripion = itemDescripion.substring(0,1000);
			 }*/

			// Description
			Element body = dom.createElement("description");
			String bodyDescription = "";
			try
			{
				if (itemImage != null && !itemImage.equals("null") && !itemImage.equals(""))
				{
					bodyDescription = bodyDescription + "<table><Tr><td>";
					bodyDescription = bodyDescription + "<img src=" + d.getWebSiteURL() + clientDataAccess.imgThumb(itemImage).replaceAll(" ", "%20") + ">";
					bodyDescription = bodyDescription + "</td><td>";
					bodyDescription = "<p>" + (bodyDescription) + itemDescripion;
					bodyDescription = bodyDescription + "</td></tr></table>" + "</p>";
					System.out.println("****** Body Description ******" + bodyDescription);
					//text = dom.createTextNode(StringEscapeUtils.escapeXml(bodyDescription));
					text = dom.createTextNode((bodyDescription));
				}
				else
				{
					text = dom.createTextNode((itemDescripion));
					//text = dom.createTextNode(StringEscapeUtils.escapeXml(itemDescripion));
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			//   itemDescripion = StringEscapeUtils.escapeXml(itemDescripion);
			//   itemDescripion = StringEscapeUtils.escapeHtml(itemDescripion);



			body.appendChild(text);


			// URL
			String itemUrl = d.getWebSiteURL() + d.getWarName() + "/client/DA/c_newsItem.jsp?element=" + id;
			Element url = dom.createElement("link");
//                           url.setAttribute("url",itemUrl);
			text = dom.createTextNode(itemUrl);
			url.appendChild(text);




			asset.appendChild(subTitle);
			asset.appendChild(pubDate);
			asset.appendChild(body);

			//         asset.appendChild(issue);
			asset.appendChild(url);
			channel.appendChild(asset);
		}


		rootEle.appendChild(channel);

	}


	/**
	 * This method uses Xerces specific classes
	 * prints the XML document to file.
	 */
	private void printToFile(HttpServletResponse response)
	{

		try
		{
			//print
			OutputFormat format = new OutputFormat(dom);
			format.setIndenting(true);

			//to generate output to console use this serializer
			//XMLSerializer serializer = new XMLSerializer(System.out, format);


			//to generate a file output use fileoutputstream instead of system.out
			//	XMLSerializer serializer = new XMLSerializer(
			//	new FileOutputStream(new File("c:/book.xml")), format);

			//	serializer.serialize(dom);

			// this is will give response back to request .....
			XMLSerializer serializer = new XMLSerializer(
					response.getOutputStream(), format);

			serializer.serialize(dom);

		}
		catch (IOException ie)
		{
			ie.printStackTrace();
		}
	}


}
