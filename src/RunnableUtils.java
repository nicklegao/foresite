import java.lang.reflect.Field;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class RunnableUtils
{

	/**
	 * @param args
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public static void main(String[] args) throws InterruptedException, ExecutionException
	{
//		Thread1 t1 = new Thread1();
//		final long id = startRunnable(t1);
//		Thread.sleep(2000);
//		new Thread(new Runnable()
//		{
//			@Override
//			public void run()
//			{
//				Thread1 t = fetchRunnable(id, Thread1.class);
//				if (t != null)
//				{
//					t.switchOff();
//				}
//			}
//
//		}).start();


		ExecutorService executor = Executors.newCachedThreadPool();
		Callable<Object> t1 = Executors.callable(new Thread1());
		System.out.println("Start");
		Future<Object> future = executor.submit(t1);
		Future<String> future1 = executor.submit(new Callable<String>()
		{
			@Override
			public String call() throws InterruptedException
			{
				Thread.sleep(2000);
				return "world";
			}
		});

		System.out.println(future.get());
		System.out.println(future1.get());
		executor.shutdown();
	}

	public static <T extends Runnable> T fetchRunnable(long id, Class<T> clazz)
	{
		for (Thread t : Thread.getAllStackTraces().keySet())
		{
			if (t.getId() != id)
			{
				continue;
			}
			if (t.getClass().isAssignableFrom(clazz))
			{
				System.out.println("Found thread");
				return (T) t;
			}
			try
			{
				Field field = t.getClass().getDeclaredField("target");
				field.setAccessible(true);
				Object target = field.get(t);

				if (target.getClass().isAssignableFrom(clazz))
				{
					System.out.println("Found runnable");
					return (T) target;
				}
				break;
			}
			catch (Exception e)
			{
				break;
			}

		}
		return null;
	}

	public static long startRunnable(Runnable runnable)
	{
		Thread t1 = new Thread(runnable);
		t1.start();
		return t1.getId();
	}


	public static class Thread1 implements Runnable
	{
		private boolean on = true;
		private int count = 0;

		@Override
		public void run()
		{
			while (on && count < 10)
			{
				try
				{
					System.out.println("I am running...");
					Thread.sleep(1000);
					count++;
				}
				catch (Exception e)
				{
				}
			}
			System.out.println("Stopped.");
		}

		public void switchOff()
		{
			this.on = false;
		}
	}
}
