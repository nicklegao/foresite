/**
 * 10:30:59 AM 04/07/2011
 * @author Vito Lefemine
 */
package config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.Logger;


/**
 * @author Vito Lefemine
 *
 */
public class PropertiesService implements IPropertiesService
{


	static Logger logger = Logger.getLogger(PropertiesService.class);

	/* (non-Javadoc)
	 * @see au.corporateinteractive.ric.service.IPropertiesService#loadProperties(java.lang.String)
	 */
	public Properties loadProperties(String propertiesFileName) throws Exception
	{

		Properties properties = new Properties();
		FileInputStream fileInputStream;
		String path = null;
		try
		{
			try
			{
				fileInputStream = new FileInputStream("config" + File.separator + propertiesFileName);
			}
			catch (FileNotFoundException e)
			{
				logger.error(e.getMessage());
				URL url = Thread.currentThread().getContextClassLoader().getResource("");
				logger.debug(("url[" + url + "]"));

				path = url.getPath() + "config" + File.separator + propertiesFileName;
				logger.error("path is  [" + path + "]");
				path = path.substring(1);
				logger.error("path is  [" + path + "]");
				try
				{
					fileInputStream = new FileInputStream(path);
					properties.load(fileInputStream);
				}
				catch (Exception e2)
				{
					logger.error("path is  [ replace %20" + path.replaceAll("%20", " ") + "]");
					fileInputStream = new FileInputStream(path.replaceAll("%20", " "));
					properties.load(fileInputStream);
				}
			}


		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			throw new Exception(" Properties file not loaded ( " + path + " )");
		}
		return properties;

	}

	/* (non-Javadoc)
	 * @see au.corporateinteractive.ric.service.IPropertiesService#getPropertiesValue(java.lang.String, java.lang.String)
	 */
	public String getPropertiesValue(String propertiesFileName,
			String propertyName) throws Exception
	{
		return "" + loadProperties(propertiesFileName).get(propertyName);
	}



}
