/**
 * 10:30:51 AM 04/07/2011
 * @author Vito Lefemine
 */
package config;

import java.util.Properties;

/**
 * @author Vito Lefemine
 *
 */
public interface IPropertiesService
{

	/**
	 * Load properties file
	 * 
	 * @param propertiesFileName
	 * @throws Exception
	 */
	public Properties loadProperties(String propertiesFileName) throws Exception;

	/**
	 * load the property from the specific file
	 * 
	 * @param propertiesFileName
	 * @param propertiesName
	 * @return the value of the property
	 */
	public String getPropertiesValue(String propertiesFileName, String propertyName) throws Exception;

}
