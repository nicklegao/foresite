/**
 * 4:07:40 PM 30/06/2011
 * @author Vito Lefemine
 */
package config;

import java.util.Enumeration;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * @author Vito Lefemine
 *
 */
public class ConfigManager
{
	static Logger logger = Logger.getLogger(ConfigManager.class);

	private static ConfigManager _instance = null;
	private Properties prop;

	private ConfigManager() throws Exception
	{
		prop = new Properties();
		addPropertiesFromFile("prop.properties", prop);
		Enumeration e = prop.propertyNames();
		while (e.hasMoreElements())
		{
			String key = (String) e.nextElement();
			logger.info("LOADED PROP: " + key + ":" + prop.getProperty(key));
		}
	}

	private void addPropertiesFromFile(String fileName, Properties prop) throws Exception
	{
		IPropertiesService ps = new PropertiesService();
		Properties aProp = ps.loadProperties(fileName);
		Enumeration<Object> em = aProp.keys();
		while (em.hasMoreElements())
		{
			String str = (String) em.nextElement();
			logger.debug("LOADED PROPERTY : " + str + ": " + aProp.get(str));
			prop.put(str, aProp.get(str));
		}

	}

	public static Properties getProperties()
	{
		return getInstance().prop;
	}

	public static ConfigManager getInstance()
	{
		if (_instance == null)
		{
			try
			{
				_instance = new ConfigManager();
			}
			catch (Exception e)
			{
				logger.fatal(e.getMessage());
				e.printStackTrace();
			}
		}
		return _instance;
	}

	public String getProp(String propName)
	{
		return prop.getProperty(propName);
	}




}
