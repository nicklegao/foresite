/**
 * 1:56:53 PM 04/08/2011
 * @author Vito Lefemine
 */
package config;

/**
 * @author Vito Lefemine
 *
 */
public class Constants
{


	public static final String DATE_FOR_BOOKING_MODULE = "dd kk";


	public static final String TIME_FORMAT_HHMMA = "HH:mma";

	/**
	 * product
	 */
//	public static final String PRODUCT_MODULE_NAME = "products";


	public static final String SALESORDERS_MODULENAME = "salesorders";

	/**
	 * WB module name for customers
	 */
	public static final String CUSTOMER_MODULE_NAME = "members";

	/**
	 * WB module name for services
	 */
	public static final String SERVICES_MODULE_NAME = "services";

	/**
	 * yyyy-MM-dd
	 */
	public static final String DATE_FORMAT_MODULE = "yyyy-MM-dd";

	/**
	 * yyyy-MM-dd HH:mm:ss
	 */
	public static final String DATE_AND_TIME_FORMAT_MODULE = "yyyy-MM-dd HH:mm:ss";


	/**
	 * Wed, 10 Aug 2011
	 * EEE, d MMM yyyy HH:mm
	 */
	public static final String DATE_AND_TIME_PRINT_FORMAT = "EEE, d MMM yyyy kk:mm a";

	/**
	 * HH:mm
	 */
	public static final String TIMEFORMATKKMM_MODULE = "kk:mm";

	/**
	 * information
	 * */
	public static final String MODULE_INFORMATION_MODULENAME = "information";
}
