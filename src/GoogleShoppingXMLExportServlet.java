import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import sbe.ShortUrls.ShortUrlManager;
import webdirector.db.DButils;
import webdirector.db.client.ClientDataAccess;
import au.net.webdirector.common.datatypes.domain.DataType;
import au.net.webdirector.common.datatypes.service.DataTypesService;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;


public class GoogleShoppingXMLExportServlet extends HttpServlet
{

	private static final String URL_PREFIX = "UrlPrefix";
	private static final String ATTR_BRAND = "ATTR_Brand";
	private static final long serialVersionUID = 3016428307021203576L;
	private static final String MODEL_NUMBER = "ATTR_ModelNumber";
	private static final String ATTR_HEADLINE = "ATTR_Headline";
	private static final String WAR_NAME = "WarName";
	private static final String HOST = "Host";
	private static final String CURRENCY = "Currency";

	private DButils db = new DButils();

	private String categoryID = "";
	private String limit = null;
	private String module;
	private String live = "1";
	private boolean isFriendlyUrl = false;

	//Hash of attributes from labelstable  which will contains internal and external name base upon export selection
	// on editAttributes from web director.
	private LinkedHashMap hAttributes;
	private ClientDataAccess cda = new ClientDataAccess();
	private au.net.webdirector.common.Defaults d = au.net.webdirector.common.Defaults.getInstance();
	private webdirector.db.client.ClientFileUtils CF = new webdirector.db.client.ClientFileUtils();
	Document dom;
	private String host;
	private String urlPrefix;

	private static Map<String, NodeContentGenerator> nodeNameToContentGeneratorMap = null;


	public void init()
	{
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		initNodeNameToContentGeneratorMap();

		String module = request.getParameter("module");
		this.module = (module != null && !"".equals(module) && !"null".equalsIgnoreCase(module)) ? module : "PRODUCTS";

		String live = request.getParameter("live");
		this.live = (live != null && !"".equals(live) && !"null".equalsIgnoreCase(live)) ? live : "1";
		// boolean live1= (live != null && live.equals("1") &&  !"".equals(live) && !"null".equalsIgnoreCase(live)) ? true: false;

		String categoryID = request.getParameter("categoryID");
		this.categoryID = (categoryID != null && !"".equals(categoryID) && !"null".equalsIgnoreCase(categoryID)) ? categoryID : "";

		limit = request.getParameter("limit");

		host = request.getHeader("host");

		isFriendlyUrl = "yes".equalsIgnoreCase(request.getParameter("friendlyUrl"));

		urlPrefix = request.getParameter("urlPrefix");
		if (urlPrefix == null || urlPrefix.length() == 0)
		{
			urlPrefix = "/products/";
		}

		//Get a DOM object
		createDocument();

		// initialize the list
		Vector data = loadData();

		//Create XML file....
		createDOMTree(data, this.module);

		// Crate response
		printToFile(response);

		// response back to caller using iostream


	}



	/**
	 * Add a list of books to the list
	 * In a production system you might populate the list from a DB
	 */
	private Vector loadData()
	{
		Vector vXMLData = new Vector();
		StringBuffer query = new StringBuffer("Select  Live, Create_date, Category_id,Element_id ");
		StringBuffer where = new StringBuffer(" where ");
		String from = " from ";

		//Hash of attributes from labelstable  which will contains internal and external name base upon export selection
		// on editAttributes from web director.
		hAttributes = new LinkedHashMap();

		// get labels tables data for particular attributes to be add into XML
		DataTypesService dtS = new DataTypesService();
		List<DataType> vLabelVariable = dtS.loadGenericDataTypes(module, false, null);

		System.out.println("+++++++++ module " + module);
		System.out.println("+++++++++ vLabelVariable.size() " + vLabelVariable.size());
		int j = 0;
		for (int i = 0; i < vLabelVariable.size(); i++)
		{
			DataType labelVariables = vLabelVariable.get(i);
			if ("1".equals(labelVariables.getExport()))
			{
				hAttributes.put(labelVariables.getInternalName(), labelVariables.getExternalName());
				j++;
			}
		}
		String cols[] = new String[4 + j];
		cols[0] = "Live";
		cols[1] = "Create_date";
		cols[2] = "Category_id";
		cols[3] = "Element_id";
		int k = 4;

		// Build query and addd attributes into cols for later use
		Iterator keys = hAttributes.keySet().iterator();
		while (keys.hasNext())
		{
			String key = (String) keys.next();
			query.append("," + key);
			cols[k] = key;
			k++;
		}

		if (!module.equals("") && !categoryID.equals(""))
		{
			//vAssets = cda.getCategoryElements(module, categoryID, live);
			where.append(" Category_id=" + categoryID + " and Live=" + live);
			from = from.concat("Elements_" + module);
		}
		else if (!module.equals(""))
		{
			where.append(" Live =" + live);
			from = from.concat("Elements_" + module);
			//String[] cols = cda.getElementCols(module);
			//  vAssets = cda.getColumns(cols, module, "Elements", "", "", false, true, live);
		}

		String limitSql = "";
		String orderColSql = " order by ";
		String orderSql = "element_id";
		String CategoryDataSql = "";
		int lastLoop = 0;
		if (limit != null)
		{
			limitSql = " limit " + limit;
		}

		// run query and put data into hashtable with key value pair where key is attribute internal name
		query = query.append(from).append(where).append(orderColSql).append(orderSql).append(limitSql);
		System.out.println("------" + query.toString());
		//Vector v = db.select(query.toString(),cols);
		Vector v = db.selectQuery(query.toString());

		for (int i = lastLoop; i < v.size(); i++)
		{
			String temp[] = (String[]) v.get(i);
			LinkedHashMap hdata = new LinkedHashMap();
			for (int x = 0; x < temp.length; x++)
			{
				hdata.put(cols[x], temp[x]);
				System.out.println("In putting Hash  :: key:" + cols[x] + "  ::>>>>>Value::" + temp[x]);
			}
			vXMLData.add(hdata);
		}
		return vXMLData;
	}

	/**
	 * Using JAXP in implementation independent manner create a document object
	 * using which we create a xml tree in memory
	 */
	private void createDocument()
	{

		//get an instance of factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try
		{
			//get an instance of builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			//create an instance of DOM
			dom = db.newDocument();

		}
		catch (ParserConfigurationException pce)
		{
			//dump it
			System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
			System.exit(1);
		}

	}

	/**
	 * The real workhorse which creates the XML structure
	 */
	private void createDOMTree(Vector myData, String module)
	{

		//create the root element <Root>
		Element rootEle = dom.createElement("rss");
		rootEle.setAttribute("xmlns:g", "http://base.google.com/ns/1.0");
		rootEle.setAttribute("version", "2.0");
		dom.appendChild(rootEle);

		Element channel = dom.createElement("channel");
		rootEle.appendChild(channel);

		channel.appendChild(createSimpleNode("title", "Online Store Data Feed"));
		channel.appendChild(createSimpleNode("link", "http://" + host));
		channel.appendChild(createSimpleNode("description", "for google shopping"));

		//Start of the item element

		Map<String, String> additionalInfo = new HashMap<String, String>();
		additionalInfo.put(CURRENCY, "AUD");
		additionalInfo.put(HOST, host);
		additionalInfo.put(WAR_NAME, d.getWarName());

		additionalInfo.put(URL_PREFIX, urlPrefix);

		for (int i = 0; i < myData.size(); i++)
		{
			LinkedHashMap xmlData = (LinkedHashMap) myData.get(i);
			Element item = dom.createElement("item");

			putCategoryName(0, (String) xmlData.get("Category_id"), additionalInfo);

			for (String column : nodeNameToContentGeneratorMap.keySet())
			{
				item.appendChild(createSimpleNode(column, nodeNameToContentGeneratorMap.get(column).generateText(xmlData, additionalInfo)));
			}

			item.appendChild(createSimpleNode("g:condition", "new"));
			item.appendChild(createSimpleNode("g:availability", "in stock"));
			channel.appendChild(item);
		}

	}

	private void initNodeNameToContentGeneratorMap()
	{
		if (nodeNameToContentGeneratorMap != null)
		{
			return;
		}

		synchronized (this)
		{
			nodeNameToContentGeneratorMap = new LinkedHashMap<String, NodeContentGenerator>();
			nodeNameToContentGeneratorMap.put("title", new SimpleNodeContentGenerator(ATTR_HEADLINE));

			nodeNameToContentGeneratorMap.put("link", new NodeContentGenerator()
			{
				public String generateText(Map<String, String> xmlData, Map<String, String> additionalInfo)
				{
					StringBuilder sb = new StringBuilder();
					sb.append("http://");
					sb.append(additionalInfo.get(HOST));
					sb.append(additionalInfo.get(URL_PREFIX));
					String categoryName = makeUrlFriendly(additionalInfo.get("CategoryLevel1"));
					String subCategoryName = makeUrlFriendly(additionalInfo.get("CategoryLevel0"));
					if (categoryName == null)
					{
						categoryName = subCategoryName;
						subCategoryName = null;
					}
					sb.append(categoryName == null ? "" : categoryName);
					sb.append("/");
					sb.append(subCategoryName == null ? "" : subCategoryName);
					sb.append("/");
					sb.append(makeUrlFriendly(xmlData.get(ATTR_HEADLINE)));
					sb.append("/");
					sb.append(makeUrlFriendly(xmlData.get(MODEL_NUMBER)));
					return sb.toString();
				}
			});

			nodeNameToContentGeneratorMap.put("description", new NodeContentGenerator()
			{
				public String generateText(Map<String, String> xmlData, Map<String, String> additionalInfo)
				{
					String shortDescription = xmlData.get("ATTR_ShortDesc");
					if (shortDescription != null && shortDescription.length() > 0)
					{
						return shortDescription;
					}
					StringBuilder sb = new StringBuilder();
					sb.append(xmlData.get(ATTR_HEADLINE));
					sb.append(" ");
					sb.append(xmlData.get(ATTR_BRAND));
					sb.append(" ");
					sb.append(xmlData.get(MODEL_NUMBER));
					return sb.toString();
				}
			});

			nodeNameToContentGeneratorMap.put("g:id", new NodeContentGenerator()
			{
				public String generateText(Map<String, String> xmlData, Map<String, String> additionalInfo)
				{
					return xmlData.get("Category_id") + "_" + xmlData.get("Element_id");
				}
			});

			nodeNameToContentGeneratorMap.put("g:image_link", new NodeContentGenerator()
			{
				public String generateText(Map<String, String> xmlData, Map<String, String> additionalInfo)
				{
					StringBuilder sb = new StringBuilder();
					sb.append("http://");
					sb.append(additionalInfo.get(HOST));
					sb.append("/");
					sb.append(additionalInfo.get(WAR_NAME));
					sb.append("stores");
					String imagePath = xmlData.get("ATTRFILE_SML");
					if (null == imagePath || imagePath.length() == 0)
					{
						imagePath = xmlData.get("ATTRFILE_LRG");
					}

					sb.append(imagePath);
					return sb.toString();
				}
			});

			nodeNameToContentGeneratorMap.put("g:price", new NodeContentGenerator()
			{
				public String generateText(Map<String, String> xmlData, Map<String, String> additionalInfo)
				{
					return xmlData.get("ATTRCURRENCY_Price") + " " + additionalInfo.get(CURRENCY);
				}
			});

			nodeNameToContentGeneratorMap.put("g:brand", new NodeContentGenerator()
			{
				public String generateText(Map<String, String> xmlData, Map<String, String> additionalInfo)
				{
					String brand = xmlData.get(ATTR_BRAND);
					if (brand != null && brand.length() > 0)
					{
						return brand;
					}
					// for NAPF specifically
					brand = xmlData.get("ATTRDROP_Filter6");
					if (brand != null && brand.length() > 0)
					{
						return brand;
					}
					return "";
				}
			});

			nodeNameToContentGeneratorMap.put("g:mpn", new SimpleNodeContentGenerator(MODEL_NUMBER));

			return;
		}
	}

	public Element createSimpleNode(String nodeName, String nodeText)
	{
		Element title = dom.createElement(nodeName);
		title.appendChild(dom.createTextNode(nodeText));
		return title;
	}

	private void putCategoryName(int level, String categoryId, Map<String, String> additionalInfo)
	{

		String sql = "select ATTR_categoryName, folderLevel, category_parentID from Categories_" + module + " where category_id = ?";
		System.out.println("##############" + sql);
		String[] queryCols = { "ATTR_categoryName", "folderLevel", "category_parentID" };
		//Vector v = new DButils().select(sql, queryCols);
		Vector v = new DButils().selectQuery(sql, new String[] { categoryId });

		if (v.size() > 0)
		{
			String[] cols = (String[]) v.get(0);
			additionalInfo.put("CategoryLevel" + level, cols[0]);
			if (!"0".equals(cols[2]))
			{
				level++;
				putCategoryName(level, cols[2], additionalInfo);
			}
		}
		else
		{
			additionalInfo.put("CategoryLevel" + level, null);
		}
	}

	/**
	 * This method uses Xerces specific classes
	 * prints the XML document to file.
	 */
	private void printToFile(HttpServletResponse response)
	{

		try
		{
			//print
			OutputFormat format = new OutputFormat(dom);
			format.setIndenting(true);

			//to generate output to console use this serializer
			//XMLSerializer serializer = new XMLSerializer(System.out, format);


			//to generate a file output use fileoutputstream instead of system.out
			//	XMLSerializer serializer = new XMLSerializer(
			//	new FileOutputStream(new File("c:/book.xml")), format);

			//	serializer.serialize(dom);

			// this is will give response back to request .....
			XMLSerializer serializer = new XMLSerializer(
					response.getOutputStream(), format);

			serializer.serialize(dom);

		}
		catch (IOException ie)
		{
			ie.printStackTrace();
		}
	}

	private String makeUrlFriendly(String url)
	{
		if (isFriendlyUrl)
		{
			return ShortUrlManager.friendly(url);
		}
		return url;
	}

	interface NodeContentGenerator
	{

		public String generateText(Map<String, String> xmlData, Map<String, String> additionalInfo);
	}

	class SimpleNodeContentGenerator implements NodeContentGenerator
	{

		private String dataColumn;

		public SimpleNodeContentGenerator(String dataColumn)
		{
			this.dataColumn = dataColumn;
		}

		public String generateText(Map<String, String> xmlData, Map<String, String> additionalInfo)
		{
			return xmlData.get(dataColumn);
		}

	}



}
