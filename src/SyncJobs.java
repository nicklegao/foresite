import webdirector.db.client.ClientDataAccess;
import webdirector.db.client.ClientFileUtils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;

import java.util.Vector;
import java.util.Hashtable;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.IOException;
import java.io.File;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 18/06/2008
 * Time: 15:40:46
 * To change this template use File | Settings | File Templates.
 */
public class SyncJobs extends HttpServlet
{
	DBaccess DA = new DBaccess();
	ClientDataAccess CDA = new ClientDataAccess();
	ClientFileUtils CFU = new ClientFileUtils();
	Defaults d = Defaults.getInstance();

	int jobsFromTrilogyCount = 0;
	int jobsDeletedFromWebDriector = 0;
	int jobsInsertedIntoWebDirectorCount = 0;

	public void init() throws ServletException
	{
		// This servlet ensures the Job DB tables are kept in sync.
		String cols[] = { "IsPermanent",
				"IsUrgent",
				"IsHotJob",
				"BranchCode",
				"DatePosted",
				"ShortListDate",
				"JobPriority",
				"JobCode",
				"JobType",
				"JobRate",
				"JobTitle",
				"JobSearchTitle",
				"JobState",
				"JobSuburb",
				"JobDivision",
				"JobSummary",
				"JobAd" };

		String query = "select IsPermanent,IsUrgent,IsHotJob,BranchCode,DatePosted," +
				"ShortListDate,JobPriority,JobCode,JobType,JobRate," +
				"JobTitle,JobSearchTitle,upper(JobState),JobSuburb,JobDivision," +
				"JobSummary,JobAd from job";

		// First let's get the jobs
		Vector jobsFromTrilogy = select(query, cols);
		jobsFromTrilogyCount = jobsFromTrilogy.size();

		// Now clear down the local table
		query = "delete from elements_jobadvertising";
		jobsDeletedFromWebDriector = DA.updateData(query);

		// Now clear down the local table
		query = "delete from categories_jobadvertising";
		DA.updateData(query);

		// Delete stores Directory::
		File f = new File(d.getStoreDir() + "/JOBADVERTISING/");
		d.nuke(f);
		// ...and write the Trilogy ones in...
		jobsFromTrilogy = insertCategories(jobsFromTrilogy);
		// TODO some category manpiulation needs to be done to create the two levels of categorisation the JobAds module needs for searching
		jobsInsertedIntoWebDirectorCount = insertJobs(jobsFromTrilogy);

	}

	private Vector insertCategories(Vector jobsFromTrilogy)
	{
		String checkCat = "";
		String catId = "";
		String childCatId = "";
		// loop through attributes and create categories
		for (int i = 0; i < jobsFromTrilogy.size(); i++)
		{
			String[] jobAttributes = (String[]) jobsFromTrilogy.elementAt(i);
			System.out.println("ITM jobs adds content::" + jobAttributes[16]);
			String[] jobAttributesWithCat = new String[jobAttributes.length + 1];
			String categoryLocation = jobAttributes[12];
			System.out.println("ITM categoryLocation" + categoryLocation);
			String categoryName = jobAttributes[14];
			System.out.println("ITM categoryName" + categoryName);
			checkCat = CDA.getCategoryIdUsingParent("JOBADVERTISING", "1", categoryLocation, null);
			System.out.println("ITM checkCat value" + checkCat);
			if (checkCat == null || checkCat.equals("") || checkCat.equals("null") || checkCat.equals("0"))
			{
				catId = createNewCategory(categoryLocation, "0", "1");
				checkCat = catId;
			}
			childCatId = CDA.getCategoryIdUsingParent("JOBADVERTISING", "2", categoryName, checkCat);
			System.out.println("ITM child ID:::>>>>>>>>" + childCatId);
			if (childCatId == null || childCatId.equals("") || childCatId.equals("null") || childCatId.equals("0"))
			{
				childCatId = createNewCategory(categoryName, checkCat, "2");
			}
			for (int j = 0; j < jobAttributes.length; j++)
			{

				jobAttributesWithCat[j] = jobAttributes[j];
			}
			jobAttributesWithCat[17] = childCatId;
			jobsFromTrilogy.removeElementAt(i);
			jobsFromTrilogy.insertElementAt(jobAttributesWithCat, i);

		}
		return jobsFromTrilogy;
	}

	private String createNewCategory(String categoryName, String category_ParentId, String folderLevel)
	{

		Hashtable htRecord = new Hashtable();
		htRecord.put("ATTR_categoryName", categoryName);
		htRecord.put("Category_ParentID", category_ParentId);
		htRecord.put("folderLevel", folderLevel);
		htRecord.put("ATTRCHECK_Live", "1");
//            htRecord.put("Lock_id","1");
//            htRecord.put("Version","1");
//            htRecord.put("Live","1");
		int id = DA.insertData(htRecord, "Category_id", "Categories_JOBADVERTISING");
		if (id > 0)
			return Integer.toString(id);
		else
			return "-99";

	}

	private boolean checkCategory()
	{

		return true;
	}

	private int insertJobs(Vector jobsFromTrilogy)
	{
		String id = "";

		int insertCount = 0;

		// loop through attributes and create
		for (int i = 0; i < jobsFromTrilogy.size(); i++)
		{
			String[] jobAttributes = (String[]) jobsFromTrilogy.elementAt(i);

			// clean up the data
			if (jobAttributes[4] == null || jobAttributes[4].equals(""))
				jobAttributes[4] = "1900-01-01 00:00:00";
			if (jobAttributes[5] == null || jobAttributes[5].equals(""))
				jobAttributes[5] = "1900-01-01 00:00:00";

			Hashtable htRecord = new Hashtable();
			htRecord.put("ATTR_Headline", jobAttributes[10]);
			htRecord.put("ATTRINTEGER_IsPermanent", jobAttributes[0]);
			htRecord.put("ATTRINTEGER_IsUrgent", jobAttributes[1]);
			htRecord.put("ATTR_BranchCode", jobAttributes[3]);
			htRecord.put("ATTR_JobPriority", jobAttributes[6]);
			htRecord.put("ATTR_JobType", jobAttributes[8]);
			htRecord.put("ATTR_JobRate", jobAttributes[9]);
			htRecord.put("ATTR_JobState", jobAttributes[12]);
			htRecord.put("ATTR_JobSuburb", jobAttributes[13]);
			htRecord.put("ATTR_JobDivision", jobAttributes[14]);
			htRecord.put("ATTR_GeneralPurpose2", jobAttributes[7]);
			htRecord.put("ATTR_GeneralPurpose4", jobAttributes[11]);
			htRecord.put("ATTR_GeneralPurpose6", jobAttributes[15]);
			htRecord.put("ATTRDATE_Date1", jobAttributes[4]);
			htRecord.put("ATTRDATE_Date2", jobAttributes[5]);
			htRecord.put("ATTRCHECK_YesNo1", jobAttributes[2]);

			String adText = jobAttributes[16];
			System.out.println("job addText:::" + adText);


			htRecord.put("Language_id", "1");
			htRecord.put("Status_id", "1");
			htRecord.put("Lock_id", "1");
			htRecord.put("Version", "1");
			htRecord.put("Live", "1");
//            String catId = CDA.getIdFromCategory("JOBADVERTISING","Banking");
//
			htRecord.put("Category_id", jobAttributes[17]);
			id = Integer.toString(DA.insertData(htRecord, "Element_id", "Elements_JOBADVERTISING"));


			// TODO Need to do the stores thing and put the path in the below TODO
			boolean fileCreated = CFU.createNewFile(d.getStoreDir() + "/JOBADVERTISING/" + id + "/ATTRLONG_PageText", d.getStoreDir() + "/JOBADVERTISING/" + id + "/ATTRLONG_PageText/ATTRLONG_PageText.txt");
			if (fileCreated)
			{
				try
				{
					if (i == 150)
					{
						System.out.println("ITM content for id 150::" + jobAttributes[16]);
					}
					CFU.overwriteTextContents(d.getStoreDir() + "/JOBADVERTISING/" + id + "/ATTRLONG_PageText/ATTRLONG_PageText.txt", jobAttributes[16]);
				}
				catch (Exception e)
				{
					System.out.println("Print strace" + e.getMessage());
					e.printStackTrace();
				}
			}
			Hashtable hupate = new Hashtable();
			hupate.put("Element_id", id);
			hupate.put("ATTRLONG_PageText", "/JOBADVERTISING/" + id + "/ATTRLONG_PageText/ATTRLONG_PageText.txt");
			DA.updateData(hupate, "Element_id", "Elements_JOBADVERTISING");
			// htRecord.put("ATTRLONG_PageText", "/stores/path Made up here");

			insertCount++;
		}

		return insertCount;
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		init();

		response.sendRedirect("/jsp/synced.jsp?from=" + jobsFromTrilogyCount + "&=" + jobsDeletedFromWebDriector + "&inserted=" + jobsInsertedIntoWebDirectorCount);
	}


	/**
	 * Standard method for performing a select on the database but unlike the
	 * other select method this one allows you
	 * to query multiple columns. The query given must be complete AND you must
	 * specify the same set of columns in a String[]
	 * If getMetaData is true then the static cols variable in this class is set
	 * using a call to getMetaData(ResultSet rs).
	 * Cache not implemented just yet.
	 * 
	 * @param query
	 * @param cols
	 * @return Vector of String[] with each String[] being equal to a row
	 *         returned by the query and
	 *         each value in the String[] equal to a column from the db.
	 */
	public Vector select(String query, String[] cols)
	{
		Vector v = new Vector(10);
		Connection conn = null;
		Statement stmt = null;  // Or PreparedStatement if needed
		ResultSet rs = null;
		String[] data = new String[cols.length];

		try
		{
			Context ctx = new InitialContext();
			if (ctx == null)
				throw new Exception("Boom - No Context");

			DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/trilogy");

			if (ds != null)
			{
				synchronized (ds)
				{
					conn = ds.getConnection();
				}

				if (conn != null)
				{
					stmt = conn.createStatement();
					rs = stmt.executeQuery(query);

					while (rs.next())
					{
						for (int i = 0; i < cols.length; i++)
						{
							Object x = rs.getString(i + 1);
							if ((String) x == null)
								data[i] = "";
							else
							{
								String s = (String) x;
								data[i] = s.trim();
							}
						}

						v.addElement((String[]) data.clone());
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			// Always make sure result sets and statements are closed,
			// and the connection is returned to the pool
			if (rs != null)
			{
				try
				{
					rs.close();
				}
				catch (SQLException e)
				{
					;
				}
				rs = null;
			}
			if (stmt != null)
			{
				try
				{
					stmt.close();
				}
				catch (SQLException e)
				{
					;
				}
				stmt = null;
			}
			if (conn != null)
			{
				try
				{
					conn.close();
				}
				catch (SQLException e)
				{
					;
				}
				conn = null;
			}
		}

		return (v);
	}

	public static void main(String[] args)
	{
		SyncJobs SJ = new SyncJobs();
		try
		{
			SJ.init();
		}
		catch (ServletException e)
		{
			e.printStackTrace();
		}
	}

}
