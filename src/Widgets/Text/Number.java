package Widgets.Text;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Random;

/**
 * Created by IntelliJ IDEA. User: Administrator Date: 30/07/2010 Time: 14:51:01
 * To change this template use File | Settings | File Templates.
 */
public class Number
{
	public static String changePriceFormat(String price)
	{
		try
		{
			NumberFormat nfInput = NumberFormat.getNumberInstance();
			nfInput.setMaximumFractionDigits(2);
			java.lang.Number n = nfInput.parse(price);
			NumberFormat nfOutput = NumberFormat.getCurrencyInstance();
			nfOutput.setMaximumFractionDigits(2);
			return nfOutput.format(n.doubleValue());
		}
		catch (Exception e)
		{
			return "0.00";
		}
	}

	public static String changePriceFormat(String price, String currencySymbol)
	{
		try
		{

			return currencySymbol + new DecimalFormat("#,##0.00").format(Double.parseDouble(price));
		}
		catch (Exception e)
		{
			return currencySymbol + "0.00";
		}
	}

	public static int getRandom(int no)
	{
		Random rand = new Random();
		return Math.abs(rand.nextInt(no));
	}
}
