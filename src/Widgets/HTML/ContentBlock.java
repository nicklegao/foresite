package Widgets.HTML;

import java.util.Hashtable;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;

import au.net.webdirector.common.Defaults;

/**
 * Created by IntelliJ IDEA. User: Administrator Date: 30/07/2010 Time: 15:07:25
 * To change this template use File | Settings | File Templates.
 */
public class ContentBlock
{

	static Defaults d = Defaults.getInstance();

	public static String getText(String moduleName, String assetName, String attribute, boolean isElement, boolean isID, boolean showMarkup)
	{
		return getText(moduleName, assetName, attribute, isElement, isID, showMarkup, false);
	}

	private static String getText(String moduleName, String assetName, String attribute, boolean isElement, boolean isID, boolean showMarkup, boolean preserverCarriageReturns)
	{
		String returnString = "";
		if (showMarkup)
		{
			returnString = "Cannot find this page. Page Name: <b>" + assetName + "</b>";
		}
		webdirector.db.client.ClientDataAccess DA = new webdirector.db.client.ClientDataAccess();
		Vector vct = null;
		if (isID)
		{
			if (isElement)
			{
				vct = DA.getElement(moduleName, assetName);
			}
			else
			{
				vct = DA.getCategory(moduleName, assetName, true);
			}
		}
		else
		{
			if (isElement)
			{
				vct = DA.getElementByName(moduleName, assetName);
			}
			else
			{
				// TODO Need to implement this
				// vct = DA.getCategory(moduleName,assetName,true);
			}
		}

		if (vct != null && vct.size() != 0)
		{
			Hashtable ht = (Hashtable) vct.get(0);
			webdirector.db.client.ClientFileUtils CF = new webdirector.db.client.ClientFileUtils();
			returnString = CF.readTextContents(CF.getFilePathFromStoreName((String) ht.get(attribute)), preserverCarriageReturns).toString();
		}

		return returnString;
	}

	public static String getText(String moduleName, String assetType, String attribute, boolean preserverCarriageReturns)
	{
		return getText(moduleName, assetType, attribute, true, false, true, preserverCarriageReturns);

	}

	public static String getText(String moduleName, String assetType, String attribute)
	{
		return getText(moduleName, assetType, attribute, true, false, true, false);

	}

	@Deprecated
	public static String getTextAndReplaceTagsOld(String moduleName, String assetType, String attribute)
	{
		String originalText = getText(moduleName, assetType, attribute, true, false, true);

		Pattern p = Pattern.compile("<ci:insert path=\"(.*)\"></ci:insert>");
		System.out.println("ContentBlock  originalText = " + originalText);
		Matcher matcher = p.matcher(Matcher.quoteReplacement(originalText));
		boolean matchFound = matcher.find();
		System.out.println("ContentBlock matchFound = " + matchFound);
		if (matchFound)
		{
			HttpClient client = new HttpClient();

			for (int i = 1; i <= matcher.groupCount(); i++)
			{
				String groupStr = matcher.group(i);
				GetMethod method = null;
				try
				{
					method = new GetMethod(d.getWebSiteURL() + d.getWarName() + groupStr);
				}
				catch (Exception e)
				{
					e.printStackTrace();
					System.out.println("ContentBlockREPLACEMENT - " + d.getWebSiteURL() + d.getWarName() + groupStr + " IS NOT VALID");
				}
				if (method != null)
				{
					String toInsert = "LOAD FAILURE " + groupStr;
					try
					{
						client.executeMethod(method);
						toInsert = method.getResponseBodyAsString();
					}
					catch (Exception e)
					{
						e.printStackTrace();
						toInsert = toInsert + e.getMessage();
					}
					finally
					{
						System.out.println(toInsert);
						originalText = originalText.replaceAll(Matcher.quoteReplacement("<ci:insert path=\".*\"></ci:insert>"), Matcher.quoteReplacement(toInsert));
					}
				}
			}
		}
		System.out.println("ContentBlock newText = " + originalText);
		return originalText;

	}

	public static String getTextAndReplaceTags(String moduleName, String assetType, String attribute)
	{

		String originalText = getText(moduleName, assetType, attribute, true, false, true);

		SimpleMatcher sm = new SimpleMatcher(originalText, "<ci:insert path=\"", "\"></ci:insert>");

		System.out.println("\n\nContentBlockoriginalText = " + originalText + "\n");
		HttpClient client = new HttpClient();
		while (sm.find())
		{
			String groupStr = sm.extract();
			System.out.println("ContentBlock groupStr = " + groupStr);
			GetMethod method = null;
			try
			{
				System.out.println("ContentBlock URL :::: " + d.getWebSiteURL() + d.getWarName() + groupStr);
				method = new GetMethod(d.getWebSiteURL() + d.getWarName() + groupStr);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				System.out.println("REPLACEMENT - " + d.getWebSiteURL() + d.getWarName() + groupStr + " IS NOT VALID");
			}
			if (method != null)
			{
				String toInsert = "LOAD FAILURE " + groupStr;
				try
				{
					client.executeMethod(method);
					toInsert = method.getResponseBodyAsString();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					toInsert = toInsert + e.getMessage();
				}
				finally
				{
					System.out.println("ContentBlock TO INSERT " + toInsert);
					originalText = originalText.replace(sm.match(), toInsert);
				}
			}

		}
		System.out.println("ContentBlock newText = " + originalText);
		return originalText;

	}

	public static void main(String[] args)
	{
		String originalText = "<td><ci:insert path=\"/client/IncludeFiles/i_homePage_newsWidget.jsp\"></ci:insert><br /><a href=\"/YORK/client/NewsRoom/c_latestNews.jsp\"><img alt=\"\" src=\"http://99onyork.com.au//YORKstores/_images/webDirector/read-more.png\" style=\"width: 87px; height: 27px\" /></a></td>		</tr>";

		ContentBlock.getTextAndReplaceTags(originalText, "AAA", "");

		SimpleMatcher sm = new SimpleMatcher(originalText, "<ci:insert path=\"", "\"></ci:insert>");
		while (sm.find())
		{
			String groupStr = sm.match();
			System.out.println(groupStr);
			String toInsert = sm.extract();
			;
			System.out.println(toInsert);
			originalText = originalText.replace(groupStr, toInsert);
		}
		System.out.println(originalText);



	}
}