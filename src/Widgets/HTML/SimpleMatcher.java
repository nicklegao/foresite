package Widgets.HTML;

public class SimpleMatcher
{
	private String prefix;
	private String subfix;
	private String org;

	public SimpleMatcher(String org, String pre, String sub)
	{
		this.org = org;
		this.prefix = pre;
		this.subfix = sub;
	}

	private int start = -1;
	private int end = -1;

	public boolean find()
	{
		if (end == -1)
		{
			end = 0;
		}
		else
		{
			end += subfix.length();
		}
		start = org.indexOf(prefix, end);
		if (start == -1)
		{
			end = -1;
			return false;
		}
		end = org.indexOf(subfix, start + prefix.length());
		if (end == -1)
		{
			start = -1;
			return false;
		}
		return true;
	}

	public String match()
	{
		return org.substring(start, end + subfix.length());
	}

	public String extract()
	{
		return org.substring(start + prefix.length(), end);
	}
}
