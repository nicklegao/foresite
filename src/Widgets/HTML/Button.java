package Widgets.HTML;

import java.util.Hashtable;
import java.util.Vector;

import au.net.webdirector.common.Defaults;

/**
 * Created by IntelliJ IDEA. User: Administrator Date: 30/07/2010 Time: 14:21:37
 * To change this template use File | Settings | File Templates.
 */
public class Button
{

	public static String getButton(String assetName, String linkURL, boolean includeWarName, String optionalAnchorAttrs, String optionalImageAttrs)
	{
		return getButton(assetName, linkURL, includeWarName, optionalAnchorAttrs, optionalImageAttrs, null);
	}

	public static String getButton(String assetName, String linkURL, boolean includeWarName, String optionalAnchorAttrs, String optionalImageAttrs, String id)
	{

		webdirector.db.client.ClientDataAccess DA = new webdirector.db.client.ClientDataAccess();

		StringBuffer sb = new StringBuffer();
		Defaults d = Defaults.getInstance();
		String warName = includeWarName ? "/" + d.getWarName() : "";

		// Build HTML
		if (linkURL == null)
		{
			sb.append("<a href='javascript:void(0);' " + optionalAnchorAttrs + ">");
		}
		else if (linkURL.equals("#"))
		{
			sb.append("<a href='#' " + optionalAnchorAttrs + ">");
		}
		else
		{
			sb.append("<a href='" + warName + linkURL + "' " + optionalAnchorAttrs + ">");
		}
		sb.append("<span class=\"button\">" + assetName + "</span>");
		sb.append("</a>");
		return sb.toString();
	}

	public static String getImage(String assetName)
	{
		webdirector.db.client.ClientDataAccess DA = new webdirector.db.client.ClientDataAccess();
		StringBuffer sb = new StringBuffer("NO BUTTON");
		Defaults d = Defaults.getInstance();
		String warName = "/" + d.getWarName();

		// get asset
		Vector v = DA.getElementByName("FLEX13", assetName);
		String ret = "";
		if (v != null && v.size() > 0)
		{
			Hashtable h = (Hashtable) v.elementAt(0);
			ret = DA.img((String) h.get("ATTRFILE_Image1"));

			// Build HTML

		}
		return ret;

	}

}
