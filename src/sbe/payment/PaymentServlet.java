package sbe.payment;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sbe.payment.HttpUtil;
import sbe.payment.TransactionRequestResult;

public class PaymentServlet extends HttpServlet
{

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		StringBuilder strPost = new StringBuilder();
		strPost.append("CustomerID=" + req.getParameter("txtMerchID"));
		strPost.append(format("UserName", req.getParameter("txtUserName")));
		// send amounts to the generator in DOLLAR FORM. ie 10.05
		strPost.append(format("Amount", req.getParameter("txtAmount")));
		strPost.append(format("Currency", req.getParameter("txtCurrency")));
		strPost.append(format("PageTitle", req.getParameter("txtPageTitle")));

		strPost.append(format("PageDescription", req.getParameter("txtPageDescription")));
		strPost.append(format("PageFooter", req.getParameter("txtPageFooter")));
		strPost.append(format("Language", req.getParameter("ddlLanguage")));

		strPost.append(format("CompanyName", req.getParameter("txtCompanyName")));
		strPost.append(format("CustomerFirstName", req.getParameter("txtFirstName")));
		strPost.append(format("CustomerLastName", req.getParameter("txtLastName")));
		strPost.append(format("CustomerAddress", req.getParameter("txtAddress")));
		strPost.append(format("CustomerCity", req.getParameter("txtCity")));
		strPost.append(format("CustomerState", req.getParameter("txtState")));
		strPost.append(format("CustomerPostCode", req.getParameter("txtPostcode")));
		strPost.append(format("CustomerCountry", req.getParameter("txtCountry")));
		strPost.append(format("CustomerEmail", req.getParameter("txtEmail")));
		strPost.append(format("CustomerPhone", req.getParameter("txtCustomerPhone")));

		strPost.append(format("InvoiceDescription", req.getParameter("txtInvoiceDescription")));
		strPost.append(format("CancelURL", req.getParameter("txtCancelUrl")));
		strPost.append(format("ReturnUrl", req.getParameter("txtReturnUrl")));

		strPost.append(format("MerchantReference", req.getParameter("txtRefNum")));
		strPost.append(format("MerchantInvoice", req.getParameter("txtInvoice")));
		strPost.append(format("MerchantOption1", req.getParameter("txtOption1")));
		strPost.append(format("MerchantOption2", req.getParameter("txtOption2")));
		strPost.append(format("MerchantOption3", req.getParameter("txtOption3")));

		strPost.append(format("PageBanner", req.getParameter("txtPageBanner")));
		strPost.append(format("CompanyLogo", req.getParameter("txtCompanyLogo")));
		strPost.append(format("ModifiableCustomerDetails", req.getParameter("ddlModDetails")));


		String url = getPaymentGatewayVariable("ewayBaseURL") + "Request?" + strPost.toString();
		String resultXML = HttpUtil.doGetRequest(url);
		try
		{
			TransactionRequestResult result = parseResults(resultXML);
			if (result.isResult())
			{
				resp.sendRedirect(result.getUrl());
				return;
			}
			else
			{
				req.setAttribute("lblResult", result.getError());
				System.out.println("Exception redirecting to " + getPaymentGatewayVariable("ewayPostbackURL"));
				RequestDispatcher rd = this.getServletConfig().getServletContext().getRequestDispatcher(getPaymentGatewayVariable("ewayPostbackURL"));
				rd.forward(req, resp);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public String getPaymentGatewayVariable(String elementName)
	{
		webdirector.db.client.ClientDataAccess cda = new webdirector.db.client.ClientDataAccess();
		java.util.Vector vt = cda.getElementByName("FLEX2", elementName);
		java.util.Hashtable ht = (java.util.Hashtable) vt.get(0);
		return (String) ht.get("ATTR_Value");
	}


	private TransactionRequestResult parseResults(String xml) throws Exception
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(new ByteArrayInputStream(xml.getBytes()));
		Element node = (Element) doc.getElementsByTagName("TransactionRequest").item(0);
		TransactionRequestResult result = new TransactionRequestResult();
		String value = node.getElementsByTagName("Result").item(0).getTextContent();
		result.setResult(Boolean.parseBoolean(value));

		value = node.getElementsByTagName("URI").item(0).getTextContent();
		result.setUrl(value);

		value = node.getElementsByTagName("Error").item(0).getTextContent();
		result.setError(value);
		return result;
	}

	private String format(String fieldName, String value)
	{
		if (value != null && value.trim().length() > 0)
			return "&" + fieldName + "=" + URLEncoder.encode(value);
		else
			return "";
	}

}
