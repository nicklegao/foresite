package sbe.payment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class HttpUtil
{

	public static String doGetRequest(String server) throws IOException
	{
		URL url = new URL(server);
		URLConnection conn = url.openConnection();
		conn.setDoInput(true);
		conn.setUseCaches(false);
		return getResponseText(conn);
	}

	public static String doPostRequest(String server, String data)
			throws IOException
	{
		URL url = new URL(server);
		URLConnection conn = url.openConnection();
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setUseCaches(false);
		// Specify the content type.
		conn.setRequestProperty("Content-Type",
				"application/x-www-form-urlencoded");
		OutputStreamWriter writer = new OutputStreamWriter(conn
				.getOutputStream());

		// write parameters
		writer.write(data);
		writer.flush();
		writer.close();
		return getResponseText(conn);
	}

	private static String getResponseText(URLConnection conn)
			throws IOException
	{
		// Get the response
		StringBuffer answer = new StringBuffer();
		BufferedReader reader = new BufferedReader(new InputStreamReader(conn
				.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null)
		{
			answer.append(line);
		}

		reader.close();
		return answer.toString();
	}

	public static void main(String[] args)
	{
		String content = "name=" + URLEncoder.encode("Buford Early");
	}
}
