package sbe.payment;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import sbe.payment.HttpUtil;
import sbe.payment.TransactionResponse;

public class MerchantResponseServlet extends HttpServlet
{

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		String accessPaymentCode = req.getParameter("AccessPaymentCode");
		TransactionResponse result = checkAccessCode(accessPaymentCode);
		if (result != null)
		{
			if (result.isTrxnStatus())
				req.setAttribute("divAuthorised", "block");
			else
				req.setAttribute("divFailed", "block");
			req.setAttribute("result", result);
		}
		else
			req.setAttribute("divFraud", "block");
		RequestDispatcher rd = this.getServletConfig().getServletContext().getRequestDispatcher("/merchantResponse.jsp");
		rd.forward(req, resp);
	}

	// Procedure to check the 64 character access payment code for security
	private TransactionResponse checkAccessCode(String accessPaymentCode)
	{
		//POST to Payment gateway the access code returned
		String strPost = "CustomerID=" + getServletContext().getInitParameter("CustomerID");
		strPost += format("AccessPaymentCode", accessPaymentCode);
		strPost += format("UserName", getServletContext().getInitParameter("Username"));

		String url = getServletContext().getInitParameter("PaymentGatewayAddr") + "Result?" + strPost;
		try
		{
			String resultXML = HttpUtil.doGetRequest(url);
			return parseXMLResult(resultXML);
		}
		catch (Exception e)
		{
			return null;
		}
	}

	private String getNodeValue(Element node, String name)
	{
		return node.getElementsByTagName(name).item(0).getTextContent();
	}

	private TransactionResponse parseXMLResult(String resultXML) throws Exception
	{
		TransactionResponse result = new TransactionResponse();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(new ByteArrayInputStream(resultXML.getBytes()));
		Element node = (Element) doc.getElementsByTagName("TransactionResponse").item(0);
		result.setAuthCode(getNodeValue(node, "AuthCode"));
		result.setResponseCode(getNodeValue(node, "ResponseCode"));
		result.setReturnAmount(Double.parseDouble(getNodeValue(node, "ReturnAmount")));
		result.setTrxnNumber(Integer.parseInt(getNodeValue(node, "TrxnNumber")));
		result.setTrxnStatus(Boolean.parseBoolean(getNodeValue(node, "TrxnStatus")));
		result.setTrxnResponseMessage(getNodeValue(node, "TrxnResponseMessage"));
		result.setMerchantOption1(getNodeValue(node, "MerchantOption1"));
		result.setMerchantOption2(getNodeValue(node, "MerchantOption2"));
		result.setMerchantOption3(getNodeValue(node, "MerchantOption3"));
		result.setMerchantReference(getNodeValue(node, "MerchantReference"));
		result.setMerchantInvoice(getNodeValue(node, "MerchantInvoice"));
		result.setErrorMessage(getNodeValue(node, "ErrorMessage"));
		return result;
	}

	private String format(String fieldName, String value)
	{
		if (value != null && value.trim().length() > 0)
			return "&" + fieldName + "=" + URLEncoder.encode(value);
		else
			return "";
	}

}
