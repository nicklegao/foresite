package sbe.payment;

public class TransactionResponse
{
	private String authCode;
	private String responseCode;
	private double returnAmount;
	private int trxnNumber;
	private boolean trxnStatus;
	private String trxnResponseMessage;
	private String merchantOption1;
	private String merchantOption2;
	private String merchantOption3;
	private String merchantReference;
	private String merchantInvoice;
	private String errorMessage;

	public String getAuthCode()
	{
		return authCode;
	}

	public void setAuthCode(String authCode)
	{
		this.authCode = authCode;
	}

	public String getResponseCode()
	{
		return responseCode;
	}

	public void setResponseCode(String responseCode)
	{
		this.responseCode = responseCode;
	}

	public double getReturnAmount()
	{
		return returnAmount;
	}

	public void setReturnAmount(double returnAmount)
	{
		this.returnAmount = returnAmount;
	}

	public int getTrxnNumber()
	{
		return trxnNumber;
	}

	public void setTrxnNumber(int trxnNumber)
	{
		this.trxnNumber = trxnNumber;
	}

	public boolean isTrxnStatus()
	{
		return trxnStatus;
	}

	public void setTrxnStatus(boolean trxnStatus)
	{
		this.trxnStatus = trxnStatus;
	}

	public String getTrxnResponseMessage()
	{
		return trxnResponseMessage;
	}

	public void setTrxnResponseMessage(String trxnResponseMessage)
	{
		this.trxnResponseMessage = trxnResponseMessage;
	}

	public String getMerchantOption1()
	{
		return merchantOption1;
	}

	public void setMerchantOption1(String merchantOption1)
	{
		this.merchantOption1 = merchantOption1;
	}

	public String getMerchantOption2()
	{
		return merchantOption2;
	}

	public void setMerchantOption2(String merchantOption2)
	{
		this.merchantOption2 = merchantOption2;
	}

	public String getMerchantOption3()
	{
		return merchantOption3;
	}

	public void setMerchantOption3(String merchantOption3)
	{
		this.merchantOption3 = merchantOption3;
	}

	public String getMerchantReference()
	{
		return merchantReference;
	}

	public void setMerchantReference(String merchantReference)
	{
		this.merchantReference = merchantReference;
	}

	public String getMerchantInvoice()
	{
		return merchantInvoice;
	}

	public void setMerchantInvoice(String merchantInvoice)
	{
		this.merchantInvoice = merchantInvoice;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

}
