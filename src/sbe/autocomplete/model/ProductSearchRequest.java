package sbe.autocomplete.model;

import java.util.List;

public class ProductSearchRequest
{

	List<String> attributes;
	List<String> attributesSearch;
	String categoryId;
	String searchKey;
	String callback;
	String _;

	public ProductSearchRequest(List<String> attributes,
			List<String> attributesSearch, String categoryId, String searchKey,
			String callback, String _)
	{
		super();
		this.attributes = attributes;
		this.attributesSearch = attributesSearch;
		this.categoryId = categoryId;
		this.searchKey = searchKey;
		this.callback = callback;
		this._ = _;
	}

	public ProductSearchRequest()
	{
		super();
	}

	public List<String> getAttributes()
	{
		return attributes;
	}

	public void setAttributes(List<String> attributes)
	{
		this.attributes = attributes;
	}

	public List<String> getAttributesSearch()
	{
		return attributesSearch;
	}

	public void setAttributesSearch(List<String> attributesSearch)
	{
		this.attributesSearch = attributesSearch;
	}

	public String getCategoryId()
	{
		return categoryId;
	}

	public void setCategoryId(String categoryId)
	{
		this.categoryId = categoryId;
	}

	public String getSearchKey()
	{
		return searchKey;
	}

	public void setSearchKey(String searchKey)
	{
		this.searchKey = searchKey;
	}

	public String getCallback()
	{
		return callback;
	}

	public void setCallback(String callback)
	{
		this.callback = callback;
	}

	public String get_()
	{
		return _;
	}

	public void set_(String _)
	{
		this._ = _;
	}


}
