package sbe.autocomplete.control;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sbe.autocomplete.model.ProductSearchRequest;
import sbe.autocomplete.service.AutoCompleteService;

@Controller
public class AutocompleteController
{
	Logger logger = Logger.getLogger(AutocompleteController.class);

	@RequestMapping(value = "/productsSearch", method = RequestMethod.GET)
	@ResponseBody
	public List<Hashtable<String, String>> searchProducts(HttpServletRequest request)
	{
		try
		{
			ProductSearchRequest search = new ProductSearchRequest();
			ArrayList<String> a = new ArrayList<String>();

			if (null != request.getParameter("attributes"))
			{
				StringTokenizer strk = new StringTokenizer(request.getParameter("attributes"), ",");
				while (strk.hasMoreElements())
				{
					String o = (String) strk.nextElement();
					a.add(o);
					logger.info("Adding attributes" + o);
				}
				search.setAttributes(a);
			}
			else
				search.setAttributes(Arrays.asList(new String[] { "Attr_Headline" }));

			a = new ArrayList<String>();
			if (null != request.getParameterValues("attributesSearch"))
			{
				StringTokenizer strk = new StringTokenizer(request.getParameter("attributesSearch"), ",");
				while (strk.hasMoreElements())
				{
					String o = (String) strk.nextElement();
					a.add(o);
					logger.info("Adding attributesSearch" + o);
				}
				search.setAttributesSearch(a);
			}
			else
			{
				search.setAttributesSearch(Arrays.asList(new String[] { "Attr_Headline" }));
			}
			/*unique
			List<String> newList = new ArrayList<String>(new HashSet<String>(search.getAttributes()));
			search.setAttributes(newList);
			newList = new ArrayList<String>(new HashSet<String>(search.getAttributesSearch()));
			search.setAttributesSearch(newList);
			*/

			search.setCategoryId(request.getParameter("categoryId"));
			search.setSearchKey(request.getParameter("searchKey"));
			AutoCompleteService service = new AutoCompleteService();
			List<Hashtable<String, String>> list = service.searchProducts(search);
			for (Hashtable<String, String> current : list)
			{
				logger.info("RETURNING " + current.get("ATTR_Headline"));

			}

			return list;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}




}
