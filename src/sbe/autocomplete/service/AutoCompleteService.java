package sbe.autocomplete.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import sbe.autocomplete.model.ProductSearchRequest;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;

import com.ci.webdirector.jsonUtils.JsonDbUtils;

public class AutoCompleteService
{

	static Logger logger = Logger.getLogger(AutoCompleteService.class);
	final String PRODUCTS_MODULE_NAME = "PRODUCTS";


	public List<Hashtable<String, String>> searchProducts(ProductSearchRequest search)
	{
		List<Hashtable<String, String>> list = new ArrayList<Hashtable<String, String>>();
		try
		{

			String query = buildQuery(search);
			logger.info("Query " + query);
			DBaccess dba = new DBaccess();
			Object[] columnsO = search.getAttributes().toArray();
			String[] columns = new String[columnsO.length];
			for (int i = 0; i < columnsO.length; i++)
			{
				columns[i] = (String) columnsO[i];
			}
			Vector<String[]> rst = (Vector<String[]>) dba.selectQuery(query);
			for (String[] current : rst)
			{
				Hashtable<String, String> record = new Hashtable<String, String>();
				for (int i = 0; i < columns.length; i++)
				{
					if (i <= current.length)
					{
						String colName = (String) columns[i];
						record.put(colName, current[i]);
					}
				}
				list.add(record);
			}
			return list;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			return new ArrayList<Hashtable<String, String>>();
		}

	}

	private String buildQuery(ProductSearchRequest search)
	{
		try
		{
			logger.info("Attributes: " + search.getAttributes() + " SIZE : " + search.getAttributes().size());
			logger.info("Attributes search: " + search.getAttributesSearch() + " SIZE : " + search.getAttributesSearch().size());
			logger.info("CallBack: " + search.getCallback());
			logger.info("Cat id: " + search.getCategoryId());
			logger.info("seach key: " + search.getSearchKey());

			Vector<String> attr = null;

			if (null != search && null != search.getAttributes())
				attr = JsonDbUtils.getExistingAttribute(Collections.enumeration(search.getAttributesSearch()), JsonDbUtils.getColumnsForModule(PRODUCTS_MODULE_NAME));
			else
				attr = new Vector<String>();

			for (String a : attr)
			{
				logger.info("Attr list : " + a);
			}
			for (String a : search.getAttributesSearch())
			{
				logger.info("Attr list : " + a + " contained ?? " + attr.contains(a));
			}

			String q = "select " + JsonDbUtils.getSelectAttributeWithAttrList(search.getAttributes(), true, PRODUCTS_MODULE_NAME);

			q += " from Elements_" + PRODUCTS_MODULE_NAME;
			q += " where live=1 ";
			boolean added = false;
			q += (null != search.getCategoryId() && !search.getCategoryId().equals("")) ? " and Category_Id='" + search.getCategoryId() + "'" : "";
			if (null != search.getAttributesSearch() && search.getAttributesSearch().size() > 0)
			{
				q += " and (";
				int idx = 0;
				for (String requestW : search.getAttributesSearch())
				{
					if (attr.contains(requestW))
					{
						added = true;
						idx += 1;
						q += requestW + " like ('" + search.getSearchKey() + "%') " + (idx <= search.getAttributesSearch().size() - 1 ? " or " : "");

					}
				}
				if (!added)
				{
					q += "1=2";
				}
				q += " ) ";
			}
			return q;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			return null;
		}


	}

}
