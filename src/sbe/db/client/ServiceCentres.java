package sbe.db.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import webdirector.db.DButils;

/**
 * Created by IntelliJ IDEA. User: Andrew Davidson Date: 01-Apr-2004 Time:
 * 11:01:52
 * 
 * Locations module utility class.
 */
public class ServiceCentres
{
	private DButils db = new DButils();
	private Defaults d;

	// constructor
	public ServiceCentres()
	{
		d = Defaults.getInstance();
	}

	/**
	 * Returns a Vector containing String[] of the following columns from the
	 * Elements_LOCATIONS table, ignores the live flag.
	 * 
	 * ATTR_Headline,ATTR_Area,ATTR_Address,ATTR_Suburb,ATTR_PostCode,ATTR_State
	 * ,ATTR_Phone,ATTR_Fax,ATTR_Email,ATTR_URL
	 * 
	 * @param AssetID
	 * @return Vector of String[] assets from the LOCATIONS table.
	 */
	public Vector getAsset(String AssetID)
	{
		String[] cols = { "ATTR_Headline", "ATTR_Area", "ATTR_Address", "ATTR_Suburb", "ATTR_PostCode", "ATTR_State", "ATTR_Phone", "ATTR_Fax", "ATTR_Email", "ATTR_URL", "ATTRLONG_PageText", "ATTR_managerName", "ATTRFILE_managerImage" };
		String query = "select ATTR_Headline,ATTR_Area,ATTR_Address,ATTR_Suburb,ATTR_PostCode,ATTR_State,ATTR_Phone,ATTR_Fax,ATTR_Email,ATTR_URL,ATTRLONG_PageText,ATTR_managerName,ATTRFILE_managerImage" + " from Elements_LOCATIONS where Element_id = ?";
		//Vector v = db.select(query, cols);
		Vector v = db.selectQuery(query, new String[] { AssetID });


		return v;
	}

	/**
	 * Wrapper to search
	 * 
	 * @param Brand_ID
	 * @param Category_ID
	 * @param categoryLevel
	 * @param State
	 * @param Area
	 * @param Suburb
	 * @param Postcode
	 * @param PostcodeSurroundings
	 * @return
	 */
	public Vector search(String Brand_ID, String Category_ID, int categoryLevel, String State, String Area, String Suburb, String[] Postcode, String PostcodeSurroundings)
	{
		return (search(Brand_ID, Category_ID, categoryLevel, State, Area, Suburb, Postcode, PostcodeSurroundings, null));
	}

	/**
	 * *** BrandID is unused ***
	 * 
	 * Allows the user to optionally set any of the category_ID, State, Suburb,
	 * Area, Postcode whilst searching for LOCATIONS. Any combination can be
	 * used except for the Suburb. In order to use the Suburb the
	 * PostcodeSurroundings argument must be non-null Any matching LOCATIONS to
	 * the above criteria are then returned to the user.
	 * 
	 * sortColumn defaults to AREA if set to NULL otherwise you can specify the
	 * sort column
	 * 
	 * @param Brand_ID
	 * @param Category_ID
	 * @param categoryLevel
	 * @param State
	 * @param Area
	 * @param Suburb
	 * @param Postcode
	 * @param PostcodeSurroundings
	 * @param sortColumn
	 * @return a Vector of matching String[] containing "ATTR_Headline",
	 *         "ATTR_Area", "Element_id", ordered by Area
	 */

	public Vector search(String Brand_ID, String Category_ID, int categoryLevel, String State, String Area, String Suburb, String[] Postcode, String PostcodeSurroundings, String sortColumn)
	{
		return search(Brand_ID, Category_ID, categoryLevel, State, Area, Suburb, Postcode, PostcodeSurroundings, sortColumn, null);
	}

	/**
	 * *** BrandID is unused ***
	 * 
	 * Allows the user to optionally set any of the category_ID, State, Suburb,
	 * Area, Postcode whilst searching for LOCATIONS. Any combination can be
	 * used except for the Suburb. In order to use the Suburb the
	 * PostcodeSurroundings argument must be non-null Any matching LOCATIONS to
	 * the above criteria are then returned to the user.
	 * 
	 * sortColumn defaults to AREA if set to NULL otherwise you can specify the
	 * sort column
	 * 
	 * @param Brand_ID
	 * @param Category_ID
	 * @param categoryLevel
	 * @param State
	 * @param Area
	 * @param Suburb
	 * @param Postcode
	 * @param PostcodeSurroundings
	 * @param sortColumn
	 * @return a Vector of matching String[] containing "ATTR_Headline",
	 *         "ATTR_Area", "Element_id", ordered by Area
	 */

	public Vector search(String Brand_ID, String Category_ID, int categoryLevel, String State, String Area, String Suburb, String[] Postcode, String PostcodeSurroundings, String sortColumn, String limit)
	{
		String sortColumnValue = "ele.ATTR_Area";
		if (sortColumn != null)
			sortColumnValue = sortColumn;

		String[] cols = { "ATTR_Headline", "ATTR_Area", "Element_id" };
		String query = "select ele.ATTR_Headline, ele.ATTR_Area, ele.Element_id from Elements_LOCATIONS ele, Categories_LOCATIONS cat where ";

		System.out.println("PostcodeSurroundings " + PostcodeSurroundings);

		// if ( Brand_ID != null && !Brand_ID.equals("0") &&
		// !Brand_ID.equals("") )
		// query +=
		// "cat.Category_id = "+Brand_ID+" and cat.folderLevel = 1 and ";
		if (Category_ID != null && !Category_ID.equals("0") && !Category_ID.equals(""))
			query += "cat.Category_id = " + DatabaseValidation.encodeParam(Category_ID) + " and cat.folderLevel = " + DatabaseValidation.encodeParam("" + categoryLevel) + " and ";
		if (State != null && !State.equals("0") && !State.equals(""))
			query += "ele.ATTR_State = '" + DatabaseValidation.encodeParam(State) + "' and ";
		if (Area != null && !Area.equals("0") && !Area.equals(""))
			query += "ele.ATTR_Area = '" + DatabaseValidation.encodeParam(Area) + "' and ";
		if (Suburb != null && !Suburb.equals("0") && !Suburb.equals(""))
		{
			if (PostcodeSurroundings == null)
				query += "ele.ATTR_Suburb = '" + DatabaseValidation.encodeParam(Suburb) + "' and ";
		}

		if (Postcode != null && !Postcode.equals("0") && !Postcode.equals(""))
		{
			query += "ele.ATTR_Postcode in (";
			for (int i = 0; i < (Postcode.length - 1); i++)
			{
				query += "'" + DatabaseValidation.encodeParam(Postcode[i]) + "', ";
			}
			query += "'" + Postcode[(Postcode.length - 1)] + "') and ";
		}

		query += "ele.Category_id = cat.Category_id order by " + sortColumnValue + "";

		if (limit != null && !limit.equals("null"))
		{
			query += " limit " + limit;
		}
		//Vector v = db.select(query, cols);
		// TODO - validation added on parameters
		Vector v = db.selectQuery(query);

		return v;
	}

	/**
	 * Wrapper for other search method but hard codes the third argument
	 * (categoryLevel) to 2. This method is purely for backward compatibility
	 * with an existing site. Please use other search method.
	 * 
	 * @param Brand_ID
	 * @param Category_ID
	 * @param State
	 * @param Area
	 * @param Suburb
	 * @param Postcode
	 * @param PostcodeSurroundings
	 * @return
	 */
	public Vector search(String Brand_ID, String Category_ID, String State, String Area, String Suburb, String[] Postcode, String PostcodeSurroundings)
	{
		return search(Brand_ID, Category_ID, 2, State, Area, Suburb, Postcode, PostcodeSurroundings);
	}

	/**
	 * Given a non-null brand this method returns the HTML markup for a
	 * &lt;OPTION...> drop down for all locations. The brandID supplied will put
	 * a ... SELECTED ... statement inside the &lt;OPTION...> if a matching
	 * LOCATION is returned from the db.
	 * 
	 * @param moduleName
	 * @param c1id
	 *            non-optional but it doesn't have to match anything in the DB
	 *            necessarily
	 * @return HTML &lt;OPTION...> for all the category LOCATIONS
	 */
	@Deprecated
	public String getBrandOptions(String moduleName, String c1id)
	{
		return getC1(moduleName, c1id, false, null);
	}

	public String getC1(String moduleName, String c1id, boolean checkLogin, String userID)
	{
		return getCategoryLevel(moduleName, c1id, "1", null, checkLogin, userID);
	}

	/**
	 * Wrapper function to getCategoryLevel using a hard coded '2' for the
	 * folderLevel 3rd argument. Used for backwards compatibility on all those
	 * sites which have 2 level category modules. Should use getCategoryLevel
	 * not this one.
	 * 
	 * @param moduleName
	 * @param moduleSelected
	 * @return HTML &lt;OPTION...> for all the categories in the module given at
	 *         level 2
	 */
	@Deprecated
	public String getProductOptions(String moduleName, String selectedID)
	{
		return getAllC2(moduleName, selectedID, false, null);
	}

	public String getAllC2(String moduleName, String selectedID, boolean checkLogin, String userID)
	{
		return getCategoryLevel(moduleName, selectedID, "2", null, checkLogin, userID);
	}

	/**
	 * Very similar to getProductOptions() in that it returns HTML for an
	 * &lt;OPTION....> list of categories but this time the set returned are
	 * only those within the brandID specified ( the parent category )
	 * 
	 * @param moduleName
	 * @param categoryID
	 * @param brandID
	 * @return HTML &lt;OPTION...> for all the categories in the module given at
	 *         level 2
	 */
	@Deprecated
	public String getProductOptionsWithinABrand(String moduleName, String c2id, String c1id)
	{
		return getC2(moduleName, c2id, c1id, false, null);
	}

	public String getC2(String moduleName, String c2id, String c1id, boolean checkLogin, String userID)
	{
		return getCategoryLevel(moduleName, c2id, "2", c1id, checkLogin, userID);
	}

	/**
	 * Wrapper method for getAllStates(String State, String Category_ID, String
	 * Brand_ID) using getAllStates( State, null, null ) to call it. i.e. the
	 * last two arguments are just set to null which means there is no state
	 * already selected.
	 * 
	 * @param State
	 * @return HTML &lt;OPTION...> for all the unique states in the locations
	 *         table
	 */
	public String getAllStates(String State)
	{
		return (getAllStates(State, null, null));
	}

	/**
	 * Returns HTML <OPTION...> for all the unique states in the locations
	 * table. Given a matching Category_ID in the set of states returned a
	 * SELECTED attribute will be added to the markup to show this option has
	 * been pre-selected.
	 * 
	 * @param State
	 * @param Category_ID
	 * @param Brand_ID
	 * @return HTML &lt;OPTION...> for all the unique states in the locations
	 *         table
	 */
	public String getAllStates(String State, String Category_ID, String Brand_ID)
	{
		// String[] states = { "ACT","NSW","NT","QLD","SA","TAS","VIC","WA"};

		StringBuffer sb = new StringBuffer();
		//String [] params = new String[]{};
		List<String> params = new ArrayList<String>();

		String whereClause = "";
		if (Category_ID != null)
		{
			whereClause = "where Category_ID = ?";
			params.add(Category_ID);
		}

		String query = "select distinct(ATTR_State) from Elements_LOCATIONS " + whereClause;

		//Vector v = db.select(query);
		Vector v = db.selectQuerySingleCol(query, params.toArray());

		if (v != null)
		{
			for (int i = 0; i < v.size(); i++)
			{
				String selected = "";
				String val = (String) v.elementAt(i);

				if (State != null && State.equals(val))
					selected = "SELECTED";
				sb.append("<option value='" + val + "' " + selected + ">" + val + "</option>");
			}
		}

		return sb.toString();
	}

	/**
	 * Returns HTML &lt;OPTION...> for all the Areas in a given state if one is
	 * specified.
	 * 
	 * @param stateSelected
	 * @param areaSelected
	 * @return
	 */
	public String getAreasInState(String stateSelected, String areaSelected)
	{
		return (getAreasInState(stateSelected, areaSelected, null, null));
	}

	/**
	 * Returns HTML &lt;OPTION...> for all the Areas in a given state if one is
	 * specified. Also if the Category_ID is given only returns Areas within
	 * that Category too.
	 * 
	 * @param stateSelected
	 * @param areaSelected
	 * @param Category_ID
	 * @param Brand_ID
	 * @return
	 */
	public String getAreasInState(String stateSelected, String areaSelected, String Category_ID, String Brand_ID)
	{
		StringBuffer sb = new StringBuffer();

		String whereClause = "";
		List<String> params = new ArrayList<String>();

		if (stateSelected != null && !stateSelected.equals(""))
		{
			whereClause = "where ATTR_State = ?";
			params.add(stateSelected);
			if (Category_ID != null)
			{
				whereClause += " and Category_ID = ?";
				params.add(Category_ID);
			}
		}

		String query = "select distinct(ATTR_Area) from Elements_LOCATIONS " + whereClause;

		//Vector v = db.select(query);
		Vector v = db.selectQuery(query, params.toArray());

		if (v != null)
		{
			for (int i = 0; i < v.size(); i++)
			{
				String selected = "";
				String val = (String) v.elementAt(i);

				if (areaSelected != null && areaSelected.equals(val))
					selected = "SELECTED";

				sb.append("<OPTION value='" + val + "' " + selected + ">" + val + "</OPTION>");
			}
		}

		return sb.toString();
	}

	/**
	 * Allows the user to return HTML markup for a set of &lt;OPTION....>
	 * statements containing all the category entries (ignores the live flag)
	 * Allows you to optionally constrain the set of &lt;OPTIONs...> returned by
	 * specifing the categories parent (brandID). If moduleSelected is specified
	 * also then a matching entry in the returned list will have the SELECTED
	 * attribute added to denote it was pre-selected.
	 * 
	 * @param moduleName
	 *            MANDATORY argument
	 * @param selectedID
	 * @param folderLevel
	 *            MANDATORY argument
	 * @param c1id
	 * @return HTML &lt;OPTION...> for all the categories in the 'moduleName'
	 *         Category table constrained by the other arguments above.
	 */
	private String getCategoryLevel(String moduleName, String selectedID, String folderLevel, String c1id, boolean checkLogin, String userID)
	{
		StringBuffer sb = new StringBuffer();

		List<String> params = new ArrayList<String>();
		params.add(folderLevel);

		String parentQuery = "";
		if (c1id != null && !c1id.equals(""))
		{
			parentQuery = " and Category_ParentID = ?";
			params.add(c1id);
		}

		String[] cols = { "Category_id", "ATTR_categoryName" };
		String query = "select Category_id, ATTR_categoryName from Categories_" + DatabaseValidation.encodeParam(moduleName) + " where folderLevel = ?" + parentQuery + " order by ATTR_categoryName";
		//Vector v = db.select(query, cols);
		Vector v = db.selectQuery(query, params.toArray());

		if (v != null)
		{
			webdirector.utils.EnhancedAssignCategories ac = new webdirector.utils.EnhancedAssignCategories();
			for (int i = 0; i < v.size(); i++)
			{
				String[] data = (String[]) v.elementAt(i);
				if (checkLogin && !ac.checkUserAssignCategory(moduleName, data[0], userID))
				{
					continue;
				}
				String selected = "";
				if (selectedID != null && selectedID.equals(data[0]))
					selected = "SELECTED";
				sb.append("<OPTION value='" + data[0] + "' " + selected + ">" + data[1] + "</OPTION>");
			}
		}
		return sb.toString();
	}

}
