/**
 * Created by IntelliJ IDEA.
 * User: Andrew Davidson
 * Date: Nov 16, 2003
 * Time: 7:08:59 AM
 * To change this template use Options | File Templates.
 */
package sbe.db.client;

import webdirector.db.DButils;
import webdirector.db.client.ClientDataAccess;

import java.util.Vector;

/**
 * Special Modules utility class
 */
public class LiveOffers
{
	/**
	 * Looks into the Special offers elements table to see if there are any rows
	 * at all.
	 * 
	 * @return true if there are entries in this table otherwise false.
	 */
	public boolean areThereLiveOffers()
	{
		DButils db = new DButils();
		ClientDataAccess cda = new ClientDataAccess();
		Vector v = cda.getAllElements("SPECIALOFFERS");
		if (v == null || v.size() == 0)
			return false;
		else
			return true;
	}
}
