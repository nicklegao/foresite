/*
 * ProductRegistration.java
 *
 * Created on February 3, 2007, 9:45 PM
 *
 */

package sbe.db.client;

import java.io.PrintStream;
import java.util.Vector;
import java.util.Hashtable;

import au.net.webdirector.common.Defaults;
import webdirector.db.DButils;

/**
 *
 * @author amitksh
 */
public class ProductRegistration
{

	/** Creates a new instance of ProductRegistration */
	public ProductRegistration()
	{
		db = new DButils();
		d = Defaults.getInstance();
	}

	public boolean insertProductRegistration(Hashtable htRecord)
	{
		int id = db.insertData(htRecord, "Element_id", "Elements_FLEX6");
		if (id == 0)
			return false;
		else
			return true;
	}

	private DButils db;
	private Defaults d;
}
