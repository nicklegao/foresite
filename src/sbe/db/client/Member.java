package sbe.db.client;


/**
 * Created by IntelliJ IDEA.
 * User: CorpInteractive
 * Date: 7/02/2008
 * Time: 14:09:28
 * To change this template use File | Settings | File Templates.
 */
public class Member extends MemberFactory
{
	private String userEmail;
	private String userImage;
	private String onlineStatus;
	private String Location;
	private String DOB;
	private String Address;
	private String id;
	private String userName;


	public String getUserEmail()
	{
		return userEmail;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public void setUserEmail(String userEmail)
	{
		this.userEmail = userEmail;
	}

	public String getUserImage()
	{
		return userImage;
	}

	public void setUserImage(String userImage)
	{
		this.userImage = userImage;
	}

	public String getLocation()
	{
		return Location;
	}

	public void setLocation(String location)
	{
		Location = location;
	}

	public String getDOB()
	{
		return DOB;
	}

	public void setDOB(String DOB)
	{
		this.DOB = DOB;
	}

	public String getAddress()
	{
		return Address;
	}

	public void setAddress(String address)
	{
		Address = address;
	}

	public String getOnlineStatus()
	{
		return onlineStatus;
	}

	public void setOnlineStatus(String onlineStatus)
	{
		this.onlineStatus = onlineStatus;
	}


}
