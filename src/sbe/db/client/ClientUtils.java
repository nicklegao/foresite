package sbe.db.client;

import webdirector.db.client.ClientFileUtils;
import webdirector.db.DButils;

import java.io.File;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.utils.module.StoreUtils;

/**
 * Created by IntelliJ IDEA.
 * User: CorpInteractive
 * Date: 5/08/2009
 * Time: 17:33:09
 * To change this template use File | Settings | File Templates.
 */
public class ClientUtils
{
	DButils db = new DButils();
	Defaults d = Defaults.getInstance();

	public boolean updateLongTextFile(String colName, String colValue, String module, int id, String tableType)
	{
		boolean longTextFileExists = true;
		boolean b = true;
		String storeDir = d.getStoreDir();

		String categories = "Categories";
		String sep1 = d.getOSTypeDirSep();
		String sep2 = "/";
		if (tableType.equals("Elements"))
		{
			sep1 = "";
			sep2 = "";
			categories = "";
		}

		// build filename for long text field
		String dir = storeDir + d.getOSTypeDirSep() + module + sep1 + categories + d.getOSTypeDirSep() + id + d.getOSTypeDirSep() + colName;
		String dest = dir + d.getOSTypeDirSep() + colName + ".txt";

		// does file already exist ? If not create it
		File f = new File(dest);
		if (!f.exists())
		{
			longTextFileExists = false;
			try
			{
				File d = new File(dir);
				d.mkdirs();
				f.createNewFile();
			}
			catch (Exception ex)
			{
				System.out.println(ex.getMessage());
			}
		}

		// update contents of file with text
		updateDB(tableType, id, module, colName, "/" + module + sep2 + categories + "/" + id + "/" + colName + "/" + colName + ".txt");
		ClientFileUtils cfu = new ClientFileUtils();
		try
		{
			cfu.overwriteTextContents(dest, colValue);
		}
		catch (Exception ex)
		{
			System.out.println(ex.getMessage());
		}

		return b;
	}

	public String moveFileTostores(String src, String module, String tablePrefix, String attributeName,
			boolean removeFile, boolean generateThumb, String elementId)
	{
		StoreUtils storeUtils = new StoreUtils();
		File srcFile = new File(src);
		String fileName = srcFile.getName();
		String dest;
		if (tablePrefix.equals("Categories"))
			dest = d.getStoreDir() + d.getOSTypeDirSep() + module + d.getOSTypeDirSep() + "Categories" + d.getOSTypeDirSep() + elementId + d.getOSTypeDirSep() + attributeName;
		else
			dest = d.getStoreDir() + d.getOSTypeDirSep() + module + d.getOSTypeDirSep() + elementId + d.getOSTypeDirSep() + attributeName;

		// we're updating, so remove any files if they exist
		if (removeFile)
			d.removeExistingFile(Integer.valueOf(elementId), attributeName, module, tablePrefix);

		if (d.getDEBUG())
		{
			System.out.println("attr " + attributeName);
			System.out.println("filename " + src);
			System.out.println("src " + src);
			System.out.println("dest " + dest);
		}

		// move files into real location
		if (!storeUtils.copyToStores(src, dest, generateThumb))
			return ("Could not move file " + src + " into the stores. File may already exist for this Asset ? Also check permissions and disk space");

		// return without update DB with file info, only if required
		if (tablePrefix.equalsIgnoreCase("popup.insertImage.jsp"))
			return ("All files moved to the stores ok.");

		// update database adn return okay
		String extraDirLevel = "";
		if (tablePrefix.equals("Categories"))
			extraDirLevel = "Categories/";
		if (!updateDB(tablePrefix, Integer.valueOf(elementId), module, attributeName, "/" + module + "/" + extraDirLevel + elementId + "/" + attributeName + "/" + fileName))
			return ("Couldn't update DB");

		return "OK";
	}

	private boolean updateDB(String tablePrefix, int id, String module, String attr, String file)
	{
		String query = "";

		if (tablePrefix.equals("Categories"))
			query = "update " + tablePrefix + "_" + module + " set " + attr + " = ? where category_id = ?";
		else
			query = "update " + tablePrefix + "_" + module + " set " + attr + " = ? where element_id = ?";

		int rows = db.updateData(query, new Object[] { file, id });
		System.out.println(query + " rows " + rows);
		if (rows == 1)
			return (true);
		else
			return (false);
	}


}
