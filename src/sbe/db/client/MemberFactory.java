package sbe.db.client;

import webdirector.db.client.ClientDataAccess;
import webdirector.db.DButils;

import java.util.Vector;
import java.util.Hashtable;

import au.net.webdirector.common.datalayer.util.DatabaseValidation;

/**
 * Created by IntelliJ IDEA.
 * User: CorpInteractive
 * Date: 7/02/2008
 * Time: 16:10:05
 * To change this template use File | Settings | File Templates.
 */
public class MemberFactory
{
	private ClientDataAccess cda = new ClientDataAccess();
	private DButils db = new DButils();

	public Hashtable getUserDetails(Vector userIDs)
	{
		String query;
		String cols[] = { "ATTR_EmailAddress", "ATTR_Headline", "ATTR_ContactName", "Element_id", "ATTRFILE_Image" };
		query = "Select ATTR_EmailAddress, ATTR_Headline, ATTR_ContactName, Element_id, ATTRFILE_Image from Elements_Members ";
		String where = "where Element_id in (";
		String id;
		String in = "";
		for (int i = 0; i < userIDs.size(); i++)
		{
			id = (String) userIDs.get(i);
			id = DatabaseValidation.encodeParam(id);
			if (i == (userIDs.size() - 1))
			{
				in = in.concat(id);
			}
			else
				in = in.concat(id + ",");
		}
		where = where + in + ")";
		query = query + where;
		System.out.println("Query::: getUserDetails>>>>>>>>>>>>>>>>>>>>>");
		//Vector v=db.select(query,cols);
		Vector v = db.selectQuery(query);
		System.out.println("Vector Size" + v.size());
		Hashtable ht = new Hashtable();
		ht = createMembers(v);
		return ht;
	}

	private Hashtable createMembers(Vector v)
	{
		Hashtable ht = new Hashtable();
		String memberData[];
		for (int i = 0; i < v.size(); i++)
		{
			memberData = (String[]) v.get(i);
			Member member = new Member();
			member.setUserEmail(memberData[0]);
			member.setUserName(memberData[1]);
			member.setLocation(memberData[2]);
			member.setUserImage(memberData[4]);
			ht.put(memberData[3], member);
		}

		return ht;
	}

}
