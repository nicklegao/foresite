package sbe.db.client;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import webdirector.db.client.ClientDataAccess;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.StringArrayMapper;

/**
 * Created by IntelliJ IDEA. User: CorpInteractive Date: 8/01/2009 Time:
 * 12:55:18 To change this template use File | Settings | File Templates.
 */
public class MemberUtility
{

	private static final int PASSWORD_CHANGE_DURATION = 180;
	ClientDataAccess cda = new ClientDataAccess();
	String module = "MEMBERS";

	public LoginResult checkUserLoggedIn(String username, String password)
	{

		String[] cols = { "live", "passwordduration" };
		LoginResult result = new LoginResult();
		String query = "select if((live = '1' and (Live_date is null or live_date <= now()) and (Expire_date is null or Expire_date >= now())), '1', '0') as live,ifnull(datediff(CURRENT_DATE(),ATTRDATE_PasswordDate),0) as passwordduration from elements_members where ATTR_Headline=? and ATTRPASS_Password = ?";
		List vtData = DataAccess.getInstance().select(query, new Object[] { username, password }, new StringArrayMapper());
		if (vtData != null && vtData.size() > 0)
		{
			result.userExist = true;
			String data[] = (String[]) vtData.get(0);
			result.loginOK = data[0].equals("1");
			result.pwdChangeNeeded = Integer.parseInt(data[1]) >= PASSWORD_CHANGE_DURATION;
		}
		return result;
	}

	/*
	 * public MemberInformation getMemberInformation(String username){
	 * MemberInformation memberInformation = new MemberInformation(); Vector v =
	 * cda.getElementByValue(module,"ATTR_Headline",username,true); if(v!=null
	 * && v.size()>0){ Hashtable h = (Hashtable)v.get(0);
	 * memberInformation.setFirstName((String)h.get("ATTR_ContactName"));
	 * memberInformation.setLastName((String)h.get("ATTR_ContactName"));
	 * memberInformation.setAddress((String)h.get("ATTR_Address"));
	 * memberInformation.setSuburb((String)h.get("ATTR_Address"));
	 * memberInformation.setEmail((String)h.get("ATTR_Headline"));
	 * memberInformation.setPhone((String)h.get("ATTR_MobilePhone"));
	 * memberInformation.setFax((String)h.get("ATTR_WorkPhone"));
	 * 
	 * } return memberInformation; }
	 */

	public Hashtable getMemberInformation(String username)
	{
		MemberInformation memberInformation = new MemberInformation();
		Vector v = cda.getElementByValue(module, "ATTR_Headline", username, true);
		Hashtable h = new Hashtable();
		if (v != null && v.size() > 0)
		{
			h = (Hashtable) v.get(0);

		}
		return h;
	}

	public class LoginResult
	{
		boolean loginOK;
		boolean userExist;
		boolean pwdChangeNeeded;

		public boolean isLoginOK()
		{
			return loginOK;
		}

		public boolean isUserExist()
		{
			return userExist;
		}

		public boolean isPwdChangeNeeded()
		{
			return pwdChangeNeeded;
		}

	}
}
