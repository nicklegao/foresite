// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   ShoppingBasket.java

package sbe.db;

import java.io.PrintStream;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import webdirector.db.client.ClientDataAccess;

public class ShoppingBasketMember
{

	public ShoppingBasketMember()
	{
		items = new Vector();
		itemsInBasket = 0;
	}

	public boolean addToShoppingBasket(String qty, String productID, String price)
	{
		if (doesProductExistAlready(qty, productID))
			return true;
		String products[] = {
				"", "", "", "", ""
		};
		products[PRODUCTID] = productID;
		products[QTY] = qty;
		products[PRICE] = price;
		ClientDataAccess db = new ClientDataAccess();
		Vector v = db.getElement("PRODUCTS", productID);
		if (v.size() > 0)
		{
			Hashtable h = (Hashtable) v.elementAt(0);
			products[PRODUCTNAME] = (String) h.get("ATTR_Headline");
			products[MODEL] = (String) h.get("ATTR_ModelNumber");
			items.add(products);
			itemsInBasket += Integer.parseInt(qty);
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean addToShoppingBasket(String qty, String productID, String price, String strAccountBalance)
	{
		if (doesProductExistAlready(qty, productID, strAccountBalance))
			return true;
		String products[] = {
				"", "", "", "", ""
		};
		products[PRODUCTID] = productID;
		products[QTY] = qty;
		products[PRICE] = price;
		ClientDataAccess db = new ClientDataAccess();
		Vector v = db.getElement("PRODUCTS", productID);
		if (v.size() > 0)
		{
			Hashtable h = (Hashtable) v.elementAt(0);
			products[PRODUCTNAME] = (String) h.get("ATTR_Headline");
			products[MODEL] = (String) h.get("ATTR_ModelNumber");
			items.add(products);
			itemsInBasket += Integer.parseInt(qty);
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean changeItemQty(String qty, String productID)
	{
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) items.elementAt(i);
			if (x[PRODUCTID].equals(productID))
			{
				int oldQty = Integer.parseInt(x[QTY]);
				x[QTY] = qty;
				items.removeElementAt(i);
				items.insertElementAt(x, i);
				itemsInBasket -= oldQty;
				itemsInBasket += Integer.parseInt(qty);
				return true;
			}
		}

		System.out.println("PROD not there");
		return false;
	}

	public boolean changeItemQty(String qty, String productID, String accountBalance)
	{
		String accountBalanceLeft = getAccountBalance(accountBalance);
		if (checkIfItemCanBeUpdated(accountBalanceLeft, qty, productID))
		{
			for (int i = 0; i < items.size(); i++)
			{
				String x[] = (String[]) items.elementAt(i);
				if (x[PRODUCTID].equals(productID))
				{
					int oldQty = Integer.parseInt(x[QTY]);
					x[QTY] = qty;
					items.removeElementAt(i);
					items.insertElementAt(x, i);
					itemsInBasket -= oldQty;
					itemsInBasket += Integer.parseInt(qty);
					return true;
				}
			}
		}
		System.out.println("PROD not there");
		return false;
	}

	public boolean checkIfItemCanBeUpdated(String accountBalanceLeft, String qty, String productID)
	{
		try
		{
			float price = 0.0F;
			for (int i = 0; i < items.size(); i++)
			{
				String x[] = (String[]) items.elementAt(i);
				if (x[PRODUCTID].equals(productID))
				{
					float oldPrice = Float.parseFloat(x[QTY]) * Float.parseFloat(x[PRICE]);
					float newPrice = Float.parseFloat(qty) * Float.parseFloat(x[PRICE]);
					price = newPrice - oldPrice;
				}
			}
			float accountBalanceLeftF = Float.parseFloat(accountBalanceLeft);
			return (price < 0 || (price <= accountBalanceLeftF)) ? true : false;
		}
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}

	}

	private boolean doesProductExistAlready(String qty, String productID)
	{
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) items.elementAt(i);
			if (x[PRODUCTID].equals(productID))
			{
				int tmpQty = Integer.parseInt(x[QTY]) + Integer.parseInt(qty);
				x[QTY] = String.valueOf(tmpQty);
				items.removeElementAt(i);
				items.insertElementAt(x, i);
				itemsInBasket += Integer.parseInt(qty);
				System.out.println("PROD already in there");
				return true;
			}
		}

		System.out.println("PROD not there");
		return false;
	}

	private boolean doesProductExistAlready(String qty, String productID, String strAccountBalance)
	{
		String accountBalanceLeft = getAccountBalance(strAccountBalance);
		System.out.println("doesProductExistAlready accountBalanceLeft: " + accountBalanceLeft);
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) items.elementAt(i);
			if (x[PRODUCTID].equals(productID))
			{
				if (checkIfAlreadyExistItemCanBeAdded(accountBalanceLeft, qty, productID))
				{
					System.out.println("Duplicate Item can be added");
					int tmpQty = Integer.parseInt(x[QTY]) + Integer.parseInt(qty);
					x[QTY] = String.valueOf(tmpQty);
					items.removeElementAt(i);
					items.insertElementAt(x, i);
					itemsInBasket += Integer.parseInt(qty);
					System.out.println("PROD already in there");
				}
				else
				{
					System.out.println("Duplicate Product cannot be added");
					duplicateItemAdded = false;
				}
				return true;
			}
		}
		return false;
	}

	public boolean checkIfAlreadyExistItemCanBeAdded(String accountBalanceLeft, String qty, String productID)
	{
		try
		{
			float price = 0.0F;
			for (int i = 0; i < items.size(); i++)
			{
				String x[] = (String[]) items.elementAt(i);
				if (x[PRODUCTID].equals(productID))
				{
					float newPrice = Float.parseFloat(qty) * Float.parseFloat(x[PRICE]);
					price = newPrice;
				}
			}
			float accountBalanceLeftF = Float.parseFloat(accountBalanceLeft);
			System.out.println("accountBalanceLeftF " + accountBalanceLeftF);
			System.out.println("Price of Item " + price);
			System.out.println("Item Qty" + qty);
			return (price <= accountBalanceLeftF) ? true : false;
		}
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}

	}

	public boolean clearShoppingBasket()
	{
		items.clear();
		return true;
	}

	public boolean deleteFromShoppingBasket(String productID)
	{
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) items.elementAt(i);
			if (x[PRODUCTID].equals(productID))
			{
				items.removeElementAt(i);
				return true;
			}
		}

		return false;
	}

	public String[] getProductsInBasket(int index)
	{
		return (String[]) items.elementAt(index);
	}

	public int getUniqueProductsInBasket()
	{
		if (items != null)
		{
			return items.size();
		}
		else
		{
			System.out.println("problem1");
			return 0;
		}
	}

	public int getItemsInBasket()
	{
		return itemsInBasket;
	}

	public String getProductTotal(String productID)
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) items.elementAt(i);
			if (x[PRODUCTID].equals(productID))
				price = Float.parseFloat(x[QTY]) * Float.parseFloat(x[PRICE]);
		}

		String costLabelString = moneyFormat(String.valueOf(price));
		return costLabelString;
	}

	public String getProductTotalWithoutFormat(String productID)
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) items.elementAt(i);
			if (x[PRODUCTID].equals(productID))
				price = Float.parseFloat(x[QTY]) * Float.parseFloat(x[PRICE]);
		}

		return Float.toString(price);
	}

	public String moneyFormat(String price)
	{
		try
		{
			NumberFormat nfInput = NumberFormat.getNumberInstance();
			nfInput.setMaximumFractionDigits(2);
			Number n = nfInput.parse(price);
			NumberFormat nfOutput = NumberFormat.getCurrencyInstance();
			nfOutput.setMaximumFractionDigits(2);
			return nfOutput.format(n.doubleValue());
		}
		catch (Exception e)
		{
			return "0.00";
		}
	}

	public boolean updateShoppingBasketIfPossible(String qty, String productID, String price, String accountBalance)
	{

		System.out.println("********** BMSB check account balanace ************ " + checkAccountBalanceIsAvailable(productID, accountBalance));
		if (checkAccountBalanceIsAvailable(productID, accountBalance))
		{
			addToShoppingBasket(qty, productID, price, accountBalance);
			return true;
		}
		else if (!checkAccountBalanceIsAvailable(productID, accountBalance))
		{
			System.out.println("Deleted from shopping basket " + productID);
			if (duplicateItemAdded)
			{
				deleteFromShoppingBasket(productID);
			}
			else
			{
				System.out.println("Duplicate item not deleted as it is duplicate");
				duplicateItemAdded = true;
			}
			return false;
		}
		else
		{
			if (!duplicateItemAdded)
			{
				duplicateItemAdded = true;
				return false;
			}
			else
			{
				System.out.println("Added in shopping basket " + productID);
				return true;
			}
		}
	}

	public boolean checkAccountBalanceIsAvailable(String productID, String accountBalance)
	{
		try
		{
			float accountBalanceLeft = Float.parseFloat(getAccountBalance(accountBalance));
			float productPrice = Float.parseFloat(getProductTotalWithoutFormat(productID));
			System.out.println("productPrice " + productPrice);
			System.out.println("accountBalanceLeft " + accountBalanceLeft);
			return accountBalanceLeft > 0 ? true : false;
		}
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	}

	public String getAccountBalance(String strAccountBalance)
	{
		try
		{
			float basketTotalPrice = Float.parseFloat(getBasketTotalPriceWithoutFormat());
			System.out.println("basketTotalPrice " + basketTotalPrice);
			float accountBalance = Float.parseFloat(strAccountBalance);
			float accountBalanceLeft = accountBalance - basketTotalPrice;
			//accountBalanceLeft = accountBalanceLeft> 0 ? accountBalanceLeft : 0.0F;
			System.out.println("accountBalanceLeft " + accountBalanceLeft);
			System.out.println("strAccountBalance " + strAccountBalance + " accountBalanceLeft " + accountBalanceLeft);
			return Float.toString(accountBalanceLeft);

		}
		catch (Exception e)
		{
			System.out.println(e);
			return "-1.0F";
		}
	}

	public String getAccountBalanceWithFormat(String strAccountBalance)
	{
		try
		{
			System.out.println("BM orig balance:::+" + strAccountBalance + ":: getAccountBalance " + getAccountBalance(strAccountBalance));
			String costLabelString = moneyFormat(getAccountBalance(strAccountBalance));
			return costLabelString;
		}
		catch (Exception e)
		{
			System.out.println(e);
			return null;
		}
	}

	public String getBasketTotalPrice()
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) items.elementAt(i);
			price += Float.parseFloat(x[QTY]) * Float.parseFloat(x[PRICE]);
		}

		String costLabelString = moneyFormat(String.valueOf(price));
		return costLabelString;
	}

	public String getBasketTotalPriceWithoutFormat()
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) items.elementAt(i);
			price += Float.parseFloat(x[QTY]) * Float.parseFloat(x[PRICE]);
		}

		return Float.toString(price);
	}

	public String getBasketTotalPrice(String postageCharge, float discountedPrice)
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) items.elementAt(i);
			price += Float.parseFloat(x[QTY]) * Float.parseFloat(x[PRICE]);
		}

		price += Float.parseFloat(postageCharge);
		price = price - discountedPrice;
		System.out.println("postageCharge " + postageCharge);
		System.out.println("Price " + price);
		String costLabelString = moneyFormat(String.valueOf(price));
		System.out.println("costLabelString  " + costLabelString);
		return costLabelString;
	}

	public String getShoppingtTotalPrice(String postageCharge, float discountedPrice)
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) items.elementAt(i);
			price += Float.parseFloat(x[QTY]) * Float.parseFloat(x[PRICE]);
		}
		return Float.toString(price);
	}

	public String getBasketTotalPrice(String postageCharge, float discountedPrice, float creditLimit)
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) items.elementAt(i);
			price += Float.parseFloat(x[QTY]) * Float.parseFloat(x[PRICE]);
		}
		if (creditLimit > price)
		{
			price = Float.parseFloat(postageCharge);
		}
		else
		{
			price += Float.parseFloat(postageCharge);
			price = price - discountedPrice - creditLimit;
		}

		System.out.println("postageCharge " + postageCharge);
		System.out.println("Price " + price);
		String costLabelString = moneyFormat(String.valueOf(price));
		System.out.println("costLabelString  " + costLabelString);
		return costLabelString;
	}

	public boolean compareCreditValueWithOrderAmount(String postageCharge, float discountedPrice, float creditLimit)
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) items.elementAt(i);
			price += Float.parseFloat(x[QTY]) * Float.parseFloat(x[PRICE]);
		}

		//price += Float.parseFloat(postageCharge);
		price = price - discountedPrice;
		if (price < creditLimit)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public float getTheCreditLimit(String postageCharge, float discountedPrice, float creditLimit)
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) items.elementAt(i);
			price += Float.parseFloat(x[QTY]) * Float.parseFloat(x[PRICE]);
		}

		float creditLimitLeft = creditLimit - price;
		return creditLimitLeft;
	}

	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		System.out.println("Vector (basket) contains " + itemsInBasket + " items");
		System.out.println("Vector itself contains " + items.size() + " objects");
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) items.elementAt(i);
			sb.append("Product : " + x[PRODUCTNAME] + " Qty : " + x[QTY] + " Price " + x[PRICE] + " Product ID " + x[PRODUCTID]);
		}

		return sb.toString();
	}

	public static int PRODUCTID = 0;
	public static int QTY = 1;
	public static int PRICE = 2;
	public static int PRODUCTNAME = 3;
	public static int MODEL = 4;
	private Vector items;
	private int itemsInBasket;
	private boolean duplicateItemAdded = true;

}
