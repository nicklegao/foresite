package sbe.db;

import java.io.PrintStream;
import java.text.NumberFormat;
import java.util.Hashtable;
import java.util.Vector;
import javax.crypto.NullCipher;
import webdirector.db.client.ClientDataAccess;

public class ShoppingBasket
{

	public ShoppingBasket()
	{
		items = new Vector();
		itemsInBasket = 0;
		checkFreightFlag();
	}

	private void checkFreightFlag()
	{
		// New code for SBE Frieght Calculator
		Vector vfrieght = cda.getElementByName("FLEX2", "Shopping Basket - Is Freight Calculation Used?");
		String frieghtSwitch = "";
		if (vfrieght != null && vfrieght.size() > 0)
		{
			Hashtable hfreight = (Hashtable) vfrieght.get(0);
			frieghtSwitch = (String) hfreight.get("ATTR_Value");

			if (frieghtSwitch != null && frieghtSwitch.equalsIgnoreCase("YES"))
			{
				vfrieght = cda.getElementByName("FLEX2", "Shopping Basket - What is the Freight Calculation Formula?");
				frieghtSwitch = "";
				if (vfrieght != null && vfrieght.size() > 0)
				{
					hfreight = (Hashtable) vfrieght.get(0);
					frieghtSwitch = (String) hfreight.get("ATTR_Value");
					System.out.println("BMSBE::Freight Calculator Switch for Shopping Basket " + frieghtSwitch);
					if (frieghtSwitch != null && frieghtSwitch.equalsIgnoreCase("FIXED PRICE"))
					{
						freightFlagForShoppingBasket = true;
					}
					else if (frieghtSwitch != null && frieghtSwitch.equalsIgnoreCase("PRICE BANDS"))
					{
						freightFlag = true;
					}
				}

			}
		}

		System.out.println("BMSBE:: FreightFlag for Shopping Basket::" + freightFlagForShoppingBasket + "freight Flag for Item:::" + freightFlag);
		if (freightFlagForShoppingBasket)
		{
			vfrieght = cda.getElementByName("FLEX2", "Shopping Basket - What is the Cap for Freight Cost?");
			if (vfrieght != null && vfrieght.size() > 0)
			{
				Hashtable hfreight = (Hashtable) vfrieght.get(0);
				freightCapforShoppingBasket = (String) hfreight.get("ATTR_Value");
				System.out.println("BMSBE::Freight Cap Cost Shopping Basket " + freightCapforShoppingBasket);
			}
			vfrieght = cda.getElementByName("FLEX2", "Shopping Basket - What is the Freight Fee when the Freight Calculation is FIXED?");
			if (vfrieght != null && vfrieght.size() > 0)
			{
				Hashtable hfreight = (Hashtable) vfrieght.get(0);
				fixedFreightforShoppingBasket = (String) hfreight.get("ATTR_Value");
				System.out.println("BMSBE:Fixed Freight for Shopping Basket " + fixedFreightforShoppingBasket);
			}
		}

	}

	public boolean addToShoppingBasket(String qty, String productID, String price)
	{
		return addToShoppingBasket(qty, productID, price, null);
	}

	public boolean addToShoppingBasket(String qty, String productID, String price, String size)
	{
		return addToShoppingBasket(qty, productID, price, size, null, null);
	}

	public boolean addToShoppingBasket(String qty, String productID, String price, String size, String braceletNo, String insertOrder)
	{
		if (size == null || size.equals("null"))
			size = "";
		if (braceletNo == null || braceletNo.equals("null"))
			braceletNo = "";
		if (insertOrder == null || insertOrder.equals("null"))
			insertOrder = "";
		if (braceletNo.equals(""))
		{
			if (doesProductExistAlready(qty, productID))
				return true;
		}
		String products[] = {
				"", "", "", "", "", "", "", "", "0.0", "0.0"
		};
		products[PRODUCTID] = productID;
		products[QTY] = qty;
		products[PRICE] = price;
		products[SIZE] = size;
		products[DISPLAY_ORDER] = insertOrder;
		products[BRACELETNO] = braceletNo;
		ClientDataAccess db = new ClientDataAccess();
		String moduleShopping = "PRODUCTS";
		String freight = "";
		Vector v = null;
		int kPV = productID.lastIndexOf("-PV");
		if (kPV != -1)
		{
			moduleShopping = "FLEX8";
			v = db.getElement(moduleShopping, productID.substring(0, kPV));
		}
		else
		{
			v = db.getElement(moduleShopping, productID);
		}
		if (v.size() > 0)
		{
			Hashtable h = (Hashtable) v.elementAt(0);

			if (braceletNo != null && !braceletNo.equals(""))
				products[PRODUCTNAME] = braceletNo + ":" + (String) h.get("ATTR_Headline");
			else
				products[PRODUCTNAME] = (String) h.get("ATTR_Headline");

			if (freightFlag)
			{
				freight = (String) h.get("ATTRDROP_FreightBand");
				if (freight != null && !freight.equals("null") && !freight.equals(""))
				{
					products[FREIGHT] = freight;
				}
				System.out.println("BMSBE Freight::" + (String) h.get("ATTRDROP_FreightBand"));
			}

			// New code for Klapp Frieght Calculator Ends
			products[MODEL] = (String) h.get("ATTR_ModelNumber");
			items.add(products);
			itemsInBasket += Integer.parseInt(qty);
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean changeItemQty(String qty, String productID)
	{
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) (String[]) items.elementAt(i);
			if (x[PRODUCTID].equals(productID))
			{
				int oldQty = Integer.parseInt(x[QTY]);
				x[QTY] = qty;
				items.removeElementAt(i);
				items.insertElementAt(x, i);
				itemsInBasket -= oldQty;
				itemsInBasket += Integer.parseInt(qty);
				return true;
			}
		}

		System.out.println("PROD not there");
		return false;
	}

	public boolean addFreightCost(String postCode)
	{
		System.out.println("In Add Freigh Cost Method:::>>>>");
		Double temp = 1.0;
		// Calculate freight Cost and store it back to items vector.
		String cCost = "";
		for (int j = 0; j < items.size(); j++)
		{

			String x[] = (String[]) (String[]) items.elementAt(j);
			cCost = freightCost(x[CWEIGHT], postCode);
			System.out.println("KLP cCost value in AddFreightCost Method::" + cCost);
			temp = Double.parseDouble(cCost) * Double.parseDouble(x[QTY]);
			//x[Freight]= temp.toString();
			x[FREIGHT] = cCost;
			items.removeElementAt(j);
			items.insertElementAt(x, j);
		}
		//items = temp;

		// Calculate any reduction in frieght  cost if some one buys more than one item.. and store it back to items vector.
		if (items.size() > 1)
		{
			applyFreightReduction();
		}
		return true;
	}

	private String freightCost(String cweight, String postCode)
	{
		System.out.println("KLP In  Freigh Cost Method:::>>>>weight::" + cweight + "postCode:::" + postCode);
		String freightFinal = "";
		String Min = "";
		String Max = "";
		String category = "";
		Vector v = cda.getAllCategories(frieghtModule);
		Hashtable h = new Hashtable();
		if (cweight == null || cweight.equals(""))
		{
			cweight = "0.0";
		}
		Double weight = Double.parseDouble(cweight);
		for (int i = 0; i < v.size(); i++)
		{
			h = (Hashtable) v.get(i);
			category = (String) h.get("ATTR_categoryName");
			Min = (String) h.get("ATTR_Min");
			Max = (String) h.get("ATTR_Max");
			Min = (Min != null && !Min.equalsIgnoreCase("null") && !Min.equals("")) ? Min : "0.0";
			Max = (Max != null && !Max.equalsIgnoreCase("null") && !Max.equals("")) ? Max : "0.0";
			Double dMin = Double.parseDouble(Min);
			System.out.println("KLP  min" + dMin);
			Double dMax = Double.parseDouble(Max);
			System.out.println("KLP  max" + dMax);
			if (weight >= dMin && weight <= dMax)
			{
				freightFinal = calculateFreight(postCode, Double.parseDouble(cweight), category);
				break;
			}

		}

		System.out.println("KLP final freight Value ::::>>>" + freightFinal);
		return freightFinal;
	}

	private String satchelFreight()
	{
		System.out.println("KLP satchel Freight Method::>>");
		String freightCost = "";
		Vector v = cda.getCategoryElementsByName(frieghtModule, "AP Frieght Variable");
		if (v != null && v.size() > 0)
		{
			Hashtable h = (Hashtable) v.get(0);
			freightCost = (String) h.get("ATTRFLOAT_StartRate");
		}
		freightCost = (freightCost != null && !freightCost.equalsIgnoreCase("null") && !freightCost.equals("")) ? freightCost : "0.0";
		return freightCost;
	}

	private String calculateFreight(String postcode, Double cweight, String category)
	{
		System.out.println("KLP in calculate Freight Method::>>");
		Double dPostCode = Double.parseDouble(postcode);
		Double dstartRate = 0.0;
		Double dPerKgRate = 0.0;
		String sprange1 = "";
		String sprange2 = "";
		Double dprange1 = 0.0;
		Double dprange2 = 0.0;
		Double freightCost = 0.0;
		Vector v = cda.getCategoryElementsByName(frieghtModule, category);
		for (int i = 0; i < v.size(); i++)
		{
			Hashtable h = (Hashtable) v.get(i);
			sprange1 = (String) h.get("ATTR_Postcode1");
			sprange1 = (sprange1 != null && !sprange1.equalsIgnoreCase("null") && !sprange1.equals("")) ? sprange1 : "0.0";
			sprange2 = (String) h.get("ATTR_Postcode2");
			sprange2 = (sprange2 != null && !sprange2.equalsIgnoreCase("null") && !sprange2.equals("")) ? sprange2 : "0.0";
			dprange1 = Double.parseDouble(sprange1);
			System.out.println("KLP sranage 1 in calculate Freight" + dprange1);
			dprange2 = Double.parseDouble(sprange2);
			System.out.println("KLP sranage 2 in calculate Freight" + dprange2);
			if (dPostCode >= dprange1 && dPostCode <= dprange2)
			{
				System.out.println("KLP Element id" + h.get("Element_id"));
				dstartRate = Double.parseDouble((String) h.get("ATTRFLOAT_StartRate"));
				dPerKgRate = Double.parseDouble((String) h.get("ATTRFLOAT_RatePerKg"));
				break;
			}

		}

		System.out.println("KLP start Rate in calculate Freight:::" + dstartRate);
		System.out.println("KLP per KG Rate in calculate Freight:::" + dPerKgRate);
		freightCost = dstartRate + dPerKgRate * cweight;

		System.out.println("KLP freight Cost in calculate Freight::" + freightCost);
		return freightCost.toString();
	}

	private Vector applyFreightReduction()
	{
		// Temp vector to hold items value

		Vector tempItems = new Vector();
		String freighReduction = "";
		Double dfreighReduction = 1.0;
		Double Max = 0.0;
		Double temp = 0.0;
		Double freightFinal = 0.0;
		int position = 0;
		// Find maximum priced Item
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) (String[]) items.elementAt(i);
			temp = Double.parseDouble(x[FREIGHT]);
			if (Max < temp)
			{
				Max = temp;
				position = i;
			}
		}

		// Read reduction percentage from SBE variable
		Vector v = cda.getElementByName("FLEX2", "Freight Reduction");
		if (v != null && v.size() > 0)
		{
			Hashtable h = (Hashtable) v.get(0);
			freighReduction = (String) h.get("ATTR_Value");
			freighReduction = (freighReduction != null && !freighReduction.equalsIgnoreCase("null") && !freighReduction.equals("")) ? freighReduction : "1.0";
			dfreighReduction = Double.parseDouble(freighReduction);
		}

		System.out.println("KLP  Freight Reduction rate in applyFreight Method" + dfreighReduction);
		System.out.println("KLP position in applyFreight Method" + position);

		// Apply reduction calculation all items
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) items.get(i);
			temp = Double.parseDouble(x[FREIGHT]);
			freightFinal = temp * (1 - dfreighReduction);
			System.out.println("KLP Freight Final after reduction apply:::" + freightFinal);
			if (i != position)
			{
				x[FREIGHT] = freightFinal.toString();
				items.removeElementAt(i);
				items.insertElementAt(x, i);
			}
		}


		return v;
	}


	private boolean doesProductExistAlready(String qty, String productID)
	{
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) (String[]) items.elementAt(i);
			if (x[PRODUCTID].equals(productID))
			{
				int tmpQty = Integer.parseInt(x[QTY]) + Integer.parseInt(qty);
				x[QTY] = String.valueOf(tmpQty);
				items.removeElementAt(i);
				items.insertElementAt(x, i);
				itemsInBasket += Integer.parseInt(qty);
				System.out.println("PROD already in there");
				return true;
			}
		}

		System.out.println("PROD not there");
		return false;
	}

	public boolean clearShoppingBasket()
	{
		items.clear();
		return true;
	}

	public boolean deleteFromShoppingBasket(String productID)
	{
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) (String[]) items.elementAt(i);
			if (x[PRODUCTID].equals(productID))
			{
				items.removeElementAt(i);
				return true;
			}
		}

		return false;
	}

	public boolean deleteBraceletFromShoppingBasket(String braceletNo)
	{
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) (String[]) items.elementAt(i);
			System.out.println("\nx[BRACELETNO]:" + i + ":" + x[BRACELETNO] + ":" + braceletNo);
			if (x[BRACELETNO].equalsIgnoreCase(braceletNo))
			{
				items.removeElementAt(i);
				deleteBraceletFromShoppingBasket(braceletNo);
			}
		}

		return false;
	}

	public String[] getProductsInBasket(int index)
	{
		return (String[]) items.elementAt(index);
	}

	public int getUniqueProductsInBasket()
	{
		if (items != null)
		{
			return items.size();
		}
		else
		{
			System.out.println("problem1");
			return 0;
		}
	}

	public int getItemsInBasket()
	{
		return itemsInBasket;
	}

	public int getItemsInBasketExcludingSampleProducts()
	{
		int itemsInBasketExcludingSample = itemsInBasket;
		System.out.println((new StringBuilder()).append("itemsInBasketExcludingSample ").append(itemsInBasketExcludingSample).toString());
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) (String[]) items.elementAt(i);
			System.out.println((new StringBuilder()).append("Price ").append(Float.parseFloat(x[PRICE])).toString());
			if ((double) Float.parseFloat(x[PRICE]) == 0.0D)
			{
				System.out.println((new StringBuilder()).append("Subtracting sample product ").append(x[PRICE]).toString());
				itemsInBasketExcludingSample -= (new Integer(x[QTY])).intValue();
			}
		}

		return itemsInBasketExcludingSample;
	}

	public String getProductTotal(String productID)
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) (String[]) items.elementAt(i);
			if (x[PRODUCTID].equals(productID))
				price = (Float.parseFloat(x[QTY]) * (Float.parseFloat(x[PRICE]) + Float.parseFloat(x[FREIGHT])));
		}

		String costLabelString = moneyFormat(String.valueOf(price));
		return costLabelString;
	}

	public String getProductTotal(String productID, float discountedPrice)
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) (String[]) items.elementAt(i);
			if (x[PRODUCTID].equals(productID))
				price = (Float.parseFloat(x[QTY]) * (Float.parseFloat(x[PRICE]) + Float.parseFloat(x[FREIGHT])));
		}

		price -= discountedPrice;
		String costLabelString = moneyFormat(String.valueOf(price));
		return costLabelString;
	}

	public String moneyFormat(String price)
	{
		try
		{
			NumberFormat nfInput = NumberFormat.getNumberInstance();
			nfInput.setMaximumFractionDigits(2);
			Number n = nfInput.parse(price);
			NumberFormat nfOutput = NumberFormat.getCurrencyInstance();
			nfOutput.setMaximumFractionDigits(2);
			return nfOutput.format(n.doubleValue());
		}
		catch (Exception e)
		{
			return "0.00";
		}
	}

	/**
	 * Method had been changed to add FREIGHT cost but it will still backword
	 * compatible
	 * 
	 * @return
	 */
	public String getBasketTotalPrice()
	{
		float price = getBasketTotal();
		System.out.println("BMSBE::getBasket Total price" + price);
		// if Freight Calculator Switch for Shopping Basket is ON than add fixed freight cost to total price
		if (freightFlagForShoppingBasket && price < Float.parseFloat(freightCapforShoppingBasket))
		{
			price += Float.parseFloat(fixedFreightforShoppingBasket);
		}
		String costLabelString = moneyFormat(String.valueOf(price));
		return costLabelString;
	}


	public float getBasketTotal()
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) (String[]) items.elementAt(i);
			System.out.println("BMSBE:: getbasket freight::" + x[FREIGHT]);
			price += (Float.parseFloat(x[QTY]) * (Float.parseFloat(x[PRICE]) + Float.parseFloat(x[FREIGHT])));
		}
		System.out.println("BMSBE:: getBasketTotal price" + price);
		return price;
	}

	public String getFixedFreightforShoppingBasket(String totalCost)
	{

		totalCost = totalCost.trim();
		totalCost = totalCost.replaceAll(",", "");
		String freight = "0";

		float price = Float.parseFloat(totalCost.substring(1));

		// if Freight Calculator Switch for Shopping Basket is ON than add fixed freight cost to total price
		if (freightFlagForShoppingBasket && price < Float.parseFloat(freightCapforShoppingBasket))
		{
			freight = fixedFreightforShoppingBasket;
		}
		return freight;
	}

	/**
	 * This is special method use for webdirector sales order Enquiry Utility.
	 * This will return true if there is
	 * freight present per Item in data base.
	 * 
	 * @return
	 */
	public boolean getTypeOfFreight()
	{
		boolean perItemFreight = false;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) (String[]) items.elementAt(i);
			if (Float.parseFloat(x[FREIGHT]) > 0)
			{
				perItemFreight = true;
				break;
			}
		}
		System.out.println("BMSBE:::getTypeofFreight::" + perItemFreight);
		return perItemFreight;
	}

	public String getBasketTotalPrice(String postageCharge, float discountedPrice)
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) (String[]) items.elementAt(i);
			price += Float.parseFloat(x[QTY]) * Float.parseFloat(x[PRICE]);
		}

		price += Float.parseFloat(postageCharge);
		price -= discountedPrice;
		System.out.println((new StringBuilder()).append("postageCharge ").append(postageCharge).toString());
		System.out.println((new StringBuilder()).append("Price ").append(price).toString());
		String costLabelString = moneyFormat(String.valueOf(price));
		System.out.println((new StringBuilder()).append("costLabelString  ").append(costLabelString).toString());
		return costLabelString;
	}

	public String getShoppingtTotalPrice(String postageCharge, float discountedPrice)
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) (String[]) items.elementAt(i);
			price += Float.parseFloat(x[QTY]) * Float.parseFloat(x[PRICE]);
		}

		return Float.toString(price);
	}

	public String getBasketTotalPrice(String postageCharge, float discountedPrice, float creditLimit)
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) (String[]) items.elementAt(i);
			price += Float.parseFloat(x[QTY]) * Float.parseFloat(x[PRICE]);
		}

		if (creditLimit > price)
		{
			price = Float.parseFloat(postageCharge);
		}
		else
		{
			price += Float.parseFloat(postageCharge);
			price = price - discountedPrice - creditLimit;
		}
		System.out.println((new StringBuilder()).append("postageCharge ").append(postageCharge).toString());
		System.out.println((new StringBuilder()).append("Price ").append(price).toString());
		String costLabelString = moneyFormat(String.valueOf(price));
		System.out.println((new StringBuilder()).append("costLabelString  ").append(costLabelString).toString());
		return costLabelString;
	}

	public boolean compareCreditValueWithOrderAmount(String postageCharge, float discountedPrice, float creditLimit)
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) (String[]) items.elementAt(i);
			price += Float.parseFloat(x[QTY]) * Float.parseFloat(x[PRICE]);
		}

		price -= discountedPrice;
		return price < creditLimit;
	}

	public float getTheCreditLimit(String postageCharge, float discountedPrice, float creditLimit)
	{
		float price = 0.0F;
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) (String[]) items.elementAt(i);
			price += Float.parseFloat(x[QTY]) * Float.parseFloat(x[PRICE]);
		}

		float creditLimitLeft = creditLimit - price;
		return creditLimitLeft;
	}

	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		System.out.println((new StringBuilder()).append("Vector (basket) contains ").append(itemsInBasket).append(" items").toString());
		System.out.println((new StringBuilder()).append("Vector itself contains ").append(items.size()).append(" objects").toString());
		for (int i = 0; i < items.size(); i++)
		{
			String x[] = (String[]) (String[]) items.elementAt(i);
			sb.append((new StringBuilder()).append("Product : ").append(x[PRODUCTNAME]).append(" Qty : ").append(x[QTY]).append(" Price ").append(x[PRICE]).append(" Product ID ").append(x[PRODUCTID]).toString());
		}

		return sb.toString();
	}

	public String getFrieghtModule()
	{
		return frieghtModule;
	}

	public void setFrieghtModule(String frieghtModule)
	{
		this.frieghtModule = frieghtModule;
	}

	public static int PRODUCTID = 0;
	public static int QTY = 1;
	public static int PRICE = 2;
	public static int PRODUCTNAME = 3;
	public static int MODEL = 4;
	public static int SIZE = 5;
	public static int DISPLAY_ORDER = 6;
	public static int BRACELETNO = 7;
	public static int FREIGHT = 8;
	public static int CWEIGHT = 9;
	private Hashtable freightVariable = new Hashtable();
	private Vector items;
	private int itemsInBasket;
	private String frieghtModule = "FLEX19";
	private ClientDataAccess cda = new ClientDataAccess();
	public boolean freightFlag = false; // true : frieght per item is on, false: freight per Item is off
	public boolean freightFlagForShoppingBasket = false;
	public String freightCapforShoppingBasket = "0";
	public String fixedFreightforShoppingBasket = "0";



}
