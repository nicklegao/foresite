// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   DBinsertSalesOrder.java

package sbe.db;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;

// Referenced classes of package webdirector.db:
//            DBaccess

public class DBinsertSalesOrder
{

	public DBinsertSalesOrder()
	{
	}

	public int setCategoriesSalesOrder(HttpServletRequest request, String countryCheck, int orderNumber,
			float postageCharge, float calculatedCreditLimit, float creditLimitLeft, String discountFlag)
	{
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String citysuburb = request.getParameter("citysuburb");
		String postcode = request.getParameter("postcode");
		String state = request.getParameter("state");
		String conactEmail = request.getParameter("email");
		String phone = request.getParameter("phone");
		String fax = request.getParameter("fax");
		String comments = request.getParameter("comments");
		Hashtable htOrderInformation = new Hashtable();
		htOrderInformation.put("Category_ParentID", "0");
		htOrderInformation.put("folderLevel", "1");
		htOrderInformation.put("ATTR_categoryName", name + ":" + orderNumber);
		htOrderInformation.put("ATTRCHECK_Live", "1");
		htOrderInformation.put("ATTR_OrderNumber", Integer.toString(orderNumber));
		htOrderInformation.put("ATTRDATE_OrderDate", getCurrentDate());
		htOrderInformation.put("ATTR_CustomerName", name);
		htOrderInformation.put("ATTR_DeliveryAddressLine1", address);
		htOrderInformation.put("ATTR_DeliveryAddressLine2", citysuburb);
		htOrderInformation.put("ATTR_DeliveryPostCode", postcode);
		htOrderInformation.put("ATTR_DeliveryState", state);
		htOrderInformation.put("ATTR_EmailAddress", conactEmail);
		htOrderInformation.put("ATTR_PhoneNumber", phone);
		htOrderInformation.put("ATTR_FaxNumber", fax);
		htOrderInformation.put("ATTR_ContactName", comments);
		htOrderInformation.put("ATTRCHECK_YesNo1", countryCheck);
		htOrderInformation.put("ATTRCHECK_YesNo2", "0");
		htOrderInformation.put("ATTRFLOAT_PostageCharge", Float.toString(postageCharge));
		htOrderInformation.put("ATTRFLOAT_CalculatedCreditLimit", Float.toString(calculatedCreditLimit));
		htOrderInformation.put("ATTRFLOAT_CreditLimitLeft", Float.toString(creditLimitLeft));
		htOrderInformation.put("ATTRCHECK_UseMemberDiscount", discountFlag);

		System.out.println("Inside new Cateories Sales Orders");
		int categoryId = dbA.insertData(htOrderInformation, "Category_Id", "CATEGORIES_SALESORDERS");
		return categoryId;
	}

	public void setElementsSalesOrder(String productName, String productUnitPrice, String productModel, String prodQty, int categoryId, int orderNumber, float discountPriceD, String promotionalCode)
	{
		StringBuffer sqlQueryB = new StringBuffer("INSERT INTO ELEMENTS_SALESORDERS ( Language_id, Status_id, Lock_id, Version,  Live_Date,   Create_Date, Live, Category_Id, ATTR_HeadLine, ATTR_ProductNumber, ATTR_OrderNumber, ATTR_OrderQty , ATTRCURRENCY_UnitPrice, ATTRCURRENCY_UnitFreightPrice, ATTRCURRENCY_Tax, ATTRFLOAT_Discount, ATTR_PromotionalCode)  VALUES (");
		sqlQueryB.append("'1',").append("'1',").append("'1',").append("'1',");
		sqlQueryB.append("?,").append("?,").append("'1',");
		sqlQueryB.append("?,").append("?,").append("?,").append("?,").append("?,");
		sqlQueryB.append("?,").append("0,").append("0,").append("?,").append("?)");
		System.out.println("sqlQueryB " + sqlQueryB.toString());
		//int elementId = dbA.insertData(sqlQueryB.toString(), "Element_Id", "ELEMENTS_SALESORDERS");
		int elementId = dbA.insertQuery(sqlQueryB.toString(), new Object[] { getCurrentDate(), getCurrentDate(), categoryId, orderNumber + ":" + productModel, productModel, orderNumber, prodQty, productUnitPrice
				, discountPriceD, promotionalCode });
	}

	private static String getCurrentDate()
	{
		Calendar cal = new GregorianCalendar();
		int year = cal.get(1);
		int month = cal.get(2) + 1;
		int day = cal.get(5);
		return month + "/" + day + "/" + year;
	}

	static DBaccess dbA = new DBaccess();

}
