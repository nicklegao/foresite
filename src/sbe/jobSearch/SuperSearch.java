package sbe.jobSearch;

import sbe.Search.SearchItems;
import webdirector.db.client.ClientDataAccess;

import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;

/**
 * Created by IntelliJ IDEA. User: Administrator Date: 16/09/2008 Time: 20:57:31
 * To change this template use File | Settings | File Templates.
 */
public class SuperSearch
{

	// it's not that super really...
	// so there are three cases 1) text only 2) category only 3) text and category
	// straight forward if it's text just pass back the results
	// if it's category we need to run another search
	// if it's both we need to filter the text results by the category
	// and of course by category in all the above I mean sector/category/location

	private String searchString;
	private String[] category1;
	private String[] category2;
	private int minSalary = 0;
	private int maxSalary = 99999999;
	private String searchModule;

	private SearchItems[] searchItems;
	private DBaccess db = new DBaccess();

	public SuperSearch(String searchString, String category1, String category2, SearchItems[] searchItems, String searchModule)
	{
		this.searchString = searchString;
		this.category1 = new String[] { category1 };
		this.category2 = new String[] { category2 };
		this.searchItems = searchItems;
		this.searchModule = searchModule;
	}

	public SuperSearch(String searchString, String[] category1, String[] category2, SearchItems[] searchItems, int minSalary, int maxSalary, String searchModule)
	{
		this.searchString = searchString;
		this.category1 = category1;
		this.category2 = category2;
		this.searchItems = searchItems;
		this.searchModule = searchModule;
		this.minSalary = minSalary;
		this.maxSalary = maxSalary;
	}

	public SearchItems[] getRefinedList()
	{
		String c1ids = contact(category1, "", "Select a Category1");
		String c2names = contact(category2, "'", "Select a Category2");
		String eids = "";
		for (int i = 0; i < searchItems.length; i++)
		{
			eids += searchItems[i].getId() + ",";
		}
		if (eids.length() != 0)
		{
			eids = eids.substring(0, eids.length() - 1);
		}

		String query = "select e.element_id from Elements_" + searchModule + " e, categories_" + searchModule + " c1, categories_" + searchModule + " c2 " + "where e.live = 1 and (e.Live_date <= now() or e.Live_date IS NULL ) and (e.Expire_date >= now() or e.Expire_date IS NULL ) " + "and c1.category_id = c2.category_parentID and c2.category_id = e.category_id ";

		// build query up
		if (c1ids.length() != 0)
		{
			query += " and c1.category_id in (" + c1ids + ") ";
		}
		if (c2names.length() != 0)
		{
			query += " and c2.ATTR_categoryName in (" + c2names + ") ";
		}
		if (eids.length() != 0)
		{
			query += " and e.element_id in (" + eids + ") ";
		}
		if (searchString != null && !"".equals(searchString) && eids.length() == 0)
		{
			query += " and 1!=1 ";
		}
		query += " and ((e.ATTRINTEGER_salaryFrom = 0 and e.ATTRINTEGER_salaryTo = 0) ";
		query += " or (e.ATTRINTEGER_salaryFrom<=" + maxSalary + " and e.ATTRINTEGER_salaryTo >= " + minSalary + " and e.ATTRINTEGER_salaryTo != 0) ";
		query += " or (e.ATTRINTEGER_salaryFrom<=" + maxSalary + " and e.ATTRINTEGER_salaryTo = 0) ) ";
		query = query + " order by e.ATTRDATE_Date1 desc";

		System.out.println("BM final Query::" + query);
		// execute query
		// TODO complex query
		Vector elementIDs = db.select(query);
		// transpose vector of strings into SearchItems;
		SearchItems[] newSearchItems = new SearchItems[elementIDs.size()];

		for (int i = 0; i < elementIDs.size(); i++)
		{
			String id = (String) elementIDs.elementAt(i);

			newSearchItems[i] = new SearchItems(-1, id, 0, searchModule);
		}

		return newSearchItems;
	}

	private String contact(String[] ids, String wrapper, String exception)
	{
		String ret = "";
		for (int i = 0; i < ids.length; i++)
		{
			if (ids[i].trim().equals(exception))
			{
				ret = "";
				break;
			}
			ret += wrapper + ids[i] + wrapper + ",";
		}
		if (ret.length() != 0)
		{
			ret = ret.substring(0, ret.length() - 1);
		}
		return ret;
	}

	/**
	 * this method will give categoryName of asset and Parent CategoryName of
	 * category of that asset referece by element_id this is very specific to
	 * jobsearch module.
	 * 
	 * @param element_id
	 * @return
	 */
	public String[] getCategoryDetails(String element_id)
	{
		String[] data = { "", "", "", "" };
		String query = "select e.element_id,c2.ATTR_categoryName,c1.ATTR_categoryName,e.ATTRINTEGER_salaryFrom, e.ATTRINTEGER_salaryTo from Elements_" + searchModule + " e, categories_" + searchModule + " c1, categories_" + searchModule + " c2 where c1.category_id = c2.category_parentID and c2.category_id = e.category_id  and e.element_id = ?";
		String cols[] = { "element_id", "c2.ATTR_categoryName", "c1.ATTR_categoryName" };
		// execute query
		//Vector v = db.select(query, cols);
		Vector v = db.selectQuery(query, new String[] { element_id });
		if (v != null && v.size() > 0)
		{
			String items[] = (String[]) v.get(0);
			data[0] = items[1];
			data[1] = items[2];
			data[2] = items[3];
			data[3] = items[4];
		}
		return data;
	}

}
