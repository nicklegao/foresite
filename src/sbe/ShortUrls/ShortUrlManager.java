package sbe.ShortUrls;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.admin.modules.ModuleConfiguration;
import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.client.CategoryData;
import au.net.webdirector.common.datalayer.client.ElementData;
import au.net.webdirector.common.datalayer.client.ModuleAccess;
import au.net.webdirector.common.datalayer.client.conf.ModuleAccessConfig;

public class ShortUrlManager
{

	static Logger logger = Logger.getLogger(ShortUrlManager.class);

	public static String friendly(String name)
	{
		if (StringUtils.isBlank(name))
		{
			return "";
		}
		String standard = "abcdefghijklmnopqrstuvwxyz-1234567890";
		String orig = StringUtils.lowerCase(StringEscapeUtils.unescapeXml(name));
		StringBuffer ret = new StringBuffer();
		char lastc = '-';
		for (int i = 0; i < orig.length(); i++)
		{
			char c = orig.charAt(i);
			if (c == '-' && lastc == '-')
			{
				continue;
			}
			boolean isCstandard = (standard.indexOf(c) != -1);
			if (!isCstandard && lastc == '-')
			{
				continue;
			}
			if (!isCstandard)
			{
				ret.append('-');
				lastc = '-';
				continue;
			}
			ret.append(c);
			lastc = c;
		}
		if (ret.length() > 0 && ret.charAt(ret.length() - 1) == '-')
		{
			ret.deleteCharAt(ret.length() - 1);
		}
		if (ret.length() == 0)
		{
			return "-";
		}
		return ret.toString();

	}

	public static ShortUrlManager getInstance()
	{
		return new ShortUrlManager(false);
	}

	public static ShortUrlManager getInstance(boolean logicalLive)
	{
		return new ShortUrlManager(logicalLive);
	}

	private ModuleAccess ma;

	private ShortUrlManager(boolean logicalLive)
	{
		ma = ModuleAccess.getInstance();
		if (logicalLive)
		{
			ma.setConfig(ModuleAccessConfig.logicalLive());
		}
	}

	public String getAssetID(String module, String... names)
	{
		try
		{
			List<String> lNames = new ArrayList<String>(Arrays.asList(names));
			ElementData condition = new ElementData();
			condition.put("friendly_name", lNames.remove(lNames.size() - 1));
			List<ElementData> es = ma.getElements(module, condition);
			for (ElementData e : es)
			{
				if (verifyCategoryHierarchy(module, e.getParentId(), lNames))
				{
					return e.getId();
				}
			}
			return "0";
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return "0";
		}
	}

	public String getAssetName(String module, String id)
	{
		try
		{
			ElementData e = ma.getElementById(module, id);
			if (e == null)
			{
				return "";
			}
			return friendly(e.getName());
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return "";
		}
	}

	public String[] getAssetNames(String module, String id)
	{
		try
		{
			ElementData e = ma.getElementById(module, id);
			if (e == null)
			{
				return null;
			}
			List<String> names = new ArrayList<String>();
			names.add(0, friendly(e.getName()));
			int level = ModuleConfiguration.getInstance().getModule(module).getLevels();
			String parentId = e.getParentId();
			for (int i = level; i > 0 && !"0".equals(parentId); i--)
			{
				CategoryData c = ma.getCategoryById(module, parentId);
				if (c == null)
				{
					return null;
				}
				names.add(0, friendly(c.getName()));
				parentId = c.getParentId();
			}
			return names.toArray(new String[] {});
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return null;
		}
	}

	private boolean verifyCategoryHierarchy(String module, String id, List<String> names)
	{
		try
		{
			String myId = id;
			List<String> lNames = new ArrayList<String>(names);
			while (!lNames.isEmpty())
			{
				CategoryData condition = new CategoryData();
				condition.put("friendly_name", lNames.remove(lNames.size() - 1));
				condition.setId(myId);
				CategoryData me = ma.getCategory(module, condition);
				if (me == null)
				{
					return false;
				}
				myId = me.getParentId();
			}
			return true;
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return false;
		}
	}

	public String getCateID(String module, String... names)
	{
		try
		{
			List<String> lNames = new ArrayList<String>(Arrays.asList(names));
			CategoryData condition = new CategoryData();
			condition.put("friendly_name", lNames.remove(lNames.size() - 1));
			List<CategoryData> cs = ma.getCategories(module, condition);
			for (CategoryData c : cs)
			{
				if (verifyCategoryHierarchy(module, c.getParentId(), lNames))
				{
					return c.getId();
				}
			}
			return "0";
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return "0";
		}
	}

	public String getCateName(String module, String id)
	{
		try
		{
			CategoryData c = ma.getCategoryById(module, id);
			if (c == null)
			{
				return "";
			}
			return friendly(c.getName());
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return "";
		}
	}

	public String[] getCateNames(String module, String id)
	{
		try
		{
			List<String> names = new ArrayList<String>();
			int level = ModuleConfiguration.getInstance().getModule(module).getLevels();
			String parentId = id;
			for (int i = level; i > 0 && !"0".equals(parentId); i--)
			{
				CategoryData c = ma.getCategoryById(module, parentId);
				if (c == null)
				{
					return null;
				}
				names.add(0, friendly(c.getName()));
				parentId = c.getParentId();
			}
			return names.toArray(new String[] {});
		}
		catch (Exception e)
		{
			logger.error("Error:", e);
			return null;
		}
	}

	public String getModuleShorUrl(String module)
	{
		//Vector<String> v = new DBaccess().select("select e.attr_value from elements_flex2 e, categories_flex2 c where c.category_id = e.category_id and c.attr_categoryName = '" + module + "' and attr_headline = 'Module - Short Mapping'");
		Vector<String> v = new DBaccess().selectQuerySingleCol("select e.attr_value from elements_flex2 e, categories_flex2 c where c.category_id = e.category_id and c.attr_categoryName = ? and attr_headline = 'Module - Short Mapping'", new String[] { module });
		if (v == null || v.size() == 0)
		{
			return "";
		}
		return v.get(0);
	}

	public boolean isUsingShortUrl(String module)
	{
		//Vector<String> v = new DBaccess().select("select e.attr_value from elements_flex2 e, categories_flex2 c where c.category_id = e.category_id and c.attr_categoryName = '" + module + "' and attr_headline = 'Short URLs - " + module + "'");
		Vector<String> v = new DBaccess().selectQuerySingleCol("select e.attr_value from elements_flex2 e, categories_flex2 c where c.category_id = e.category_id and c.attr_categoryName = ? and attr_headline = ?", new String[] { module, "Short URLs - " + module });
		if (v == null || v.size() == 0)
		{
			return false;
		}
		return !"NO".equalsIgnoreCase(v.get(0));
	}

	public String getAssetUrl(String module, String id)
	{
		return getAssetUrl(module, id, true);
	}

	public String getAssetUrl(String module, String id, boolean withModuleShortUrl)
	{
		if ("PRODUCTS".equalsIgnoreCase(module))
		{
			//String modelNumber = (String)new DBaccess().select("select attr_modelnumber from elements_products where element_id = '"+ id +"'").get(0);
			String modelNumber = (String) new DBaccess().selectQuerySingleCol("select attr_modelnumber from elements_products where element_id = ?", new String[] { id }).get(0);
			return (withModuleShortUrl ? getModuleShorUrl(module) : "") + "/" + StringUtils.join(getAssetNames(module, id), "/") + "/" + friendly(modelNumber);
		}
		return (withModuleShortUrl ? getModuleShorUrl(module) : "") + "/" + StringUtils.join(getAssetNames(module, id), "/");
	}

	public String getCateUrl(String module, String id)
	{
		return getCateUrl(module, id, true);
	}

	public String getCateUrl(String module, String id, boolean withModuleShortUrl)
	{
		return (withModuleShortUrl ? getModuleShorUrl(module) : "") + "/" + StringUtils.join(getCateNames(module, id), "/");
	}
}
