package sbe.ShortUrls;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import webdirector.db.client.ClientDataAccess;
import webdirector.seo.ShortUrl;
import webdirector.seo.ShortUrlGenerator;
import webdirector.seo.UrlMapping;

/**
 * Created by IntelliJ IDEA. User: CorpInteractive Date: 30/09/2010 Time:
 * 08:25:45 To change this template use File | Settings | File Templates.
 */
public class StartupURL
{

	public void init(ServletContext cxt) throws ServletException
	{
		getShortUrlMappings(cxt);
	}

	private void getShortUrlMappings(ServletContext ctx)
	{

		ShortUrlGenerator inst = ShortUrlGenerator.getInstance();
		Vector<String[]> modules = inst.getModules();
		for (String[] module : modules)
		{
			Map<String, UrlMapping> mappings = inst.generalShortUrl(module[0], module[1]);
			int depth = mappings.size();
			for (UrlMapping mapping : mappings.values())
			{
				ShortUrl rule = new ShortUrl(mapping.getFrom(), mapping.getTo(), true);
				ctx.setAttribute(module[1] + " - Map" + depth, rule);
				depth--;
			}
		}
	}

	private List getShortUrlMappingsOld(ServletContext ctx)
	{

		au.net.webdirector.common.Defaults d = au.net.webdirector.common.Defaults.getInstance();
		d.getWebSiteURL();
		// get all the live short URLs and asset names from the INFORMATION
		// module where they are live
		ClientDataAccess cda = new ClientDataAccess();
		Vector v = cda.getAllElements("FLEX2");
		String[] modules = { "NEWS", "FAQ", "ADVERT", "CONTRACTS", "DOWNLOADS", "EVENTS", "HELP", "JOBADVERTISING", "LINKS", "LOCATIONS", "PRODUCTS", "PROJECTGALLERY", "REALESTATE", "RECALL",
				"RECIPES", "REFERENCELIB", "SERVICES", "SMALLBUSINESS", "TECHNICALTESTING", "SPECIALOFFERS", };
		// where there is a short url create the rule
		List<ShortUrl> listOfUrlRewrites = new Vector<ShortUrl>();
		Hashtable h = new Hashtable();
		for (int i = 0; i < v.size(); i++)
		{
			Hashtable hashtable = (Hashtable) v.elementAt(i);
			h.put((String) hashtable.get("ATTR_Headline"), (String) hashtable.get("ATTR_Value"));
		}
		for (int i = 0; i < modules.length; i++)
		{
			if (h.get("Short URLs - " + modules[i] + " - Short Mapping Module") != null)
			{
				// String fromURL = d.getWebSiteURL() +
				// h.get("Short URLs - "+modules[i]+" - Short Mapping Module");
				// why use website like http://xxx.xxx/?
				String fromURL = (String) h.get("Short URLs - " + modules[i] + " - Short Mapping Module");
				String toURL = "" + h.get("Short URLs - " + modules[i] + " - Long Mapping Module");
				if (!toURL.equals("") && !fromURL.equals(""))
				{
					ShortUrl rule = new ShortUrl(fromURL, toURL, true);
					listOfUrlRewrites.add(rule);
					ctx.setAttribute(modules[i] + " - Map1", rule);
				}
			}
			int count = 2;
			while (h.get("Short URLs - " + modules[i] + " - Short Mapping Category " + (count - 1)) != null && h.get("Short URLs - " + modules[i] + " - Long Mapping Category " + (count - 1)) != null)
			{
				// String fromURL = d.getWebSiteURL() +
				// h.get("Short URLs - "+modules[i]+" - Short Mapping Category "
				// + (count-1));
				// why use website like http://xxx.xxx/?
				String fromURL = (String) h.get("Short URLs - " + modules[i] + " - Short Mapping Category " + (count - 1));
				String toURL = "" + h.get("Short URLs - " + modules[i] + " - Long Mapping Category " + (count - 1));
				if (!toURL.equals("") && !fromURL.equals(""))
				{
					ShortUrl rule = new ShortUrl(fromURL, toURL, true);
					listOfUrlRewrites.add(rule);
					ctx.setAttribute(modules[i] + " - Map" + count, rule);
				}
				count++;

			}
			if (h.get("Short URLs - " + modules[i] + " - Short Mapping Asset") != null)
			{
				// String fromURL = d.getWebSiteURL() +
				// h.get("Short URLs - "+modules[i]+" - Short Mapping Asset");
				String fromURL = (String) h.get("Short URLs - " + modules[i] + " - Short Mapping Asset");
				// why use website like http://xxx.xxx/?
				String toURL = "" + h.get("Short URLs - " + modules[i] + " - Long Mapping Asset");
				if (!toURL.equals("") && !fromURL.equals(""))
				{
					ShortUrl rule = new ShortUrl(fromURL, toURL, true);
					listOfUrlRewrites.add(rule);
					ctx.setAttribute(modules[i] + " - Map" + count, rule);
					count++;
				}
			}
			int count2 = 1;
			while (h.get("Short URLs - " + modules[i] + " - Short Mapping Other " + count2) != null && h.get("Short URLs - " + modules[i] + " - Long Mapping Other " + count2) != null)
			{
				// String fromURL = d.getWebSiteURL() +
				// h.get("Short URLs - "+modules[i]+" - Short Mapping Other " +
				// count2);
				// why use website like http://xxx.xxx/?
				String fromURL = (String) h.get("Short URLs - " + modules[i] + " - Short Mapping Other " + count2);
				String toURL = "" + h.get("Short URLs - " + modules[i] + " - Long Mapping Other " + count2);
				String AssetName = (String) h.get("Short URLs - " + modules[i] + " - Other " + count2 + " AssetName");
				if (!toURL.equals("") && !fromURL.equals(""))
				{
					ShortUrl rule = new ShortUrl(fromURL, toURL, true);
					listOfUrlRewrites.add(rule);
					ctx.setAttribute(modules[i] + " - Map " + AssetName, rule);
				}
				count++;
				count2++;
			}
		}

		return listOfUrlRewrites;
	}


}
