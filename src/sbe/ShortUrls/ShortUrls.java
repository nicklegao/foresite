package sbe.ShortUrls;

import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import webdirector.seo.ShortUrl;

/**
 * Created by IntelliJ IDEA. User: CorpInteractive Date: 29/09/2010 Time:
 * 12:30:56 To change this template use File | Settings | File Templates.
 */

public class ShortUrls
{

	String AssetID = "0";
	String categoryID = "0";
	String childCategoryID = "0";
	Vector URLLevel = new Vector(10);
	boolean UsingShortURL = false;
	String ModuleName;
	String CategoryName;
	String childCategoryName;
	String ModuleLevels;
	Hashtable CategoryData;
	ServletContext context;
	private au.net.webdirector.common.Defaults d = au.net.webdirector.common.Defaults.getInstance();

	/**
	 * 
	 * @param elementName
	 *            - null or empty if this doesnt exist
	 * @param categoryName
	 *            - null or empty if this doesnt exist
	 * @param ChildCategoryName
	 *            - null or empty if this doesnt exist
	 * @param elementID
	 *            - null or empty if this doesnt exist
	 * @param CategoryID
	 *            - null or empty if this doesnt exist
	 * @param ChildCategoryID
	 *            - null or empty if this doesnt exist
	 * @param moduleName
	 *            - null if this doesnt exist
	 * @param ctx
	 *            - Must be supplied
	 * 
	 *            This constructor looks up the various category/element names
	 *            within a module and tries to find an id for them. If none is
	 *            found then it attempts to assign the id's which have been
	 *            passed in. If these are invalid then it sets the id's to 0. It
	 *            also determines whether or not a module is using short URL's
	 *            The id's and urls caluclated by this function are returned to
	 *            the jsp's via other functions below
	 */

	public ShortUrls(String elementName, String categoryName, String ChildCategoryName, String elementID, String CategoryID, String ChildCategoryID, String moduleName, ServletContext ctx)
	{
		if (moduleName == null)
		{
			return;
		}
		au.net.webdirector.common.datalayer.admin.db.DBaccess db = new au.net.webdirector.common.datalayer.admin.db.DBaccess();
		webdirector.db.client.ClientDataAccess DA = new webdirector.db.client.ClientDataAccess();
		CategoryData = getCategoryData(moduleName);
		ModuleName = moduleName;
		CategoryName = categoryName;
		childCategoryName = ChildCategoryName;

		if (StringUtils.isNotBlank(CategoryID) && !StringUtils.equals(CategoryID, "0"))
		{
			this.categoryID = CategoryID;
			this.CategoryName = ShortUrlManager.getInstance().getCateName(moduleName, CategoryID);
		}
		else if (StringUtils.isNotBlank(categoryName))
		{
			categoryID = ShortUrlManager.getInstance().getCateID(moduleName, ShortUrlManager.friendly(categoryName));
		}
		else
		{
			categoryID = "0";
		}


		if (StringUtils.isNotBlank(ChildCategoryID) && !StringUtils.equals(ChildCategoryID, "0"))
		{
			this.childCategoryID = ChildCategoryID;
			this.childCategoryName = ShortUrlManager.getInstance().getCateName(moduleName, ChildCategoryID);
		}
		else if (StringUtils.isNotBlank(ChildCategoryName))
		{
			childCategoryID = ShortUrlManager.getInstance().getCateID(moduleName, ShortUrlManager.friendly(categoryName), ShortUrlManager.friendly(ChildCategoryName));
		}
		else
		{
			childCategoryID = "0";
		}

		if (StringUtils.isNotBlank(elementID) && !StringUtils.equals(elementID, "0"))
		{
			AssetID = elementID;
		}
		else if (StringUtils.isNotBlank(elementName))
		{
			String[] args = new String[] {};
			if (!categoryID.equals("0"))
			{
				args = (String[]) ArrayUtils.add(args, ShortUrlManager.friendly(categoryName));
			}
			if (!childCategoryID.equals("0"))
			{
				args = (String[]) ArrayUtils.add(args, ShortUrlManager.friendly(ChildCategoryName));
			}
			args = (String[]) ArrayUtils.add(args, ShortUrlManager.friendly(elementName));
			AssetID = ShortUrlManager.getInstance().getAssetID(moduleName, args);
		}
		else
		{
			AssetID = "0";
		}



		Vector v = DA.getElementByName("FLEX2", "Short URLs - " + moduleName);
		if (v != null && v.size() != 0)
		{
			Hashtable h = (Hashtable) v.get(0);
			System.out.println("exception " + d + " " + moduleName + " ");
			ModuleLevels = d.getModuleLevels(moduleName);
			for (int i = 1; i <= Integer.parseInt(ModuleLevels) + 2; i++)
			{
				URLLevel.add(ctx.getAttribute(moduleName + " - Map" + i));
			}
			if ("YES".equals(h.get("ATTR_Value")))
			{
				UsingShortURL = true;
			}
		}
		context = ctx;

	}

	public String getCategoryID()
	{
		return categoryID;
	}

	public String getChildCategoryID()
	{
		return childCategoryID;
	}

	public String getAssetID()
	{
		return AssetID;
	}

	/** Added by Nick begin */
	public String getURL(Hashtable h)
	{
		return "/" + d.getWarName() + getURLWithoutContextPath(h);
	}

	private String getURLWithoutContextPath(Hashtable h)
	{
		String sUrl = (String) h.get("ATTR_ShortURL");
		if (UsingShortURL && StringUtils.isNotBlank(sUrl))
		{
			return getDirectShortUrl(sUrl);
		}

		// If hashtable is an element
		String highlight = (String) h.get("ATTR_Headline");
		if (StringUtils.isNotBlank(highlight))
		{
			ShortUrl url = (ShortUrl) URLLevel.get(Integer.parseInt(ModuleLevels) + 1);
			if (UsingShortURL)
			{
				return getShortUrlForAsset(h, highlight, url);
			}
			return getNormalUrlForAsset(url, h);

		}
		// If Hashtable is a category
		ShortUrl url = (ShortUrl) URLLevel.get(getFolderLevel(h));
		if (UsingShortURL)
		{
			highlight = (String) h.get("ATTR_categoryName");
			return getShortUrlForCate(h, highlight, url);
		}
		return getNormalUrlForCate(url, h);
	}

	private String getDirectShortUrl(String sUrl)
	{
		return (sUrl.startsWith("/")) ? sUrl : "/" + sUrl;
	}

	private String getShortUrlForCate(Hashtable h, String highlight, ShortUrl url)
	{
		String fromUrl = url.getFromUrl();
		fromUrl = replace(fromUrl, ShortUrlManager.friendly(highlight));
		if (getFolderLevel(h) == 2)
		{
			fromUrl = replace(fromUrl, ShortUrlManager.friendly(getCategoryParentName(h)));
		}
		return fromUrl;
	}

	private String getShortUrlForAsset(Hashtable h, String highlight, ShortUrl url)
	{
		String fromUrl = url.getFromUrl();
		fromUrl = replace(fromUrl, ShortUrlManager.friendly(highlight));
		if (ModuleLevels.equals("1"))
		{
			fromUrl = replace(fromUrl, ShortUrlManager.friendly(CategoryName));
		}
		else if (ModuleLevels.equals("2"))
		{
			fromUrl = replace(fromUrl, ShortUrlManager.friendly(childCategoryName));
			fromUrl = replace(fromUrl, ShortUrlManager.friendly(CategoryName));
		}
		return fromUrl;
	}

	private String getNormalUrlForCate(ShortUrl url, Hashtable h)
	{
		String toUrl = url.getToUrl();
		if (getFolderLevel(h) == 2)
		{
			toUrl = toUrl.replaceAll("childCategoryName=\\$[0-9]", "childCategoryID=" + (String) h.get("Category_id"));
			toUrl = toUrl.replaceAll("categoryName=\\$[0-9]", "categoryID=" + getCategoryParent(h));
		}
		else if (getFolderLevel(h) == 1)
		{
			toUrl = toUrl.replaceAll("categoryName=\\$[0-9]", "categoryID=" + (String) h.get("Category_id"));
		}
		return toUrl;
	}

	private String getNormalUrlForAsset(ShortUrl url, Hashtable h)
	{
		String toUrl = url.getToUrl();
		if (ModuleLevels.equals("1"))
		{
			toUrl = toUrl.replaceAll("categoryName=\\$[0-9]", "categoryID=" + categoryID);
		}
		else if (ModuleLevels.equals("2"))
		{
			toUrl = toUrl.replaceAll("categoryName=\\$[0-9]", "categoryID=" + categoryID);
			toUrl = toUrl.replaceAll("childCategoryName=\\$[0-9]", "childCategoryID=" + childCategoryID);
		}
		toUrl = toUrl.replaceAll("elementName=\\$[0-9]", "elementID=" + (String) h.get("Element_id"));
		toUrl = toUrl.replaceAll("modelNumber=\\$[0-9]", "modelNumber=" + (String) h.get("ATTR_modelNumber"));
		return toUrl;
	}

	/** Added by Nick end */

	/**
	 * 
	 * Given a hashtable, this function returns a URL which would lead to it It
	 * determines whether or not the hash represents an element or a category,
	 * whether or not shorturls for the module are being used, whether or not
	 * the ATTR_ShortURL field should be used, and returns the URL.
	 * 
	 * ShortURLs should NOT be used on demo, as they do not include a warName
	 * and will not work
	 * 
	 * @param h
	 *            - A hashtable of the element/category
	 * @return - A link (URL) to the Hashtable passed through
	 */

	private String getURLOld(Hashtable h)
	{

		if (UsingShortURL && h.get("ATTR_ShortURL") != null && !h.get("ATTR_ShortURL").equals(""))
		{
			// Use short URL and the url has been specified in WD, no matter it
			// is an element or category,
			// return short url directly
			String sUrl = (String) h.get("ATTR_ShortURL");
			return (sUrl.startsWith("/")) ? sUrl : "/" + sUrl;
		}

		// If hashtable is an element
		if (h.get("ATTR_Headline") != null)
		{
			ShortUrl url = (ShortUrl) URLLevel.get(Integer.parseInt(ModuleLevels) + 1);
			if (UsingShortURL)
			{
				return transformShortURLElement(url.getFromUrl(), h, "ATTR_Headline");
			}
			return "/" + d.getWarName() + transformLongURLElement(url.getToUrl(), h);

		}
		// If Hashtable is a category
		ShortUrl url = (ShortUrl) URLLevel.get(getFolderLevel(h));
		if (UsingShortURL)
		{
			return transformShortURL(url.getFromUrl(), h, "ATTR_categoryName");
		}
		return "/" + d.getWarName() + transformLongURL(url.getToUrl(), h);
	}

	private String getAssetURL(String AssetName)
	{
		ShortUrl URLLink = (ShortUrl) context.getAttribute(ModuleName + " - Map " + AssetName);
		if (UsingShortURL)
		{
			return d.getWarName() + URLLink.getFromUrl();
		}
		else
		{
			return d.getWarName() + URLLink.getToUrl();
		}
	}

	private String transformLongURLElement(String url, Hashtable h)
	{
		if (ModuleLevels.equals("1"))
		{
			url = url.replaceFirst("categoryName=\\$1", "categoryID=" + categoryID);
			url = url.replaceFirst("elementName=\\$2", "elementID=" + (String) h.get("Element_id"));
		}
		else if (ModuleLevels.equals("2"))
		{
			url = url.replaceFirst("categoryName=\\$1", "categoryID=" + categoryID);
			url = url.replaceFirst("childCategoryName=\\$2", "childCategoryID=" + childCategoryID);
			url = url.replaceFirst("elementName=\\$3", "elementID=" + (String) h.get("Element_id"));
		}
		return url;
	}

	private String transformShortURLElement(String url, Hashtable h, String attr)
	{
		if (ModuleLevels.equals("1"))
		{
			url = url.replaceFirst("\\(\\.\\+\\)", CategoryName);
			url = url.replaceFirst("\\(\\.\\+\\)", (String) h.get(attr));
		}
		else if (ModuleLevels.equals("2"))
		{
			url = url.replaceFirst("\\(\\.\\+\\)", CategoryName);
			url = url.replaceFirst("\\(\\.\\+\\)", childCategoryName);
			url = url.replaceFirst("\\(\\.\\+\\)", (String) h.get(attr));
		}
		return url;
	}

	private String transformLongURL(String url, Hashtable h)
	{
		if (getFolderLevel(h) == 2)
		{
			url = url.replaceFirst("childCategoryName=\\$2", "childCategoryID=" + (String) h.get("Category_id"));
			url = url.replaceFirst("categoryName=\\$1", "categoryID=" + getCategoryParent(h));
		}
		else if (getFolderLevel(h) == 1)
		{
			url = url.replaceFirst("categoryName=\\$1", "categoryID=" + (String) h.get("Category_id"));
		}

		return url;
	}

	private String transformShortURL(String url, Hashtable h, String attr)
	{
		if (getFolderLevel(h) == 1)
		{
			url = url.replaceFirst("\\(\\.\\+\\)", (String) h.get(attr));
		}
		else if (getFolderLevel(h) == 2)
		{
			url = url.replaceFirst("\\(\\.\\+\\)", getCategoryParentName(h));
			url = url.replaceFirst("\\(\\.\\+\\)", (String) h.get(attr));
		}

		return url;
	}

	private int getFolderLevel(Hashtable h)
	{
		Hashtable hsh = (Hashtable) CategoryData.get(h.get("Category_id"));
		return Integer.parseInt((String) hsh.get("folderLevel"));
	}

	private int getCategoryParent(Hashtable h)
	{
		Hashtable hsh = (Hashtable) CategoryData.get(h.get("Category_id"));
		return Integer.parseInt((String) hsh.get("Category_ParentID"));
	}

	private String getCategoryParentName(Hashtable h)
	{
		Hashtable self = (Hashtable) CategoryData.get(h.get("Category_id"));
		Hashtable hsh = (Hashtable) CategoryData.get(self.get("Category_ParentID"));
		// if(hsh.get("ATTR_ShortURL") != null &&
		// !hsh.get("ATTR_ShortURL").equals("")){
		// return (String)hsh.get("ATTR_ShortURL");
		// }else{
		// Should use short url?
		return (String) hsh.get("ATTR_categoryName");
		// }
	}

	/**
	 * 
	 * @param moduleName
	 * @return - A hash of all category data that might be needed for shortURLs
	 */
	private Hashtable getCategoryData(String moduleName)
	{
		au.net.webdirector.common.datalayer.admin.db.DBaccess db = new au.net.webdirector.common.datalayer.admin.db.DBaccess();
		Hashtable hsh = new Hashtable();
		if (moduleName != null)
		{
			// Try/Catch incase ATTR_shortURL doesnt exist
			String cols[] = { "Category_id", "Category_ParentID", "folderLevel", "ATTR_categoryName", "ATTR_ShortURL" };
			String query = "select Category_id,Category_ParentID,folderLevel,ATTR_categoryName,ATTR_ShortURL from Categories_" + DatabaseValidation.encodeParam(moduleName);
			//Vector v = db.select(query, cols);
			// TODO validated
			Vector v = db.selectQuery(query);
			if (v == null || v.size() == 0)
			{
				String cols2[] = { "Category_id", "Category_ParentID", "folderLevel", "ATTR_categoryName" };
				query = "select Category_id,Category_ParentID,folderLevel,ATTR_categoryName from Categories_" + DatabaseValidation.encodeParam(ModuleName);
				//v = db.select(query, cols2);
				// TODO validated
				v = db.selectQuery(query);
			}
			if (v != null && v.size() != 0)
			{
				for (int i = 0; i < v.size(); i++)
				{
					Hashtable h = new Hashtable();
					String[] array = (String[]) v.get(i);
					h.put("Category_id", array[0]);
					h.put("Category_ParentID", array[1]);
					h.put("folderLevel", array[2]);
					h.put("ATTR_categoryName", array[3]);
					if (array.length == 5)
						h.put("ATTR_ShortURL", array[4]);
					else
						h.put("ATTR_ShortURL", "");
					hsh.put(array[0], h);
				}
			}
		}
		return hsh;
	}

	private static String replace(String url, String name)
	{
		int pos = url.lastIndexOf("(.+)");
		if (pos == -1)
		{
			return url;
		}
		return url.substring(0, pos) + name + url.substring(pos + "(.+)".length());
	}

	public static void main(String[] args)
	{
		String url = "/a/(.+)";
		url = replace(url, "c");
		url = replace(url, "b");
		System.out.println(url);
	}
}
