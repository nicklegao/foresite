package sbe.Search;

import java.util.Vector;

import org.apache.log4j.Logger;

import au.net.webdirector.common.Defaults;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import webdirector.db.DButils;
import webdirector.db.client.ClientDataAccess;
import webdirector.db.client.ClientFileUtils;

/**
 * Created by IntelliJ IDEA. User: CorpInteractive Date: 25/01/2008 Time:
 * 10:53:24 To change this template use File | Settings | File Templates.
 */
public class SearchUtils
{

	private Logger logger = Logger.getLogger(SearchUtils.class);
	private Defaults d = Defaults.getInstance();
	private String emptyrating = "<img src=\'/" + d.getWarName() + "/clientImages/ratings_emptyStar.gif\'>";
	private String halfrating = "<img src=\'/" + d.getWarName() + "/clientImages/ratings_halfStar.gif\'>";
	private String fullrating = "<img src=\'/" + d.getWarName() + "/clientImages/ratings_fullStar.gif\'>";
	private int[][] rating = { { 0, 1, 4 }, { 1, 0, 4 }, { 1, 1, 3 }, { 2, 0, 3 }, { 2, 1, 2 }, { 3, 0, 2 }, { 3, 1, 1 }, { 4, 0, 1 }, { 4, 1, 0 }, { 5, 0, 0 } };
	private DButils db = new DButils();
	private ClientFileUtils CFU = new ClientFileUtils();
	private ClientDataAccess cda = new ClientDataAccess();
	private String fileType = "";

	public void setFileType(String fileType)
	{
		this.fileType = fileType;
	}

	public SearchUtils()
	{


	}

	public String[] searchResults(String[] variable, String modulName, String id)
	{
		logger.info("exception In SearchResults method===>>>>>>>>>>>> ");
		String[] result = new String[1];
		String querry = "Select ";

		int index = 0;
		for (int i = 0; i < variable.length; i++)
		{
			if ((variable[i] != null) && (!variable[i].equals("")) && (!variable[i].equals(" ")))
			{
				index++;
				if (i < variable.length - 1)
					querry = querry.concat(variable[i] + ", ");
				else
					querry = querry.concat(variable[i] + " ");
			}
		}
		String[] cols = new String[index];
		for (int i = 0; i < index; i++)
		{
			cols[i] = variable[i];
		}
		String from = "from Elements_" + DatabaseValidation.encodeParam(modulName) + " ";
		String where = "where Element_id = ?";
		querry = querry + from + where;
		logger.info("LUC::Querry + ++++++++++++>>>>>" + querry);
		//Vector v = this.db.select(querry, variable);
		Vector v = this.db.selectQuery(querry, new String[] { id });
		if ((v != null) && (v.size() != 0))
		{
			result = (String[]) (String[]) v.get(0);
			result = createResult(result, variable, id);
			result = getResultBasedOnType(result, variable, id, modulName);
		}
		return result;
	}

	private String[] createResult(String results[], String[] variable, String id)
	{
		String resultBak[] = new String[variable.length];
		for (int i = 0; i < results.length; i++)
		{
			/*
			 * if(variable[i].startsWith("ATTRFILE") && results[i]!=null &&
			 * !results[i].equals("null")){ //<a
			 * href="<%="Reimplement me because download servlet is bad and doesnt exist anymore"+"?type=Reference Item
			 * File&id="+elementID+"&f="+file%>"><%=
			 * CFU.getFileNameFromStoreName(file) %></a>&nbsp;(<%=
			 * CFU.getFileSizeInStore(file) %>)
			 * resultBak[i]="<a href="+d.getDownloadServlet
			 * ()+"?type=Reference%20Item%20File&id="
			 * +id+"&f="+results[i]+">"+CFU
			 * .getFileNameFromStoreName(results[i])+"</a>&nbsp;("+
			 * CFU.getFileSizeInStore(results[i])+")";
			 * System.out.println("ResultBack"+resultBak[i]);
			 * System.out.println("ResultOriginal"+results[i]); } else
			 */
			resultBak[i] = results[i];
		}
		System.out.println("size of results" + resultBak.length);
		System.out.println("size of variable" + variable.length);
		for (int i = resultBak.length; i < variable.length; i++)
		{
			resultBak[i] = "";
		}
		return resultBak;
	}

	public String getRatingImagePath(int index)
	{
		int in = 9;
		String temp = "";
		for (int j = 0; j < rating[index - 1][0]; j++)
		{
			temp = temp.concat(fullrating);
		}
		for (int i = 0; i < rating[index - 1][1]; i++)
		{
			temp = temp.concat(halfrating);
		}
		for (int i = 0; i < rating[index - 1][2]; i++)
		{
			temp = temp.concat(emptyrating);
		}
		return temp;
	}

	private String[] getResultBasedOnType(String result[], String variable[], String id, String moduleName)
	{

		String StoreFile;
		String[] resultfinal = new String[variable.length];
		for (int i = 0; i < result.length; i++)
		{
			if (variable[i].startsWith("ATTRFILE") && fileType.equalsIgnoreCase("File"))
			{
				StoreFile = result[i].replaceAll(" ", "%20");
				resultfinal[i] = "<a href=" + "Reimplement me because download servlet is bad and doesnt exist anymore" + "?type=Reference%20Item%20File&id=" + id + "&f=" + StoreFile + ">" + CFU.getFileNameFromStoreName(result[i]) + "</a>&nbsp;(" + CFU.getFileSizeInStore(result[i]) + ")";
			}
			else if (variable[i].startsWith("ATTRFILE") && fileType.equalsIgnoreCase("Image"))
			{
				StoreFile = result[i].replaceAll(" ", "%20");
				String href = "<a href='" + cda.img(StoreFile) + "' rel=\"shadowbox[Aston Martin];options={slideshowDelay:3}\"> ";
				resultfinal[i] = cda.renderRichContent(cda.imgThumb(StoreFile), "", "", "border='0' ", false);
				resultfinal[i] = href + resultfinal[i] + "</a>";

			}
			else if (variable[i].startsWith("ATTRDATE"))
			{
				resultfinal[i] = cda.formatDate(result[i], "MMMM dd, yyyy");
			}
			else if (variable[i].startsWith("ATTRCHECK"))
			{
				if (result[i].equals("1"))
					resultfinal[i] = "true";
				else
					resultfinal[i] = "false";
			}
			else if (variable[i].startsWith("ATTRLONG"))
			{
				resultfinal[i] = "<a href = " + d.getWebSiteURL() + "/UsefullLinks/c_PrinterFriendly.jsp??moduleName=<%=categoryName%>&subCategoryName=" + moduleName + ">" + CFU.getFileNameFromStoreName(result[i]) + "</a>";
			}
			else if (variable[i].startsWith("ATTRCURRENCY"))
			{
				resultfinal[i] = Widgets.Text.Number.changePriceFormat(result[i]);
			}
			else
			{
				resultfinal[i] = result[i];
			}

		}
		return resultfinal;

	}
}
