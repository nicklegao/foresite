package sbe.Search;


/**
 * Created by IntelliJ IDEA.
 * User: CorpInteractive
 * Date: 30/01/2008
 * Time: 09:10:14
 * To change this template use File | Settings | File Templates.
 */
public class SearchItems
{
	private int docNumber;
	private String id;
	private float rating;
	private String module;
	private String categoryName;
	private String parentCategoryName;

	public SearchItems(int docNumber, String id, float rating, String module)
	{
		this.rating = rating;
		this.id = id;
		this.docNumber = docNumber;
		this.module = module;
	}

	public SearchItems(int docNumber, String id, float rating, String module, String categoryName, String parentCategoryName)
	{
		this.rating = rating;
		this.id = id;
		this.docNumber = docNumber;
		this.module = module;
		this.categoryName = categoryName;
		this.parentCategoryName = parentCategoryName;
	}

	public SearchItems()
	{

	}

	public String getCategoryName()
	{
		return categoryName;
	}

	public void setCategoryName(String categoryName)
	{
		this.categoryName = categoryName;
	}

	public String getParentCategoryName()
	{
		return parentCategoryName;
	}

	public void setParentCategoryName(String parentCategoryName)
	{
		this.parentCategoryName = parentCategoryName;
	}

	public String getModule()
	{
		return module;
	}

	public void setModule(String module)
	{
		this.module = module;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public float getRating()
	{
		return rating;
	}

	public void setRating(float rating)
	{
		this.rating = rating;
	}

	public int getDocNumber()
	{
		return docNumber;
	}

}
