package sbe.Search;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;

public class SecurityUtils
{

	public static Set<String> getAllPrivateCategoryIDsIncludingSubCategories(String moduleName)
	{
		String categorySql = "select Category_id, Category_ParentID, ATTRCHECK_Private from categories_" + DatabaseValidation.encodeParam(moduleName);
		Set<String> privateCategories = new HashSet<String>();
		DBaccess dba = new DBaccess();
		// TODO -- validated
		Vector categories = dba.selectQuery(categorySql);
		Map<String, String> parentMap = new HashMap<String, String>();
		for (int i = 0; i < categories.size(); i++)
		{
			String[] category = (String[]) categories.get(i);
			String categoryId = category[0];
			String parentCategoryId = category[1];
			String isPrivate = category[2];
			parentMap.put(categoryId, parentCategoryId);
			if ("1".equals(isPrivate))
			{
				privateCategories.add(categoryId);
			}
		}

		if (privateCategories.size() == 0)
		{
			return privateCategories;
		}

		for (String categoryId : parentMap.keySet())
		{
			if (privateCategories.contains(categoryId))
			{
				continue;
			}
			String parent = parentMap.get(categoryId);
			int count = 0;
			while (!"0".equals(parent) && parent != null && count < 10)
			{
				if (privateCategories.contains(parent))
				{
					privateCategories.add(categoryId);
					break;
				}
				parent = parentMap.get(parent);
				count++;
			}
		}

		return privateCategories;
	}

}
