package sbe.LocationSearch.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import au.net.webdirector.common.datalayer.admin.db.DBaccess;
import sbe.LocationSearch.model.LocationSearchEngineResponse;

public class LocationSearchEngineService
{

	private final String MODULE_NAME = "LOCATIONS";

	Logger logger = Logger.getLogger(LocationSearchEngineService.class);

	public List<LocationSearchEngineResponse> search(String input)
	{
		Connection conn = null;
		ResultSet rst = null;
		List<LocationSearchEngineResponse> list = new ArrayList<LocationSearchEngineResponse>();
		try
		{
			String q = getQuery(input);
			if (null != q)
			{
				DBaccess dba = new DBaccess();
				conn = dba.getDBConnection();
				PreparedStatement prst = conn.prepareStatement(q);
				rst = prst.executeQuery();
				while (rst.next())
				{
					LocationSearchEngineResponse location = loadFromRst(rst);
					if (null != location)
						list.add(location);
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			list = new ArrayList<LocationSearchEngineResponse>();
		}
		if (null != conn)
			try
			{
				conn.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		if (null != rst)
			try
			{
				rst.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}

		return list;
	}


	private String getQuery(String input)
	{
		try
		{
			String q = "select * from elements_" + MODULE_NAME + " where Live = 1 and ( Expire_date is null or Expire_date>now() ) and ( ";
			int postCode = -1;
			try
			{
				postCode = Integer.parseInt(input);
			}
			catch (Exception e)
			{
				postCode = -1;
			}
			if (postCode > 0)
			{
				q += " ATTR_PostCode = " + postCode;
			}
			else
			{
				q += " ATTR_Headline like  '%" + input + "%'";
				q += " or ATTR_Area like  '%" + input + "%'";
				q += " or ATTR_Address like  '%" + input + "%'";
				q += " or ATTR_Suburb like  '%" + input + "%'";
				q += " or ATTR_State like  '%" + input + "%' ";
			}

			q += ")";


			logger.info(q);
			return q;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}

	}

	private LocationSearchEngineResponse loadFromRst(ResultSet rst)
	{
		try
		{
			LocationSearchEngineResponse location = new LocationSearchEngineResponse();
			location.setElementId(rst.getInt("Element_id"));
			location.setCategoryId(rst.getInt("Category_id"));
			location.setAttrHeadline(rst.getString("ATTR_Headline"));
			location.setArea(rst.getString("ATTR_Area"));
			location.setAddress(rst.getString("ATTR_Address"));
			location.setSuburb(rst.getString("ATTR_Suburb"));
			location.setPostCode(rst.getInt("ATTR_postCode"));
			location.setState(rst.getString("ATTR_state"));
			location.setPhone(rst.getString("ATTR_phone"));
			location.setFax(rst.getString("ATTR_fax"));
			location.setEmail(rst.getString("ATTR_Email"));
			location.setUrl(rst.getString("ATTR_URl"));
			location.setGoogleMapAddress(rst.getString("ATTR_GoogleMapAddress"));
			location.setImage(rst.getString("ATTRFILE_Image1"));
			location.setManagerName(rst.getString("ATTR_ManagerName"));
			location.setManagerImage(rst.getString("ATTRFIle_ManagerImage"));
			return location;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}




	}


}
