package sbe.LocationSearch.model;

public class LocationSearchEngineResponse
{

	int elementId;
	int categoryId;
	String attrHeadline;
	String area;
	String address;
	String suburb;
	int postCode;
	String state;
	String phone;
	String fax;
	String email;
	String url;
	String googleMapAddress;
	String image;
	String managerName;
	String managerImage;



	public LocationSearchEngineResponse()
	{
		super();
	}

	public LocationSearchEngineResponse(int elementId, int categoryId,
			String attrHeadline, String area, String address, String suburb,
			int postCode, String state, String phone, String fax, String email,
			String url, String googleMapAddress, String image,
			String managerName, String managerImage)
	{
		super();
		this.elementId = elementId;
		this.categoryId = categoryId;
		this.attrHeadline = attrHeadline;
		this.area = area;
		this.address = address;
		this.suburb = suburb;
		this.postCode = postCode;
		this.state = state;
		this.phone = phone;
		this.fax = fax;
		this.email = email;
		this.url = url;
		this.googleMapAddress = googleMapAddress;
		this.image = image;
		this.managerName = managerName;
		this.managerImage = managerImage;
	}

	public int getElementId()
	{
		return elementId;
	}

	public void setElementId(int elementId)
	{
		this.elementId = elementId;
	}

	public int getCategoryId()
	{
		return categoryId;
	}

	public void setCategoryId(int categoryId)
	{
		this.categoryId = categoryId;
	}

	public String getAttrHeadline()
	{
		return attrHeadline;
	}

	public void setAttrHeadline(String attrHeadline)
	{
		this.attrHeadline = attrHeadline;
	}

	public String getArea()
	{
		return area;
	}

	public void setArea(String area)
	{
		this.area = area;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getSuburb()
	{
		return suburb;
	}

	public void setSuburb(String suburb)
	{
		this.suburb = suburb;
	}

	public int getPostCode()
	{
		return postCode;
	}

	public void setPostCode(int postCode)
	{
		this.postCode = postCode;
	}

	public String getState()
	{
		return state;
	}

	public void setState(String state)
	{
		this.state = state;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public String getFax()
	{
		return fax;
	}

	public void setFax(String fax)
	{
		this.fax = fax;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getGoogleMapAddress()
	{
		return googleMapAddress;
	}

	public void setGoogleMapAddress(String googleMapAddress)
	{
		this.googleMapAddress = googleMapAddress;
	}

	public String getImage()
	{
		return image;
	}

	public void setImage(String image)
	{
		this.image = image;
	}

	public String getManagerName()
	{
		return managerName;
	}

	public void setManagerName(String managerName)
	{
		this.managerName = managerName;
	}

	public String getManagerImage()
	{
		return managerImage;
	}

	public void setManagerImage(String managerImage)
	{
		this.managerImage = managerImage;
	}


}
