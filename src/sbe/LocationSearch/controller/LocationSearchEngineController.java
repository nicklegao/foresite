package sbe.LocationSearch.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sbe.LocationSearch.model.LocationSearchEngineResponse;
import sbe.LocationSearch.service.LocationSearchEngineService;

@Controller
public class LocationSearchEngineController
{

	@ResponseBody
	@RequestMapping(value = "/searchLocation", method = RequestMethod.GET)
	public List<LocationSearchEngineResponse> searchLocation(String input)
	{
		LocationSearchEngineService service = new LocationSearchEngineService();
		return service.search(input);
	}

}
