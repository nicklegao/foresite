import org.xhtmlrenderer.pdf.ITextRenderer;
import org.w3c.dom.Document;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Pattern;
import java.util.regex.Matcher;


/**
 * Created by IntelliJ IDEA.
 * User: CorpInteractive
 * Date: 15/04/2010
 * Time: 13:40:48
 * To change this template use File | Settings | File Templates.
 */
public class PDFConverter extends HttpServlet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7171120871863578155L;


	public void init()
	{

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("application/pdf");

		String url = request.getParameter("url");
		url = "http://demo.webdirector.net.au/SBE/client/GeneralContent/c_singleDynamicPage2.jsp?pageView=CONTENTONLY&pageName=test";
		StringBuffer buf = new StringBuffer(getURLContent(url));


		// parse our markup into an xml Document
		try
		{
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = builder.parse(new StringBufferInputStream(buf.toString()));
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocument(doc, null);
			renderer.layout();
			OutputStream os = response.getOutputStream();
			renderer.createPDF(os);
			os.close();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}



		/* StringBuffer buf = new StringBuffer();
		 buf.append("<html>");

		 String css = getServletContext().getRealPath("/PDFservlet.css");
		 // put in some style
		 buf.append("<head><link rel='stylesheet' type='text/css' "+
		         "href='"+css+"' media='print'/></head>");

		 //generate the rest of the HTML

		 // parse our markup into an xml Document
		 try {
		     DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		     Document doc = builder.parse(new StringBufferInputStream(buf.toString()));
		     ITextRenderer renderer = new ITextRenderer();
		     renderer.setDocument(doc, null);
		     renderer.layout();
		     OutputStream os = response.getOutputStream();
		     renderer.createPDF(os);
		     os.close();
		 } catch (Exception ex) {
		     ex.printStackTrace();
		 }*/
	}

	private String getURLContent(String url)
	{

		String st = "";
		try
		{
			URL hp = new URL(url);
			URLConnection hpCon = hp.openConnection();
			InputStream input = hpCon.getInputStream();
			st = convertStreamToString(input);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}


		return regex(st);
	}

	public String convertStreamToString(InputStream is) throws IOException
	{
		/*
		 * To convert the InputStream to String we use the BufferedReader.readLine()
		 * method. We iterate until the BufferedReader return null which means
		 * there's no more data to read. Each line will appended to a StringBuilder
		 * and returned as String.
		 */
		if (is != null)
		{
			StringBuilder sb = new StringBuilder();
			String line;

			try
			{
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				while ((line = reader.readLine()) != null)
				{
					sb.append(line).append("\n");
				}
			}
			finally
			{
				is.close();
			}
			return sb.toString();
		}
		else
		{
			return "";
		}
	}


	private String regex(String st)
	{
		Pattern pattern = Pattern.compile("</?(a|A).*?>");
		StringBuffer template = new StringBuffer(st);
		/*
		 * The Matcher class also don't have the public constructor so to create a matcher
		 * class the Patter's class matcher() method. The Matcher object it self is the engine
		 * that match the input string against the provided pattern.
		 */
		Matcher matcher = pattern.matcher(template);

		while (matcher.find())
		{
//            System.out.format("Text \"%s\" found at %d to %d.%n",matcher.group(), matcher.start(), matcher.end());
			template.replace(matcher.start(), matcher.end(), "");
			matcher.reset();
		}
		System.out.println("**** ST ***" + st);
		return st;
	}
	/*  private void  createPDF( HttpServletResponse response){
	      try {
	          Document document = new Document();
	          document.setPageSize(PageSize.A4.rotate());
	          OutputStream out1 = response.getOutputStream();
	          StringBuffer sb = new StringBuffer("test.html");
	          document.open();
	          PdfWriter.getInstance(document, out1);
	          HtmlParser.parse(document, sb.toString());
	             } catch (Exception ex) {
	                 ex.printStackTrace();
	             }
	      
	  }*/

	/* private void createPDF1(HttpServletResponse response){
	     Document document = new Document();
	     document.open();

	 try {
	     PdfWriter.getInstance(document, response.getOutputStream());
	     HtmlParser.parse(document,"test.html");

	 } catch (Exception e) {
	    e.printStackTrace();
	 }
	 }*/



}
