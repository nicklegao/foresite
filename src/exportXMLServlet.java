import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import sbe.ShortUrls.ShortUrlManager;
import webdirector.db.client.ClientDataAccess;
import au.net.webdirector.common.datalayer.admin.db.DataAccess;
import au.net.webdirector.common.datalayer.base.db.mapper.HashMapMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.HashtableMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.StringArrayMapper;
import au.net.webdirector.common.datalayer.base.db.mapper.StringMapper;
import au.net.webdirector.common.datalayer.util.DatabaseValidation;
import au.net.webdirector.common.datatypes.domain.DataType;
import au.net.webdirector.common.datatypes.service.DataTypesService;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

/**
 * Created by IntelliJ IDEA. User: CorpInteractive Date: 28/05/2008 Time:
 * 15:40:27 To change this template use File | Settings | File Templates.
 */
public class exportXMLServlet extends HttpServlet
{

	private DataAccess db = DataAccess.getInstance();

	private String categoryID = "";
	private String assetID = "";
	private String limit = "";
	private String orderCol = "";
	private String order = "";
	private String CategoryData = "";
	private String module = "";
	private String live = "";
	private String last = "";

	// Hash of attributes from labelstable which will contains internal and
	// external name base upon export selection
	// on editAttributes from web director.
	private HashMap<String, String> hAttributes;
	private ClientDataAccess cda = new ClientDataAccess();
	private String storeDir = null;
	private String root = "WebDirectorExport";
	private String root1 = "asset";
	private String url = "url";
	private au.net.webdirector.common.Defaults d = au.net.webdirector.common.Defaults.getInstance();
	private webdirector.db.client.ClientFileUtils CF = new webdirector.db.client.ClientFileUtils();
	Document dom;

	public void init()
	{
		System.out.println("exportXMLServlet.init()");
	}

	private void respondWithHelpMessage(HttpServletResponse response)
	{
		StringBuffer sb = new StringBuffer("<html><head><title>export.xml help</title></head><body><h1>export.xml helper text</h1>");

		sb.append("<br><br><br>");
		sb.append("Usage:<br><br>");
		sb.append("<table cellpadding=10 cellspacing=10>");
		sb.append("<tr><td>module=&lt;module name&gt;</td><td>Mandatory</td><td>example: module=INFORMATION</td></tr>");
		sb.append("<tr><td>live=[ 1 | 0 ]</td><td>Optional, defaults to 1</td><td>example: live=1</td></tr>");
		sb.append("<tr><td>assetID=&lt;asset id&gt;</td><td>Optional</td><td>example: assetID=62</td></tr>");
		sb.append("<tr><td>categoryID=&lt;category id&gt;</td><td>Optional</td><td>example: categoryID=38</td></tr>");
		sb.append("<tr><td>limit=&lt;number of rows to return&gt;</td><td>Optional</td><td>example: limit=5</td></tr>");
		sb.append("<tr><td>orderCol=&lt;Name of column to order by&gt;</td><td>Optional</td><td>example: orderCol=ATTR_Headline</td></tr>");
		sb.append("<tr><td>order=[ ASC | DESC ]</td><td>Optional (if no then order is ASC)</td><td>example: orderCol=no</td></tr>");
		sb.append("<tr><td>categoryData=[ yes | no ]</td><td>Optional (no is the default - setting it to yes exports category data also)</td><td>example: CategoryData=yes</td></tr>");
		sb.append("<tr><td>last=&lt;like limit but the last number of rows to return&gt;</td><td></td><td>example: last=20</td></tr>");
		sb.append("<table>");

		sb.append("<br><br>");

		sb.append("Full example: export.xml?module=INFORMATION&live=1&assetID=23");

		sb.append("</body></html>");
		response.setContentType("text/html; charset=utf-8");
		try
		{

			PrintWriter out = new PrintWriter(response.getOutputStream());
			out.print(sb.toString());
			out.close();
		}
		catch (Exception ex)
		{
			System.out.println("Exception occured writing back from treeServlet");
			ex.printStackTrace();
		}
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("in Doget Method");
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In dopost Method:::>>>>>>");

		String helpCheck = request.getParameter("help");
		if (helpCheck != null)
		{
			respondWithHelpMessage(response);
			return;
		}

		String module = request.getParameter("module");
		this.module = (module != null && !"".equals(module) && !"null".equalsIgnoreCase(module)) ? module : "";

		String live = request.getParameter("live");
		this.live = StringUtils.isNotBlank(live) ? live : "1";

		String categoryID = request.getParameter("categoryID");
		this.categoryID = StringUtils.isNotBlank(categoryID) ? categoryID : "";

		String assetID = request.getParameter("assetID");
		this.assetID = StringUtils.isNotBlank(assetID) ? assetID : "";

		String url = request.getParameter("url");
		this.url = StringUtils.isNotBlank(url) ? url : "";

		limit = request.getParameter("limit");
		orderCol = request.getParameter("orderCol");
		order = request.getParameter("order");
		CategoryData = request.getParameter("categoryData");
		last = StringUtils.isNotBlank(request.getParameter("last")) ? request.getParameter("last") : "0";

		// Get a DOM object
		createDocument();

		// initialize the list
		List<Map<String, String>> data = loadData();

		// Create XML file....
		createDOMTree(data);

		// Crate response
		printToFile(response);

		// response back to caller using iostream

	}

	/**
	 * Add a list of books to the list In a production system you might populate
	 * the list from a DB
	 */
	private List<Map<String, String>> loadData()
	{
		StringBuffer query = new StringBuffer("Select ");
		StringBuffer where = new StringBuffer(" where ");
		String from = " from Elements_" + module;

		// Hash of attributes from labelstable which will contains internal and
		// external name base upon export selection
		// on editAttributes from web director.
		hAttributes = new HashMap<String, String>();
		hAttributes.put("Live", "Live");
		hAttributes.put("Create_date", "Create Date");
		hAttributes.put("Category_id", "Category ID");
		hAttributes.put("Element_id", "Element ID");
		// get labels tables data for particular attributes to be add into XML
		DataTypesService dtS = new DataTypesService();
		List<DataType> vLabelVariable = dtS.loadGenericDataTypes(module, false, null);
		for (DataType labelVariables : vLabelVariable)
		{
			if ("1".equals(labelVariables.getExport()))
			{
				hAttributes.put(labelVariables.getInternalName(), labelVariables.getExternalName());
			}
		}
		// Build query and addd attributes into cols for later use
		String[] cols = hAttributes.keySet().toArray(new String[] {});
		query.append(StringUtils.join(cols, ","));
		where.append(" Live=" + live);
		if (!assetID.equals(""))
		{
			// vAssets = cda.getElement(module, assetID,live);
			where.append(" and Element_id=" + assetID);
		}
		else if (!categoryID.equals(""))
		{
			// vAssets = cda.getCategoryElements(module, categoryID, live);
			where.append(" and Category_id=" + categoryID);
		}

		String limitSql = "";
		String orderColSql = "";
		String orderSql = "";
		int lastLoop = 0;
		if (limit != null)
		{
			limitSql = " limit " + last + "," + limit;
		}
		if (orderCol != null)
		{
			orderColSql = " order by " + orderCol + " ";
		}
		if (order != null)
		{
			orderSql = " " + order + " ";
		}

		// run query and put data into hashtable with key value pair where key
		// is attribute internal name
		query = query.append(from).append(where).append(orderColSql).append(orderSql).append(limitSql);
		List<Map<String, String>> results = db.select(query.toString(), new HashMapMapper());
		return results;
	}

	/**
	 * Using JAXP in implementation independent manner create a document object
	 * using which we create a xml tree in memory
	 */
	private void createDocument()
	{

		// get an instance of factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try
		{
			// get an instance of builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			// create an instance of DOM
			dom = db.newDocument();

		}
		catch (ParserConfigurationException pce)
		{
			// dump it
			System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
			System.exit(1);
		}

	}

	/**
	 * The real workhorse which creates the XML structure
	 */
	private void createDOMTree(List<Map<String, String>> myData)
	{
		ShortUrlManager sum = ShortUrlManager.getInstance();
		// create the root element <Root>
		Element rootEle = dom.createElement(root);
		dom.appendChild(rootEle);

		// Run thorugh mydata vector which essentially contains all informations
		// including system coloumns of assets for XML .....
		for (int i = 0; i < myData.size(); i++)
		{
			HashMap hXMLData = (HashMap) myData.get(i);

			// Creating Asset element whcih is Root1 in our case
			Element asset = dom.createElement(root1);
			String id = hXMLData.get("Element_id").toString();
			System.out.println("Element_ID in crateDomTree " + id);
			String createDate = hXMLData.get("Create_date").toString();
			System.out.println("Create_date in crateDomTree " + createDate);
			String live = hXMLData.get("Live").toString();
			System.out.println("live in crateDomTree " + live);
			System.out.println("Module in crateDomTree " + module);

			// sets different arrribtes for particular node...
			asset.setAttribute("id", id);
			asset.setAttribute("createDate", createDate);
			asset.setAttribute("live", live);
			asset.setAttribute("module", module);
			if (CategoryData != null)
			{
				getCategoryFolderData(asset, hXMLData.get("Category_id").toString());
			}

			// Run through hAttributes hashtable which contains attributes
			// selected through cogs in webdirector...
			Iterator keys = hAttributes.keySet().iterator();
			while (keys.hasNext())
			{
				String key = (String) keys.next();
				System.out.println("Key in innerloop " + key);
				String assetName = (String) hAttributes.get(key);
				assetName = assetName.replaceAll("[^a-zA-Z0-9]", "_");
				System.out.println("assetName" + assetName);
				// Create new element for each atrributes in loop
				Element assetBody = dom.createElement(assetName);
				assetBody.setAttribute("internalName", key);
				if (key.startsWith("ATTRLONG") || key.startsWith("ATTRTEXT"))
				{
					Object o = hXMLData.get(key);
					if (o != null)
					{
						StringBuffer text = CF.readTextContents(CF.getFilePathFromStoreName(o.toString()));
						System.out.println("Text Value for ATTRLONG in XML" + text.toString());
						Text assetText = dom.createTextNode(text.toString());
						assetBody.appendChild(assetText);
					}
				}
				else if (key.startsWith("ATTRNOTES"))
				{
					String sql = "select id, createDate, note, who, content from notes where moduleName = ? and elementOrCategory = 'Elements' and foriegnKey = ? and fieldName like '" + key + "%'";
					List<Map<String, String>> notes = db.select(sql, new String[] { module, id }, new HashtableMapper());
					for (Map<String, String> note : notes)
					{
						Element noteBody = dom.createElement("Note");
						noteBody.setAttribute("id", note.get("id"));
						noteBody.setAttribute("createDate", note.get("createDate"));
						noteBody.setAttribute("who", note.get("who"));
						Text assetText = dom.createTextNode(note.get("content"));
						noteBody.appendChild(assetText);
						assetBody.appendChild(noteBody);
					}
				}
				else if (key.startsWith("ATTRMULTI"))
				{
					String sql = "select SELECTED_VALUE from drop_down_multi_selections where MODULE_NAME = ? and ELEMENT_ID = ? and ATTRMULTI_NAME = ? and MODULE_TYPE = 'element'";
					List<String> multi = db.select(sql, new String[] { module, id, key }, new StringMapper());
					Text assetText = dom.createTextNode(StringUtils.join(multi, "|").replaceAll("&amp;", "&"));
					assetBody.appendChild(assetText);
				}
				else
				{
					Object o = hXMLData.get(key);
					if (o != null)
					{
						String textValue = o.toString();
						System.out.println("Text Value in XML" + textValue);
						// Adding value of node using Text .....
						Text assetText = dom.createTextNode(textValue.replaceAll("&amp;", "&"));
						assetBody.appendChild(assetText);
					}
				}
				asset.appendChild(assetBody);
			}

			// this is special code to do with url argument which will add new
			// tag with value...
			if (url.equals("1") && module.equalsIgnoreCase("PRODUCTS"))
			{
				Element assetURL = dom.createElement("url");
				String textValue = d.getWebSiteURL() + sum.getAssetUrl(module, id);
				Text assetText = dom.createTextNode(textValue);
				assetURL.appendChild(assetText);
				asset.appendChild(assetURL);
			}

			rootEle.appendChild(asset);
		}

	}

	private void getCategoryFolderData(Element asset, String category_id)
	{
		module = DatabaseValidation.encodeParam(module);
		String sql = "select ATTR_categoryName, folderLevel, category_parentID from Categories_" + module + " where category_id = ?";
		String[] queryCols = { "ATTR_categoryName", "folderLevel", "category_parentID" };
		// Vector v = db.select(sql, queryCols);
		List<String[]> v = db.select(sql, new String[] { category_id }, new StringArrayMapper());

		String[] cols = (String[]) v.get(0);

		asset.setAttribute("categoryNameLevel" + cols[1], cols[0]); // cat name
		asset.setAttribute("categoryIdLevel" + cols[1], category_id); // cat id

		if (!cols[1].equals("1"))
		{
			getCategoryFolderData(asset, cols[2]);
		}
	}

	/**
	 * This method uses Xerces specific classes prints the XML document to file.
	 */
	private void printToFile(HttpServletResponse response)
	{

		try
		{
			// print
			OutputFormat format = new OutputFormat(dom);
			format.setIndenting(true);

			// to generate output to console use this serializer
			// XMLSerializer serializer = new XMLSerializer(System.out, format);

			// to generate a file output use fileoutputstream instead of
			// system.out
			// XMLSerializer serializer = new XMLSerializer(
			// new FileOutputStream(new File("c:/book.xml")), format);

			// serializer.serialize(dom);

			// this is will give response back to request .....
			XMLSerializer serializer = new XMLSerializer(response.getOutputStream(), format);

			serializer.serialize(dom);

		}
		catch (IOException ie)
		{
			ie.printStackTrace();
		}
	}

}
