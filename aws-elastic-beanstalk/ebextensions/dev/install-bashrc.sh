#!/bin/sh

grep ci-bashrc /home/ec2-user/.bashrc >/dev/null

if [ $? -eq 0 ]
then
    echo NO NEED TO SETUP
else
    echo source /home/ec2-user/ci-bashrc >> /home/ec2-user/.bashrc
    echo Installed
fi
