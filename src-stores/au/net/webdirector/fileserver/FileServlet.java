/**
 * @author Nick Yiming Gao
 * @date 2016-4-11
 */
package au.net.webdirector.fileserver;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.net.webdirector.common.utils.Encryptor;
import au.net.webdirector.common.utils.StreamGobbler;

/**
 * 
 */

public class FileServlet extends HttpServlet
{
	private static Logger logger = Logger.getLogger(FileServlet.class);

	private static String imageMagickInstallDir;
	private static String storeDir;

	@Override
	public void init(ServletConfig config)
	{
		imageMagickInstallDir = config.getServletContext().getInitParameter("imageMagickInstallDir");
		storeDir = config.getServletContext().getInitParameter("storeDir");
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		String url = URLDecoder.decode(request.getRequestURI(), "UTF-8").substring(request.getContextPath().length());
		if (url.startsWith("/stores"))
		{
			url = url.substring("/stores".length());
		}
		String imageSize = request.getParameter("image-size");

		if (imageSize != null)
		{
			String reversed = new StringBuilder(url).reverse().toString();
			String cropped = StringUtils.replaceOnce(reversed, new StringBuilder(imageSize + "/").reverse().toString(), "");
			url = new StringBuilder(cropped).reverse().toString();
		}

		File file = new File(storeDir, url);
		if (!file.exists() || !file.isFile())
		{
			OutputStream out = response.getOutputStream();
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			out.write(new String("Cannot find: " + url).getBytes());
			out.flush();
			return;
		}
		if (imageSize != null)
		{
			String imageMagicSize = imageSize;
			if (imageSize.endsWith("-fit"))
			{
				imageMagicSize = imageSize.replace("-fit", "^");
			}
			File temp = new File(storeDir, "/_cached" + url);
			File cachedFile = new File(temp.getParent() + "/" + imageSize, temp.getName());
			file = cacheFile(file, cachedFile, imageMagicSize);
		}
		if (request.getDateHeader("If-Modified-Since") > 0 && request.getDateHeader("If-Modified-Since") / 1000 >= file.lastModified() / 1000)
		{
			response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
			return;
		}

		InputStream in = null;
		OutputStream out = null;
		File decrypted = null;
		try
		{
			out = response.getOutputStream();
			response.setDateHeader("Last-Modified", file.lastModified());
			if(!Encryptor.isEncrypted(file))
			{
				decrypted = file;
			}else{
				decrypted = Encryptor.createTempDecryptFile(file);
			}
			
			Path path = decrypted.toPath();
			response.setContentType(Files.probeContentType(path));
			response.setContentLength((int) decrypted.length());

			in = Files.newInputStream(path, StandardOpenOption.READ);
			IOUtils.copy(in, out);
		}
		catch (NoSuchFileException e)
		{
			logger.error("Error:", e);
			out = response.getOutputStream();
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			out.write(new String("Cannot find: " + url).getBytes());
			return;
		}
		catch (AccessDeniedException e)
		{
			logger.error("Error:", e);
			out = response.getOutputStream();
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			out.write(new String("Access denied: " + url).getBytes());
			return;
		}
		finally
		{
			out.flush();
			IOUtils.closeQuietly(in);
			if(file != null && decrypted != null && file !=decrypted){
				FileUtils.deleteQuietly(decrypted);
			}
		}

	}

	protected File cacheFile(File file, File cachedFile, String imageSize)
	{
		if (cachedFile.exists() && cachedFile.lastModified() >= file.lastModified())
		{
			return cachedFile;
		}
		if (!cachedFile.getParentFile().exists())
		{
			cachedFile.getParentFile().mkdirs();
		}
		if (resize(file, imageSize, cachedFile))
		{
			return cachedFile;
		}
		logger.error("Cannot resize image:" + file);
		return file;
	}

	private boolean resize(File source, String imageSize, File dest)
	{
		int exitVal = 0;
		try
		{
			String[] cmd = new String[5];

			cmd[0] = imageMagickInstallDir + "/convert";
			cmd[1] = source.getAbsolutePath();
			cmd[2] = "-resize";
			cmd[3] = imageSize;// "64x64"/"64x64^"/"64x"/"x64";
			cmd[4] = dest.getAbsolutePath();

			Runtime rt = Runtime.getRuntime();
			logger.info("Executing " + StringUtils.join(cmd, " "));
			Process proc = rt.exec(cmd);
			// any error message?
			StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "ERROR");

			// any output?
			StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "OUTPUT");

			// kick them off
			errorGobbler.start();
			outputGobbler.start();

			// any error???
			exitVal = proc.waitFor();
			logger.info("ExitValue: " + exitVal);
		}
		catch (Throwable t)
		{
			t.printStackTrace();
		}

		if (exitVal == 0)
			return true;
		else
			return false;
	}
}
